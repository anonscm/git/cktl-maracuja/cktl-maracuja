

Pb : l'IM porte sur la facture, pas sur le mandat. Un mandat doit etre pay� en globalit�, donc si le comptable decide de saisir une suspension de DGP sur une des factures du mandat, 
les autres factures ne peuvent �tre pay�es que si le mandat est pris en charge. Donc si une suspension est n�cessaire il y a plusieurs possibilit�s :
- la saisir (rattach�e � la facture) et rejeter le mandat (sans supprimer les factures) en indiquant � l'ordonnateur de cr�er un nouveau mandat avec les factures ne n�cessitant pas de suspension.
- ou bien ne pas viser le bordereau tant que la suspension est active, 
- ou bien viser le mandat et le mettre en attente de paiement, au besoin faire un OP pour payer les autres factures au fournisseur...


Part comptable / part Ordo : le pb se r�glera au moment de l'engagement des IM par l'ordo, l'application ne propose rien la dessus.




