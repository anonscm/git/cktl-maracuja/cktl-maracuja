select ORI_KEY_ENTITE as conv_ordre, ex.exe_ordre, ex.EXE_EXERCICE, ecd.pco_num, sum(ecd_credit) as credit, sum(ecd_debit) as debit 
from ecriture_detail ecd, ecriture e, exercice ex, origine o 
where 
e.ecr_ordre=ecd.ecr_ordre
and e.exe_ordre=ex.exe_ordre
and e.ecr_etat='VALIDE'
and e.ori_ordre=o.ori_ordre
and ori_entite='CONVENTION.CONTRAT'
group by ORI_KEY_ENTITE,ex.exe_ordre, ex.EXE_EXERCICE, ecd.pco_num