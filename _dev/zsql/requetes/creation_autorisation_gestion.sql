--drop sequence Autorisation_gestion_SEQ
/

--drop table Autorisation_gestion cascade constraints
/

CREATE TABLE Autorisation_gestion (aug_ordre NUMBER(38) NOT NULL, aut_ORDRE NUMBER NOT NULL, ges_Code VARCHAR2(10) NOT NULL)
/

create table eo_temp_table as select max(aug_ordre) counter from Autorisation_gestion
/

create sequence Autorisation_gestion_SEQ
/

create procedure eo_set_sequence is
    xxx number;
    yyy number;
begin
    select max(counter) into xxx from eo_temp_table;
    if xxx is not NULL then
        yyy := 0;
        while (yyy < xxx) loop
            select Autorisation_gestion_SEQ.nextval into yyy from dual;
        end loop;
    end if;
end;

/

begin eo_set_sequence; end;
/

drop procedure eo_set_sequence
/

drop table eo_temp_table
/

ALTER TABLE Autorisation_gestion ADD PRIMARY KEY (aug_ordre)
/

ALTER TABLE Autorisation_gestion ADD CONSTRAINT Aug_autorisation_FK FOREIGN KEY (aut_ORDRE) REFERENCES Autorisation (aut_ORDRE) DEFERRABLE INITIALLY DEFERRED
/

ALTER TABLE Autorisation_gestion ADD CONSTRAINT Aug_gestion_FK FOREIGN KEY (ges_Code) REFERENCES Gestion (ges_Code) DEFERRABLE INITIALLY DEFERRED
/
