CREATE OR REPLACE FORCE VIEW JEFY05.BUDGEST_DESTIN
(ORG_COMP, TCD_CODE, DESTINATION, PREVISION, EXECUTION)
AS 
select org_comp, tcd_code, substr(d.dst_code,1,2) destination, sum(bdg_credit) prevision, sum(bdg_reel) execution 
from destin_budgest2 d, organ o 
where d.org_ordre = o.org_ordre 
and org_niv >= 2 
group by org_comp, tcd_code, substr(d.dst_code,1,2);