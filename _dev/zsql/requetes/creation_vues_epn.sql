-- vues maracuja pour EPN 



CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECRITURE_INFOS
(ECD_ORDRE, ECR_SACD)
AS 
select ecd_ordre, to_char('O') as ecr_sacd from ecriture_detail where ges_code in (select g.ges_code from gestion g where g.PCO_NUM_185 is not null)
union all
select ecd_ordre, to_char('N') as ecr_sacd from ecriture_detail where ges_code not in (select g.ges_code from gestion g where g.PCO_NUM_185 is not null);


CREATE OR REPLACE FORCE VIEW MARACUJA.VCREDIT
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_credit) montant
	FROM ECRITURE_detail e, ecriture j, PLAN_comptable p, v_ECRITURE_INFOS ei, exercice ex
	WHERE e.pco_num = p.pco_num
	AND e.ecd_sens = 'C'
	AND e.ecr_ordre = j.ecr_ordre
	AND j.tjo_ordre <> 2
	and j.exe_ordre=ex.exe_ordre
	AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
	GROUP BY ex.exe_exercice,j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VCREDIT_BE
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_credit) montant
	FROM ECRITURE_detail e, ecriture j, PLAN_comptable p, v_ECRITURE_INFOS ei, exercice ex
	WHERE e.pco_num = p.pco_num
	AND e.ecd_sens = 'C'
	AND e.ecr_ordre = j.ecr_ordre
	AND j.tjo_ordre = 2
	and j.exe_ordre=ex.exe_ordre
	AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
	GROUP BY ex.exe_exercice,j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VDEBIT
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_debit) montant
	FROM ECRITURE_detail e, ecriture j, PLAN_comptable p, v_ECRITURE_INFOS ei, exercice ex
	WHERE e.pco_num = p.pco_num
	AND e.ecd_sens = 'D'
	AND e.ecr_ordre = j.ecr_ordre
	AND j.tjo_ordre <> 2
	and j.exe_ordre=ex.exe_ordre
	AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
	GROUP BY ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VDEBIT_BE
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_debit) montant
	FROM ECRITURE_detail e, ecriture j, PLAN_comptable p, v_ECRITURE_INFOS ei, exercice ex
	WHERE e.pco_num = p.pco_num
	AND e.ecd_sens = 'D'
	AND e.ecr_ordre = j.ecr_ordre
	AND j.tjo_ordre = 2
	and j.exe_ordre=ex.exe_ordre
	AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
	GROUP BY ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VBALANCE0
(EXE_EXERCICE, JOU_DATE, IMPUTATION, GESTION, LIBELLE, 
 ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB)
AS 
SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
	montant BE_CR,0 MONTANT_CR,0 BE_DB,0 MONTANT_DB
	FROM Vcredit_be
	UNION ALL
	SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
	0 BE_CR,montant MONTANT_CR,0 BE_DB,0 MONTANT_DB
	FROM Vcredit
	UNION ALL
	SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
	0 BE_CR,0 MONTANT_CR,montant BE_DB,0 MONTANT_DB
	FROM Vdebit_be
	UNION ALL
	SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
	0 BE_CR,0 MONTANT_CR,0 BE_DB,montant MONTANT_DB
	FROM Vdebit;


