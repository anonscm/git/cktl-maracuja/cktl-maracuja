
-- correction des donnees suite a un paiement pass� deux fois (double paiement + ecritures de paiement doublees).


-- 1. lancer cette requete, recuperer la liste des ecr_ordre 
-- requete pour obtenir la liste des ecr_ordre cr��es en doublon par erreur lors d'un paiement
-- remplacer le pai_ordre par celui affect� aux mandats. Il doit normalement rester un enregistrement dans paiement (non reli�s � des mandats) qui est inutile.

-----------------------------------------------------------------------------------------------------
-- ecriture en doublons liees a un paiement
select distinct ecr_ordre from ecriture_detail where ecd_ordre in (
    select ecd_ordre from mandat_detail_ecriture where mde_origine='VIREMENT' and man_id in (select man_id from mandat where pai_ordre=421)
    union 
    select ecd_ordre from odpaiem_detail_ecriture where ope_origine='VIREMENT' and odp_ordre in (select odp_ordre from ordre_de_paiement where pai_ordre=421)
    )     
minus        
select distinct ecr_ordre from (
select ecd_libelle, pco_num, ges_code, min(e.ecr_ordre) ecr_ordre
from ecriture e, ecriture_detail ecd 
where ecd.ecr_ordre=e.ecr_ordre 
and e.ecr_libelle like 'PAIEMENT%'
and ecd_ordre in (
    select ecd_ordre from mandat_detail_ecriture where mde_origine='VIREMENT' and man_id in (select man_id from mandat where pai_ordre=421)
    union 
    select ecd_ordre from odpaiem_detail_ecriture where ope_origine='VIREMENT' and odp_ordre in (select odp_ordre from ordre_de_paiement where pai_ordre=421)
    ) 
group by ecd_libelle, pco_num, ges_code
having count(*) >1
)
order by ecr_ordre

--------------------------------------------------------------
2. avec la liste des ecr_ordre, creer le script suivant

declare 
	pai_ordre_ok number;
	pai_ordre_ko number;
	

BEGIN 
	pai_ordre_ok := 421;
	pai_ordre_ko := 420;
	
-- script pour suppression des ecritures en double suite pb paiement

-- suppression du paiement inutile (verifier qu'il n'est pas reference dans la table mandat ou ordre_de_paiement) pai_ordre=pai_ordre_ko
delete from maracuja.virement_fichier where pai_ordre=pai_ordre_ko;
delete from maracuja.paiement where pai_ordre=pai_ordre_ko;
commit;

-- suppression des mandat_detail_ecriture
delete from maracuja.mandat_detail_ecriture where ecd_ordre in (
select ecd_ordre from maracuja.ecriture_detail where ecr_ordre in (
-- mettre la liste des ecr_ordre ici. Attention, le nombre d'elements autoris� dans la liste est limit� (repeter l'instruction par tranches de 500 elements au besoin)
xxxx,
yyyy
);

commit;


-- annuler les ecritures en double. Repeter l'instruction pour chaque ecr_ordre

MARACUJA.UTIL.CREER_ECRITURE_ANNULATION(xxxxx);
MARACUJA.UTIL.CREER_ECRITURE_ANNULATION(yyyyy);


commit;

MARACUJA.NUMEROTATIONOBJECT.NUMEROTER_PAIEMENT (pai_ordre_ok);
COMMIT; 

MARACUJA.AFAIREAPRESTRAITEMENT.APRES_PAIEMENT (pai_ordre_ok);
  COMMIT; 
  
  
end; 