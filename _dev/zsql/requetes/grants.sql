grant execute on JEFY.MAJ_MANDAT_REJET to MARACUJA;
grant execute on JEFY.MAJ_TITRE_REJET to MARACUJA;
grant select on CONVENTION.CONTRAT to MARACUJA;
grant select on GRHUM.BANQUE to MARACUJA;
grant select on GRHUM.INDIVIDU_ULR to MARACUJA;
grant select on GRHUM.RIBFOUR_ULR to MARACUJA;
grant select on GRHUM.V_FOURNIS_GRHUM to MARACUJA;
grant select on JEFY.AGENT to MARACUJA;
grant select on JEFY.BORDERO to MARACUJA;
grant select on JEFY.BORDERO_REJET to MARACUJA;
grant select on JEFY.COMMANDE to MARACUJA;
grant select on JEFY.ENGAGE to MARACUJA;
grant select on JEFY.FACTURE to MARACUJA;
grant select on JEFY.FACTURES to MARACUJA;
grant select on JEFY.FACTURE_EXT to MARACUJA;
grant select on JEFY.FACTURE_REJET to MARACUJA;
grant select on JEFY.MANDAT to MARACUJA;
grant select on JEFY.MANDAT_AUTRES to MARACUJA;
grant select on JEFY.MODPAY to MARACUJA;
grant select on JEFY.ORGAN to MARACUJA;
grant select on JEFY.PAPAYE_COMPTA to MARACUJA;
grant select on JEFY.RIBFOUR_EXT to MARACUJA;
grant select on JEFY.TAUX to MARACUJA;
grant select on JEFY.TITRE to MARACUJA;
grant select on JEFY.VENTIL_TITRE to MARACUJA;
grant select on JEFY04.GESTION to MARACUJA;
grant select on JEFY04.MODPAY to MARACUJA;
grant select on JEFY04.PARAMETRES to MARACUJA;
grant select on JEFY04.PLANCO_CREDIT to MARACUJA;
grant select on JEFY05.DISK_PARAM to MARACUJA;
grant select on JEFY05.ETEBAC_PARAM_DETAIL to MARACUJA;
grant select on JEFY05.ETEBAC_PARAM_ENTETE to MARACUJA;
grant select on JEFY05.ETEBAC_PARAM_TOTAL to MARACUJA;
grant select on JEFY05.GESTION to MARACUJA;
grant select on JEFY05.MODPAY to MARACUJA;
grant select on JEFY05.PARAMETRES to MARACUJA;
grant select on JEFY05.PLANCO to MARACUJA;
grant select on JEFY05.PLANCO_CREDIT to MARACUJA;
grant select on JEFY05.PLANCO_VISA to MARACUJA;
grant select on JEFY05.TYPCRED to MARACUJA;
grant select on MARCHES.ATTRIBUTION to MARACUJA;
grant select on MARCHES.LOT to MARACUJA;
grant select on MARCHES.MARCHE to MARACUJA;
grant select on MISSIONS_NEW.WEB_PAYS to MARACUJA;
grant select on PAPAYE.JEFY_ECRITURES to MARACUJA;
grant select on PAPAYE.PAYE_MOIS to MARACUJA;
grant select on PAPAYE.PAYE_PARAMETRES to MARACUJA;


grant insert, update on jefy.mandat to Maracuja;
grant INSERT on jefy.FACTURE_REJET to Maracuja;
grant INSERT on jefy.BORDERO_REJET to Maracuja;
grant insert, update on jefy.titre to Maracuja;
grant update on jefy.papaye_compta to Maracuja;
grant update on jefy.BORDERO to Maracuja;