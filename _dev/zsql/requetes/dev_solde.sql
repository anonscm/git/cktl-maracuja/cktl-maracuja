-- devsolde
/* Formatted on 2008/03/17 10:58 (Formatter Plus v4.8.8) */

SELECT   ecr_numero, ecr_date, ecr_libelle, ecd_credit, ecd_debit,
         ecd_reste_emarger, ecd_libelle, ecd_montant, ges_code, exe_ordre,
         pco_num, pco_libelle, tot_debit, tot_credit
    FROM (SELECT ecr_numero, ecr_date_saisie AS ecr_date, ecr_libelle,
                 ecd_credit, ecd_debit, ecd_reste_emarger, ecd_libelle,
                 ecd_montant, ecriture_detail.ges_code,
                 ecriture_detail.exe_ordre, ecriture_detail.pco_num,
                 x.pco_libelle, x.tot_debit, x.tot_credit
            FROM ecriture,
                 ecriture_detail,
                 (SELECT   comptabilite.com_ordre, ecriture_detail.exe_ordre,
                           plan_comptable.pco_num, plan_comptable.pco_libelle,
                           SUM (ecd_debit) AS tot_debit,
                           SUM (ecd_credit) AS tot_credit
                      FROM comptabilite,
                           ecriture_detail,
                           ecriture,
                           plan_comptable
                     WHERE ecriture.com_ordre = comptabilite.com_ordre
                       AND ecriture_detail.ecr_ordre = ecriture.ecr_ordre
                       AND plan_comptable.pco_num = ecriture_detail.pco_num
                       AND ecriture.ecr_numero <> 0
                       AND SUBSTR (ecriture.ecr_etat, 1, 1) = 'V'
                       AND TO_CHAR (ecriture.ecr_date_saisie, 'YYYYMMDD') <
                                                                    '20080101'
                       AND (ecriture.exe_ordre = 2007)
                       AND (ecriture.com_ordre = 1)
                       AND (ecriture_detail.pco_num LIKE '4%')
                  GROUP BY comptabilite.com_ordre,
                           ecriture_detail.exe_ordre,
                           plan_comptable.pco_num,
                           plan_comptable.pco_libelle) x
           WHERE ecriture.com_ordre = x.com_ordre
             AND (ecriture.ecr_numero <> 0)
             AND ecriture_detail.exe_ordre = x.exe_ordre
             AND ecriture_detail.pco_num = x.pco_num
             AND ecriture.ecr_ordre = ecriture_detail.ecr_ordre
             AND SUBSTR (ecriture.ecr_etat, 1, 1) = 'V'
             AND ecriture_detail.ecd_reste_emarger <> 0
             AND TO_CHAR (ecriture.ecr_date_saisie, 'YYYYMMDD') < '20080101')
ORDER BY pco_num, ecr_numero