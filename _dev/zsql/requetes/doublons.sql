
select e.*,ecd.*  
from ecriture_detail ecd,ecriture e, 
(
select ecd1.ecd_ordre, ecd2.ecd_ordre as ecd_ordre2
from ecriture_detail ecd1,ecriture e1,
ecriture_detail ecd2, ecriture e2
where
e1.ecr_ordre=ecd1.ecr_ordre
and e2.ecr_ordre=ecd2.ecr_ordre
and ecd1.exe_ordre=2009
and ecd1.ecd_ordre <> ecd2.ecd_ordre
and ecd1.exe_ordre = ecd2.exe_ordre
  and ecd1.ges_code=ecd2.ges_code
  and ecd1.pco_num=ecd2.pco_num
  and ecd1.ecd_libelle=ecd2.ecd_libelle  
  and ecd1.ecd_debit=ecd2.ecd_debit
  and ecd1.ecd_credit=ecd2.ecd_credit
  and e1.ecr_libelle= e2.ecr_libelle
  and ecd1.ecd_ordre in (  
    select ecd_ordre from ecriture_detail where exe_ordre=2009
    minus 
    select ecd1.ecd_ordre
      from ecriture_detail ecd1, v_emargement_detail dem, ecriture_detail ecd2 
      where 
      ecd1.ecd_ordre = dem.ecd_ordre
      and ecd1.exe_ordre=2009
      and ecd1.exe_ordre=ecd2.exe_ordre
      and dem.ecd_ordre_em = ecd2.ecd_ordre
      and ecd1.ecd_credit=-ecd2.ecd_credit
      and ecd1.ecd_debit=-ecd2.ecd_debit   
)
) x
where 
ecd.ecr_ordre=e.ecr_ordre
and (ecd.ecd_ordre=x.ecd_ordre or ecd.ecd_ordre = x.ecd_ordre2)
order by pco_num, ges_code, ecd_libelle, ecd.ecd_ordre