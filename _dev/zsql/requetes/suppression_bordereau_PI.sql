
update jefy_depense.depense_ctrl_planco set man_id = null where man_id in (select man_id from mandat where bor_id in (16623, 16624));

update jefy_recette.recette_ctrl_planco set tit_id = null where tit_id in (select tit_id from titre where bor_id in (16623, 16624));

delete from titre_brouillard where tit_id in (select tit_id from titre where bor_id in (16623, 16624));
delete from recette where tit_id in (select tit_id from titre where bor_id in (16623, 16624));
delete from titre where bor_id in (16623, 16624);
delete from mandat_brouillard where man_id in (select man_id from mandat where bor_id in (16623, 16624));
delete from depense where man_id in (select man_id from mandat where bor_id in (16623, 16624));
delete from mandat where bor_id in (16623, 16624);
delete from bordereau where bor_id in (16623, 16624);