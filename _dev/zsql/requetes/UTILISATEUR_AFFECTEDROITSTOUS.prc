CREATE OR REPLACE PROCEDURE utilisateur_affecteDroitsTous(utlOrdre INTEGER)

IS
fonordre INTEGER;
fonspecgestion fonction.fon_spec_gestion%TYPE; 
gescode gestion.ges_code%TYPE;
autordre autorisation.aut_ordre%type;
augordre autorisation_gestion.aug_ordre%type;

CURSOR c1 IS SELECT fon_ordre, fon_spec_gestion FROM fonction;
CURSOR c2 IS SELECT ges_code FROM gestion;


BEGIN

-- on supprime les ancienne autorisations
delete from autorisation_gestion where aut_ordre in (select distinct aut_ordre from autorisation where utl_ordre = utlOrdre);
delete from autorisation where utl_ordre = utlOrdre;



OPEN c1;
LOOP
	FETCH C1 INTO fonordre, fonspecgestion;
		EXIT WHEN c1%NOTFOUND;
		select autorisation_seq.nextval into autordre from dual;
		--table autorisation
		insert into autorisation (aut_ordre, fon_ordre, utl_ordre) values (autordre, fonordre,utlordre);
		
		if fonspecgestion='O' then		
			open c2;			  
			loop
				 fetch c2 into gescode; 
				 	   exit when c2%notfound;
			      select autorisation_gestion_seq.nextval into augordre from dual; 
				  insert into autorisation_gestion (AUG_ORDRE, AUT_ORDRE, GES_CODE) values (augordre, autordre, gescode);
				  
			END LOOP;
			CLOSE c2;					
		end if;
END LOOP;
CLOSE c1;


END;
/

