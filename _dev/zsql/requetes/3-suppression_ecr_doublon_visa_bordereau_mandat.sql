-- récupérer le bor_id du bordereau incriminé et le passer en parametre de la procedure suivante
create procedure maracuja.supp_ecr_doublon_visa_mandat(borid integer) is

  ecrordre integer;
  cursor ecrOrdreDoublon(bid integer) is 
    -- recupération des ecritures en doublon : créer une liste à partir des ecr_ordre qui servira aux requetes suivantes
    select ecr_ordre from maracuja.ecriture_detail where ecd_ordre in (select ecd_ordre from maracuja.mandat_detail_ecriture mde 
    inner join maracuja.mandat m on m.man_id=mde.man_id where bor_id=bid)
    minus
    select ecr_ordre from (
    select m.man_id, min(ecr_ordre) as ecr_ordre
    from maracuja.ecriture_detail ecd 
    inner join maracuja.mandat_detail_ecriture mde on (ecd.ecd_ordre=mde.ecd_ordre) 
    inner join maracuja.mandat m on m.man_id=mde.man_id
    where bor_id=bid
    group by  m.man_id)
    order by ecr_ordre;
    
    flag integer;

begin

  -- verification que les écritures ne sont pas émargées. Cette requete ne doit pas renvoyer de valeur, sinon il y a des opérations qui ont été effectuées depuis le visa.
  select count(*) into flag 
  from maracuja.ecriture_detail where ecd_ordre in (select ecd_ordre from maracuja.mandat_detail_ecriture mde 
  inner join maracuja.mandat m on m.man_id=mde.man_id where bor_id=borid) and ecd_reste_emarger<>abs(ecd_montant);

  if (flag>0) then
    raise_application_error (-20001, 'Certaines ecritures du visa ont été émargées, impossible de supprimer les doublons.');
  end if;

    open ecrOrdreDoublon (borid);
      loop
         fetch ecrOrdreDoublon
         into  ecrordre;

         exit when ecrOrdreDoublon%notfound;

          -- supprimer les mandat_detail_ecriture pour les ecritures en doublon
        delete from maracuja.mandat_detail_ecriture where ecd_ordre in (select ecd_ordre from maracuja.ecriture_detail where ecr_ordre =ecrordre);
    
        -- créer des ecritures d'annulation pour les ecritures en doublon
        maracuja.util.creer_ecriture_annulation(ecrordre);
      end loop;
      close ecrOrdreDoublon;

end;