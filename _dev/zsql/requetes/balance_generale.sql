/* Formatted on 2008/03/17 10:57 (Formatter Plus v4.8.8) */
SELECT   SUBSTR (ecriture_detail.pco_num, 1, 1) AS classe,
         SUBSTR (ecriture_detail.pco_num, 1, 2) AS chapitre,
         ecriture_detail.ges_code, ecriture_detail.pco_num,
         ecriture_detail.exe_ordre, plan_comptable.pco_libelle,
         SUM (ecriture_detail.ecd_debit) ecd_debit,
         SUM (ecriture_detail.ecd_credit) ecd_credit,
         SUM (DECODE (ecriture.tjo_ordre, 2, ecd_debit, 0)) ecd_debit_be,
         SUM (DECODE (ecriture.tjo_ordre, 2, ecd_credit, 0)) ecd_credit_be,
         SUM (DECODE (ecriture.tjo_ordre, 2, 0, ecd_debit)) ecd_debit_exe,
         SUM (DECODE (ecriture.tjo_ordre, 2, 0, ecd_credit)) ecd_credit_exe
    FROM ecriture, ecriture_detail, plan_comptable, type_journal
   WHERE ecriture.ecr_ordre = ecriture_detail.ecr_ordre
     AND ecriture_detail.pco_num = plan_comptable.pco_num
     AND ecriture.tjo_ordre = type_journal.tjo_ordre
     AND TO_CHAR (ecriture.ecr_date_saisie, 'YYYYMMDD') < '20080101'
     AND ecriture.ecr_numero > 0
     AND SUBSTR (ecriture.ecr_etat, 1, 1) = 'V'
     AND (ecriture.exe_ordre = 2007)
     AND (ecriture.com_ordre = 1)
GROUP BY SUBSTR (ecriture_detail.pco_num, 1, 1),
         SUBSTR (ecriture_detail.pco_num, 1, 2),
         ecriture_detail.ges_code,
         ecriture_detail.pco_num,
         ecriture_detail.exe_ordre,
         plan_comptable.pco_libelle
ORDER BY exe_ordre, classe, ges_code, chapitre, pco_num