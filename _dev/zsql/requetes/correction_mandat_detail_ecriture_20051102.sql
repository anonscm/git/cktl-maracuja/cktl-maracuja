-- corrige la perte des mandat_detail_ecriture

insert INTO mandat_detail_ecriture (ECD_ORDRE, EXE_ORDRE, MAN_ID, MDE_DATE, MDE_ORDRE, MDE_ORIGINE, ORI_ORDRE) 
select ecd_ordre, e.exe_ordre, x.man_id, to_date('10/02/2005', 'dd/mm/yyyy'), MANDAT_DETAIL_ECRITURE_SEQ.nextval,'VISA',null 
from ecriture_detail ecd, ecriture e, 
(
	select man_id, man_numero, bor_num, b.ges_code, m.MAN_TTC from mandat m, bordereau b where b.bor_id=m.bor_id and  m.man_id in (
		select man_id from mandat m, bordereau b, type_bordereau tb 
			where m.bor_id=b.bor_id and b.tbo_ordre=tb.tbo_ordre
			and tb.TBO_TYPE='BTME'
			and (m.man_etat='VISE' or m.man_etat='PAYE')
			and m.exe_ordre=2005			
		minus
			 select distinct man_id from mandat_detail_ecriture	where mde_origine='VISA'
	)
) x
where ecd_libelle like ('%Md.  ' || to_char(x.man_numero) || ' Bord.  ' || to_char(x.bor_num) || ' du  ' || x.ges_code || '%') 
and e.ecr_ordre=ecd.ecr_ordre 
and e.ecr_date_saisie>=to_date('10/02/2005', 'dd/mm/yyyy') 
and e.ecR_date_saisie<to_date('11/02/2005', 'dd/mm/yyyy') 
and e.tjo_ordre=4


select tit_id, tit_numero, bor_num, b.ges_code, m.tit_TTC from titre m, 
bordereau b where b.bor_id=m.bor_id and  m.tit_id in (
		select tit_id from titre m, bordereau b
			where m.bor_id=b.bor_id 
			and (m.tit_etat='VISE')
			and m.exe_ordre=2005			
		minus
			 select distinct tit_id from titre_detail_ecriture	where tde_origine='VISA'
	)