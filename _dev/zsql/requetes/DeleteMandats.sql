
-- Recherche des factures pour trouver le no de mandat

SELECT * FROM jefy.facture f, jefy.organ o, jefy.engage e
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900'
AND dep_fact LIKE 'SEPTEM%2006%';

SELECT SUM(dep_ttc) FROM jefy.facture f, jefy.organ o, jefy.engage e
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900'
AND dep_fact LIKE 'SEPTEM%2006%';

SELECT DISTINCT m.man_id, m.man_ordre, f.pco_num 
FROM jefy.facture f, jefy.organ o, jefy.engage e, maracuja.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%';

-- Suppression des MANDAT_BROUILLARD
DELETE FROM maracuja.mandat_brouillard WHERE man_id IN 
(
SELECT DISTINCT m.man_id
FROM jefy.facture f, jefy.organ o, jefy.engage e, maracuja.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%'
);

-- Suppression des DEPENSE
DELETE FROM maracuja.depense WHERE man_id IN 
(
SELECT DISTINCT m.man_id
FROM jefy.facture f, jefy.organ o, jefy.engage e, maracuja.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%'
);

-- Suppression des MANDAT
DELETE FROM maracuja.mandat WHERE man_id IN 
(
SELECT DISTINCT m.man_id
FROM jefy.facture f, jefy.organ o, jefy.engage e, maracuja.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%'
);

-- *******************************************
-- Suppression des Bordereaux

SELECT * FROM maracuja.bordereau_brouillard WHERE bob_libelle1 LIKE '%SEPTEM%'

SELECT  SUM(bob_montant) FROM maracuja.bordereau_brouillard 
WHERE bob_libelle1 LIKE 'VISA%SEPTEM%2006%' AND ges_code = '900' AND bob_sens = 'C';

SELECT  DISTINCT (bor_id) FROM maracuja.bordereau_brouillard WHERE bob_libelle1 LIKE '%SEPTEM%2006%' AND ges_code = '900';

-- BORDEREAU_INFO
DELETE FROM maracuja.bordereau_info WHERE bor_id  IN
(
SELECT  DISTINCT (bor_id) FROM maracuja.bordereau_brouillard WHERE bob_libelle1 LIKE '%SEPTEM%2006%' AND ges_code = '900'
);

-- BORDEREAU
DELETE FROM maracuja.bordereau WHERE bor_id IN
(
SELECT  DISTINCT (bor_id) FROM maracuja.bordereau_brouillard WHERE bob_libelle1 LIKE '%SEPTEM%2006%' AND ges_code = '900'
);

-- BORDEREAU_BROUILLARD
DELETE FROM maracuja.bordereau_brouillard WHERE bor_id IN
(
SELECT  DISTINCT (bor_id) FROM maracuja.bordereau_brouillard WHERE bob_libelle1 LIKE '%SEPTEM%2006%' AND ges_code = '900'
);


-- ***************************************************
-- SUPPRESSION DES MANDATS 

SELECT DISTINCT m.man_ordre, f.pco_num 
FROM jefy.facture f, jefy.organ o, jefy.engage e, jefy.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%';

-- MISE A JOUR DES BORDEREAUX
UPDATE jefy.bordero SET bor_stat = 'A'
WHERE bor_ordre IN (
SELECT DISTINCT m.bor_ordre
FROM jefy.facture f, jefy.organ o, jefy.engage e, jefy.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%'
);

-- MISE A JOUR DE MANDAT
UPDATE jefy.mandat SET man_stat = 'A'
WHERE man_ordre IN 
(
SELECT DISTINCT m.man_ordre
FROM jefy.facture f, jefy.organ o, jefy.engage e, jefy.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%'
);

-- UPDATE DE FACTURE
UPDATE jefy.facture SET man_ordre = NULL
WHERE dep_ordre IN 
(
SELECT DISTINCT f.dep_ordre
FROM jefy.facture f, jefy.organ o, jefy.engage e, jefy.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '900' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE 'SEPTEM%2006%'
);

/*-- Suppression des MANDATs
DELETE FROM jefy.mandat WHERE man_ordre IN
(
SELECT DISTINCT m.man_ordre
FROM jefy.facture f, jefy.organ o, jefy.engage e, jefy.mandat m
WHERE f.cde_ordre = e.cde_ordre AND e.org_ordre = o.org_ordre AND o.org_comp = '918' AND f.man_ordre = m.man_ordre
AND dep_fact LIKE '%DECEM%2005%';
);*/

-- 