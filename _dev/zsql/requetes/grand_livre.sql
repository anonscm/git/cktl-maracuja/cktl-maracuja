/* Formatted on 2005/09/27 11:11 (Formatter Plus v4.8.0) */
-- grand livre
SELECT   com_libelle, ecr_numero, ecr_date_saisie AS ecr_date, ecr_libelle,
         ecd_libelle, ecd_debit, ecd_credit, ecd_index, ecriture.exe_ordre,
         ecriture_detail.pco_num, pco_libelle, ecriture_detail.ges_code,
         y.tot_debit AS prec_debit, y.tot_credit AS prec_credit
    FROM comptabilite,
         ecriture,
         ecriture_detail,
         plan_comptable,
         (SELECT   ecriture_detail.pco_num,
                   NVL (SUM (NVL (ecriture_detail.ecd_debit, 0)),
                        0
                       ) AS tot_debit,
                   NVL (SUM (NVL (ecriture_detail.ecd_credit, 0)),
                        0
                       ) AS tot_credit
              FROM ecriture, ecriture_detail
             WHERE ecriture.ecr_ordre = ecriture_detail.ecr_ordre
               AND ecriture.ecr_date_saisie <
                                          TO_DATE ('01/01/2005', 'DD/MM/YYYY')
               AND ecriture.ecr_numero > 0
               AND ecriture.ecr_etat = 'VALIDE'
               AND (ecriture_detail.pco_num = '5112')
               AND (ecriture.exe_ordre = 2005)
               AND (ecriture.com_ordre = 1)
          GROUP BY ecriture_detail.pco_num) y
   WHERE ecriture_detail.pco_num = y.pco_num(+)
     AND ecriture.ecr_ordre = ecriture_detail.ecr_ordre
     AND plan_comptable.pco_num = ecriture_detail.pco_num
     AND ecriture.ecr_date_saisie >= TO_DATE ('01/01/2005', 'DD/MM/YYYY')
     AND ecriture.ecr_date_saisie < TO_DATE ('24/09/2005', 'DD/MM/YYYY')
     AND ecriture.ecr_numero > 0
     AND ecriture.ecr_etat = 'VALIDE'
     AND (ecriture_detail.pco_num = '5112')
     AND (ecriture.exe_ordre = 2005)
     AND (ecriture.com_ordre = 1)
ORDER BY ecriture_detail.pco_num, ecr_numero, ecd_index