-- renvoie les écritres déséquilibrées
select e.ecr_ordre, e.ecr_numero, e.ecr_date_saisie, e.ecr_libelle, sum(ecd.ecd_debit), sum (ecd.ecd_credit) 
from ecriture e, ecriture_detail ecd 
where e.ecr_ordre=ecd.ecr_ordre
and e.ecr_etat='VALIDE'
group by e.ecr_ordre, e.ecr_numero, e.ecr_date_saisie, e.ecr_libelle
having sum(ecd.ecd_debit) <> sum (ecd.ecd_credit)
order by e.ecr_ordre