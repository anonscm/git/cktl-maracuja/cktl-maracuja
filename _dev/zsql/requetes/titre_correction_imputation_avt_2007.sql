-- *******************************************
-- modification des imputations de titre
-- ********************************************

DECLARE
	   titid maracuja.TITRE.tit_id%TYPE;
	   ecrnum maracuja.ECRITURE.ecr_numero%TYPE;
	   exeordre maracuja.exercice.exe_ordre%TYPE;
	   pcoNumNew maracuja.PLAN_COMPTABLE.pco_num%TYPE;
	   pcoNumOld maracuja.PLAN_COMPTABLE.pco_num%TYPE;
	   
BEGIN
	 
	 titid = ;
	 ecrnum := 18530;
	 exeordre := 2006;
	 pcoNumOld := '1311';
	 pcoNumNew := '13111';

	 --- **********************************
	 -- verification exercice
	 
	 IF (exeordre<=2006) THEN
	 					 UPDATE maracuja.TITRE SET pco_num=pcoNumNew WHERE tit_id=titid AND pco_num = pcoNumOld AND exe_ordre=exeordre;
						  UPDATE maracuja.RECETTE SET pco_num=pcoNumNew WHERE pco_num = pcoNumOld AND exe_ordre=exeordre AND tit_id=titid;						 
						  UPDATE jefy.TITRE SET pco_num=pcoNumNew WHERE pco_num = pcoNumOld AND exe_ordre=exeordre AND tit_ordre=(SELECT tit_ordre FROM maracuja.TITRE WHERE tit_id=titid);     

     
--						   SELECT * FROM ECRITURE_DETAIL WHERE  ecr_ordre IN (SELECT ecr_ordre  FROM maracuja.ECRITURE WHERE exe_ordre=exeordre AND ecr_numero=ecrnum)
						   UPDATE maracuja.ECRITURE_DETAIL WHERE pco_num=pcoNumOld AND ecr_ordre IN (SELECT ecr_ordre  FROM maracuja.ECRITURE WHERE exe_ordre=exeordre AND ecr_numero=ecrnum);
		
	 END IF;
	 
END;	   




