--drop sequence Comptabilite_Exercice_SEQ
/

--drop table Comptabilite_Exercice cascade constraints
/

CREATE TABLE Comptabilite_Exercice (coe_ordre NUMBER NOT NULL, com_brouillard VARCHAR2(1) , com_Cloture DATE , com_Complementaire DATE , com_ordre NUMBER NOT NULL, com_Ouverture DATE , EXE_ORDRE NUMBER NOT NULL)
/

create table eo_temp_table as select max(coe_ordre) counter from Comptabilite_Exercice
/

create sequence Comptabilite_Exercice_SEQ
/

create procedure eo_set_sequence is
    xxx number;
    yyy number;
begin
    select max(counter) into xxx from eo_temp_table;
    if xxx is not NULL then
        yyy := 0;
        while (yyy < xxx) loop
            select Comptabilite_Exercice_SEQ.nextval into yyy from dual;
        end loop;
    end if;
end;

/

begin eo_set_sequence; end;
/

drop procedure eo_set_sequence
/

drop table eo_temp_table
/

ALTER TABLE Comptabilite_Exercice ADD PRIMARY KEY (coe_ordre)
/

ALTER TABLE Comptabilite_Exercice ADD CONSTRAINT Coe_comptabilite_FK FOREIGN KEY (com_ordre) REFERENCES Comptabilite (com_ordre) DEFERRABLE INITIALLY DEFERRED
/

ALTER TABLE Comptabilite_Exercice ADD CONSTRAINT Coe_exercice_FK FOREIGN KEY (EXE_ORDRE) REFERENCES Exercice (EXE_ORDRE) DEFERRABLE INITIALLY DEFERRED
/


ALTER TABLE MARACUJA.COMPTABILITE
 DROP (COM_BROUILLARD, COM_CLOTURE, COM_COMPLEMENTAIRE, COM_OUVERTURE)

