
select exe_ordre, pco_num, ges_code, sum(total_mandats_sauf_pi) total_mandats_sauf_pi , sum(total_mandats_pi) total_mandats_pi, sum(total_reversements) total_reversements, sum(debits) debits, sum(credits) credits
from
(
  -- mandats vises/payes (sauf reversements) sauf PI  
    select m.exe_ordre, m.pco_num, b.ges_code, sum(man_ht) total_mandats_sauf_pi , 0 total_mandats_pi, 0 total_reversements, 0 debits, 0 credits
    from mandat m, bordereau b 
    where m.bor_id=b.bor_id
     AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE')
     AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
    AND tbo_ordre <> 8 AND tbo_ordre <>18 AND tbo_ordre <> 16  and tbo_ordre <> 200 and tbo_ordre <> 201 
    and m.exe_ordre>2006
    group by m.exe_ordre, m.pco_num, b.ges_code
union all   
    select m.exe_ordre, m.pco_num, b.ges_code, 0 total_mandats_sauf_pi , sum(man_ht) total_mandats_pi, 0 total_reversements, 0 debits, 0 credits
    from mandat m, bordereau b 
    where m.bor_id=b.bor_id
     AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE')
     AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
    and tbo_ordre = 201 
    and m.exe_ordre>2006
    group by m.exe_ordre, m.pco_num, b.ges_code
union all   
    select m.exe_ordre, m.pco_num, b.ges_code, 0 total_mandats_sauf_pi , 0 total_mandats_pi, sum(man_ht) total_reversements, 0 debits, 0 credits
    from mandat m, bordereau b 
    where m.bor_id=b.bor_id
     AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE')
     AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
    and (tbo_ordre = 18 or tbo_ordre = 8) 
    and m.exe_ordre>2006
    group by m.exe_ordre, m.pco_num, b.ges_code
union all
    select e.exe_ordre, pco_num, ges_code, 0 total_mandats_sauf_pi , 0 total_mandats_pi, 0 total_reversements, sum(ecd_debit) debits, 0 credits 
    from ecriture_detail ecd, ecriture e , type_journal j
    where e.ecr_ordre=ecd.ecr_ordre and e.tjo_ordre=j.tjo_ordre and j.TJO_LIBELLE like 'JOURNAL EXERCICE%'
    and E.exe_ordre>2006 
    group by e.exe_ordre, pco_num, ges_code
union all
    select e.exe_ordre, pco_num, ges_code, 0 total_mandats_sauf_pi , 0 total_mandats_pi, 0 total_reversements, 0 debits, sum(ecd_credit) credits 
    from ecriture_detail ecd, ecriture e , type_journal j
    where e.ecr_ordre=ecd.ecr_ordre and e.tjo_ordre=j.tjo_ordre and j.TJO_LIBELLE like 'JOURNAL EXERCICE%'
    and E.exe_ordre>2006 
    group by e.exe_ordre, pco_num, ges_code  
)
group by exe_ordre, pco_num, ges_code