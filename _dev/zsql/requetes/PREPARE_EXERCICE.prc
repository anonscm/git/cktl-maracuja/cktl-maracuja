CREATE OR REPLACE PROCEDURE prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 14/11/2005
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

begin

precedentExercice := nouvelExercice - 1;

-- -------------------------------------------------------
-- Vérifications concernant l'exercice precedent


-- Verif que l'exercice precedent existe
select count(*) into flag from exercice where exe_exercice=precedentExercice;
if (flag=0) then
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
end if;




-- -------------------------------------------------------
-- Vérifications concernant l'exercice nouveau

-- Verif que le nouvel xercice n'existe pas
select count(*) into flag from exercice where exe_exercice=nouvelExercice;
if (flag <> 0) then
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' existe deja, impossible d''effectuer une nouvelle preparation.');
end if;






--  -------------------------------------------------------
-- Ajout du nouvel exercice
insert into exercice (EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE, EXE_ORDRE, EXE_OUVERTURE, EXE_STAT, EXE_TYPE)
	   values (null, nouvelExercice, null, nouvelExercice, to_date('01/01/'||nouvelExercice   ,'DD/MM/YYYY'), 'P','C' );

-- // FIXME Vérifier exe_stat et exe_type

--  -------------------------------------------------------
-- Preparation des parametres
insert into parametre (select nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.nextval, PAR_VALUE
	   				  from parametre
					  where exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- Récupération des codes gestion
insert into gestion_exercice (select nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185
	   						 from gestion_exercice
							 where exe_ordre=precedentExercice) ;


--  -------------------------------------------------------
-- Récupération des modes de paiement
insert into mode_paiement (select nouvelExercice, MOD_LIBELLE, mode_paiement_seq.nextval, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM, 
	   					  MOD_VISA_TYPE, MOD_EMA_AUTO
	   					  from mode_paiement
						  where exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- Récupération des modes de recouvrement
insert into mode_recouvrement (select nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.nextval, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO
	   					  from mode_recouvrement
						  where exe_ordre=precedentExercice);




end;
/

