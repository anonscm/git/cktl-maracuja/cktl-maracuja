-- les prestations internes ayant donne lieu a plusieurs factures
select pi.prest_id, count(*) 
from jefy_recette.pi_eng_fac p, jefy_recette.pi_dep_rec r, jefy_recette.prestation pi
where p.PEF_ID=r.PEF_ID and p.prest_id=pi.prest_id
and exe_ordre=2010
group by pi.prest_id
having count(*)>1






-- les depenses / recettes passées en plusieurs fois pour la même prestation
select pi.prest_id , pi.prest_numero, pi.PREST_LIBELLE, pi.PREST_TOTAL_TTC ,
f.fou_ordre fou_ordre_client, f.adr_nom nom_client, r.rec_id, rc.REC_TTC_SAISIE,
o.org_ub,o.org_cr, o.org_souscr, dpp.FOU_ORDRE fou_ordre_fournisseur, c.adr_nom nom_fournisseur,db.dep_id, db.DEP_TTC_SAISIE
from jefy_recette.pi_eng_fac p, jefy_recette.pi_dep_rec r, jefy_recette.recette rc, grhum.v_fournis_grhum f, grhum.v_fournis_grhum c,
jefy_recette.facture fa, jefy_recette.prestation pi, jefy_admin.organ o, 
jefy_depense.depense_budget db, jefy_depense.depense_papier dpp,
jefy_recette.recette_ctrl_planco rcp,
(select pi.prest_id, count(*) 
from jefy_recette.pi_eng_fac p, jefy_recette.pi_dep_rec r, jefy_recette.prestation pi
where p.PEF_ID=r.PEF_ID and p.prest_id=pi.prest_id
group by pi.prest_id
having count(*)>1
) x
where
pi.prest_id=p.prest_id
and p.PEF_ID=r.PEF_ID 
and p.prest_id=x.prest_id 
and r.rec_id=rc.rec_id
and rc.FAC_ID=fa.FAC_ID
and fa.fou_ordre=f.fou_ordre
and r.dep_id=db.dep_id
and db.DPP_ID=dpp.dpp_id
and dpp.fou_ordre=c.fou_ordre
and pi.ORG_ID=o.org_id
and rcp.rec_id=rc.rec_id
and rc.exe_ordre=2010
-- activer critere suivant pour recuperer seulement les pi non titrées
and rcp.TIT_ID is null
