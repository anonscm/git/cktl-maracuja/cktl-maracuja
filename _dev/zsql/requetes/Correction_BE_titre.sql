
-- Correction PB : 
-- BE ne transferait pas les ecritures de titre 
-- dans titre_detail_ecriture
/* Formatted on 2006/02/21 11:29 (Formatter Plus v4.8.0) */
INSERT INTO titre_detail_ecriture
            (ecd_ordre, exe_ordre, ori_ordre, tde_date, tde_ordre,
             tde_origine, tit_id)
   (SELECT ecd.ecd_ordre, ecd.exe_ordre, x.ori_ordre, SYSDATE,
           titre_detail_ecriture_seq.NEXTVAL, x.tde_origine, x.tit_id
      FROM ecriture_detail ecd,
           ecriture e,
           (SELECT ecd.ecd_ordre, ecd_libelle, tde.ori_ordre, tde.tde_origine,
                   tde.tit_id
              FROM titre_detail_ecriture tde,
                   ecriture_detail ecd,
                   ecriture_detail_be_log edb
             WHERE tde.ecd_ordre = ecd.ecd_ordre
               AND ecd.ecd_ordre = edb.ecd_ordre
               AND ecd.exe_ordre = 2005
               AND ecd_libelle NOT IN (
                      SELECT ecd_libelle
                        FROM ecriture_detail ecd,
                             ecriture e,
                             titre_detail_ecriture tde
                       WHERE tde.ecd_ordre = ecd.ecd_ordre
                         AND ecd.exe_ordre = 2006
                         AND e.ecr_ordre = ecd.ecr_ordre
                         AND e.top_ordre = 11)) x
     WHERE e.ecr_ordre = ecd.ecr_ordre
       AND e.top_ordre = 11
       AND ecd.exe_ordre = 2006
       AND x.ecd_libelle = ecd.ecd_libelle)