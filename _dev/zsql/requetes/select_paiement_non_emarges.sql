-- recuperation des paiement qui n'ont pas eu d'emargement auto
select distinct p.* 
from ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
where ecd.ecr_ordre=e.ecr_ordre
and e.tjo_ordre=9
and mde.ecd_ordre=ecd.ecd_ordre
and m.man_id =mde.man_id 
and m.pai_ordre=p.pai_ordre
and ecd.ecd_reste_emarger<>0
and ecd.pco_num like '4%'