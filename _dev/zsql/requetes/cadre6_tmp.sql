
select pco_num, sum(debit_be) debit_be, sum(debit_exe) debit_exe, sum(debit_be)+sum(debit_exe) debit_total,
	   sum(credit_be) credit_be, sum(credit_exe) credit_exe, sum(credit_be)+sum(credit_exe) credit_total
from	   
(
	select pco_num, 0 as debit_be, sum(ecd_debit) debit_exe, 0 as credit_be, sum(ecd_credit) credit_exe  
	from ecriture_detail ecd, ecriture e 
	where ecd.ecr_ordre=e.ecr_ordre
	and substr(e.ecr_etat,1,1)='V'
	and e.tjo_ordre<>2
	group by pco_num
	union
	select pco_num, sum(ecd_debit) as debit_be, 0 debit_exe, sum(ecd_credit) as credit_be, 0 credit_exe  
	from ecriture_detail ecd, ecriture e 
	where ecd.ecr_ordre=e.ecr_ordre
	and substr(e.ecr_etat,1,1)='V'
	and e.tjo_ordre=2
	group by pco_num
)
group by pco_num



