-- renvoi la liste des émargements inter composantes

select distinct ema_numero, ema_date, ecd1.pco_num, ecd1.ges_code code_gestion1,  e1.ecr_numero ecriture_numero1, ecd2.ges_code code_gestion2, e2.ecr_numero ecriture_numero2 
from emargement_detail emd, emargement em, ecriture_detail ecd1, ecriture_detail ecd2, ecriture e1, ecriture e2 
where 
emd.ema_ordre=em.ema_ordre
and emd.ecd_ordre_destination=ecd1.ecd_ordre
and emd.ecd_ordre_source=ecd2.ecd_ordre
and ecd2.ecr_ordre=e2.ecr_ordre
and ecd1.ecr_ordre=e1.ecr_ordre
and em.ema_etat='VALIDE'
and e1.ecr_etat='VALIDE'
and e2.ecr_etat='VALIDE'
and ecd1.GES_CODE <> ecd2.ges_code
order by ema_numero