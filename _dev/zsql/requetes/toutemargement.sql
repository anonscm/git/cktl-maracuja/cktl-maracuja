-- Requete pour etat TOUT EMARGEMENT
-- on liste les details ecritures, pour chaque detail on a les detail ecritures destination de l'emargement
select d.ges_code, e.ecr_numero, e.ECR_DATE_SAISIE, e.ecr_libelle, d.ecd_ordre, d.ecd_libelle, d.ecd_debit, d.ecd_credit, 
d.ecd_sens, d.ecd_montant, d.ECD_RESTE_EMARGER, ema.EMA_NUMERO, emd.EMD_MONTANT , e2.ecr_numero as ecr_dest_num
from ecriture_detail d, ecriture e, emargement ema, emargement_detail emd, ecriture_detail d2, ecriture e2
where e.ecr_ordre=d.ecr_ordre
and emd.EMA_ORDRE = ema.EMA_ORDRE(+)
and d.ECD_ORDRE = emd.ECD_ORDRE_SOURCE (+)
and emd.ECD_ORDRE_DESTINATION = d2.ECD_ORDRE (+)
and d2.ecr_ordre=e2.ecr_ordre (+)
and 'VALIDE'=ema.EMA_ETAT(+)
and e.ecr_date >= to_date('01/02/2005', 'DD/MM/YYYY')
and e.ecr_date < to_date('27/03/2005', 'DD/MM/YYYY')
and e.ecr_numero > 0 
and e.ecr_etat='VALIDE'
order by d.ges_code, e.ecr_numero, d.ecd_sens desc, ema.ema_numero 


