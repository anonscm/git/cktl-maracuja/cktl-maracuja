CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECRITURE_MONTANT
(ECR_ORDRE, ECR_MONTANT)
AS 
select ecr_ordre, sum(ecd_debit) ecr_montant from ecriture_detail group by ecr_ordre;