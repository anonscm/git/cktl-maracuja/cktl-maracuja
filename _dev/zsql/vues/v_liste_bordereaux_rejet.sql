select 
m.exe_ordre, tbo2.tbo_libelle as BT_type, 
tbo.tbo_libelle type_bordereau_origine, b.ges_code, m.man_numero numero_objet, b.bor_num bor_numero_origine,
brj.brj_num bordereau_rejet_numero, pco_num, man_ht montant_ht, man_tva montant_tva, man_ttc montant_ttc, man_motif_rejet motif_rejet, b.BOR_DATE_VISA date_rejet,(u.nom_usuel ||' '|| u.prenom) as REJETE_PAR,  
brj.brj_etat bordereau_rejet_etat
from maracuja.mandat m, maracuja.bordereau b, maracuja.bordereau_rejet brj, maracuja.type_bordereau tbo, 
maracuja.type_bordereau tbo2, 
jefy_admin.v_utilisateur u 
where m.brj_ordre=brj.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tbo.tbo_ordre
and brj.UTL_ORDRE = u.utl_ordre
and brj.tbo_ordre=tbo2.tbo_ordre
union all
select m.exe_ordre, tbo2.tbo_libelle as BT_type, tbo.tbo_libelle type_bordereau_origine, b.ges_code, m.tit_numero numero_objet, b.bor_num bor_numero_origine, 
brj.brj_num bordereau_rejet_numero, pco_num, tit_ht montant_ht, tit_tva montant_tva, tit_ttc montant_ttc, tit_motif_rejet motif_rejet,b.BOR_DATE_VISA date_rejet, (u.nom_usuel ||' '|| u.prenom) as REJETE_PAR ,  
brj.brj_etat bordereau_rejet_etat
from maracuja.titre m, maracuja.bordereau b, maracuja.bordereau_rejet brj, maracuja.type_bordereau tbo,  maracuja.type_bordereau tbo2, jefy_admin.v_utilisateur u 
where m.brj_ordre=brj.brj_ordre
and m.bor_id=b.bor_id
and b.tbo_ordre=tbo.tbo_ordre
and brj.tbo_ordre=tbo2.tbo_ordre
and brj.UTL_ORDRE = u.utl_ordre 
order by BT_type,exe_ordre, ges_code, type_bordereau_origine, bordereau_rejet_numero, numero_objet