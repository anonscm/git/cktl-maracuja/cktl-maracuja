--create or replace force view comptefi.v_dvlop_dep_listeExt 
--(exe_ordre, ges_code, section, pco_num_bdn, pco_num, mandats, reversements, cr_ouverts, dep_date, section_lib, ordre_presentation)
--as

select   m.exe_ordre,
         m.ges_code,
         s.section,
         p.pco_num_bdn,
         m.pco_num,
         sum (dep_ht),
         0,
         0,
         e.date_saisie,
         s.section_lib,
         s.ordre_presentation
from     maracuja.mandat m inner join maracuja.bordereau b on (m.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (m.pco_num = p.pco_num)
         inner join maracuja.depense dep on (m.man_id = dep.man_id)
         inner join maracuja.v_depense_tcd dtcd on (dep.dep_id = dtcd.dep_id)
         inner join comptefi.v_section s on (dtcd.tcd_sect = s.section)
         inner join
         (select   m.man_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.mandat_detail_ecriture mde, maracuja.ecriture e, maracuja.mandat m
          where    m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.man_id) e on (m.man_id = e.man_id)
where    s.section_type = 'D' and tbo_ordre <> 8 and tbo_ordre <> 21 and tbo_ordre <> 18 and tbo_ordre <> 16 and (m.man_etat = 'VISE' or m.man_etat = 'PAYE')
group by m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num, e.date_saisie, s.section_lib, s.ordre_presentation
union all
-- ordre de reversement avant 2007
select   t.exe_ordre,
         t.ges_code,
         p.section,
         p.pco_num_bdn,
         t.pco_num,
         0,
         sum (tit_ht),
         0,
         e.date_saisie,
         p.section_lib,
         p.ordre_presentation
from     maracuja.titre t inner join maracuja.bordereau b on (t.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (t.pco_num = p.pco_num)
         inner join
         (select   m.tit_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.titre_detail_ecriture mde, maracuja.ecriture e, maracuja.titre m
          where    m.tit_id = mde.tit_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.tit_id) e on (t.tit_id = e.tit_id)
where    tbo_ordre = 8 and t.tit_etat = 'VISE'
group by t.exe_ordre, t.ges_code, p.section, p.pco_num_bdn, t.pco_num, e.date_saisie, p.section_lib, p.ordre_presentation
union all
-- ordre de reversement à partir de 2007
select   m.exe_ordre,
         m.ges_code,
         s.section,
         p.pco_num_bdn,
         m.pco_num,
         0,
         -sum (dep_ht),
         0,
         e.date_saisie,
         s.section_lib,
         s.ordre_presentation
from     maracuja.mandat m inner join maracuja.bordereau b on (m.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (m.pco_num = p.pco_num)
         inner join maracuja.depense dep on (m.man_id = dep.man_id)
         inner join maracuja.v_depense_tcd dtcd on (dep.dep_id = dtcd.dep_id)
         inner join comptefi.v_section s on (dtcd.tcd_sect = s.section)
         inner join
         (select   m.man_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.mandat_detail_ecriture mde, maracuja.ecriture e, maracuja.mandat m
          where    m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.man_id) e on (m.man_id = e.man_id)
where    s.section_type = 'D' and (tbo_ordre = 8 or tbo_ordre = 18 or tbo_ordre = 21) and (m.man_etat = 'VISE' or m.man_etat = 'PAYE')
group by m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num, e.date_saisie, s.section_lib, s.ordre_presentation
union all
-- Crédits ouverts
select exe_ordre,
       ges_code,
       bdn_section section,
       pco_num,
       pco_num,
       0,
       0,
       co,
       date_co,
       s.section_lib,
       s.ordre_presentation
from   comptefi.v_budnat2 inner join comptefi.v_section s on (bdn_section = s.section)
where  co <> 0 and bdn_quoi = 'D' and s.section_type = 'D';