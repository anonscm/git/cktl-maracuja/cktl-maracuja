SELECT bvn.exe_ordre, o.org_UB, SUBSTR(bvn.pco_num,1,2), nature, SUM(bvn.bdvn_votes)
FROM
jefy_budget.BUDGET_VOTE_NATURE bvn,
jefy_budget.BUDGET_PARAM_EDITIONS bpe,
maracuja.v_organ_exer o
WHERE bvn.org_id = o.org_id
AND bvn.exe_ordre = o.exe_ordre
AND bvn.pco_num LIKE bpe.chapitre||'%'
AND budgetaire = 'O'
AND num_section = 1
GROUP BY bvn.exe_ordre, o.org_UB, SUBSTR(bvn.pco_num,1,2), nature
UNION ALL
SELECT bvn.exe_ordre, o.org_UB, SUBSTR(bvn.pco_num,1,3), nature, SUM(bdvn_votes)
FROM jefy_budget.BUDGET_VOTE_NATURE bvn, jefy_budget.BUDGET_PARAM_EDITIONS bpe, maracuja.v_organ_exer o
WHERE bvn.org_id = o.org_id AND bvn.exe_ordre = o.exe_ordre
AND bvn.pco_num LIKE bpe.chapitre||'%' AND budgetaire = 'O' AND num_section = 3
GROUP BY bvn.exe_ordre, o.org_UB, SUBSTR(bvn.pco_num,1,3), nature 