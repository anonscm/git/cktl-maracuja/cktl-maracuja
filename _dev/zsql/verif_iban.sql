create or replace
FUNCTION       grhum.Verif_Iban(iban_a_tester RIBFOUR_ULR.IBAN%TYPE)
RETURN INTEGER
-- Fonction qui verifie si le code IBAN du RIB est correct
-- retourne 1 si le code IBAN est OK
-- retourne 0 si  le code IBAN est incorrect

/*
   Le code IBAN (International Bank Account Number) est un identifiant international pour les comptes clients des institutions financieres. Il est compose de trois parties :
   Le code pays : 2 lettres identifiant le pays dans lequel l'institution financiere reside. On utilise les codes specifies dans la norme ISO 3166.
   Une cle de controle composee de 2 chiffres.
   Le numero de compte (BBAN, Basic Bank Account Number) compose au maximum de 30 caracteres alphanumeriques (les 10 chiffres plus les lettres majuscules de "A" a "Z".
   ex : FR14 2004 1010 0505 0001 3M02 606
*/

IS

i  	  		 INTEGER;
car			 CHAR(1);
iban		 RIBFOUR_ULR.iban%TYPE;
codePays	 VARCHAR2(2);
clefIban	 VARCHAR2(2);
bban		 VARCHAR2(30);
pays1		 VARCHAR2(2);
pays2		 VARCHAR2(2);
etape1		 RIBFOUR_ULR.iban%TYPE;
etape2		 RIBFOUR_ULR.iban%TYPE;
clefCalculee NUMBER;
clefFinale	 VARCHAR2(2);
nb_car integer;

BEGIN

	 -- Conversion du code IBAN en Majuscule et sans les Espaces
	 iban := UPPER(REPLACE(iban_a_tester,' ',''));

	 codePays := SUBSTR(iban,1,2);
	 --DBMS_OUTPUT.PUT_LINE('codePays : '||codePays);

	 clefIban := SUBSTR(iban,3,2);
	 --DBMS_OUTPUT.PUT_LINE('clefIban : '||clefIban);

	 bban := SUBSTR(iban,5,LENGTH(iban));
	 --DBMS_OUTPUT.PUT_LINE('bban : '||bban);

	 etape1 := bban || codePays || '00';
	 --DBMS_OUTPUT.PUT_LINE('etape1 : '||etape1);

	 DBMS_OUTPUT.PUT_LINE('bban 1:  '||bban);
	 DBMS_OUTPUT.PUT_LINE('length bban 1:  '||LENGTH(bban));
	-- Conversion des lettres du BBAN en chiffre
  nb_car := LENGTH(bban);
  i := 1;
  while i <= nb_car
  loop
		car := SUBSTR(bban,i,1);
--	 DBMS_OUTPUT.PUT_LINE('verifiban car :  '||car);
		-- Pour chaque lettre du BBAN
		IF (  car >= CHR(65) AND car <= CHR(90) ) THEN
--	 DBMS_OUTPUT.PUT_LINE('verifiban cartransforme :  '||car);
		   bban := REPLACE(bban,car,Conversion_Table_Iban(car));
       nb_car := LENGTH(bban);
		END IF;
    i := i+1;
--	 DBMS_OUTPUT.PUT_LINE('bban et length :  '|| bban || ' nb:' || nb_car || ' i:' || i);
  
  end loop;
  
 
	 DBMS_OUTPUT.PUT_LINE('bban 2:  '||bban);

	-- Conversion des lettres du code Pays en chiffre
	 pays1 := Conversion_Table_Iban(SUBSTR(codePays,1,1));
	 pays2 := Conversion_Table_Iban(SUBSTR(codePays,2,1));

	 etape2 := bban || pays1 || pays2 || '00';
	 DBMS_OUTPUT.PUT_LINE('bban 3:  '||bban);
	 DBMS_OUTPUT.PUT_LINE('pays1 :  '||pays1);
	 DBMS_OUTPUT.PUT_LINE('pays2 :  '||pays2);

	 clefCalculee := 98 - ( MOD(TO_NUMBER(etape2),97));
	 --DBMS_OUTPUT.PUT_LINE('clefCalculee :  '||clefCalculee);

	 IF (clefCalculee < 10) THEN
	 	clefFinale := '0' || TO_CHAR(clefCalculee);
	 ELSE
	 	clefFinale := TO_CHAR(clefCalculee);
	 END IF;

	 -- test entre la clefFinale et la clef saisie
	 IF (clefFinale = clefIban) THEN
	 	 RETURN(1);
	 ELSE
	 	 RETURN (0);
	 END IF;

END;
 