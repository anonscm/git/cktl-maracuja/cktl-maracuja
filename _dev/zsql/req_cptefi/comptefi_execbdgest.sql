
-- exec bud gest
SELECT   exe_ordre, NULL AS ges_code, dst_code, dst_lib, ssdst_code,
         ssdst_lib, SUM (sum_dep10) sum_dep10, SUM (sum_dep20) sum_dep20,
         SUM (sum_dep30) sum_dep30
    FROM comptefi.v_situ_exebudgdep
   WHERE (exe_ordre = 2006)
GROUP BY exe_ordre, dst_code, dst_lib, ssdst_code, ssdst_lib
ORDER BY exe_ordre, dst_code, dst_lib, ssdst_code, ssdst_lib