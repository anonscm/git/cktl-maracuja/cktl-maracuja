SELECT   d.exe_ordre, SECTION, pco_num_bdn, d.pco_num pco_num, pco_libelle,
         SUM (mandats) mandats, SUM (reversements) reversements,
         SUM (montant_net) montant_net, SUM (cr_ouverts) cr_ouverts,
         SUM (non_empl) non_empl
    FROM comptefi.v_dvlop_dep d,
         maracuja.v_organ_exer o,
         maracuja.plan_comptable p
   WHERE TO_CHAR (dep_date, 'YYYYMMDD') < '20070101'
     AND (d.exe_ordre = 2006)
     AND (d.ges_code = o.org_ub AND org_niv = 2)
     AND d.pco_num = p.pco_num
     AND o.exe_ordre = d.exe_ordre
GROUP BY d.exe_ordre, SECTION, pco_num_bdn, d.pco_num, pco_libelle
ORDER BY exe_ordre, SECTION, pco_num_bdn, pco_num