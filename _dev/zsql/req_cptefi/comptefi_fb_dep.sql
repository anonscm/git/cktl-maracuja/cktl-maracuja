
-- FB depenses
SELECT   exe_ordre, ges_code, x.pco_num, bor_id, man_id, man_num, man_lib,
         man_mont, man_tva, man_ttc, ecr_sacd, ecr_date, ecr_date_saisie,
         bor_num, imp_lib, ges_lib, pco_chapitre, tbo_ordre
    FROM v_planco_chapitre p,
         (SELECT exe_ordre, ges_code, pco_num, bor_id, man_id, man_num,
                 man_lib, man_mont, man_tva, man_ttc, ecr_sacd, ecr_date,
                 ecr_date_saisie, bor_num, imp_lib, ges_lib, tbo_ordre
            FROM v_mandat_reimp
           WHERE (exe_ordre = 2006)
             AND SUBSTR (pco_num, 1, 1) <> '4'
             AND TO_CHAR (ecr_date_saisie, 'YYYYMMDD') >= '20060101'
             AND TO_CHAR (ecr_date_saisie, 'YYYYMMDD') < '20070101') x
   WHERE x.pco_num = p.pco_num
ORDER BY exe_ordre, pco_chapitre, man_num, ges_code, ecr_date_saisie,
         ecr_date