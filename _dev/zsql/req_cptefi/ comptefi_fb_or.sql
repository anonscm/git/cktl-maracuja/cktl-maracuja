
-- FB OR
SELECT   exe_ordre, ges_code, x.pco_num, bor_id, tit_id, tit_num, tit_lib,
         tit_mont, tit_tva, debiteur, tbo_ordre, tit_ttc, ecr_sacd, ecr_date,
         ecr_date_saisie, bor_num, imp_lib, ges_lib, pco_chapitre
    FROM v_planco_chapitre p,
         (SELECT exe_ordre, ges_code, pco_num, bor_id, tit_id, tit_num,
                 tit_lib, tit_mont, tit_tva, debiteur, tbo_ordre, tit_ttc,
                 ecr_sacd, ecr_date, ecr_date_saisie, bor_num, imp_lib,
                 ges_lib
            FROM v_titre_reimp
           WHERE (exe_ordre = 2006)
             AND TO_CHAR (ecr_date_saisie, 'YYYYMMDD') >= '20060101'
             AND TO_CHAR (ecr_date_saisie, 'YYYYMMDD') < '20070101'
             AND tbo_ordre = 8) x
   WHERE x.pco_num = p.pco_num
ORDER BY exe_ordre, pco_chapitre, tit_num, ges_code, ecr_date_saisie,
         ecr_date