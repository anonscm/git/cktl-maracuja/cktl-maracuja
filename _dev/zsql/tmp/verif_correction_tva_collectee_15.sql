-- requete de test correction TVA collectée 1/2
-- cette requête ne doit pas renvoyer de ligne
-- (ancienne minus nouvelle)

SELECT ges_code,
    exe_ordre,
    tit_numero,
    tit_libelle,
    TIT_HT,
    TAUX_TVA,
    round(round(100*tit_tva *100/tit_ht)/100,1) taux_recalcule,
    TIT_TVA,
    TIT_TTC from (
select *    
FROM
  (-- Recette avec facture récupération du taux de TVA
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(fpl.FLIG_TOTAL_HT) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    (SUM(fpl.FLIG_TOTAL_TTC)-SUM(fpl.FLIG_TOTAL_HT)) TIT_TVA,
    SUM(fpl.FLIG_TOTAL_TTC) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_recette.facture_papier fp,
    jefy_recette.facture_papier_ligne fpl,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND f.fac_id = fp.FAC_ID
  AND fp.fap_id = fpl.FAP_ID
  AND fpl.tva_id = tva.TVA_ID
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type <>'REVERSEMENTS'
  AND tb.tbo_sous_type <>'REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva.TVA_TAUX
  UNION ALL -- Recette sans facture estimation du taux de TVA non stocké
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(f.FAC_HT_SAISIE) TIT_HT,
    tva.TVA_TAUX TAUX_TVA,
    SUM(f.FAC_TVA_SAISIE) TIT_TVA,
    SUM(f.FAC_TTC_SAISIE) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND tit_ht <>0
  AND trunc(ROUND(tit_tva *100/tit_ht, 1)) = tva.TVA_id (+)
  AND f.fac_id IN
    (SELECT fac_id FROM jefy_recette.facture
    MINUS
    SELECT fac_id FROM jefy_recette.facture_papier
    )
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type <>'REVERSEMENTS'
  AND tb.tbo_sous_type <>'REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva_taux
  UNION ALL -- reduction complete sur recette avec facture recuperation du taux de TVA dans la facture
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    -SUM(fpl.FLIG_TOTAL_HT) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    -(SUM(fpl.FLIG_TOTAL_TTC)-SUM(fpl.FLIG_TOTAL_HT)) TIT_TVA,
    -SUM(fpl.FLIG_TOTAL_TTC) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_recette.facture_papier fp,
    jefy_recette.facture_papier_ligne fpl,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND f.fac_id = fp.FAC_ID
  AND fp.fap_id = fpl.FAP_ID
  AND fpl.tva_id = tva.TVA_ID (+)
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type ='REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  AND ABS(rec.rec_montant_budgetaire) = ABS(f.fac_montant_budgetaire)
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva.TVA_TAUX,
    m.tit_ttc
  UNION ALL -- Réduction partielle ou réduction sans facture estimation du taux de TVA non stocké
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(rec.REC_HT_SAISIE) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    SUM(rec.REC_TVA_SAISIE) TIT_TVA,
    SUM(rec.REC_TTC_SAISIE) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND tit_ht <>0
  AND trunc(ROUND(tit_tva *100/tit_ht, 1)) = tva.TVA_id (+)
  AND f.fac_id IN
    (SELECT fac_id FROM jefy_recette.facture
    MINUS
    SELECT fac_id FROM jefy_recette.facture_papier
    )
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type ='REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva_taux
  UNION ALL -- Réduction partielle avec facture initiale estimation du taux de TVA non stocké
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(rec.REC_HT_SAISIE) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    SUM(rec.REC_TVA_SAISIE) TIT_TVA,
    SUM(rec.REC_TTC_SAISIE) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_recette.facture_papier fp,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND f.fac_id =fp.fac_id
  AND trunc(ROUND(tit_tva *100/tit_ht, 1)) = tva.TVA_id (+)
  AND ABS(rec.rec_montant_budgetaire) <>ABS(f.fac_montant_budgetaire)
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type ='REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva_taux
  )
WHERE taux_tva<>0 
minus
--- nouvelle requete
SELECT *
FROM
  (-- Recette avec facture récupération du taux de TVA
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(fpl.FLIG_TOTAL_HT) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    (SUM(fpl.FLIG_TOTAL_TTC)-SUM(fpl.FLIG_TOTAL_HT)) TIT_TVA,
    SUM(fpl.FLIG_TOTAL_TTC) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_recette.facture_papier fp,
    jefy_recette.facture_papier_ligne fpl,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND f.fac_id = fp.FAC_ID
  AND fp.fap_id = fpl.FAP_ID
  AND fpl.tva_id = tva.TVA_ID
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type <>'REVERSEMENTS'
  AND tb.tbo_sous_type <>'REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva.TVA_TAUX
  UNION ALL -- Recette sans facture estimation du taux de TVA non stocké
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(f.FAC_HT_SAISIE) TIT_HT,
    tva.TVA_TAUX TAUX_TVA,
    SUM(f.FAC_TVA_SAISIE) TIT_TVA,
    SUM(f.FAC_TTC_SAISIE) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND tit_ht <>0
  AND round(round(100*tit_tva *100/tit_ht)/100,1) = tva.TVA_taux (+)
  AND f.fac_id IN
    (SELECT fac_id FROM jefy_recette.facture
    MINUS
    SELECT fac_id FROM jefy_recette.facture_papier
    )
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type <>'REVERSEMENTS'
  AND tb.tbo_sous_type <>'REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva_taux
  UNION ALL -- reduction complete sur recette avec facture recuperation du taux de TVA dans la facture
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    -SUM(fpl.FLIG_TOTAL_HT) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    -(SUM(fpl.FLIG_TOTAL_TTC)-SUM(fpl.FLIG_TOTAL_HT)) TIT_TVA,
    -SUM(fpl.FLIG_TOTAL_TTC) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_recette.facture_papier fp,
    jefy_recette.facture_papier_ligne fpl,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND f.fac_id = fp.FAC_ID
  AND fp.fap_id = fpl.FAP_ID
  AND fpl.tva_id = tva.TVA_ID (+)
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type ='REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  AND ABS(rec.rec_montant_budgetaire) = ABS(f.fac_montant_budgetaire)
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva.TVA_TAUX,
    m.tit_ttc
  UNION ALL -- Réduction partielle ou réduction sans facture estimation du taux de TVA non stocké
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(rec.REC_HT_SAISIE) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    SUM(rec.REC_TVA_SAISIE) TIT_TVA,
    SUM(rec.REC_TTC_SAISIE) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND tit_ht <>0
  AND round(round(100*tit_tva *100/tit_ht)/100,1) = tva.TVA_taux (+)
  AND f.fac_id IN
    (SELECT fac_id FROM jefy_recette.facture
    MINUS
    SELECT fac_id FROM jefy_recette.facture_papier
    )
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type ='REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva_taux
  UNION ALL -- Réduction partielle avec facture initiale estimation du taux de TVA non stocké
  SELECT b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    SUM(rec.REC_HT_SAISIE) TIT_HT,
    NVL(tva.TVA_TAUX,0) TAUX_TVA,
    SUM(rec.REC_TVA_SAISIE) TIT_TVA,
    SUM(rec.REC_TTC_SAISIE) TIT_TTC
  FROM maracuja.bordereau b,
    maracuja.titre m,
    maracuja.type_bordereau tb,
    maracuja.recette r,
    jefy_recette.recette_ctrl_planco rcp,
    jefy_recette.recette rec,
    jefy_recette.facture f,
    jefy_recette.facture_papier fp,
    jefy_admin.tva tva
  WHERE b.bor_id =m.bor_id
  AND b.tbo_ordre =tb.tbo_ordre
  AND m.tit_id = r.tit_id
  AND r.rec_ordre = rcp.RPCO_ID
  AND rcp.rec_id = rec.rec_id
  AND rec.FAC_ID = f.FAC_ID
  AND f.fac_id =fp.fac_id
  AND round(round(100*tit_tva *100/tit_ht)/100,1) = tva.TVA_taux (+)
  AND ABS(rec.rec_montant_budgetaire) <>ABS(f.fac_montant_budgetaire)
  AND m.TIT_ETAT ='VISE'
  AND (tb.tbo_sous_type ='REDUCTIONS' )
  AND (m.exe_ordre >= 2013)
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') >= '20140101'
  AND TO_CHAR( b.bor_date_visa,'YYYYMMDD') < '20140624'
  GROUP BY b.ges_code,
    m.exe_ordre,
    tit_numero,
    tit_libelle,
    tva_taux
  )
WHERE taux_tva<>0
)
;
