SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Util  IS

    PROCEDURE annuler_visa_bor_mandat(borid INTEGER);

    PROCEDURE supprimer_visa_btme(borid INTEGER);

    PROCEDURE supprimer_visa_btte(borid INTEGER);

    procedure creer_ecriture_annulation(ecrordre INTEGER);
    
    function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer;
     
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER);
    
    procedure supprimer_bordereau_dep(borid integer);
    
    procedure supprimer_bordereau_rec(borid integer);
    
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Util IS

    -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

 -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux de paye ni les OR)
       PROCEDURE annuler_visa_bor_mandat(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, mandat_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.man_id=tmpmanid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME' and tbotype <> 'BTRU') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME ou BTRU');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandats associ�s au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
--             
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

--                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

--                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;



       -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux de paye ni les OR)
       PROCEDURE supprimer_visa_btme(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
                            if (flag>0) then
                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                            end if;
                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;

   -- annulation du visa d'un bordereau de depense BTTE
       PROCEDURE supprimer_visa_btte(borid INTEGER) IS
                    flag INTEGER;
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmptitid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associ� au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associ�s au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associ�s au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmptitid;
                             EXIT WHEN lestitres%NOTFOUND;

                            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, TITRE_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.tit_id=tmptitid;

                            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                            DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmptitid;
                            UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmptitid;


             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;
       
       
       
       procedure creer_ecriture_annulation(ecrordre INTEGER) is
            ecr maracuja.ecriture%rowtype;
            ecd maracuja.ecriture_detail%rowtype;
            flag integer;
            str ecriture.ecr_libelle%type;
            newEcrOrdre ecriture.ecr_ordre%type;
            newEcdOrdre ecriture_detail.ecd_ordre%type;
            x  integer;
            cursor c1 is select * from maracuja.ecriture_detail where ecr_ordre=ecrordre;
       
       begin
            
            
            select count(*) into flag from maracuja.ecriture where ecr_ordre=ecrordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture retrouvee pour ecr_ordre= '|| ecrordre || ' .');
            end if;

            select * into ecr from maracuja.ecriture where ecr_ordre=ecrordre;              
            
            str := 'Annulation Ecriture '|| ecr.exe_ordre || '/'||ecr.ecr_numero;
            
            -- verifier que l'ecriture n'a pas deja ete annulee
            select count(*) into flag from maracuja.ecriture where ecr_libelle = str; 
            if (flag > 0) then
                select ecr_numero into flag from maracuja.ecriture where ecr_libelle = str; 
                RAISE_APPLICATION_ERROR (-20001,'L''ecriture numero '||ecr.ecr_numero ||' (ecr_ordre= '|| ecrordre || ') a deja ete annulee par l''ecriture numero '|| flag ||'.');
            end if;
                   
            
            -- verifier que les details ne sont pas emarges
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                if (ecd.ecd_reste_emarger<abs(ecd.ecd_montant) ) then
                    RAISE_APPLICATION_ERROR (-20001,'L''ecriture '||ecr.ecr_numero ||' ecr_ordre= '|| ecrordre || ' a ete emargee pour le compte '||ecd.pco_num||'. Impossible d''annuler');                    
                end if;

             END LOOP;
             CLOSE c1;


--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...              
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--                    
--                    

--             END LOOP;
--             CLOSE c1;

            newEcrOrdre := api_plsql_journal.creerecriture(
                ecr.com_ordre,
                SYSDATE,
                str,
                ecr.exe_ordre,
                ecr.ori_ordre,
                ecr.tjo_ordre,
                ecr.top_ordre,
                ecr.utl_ordre);            
            
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                newEcdOrdre := api_plsql_journal.creerEcritureDetail (
                        ecd.ecd_commentaire,
                        ecd.ecd_libelle,  
                        -ecd.ECD_MONTANT,
                        ecd.ECD_SECONDAIRE,
                        ecd.ECD_SENS,
                        newEcrOrdre,
                        ecd.GES_CODE,
                        ecd.PCO_NUM );
                        
                x := creerEmargementMemeSens(ecd.ecd_ordre, newEcdOrdre, 1);
             END LOOP;
             CLOSE c1;                  
            NUMEROTATIONOBJECT.numeroter_ecriture(newEcrOrdre);

       end;
           
       
       
       
       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            
            utlOrdre ecriture.utl_ordre%type;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les ecriture_detail ne sont pas emargees
            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les montant s'annulent
            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            -- trouver l'utilisateur
            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       
       
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER) AS
        flag integer;
        emargementdetail EMARGEMENT_DETAIL%ROWTYPE;
        CURSOR c1 IS SELECT * INTO emargementdetail FROM EMARGEMENT_DETAIL WHERE ema_ordre = emaordre;
    BEGIN
        select count(*) into flag from emargement where ema_etat='VALIDE' and ema_ordre = emaordre;
        
        if (flag=1) then
             OPEN c1;
            LOOP
            FETCH c1 INTO emargementdetail;
            EXIT WHEN c1%NOTFOUND;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_source;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_destination;

            END LOOP;
            CLOSE c1;
   
            -- annulation de la emargement
            UPDATE EMARGEMENT SET ema_etat = 'ANNULE'
            WHERE ema_ordre = emaordre;        
        
        end if;

       
   
    END;
    
    
    
        
    procedure supprimer_bordereau_dep(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des mandats
        select count(*) into flag from mandat where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de depense.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les mandat_brouillards
        DELETE FROM maracuja.mandat_BROUILLARD WHERE man_id in (select man_id from mandat where bor_id=borid);
                
        -- supprimer les depenses
        DELETE FROM maracuja.depense WHERE man_id in (select man_id from mandat where bor_id=borid);

        -- mettre a jour les depense_ctrl_planco
        UPDATE jefy_depense.depense_ctrl_planco SET man_id = NULL WHERE man_id IN (SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid) ;

        -- supprimer les mandats        
        delete from  maracuja.mandat where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
        
        
    end;
    
    
    
    procedure supprimer_bordereau_rec(borid integer)  as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des titres
        select count(*) into flag from titre where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de recette.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les titre_brouillards
        DELETE FROM maracuja.titre_BROUILLARD WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);
                
        -- supprimer les recettes
        DELETE FROM maracuja.recette WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);

        -- mettre a jour les recette_ctrl_planco
        UPDATE jefy_recette.recette_ctrl_planco SET tit_id = NULL WHERE tit_id IN (SELECT tit_id FROM maracuja.titre WHERE bor_id = borid) ;

        -- supprimer les titres        
        delete from  maracuja.titre where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
    
    end;
     
       
END;
/


