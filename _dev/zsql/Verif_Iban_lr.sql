CREATE OR REPLACE FUNCTION GRHUM.Verif_Iban(iban_a_tester RIBFOUR_ULR.IBAN%TYPE)
RETURN INTEGER
-- Fonction qui vérifie si le code IBAN du RIB est correct
-- retourne 1 si le code IBAN est OK
-- retourne 0 si  le code IBAN est incorrect

/*
   Le code IBAN (International Bank Account Number) est un identifiant international pour les comptes clients des institutions financières. Il est composé de trois parties :
   Le code pays : 2 lettres identifiant le pays dans lequel l'institution financière réside. On utilise les codes spécifiés dans la norme ISO 3166.
   Une clé de contrôle composée de 2 chiffres.
   Le numéro de compte (BBAN, Basic Bank Account Number) composé au maximum de 30 caractères alphanumériques (les 10 chiffres plus les lettres majuscules de "A" à "Z".
   ex : FR14 2004 1010 0505 0001 3M02 606
*/

IS

i  	  		 INTEGER;
car			 CHAR(1);
iban		 RIBFOUR_ULR.iban%TYPE;
codePays	 VARCHAR2(2);
clefIban	 VARCHAR2(2);
bban		 VARCHAR2(30);
pays1		 VARCHAR2(2);
pays2		 VARCHAR2(2);
etape1		 RIBFOUR_ULR.iban%TYPE;
etape2		 RIBFOUR_ULR.iban%TYPE;
clefCalculee NUMBER;
clefFinale	 VARCHAR2(2);

BEGIN

	 -- Conversion du code IBAN en Majuscule et sans les Espaces
	 iban := UPPER(REPLACE(iban_a_tester,' ',''));

	 codePays := SUBSTR(iban,1,2);
	 --DBMS_OUTPUT.PUT_LINE('codePays : '||codePays);

	 clefIban := SUBSTR(iban,3,2);
	 --DBMS_OUTPUT.PUT_LINE('clefIban : '||clefIban);

	 bban := SUBSTR(iban,5,LENGTH(iban));
	 --DBMS_OUTPUT.PUT_LINE('bban : '||bban);

	 etape1 := bban || codePays || '00';
	 --DBMS_OUTPUT.PUT_LINE('etape1 : '||etape1);

	-- Conversion des lettres du BBAN en chiffre
	FOR i IN 1..LENGTH(bban)
	LOOP
		car := SUBSTR(bban,i,1);
		-- Pour chaque lettre du BBAN
		IF (  car >= CHR(65) AND car <= CHR(90) ) THEN
		   bban := REPLACE(bban,car,Conversion_Table_Iban(car));
		END IF;
	END LOOP;

	-- Conversion des lettres du code Pays en chiffre
	 pays1 := Conversion_Table_Iban(SUBSTR(codePays,1,1));
	 pays2 := Conversion_Table_Iban(SUBSTR(codePays,2,1));

	 etape2 := bban || pays1 || pays2 || '00';
	 --DBMS_OUTPUT.PUT_LINE('etape2 :  '||etape2);

	 clefCalculee := 98 - ( MOD(TO_NUMBER(etape2),97));
	 --DBMS_OUTPUT.PUT_LINE('clefCalculee :  '||clefCalculee);

	 IF (clefCalculee < 10) THEN
	 	clefFinale := '0' || TO_CHAR(clefCalculee);
	 ELSE
	 	clefFinale := TO_CHAR(clefCalculee);
	 END IF;

	 -- test entre la clefFinale et la clef saisie
	 IF (clefFinale = clefIban) THEN
	 	 RETURN(1);
	 ELSE
	 	 RETURN (0);
	 END IF;
     
     EXCEPTION
     -- si on a une exception notamment sur un to_number, l'IBAN n''est pas correct
  WHEN others THEN
    RETURN (0);


END;
/


