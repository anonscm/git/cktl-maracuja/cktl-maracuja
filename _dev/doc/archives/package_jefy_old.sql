CREATE OR REPLACE PACKAGE MARACUJA.bordereau_titre_05 IS

PROCEDURE Get_Titre_Jefy ( exeordre INTEGER, borordre INTEGER, borid INTEGER, utlordre INTEGER, agtordre INTEGER );
PROCEDURE Get_Recette_Jefy(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER);
PROCEDURE Get_Btte_Jefy(borordre NUMBER,exeordre NUMBER);
PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_titre_non_admis IS

PROCEDURE expedier_bttna (brjordre INTEGER,exeordre INTEGER);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Bordereau_Titre IS

PROCEDURE Get_Titre_Jefy ( exeordre INTEGER, borordre INTEGER, borid INTEGER, utlordre INTEGER, agtordre INTEGER );
PROCEDURE Get_Recette_Jefy(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER);
PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER);
PROCEDURE Get_Btte_Jefy(borordre NUMBER,exeordre NUMBER);
PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_papaye IS

PROCEDURE recup_brouillards_payes (moisordre NUMBER);

PROCEDURE maj_brouillards_payes (moisordre NUMBER, borordre NUMBER);

PROCEDURE GET_BTME_JEFY(borordre NUMBER,exeordre NUMBER);

PROCEDURE set_mandat_brouillard(manid INTEGER);

PROCEDURE set_bord_brouillard_visa(borid INTEGER);

PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER);

PROCEDURE set_bord_brouillard_retenues(borid NUMBER);

PROCEDURE set_bord_brouillard_sacd(borid NUMBER);

PROCEDURE get_facture_jefy(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER);

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 );


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Bordereau_Mandat_05 IS
PROCEDURE GET_BTME_JEFY(borordre NUMBER,exeordre NUMBER);

PROCEDURE set_mandat_brouillard(manid INTEGER);

PROCEDURE get_facture_jefy(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER);

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 );

PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_mandat_non_admis IS

PROCEDURE expedier_btmna (brjordre INTEGER,exeordre INTEGER);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Bordereau_Mandat IS

PROCEDURE GET_BTME_JEFY(borordre NUMBER,exeordre NUMBER);

PROCEDURE set_mandat_brouillard(manid INTEGER);

PROCEDURE get_facture_jefy(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER);

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 );

PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Titre_05 IS
--version 1.1.8 31/05/2005 - modifs prestations internes
--version 1.1.7 26/04/2005 - prestations internes
--version 1.1.8 23/06/2005 - structure table recette
--version 1.2.0 20/12/2005 - Multi exercices jefy
PROCEDURE Get_Btte_Jefy
(borordre NUMBER,exeordre NUMBER)

IS
	cpt 	 	INTEGER;

	lebordereau jefy05.bordero%ROWTYPE;

	agtordre 	jefy05.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;

BEGIN


	dbms_output.put_line('exercice : '||exeordre);

	-- est ce un bordereau de titre --
	SELECT COUNT(*) INTO cpt FROM jefy05.TITRE
		   WHERE bor_ordre = borordre;

	IF cpt != 0 THEN

		-- le bordereau est il deja vis  ? --
		tboordre:=Gestionorigine.recup_type_bordereau_titre(borordre, exeordre);

		SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;




		-- l exercice est il encore ouvert ? --


		-- pas de raise mais pas de recup s il est deja dans maracuja --
		IF cpt = 0 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
				FROM jefy05.bordero
				WHERE bor_ordre = borordre;
			/*
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy05.TITRE
			WHERE tit_ordre =
			 ( SELECT MAX(tit_ordre)
			   FROM jefy05.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre
			  );

			-- recuperation du type de bordereau --
			SELECT COUNT(*) INTO cpt
			FROM OLD_AGENT_TYPE_BORD
			WHERE agt_ordre = agtordre;

			IF cpt <> 0 THEN
			 SELECT tbo_ordre INTO tboordre
			 FROM OLD_AGENT_TYPE_BORD
			 WHERE agt_ordre = agtordre;
			ELSE
			 SELECT tbo_ordre INTO tboordre
			 FROM TYPE_BORDEREAU
			 WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' and EXE_ORDRE=exeordre);
			END IF;
			*/

			-- creation du bor_id --
			SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

			-- recuperation de l utilisateur --
			/*select utl_ordre into utlordre
			from utilisateur
			where agt_ordre = agtordre;
			*/
			-- TEST  TEST  TEST  TEST  TEST
			SELECT 1 INTO utlordre
				   FROM dual;
			-- TEST  TEST  TEST  TEST  TEST

			-- creation du bordereau --
			INSERT INTO BORDEREAU VALUES (
				NULL , 	 	 	      --BOR_DATE_VISA,
				'VALIDE',			  --BOR_ETAT,
				borid,				  --BOR_ID,
				leBordereau.bor_num,  --BOR_NUM,
				leBordereau.bor_ordre,--BOR_ORDRE,
				exeordre,	  		  --EXE_ORDRE,
				leBordereau.ges_code, --GES_CODE,
				tboordre,			  --TBO_ORDRE,
				utlordre,		 	  --UTL_ORDRE,
				NULL				  --UTL_ORDRE_VISA
				);

			Get_Titre_Jefy(exeordre,borordre,borid,utlordre,agtordre);

		END IF;
		--else
		-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
	END IF;

END;


PROCEDURE Get_Titre_Jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	jefytitre jefy05.TITRE%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	titid 		 TITRE.tit_id%TYPE;

	titoriginekey  TITRE.tit_ORGINE_KEY%TYPE;
	titoriginelib TITRE.tit_ORIGINE_LIB%TYPE;
	ORIORDRE 	   TITRE.ORI_ORDRE%TYPE;
	PRESTID 	   TITRE.PREST_ID%TYPE;
	TORORDRE 	   TITRE.TOR_ORDRE%TYPE;
	modordre	   TITRE.mod_ordre%TYPE;
	presid		   INTEGER;
	cpt			   INTEGER;

	CURSOR lesJefytitres IS
	SELECT *
	FROM jefy05.TITRE
	WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefytitres;
	LOOP
		FETCH lesJefytitres INTO jefytitre;
			  EXIT WHEN lesJefytitres%NOTFOUND;

		-- recup ??
		IF (jefytitre.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(jefytitre.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(jefytitre.org_ordre);
		END IF;

		titoriginekey  :=NULL;
		titoriginelib   :=NULL;

		-- recup du torordre origine JEFYCO
		TORORDRE 	     :=1;
		-- JEFYCO
		torordre := 1;
		-- recup du mode de paiement OP ordo
		modordre	     :=NULL;

		dbms_output.put_line('titreOrdre : '||jefytitre.tit_ordre);
		dbms_output.put_line('mod_code : '||jefytitre.mod_code);


		IF jefytitre.tit_type = 'C' THEN
		   SELECT mod_ordre INTO modordre
		   FROM MODE_PAIEMENT
		   WHERE mod_code = jefytitre.mod_code
		   AND exe_ordre = exeordre;
		ELSE
			modordre := NULL;
		END IF;



		-- recup du prestid
		presid		     :=NULL;


		--TODO des gescode sont nuls, recuperer le gescode de la table comptabilite ?
		IF jefytitre.ges_code IS NULL THEN
			 SELECT DISTINCT ges_code INTO gescode
			 FROM BORDEREAU
			 WHERE bor_id=borid;
		ELSE
		 	gescode:=jefytitre.ges_code;
		END IF;

		SELECT titre_seq.NEXTVAL INTO titid FROM dual;

		INSERT INTO TITRE (
		   BOR_ID,
		   BOR_ORDRE,
		   BRJ_ORDRE,
		   EXE_ORDRE,
		   GES_CODE,
		   MOD_ORDRE,
		   ORI_ORDRE,
		   PCO_NUM,
		   PREST_ID,
		   TIT_DATE_REMISE,
		   TIT_DATE_VISA_PRINC,
		   TIT_ETAT,
		   TIT_ETAT_REMISE,
		   TIT_HT,
		   TIT_ID,
		   TIT_MOTIF_REJET,
		   TIT_NB_PIECE,
		   TIT_NUMERO,
		   TIT_NUMERO_REJET,
		   TIT_ORDRE,
		   TIT_ORGINE_KEY,
		   TIT_ORIGINE_LIB,
		   TIT_TTC,
		   TIT_TVA,
		   TOR_ORDRE,
		   UTL_ORDRE,
		   ORG_ORDRE,
		   FOU_ORDRE,
		   MOR_ORDRE,
		   PAI_ORDRE,
		   rib_ordre_ordonnateur,
		   rib_ordre_comptable,
		   tit_libelle)
		VALUES
			(
			borid,--BOR_ID,
			borordre,--BOR_ORDRE,
			NULL,--BRJ_ORDRE,
			exeordre,--EXE_ORDRE,
			gescode,--GES_CODE,
			modordre,--MOD_ORDRE,
			oriordre,--ORI_ORDRE,
			jefytitre.pco_num,--PCO_NUM,
			jefytitre.pres_ordre,--PREST_ID,
			NULL,--TIT_DATE_REMISE,
			NULL,--TIT_DATE_VISA_PRINC,
			'ATTENTE',--TIT_ETAT,
			'ATTENTE',--TIT_ETAT_REMISE,
			NVL(En_Nombre(jefytitre.tit_mont),0),--TIT_HT,
			titid,--TIT_ID,
			NULL,--TIT_MOTIF_REJET,
			jefytitre.tit_piece,--TIT_NB_PIECE,
			jefytitre.tit_num,--TIT_NUMERO,
			NULL,--TIT_NUMERO_REJET,
			jefytitre.tit_ordre,--TIT_ORDRE,
			titoriginekey,--TIT_ORGINE_KEY,
			titoriginelib,--TIT_ORIGINE_LIB,
			NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TIT_TTC,
			NVL(En_Nombre(jefytitre.tit_monttva),0),--TIT_TVA,
			torordre,--TOR_ORDRE,
			utlordre,--UTL_ORDRE
			jefytitre.org_ordre,		--ORG_ORDRE,
			jefytitre.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
			modordre,				 --MOR_ORDRE
			NULL, -- VIR_ORDRE
			jefytitre.rib_ordre,
			jefytitre.rib_ordre,
			jefytitre.tit_lib
			);

		--on met a jour le type de bordereau s'il s'agit d'un bordereau de prestation interne
		SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=jefytitre.tit_ordre;
		IF cpt != 0 THEN
--		if jefytitre.pres_ordre is not null then
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;

		Get_Recette_Jefy(exeordre,titid,jefytitre.tit_ordre,utlordre);
		Set_Titre_Brouillard(titid);

	END LOOP;

	CLOSE lesJefytitres;

END;




PROCEDURE Get_Recette_Jefy
(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefytitre		 jefy05.TITRE%ROWTYPE;
	--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	--fouadresse  	 DEPENSE.dep_adresse%TYPE;
	--founom  		 DEPENSE.dep_fournisseur%TYPE;
	--lotordre  		 DEPENSE.dep_lot%TYPE;
	--marordre		 DEPENSE.dep_marches%TYPE;
	--fouordre		 DEPENSE.fou_ordre%TYPE;
	--gescode			 DEPENSE.ges_code%TYPE;
	cpt				 INTEGER;
	recid			 RECETTE.rec_id%TYPE;
	rectype			 RECETTE.rec_type%TYPE;
	gescode			 TITRE.GES_CODE%TYPE;

	modordre	   DEPENSE.mod_ordre%TYPE;


BEGIN

	SELECT * INTO jefytitre
		FROM jefy05.TITRE
		WHERE tit_ordre = titordre;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;

	rectype := NULL;

	-- ajout rod
	IF jefytitre.tit_type = 'C' THEN
	   SELECT mod_ordre INTO modordre
	   FROM MODE_PAIEMENT
	   WHERE mod_code = jefytitre.mod_code
	   AND exe_ordre = exeordre;
	ELSE
		modordre := NULL;
	END IF;


	IF jefytitre.ges_code IS NULL THEN
	 SELECT DISTINCT ges_code INTO gescode
	 FROM jefy05.ventil_titre
	 WHERE tit_ordre = jefytitre.tit_ordre;
	ELSE
	 gescode:=jefytitre.ges_code;
	END IF;

	SELECT recette_seq.NEXTVAL INTO recid FROM dual;


	INSERT INTO RECETTE VALUES
		(
		exeordre,--EXE_ORDRE,
		gescode,--GES_CODE,
		jefytitre.mod_code,--MOD_CODE,
		jefytitre.pco_num,--PCO_NUM,
		jefytitre.tit_date,-- REC_DATE,
		jefytitre.tit_debiteur,-- REC_DEBITEUR,
		recid,-- REC_ID,
		jefytitre.pco_num,-- REC_IMPUTTVA,
		jefytitre.tit_interne,-- REC_INTERNE,
		jefytitre.tit_lib,-- REC_LIBELLE,
		jefytitre.org_ordre,-- REC_LIGNE_BUDGETAIRE,
		'E',-- REC_MONNAIE,
		NVL(En_Nombre(jefytitre.tit_mont),0),--HT,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TTC,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--DISQUETTE,
		NVL(En_Nombre(jefytitre.tit_monttva),0),--   REC_MONTTVA,
		jefytitre.tit_num,--   REC_NUM,
		jefytitre.tit_ordre,--   REC_ORDRE,
		jefytitre.tit_piece,--   REC_PIECE,
		jefytitre.tit_ref,--   REC_REF,
		'VALIDE',--   REC_STAT,
		'NON',--    REC_SUPPRESSION,  Modif Rod
		jefytitre.tit_type,--	 REC_TYPE,
		NULL,--	 REC_VIREMENT,
		titid,--	  TIT_ID,
		titordre,--	  TIT_ORDRE,
		utlordre,--	   UTL_ORDRE
		jefytitre.org_ordre,		--	   ORG_ORDRE --ajout rod
		jefytitre.fou_ordre,   --FOU_ORDRE --ajout rod
		modordre, --mod_ordre
		NULL,  --mor_ordre
		jefytitre.rib_ordre,
		NULL
		);



END;


PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy05.TITRE%ROWTYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
BEGIN

	SELECT * INTO letitre
		FROM TITRE
		WHERE tit_id = titid;

	SELECT * INTO jefytitre
		FROM jefy05.TITRE
		WHERE tit_ordre = letitre.tit_ordre;



	SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=letitre.tit_ordre;
	IF cpt = 0 THEN
--	IF letitre.prest_id IS NULL THEN

		-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
		IF jefytitre.tit_type IN ('C','D') THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;


		-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			letitre.pco_num,			   --PCO_NUM
			letitre.tit_ht,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);

		IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
			-- creation du titre_brouillard visa --
			INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
				(
				NULL,  						   --ECD_ORDRE,
				letitre.exe_ordre,			   --EXE_ORDRE,
				letitre.ges_code,			   --GES_CODE,
				jefytitre.tit_imputtva,			   --PCO_NUM
				letitre.tit_tva,			   --TIB_MONTANT,
				'VISA TITRE',				   --TIB_OPERATION,
				titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
				sens,						   --TIB_SENS,
				titid						   --TIT_ID,
				);
		END IF;

		-- on inverse le sens
		IF sens = 'C' THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;

		-- creation du titre_brouillard contre partie --
		INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID)
			SELECT
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			ges_code,			   --GES_CODE,
			pco_num,			   --PCO_NUM
			NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			FROM jefy05.ventil_titre
			WHERE tit_ordre = letitre.tit_ordre;


	ELSE
		Bordereau_Titre_05.Set_Titre_Brouillard_intern(titid);
	END IF;

END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy05.TITRE%ROWTYPE;
	leplancomptable PLAN_COMPTABLE%ROWTYPE;
	gescodecompta TITRE.ges_code%TYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
BEGIN


	SELECT * INTO letitre
	FROM TITRE
	WHERE tit_id = titid;



	SELECT c.ges_code
		INTO gescodecompta
		FROM GESTION g, COMPTABILITE c
		WHERE g.ges_code = letitre.ges_code
		AND g.com_ordre = c.com_ordre;

	SELECT * INTO jefytitre
		FROM jefy05.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

	-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
	IF jefytitre.tit_type IN ('C','D') THEN
	 sens := 'D';
	ELSE
	 sens := 'C';
	END IF;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||letitre.pco_num;

	IF cpt = 0 THEN
		SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
		WHERE pco_num = letitre.pco_num;

		maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||letitre.pco_num);
	END IF;
--	letitre.pco_num := '18'||letitre.pco_num;

	-- creation du titre_brouillard visa --
	INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
		(
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		letitre.ges_code,			   --GES_CODE,
		'18'||letitre.pco_num,			   --PCO_NUM
		letitre.tit_ht,			   --TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		);

	IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
	-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			jefytitre.tit_imputtva,			   --PCO_NUM
			letitre.tit_tva,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);
	END IF;

	-- on inverse le sens
	IF sens = 'C' THEN
	   sens := 'D';
	ELSE
		sens := 'C';
	END IF;

	-- creation du titre_brouillard contre partie --
	INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID)
		SELECT
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			   --GES_CODE,
		'181',			   --PCO_NUM
		NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		FROM jefy05.ventil_titre
		WHERE tit_ordre = letitre.tit_ordre;



END;



PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
  niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Titre_Non_Admis
IS

--version 1.2.0 20/12/2005 - Multi exercices jefy

   PROCEDURE expedier_bttna (brjordre INTEGER, exeordre INTEGER)
   IS
      cpt            INTEGER;
      borordre       INTEGER;
      nume           INTEGER;
      nb             INTEGER;
      tbolibelle     TYPE_BORDEREAU.tbo_libelle%TYPE;
      titordre       TITRE.tit_ordre%TYPE;
      letitre        TITRE%ROWTYPE;
--ladepense  depense%rowtype;
      titid          TITRE.tit_id%TYPE;
      brjnum         BORDEREAU_REJET.brj_num%TYPE;
      lestitordres   VARCHAR2 (1000);
      ex             INTEGER;

--lesdepordres varchar2(1000);

      -- les mandats rejetes --
      CURSOR c1
      IS
         SELECT *
           FROM TITRE
          WHERE brj_ordre = brjordre;
-- depenses a supprimer --
/*cursor c2 is
select * from depense
where man_id = manid
and dep_suppression = 'OUI' ;
*/
   BEGIN
/*
lesmanordre
   "manordre$expli$manordre$expli$.......$manordre$expli$$"
   lesdepordres
   "depordre$depordre$.......$depordre$$"
   */
      lestitordres := NULL;

      SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';


	   IF ex IS NULL
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'PARAMETRE EXERCICE NON DEFINI');
            END IF;


      IF exeordre = 2006
      THEN
--lesdepordres:=null;
         SELECT bor_ordre
           INTO borordre
           FROM jefy.bordero
          WHERE bor_ordre IN (SELECT MAX (bor_ordre)
                                FROM TITRE
                               WHERE brj_ordre = brjordre);

         SELECT brj_num
           INTO nume
           FROM BORDEREAU_REJET
          WHERE brj_ordre = brjordre;

         SELECT COUNT (*)
           INTO nb
           FROM jefy.bordero_rejet
          WHERE bor_ordre = borordre + 1000000;

         IF nb = 0
         THEN
            INSERT INTO jefy.bordero_rejet
               SELECT bor_ordre + 1000000, bor_date, 'N', bor_visa, bor_quoi,
                      bor_num, ges_code, dsk_numero, nume, NULL
                 FROM jefy.bordero
                WHERE bor_ordre = borordre;

            SELECT f.tbo_libelle, b.brj_num
              INTO tbolibelle, brjnum
              FROM BORDEREAU_REJET b, TYPE_BORDEREAU f
             WHERE b.brj_ordre = brjordre AND b.tbo_ordre = f.tbo_ordre;

            IF tbolibelle != 'BORDEREAU DE TITRE NON ADMIS'
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTTNA');
            END IF;

            OPEN c1;

            LOOP
               FETCH c1
                INTO letitre;

               EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
               UPDATE jefy.TITRE
                  SET tit_stat = 'A'
                WHERE tit_ordre = letitre.tit_ordre;

               lestitordres :=
                     lestitordres
                  || '$'
                  || letitre.tit_ordre
                  || '$'
                  || letitre.tit_motif_rejet;
               titid := letitre.tit_ordre;
            END LOOP;

            CLOSE c1;

-- enlever le premier $ --
            SELECT SUBSTR (lestitordres, 2, LENGTH (lestitordres))
              INTO lestitordres
              FROM DUAL;

            lestitordres := lestitordres || '$$';
			
			dbms_output.put_line(lestitordres);
			
            jefy.maj_titre_rejet (lestitordres);
         END IF;
      ELSE
         IF exeordre = 2005
         THEN
--lesdepordres:=null;
            SELECT bor_ordre
              INTO borordre
              FROM jefy05.bordero
             WHERE bor_ordre IN (SELECT MAX (bor_ordre)
                                   FROM TITRE
                                  WHERE brj_ordre = brjordre);

            SELECT brj_num
              INTO nume
              FROM BORDEREAU_REJET
             WHERE brj_ordre = brjordre;

            SELECT COUNT (*)
              INTO nb
              FROM jefy05.bordero_rejet
             WHERE bor_ordre = borordre + 1000000;

            IF nb = 0
            THEN
               INSERT INTO jefy05.bordero_rejet
                  SELECT bor_ordre + 1000000, bor_date, 'N', bor_visa,
                         bor_quoi, bor_num, ges_code, dsk_numero, nume, NULL
                    FROM jefy05.bordero
                   WHERE bor_ordre = borordre;

               SELECT f.tbo_libelle, b.brj_num
                 INTO tbolibelle, brjnum
                 FROM BORDEREAU_REJET b, TYPE_BORDEREAU f
                WHERE b.brj_ordre = brjordre AND b.tbo_ordre = f.tbo_ordre;

               IF tbolibelle != 'BORDEREAU DE TITRE NON ADMIS'
               THEN
                  RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTTNA');
               END IF;

               OPEN c1;

               LOOP
                  FETCH c1
                   INTO letitre;

                  EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
                  UPDATE jefy05.TITRE
                     SET tit_stat = 'A'
                   WHERE tit_ordre = letitre.tit_ordre;

                  lestitordres :=
                        lestitordres
                     || '$'
                     || letitre.tit_ordre
                     || '$'
                     || letitre.tit_motif_rejet;
                  titid := letitre.tit_ordre;
               END LOOP;

               CLOSE c1;

-- enlever le premier $ --
               SELECT SUBSTR (lestitordres, 2, LENGTH (lestitordres))
                 INTO lestitordres
                 FROM DUAL;

               lestitordres := lestitordres || '$$';
               jefy05.maj_titre_rejet (lestitordres);
            END IF;
		 ELSE
		 	 RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');

         END IF;
      END IF;
   END;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Titre IS
-- version 1.5.0 08/11/2006 - modifs pour le rec_id dans titre_brouillard
--version 1.1.9 04/05/2006 - modif pour plusieurs ventilations
--version 1.1.8 31/05/2005 - modifs prestations internes
--version 1.1.7 26/04/2005 - prestations internes
--version 1.1.8 23/06/2005 - structure table recette

PROCEDURE Get_Btte_Jefy
(borordre NUMBER,exeordre NUMBER)

IS
	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;
	dbms_output.put_line('exercice : '||exeordre);

	-- est ce un bordereau de titre --
	SELECT COUNT(*) INTO cpt FROM jefy.TITRE
		   WHERE bor_ordre = borordre;

	IF cpt != 0 THEN

		-- le bordereau est il deja vis? ? --
		tboordre:=Gestionorigine.recup_type_bordereau_titre(borordre, exeordre);

		SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;




		-- l exercice est il encore ouvert ? --


		-- pas de raise mais pas de recup s il est deja dans maracuja --
		IF cpt = 0 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;
			/*
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy.TITRE
			WHERE tit_ordre =
			 ( SELECT MAX(tit_ordre)
			   FROM jefy.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre
			  );

			-- recuperation du type de bordereau --
			SELECT COUNT(*) INTO cpt
			FROM OLD_AGENT_TYPE_BORD
			WHERE agt_ordre = agtordre;

			IF cpt <> 0 THEN
			 SELECT tbo_ordre INTO tboordre
			 FROM OLD_AGENT_TYPE_BORD
			 WHERE agt_ordre = agtordre;
			ELSE
			 SELECT tbo_ordre INTO tboordre
			 FROM TYPE_BORDEREAU
			 WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' and EXE_ORDRE=exeordre);
			END IF;
			*/

			-- creation du bor_id --
			SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

			-- recuperation de l utilisateur --
			/*select utl_ordre into utlordre
			from utilisateur
			where agt_ordre = agtordre;
			*/
			-- TEST  TEST  TEST  TEST  TEST
			SELECT 1 INTO utlordre
				   FROM dual;
			-- TEST  TEST  TEST  TEST  TEST

			-- creation du bordereau --
			INSERT INTO BORDEREAU VALUES (
				NULL , 	 	 	      --BOR_DATE_VISA,
				'VALIDE',			  --BOR_ETAT,
				borid,				  --BOR_ID,
				leBordereau.bor_num,  --BOR_NUM,
				leBordereau.bor_ordre,--BOR_ORDRE,
				exeordre,	  		  --EXE_ORDRE,
				leBordereau.ges_code, --GES_CODE,
				tboordre,			  --TBO_ORDRE,
				utlordre,		 	  --UTL_ORDRE,
				NULL,				  --UTL_ORDRE_VISA
				leBordereau.bor_date
				);

			Get_Titre_Jefy(exeordre,borordre,borid,utlordre,agtordre);

		END IF;
		--else
		-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
	END IF;

END;


PROCEDURE Get_Titre_Jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	jefytitre jefy.TITRE%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	titid 		 TITRE.tit_id%TYPE;

	titoriginekey  TITRE.tit_ORGINE_KEY%TYPE;
	titoriginelib TITRE.tit_ORIGINE_LIB%TYPE;
	ORIORDRE 	   TITRE.ORI_ORDRE%TYPE;
	PRESTID 	   TITRE.PREST_ID%TYPE;
	TORORDRE 	   TITRE.TOR_ORDRE%TYPE;
	modordre	   TITRE.mod_ordre%TYPE;
	presid		   INTEGER;
	cpt			   INTEGER;

	CURSOR lesJefytitres IS
	SELECT *
	FROM jefy.TITRE
	WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefytitres;
	LOOP
		FETCH lesJefytitres INTO jefytitre;
			  EXIT WHEN lesJefytitres%NOTFOUND;

		-- recup ??
		IF (jefytitre.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(jefytitre.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(jefytitre.org_ordre);
		END IF;

		titoriginekey  :=NULL;
		titoriginelib   :=NULL;

		-- recup du torordre origine JEFYCO
		TORORDRE 	     :=1;
		-- JEFYCO
		torordre := 1;
		-- recup du mode de paiement OP ordo
		modordre	     :=NULL;

		dbms_output.put_line('titreOrdre : '||jefytitre.tit_ordre);
		dbms_output.put_line('mod_code : '||jefytitre.mod_code);


		IF jefytitre.tit_type = 'C' THEN
		   SELECT mod_ordre INTO modordre
		   FROM MODE_PAIEMENT
		   WHERE mod_code = jefytitre.mod_code
		   AND exe_ordre = exeordre;
		ELSE
			modordre := NULL;
		END IF;



		-- recup du prestid
		presid		     :=NULL;


		--TODO des gescode sont nuls, recuperer le gescode de la table comptabilite ?
		IF jefytitre.ges_code IS NULL THEN
			 SELECT DISTINCT ges_code INTO gescode
			 FROM BORDEREAU
			 WHERE bor_id=borid;
		ELSE
		 	gescode:=jefytitre.ges_code;
		END IF;

		SELECT titre_seq.NEXTVAL INTO titid FROM dual;

		INSERT INTO TITRE (
		   BOR_ID,
		   BOR_ORDRE,
		   BRJ_ORDRE,
		   EXE_ORDRE,
		   GES_CODE,
		   MOD_ORDRE,
		   ORI_ORDRE,
		   PCO_NUM,
		   PREST_ID,
		   TIT_DATE_REMISE,
		   TIT_DATE_VISA_PRINC,
		   TIT_ETAT,
		   TIT_ETAT_REMISE,
		   TIT_HT,
		   TIT_ID,
		   TIT_MOTIF_REJET,
		   TIT_NB_PIECE,
		   TIT_NUMERO,
		   TIT_NUMERO_REJET,
		   TIT_ORDRE,
		   TIT_ORGINE_KEY,
		   TIT_ORIGINE_LIB,
		   TIT_TTC,
		   TIT_TVA,
		   TOR_ORDRE,
		   UTL_ORDRE,
		   ORG_ORDRE,
		   FOU_ORDRE,
		   MOR_ORDRE,
		   PAI_ORDRE,
		   rib_ordre_ordonnateur,
		   rib_ordre_comptable,
		   tit_libelle)
		VALUES
			(
			borid,--BOR_ID,
			borordre,--BOR_ORDRE,
			NULL,--BRJ_ORDRE,
			exeordre,--EXE_ORDRE,
			gescode,--GES_CODE,
			modordre,--MOD_ORDRE,
			oriordre,--ORI_ORDRE,
			jefytitre.pco_num,--PCO_NUM,
			jefytitre.pres_ordre,--PREST_ID,
			NULL,--TIT_DATE_REMISE,
			NULL,--TIT_DATE_VISA_PRINC,
			'ATTENTE',--TIT_ETAT,
			'ATTENTE',--TIT_ETAT_REMISE,
			NVL(En_Nombre(jefytitre.tit_mont),0),--TIT_HT,
			titid,--TIT_ID,
			NULL,--TIT_MOTIF_REJET,
			jefytitre.tit_piece,--TIT_NB_PIECE,
			jefytitre.tit_num,--TIT_NUMERO,
			NULL,--TIT_NUMERO_REJET,
			jefytitre.tit_ordre,--TIT_ORDRE,
			titoriginekey,--TIT_ORGINE_KEY,
			titoriginelib,--TIT_ORIGINE_LIB,
			NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TIT_TTC,
			NVL(En_Nombre(jefytitre.tit_monttva),0),--TIT_TVA,
			torordre,--TOR_ORDRE,
			utlordre,--UTL_ORDRE
			jefytitre.org_ordre,		--ORG_ORDRE,
			jefytitre.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
			modordre,				 --MOR_ORDRE
			NULL, -- VIR_ORDRE
			jefytitre.rib_ordre,
			jefytitre.rib_ordre,
			jefytitre.tit_lib
			);

		--on met a jour le type de bordereau s'il s'agit d'un bordereau de prestation interne
		SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=jefytitre.tit_ordre AND exe_ordre=exeordre;
		IF cpt != 0 THEN
--		if jefytitre.pres_ordre is not null then
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;

		Get_Recette_Jefy(exeordre,titid,jefytitre.tit_ordre,utlordre);
		Set_Titre_Brouillard(titid);

	END LOOP;

	CLOSE lesJefytitres;

END;




PROCEDURE Get_Recette_Jefy
(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefytitre		 jefy.TITRE%ROWTYPE;
	--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	--fouadresse  	 DEPENSE.dep_adresse%TYPE;
	--founom  		 DEPENSE.dep_fournisseur%TYPE;
	--lotordre  		 DEPENSE.dep_lot%TYPE;
	--marordre		 DEPENSE.dep_marches%TYPE;
	--fouordre		 DEPENSE.fou_ordre%TYPE;
	--gescode			 DEPENSE.ges_code%TYPE;
	cpt				 INTEGER;
	recid			 RECETTE.rec_id%TYPE;
	rectype			 RECETTE.rec_type%TYPE;
	gescode			 TITRE.GES_CODE%TYPE;

	modordre	   DEPENSE.mod_ordre%TYPE;


BEGIN

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = titordre;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;

	rectype := NULL;

	-- ajout rod
	IF jefytitre.tit_type = 'C' THEN
	   SELECT mod_ordre INTO modordre
	   FROM MODE_PAIEMENT
	   WHERE mod_code = jefytitre.mod_code
	   AND exe_ordre = exeordre;
	ELSE
		modordre := NULL;
	END IF;


-- 	IF jefytitre.ges_code IS NULL THEN
-- 	 SELECT distinct ges_code INTO gescode
-- 	 FROM jefy.ventil_titre
-- 	 WHERE tit_ordre = jefytitre.tit_ordre;
-- 	else
-- 	 gescode:=jefytitre.ges_code;
-- 	END IF;


	 SELECT DISTINCT ges_code INTO gescode
	 FROM maracuja.TITRE
	 WHERE tit_id = titid;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;


	INSERT INTO RECETTE VALUES
		(
		exeordre,--EXE_ORDRE,
		gescode,--GES_CODE,
		jefytitre.mod_code,--MOD_CODE,
		jefytitre.pco_num,--PCO_NUM,
		jefytitre.tit_date,-- REC_DATE,
		jefytitre.tit_debiteur,-- REC_DEBITEUR,
		recid,-- REC_ID,
		jefytitre.pco_num,-- REC_IMPUTTVA,
		jefytitre.tit_interne,-- REC_INTERNE,
		jefytitre.tit_lib,-- REC_LIBELLE,
		jefytitre.org_ordre,-- REC_LIGNE_BUDGETAIRE,
		'E',-- REC_MONNAIE,
		NVL(En_Nombre(jefytitre.tit_mont),0),--HT,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TTC,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--DISQUETTE,
		NVL(En_Nombre(jefytitre.tit_monttva),0),--   REC_MONTTVA,
		jefytitre.tit_num,--   REC_NUM,
		jefytitre.tit_ordre,--   REC_ORDRE,
		jefytitre.tit_piece,--   REC_PIECE,
		jefytitre.tit_ref,--   REC_REF,
		'VALIDE',--   REC_STAT,
		'NON',--    REC_SUPPRESSION,  Modif Rod
		jefytitre.tit_type,--	 REC_TYPE,
		NULL,--	 REC_VIREMENT,
		titid,--	  TIT_ID,
		titordre,--	  TIT_ORDRE,
		utlordre,--	   UTL_ORDRE
		jefytitre.org_ordre,		--	   ORG_ORDRE --ajout rod
		jefytitre.fou_ordre,   --FOU_ORDRE --ajout rod
		modordre, --mod_ordre
		NULL,  --mor_ordre
		jefytitre.rib_ordre,
		NULL
		);

-- recuperation des echeanciers
Bordereau_Titre.Get_recette_prelevements (exeordre ,titid ,titordre ,utlordre,recid );


END;



PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
echancier_data PRELEV.ECHEANCIER%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;
personne_data grhum.v_personne%ROWTYPE;
ORIORDRE INTEGER;
modordre INTEGER;

BEGIN

-- verifier s il y a une facture_titre associee
SELECT COUNT(*) INTO cpt FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;
IF (cpt=0) THEN
	  RETURN;
END IF;


-- recup du facture_titre pour le titre concerne
SELECT * INTO FACTURE_TITRE_data
FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;

dbms_output.put_line ('titordre='||TITORDRE);


SELECT COUNT(*) INTO cpt FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';
IF (cpt=0) THEN
	  RETURN;
END IF;


SELECT * INTO echancier_data
FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';





SELECT * INTO CLIENT_data
FROM PRELEV.CLIENT
WHERE CLIENT_ORDRE = echancier_data.client_ordre
AND supprime = 'N';




-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;



-- recup ??
IF (echancier_data.con_ordre IS NOT NULL) THEN
 ORIORDRE :=Gestionorigine.traiter_convordre(echancier_data.con_ordre);
ELSE
 ORIORDRE :=Gestionorigine.traiter_orgordre(echancier_data.org_ordre);
END IF;

SELECT * INTO personne_data
FROM GRHUM.V_PERSONNE WHERE pers_id = CLIENT_data.pers_id;

INSERT INTO MARACUJA.ECHEANCIER (ECHE_AUTORIS_SIGNEE,
FOU_ORDRE_CLIENT, CON_ORDRE,
ECHE_DATE_1ERE_ECHEANCE, ECHE_DATE_CREATION, ECHE_DATE_MODIF,
ECHE_ECHEANCIER_ORDRE,
ECHE_ETAT_PRELEVEMENT, FT_ORDRE, ECHE_LIBELLE, ECHE_MONTANT, ECHE_MONTANT_EN_LETTRES,
ECHE_NOMBRE_ECHEANCES, ECHE_NUMERO_INDEX, ORG_ORDRE, PREST_ORDRE, ECHE_PRISE_EN_CHARGE,
ECHE_REF_FACTURE_EXTERNE, ECHE_SUPPRIME, EXE_ORDRE, TIT_ID, REC_ID, TIT_ORDRE, ORI_ORDRE,
PERS_ID, ORG_ID, PERS_DESCRIPTION)  VALUES
(
echancier_data.AUTORIS_SIGNEE  ,--ECHE_AUTORIS_SIGNEE
echancier_data.CLIENT_ORDRE  ,--FOU_ORDRE_CLIENT
echancier_data.CON_ORDRE  ,--CON_ORDRE
echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
echancier_data.FT_ORDRE  ,--FT_ORDRE
echancier_data.LIBELLE,--ECHE_LIBELLE
echancier_data.MONTANT  ,--ECHE_MONTANT
echancier_data.MONTANT_EN_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echancier_data.NOMBRE_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
echancier_data.ORG_ORDRE  ,--ORG_ORDRE
echancier_data.PREST_ORDRE  ,--PREST_ORDRE
echancier_data.PRISE_EN_CHARGE  ,--ECHE_PRISE_EN_CHARGE
echancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
echancier_data.SUPPRIME  ,--ECHE_SUPPRIME
2006  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
TITORDRE,
ORIORDRE,--ORI_ORDRE,
CLIENT_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
NULL,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT (ECHE_ECHEANCIER_ORDRE,
RECO_ORDRE, FOU_ORDRE,
PREL_COMMENTAIRE, PREL_DATE_MODIF, PREL_DATE_PRELEVEMENT,
PREL_PRELEV_DATE_SAISIE,
PREL_PRELEV_ETAT, PREL_NUMERO_INDEX, PREL_PRELEV_MONTANT,
PREL_PRELEV_ORDRE, RIB_ORDRE, PREL_ETAT_MARACUJA)
SELECT
ECHEANCIER_ORDRE,--ECHE_ECHEANCIER_ORDRE
FICP_ORDRE,--PREL_FICP_ORDRE
FOU_ORDRE,--FOU_ORDRE
COMMENTAIRE,--PREL_COMMENTAIRE
DATE_MODIF,--PREL_DATE_MODIF
DATE_PRELEVEMENT,--PREL_DATE_PRELEVEMENT
PRELEV_DATE_SAISIE,--PREL_PRELEV_DATE_SAISIE
PRELEV_ETAT,--PREL_PRELEV_ETAT
NUMERO_INDEX,--PREL_NUMERO_INDEX
PRELEV_MONTANT,--PREL_PRELEV_MONTANT
PRELEV_ORDRE,--PREL_PRELEV_ORDRE
RIB_ORDRE,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM PRELEV.PRELEVEMENT
WHERE ECHEANCIER_ORDRE = echancier_data.ECHEANCIER_ORDRE;

END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
	exeordre	  INTEGER;
	recid		  INTEGER;
BEGIN

	SELECT * INTO letitre
		FROM TITRE
		WHERE tit_id = titid;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

		SELECT rec_id INTO recid FROM RECETTE WHERE tit_id=titid AND ROWNUM=1;


	SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=letitre.tit_ordre AND exe_ordre=letitre.exe_ordre;
	IF cpt = 0 THEN
--	IF letitre.prest_id IS NULL THEN

		-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
		IF jefytitre.tit_type IN ('C','D') THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;


		-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			letitre.pco_num,			   --PCO_NUM
			letitre.tit_ht,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid,						   --TIT_ID,
			NULL
			);

		IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
			-- creation du titre_brouillard visa --
			INSERT INTO TITRE_BROUILLARD VALUES
				(
				NULL,  						   --ECD_ORDRE,
				letitre.exe_ordre,			   --EXE_ORDRE,
				letitre.ges_code,			   --GES_CODE,
				jefytitre.tit_imputtva,			   --PCO_NUM
				letitre.tit_tva,			   --TIB_MONTANT,
				'VISA TITRE',				   --TIB_OPERATION,
				titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
				sens,						   --TIB_SENS,
				titid	,					   --TIT_ID,
				NULL
				);
		END IF;

		-- on inverse le sens
		IF sens = 'C' THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;

		-- creation du titre_brouillard contre partie --
		INSERT INTO TITRE_BROUILLARD
			SELECT
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			ges_code,			   --GES_CODE,
			pco_num,			   --PCO_NUM
			NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid	,					   --TIT_ID,
			recid
			FROM jefy.ventil_titre
			WHERE tit_ordre = letitre.tit_ordre;


	ELSE
		Bordereau_Titre.Set_Titre_Brouillard_intern(titid);
	END IF;

END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	leplancomptable maracuja.PLAN_COMPTABLE%ROWTYPE;
	gescodecompta TITRE.ges_code%TYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
	recid		  INTEGER;
BEGIN


	SELECT * INTO letitre
	FROM TITRE
	WHERE tit_id = titid;

		SELECT rec_id INTO recid FROM RECETTE WHERE tit_id=titid AND ROWNUM=1;

	SELECT c.ges_code
		INTO gescodecompta
		FROM GESTION g, COMPTABILITE c
		WHERE g.ges_code = letitre.ges_code
		AND g.com_ordre = c.com_ordre;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

	-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
	IF jefytitre.tit_type IN ('C','D') THEN
	 sens := 'D';
	ELSE
	 sens := 'C';
	END IF;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||letitre.pco_num;

	IF cpt = 0 THEN
		SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
		WHERE pco_num = letitre.pco_num;

		maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||letitre.pco_num);
	END IF;
--	letitre.pco_num := '18'||letitre.pco_num;

	-- creation du titre_brouillard visa --
	INSERT INTO TITRE_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		letitre.ges_code,			   --GES_CODE,
		'18'||letitre.pco_num,			   --PCO_NUM
		letitre.tit_ht,			   --TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid	,					   --TIT_ID,
		NULL
		);

	IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
	-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			jefytitre.tit_imputtva,			   --PCO_NUM
			letitre.tit_tva,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid		,				   --TIT_ID,
			NULL
			);
	END IF;

	-- on inverse le sens
	IF sens = 'C' THEN
	   sens := 'D';
	ELSE
		sens := 'C';
	END IF;

	-- creation du titre_brouillard contre partie --
	INSERT INTO TITRE_BROUILLARD
		SELECT
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			   --GES_CODE,
		'181',			   --PCO_NUM
		NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid	,					   --TIT_ID,
		recid
		FROM jefy.ventil_titre
		WHERE tit_ordre = letitre.tit_ordre;



END;



PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
  niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Papaye IS
--version 1.1.8b - recup du tcd_ordre dans depense
--version 1.1.9 - modifs pour subrogation

PROCEDURE Recup_Brouillards_Payes (moisordre NUMBER)
IS

-- Recuperation de tous les bordereaux de paye deja
CURSOR bordereaux IS
SELECT DISTINCT bor_ordre FROM jefy.bordero WHERE bor_ordre IN (
SELECT bor_ordre FROM jefy.MANDAT WHERE man_ordre IN (
SELECT man_ordre FROM jefy.facture WHERE agt_ordre IN (
SELECT TO_NUMBER(param_value) FROM papaye.paye_parametres WHERE param_key = 'AGENT_JEFY')
AND dep_fact IN (SELECT mois_complet FROM papaye.paye_mois WHERE mois_ordre = moisordre)));
--select distinct bor_ordre from jefy.papaye_compta where mois_ordre = moisordre;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
tboordre maracuja.TYPE_BORDEREAU.tbo_ordre%TYPE;
cpt INTEGER;

BEGIN

  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO borordre;
    EXIT WHEN bordereaux%NOTFOUND;

	maracuja.Bordereau_Papaye.maj_brouillards_payes(moisordre, borordre);

  END LOOP;
  CLOSE bordereaux;

END;

PROCEDURE maj_brouillards_payes (moisordre NUMBER, borordre NUMBER)
IS

borid  	 maracuja.BORDEREAU.bor_id%TYPE;
tboordre maracuja.TYPE_BORDEREAU.tbo_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;

BEGIN

	 SELECT ges_code INTO gescode FROM jefy.bordero WHERE bor_ordre = borordre;

	 -- On verifie que les ecritures aient bien ete generees pour la composante en question.
	 SELECT COUNT(*) INTO cpt
	 FROM papaye.jefy_ecritures WHERE ecr_comp = gescode AND mois_ordre = moisordre;

	 IF (cpt = 0)
	 THEN
	 	 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas préparé les écritures pour la composante '||gescode||' !');
	 END IF;

	 -- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
	 SELECT SUM(ecr_mont) INTO sumdebits FROM papaye.jefy_ecritures
	 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' AND ecr_comp = gescode AND ecr_sens = 'D';

	 SELECT SUM(ecr_mont) INTO sumcredits FROM papaye.jefy_ecritures
	 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' AND ecr_comp = gescode AND ecr_sens = 'C';

	 IF (sumcredits <> sumdebits)
	 THEN
	 	 RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||gescode||', la somme des DEBITS ('||sumdebits||') est différente de la somme des CREDITS ('||sumcredits||') !');
	 END IF;


  	SELECT mois_annee INTO exeordre FROM papaye.paye_mois WHERE mois_ordre = moisordre;
	tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

	SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
	WHERE bor_ordre = borordre AND tbo_ordre = tboordre AND exe_ordre = exeordre;

	IF cpt = 0
	THEN

		Bordereau_Papaye.get_btme_jefy(borordre, exeordre);

		SELECT bor_id INTO borid FROM maracuja.BORDEREAU WHERE bor_ordre = borordre AND tbo_ordre = tboordre AND exe_ordre = exeordre;

		Bordereau_Papaye.set_bord_brouillard_visa(borid);

		Bordereau_Papaye.set_bord_brouillard_retenues(borid);

		Bordereau_Papaye.set_bord_brouillard_sacd(borid);

	END IF;

  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Papaye.set_bord_brouillard_paiement(moisordre, borid, exeordre);

END;


PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	utilisateur.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN
SELECT bor_stat INTO etat FROM jefy.bordero WHERE bor_ordre = borordre;

IF etat = 'N' OR etat = 'X'
THEN

-- est ce un bordereau de titre --
SELECT COUNT(*) INTO cpt FROM jefy.MANDAT
WHERE bor_ordre = borordre;

IF cpt != 0 THEN

tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

-- le bordereau est il deja visé ? --
SELECT COUNT(*) INTO cpt
FROM BORDEREAU
WHERE bor_ordre = borordre
AND exe_ordre = exeordre
AND tbo_ordre = tboordre;

-- l exercice est il encore ouvert ? --

-- pas de raise mais pas de recup s il est deja dans maracuja --
IF cpt = 0 THEN

-- recup des infos --
SELECT * INTO lebordereau FROM jefy.bordero WHERE bor_ordre = borordre;

-- creation du bor_id --
SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

-- creation du bordereau --
INSERT INTO BORDEREAU VALUES (
NULL , 	 	 	      --BOR_DATE_VISA,
'VALIDE',			  --BOR_ETAT,
borid,				  --BOR_ID,
leBordereau.bor_num,  --BOR_NUM,
leBordereau.bor_ordre,--BOR_ORDRE,
exeordre,	  		  --EXE_ORDRE,
leBordereau.ges_code, --GES_CODE,
tboordre,			  --TBO_ORDRE,
1,		 	  		  --UTL_ORDRE,
NULL,				  --UTL_ORDRE_VISA
lebordereau.bor_date
);

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre = borordre);

INSERT INTO BORDEREAU_INFO VALUES
(
borid,
moislibelle,
NULL
);

Bordereau_Papaye.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

END IF;
--else
-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
END IF;
END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

lejefymandat jefy.MANDAT%ROWTYPE;
gescode 	 GESTION.ges_code%TYPE;
manid 		 MANDAT.man_id%TYPE;

MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
PRESTID 	   MANDAT.PREST_ID%TYPE;
TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
modordre	   MANDAT.mod_ordre%TYPE;
CURSOR lesJefyMandats IS
SELECT *
FROM jefy.MANDAT
WHERE bor_ordre =borordre;


BEGIN


--boucle de traitement --
OPEN lesJefyMandats;
LOOP
FETCH lesJefyMandats INTO lejefymandat;
EXIT WHEN lesJefyMandats%NOTFOUND;

-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
MANORGINE_KEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
MANORIGINE_LIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
IF (lejefymandat.conv_ordre IS NOT NULL) THEN
  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
ELSE
  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
END IF;

--PRESTID : PRESTATION INTERNE --
PRESTID :=NULL;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

--MOD_ORDRE --
SELECT mod_ordre INTO modordre
FROM MODE_PAIEMENT
WHERE mod_code =
(
 SELECT MAX(mod_code)
 FROM jefy.factures
 WHERE man_ordre = lejefymandat.man_ordre
)
AND exe_ordre=exeordre;

-- creation du man_id --
SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

-- creation du mandat --
INSERT INTO MANDAT (BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE,
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT,
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET,
		MAN_NB_PIECE, MAN_NUMERO, MAN_NUMERO_REJET,
		MAN_ORDRE, MAN_ORGINE_KEY, MAN_ORIGINE_LIB, MAN_TTC,
		MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM,
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE,
		RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE,
		MAN_ATTENTE_PAIEMENT,
		MAN_ATTENTE_DATE,
		MAN_ATTENTE_OBJET,
		UTL_ORDRE_ATTENTE) VALUES
(
 borid ,		   		--BOR_ID,
 NULL, 			   		--BRJ_ORDRE,
 exeordre,		   		--EXE_ORDRE,
 lejefymandat.fou_ordre,--FOU_ORDRE,
 gescode,				--GES_CODE,
 NULL,				    --MAN_DATE_REMISE,
 NULL,					--MAN_DATE_VISA_PRINC,
 'ATTENTE',				--MAN_ETAT,
 'ATTENTE',			    --MAN_ETAT_REMISE,
 lejefymandat.man_mont, --MAN_HT,
 manid,					--MAN_ID,
 NULL,					--MAN_MOTIF_REJET,
 lejefymandat.man_piece,--MAN_NB_PIECE,
 lejefymandat.man_num,	--MAN_NUMERO,
 NULL,					--MAN_NUMERO_REJET,
 lejefymandat.man_ordre,--MAN_ORDRE,
 MANORGINE_KEY,			--MAN_ORGINE_KEY,
 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
 lejefymandat.man_tva,  --MAN_TVA,
 modordre,				--MOD_ORDRE,
 ORIORDRE,				--ORI_ORDRE,
 lejefymandat.pco_num,	--PCO_NUM,
 lejefymandat.PRES_ORDRE,				--PREST_ID,
 TORORDRE,				--TOR_ORDRE,
 VIRORDRE,				--VIR_ORDRE
 lejefymandat.org_ordre,  --org_ordre
 lejefymandat.rib_ordre, --rib ordo
 lejefymandat.rib_ordre, -- rib_comptable
 4,
 NULL,
 NULL,
 NULL
);

Bordereau_Papaye.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
Bordereau_Papaye.set_mandat_brouillard(manid);

END LOOP;

CLOSE lesJefyMandats;

END;


-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
moislibelle papaye.paye_mois.mois_complet%TYPE;

cpt INTEGER;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

tboordre:=Gestionorigine.recup_type_bordereau_mandat(currentBordereau.bor_ordre, exeordre);

SELECT mois_complet INTO moislibelle FROM papaye.paye_mois WHERE mois_ordre = moisordre;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id IN (
SELECT bor_id FROM BORDEREAU WHERE tbo_ordre = tboordre)
AND bob_operation LIKE '%PAIEMENT%' AND bob_libelle2 = moislibelle;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'PAIEMENT SALAIRES',
	currentecriture.pco_num,
	'PAIEMENT SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans PAPAYE.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from papaye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.JEFY_ECRITURES WHERE mois_ordre = mois AND ecr_comp = gescode  AND ecr_type='64' AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||gescode||' , moisordre : '||moisordre);


  OPEN ecriturescredit64(moisordre, gescode);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'VISA SALAIRES',
	currentecriture.pco_num,
	'VISA SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecriturescredit64;

END;

-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans Papaye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = mois AND ecr_comp = gescode AND ecr_type='44';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, gescode);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'RETENUES SALAIRES',
	currentecriture.pco_num,
	'RETENUES SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans Papaye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = mois AND ecr_comp = gescode AND ecr_type='18';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, gescode);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'SACD SALAIRES',
	currentecriture.pco_num,
	'SACD SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecrituressacd;

END;


-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat  	  MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,  						   --ECD_ORDRE,
lemandat.exe_ordre,			   --EXE_ORDRE,
lemandat.ges_code,			   --GES_CODE,
lemandat.man_ht,			   --MAB_MONTANT,
'VISA SALAIRES',				   --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',						   --MAB_SENS,
manid,						   --MAN_ID,
lemandat.pco_num			   --PCO_NU
);

END;

PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid 	  		 DEPENSE.dep_id%TYPE;
jefyfacture 	 jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse  	 DEPENSE.dep_adresse%TYPE;
founom  		 DEPENSE.dep_fournisseur%TYPE;
lotordre  		 DEPENSE.dep_lot%TYPE;
marordre		 DEPENSE.dep_marches%TYPE;
fouordre		 DEPENSE.fou_ordre%TYPE;
gescode			 DEPENSE.ges_code%TYPE;
cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;

lemandat MANDAT%ROWTYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
 			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,				   --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,			   --DEP_ETAT,
founom ,			   --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
lotordre ,			   --DEP_LOT,
marordre ,			   --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,				   --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,				   --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,			   --EXE_ORDRE,
fouordre, 			   --FOU_ORDRE,
gescode,  			   --GES_CODE,
manid ,				   --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,    		   --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Mandat_05 IS
-- version 1.1.8 du 07/07/2005 - modifs sur le mod_ordre dans depense
-- version 1.1.7 du 31/05/2005 - modifs concernant les prestations internes
-- version 1.1.6 du 25/04/2005 - modifs concernant les prestations internes
-- version 1.1.8 du 15/09/2005 - modifs pour compatibilite avec nouveau gestion_exercice
-- version 1.1.9 du 26/09/2005 - modifs pour emargements semi-auto
-- version 1.2.0 du 20/12/2005 - modifs pour multi exercice jefy


PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

	cpt 	 	INTEGER;

	lebordereau JEFY05.bordero%ROWTYPE;

	agtordre 	JEFY05.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	etat		JEFY05.bordero.bor_stat%TYPE;
BEGIN

	SELECT bor_stat INTO etat FROM JEFY05.bordero
		   WHERE bor_ordre = borordre;

	IF etat = 'N' THEN

		-- est ce un bordereau de titre --
		SELECT COUNT(*) INTO cpt FROM JEFY05.MANDAT
		WHERE bor_ordre = borordre;

		IF cpt != 0 THEN

			tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

			-- le bordereau est il deja vis  ? --
			SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;

			-- l exercice est il encore ouvert ? --


			-- pas de raise mais pas de recup s il est deja dans maracuja --
			IF cpt = 0 THEN
				-- recup des infos --
				SELECT * INTO lebordereau
				FROM JEFY05.bordero
				WHERE bor_ordre = borordre;

				-- recup de l agent de ce bordereau --
				/*select max(agt_ordre) into agtordre
				from JEFY05.facture
				where man_ordre =
				 ( select max(man_ordre)
				   from JEFY05.mandat
				   where bor_ordre =borordre
				  );

				-- recuperation du type de bordereau --
				select count(*) into cpt
				from old_agent_type_bord
				where agt_ordre = agtordre;

				if cpt <> 0 then
				 select tbo_ordre into tboordre
				 from old_agent_type_bord
				 where agt_ordre = agtordre;
				else
				 select tbo_ordre into tboordre
				 from type_bordereau
				 where tbo_ordre = (select par_value from parametre where par_key ='BTME GENERIQUE' and exe_ordre = exeordre);
				end if;
				*/


				-- creation du bor_id --
				SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

				-- recuperation de l utilisateur --
				/*select utl_ordre into utlordre
				from utilisateur
				where agt_ordre = agtordre;
				*/
				-- TEST  TEST  TEST  TEST  TEST
				SELECT 1 INTO utlordre
				FROM dual;
				-- TEST  TEST  TEST  TEST  TEST

				-- creation du bordereau --
				INSERT INTO BORDEREAU VALUES (
					NULL , 	 	 	      --BOR_DATE_VISA,
					'VALIDE',			  --BOR_ETAT,
					borid,				  --BOR_ID,
					leBordereau.bor_num,  --BOR_NUM,
					leBordereau.bor_ordre,--BOR_ORDRE,
					exeordre,	  		  --EXE_ORDRE,
					leBordereau.ges_code, --GES_CODE,
					tboordre,			  --TBO_ORDRE,
					utlordre,		 	  --UTL_ORDRE,
					NULL				  --UTL_ORDRE_VISA
					);

				--get_mandat_JEFY_proc(exeordre,borordre,borid,utlordre,agtordre);
				Bordereau_Mandat_05.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

			END IF;
			--else
			-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
		END IF;
	END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	cpt 	 	INTEGER;

	lejefymandat JEFY05.MANDAT%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	manid 		 MANDAT.man_id%TYPE;

	MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
	MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
	ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
	PRESTID 	   MANDAT.PREST_ID%TYPE;
	TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
	VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
	modordre	   MANDAT.mod_ordre%TYPE;
	CURSOR lesJefyMandats IS
		SELECT *
			FROM JEFY05.MANDAT
			WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefyMandats;
	LOOP
		FETCH lesJefyMandats INTO lejefymandat;
			  EXIT WHEN lesJefyMandats%NOTFOUND;

		-- recuperation du ges_code --
		SELECT ges_code INTO gescode
			FROM BORDEREAU
			WHERE bor_id = borid;

		-- recuperations --
		--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
		MANORGINE_KEY:=NULL;

		--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
		MANORIGINE_LIB:=NULL;

		--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
		IF (lejefymandat.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
		END IF;

		--PRESTID : PRESTATION INTERNE --
		PRESTID :=NULL;

		--TORORDRE : ORIGINE DU MANDAT --
		TORORDRE := 1;

		--VIRORDRE --
		VIRORDRE := NULL;

		--MOD_ORDRE --
		SELECT mod_ordre INTO modordre
		FROM MODE_PAIEMENT
		WHERE mod_code =
		(
		 SELECT MAX(mod_code)
		 FROM JEFY05.factures
		 WHERE man_ordre = lejefymandat.man_ordre
		)
		AND exe_ordre=exeordre;

		-- creation du man_id --
		SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

		-- creation du mandat --
		INSERT INTO MANDAT (BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE,
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT,
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET, MAN_NB_PIECE,
		MAN_NUMERO, MAN_NUMERO_REJET, MAN_ORDRE, MAN_ORGINE_KEY,
		MAN_ORIGINE_LIB, MAN_TTC, MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM,
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE, RIB_ORDRE_ORDONNATEUR,
		RIB_ORDRE_COMPTABLE, MAN_ATTENTE_PAIEMENT, MAN_ATTENTE_DATE,
		MAN_ATTENTE_OBJET, UTL_ORDRE_ATTENTE) VALUES (
		 borid ,		   		--BOR_ID,
		 NULL, 			   		--BRJ_ORDRE,
		 exeordre,		   		--EXE_ORDRE,
		 lejefymandat.fou_ordre,--FOU_ORDRE,
		 gescode,				--GES_CODE,
		 NULL,				    --MAN_DATE_REMISE,
		 NULL,					--MAN_DATE_VISA_PRINC,
		 'ATTENTE',				--MAN_ETAT,
		 'ATTENTE',			    --MAN_ETAT_REMISE,
		 lejefymandat.man_mont, --MAN_HT,
		 manid,					--MAN_ID,
		 NULL,					--MAN_MOTIF_REJET,
		 lejefymandat.man_piece,--MAN_NB_PIECE,
		 lejefymandat.man_num,	--MAN_NUMERO,
		 NULL,					--MAN_NUMERO_REJET,
		 lejefymandat.man_ordre,--MAN_ORDRE,
		 MANORGINE_KEY,			--MAN_ORGINE_KEY,
		 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
		 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
		 lejefymandat.man_tva,  --MAN_TVA,
		 modordre,				--MOD_ORDRE,
		 ORIORDRE,				--ORI_ORDRE,
		 lejefymandat.pco_num,	--PCO_NUM,
		 lejefymandat.PRES_ORDRE,				--PREST_ID,
		 TORORDRE,				--TOR_ORDRE,
		 VIRORDRE,				--VIR_ORDRE
		 lejefymandat.org_ordre,  --org_ordre
		 lejefymandat.rib_ordre, --rib ordo
		 lejefymandat.rib_ordre, -- rib_comptable
		 0,
		 NULL,
		 NULL,
		 NULL
		);

		--on met a jour le type de bordereau s'il s'agit d'un mandat faisant partie d'une prestation interne

		--select count(*) into cpt from v_titre_prest_interne where man_ordre=lejefymandat.man_ordre and tit_ordre is not null;
		--if cpt != 0 then
		IF lejefymandat.PRES_ORDRE IS NOT NULL THEN
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;


		Bordereau_Mandat_05.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
		Bordereau_Mandat_05.set_mandat_brouillard(manid);

	END LOOP;

	CLOSE lesJefyMandats;
	/*
	EXCEPTION
	WHEN OTHERS THEN
	 RAISE_APPLICATION_ERROR (-20002,'mandat :'||lejefymandat.man_ordre);
	*/
END;



PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;



--	select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--	if cpt = 0 then
	IF lemandat.prest_id IS NULL THEN
		-- creation du mandat_brouillard visa DEBIT--
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			lemandat.exe_ordre,			   --EXE_ORDRE,
			lemandat.ges_code,			   --GES_CODE,
			lemandat.man_ht,			   --MAB_MONTANT,
			'VISA MANDAT',				   --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
			'D',						   --MAB_SENS,
			manid,						   --MAN_ID,
			lemandat.pco_num			   --PCO_NU
			);


		-- credit=ctrepartie
		--debit = ordonnateur
		-- recup des infos du VISA CREDIT --
		SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

		IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
		END IF;

		SELECT pco_num_ctrepartie, PCO_NUM_TVA
			INTO pconum_ctrepartie, PCONUM_TVA
			FROM PLANCO_VISA
			WHERE pco_num_ordonnateur = lemandat.pco_num;


		SELECT  COUNT(*) INTO cpt
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;

		IF cpt != 0 THEN
			SELECT  pco_num_visa INTO pconum_ctrepartie
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;
		END IF;



		-- recup de l agence comptable --
-- 		SELECT c.ges_code,PCO_NUM_185
-- 			INTO gescodecompta,pconum_185
-- 			FROM GESTION g, COMPTABILITE c
-- 			WHERE g.ges_code = lemandat.ges_code
-- 			AND g.com_ordre = c.com_ordre;
--
			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;


		SELECT par_value   INTO parvalue
			FROM PARAMETRE
			WHERE par_key ='CONTRE PARTIE VISA'
			AND exe_ordre = lemandat.exe_ordre;

		IF parvalue = 'COMPOSANTE' THEN
		   gescodecompta := lemandat.ges_code;
		END IF;

		IF pconum_185 IS NULL THEN
			-- creation du mandat_brouillard visa CREDIT --
			INSERT INTO MANDAT_BROUILLARD VALUES (
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			gescodecompta,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		ELSE
			--au SACD --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		END IF;

		IF lemandat.man_tva != 0 THEN
			-- creation du mandat_brouillard visa CREDIT TVA --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
		END IF;

		/*
		if pconum_185 is not null then

		-- creation du mandat_brouillard visa SACD DEBIT --
		-- creation du mandat_brouillard visa DEBIT--
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'C',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);


		-- creation du mandat_brouillard visa SACD CREDIT --
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			       --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);

		end if;

		*/
	ELSE
		Bordereau_Mandat_05.set_mandat_brouillard_intern(manid);
	END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;
	leplancomptable 	  PLAN_COMPTABLE%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;


-- 	-- recup de l agence comptable --
-- 	SELECT c.ges_code,PCO_NUM_185
-- 		INTO gescodecompta,pconum_185
-- 		FROM GESTION g, COMPTABILITE c
-- 		WHERE g.ges_code = lemandat.ges_code
-- 		AND g.com_ordre = c.com_ordre;


			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;



	-- recup des infos du VISA CREDIT --
	SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	IF cpt = 0 THEN
	   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
	END IF;
	SELECT pco_num_ctrepartie, PCO_NUM_TVA
		INTO pconum_ctrepartie, PCONUM_TVA
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||lemandat.pco_num;

	IF cpt = 0 THEN
	 SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
	 WHERE pco_num = lemandat.pco_num;

	 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
	END IF;

--	lemandat.pco_num := '18'||lemandat.pco_num;

	-- creation du mandat_brouillard visa DEBIT--
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ht,			   --MAB_MONTANT,
		'VISA MANDAT',				   --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		'18'||lemandat.pco_num			   --PCO_NU
		);

	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '181';

	IF cpt = 0 THEN
		 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');
		--ELSE
		-- SELECT * INTO leplancomptable FROM plan_comptable
		-- WHERE pco_num = '181';
	END IF;


	-- planco de CREDIT 181
	-- creation du mandat_brouillard visa CREDIT --
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						  --ECD_ORDRE,
		lemandat.exe_ordre,			  --EXE_ORDRE,
		gescodecompta,				  --GES_CODE,
		lemandat.man_ttc,		  	  --MAB_MONTANT,
		'VISA MANDAT',				  --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
		'C',						  --MAB_SENS,
		manid,						  --MAN_ID,
		'181'				  --PCO_NU
		);



	IF lemandat.man_tva != 0 THEN
		-- creation du mandat_brouillard visa CREDIT TVA --
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
	END IF;

END;

PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
	niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


PROCEDURE get_facture_jefy
	(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefyfacture 	 JEFY05.factures%ROWTYPE;
	lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	fouadresse  	 DEPENSE.dep_adresse%TYPE;
	founom  		 DEPENSE.dep_fournisseur%TYPE;
	lotordre  		 DEPENSE.dep_lot%TYPE;
	marordre		 DEPENSE.dep_marches%TYPE;
	fouordre		 DEPENSE.fou_ordre%TYPE;
	gescode			 DEPENSE.ges_code%TYPE;
	modordre		 DEPENSE.mod_ordre%TYPE;
	cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;
	ecd_ordre_ema	 ECRITURE_DETAIL.ecd_ordre%TYPE;

	CURSOR factures IS
	 SELECT * FROM JEFY05.factures
	 WHERE man_ordre = manordre;

BEGIN

	OPEN factures;
	LOOP
		FETCH factures INTO jefyfacture;
			  EXIT WHEN factures%NOTFOUND;

		-- creation du depid --
		SELECT depense_seq.NEXTVAL INTO depid FROM dual;


		SELECT COUNT(*) INTO cpt FROM JEFY05.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		 IF cpt = 0 THEN
			-- creation de lignebudgetaire--
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM JEFY05.engage
				 WHERE cde_ordre = JEFYfacture.cde_ordre
				 AND eng_stat !='A'
				);
		ELSE
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				 SELECT  MAX(org_ordre)
				 FROM JEFY05.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);
		END IF;

		-- recuperations --

		-- gescode --
		SELECT COUNT(*) INTO cpt FROM JEFY05.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		IF cpt = 0 THEN

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM JEFY05.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;



			SELECT org_comp
				INTO gescode
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM JEFY05.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouordre --
			 SELECT fou_ordre INTO fouordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				 SELECT lot_ordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
				   SELECT lot_ordre
				   FROM JEFY05.commande
				   WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;



		ELSE
			 --recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM JEFY05.facture_ext WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

			SELECT org_comp
				INTO gescode
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				SELECT MAX(org_ordre)
				 FROM JEFY05.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre  FROM MANDAT
				 WHERE man_id = manid
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre
				 FROM MANDAT
				 WHERE man_id = manid
				);



			-- fouordre --
			SELECT fou_ordre INTO fouordre
			 FROM MANDAT
			 WHERE man_id = manid;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				SELECT MAX(lot_ordre)
				 FROM JEFY05.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
					  SELECT MAX(lot_ordre)
					  FROM JEFY05.facture_ext
					  WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;
		END IF;




		-- marordre --
		SELECT COUNT(*) INTO cpt
			FROM marches.lot
			WHERE lot_ordre = lotordre;

		IF cpt = 0 THEN
		   	   marordre :=NULL;
		ELSE
			 SELECT mar_ordre
				 INTO marordre
				 FROM marches.lot
				 WHERE lot_ordre = lotordre;
		END IF;



			--MOD_ORDRE --
			SELECT mod_ordre INTO modordre
			FROM MODE_PAIEMENT
			WHERE mod_code =jefyfacture.mod_code AND exe_ordre=exeordre;


		-- recuperer l'ecriture_detail pour emargements semi-auto
		SELECT MAX(ecd_ordre) INTO ecd_ordre_ema FROM JEFY05.facture_emargement WHERE dep_ordre=jefyfacture.dep_ordre;




		-- creation de la depense --
		INSERT INTO DEPENSE VALUES
			(
			fouadresse ,           --DEP_ADRESSE,
			NULL ,				   --DEP_DATE_COMPTA,
			jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
			jefyfacture.dep_date , --DEP_DATE_SERVICE,
			'VALIDE' ,			   --DEP_ETAT,
			founom ,			   --DEP_FOURNISSEUR,
			jefyfacture.dep_mont , --DEP_HT,
			depense_seq.NEXTVAL ,  --DEP_ID,
			lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
			lotordre ,			   --DEP_LOT,
			marordre ,			   --DEP_MARCHES,
			jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
			jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
			jefyfacture.dep_fact  ,--DEP_NUMERO,
			jefyfacture.dep_ordre ,--DEP_ORDRE,
			NULL ,				   --DEP_REJET,
			jefyfacture.rib_ordre ,--DEP_RIB,
			'NON' ,				   --DEP_SUPPRESSION,
			jefyfacture.dep_ttc ,  --DEP_TTC,
			jefyfacture.dep_ttc - jefyfacture.dep_mont, -- DEP_TVA,
			exeordre ,			   --EXE_ORDRE,
			fouordre, 			   --FOU_ORDRE,
			gescode,  			   --GES_CODE,
			manid ,				   --MAN_ID,
			jefyfacture.man_ordre, --MAN_ORDRE,
--			jefyfacture.mod_code,  --MOD_ORDRE,
			modordre,
			jefyfacture.pco_num ,  --PCO_ORDRE,
			utlordre,    		   --UTL_ORDRE
			NULL, --org_ordre
			tcdordre,
			ecd_ordre_ema -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
			);

	END LOOP;
	CLOSE factures;

END;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Mandat_Non_Admis
IS
--version 1.2.0 20/12/2005 - Multi exercices jefy

   PROCEDURE expedier_btmna (brjordre INTEGER, exeordre INTEGER)
   IS
      cpt            INTEGER;
      borordrejefy   INTEGER;
      tbolibelle     TYPE_BORDEREAU.tbo_libelle%TYPE;
      manordre       MANDAT.man_ordre%TYPE;
      lemandat       MANDAT%ROWTYPE;
      ladepense      DEPENSE%ROWTYPE;
      manid          MANDAT.man_id%TYPE;
      brjnum         BORDEREAU_REJET.brj_num%TYPE;
      lesmanordres   VARCHAR2 (2000);
      lesdepordres   VARCHAR2 (1000);
      ex             INTEGER;

-- les mandats rejetes --
      CURSOR c1
      IS
         SELECT *
           FROM MANDAT
          WHERE brj_ordre = brjordre;

-- depenses a supprimer --
      CURSOR c2
      IS
         SELECT *
           FROM DEPENSE
          WHERE man_id = manid AND dep_suppression = 'OUI';
   BEGIN
      SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

/*
lesmanordre
   "manordre$expli$manordre$expli$.......$manordre$expli$$"
   lesdepordres
   "depordre$depordre$.......$depordre$$"
   */
      lesmanordres := '';
      lesdepordres := '';

      SELECT f.tbo_libelle, b.brj_num
        INTO tbolibelle, brjnum
        FROM BORDEREAU_REJET b, TYPE_BORDEREAU f
       WHERE b.brj_ordre = brjordre AND b.tbo_ordre = f.tbo_ordre;

      SELECT bor_ordre
        INTO borordrejefy
        FROM BORDEREAU
       WHERE bor_id IN (SELECT MAX (bor_id)
                          FROM MANDAT
                         WHERE brj_ordre = brjordre);


	   IF ex IS NULL
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'PARAMETRE EXERCICE NON DEFINI');
            END IF;

      IF exeordre = ex
      THEN
         SELECT COUNT (*)
           INTO cpt
           FROM jefy.bordero_rejet
          WHERE bor_ordre = borordrejefy + 1000000;

         IF cpt = 0
         THEN
            IF tbolibelle != 'BORDEREAU DE MANDAT NON ADMIS'
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTMNA');
            END IF;

            OPEN c1;

            LOOP
               FETCH c1
                INTO lemandat;

               EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
               UPDATE jefy.MANDAT
                  SET man_stat = 'A'
                WHERE man_ordre = lemandat.man_ordre;

-- les facture_rejet --
               INSERT INTO jefy.facture_rejet
                  SELECT dep_ordre, dep_fact, dep_date, dep_lib, dep_ttc,
                         dep_mont, dep_piece, dep_sect, can_code, rib_ordre,
                         mod_code, pco_num, dst_code, agt_ordre, man_ordre,
                         cde_ordre, dep_monnaie, dep_virement, dep_inventaire,
                         dep_ht_saisie, dep_creation, dep_prorata, cm_ordre,
                         cm_type, cm_achat, 'N',
                         NVL (dep_datecomposante, SYSDATE),
                         NVL (dep_datedaf, SYSDATE), dep_lucrativite
                    FROM jefy.factures
                   WHERE man_ordre = lemandat.man_ordre;

               lesmanordres :=
                     lemandat.man_ordre
                  || '$'
                  || SUBSTR (lemandat.man_motif_rejet, 1, 200)
                  || '$'
                  || lesmanordres;
               manid := lemandat.man_id;

               OPEN c2;

               LOOP
                  FETCH c2
                   INTO ladepense;

                  EXIT WHEN c2%NOTFOUND;
                  lesdepordres := ladepense.dep_ordre || '$' || lesdepordres;
               END LOOP;

               CLOSE c2;
            END LOOP;

            CLOSE c1;

            lesmanordres := lesmanordres || '$$';
            lesdepordres := lesdepordres || '$$';
            jefy.maj_mandat_rejet (lesmanordres, lesdepordres, brjnum);
         END IF;
      ELSE
         IF exeordre = 2005
         THEN
            SELECT COUNT (*)
              INTO cpt
              FROM jefy05.bordero_rejet
             WHERE bor_ordre = borordrejefy + 1000000;

            IF cpt = 0
            THEN
               IF tbolibelle != 'BORDEREAU DE MANDAT NON ADMIS'
               THEN
                  RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTMNA');
               END IF;

               OPEN c1;

               LOOP
                  FETCH c1
                   INTO lemandat;

                  EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
                  UPDATE jefy05.MANDAT
                     SET man_stat = 'A'
                   WHERE man_ordre = lemandat.man_ordre;

-- les facture_rejet --
                  INSERT INTO jefy05.facture_rejet
                     SELECT dep_ordre, dep_fact, dep_date, dep_lib, dep_ttc,
                            dep_mont, dep_piece, dep_sect, can_code,
                            rib_ordre, mod_code, pco_num, dst_code, agt_ordre,
                            man_ordre, cde_ordre, dep_monnaie, dep_virement,
                            dep_inventaire, dep_ht_saisie, dep_creation,
                            dep_prorata, cm_ordre, cm_type, cm_achat, 'N',
                            NVL (dep_datecomposante, SYSDATE),
                            NVL (dep_datedaf, SYSDATE), dep_lucrativite
                       FROM jefy05.factures
                      WHERE man_ordre = lemandat.man_ordre;

                  lesmanordres :=
                        lemandat.man_ordre
                     || '$'
                     || SUBSTR (lemandat.man_motif_rejet, 1, 200)
                     || '$'
                     || lesmanordres;
                  manid := lemandat.man_id;

                  OPEN c2;

                  LOOP
                     FETCH c2
                      INTO ladepense;

                     EXIT WHEN c2%NOTFOUND;
                     lesdepordres :=
                                   ladepense.dep_ordre || '$' || lesdepordres;
                  END LOOP;

                  CLOSE c2;
               END LOOP;

               CLOSE c1;

               lesmanordres := lesmanordres || '$$';
               lesdepordres := lesdepordres || '$$';
               jefy05.maj_mandat_rejet (lesmanordres, lesdepordres,brjnum);
            END IF;
		 ELSE
		 	 RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');
         END IF;
      END IF;
   END;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Mandat IS
-- version 1.1.8 du 07/07/2005 - modifs sur le mod_ordre dans depense
-- version 1.1.7 du 31/05/2005 - modifs concernant les prestations internes
-- version 1.1.6 du 25/04/2005 - modifs concernant les prestations internes
-- version 1.1.8 du 15/09/2005 - modifs pour compatibilite avec nouveau gestion_exercice
-- version 1.1.9 du 26/09/2005 - modifs pour emargements semi-auto
-- version 1.5.0 du 08/12/2006 -- modifs pour attentes de paiement

PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	etat		jefy.bordero.bor_stat%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;

	 IF (ex <> ex2) THEN
	 	RAISE_APPLICATION_ERROR (-20001,'MAUVAIS EXERCICE');
	 END IF;


	SELECT bor_stat INTO etat FROM jefy.bordero
		   WHERE bor_ordre = borordre;

	IF etat = 'N' THEN

		-- est ce un bordereau de titre --
		SELECT COUNT(*) INTO cpt FROM jefy.MANDAT
		WHERE bor_ordre = borordre;

		IF cpt != 0 THEN

			tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

			-- le bordereau est il deja visé ? --
			SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;

			-- l exercice est il encore ouvert ? --


			-- pas de raise mais pas de recup s il est deja dans maracuja --
			IF cpt = 0 THEN
				-- recup des infos --
				SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;

				-- recup de l agent de ce bordereau --
				/*select max(agt_ordre) into agtordre
				from jefy.facture
				where man_ordre =
				 ( select max(man_ordre)
				   from jefy.mandat
				   where bor_ordre =borordre
				  );

				-- recuperation du type de bordereau --
				select count(*) into cpt
				from old_agent_type_bord
				where agt_ordre = agtordre;

				if cpt <> 0 then
				 select tbo_ordre into tboordre
				 from old_agent_type_bord
				 where agt_ordre = agtordre;
				else
				 select tbo_ordre into tboordre
				 from type_bordereau
				 where tbo_ordre = (select par_value from parametre where par_key ='BTME GENERIQUE' and exe_ordre = exeordre);
				end if;
				*/


				-- creation du bor_id --
				SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

				-- recuperation de l utilisateur --
				/*select utl_ordre into utlordre
				from utilisateur
				where agt_ordre = agtordre;
				*/
				-- TEST  TEST  TEST  TEST  TEST
				SELECT 1 INTO utlordre
				FROM dual;
				-- TEST  TEST  TEST  TEST  TEST

				-- creation du bordereau --
				INSERT INTO BORDEREAU VALUES (
					NULL , 	 	 	      --BOR_DATE_VISA,
					'VALIDE',			  --BOR_ETAT,
					borid,				  --BOR_ID,
					leBordereau.bor_num,  --BOR_NUM,
					leBordereau.bor_ordre,--BOR_ORDRE,
					exeordre,	  		  --EXE_ORDRE,
					leBordereau.ges_code, --GES_CODE,
					tboordre,			  --TBO_ORDRE,
					utlordre,		 	  --UTL_ORDRE,
					NULL,				  --UTL_ORDRE_VISA
					SYSDATE
					);

				--get_mandat_jefy_proc(exeordre,borordre,borid,utlordre,agtordre);
				Bordereau_Mandat.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

			END IF;
			--else
			-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
		END IF;
	END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	cpt 	 	INTEGER;

	lejefymandat jefy.MANDAT%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	manid 		 MANDAT.man_id%TYPE;

	MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
	MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
	ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
	PRESTID 	   MANDAT.PREST_ID%TYPE;
	TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
	VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
	modordre	   MANDAT.mod_ordre%TYPE;
	CURSOR lesJefyMandats IS
		SELECT *
			FROM jefy.MANDAT
			WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefyMandats;
	LOOP
		FETCH lesJefyMandats INTO lejefymandat;
			  EXIT WHEN lesJefyMandats%NOTFOUND;

		-- recuperation du ges_code --
		SELECT ges_code INTO gescode
			FROM BORDEREAU
			WHERE bor_id = borid;

		-- recuperations --
		--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
		MANORGINE_KEY:=NULL;

		--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
		MANORIGINE_LIB:=NULL;

		--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
		IF (lejefymandat.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
		END IF;

		--PRESTID : PRESTATION INTERNE --
		PRESTID :=NULL;

		--TORORDRE : ORIGINE DU MANDAT --
		TORORDRE := 1;

		--VIRORDRE --
		VIRORDRE := NULL;

		--MOD_ORDRE --
		SELECT mod_ordre INTO modordre
		FROM MODE_PAIEMENT
		WHERE mod_code =
		(
		 SELECT MAX(mod_code)
		 FROM jefy.factures
		 WHERE man_ordre = lejefymandat.man_ordre
		)
		AND exe_ordre=exeordre;

		-- creation du man_id --
		SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

		-- creation du mandat --
		INSERT INTO MANDAT(BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE,
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT,
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET,
		MAN_NB_PIECE, MAN_NUMERO, MAN_NUMERO_REJET,
		MAN_ORDRE, MAN_ORGINE_KEY, MAN_ORIGINE_LIB, MAN_TTC,
		MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM,
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE,
		RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE,
		MAN_ATTENTE_PAIEMENT,
		MAN_ATTENTE_DATE,
		MAN_ATTENTE_OBJET,
		UTL_ORDRE_ATTENTE) VALUES (
		 borid ,		   		--BOR_ID,
		 NULL, 			   		--BRJ_ORDRE,
		 exeordre,		   		--EXE_ORDRE,
		 lejefymandat.fou_ordre,--FOU_ORDRE,
		 gescode,				--GES_CODE,
		 NULL,				    --MAN_DATE_REMISE,
		 NULL,					--MAN_DATE_VISA_PRINC,
		 'ATTENTE',				--MAN_ETAT,
		 'ATTENTE',			    --MAN_ETAT_REMISE,
		 lejefymandat.man_mont, --MAN_HT,
		 manid,					--MAN_ID,
		 NULL,					--MAN_MOTIF_REJET,
		 lejefymandat.man_piece,--MAN_NB_PIECE,
		 lejefymandat.man_num,	--MAN_NUMERO,
		 NULL,					--MAN_NUMERO_REJET,
		 lejefymandat.man_ordre,--MAN_ORDRE,
		 MANORGINE_KEY,			--MAN_ORGINE_KEY,
		 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
		 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
		 lejefymandat.man_tva,  --MAN_TVA,
		 modordre,				--MOD_ORDRE,
		 ORIORDRE,				--ORI_ORDRE,
		 lejefymandat.pco_num,	--PCO_NUM,
		 lejefymandat.PRES_ORDRE,				--PREST_ID,
		 TORORDRE,				--TOR_ORDRE,
		 VIRORDRE,				--VIR_ORDRE
		 lejefymandat.org_ordre,  --org_ordre
		 lejefymandat.rib_ordre, --rib ordo
		 lejefymandat.rib_ordre, -- rib_comptable
		 4, 					   	--MAN_ATTENTE_PAIEMENT,
		NULL, 					--MAN_ATTENTE_DATE,
		NULL, 					--MAN_ATTENTE_OBJET,
		NULL 					--UTL_ORDRE_ATTENTE
		);

		--on met a jour le type de bordereau s'il s'agit d'un mandat faisant partie d'une prestation interne

		--select count(*) into cpt from v_titre_prest_interne where man_ordre=lejefymandat.man_ordre and tit_ordre is not null;
		--if cpt != 0 then
		IF lejefymandat.PRES_ORDRE IS NOT NULL THEN
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;


		Bordereau_Mandat.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
		Bordereau_Mandat.set_mandat_brouillard(manid);

	END LOOP;

	CLOSE lesJefyMandats;
	/*
	EXCEPTION
	WHEN OTHERS THEN
	 RAISE_APPLICATION_ERROR (-20002,'mandat :'||lejefymandat.man_ordre);
	*/
END;



PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;



--	select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--	if cpt = 0 then
	IF lemandat.prest_id IS NULL THEN
		-- creation du mandat_brouillard visa DEBIT--
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			lemandat.exe_ordre,			   --EXE_ORDRE,
			lemandat.ges_code,			   --GES_CODE,
			lemandat.man_ht,			   --MAB_MONTANT,
			'VISA MANDAT',				   --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
			'D',						   --MAB_SENS,
			manid,						   --MAN_ID,
			lemandat.pco_num			   --PCO_NU
			);


		-- credit=ctrepartie
		--debit = ordonnateur
		-- recup des infos du VISA CREDIT --
		SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

		IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
		END IF;

		SELECT pco_num_ctrepartie, PCO_NUM_TVA
			INTO pconum_ctrepartie, PCONUM_TVA
			FROM PLANCO_VISA
			WHERE pco_num_ordonnateur = lemandat.pco_num;


		SELECT  COUNT(*) INTO cpt
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;

		IF cpt != 0 THEN
			SELECT  pco_num_visa INTO pconum_ctrepartie
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;
		END IF;



		-- recup de l agence comptable --
-- 		SELECT c.ges_code,PCO_NUM_185
-- 			INTO gescodecompta,pconum_185
-- 			FROM GESTION g, COMPTABILITE c
-- 			WHERE g.ges_code = lemandat.ges_code
-- 			AND g.com_ordre = c.com_ordre;
--
			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;


		SELECT par_value   INTO parvalue
			FROM PARAMETRE
			WHERE par_key ='CONTRE PARTIE VISA'
			AND exe_ordre = lemandat.exe_ordre;

		IF parvalue = 'COMPOSANTE' THEN
		   gescodecompta := lemandat.ges_code;
		END IF;

		IF pconum_185 IS NULL THEN
			-- creation du mandat_brouillard visa CREDIT --
			INSERT INTO MANDAT_BROUILLARD VALUES (
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			gescodecompta,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		ELSE
			--au SACD --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		END IF;

		IF lemandat.man_tva != 0 THEN
			-- creation du mandat_brouillard visa CREDIT TVA --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
		END IF;

		/*
		if pconum_185 is not null then

		-- creation du mandat_brouillard visa SACD DEBIT --
		-- creation du mandat_brouillard visa DEBIT--
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'C',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);


		-- creation du mandat_brouillard visa SACD CREDIT --
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			       --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);

		end if;

		*/
	ELSE
		Bordereau_Mandat.set_mandat_brouillard_intern(manid);
	END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;
	leplancomptable 	  PLAN_COMPTABLE%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;


-- 	-- recup de l agence comptable --
-- 	SELECT c.ges_code,PCO_NUM_185
-- 		INTO gescodecompta,pconum_185
-- 		FROM GESTION g, COMPTABILITE c
-- 		WHERE g.ges_code = lemandat.ges_code
-- 		AND g.com_ordre = c.com_ordre;


			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;



	-- recup des infos du VISA CREDIT --
	SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	IF cpt = 0 THEN
	   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
	END IF;
	SELECT pco_num_ctrepartie, PCO_NUM_TVA
		INTO pconum_ctrepartie, PCONUM_TVA
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||lemandat.pco_num;

	IF cpt = 0 THEN
	 SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
	 WHERE pco_num = lemandat.pco_num;

	 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
	END IF;

--	lemandat.pco_num := '18'||lemandat.pco_num;

	-- creation du mandat_brouillard visa DEBIT--
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ht,			   --MAB_MONTANT,
		'VISA MANDAT',				   --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		'18'||lemandat.pco_num			   --PCO_NU
		);

	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '181';

	IF cpt = 0 THEN
		 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');
		--ELSE
		-- SELECT * INTO leplancomptable FROM plan_comptable
		-- WHERE pco_num = '181';
	END IF;


	-- planco de CREDIT 181
	-- creation du mandat_brouillard visa CREDIT --
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						  --ECD_ORDRE,
		lemandat.exe_ordre,			  --EXE_ORDRE,
		gescodecompta,				  --GES_CODE,
		lemandat.man_ttc,		  	  --MAB_MONTANT,
		'VISA MANDAT',				  --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
		'C',						  --MAB_SENS,
		manid,						  --MAN_ID,
		'181'				  --PCO_NU
		);



	IF lemandat.man_tva != 0 THEN
		-- creation du mandat_brouillard visa CREDIT TVA --
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
	END IF;

END;

PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
	niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


PROCEDURE get_facture_jefy
	(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefyfacture 	 jefy.factures%ROWTYPE;
	lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	fouadresse  	 DEPENSE.dep_adresse%TYPE;
	founom  		 DEPENSE.dep_fournisseur%TYPE;
	lotordre  		 DEPENSE.dep_lot%TYPE;
	marordre		 DEPENSE.dep_marches%TYPE;
	fouordre		 DEPENSE.fou_ordre%TYPE;
	gescode			 DEPENSE.ges_code%TYPE;
	modordre		 DEPENSE.mod_ordre%TYPE;
	cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;
	ecd_ordre_ema	 ECRITURE_DETAIL.ecd_ordre%TYPE;

	CURSOR factures IS
	 SELECT * FROM jefy.factures
	 WHERE man_ordre = manordre;

BEGIN

	OPEN factures;
	LOOP
		FETCH factures INTO jefyfacture;
			  EXIT WHEN factures%NOTFOUND;

		-- creation du depid --
		SELECT depense_seq.NEXTVAL INTO depid FROM dual;


		SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		 IF cpt = 0 THEN
			-- creation de lignebudgetaire--
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM jefy.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);
		ELSE
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT  MAX(org_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);
		END IF;

		-- recuperations --

		-- gescode --
		SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		IF cpt = 0 THEN

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;



			SELECT org_comp
				INTO gescode
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM jefy.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouordre --
			 SELECT fou_ordre INTO fouordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				 SELECT lot_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
				   SELECT lot_ordre
				   FROM jefy.commande
				   WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;



		ELSE
			 --recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.facture_ext WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

			SELECT org_comp
				INTO gescode
				FROM jefy.organ
				WHERE org_ordre =
				(
				SELECT MAX(org_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre  FROM MANDAT
				 WHERE man_id = manid
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre
				 FROM MANDAT
				 WHERE man_id = manid
				);



			-- fouordre --
			SELECT fou_ordre INTO fouordre
			 FROM MANDAT
			 WHERE man_id = manid;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				SELECT MAX(lot_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
					  SELECT MAX(lot_ordre)
					  FROM jefy.facture_ext
					  WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;
		END IF;




		-- marordre --
		SELECT COUNT(*) INTO cpt
			FROM marches.lot
			WHERE lot_ordre = lotordre;

		IF cpt = 0 THEN
		   	   marordre :=NULL;
		ELSE
			 SELECT mar_ordre
				 INTO marordre
				 FROM marches.lot
				 WHERE lot_ordre = lotordre;
		END IF;



			--MOD_ORDRE --
			SELECT mod_ordre INTO modordre
			FROM MODE_PAIEMENT
			WHERE mod_code =jefyfacture.mod_code AND exe_ordre=exeordre;


		-- recuperer l'ecriture_detail pour emargements semi-auto
		SELECT MAX(ecd_ordre) INTO ecd_ordre_ema FROM jefy.facture_emargement WHERE dep_ordre=jefyfacture.dep_ordre;




		-- creation de la depense --
		INSERT INTO DEPENSE VALUES
			(
			fouadresse ,           --DEP_ADRESSE,
			NULL ,				   --DEP_DATE_COMPTA,
			jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
			jefyfacture.dep_date , --DEP_DATE_SERVICE,
			'VALIDE' ,			   --DEP_ETAT,
			founom ,			   --DEP_FOURNISSEUR,
			jefyfacture.dep_mont , --DEP_HT,
			depense_seq.NEXTVAL ,  --DEP_ID,
			lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
			lotordre ,			   --DEP_LOT,
			marordre ,			   --DEP_MARCHES,
			jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
			jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
			jefyfacture.dep_fact  ,--DEP_NUMERO,
			jefyfacture.dep_ordre ,--DEP_ORDRE,
			NULL ,				   --DEP_REJET,
			jefyfacture.rib_ordre ,--DEP_RIB,
			'NON' ,				   --DEP_SUPPRESSION,
			jefyfacture.dep_ttc ,  --DEP_TTC,
			jefyfacture.dep_ttc - jefyfacture.dep_mont, -- DEP_TVA,
			exeordre ,			   --EXE_ORDRE,
			fouordre, 			   --FOU_ORDRE,
			gescode,  			   --GES_CODE,
			manid ,				   --MAN_ID,
			jefyfacture.man_ordre, --MAN_ORDRE,
--			jefyfacture.mod_code,  --MOD_ORDRE,
			modordre,
			jefyfacture.pco_num ,  --PCO_ORDRE,
			utlordre,    		   --UTL_ORDRE
			NULL, --org_ordre
			tcdordre,
			ecd_ordre_ema, -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
			NULL
			);

	END LOOP;
	CLOSE factures;

END;

END;
/






CREATE OR REPLACE PACKAGE MARACUJA.gestionOrigine IS

/*
ORI_ENTITE  	  USER.TABLE  -
ORI_KEY_NAME 	  PRIMARY KEY -
ORI_LIBELLE 	  LIBELLE UTILISATEUR -
ORI_ORDRE 		  ID -
ORI_KEY_ENTITE	  VALEUR DE LA KEY -
TOP_ORDRE		  type d origine -
*/

FUNCTION traiter_orgordre (orgordre INTEGER) RETURN INTEGER;
FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION traiter_convordre (convordre INTEGER) RETURN  INTEGER;
FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
PROCEDURE maj_origine;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Gestionorigine IS
--version 1.1.7 31/05/2005 - mise a jour libelle origine dans le cas des conventions
--version 1.1.6 27/04/2005 - ajout maj_origine
--version 1.2.0 20/12/2005 - Multi exercices jefy
--version 1.2.1 04/01/2006
-- version 1.5.6 29/03/2007 - corrections doublons

FUNCTION traiter_orgordre (orgordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	 	   IF orgordre IS NULL THEN RETURN NULL; END IF;

		-- recup du type_origine --
		SELECT top_ordre INTO topordre FROM TYPE_OPERATION
		WHERE top_libelle = 'OPERATION LUCRATIVE';



		-- l origine est t elle deja  suivie --
		SELECT COUNT(*)INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'ORG_ORDRE'
		AND ORI_ENTITE ='JEFY.ORGAN'
		AND ORI_KEY_ENTITE	=orgordre;

		IF cpt >= 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'ORG_ORDRE'
			AND ORI_ENTITE ='JEFY.ORGAN'
			AND ORI_KEY_ENTITE	=orgordre
			AND ROWNUM=1;

		ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

			-- recup de la LIGNE BUDGETAIRE --
			--le libelle utilisateur pour le suivie en compta --
			SELECT org_comp||'-'||org_lbud||'-'||org_uc
			 INTO orilibelle
			FROM jefy.organ
			WHERE org_ordre = orgordre;

			INSERT INTO ORIGINE
			(ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
			VALUES ('JEFY.ORGAN','ORG_ORDRE',orilibelle,cpt,orgordre,topordre);

		END IF;

		RETURN cpt;

END;


FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
typeOrgan v_organ_exer.TYOR_ID%TYPE;

BEGIN

	 	  IF orgid IS NULL THEN RETURN NULL; END IF;

		  SELECT tyor_id INTO typeOrgan FROM jefy_admin.organ WHERE org_id=orgid;

			SELECT COUNT(*) INTO cpt
					FROM accords.CONVENTION_LIMITATIVE
					WHERE org_id = orgid AND exe_ordre = exeordre;

		  -- si c une convention RA
		  IF (typeOrgan = 2 OR cpt>0) THEN
					 -- recup du type_origine CONVENTION RA--
					 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
					 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

					SELECT COUNT(*) INTO cpt
							FROM accords.CONVENTION_LIMITATIVE
							WHERE org_id = orgid AND exe_ordre = exeordre;

						-- si on a affaire a une convention RA identifiee dans convention
						IF cpt >0 THEN
									-- recup de la convention
									  SELECT MAX(con_ordre) INTO convordre
									 FROM accords.CONVENTION_LIMITATIVE
									 WHERE org_id = orgid
									 AND exe_ordre = exeordre;

									 SELECT SUBSTR(EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET,1,2000)
									 INTO orilibelle
									 FROM accords.contrat
									 WHERE con_ordre = convordre;

						ELSE -- c une convention RA mais pas dans accords
									 SELECT COUNT(*) INTO cpt
									 FROM jefy_admin.organ
									 WHERE org_id = orgid;

									 IF cpt = 1 THEN
											 --le libelle utilisateur pour le suivie en compta --
											 SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
											 INTO orilibelle
											 FROM jefy_admin.organ
											 WHERE org_id = orgid;

									 ELSE -- pas d'organ trouvee
									  	 	RETURN NULL;
									 END IF;
							END IF;

		  ELSE
		  -- c pas une convention RA, c peut-etre une ligne lucrative
		  -- a voir s'il ne faut pas la traiter d'une part en tant que convention RA et d'autre par en tant que lucrative si c le cas
							 -- recup du type_origine OPERATION LUCRATIVE --
							 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
							 WHERE top_libelle = 'OPERATION LUCRATIVE';

 		  	   	   	   			  SELECT COUNT(*) INTO cpt
									 FROM jefy_admin.organ
									 WHERE org_id = orgid
									 AND org_lucrativite = 1;

									 IF cpt = 1 THEN
											 --le libelle utilisateur pour le suivie en compta --
										 SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
											 INTO orilibelle
											 FROM jefy_admin.organ
											 WHERE org_id = orgid;

									 ELSE -- pas d'organ lucrative trouvee
									  	 	RETURN NULL;
									 END IF;
		  END IF;

		-- l origine est t elle deja  suivie --
		SELECT COUNT(*) INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'ORG_ID'
		AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
		AND ORI_KEY_ENTITE	=orgid;

		IF cpt >= 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'ORG_ID'
			AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
			AND ORI_KEY_ENTITE	=orgid
			AND ROWNUM=1;

			--on met a jour le libelle
			UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

		ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
			INSERT INTO ORIGINE (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
			VALUES ('JEFY_ADMIN.ORGAN','ORG_ID',orilibelle,cpt,orgid,topordre);

		END IF;

		RETURN cpt;

END;


FUNCTION traiter_convordre (convordre INTEGER)
RETURN INTEGER

IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	IF convordre IS NULL THEN RETURN NULL; END IF;

	-- recup du type_origine --
	SELECT top_ordre INTO topordre FROM TYPE_OPERATION
	WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

	-- l origine est t elle deja  suivie --
	SELECT COUNT(*) INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;


	-- recup de la convention --
	--le libelle utilisateur pour le suivie en compta --
--	select CON_REFERENCE_EXTERNE||' '||CON_OBJET into orilibelle from convention.contrat where con_ordre=convordre;
--on change le libelle (demande INP Toulouse 30/05/2005)
--	SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM convention.contrat WHERE con_ordre=convordre;
		SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM accords.contrat WHERE con_ordre=convordre;


	-- l'origine est deja referencee
	IF cpt = 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'CONV_ORDRE'
			AND ORI_ENTITE ='CONVENTION.CONTRAT'
			AND ORI_KEY_ENTITE	=convordre;

			--on met a jour le libelle
			UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

	-- il faut creer l'origine
	ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
			INSERT INTO ORIGINE VALUES (
					'CONVENTION.CONTRAT' ,--	  USER.TABLE  -
					'CONV_ORDRE',-- 	  PRIMARY KEY -
					ORILIBELLE,-- 	  LIBELLE UTILISATEUR -
					cpt ,--		  ID -
					convordre,--	  VALEUR DE LA KEY -
					topordre--		  type d origine -
				);

	END IF;

	RETURN cpt;
END;


FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
letitrejefy jefy.TITRE%ROWTYPE;
BEGIN


		IF exeordre=2005 THEN
			SELECT * INTO lebordereau
			FROM jefy05.bordero
			WHERE bor_ordre = borordre;

			-- recup de l agent de ce bordereau --
			SELECT * INTO letitrejefy FROM jefy05.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
			   FROM jefy05.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre);
		ELSE
			SELECT * INTO lebordereau
			FROM jefy.bordero
			WHERE bor_ordre = borordre;

			-- recup de l agent de ce bordereau --
			SELECT * INTO letitrejefy FROM jefy.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
			   FROM jefy.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre);
		END IF;




		IF letitrejefy.tit_type ='V' THEN
		   SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REVERSEMENTS';
		ELSE
		  IF letitrejefy.tit_type ='D' THEN
		    SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REDUCTIONS';
		  ELSE
		    IF letitrejefy.tit_type ='P' THEN
		      SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU D ORDRE DE PAIEMENT';
		    ELSE
			  SELECT tbo_ordre INTO tboordre
		      FROM TYPE_BORDEREAU
		      WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' AND EXE_ORDRE=exeordre);
		    END IF;
		  END IF;
		END IF;


		RETURN tboordre;
END;

FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;
fouordrepapaye jefy.MANDAT.fou_ordre%TYPE;
agtordre 	jefy.facture.agt_ordre%TYPE;
fouordre 	jefy.MANDAT.fou_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN

		IF exeordre=2005 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
			FROM jefy05.bordero
			WHERE bor_ordre = borordre;

			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy05.facture
			WHERE man_ordre =
			 ( SELECT MAX(man_ordre)
			   FROM jefy05.MANDAT
			   WHERE bor_ordre =borordre
			  );

			SELECT MAX(fou_ordre) INTO fouordre
			   FROM jefy05.MANDAT
			   WHERE bor_ordre =borordre;
		ELSE
			-- recup des infos --
			SELECT * INTO lebordereau
			FROM jefy.bordero
			WHERE bor_ordre = borordre;

			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy.facture
			WHERE man_ordre =
			 ( SELECT MAX(man_ordre)
			   FROM jefy.MANDAT
			   WHERE bor_ordre =borordre
			  );

			SELECT MAX(fou_ordre) INTO fouordre
			   FROM jefy.MANDAT
			   WHERE bor_ordre =borordre;
		END IF;

		-- recuperation du type de bordereau --
		SELECT COUNT(*) INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
		 (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');


		IF (fouordrepapaye <> 0) THEN
		   SELECT fou_ordre INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
		   (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
		ELSE
			fouordrepapaye := NULL;
		END IF;



		IF fouordre =fouordrepapaye THEN
		  SELECT tbo_ordre INTO tboordre
		  FROM TYPE_BORDEREAU
		  WHERE tbo_libelle='BORDEREAU DE MANDAT SALAIRES';
		ELSE
		  SELECT COUNT(*) INTO cpt
		  FROM OLD_AGENT_TYPE_BORD
		  WHERE agt_ordre = agtordre;

		  IF cpt <> 0 THEN
		   SELECT tbo_ordre INTO tboordre
		   FROM OLD_AGENT_TYPE_BORD
		   WHERE agt_ordre = agtordre;
		  ELSE
		   SELECT tbo_ordre INTO tboordre
		   FROM TYPE_BORDEREAU
		   WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTME GENERIQUE' AND exe_ordre = exeordre);
		  END IF;

		END IF;


		RETURN tboordre;
END;

FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
BEGIN

RETURN 1;
END;


PROCEDURE Maj_Origine
IS
  -- cursor pour recuperer les organ lucratif + conventions RA
	CURSOR c1 IS
	SELECT org_id, exe_ordre FROM v_organ_exer o
	WHERE  exe_ordre>2006 AND (org_lucrativite !=0 OR tyor_id=2);

	CURSOR c2 IS
	SELECT org_id, exe_ordre FROM accords.convention_limitative WHERE org_id>0;
	orgordre INTEGER;
	conordre INTEGER;
	exeordre INTEGER;
	cpt INTEGER;
BEGIN
		-- On balaye les organ lucratives + taggees RA
		OPEN c1;
		LOOP
			FETCH C1 INTO orgordre, exeordre;
				  EXIT WHEN c1%NOTFOUND;
				  cpt := TRAITER_ORGid ( orgordre , exeordre);
		END LOOP;
		CLOSE c1;

		-- on balaye les conventions RA
		OPEN c2;
		LOOP
			FETCH C2 INTO orgordre, exeordre;
				  EXIT WHEN c2%NOTFOUND;
				  cpt := TRAITER_ORGid ( orgordre , exeordre);
		END LOOP;
		CLOSE c2;

-- Les conventions RA sont maintenant gerees a partir d'un Org_id
-- 		OPEN c2;
-- 		LOOP
-- 			FETCH C2 INTO conordre;
-- 				  EXIT WHEN c2%NOTFOUND;
-- 				  cpt := TRAITER_CONVORDRE ( conordre );
-- 		END LOOP;
-- 		CLOSE c2;


END;


END;
/


CREATE OR REPLACE PROCEDURE MARACUJA.corrige_ecriture_be_SACD
IS


/*
select ecd_ordre, x.ges_code
from ecriture_detail ecd,
(
 -- les ecriture BE sur SACD
	select distinct e.ecr_ordre, ecd.ges_code
	from ecriture_detail ecd, ecriture e
	where ecd.ecr_ordre=e.ecr_ordre and e.exe_ordre=2006 and e.tjo_ordre=2 and e.TOP_ORDRE=11
	and ecd.ges_code in
	(
		select ges_code
		from gestion_exercice
		where exe_ordre=2006 and pco_num_185 is not null
	)
) x
where
ecd.ecr_ordre = x.ecr_ordre
and ecd.ges_code=900
and ecd.pco_num=890

*/



ecdordre ecriture_detail.ecd_ordre%type;
gescode ecriture_detail.ges_code%type;
gescodeagence ecriture_detail.ges_code%type;

CURSOR c1 IS
Select ecd_ordre, x.ges_code
		from ecriture_detail ecd,
		(
		 -- les ecriture BE sur SACD
			select distinct e.ecr_ordre, ecd.ges_code
			from ecriture_detail ecd, ecriture e
			where ecd.ecr_ordre=e.ecr_ordre and e.exe_ordre=2006 and e.tjo_ordre=2 and e.TOP_ORDRE=11
			and ecd.ges_code in
			(
				select ges_code
				from gestion_exercice
				where exe_ordre=2006 and pco_num_185 is not null
			)
		) x
		where
		ecd.ecr_ordre = x.ecr_ordre
		and ecd.ges_code=gescodeagence
		and ecd.pco_num=890;



BEGIN

	 select min(ges_code) into gescodeagence from comptabilite;


	OPEN c1;
	LOOP
	FETCH C1 INTO ecdordre, gescode;
	EXIT WHEN c1%NOTFOUND;

		dbms_output.put_line('Mise a jour de ecd_ordre ' ||ecdordre);

		--mise à jour des ecritures
		update ecriture_detail set ges_code=gescode where ecd_ordre=ecdordre;

	END LOOP;
	CLOSE c1;



end;
/


