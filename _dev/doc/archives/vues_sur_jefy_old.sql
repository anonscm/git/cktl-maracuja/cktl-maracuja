CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_DESTIN
(EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV)
AS 
SELECT 2006 AS exe_exercice, d.* FROM jefy.destin d
   WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
  SELECT 2005 AS exe_exercice, d.* FROM jefy05.destin d
UNION ALL
  SELECT 2004 AS exe_exercice, d.* FROM jefy04.destin d
/


GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_DESTIN TO COMPTEFI
/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_DESTIN_REC
(EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV)
AS 
SELECT 2006 AS exe_exercice, d.* FROM jefy.destin_rec d
	   WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
	  SELECT 2005 AS exe_exercice, d.* FROM jefy05.destin_rec d
UNION ALL
	  SELECT 2004 AS exe_exercice, d.* FROM jefy04.destin_rec d
/


GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_DESTIN_REC TO COMPTEFI
/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_FACTURE
(EXE_EXERCICE, DEP_ORDRE, DEP_FACT, DEP_DATE, DEP_LIB, 
 DEP_TTC, DEP_MONT, DEP_PIECE, DEP_SECT, CAN_CODE, 
 RIB_ORDRE, MOD_CODE, PCO_NUM, DST_CODE, AGT_ORDRE, 
 MAN_ORDRE, CDE_ORDRE, DEP_MONNAIE, DEP_VIREMENT, DEP_INVENTAIRE, 
 DEP_HT_SAISIE, DEP_CREATION, DEP_PRORATA, CM_ORDRE, CM_TYPE, 
 CM_ACHAT, DEP_DATECOMPOSANTE, DEP_DATEDAF, DEP_LUCRATIVITE, DEP_RGP, 
 DEP_BUDNAT)
AS 
SELECT 2006 AS exe_exercice, f.* FROM jefy.facture f
	   WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
	  SELECT 2005 AS exe_exercice, f.* FROM jefy05.FACTURE f
UNION ALL
	  SELECT 2004 AS exe_exercice, f.* FROM jefy04.FACTURE f
/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_MANDAT
(EXE_EXERCICE, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, 
 MAN_STAT, FOU_ORDRE, PCO_NUM, BOR_ORDRE, RIB_ORDRE, 
 JOU_ORDRE, MAN_NUM, MAN_MONNAIE, ORG_ORDRE, CONV_ORDRE, 
 PRES_ORDRE)
AS 
SELECT 2006 AS exe_exercice, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, MAN_STAT,
 FOU_ORDRE, PCO_NUM, BOR_ORDRE,
RIB_ORDRE, JOU_ORDRE, MAN_NUM, MAN_MONNAIE, ORG_ORDRE, CONV_ORDRE, PRES_ORDRE
FROM jefy.MANDAT f
WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
SELECT 2005 AS exe_exercice, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, MAN_STAT,
	FOU_ORDRE,PCO_NUM,
	BOR_ORDRE, RIB_ORDRE, JOU_ORDRE, MAN_NUM, MAN_MONNAIE,
	ORG_ORDRE, -1 AS CONV_ORDRE, -1 AS PRES_ORDRE

FROM jefy05.MANDAT f
UNION ALL
SELECT 2004 AS exe_exercice, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, MAN_STAT,
	FOU_ORDRE,PCO_NUM,
	BOR_ORDRE, RIB_ORDRE, JOU_ORDRE, MAN_NUM, MAN_MONNAIE,
	ORG_ORDRE, -1 AS CONV_ORDRE, -1 AS PRES_ORDRE
FROM jefy04.MANDAT f
/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_TITRE
(EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE)
AS 
SELECT 2006 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE,
TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE,
TIT_VIREMENT, MOD_CODE,
DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
FROM jefy.TITRE f
WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
SELECT 2005 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '' AS can_code, -1 AS pres_ordre
FROM jefy05.TITRE f
UNION ALL
SELECT 2004 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '' AS can_code, -1 AS pres_ordre
FROM jefy04.TITRE f
UNION ALL
SELECT exe_ordre AS exe_exercice, tit_ordre, TIT_DATE_REMISE AS tit_date, REPLACE( TO_CHAR(TIT_TTC),',','.' )AS tit_mont,  REPLACE( TO_CHAR(TIT_TVA),',','.' )AS tit_monttva, tit_libelle AS tit_lib, NULL AS tit_type, tit_nb_piece AS tit_piece, TO_NUMBER(NULL) AS jou_ordre, f.fou_ordre, pco_num, NULL AS tit_imputtva, bor_ordre, org_ordre,
TO_NUMBER(NULL) AS tit_interne,  TIT_NUMero AS tit_num,utl_ORDRE AS agt_ordre, NULL AS TIT_STAT, GES_CODE,  f.ADR_NOM || ' ' || f.adr_prenom    AS TIT_DEBITEUR, TO_NUMBER(NULL) AS DEP_ORDRE, TO_NUMBER(NULL) AS RIB_ORDRE, NULL AS TIT_MONNAIE,
NULL AS TIT_VIREMENT, NULL AS MOD_CODE,
NULL AS DST_CODE, NULL AS TIT_REF, TO_NUMBER(NULL) AS CONV_ORDRE, NULL AS CAN_CODE, TO_NUMBER(NULL) AS PRES_ORDRE
FROM maracuja.TITRE t, v_fournisseur f
WHERE t.exe_ordre>2006 AND t.fou_ordre=f.fou_ordre
/


GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_TITRE TO COMPTEFI
/






CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN
(ORG_ORDRE, ORG_UNIT, ORG_COMP, ORG_LBUD, ORG_UC, 
 ORG_LIB, ORG_NIV, ORG_STAT, ORG_DATE, ORG_TVA, 
 SCT_CODE, ORG_RAT, DST_CODE, ORG_LUCRATIVITE, ORG_TYPE_LIGNE, 
 ORG_TYPE_FLECHAGE, ORG_OUVERTURE_CREDITS, ORG_OBSERVATION, OTC_CODE, ORG_DELEGATION)
AS 
SELECT ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
FROM jefy.organ
UNION ALL
SELECT ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
FROM jefy05.organ WHERE org_ordre IN (SELECT org_ordre FROM jefy05.organ MINUS SELECT org_ordre FROM jefy.organ)
/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_AGENT_JEFY
(AGT_ORDRE, AGT_NOM, AGT_PRENOM, AGT_LOGIN, AGT_LOCAL, 
 AGT_UID, ADR_ORDRE, AGT_PRE_CDE, AGT_CDE_ENG, AGT_PRE_FACT, 
 AGT_FACT_LIQ, AGT_MANDAT, AGT_COMPTA, AGT_BUDGET, AGT_FOURNIS, 
 AGT_TOUT, AGT_RESP, AGT_MARCHE, AGT_MISSION, AGT_TOUT_ORGAN, 
 AGT_TITRE, AGT_ADM, NO_INDIVIDU, AGT_MAIL, AGT_TAXE, 
 AGT_CONTINU, AGT_UNIQUE)
AS 
SELECT "AGT_ORDRE","AGT_NOM","AGT_PRENOM","AGT_LOGIN","AGT_LOCAL","AGT_UID","ADR_ORDRE","AGT_PRE_CDE","AGT_CDE_ENG","AGT_PRE_FACT","AGT_FACT_LIQ","AGT_MANDAT","AGT_COMPTA","AGT_BUDGET","AGT_FOURNIS","AGT_TOUT","AGT_RESP","AGT_MARCHE","AGT_MISSION","AGT_TOUT_ORGAN","AGT_TITRE","AGT_ADM","NO_INDIVIDU","AGT_MAIL","AGT_TAXE","AGT_CONTINU","AGT_UNIQUE" FROM jefy.agent
/



CREATE OR REPLACE FORCE VIEW COMPTEFI.V_JEFY_MULTIEX_SITU
(EXE_ORDRE, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_CODE, 
 PCO_NUM, DST_CODE, ENG_LIB, ENG_ORDRE, ENG_DATE, 
 ENG_TTC, ORG_ORDRE, DEP_ORDRE, DEP_TTC, MAN_ORDRE, 
 SI_EXER, MAN_DATE, CLE_ENG, CLE_DEP, CM_CODE, 
 RGP_NUMERO, CAN_CODE)
AS 
select 2005, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_code, pco_num, dst_code,
eng_lib, eng_ordre, eng_date,eng_ttc, org_ordre, dep_ordre, dep_ttc,
man_ordre, sit_exer, man_date, cle_eng, cle_dep, cm_code, rgp_numero, can_code from jefy05.situation
union all
select 2006, CDE_ORDRE, CDE_STAT, FOU_ORDRE, TCD_code, pco_num, dst_code,
eng_lib, eng_ordre, eng_date,eng_ttc, org_ordre, dep_ordre, dep_ttc,
man_ordre, sit_exer, man_date, cle_eng, cle_dep, cm_code, rgp_numero, can_code from jefy.situation
/





CREATE OR REPLACE FORCE VIEW COMPTEFI.V_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 1
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 2
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 3
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy.budnat b, jefy.exer e, jefy.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy.budnat b, jefy.exer e, jefy.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq)
FROM jefy.budnat b, jefy.organ o
WHERE b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY')
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq)
FROM jefy.budnat b, jefy.organ o
WHERE b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY')
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'RELI'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'RELI'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture
/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_WEB_PAYS
(REM_TAUX, WMO_CODE, WPA_CODE, WPA_LIBELLE)
AS 
select "REM_TAUX","WMO_CODE","WPA_CODE","WPA_LIBELLE" from missions_new.WEB_PAYS
/


