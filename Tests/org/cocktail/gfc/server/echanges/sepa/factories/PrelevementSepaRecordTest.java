package org.cocktail.gfc.server.echanges.sepa.factories;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PrelevementSepaRecordTest {

	@Test
	public void testCheckComplet() {
		PrelevementSepaRecord prelevementSepaRecord = PrelevementSepaTestHelpers.getPrelevementSepaRecord();
		String res = null;
		try {
			prelevementSepaRecord.checkComplet();
		} catch (Exception e) {
			res = e.getMessage();
		}
		assertNull("L'objet est complet, aucune exception ne devrait être levée", res);
		
		prelevementSepaRecord.setDateOfSignature(null);
		try {
			prelevementSepaRecord.checkComplet();
		} catch (Exception e) {
			res = e.getMessage();
		}
		assertNotNull("La date de signature est nulle, une exception devrait être levée", res);
		
	}
	
	@Test
	public void testGetAmendmentIndicator() {
		PrelevementSepaRecord record = PrelevementSepaTestHelpers.getPrelevementSepaRecord();
		assertFalse("Valeurs identiques aux anciennes valeurs : AmendmentIndicator doit etre false : " + record.getAmendmentIndicator() + "  " + record.toString(), record.getAmendmentIndicator());
		record.setPreviousDebtorBIC("CRLYFRPP");
		assertTrue("L'ancien Debtor BIC est différent, la valeur de AmendmentIndicator doit etre true : " + record.getAmendmentIndicator(), record.getAmendmentIndicator());
		record.setPreviousDebtorBIC("BNPAFRPP");
		assertFalse("L'ancien Debtor BIC est identique, la valeur de AmendmentIndicator doit etre false : " + record.getAmendmentIndicator() + "  " + record.toString(), record.getAmendmentIndicator());
		record.setPreviousDebtorIBAN("FR7618206002105487266700217");
		assertTrue("L'ancien Debtor IBAN est différent, la valeur de AmendmentIndicator doit etre true : " + record.getAmendmentIndicator(), record.getAmendmentIndicator());
		record.setPreviousDebtorIBAN("FR7030002005500000157845Z02");
		assertFalse("L'ancien Debtor IBAN est identique, la valeur de AmendmentIndicator doit etre false : " + record.getAmendmentIndicator() + "  " + record.toString(), record.getAmendmentIndicator());
		record.setPreviousMandatID("400480 CUFR ALBI 78177465");
		assertTrue("L'ancien identifiant du mandat est différent, la valeur de AmendmentIndicator doit etre true : " + record.getAmendmentIndicator(), record.getAmendmentIndicator());
	}
}
