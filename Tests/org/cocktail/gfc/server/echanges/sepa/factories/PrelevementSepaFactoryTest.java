package org.cocktail.gfc.server.echanges.sepa.factories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.gfc.server.echanges.sepa.model.AmendmentInformationDetails;
import org.cocktail.gfc.server.echanges.sepa.model.DirectDebitTransactionInformation;
import org.cocktail.gfc.server.echanges.sepa.model.Element;
import org.cocktail.gfc.server.echanges.sepa.model.GroupHeader;
import org.cocktail.gfc.server.echanges.sepa.model.Identification;
import org.cocktail.gfc.server.echanges.sepa.model.Other;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentInformationSDD;
import org.cocktail.gfc.server.echanges.sepa.model.PrivateIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.Proprietary;
import org.cocktail.gfc.server.echanges.sepa.model.SchemeName;
import org.junit.Test;

import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class PrelevementSepaFactoryTest {
	private static final String NS = "xmlns=urn:iso:std:iso:20022:tech:xsd:pain.008.001.02";
	private static final String NATURE_REMISE_SDD = "SDD";
	private static String numeroDeFichierDansLaJournee3Car = "001";
	private static Date dateDuJour  = new Date();
	private static NSTimestamp dateTimeCreation = new NSTimestamp(dateDuJour.getTime());
	private ParametresSepa paramSepa = PrelevementSepaTestHelpers.getParamSepa();
	private PrelevementSepaRecord record = PrelevementSepaTestHelpers.getPrelevementSepaRecord();
	private NSMutableArray<PrelevementSepaRecord> lesPrelevementsRecords = new NSMutableArray<PrelevementSepaRecord>();
	private static final String XSD_SDD_NAME = "Resources/xsd/pain.008.001.02.xsd";
	private PrelevementSepaFactory factory = new PrelevementSepaFactory();
	
	private static String valideElement(Element element) {
		String res = null;
		try {
			element.validate();
		} catch (Exception e) {
			res = e.getMessage();
		}
		return res;
		
	}
	

	
	@Test
	public void testGenMessageIdentification() {
		String pattern = "[A-Za-z0-9-]{8,8}-DFT-SDD-[0-9]{5,5}-[a-zA-Z0-9]{3,3}";
		String annee2Chars = (new SimpleDateFormat("yy")).format(dateTimeCreation);
		String numeroDuJourDansLAnnee3Car = (new SimpleDateFormat("DDD")).format(dateTimeCreation.getTime());
		String msgId = ASepaFactory.genMessageIdentification(paramSepa.getEmetteurTransfertId(), NATURE_REMISE_SDD, annee2Chars, numeroDuJourDansLAnnee3Car, numeroDeFichierDansLaJournee3Car);
		assertTrue("La longueur du message d'identification ne doit pas dépasser 35 caractères" + " : " + msgId + " : " + msgId.length(), msgId.length() <= 35);
		assertTrue("La structure de ce message se présente sous la forme XXXXXXXX-DFT-SDD-AAQQQ-XXX", msgId.matches(pattern));
		
	}

	@Test
	public void testConstruitGroupHeaderParParametres() {
		GroupHeader groupHeader = PrelevementSepaFactory.construitGroupHeaderParParametres(paramSepa.getCompteDftTitulaire(), "TGDFT081-DFT-SDD-13184-001", dateTimeCreation, new BigDecimal("0"), 1, NS);
		String res = valideElement(groupHeader);
		assertNull("Une exception à la validation de l'objet GroupHeader : " + res, res);

	}
	
	@Test
	public void testConstruitPrivateIdentification() {
		PrivateIdentification privateIdentification = new PrivateIdentification(new Other(new Identification(paramSepa.getIcs(), "2.27", NS), 
				new SchemeName(new Proprietary("SEPA", "2.27", NS), "2.27", NS), "2.27", NS), "2.27", NS);
		String res = valideElement(privateIdentification);
		assertNull("Une exception à la validation de l'objet PrivateIdentification : " + res, res);
		
		PrivateIdentification privateIdentificationBadICS = new PrivateIdentification(new Other(new Identification("FR53ZZZ947542FR53ZZZ947542FR53ZZZ947542", "2.27", NS), 
				new SchemeName(new Proprietary("SEPA", "2.27", NS), "2.27", NS), "2.27", NS), "2.27", NS);
		res = valideElement(privateIdentificationBadICS);
		assertNotNull("Une exception doit être levée pour une identification trop longue : " + res, res);

	}
	
	@Test
	public void testConstruitPrivateIdentificationBadICS() {
		PrivateIdentification privateIdentificationBadICS = new PrivateIdentification(new Other(new Identification("FR53ZZZ947542FR53ZZZ947542FR53ZZZ947542", "2.27", NS), 
				new SchemeName(new Proprietary("SEPA", "2.27", NS), "2.27", NS), "2.27", NS), "2.27", NS);
		String res = valideElement(privateIdentificationBadICS);
		assertNotNull("Une exception doit être levée pour une identification trop longue : " + res, res);

	}
	
	@Test
	public void testConstruitPaymentInformationSDDParParametres() {
		lesPrelevementsRecords.add(record);
		PaymentInformationSDD paymentInformation = factory.construitPaymentInformationSDDParParametres(paramSepa, lesPrelevementsRecords, dateTimeCreation, "identifiantdufichier01", "FRST");
		String res = valideElement(paymentInformation);
		assertNull("Une exception à la validation de l'objet PaymentInformationSDD : " + res, res);
	}
	
	@Test
	public void testConstruitPaymentInformationSDDParParametresSeqTypeNonConforme() {
		lesPrelevementsRecords.add(record);
		PaymentInformationSDD paymentInformationSeqTypeNonConforme = factory.construitPaymentInformationSDDParParametres(paramSepa, lesPrelevementsRecords, dateTimeCreation, "identifiantdufichier01", "FIRST");
		String res = valideElement(paymentInformationSeqTypeNonConforme);
		assertNotNull("Valeur FIRST non conforme pour SequenceType: " + res, res);

	}
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementRecord() {
		DirectDebitTransactionInformation directDebitTransactionInformation = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String res = valideElement(directDebitTransactionInformation);
		assertNull("Une exception à la validation de l'objet DirectDebitTransactionInformation : " + res, res);
		assertEquals("false", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNull("AmendmentIndicator=false, AmendmentInformationDetails doit être nul", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
	}
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementRecordBICChange() {
		record.setPreviousDebtorBIC("CRLYFRPP");
		DirectDebitTransactionInformation directDebitTransactionInformationBICChange = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String res = valideElement(directDebitTransactionInformationBICChange);
		assertNull(res, res);
		assertEquals("true", directDebitTransactionInformationBICChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNotNull("AmendmentIndicator=true, AmendmentInformationDetails ne doit pas être nul", directDebitTransactionInformationBICChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		assertEquals("SMNDA", directDebitTransactionInformationBICChange.getDirectDebitTransaction()
				.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalDebtorAgent().getFinancialInstitutionIdentification().getOther().getIdentification().getTextTrim());
	}
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementRecordICSChange() {
		record.setPreviousICS("FR53ZZZ947522");
		DirectDebitTransactionInformation directDebitTransactionInformation = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String res = valideElement(directDebitTransactionInformation);
		assertNull(res, res);
		assertEquals("true", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNotNull("AmendmentIndicator=true, AmendmentInformationDetails ne doit pas être nul", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		assertNotNull(directDebitTransactionInformation.getDirectDebitTransaction()
				.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalCreditorSchemeIdentification());
	}
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementRecordCreditorNameChange() {
		DirectDebitTransactionInformation directDebitTransactionInformation = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String res = valideElement(directDebitTransactionInformation);
		assertNull(res, res);
		assertEquals("false", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNull("AmendmentIndicator=false, AmendmentInformationDetails doit être nul", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		
		record.setPreviousCreditorName("DDGFIP DU TARN");
		DirectDebitTransactionInformation directDebitTransactionInformationAfterChange = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String resAfterChange = valideElement(directDebitTransactionInformationAfterChange);
		assertNull(resAfterChange, resAfterChange);
		assertEquals("true", directDebitTransactionInformationAfterChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNotNull("AmendmentIndicator=true, AmendmentInformationDetails ne doit pas être nul", directDebitTransactionInformationAfterChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		assertNotNull(directDebitTransactionInformationAfterChange.getDirectDebitTransaction()
				.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalCreditorSchemeIdentification());
	}
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementMandatIDChange() {
		DirectDebitTransactionInformation directDebitTransactionInformation = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String res = valideElement(directDebitTransactionInformation);
		assertNull(res, res);
		assertEquals("false", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNull("AmendmentIndicator=false, AmendmentInformationDetails doit être nul", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		
		record.setPreviousMandatID("400480 CUFR ALBI 78177465");
		DirectDebitTransactionInformation directDebitTransactionInformationAfterChange = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String resAfterChange = valideElement(directDebitTransactionInformationAfterChange);
		assertNull(resAfterChange, resAfterChange);
		assertEquals("true", directDebitTransactionInformationAfterChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNotNull("AmendmentIndicator=true, AmendmentInformationDetails ne doit pas être nul", directDebitTransactionInformationAfterChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		assertNotNull(directDebitTransactionInformationAfterChange.getDirectDebitTransaction()
				.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalMandateIdentification());
	}
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementIBANChange() {
		record.setPreviousDebtorIBAN("FR7618206002105487266700217");
		DirectDebitTransactionInformation directDebitTransactionInformation = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
//		assertEquals(record.getDateOfSignature(),directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getDateOfSignature().getText());
		String res = valideElement(directDebitTransactionInformation);
		assertNull(res, res);
		assertEquals("true", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNotNull("AmendmentIndicator=true, AmendmentInformationDetails ne doit pas être nul", directDebitTransactionInformation.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		assertNotNull(directDebitTransactionInformation.getDirectDebitTransaction()
				.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalDebtorAccount());
	}
	
	
	@Test
	public void testConstruitDirectDebitTransactionInformationParPrelevementRecordBICAndIBANChange() {
		record.setPreviousDebtorIBAN("FR7618206002105487266700217");
		record.setPreviousDebtorBIC("CRLYFRPP");
		DirectDebitTransactionInformation directDebitTransactionInformationBICAndIBANChange = factory.construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
		String res = valideElement(directDebitTransactionInformationBICAndIBANChange);
		assertNull(res, res);
		assertEquals("true", directDebitTransactionInformationBICAndIBANChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator().getTextTrim());
		assertNotNull("AmendmentIndicator=true, AmendmentInformationDetails ne doit pas être nul", directDebitTransactionInformationBICAndIBANChange.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentInformationDetails());
		assertEquals("SMNDA", directDebitTransactionInformationBICAndIBANChange.getDirectDebitTransaction()
				.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalDebtorAgent().getFinancialInstitutionIdentification().getOther().getIdentification().getTextTrim());
	}
	
	
	@Test
	public void testConstruitAmendmentInformationDetailsParPrelevementRecord() {
		AmendmentInformationDetails amendmentInformationDetails = PrelevementSepaFactory.construitAmendmentInformationDetailsParPrelevementRecord(paramSepa, record);
		String res = valideElement(amendmentInformationDetails);
		assertNull("Une exception a la validation de l'objet DirectDebitTransactionInformation : " + res, res);
	}
	
	@Test
	public void testGenereMessage() {
		
		PrelevementSepaFactory prelevementSepaFactory = new PrelevementSepaFactory();
		NSData xmlData = null;
		String res = null;
		//record.setPreviousDebtorBIC("CRLYFRPP");
		lesPrelevementsRecords.add(record);
		try {
			xmlData = prelevementSepaFactory.genereMessage(dateTimeCreation, paramSepa, lesPrelevementsRecords, new BigDecimal("0"), 1, dateTimeCreation, numeroDeFichierDansLaJournee3Car, "identifiantdufichier01", XSD_SDD_NAME, "FRST");
		} catch (Exception e) {
			res = e.getMessage();
		}
		assertNotNull("Génération du fichier xml et validation xsd : " + res + " xml :" + xmlData.toString(), xmlData);

	}
	
}
