package org.cocktail.gfc.server.echanges.sepa.factories;

import java.math.BigDecimal;
import java.util.Date;

public class PrelevementSepaTestHelpers {

	public static ParametresSepa getParamSepa() {
		ParametresSepa paramSepa = new ParametresSepa();
		paramSepa.setCompteDftTitulaire("Université de Neptune");
		paramSepa.setCompteDftIban("FR7030002005500000157845Z02");
		paramSepa.setTgIban("FR7030002005500000157845Z02");
		paramSepa.setTgBic("BDFEFRPPCCT");
		paramSepa.setTgNom("Ma TG preferee");
		paramSepa.setEmetteurTransfertId("TGDFT081");
		paramSepa.setDevise("EUR");
		paramSepa.setTgCodique("0810000");
		paramSepa.setIcs("FR53ZZZ947541");
		return paramSepa;
	}

	public static PrelevementSepaRecord getPrelevementSepaRecord() {
		PrelevementSepaRecord prelevementSepaRecord = new PrelevementSepaRecord();
		prelevementSepaRecord.setPaymentID("12345678910");
		prelevementSepaRecord.setInstructedAmount(new BigDecimal(1540.50));
		prelevementSepaRecord.setDebtorBIC("BNPAFRPP");
		prelevementSepaRecord.setDebtorIBAN("FR7030002005500000157845Z02");
		prelevementSepaRecord.setDebtorName("THEROUX Théophile");
		prelevementSepaRecord.setCurrency(getParamSepa().getDevise());
		prelevementSepaRecord.setMandatID("400258 CROUS RENNES 78177465");
		prelevementSepaRecord.setPreviousMandatID("400258 CROUS RENNES 78177465");
		prelevementSepaRecord.setDateOfSignature("2012-07-11");
		prelevementSepaRecord.setPreviousDebtorBIC("BNPAFRPP");
		prelevementSepaRecord.setPreviousDebtorIBAN("FR7030002005500000157845Z02");
		prelevementSepaRecord.setPreviousICS("FR53ZZZ947541");
		prelevementSepaRecord.setPreviousCreditorName("Ma TG preferee");
		prelevementSepaRecord.setPurposeCode("ANNI");
		prelevementSepaRecord.setRemittanceInformation("CANTINE MARS 2012");
		prelevementSepaRecord.setMigrationNNE(false);
		return prelevementSepaRecord;
	}

}
