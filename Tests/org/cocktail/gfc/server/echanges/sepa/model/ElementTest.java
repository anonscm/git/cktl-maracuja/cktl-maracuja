package org.cocktail.gfc.server.echanges.sepa.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class ElementTest {

	@Test
	public void checkMontantDifferentDeZero() {
		String NS = "xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03";
		InstructedAmount amount = new InstructedAmount("EUR", new BigDecimal("0"), "2.43", NS);
		String res = null;
		try {
			amount.validate();
		} catch (Exception e) {
			res = e.getMessage();
		}
		assertNotNull("Une exception devrait apparaitre pour un montant egal à 0", res);
		assertTrue("Une exception indiquant que le montant ne peut être egal a 0 devrait etre declenchee", res.contains(SepaExceptionMsg.MONTANT_EGAL_0));

	}

}
