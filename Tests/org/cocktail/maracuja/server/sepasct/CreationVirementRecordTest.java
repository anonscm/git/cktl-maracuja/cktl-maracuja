package org.cocktail.maracuja.server.sepasct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.maracuja.server.metier.EODepense;
import org.cocktail.maracuja.server.metier.EOExercice;
import org.cocktail.maracuja.server.metier.EOFournisseur;
import org.cocktail.maracuja.server.metier.EOMandat;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;
import org.cocktail.maracuja.server.sepasct.CreationVirementRecordParNoFacture;
import org.cocktail.maracuja.server.sepasct.CreationVirementRecordParRib;
import org.cocktail.maracuja.server.sepasct.CreationVirementRecordSansGroupement;
import org.cocktail.maracuja.server.sepasct.SepaSCTFactory;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class CreationVirementRecordTest {

	@Test
	public void unElementPourUnRib() {
		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;

		String bic = "XXXX";
		String iban = "123456789";
		String titco = "Jean Paul";
		String nom = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom);
		EORib rib = VirementSepaTestHelpers.getRib(bic, iban, titco, fournisseur);
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		CreationVirementRecordParRib creationVirementRecordParRib = new CreationVirementRecordParRib();

		NSMutableArray elementsAPayer = new NSMutableArray();
		EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(100));
		elementsAPayer.add(dep);

		NSArray<VirementSepaRecord> virementRecords;
		try {
			virementRecords = creationVirementRecordParRib.createRecords(sepaFactory, 1, rib, elementsAPayer);

			assertEquals(1, virementRecords.count());

			VirementSepaRecord virementRecord = virementRecords.objectAtIndex(0);

			assertNotNull(virementRecord);

			assertEquals(bic, virementRecord.getCreditorBIC());
			assertEquals(iban.toUpperCase(), virementRecord.getCreditorIBAN());
			assertEquals(titco.toUpperCase(), virementRecord.getCreditorName());
			assertEquals(BigDecimal.valueOf(100), virementRecord.getAmount());
			//assertEquals("100.00", virementRecord.getAmount().toString());

			assertEquals("EUR", virementRecord.getCurrency());
			assertEquals("Université d20120100000001", virementRecord.getPaymentID());
			assertNull(virementRecord.getPurposeCode());
			assertEquals("Vir Sct Universite de Neptun / multiple / Paiement 2012-", virementRecord.getRemittanceInformation());
			assertEquals("JUPITER                                                               ", virementRecord.getUltimateCreditorName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deuxElementsPourUnRib() {
		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;
		String bic = "XXXX";
		String iban = "123456789";
		String titco = "Jean Paul";
		String nom = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom);
		EORib rib = VirementSepaTestHelpers.getRib(bic, iban, titco, fournisseur);
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		CreationVirementRecordParRib creationVirementRecordParRib = new CreationVirementRecordParRib();

		NSMutableArray elementsAPayer = new NSMutableArray();

		elementsAPayer.add(VirementSepaTestHelpers.getDepense(BigDecimal.valueOf(100)));
		elementsAPayer.add(VirementSepaTestHelpers.getOrdreDePaiement(BigDecimal.valueOf(200)));

		try {
			NSArray<VirementSepaRecord> virementRecords = creationVirementRecordParRib.createRecords(sepaFactory, 1, rib, elementsAPayer);

			assertEquals(1, virementRecords.count());

			VirementSepaRecord virementRecord = virementRecords.objectAtIndex(0);

			assertEquals(bic, virementRecord.getCreditorBIC());
			assertEquals(iban.toUpperCase(), virementRecord.getCreditorIBAN());
			assertEquals(titco.toUpperCase(), virementRecord.getCreditorName());
			assertEquals(BigDecimal.valueOf(300), virementRecord.getAmount());
			assertEquals("EUR", virementRecord.getCurrency());
			assertEquals("Université d20120100000001", virementRecord.getPaymentID());
			assertNull(virementRecord.getPurposeCode());
			assertEquals("Vir Sct Universite de Neptun / multiple / Paiement 2012-", virementRecord.getRemittanceInformation());
			assertEquals("JUPITER                                                               ", virementRecord.getUltimateCreditorName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deuxElementsPourUnRibAvecUnMontanAZero() {
		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;
		String bic = "XXXX";
		String iban = "123456789";
		String titco = "Jean Paul";
		String nom = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom);
		EORib rib = VirementSepaTestHelpers.getRib(bic, iban, titco, fournisseur);
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		CreationVirementRecordParRib creationVirementRecordParRib = new CreationVirementRecordParRib();

		NSMutableArray elementsAPayer = new NSMutableArray();
		EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(0));
		elementsAPayer.add(dep);

		EOOrdreDePaiement ordreDePaiement = mock(EOOrdreDePaiement.class);
		when(ordreDePaiement.montantAPayer()).thenReturn(BigDecimal.valueOf(200));
		elementsAPayer.add(ordreDePaiement);
		try {
			NSArray<VirementSepaRecord> virementRecords = creationVirementRecordParRib.createRecords(sepaFactory, 1, rib, elementsAPayer);

			assertEquals(1, virementRecords.count());

			VirementSepaRecord virementRecord = virementRecords.objectAtIndex(0);

			assertEquals(bic, virementRecord.getCreditorBIC());
			assertEquals(iban.toUpperCase(), virementRecord.getCreditorIBAN());
			assertEquals(titco.toUpperCase(), virementRecord.getCreditorName());
			assertEquals(BigDecimal.valueOf(200), virementRecord.getAmount());
			assertEquals("EUR", virementRecord.getCurrency());
			assertEquals("Université d20120100000001", virementRecord.getPaymentID());
			assertNull(virementRecord.getPurposeCode());
			assertEquals("Vir Sct Universite de Neptun / multiple / Paiement 2012-", virementRecord.getRemittanceInformation());
			assertEquals("JUPITER                                                               ", virementRecord.getUltimateCreditorName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void deuxElementsPourDeuxRibs() {
		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		String bic1 = "XXXX";
		String iban1 = "123456789";
		String titco1 = "Jean Paul";
		String nom1 = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom1);
		EORib rib1 = VirementSepaTestHelpers.getRib(bic1, iban1, titco1, fournisseur);

		String bic2 = "XXXX";
		String iban2 = "123456789";
		String titco2 = "Jean Paul";
		String nom2 = "Jupiter";
		EORib rib2 = VirementSepaTestHelpers.getRib(bic2, iban2, titco2, fournisseur);

		EOExercice exercice = VirementSepaTestHelpers.getExercice(2012);
		EOMandat mandat = VirementSepaTestHelpers.getMandat(2, exercice);

		CreationVirementRecordSansGroupement creationVirementRecordSansGroupement = new CreationVirementRecordSansGroupement();

		NSMutableArray elementsAPayer = new NSMutableArray();
		EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(100));
		when(dep.rib()).thenReturn(rib1);
		when(dep.mandat()).thenReturn(mandat);
		elementsAPayer.add(dep);

		EOOrdreDePaiement ordreDePaiement = mock(EOOrdreDePaiement.class);
		when(ordreDePaiement.montantAPayer()).thenReturn(BigDecimal.valueOf(200));
		when(ordreDePaiement.rib()).thenReturn(rib2);
		when(ordreDePaiement.exercice()).thenReturn(exercice);
		when(ordreDePaiement.odpDateSaisie()).thenReturn(new NSTimestamp());
		elementsAPayer.add(ordreDePaiement);

		NSMutableArray<VirementSepaRecord> virementRecords = new NSMutableArray<VirementSepaRecord>();
		try {
			virementRecords.addObjectsFromArray(creationVirementRecordSansGroupement.createRecords(sepaFactory, 1, rib1, elementsAPayer));

			assertEquals(2, virementRecords.count());

			VirementSepaRecord virementRecord = virementRecords.objectAtIndex(0);
			assertEquals(bic1, virementRecord.getCreditorBIC());
			assertEquals(iban1.toUpperCase(), virementRecord.getCreditorIBAN());
			assertEquals(titco1.toUpperCase(), virementRecord.getCreditorName());
			assertEquals(BigDecimal.valueOf(100), virementRecord.getAmount());
			assertEquals("EUR", virementRecord.getCurrency());
			assertNull(virementRecord.getPurposeCode());

			virementRecord = virementRecords.objectAtIndex(1);
			assertEquals(bic2, virementRecord.getCreditorBIC());
			assertEquals(iban2.toUpperCase(), virementRecord.getCreditorIBAN());
			assertEquals(titco2.toUpperCase(), virementRecord.getCreditorName());
			assertEquals(BigDecimal.valueOf(200), virementRecord.getAmount());
			assertEquals("EUR", virementRecord.getCurrency());
			assertNull(virementRecord.getPurposeCode());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void groupementParNoFacture() {

		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		String bic1 = "XXXX";
		String iban1 = "123456789";
		String titco1 = "Jean Paul";
		String nom1 = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom1);
		EORib rib1 = VirementSepaTestHelpers.getRib(bic1, iban1, titco1, fournisseur);

		EOExercice exercice = VirementSepaTestHelpers.getExercice(2012);
		EOMandat mandat = VirementSepaTestHelpers.getMandat(2, exercice);

		NSMutableArray elementsAPayer = new NSMutableArray();
		final EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(100));
		when(dep.rib()).thenReturn(rib1);
		when(dep.mandat()).thenReturn(mandat);
		when(dep.virementReferenceFournisseurNumero()).thenReturn("facture numero 1");
		when(dep.virementReferenceFournisseur()).thenReturn("RF 1");
		when(dep.virementReferenceInterne()).thenReturn("RI 1");
		//when(dep.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 1");
		elementsAPayer.add(dep);

		final EODepense dep2 = mock(EODepense.class);
		when(dep2.montantAPayer()).thenReturn(BigDecimal.valueOf(250));
		when(dep2.rib()).thenReturn(rib1);
		when(dep2.mandat()).thenReturn(mandat);
		when(dep2.virementReferenceFournisseurNumero()).thenReturn("facture numero 2");
		when(dep2.virementReferenceFournisseur()).thenReturn("RF 2");
		when(dep2.virementReferenceInterne()).thenReturn("RI 2");
		//when(dep2.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 2");
		elementsAPayer.add(dep2);

		final EODepense dep3 = mock(EODepense.class);
		when(dep3.montantAPayer()).thenReturn(BigDecimal.valueOf(300));
		when(dep3.rib()).thenReturn(rib1);
		when(dep3.mandat()).thenReturn(mandat);
		when(dep3.virementReferenceFournisseurNumero()).thenReturn("facture numero 3");
		when(dep3.virementReferenceFournisseur()).thenReturn("RF 3");
		when(dep3.virementReferenceInterne()).thenReturn("RI 3");
		//when(dep3.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 3");
		elementsAPayer.add(dep3);

		final EODepense dep4 = mock(EODepense.class);
		when(dep4.montantAPayer()).thenReturn(BigDecimal.valueOf(200));
		when(dep4.rib()).thenReturn(rib1);
		when(dep4.mandat()).thenReturn(mandat);
		when(dep4.virementReferenceFournisseurNumero()).thenReturn("facture numero 2");
		when(dep4.virementReferenceFournisseur()).thenReturn("RF 2");
		when(dep4.virementReferenceInterne()).thenReturn("RI 2");
		//when(dep4.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 2");
		elementsAPayer.add(dep4);

		final EOOrdreDePaiement ordreDePaiement = mock(EOOrdreDePaiement.class);
		when(ordreDePaiement.montantAPayer()).thenReturn(BigDecimal.valueOf(200));
		when(ordreDePaiement.rib()).thenReturn(rib1);
		when(ordreDePaiement.exercice()).thenReturn(exercice);
		when(ordreDePaiement.odpDateSaisie()).thenReturn(new NSTimestamp());
		when(ordreDePaiement.virementReferenceFournisseurNumero()).thenReturn("odp 1");
		when(ordreDePaiement.virementReferenceFournisseur()).thenReturn("RFO 1");
		when(ordreDePaiement.virementReferenceInterne()).thenReturn("RIO 1");
		//when(ordreDePaiement.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("odp 1");
		elementsAPayer.add(ordreDePaiement);

		//on sous-classe pour éviter de tester filtreSupportVirementsByReferenceFournisseurNumero qui fait appel à EOQualifier.filteredArrayWithQualifier qui ne fonctionne pas avec des mocks
		// (technique = stub testing)
		CreationVirementRecordParNoFacture creationVirementRecordSansGroupement = new CreationVirementRecordParNoFacture() {
			@Override
			protected NSArray<ISupportVirement> filtreSupportVirementsByReferenceFournisseurNumero(String referenceNumero, NSArray<ISupportVirement> elementsAPayer) {
				if ("facture numero 1".equals(referenceNumero)) {
					return new NSArray(new Object[] {
							dep
					});
				}
				else if ("facture numero 2".equals(referenceNumero)) {
					return new NSArray(new Object[] {
							dep2, dep4
					});
				}
				else if ("facture numero 3".equals(referenceNumero)) {
					return new NSArray(new Object[] {
							dep3
					});
				}
				else if ("odp 1".equals(referenceNumero)) {
					return new NSArray(new Object[] {
							ordreDePaiement
					});
				}
				else
					fail("Screnario non prévu");

				return super.filtreSupportVirementsByReferenceFournisseurNumero(referenceNumero, elementsAPayer);
			}

		};

		//		assertEquals("", creationVirementRecordSansGroupement.getLibelleTransaction(sepaFactory, "facture numero 1", creationVirementRecordSansGroupement.filtreSupportVirementsByReferenceFournisseurNumero("facture numero 1", elementsAPayer)));

		NSMutableArray<VirementSepaRecord> virementRecords = new NSMutableArray<VirementSepaRecord>();
		try {
			virementRecords.addObjectsFromArray(creationVirementRecordSansGroupement.createRecords(sepaFactory, 1, rib1, elementsAPayer));

			assertEquals(4, virementRecords.count());

			//Test limités : impossible de tester simplement la methode creationVirementRecordSansGroupement.filtreSupportVirementsByReferenceFournisseurNumero("facture numero 1", elementsAPayer)
			//car la methode utilise un EOQualifier.filteredArrayWithQualifier
			//

			VirementSepaRecord record0 = virementRecords.objectAtIndex(0);
			assertEquals(BigDecimal.valueOf(100), record0.getAmount());
			assertEquals(bic1, record0.getCreditorBIC());
			assertEquals(iban1, record0.getCreditorIBAN());
			//		assertEquals("test", record0.getCreditorName());
			//		assertEquals("test", record0.getCurrency());
			//		assertEquals("test", record0.getPaymentID());
			//		assertEquals("test", record0.getPurposeCode());
			assertEquals("Vir Sct Universite de NeptunRF 1 RI 1 / Paiement 2012-1000", record0.getRemittanceInformation());
			//		assertEquals("test", record0.getUltimateCreditorName());

			VirementSepaRecord record1 = virementRecords.objectAtIndex(1);
			assertEquals(BigDecimal.valueOf(450), record1.getAmount());
			assertEquals(bic1, record1.getCreditorBIC());
			assertEquals("Vir Sct Universite de Neptun / facture numero 2 / Multiple / Paiement 2012-1000", record1.getRemittanceInformation());

			VirementSepaRecord record2 = virementRecords.objectAtIndex(2);
			assertEquals(BigDecimal.valueOf(300), record2.getAmount());
			assertEquals(bic1, record2.getCreditorBIC());
			assertEquals("Vir Sct Universite de NeptunRF 3 RI 3 / Paiement 2012-1000", record2.getRemittanceInformation());

			VirementSepaRecord record3 = virementRecords.objectAtIndex(3);
			assertEquals(BigDecimal.valueOf(200), record3.getAmount());
			assertEquals(bic1, record3.getCreditorBIC());
			assertEquals("Vir Sct Universite de NeptunRFO 1 RIO 1 / Paiement 2012-1000", record3.getRemittanceInformation());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void groupementParNoFactureAvecRibsDifferents() throws Exception {

		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		String bic1 = "XXXX";
		String iban1 = "123456789";
		String titco1 = "Jean Paul";
		String nom1 = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom1);
		EORib rib1 = VirementSepaTestHelpers.getRib(bic1, iban1, titco1, fournisseur);

		String bic2 = "YYYY";
		String iban2 = "10101010";
		String titco2 = "Robert";
		String nom2 = "Saturne";
		//EOFournisseur fournisseur = getFournisseur(nom1);
		EORib rib2 = VirementSepaTestHelpers.getRib(bic2, iban2, titco2, fournisseur);

		EOExercice exercice = VirementSepaTestHelpers.getExercice(2012);
		EOMandat mandat = VirementSepaTestHelpers.getMandat(2, exercice);

		NSMutableArray elementsAPayer = new NSMutableArray();
		final EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(100));
		when(dep.rib()).thenReturn(rib1);
		when(dep.mandat()).thenReturn(mandat);
		when(dep.virementReferenceFournisseurNumero()).thenReturn("facture numero 1");
		when(dep.virementReferenceFournisseur()).thenReturn("RF 1");
		when(dep.virementReferenceInterne()).thenReturn("RI 1");
		//when(dep.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 1");
		elementsAPayer.add(dep);

		final EODepense dep2 = mock(EODepense.class);
		when(dep2.montantAPayer()).thenReturn(BigDecimal.valueOf(250));
		when(dep2.rib()).thenReturn(rib2);
		when(dep2.mandat()).thenReturn(mandat);
		when(dep2.virementReferenceFournisseurNumero()).thenReturn("facture numero 2");
		when(dep2.virementReferenceFournisseur()).thenReturn("RF 2");
		when(dep2.virementReferenceInterne()).thenReturn("RI 2");
		//when(dep2.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 2");
		elementsAPayer.add(dep2);

		//on sous-classe pour éviter de tester filtreSupportVirementsByReferenceFournisseurNumero qui fait appel à EOQualifier.filteredArrayWithQualifier qui ne fonctionne pas avec des mocks
		// (technique = stub testing)
		CreationVirementRecordParNoFacture creationVirementRecordSansGroupement = new CreationVirementRecordParNoFacture() {
			@Override
			protected NSArray<ISupportVirement> filtreSupportVirementsByReferenceFournisseurNumero(String referenceNumero, NSArray<ISupportVirement> elementsAPayer) {
				if ("facture numero 1".equals(referenceNumero)) {
					return new NSArray(new Object[] {
							dep
					});
				}
				else if ("facture numero 2".equals(referenceNumero)) {
					return new NSArray(new Object[] {
							dep2
					});
				}
				else
					fail("Screnario non prévu");

				return super.filtreSupportVirementsByReferenceFournisseurNumero(referenceNumero, elementsAPayer);
			}

		};

		//		assertEquals("", creationVirementRecordSansGroupement.getLibelleTransaction(sepaFactory, "facture numero 1", creationVirementRecordSansGroupement.filtreSupportVirementsByReferenceFournisseurNumero("facture numero 1", elementsAPayer)));

		NSMutableArray<VirementSepaRecord> virementRecords = new NSMutableArray<VirementSepaRecord>();
		virementRecords.addObjectsFromArray(creationVirementRecordSansGroupement.createRecords(sepaFactory, 1, rib1, elementsAPayer));

	}

	@Test
	public void getLibelleTransaction() {
		CreationVirementRecordParNoFacture creationVirementRecordParNoFacture = new CreationVirementRecordParNoFacture();
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		String bic1 = "XXXX";
		String iban1 = "123456789";
		String titco1 = "Jean Paul";
		String nom1 = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom1);
		EORib rib1 = VirementSepaTestHelpers.getRib(bic1, iban1, titco1, fournisseur);

		EOExercice exercice = VirementSepaTestHelpers.getExercice(2012);
		EOMandat mandat = VirementSepaTestHelpers.getMandat(2, exercice);

		final EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(100));
		when(dep.rib()).thenReturn(rib1);
		when(dep.mandat()).thenReturn(mandat);
		when(dep.virementReferenceFournisseurNumero()).thenReturn("facture numero 1");
		when(dep.virementReferenceFournisseur()).thenReturn("RF 1");
		when(dep.virementReferenceInterne()).thenReturn("RI 1");
		//when(dep.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 1");

		final EODepense dep1 = mock(EODepense.class);
		when(dep1.montantAPayer()).thenReturn(BigDecimal.valueOf(200));
		when(dep1.rib()).thenReturn(rib1);
		when(dep1.mandat()).thenReturn(mandat);
		when(dep1.virementReferenceFournisseurNumero()).thenReturn("facture numero 1");
		when(dep1.virementReferenceFournisseur()).thenReturn("RF 1");
		when(dep1.virementReferenceInterne()).thenReturn("RI 1");
		//when(dep.valueForKey(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY)).thenReturn("facture numero 1");

		assertEquals("Vir Sct Université de Neptun /  / Multiple / Paiement 2012-1000", creationVirementRecordParNoFacture.getLibelleTransaction(sepaFactory, "", new NSArray<ISupportVirement>()));
		assertEquals("Vir Sct Université de NeptunRF 1 RI 1 / Paiement 2012-1000", creationVirementRecordParNoFacture.getLibelleTransaction(sepaFactory, "RF 1", new NSArray(dep)));
		assertEquals("Vir Sct Université de Neptun / RF / Multiple / Paiement 2012-1000", creationVirementRecordParNoFacture.getLibelleTransaction(sepaFactory, "RF", new NSArray(new Object[] {
				dep, dep1
		})));
	}

}
