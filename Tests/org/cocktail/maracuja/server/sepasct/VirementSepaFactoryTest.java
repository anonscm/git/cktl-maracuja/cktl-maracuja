package org.cocktail.maracuja.server.sepasct;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaFactory;
import org.cocktail.gfc.server.echanges.sepa.model.Amount;
import org.cocktail.gfc.server.echanges.sepa.model.CreditTransferTransactionInformation;
import org.cocktail.gfc.server.echanges.sepa.model.EndToEndIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.InstructedAmount;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentIdentification;
import org.cocktail.maracuja.server.metier.EODepense;
import org.cocktail.maracuja.server.metier.EOFournisseur;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.sepasct.CreationVirementRecordParRib;
import org.cocktail.maracuja.server.sepasct.SepaSCTFactory;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class VirementSepaFactoryTest {

	@Test
	public void montantSansDecimales() {
		//import static org.junit.Assert.*;
		//import static org.mockito.Mockito.*;

		String bic = "XXXX";
		String iban = "123456789";
		String titco = "Jean Paul";
		String nom = "Jupiter";
		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseur(nom);
		EORib rib = VirementSepaTestHelpers.getRib(bic, iban, titco, fournisseur);
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		CreationVirementRecordParRib creationVirementRecordParRib = new CreationVirementRecordParRib();

		NSMutableArray elementsAPayer = new NSMutableArray();
		EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(BigDecimal.valueOf(100));
		elementsAPayer.add(dep);

		NSArray<VirementSepaRecord> virementRecords;
		try {
			virementRecords = creationVirementRecordParRib.createRecords(sepaFactory, 1, rib, elementsAPayer);
			VirementSepaRecord record = virementRecords.objectAtIndex(0);

			CreditTransferTransactionInformation creditTransferTransactionInformation = new CreditTransferTransactionInformation("2.27", VirementSepaFactory.NS);
			creditTransferTransactionInformation.setPaymentIdentification(new PaymentIdentification(new EndToEndIdentification("1D" + paramSepa.getTgCodique() + record.getPaymentID(), "2.30", VirementSepaFactory.NS), "2.28", VirementSepaFactory.NS));
			creditTransferTransactionInformation.setAmount(new Amount(new InstructedAmount(record.getCurrency(), record.getAmount(), "2.43", VirementSepaFactory.NS), "2.42", VirementSepaFactory.NS));
			assertEquals("100", creditTransferTransactionInformation.getAmount().getInstructedAmount().getText());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void suppressionElementsAvecMontantInferieurAZero() {

		EOFournisseur fournisseur = VirementSepaTestHelpers.getFournisseurExemple(null);
		//EORib rib = VirementSepaTestHelpers.getRibExemple(fournisseur);
		ParametresSepa paramSepa = VirementSepaTestHelpers.getParamSepa();
		SepaSCTFactory sepaFactory = VirementSepaTestHelpers.getSepaFactory(paramSepa);

		CreationVirementRecordParRib creationVirementRecordParRib = new CreationVirementRecordParRib();

		NSMutableArray elementsAPayer = new NSMutableArray();
		elementsAPayer.add(VirementSepaTestHelpers.getDepenseExemple(BigDecimal.valueOf(100)));
		elementsAPayer.add(VirementSepaTestHelpers.getDepenseExemple(BigDecimal.valueOf(200)));

		NSMutableArray res = sepaFactory.supprimeElementsAvecMontantInferieurOuEgalAZero(elementsAPayer);
		assertEquals(Integer.valueOf(2), Integer.valueOf(res.count()));

		elementsAPayer.add(VirementSepaTestHelpers.getDepenseExemple(BigDecimal.valueOf(0)));

		NSMutableArray res2 = sepaFactory.supprimeElementsAvecMontantInferieurOuEgalAZero(elementsAPayer);
		assertEquals(Integer.valueOf(2), Integer.valueOf(res2.count()));

		elementsAPayer.add(VirementSepaTestHelpers.getOrdreDePaiementExemple(BigDecimal.valueOf(0)));

		NSMutableArray res3 = sepaFactory.supprimeElementsAvecMontantInferieurOuEgalAZero(elementsAPayer);
		assertEquals(Integer.valueOf(2), Integer.valueOf(res3.count()));

		elementsAPayer.add(VirementSepaTestHelpers.getDepenseExemple(BigDecimal.valueOf(0.01)));

		NSMutableArray res4 = sepaFactory.supprimeElementsAvecMontantInferieurOuEgalAZero(elementsAPayer);
		assertEquals(Integer.valueOf(3), Integer.valueOf(res4.count()));

	}


}
