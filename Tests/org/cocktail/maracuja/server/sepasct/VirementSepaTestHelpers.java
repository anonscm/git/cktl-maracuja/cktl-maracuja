package org.cocktail.maracuja.server.sepasct;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.maracuja.server.metier.EODepense;
import org.cocktail.maracuja.server.metier.EOExercice;
import org.cocktail.maracuja.server.metier.EOFournisseur;
import org.cocktail.maracuja.server.metier.EOMandat;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.sepasct.SepaSCTFactory;

public class VirementSepaTestHelpers {

	public static SepaSCTFactory getSepaFactory(ParametresSepa paramSepa) {
		SepaSCTFactory sepaFactory = new SepaSCTFactory(paramSepa, "xsdLocation", "2012", "1000", "XX,FR");
		return sepaFactory;
	}

	public static ParametresSepa getParamSepa() {
		ParametresSepa paramSepa = new ParametresSepa();
		paramSepa.setCompteDftTitulaire("Université de Neptune");
		paramSepa.setDevise("EUR");
		return paramSepa;
	}

	public static EOMandat getMandat(Integer numero, EOExercice exercice) {
		EOMandat mandat = mock(EOMandat.class);
		when(mandat.manNumero()).thenReturn(numero);
		when(mandat.exercice()).thenReturn(exercice);

		return mandat;
	}

	public static EOExercice getExercice(Integer exeOrdre) {
		EOExercice exercice = mock(EOExercice.class);
		when(exercice.exeExercice()).thenReturn(exeOrdre);
		return exercice;
	}

	public static EORib getRib(String bic, String iban, String titco, EOFournisseur fournisseur) {
		EORib rib = mock(EORib.class);
		when(rib.ribBic()).thenReturn(bic);
		when(rib.ribIban()).thenReturn(iban);
		when(rib.ribTitco()).thenReturn(titco);
		when(rib.fournisseur()).thenReturn(fournisseur);
		assertEquals(bic, rib.ribBic());
		return rib;
	}

	public static EOFournisseur getFournisseur(String nom) {
		EOFournisseur fournisseur = mock(EOFournisseur.class);
		when(fournisseur.adrNom()).thenReturn(nom);
		return fournisseur;
	}

	public static EODepense getDepense(BigDecimal montant) {
		EODepense dep = mock(EODepense.class);
		when(dep.montantAPayer()).thenReturn(montant);
		return dep;
	}

	/**
	 * @param montant nullable
	 * @return
	 */
	public static EODepense getDepenseExemple(BigDecimal montant) {
		return getDepense((montant != null ? montant : BigDecimal.valueOf(100.5d)));
	}

	/**
	 * @param fournisseur nullable
	 * @return
	 */
	public static EORib getRibExemple(EOFournisseur fournisseur) {
		String bic = "XXXX";
		String iban = "123456789";
		String titco = "Jean Paul";
		String nom = "Jupiter";
		if (fournisseur == null) {
			fournisseur = getFournisseurExemple(nom);
		}
		return getRib(bic, iban, titco, fournisseur);
	}

	/**
	 * @param nom nullable
	 * @return
	 */
	public static EOFournisseur getFournisseurExemple(String nom) {
		if (nom == null) {
			nom = "Jupiter";
		}
		return getFournisseur(nom);
	}

	public static EOOrdreDePaiement getOrdreDePaiement(BigDecimal montant) {
		EOOrdreDePaiement ordreDePaiement = mock(EOOrdreDePaiement.class);
		when(ordreDePaiement.montantAPayer()).thenReturn(montant);
		return ordreDePaiement;
	}

	/**
	 * @param montant nullable
	 * @return
	 */
	public static EOOrdreDePaiement getOrdreDePaiementExemple(BigDecimal montant) {
		return getOrdreDePaiement((montant != null ? montant : BigDecimal.valueOf(100.5d)));
	}

}
