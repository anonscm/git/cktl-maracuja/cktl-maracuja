package org.cocktail.maracuja.server.sepasdd;

import static org.junit.Assert.fail;

import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class RecouvrementSepaSddServiceTest {

    /**
     * On teste la génération de l'identifiant des fichiers de virement. On se
     * laisse de la marge dans le numéro du recouvrement ( < 1 000 000 000)
     */
    @Test
    public void testGetIdentifiantFichierVirementValide() {
        NSTimestamp dateCreation = new NSTimestamp();
        RecouvrementSepaSddService rsss = new RecouvrementSepaSddService(null, null, null, null, dateCreation, null, null, null, null);
        try {
            rsss.getIdentifiantFichierVirement(1);
            rsss.getIdentifiantFichierVirement(9);
            rsss.getIdentifiantFichierVirement(99);
            rsss.getIdentifiantFichierVirement(999);
            rsss.getIdentifiantFichierVirement(9999);
            rsss.getIdentifiantFichierVirement(99999);
            rsss.getIdentifiantFichierVirement(999999);
            rsss.getIdentifiantFichierVirement(9999999);
            rsss.getIdentifiantFichierVirement(99999999);
            rsss.getIdentifiantFichierVirement(999999999);
        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * @throws Exception
     *             Si le numéro de recouvrement est <code>null</code>
     */
    @Test(expected = Exception.class)
    public void testGetIdentifiantFichierVirementNonValide() throws Exception {
        NSTimestamp dateCreation = new NSTimestamp();
        RecouvrementSepaSddService rsss = new RecouvrementSepaSddService(null, null, null, null, dateCreation, null, null, null, null);
        rsss.getIdentifiantFichierVirement(null);
    }

}
