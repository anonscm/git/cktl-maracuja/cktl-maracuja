/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 

package org.cocktail.maracuja.client.factory;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.cocktail.maracuja.client.metier.EOImTypeTaux;
import org.cocktail.zutil.client.ZDateUtil;
import org.junit.Test;

public class FactoryImTest {

	@Test
	public void testTauxCompensation() {
		Date date20130401 = ZDateUtil.stringToDate("01/04/2013");
		Date date20130501 = ZDateUtil.stringToDate("01/05/2013");
		Date date20130502 = ZDateUtil.stringToDate("02/05/2013");

		EOImTypeTaux typeTauxTil = mock(EOImTypeTaux.class);
		when(typeTauxTil.imttCode()).thenReturn(EOImTypeTaux.IMTT_CODE_TIL);

		FactoryIm factoryIm = new FactoryIm(false);

		assertEquals(FactoryIm.BIG_DECIMAL_2, factoryIm.determineTauxCompensation(typeTauxTil, date20130401));
		assertEquals(FactoryIm.BIG_DECIMAL_2, factoryIm.determineTauxCompensation(typeTauxTil, date20130501));

		EOImTypeTaux typeTauxTbce = mock(EOImTypeTaux.class);
		when(typeTauxTbce.imttCode()).thenReturn(EOImTypeTaux.IMTT_CODE_TBCE);
		assertEquals(FactoryIm.BIG_DECIMAL_7, factoryIm.determineTauxCompensation(typeTauxTbce, date20130401));
		assertEquals(FactoryIm.BIG_DECIMAL_8, factoryIm.determineTauxCompensation(typeTauxTbce, date20130501));
		assertEquals(FactoryIm.BIG_DECIMAL_8, factoryIm.determineTauxCompensation(typeTauxTbce, date20130502));

	}
}
