/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server;
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

import java.io.FileInputStream;
import java.util.Properties;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.foundation.NSLog;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class HelpCenter {
    private final String FILENAME="helpmap.properties";
    private Properties helpMap;
    private WOResourceManager rm;
    
    /**
     * 
     */
    public HelpCenter() throws Exception {
        super();
        try {
            rm = ((Application)Application.application()).resourceManager();
            
            if (rm==null) {
                throw new Exception("Impossible de récupérer le resourceManager!");
            }
            
            helpMap = new Properties();
            
            String path = ((Application)Application.application()).appResources().pathForResource(FILENAME);
            
//            FileInputStream stream = ; 
            
//            helpMap.load( rm.inputStreamForResourceNamed( FILENAME ,null,new NSArray()) );
            helpMap.load( new FileInputStream(path) );
            NSLog.out.appendln("HelpCenter initialise");
        }
        catch (Exception e) {
            throw new Exception("Erreur lors de l'initialisation du help center : " + e.getMessage() );
        }
    }

    
	/**
	 * @param context
	 * @param id
	 * @return l'url associée à l'identifiant de topic d'aide.
	 */
	public final String getUrlForHelp(WOContext context, String id) {
	    String res = null;
	    String root = ((Application)Application.application())  .config().stringForKey("ROOT_URL_FOR_HELP"); 
        if (( root!=null) && (root.length()>0) ) {
            res = root + helpMap.getProperty(id) ;
        }
        else {
            res = rm.urlForResourceNamed( helpMap.getProperty(id),null,context.session().languages(),context.request());
        }
        NSLog.out.appendln("URL help pour " + id + " = "+res);
        return res;
	}    
    
	
	
	
	
	
	
    
}
