/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.TimeZone;

import org.cocktail.fwkcktlcompta.server.remotes.RemoteDelegateCompta;
import org.cocktail.fwkcktlcompta.server.remotes.RemoteDelegateSepaSDD;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.server.invocation.CktlClientInvocation;
import org.cocktail.maracuja.server.ctrl.UserAuthenticationEngine;
import org.cocktail.maracuja.server.ctrl.UserAuthenticationEngineCas;
import org.cocktail.maracuja.server.exports.ExcelExport;
import org.cocktail.maracuja.server.factory.FactoryNumerotation;
import org.cocktail.maracuja.server.factory.FactoryPreference;
import org.cocktail.maracuja.server.factory.FactoryProcessApresTraitement;
import org.cocktail.maracuja.server.finders.ZFinder;
import org.cocktail.maracuja.server.metier.EOBordereau;
import org.cocktail.maracuja.server.metier.EOBordereauRejet;
import org.cocktail.maracuja.server.metier.EOEcriture;
import org.cocktail.maracuja.server.metier.EOEmargement;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EOPaiement;
import org.cocktail.maracuja.server.metier.EORecouvrement;
import org.cocktail.maracuja.server.metier.EOReimputation;
import org.cocktail.maracuja.server.metier.EORetenue;
import org.cocktail.maracuja.server.metier.EOUtilisateur;
import org.cocktail.maracuja.server.metier.EOUtilisateurPreference;
import org.cocktail.zutil.server.ZDateUtil;
import org.cocktail.zutil.server.logging.ZLogger;
import org.cocktail.zutil.server.zwebapp.ZSession;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.EODistributionContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;

import er.extensions.eof.ERXEC;

public class Session extends ZSession {
	private static final String SESSION_NAME_ID = "session";

	public Application myApplication;
	public EOEditingContext defaultEC;

	private String login;
	private final ConnectedUser connectedUser = new ConnectedUser();

	private Exception lastSqlStatement_Error;
	private Statement lastSqlStatement_statement;


	private NSData lastExcel;
	private Thread exportTaskThread;
	private Exception lastExportError;

	private final static String EOECRITURE_SID = EOEcriture.ENTITY_NAME;
	private final static String EOECRITUREBROUILLARD_SID = "EOEcritureBrouillard";
	private final static String EOBORDEREAUREJET_SID = EOBordereauRejet.ENTITY_NAME;
	private final static String EOEMARGEMENT_SID = EOEmargement.ENTITY_NAME;
	private final static String EOPAIEMENT_SID = EOPaiement.ENTITY_NAME;
	private final static String EOORDREPAIEMENT_SID = EOOrdreDePaiement.ENTITY_NAME;
	private final static String EOREIMPUTATION_SID = EOReimputation.ENTITY_NAME;
	private final static String EOBORDEREAU_SID = EOBordereau.ENTITY_NAME;
	private final static String EORECOUVREMENT_SID = EORecouvrement.ENTITY_NAME;
	private final static String EORETENUE_SID = EORetenue.ENTITY_NAME;

	public RemoteDelegateSepa remoteDelegateSepa;
	public RemoteDelegatePaiement remoteDelegatePaiement;
	public RemoteDelegateDatabase remoteDelegateDatabase;
	public CktlClientInvocation cktlClientInvocation;
	public RemoteDelegateCompta remoteDelegateCompta;
	//public RemoteDelegateReport remoteDelegateReport;
	public RemoteDelegateSepaSDD remoteDelegateSepaSDD;
	public RemoteDelegatePrint remoteDelegatePrint;

	public Session() {
		super();
		// Recuperation de l'application en cours
		myApplication = (Application) WOApplication.application();
		// Recup de l'EC par default
		defaultEC = this.defaultEditingContext();
		remoteDelegateSepa = new RemoteDelegateSepa(this);
		remoteDelegatePaiement = new RemoteDelegatePaiement(this);
		remoteDelegateDatabase = new RemoteDelegateDatabase(this);
		cktlClientInvocation = new CktlClientInvocation(this);
		remoteDelegateCompta = new RemoteDelegateCompta(this);
		remoteDelegateSepaSDD = new RemoteDelegateSepaSDD(this);
		//remoteDelegateReport = new RemoteDelegateReport(this);
		remoteDelegatePrint = new RemoteDelegatePrint(this);


		connectedUser.setDateConnection(ZDateUtil.now());
		connectedUser.setDateLastHeartBeat(ZDateUtil.now());
		connectedUser.setSessionID(sessionID());
		getMyApplication().getMySessions().put(sessionID(), this);
		afterSessionOpened();
	}

	public void afterSessionOpened() {

	}

	/**
	 * Met à jour les origines dans la base de données.
	 */
	private void majOrigines() {
		if (myApplication.majOriginesLastExec == null || ZDateUtil.calcMillisecondsBetween(myApplication.majOriginesLastExec, new Date()) > myApplication.getMajOrigineRefreshPeriod()) {
			ZLogger.info("Mise a jour des origines...");
			myApplication.majOriginesLastExec = new Date();
			Connection conn = null;
			final String sql = "begin\n maracuja.gestionorigine.maj_origine(); commit; \n end;";

			try {
				conn = myApplication.getNewJDBCConnection(false, false);
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			try {
				final SqlStatementTaskThread thread = new SqlStatementTaskThread(conn, sql, false, true);
				thread.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void autowash() {
		ZLogger.info("Autowash...");
		Connection conn = null;
		final String sql = "begin\n maracuja.autowash; commit; \n end;";

		try {
			conn = myApplication.getNewJDBCConnection(false, false);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		try {
			final SqlStatementTaskThread thread = new SqlStatementTaskThread(conn, sql, false, true);
			thread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Application getMyApplication() {
		return (Application) WOApplication.application();
	}

	private SAUTClient sautClient() {
		return ((Application) Application.application()).sautClient();
	}

	/**
	 * Retourne les informations de connexion de l'utilisateur a partir de sa signature (login, mot de passe).
	 */
	public NSDictionary clientSideRequestGetUserIdentity(String userLogin, String userPass, Boolean isCrypted, Boolean useCas) {
		NSDictionary userInfos;
		try {
			UserAuthenticationEngine userAuthenticationEngine = getMyApplication().getMyUserAuthenticationEngine();
			if (useCas.booleanValue()) {
				userAuthenticationEngine = new UserAuthenticationEngineCas(getMyApplication().dataBus());
				ZLogger.info("connection via CAS");
			}

			ZLogger.info("Tentative de connexion pour " + userLogin);
			Hashtable tmp = userAuthenticationEngine.authenticate(userLogin, userPass);
			Integer noIndividu = null;
			Integer persId = null;
			if (tmp.get("persId") != null) {
				persId = new Integer(tmp.get("persId").toString());
			}

			userAuthenticationEngine.authenticateForLocalApplication(getMyApplication().dataBus(), this.defaultEditingContext(), persId, noIndividu, tmp);
			ZLogger.info("authenticate effectue");

			//On crée un dictionnaire à partir de ce qu'on a recupéré
			NSMutableDictionary tmpUserInfos = new NSMutableDictionary();
			transfertValueFromHashtableToDico("persId", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("login", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("vLan", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("email", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("prenom", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("nom", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("noCompte", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("noIndividu", tmp, tmpUserInfos);
			userInfos = tmpUserInfos.immutableClone();
			ZLogger.info("Login reussi pour " + userLogin);

		} catch (Exception e) {
			e.printStackTrace();
			String s = e.getMessage();
			if (s == null) {
				s = "Probleme d'identification. Erreur non recuperee.";
			}
			ZLogger.info("Login refuse pour " + userLogin + "(" + s + ")");
			userInfos = new NSDictionary(s, "erreur");
		}
		return userInfos;
	}

	private void transfertValueFromHashtableToDico(String key, Hashtable dicoFrom, NSMutableDictionary dicoTo) {
		if (dicoFrom.keySet().contains(key)) {
			dicoTo.takeValueForKey(dicoFrom.get(key), key);
		}
	}

	/**
	 * Une methode necessaire pour autoriser l'appel de methodes distants par le client
	 */
	public boolean distributionContextShouldFollowKeyPath(EODistributionContext context, String path) {
		return path.startsWith(SESSION_NAME_ID);
		//return path.equals("session");
		//        if(path.equals("session"))
		//            return true;
		//        else
		//            return false;
	}

	//Méthodes appelées par les clients

	public NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) {
		if (eo == null) {
			return null;
		}
		EOEditingContext tmpEdc = ERXEC.newEditingContext();
		EOGlobalID eoglobalid = eo.editingContext().globalIDForObject(eo);
		NSDictionary myResult = EOUtilities.primaryKeyForObject(tmpEdc, tmpEdc.faultForGlobalID(eoglobalid, tmpEdc));
		return myResult;
	}

	public void clientSideRequestSetFwkCktlWebApp(String plogin) {
		login = plogin;
		System.out.println("login=" + login);
		//        getSessLog().setMyUserName(login);
		ZLogger.info("" + java.text.DateFormat.getTimeInstance().format(new java.util.Date()) + " >>> Nouvelle connexion de " + login + " (" + getClientPlateformeInfo() + ")");
		//si connexion ok, on met a jour les origines
		autowash();
		majOrigines();

	}

	public void clientSideRequestHelloImAlive(String login, String ipAdress) {
		connectedUser.setDateLastHeartBeat(ZDateUtil.now());
		connectedUser.setIp(ipAdress);
		getMyApplication().listConnectedUsers();
		getMyApplication().cleanSessionsNonIdentifiees();
		//        getMyApplication().cleanSessionsInactives();
	}

	public void clientSideRequestMsgToServer(String msg, String ipAdress) {
		ZLogger.debug("Message recu de (" + ipAdress + ") (login=" + connectedUser.getLogin() + ") (sessionID=" + sessionID() + ") : " + msg);
		connectedUser.setDateLastHeartBeat(ZDateUtil.now());
		connectedUser.setIp(ipAdress);
		//	System.out.println(defaultEditingContext().updatedObjects());
	}

	public void clientSideRequestSetClientPlateforme(String s) {
		setClientPlateformeInfo(s);
	}

	public String clientSideRequestVersion() {
		return VersionMe.appliVersion();
		//return ((Application) WOApplication.application()).versionTxt();
	}

	public String clientSideRequestVersionRaw() {
		return VersionMe.appliVersion();
		//return VersionCommon.rawVersion();
		//return VersionCommon.VERSIONNUMMAJ + "." + VersionCommon.VERSIONNUMMIN + "." + VersionCommon.VERSIONNUMBUILD + "." + VersionCommon.VERSIONNUMPATCH;
	}

	public String clientSideRequestCopyright() {
		return ((Application) WOApplication.application()).copyright();
	}

	public String clientSideRequestBdConnexionName() {
		return ((Application) WOApplication.application()).showBdConnexionInfo();
	}

	public NSTimeZone clientSideRequestDefaultNSTimeZone() {
		return NSTimeZone.defaultTimeZone();
	}

	public NSArray clientSideRequestGetConnectedUsers() {
		NSMutableArray t = new NSMutableArray();
		for (Iterator iter = getMyApplication().getMySessions().values().iterator(); iter.hasNext();) {
			Session element = (Session) iter.next();
			t.addObject(element.connectedUser.toNSDictionary());
		}
		return t;
	}

	public TimeZone clientSideRequestDefaultTimeZone() {
		return TimeZone.getDefault();
	}

	public String clientSideRequestGetEtablissementName() {
		return myApplication.getEtablissementName();
	}

	public String clientSideRequestGetConfigParam(String paramKey) {
		return myApplication.getAppParametresConfig().valueForKey(paramKey).toString();
	}

	public String clientSideRequestGetUrlForHelpId(String id) {
		return myApplication.getHelpCenter().getUrlForHelp(context(), id);
	}

	public String clientSideRequestGetWsResourceUrl(String id) throws Exception {
		WOResourceManager rm = ((Application) Application.application()).resourceManager();
		if (rm == null) {
			throw new Exception("Impossible de récupérer le resourceManager!");
		}
		return rm.urlForResourceNamed(id, null, null, context().request());
	}

	public String clientSideRequestGetGrhumParam(String paramKey) {
		return myApplication.config().stringForKey(paramKey);
	}

	public void clientSideRequestSessDeconnect(String s) {
		ZLogger.info("Deconnexion de l'utilisateur " + s);
	}

	/**
	 * Renvoie le mode d'authentification à utiliser.
	 */
	public String clientSideRequestRequestsAuthenticationMode() {
		return ((Application) Application.application()).config().stringForKey("LCL_USER_AUT_MODE");
	}

	/**
	 * Renvoie le nom de l'application pour utilisation par le client.
	 */
	public String clientSideRequestGetApplicationName() {
		return getMyApplication().getApplicationFinalName();
	}

	public String clientSideRequestGetApplicationServerLogName() {
		return getMyApplication().getServerLogFile().getName();
	}

	public String clientSideRequestGetApplicationServerLogPrefix() {
		return getMyApplication().getLogPrefix();
	}

	/**
	 * Renvoie l'alias de l'application pour utilisation par le client.
	 */
	public String clientSideRequestGetAppAlias() {
		return getMyApplication().getAppAlias();
	}

	public Boolean clientSideRequestIsTestMode() {
		return getMyApplication().isTestMode();
	}

	/**
	 * Permet d'envoyer un mail à partir du client.
	 *
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, NSArray names, NSArray pjs) throws Exception {
		try {
			String[] _names = new String[names.count()];
			NSData[] _pjs = new NSData[pjs.count()];

			for (int i = 0; i < pjs.count(); i++) {
				NSData element = (NSData) pjs.objectAtIndex(i);
				_pjs[i] = element;
			}

			for (int i = 0; i < names.count(); i++) {
				String element = (String) names.objectAtIndex(i);
				_names[i] = element;
			}

			if (!getMyApplication().sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody, _names, _pjs)) {
				throw new Exception("Erreur lors de l'envoi du mail.");
			}

		} catch (Throwable e) {
			throw new Exception(e.getMessage());
		}

	}

	public void clientSideRequestSendMailToAdmin(String mailSubject, String mailBody) throws Exception {
		try {
			if (!getMyApplication().sendMailToAdmin(mailSubject, mailBody)) {
				throw new Exception("Erreur lors de l'envoi du mail.");
			}
		} catch (Throwable e) {
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Renvoie le dictionnaires des fonctions dispos de l'appli pour le client.
	 *
	 * @throws Exception
	 */
	public final NSDictionary clientSideRequestAllFonctions() throws Exception {
		try {
			return getMyApplication().getAllFonctionsDico(defaultEditingContext());
		} catch (Throwable e) {
			throw new Exception(e.getMessage());
		}
	}

	public final void clientSideRequestAutowash() {
		autowash();
	}

	public final NSData clientSideRequestGetCurrentServerLogFile() throws Exception {
		if (myApplication.getServerLogFile() != null) {
			FileInputStream inputStream = new FileInputStream(myApplication.getServerLogFile());
			NSData res = new NSData(inputStream, 1024);
			return res;
		}
		return null;
	}

	/**
	 * On vérifie si le report est défini dans la table, sinon renvoie le chemin d'accès au report fourni par défaut par l'application.
	 *
	 * @param fn
	 * @return
	 */
	public final String getRealJasperFileName(String rname) {
		String loc = null;
		EOEnterpriseObject obj = ZFinder.fetchObject(defaultEditingContext(), "Report", "repId=%@", new NSArray(new Object[] {
				rname
		}), null, true);
		if (obj != null) {
			loc = (String) obj.valueForKey("repLocation");
			if (loc != null) {
				ZLogger.debug("Report " + rname + " défini dans la table REPORT : " + loc);
				return loc;
			}
		}

		if (loc == null) {
			ZLogger.debug("Report " + rname + " NON défini dans la table REPORT, utilisation du report par defaut");
			loc = myApplication.getReportsLocation() + rname;
		}
		return loc;
	}

	public final NSDictionary clientSideRequestExecuteStoredProcedureNamed(String name, NSDictionary args) throws Exception {
		ZLogger.debug("execution sp ", name);
		ZLogger.debug("args ", args);
		try {
			return EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), name, args);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final NSData clientSideRequestExportToExcelFromEOs(NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) throws Exception {
		try {
			System.out.println("clientSideRequestExportToExcelFromEOs");
			ExcelExport excelExport = new ExcelExport();
			ByteArrayOutputStream s = excelExport.defaultExcelExport(keyNames, headers, values);
			NSData res = new NSData(s.toByteArray());
			return res;
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public final void clientSideRequestExportToExcelFromSQLByThread(NSArray keyNames, NSArray headers, NSArray keysToSum, String sql, NSDictionary parametres) throws Exception {
		try {
			exportTaskThread = null;
			lastExcel = null;
			lastExportError = null;

			exportTaskThread = new ExportSQLTaskThread(keyNames, headers, keysToSum, sql, new HashMap(parametres.hashtable()));

			exportTaskThread.setDaemon(true);
			exportTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

			ZLogger.debug("Tache d'export lancee ...");
			exportTaskThread.start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public final void clientSideRequestExportToExcelFromFetchSpecByThread(NSArray keyNames, NSArray headers, EOFetchSpecification fetchSpec, NSArray postSorts, NSDictionary parametres) throws Exception {
		try {
			exportTaskThread = null;
			lastExcel = null;
			lastExportError = null;

			exportTaskThread = new ExportFetchSpecTaskThread(fetchSpec, headers, keyNames, postSorts, (parametres != null ? new HashMap(parametres.hashtable()) : null));

			exportTaskThread.setDaemon(true);
			exportTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

			ZLogger.debug("Tache d'export lancee ...");
			exportTaskThread.start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public final void clientSideRequestExportToExcelFromEOsByThread(NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) throws Exception {
		try {
			exportTaskThread = null;
			lastExcel = null;
			lastExportError = null;

			exportTaskThread = new ExportEOsTaskThread(keyNames, headers, values, (formats == null ? null : new HashMap(formats.hashtable())));

			exportTaskThread.setDaemon(true);
			exportTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

			ZLogger.debug("Tache d'export lancee ...");
			exportTaskThread.start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public final NSData clientSideRequestExportToExcelGetResult() throws Exception {
		if (lastExportError != null) {
			throw lastExportError;
		}
		Thread.yield();
		if (lastExcel != null && exportTaskThread != null) {
			return lastExcel;
		}
		return null;
	}

	public final void clientSideRequestExportToExcelByThreadCancel() {
		System.out.println("Interruption de la tache d'export en cours... ");
		lastExcel = null;
		if (exportTaskThread != null && exportTaskThread.isAlive() && !exportTaskThread.isInterrupted()) {
			exportTaskThread.interrupt();
			exportTaskThread = null;
		}
	}

	public final void clientSideRequestExportToExcelReset() {
		System.out.println("Reset Export... ");
		lastExcel = null;
		if (exportTaskThread != null && exportTaskThread.isAlive() && !exportTaskThread.isInterrupted()) {
			exportTaskThread.interrupt();
			exportTaskThread = null;
		}
	}

	/**
	 * Méthode serveur pour faire appel à la méthode de numérotation d'un objet métier.
	 *
	 * @param obj L'objet à numéroter
	 * @param objClass Le nom de la classe de l'objet à numeroter (dans le cas de la numerotation d'un ecriture brouillard, utiliser
	 *            "EOEcritureBrouillard")
	 */
	public final void clientSideRequestNumerotation(EOEnterpriseObject obj, final String objClass) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("");
		System.out.println("Session.clientSideRequestNumerotation()");
		System.out.println("Numerotation d'un objet " + objClass);

		try {
			FactoryNumerotation myFactoryNumerotation = new FactoryNumerotation(myApplication.showTrace());
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);

			if (EOECRITURE_SID.equals(objClass)) {
				System.out.println("Num ecr");
				myFactoryNumerotation.getNumeroEOEcriture(defaultEditingContext(), (EOEcriture) localObj);
				return;
			}
			else if (EOECRITUREBROUILLARD_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOEcritureBrouillard(defaultEditingContext(), (EOEcriture) localObj);
				return;
			}
			else if (EOBORDEREAUREJET_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOBordereauRejet(defaultEditingContext(), (EOBordereauRejet) localObj);
				return;
			}
			else if (EOEMARGEMENT_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOEmargement(defaultEditingContext(), (EOEmargement) localObj);
				return;
			}
			else if (EOPAIEMENT_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOPaiement(defaultEditingContext(), (EOPaiement) localObj);
				return;
			}
			else if (EOORDREPAIEMENT_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOOrdreDePaiement(defaultEditingContext(), (EOOrdreDePaiement) localObj);
				return;
			}
			else if (EOREIMPUTATION_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOReimputation(defaultEditingContext(), (EOReimputation) localObj);
				return;
			}
			else if (EORETENUE_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEORetenue(defaultEditingContext(), (EORetenue) localObj);
				return;
			}
			else if (EOBORDEREAU_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEOBordereauCheque(defaultEditingContext(), (EOBordereau) localObj);
				return;
			}
			else if (EORECOUVREMENT_SID.equals(objClass)) {
				myFactoryNumerotation.getNumeroEORecouvrement(defaultEditingContext(), (EORecouvrement) localObj);
				return;
			}

			else {
				System.out.println("Type de numerotation non reconnu");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestAfaireApresTraitementVisaBordereau(final EOEnterpriseObject obj) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("A faire apres visa bordereau ");
		try {
			FactoryProcessApresTraitement factoryProcessApresTraitement = new FactoryProcessApresTraitement(myApplication.showTrace());
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			factoryProcessApresTraitement.afaireApresVisaBordereau(defaultEditingContext(), (EOBordereau) localObj);
			System.out.println("Bordereau n° : " + ((EOBordereau) localObj).borNum());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public final Integer clientSideRequestExtourneCreerBordereau(final EOEnterpriseObject obj) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("clientSideRequestExtourneCreerBordereau");
		try {
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			EOBordereau bordereauInitial = (EOBordereau) localObj;
			NSDictionary dico = EOUtilities.primaryKeyForObject(defaultEditingContext(), bordereauInitial);

			NSMutableDictionary myDicoId = new NSMutableDictionary();
			myDicoId.takeValueForKey(dico.valueForKey(EOBordereau.BOR_ID_KEY), "bordIdn");
			NSDictionary dicoRes = EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "extourne.creer_bordereau", myDicoId);
			Integer res = (Integer) dicoRes.valueForKey("borIdn1");
			return res;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public final void clientSideRequestAfaireApresTraitementReimputation(final EOEnterpriseObject obj) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("A faire apres visa Reimpuitation ");
		try {
			FactoryProcessApresTraitement factoryProcessApresTraitement = new FactoryProcessApresTraitement(myApplication.showTrace());
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			factoryProcessApresTraitement.afaireApresReimputation(defaultEditingContext(), (EOReimputation) localObj);
			System.out.println("Reimputation n° : " + ((EOReimputation) localObj).reiNumero());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public NSDictionary clientSideRequestGetAppParametres() {
		return myApplication.appParametres();
	}

	public final void clientSideRequestAfaireApresTraitementPaiement(final EOEnterpriseObject obj) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("A faire apres paiement");
		try {
			FactoryProcessApresTraitement factoryProcessApresTraitement = new FactoryProcessApresTraitement(myApplication.showTrace());
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			factoryProcessApresTraitement.afaireApresPaiement(defaultEditingContext(), (EOPaiement) localObj);
			System.out.println("Paiement n° : " + ((EOPaiement) localObj).paiNumero());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestAfaireApresTraitementRecouvrement(final EOEnterpriseObject obj) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("A faire apres recouvrement");
		try {
			FactoryProcessApresTraitement factoryProcessApresTraitement = new FactoryProcessApresTraitement(myApplication.showTrace());
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			factoryProcessApresTraitement.afaireApresRecouvrement(defaultEditingContext(), (EORecouvrement) localObj);
			System.out.println("Recouvrement n° : " + ((EORecouvrement) localObj).recoNumero());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestAfaireApresTraitementRecouvrementReleve(final EOEnterpriseObject obj) throws Exception {
		//	  Récupérer l'objet au niveau du serveur
		System.out.println("A faire apres recouvrement releve");
		try {
			FactoryProcessApresTraitement factoryProcessApresTraitement = new FactoryProcessApresTraitement(myApplication.showTrace());
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			factoryProcessApresTraitement.afaireApresRecouvrementReleve(defaultEditingContext(), (EORecouvrement) localObj);
			System.out.println("Recouvrement n° : " + ((EORecouvrement) localObj).recoNumero());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestPapayePasserEcritureVISABord(final EOEnterpriseObject obj) throws Exception {
		try {
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			NSDictionary myDicoId = EOUtilities.primaryKeyForObject(defaultEditingContext(), localObj);
			ZLogger.debug(myDicoId);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "api_plsql_papaye.passerEcritureVISABord", myDicoId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestPapayePasserEcritureOppRetBord(final EOEnterpriseObject obj, final String genEcritures) throws Exception {
		try {
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			NSDictionary myDicoId = EOUtilities.primaryKeyForObject(defaultEditingContext(), localObj);
			NSMutableDictionary dic = new NSMutableDictionary();
			dic.takeValueForKey(myDicoId.valueForKey("borId"), "borId");
			dic.takeValueForKey(genEcritures, "passer_ecritures");
			ZLogger.debug(dic);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "api_plsql_papaye.passerEcritureOppRetBord", dic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestPapayePasserEcriturePaiement(final String obj, final String genEcritures) throws Exception {
		try {
			//	        EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject( defaultEditingContext() , obj);
			NSMutableDictionary dic = new NSMutableDictionary();

			dic.takeValueForKey(obj, "borlibelle_mois");
			dic.takeValueForKey(genEcritures, "passer_ecritures");

			ZLogger.debug(dic);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "api_plsql_papaye.passerEcriturePaiement", dic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestPayePafPasserEcritureVISABord(final EOEnterpriseObject obj, final EOEnterpriseObject util) throws Exception {
		try {
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			NSDictionary myDicoId = EOUtilities.primaryKeyForObject(defaultEditingContext(), localObj);
			NSMutableDictionary dic = new NSMutableDictionary();
			dic.takeValueForKey(myDicoId.valueForKey("borId"), "borId");
			dic.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), util).valueForKey(EOUtilisateur.UTL_ORDRE_KEY), "utlOrdre");
			ZLogger.debug(myDicoId);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "api_plsql_paf.passerEcritureVISABord", dic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestPapayePafPasserEcritureOppRetBord(final EOEnterpriseObject obj, final String genEcritures, EOEnterpriseObject util) throws Exception {
		try {
			EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
			NSDictionary myDicoId = EOUtilities.primaryKeyForObject(defaultEditingContext(), localObj);
			NSMutableDictionary dic = new NSMutableDictionary();
			dic.takeValueForKey(myDicoId.valueForKey("borId"), "borId");
			dic.takeValueForKey(genEcritures, "passer_ecritures");
			dic.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), util).valueForKey(EOUtilisateur.UTL_ORDRE_KEY), "utlOrdre");
			ZLogger.debug(dic);
			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "api_plsql_paf.passerEcritureOppRetBord", dic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void clientSideRequestPayePafPasserEcriturePaiement(final String obj, final String genEcritures, final EOEnterpriseObject util) throws Exception {
		try {
			//	        EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject( defaultEditingContext() , obj);
			NSMutableDictionary dic = new NSMutableDictionary();

			dic.takeValueForKey(obj, "borlibelle_mois");
			dic.takeValueForKey(genEcritures, "passer_ecritures");
			dic.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), util).valueForKey(EOUtilisateur.UTL_ORDRE_KEY), "utlOrdre");

			ZLogger.debug(dic);

			EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "api_plsql_paf.passerEcriturePaiement", dic);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Execute une requete sql et renvoie le resultat.
	 *
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public final NSArray clientSideRequestSqlQuery(final String sql) throws Exception {
		try {
			ZLogger.info("SQL=" + sql);
			return EOUtilities.rawRowsForSQL(defaultEditingContext(), myApplication.mainModelName(), sql, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Renvoie un tableau contenant des entités déjà fecthées dans le sharedEditingContext
	 *
	 * @param entite
	 * @return
	 */
	/*
	 * public NSArray clientSideRequestSharedObjects(String entite) { if (entite.equals("Fonction")) return getMyApplication().sharedAllFonctionsList;
	 *
	 * return new NSArray(); }
	 */

	/*
	 * (non-Javadoc)
	 *
	 * @see com.webobjects.appserver.WOSession#terminate()
	 */
	public void terminate() {
		ZLogger.info(getInfoConnectedUser() + " ==> Deconnexion");
		getMyApplication().getMySessions().remove(sessionID());
		super.terminate();
	}

	public String getInfoConnectedUser() {
		return connectedUser.toString();
	}

	/**
	 * Représente un client connecté.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class ConnectedUser {
		public Date getDateConnection() {
			return dateConnection;
		}

		public void setDateConnection(Date dateConnection) {
			this.dateConnection = dateConnection;
		}

		public String getIp() {
			return ip;
		}

		public void setIp(String ip) {
			this.ip = ip;
		}

		public String getLogin() {
			return login;
		}

		private Date dateConnection;
		private Date dateLastHeartBeat;

		private String ip;
		private String sessionID;

		/**
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return getLogin() + " connecte depuis le " + dateConnection + "(IP:" + getIp() + ") Dernier contact : " + dateLastHeartBeat + " (sessionID=" + sessionID + ")";
		}

		public String getSessionID() {
			return sessionID;
		}

		public void setSessionID(String sessionID) {
			this.sessionID = sessionID;
		}

		public Date getDateLastHeartBeat() {
			return dateLastHeartBeat;
		}

		public void setDateLastHeartBeat(Date dateLastHeartBeat) {
			this.dateLastHeartBeat = dateLastHeartBeat;
		}

		public HashMap toHashMap() {
			HashMap t = new HashMap();
			t.put("dateConnection", dateConnection);
			t.put("sessionID", sessionID);
			t.put("dateLastHeartBeat", dateLastHeartBeat);
			t.put("ip", getIp());
			t.put("login", getLogin());
			return t;
		}

		public NSDictionary toNSDictionary() {
			NSMutableDictionary t = new NSMutableDictionary();
			t.takeValueForKey(dateConnection, "dateConnection");
			t.takeValueForKey(sessionID, "sessionID");
			t.takeValueForKey(dateLastHeartBeat, "dateLastHeartBeat");
			t.takeValueForKey(getIp(), "ip");
			t.takeValueForKey(getLogin(), "login");
			return t;
		}

	}


	public class ExportEOsTaskThread extends Thread {
		//        private NSData excel;
		private final NSArray _keyNames, _headers, _values;

		//        private HashMap _formats;

		/**
         *
         */
		public ExportEOsTaskThread(NSArray keyNames, NSArray headers, NSArray values, HashMap formats) {
			_keyNames = keyNames;
			_headers = headers;
			_values = values;
			//            _formats = formats;
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			try {
				try {
					System.out.println("clientSideRequestExportToExcel");
					ExcelExport excelExport = new ExcelExport();
					ByteArrayOutputStream s = excelExport.defaultExcelExport(_keyNames, _headers, _values);
					NSData res = new NSData(s.toByteArray());
					lastExcel = res;
				} catch (Exception e) {
					throw e;
				}
			} catch (Exception e) {
				lastExportError = e;
				e.printStackTrace();
			}

		}
	}

	public class ExportSQLTaskThread extends Thread {
		//        private NSData excel;
		private final NSArray _keyNames, _headers;
		private final NSArray _keysToSum;
		private final String _sql;
		private final HashMap _parametres;

		/**
		 * @param parametres
		 */
		public ExportSQLTaskThread(NSArray keyNames, NSArray headers, NSArray keysToSum, String sql, HashMap parametres) {
			_keyNames = keyNames;
			_headers = headers;
			_keysToSum = keysToSum;
			_sql = sql;
			_parametres = parametres;
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			try {
				try {
					ExcelExport excelExport = new ExcelExport(_parametres);
					ByteArrayOutputStream s = excelExport.defaultExcelExport(defaultEditingContext(), myApplication.mainModelName(), _sql, _headers, _keyNames, _keysToSum);
					NSData res = new NSData(s.toByteArray());
					lastExcel = res;
				} catch (Exception e) {
					throw e;
				}
			} catch (Exception e) {
				lastExportError = e;
				e.printStackTrace();
			}

		}
	}

	public class ExportFetchSpecTaskThread extends Thread {
		//        private NSData excel;
		private final NSArray _keyNames, _headers;
		private final EOFetchSpecification _fetchSpecification;
		private final HashMap _parametres;
		private final NSArray _postSorts;

		/**
		 * @param postSorts
		 * @param parametres
		 */
		public ExportFetchSpecTaskThread(EOFetchSpecification fetchSpecification, final NSArray headers, final NSArray keyNames, NSArray postSorts, HashMap parametres) {
			_keyNames = keyNames;
			_headers = headers;
			_fetchSpecification = fetchSpecification;
			_parametres = parametres;
			_postSorts = postSorts;
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			try {
				try {
					ExcelExport excelExport = new ExcelExport(_parametres);
					ByteArrayOutputStream s = excelExport.defaultExcelExport(defaultEditingContext(), _fetchSpecification, _postSorts, _headers, _keyNames);
					NSData res = new NSData(s.toByteArray());
					lastExcel = res;
				} catch (Exception e) {
					throw e;
				}
			} catch (Exception e) {
				lastExportError = e;
				e.printStackTrace();
			}

		}
	}

	public class SqlStatementTaskThread extends Thread {
		private final String _sql;
		private final Connection _connection;
		private final boolean _commitAfterStatement;
		private Statement _statement;
		private final boolean _closeConnectionAfterStatement;

		/**
		 * @param connection Connection JDBC pour l'execution sql
		 * @param sql Instruction SQL a executer
		 * @param commitAfterStatement Indique s'il faut fair un commit a la fin de l'instruction
		 * @param closeConnectionAfterStatement Indique s'il faut fermer la connexion jdbc apres execution
		 * @throws Exception
		 */
		public SqlStatementTaskThread(Connection connection, String sql, boolean commitAfterStatement, boolean closeConnectionAfterStatement) throws Exception {
			_sql = sql;
			if (connection == null) {
				throw new Exception("La connection n'est pas definie");
			}
			_connection = connection;
			_commitAfterStatement = commitAfterStatement;
			_closeConnectionAfterStatement = closeConnectionAfterStatement;
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			try {
				try {
					//Lancer l'execution

					lastSqlStatement_Error = null;
					lastSqlStatement_statement = null;
					_statement = _connection.createStatement();
					_statement.execute(_sql);
					if (_commitAfterStatement) {
						_connection.commit();
					}
					lastSqlStatement_statement = _statement;

					if (_closeConnectionAfterStatement) {
						//S'il ne s'agit pas de la connexion globale a l'application, on la ferme
						if (!myApplication.getJDBCConnection().equals(_connection)) {
							_connection.close();
						}
					}

					ZLogger.info("L'instruction sql suivante a ete executee");
					ZLogger.info(_sql);

				} catch (Exception e) {
					throw e;
				}
			} catch (Exception e) {
				lastSqlStatement_Error = e;
				e.printStackTrace();
			}
		}
	}

	private class TestThread extends Thread {
		private long _duree;
		private int progress;

		public TestThread(long duree) {
			_duree = duree;
		}

		public void run() {
			try {
				progress = 0;
				long periode = _duree / 100;
				while (progress < _duree) {
					Thread.yield();
					progress++;
					Thread.sleep(periode);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		public void setDuree(long _duree) {
			this._duree = _duree;
		}
	}

	private final TestThread testThread = new TestThread(0);

	public final void clientSideRequestTestThread(final Integer duree) throws Exception {
		try {
			testThread.setDuree(duree.longValue());
			testThread.start();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Object clientSideRequestTestThreadProgress() throws Exception {
		if (testThread != null && testThread.isAlive()) {
			Thread.yield();
			return new Integer(testThread.progress);
		}
		return null;
	}

	public final EOEnterpriseObject clientSideRequestSavePreference(final EOEnterpriseObject util, final String prefKey, final String newValue, final String defaultValue, final String description) throws Exception {
		//        System.out.println("clientSideRequestSavePreference");
		//        ZLogger.verbose("clientSideRequestSavePreference");
		final EOUtilisateur utilisateur = (EOUtilisateur) getLocalInstanceOfEo(util);
		final FactoryPreference factoryPreference = new FactoryPreference(true);
		try {
			//            defaultEditingContext().revert();
			final EOUtilisateurPreference utilisateurPreference = factoryPreference.savePreference(defaultEditingContext(), utilisateur, prefKey, newValue, defaultValue, description);
			//            final EOUtilisateurPreference utilisateurPreference = factoryPreference.savePreference(defaultEditingContext(), utilisateur, prefKey, newValue);
			//            defaultEditingContext().saveChanges();
			return utilisateurPreference;
		} catch (Exception e) {
			e.printStackTrace();
			//            defaultEditingContext().revert();
			throw e;
		}
	}

	/**
	 * Renvoie l'equivalent de l'EOEnterpriseObject
	 *
	 * @param obj
	 * @return
	 */
	public final EOEnterpriseObject getLocalInstanceOfEo(final EOEnterpriseObject obj) {
		final EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
		return localObj;
	}

	public ConnectedUser getConnectedUser() {
		return connectedUser;
	}



}
