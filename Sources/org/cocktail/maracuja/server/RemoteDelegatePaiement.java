package org.cocktail.maracuja.server;

import org.cocktail.maracuja.server.metier.EOComptabilite;
import org.cocktail.maracuja.server.metier.EOExercice;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class RemoteDelegatePaiement extends RemoteDelegate {

	public RemoteDelegatePaiement(Session session) {
		super(session);
	}

	public final Integer clientSideRequestNextNumeroPaiement(final EOEnterpriseObject comptabilite, final EOEnterpriseObject exercice) throws Exception {
		try {
			EOComptabilite localCompta = (EOComptabilite) EOUtilities.localInstanceOfObject(defaultEditingContext(), comptabilite);
			EOExercice localExercice = (EOExercice) EOUtilities.localInstanceOfObject(defaultEditingContext(), exercice);
			NSMutableDictionary dico = new NSMutableDictionary();

			dico.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), localCompta).valueForKey(EOComptabilite.COM_ORDRE_KEY), "001comordre");
			dico.takeValueForKey(EOUtilities.primaryKeyForObject(defaultEditingContext(), localExercice).valueForKey(EOExercice.EXE_ORDRE_KEY), "002exeordre");
			//this.executeStoredProcedure(defaultEditingContext(), "afaireaprestraitement.apres_reimputation", dico);	
			NSDictionary res = EOUtilities.executeStoredProcedureNamed(defaultEditingContext(), "numerotationObject.next_numero_paiement", dico);
			return (Integer) res.valueForKey("000result");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}
}
