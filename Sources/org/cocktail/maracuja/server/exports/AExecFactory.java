/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server.exports;

import java.io.File;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.ICktlReportingTaskListener;

import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.jdbcadaptor.JDBCContext;

public abstract class AExecFactory {
	protected Connection getJDBCConnection() {
		return ((JDBCContext) getAdaptorContext()).connection();
	}

	protected EOAdaptorContext getAdaptorContext() {
		return CktlDataBus.databaseContext().availableChannel().adaptorChannel().adaptorContext();
	}

	protected static boolean fileExists(String path) {
		File f = new File(path);
		return (f.exists());
	}

	protected static boolean isDirectory(String path) {
		File f = new File(path);
		return (f.isDirectory());
	}

	public static String getDateTimeAsString() {
		String format = "yyyyMMdd-HHmmss";
		return new SimpleDateFormat(format).format(new Date());
	}

	public abstract CktlAbstractReporter getReporter();

	public abstract ICktlReportingTaskListener getReporterProgress();

}
