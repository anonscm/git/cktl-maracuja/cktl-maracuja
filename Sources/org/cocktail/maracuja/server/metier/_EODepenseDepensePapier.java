/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepenseDepensePapier.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepenseDepensePapier extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "DepenseDepensePapier";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_DEPENSE_DEPENSE_PAPIER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dpcoId";

	public static final String JD_DEP_ID_KEY = "jdDepId";

// Attributs non visibles
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String DPP_ID_KEY = "dppId";
	public static final String MAN_ID_KEY = "manId";
	public static final String MARA_DEP_ID_KEY = "maraDepId";

//Colonnes dans la base de donnees
	public static final String JD_DEP_ID_COLKEY = "JD_DEP_ID";

	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String MARA_DEP_ID_COLKEY = "MARA_DEP_ID";


	// Relationships
	public static final String DEPENSE_KEY = "depense";
	public static final String JD_DEPENSE_CTRL_PLANCO_KEY = "jdDepenseCtrlPlanco";
	public static final String JD_DEPENSE_PAPIER_KEY = "jdDepensePapier";
	public static final String MANDAT_KEY = "mandat";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer jdDepId() {
    return (Integer) storedValueForKey(JD_DEP_ID_KEY);
  }

  public void setJdDepId(Integer value) {
    takeStoredValueForKey(value, JD_DEP_ID_KEY);
  }

  public org.cocktail.maracuja.server.metier.EODepense depense() {
    return (org.cocktail.maracuja.server.metier.EODepense)storedValueForKey(DEPENSE_KEY);
  }

  public void setDepenseRelationship(org.cocktail.maracuja.server.metier.EODepense value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EODepense oldValue = depense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOJdDepenseCtrlPlanco jdDepenseCtrlPlanco() {
    return (org.cocktail.maracuja.server.metier.EOJdDepenseCtrlPlanco)storedValueForKey(JD_DEPENSE_CTRL_PLANCO_KEY);
  }

  public void setJdDepenseCtrlPlancoRelationship(org.cocktail.maracuja.server.metier.EOJdDepenseCtrlPlanco value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOJdDepenseCtrlPlanco oldValue = jdDepenseCtrlPlanco();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, JD_DEPENSE_CTRL_PLANCO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, JD_DEPENSE_CTRL_PLANCO_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOJdDepensePapier jdDepensePapier() {
    return (org.cocktail.maracuja.server.metier.EOJdDepensePapier)storedValueForKey(JD_DEPENSE_PAPIER_KEY);
  }

  public void setJdDepensePapierRelationship(org.cocktail.maracuja.server.metier.EOJdDepensePapier value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOJdDepensePapier oldValue = jdDepensePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, JD_DEPENSE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, JD_DEPENSE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOMandat mandat() {
    return (org.cocktail.maracuja.server.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.maracuja.server.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  

/**
 * Créer une instance de EODepenseDepensePapier avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODepenseDepensePapier createEODepenseDepensePapier(EOEditingContext editingContext, Integer jdDepId
, org.cocktail.maracuja.server.metier.EODepense depense, org.cocktail.maracuja.server.metier.EOJdDepenseCtrlPlanco jdDepenseCtrlPlanco, org.cocktail.maracuja.server.metier.EOJdDepensePapier jdDepensePapier, org.cocktail.maracuja.server.metier.EOMandat mandat			) {
    EODepenseDepensePapier eo = (EODepenseDepensePapier) createAndInsertInstance(editingContext, _EODepenseDepensePapier.ENTITY_NAME);    
		eo.setJdDepId(jdDepId);
    eo.setDepenseRelationship(depense);
    eo.setJdDepenseCtrlPlancoRelationship(jdDepenseCtrlPlanco);
    eo.setJdDepensePapierRelationship(jdDepensePapier);
    eo.setMandatRelationship(mandat);
    return eo;
  }

  
	  public EODepenseDepensePapier localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepenseDepensePapier)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepenseDepensePapier creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepenseDepensePapier creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODepenseDepensePapier object = (EODepenseDepensePapier)createAndInsertInstance(editingContext, _EODepenseDepensePapier.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODepenseDepensePapier localInstanceIn(EOEditingContext editingContext, EODepenseDepensePapier eo) {
    EODepenseDepensePapier localInstance = (eo == null) ? null : (EODepenseDepensePapier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODepenseDepensePapier#localInstanceIn a la place.
   */
	public static EODepenseDepensePapier localInstanceOf(EOEditingContext editingContext, EODepenseDepensePapier eo) {
		return EODepenseDepensePapier.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepenseDepensePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepenseDepensePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepenseDepensePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepenseDepensePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepenseDepensePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepenseDepensePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepenseDepensePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepenseDepensePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepenseDepensePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepenseDepensePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepenseDepensePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepenseDepensePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
