/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCompteBancaire.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCompteBancaire extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "CompteBancaire";
	public static final String ENTITY_TABLE_NAME = "maracuja.Compte_Bancaire";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cbaOrdre";

	public static final String CBA_ATTENTE_CREDIT_KEY = "cbaAttenteCredit";
	public static final String CBA_ATTENTE_DEBIT_KEY = "cbaAttenteDebit";
	public static final String CBA_DEBUT_KEY = "cbaDebut";
	public static final String CBA_ETAT_KEY = "cbaEtat";
	public static final String CBA_FIN_KEY = "cbaFin";
	public static final String CBA_LIB_COURT_KEY = "cbaLibCourt";
	public static final String CBA_LIBELLE_KEY = "cbaLibelle";
	public static final String CBA_SOLDE_KEY = "cbaSolde";
	public static final String CBA_TYPE_KEY = "cbaType";

// Attributs non visibles
	public static final String CBA_ORDRE_KEY = "cbaOrdre";
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String PCO_NUM_EMI_KEY = "pcoNumEmi";
	public static final String PCO_NUM_ENC_KEY = "pcoNumEnc";
	public static final String PCO_NUM_OPE_KEY = "pcoNumOpe";
	public static final String RIB_ORDRE_KEY = "ribOrdre";

//Colonnes dans la base de donnees
	public static final String CBA_ATTENTE_CREDIT_COLKEY = "cba_Attente_Credit";
	public static final String CBA_ATTENTE_DEBIT_COLKEY = "cba_Attente_Debit";
	public static final String CBA_DEBUT_COLKEY = "cba_Debut";
	public static final String CBA_ETAT_COLKEY = "cba_Etat";
	public static final String CBA_FIN_COLKEY = "cba_Fin";
	public static final String CBA_LIB_COURT_COLKEY = "cba_LibCourt";
	public static final String CBA_LIBELLE_COLKEY = "cba_Libelle";
	public static final String CBA_SOLDE_COLKEY = "cba_Solde";
	public static final String CBA_TYPE_COLKEY = "cba_type";

	public static final String CBA_ORDRE_COLKEY = "cba_Ordre";
	public static final String COM_ORDRE_COLKEY = "com_ordre";
	public static final String PCO_NUM_EMI_COLKEY = "PCO_NUM_EMI";
	public static final String PCO_NUM_ENC_COLKEY = "PCO_NUM_ENC";
	public static final String PCO_NUM_OPE_COLKEY = "PCO_NUM_OPE";
	public static final String RIB_ORDRE_COLKEY = "rib_Ordre";


	// Relationships
	public static final String COMPTABILITE_KEY = "comptabilite";
	public static final String PLAN_COMPTABLE_EMI_KEY = "planComptableEmi";
	public static final String PLAN_COMPTABLE_ENC_KEY = "planComptableEnc";
	public static final String PLAN_COMPTABLE_OPE_KEY = "planComptableOpe";
	public static final String RIB_KEY = "rib";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal cbaAttenteCredit() {
    return (java.math.BigDecimal) storedValueForKey(CBA_ATTENTE_CREDIT_KEY);
  }

  public void setCbaAttenteCredit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBA_ATTENTE_CREDIT_KEY);
  }

  public java.math.BigDecimal cbaAttenteDebit() {
    return (java.math.BigDecimal) storedValueForKey(CBA_ATTENTE_DEBIT_KEY);
  }

  public void setCbaAttenteDebit(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBA_ATTENTE_DEBIT_KEY);
  }

  public NSTimestamp cbaDebut() {
    return (NSTimestamp) storedValueForKey(CBA_DEBUT_KEY);
  }

  public void setCbaDebut(NSTimestamp value) {
    takeStoredValueForKey(value, CBA_DEBUT_KEY);
  }

  public String cbaEtat() {
    return (String) storedValueForKey(CBA_ETAT_KEY);
  }

  public void setCbaEtat(String value) {
    takeStoredValueForKey(value, CBA_ETAT_KEY);
  }

  public NSTimestamp cbaFin() {
    return (NSTimestamp) storedValueForKey(CBA_FIN_KEY);
  }

  public void setCbaFin(NSTimestamp value) {
    takeStoredValueForKey(value, CBA_FIN_KEY);
  }

  public String cbaLibCourt() {
    return (String) storedValueForKey(CBA_LIB_COURT_KEY);
  }

  public void setCbaLibCourt(String value) {
    takeStoredValueForKey(value, CBA_LIB_COURT_KEY);
  }

  public String cbaLibelle() {
    return (String) storedValueForKey(CBA_LIBELLE_KEY);
  }

  public void setCbaLibelle(String value) {
    takeStoredValueForKey(value, CBA_LIBELLE_KEY);
  }

  public java.math.BigDecimal cbaSolde() {
    return (java.math.BigDecimal) storedValueForKey(CBA_SOLDE_KEY);
  }

  public void setCbaSolde(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CBA_SOLDE_KEY);
  }

  public String cbaType() {
    return (String) storedValueForKey(CBA_TYPE_KEY);
  }

  public void setCbaType(String value) {
    takeStoredValueForKey(value, CBA_TYPE_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOComptabilite comptabilite() {
    return (org.cocktail.maracuja.server.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
  }

  public void setComptabiliteRelationship(org.cocktail.maracuja.server.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOComptabilite oldValue = comptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptableEmi() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_EMI_KEY);
  }

  public void setPlanComptableEmiRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptableEmi();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_EMI_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_EMI_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptableEnc() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_ENC_KEY);
  }

  public void setPlanComptableEncRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptableEnc();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_ENC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_ENC_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptableOpe() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_OPE_KEY);
  }

  public void setPlanComptableOpeRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptableOpe();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_OPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_OPE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORib rib() {
    return (org.cocktail.maracuja.server.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.server.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  

/**
 * Créer une instance de EOCompteBancaire avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCompteBancaire createEOCompteBancaire(EOEditingContext editingContext, java.math.BigDecimal cbaAttenteCredit
, java.math.BigDecimal cbaAttenteDebit
, NSTimestamp cbaDebut
, String cbaEtat
, String cbaLibCourt
, String cbaLibelle
, java.math.BigDecimal cbaSolde
, String cbaType
, org.cocktail.maracuja.server.metier.EOComptabilite comptabilite, org.cocktail.maracuja.server.metier.EOPlanComptable planComptableEmi, org.cocktail.maracuja.server.metier.EOPlanComptable planComptableEnc, org.cocktail.maracuja.server.metier.EOPlanComptable planComptableOpe, org.cocktail.maracuja.server.metier.EORib rib			) {
    EOCompteBancaire eo = (EOCompteBancaire) createAndInsertInstance(editingContext, _EOCompteBancaire.ENTITY_NAME);    
		eo.setCbaAttenteCredit(cbaAttenteCredit);
		eo.setCbaAttenteDebit(cbaAttenteDebit);
		eo.setCbaDebut(cbaDebut);
		eo.setCbaEtat(cbaEtat);
		eo.setCbaLibCourt(cbaLibCourt);
		eo.setCbaLibelle(cbaLibelle);
		eo.setCbaSolde(cbaSolde);
		eo.setCbaType(cbaType);
    eo.setComptabiliteRelationship(comptabilite);
    eo.setPlanComptableEmiRelationship(planComptableEmi);
    eo.setPlanComptableEncRelationship(planComptableEnc);
    eo.setPlanComptableOpeRelationship(planComptableOpe);
    eo.setRibRelationship(rib);
    return eo;
  }

  
	  public EOCompteBancaire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCompteBancaire)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompteBancaire creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCompteBancaire creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCompteBancaire object = (EOCompteBancaire)createAndInsertInstance(editingContext, _EOCompteBancaire.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCompteBancaire localInstanceIn(EOEditingContext editingContext, EOCompteBancaire eo) {
    EOCompteBancaire localInstance = (eo == null) ? null : (EOCompteBancaire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCompteBancaire#localInstanceIn a la place.
   */
	public static EOCompteBancaire localInstanceOf(EOEditingContext editingContext, EOCompteBancaire eo) {
		return EOCompteBancaire.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCompteBancaire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCompteBancaire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCompteBancaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCompteBancaire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCompteBancaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCompteBancaire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCompteBancaire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCompteBancaire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCompteBancaire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCompteBancaire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCompteBancaire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCompteBancaire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
