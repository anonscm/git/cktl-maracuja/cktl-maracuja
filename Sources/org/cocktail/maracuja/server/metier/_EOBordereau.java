/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereau.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBordereau extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Bordereau";
	public static final String ENTITY_TABLE_NAME = "maracuja.Bordereau";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "borId";

	public static final String BOR_DATE_CREATION_KEY = "borDateCreation";
	public static final String BOR_DATE_VISA_KEY = "borDateVisa";
	public static final String BOR_ETAT_KEY = "borEtat";
	public static final String BOR_NUM_KEY = "borNum";
	public static final String BOR_ORDRE_KEY = "borOrdre";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_ORDRE_VISA_KEY = "utlOrdreVisa";

//Colonnes dans la base de donnees
	public static final String BOR_DATE_CREATION_COLKEY = "bor_Date_creation";
	public static final String BOR_DATE_VISA_COLKEY = "bor_Date_Visa";
	public static final String BOR_ETAT_COLKEY = "bor_etat";
	public static final String BOR_NUM_COLKEY = "bor_num";
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";
	public static final String UTL_ORDRE_VISA_COLKEY = "utl_ORDRE_VISA";


	// Relationships
	public static final String BORDEREAU_BROUILLARDS_KEY = "bordereauBrouillards";
	public static final String BORDEREAU_INFO_KEY = "bordereauInfo";
	public static final String CHEQUES_KEY = "cheques";
	public static final String EXERCICE_KEY = "exercice";
	public static final String GESTION_KEY = "gestion";
	public static final String MANDATS_KEY = "mandats";
	public static final String TITRES_KEY = "titres";
	public static final String TO_BROUILLARDS_KEY = "toBrouillards";
	public static final String TYPE_BORDEREAU_KEY = "typeBordereau";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String UTILISATEUR_VISA_KEY = "utilisateurVisa";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp borDateCreation() {
    return (NSTimestamp) storedValueForKey(BOR_DATE_CREATION_KEY);
  }

  public void setBorDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, BOR_DATE_CREATION_KEY);
  }

  public NSTimestamp borDateVisa() {
    return (NSTimestamp) storedValueForKey(BOR_DATE_VISA_KEY);
  }

  public void setBorDateVisa(NSTimestamp value) {
    takeStoredValueForKey(value, BOR_DATE_VISA_KEY);
  }

  public String borEtat() {
    return (String) storedValueForKey(BOR_ETAT_KEY);
  }

  public void setBorEtat(String value) {
    takeStoredValueForKey(value, BOR_ETAT_KEY);
  }

  public Integer borNum() {
    return (Integer) storedValueForKey(BOR_NUM_KEY);
  }

  public void setBorNum(Integer value) {
    takeStoredValueForKey(value, BOR_NUM_KEY);
  }

  public Integer borOrdre() {
    return (Integer) storedValueForKey(BOR_ORDRE_KEY);
  }

  public void setBorOrdre(Integer value) {
    takeStoredValueForKey(value, BOR_ORDRE_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOBordereauInfo bordereauInfo() {
    return (org.cocktail.maracuja.server.metier.EOBordereauInfo)storedValueForKey(BORDEREAU_INFO_KEY);
  }

  public void setBordereauInfoRelationship(org.cocktail.maracuja.server.metier.EOBordereauInfo value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOBordereauInfo oldValue = bordereauInfo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_INFO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_INFO_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeBordereau typeBordereau() {
    return (org.cocktail.maracuja.server.metier.EOTypeBordereau)storedValueForKey(TYPE_BORDEREAU_KEY);
  }

  public void setTypeBordereauRelationship(org.cocktail.maracuja.server.metier.EOTypeBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeBordereau oldValue = typeBordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateurVisa() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VISA_KEY);
  }

  public void setUtilisateurVisaRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateurVisa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_VISA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VISA_KEY);
    }
  }
  
  public NSArray bordereauBrouillards() {
    return (NSArray)storedValueForKey(BORDEREAU_BROUILLARDS_KEY);
  }

  public NSArray bordereauBrouillards(EOQualifier qualifier) {
    return bordereauBrouillards(qualifier, null, false);
  }

  public NSArray bordereauBrouillards(EOQualifier qualifier, boolean fetch) {
    return bordereauBrouillards(qualifier, null, fetch);
  }

  public NSArray bordereauBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOBordereauBrouillard.BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOBordereauBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = bordereauBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToBordereauBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOBordereauBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BORDEREAU_BROUILLARDS_KEY);
  }

  public void removeFromBordereauBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOBordereauBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BORDEREAU_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOBordereauBrouillard createBordereauBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BordereauBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BORDEREAU_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.server.metier.EOBordereauBrouillard) eo;
  }

  public void deleteBordereauBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOBordereauBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BORDEREAU_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBordereauBrouillardsRelationships() {
    Enumeration objects = bordereauBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBordereauBrouillardsRelationship((org.cocktail.maracuja.server.metier.EOBordereauBrouillard)objects.nextElement());
    }
  }

  public NSArray cheques() {
    return (NSArray)storedValueForKey(CHEQUES_KEY);
  }

  public NSArray cheques(EOQualifier qualifier) {
    return cheques(qualifier, null, false);
  }

  public NSArray cheques(EOQualifier qualifier, boolean fetch) {
    return cheques(qualifier, null, fetch);
  }

  public NSArray cheques(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOCheque.BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOCheque.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = cheques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToChequesRelationship(org.cocktail.maracuja.server.metier.EOCheque object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CHEQUES_KEY);
  }

  public void removeFromChequesRelationship(org.cocktail.maracuja.server.metier.EOCheque object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CHEQUES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOCheque createChequesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Cheque");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CHEQUES_KEY);
    return (org.cocktail.maracuja.server.metier.EOCheque) eo;
  }

  public void deleteChequesRelationship(org.cocktail.maracuja.server.metier.EOCheque object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CHEQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllChequesRelationships() {
    Enumeration objects = cheques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteChequesRelationship((org.cocktail.maracuja.server.metier.EOCheque)objects.nextElement());
    }
  }

  public NSArray mandats() {
    return (NSArray)storedValueForKey(MANDATS_KEY);
  }

  public NSArray mandats(EOQualifier qualifier) {
    return mandats(qualifier, null, false);
  }

  public NSArray mandats(EOQualifier qualifier, boolean fetch) {
    return mandats(qualifier, null, fetch);
  }

  public NSArray mandats(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOMandat.BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOMandat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = mandats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMandatsRelationship(org.cocktail.maracuja.server.metier.EOMandat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
  }

  public void removeFromMandatsRelationship(org.cocktail.maracuja.server.metier.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOMandat createMandatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Mandat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MANDATS_KEY);
    return (org.cocktail.maracuja.server.metier.EOMandat) eo;
  }

  public void deleteMandatsRelationship(org.cocktail.maracuja.server.metier.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMandatsRelationships() {
    Enumeration objects = mandats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMandatsRelationship((org.cocktail.maracuja.server.metier.EOMandat)objects.nextElement());
    }
  }

  public NSArray titres() {
    return (NSArray)storedValueForKey(TITRES_KEY);
  }

  public NSArray titres(EOQualifier qualifier) {
    return titres(qualifier, null, false);
  }

  public NSArray titres(EOQualifier qualifier, boolean fetch) {
    return titres(qualifier, null, fetch);
  }

  public NSArray titres(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOTitre.BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOTitre.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = titres();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTitresRelationship(org.cocktail.maracuja.server.metier.EOTitre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TITRES_KEY);
  }

  public void removeFromTitresRelationship(org.cocktail.maracuja.server.metier.EOTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOTitre createTitresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Titre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TITRES_KEY);
    return (org.cocktail.maracuja.server.metier.EOTitre) eo;
  }

  public void deleteTitresRelationship(org.cocktail.maracuja.server.metier.EOTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTitresRelationships() {
    Enumeration objects = titres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTitresRelationship((org.cocktail.maracuja.server.metier.EOTitre)objects.nextElement());
    }
  }

  public NSArray toBrouillards() {
    return (NSArray)storedValueForKey(TO_BROUILLARDS_KEY);
  }

  public NSArray toBrouillards(EOQualifier qualifier) {
    return toBrouillards(qualifier, null, false);
  }

  public NSArray toBrouillards(EOQualifier qualifier, boolean fetch) {
    return toBrouillards(qualifier, null, fetch);
  }

  public NSArray toBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOBrouillard.TO_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_BROUILLARDS_KEY);
  }

  public void removeFromToBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOBrouillard createToBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Brouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.server.metier.EOBrouillard) eo;
  }

  public void deleteToBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToBrouillardsRelationships() {
    Enumeration objects = toBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToBrouillardsRelationship((org.cocktail.maracuja.server.metier.EOBrouillard)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOBordereau avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBordereau createEOBordereau(EOEditingContext editingContext, NSTimestamp borDateCreation
, Integer borOrdre
, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOGestion gestion, org.cocktail.maracuja.server.metier.EOTypeBordereau typeBordereau, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EOBordereau eo = (EOBordereau) createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME);    
		eo.setBorDateCreation(borDateCreation);
		eo.setBorOrdre(borOrdre);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    eo.setTypeBordereauRelationship(typeBordereau);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOBordereau localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBordereau)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBordereau creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBordereau creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOBordereau object = (EOBordereau)createAndInsertInstance(editingContext, _EOBordereau.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOBordereau localInstanceIn(EOEditingContext editingContext, EOBordereau eo) {
    EOBordereau localInstance = (eo == null) ? null : (EOBordereau)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBordereau#localInstanceIn a la place.
   */
	public static EOBordereau localInstanceOf(EOEditingContext editingContext, EOBordereau eo) {
		return EOBordereau.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBordereau fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBordereau fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereau)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereau)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBordereau fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereau eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereau ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereau fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
