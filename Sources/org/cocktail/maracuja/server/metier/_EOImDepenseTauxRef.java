/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOImDepenseTauxRef.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOImDepenseTauxRef extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "ImDepenseTauxRef";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_DEPENSE_IM_TAUX_REF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dppId";

	public static final String DATE_DEBUT_DGP_KEY = "dateDebutDgp";
	public static final String DATE_DETERMINATION_TAUX_KEY = "dateDeterminationTaux";
	public static final String DEBUT_SUSPENSION_KEY = "debutSuspension";
	public static final String DPP_DATE_RECEPTION_KEY = "dppDateReception";
	public static final String DPP_DATE_SERVICE_FAIT_KEY = "dppDateServiceFait";
	public static final String DUREE_SUSPENSION_KEY = "dureeSuspension";
	public static final String FIN_DGP_REELLE_KEY = "finDgpReelle";
	public static final String FIN_DGP_THEORIQUE_KEY = "finDgpTheorique";
	public static final String FIN_SUSPENSION_KEY = "finSuspension";
	public static final String IMDG_DGP_KEY = "imdgDgp";

// Attributs non visibles
	public static final String DPP_ID_KEY = "dppId";
	public static final String IMTA_ID_KEY = "imtaId";
	public static final String IMTT_ID_KEY = "imttId";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_DGP_COLKEY = "date_DEBUT_dgp";
	public static final String DATE_DETERMINATION_TAUX_COLKEY = "DATE_DETERMINATION_TAUX";
	public static final String DEBUT_SUSPENSION_COLKEY = "debut_suspension";
	public static final String DPP_DATE_RECEPTION_COLKEY = "DPP_DATE_RECEPTION";
	public static final String DPP_DATE_SERVICE_FAIT_COLKEY = "DPP_DATE_SERVICE_FAIT";
	public static final String DUREE_SUSPENSION_COLKEY = "duree_suspension";
	public static final String FIN_DGP_REELLE_COLKEY = "fin_dgp_reelle";
	public static final String FIN_DGP_THEORIQUE_COLKEY = "fin_dgp_theorique";
	public static final String FIN_SUSPENSION_COLKEY = "fin_suspension";
	public static final String IMDG_DGP_COLKEY = "IMDG_DGP";

	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String IMTA_ID_COLKEY = "IMTA_ID";
	public static final String IMTT_ID_COLKEY = "IMTT_ID";


	// Relationships
	public static final String IM_TAUX_KEY = "imTaux";
	public static final String IM_TYPE_TAUX_KEY = "imTypeTaux";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebutDgp() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_DGP_KEY);
  }

  public void setDateDebutDgp(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_DGP_KEY);
  }

  public NSTimestamp dateDeterminationTaux() {
    return (NSTimestamp) storedValueForKey(DATE_DETERMINATION_TAUX_KEY);
  }

  public void setDateDeterminationTaux(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DETERMINATION_TAUX_KEY);
  }

  public NSTimestamp debutSuspension() {
    return (NSTimestamp) storedValueForKey(DEBUT_SUSPENSION_KEY);
  }

  public void setDebutSuspension(NSTimestamp value) {
    takeStoredValueForKey(value, DEBUT_SUSPENSION_KEY);
  }

  public NSTimestamp dppDateReception() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_RECEPTION_KEY);
  }

  public void setDppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp dppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setDppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SERVICE_FAIT_KEY);
  }

  public Long dureeSuspension() {
    return (Long) storedValueForKey(DUREE_SUSPENSION_KEY);
  }

  public void setDureeSuspension(Long value) {
    takeStoredValueForKey(value, DUREE_SUSPENSION_KEY);
  }

  public NSTimestamp finDgpReelle() {
    return (NSTimestamp) storedValueForKey(FIN_DGP_REELLE_KEY);
  }

  public void setFinDgpReelle(NSTimestamp value) {
    takeStoredValueForKey(value, FIN_DGP_REELLE_KEY);
  }

  public NSTimestamp finDgpTheorique() {
    return (NSTimestamp) storedValueForKey(FIN_DGP_THEORIQUE_KEY);
  }

  public void setFinDgpTheorique(NSTimestamp value) {
    takeStoredValueForKey(value, FIN_DGP_THEORIQUE_KEY);
  }

  public NSTimestamp finSuspension() {
    return (NSTimestamp) storedValueForKey(FIN_SUSPENSION_KEY);
  }

  public void setFinSuspension(NSTimestamp value) {
    takeStoredValueForKey(value, FIN_SUSPENSION_KEY);
  }

  public Long imdgDgp() {
    return (Long) storedValueForKey(IMDG_DGP_KEY);
  }

  public void setImdgDgp(Long value) {
    takeStoredValueForKey(value, IMDG_DGP_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOImTaux imTaux() {
    return (org.cocktail.maracuja.server.metier.EOImTaux)storedValueForKey(IM_TAUX_KEY);
  }

  public void setImTauxRelationship(org.cocktail.maracuja.server.metier.EOImTaux value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOImTaux oldValue = imTaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IM_TAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, IM_TAUX_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOImTypeTaux imTypeTaux() {
    return (org.cocktail.maracuja.server.metier.EOImTypeTaux)storedValueForKey(IM_TYPE_TAUX_KEY);
  }

  public void setImTypeTauxRelationship(org.cocktail.maracuja.server.metier.EOImTypeTaux value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOImTypeTaux oldValue = imTypeTaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IM_TYPE_TAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, IM_TYPE_TAUX_KEY);
    }
  }
  

/**
 * Créer une instance de EOImDepenseTauxRef avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOImDepenseTauxRef createEOImDepenseTauxRef(EOEditingContext editingContext, NSTimestamp dateDebutDgp
, NSTimestamp dateDeterminationTaux
, NSTimestamp debutSuspension
, NSTimestamp dppDateReception
, NSTimestamp dppDateServiceFait
, Long dureeSuspension
, NSTimestamp finDgpReelle
, NSTimestamp finDgpTheorique
, NSTimestamp finSuspension
, Long imdgDgp
, org.cocktail.maracuja.server.metier.EOImTaux imTaux, org.cocktail.maracuja.server.metier.EOImTypeTaux imTypeTaux			) {
    EOImDepenseTauxRef eo = (EOImDepenseTauxRef) createAndInsertInstance(editingContext, _EOImDepenseTauxRef.ENTITY_NAME);    
		eo.setDateDebutDgp(dateDebutDgp);
		eo.setDateDeterminationTaux(dateDeterminationTaux);
		eo.setDebutSuspension(debutSuspension);
		eo.setDppDateReception(dppDateReception);
		eo.setDppDateServiceFait(dppDateServiceFait);
		eo.setDureeSuspension(dureeSuspension);
		eo.setFinDgpReelle(finDgpReelle);
		eo.setFinDgpTheorique(finDgpTheorique);
		eo.setFinSuspension(finSuspension);
		eo.setImdgDgp(imdgDgp);
    eo.setImTauxRelationship(imTaux);
    eo.setImTypeTauxRelationship(imTypeTaux);
    return eo;
  }

  
	  public EOImDepenseTauxRef localInstanceIn(EOEditingContext editingContext) {
	  		return (EOImDepenseTauxRef)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImDepenseTauxRef creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImDepenseTauxRef creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOImDepenseTauxRef object = (EOImDepenseTauxRef)createAndInsertInstance(editingContext, _EOImDepenseTauxRef.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOImDepenseTauxRef localInstanceIn(EOEditingContext editingContext, EOImDepenseTauxRef eo) {
    EOImDepenseTauxRef localInstance = (eo == null) ? null : (EOImDepenseTauxRef)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOImDepenseTauxRef#localInstanceIn a la place.
   */
	public static EOImDepenseTauxRef localInstanceOf(EOEditingContext editingContext, EOImDepenseTauxRef eo) {
		return EOImDepenseTauxRef.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOImDepenseTauxRef fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOImDepenseTauxRef fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOImDepenseTauxRef eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOImDepenseTauxRef)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOImDepenseTauxRef fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOImDepenseTauxRef fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOImDepenseTauxRef eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOImDepenseTauxRef)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOImDepenseTauxRef fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOImDepenseTauxRef eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOImDepenseTauxRef ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOImDepenseTauxRef fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
