/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecetteRelance.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecetteRelance extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "RecetteRelance";
	public static final String ENTITY_TABLE_NAME = "maracuja.Recette_relance";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rerOrdre";

	public static final String RER_CONTACT_KEY = "rerContact";
	public static final String RER_DATE_CREATION_KEY = "rerDateCreation";
	public static final String RER_DATE_IMPRESSION_KEY = "rerDateImpression";
	public static final String RER_DELAI_PAIEMENT_KEY = "rerDelaiPaiement";
	public static final String RER_ETAT_KEY = "rerEtat";
	public static final String RER_LIBELLE_KEY = "rerLibelle";
	public static final String RER_MONT_KEY = "rerMont";
	public static final String RER_NUMERO_KEY = "rerNumero";
	public static final String RER_PS_KEY = "rerPs";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String REC_ID_KEY = "recId";
	public static final String RER_ORDRE_KEY = "rerOrdre";
	public static final String TRL_ORDRE_KEY = "trlOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RER_CONTACT_COLKEY = "rer_contact";
	public static final String RER_DATE_CREATION_COLKEY = "rer_date_creation";
	public static final String RER_DATE_IMPRESSION_COLKEY = "rer_date_impression";
	public static final String RER_DELAI_PAIEMENT_COLKEY = "rer_delai_paiement";
	public static final String RER_ETAT_COLKEY = "rer_etat";
	public static final String RER_LIBELLE_COLKEY = "rer_libelle";
	public static final String RER_MONT_COLKEY = "rer_MONT";
	public static final String RER_NUMERO_COLKEY = "rer_numero";
	public static final String RER_PS_COLKEY = "rer_ps";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String RER_ORDRE_COLKEY = "rer_ORDRE";
	public static final String TRL_ORDRE_COLKEY = "TRL_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String RECETTE_KEY = "recette";
	public static final String TYPE_RELANCE_KEY = "typeRelance";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String rerContact() {
    return (String) storedValueForKey(RER_CONTACT_KEY);
  }

  public void setRerContact(String value) {
    takeStoredValueForKey(value, RER_CONTACT_KEY);
  }

  public NSTimestamp rerDateCreation() {
    return (NSTimestamp) storedValueForKey(RER_DATE_CREATION_KEY);
  }

  public void setRerDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, RER_DATE_CREATION_KEY);
  }

  public NSTimestamp rerDateImpression() {
    return (NSTimestamp) storedValueForKey(RER_DATE_IMPRESSION_KEY);
  }

  public void setRerDateImpression(NSTimestamp value) {
    takeStoredValueForKey(value, RER_DATE_IMPRESSION_KEY);
  }

  public Integer rerDelaiPaiement() {
    return (Integer) storedValueForKey(RER_DELAI_PAIEMENT_KEY);
  }

  public void setRerDelaiPaiement(Integer value) {
    takeStoredValueForKey(value, RER_DELAI_PAIEMENT_KEY);
  }

  public String rerEtat() {
    return (String) storedValueForKey(RER_ETAT_KEY);
  }

  public void setRerEtat(String value) {
    takeStoredValueForKey(value, RER_ETAT_KEY);
  }

  public String rerLibelle() {
    return (String) storedValueForKey(RER_LIBELLE_KEY);
  }

  public void setRerLibelle(String value) {
    takeStoredValueForKey(value, RER_LIBELLE_KEY);
  }

  public java.math.BigDecimal rerMont() {
    return (java.math.BigDecimal) storedValueForKey(RER_MONT_KEY);
  }

  public void setRerMont(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RER_MONT_KEY);
  }

  public Integer rerNumero() {
    return (Integer) storedValueForKey(RER_NUMERO_KEY);
  }

  public void setRerNumero(Integer value) {
    takeStoredValueForKey(value, RER_NUMERO_KEY);
  }

  public String rerPs() {
    return (String) storedValueForKey(RER_PS_KEY);
  }

  public void setRerPs(String value) {
    takeStoredValueForKey(value, RER_PS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORecette recette() {
    return (org.cocktail.maracuja.server.metier.EORecette)storedValueForKey(RECETTE_KEY);
  }

  public void setRecetteRelationship(org.cocktail.maracuja.server.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORecette oldValue = recette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeRelance typeRelance() {
    return (org.cocktail.maracuja.server.metier.EOTypeRelance)storedValueForKey(TYPE_RELANCE_KEY);
  }

  public void setTypeRelanceRelationship(org.cocktail.maracuja.server.metier.EOTypeRelance value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeRelance oldValue = typeRelance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RELANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RELANCE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EORecetteRelance avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORecetteRelance createEORecetteRelance(EOEditingContext editingContext, NSTimestamp rerDateCreation
, Integer rerDelaiPaiement
, String rerEtat
, String rerLibelle
, java.math.BigDecimal rerMont
, Integer rerNumero
, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EORecette recette, org.cocktail.maracuja.server.metier.EOTypeRelance typeRelance, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EORecetteRelance eo = (EORecetteRelance) createAndInsertInstance(editingContext, _EORecetteRelance.ENTITY_NAME);    
		eo.setRerDateCreation(rerDateCreation);
		eo.setRerDelaiPaiement(rerDelaiPaiement);
		eo.setRerEtat(rerEtat);
		eo.setRerLibelle(rerLibelle);
		eo.setRerMont(rerMont);
		eo.setRerNumero(rerNumero);
    eo.setExerciceRelationship(exercice);
    eo.setRecetteRelationship(recette);
    eo.setTypeRelanceRelationship(typeRelance);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EORecetteRelance localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecetteRelance)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORecetteRelance creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORecetteRelance creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORecetteRelance object = (EORecetteRelance)createAndInsertInstance(editingContext, _EORecetteRelance.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORecetteRelance localInstanceIn(EOEditingContext editingContext, EORecetteRelance eo) {
    EORecetteRelance localInstance = (eo == null) ? null : (EORecetteRelance)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORecetteRelance#localInstanceIn a la place.
   */
	public static EORecetteRelance localInstanceOf(EOEditingContext editingContext, EORecetteRelance eo) {
		return EORecetteRelance.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORecetteRelance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORecetteRelance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecetteRelance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecetteRelance)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecetteRelance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecetteRelance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecetteRelance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecetteRelance)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORecetteRelance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecetteRelance eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecetteRelance ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecetteRelance fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
