/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeVirement.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeVirement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TypeVirement";
	public static final String ENTITY_TABLE_NAME = "maracuja.Type_Virement";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tviOrdre";

	public static final String MOD_DOM_KEY = "modDom";
	public static final String TVI_FORMAT_KEY = "tviFormat";
	public static final String TVI_LIBELLE_KEY = "tviLibelle";
	public static final String TVI_VALIDITE_KEY = "tviValidite";

// Attributs non visibles
	public static final String TVI_ORDRE_KEY = "tviOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_DOM_COLKEY = "mod_dom";
	public static final String TVI_FORMAT_COLKEY = "tvi_format";
	public static final String TVI_LIBELLE_COLKEY = "TVI_LIBELLE";
	public static final String TVI_VALIDITE_COLKEY = "TVI_VALIDITE";

	public static final String TVI_ORDRE_COLKEY = "TVI_ORDRE";


	// Relationships
	public static final String TO_VIREMENT_PARAM_BDFS_KEY = "toVirementParamBdfs";
	public static final String TO_VIREMENT_PARAM_SEPAS_KEY = "toVirementParamSepas";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String modDom() {
    return (String) storedValueForKey(MOD_DOM_KEY);
  }

  public void setModDom(String value) {
    takeStoredValueForKey(value, MOD_DOM_KEY);
  }

  public String tviFormat() {
    return (String) storedValueForKey(TVI_FORMAT_KEY);
  }

  public void setTviFormat(String value) {
    takeStoredValueForKey(value, TVI_FORMAT_KEY);
  }

  public String tviLibelle() {
    return (String) storedValueForKey(TVI_LIBELLE_KEY);
  }

  public void setTviLibelle(String value) {
    takeStoredValueForKey(value, TVI_LIBELLE_KEY);
  }

  public String tviValidite() {
    return (String) storedValueForKey(TVI_VALIDITE_KEY);
  }

  public void setTviValidite(String value) {
    takeStoredValueForKey(value, TVI_VALIDITE_KEY);
  }

  public NSArray toVirementParamBdfs() {
    return (NSArray)storedValueForKey(TO_VIREMENT_PARAM_BDFS_KEY);
  }

  public NSArray toVirementParamBdfs(EOQualifier qualifier) {
    return toVirementParamBdfs(qualifier, null, false);
  }

  public NSArray toVirementParamBdfs(EOQualifier qualifier, boolean fetch) {
    return toVirementParamBdfs(qualifier, null, fetch);
  }

  public NSArray toVirementParamBdfs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOVirementParamBdf.TO_TYPE_VIREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOVirementParamBdf.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVirementParamBdfs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVirementParamBdfsRelationship(org.cocktail.maracuja.server.metier.EOVirementParamBdf object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_BDFS_KEY);
  }

  public void removeFromToVirementParamBdfsRelationship(org.cocktail.maracuja.server.metier.EOVirementParamBdf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_BDFS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOVirementParamBdf createToVirementParamBdfsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VirementParamBdf");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_VIREMENT_PARAM_BDFS_KEY);
    return (org.cocktail.maracuja.server.metier.EOVirementParamBdf) eo;
  }

  public void deleteToVirementParamBdfsRelationship(org.cocktail.maracuja.server.metier.EOVirementParamBdf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_BDFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToVirementParamBdfsRelationships() {
    Enumeration objects = toVirementParamBdfs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVirementParamBdfsRelationship((org.cocktail.maracuja.server.metier.EOVirementParamBdf)objects.nextElement());
    }
  }

  public NSArray toVirementParamSepas() {
    return (NSArray)storedValueForKey(TO_VIREMENT_PARAM_SEPAS_KEY);
  }

  public NSArray toVirementParamSepas(EOQualifier qualifier) {
    return toVirementParamSepas(qualifier, null, false);
  }

  public NSArray toVirementParamSepas(EOQualifier qualifier, boolean fetch) {
    return toVirementParamSepas(qualifier, null, fetch);
  }

  public NSArray toVirementParamSepas(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOVirementParamSepa.TO_TYPE_VIREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOVirementParamSepa.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVirementParamSepas();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVirementParamSepasRelationship(org.cocktail.maracuja.server.metier.EOVirementParamSepa object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
  }

  public void removeFromToVirementParamSepasRelationship(org.cocktail.maracuja.server.metier.EOVirementParamSepa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOVirementParamSepa createToVirementParamSepasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VirementParamSepa");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_VIREMENT_PARAM_SEPAS_KEY);
    return (org.cocktail.maracuja.server.metier.EOVirementParamSepa) eo;
  }

  public void deleteToVirementParamSepasRelationship(org.cocktail.maracuja.server.metier.EOVirementParamSepa object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_VIREMENT_PARAM_SEPAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToVirementParamSepasRelationships() {
    Enumeration objects = toVirementParamSepas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVirementParamSepasRelationship((org.cocktail.maracuja.server.metier.EOVirementParamSepa)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTypeVirement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeVirement createEOTypeVirement(EOEditingContext editingContext, String modDom
, String tviFormat
, String tviLibelle
, String tviValidite
			) {
    EOTypeVirement eo = (EOTypeVirement) createAndInsertInstance(editingContext, _EOTypeVirement.ENTITY_NAME);    
		eo.setModDom(modDom);
		eo.setTviFormat(tviFormat);
		eo.setTviLibelle(tviLibelle);
		eo.setTviValidite(tviValidite);
    return eo;
  }

  
	  public EOTypeVirement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeVirement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeVirement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeVirement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeVirement object = (EOTypeVirement)createAndInsertInstance(editingContext, _EOTypeVirement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeVirement localInstanceIn(EOEditingContext editingContext, EOTypeVirement eo) {
    EOTypeVirement localInstance = (eo == null) ? null : (EOTypeVirement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeVirement#localInstanceIn a la place.
   */
	public static EOTypeVirement localInstanceOf(EOEditingContext editingContext, EOTypeVirement eo) {
		return EOTypeVirement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeVirement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeVirement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeVirement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeVirement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeVirement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeVirement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeVirement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeVirement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeVirement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeVirement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeVirement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeVirement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
