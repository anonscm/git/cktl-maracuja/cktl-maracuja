/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrelevement.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrelevement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Prelevement";
	public static final String ENTITY_TABLE_NAME = "maracuja.Prelevement";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prelevOrdre";

	public static final String PRELEV_COMMENTAIRE_KEY = "prelevCommentaire";
	public static final String PRELEV_DATE_MODIF_KEY = "prelevDateModif";
	public static final String PRELEV_DATE_PRELEVEMENT_KEY = "prelevDatePrelevement";
	public static final String PRELEV_DATE_SAISIE_KEY = "prelevDateSaisie";
	public static final String PRELEV_ETAT_MARACUJA_KEY = "prelevEtatMaracuja";
	public static final String PRELEV_INDEX_KEY = "prelevIndex";
	public static final String PRELEV_MONTANT_KEY = "prelevMontant";

// Attributs non visibles
	public static final String ECHEANCIER_ORDRE_KEY = "echeancierOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String PRELEV_ORDRE_KEY = "prelevOrdre";
	public static final String RECO_ORDRE_KEY = "reco_ordre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";

//Colonnes dans la base de donnees
	public static final String PRELEV_COMMENTAIRE_COLKEY = "prel_COMMENTAIRE";
	public static final String PRELEV_DATE_MODIF_COLKEY = "prel_DATE_MODIF";
	public static final String PRELEV_DATE_PRELEVEMENT_COLKEY = "prel_DATE_PRELEVEMENT";
	public static final String PRELEV_DATE_SAISIE_COLKEY = "prel_prelev_Date_saisie";
	public static final String PRELEV_ETAT_MARACUJA_COLKEY = "PREL_ETAT_MARACUJA";
	public static final String PRELEV_INDEX_COLKEY = "prel_NUMERO_INDEX";
	public static final String PRELEV_MONTANT_COLKEY = "prel_prelev_Montant";

	public static final String ECHEANCIER_ORDRE_COLKEY = "ECHE_ECHEANCIER_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String PRELEV_ORDRE_COLKEY = "PREL_PRELEV_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "reco_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";


	// Relationships
	public static final String ECHEANCIER_KEY = "echeancier";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String PRELEVEMENT_DETAIL_ECRS_KEY = "prelevementDetailEcrs";
	public static final String RECOUVREMENT_KEY = "recouvrement";
	public static final String RIB_KEY = "rib";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String prelevCommentaire() {
    return (String) storedValueForKey(PRELEV_COMMENTAIRE_KEY);
  }

  public void setPrelevCommentaire(String value) {
    takeStoredValueForKey(value, PRELEV_COMMENTAIRE_KEY);
  }

  public NSTimestamp prelevDateModif() {
    return (NSTimestamp) storedValueForKey(PRELEV_DATE_MODIF_KEY);
  }

  public void setPrelevDateModif(NSTimestamp value) {
    takeStoredValueForKey(value, PRELEV_DATE_MODIF_KEY);
  }

  public NSTimestamp prelevDatePrelevement() {
    return (NSTimestamp) storedValueForKey(PRELEV_DATE_PRELEVEMENT_KEY);
  }

  public void setPrelevDatePrelevement(NSTimestamp value) {
    takeStoredValueForKey(value, PRELEV_DATE_PRELEVEMENT_KEY);
  }

  public NSTimestamp prelevDateSaisie() {
    return (NSTimestamp) storedValueForKey(PRELEV_DATE_SAISIE_KEY);
  }

  public void setPrelevDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, PRELEV_DATE_SAISIE_KEY);
  }

  public String prelevEtatMaracuja() {
    return (String) storedValueForKey(PRELEV_ETAT_MARACUJA_KEY);
  }

  public void setPrelevEtatMaracuja(String value) {
    takeStoredValueForKey(value, PRELEV_ETAT_MARACUJA_KEY);
  }

  public Integer prelevIndex() {
    return (Integer) storedValueForKey(PRELEV_INDEX_KEY);
  }

  public void setPrelevIndex(Integer value) {
    takeStoredValueForKey(value, PRELEV_INDEX_KEY);
  }

  public java.math.BigDecimal prelevMontant() {
    return (java.math.BigDecimal) storedValueForKey(PRELEV_MONTANT_KEY);
  }

  public void setPrelevMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRELEV_MONTANT_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOEcheancier echeancier() {
    return (org.cocktail.maracuja.server.metier.EOEcheancier)storedValueForKey(ECHEANCIER_KEY);
  }

  public void setEcheancierRelationship(org.cocktail.maracuja.server.metier.EOEcheancier value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOEcheancier oldValue = echeancier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECHEANCIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECHEANCIER_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.server.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORecouvrement recouvrement() {
    return (org.cocktail.maracuja.server.metier.EORecouvrement)storedValueForKey(RECOUVREMENT_KEY);
  }

  public void setRecouvrementRelationship(org.cocktail.maracuja.server.metier.EORecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORecouvrement oldValue = recouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORib rib() {
    return (org.cocktail.maracuja.server.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.server.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public NSArray prelevementDetailEcrs() {
    return (NSArray)storedValueForKey(PRELEVEMENT_DETAIL_ECRS_KEY);
  }

  public NSArray prelevementDetailEcrs(EOQualifier qualifier) {
    return prelevementDetailEcrs(qualifier, null, false);
  }

  public NSArray prelevementDetailEcrs(EOQualifier qualifier, boolean fetch) {
    return prelevementDetailEcrs(qualifier, null, fetch);
  }

  public NSArray prelevementDetailEcrs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr.PRELEVEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prelevementDetailEcrs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrelevementDetailEcrsRelationship(org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRELEVEMENT_DETAIL_ECRS_KEY);
  }

  public void removeFromPrelevementDetailEcrsRelationship(org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENT_DETAIL_ECRS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr createPrelevementDetailEcrsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PrelevementDetailEcr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRELEVEMENT_DETAIL_ECRS_KEY);
    return (org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr) eo;
  }

  public void deletePrelevementDetailEcrsRelationship(org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENT_DETAIL_ECRS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrelevementDetailEcrsRelationships() {
    Enumeration objects = prelevementDetailEcrs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrelevementDetailEcrsRelationship((org.cocktail.maracuja.server.metier.EOPrelevementDetailEcr)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPrelevement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrelevement createEOPrelevement(EOEditingContext editingContext, NSTimestamp prelevDatePrelevement
, NSTimestamp prelevDateSaisie
, String prelevEtatMaracuja
, Integer prelevIndex
, java.math.BigDecimal prelevMontant
, org.cocktail.maracuja.server.metier.EOEcheancier echeancier			) {
    EOPrelevement eo = (EOPrelevement) createAndInsertInstance(editingContext, _EOPrelevement.ENTITY_NAME);    
		eo.setPrelevDatePrelevement(prelevDatePrelevement);
		eo.setPrelevDateSaisie(prelevDateSaisie);
		eo.setPrelevEtatMaracuja(prelevEtatMaracuja);
		eo.setPrelevIndex(prelevIndex);
		eo.setPrelevMontant(prelevMontant);
    eo.setEcheancierRelationship(echeancier);
    return eo;
  }

  
	  public EOPrelevement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrelevement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrelevement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrelevement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPrelevement object = (EOPrelevement)createAndInsertInstance(editingContext, _EOPrelevement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrelevement localInstanceIn(EOEditingContext editingContext, EOPrelevement eo) {
    EOPrelevement localInstance = (eo == null) ? null : (EOPrelevement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrelevement#localInstanceIn a la place.
   */
	public static EOPrelevement localInstanceOf(EOEditingContext editingContext, EOPrelevement eo) {
		return EOPrelevement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrelevement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrelevement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrelevement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrelevement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrelevement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrelevement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrelevement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrelevement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrelevement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrelevement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrelevement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrelevement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
