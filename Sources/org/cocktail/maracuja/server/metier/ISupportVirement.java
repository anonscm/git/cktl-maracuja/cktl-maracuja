package org.cocktail.maracuja.server.metier;

import java.math.BigDecimal;

/**
 * Interface pour identifier les éléments qui peuvent donner lieu à un virement.
 * 
 * @author rprin
 */
public interface ISupportVirement {
	String REFERENCE_FOURNISSEUR_NUMERO_KEY = "virementReferenceFournisseurNumero";

	public BigDecimal montantAPayer();

	public String virementReferenceFournisseur();

	public EORib rib();

	public EOPaiement paiement();

	public String virementReferenceInterne();

	public String virementReferenceFournisseurNumero();

}
