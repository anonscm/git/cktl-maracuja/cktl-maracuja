/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeRelance.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeRelance extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TypeRelance";
	public static final String ENTITY_TABLE_NAME = "maracuja.Type_Relance";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "trlOrdre";

	public static final String TRL_CONTACT_SUIVI_KEY = "trlContactSuivi";
	public static final String TRL_DELAI_PAIEMENT_KEY = "trlDelaiPaiement";
	public static final String TRL_ETAT_KEY = "trlEtat";
	public static final String TRL_LIBELLE1_KEY = "trlLibelle1";
	public static final String TRL_LIBELLE2_KEY = "trlLibelle2";
	public static final String TRL_LIBELLE3_KEY = "trlLibelle3";
	public static final String TRL_NIVEAU_KEY = "trlNiveau";
	public static final String TRL_NOM_KEY = "trlNom";
	public static final String TRL_REPORT_ID_KEY = "trlReportId";
	public static final String TRL_SIGN_KEY = "trlSign";

// Attributs non visibles
	public static final String TRL_ORDRE_KEY = "trlOrdre";

//Colonnes dans la base de donnees
	public static final String TRL_CONTACT_SUIVI_COLKEY = "TRL_CONTACT_SUIVI";
	public static final String TRL_DELAI_PAIEMENT_COLKEY = "trl_delai_paiement";
	public static final String TRL_ETAT_COLKEY = "trl_Etat";
	public static final String TRL_LIBELLE1_COLKEY = "trl_libelle1";
	public static final String TRL_LIBELLE2_COLKEY = "trl_libelle2";
	public static final String TRL_LIBELLE3_COLKEY = "trl_libelle3";
	public static final String TRL_NIVEAU_COLKEY = "trl_niveau";
	public static final String TRL_NOM_COLKEY = "trl_Nom";
	public static final String TRL_REPORT_ID_COLKEY = "TRL_REPORT_ID";
	public static final String TRL_SIGN_COLKEY = "trl_sign";

	public static final String TRL_ORDRE_COLKEY = "TRL_ORDRE";


	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String trlContactSuivi() {
    return (String) storedValueForKey(TRL_CONTACT_SUIVI_KEY);
  }

  public void setTrlContactSuivi(String value) {
    takeStoredValueForKey(value, TRL_CONTACT_SUIVI_KEY);
  }

  public Integer trlDelaiPaiement() {
    return (Integer) storedValueForKey(TRL_DELAI_PAIEMENT_KEY);
  }

  public void setTrlDelaiPaiement(Integer value) {
    takeStoredValueForKey(value, TRL_DELAI_PAIEMENT_KEY);
  }

  public String trlEtat() {
    return (String) storedValueForKey(TRL_ETAT_KEY);
  }

  public void setTrlEtat(String value) {
    takeStoredValueForKey(value, TRL_ETAT_KEY);
  }

  public String trlLibelle1() {
    return (String) storedValueForKey(TRL_LIBELLE1_KEY);
  }

  public void setTrlLibelle1(String value) {
    takeStoredValueForKey(value, TRL_LIBELLE1_KEY);
  }

  public String trlLibelle2() {
    return (String) storedValueForKey(TRL_LIBELLE2_KEY);
  }

  public void setTrlLibelle2(String value) {
    takeStoredValueForKey(value, TRL_LIBELLE2_KEY);
  }

  public String trlLibelle3() {
    return (String) storedValueForKey(TRL_LIBELLE3_KEY);
  }

  public void setTrlLibelle3(String value) {
    takeStoredValueForKey(value, TRL_LIBELLE3_KEY);
  }

  public Integer trlNiveau() {
    return (Integer) storedValueForKey(TRL_NIVEAU_KEY);
  }

  public void setTrlNiveau(Integer value) {
    takeStoredValueForKey(value, TRL_NIVEAU_KEY);
  }

  public String trlNom() {
    return (String) storedValueForKey(TRL_NOM_KEY);
  }

  public void setTrlNom(String value) {
    takeStoredValueForKey(value, TRL_NOM_KEY);
  }

  public String trlReportId() {
    return (String) storedValueForKey(TRL_REPORT_ID_KEY);
  }

  public void setTrlReportId(String value) {
    takeStoredValueForKey(value, TRL_REPORT_ID_KEY);
  }

  public String trlSign() {
    return (String) storedValueForKey(TRL_SIGN_KEY);
  }

  public void setTrlSign(String value) {
    takeStoredValueForKey(value, TRL_SIGN_KEY);
  }


/**
 * Créer une instance de EOTypeRelance avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeRelance createEOTypeRelance(EOEditingContext editingContext, Integer trlDelaiPaiement
, String trlEtat
, String trlLibelle1
, Integer trlNiveau
, String trlNom
			) {
    EOTypeRelance eo = (EOTypeRelance) createAndInsertInstance(editingContext, _EOTypeRelance.ENTITY_NAME);    
		eo.setTrlDelaiPaiement(trlDelaiPaiement);
		eo.setTrlEtat(trlEtat);
		eo.setTrlLibelle1(trlLibelle1);
		eo.setTrlNiveau(trlNiveau);
		eo.setTrlNom(trlNom);
    return eo;
  }

  
	  public EOTypeRelance localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeRelance)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeRelance creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeRelance creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeRelance object = (EOTypeRelance)createAndInsertInstance(editingContext, _EOTypeRelance.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeRelance localInstanceIn(EOEditingContext editingContext, EOTypeRelance eo) {
    EOTypeRelance localInstance = (eo == null) ? null : (EOTypeRelance)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeRelance#localInstanceIn a la place.
   */
	public static EOTypeRelance localInstanceOf(EOEditingContext editingContext, EOTypeRelance eo) {
		return EOTypeRelance.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeRelance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeRelance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeRelance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeRelance)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeRelance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeRelance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeRelance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeRelance)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeRelance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeRelance eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeRelance ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeRelance fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
