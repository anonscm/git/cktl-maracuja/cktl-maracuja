/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EODepense.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EODepense extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Depense";
	public static final String ENTITY_TABLE_NAME = "maracuja.Depense";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String DEP_ADRESSE_KEY = "depAdresse";
	public static final String DEP_DATE_COMPTA_KEY = "depDateCompta";
	public static final String DEP_DATE_FOURNIS_KEY = "depDateFournis";
	public static final String DEP_DATE_RECEPTION_KEY = "depDateReception";
	public static final String DEP_DATE_SERVICE_KEY = "depDateService";
	public static final String DEP_ETAT_KEY = "depEtat";
	public static final String DEP_FOURNISSEUR_KEY = "depFournisseur";
	public static final String DEP_HT_KEY = "depHt";
	public static final String DEP_LIGNE_BUDGETAIRE_KEY = "depLigneBudgetaire";
	public static final String DEP_LOT_KEY = "depLot";
	public static final String DEP_MARCHES_KEY = "depMarches";
	public static final String DEP_MONTANT_DISQUETTE_KEY = "depMontantDisquette";
	public static final String DEP_NOMENCLATURE_KEY = "depNomenclature";
	public static final String DEP_NUMERO_KEY = "depNumero";
	public static final String DEP_ORDRE_KEY = "depOrdre";
	public static final String DEP_REJET_KEY = "depRejet";
	public static final String DEP_SUPPRESSION_KEY = "depSuppression";
	public static final String DEP_TTC_KEY = "depTtc";
	public static final String DEP_TVA_KEY = "depTva";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DEP_RIB_KEY = "depRib";
	public static final String ECD_ORDRE_EMA_KEY = "ecdOrdreEma";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_ID_KEY = "manId";
	public static final String MAN_ORDRE_KEY = "manOrdre";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PCO_ORDRE_KEY = "pcoOrdre";
	public static final String TCD_ORDRE_KEY = "tcd_ordre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_ADRESSE_COLKEY = "dep_Adresse";
	public static final String DEP_DATE_COMPTA_COLKEY = "dep_Date_Compta";
	public static final String DEP_DATE_FOURNIS_COLKEY = "dep_Date_fournis";
	public static final String DEP_DATE_RECEPTION_COLKEY = "dep_Date_Reception";
	public static final String DEP_DATE_SERVICE_COLKEY = "dep_Date_Service";
	public static final String DEP_ETAT_COLKEY = "dep_Etat";
	public static final String DEP_FOURNISSEUR_COLKEY = "dep_Fournisseur";
	public static final String DEP_HT_COLKEY = "dep_Ht";
	public static final String DEP_LIGNE_BUDGETAIRE_COLKEY = "dep_Ligne_Budgetaire";
	public static final String DEP_LOT_COLKEY = "dep_Lot";
	public static final String DEP_MARCHES_COLKEY = "dep_Marches";
	public static final String DEP_MONTANT_DISQUETTE_COLKEY = "dep_Montant_Disquette";
	public static final String DEP_NOMENCLATURE_COLKEY = "dep_Nomenclature";
	public static final String DEP_NUMERO_COLKEY = "dep_Numero";
	public static final String DEP_ORDRE_COLKEY = "dep_ORDRE";
	public static final String DEP_REJET_COLKEY = "dep_Rejet";
	public static final String DEP_SUPPRESSION_COLKEY = "dep_Suppression";
	public static final String DEP_TTC_COLKEY = "dep_Ttc";
	public static final String DEP_TVA_COLKEY = "dep_Tva";

	public static final String DEP_ID_COLKEY = "dep_id";
	public static final String DEP_RIB_COLKEY = "dep_Rib";
	public static final String ECD_ORDRE_EMA_COLKEY = "ecd_ordre_ema";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String MAN_ORDRE_COLKEY = "MAN_ORDRE";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String PCO_ORDRE_COLKEY = "PCO_ORDRE";
	public static final String TCD_ORDRE_COLKEY = "tcd_ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String ECRITURE_DETAIL_EMA_KEY = "ecritureDetailEma";
	public static final String EXERCICE_KEY = "exercice";
	public static final String GESTION_KEY = "gestion";
	public static final String MANDAT_KEY = "mandat";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String RETENUES_KEY = "retenues";
	public static final String RIB_KEY = "rib";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String depAdresse() {
    return (String) storedValueForKey(DEP_ADRESSE_KEY);
  }

  public void setDepAdresse(String value) {
    takeStoredValueForKey(value, DEP_ADRESSE_KEY);
  }

  public NSTimestamp depDateCompta() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_COMPTA_KEY);
  }

  public void setDepDateCompta(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_COMPTA_KEY);
  }

  public NSTimestamp depDateFournis() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_FOURNIS_KEY);
  }

  public void setDepDateFournis(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_FOURNIS_KEY);
  }

  public NSTimestamp depDateReception() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_RECEPTION_KEY);
  }

  public void setDepDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp depDateService() {
    return (NSTimestamp) storedValueForKey(DEP_DATE_SERVICE_KEY);
  }

  public void setDepDateService(NSTimestamp value) {
    takeStoredValueForKey(value, DEP_DATE_SERVICE_KEY);
  }

  public String depEtat() {
    return (String) storedValueForKey(DEP_ETAT_KEY);
  }

  public void setDepEtat(String value) {
    takeStoredValueForKey(value, DEP_ETAT_KEY);
  }

  public String depFournisseur() {
    return (String) storedValueForKey(DEP_FOURNISSEUR_KEY);
  }

  public void setDepFournisseur(String value) {
    takeStoredValueForKey(value, DEP_FOURNISSEUR_KEY);
  }

  public java.math.BigDecimal depHt() {
    return (java.math.BigDecimal) storedValueForKey(DEP_HT_KEY);
  }

  public void setDepHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_HT_KEY);
  }

  public String depLigneBudgetaire() {
    return (String) storedValueForKey(DEP_LIGNE_BUDGETAIRE_KEY);
  }

  public void setDepLigneBudgetaire(String value) {
    takeStoredValueForKey(value, DEP_LIGNE_BUDGETAIRE_KEY);
  }

  public String depLot() {
    return (String) storedValueForKey(DEP_LOT_KEY);
  }

  public void setDepLot(String value) {
    takeStoredValueForKey(value, DEP_LOT_KEY);
  }

  public String depMarches() {
    return (String) storedValueForKey(DEP_MARCHES_KEY);
  }

  public void setDepMarches(String value) {
    takeStoredValueForKey(value, DEP_MARCHES_KEY);
  }

  public java.math.BigDecimal depMontantDisquette() {
    return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_DISQUETTE_KEY);
  }

  public void setDepMontantDisquette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_MONTANT_DISQUETTE_KEY);
  }

  public String depNomenclature() {
    return (String) storedValueForKey(DEP_NOMENCLATURE_KEY);
  }

  public void setDepNomenclature(String value) {
    takeStoredValueForKey(value, DEP_NOMENCLATURE_KEY);
  }

  public String depNumero() {
    return (String) storedValueForKey(DEP_NUMERO_KEY);
  }

  public void setDepNumero(String value) {
    takeStoredValueForKey(value, DEP_NUMERO_KEY);
  }

  public Integer depOrdre() {
    return (Integer) storedValueForKey(DEP_ORDRE_KEY);
  }

  public void setDepOrdre(Integer value) {
    takeStoredValueForKey(value, DEP_ORDRE_KEY);
  }

  public String depRejet() {
    return (String) storedValueForKey(DEP_REJET_KEY);
  }

  public void setDepRejet(String value) {
    takeStoredValueForKey(value, DEP_REJET_KEY);
  }

  public String depSuppression() {
    return (String) storedValueForKey(DEP_SUPPRESSION_KEY);
  }

  public void setDepSuppression(String value) {
    takeStoredValueForKey(value, DEP_SUPPRESSION_KEY);
  }

  public java.math.BigDecimal depTtc() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TTC_KEY);
  }

  public void setDepTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TTC_KEY);
  }

  public java.math.BigDecimal depTva() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TVA_KEY);
  }

  public void setDepTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TVA_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOEcritureDetail ecritureDetailEma() {
    return (org.cocktail.maracuja.server.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_EMA_KEY);
  }

  public void setEcritureDetailEmaRelationship(org.cocktail.maracuja.server.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOEcritureDetail oldValue = ecritureDetailEma();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_EMA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_EMA_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOMandat mandat() {
    return (org.cocktail.maracuja.server.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.maracuja.server.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOModePaiement modePaiement() {
    return (org.cocktail.maracuja.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.maracuja.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrgan organ() {
    return (org.cocktail.maracuja.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.maracuja.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORib rib() {
    return (org.cocktail.maracuja.server.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.server.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.maracuja.server.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.maracuja.server.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray retenues() {
    return (NSArray)storedValueForKey(RETENUES_KEY);
  }

  public NSArray retenues(EOQualifier qualifier) {
    return retenues(qualifier, null, false);
  }

  public NSArray retenues(EOQualifier qualifier, boolean fetch) {
    return retenues(qualifier, null, fetch);
  }

  public NSArray retenues(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EORetenue.DEPENSE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EORetenue.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = retenues();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRetenuesRelationship(org.cocktail.maracuja.server.metier.EORetenue object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RETENUES_KEY);
  }

  public void removeFromRetenuesRelationship(org.cocktail.maracuja.server.metier.EORetenue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RETENUES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EORetenue createRetenuesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Retenue");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RETENUES_KEY);
    return (org.cocktail.maracuja.server.metier.EORetenue) eo;
  }

  public void deleteRetenuesRelationship(org.cocktail.maracuja.server.metier.EORetenue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RETENUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRetenuesRelationships() {
    Enumeration objects = retenues().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRetenuesRelationship((org.cocktail.maracuja.server.metier.EORetenue)objects.nextElement());
    }
  }


/**
 * Créer une instance de EODepense avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EODepense createEODepense(EOEditingContext editingContext, String depEtat
, String depFournisseur
, java.math.BigDecimal depHt
, String depLigneBudgetaire
, java.math.BigDecimal depMontantDisquette
, String depNumero
, Integer depOrdre
, String depSuppression
, java.math.BigDecimal depTtc
, java.math.BigDecimal depTva
, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOGestion gestion, org.cocktail.maracuja.server.metier.EOMandat mandat, org.cocktail.maracuja.server.metier.EOModePaiement modePaiement, org.cocktail.maracuja.server.metier.EOPlanComptable planComptable, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EODepense eo = (EODepense) createAndInsertInstance(editingContext, _EODepense.ENTITY_NAME);    
		eo.setDepEtat(depEtat);
		eo.setDepFournisseur(depFournisseur);
		eo.setDepHt(depHt);
		eo.setDepLigneBudgetaire(depLigneBudgetaire);
		eo.setDepMontantDisquette(depMontantDisquette);
		eo.setDepNumero(depNumero);
		eo.setDepOrdre(depOrdre);
		eo.setDepSuppression(depSuppression);
		eo.setDepTtc(depTtc);
		eo.setDepTva(depTva);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    eo.setMandatRelationship(mandat);
    eo.setModePaiementRelationship(modePaiement);
    eo.setPlanComptableRelationship(planComptable);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EODepense localInstanceIn(EOEditingContext editingContext) {
	  		return (EODepense)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepense creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EODepense creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EODepense object = (EODepense)createAndInsertInstance(editingContext, _EODepense.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EODepense localInstanceIn(EOEditingContext editingContext, EODepense eo) {
    EODepense localInstance = (eo == null) ? null : (EODepense)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EODepense#localInstanceIn a la place.
   */
	public static EODepense localInstanceOf(EOEditingContext editingContext, EODepense eo) {
		return EODepense.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EODepense fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EODepense fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EODepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EODepense)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EODepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EODepense fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EODepense eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EODepense)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EODepense fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EODepense eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EODepense ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EODepense fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
