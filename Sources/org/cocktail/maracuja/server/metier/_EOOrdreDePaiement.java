/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrdreDePaiement.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrdreDePaiement extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "OrdreDePaiement";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ordre_De_Paiement";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "odpOrdre";

	public static final String ODP_DATE_KEY = "odpDate";
	public static final String ODP_DATE_SAISIE_KEY = "odpDateSaisie";
	public static final String ODP_ETAT_KEY = "odpEtat";
	public static final String ODP_HT_KEY = "odpHt";
	public static final String ODP_LIBELLE_KEY = "odpLibelle";
	public static final String ODP_LIBELLE_FOURNISSEUR_KEY = "odpLibelleFournisseur";
	public static final String ODP_MONTANT_PAIEMENT_KEY = "odpMontantPaiement";
	public static final String ODP_NUMERO_KEY = "odpNumero";
	public static final String ODP_REFERENCE_PAIEMENT_KEY = "odpReferencePaiement";
	public static final String ODP_TTC_KEY = "odpTtc";
	public static final String ODP_TVA_KEY = "odpTva";
	public static final String ORG_ORDRE_KEY = "orgOrdre";

// Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String ODP_ORDRE_KEY = "odpOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ODP_DATE_COLKEY = "odp_date";
	public static final String ODP_DATE_SAISIE_COLKEY = "odp_date_saisie";
	public static final String ODP_ETAT_COLKEY = "odp_etat";
	public static final String ODP_HT_COLKEY = "odp_Ht";
	public static final String ODP_LIBELLE_COLKEY = "odp_Libelle";
	public static final String ODP_LIBELLE_FOURNISSEUR_COLKEY = "odp_Libelle_Fournisseur";
	public static final String ODP_MONTANT_PAIEMENT_COLKEY = "odp_montant_paiement";
	public static final String ODP_NUMERO_COLKEY = "ODP_NUMERO";
	public static final String ODP_REFERENCE_PAIEMENT_COLKEY = "odp_reference_paiement";
	public static final String ODP_TTC_COLKEY = "odp_Ttc";
	public static final String ODP_TVA_COLKEY = "odp_Tva";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";

	public static final String COM_ORDRE_COLKEY = "com_ordre";
	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String ODP_ORDRE_COLKEY = "odp_Ordre";
	public static final String ORI_ORDRE_COLKEY = "ori_Ordre";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String RIB_ORDRE_COLKEY = "rib_Ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String COMPTABILITE_KEY = "comptabilite";
	public static final String ECRITURE_KEY = "ecriture";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String ORDRE_DE_PAIEMENT_BROUILLARDS_KEY = "ordreDePaiementBrouillards";
	public static final String ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY = "ordrePaiementDetailEcritures";
	public static final String ORGAN_KEY = "organ";
	public static final String ORIGINE_KEY = "origine";
	public static final String PAIEMENT_KEY = "paiement";
	public static final String RIB_KEY = "rib";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp odpDate() {
    return (NSTimestamp) storedValueForKey(ODP_DATE_KEY);
  }

  public void setOdpDate(NSTimestamp value) {
    takeStoredValueForKey(value, ODP_DATE_KEY);
  }

  public NSTimestamp odpDateSaisie() {
    return (NSTimestamp) storedValueForKey(ODP_DATE_SAISIE_KEY);
  }

  public void setOdpDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ODP_DATE_SAISIE_KEY);
  }

  public String odpEtat() {
    return (String) storedValueForKey(ODP_ETAT_KEY);
  }

  public void setOdpEtat(String value) {
    takeStoredValueForKey(value, ODP_ETAT_KEY);
  }

  public java.math.BigDecimal odpHt() {
    return (java.math.BigDecimal) storedValueForKey(ODP_HT_KEY);
  }

  public void setOdpHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_HT_KEY);
  }

  public String odpLibelle() {
    return (String) storedValueForKey(ODP_LIBELLE_KEY);
  }

  public void setOdpLibelle(String value) {
    takeStoredValueForKey(value, ODP_LIBELLE_KEY);
  }

  public String odpLibelleFournisseur() {
    return (String) storedValueForKey(ODP_LIBELLE_FOURNISSEUR_KEY);
  }

  public void setOdpLibelleFournisseur(String value) {
    takeStoredValueForKey(value, ODP_LIBELLE_FOURNISSEUR_KEY);
  }

  public java.math.BigDecimal odpMontantPaiement() {
    return (java.math.BigDecimal) storedValueForKey(ODP_MONTANT_PAIEMENT_KEY);
  }

  public void setOdpMontantPaiement(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_MONTANT_PAIEMENT_KEY);
  }

  public Integer odpNumero() {
    return (Integer) storedValueForKey(ODP_NUMERO_KEY);
  }

  public void setOdpNumero(Integer value) {
    takeStoredValueForKey(value, ODP_NUMERO_KEY);
  }

  public String odpReferencePaiement() {
    return (String) storedValueForKey(ODP_REFERENCE_PAIEMENT_KEY);
  }

  public void setOdpReferencePaiement(String value) {
    takeStoredValueForKey(value, ODP_REFERENCE_PAIEMENT_KEY);
  }

  public java.math.BigDecimal odpTtc() {
    return (java.math.BigDecimal) storedValueForKey(ODP_TTC_KEY);
  }

  public void setOdpTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_TTC_KEY);
  }

  public java.math.BigDecimal odpTva() {
    return (java.math.BigDecimal) storedValueForKey(ODP_TVA_KEY);
  }

  public void setOdpTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODP_TVA_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOComptabilite comptabilite() {
    return (org.cocktail.maracuja.server.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
  }

  public void setComptabiliteRelationship(org.cocktail.maracuja.server.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOComptabilite oldValue = comptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOEcriture ecriture() {
    return (org.cocktail.maracuja.server.metier.EOEcriture)storedValueForKey(ECRITURE_KEY);
  }

  public void setEcritureRelationship(org.cocktail.maracuja.server.metier.EOEcriture value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOEcriture oldValue = ecriture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.server.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOModePaiement modePaiement() {
    return (org.cocktail.maracuja.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.maracuja.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrgan organ() {
    return (org.cocktail.maracuja.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.maracuja.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrigine origine() {
    return (org.cocktail.maracuja.server.metier.EOOrigine)storedValueForKey(ORIGINE_KEY);
  }

  public void setOrigineRelationship(org.cocktail.maracuja.server.metier.EOOrigine value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrigine oldValue = origine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPaiement paiement() {
    return (org.cocktail.maracuja.server.metier.EOPaiement)storedValueForKey(PAIEMENT_KEY);
  }

  public void setPaiementRelationship(org.cocktail.maracuja.server.metier.EOPaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPaiement oldValue = paiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORib rib() {
    return (org.cocktail.maracuja.server.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.server.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray ordreDePaiementBrouillards() {
    return (NSArray)storedValueForKey(ORDRE_DE_PAIEMENT_BROUILLARDS_KEY);
  }

  public NSArray ordreDePaiementBrouillards(EOQualifier qualifier) {
    return ordreDePaiementBrouillards(qualifier, null, false);
  }

  public NSArray ordreDePaiementBrouillards(EOQualifier qualifier, boolean fetch) {
    return ordreDePaiementBrouillards(qualifier, null, fetch);
  }

  public NSArray ordreDePaiementBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard.ORDRE_DE_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = ordreDePaiementBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrdreDePaiementBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORDRE_DE_PAIEMENT_BROUILLARDS_KEY);
  }

  public void removeFromOrdreDePaiementBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORDRE_DE_PAIEMENT_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard createOrdreDePaiementBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("OrdreDePaiementBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORDRE_DE_PAIEMENT_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard) eo;
  }

  public void deleteOrdreDePaiementBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORDRE_DE_PAIEMENT_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrdreDePaiementBrouillardsRelationships() {
    Enumeration objects = ordreDePaiementBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrdreDePaiementBrouillardsRelationship((org.cocktail.maracuja.server.metier.EOOrdreDePaiementBrouillard)objects.nextElement());
    }
  }

  public NSArray ordrePaiementDetailEcritures() {
    return (NSArray)storedValueForKey(ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY);
  }

  public NSArray ordrePaiementDetailEcritures(EOQualifier qualifier) {
    return ordrePaiementDetailEcritures(qualifier, null, false);
  }

  public NSArray ordrePaiementDetailEcritures(EOQualifier qualifier, boolean fetch) {
    return ordrePaiementDetailEcritures(qualifier, null, fetch);
  }

  public NSArray ordrePaiementDetailEcritures(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture.ORDRE_DE_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = ordrePaiementDetailEcritures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrdrePaiementDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY);
  }

  public void removeFromOrdrePaiementDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture createOrdrePaiementDetailEcrituresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("OrdrePaiementDetailEcriture");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY);
    return (org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture) eo;
  }

  public void deleteOrdrePaiementDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrdrePaiementDetailEcrituresRelationships() {
    Enumeration objects = ordrePaiementDetailEcritures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrdrePaiementDetailEcrituresRelationship((org.cocktail.maracuja.server.metier.EOOrdrePaiementDetailEcriture)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOOrdreDePaiement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOrdreDePaiement createEOOrdreDePaiement(EOEditingContext editingContext, NSTimestamp odpDate
, NSTimestamp odpDateSaisie
, String odpEtat
, java.math.BigDecimal odpHt
, java.math.BigDecimal odpMontantPaiement
, Integer odpNumero
, java.math.BigDecimal odpTtc
, java.math.BigDecimal odpTva
, org.cocktail.maracuja.server.metier.EOComptabilite comptabilite, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOFournisseur fournisseur, org.cocktail.maracuja.server.metier.EOModePaiement modePaiement, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EOOrdreDePaiement eo = (EOOrdreDePaiement) createAndInsertInstance(editingContext, _EOOrdreDePaiement.ENTITY_NAME);    
		eo.setOdpDate(odpDate);
		eo.setOdpDateSaisie(odpDateSaisie);
		eo.setOdpEtat(odpEtat);
		eo.setOdpHt(odpHt);
		eo.setOdpMontantPaiement(odpMontantPaiement);
		eo.setOdpNumero(odpNumero);
		eo.setOdpTtc(odpTtc);
		eo.setOdpTva(odpTva);
    eo.setComptabiliteRelationship(comptabilite);
    eo.setExerciceRelationship(exercice);
    eo.setFournisseurRelationship(fournisseur);
    eo.setModePaiementRelationship(modePaiement);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOOrdreDePaiement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrdreDePaiement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrdreDePaiement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrdreDePaiement creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOrdreDePaiement object = (EOOrdreDePaiement)createAndInsertInstance(editingContext, _EOOrdreDePaiement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOOrdreDePaiement localInstanceIn(EOEditingContext editingContext, EOOrdreDePaiement eo) {
    EOOrdreDePaiement localInstance = (eo == null) ? null : (EOOrdreDePaiement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOrdreDePaiement#localInstanceIn a la place.
   */
	public static EOOrdreDePaiement localInstanceOf(EOEditingContext editingContext, EOOrdreDePaiement eo) {
		return EOOrdreDePaiement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOrdreDePaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrdreDePaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrdreDePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrdreDePaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrdreDePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrdreDePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrdreDePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrdreDePaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrdreDePaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrdreDePaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrdreDePaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrdreDePaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
