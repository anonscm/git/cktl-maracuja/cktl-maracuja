/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAttribution.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOAttribution extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Attribution";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_ATTRIBUTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "attOrdre";

	public static final String ATT_ACCEPTEE_KEY = "attAcceptee";
	public static final String ATT_DATE_KEY = "attDate";
	public static final String ATT_DEBUT_KEY = "attDebut";
	public static final String ATT_FIN_KEY = "attFin";
	public static final String ATT_HT_KEY = "attHt";
	public static final String ATT_SUPPR_KEY = "attSuppr";
	public static final String ATT_TYPE_CONTROLE_KEY = "attTypeControle";
	public static final String ATT_VALIDE_KEY = "attValide";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TIT_ORDRE_KEY = "titOrdre";

// Attributs non visibles
	public static final String ATT_ORDRE_KEY = "attOrdre";
	public static final String LOT_ORDRE_KEY = "lotOrdre";

//Colonnes dans la base de donnees
	public static final String ATT_ACCEPTEE_COLKEY = "ATT_ACCEPTEE";
	public static final String ATT_DATE_COLKEY = "ATT_DATE";
	public static final String ATT_DEBUT_COLKEY = "ATT_DEBUT";
	public static final String ATT_FIN_COLKEY = "ATT_FIN";
	public static final String ATT_HT_COLKEY = "ATT_HT";
	public static final String ATT_SUPPR_COLKEY = "ATT_SUPPR";
	public static final String ATT_TYPE_CONTROLE_COLKEY = "ATT_TYPE_CONTROLE";
	public static final String ATT_VALIDE_COLKEY = "ATT_VALIDE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";

	public static final String ATT_ORDRE_COLKEY = "ATT_ORDRE";
	public static final String LOT_ORDRE_COLKEY = "LOT_ORDRE";


	// Relationships
	public static final String LOT_KEY = "lot";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String attAcceptee() {
    return (String) storedValueForKey(ATT_ACCEPTEE_KEY);
  }

  public void setAttAcceptee(String value) {
    takeStoredValueForKey(value, ATT_ACCEPTEE_KEY);
  }

  public NSTimestamp attDate() {
    return (NSTimestamp) storedValueForKey(ATT_DATE_KEY);
  }

  public void setAttDate(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_DATE_KEY);
  }

  public NSTimestamp attDebut() {
    return (NSTimestamp) storedValueForKey(ATT_DEBUT_KEY);
  }

  public void setAttDebut(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_DEBUT_KEY);
  }

  public NSTimestamp attFin() {
    return (NSTimestamp) storedValueForKey(ATT_FIN_KEY);
  }

  public void setAttFin(NSTimestamp value) {
    takeStoredValueForKey(value, ATT_FIN_KEY);
  }

  public java.math.BigDecimal attHt() {
    return (java.math.BigDecimal) storedValueForKey(ATT_HT_KEY);
  }

  public void setAttHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ATT_HT_KEY);
  }

  public String attSuppr() {
    return (String) storedValueForKey(ATT_SUPPR_KEY);
  }

  public void setAttSuppr(String value) {
    takeStoredValueForKey(value, ATT_SUPPR_KEY);
  }

  public String attTypeControle() {
    return (String) storedValueForKey(ATT_TYPE_CONTROLE_KEY);
  }

  public void setAttTypeControle(String value) {
    takeStoredValueForKey(value, ATT_TYPE_CONTROLE_KEY);
  }

  public String attValide() {
    return (String) storedValueForKey(ATT_VALIDE_KEY);
  }

  public void setAttValide(String value) {
    takeStoredValueForKey(value, ATT_VALIDE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public Integer titOrdre() {
    return (Integer) storedValueForKey(TIT_ORDRE_KEY);
  }

  public void setTitOrdre(Integer value) {
    takeStoredValueForKey(value, TIT_ORDRE_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOLot lot() {
    return (org.cocktail.maracuja.server.metier.EOLot)storedValueForKey(LOT_KEY);
  }

  public void setLotRelationship(org.cocktail.maracuja.server.metier.EOLot value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOLot oldValue = lot();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOT_KEY);
    }
  }
  

/**
 * Créer une instance de EOAttribution avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAttribution createEOAttribution(EOEditingContext editingContext, Integer fouOrdre
, org.cocktail.maracuja.server.metier.EOLot lot			) {
    EOAttribution eo = (EOAttribution) createAndInsertInstance(editingContext, _EOAttribution.ENTITY_NAME);    
		eo.setFouOrdre(fouOrdre);
    eo.setLotRelationship(lot);
    return eo;
  }

  
	  public EOAttribution localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAttribution)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAttribution creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAttribution creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAttribution object = (EOAttribution)createAndInsertInstance(editingContext, _EOAttribution.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAttribution localInstanceIn(EOEditingContext editingContext, EOAttribution eo) {
    EOAttribution localInstance = (eo == null) ? null : (EOAttribution)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAttribution#localInstanceIn a la place.
   */
	public static EOAttribution localInstanceOf(EOEditingContext editingContext, EOAttribution eo) {
		return EOAttribution.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAttribution fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAttribution fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAttribution fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAttribution eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAttribution)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAttribution fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAttribution eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAttribution ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAttribution fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
