/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitre.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTitre extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Titre";
	public static final String ENTITY_TABLE_NAME = "maracuja.Titre";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "titId";

	public static final String BOR_ORDRE_KEY = "borOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PREST_ID_KEY = "prestId";
	public static final String TIT_DATE_REMISE_KEY = "titDateRemise";
	public static final String TIT_DATE_VISA_PRINC_KEY = "titDateVisaPrinc";
	public static final String TIT_ETAT_KEY = "titEtat";
	public static final String TIT_ETAT_REMISE_KEY = "titEtatRemise";
	public static final String TIT_HT_KEY = "titHt";
	public static final String TIT_LIBELLE_KEY = "titLibelle";
	public static final String TIT_MOTIF_REJET_KEY = "titMotifRejet";
	public static final String TIT_NB_PIECE_KEY = "titNbPiece";
	public static final String TIT_NUMERO_KEY = "titNumero";
	public static final String TIT_NUMERO_REJET_KEY = "titNumeroRejet";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String TIT_ORIGINE_KEY_KEY = "titOrigineKey";
	public static final String TIT_ORIGINE_LIB_KEY = "titOrigineLib";
	public static final String TIT_TTC_KEY = "titTtc";
	public static final String TIT_TVA_KEY = "titTva";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TIT_ID_KEY = "titId";
	public static final String TOR_ORDRE_KEY = "torOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String BOR_ORDRE_COLKEY = "BOR_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String PREST_ID_COLKEY = "prest_id";
	public static final String TIT_DATE_REMISE_COLKEY = "tit_Date_Remise";
	public static final String TIT_DATE_VISA_PRINC_COLKEY = "tit_Date_Visa_Princ";
	public static final String TIT_ETAT_COLKEY = "tit_etat";
	public static final String TIT_ETAT_REMISE_COLKEY = "tit_Etat_Remise";
	public static final String TIT_HT_COLKEY = "tit_Ht";
	public static final String TIT_LIBELLE_COLKEY = "tit_libelle";
	public static final String TIT_MOTIF_REJET_COLKEY = "tit_Motif_Rejet";
	public static final String TIT_NB_PIECE_COLKEY = "TIT_NB_PIECE";
	public static final String TIT_NUMERO_COLKEY = "tit_Numero";
	public static final String TIT_NUMERO_REJET_COLKEY = "tit_Numero_rejet";
	public static final String TIT_ORDRE_COLKEY = "TIT_ORDRE";
	public static final String TIT_ORIGINE_KEY_COLKEY = "tit_orgine_key";
	public static final String TIT_ORIGINE_LIB_COLKEY = "tit_Origine_Lib";
	public static final String TIT_TTC_COLKEY = "tit_Ttc";
	public static final String TIT_TVA_COLKEY = "tit_Tva";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "rib_ordre_comptable";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "rib_ordre_ordonnateur";
	public static final String TIT_ID_COLKEY = "TIT_ID";
	public static final String TOR_ORDRE_COLKEY = "tor_Ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String BORDEREAU_REJET_KEY = "bordereauRejet";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String GESTION_KEY = "gestion";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String ORGAN_KEY = "organ";
	public static final String ORIGINE_KEY = "origine";
	public static final String PAIEMENT_KEY = "paiement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String RECETTES_KEY = "recettes";
	public static final String REIMPUTATIONS_KEY = "reimputations";
	public static final String RIB_KEY = "rib";
	public static final String TITRE_BROUILLARDS_KEY = "titreBrouillards";
	public static final String TITRE_DETAIL_ECRITURES_KEY = "titreDetailEcritures";
	public static final String TYPE_ORIGINE_KEY = "typeOrigine";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer borOrdre() {
    return (Integer) storedValueForKey(BOR_ORDRE_KEY);
  }

  public void setBorOrdre(Integer value) {
    takeStoredValueForKey(value, BOR_ORDRE_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public Integer prestId() {
    return (Integer) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Integer value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public NSTimestamp titDateRemise() {
    return (NSTimestamp) storedValueForKey(TIT_DATE_REMISE_KEY);
  }

  public void setTitDateRemise(NSTimestamp value) {
    takeStoredValueForKey(value, TIT_DATE_REMISE_KEY);
  }

  public NSTimestamp titDateVisaPrinc() {
    return (NSTimestamp) storedValueForKey(TIT_DATE_VISA_PRINC_KEY);
  }

  public void setTitDateVisaPrinc(NSTimestamp value) {
    takeStoredValueForKey(value, TIT_DATE_VISA_PRINC_KEY);
  }

  public String titEtat() {
    return (String) storedValueForKey(TIT_ETAT_KEY);
  }

  public void setTitEtat(String value) {
    takeStoredValueForKey(value, TIT_ETAT_KEY);
  }

  public String titEtatRemise() {
    return (String) storedValueForKey(TIT_ETAT_REMISE_KEY);
  }

  public void setTitEtatRemise(String value) {
    takeStoredValueForKey(value, TIT_ETAT_REMISE_KEY);
  }

  public java.math.BigDecimal titHt() {
    return (java.math.BigDecimal) storedValueForKey(TIT_HT_KEY);
  }

  public void setTitHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_HT_KEY);
  }

  public String titLibelle() {
    return (String) storedValueForKey(TIT_LIBELLE_KEY);
  }

  public void setTitLibelle(String value) {
    takeStoredValueForKey(value, TIT_LIBELLE_KEY);
  }

  public String titMotifRejet() {
    return (String) storedValueForKey(TIT_MOTIF_REJET_KEY);
  }

  public void setTitMotifRejet(String value) {
    takeStoredValueForKey(value, TIT_MOTIF_REJET_KEY);
  }

  public Integer titNbPiece() {
    return (Integer) storedValueForKey(TIT_NB_PIECE_KEY);
  }

  public void setTitNbPiece(Integer value) {
    takeStoredValueForKey(value, TIT_NB_PIECE_KEY);
  }

  public Integer titNumero() {
    return (Integer) storedValueForKey(TIT_NUMERO_KEY);
  }

  public void setTitNumero(Integer value) {
    takeStoredValueForKey(value, TIT_NUMERO_KEY);
  }

  public Integer titNumeroRejet() {
    return (Integer) storedValueForKey(TIT_NUMERO_REJET_KEY);
  }

  public void setTitNumeroRejet(Integer value) {
    takeStoredValueForKey(value, TIT_NUMERO_REJET_KEY);
  }

  public Integer titOrdre() {
    return (Integer) storedValueForKey(TIT_ORDRE_KEY);
  }

  public void setTitOrdre(Integer value) {
    takeStoredValueForKey(value, TIT_ORDRE_KEY);
  }

  public Integer titOrigineKey() {
    return (Integer) storedValueForKey(TIT_ORIGINE_KEY_KEY);
  }

  public void setTitOrigineKey(Integer value) {
    takeStoredValueForKey(value, TIT_ORIGINE_KEY_KEY);
  }

  public String titOrigineLib() {
    return (String) storedValueForKey(TIT_ORIGINE_LIB_KEY);
  }

  public void setTitOrigineLib(String value) {
    takeStoredValueForKey(value, TIT_ORIGINE_LIB_KEY);
  }

  public java.math.BigDecimal titTtc() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TTC_KEY);
  }

  public void setTitTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TTC_KEY);
  }

  public java.math.BigDecimal titTva() {
    return (java.math.BigDecimal) storedValueForKey(TIT_TVA_KEY);
  }

  public void setTitTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TIT_TVA_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOBordereau bordereau() {
    return (org.cocktail.maracuja.server.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.maracuja.server.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOBordereauRejet bordereauRejet() {
    return (org.cocktail.maracuja.server.metier.EOBordereauRejet)storedValueForKey(BORDEREAU_REJET_KEY);
  }

  public void setBordereauRejetRelationship(org.cocktail.maracuja.server.metier.EOBordereauRejet value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOBordereauRejet oldValue = bordereauRejet();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_REJET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_REJET_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.server.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOModePaiement modePaiement() {
    return (org.cocktail.maracuja.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.maracuja.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.maracuja.server.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }

  public void setModeRecouvrementRelationship(org.cocktail.maracuja.server.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrgan organ() {
    return (org.cocktail.maracuja.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.maracuja.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrigine origine() {
    return (org.cocktail.maracuja.server.metier.EOOrigine)storedValueForKey(ORIGINE_KEY);
  }

  public void setOrigineRelationship(org.cocktail.maracuja.server.metier.EOOrigine value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrigine oldValue = origine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPaiement paiement() {
    return (org.cocktail.maracuja.server.metier.EOPaiement)storedValueForKey(PAIEMENT_KEY);
  }

  public void setPaiementRelationship(org.cocktail.maracuja.server.metier.EOPaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPaiement oldValue = paiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORib rib() {
    return (org.cocktail.maracuja.server.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.server.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau typeOrigine() {
    return (org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau)storedValueForKey(TYPE_ORIGINE_KEY);
  }

  public void setTypeOrigineRelationship(org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau oldValue = typeOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray recettes() {
    return (NSArray)storedValueForKey(RECETTES_KEY);
  }

  public NSArray recettes(EOQualifier qualifier) {
    return recettes(qualifier, null, false);
  }

  public NSArray recettes(EOQualifier qualifier, boolean fetch) {
    return recettes(qualifier, null, fetch);
  }

  public NSArray recettes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EORecette.TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EORecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = recettes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRecettesRelationship(org.cocktail.maracuja.server.metier.EORecette object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
  }

  public void removeFromRecettesRelationship(org.cocktail.maracuja.server.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EORecette createRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Recette");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTES_KEY);
    return (org.cocktail.maracuja.server.metier.EORecette) eo;
  }

  public void deleteRecettesRelationship(org.cocktail.maracuja.server.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecettesRelationships() {
    Enumeration objects = recettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecettesRelationship((org.cocktail.maracuja.server.metier.EORecette)objects.nextElement());
    }
  }

  public NSArray reimputations() {
    return (NSArray)storedValueForKey(REIMPUTATIONS_KEY);
  }

  public NSArray reimputations(EOQualifier qualifier) {
    return reimputations(qualifier, null, false);
  }

  public NSArray reimputations(EOQualifier qualifier, boolean fetch) {
    return reimputations(qualifier, null, fetch);
  }

  public NSArray reimputations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOReimputation.TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOReimputation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationsRelationship(org.cocktail.maracuja.server.metier.EOReimputation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
  }

  public void removeFromReimputationsRelationship(org.cocktail.maracuja.server.metier.EOReimputation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOReimputation createReimputationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Reimputation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATIONS_KEY);
    return (org.cocktail.maracuja.server.metier.EOReimputation) eo;
  }

  public void deleteReimputationsRelationship(org.cocktail.maracuja.server.metier.EOReimputation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationsRelationships() {
    Enumeration objects = reimputations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationsRelationship((org.cocktail.maracuja.server.metier.EOReimputation)objects.nextElement());
    }
  }

  public NSArray titreBrouillards() {
    return (NSArray)storedValueForKey(TITRE_BROUILLARDS_KEY);
  }

  public NSArray titreBrouillards(EOQualifier qualifier) {
    return titreBrouillards(qualifier, null, false);
  }

  public NSArray titreBrouillards(EOQualifier qualifier, boolean fetch) {
    return titreBrouillards(qualifier, null, fetch);
  }

  public NSArray titreBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOTitreBrouillard.TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOTitreBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = titreBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTitreBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOTitreBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TITRE_BROUILLARDS_KEY);
  }

  public void removeFromTitreBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOTitreBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOTitreBrouillard createTitreBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("TitreBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TITRE_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.server.metier.EOTitreBrouillard) eo;
  }

  public void deleteTitreBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOTitreBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTitreBrouillardsRelationships() {
    Enumeration objects = titreBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTitreBrouillardsRelationship((org.cocktail.maracuja.server.metier.EOTitreBrouillard)objects.nextElement());
    }
  }

  public NSArray titreDetailEcritures() {
    return (NSArray)storedValueForKey(TITRE_DETAIL_ECRITURES_KEY);
  }

  public NSArray titreDetailEcritures(EOQualifier qualifier) {
    return titreDetailEcritures(qualifier, null, false);
  }

  public NSArray titreDetailEcritures(EOQualifier qualifier, boolean fetch) {
    return titreDetailEcritures(qualifier, null, fetch);
  }

  public NSArray titreDetailEcritures(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOTitreDetailEcriture.TITRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOTitreDetailEcriture.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = titreDetailEcritures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTitreDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOTitreDetailEcriture object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TITRE_DETAIL_ECRITURES_KEY);
  }

  public void removeFromTitreDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOTitreDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_DETAIL_ECRITURES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOTitreDetailEcriture createTitreDetailEcrituresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("TitreDetailEcriture");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TITRE_DETAIL_ECRITURES_KEY);
    return (org.cocktail.maracuja.server.metier.EOTitreDetailEcriture) eo;
  }

  public void deleteTitreDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOTitreDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_DETAIL_ECRITURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTitreDetailEcrituresRelationships() {
    Enumeration objects = titreDetailEcritures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTitreDetailEcrituresRelationship((org.cocktail.maracuja.server.metier.EOTitreDetailEcriture)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTitre avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTitre createEOTitre(EOEditingContext editingContext, Integer borOrdre
, String titEtat
, java.math.BigDecimal titHt
, Integer titNumero
, Integer titOrdre
, java.math.BigDecimal titTtc
, java.math.BigDecimal titTva
, org.cocktail.maracuja.server.metier.EOBordereau bordereau, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOGestion gestion, org.cocktail.maracuja.server.metier.EOModeRecouvrement modeRecouvrement, org.cocktail.maracuja.server.metier.EOPlanComptable planComptable, org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau typeOrigine, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EOTitre eo = (EOTitre) createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME);    
		eo.setBorOrdre(borOrdre);
		eo.setTitEtat(titEtat);
		eo.setTitHt(titHt);
		eo.setTitNumero(titNumero);
		eo.setTitOrdre(titOrdre);
		eo.setTitTtc(titTtc);
		eo.setTitTva(titTva);
    eo.setBordereauRelationship(bordereau);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    eo.setModeRecouvrementRelationship(modeRecouvrement);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeOrigineRelationship(typeOrigine);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOTitre localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitre)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitre creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTitre creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTitre object = (EOTitre)createAndInsertInstance(editingContext, _EOTitre.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTitre localInstanceIn(EOEditingContext editingContext, EOTitre eo) {
    EOTitre localInstance = (eo == null) ? null : (EOTitre)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTitre#localInstanceIn a la place.
   */
	public static EOTitre localInstanceOf(EOEditingContext editingContext, EOTitre eo) {
		return EOTitre.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTitre fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTitre fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitre fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitre eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitre)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTitre fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitre eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitre ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitre fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
