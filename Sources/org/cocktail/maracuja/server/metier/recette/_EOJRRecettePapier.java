/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJRRecettePapier.java instead.
package org.cocktail.maracuja.server.metier.recette;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJRRecettePapier extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JRRecettePapier";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.RECETTE_PAPIER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rppId";

	public static final String RPP_DATE_RECEPTION_KEY = "rppDateReception";
	public static final String RPP_DATE_RECETTE_KEY = "rppDateRecette";
	public static final String RPP_DATE_SAISIE_KEY = "rppDateSaisie";
	public static final String RPP_DATE_SERVICE_FAIT_KEY = "rppDateServiceFait";
	public static final String RPP_HT_SAISIE_KEY = "rppHtSaisie";
	public static final String RPP_NB_PIECE_KEY = "rppNbPiece";
	public static final String RPP_NUMERO_KEY = "rppNumero";
	public static final String RPP_TTC_SAISIE_KEY = "rppTtcSaisie";
	public static final String RPP_TVA_SAISIE_KEY = "rppTvaSaisie";
	public static final String RPP_VISIBLE_KEY = "rppVisible";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String RPP_ID_KEY = "rppId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RPP_DATE_RECEPTION_COLKEY = "RPP_DATE_RECEPTION";
	public static final String RPP_DATE_RECETTE_COLKEY = "RPP_DATE_RECETTE";
	public static final String RPP_DATE_SAISIE_COLKEY = "RPP_DATE_SAISIE";
	public static final String RPP_DATE_SERVICE_FAIT_COLKEY = "RPP_DATE_SERVICE_FAIT";
	public static final String RPP_HT_SAISIE_COLKEY = "RPP_HT_SAISIE";
	public static final String RPP_NB_PIECE_COLKEY = "RPP_NB_PIECE";
	public static final String RPP_NUMERO_COLKEY = "RPP_NUMERO";
	public static final String RPP_TTC_SAISIE_COLKEY = "RPP_TTC_SAISIE";
	public static final String RPP_TVA_SAISIE_COLKEY = "RPP_TVA_SAISIE";
	public static final String RPP_VISIBLE_COLKEY = "RPP_VISIBLE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String RPP_ID_COLKEY = "RPP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp rppDateReception() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_RECEPTION_KEY);
  }

  public void setRppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp rppDateRecette() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_RECETTE_KEY);
  }

  public void setRppDateRecette(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_RECETTE_KEY);
  }

  public NSTimestamp rppDateSaisie() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_SAISIE_KEY);
  }

  public void setRppDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_SAISIE_KEY);
  }

  public NSTimestamp rppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setRppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_SERVICE_FAIT_KEY);
  }

  public java.math.BigDecimal rppHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPP_HT_SAISIE_KEY);
  }

  public void setRppHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPP_HT_SAISIE_KEY);
  }

  public Integer rppNbPiece() {
    return (Integer) storedValueForKey(RPP_NB_PIECE_KEY);
  }

  public void setRppNbPiece(Integer value) {
    takeStoredValueForKey(value, RPP_NB_PIECE_KEY);
  }

  public String rppNumero() {
    return (String) storedValueForKey(RPP_NUMERO_KEY);
  }

  public void setRppNumero(String value) {
    takeStoredValueForKey(value, RPP_NUMERO_KEY);
  }

  public java.math.BigDecimal rppTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPP_TTC_SAISIE_KEY);
  }

  public void setRppTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal rppTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPP_TVA_SAISIE_KEY);
  }

  public void setRppTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPP_TVA_SAISIE_KEY);
  }

  public String rppVisible() {
    return (String) storedValueForKey(RPP_VISIBLE_KEY);
  }

  public void setRppVisible(String value) {
    takeStoredValueForKey(value, RPP_VISIBLE_KEY);
  }


/**
 * Créer une instance de EOJRRecettePapier avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJRRecettePapier createEOJRRecettePapier(EOEditingContext editingContext, NSTimestamp rppDateSaisie
, java.math.BigDecimal rppHtSaisie
, Integer rppNbPiece
, String rppNumero
, java.math.BigDecimal rppTtcSaisie
, java.math.BigDecimal rppTvaSaisie
, String rppVisible
			) {
    EOJRRecettePapier eo = (EOJRRecettePapier) createAndInsertInstance(editingContext, _EOJRRecettePapier.ENTITY_NAME);    
		eo.setRppDateSaisie(rppDateSaisie);
		eo.setRppHtSaisie(rppHtSaisie);
		eo.setRppNbPiece(rppNbPiece);
		eo.setRppNumero(rppNumero);
		eo.setRppTtcSaisie(rppTtcSaisie);
		eo.setRppTvaSaisie(rppTvaSaisie);
		eo.setRppVisible(rppVisible);
    return eo;
  }

  
	  public EOJRRecettePapier localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJRRecettePapier)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJRRecettePapier creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJRRecettePapier creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJRRecettePapier object = (EOJRRecettePapier)createAndInsertInstance(editingContext, _EOJRRecettePapier.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJRRecettePapier localInstanceIn(EOEditingContext editingContext, EOJRRecettePapier eo) {
    EOJRRecettePapier localInstance = (eo == null) ? null : (EOJRRecettePapier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJRRecettePapier#localInstanceIn a la place.
   */
	public static EOJRRecettePapier localInstanceOf(EOEditingContext editingContext, EOJRRecettePapier eo) {
		return EOJRRecettePapier.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJRRecettePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJRRecettePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJRRecettePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJRRecettePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJRRecettePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJRRecettePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJRRecettePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJRRecettePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJRRecettePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJRRecettePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJRRecettePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJRRecettePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
