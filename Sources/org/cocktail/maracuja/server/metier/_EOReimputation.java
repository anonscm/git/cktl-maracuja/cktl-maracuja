/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOReimputation.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOReimputation extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Reimputation";
	public static final String ENTITY_TABLE_NAME = "maracuja.Reimputation";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "reiOrdre";

	public static final String REI_DATE_KEY = "reiDate";
	public static final String REI_LIBELLE_KEY = "reiLibelle";
	public static final String REI_NUMERO_KEY = "reiNumero";

// Attributs non visibles
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String MAN_ID_KEY = "manId";
	public static final String PCO_NUM_ANCIEN_KEY = "pcoNumAncien";
	public static final String PCO_NUM_NOUVEAU_KEY = "pcoNumNouveau";
	public static final String REI_ORDRE_KEY = "reiOrdre";
	public static final String TIT_ID_KEY = "titId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String REI_DATE_COLKEY = "rei_date";
	public static final String REI_LIBELLE_COLKEY = "rei_libelle";
	public static final String REI_NUMERO_COLKEY = "rei_numero";

	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String MAN_ID_COLKEY = "MAN_ID";
	public static final String PCO_NUM_ANCIEN_COLKEY = "PCO_NUM_ancien";
	public static final String PCO_NUM_NOUVEAU_COLKEY = "PCO_NUM_nouveau";
	public static final String REI_ORDRE_COLKEY = "rei_ordre";
	public static final String TIT_ID_COLKEY = "TIT_ID";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String ECRITURE_KEY = "ecriture";
	public static final String EXERCICE_KEY = "exercice";
	public static final String MANDAT_KEY = "mandat";
	public static final String PLAN_COMPTABLE_ANCIEN_KEY = "planComptableAncien";
	public static final String PLAN_COMPTABLE_NOUVEAU_KEY = "planComptableNouveau";
	public static final String TITRE_KEY = "titre";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp reiDate() {
    return (NSTimestamp) storedValueForKey(REI_DATE_KEY);
  }

  public void setReiDate(NSTimestamp value) {
    takeStoredValueForKey(value, REI_DATE_KEY);
  }

  public String reiLibelle() {
    return (String) storedValueForKey(REI_LIBELLE_KEY);
  }

  public void setReiLibelle(String value) {
    takeStoredValueForKey(value, REI_LIBELLE_KEY);
  }

  public Integer reiNumero() {
    return (Integer) storedValueForKey(REI_NUMERO_KEY);
  }

  public void setReiNumero(Integer value) {
    takeStoredValueForKey(value, REI_NUMERO_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOEcriture ecriture() {
    return (org.cocktail.maracuja.server.metier.EOEcriture)storedValueForKey(ECRITURE_KEY);
  }

  public void setEcritureRelationship(org.cocktail.maracuja.server.metier.EOEcriture value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOEcriture oldValue = ecriture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOMandat mandat() {
    return (org.cocktail.maracuja.server.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.maracuja.server.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptableAncien() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_ANCIEN_KEY);
  }

  public void setPlanComptableAncienRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptableAncien();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_ANCIEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_ANCIEN_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptableNouveau() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_NOUVEAU_KEY);
  }

  public void setPlanComptableNouveauRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptableNouveau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_NOUVEAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_NOUVEAU_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTitre titre() {
    return (org.cocktail.maracuja.server.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.maracuja.server.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOReimputation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOReimputation createEOReimputation(EOEditingContext editingContext, NSTimestamp reiDate
, Integer reiNumero
, org.cocktail.maracuja.server.metier.EOEcriture ecriture, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOPlanComptable planComptableAncien, org.cocktail.maracuja.server.metier.EOPlanComptable planComptableNouveau, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EOReimputation eo = (EOReimputation) createAndInsertInstance(editingContext, _EOReimputation.ENTITY_NAME);    
		eo.setReiDate(reiDate);
		eo.setReiNumero(reiNumero);
    eo.setEcritureRelationship(ecriture);
    eo.setExerciceRelationship(exercice);
    eo.setPlanComptableAncienRelationship(planComptableAncien);
    eo.setPlanComptableNouveauRelationship(planComptableNouveau);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOReimputation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOReimputation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOReimputation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOReimputation object = (EOReimputation)createAndInsertInstance(editingContext, _EOReimputation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOReimputation localInstanceIn(EOEditingContext editingContext, EOReimputation eo) {
    EOReimputation localInstance = (eo == null) ? null : (EOReimputation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOReimputation#localInstanceIn a la place.
   */
	public static EOReimputation localInstanceOf(EOEditingContext editingContext, EOReimputation eo) {
		return EOReimputation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOReimputation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOReimputation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOReimputation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOReimputation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOReimputation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOReimputation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOReimputation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOReimputation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOReimputation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOReimputation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
