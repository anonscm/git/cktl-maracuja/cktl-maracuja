/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOperationTresorerie.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOperationTresorerie extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "OperationTresorerie";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ope_Tresorerie";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "optOrdre";

	public static final String OPT_DATE_KEY = "optDate";
	public static final String OPT_DATE_VALEUR_KEY = "optDateValeur";
	public static final String OPT_ETAT_KEY = "optEtat";
	public static final String OPT_LIBELLE_KEY = "optLibelle";
	public static final String OPT_MONTANT_KEY = "optMontant";
	public static final String OPT_NUMERO_CHEQUE_KEY = "optNumeroCheque";
	public static final String OPT_RESTE_EMARGER_KEY = "optResteEmarger";
	public static final String OPT_SENS_KEY = "optSens";

// Attributs non visibles
	public static final String CBA_ORDRE_KEY = "cbaOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String OPT_ORDRE_KEY = "optOrdre";
	public static final String TOT_ORDRE_KEY = "totOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String OPT_DATE_COLKEY = "opt_date";
	public static final String OPT_DATE_VALEUR_COLKEY = "opt_date_valeur";
	public static final String OPT_ETAT_COLKEY = "opt_etat";
	public static final String OPT_LIBELLE_COLKEY = "opt_Libelle";
	public static final String OPT_MONTANT_COLKEY = "opt_Montant";
	public static final String OPT_NUMERO_CHEQUE_COLKEY = "opt_Numero_Cheque";
	public static final String OPT_RESTE_EMARGER_COLKEY = "opt_Reste_Emarger";
	public static final String OPT_SENS_COLKEY = "opt_sens";

	public static final String CBA_ORDRE_COLKEY = "cba_Ordre";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String OPT_ORDRE_COLKEY = "opt_Ordre";
	public static final String TOT_ORDRE_COLKEY = "tot_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String COMPTE_BANCAIRE_KEY = "compteBancaire";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_OPERATION_TRESOR_KEY = "typeOperationTresor";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp optDate() {
    return (NSTimestamp) storedValueForKey(OPT_DATE_KEY);
  }

  public void setOptDate(NSTimestamp value) {
    takeStoredValueForKey(value, OPT_DATE_KEY);
  }

  public NSTimestamp optDateValeur() {
    return (NSTimestamp) storedValueForKey(OPT_DATE_VALEUR_KEY);
  }

  public void setOptDateValeur(NSTimestamp value) {
    takeStoredValueForKey(value, OPT_DATE_VALEUR_KEY);
  }

  public String optEtat() {
    return (String) storedValueForKey(OPT_ETAT_KEY);
  }

  public void setOptEtat(String value) {
    takeStoredValueForKey(value, OPT_ETAT_KEY);
  }

  public String optLibelle() {
    return (String) storedValueForKey(OPT_LIBELLE_KEY);
  }

  public void setOptLibelle(String value) {
    takeStoredValueForKey(value, OPT_LIBELLE_KEY);
  }

  public java.math.BigDecimal optMontant() {
    return (java.math.BigDecimal) storedValueForKey(OPT_MONTANT_KEY);
  }

  public void setOptMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, OPT_MONTANT_KEY);
  }

  public String optNumeroCheque() {
    return (String) storedValueForKey(OPT_NUMERO_CHEQUE_KEY);
  }

  public void setOptNumeroCheque(String value) {
    takeStoredValueForKey(value, OPT_NUMERO_CHEQUE_KEY);
  }

  public java.math.BigDecimal optResteEmarger() {
    return (java.math.BigDecimal) storedValueForKey(OPT_RESTE_EMARGER_KEY);
  }

  public void setOptResteEmarger(java.math.BigDecimal value) {
    takeStoredValueForKey(value, OPT_RESTE_EMARGER_KEY);
  }

  public String optSens() {
    return (String) storedValueForKey(OPT_SENS_KEY);
  }

  public void setOptSens(String value) {
    takeStoredValueForKey(value, OPT_SENS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOCompteBancaire compteBancaire() {
    return (org.cocktail.maracuja.server.metier.EOCompteBancaire)storedValueForKey(COMPTE_BANCAIRE_KEY);
  }

  public void setCompteBancaireRelationship(org.cocktail.maracuja.server.metier.EOCompteBancaire value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOCompteBancaire oldValue = compteBancaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTE_BANCAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTE_BANCAIRE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeOperationTresor typeOperationTresor() {
    return (org.cocktail.maracuja.server.metier.EOTypeOperationTresor)storedValueForKey(TYPE_OPERATION_TRESOR_KEY);
  }

  public void setTypeOperationTresorRelationship(org.cocktail.maracuja.server.metier.EOTypeOperationTresor value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeOperationTresor oldValue = typeOperationTresor();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_OPERATION_TRESOR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_OPERATION_TRESOR_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOOperationTresorerie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOperationTresorerie createEOOperationTresorerie(EOEditingContext editingContext, NSTimestamp optDate
, NSTimestamp optDateValeur
, String optLibelle
, java.math.BigDecimal optMontant
, java.math.BigDecimal optResteEmarger
, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOTypeOperationTresor typeOperationTresor, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EOOperationTresorerie eo = (EOOperationTresorerie) createAndInsertInstance(editingContext, _EOOperationTresorerie.ENTITY_NAME);    
		eo.setOptDate(optDate);
		eo.setOptDateValeur(optDateValeur);
		eo.setOptLibelle(optLibelle);
		eo.setOptMontant(optMontant);
		eo.setOptResteEmarger(optResteEmarger);
    eo.setExerciceRelationship(exercice);
    eo.setTypeOperationTresorRelationship(typeOperationTresor);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOOperationTresorerie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOperationTresorerie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOperationTresorerie creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOperationTresorerie creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOperationTresorerie object = (EOOperationTresorerie)createAndInsertInstance(editingContext, _EOOperationTresorerie.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOOperationTresorerie localInstanceIn(EOEditingContext editingContext, EOOperationTresorerie eo) {
    EOOperationTresorerie localInstance = (eo == null) ? null : (EOOperationTresorerie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOperationTresorerie#localInstanceIn a la place.
   */
	public static EOOperationTresorerie localInstanceOf(EOEditingContext editingContext, EOOperationTresorerie eo) {
		return EOOperationTresorerie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOperationTresorerie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOperationTresorerie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOperationTresorerie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOperationTresorerie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOperationTresorerie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOperationTresorerie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOperationTresorerie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOperationTresorerie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOperationTresorerie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOperationTresorerie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOperationTresorerie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOperationTresorerie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
