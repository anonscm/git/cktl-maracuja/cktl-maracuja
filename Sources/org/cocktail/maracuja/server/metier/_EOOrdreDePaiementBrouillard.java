/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrdreDePaiementBrouillard.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrdreDePaiementBrouillard extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "OrdreDePaiementBrouillard";
	public static final String ENTITY_TABLE_NAME = "maracuja.ODPaiement_Brouillard";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "opbOrdre";

	public static final String ODB_LIBELLE_KEY = "odbLibelle";
	public static final String ODB_MONTANT_KEY = "odbMontant";
	public static final String ODB_SENS_KEY = "odbSens";

// Attributs non visibles
	public static final String GES_CODE_KEY = "gesCode";
	public static final String ODP_ORDRE_KEY = "odpOrdre";
	public static final String OPB_ORDRE_KEY = "opbOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String ODB_LIBELLE_COLKEY = "odb_libelle";
	public static final String ODB_MONTANT_COLKEY = "odb_Montant";
	public static final String ODB_SENS_COLKEY = "odb_Sens";

	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String ODP_ORDRE_COLKEY = "odp_Ordre";
	public static final String OPB_ORDRE_COLKEY = "opb_Ordre";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String GESTION_KEY = "gestion";
	public static final String ORDRE_DE_PAIEMENT_KEY = "ordreDePaiement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String odbLibelle() {
    return (String) storedValueForKey(ODB_LIBELLE_KEY);
  }

  public void setOdbLibelle(String value) {
    takeStoredValueForKey(value, ODB_LIBELLE_KEY);
  }

  public java.math.BigDecimal odbMontant() {
    return (java.math.BigDecimal) storedValueForKey(ODB_MONTANT_KEY);
  }

  public void setOdbMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ODB_MONTANT_KEY);
  }

  public String odbSens() {
    return (String) storedValueForKey(ODB_SENS_KEY);
  }

  public void setOdbSens(String value) {
    takeStoredValueForKey(value, ODB_SENS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrdreDePaiement ordreDePaiement() {
    return (org.cocktail.maracuja.server.metier.EOOrdreDePaiement)storedValueForKey(ORDRE_DE_PAIEMENT_KEY);
  }

  public void setOrdreDePaiementRelationship(org.cocktail.maracuja.server.metier.EOOrdreDePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrdreDePaiement oldValue = ordreDePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORDRE_DE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORDRE_DE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

/**
 * Créer une instance de EOOrdreDePaiementBrouillard avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOrdreDePaiementBrouillard createEOOrdreDePaiementBrouillard(EOEditingContext editingContext, java.math.BigDecimal odbMontant
, String odbSens
, org.cocktail.maracuja.server.metier.EOGestion gestion, org.cocktail.maracuja.server.metier.EOOrdreDePaiement ordreDePaiement, org.cocktail.maracuja.server.metier.EOPlanComptable planComptable			) {
    EOOrdreDePaiementBrouillard eo = (EOOrdreDePaiementBrouillard) createAndInsertInstance(editingContext, _EOOrdreDePaiementBrouillard.ENTITY_NAME);    
		eo.setOdbMontant(odbMontant);
		eo.setOdbSens(odbSens);
    eo.setGestionRelationship(gestion);
    eo.setOrdreDePaiementRelationship(ordreDePaiement);
    eo.setPlanComptableRelationship(planComptable);
    return eo;
  }

  
	  public EOOrdreDePaiementBrouillard localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrdreDePaiementBrouillard)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrdreDePaiementBrouillard creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrdreDePaiementBrouillard creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOrdreDePaiementBrouillard object = (EOOrdreDePaiementBrouillard)createAndInsertInstance(editingContext, _EOOrdreDePaiementBrouillard.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOOrdreDePaiementBrouillard localInstanceIn(EOEditingContext editingContext, EOOrdreDePaiementBrouillard eo) {
    EOOrdreDePaiementBrouillard localInstance = (eo == null) ? null : (EOOrdreDePaiementBrouillard)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOrdreDePaiementBrouillard#localInstanceIn a la place.
   */
	public static EOOrdreDePaiementBrouillard localInstanceOf(EOEditingContext editingContext, EOOrdreDePaiementBrouillard eo) {
		return EOOrdreDePaiementBrouillard.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOOrdreDePaiementBrouillard fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrdreDePaiementBrouillard fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrdreDePaiementBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrdreDePaiementBrouillard)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrdreDePaiementBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrdreDePaiementBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrdreDePaiementBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrdreDePaiementBrouillard)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrdreDePaiementBrouillard fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrdreDePaiementBrouillard eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrdreDePaiementBrouillard ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrdreDePaiementBrouillard fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
