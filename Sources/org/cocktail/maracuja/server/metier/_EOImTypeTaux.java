/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOImTypeTaux.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOImTypeTaux extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "ImTypeTaux";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_IM_TYPE_TAUX";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "imttId";

	public static final String IMTT_CODE_KEY = "imttCode";
	public static final String IMTT_LIBELLE_KEY = "imttLibelle";
	public static final String IMTT_PRIORITE_KEY = "imttPriorite";

// Attributs non visibles
	public static final String IMTT_ID_KEY = "imttId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String IMTT_CODE_COLKEY = "IMTT_CODE";
	public static final String IMTT_LIBELLE_COLKEY = "IMTT_LIBELLE";
	public static final String IMTT_PRIORITE_COLKEY = "IMTT_Priorite";

	public static final String IMTT_ID_COLKEY = "IMTT_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String IM_TAUXS_KEY = "imTauxs";
	public static final String TYPE_ETAT_KEY = "typeEtat";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String imttCode() {
    return (String) storedValueForKey(IMTT_CODE_KEY);
  }

  public void setImttCode(String value) {
    takeStoredValueForKey(value, IMTT_CODE_KEY);
  }

  public String imttLibelle() {
    return (String) storedValueForKey(IMTT_LIBELLE_KEY);
  }

  public void setImttLibelle(String value) {
    takeStoredValueForKey(value, IMTT_LIBELLE_KEY);
  }

  public Long imttPriorite() {
    return (Long) storedValueForKey(IMTT_PRIORITE_KEY);
  }

  public void setImttPriorite(Long value) {
    takeStoredValueForKey(value, IMTT_PRIORITE_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.maracuja.server.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.maracuja.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray imTauxs() {
    return (NSArray)storedValueForKey(IM_TAUXS_KEY);
  }

  public NSArray imTauxs(EOQualifier qualifier) {
    return imTauxs(qualifier, null, false);
  }

  public NSArray imTauxs(EOQualifier qualifier, boolean fetch) {
    return imTauxs(qualifier, null, fetch);
  }

  public NSArray imTauxs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOImTaux.IM_TYPE_TAUX_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOImTaux.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = imTauxs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToImTauxsRelationship(org.cocktail.maracuja.server.metier.EOImTaux object) {
    addObjectToBothSidesOfRelationshipWithKey(object, IM_TAUXS_KEY);
  }

  public void removeFromImTauxsRelationship(org.cocktail.maracuja.server.metier.EOImTaux object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, IM_TAUXS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOImTaux createImTauxsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ImTaux");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, IM_TAUXS_KEY);
    return (org.cocktail.maracuja.server.metier.EOImTaux) eo;
  }

  public void deleteImTauxsRelationship(org.cocktail.maracuja.server.metier.EOImTaux object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, IM_TAUXS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllImTauxsRelationships() {
    Enumeration objects = imTauxs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteImTauxsRelationship((org.cocktail.maracuja.server.metier.EOImTaux)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOImTypeTaux avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOImTypeTaux createEOImTypeTaux(EOEditingContext editingContext, String imttCode
, String imttLibelle
, Long imttPriorite
, org.cocktail.maracuja.server.metier.EOTypeEtat typeEtat			) {
    EOImTypeTaux eo = (EOImTypeTaux) createAndInsertInstance(editingContext, _EOImTypeTaux.ENTITY_NAME);    
		eo.setImttCode(imttCode);
		eo.setImttLibelle(imttLibelle);
		eo.setImttPriorite(imttPriorite);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOImTypeTaux localInstanceIn(EOEditingContext editingContext) {
	  		return (EOImTypeTaux)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImTypeTaux creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImTypeTaux creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOImTypeTaux object = (EOImTypeTaux)createAndInsertInstance(editingContext, _EOImTypeTaux.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOImTypeTaux localInstanceIn(EOEditingContext editingContext, EOImTypeTaux eo) {
    EOImTypeTaux localInstance = (eo == null) ? null : (EOImTypeTaux)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOImTypeTaux#localInstanceIn a la place.
   */
	public static EOImTypeTaux localInstanceOf(EOEditingContext editingContext, EOImTypeTaux eo) {
		return EOImTypeTaux.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOImTypeTaux fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOImTypeTaux fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOImTypeTaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOImTypeTaux)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOImTypeTaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOImTypeTaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOImTypeTaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOImTypeTaux)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOImTypeTaux fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOImTypeTaux eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOImTypeTaux ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOImTypeTaux fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
