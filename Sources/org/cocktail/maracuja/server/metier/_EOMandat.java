/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMandat.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMandat extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Mandat";
	public static final String ENTITY_TABLE_NAME = "maracuja.Mandat";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "manId";

	public static final String MAN_ATTENTE_DATE_KEY = "manAttenteDate";
	public static final String MAN_ATTENTE_OBJET_KEY = "manAttenteObjet";
	public static final String MAN_DATE_REMISE_KEY = "manDateRemise";
	public static final String MAN_DATE_VISA_PRINC_KEY = "manDateVisaPrinc";
	public static final String MAN_ETAT_KEY = "manEtat";
	public static final String MAN_ETAT_REMISE_KEY = "manEtatRemise";
	public static final String MAN_HT_KEY = "manHt";
	public static final String MAN_MOTIF_REJET_KEY = "manMotifRejet";
	public static final String MAN_NB_PIECE_KEY = "manNbPiece";
	public static final String MAN_NUMERO_KEY = "manNumero";
	public static final String MAN_NUMERO_REJET_KEY = "manNumeroRejet";
	public static final String MAN_ORDRE_KEY = "manOrdre";
	public static final String MAN_ORIGINE_KEY_KEY = "manOrigineKey";
	public static final String MAN_ORIGINE_LIB_KEY = "manOrigineLib";
	public static final String MAN_TTC_KEY = "manTtc";
	public static final String MAN_TVA_KEY = "manTva";
	public static final String PREST_ID_KEY = "prestId";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String BRJ_ORDRE_KEY = "brjOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAN_ATTENTE_PAIEMENT_KEY = "manAttentePaiement";
	public static final String MAN_ID_KEY = "manId";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String RIB_ORDRE_COMPTABLE_KEY = "ribOrdreComptable";
	public static final String RIB_ORDRE_ORDONNATEUR_KEY = "ribOrdreOrdonnateur";
	public static final String TOR_ORDRE_KEY = "torOrdre";
	public static final String UTL_ORDRE_ATTENTE_KEY = "utlOrdreAttente";

//Colonnes dans la base de donnees
	public static final String MAN_ATTENTE_DATE_COLKEY = "man_attente_date";
	public static final String MAN_ATTENTE_OBJET_COLKEY = "man_attente_objet";
	public static final String MAN_DATE_REMISE_COLKEY = "man_Date_Remise";
	public static final String MAN_DATE_VISA_PRINC_COLKEY = "man_Date_Visa_Princ";
	public static final String MAN_ETAT_COLKEY = "man_etat";
	public static final String MAN_ETAT_REMISE_COLKEY = "man_Etat_Remise";
	public static final String MAN_HT_COLKEY = "man_Ht";
	public static final String MAN_MOTIF_REJET_COLKEY = "man_Motif_Rejet";
	public static final String MAN_NB_PIECE_COLKEY = "man_NB_PIECE";
	public static final String MAN_NUMERO_COLKEY = "man_Numero";
	public static final String MAN_NUMERO_REJET_COLKEY = "man_Numero_rejet";
	public static final String MAN_ORDRE_COLKEY = "MAN_ORDRE";
	public static final String MAN_ORIGINE_KEY_COLKEY = "man_orgine_key";
	public static final String MAN_ORIGINE_LIB_COLKEY = "man_Origine_Lib";
	public static final String MAN_TTC_COLKEY = "man_Ttc";
	public static final String MAN_TVA_COLKEY = "man_Tva";
	public static final String PREST_ID_COLKEY = "prest_id";

	public static final String BOR_ID_COLKEY = "BOR_id";
	public static final String BRJ_ORDRE_COLKEY = "BRJ_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MAN_ATTENTE_PAIEMENT_COLKEY = "man_attente_paiement";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String RIB_ORDRE_COMPTABLE_COLKEY = "rib_ordre_comptable";
	public static final String RIB_ORDRE_ORDONNATEUR_COLKEY = "rib_ordre_ordonnateur";
	public static final String TOR_ORDRE_COLKEY = "tor_Ordre";
	public static final String UTL_ORDRE_ATTENTE_COLKEY = "utl_ordre_attente";


	// Relationships
	public static final String ATTENTE_PAIEMENT_KEY = "attentePaiement";
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String BORDEREAU_REJET_KEY = "bordereauRejet";
	public static final String DEPENSES_KEY = "depenses";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String GESTION_KEY = "gestion";
	public static final String MANDAT_BROUILLARDS_KEY = "mandatBrouillards";
	public static final String MANDAT_DETAIL_ECRITURES_KEY = "mandatDetailEcritures";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String ORIGINE_KEY = "origine";
	public static final String PAIEMENT_KEY = "paiement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String REIMPUTATIONS_KEY = "reimputations";
	public static final String RIB_KEY = "rib";
	public static final String TO_EXTOURNE_MANDATS_N_KEY = "toExtourneMandatsN";
	public static final String TO_EXTOURNE_MANDATS_N1_KEY = "toExtourneMandatsN1";
	public static final String TYPE_ORIGINE_KEY = "typeOrigine";
	public static final String UTILISATEUR_ATTENTE_KEY = "utilisateurAttente";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp manAttenteDate() {
    return (NSTimestamp) storedValueForKey(MAN_ATTENTE_DATE_KEY);
  }

  public void setManAttenteDate(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_ATTENTE_DATE_KEY);
  }

  public String manAttenteObjet() {
    return (String) storedValueForKey(MAN_ATTENTE_OBJET_KEY);
  }

  public void setManAttenteObjet(String value) {
    takeStoredValueForKey(value, MAN_ATTENTE_OBJET_KEY);
  }

  public NSTimestamp manDateRemise() {
    return (NSTimestamp) storedValueForKey(MAN_DATE_REMISE_KEY);
  }

  public void setManDateRemise(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_DATE_REMISE_KEY);
  }

  public NSTimestamp manDateVisaPrinc() {
    return (NSTimestamp) storedValueForKey(MAN_DATE_VISA_PRINC_KEY);
  }

  public void setManDateVisaPrinc(NSTimestamp value) {
    takeStoredValueForKey(value, MAN_DATE_VISA_PRINC_KEY);
  }

  public String manEtat() {
    return (String) storedValueForKey(MAN_ETAT_KEY);
  }

  public void setManEtat(String value) {
    takeStoredValueForKey(value, MAN_ETAT_KEY);
  }

  public String manEtatRemise() {
    return (String) storedValueForKey(MAN_ETAT_REMISE_KEY);
  }

  public void setManEtatRemise(String value) {
    takeStoredValueForKey(value, MAN_ETAT_REMISE_KEY);
  }

  public java.math.BigDecimal manHt() {
    return (java.math.BigDecimal) storedValueForKey(MAN_HT_KEY);
  }

  public void setManHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_HT_KEY);
  }

  public String manMotifRejet() {
    return (String) storedValueForKey(MAN_MOTIF_REJET_KEY);
  }

  public void setManMotifRejet(String value) {
    takeStoredValueForKey(value, MAN_MOTIF_REJET_KEY);
  }

  public Integer manNbPiece() {
    return (Integer) storedValueForKey(MAN_NB_PIECE_KEY);
  }

  public void setManNbPiece(Integer value) {
    takeStoredValueForKey(value, MAN_NB_PIECE_KEY);
  }

  public Integer manNumero() {
    return (Integer) storedValueForKey(MAN_NUMERO_KEY);
  }

  public void setManNumero(Integer value) {
    takeStoredValueForKey(value, MAN_NUMERO_KEY);
  }

  public Integer manNumeroRejet() {
    return (Integer) storedValueForKey(MAN_NUMERO_REJET_KEY);
  }

  public void setManNumeroRejet(Integer value) {
    takeStoredValueForKey(value, MAN_NUMERO_REJET_KEY);
  }

  public Integer manOrdre() {
    return (Integer) storedValueForKey(MAN_ORDRE_KEY);
  }

  public void setManOrdre(Integer value) {
    takeStoredValueForKey(value, MAN_ORDRE_KEY);
  }

  public Integer manOrigineKey() {
    return (Integer) storedValueForKey(MAN_ORIGINE_KEY_KEY);
  }

  public void setManOrigineKey(Integer value) {
    takeStoredValueForKey(value, MAN_ORIGINE_KEY_KEY);
  }

  public String manOrigineLib() {
    return (String) storedValueForKey(MAN_ORIGINE_LIB_KEY);
  }

  public void setManOrigineLib(String value) {
    takeStoredValueForKey(value, MAN_ORIGINE_LIB_KEY);
  }

  public java.math.BigDecimal manTtc() {
    return (java.math.BigDecimal) storedValueForKey(MAN_TTC_KEY);
  }

  public void setManTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_TTC_KEY);
  }

  public java.math.BigDecimal manTva() {
    return (java.math.BigDecimal) storedValueForKey(MAN_TVA_KEY);
  }

  public void setManTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAN_TVA_KEY);
  }

  public Integer prestId() {
    return (Integer) storedValueForKey(PREST_ID_KEY);
  }

  public void setPrestId(Integer value) {
    takeStoredValueForKey(value, PREST_ID_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOTypeEtat attentePaiement() {
    return (org.cocktail.maracuja.server.metier.EOTypeEtat)storedValueForKey(ATTENTE_PAIEMENT_KEY);
  }

  public void setAttentePaiementRelationship(org.cocktail.maracuja.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeEtat oldValue = attentePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ATTENTE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ATTENTE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOBordereau bordereau() {
    return (org.cocktail.maracuja.server.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.maracuja.server.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOBordereauRejet bordereauRejet() {
    return (org.cocktail.maracuja.server.metier.EOBordereauRejet)storedValueForKey(BORDEREAU_REJET_KEY);
  }

  public void setBordereauRejetRelationship(org.cocktail.maracuja.server.metier.EOBordereauRejet value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOBordereauRejet oldValue = bordereauRejet();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_REJET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_REJET_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.server.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.server.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOModePaiement modePaiement() {
    return (org.cocktail.maracuja.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.maracuja.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrgan organ() {
    return (org.cocktail.maracuja.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.maracuja.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOOrigine origine() {
    return (org.cocktail.maracuja.server.metier.EOOrigine)storedValueForKey(ORIGINE_KEY);
  }

  public void setOrigineRelationship(org.cocktail.maracuja.server.metier.EOOrigine value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOOrigine oldValue = origine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPaiement paiement() {
    return (org.cocktail.maracuja.server.metier.EOPaiement)storedValueForKey(PAIEMENT_KEY);
  }

  public void setPaiementRelationship(org.cocktail.maracuja.server.metier.EOPaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPaiement oldValue = paiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EORib rib() {
    return (org.cocktail.maracuja.server.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.server.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau typeOrigine() {
    return (org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau)storedValueForKey(TYPE_ORIGINE_KEY);
  }

  public void setTypeOrigineRelationship(org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau oldValue = typeOrigine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateurAttente() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_ATTENTE_KEY);
  }

  public void setUtilisateurAttenteRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateurAttente();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_ATTENTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_ATTENTE_KEY);
    }
  }
  
  public NSArray depenses() {
    return (NSArray)storedValueForKey(DEPENSES_KEY);
  }

  public NSArray depenses(EOQualifier qualifier) {
    return depenses(qualifier, null, false);
  }

  public NSArray depenses(EOQualifier qualifier, boolean fetch) {
    return depenses(qualifier, null, fetch);
  }

  public NSArray depenses(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EODepense.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EODepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = depenses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDepensesRelationship(org.cocktail.maracuja.server.metier.EODepense object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSES_KEY);
  }

  public void removeFromDepensesRelationship(org.cocktail.maracuja.server.metier.EODepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EODepense createDepensesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Depense");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSES_KEY);
    return (org.cocktail.maracuja.server.metier.EODepense) eo;
  }

  public void deleteDepensesRelationship(org.cocktail.maracuja.server.metier.EODepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepensesRelationships() {
    Enumeration objects = depenses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepensesRelationship((org.cocktail.maracuja.server.metier.EODepense)objects.nextElement());
    }
  }

  public NSArray mandatBrouillards() {
    return (NSArray)storedValueForKey(MANDAT_BROUILLARDS_KEY);
  }

  public NSArray mandatBrouillards(EOQualifier qualifier) {
    return mandatBrouillards(qualifier, null, false);
  }

  public NSArray mandatBrouillards(EOQualifier qualifier, boolean fetch) {
    return mandatBrouillards(qualifier, null, fetch);
  }

  public NSArray mandatBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOMandatBrouillard.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOMandatBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = mandatBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMandatBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOMandatBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MANDAT_BROUILLARDS_KEY);
  }

  public void removeFromMandatBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOMandatBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDAT_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOMandatBrouillard createMandatBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MandatBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MANDAT_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.server.metier.EOMandatBrouillard) eo;
  }

  public void deleteMandatBrouillardsRelationship(org.cocktail.maracuja.server.metier.EOMandatBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDAT_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMandatBrouillardsRelationships() {
    Enumeration objects = mandatBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMandatBrouillardsRelationship((org.cocktail.maracuja.server.metier.EOMandatBrouillard)objects.nextElement());
    }
  }

  public NSArray mandatDetailEcritures() {
    return (NSArray)storedValueForKey(MANDAT_DETAIL_ECRITURES_KEY);
  }

  public NSArray mandatDetailEcritures(EOQualifier qualifier) {
    return mandatDetailEcritures(qualifier, null, false);
  }

  public NSArray mandatDetailEcritures(EOQualifier qualifier, boolean fetch) {
    return mandatDetailEcritures(qualifier, null, fetch);
  }

  public NSArray mandatDetailEcritures(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOMandatDetailEcriture.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOMandatDetailEcriture.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = mandatDetailEcritures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMandatDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOMandatDetailEcriture object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MANDAT_DETAIL_ECRITURES_KEY);
  }

  public void removeFromMandatDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOMandatDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDAT_DETAIL_ECRITURES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOMandatDetailEcriture createMandatDetailEcrituresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("MandatDetailEcriture");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MANDAT_DETAIL_ECRITURES_KEY);
    return (org.cocktail.maracuja.server.metier.EOMandatDetailEcriture) eo;
  }

  public void deleteMandatDetailEcrituresRelationship(org.cocktail.maracuja.server.metier.EOMandatDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDAT_DETAIL_ECRITURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMandatDetailEcrituresRelationships() {
    Enumeration objects = mandatDetailEcritures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMandatDetailEcrituresRelationship((org.cocktail.maracuja.server.metier.EOMandatDetailEcriture)objects.nextElement());
    }
  }

  public NSArray reimputations() {
    return (NSArray)storedValueForKey(REIMPUTATIONS_KEY);
  }

  public NSArray reimputations(EOQualifier qualifier) {
    return reimputations(qualifier, null, false);
  }

  public NSArray reimputations(EOQualifier qualifier, boolean fetch) {
    return reimputations(qualifier, null, fetch);
  }

  public NSArray reimputations(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOReimputation.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOReimputation.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = reimputations();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToReimputationsRelationship(org.cocktail.maracuja.server.metier.EOReimputation object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
  }

  public void removeFromReimputationsRelationship(org.cocktail.maracuja.server.metier.EOReimputation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOReimputation createReimputationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Reimputation");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REIMPUTATIONS_KEY);
    return (org.cocktail.maracuja.server.metier.EOReimputation) eo;
  }

  public void deleteReimputationsRelationship(org.cocktail.maracuja.server.metier.EOReimputation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REIMPUTATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllReimputationsRelationships() {
    Enumeration objects = reimputations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteReimputationsRelationship((org.cocktail.maracuja.server.metier.EOReimputation)objects.nextElement());
    }
  }

  public NSArray toExtourneMandatsN() {
    return (NSArray)storedValueForKey(TO_EXTOURNE_MANDATS_N_KEY);
  }

  public NSArray toExtourneMandatsN(EOQualifier qualifier) {
    return toExtourneMandatsN(qualifier, null, false);
  }

  public NSArray toExtourneMandatsN(EOQualifier qualifier, boolean fetch) {
    return toExtourneMandatsN(qualifier, null, fetch);
  }

  public NSArray toExtourneMandatsN(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOExtourneMandat.TO_MANDAT_N_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOExtourneMandat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toExtourneMandatsN();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExtourneMandatsNRelationship(org.cocktail.maracuja.server.metier.EOExtourneMandat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_MANDATS_N_KEY);
  }

  public void removeFromToExtourneMandatsNRelationship(org.cocktail.maracuja.server.metier.EOExtourneMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_MANDATS_N_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOExtourneMandat createToExtourneMandatsNRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ExtourneMandat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_MANDATS_N_KEY);
    return (org.cocktail.maracuja.server.metier.EOExtourneMandat) eo;
  }

  public void deleteToExtourneMandatsNRelationship(org.cocktail.maracuja.server.metier.EOExtourneMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_MANDATS_N_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExtourneMandatsNRelationships() {
    Enumeration objects = toExtourneMandatsN().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExtourneMandatsNRelationship((org.cocktail.maracuja.server.metier.EOExtourneMandat)objects.nextElement());
    }
  }

  public NSArray toExtourneMandatsN1() {
    return (NSArray)storedValueForKey(TO_EXTOURNE_MANDATS_N1_KEY);
  }

  public NSArray toExtourneMandatsN1(EOQualifier qualifier) {
    return toExtourneMandatsN1(qualifier, null, false);
  }

  public NSArray toExtourneMandatsN1(EOQualifier qualifier, boolean fetch) {
    return toExtourneMandatsN1(qualifier, null, fetch);
  }

  public NSArray toExtourneMandatsN1(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.server.metier.EOExtourneMandat.TO_MANDAT_N1_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.server.metier.EOExtourneMandat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toExtourneMandatsN1();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToExtourneMandatsN1Relationship(org.cocktail.maracuja.server.metier.EOExtourneMandat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_MANDATS_N1_KEY);
  }

  public void removeFromToExtourneMandatsN1Relationship(org.cocktail.maracuja.server.metier.EOExtourneMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_MANDATS_N1_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOExtourneMandat createToExtourneMandatsN1Relationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ExtourneMandat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_EXTOURNE_MANDATS_N1_KEY);
    return (org.cocktail.maracuja.server.metier.EOExtourneMandat) eo;
  }

  public void deleteToExtourneMandatsN1Relationship(org.cocktail.maracuja.server.metier.EOExtourneMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_EXTOURNE_MANDATS_N1_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToExtourneMandatsN1Relationships() {
    Enumeration objects = toExtourneMandatsN1().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToExtourneMandatsN1Relationship((org.cocktail.maracuja.server.metier.EOExtourneMandat)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOMandat avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMandat createEOMandat(EOEditingContext editingContext, String manEtat
, java.math.BigDecimal manHt
, Integer manNumero
, Integer manOrdre
, java.math.BigDecimal manTtc
, java.math.BigDecimal manTva
, org.cocktail.maracuja.server.metier.EOTypeEtat attentePaiement, org.cocktail.maracuja.server.metier.EOBordereau bordereau, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOFournisseur fournisseur, org.cocktail.maracuja.server.metier.EOGestion gestion, org.cocktail.maracuja.server.metier.EOModePaiement modePaiement, org.cocktail.maracuja.server.metier.EOPlanComptable planComptable, org.cocktail.maracuja.server.metier.EOTypeOrigineBordereau typeOrigine			) {
    EOMandat eo = (EOMandat) createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME);    
		eo.setManEtat(manEtat);
		eo.setManHt(manHt);
		eo.setManNumero(manNumero);
		eo.setManOrdre(manOrdre);
		eo.setManTtc(manTtc);
		eo.setManTva(manTva);
    eo.setAttentePaiementRelationship(attentePaiement);
    eo.setBordereauRelationship(bordereau);
    eo.setExerciceRelationship(exercice);
    eo.setFournisseurRelationship(fournisseur);
    eo.setGestionRelationship(gestion);
    eo.setModePaiementRelationship(modePaiement);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeOrigineRelationship(typeOrigine);
    return eo;
  }

  
	  public EOMandat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMandat)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandat creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandat creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMandat object = (EOMandat)createAndInsertInstance(editingContext, _EOMandat.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMandat localInstanceIn(EOEditingContext editingContext, EOMandat eo) {
    EOMandat localInstance = (eo == null) ? null : (EOMandat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMandat#localInstanceIn a la place.
   */
	public static EOMandat localInstanceOf(EOEditingContext editingContext, EOMandat eo) {
		return EOMandat.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMandat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMandat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMandat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMandat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMandat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMandat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMandat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMandat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMandat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
