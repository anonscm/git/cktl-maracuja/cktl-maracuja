/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOMandatBrouillard.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOMandatBrouillard extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "MandatBrouillard";
	public static final String ENTITY_TABLE_NAME = "maracuja.Mandat_Brouillard";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "mabOrdre";

	public static final String MAB_MONTANT_KEY = "mabMontant";
	public static final String MAB_OPERATION_KEY = "mabOperation";
	public static final String MAB_SENS_KEY = "mabSens";

// Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MAB_ORDRE_KEY = "mabOrdre";
	public static final String MAN_ID_KEY = "manId";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String MAB_MONTANT_COLKEY = "mab_Montant";
	public static final String MAB_OPERATION_COLKEY = "mab_Operation";
	public static final String MAB_SENS_COLKEY = "mab_Sens";

	public static final String ECD_ORDRE_COLKEY = "ECR_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String MAB_ORDRE_COLKEY = "MAB_ORDRE";
	public static final String MAN_ID_COLKEY = "MAN_ID";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String GESTION_KEY = "gestion";
	public static final String MANDAT_KEY = "mandat";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal mabMontant() {
    return (java.math.BigDecimal) storedValueForKey(MAB_MONTANT_KEY);
  }

  public void setMabMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MAB_MONTANT_KEY);
  }

  public String mabOperation() {
    return (String) storedValueForKey(MAB_OPERATION_KEY);
  }

  public void setMabOperation(String value) {
    takeStoredValueForKey(value, MAB_OPERATION_KEY);
  }

  public String mabSens() {
    return (String) storedValueForKey(MAB_SENS_KEY);
  }

  public void setMabSens(String value) {
    takeStoredValueForKey(value, MAB_SENS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOEcritureDetail ecritureDetail() {
    return (org.cocktail.maracuja.server.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
  }

  public void setEcritureDetailRelationship(org.cocktail.maracuja.server.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOEcritureDetail oldValue = ecritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.server.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.server.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOMandat mandat() {
    return (org.cocktail.maracuja.server.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.maracuja.server.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  

/**
 * Créer une instance de EOMandatBrouillard avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOMandatBrouillard createEOMandatBrouillard(EOEditingContext editingContext, java.math.BigDecimal mabMontant
, String mabSens
, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOGestion gestion, org.cocktail.maracuja.server.metier.EOMandat mandat, org.cocktail.maracuja.server.metier.EOPlanComptable planComptable			) {
    EOMandatBrouillard eo = (EOMandatBrouillard) createAndInsertInstance(editingContext, _EOMandatBrouillard.ENTITY_NAME);    
		eo.setMabMontant(mabMontant);
		eo.setMabSens(mabSens);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    eo.setMandatRelationship(mandat);
    eo.setPlanComptableRelationship(planComptable);
    return eo;
  }

  
	  public EOMandatBrouillard localInstanceIn(EOEditingContext editingContext) {
	  		return (EOMandatBrouillard)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandatBrouillard creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOMandatBrouillard creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOMandatBrouillard object = (EOMandatBrouillard)createAndInsertInstance(editingContext, _EOMandatBrouillard.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOMandatBrouillard localInstanceIn(EOEditingContext editingContext, EOMandatBrouillard eo) {
    EOMandatBrouillard localInstance = (eo == null) ? null : (EOMandatBrouillard)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOMandatBrouillard#localInstanceIn a la place.
   */
	public static EOMandatBrouillard localInstanceOf(EOEditingContext editingContext, EOMandatBrouillard eo) {
		return EOMandatBrouillard.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOMandatBrouillard fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOMandatBrouillard fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOMandatBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOMandatBrouillard)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOMandatBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOMandatBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOMandatBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOMandatBrouillard)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOMandatBrouillard fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOMandatBrouillard eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOMandatBrouillard ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOMandatBrouillard fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
