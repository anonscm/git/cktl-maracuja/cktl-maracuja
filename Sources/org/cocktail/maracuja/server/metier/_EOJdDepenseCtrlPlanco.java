/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJdDepenseCtrlPlanco.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJdDepenseCtrlPlanco extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "JdDepenseCtrlPlanco";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_JD_DEPENSE_CTRL_PLANCO";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dpcoId";

	public static final String DATE_DEBUT_DGP_KEY = "dateDebutDgp";
	public static final String DEP_ID_KEY = "depId";
	public static final String DPCO_HT_SAISIE_KEY = "dpcoHtSaisie";
	public static final String DPCO_MONTANT_BUDGETAIRE_KEY = "dpcoMontantBudgetaire";
	public static final String DPCO_TTC_SAISIE_KEY = "dpcoTtcSaisie";
	public static final String DPCO_TVA_SAISIE_KEY = "dpcoTvaSaisie";
	public static final String DPP_DATE_FACTURE_KEY = "dppDateFacture";
	public static final String DPP_DATE_RECEPTION_KEY = "dppDateReception";
	public static final String DPP_DATE_SERVICE_FAIT_KEY = "dppDateServiceFait";
	public static final String DPP_IM_DGP_KEY = "dppImDgp";
	public static final String DPP_NUMERO_FACTURE_KEY = "dppNumeroFacture";
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TBO_ORDRE_KEY = "tboOrdre";

// Attributs non visibles
	public static final String DPCO_ID_KEY = "dpcoId";
	public static final String DPP_ID_KEY = "dppId";
	public static final String MAN_ID_KEY = "manId";

//Colonnes dans la base de donnees
	public static final String DATE_DEBUT_DGP_COLKEY = "DATE_DEBUT_DGP";
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DPCO_HT_SAISIE_COLKEY = "DPCO_HT_SAISIE";
	public static final String DPCO_MONTANT_BUDGETAIRE_COLKEY = "DPCO_MONTANT_BUDGETAIRE";
	public static final String DPCO_TTC_SAISIE_COLKEY = "DPCO_TTC_SAISIE";
	public static final String DPCO_TVA_SAISIE_COLKEY = "DPCO_TVA_SAISIE";
	public static final String DPP_DATE_FACTURE_COLKEY = "DPP_DATE_FACTURE";
	public static final String DPP_DATE_RECEPTION_COLKEY = "DPP_DATE_RECEPTION";
	public static final String DPP_DATE_SERVICE_FAIT_COLKEY = "DPP_DATE_SERVICE_FAIT";
	public static final String DPP_IM_DGP_COLKEY = "DPP_IM_DGP";
	public static final String DPP_NUMERO_FACTURE_COLKEY = "DPP_NUMERO_FACTURE";
	public static final String ECD_ORDRE_COLKEY = "ECD_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";

	public static final String DPCO_ID_COLKEY = "DPCO_ID";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String MAN_ID_COLKEY = "MAN_ID";


	// Relationships
	public static final String DEPENSES_KEY = "depenses";
	public static final String JD_DEPENSE_PAPIER_KEY = "jdDepensePapier";
	public static final String MANDAT_KEY = "mandat";
	public static final String TO_DEPENSE_BUDGET_KEY = "toDepenseBudget";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp dateDebutDgp() {
    return (NSTimestamp) storedValueForKey(DATE_DEBUT_DGP_KEY);
  }

  public void setDateDebutDgp(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_DEBUT_DGP_KEY);
  }

  public Integer depId() {
    return (Integer) storedValueForKey(DEP_ID_KEY);
  }

  public void setDepId(Integer value) {
    takeStoredValueForKey(value, DEP_ID_KEY);
  }

  public java.math.BigDecimal dpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_HT_SAISIE_KEY);
  }

  public void setDpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal dpcoMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDpcoMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal dpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TTC_SAISIE_KEY);
  }

  public void setDpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DPCO_TVA_SAISIE_KEY);
  }

  public void setDpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DPCO_TVA_SAISIE_KEY);
  }

  public NSTimestamp dppDateFacture() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_FACTURE_KEY);
  }

  public void setDppDateFacture(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_FACTURE_KEY);
  }

  public NSTimestamp dppDateReception() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_RECEPTION_KEY);
  }

  public void setDppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp dppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(DPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setDppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, DPP_DATE_SERVICE_FAIT_KEY);
  }

  public Integer dppImDgp() {
    return (Integer) storedValueForKey(DPP_IM_DGP_KEY);
  }

  public void setDppImDgp(Integer value) {
    takeStoredValueForKey(value, DPP_IM_DGP_KEY);
  }

  public String dppNumeroFacture() {
    return (String) storedValueForKey(DPP_NUMERO_FACTURE_KEY);
  }

  public void setDppNumeroFacture(String value) {
    takeStoredValueForKey(value, DPP_NUMERO_FACTURE_KEY);
  }

  public Integer ecdOrdre() {
    return (Integer) storedValueForKey(ECD_ORDRE_KEY);
  }

  public void setEcdOrdre(Integer value) {
    takeStoredValueForKey(value, ECD_ORDRE_KEY);
  }

  public Double exeOrdre() {
    return (Double) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Double value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOJdDepensePapier jdDepensePapier() {
    return (org.cocktail.maracuja.server.metier.EOJdDepensePapier)storedValueForKey(JD_DEPENSE_PAPIER_KEY);
  }

  public void setJdDepensePapierRelationship(org.cocktail.maracuja.server.metier.EOJdDepensePapier value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOJdDepensePapier oldValue = jdDepensePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, JD_DEPENSE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, JD_DEPENSE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOMandat mandat() {
    return (org.cocktail.maracuja.server.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.maracuja.server.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.depense.EOJDDepenseBudget toDepenseBudget() {
    return (org.cocktail.maracuja.server.metier.depense.EOJDDepenseBudget)storedValueForKey(TO_DEPENSE_BUDGET_KEY);
  }

  public void setToDepenseBudgetRelationship(org.cocktail.maracuja.server.metier.depense.EOJDDepenseBudget value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.depense.EOJDDepenseBudget oldValue = toDepenseBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPENSE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPENSE_BUDGET_KEY);
    }
  }
  
  public NSArray depenses() {
    return (NSArray)storedValueForKey(DEPENSES_KEY);
  }

  public NSArray depenses(EOQualifier qualifier) {
    return depenses(qualifier, null);
  }

  public NSArray depenses(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = depenses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDepensesRelationship(org.cocktail.maracuja.server.metier.EODepense object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DEPENSES_KEY);
  }

  public void removeFromDepensesRelationship(org.cocktail.maracuja.server.metier.EODepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSES_KEY);
  }

  public org.cocktail.maracuja.server.metier.EODepense createDepensesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Depense");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DEPENSES_KEY);
    return (org.cocktail.maracuja.server.metier.EODepense) eo;
  }

  public void deleteDepensesRelationship(org.cocktail.maracuja.server.metier.EODepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DEPENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepensesRelationships() {
    Enumeration objects = depenses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepensesRelationship((org.cocktail.maracuja.server.metier.EODepense)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOJdDepenseCtrlPlanco avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOJdDepenseCtrlPlanco createEOJdDepenseCtrlPlanco(EOEditingContext editingContext, Integer depId
, java.math.BigDecimal dpcoHtSaisie
, java.math.BigDecimal dpcoMontantBudgetaire
, java.math.BigDecimal dpcoTtcSaisie
, java.math.BigDecimal dpcoTvaSaisie
, NSTimestamp dppDateFacture
, NSTimestamp dppDateReception
, NSTimestamp dppDateServiceFait
, String dppNumeroFacture
, Double exeOrdre
, String pcoNum
, Integer tboOrdre
, org.cocktail.maracuja.server.metier.EOJdDepensePapier jdDepensePapier, org.cocktail.maracuja.server.metier.depense.EOJDDepenseBudget toDepenseBudget			) {
    EOJdDepenseCtrlPlanco eo = (EOJdDepenseCtrlPlanco) createAndInsertInstance(editingContext, _EOJdDepenseCtrlPlanco.ENTITY_NAME);    
		eo.setDepId(depId);
		eo.setDpcoHtSaisie(dpcoHtSaisie);
		eo.setDpcoMontantBudgetaire(dpcoMontantBudgetaire);
		eo.setDpcoTtcSaisie(dpcoTtcSaisie);
		eo.setDpcoTvaSaisie(dpcoTvaSaisie);
		eo.setDppDateFacture(dppDateFacture);
		eo.setDppDateReception(dppDateReception);
		eo.setDppDateServiceFait(dppDateServiceFait);
		eo.setDppNumeroFacture(dppNumeroFacture);
		eo.setExeOrdre(exeOrdre);
		eo.setPcoNum(pcoNum);
		eo.setTboOrdre(tboOrdre);
    eo.setJdDepensePapierRelationship(jdDepensePapier);
    eo.setToDepenseBudgetRelationship(toDepenseBudget);
    return eo;
  }

  
	  public EOJdDepenseCtrlPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJdDepenseCtrlPlanco)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJdDepenseCtrlPlanco creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOJdDepenseCtrlPlanco creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOJdDepenseCtrlPlanco object = (EOJdDepenseCtrlPlanco)createAndInsertInstance(editingContext, _EOJdDepenseCtrlPlanco.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOJdDepenseCtrlPlanco localInstanceIn(EOEditingContext editingContext, EOJdDepenseCtrlPlanco eo) {
    EOJdDepenseCtrlPlanco localInstance = (eo == null) ? null : (EOJdDepenseCtrlPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOJdDepenseCtrlPlanco#localInstanceIn a la place.
   */
	public static EOJdDepenseCtrlPlanco localInstanceOf(EOEditingContext editingContext, EOJdDepenseCtrlPlanco eo) {
		return EOJdDepenseCtrlPlanco.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOJdDepenseCtrlPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOJdDepenseCtrlPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJdDepenseCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJdDepenseCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJdDepenseCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJdDepenseCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJdDepenseCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJdDepenseCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOJdDepenseCtrlPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJdDepenseCtrlPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJdDepenseCtrlPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJdDepenseCtrlPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
