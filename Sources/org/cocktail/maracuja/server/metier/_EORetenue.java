/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORetenue.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORetenue extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Retenue";
	public static final String ENTITY_TABLE_NAME = "maracuja.Retenue";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "retId";

	public static final String RET_DATE_KEY = "retDate";
	public static final String RET_LIBELLE_KEY = "retLibelle";
	public static final String RET_MONTANT_KEY = "retMontant";
	public static final String RET_NUMERO_KEY = "retNumero";

// Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String DEP_ID_KEY = "depId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String RET_ID_KEY = "retId";
	public static final String TRE_ORDRE_KEY = "treOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RET_DATE_COLKEY = "ret_Date";
	public static final String RET_LIBELLE_COLKEY = "ret_libelle";
	public static final String RET_MONTANT_COLKEY = "ret_montant";
	public static final String RET_NUMERO_COLKEY = "ret_numero";

	public static final String COM_ORDRE_COLKEY = "com_Ordre";
	public static final String DEP_ID_COLKEY = "dep_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String RET_ID_COLKEY = "ret_id";
	public static final String TRE_ORDRE_COLKEY = "TRE_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String COMPTABILITE_KEY = "comptabilite";
	public static final String DEPENSE_KEY = "depense";
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_RETENUE_KEY = "typeRetenue";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp retDate() {
    return (NSTimestamp) storedValueForKey(RET_DATE_KEY);
  }

  public void setRetDate(NSTimestamp value) {
    takeStoredValueForKey(value, RET_DATE_KEY);
  }

  public String retLibelle() {
    return (String) storedValueForKey(RET_LIBELLE_KEY);
  }

  public void setRetLibelle(String value) {
    takeStoredValueForKey(value, RET_LIBELLE_KEY);
  }

  public java.math.BigDecimal retMontant() {
    return (java.math.BigDecimal) storedValueForKey(RET_MONTANT_KEY);
  }

  public void setRetMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RET_MONTANT_KEY);
  }

  public Integer retNumero() {
    return (Integer) storedValueForKey(RET_NUMERO_KEY);
  }

  public void setRetNumero(Integer value) {
    takeStoredValueForKey(value, RET_NUMERO_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOComptabilite comptabilite() {
    return (org.cocktail.maracuja.server.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
  }

  public void setComptabiliteRelationship(org.cocktail.maracuja.server.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOComptabilite oldValue = comptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EODepense depense() {
    return (org.cocktail.maracuja.server.metier.EODepense)storedValueForKey(DEPENSE_KEY);
  }

  public void setDepenseRelationship(org.cocktail.maracuja.server.metier.EODepense value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EODepense oldValue = depense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.server.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOTypeRetenue typeRetenue() {
    return (org.cocktail.maracuja.server.metier.EOTypeRetenue)storedValueForKey(TYPE_RETENUE_KEY);
  }

  public void setTypeRetenueRelationship(org.cocktail.maracuja.server.metier.EOTypeRetenue value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeRetenue oldValue = typeRetenue();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RETENUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RETENUE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EORetenue avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORetenue createEORetenue(EOEditingContext editingContext, NSTimestamp retDate
, String retLibelle
, java.math.BigDecimal retMontant
, Integer retNumero
, org.cocktail.maracuja.server.metier.EOComptabilite comptabilite, org.cocktail.maracuja.server.metier.EODepense depense, org.cocktail.maracuja.server.metier.EOExercice exercice, org.cocktail.maracuja.server.metier.EOTypeRetenue typeRetenue, org.cocktail.maracuja.server.metier.EOUtilisateur utilisateur			) {
    EORetenue eo = (EORetenue) createAndInsertInstance(editingContext, _EORetenue.ENTITY_NAME);    
		eo.setRetDate(retDate);
		eo.setRetLibelle(retLibelle);
		eo.setRetMontant(retMontant);
		eo.setRetNumero(retNumero);
    eo.setComptabiliteRelationship(comptabilite);
    eo.setDepenseRelationship(depense);
    eo.setExerciceRelationship(exercice);
    eo.setTypeRetenueRelationship(typeRetenue);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EORetenue localInstanceIn(EOEditingContext editingContext) {
	  		return (EORetenue)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORetenue creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORetenue creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORetenue object = (EORetenue)createAndInsertInstance(editingContext, _EORetenue.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORetenue localInstanceIn(EOEditingContext editingContext, EORetenue eo) {
    EORetenue localInstance = (eo == null) ? null : (EORetenue)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORetenue#localInstanceIn a la place.
   */
	public static EORetenue localInstanceOf(EOEditingContext editingContext, EORetenue eo) {
		return EORetenue.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORetenue fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORetenue fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORetenue eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORetenue)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORetenue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORetenue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORetenue eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORetenue)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORetenue fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORetenue eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORetenue ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORetenue fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
