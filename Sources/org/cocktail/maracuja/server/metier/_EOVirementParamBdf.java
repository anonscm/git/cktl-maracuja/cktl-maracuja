/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVirementParamBdf.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVirementParamBdf extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "VirementParamBdf";
	public static final String ENTITY_TABLE_NAME = "maracuja.Virement_Param_Bdf";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vpbOrdre";

	public static final String VPB_A1_KEY = "vpbA1";
	public static final String VPB_A2_KEY = "vpbA2";
	public static final String VPB_B1_KEY = "vpbB1";
	public static final String VPB_C1_KEY = "vpbC1";
	public static final String VPB_C2_KEY = "vpbC2";
	public static final String VPB_C3_KEY = "vpbC3";
	public static final String VPB_C41_KEY = "vpbC41";
	public static final String VPB_C42_KEY = "vpbC42";
	public static final String VPB_C5_KEY = "vpbC5";
	public static final String VPB_C6_KEY = "vpbC6";
	public static final String VPB_COMPTE_TPG_KEY = "vpbCompteTpg";
	public static final String VPB_D10_KEY = "vpbD10";
	public static final String VPB_ETAT_KEY = "vpbEtat";
	public static final String VPB_NOM_REMETTANT_KEY = "vpbNomRemettant";
	public static final String VPB_NOM_TPG_KEY = "vpbNomTpg";

// Attributs non visibles
	public static final String TVI_ORDRE_KEY = "tviOrdre";
	public static final String VPB_ORDRE_KEY = "vpbOrdre";

//Colonnes dans la base de donnees
	public static final String VPB_A1_COLKEY = "vpb_A1";
	public static final String VPB_A2_COLKEY = "vpb_A2";
	public static final String VPB_B1_COLKEY = "vpb_B1";
	public static final String VPB_C1_COLKEY = "vpb_C1";
	public static final String VPB_C2_COLKEY = "vpb_C2";
	public static final String VPB_C3_COLKEY = "vpb_C3";
	public static final String VPB_C41_COLKEY = "vpb_C41";
	public static final String VPB_C42_COLKEY = "vpb_C42";
	public static final String VPB_C5_COLKEY = "vpb_C5";
	public static final String VPB_C6_COLKEY = "vpb_C6";
	public static final String VPB_COMPTE_TPG_COLKEY = "vpb_compte_tpg";
	public static final String VPB_D10_COLKEY = "vpb_D10";
	public static final String VPB_ETAT_COLKEY = "vpb_Etat";
	public static final String VPB_NOM_REMETTANT_COLKEY = "VPB_NOM_REMETTANT";
	public static final String VPB_NOM_TPG_COLKEY = "VPB_NOM_TPG";

	public static final String TVI_ORDRE_COLKEY = "tvi_Ordre";
	public static final String VPB_ORDRE_COLKEY = "vpb_Ordre";


	// Relationships
	public static final String TO_TYPE_VIREMENT_KEY = "toTypeVirement";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String vpbA1() {
    return (String) storedValueForKey(VPB_A1_KEY);
  }

  public void setVpbA1(String value) {
    takeStoredValueForKey(value, VPB_A1_KEY);
  }

  public String vpbA2() {
    return (String) storedValueForKey(VPB_A2_KEY);
  }

  public void setVpbA2(String value) {
    takeStoredValueForKey(value, VPB_A2_KEY);
  }

  public String vpbB1() {
    return (String) storedValueForKey(VPB_B1_KEY);
  }

  public void setVpbB1(String value) {
    takeStoredValueForKey(value, VPB_B1_KEY);
  }

  public String vpbC1() {
    return (String) storedValueForKey(VPB_C1_KEY);
  }

  public void setVpbC1(String value) {
    takeStoredValueForKey(value, VPB_C1_KEY);
  }

  public String vpbC2() {
    return (String) storedValueForKey(VPB_C2_KEY);
  }

  public void setVpbC2(String value) {
    takeStoredValueForKey(value, VPB_C2_KEY);
  }

  public String vpbC3() {
    return (String) storedValueForKey(VPB_C3_KEY);
  }

  public void setVpbC3(String value) {
    takeStoredValueForKey(value, VPB_C3_KEY);
  }

  public String vpbC41() {
    return (String) storedValueForKey(VPB_C41_KEY);
  }

  public void setVpbC41(String value) {
    takeStoredValueForKey(value, VPB_C41_KEY);
  }

  public String vpbC42() {
    return (String) storedValueForKey(VPB_C42_KEY);
  }

  public void setVpbC42(String value) {
    takeStoredValueForKey(value, VPB_C42_KEY);
  }

  public String vpbC5() {
    return (String) storedValueForKey(VPB_C5_KEY);
  }

  public void setVpbC5(String value) {
    takeStoredValueForKey(value, VPB_C5_KEY);
  }

  public String vpbC6() {
    return (String) storedValueForKey(VPB_C6_KEY);
  }

  public void setVpbC6(String value) {
    takeStoredValueForKey(value, VPB_C6_KEY);
  }

  public String vpbCompteTpg() {
    return (String) storedValueForKey(VPB_COMPTE_TPG_KEY);
  }

  public void setVpbCompteTpg(String value) {
    takeStoredValueForKey(value, VPB_COMPTE_TPG_KEY);
  }

  public String vpbD10() {
    return (String) storedValueForKey(VPB_D10_KEY);
  }

  public void setVpbD10(String value) {
    takeStoredValueForKey(value, VPB_D10_KEY);
  }

  public String vpbEtat() {
    return (String) storedValueForKey(VPB_ETAT_KEY);
  }

  public void setVpbEtat(String value) {
    takeStoredValueForKey(value, VPB_ETAT_KEY);
  }

  public String vpbNomRemettant() {
    return (String) storedValueForKey(VPB_NOM_REMETTANT_KEY);
  }

  public void setVpbNomRemettant(String value) {
    takeStoredValueForKey(value, VPB_NOM_REMETTANT_KEY);
  }

  public String vpbNomTpg() {
    return (String) storedValueForKey(VPB_NOM_TPG_KEY);
  }

  public void setVpbNomTpg(String value) {
    takeStoredValueForKey(value, VPB_NOM_TPG_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOTypeVirement toTypeVirement() {
    return (org.cocktail.maracuja.server.metier.EOTypeVirement)storedValueForKey(TO_TYPE_VIREMENT_KEY);
  }

  public void setToTypeVirementRelationship(org.cocktail.maracuja.server.metier.EOTypeVirement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOTypeVirement oldValue = toTypeVirement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_VIREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_VIREMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOVirementParamBdf avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOVirementParamBdf createEOVirementParamBdf(EOEditingContext editingContext, org.cocktail.maracuja.server.metier.EOTypeVirement toTypeVirement			) {
    EOVirementParamBdf eo = (EOVirementParamBdf) createAndInsertInstance(editingContext, _EOVirementParamBdf.ENTITY_NAME);    
    eo.setToTypeVirementRelationship(toTypeVirement);
    return eo;
  }

  
	  public EOVirementParamBdf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVirementParamBdf)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVirementParamBdf creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOVirementParamBdf creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOVirementParamBdf object = (EOVirementParamBdf)createAndInsertInstance(editingContext, _EOVirementParamBdf.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOVirementParamBdf localInstanceIn(EOEditingContext editingContext, EOVirementParamBdf eo) {
    EOVirementParamBdf localInstance = (eo == null) ? null : (EOVirementParamBdf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOVirementParamBdf#localInstanceIn a la place.
   */
	public static EOVirementParamBdf localInstanceOf(EOEditingContext editingContext, EOVirementParamBdf eo) {
		return EOVirementParamBdf.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOVirementParamBdf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVirementParamBdf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVirementParamBdf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVirementParamBdf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVirementParamBdf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVirementParamBdf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVirementParamBdf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVirementParamBdf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOVirementParamBdf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVirementParamBdf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVirementParamBdf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVirementParamBdf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
