/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EOEcritureDetail.java
// 

package org.cocktail.maracuja.server.metier;


import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

public class EOEcritureDetail extends _EOEcritureDetail
{
    public static final String tableName           = "EcritureDetail";
    public static final String[] entitesEnRelation = new String[]{};

    public static final String resteEmargerNegatif = "LE RESTE A EMARGER NE PEUT PAS ETRE NEGATIF";
    public static final String MSG_ECDDEBIT_NUL = "ecdDebit ne peut pas etre NULL";
    public static final String MSG_CREDIT_NUL = "ecdCredit ne peut pas etre NULL";
    public static final String MSG_CREDIT_MONTANT = "ecdCredit doit etre egal a ecdMontant";
    public static final String MSG_DEBIT_MONTANT = "ecdDebit doit etre egal a ecdMontant";
    public static final String MSG_RESTEEMARGER_GRAND = "Le reste a emarger ne peut etre plus grand que le montant initial. Si vous êtes en train d'annuler un émargement, vérifiez qu'une autre personne n'est pas en train d'annuler le même.";
    public static final String MSG_RESTEEMARGER_0 = "Le reste a emarger ne peut etre inférieur à 0. Vérifiez q'une autre personne n'est pas en train de faire un émargement sur la même écriture.";
    public static final String MSG_GESTION_OBLIGATOIRE = "La relation gestion est obligatoire.";
    public static final String MSG_EXERCICE_OBLIGATOIRE = "La relation exercice est obligatoire.";
    public static final String MSG_ECRITURE_OBLIGATOIRE = "La relation ecriture est obligatoire.";
    public static final String MSG_PLANCOMPTABLE_OBLIGATOIRE = "La relation planComptable est obligatoire.";
    
    public static final String SENS_DEBIT="D";
    public static final String SENS_CREDIT="C";

    public EOEcritureDetail() {
        super();
    }
    
        public void setEcdResteEmarger(
            BigDecimal value) {
        super.setEcdResteEmarger(value);
        if (value.floatValue() < 0)
            throw new NSValidation.ValidationException(MSG_RESTEEMARGER_0);
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/




    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }



    public void validateObjectMetier() throws NSValidation.ValidationException {
        if (SENS_DEBIT.equals(ecdSens())  && this.ecdDebit() == null)
            throw new NSValidation.ValidationException(MSG_ECDDEBIT_NUL);

        if (SENS_CREDIT.equals(ecdSens())  && this.ecdCredit() == null)
            throw new NSValidation.ValidationException(MSG_CREDIT_NUL);

        if (SENS_CREDIT.equals(ecdSens())
                && !(this.ecdCredit().equals(this.ecdMontant())))
            throw new NSValidation.ValidationException(MSG_CREDIT_MONTANT + " "+ecdCredit()+" / "+ecdMontant());

        if (SENS_DEBIT.equals(ecdSens())
                && !(this.ecdDebit().equals(this.ecdMontant())))
            throw new NSValidation.ValidationException(MSG_DEBIT_MONTANT + " "+ecdDebit()+" / "+ecdMontant());

        
        if (ecdResteEmarger().compareTo(ecdMontant().abs())>0 ) {
            throw new NSValidation.ValidationException(MSG_RESTEEMARGER_GRAND);
        }
        
        if (ecdResteEmarger().floatValue()<0 ) {
            throw new NSValidation.ValidationException(MSG_RESTEEMARGER_0);
        }
        
        
        // relationShip Mandatory :
        if (this.gestion() == null)
            throw new NSValidation.ValidationException(MSG_GESTION_OBLIGATOIRE);

        if (this.exercice() == null)
            throw new NSValidation.ValidationException(MSG_EXERCICE_OBLIGATOIRE);

        if (this.ecriture() == null)
            throw new NSValidation.ValidationException(MSG_ECRITURE_OBLIGATOIRE);

        if (this.planComptable() == null)
            throw new NSValidation.ValidationException(MSG_PLANCOMPTABLE_OBLIGATOIRE);
        
    }
    
    
    
 
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
        

    public void setEcdLibelle(String value) {
        if (value != null) {
            if (value.length()>200) {
                value = value.substring(0,200);
            }
        }
        super.setEcdLibelle(value);
    } 
    
}
