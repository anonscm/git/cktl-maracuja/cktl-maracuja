/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeOperationTresor.java instead.
package org.cocktail.maracuja.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import er.extensions.eof.ERXGenericRecord;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeOperationTresor extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TypeOperationTresor";
	public static final String ENTITY_TABLE_NAME = "maracuja.Type_Operation_Tresor";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "totOrdre";

	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String TOT_LIBELLE_KEY = "totLibelle";
	public static final String TOT_SENS_KEY = "totSens";

// Attributs non visibles
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_NUM_CONTRE_PARTIE_KEY = "pcoNumContrePartie";
	public static final String TOT_ORDRE_KEY = "totOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOr_ORDRE";
	public static final String TOT_LIBELLE_COLKEY = "tot_Libelle";
	public static final String TOT_SENS_COLKEY = "tot_sens";

	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_NUM_CONTRE_PARTIE_COLKEY = "pco_Num_Contre_Partie";
	public static final String TOT_ORDRE_COLKEY = "tot_ORDRE";


	// Relationships
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String PLAN_COMPTABLE_CONTRE_PARTIE_KEY = "planComptableContrePartie";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer modOrdre() {
    return (Integer) storedValueForKey(MOD_ORDRE_KEY);
  }

  public void setModOrdre(Integer value) {
    takeStoredValueForKey(value, MOD_ORDRE_KEY);
  }

  public Integer morOrdre() {
    return (Integer) storedValueForKey(MOR_ORDRE_KEY);
  }

  public void setMorOrdre(Integer value) {
    takeStoredValueForKey(value, MOR_ORDRE_KEY);
  }

  public String totLibelle() {
    return (String) storedValueForKey(TOT_LIBELLE_KEY);
  }

  public void setTotLibelle(String value) {
    takeStoredValueForKey(value, TOT_LIBELLE_KEY);
  }

  public String totSens() {
    return (String) storedValueForKey(TOT_SENS_KEY);
  }

  public void setTotSens(String value) {
    takeStoredValueForKey(value, TOT_SENS_KEY);
  }

  public org.cocktail.maracuja.server.metier.EOModePaiement modePaiement() {
    return (org.cocktail.maracuja.server.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.maracuja.server.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.maracuja.server.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }

  public void setModeRecouvrementRelationship(org.cocktail.maracuja.server.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.maracuja.server.metier.EOPlanComptable planComptableContrePartie() {
    return (org.cocktail.maracuja.server.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_CONTRE_PARTIE_KEY);
  }

  public void setPlanComptableContrePartieRelationship(org.cocktail.maracuja.server.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.server.metier.EOPlanComptable oldValue = planComptableContrePartie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_CONTRE_PARTIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_CONTRE_PARTIE_KEY);
    }
  }
  

/**
 * Créer une instance de EOTypeOperationTresor avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeOperationTresor createEOTypeOperationTresor(EOEditingContext editingContext, String totLibelle
, String totSens
, org.cocktail.maracuja.server.metier.EOPlanComptable planComptable, org.cocktail.maracuja.server.metier.EOPlanComptable planComptableContrePartie			) {
    EOTypeOperationTresor eo = (EOTypeOperationTresor) createAndInsertInstance(editingContext, _EOTypeOperationTresor.ENTITY_NAME);    
		eo.setTotLibelle(totLibelle);
		eo.setTotSens(totSens);
    eo.setPlanComptableRelationship(planComptable);
    eo.setPlanComptableContrePartieRelationship(planComptableContrePartie);
    return eo;
  }

  
	  public EOTypeOperationTresor localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeOperationTresor)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeOperationTresor creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeOperationTresor creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypeOperationTresor object = (EOTypeOperationTresor)createAndInsertInstance(editingContext, _EOTypeOperationTresor.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeOperationTresor localInstanceIn(EOEditingContext editingContext, EOTypeOperationTresor eo) {
    EOTypeOperationTresor localInstance = (eo == null) ? null : (EOTypeOperationTresor)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeOperationTresor#localInstanceIn a la place.
   */
	public static EOTypeOperationTresor localInstanceOf(EOEditingContext editingContext, EOTypeOperationTresor eo) {
		return EOTypeOperationTresor.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeOperationTresor fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeOperationTresor fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeOperationTresor eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeOperationTresor)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeOperationTresor fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeOperationTresor fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeOperationTresor eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeOperationTresor)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeOperationTresor fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeOperationTresor eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeOperationTresor ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeOperationTresor fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
