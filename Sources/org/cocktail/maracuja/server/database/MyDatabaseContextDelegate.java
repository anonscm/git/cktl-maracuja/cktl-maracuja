package org.cocktail.maracuja.server.database;

import java.util.Map;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorOperation;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EODatabaseOperation;
import com.webobjects.eoaccess.EOStoredProcedure;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

import er.extensions.eof.ERXDatabaseContextDelegate;
import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXSelectorUtilities;

/**
 * Délégué qui permet d'ajouter des procedures stockées à la pile des operations à executer lors de l'enregistrement en base de données. A initialiser
 * dans le constructeur de l'application : <br>
 * <code>
 * 			databaseContextDelegate = new MyDatabaseContextDelegate();<br>
			getDatabaseContext().setDelegate(databaseContextDelegate);<br>
 * </code> Ensuite ajouter des opérations à executer lors du prochain saveChanges sur l'editingContext de la session. Les opérations seront effectuées
 * à la fin de la transaction, avant le commit. <br>
 * <code>
 * 			EOStoredProcedure sp = EOUtilities.modelGroup(defaultEditingContext()).storedProcedureNamed(storedProcedureNameInModel);<br>
			if (sp == null) {<br>
				throw new Exception("Procedure " + storedProcedureNameInModel + " non retrouvée dans le modele.");<br>
			}
			getMyApplication().getDatabaseContextDelegate().addStoredProcedureAsOperation(defaultEditingContext(), sp, parameters);<br>
 * </code>
 * 
 * @author rprin
 */
public class MyDatabaseContextDelegate extends ERXDatabaseContextDelegate {
	public static final Logger log = Logger.getLogger(MyDatabaseContextDelegate.class);
	private Map<EOEditingContext, NSMutableArray<EOAdaptorOperation>> pendingOperationsByEdc = new NSMutableDictionary<EOEditingContext, NSMutableArray<EOAdaptorOperation>>();

	public MyDatabaseContextDelegate() {
		super();
		registerForNotifications();
	}

	protected void registerForNotifications() {
		NSSelector selectorDidSave = ERXSelectorUtilities.notificationSelector("editingContextDidSaveChanges");
		NSNotificationCenter.defaultCenter().addObserver(this, selectorDidSave, EOEditingContext.EditingContextDidSaveChangesNotification, null);
		NSSelector selectorFailToSave = ERXSelectorUtilities.notificationSelector("editingContextFailedToSaveChanges");
		NSNotificationCenter.defaultCenter().addObserver(this, selectorFailToSave, ERXEC.EditingContextFailedToSaveChanges, null);
		NSSelector selectorDidRevert = ERXSelectorUtilities.notificationSelector("editingContextDidRevertChanges");
		NSNotificationCenter.defaultCenter().addObserver(this, selectorDidRevert, ERXEC.EditingContextDidRevertChanges, null);
	}

	public NSArray databaseContextWillPerformAdaptorOperations(EODatabaseContext dbCtxt,
			NSArray adaptorOps,
			EOAdaptorChannel adChannel) {
		//Récupérer les opérations originales triées
		NSArray ads = super.databaseContextWillPerformAdaptorOperations(dbCtxt, adaptorOps, adChannel);
		NSMutableArray res = new NSMutableArray(ads.count());
		res.addObjectsFromArray(ads);

		//récupérer les opérations en attente affectées à l'editingcontext sur lequel est en train de s'effectuer le save
		if (!(dbCtxt._databaseContextState().valueForKey("editingContext") instanceof ERXEC)) {
			throw new RuntimeException("L'editing context devrait etre de type ERXEC");
		}
		ERXEC currentEdc = (ERXEC) dbCtxt._databaseContextState().valueForKey("editingContext");
		NSArray<EOAdaptorOperation> op = pendingOperationsByEdc.get(currentEdc);
		if (op != null && op.count() > 0) {
			res.addObjectsFromArray(op);
		}
		return res.immutableClone();
	}

	public Map<EOEditingContext, NSMutableArray<EOAdaptorOperation>> getPendingOperationsByEdc() {
		return pendingOperationsByEdc;
	}

	public void setPendingOperationsByEdc(Map<EOEditingContext, NSMutableArray<EOAdaptorOperation>> pendingOperationsByEdc) {
		this.pendingOperationsByEdc = pendingOperationsByEdc;
	}

	public void addOperation(EOEditingContext edc, EOAdaptorOperation operation) throws Exception {
		if (edc == null) {
			throw new Exception("Editing context est null");
		}
		if (operation == null) {
			throw new Exception("Operation est null");
		}

		if (!getPendingOperationsByEdc().containsKey(edc)) {
			getPendingOperationsByEdc().put(edc, new NSMutableArray<EOAdaptorOperation>(1));
		}
		getPendingOperationsByEdc().get(edc).addObject(operation);
		if (log.isDebugEnabled()) {
			log.debug("Operation " + operation + " ajoutée à la pile pour editingContext " + edc);
		}
	}

	public void addStoredProcedureAsOperation(EOEditingContext edc, EOStoredProcedure storedProcedure, NSDictionary<String, Object> values) throws Exception {
		if (storedProcedure == null) {
			throw new RuntimeException("storedProcedure est null");
		}

		EOAdaptorOperation operation = new EOAdaptorOperation(null);
		operation.setStoredProcedure(storedProcedure);
		operation.setAdaptorOperator(EODatabaseOperation.AdaptorStoredProcedureOperator);
		operation.setChangedValues(values);
		addOperation(edc, operation);
	}

	public void editingContextDidSaveChanges(NSNotification n) {
		EOEditingContext ec = (EOEditingContext) n.object();
		cleanOperations(ec);
	}

	public void editingContextFailedToSaveChanges(NSNotification n) {
		EOEditingContext ec = (EOEditingContext) n.object();
		//supprimer les opérations en attente
		//Il n'y a pas de revert qui est effectué automatiquement en cas de plantage lors de l'enregistrement en base.
		cleanOperations(ec);
	}

	public void editingContextDidRevertChanges(NSNotification n) {
		EOEditingContext ec = (EOEditingContext) n.object();
		cleanOperations(ec);
	}

	public void cleanOperations(EOEditingContext edc) {
		if (getPendingOperationsByEdc().containsKey(edc)) {
			if (log.isDebugEnabled()) {
				log.debug("Nettoyage des opérations en attente pour l'editingContext " + edc);
			}
			getPendingOperationsByEdc().get(edc).removeAllObjects();
			getPendingOperationsByEdc().remove(edc);
		}
	}

}
