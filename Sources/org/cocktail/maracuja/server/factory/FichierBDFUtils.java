/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.server.factory;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr> Classe offrant des utilitaires pour travailler avec les
 *         fichiers au format BDF.
 */

public class FichierBDFUtils extends Object {
	public static HashMap escapeChars = new HashMap();

	/**
	 * Créer un fichier xml representant un fichier de virement (utilise pour imprimer le detail).
	 * 
	 * @param paiOrdre
	 */
	public static StringWriter createXmlForFichierBDFVirement(EOEditingContext eoContext, String modelName, Number paiOrdre) throws Exception {
		if (paiOrdre == null) {
			throw new Exception("PaiOrdre est null");
		}

		//Recuperer le fichier
		String sqlQuery = "select EXE_ORDRE, VIR_ORDRE, PAI_NUMERO, PAI_DATE_CREATION, PAI_MONTANT, PAI_NB_VIREMENTS, VIR_CONTENU from (select vir_ordre, vir_contenu, pai_numero, p.exe_ordre, PAI_DATE_CREATION, PAI_MONTANT, PAI_NB_VIREMENTS from MARACUJA.virement_fichier f, MARACUJA.paiement p where f.pai_ordre = p.pai_ordre and p.pai_ordre="
				+ paiOrdre.intValue() + " order by vir_ordre desc) where rownum=1";
		NSArray res = EOUtilities.rawRowsForSQL(eoContext, modelName, sqlQuery, null);
		if (res == null || res.count() == 0) {
			throw new Exception("Le paiement ne correspond pas a un virement.");
		}
		//		System.out.println((NSDictionary)res.objectAtIndex(0));

		Number exercice = (Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("EXE_ORDRE");
		Number paiNumero = (Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("PAI_NUMERO");
		Number paiNbVirements = (Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("PAI_NB_VIREMENTS");
		Date paiDateCreation = (Date) ((NSDictionary) res.objectAtIndex(0)).valueForKey("PAI_DATE_CREATION");
		String contenuFichier = (String) ((NSDictionary) res.objectAtIndex(0)).valueForKey("VIR_CONTENU");

		final NSMutableDictionary metas = new NSMutableDictionary();
		metas.addEntriesFromDictionary((NSDictionary) res.objectAtIndex(0));
		metas.removeObjectForKey("VIR_CONTENU");

		int l = contenuFichier.length();

		final String enTete = contenuFichier.substring(0, 240);
		final String details = contenuFichier.substring(240, l - 240);
		final String total = contenuFichier.substring(l - 240);

		//        Hashtable dicoEntete = new Hashtable();
		//        Hashtable dicoTotal = new Hashtable();
		//        dicoEntete.put("e_a1", enTete.substring(0, 2));
		//        dicoEntete.put("e_a2", enTete.substring(2, 8));
		//        dicoEntete.put("e_b1", enTete.substring(8, 10));
		//        dicoEntete.put("e_b2", enTete.substring(10, 16));
		//        dicoEntete.put("e_c1_1", enTete.substring(16, 17));
		//        dicoEntete.put("e_c1_2", enTete.substring(17, 21));
		//        dicoEntete.put("e_c2", enTete.substring(21, 26));
		//        dicoEntete.put("e_c3", enTete.substring(26, 31));
		//        dicoEntete.put("e_c4_1", enTete.substring(31, 35));
		//        dicoEntete.put("e_c4_2", enTete.substring(35, 42));
		//        dicoEntete.put("e_c5", enTete.substring(42, 66));
		//        dicoEntete.put("e_d", enTete.substring(66, 240));
		//        
		//        dicoTotal.put("t_a1", total.substring(0, 2));
		//        dicoTotal.put("t_a2", total.substring(2, 8));
		//        dicoTotal.put("t_b1", total.substring(8, 10));
		//        dicoTotal.put("t_b2", total.substring(10, 16));
		//        dicoTotal.put("t_c1_1", total.substring(16, 17));
		//        dicoTotal.put("t_c1_2", total.substring(17, 21));
		//        dicoTotal.put("t_c2", total.substring(21, 26));
		//        dicoTotal.put("t_c3", total.substring(26, 31));
		//        dicoTotal.put("t_c4_1", total.substring(31, 35));
		//        dicoTotal.put("t_c4_2", total.substring(35, 42));
		//        dicoTotal.put("t_c5", total.substring(42, 66));
		//        dicoTotal.put("t_d1", total.substring(66, 228));
		//        dicoTotal.put("t_d11", total.substring(228, 240));        
		//        
		//        NSDictionary nsdicoEnTete = new NSMutableDictionary(dicoEntete, true);
		//        NSDictionary nsdicoTotal = new NSMutableDictionary(dicoTotal, true);

		//        metas.addEntriesFromDictionary(nsdicoEnTete);
		//        metas.addEntriesFromDictionary(nsdicoTotal);

		//        System.out.println("longueur fichier = "+l);
		//        System.out.println("entete = "+enTete);
		//        System.out.println("longueur entete = "+enTete.length());
		//        
		//        System.out.println("total = "+total);
		//        System.out.println("longueur total = "+total.length());
		//        
		//        System.out.println("details = "+details);
		//        System.out.println("longueur details = "+details.length());

		int nbdetails = details.length() / 240;

		Hashtable[] detailsArray = new Hashtable[nbdetails];
		for (int i = 0; i < nbdetails; i++) {
			System.out.println("");
			System.out.println("recup detail ligne : " + i + "   " + i * 240 + " -> " + 240 * (i + 1));
			final String unDetail = details.substring(i * 240, 240 * (i + 1));
			detailsArray[i] = new Hashtable();
			detailsArray[i].put("d_a1", unDetail.substring(0, 2));
			detailsArray[i].put("d_a2", unDetail.substring(2, 8));
			detailsArray[i].put("d_b1", unDetail.substring(8, 10));
			detailsArray[i].put("d_b2", unDetail.substring(10, 16));
			detailsArray[i].put("d_c1_1", unDetail.substring(16, 17));
			detailsArray[i].put("d_c1_2", unDetail.substring(17, 21));
			detailsArray[i].put("d_c2", unDetail.substring(21, 26));
			detailsArray[i].put("d_c3", unDetail.substring(26, 31));
			detailsArray[i].put("d_c4_1", unDetail.substring(31, 35));
			detailsArray[i].put("d_c4_2", unDetail.substring(35, 42));
			detailsArray[i].put("d_c5", unDetail.substring(42, 66));
			detailsArray[i].put("d_c6", unDetail.substring(66, 72));
			detailsArray[i].put("d_d1", unDetail.substring(72, 77));
			detailsArray[i].put("d_d2", unDetail.substring(77, 82));
			detailsArray[i].put("d_d3", unDetail.substring(82, 87));
			detailsArray[i].put("d_d4", unDetail.substring(87, 98));
			detailsArray[i].put("d_d5", unDetail.substring(98, 122));
			detailsArray[i].put("d_d6", unDetail.substring(122, 128));
			detailsArray[i].put("d_d7", unDetail.substring(128, 152));
			detailsArray[i].put("d_d8", unDetail.substring(152, 184));
			detailsArray[i].put("d_d9", unDetail.substring(184, 216));
			detailsArray[i].put("d_d10_1", unDetail.substring(216, 226));
			detailsArray[i].put("d_d10_2", unDetail.substring(226, 228));
			detailsArray[i].put("d_d11", unDetail.substring(228, 240));
			detailsArray[i].put("d_d11", unDetail.substring(228, 240));
			detailsArray[i].put("d_d11", unDetail.substring(228, 240));

			//on repete entete + total

			detailsArray[i].put("e_a1", enTete.substring(0, 2));
			detailsArray[i].put("e_a2", enTete.substring(2, 8));
			detailsArray[i].put("e_b1", enTete.substring(8, 10));
			detailsArray[i].put("e_b2", enTete.substring(10, 16));
			detailsArray[i].put("e_c1_1", enTete.substring(16, 17));
			detailsArray[i].put("e_c1_2", enTete.substring(17, 21));
			detailsArray[i].put("e_c2", enTete.substring(21, 26));
			detailsArray[i].put("e_c3", enTete.substring(26, 31));
			detailsArray[i].put("e_c4_1", enTete.substring(31, 35));
			detailsArray[i].put("e_c4_2", enTete.substring(35, 42));
			detailsArray[i].put("e_c5", enTete.substring(42, 66));
			detailsArray[i].put("e_d", enTete.substring(66, 240));

			detailsArray[i].put("t_a1", total.substring(0, 2));
			detailsArray[i].put("t_a2", total.substring(2, 8));
			detailsArray[i].put("t_b1", total.substring(8, 10));
			detailsArray[i].put("t_b2", total.substring(10, 16));
			detailsArray[i].put("t_c1_1", total.substring(16, 17));
			detailsArray[i].put("t_c1_2", total.substring(17, 21));
			detailsArray[i].put("t_c2", total.substring(21, 26));
			detailsArray[i].put("t_c3", total.substring(26, 31));
			detailsArray[i].put("t_c4_1", total.substring(31, 35));
			detailsArray[i].put("t_c4_2", total.substring(35, 42));
			detailsArray[i].put("t_c5", total.substring(42, 66));
			detailsArray[i].put("t_d1", total.substring(66, 228));
			detailsArray[i].put("t_d11", total.substring(228, 240));

			//metas
			detailsArray[i].put("exercice", exercice);
			detailsArray[i].put("paiNumero", paiNumero);
			detailsArray[i].put("paiNbVirements", paiNbVirements);
			detailsArray[i].put("paiDateCreation", new SimpleDateFormat("dd/MM/yyyy").format(paiDateCreation));
		}

		//On echappe les caracteres speciaux (pb dans methode d'echappement de CktlXMLWriter)
		getEscapeChars().put("&", CktlXMLWriter.SpecChars.amp);
		getEscapeChars().put("<", CktlXMLWriter.SpecChars.lt);
		getEscapeChars().put(">", CktlXMLWriter.SpecChars.gt);
		getEscapeChars().put("\"", "&quot;");
		for (int j = 0; j < detailsArray.length; j++) {
			Iterator it = detailsArray[j].keySet().iterator();
			while (it.hasNext()) {
				Object key = it.next();
				Object val = detailsArray[j].get(key);
				if (val instanceof String) {
					detailsArray[j].put(key, escapeSpecialChars((String) detailsArray[j].get(key)));
				}
			}
		}

		StringWriter myStringWriter = new StringWriter();
		CktlXMLWriter myCktlXMLWriter = new CktlXMLWriter(myStringWriter);
		myCktlXMLWriter.setUseCompactMode(false);
		myCktlXMLWriter.setEscapeSpecChars(false);

		myCktlXMLWriter.startDocument();
		myCktlXMLWriter.startElement("virements");
		for (int j = 0; j < detailsArray.length; j++) {
			myCktlXMLWriter.startElement("virement", detailsArray[j]);
			myCktlXMLWriter.endElement();
		}
		myCktlXMLWriter.endElement();//root
		myCktlXMLWriter.close();
		return myStringWriter;

	}

	protected static String escapeSpecialChars(final String val) {
		String tmpStr = val;
		final Iterator iter = getEscapeChars().keySet().iterator();
		while (iter.hasNext()) {
			final String element = (String) iter.next();
			tmpStr = tmpStr.replaceAll(element, (String) getEscapeChars().get(element));
		}
		return tmpStr;
	}

	public static HashMap getEscapeChars() {
		return escapeChars;
	}

}
