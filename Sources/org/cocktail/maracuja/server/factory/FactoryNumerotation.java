/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 17 oct. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.server.factory;

import org.cocktail.maracuja.server.metier.EOBordereau;
import org.cocktail.maracuja.server.metier.EOBordereauRejet;
import org.cocktail.maracuja.server.metier.EOEcriture;
import org.cocktail.maracuja.server.metier.EOEmargement;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EOPaiement;
import org.cocktail.maracuja.server.metier.EORecouvrement;
import org.cocktail.maracuja.server.metier.EOReimputation;
import org.cocktail.maracuja.server.metier.EORetenue;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;


/**
 * @author frivalla
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FactoryNumerotation extends Factory {

    /**
     *  
     */
    public FactoryNumerotation() {
        super();
    }

    /**
     * @param withLog
     */
    public FactoryNumerotation(boolean withLog) {
        super(withLog);
    }

    public void getNumeroEOEcriture(EOEditingContext ed, EOEcriture monEcriture) {
        System.out.println();
        System.out.println("FactoryNumerotation.getNumeroEOEcriture()");
//        System.out.println(monEcriture);
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monEcriture);
//        System.out.println(myDicoId);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_ecriture", myDicoId);

    }

    public void getNumeroEOEcritureBrouillard(EOEditingContext ed, EOEcriture monEcritureBrouillard) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monEcritureBrouillard);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_brouillard", myDicoId);

    }

    public void getNumeroEOBordereauRejet(EOEditingContext ed, EOBordereauRejet monBordereauRejet) {
        System.out.println();
        System.out.println("FactoryNumerotation.getNumeroEOBordereauRejet()");
//        System.out.println(monBordereauRejet);
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monBordereauRejet);
//        System.out.println(myDicoId);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_bordereau_rejet", myDicoId);

    }

    public void getNumeroEOEmargement(EOEditingContext ed, EOEmargement monEmargement) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monEmargement);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_emargement", myDicoId);

    }

    public void getNumeroEOPaiement(EOEditingContext ed, EOPaiement monVirement) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monVirement);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_paiement", myDicoId);

    }

    public void getNumeroEOOrdreDePaiement(EOEditingContext ed, EOOrdreDePaiement monOrdreDePaiement) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monOrdreDePaiement);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_ordre_paiement", myDicoId);
    }

    public void getNumeroEOReimputation(EOEditingContext ed, EOReimputation monEOReimputation) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monEOReimputation);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_reimputation", myDicoId);
    }

    public void getNumeroEORetenue(EOEditingContext ed, EORetenue monEORetenue) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, monEORetenue);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_retenue", myDicoId);
    }
    public void getNumeroEOBordereauCheque(EOEditingContext ed, EOBordereau bordereau) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, bordereau);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_bordereaucheques", myDicoId);
    }
    public void getNumeroEORecouvrement(EOEditingContext ed, EORecouvrement recouvrement) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, recouvrement);
        this.executeStoredProcedure(ed, "numerotationObject.numeroter_recouvrement", myDicoId);
    }



}
