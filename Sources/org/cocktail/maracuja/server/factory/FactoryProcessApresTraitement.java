/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server.factory;

import org.cocktail.maracuja.server.metier.EOBordereau;
import org.cocktail.maracuja.server.metier.EOPaiement;
import org.cocktail.maracuja.server.metier.EORecouvrement;
import org.cocktail.maracuja.server.metier.EOReimputation;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class FactoryProcessApresTraitement  extends Factory {

    /**
     * 
     */
    public FactoryProcessApresTraitement(boolean withLog) {
        super(withLog);
    }

    
    
	public final void afaireApresVisaBordereau(final EOEditingContext ed, final EOBordereau bord) {
	    NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, bord);
	    this.executeStoredProcedure(ed, "afaireaprestraitement.apres_visa_bordereau", myDicoId);	    
	}
	public final void afaireApresReimputation(final EOEditingContext ed, final EOReimputation bord) {
	    NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, bord);
	    this.executeStoredProcedure(ed, "afaireaprestraitement.apres_reimputation", myDicoId);	    
	}
	public final void afaireApresPaiement(final EOEditingContext ed, final EOPaiement bord) {
	    NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, bord);
	    this.executeStoredProcedure(ed, "afaireaprestraitement.apres_paiement", myDicoId);	    
	}



    public void afaireApresRecouvrement(final EOEditingContext ed, final EORecouvrement recouvrement) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, recouvrement);
        this.executeStoredProcedure(ed, "afaireaprestraitement.apres_recouvrement", myDicoId);           
    }
	
    
    public void afaireApresRecouvrementReleve(final EOEditingContext ed, final EORecouvrement recouvrement) {
        NSDictionary myDicoId = EOUtilities.primaryKeyForObject(ed, recouvrement);
        this.executeStoredProcedure(ed, "afaireaprestraitement.apres_recouvrement_releve", myDicoId);           
    }
    
    
    
}
