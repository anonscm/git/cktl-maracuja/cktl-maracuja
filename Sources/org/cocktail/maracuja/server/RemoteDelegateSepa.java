/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server;

import java.math.BigDecimal;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.remises.SepaSddEcheancesCombinaison;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation;
import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.maracuja.common.services.ISepaSddService;
import org.cocktail.maracuja.server.metier.EODepense;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EOPaiement;
import org.cocktail.maracuja.server.metier.EOVirementFichier;
import org.cocktail.maracuja.server.metier.EOVirementParamSepa;
import org.cocktail.maracuja.server.sepasct.SepaSCTFactory;
import org.cocktail.maracuja.server.sepasdd.RecouvrementSepaSddReleveService;
import org.cocktail.maracuja.server.sepasdd.RecouvrementSepaSddService;
import org.cocktail.zutil.server.ZDateUtil;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEC;

public class RemoteDelegateSepa extends RemoteDelegate {

	private static final String XSD_SCT_NAME = "xsd/pain.001.001.03.xsd";
	private static final String XSD_SDD_NAME = "xsd/pain.008.001.02.xsd";

	public RemoteDelegateSepa(Session session) {
		super(session);
	}

	public final NSDictionary clientSideRequestGenereVirementSepa(String exeOrdre, String paiNumero, EOEnterpriseObject virementParamSepaClient, NSTimestamp dateTimeCreation, NSArray lesDepensesGids,
			NSArray lesOrdresDePaimentGids, BigDecimal montantTotal,
			Integer nbTransactions, NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car,
			String identifiantFichierVirement, Integer optionRegroupementLignes) throws Exception {

		try {
			EOVirementParamSepa virementParamSepa = (EOVirementParamSepa) getLocalInstanceOfEo(virementParamSepaClient);
			NSMutableArray depensesLoc = new NSMutableArray();
			Iterator<EOGlobalID> iterdep = lesDepensesGids.iterator();
			while (iterdep.hasNext()) {
				EOGlobalID gid = (EOGlobalID) iterdep.next();
				EODepense dep = (EODepense) defaultEditingContext().faultForGlobalID(gid, defaultEditingContext());
				if (dep == null) {
					throw new Exception("Impossible de récupérer l'objet depense coté serveur pour le gid = " + gid);
				}
				depensesLoc.addObject(dep);
			}

			NSMutableArray odpsLoc = new NSMutableArray();
			Iterator<EOGlobalID> iterOdp = lesOrdresDePaimentGids.iterator();
			while (iterOdp.hasNext()) {
				EOGlobalID gid = (EOGlobalID) iterOdp.next();
				EOOrdreDePaiement odp = (EOOrdreDePaiement) defaultEditingContext().faultForGlobalID(gid, defaultEditingContext());
				if (odp == null) {
					throw new Exception("Impossible de récupérer l'objet ordre de paiement coté serveur pour le gid = " + gid);
				}
				odpsLoc.addObject(odp);
			}

			ParametresSepa paramSepa = new ParametresSepa();
			paramSepa.setCompteDftIban(virementParamSepa.vpsDftIban());
			paramSepa.setCompteDftTitulaire(virementParamSepa.vpsDftTitulaire());
			paramSepa.setDevise(virementParamSepa.vpsDftDevise());
			paramSepa.setEmetteurNom(virementParamSepa.vpsEmetteurNom());
			paramSepa.setTgBic(virementParamSepa.vpsTgBic());
			paramSepa.setTgCodique(virementParamSepa.vpsTgCodique());
			paramSepa.setTgIban(virementParamSepa.vpsTgIban());
			paramSepa.setTgNom(virementParamSepa.vpsTgNom());
			paramSepa.setEmetteurTransfertId(virementParamSepa.vpsDftTransfertId());

			URL xsdLocationUrl = getMySession().getMyApplication().resourceManager().pathURLForResourceNamed(XSD_SCT_NAME, null, null);
			if (xsdLocationUrl == null) {
				throw new Exception("Le fichier " + XSD_SCT_NAME + " n'a pas été trouvé dans les resources de l'application");
			}

			String listeCodesPaysSepa = (String) getMyApplication().config().get("org.cocktail.grhum.pays.zonesepa");

			SepaSCTFactory sepaFactory = new SepaSCTFactory(paramSepa, xsdLocationUrl.getPath(), exeOrdre, paiNumero, listeCodesPaysSepa);
			NSData d = sepaFactory.genereMessageVirement(dateTimeCreation, depensesLoc.immutableClone(), odpsLoc.immutableClone(), montantTotal, nbTransactions, dateExecutionDemandee, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, optionRegroupementLignes);
			String s = stringFromInputStream(d.stream(), "UTF-8");

			NSMutableDictionary res = new NSMutableDictionary();
			res.takeValueForKey(s, "contenu");
			res.takeValueForKey(nbTransactions, "nbTransactions");
			return res.immutableClone();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * @param virementParamSepaClient
	 * @param dateCreation
	 * @return Le nombre de fichiers de virements générés à partir d'un même compte pour une date donnée.
	 * @throws Exception
	 */
	public final Integer clientSideRequestNbPaiementJour(EOEnterpriseObject virementParamSepaClient, NSTimestamp dateCreation) throws Exception {
		try {
			EOVirementParamSepa virementParamSepa = (EOVirementParamSepa) getLocalInstanceOfEo(virementParamSepaClient);

			//On recupere tous les paiements effectués dans la journée sur le type sepa pour le compte spécifié
			NSArray res = EOVirementFichier.fetchAll(
					defaultEditingContext(),
					new EOAndQualifier(new NSArray(new Object[] {
							new EOKeyValueQualifier(EOVirementFichier.PAIEMENT_KEY + "." + EOPaiement.PAI_DATE_CREATION_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, ZDateUtil.getDateOnly(dateCreation)),
							new EOKeyValueQualifier(EOVirementFichier.PAIEMENT_KEY + "." + EOPaiement.PAI_DATE_CREATION_KEY, EOQualifier.QualifierOperatorLessThan, ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateCreation), 1, 0, 0, 0)),
							new EOKeyValueQualifier(EOVirementFichier.TYPE_VIREMENT_KEY, EOQualifier.QualifierOperatorEqual, virementParamSepa.toTypeVirement()),
							new EOKeyValueQualifier(EOVirementFichier.VIR_COMPTE_KEY, EOQualifier.QualifierOperatorEqual, virementParamSepa.vpsDftIban())

					})), null);
			int i = res.count();
			return Integer.valueOf(i);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	
	public final NSDictionary clientSideRequestGenerePrelevementsSepa(EOEnterpriseObject exercice, EOEnterpriseObject comptabilite, EOEnterpriseObject utilisateur, NSTimestamp dateCreation, EOEnterpriseObject sepaSddParam, NSDictionary<String, NSArray> echeancesGidsRegroupeesParCombinaison)
			throws Exception {
		try {
			//on travaille sur un editingcontext specifique pour éviter les erreurs liées au undoManager
			EOEditingContext editingContext = ERXEC.newEditingContext();

			EOJefyAdminExercice exerciceLoc = (EOJefyAdminExercice) EOUtilities.localInstanceOfObject(editingContext, exercice);
			EOComptabilite comptabiliteLoc = (EOComptabilite) EOUtilities.localInstanceOfObject(editingContext, comptabilite);
			EOJefyAdminUtilisateur utilisateurLoc = (EOJefyAdminUtilisateur) EOUtilities.localInstanceOfObject(editingContext, utilisateur);
			EOSepaSddParam sepaSddParamLoc = (EOSepaSddParam) EOUtilities.localInstanceOfObject(editingContext, sepaSddParam);

			EOTypeEmargement typeEmargement = EOTypeEmargement.fetchByKeyValue(editingContext, EOTypeEmargement.TEM_LIBELLE_KEY, EOTypeEmargement.typeLettrageAutomatique);
			EOTypeJournal typeJournal = EOTypeJournal.fetchByKeyValue(editingContext, EOTypeJournal.TJO_LIBELLE_KEY, EOTypeJournal.typeJournalRecouvrement);
			EOTypeOperation typeOperation = EOTypeOperation.fetchByKeyValue(editingContext, EOTypeOperation.TOP_LIBELLE_KEY, EOTypeOperation.TOP_LIBELLE_RECOUVREMENT);

			Map<SepaSddEcheancesCombinaison, NSArray<EOSepaSddEcheance>> regroupementsLoc = new HashMap<SepaSddEcheancesCombinaison, NSArray<EOSepaSddEcheance>>();

			NSArray<String> allKeys = echeancesGidsRegroupeesParCombinaison.allKeys();

			for (Iterator iterator = allKeys.iterator(); iterator.hasNext();) {
				String combinaison = (String) iterator.next();
				NSArray lesEcheancesGids = (NSArray) echeancesGidsRegroupeesParCombinaison.valueForKey(combinaison);
				SepaSddEcheancesCombinaison sddEcheancesCombinaison = SepaSddEcheancesCombinaison.fromString(combinaison);

				if (sddEcheancesCombinaison == null) {
					throw new Exception("Impossible de récupérer l'objet Combinaison coté serveur");
				}
				NSMutableArray echeancesLoc = new NSMutableArray();
				Iterator<EOGlobalID> iterdep = lesEcheancesGids.iterator();
				while (iterdep.hasNext()) {
					EOGlobalID gid = (EOGlobalID) iterdep.next();
					EOSepaSddEcheance dep = (EOSepaSddEcheance) editingContext.faultForGlobalID(gid, editingContext);
					if (dep == null) {
						throw new Exception("Impossible de récupérer l'objet EOSepaSddEcheance coté serveur pour le gid = " + gid);
					}
					echeancesLoc.addObject(dep);
				}
				regroupementsLoc.put(sddEcheancesCombinaison, echeancesLoc);
			}
			
			URL xsdLocationUrl = getMySession().getMyApplication().resourceManager().pathURLForResourceNamed(XSD_SDD_NAME, null, null);
			if (xsdLocationUrl == null) {
				throw new Exception("Le fichier " + XSD_SDD_NAME + " n'a pas été trouvé dans les resources de l'application");
			}
			


			RecouvrementSepaSddService service = new RecouvrementSepaSddService(editingContext, comptabiliteLoc, exerciceLoc, utilisateurLoc, dateCreation, sepaSddParamLoc, typeEmargement, typeJournal, typeOperation);
			NSDictionary res = service.faireTraitement(regroupementsLoc, xsdLocationUrl);
			return res;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * @param exercice
	 * @param comptabilite
	 * @param utilisateur
	 * @param dateCreation
	 * @param pcoDebitForRejet
	 * @param pcoDebitForConfirme
	 * @param echeancesGidsRegroupeesParEtat
	 * @return Un dictionaire avec les clés {@link ISepaSddService#ECRITURES_GENEREES_GIDS_KEY}, {@link ISepaSddService#EMARGEMENTS_GENEREES_GIDS_KEY}
	 *         et {@link ISepaSddService#ERREURS_KEY}.
	 * @throws Exception
	 */
	public final NSDictionary clientSideRequestSepaSddGenereEcrituresReleve(EOEnterpriseObject exercice, EOEnterpriseObject comptabilite, EOEnterpriseObject utilisateur, NSTimestamp dateCreation, EOEnterpriseObject pcoDebitForRejet, EOEnterpriseObject pcoDebitForConfirme,
			NSDictionary<String, NSArray> echeancesGidsRegroupeesParEtat, Boolean genererEcritures)
			throws Exception {
		try {
			//on travaille sur un editingcontext specifique pour éviter les erreurs liées au undoManager
			EOEditingContext editingContext = ERXEC.newEditingContext();

			EOJefyAdminExercice exerciceLoc = (EOJefyAdminExercice) EOUtilities.localInstanceOfObject(editingContext, exercice);
			EOComptabilite comptabiliteLoc = (EOComptabilite) EOUtilities.localInstanceOfObject(editingContext, comptabilite);
			EOJefyAdminUtilisateur utilisateurLoc = (EOJefyAdminUtilisateur) EOUtilities.localInstanceOfObject(editingContext, utilisateur);

			EOTypeEmargement typeEmargement = EOTypeEmargement.fetchByKeyValue(editingContext, EOTypeEmargement.TEM_LIBELLE_KEY, EOTypeEmargement.typeLettrageAutomatique);
			EOTypeJournal typeJournal = EOTypeJournal.fetchByKeyValue(editingContext, EOTypeJournal.TJO_LIBELLE_KEY, EOTypeJournal.typeJournalRecouvrement);
			EOTypeOperation typeOperation = EOTypeOperation.fetchByKeyValue(editingContext, EOTypeOperation.TOP_LIBELLE_KEY, EOTypeOperation.TOP_LIBELLE_RECOUVREMENT);

			EOPlanComptableExer pcoDebitForRejetLoc = (EOPlanComptableExer) EOUtilities.localInstanceOfObject(editingContext, pcoDebitForRejet);
			EOPlanComptableExer pcoDebitForConfirmeLoc = (EOPlanComptableExer) EOUtilities.localInstanceOfObject(editingContext, pcoDebitForConfirme);

			Map<ISepaSddEcheance.Etat, NSArray<EOSepaSddEcheance>> echeancesParEtat = new HashMap<ISepaSddEcheance.Etat, NSArray<EOSepaSddEcheance>>();
			NSArray<String> allKeys = echeancesGidsRegroupeesParEtat.allKeys();

			for (Iterator iterator = allKeys.iterator(); iterator.hasNext();) {
				String etat = (String) iterator.next();
				NSArray lesEcheancesGids = (NSArray) echeancesGidsRegroupeesParEtat.valueForKey(etat);
				ISepaSddEcheance.Etat etatAsEnum = ISepaSddEcheance.Etat.valueOf(etat);

				NSMutableArray echeancesLoc = new NSMutableArray();
				Iterator<EOGlobalID> iterdep = lesEcheancesGids.iterator();
				while (iterdep.hasNext()) {
					EOGlobalID gid = (EOGlobalID) iterdep.next();
					EOSepaSddEcheance dep = (EOSepaSddEcheance) editingContext.faultForGlobalID(gid, editingContext);
					if (dep == null) {
						throw new Exception("Impossible de récupérer l'objet EOSepaSddEcheance coté serveur pour le gid = " + gid);
					}
					echeancesLoc.addObject(dep);
				}
				echeancesParEtat.put(etatAsEnum, echeancesLoc);
			}

			RecouvrementSepaSddReleveService service = new RecouvrementSepaSddReleveService(editingContext, comptabiliteLoc, exerciceLoc, utilisateurLoc, dateCreation, typeEmargement, typeJournal, typeOperation);
			NSDictionary res = service.faireTraitement(echeancesParEtat, pcoDebitForRejetLoc, pcoDebitForConfirmeLoc, genererEcritures);
			return res;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

}
