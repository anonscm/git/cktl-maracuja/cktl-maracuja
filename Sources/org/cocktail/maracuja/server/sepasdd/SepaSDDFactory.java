/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server.sepasdd;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.util.ZListUtil;
import org.cocktail.fwkcktlcompta.server.metier.EOGrhumFournis;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance;
import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.gfc.server.echanges.sepa.factories.PrelevementSepaFactory;
import org.cocktail.gfc.server.echanges.sepa.factories.PrelevementSepaRecord;
import org.cocktail.maracuja.server.metier.EODepense;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;
import org.cocktail.zutil.server.ZStringUtil;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Genere des messages SEPA
 * 
 * @author rprin
 */
public class SepaSDDFactory {

	private ParametresSepa paramSepa;
	private String xsdLocation;
	private String recoNumero;
	private String exeOrdre;
	private List<String> codesPaysZoneSepaList;

	private static final String CODES_PAYS_SEPA_DEFAULT = "AT, BE, BG, CH, CY, CZ, DE, DK, EE, ES, FI, FR, GB, GF, GI, GR, GP, HR, HU, IE, IS, IT, LI, LT, LU, LV, MC, MQ, MT, NL, NO, PL, PM, PT, RE, RO, SE, SI, SK, YT";;


		/**
	 * @param paramSepa
	 * @param xsdLocation Emplacement du fichier xsd qui permet de valider le xml.
	 * @param exeOrdre
	 * @param recoNumero
	 * @param codesPaysZoneSepa
	 */
	public SepaSDDFactory(ParametresSepa paramSepa, String xsdLocation, String exeOrdre, String recoNumero, String codesPaysZoneSepa) {
		this.paramSepa = paramSepa;
		this.xsdLocation = xsdLocation;
		this.recoNumero = recoNumero;
		this.exeOrdre = exeOrdre;
		if (codesPaysZoneSepa == null) {
			codesPaysZoneSepa = CODES_PAYS_SEPA_DEFAULT;
		}
		this.codesPaysZoneSepaList = ZListUtil.createListFromCommaSeparatedStringAndTrim(codesPaysZoneSepa);
	}




		/**
	 * Genere un message SEPA de type Prelevement SDD
	 * 
	 * @param dateTimeCreation
	 * @param lesDepenses
	 * @param lesOrdresDePaiment
	 * @param montantTotal
	 * @param nbTransactions
	 * @param dateExecutionDemandee
	 * @param numeroDeFichierDansLaJournee3Car
	 * @param identifiantFichierVirement
	 * @param optionRegroupementLignes
	 * @param typeOperation
	 * @return
	 * @throws Exception
	 */
	public NSData genereMessagePrelevement(NSTimestamp dateTimeCreation, NSArray lesEcheances, BigDecimal montantTotal, Integer nbTransactions, NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car,
			String identifiantFichierVirement, String typeOperation)
			throws Exception {

		//Nettoyer les elements à prelever
		lesEcheances = supprimeElementsAvecMontantInferieurOuEgalAZero(lesEcheances);

		int no_transaction = 0;
		NSArray<PrelevementSepaRecord> records = createRecords(this, no_transaction, lesEcheances);
		checkBicInSEPA(records);
		nbTransactions = records.count();

		if (Integer.valueOf(0).equals(nbTransactions)) {
			throw new Exception("Aucune transaction a écrire dans le fichier. Les montants sont peut-etre tous à 0.");
		}

		PrelevementSepaFactory prelevementSepaFactory = new PrelevementSepaFactory();
		return prelevementSepaFactory.genereMessage(dateTimeCreation, paramSepa, records, montantTotal, nbTransactions, dateExecutionDemandee, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, xsdLocation, typeOperation);

	}

	protected NSMutableArray<EOSepaSddEcheance> supprimeElementsAvecMontantInferieurOuEgalAZero(NSArray<EOSepaSddEcheance> objs) {
		NSMutableArray<EOSepaSddEcheance> res = new NSMutableArray<EOSepaSddEcheance>();
		Iterator<EOSepaSddEcheance> iterElements = objs.iterator();
		while (iterElements.hasNext()) {
			EOSepaSddEcheance iSupportVirement = (EOSepaSddEcheance) iterElements.next();
			if (BigDecimal.ZERO.compareTo(iSupportVirement.montantAPayer()) < 0) {
				res.addObject(iSupportVirement);
			}
		}
		return res;
	}

	private NSMutableArray<EORib> listerTousLesRibs(NSArray lesDepenses, NSArray lesOrdresDePaiment) {
		NSMutableArray<EORib> ribs = new NSMutableArray<EORib>();
		Iterator<EODepense> iter = lesDepenses.iterator();
		while (iter.hasNext()) {
			EODepense eoDepense = (EODepense) iter.next();
			if (ribs.indexOfIdenticalObject(eoDepense.rib()) == NSArray.NotFound) {
				ribs.addObject(eoDepense.rib());
			}
		}

		Iterator<EOOrdreDePaiement> iter2 = lesOrdresDePaiment.iterator();
		while (iter2.hasNext()) {
			EOOrdreDePaiement eoOrdreDePaiement = (EOOrdreDePaiement) iter2.next();
			if (ribs.indexOfIdenticalObject(eoOrdreDePaiement.rib()) == NSArray.NotFound) {
				ribs.addObject(eoOrdreDePaiement.rib());
			}
		}

		//Trier les ribs
		ribs = EOSortOrdering.sortedArrayUsingKeyOrderArray(ribs, new NSArray(new Object[] {
				EORib.SORT_BIC_ASC, EORib.SORT_IBAN_ASC
		})).mutableClone();

		return ribs;
	}

	/**
	 * @param paiement
	 * @param index
	 * @return Un id de transaction construit à partir du titulaire du compte + exercice + n° paiement + n° transaction
	 */
	protected String buildTransactionId(int index) {
		String emetteur = ZStringUtil.extendWithChars(paramSepa.getCompteDftTitulaire(), " ", 12, false);
		String lotNum = ZStringUtil.extendWithChars(recoNumero, "0", 5, true);
		String transactionNum = ZStringUtil.extendWithChars("" + index, "0", 5, true);
		String res = emetteur.concat(exeOrdre).concat(lotNum).concat(transactionNum);
		return res;
	}

	/**
	 * Verifie si tous les BIC dependent bien de la zone SEPA.
	 * 
	 * @param ribs
	 * @throws Exception
	 */
	public void checkBicInSEPA(NSArray records) throws Exception {
		Iterator<PrelevementSepaRecord> iterRibs = records.iterator();
		while (iterRibs.hasNext()) {
			PrelevementSepaRecord rec = (PrelevementSepaRecord) iterRibs.next();
			String bic = rec.getDebtorBIC();
			String codePaysISO = bic.substring(4, 6);
			if (!codesPaysZoneSepaList.contains(codePaysISO)) {
				throw new Exception("Le code BIC " + bic + " n'identifie pas une banque d'un pays de la zone SEPA (caractères 5 et 6 du code=" + codePaysISO + ")");
			}
		}
	}

	/**
	 * Max 140 caracteres.
	 * 
	 * @param elementAPayer
	 * @return
	 */
	protected String getLibelleTransaction(ISepaSddEcheance echeance) {
		String res = ZStringUtil.cut("SDD " + ZStringUtil.cut(paramSepa.getCompteDftTitulaire(), 20), 28);
		res += ZStringUtil.cut("/ Mandat " + echeance.echeancier().mandat().rum(), 39);
		res += ZStringUtil.cut(" / " + echeance.echeancier().toSepaSddOrigine().getLibellePourDebiteur(), 44);
		res += ZStringUtil.cut(" / Rec " + getExeOrdre() + "-" + recoNumero, 17);
		res += ZStringUtil.cut(" / " + SepaSddEcheanceHelper.getSharedInstance().getNumeroEcheanceAPreleverPourEcheancier(echeance), 7);
		return ZStringUtil.cut(res, 140);
	}

	protected String getNomFournisseur(EOGrhumFournis fournisseur) {
		String res = fournisseur.toPersonne().getNomAndPrenom();
		res = ZStringUtil.extendWithChars(res, " ", 70, false);
		return res;
	}

	public ParametresSepa getParamSepa() {
		return paramSepa;
	}

	public String getPaiNumero() {
		return recoNumero;
	}

	public String getExeOrdre() {
		return exeOrdre;
	}

	public BigDecimal calculMontantPaiement(NSArray<ISupportVirement> elementsAPayer) {
		BigDecimal montant = new BigDecimal(0.00);
		Iterator<ISupportVirement> objs = elementsAPayer.iterator();
		while (objs.hasNext()) {
			montant = montant.add(objs.next().montantAPayer());
		}
		return montant;
	}

	public NSArray<PrelevementSepaRecord> createRecords(SepaSDDFactory factory, int noTransaction, NSArray<EOSepaSddEcheance> elementsAPayer) throws Exception {
		NSMutableArray<PrelevementSepaRecord> records = new NSMutableArray<PrelevementSepaRecord>();
		int noTransactionLocal = noTransaction;
		Iterator<EOSepaSddEcheance> iterDeps = elementsAPayer.iterator();
		while (iterDeps.hasNext()) {
			noTransactionLocal++;
			records.addObject(createRecordFrom(noTransactionLocal, iterDeps.next()));
		}
		return records;
	}

	private PrelevementSepaRecord createRecordFrom(int noTransaction, EOSepaSddEcheance echeance) {
		PrelevementSepaRecord prelevementSepaRecord = new PrelevementSepaRecord();
		prelevementSepaRecord.setPaymentID(buildTransactionId(noTransaction));
		prelevementSepaRecord.setInstructedAmount(echeance.montantAPayer());
		prelevementSepaRecord.setDebtorBIC(ZStringUtil.ifNull(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().toBanque().bic(), "XXXXFRXXXXX"));
		prelevementSepaRecord.setDebtorIBAN(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().iban());
		prelevementSepaRecord.setDebtorName(getNomFournisseur(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().toFournis()));
		prelevementSepaRecord.setCurrency(getParamSepa().getDevise());
		prelevementSepaRecord.setMandatID(echeance.toSepaSddEcheancier().toSepaSddMandat().rum());
		prelevementSepaRecord.setDateOfSignature(echeance.echeancier().mandat().dMandatSignature());
		prelevementSepaRecord.setPurposeCode(null);
		prelevementSepaRecord.setRemittanceInformation(getLibelleTransaction(echeance));
		prelevementSepaRecord.setMigrationNNE(false);

		EOSepaSddEcheance derniereEcheancePreleveeDuMandat = (EOSepaSddEcheance) SepaSddEcheanceHelper.getSharedInstance().echeancePrecedenteRemiseDuMandat(echeance);
		if (derniereEcheancePreleveeDuMandat != null) {
			prelevementSepaRecord.setPreviousMandatID(derniereEcheancePreleveeDuMandat.rum());
			prelevementSepaRecord.setPreviousDebtorBIC(derniereEcheancePreleveeDuMandat.debiteurBic());
			prelevementSepaRecord.setPreviousDebtorIBAN(derniereEcheancePreleveeDuMandat.debiteurIban());
			prelevementSepaRecord.setPreviousICS(derniereEcheancePreleveeDuMandat.creancierIcs());
			prelevementSepaRecord.setPreviousCreditorName(derniereEcheancePreleveeDuMandat.creancierNom());
		}
		else {
			prelevementSepaRecord.setPreviousMandatID(prelevementSepaRecord.getMandatID());
			prelevementSepaRecord.setPreviousDebtorBIC(prelevementSepaRecord.getDebtorBIC());
			prelevementSepaRecord.setPreviousDebtorIBAN(prelevementSepaRecord.getDebtorIBAN());
			prelevementSepaRecord.setPreviousICS(paramSepa.getIcs());
			prelevementSepaRecord.setPreviousCreditorName(paramSepa.getTgNom());
		}

		return prelevementSepaRecord;
	}

}
