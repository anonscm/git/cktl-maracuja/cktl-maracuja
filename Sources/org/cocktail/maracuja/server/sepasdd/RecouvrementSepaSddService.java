package org.cocktail.maracuja.server.sepasdd;

import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlcompta.common.ZConst;
import org.cocktail.fwkcktlcompta.common.exception.DefaultClientException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.remises.SepaSddEcheancesCombinaison;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheanceRule;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcompta.common.util.ZStreamUtil;
import org.cocktail.fwkcktlcompta.common.util.ZStringUtil;
import org.cocktail.fwkcktlcompta.server.factories.EOEcritureFactory;
import org.cocktail.fwkcktlcompta.server.factories.EOEmargementFactory;
import org.cocktail.fwkcktlcompta.server.factories.PrelevementEcrituresFactory;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOPrelevementFichier;
import org.cocktail.fwkcktlcompta.server.metier.EORecouvrement;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddParam;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeRecouvrement;
import org.cocktail.fwkcktlcompta.server.sepasdd.factories.SepaSddPrelevementFichierFactory;
import org.cocktail.fwkcktlcompta.server.sepasdd.factories.SepaSddRecouvrementFactory;
import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentInformation;
import org.cocktail.maracuja.common.services.ISepaSddService;
import org.cocktail.maracuja.server.Application;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Services pour les recouvrement de type SEPA SDD.
 * 
 * @author rprin
 */
public class RecouvrementSepaSddService implements ISepaSddService {
	private static final String XSD_SDD_NAME = "xsd/pain.008.001.02.xsd";

	public static final String FICHIER_NB_TRANSACTIONS_KEY = "nbTransactions";
	public static final String FICHIER_CONTENU_KEY = "contenu";
	private final EOComptabilite comptabilite;
	private final EOJefyAdminExercice exercice;
	private final EOJefyAdminUtilisateur utilisateur;
	private final NSTimestamp dateCreation;
	private final EOEditingContext editingContext;
	private final EOSepaSddParam sepaSddParam;
	private final EOTypeEmargement typeEmargement;
	private final EOTypeJournal typeJournal;
	private final EOTypeOperation typeOperation;

	/**
	 * @param editingContext
	 * @param comptabilite
	 * @param exercice
	 * @param utilisateur
	 * @param dateCreation
	 * @param sepaSddParam
	 * @param typeEmargement TypeEmargement à utiliser pour les émargements entre écriture de prélèvement et ecriture de prise en charge echeancier.
	 */
	public RecouvrementSepaSddService(EOEditingContext editingContext, EOComptabilite comptabilite, EOJefyAdminExercice exercice, EOJefyAdminUtilisateur utilisateur, NSTimestamp dateCreation, EOSepaSddParam sepaSddParam,
			EOTypeEmargement typeEmargement, EOTypeJournal typeJournal, EOTypeOperation typeOperation) {
		this.editingContext = editingContext;
		this.comptabilite = comptabilite;
		this.exercice = exercice;
		this.utilisateur = utilisateur;
		this.dateCreation = dateCreation;
		this.sepaSddParam = sepaSddParam;
		this.typeEmargement = typeEmargement;
		this.typeJournal = typeJournal;
		this.typeOperation = typeOperation;
	}

	public BigDecimal getMontantForEcheances(NSArray echeances) {
		return SepaSddEcheanceHelper.getSharedInstance().calculeMontantAPayerDesEcheancesNonAnnulees(echeances);
	}

	public Integer getNbTransactions(NSArray echeances) {
		return echeances.count();
	}

	public EORecouvrement creerRecouvrementForEcheances(NSArray echeances, Integer recoNumero) {
		final BigDecimal montant = getMontantForEcheances(echeances);
		final Integer nbPrelevements = getNbTransactions(echeances);

		final SepaSddRecouvrementFactory factoryRecouvrement = new SepaSddRecouvrementFactory();
		EORecouvrement recouvrement = factoryRecouvrement.creerRecouvrement(getEditingContext(), comptabilite, exercice, utilisateur, getSepaSddParam().toTypeRecouvrement(), dateCreation, montant, nbPrelevements, recoNumero);
		factoryRecouvrement.associerEcheances(recouvrement, echeances);

		//memoriser les infos du mandat au niveau de 'echeance (pour tracabilite)
		return recouvrement;
	}

	public void memoriserInformationsDuMandatAuNiveauDesEcheances(NSArray echeances) {
		for (int i = 0; i < echeances.count(); i++) {
			EOSepaSddEcheance echeance = (EOSepaSddEcheance) echeances.objectAtIndex(i);
			echeance.setRum(echeance.toSepaSddEcheancier().toSepaSddMandat().rum());
			echeance.setCreancierIcs(echeance.toSepaSddEcheancier().toSepaSddMandat().toSepaSddParam().creancierIcs());
			echeance.setCreancierId(echeance.toSepaSddEcheancier().toSepaSddMandat().toSepaSddParam().toPersonneCreancier().persId().toString());
			echeance.setCreancierNom(echeance.toSepaSddEcheancier().toSepaSddMandat().toSepaSddParam().teneurCompteNom());
			echeance.setDebiteurBic(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().toBanque().bic());
			echeance.setDebiteurIban(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().iban());
			echeance.setDebiteurId(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurPersonne().persId().toString());
			echeance.setDebiteurNom(echeance.toSepaSddEcheancier().toSepaSddMandat().toDebiteurPersonne().getNomAndPrenom());
		}
	}

	/**
	 * Crée le contenu du fichier de prélèvement.
	 * 
	 * @param recouvrement
	 * @param laDateValeur
	 * @param typeOperation
	 * @param echeances
	 * @return
	 * @throws Exception
	 */
	public NSDictionary creerContenuFichierSepaSDDForEcheances(EORecouvrement recouvrement, URL xsdFileUrl, NSTimestamp laDateValeur, String typeOperation, NSArray echeances) throws Exception {
		Integer recoNumero = recouvrement.recoNumero();

		Integer nbPaiementPourJournee = getNbRecouvrementJour(getEditingContext(), getSepaSddParam(), getDateCreation());
		nbPaiementPourJournee = Integer.valueOf(nbPaiementPourJournee.intValue() + 1);
		String numeroDeFichierDansLaJournee3Car = ZStringUtil.extendWithChars("" + nbPaiementPourJournee.intValue(), "0", 3, true);

		String identifiantFichierVirement = getIdentifiantFichierVirement(recoNumero);
		NSDictionary res = generePrelevementSepa(getEditingContext(), xsdFileUrl, getExercice().exeExercice().toString(), recoNumero.toString(), getSepaSddParam(), new NSTimestamp(new Date()),
				echeances, getMontantForEcheances(echeances), getNbTransactions(echeances), laDateValeur, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, typeOperation
				);

		return res;
	}

    protected String getIdentifiantFichierVirement(Integer recoNumero) throws Exception {
        if (recoNumero == null) {
            throw new Exception("Le numéro du recouvrement n'a pas été trouvé");
        }
        String str_start = "Prélèvement num. ";
        String str_start_short = "Prélèvement ";
        String str_date = " du " + ZConst.FORMAT_DATESHORT.format(getDateCreation());
        String identifiantFichierVirement = str_start + recoNumero + str_date;
        if (identifiantFichierVirement.length() > PaymentInformation.PMTINFID_MAX_LENGTH) {
            identifiantFichierVirement = str_start_short + recoNumero + str_date;
        }
        if (identifiantFichierVirement.length() > PaymentInformation.PMTINFID_MAX_LENGTH) {
            throw new Exception("La longueur de l'identifiant du fichier de virement est trop long : " + identifiantFichierVirement + " ("
                    + PaymentInformation.PMTINFID_MAX_LENGTH + " caractères maximum)");
        }
        return identifiantFichierVirement;
    }

	private final NSDictionary generePrelevementSepa(EOEditingContext edc, URL xsdFileUrl, String exeOrdre, String recoNumero, EOSepaSddParam sepaSddParam, NSTimestamp dateTimeCreation, NSArray lesEcheances, BigDecimal montantTotal,
			Integer nbTransactions, NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car,
			String identifiantFichierVirement, String typeOperation) throws Exception {

		try {
			EOSepaSddParam paramSepaSdd = sepaSddParam;
			ParametresSepa paramSepa = new ParametresSepa();
			paramSepa.setCompteDftIban(paramSepaSdd.dftIban());
			paramSepa.setCompteDftTitulaire(paramSepaSdd.dftTitulaire());
			paramSepa.setDevise(paramSepaSdd.devise());
			paramSepa.setEmetteurNom(paramSepaSdd.emetteurNom());
			paramSepa.setTgBic(paramSepaSdd.teneurCompteBic());
			paramSepa.setTgCodique(paramSepaSdd.teneurCompteCodique());
			paramSepa.setTgIban(paramSepaSdd.teneurCompteIban());
			paramSepa.setTgNom(paramSepaSdd.teneurCompteNom());
			paramSepa.setEmetteurTransfertId(paramSepaSdd.transfertId());
			paramSepa.setIcs(paramSepaSdd.creancierIcs());

			//			URL xsdLocationUrl = getMySession().getMyApplication().resourceManager().pathURLForResourceNamed(XSD_SDD_NAME, null, null);
			URL xsdLocationUrl = xsdFileUrl;
			if (xsdLocationUrl == null) {
				throw new Exception("Le fichier " + XSD_SDD_NAME + " n'a pas été trouvé dans les resources de l'application");
			}

			String listeCodesPaysSepa = (String) Application.application().config().get("org.cocktail.grhum.pays.zonesepa");

			SepaSDDFactory sepaFactory = new SepaSDDFactory(paramSepa, xsdLocationUrl.getPath(), exeOrdre, recoNumero, listeCodesPaysSepa);
			NSData d = sepaFactory.genereMessagePrelevement(dateTimeCreation, lesEcheances.immutableClone(), montantTotal, nbTransactions, dateExecutionDemandee, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, typeOperation);
			String s = ZStreamUtil.stringFromInputStream(d.stream(), "UTF-8");

			NSMutableDictionary res = new NSMutableDictionary();
			res.takeValueForKey(s, "contenu");
			res.takeValueForKey(nbTransactions, "nbTransactions");
			return res.immutableClone();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private final Integer getNbRecouvrementJour(EOEditingContext edc, EOSepaSddParam sepaSddParam, NSTimestamp dateCreation) throws Exception {
		try {
			//On recupere tous les fichiers de prélèvements effectués dans la journée sur le type sepa pour le compte spécifié
			NSArray res = EOPrelevementFichier.fetchAll(
					edc,
					new EOAndQualifier(new NSArray(new Object[] {
							new EOKeyValueQualifier(EOPrelevementFichier.TO_RECOUVREMENT_KEY + "." + EORecouvrement.RECO_DATE_CREATION_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, ZDateUtil.getDateOnly(dateCreation)),
							new EOKeyValueQualifier(EOPrelevementFichier.TO_RECOUVREMENT_KEY + "." + EORecouvrement.RECO_DATE_CREATION_KEY, EOQualifier.QualifierOperatorLessThan, ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateCreation), 1, 0, 0, 0)),
							new EOKeyValueQualifier(EOPrelevementFichier.TO_TYPE_RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, sepaSddParam.toTypeRecouvrement()),
							new EOKeyValueQualifier(EOPrelevementFichier.FICP_COMPTE_KEY, EOQualifier.QualifierOperatorEqual, sepaSddParam.dftIban())
					})), null);
			int i = res.count();
			return Integer.valueOf(i);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public EOComptabilite getComptabilite() {
		return comptabilite;
	}

	public EOJefyAdminExercice getExercice() {
		return exercice;
	}

	public EOJefyAdminUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public NSTimestamp getDateCreation() {
		return dateCreation;
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public EOSepaSddParam getSepaSddParam() {
		return sepaSddParam;
	}

	public EOPrelevementFichier creerEOPrelevementFichier(NSDictionary fichier, EORecouvrement recouvrement, NSTimestamp dateRemise, NSTimestamp dateReglement, String typeOperation) {
		final SepaSddPrelevementFichierFactory factorySauverVirement = new SepaSddPrelevementFichierFactory();
		return factorySauverVirement.creerPrelevementFichier(getEditingContext(), recouvrement, recouvrement.toTypeRecouvrement(), utilisateur,
				(String) fichier.valueForKey(FICHIER_CONTENU_KEY), dateCreation, dateRemise, dateReglement, "Fichier de prélèvement SEPA SDD", recouvrement.recoMontant(), recouvrement.recoNb(), (Integer) recouvrement.recoNumero(),
				typeOperation);
	}

	/**
	 * Enregistre le contenu du champ ficpContenu de prelemevementFichier dans le répartoire.
	 * 
	 * @param prelevementFichier
	 * @param directory
	 * @return
	 * @throws Exception
	 */
	public File enregistrerFichier(EOPrelevementFichier prelevementFichier, String directory) throws Exception {
		File f;

		// nommer le fichier en fonction du numéro 
		EOTypeRecouvrement typeVirement = prelevementFichier.toRecouvrement().toTypeRecouvrement();
		String typeShort = typeVirement.trecFormat();
		if (typeShort == null) {
			throw new DefaultClientException("Type de recouvrement non pris en charge.");
		}

		//nommer le fichier en fonction du numéro de virement
		String defaultFileName = "Fichier_" + typeShort + "PREL" + "_" + ZStringUtil.extendWithChars("" + prelevementFichier.toRecouvrement().recoNumero(), "0", 10, true) + ".xml";

		if (prelevementFichier.ficpContenu() == null || prelevementFichier.ficpContenu().length() == 0) {
			throw new DefaultClientException("Le contenu du fichier est vide !");
		}

		f = new File(directory, defaultFileName);
		f = new File(new File(directory, defaultFileName).getCanonicalPath());

		FileWriter writer = new FileWriter(f);
		writer.write(prelevementFichier.ficpContenu());
		writer.close();

		System.out.println("Fichier " + f + " enregistré.");
		return f;
	}

	public void changeEtatOfEcheancesAPrelevees(NSArray echeances) {
		SepaSddEcheanceHelper.getSharedInstance().changeEtatOfEcheancesAPreleve(echeances);
	}

	public void affecteTypeOperationOfEcheances(NSArray echeances, ISepaSddEcheance.TypeOperation typeOperation) {
		SepaSddEcheanceHelper.getSharedInstance().affecteTypeOperationOfEcheances(echeances, typeOperation);
	}

	public void checkEcheances(NSArray<ISepaSddEcheance> echeances, SepaSddEcheancesCombinaison combinaison) {
		if (ISepaSddEcheance.TypeOperation.FRST.equals(combinaison.getTypeOperation())) {
			SepaSddEcheanceRule.getSharedInstance().checkPlusieursEcheancesPourMemeMandat(echeances);
		}
	}

	/**
	 * @param echeancesRegroupements Les echeances regroupées par combinaison.
	 * @param xsdUrl URL d'accès au schema xsd de validation du message sepa ssd.
	 * @return Un dictionaire avec les clés {@link ISepaSddService#RECOUVREMENTS_GENEREES_GIDS_KEY} et {@link ISepaSddService#ERREURS_KEY}.
	 * @throws Exception
	 */
	public NSDictionary faireTraitement(Map<SepaSddEcheancesCombinaison, NSArray<EOSepaSddEcheance>> echeancesRegroupements, URL xsdUrl) throws Exception {
		String erreurs = null;
		try {
			Set<SepaSddEcheancesCombinaison> echeancesCombinaisons = echeancesRegroupements.keySet();
			NSMutableArray<EORecouvrement> recouvrementGeneres = new NSMutableArray<EORecouvrement>();
			Map<SepaSddEcheancesCombinaison, Exception> recouvrementExceptions = new HashMap<SepaSddEcheancesCombinaison, Exception>();

			if (getSepaSddParam() != null) {
				final EOComptabilite comptabilite = getComptabilite();
				final EOJefyAdminExercice exercice = getExercice();
				final NSTimestamp dateCreation = new NSTimestamp();

				for (Iterator<SepaSddEcheancesCombinaison> echeancesCombinaisonsIter = echeancesCombinaisons.iterator(); echeancesCombinaisonsIter.hasNext();) {
					SepaSddEcheancesCombinaison combinaison = (SepaSddEcheancesCombinaison) echeancesCombinaisonsIter.next();
					try {
						//On reserve un numero de recouvrement
						Integer numeroRecouvrement = getNextNumeroRecouvrement(comptabilite, exercice);
						NSArray echeances = echeancesRegroupements.get(combinaison);

						checkEcheances(echeances, combinaison);
						EORecouvrement recouvrement = creerRecouvrementForEcheances(echeances, numeroRecouvrement);

						memoriserInformationsDuMandatAuNiveauDesEcheances(echeances);

						//FIXME A verifier (date remise = date creation ?)
						NSTimestamp dateRemise = dateCreation;
						//doit etre la même pour les echeances contenues
						NSTimestamp dateReglement = new NSTimestamp(DateConversionUtil.sharedInstance().parseDateSilent(combinaison.getDatePrelevement()).toDate());
						String typeOperation = combinaison.getTypeOperation().toString();

						//Creation du contenu du fichier
						NSDictionary fichier = creerContenuFichierSepaSDDForEcheances(recouvrement, xsdUrl, dateReglement, typeOperation, echeancesRegroupements.get(combinaison));
						EOPrelevementFichier prelevementFichier = creerEOPrelevementFichier(fichier, recouvrement, dateRemise, dateReglement, typeOperation);

						//Mise à jour des echeances
						changeEtatOfEcheancesAPrelevees(echeances);
						affecteTypeOperationOfEcheances(echeances, combinaison.getTypeOperation());
						metAJourDatePrelevement(echeances, ZConst.FORMAT_DATE_ISO.format(dateReglement));

						//Création des écritures de prélèvement
						List<EOEcriture> ecrituresGenerees = new ArrayList<EOEcriture>();
						List<EOEmargement> emargementsGeneres = new ArrayList<EOEmargement>();
						creerEcrituresPourLesEcheances(echeances, getComptabilite().toGestion(), ecrituresGenerees, emargementsGeneres);

						//Enregistrement des données en base
						getEditingContext().saveChanges();

						recouvrementGeneres.addObject(recouvrement);

						//Numeroter les ecritures et les emargements
						EOEcritureFactory.getSharedInstance().numeroterEcrituresViaProcedureStockee(getEditingContext(), ecrituresGenerees);
						EOEmargementFactory.getSharedInstance().numeroterEmargementsViaProcedureStockee(getEditingContext(), emargementsGeneres);

						
						//balayer les origines des echeanciers pour notifier du fait que l'echeance a été prélevée
						notifierOrigineEntityForPrelevement(echeances);
						//Enregistrement des données en base
						getEditingContext().saveChanges();

						//

					} catch (Exception e) {
						e.printStackTrace();
						recouvrementExceptions.put(combinaison, e);
						//on annule les modifs qui ont généré l'exception mais on continue les autres regroupements
						getEditingContext().revert();
					}
				}

				if (recouvrementExceptions.size() > 0) {
					erreurs = "Il y a eu des erreurs lors de la génération des recouvrements suivant : ";
					for (SepaSddEcheancesCombinaison combinaison : recouvrementExceptions.keySet()) {
						erreurs += "\n[" + combinaison.getTypeOperation() + " " + combinaison.getDatePrevue() + "] : " + recouvrementExceptions.get(combinaison).getMessage();
					}
					erreurs += " \n Consultez les logs pour plus d'information.";
				}

			}
			else {
				throw new DefaultClientException("SepaSddParam non défini");
			}
			NSMutableDictionary resultat = new NSMutableDictionary();
			resultat.takeValueForKey(ERXEOControlUtilities.globalIDsForObjects(recouvrementGeneres), ISepaSddService.RECOUVREMENTS_GENEREES_GIDS_KEY);
			if (erreurs != null) {
				resultat.takeValueForKey(erreurs, ERREURS_KEY);
			}
			return resultat.immutableClone();
		} catch (Exception e) {
			getEditingContext().revert();
			e.printStackTrace();
			throw e;
		}
	}

	private void metAJourDatePrelevement(NSArray echeances, String dateOperation) {
		for (int i = 0; i < echeances.count(); i++) {
			EOSepaSddEcheance echeance = (EOSepaSddEcheance) echeances.objectAtIndex(i);
			echeance.setDPreleve(dateOperation);
		}

	}

	public void notifierOrigineEntityForPrelevement(NSArray echeances) {
		List<ISepaSddEcheance> echeancesList = WebObjectConversionUtil.asList(echeances);
		for (ISepaSddEcheance iSepaSddEcheance : echeancesList) {
			iSepaSddEcheance.echeancier().toSepaSddOrigine().toEntity().onEcheancePrelevee(iSepaSddEcheance);
		}
	}

	private void creerEcrituresPourLesEcheances(NSArray echeances, EOGestion gestionPourAgence, List<EOEcriture> ecrituresGenerees, List<EOEmargement> emargementsGeneres) throws Exception {
		PrelevementEcrituresFactory factory = new PrelevementEcrituresFactory();
		EOEcritureFactory ecrFactory = new EOEcritureFactory();
		
		for (int i = 0; i < echeances.count(); i++) {
			EOSepaSddEcheance echeance = (EOSepaSddEcheance) echeances.objectAtIndex(i);
			if (echeance.toSepaSddEcheancier().toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
    			EOEcritureDetail ecdEcheancePreleveeCredit = factory.creerEcritureDetailCreditPourEcheancePrelevee(getExercice(), Integer.valueOf(1), echeance);
    			EOEcritureDetail ecdEcheancePreleveeDebit = factory.creerEcritureDetailDebitPourEcheancePrelevee(getExercice(), gestionPourAgence, Integer.valueOf(2), echeance);
    			String ecrLibelle = factory.creerLibelleEcriturePrelevement(echeance); 
    			EOEcriture ecr = ecrFactory.creerEcriture(getEditingContext(), getDateCreation(), ecrLibelle, getComptabilite(), getExercice(), getTypeJournal(), getTypeOperation(), getUtilisateur());
    			ecr.addToToEcritureDetailsRelationship(ecdEcheancePreleveeCredit);
    			ecr.addToToEcritureDetailsRelationship(ecdEcheancePreleveeDebit);
    			ecrituresGenerees.add(ecr);
    			EOEmargement emargement = factory.creerEmargementEntreEcritureEcheanceEtEcritureEcheancier(getTypeEmargement(), getUtilisateur(), (EOSepaSddEcheancier) echeance.echeancier(), ecdEcheancePreleveeCredit);
    			emargementsGeneres.add(emargement);
			}
		}
		
	}

	public final Integer getNextNumeroRecouvrement(final EOComptabilite comptabilite, final EOJefyAdminExercice exercice) throws Exception {
		try {

			NSMutableDictionary dico = new NSMutableDictionary();
			dico.takeValueForKey(EOUtilities.primaryKeyForObject(getEditingContext(), comptabilite).valueForKey(EOComptabilite.COM_ORDRE_KEY), "001comordre");
			dico.takeValueForKey(EOUtilities.primaryKeyForObject(getEditingContext(), exercice).valueForKey(EOJefyAdminExercice.EXE_ORDRE_KEY), "002exeordre");
			NSDictionary res = EOUtilities.executeStoredProcedureNamed(getEditingContext(), "FwkCktlCompta_numerotationObject.next_numero_recouvrement", dico);
			return (Integer) res.valueForKey("000result");

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public EOTypeEmargement getTypeEmargement() {
		return typeEmargement;
	}

	public EOTypeJournal getTypeJournal() {
		return typeJournal;
	}

	public EOTypeOperation getTypeOperation() {
		return typeOperation;
	}

}
