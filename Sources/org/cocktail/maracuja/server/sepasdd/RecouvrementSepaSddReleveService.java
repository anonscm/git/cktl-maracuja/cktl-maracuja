package org.cocktail.maracuja.server.sepasdd;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlcompta.common.helpers.EcritureDetailHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheanceHelper;
import org.cocktail.fwkcktlcompta.common.util.WebObjectConversionUtil;
import org.cocktail.fwkcktlcompta.server.factories.EOEcritureFactory;
import org.cocktail.fwkcktlcompta.server.factories.EOEmargementFactory;
import org.cocktail.fwkcktlcompta.server.factories.PlancoExerBeCache;
import org.cocktail.fwkcktlcompta.server.factories.PrelevementEcrituresFactory;
import org.cocktail.fwkcktlcompta.server.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.server.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.server.metier.EOEcritureDetail;
import org.cocktail.fwkcktlcompta.server.metier.EOEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOGestion;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.server.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeEmargement;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeJournal;
import org.cocktail.fwkcktlcompta.server.metier.EOTypeOperation;
import org.cocktail.maracuja.common.services.ISepaSddService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;

/**
 * Services pour la gestion des confirmations ou rejets d'échéances.
 * 
 * @author rprin
 */
public class RecouvrementSepaSddReleveService implements ISepaSddService {
	private final EOComptabilite comptabilite;
	private final EOJefyAdminExercice exercice;
	private final EOJefyAdminUtilisateur utilisateur;
	private final NSTimestamp dateCreation;
	private final EOEditingContext editingContext;
	private final EOTypeEmargement typeEmargement;
	private final EOTypeJournal typeJournal;
	private final EOTypeOperation typeOperation;

	/**
	 * @param editingContext
	 * @param comptabilite
	 * @param exercice
	 * @param utilisateur
	 * @param dateCreation
	 * @param sepaSddParam
	 * @param typeEmargement TypeEmargement à utiliser pour les émargements entre écriture de prélèvement et ecriture de prise en charge echeancier.
	 */
	public RecouvrementSepaSddReleveService(EOEditingContext editingContext, EOComptabilite comptabilite, EOJefyAdminExercice exercice, EOJefyAdminUtilisateur utilisateur, NSTimestamp dateCreation,
			EOTypeEmargement typeEmargement, EOTypeJournal typeJournal, EOTypeOperation typeOperation) {
		this.editingContext = editingContext;
		this.comptabilite = comptabilite;
		this.exercice = exercice;
		this.utilisateur = utilisateur;
		this.dateCreation = dateCreation;
		this.typeEmargement = typeEmargement;
		this.typeJournal = typeJournal;
		this.typeOperation = typeOperation;
	}

	public EOComptabilite getComptabilite() {
		return comptabilite;
	}

	public EOJefyAdminExercice getExercice() {
		return exercice;
	}

	public EOJefyAdminUtilisateur getUtilisateur() {
		return utilisateur;
	}

	public NSTimestamp getDateCreation() {
		return dateCreation;
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}




	public void changeEtatOfEcheancesAConfirme(NSArray echeances) {
		SepaSddEcheanceHelper.getSharedInstance().changeEtatOfEcheancesAConfirme(echeances);
	}

	public void changeEtatOfEcheancesARejete(NSArray echeances) {
		SepaSddEcheanceHelper.getSharedInstance().changeEtatOfEcheancesARejete(echeances);
	}

	private void traiterRejets(NSArray<EOSepaSddEcheance> echeances, Boolean genererEcritures, EOPlanComptableExer pcoDebitForRejet, List<EOEcriture> ecrituresGenerees, List<EOEmargement> emargementsGeneres, StringWriter swMsg) throws Exception {
		if (genererEcritures) {
			creerEcrituresEtEmargementEventuelPourLesEcheances(echeances, ecrituresGenerees, emargementsGeneres, pcoDebitForRejet, ISepaSddEcheance.Etat.REJETE, swMsg);
		}
		changeEtatOfEcheancesARejete(echeances);
		notifierOrigineEntityForRejet(echeances);
	}

	private void traiterConfirmes(NSArray<EOSepaSddEcheance> echeances, Boolean genererEcritures, EOPlanComptableExer pcoDebitForConfirme, List<EOEcriture> ecrituresGenerees, List<EOEmargement> emargementsGeneres, StringWriter swMsg) throws Exception {
		if (genererEcritures) {
			creerEcrituresEtEmargementEventuelPourLesEcheances(echeances, ecrituresGenerees, emargementsGeneres, pcoDebitForConfirme, ISepaSddEcheance.Etat.CONFIRME, swMsg);
		}
		changeEtatOfEcheancesAConfirme(echeances);
	}



	/**
	 * @param echeancesParEtat
	 * @param pcoForRejet
	 * @param pcoForConfirme Compte à utiliser en débit pour la création des écritures de confirmation.
	 * @param genererEcritures
	 * @return Un dictionaire avec les clés {@link ISepaSddService#ECRITURES_GENEREES_GIDS_KEY}, {@link ISepaSddService#EMARGEMENTS_GENEREES_GIDS_KEY}
	 *         et {@link ISepaSddService#ERREURS_KEY}.
	 * @throws Exception
	 */
	public NSDictionary<String, Object> faireTraitement(Map<ISepaSddEcheance.Etat, NSArray<EOSepaSddEcheance>> echeancesParEtat, EOPlanComptableExer pcoForRejet, EOPlanComptableExer pcoForConfirme, Boolean genererEcritures) throws Exception {
		String erreurs = null;
		List<EOEcriture> ecrituresGenerees = new ArrayList<EOEcriture>();
		List<EOEmargement> emargementsGeneres = new ArrayList<EOEmargement>();
		List<Exception> exceptions = new ArrayList<Exception>();
		StringWriter swMsg = new StringWriter();
		try {
			Set<ISepaSddEcheance.Etat> etats = echeancesParEtat.keySet();
				final EOComptabilite comptabilite = getComptabilite();
				final EOJefyAdminExercice exercice = getExercice();
				final NSTimestamp dateCreation = new NSTimestamp();

				for (Iterator<ISepaSddEcheance.Etat> etatIter = etats.iterator(); etatIter.hasNext();) {
					ISepaSddEcheance.Etat etat = etatIter.next();
					try {
						NSArray echeances = echeancesParEtat.get(etat);
					
					List<EOEcriture> ecrituresGenereesTmp = new ArrayList<EOEcriture>();
					List<EOEmargement> emargementsGeneresTmp = new ArrayList<EOEmargement>();

						if (ISepaSddEcheance.Etat.REJETE.equals(etat)) {
						traiterRejets(echeances, genererEcritures, pcoForRejet, ecrituresGenereesTmp, emargementsGeneresTmp, swMsg);
						}
						else if (ISepaSddEcheance.Etat.CONFIRME.equals(etat)) {
						traiterConfirmes(echeances, genererEcritures, pcoForConfirme, ecrituresGenereesTmp, emargementsGeneresTmp, swMsg);
						}

					//Enregistrement des données en base
					getEditingContext().saveChanges();

					ecrituresGenerees.addAll(ecrituresGenereesTmp);
					emargementsGeneres.addAll(emargementsGeneresTmp);
						//Numeroter les ecritures et les emargements
					EOEcritureFactory.getSharedInstance().numeroterEcrituresViaProcedureStockee(getEditingContext(), ecrituresGenereesTmp);
					EOEmargementFactory.getSharedInstance().numeroterEmargementsViaProcedureStockee(getEditingContext(), emargementsGeneresTmp);

					} catch (Exception e) {
						e.printStackTrace();
						exceptions.add(e);
						getEditingContext().revert();
					}
				}

				if (exceptions.size() > 0) {
					erreurs = "Il y a eu des erreurs lors de la génération des écritures de rejet ou de confirmation d'échéances : ";
				for (Exception exception : exceptions) {
					erreurs += "\n" + exception.getMessage();
					}
					erreurs += " \n Consultez les logs pour plus d'information.";
				}


			NSMutableDictionary<String, Object> resultat = new NSMutableDictionary<String, Object>();
			resultat.takeValueForKey(ERXEOControlUtilities.globalIDsForObjects(WebObjectConversionUtil.asRawNSArray(ecrituresGenerees)), ECRITURES_GENEREES_GIDS_KEY);
			resultat.takeValueForKey(ERXEOControlUtilities.globalIDsForObjects(WebObjectConversionUtil.asRawNSArray(emargementsGeneres)), EMARGEMENTS_GENEREES_GIDS_KEY);
			if (erreurs != null) {
				resultat.takeValueForKey(erreurs, ERREURS_KEY);
			}
			if (swMsg.toString().length() > 0) {
				resultat.takeValueForKey(erreurs, MSGS_KEY);
			}

			return resultat.immutableClone();
		} catch (Exception e) {
			getEditingContext().revert();
			e.printStackTrace();
			throw e;
		}
	}

	public void notifierOrigineEntityForRejet(NSArray echeances) {
		List<ISepaSddEcheance> echeancesList = WebObjectConversionUtil.asList(echeances);
		for (ISepaSddEcheance iSepaSddEcheance : echeancesList) {
			iSepaSddEcheance.echeancier().toSepaSddOrigine().toEntity().onEcheanceRejetee(iSepaSddEcheance);
		}
	}

	public void creerEcrituresEtEmargementEventuelPourLesEcheances(NSArray echeances, List<EOEcriture> ecrituresGenerees, List<EOEmargement> emargementsGeneres, EOPlanComptableExer pcoDebit, ISepaSddEcheance.Etat etat, StringWriter swMsg) throws Exception {
		PrelevementEcrituresFactory factory = new PrelevementEcrituresFactory();
		EOEcritureFactory ecrFactory = new EOEcritureFactory();

		for (int i = 0; i < echeances.count(); i++) {
			EOSepaSddEcheance echeance = (EOSepaSddEcheance) echeances.objectAtIndex(i);
			//recuperer l'ecriture detail de prelevement
			EOEcritureDetail ecdDebitPrelevement = (EOEcritureDetail) SepaSddEcheanceHelper.getSharedInstance().getLastEcritureDetailDebit(echeance);
			EOGestion gestion = ecdDebitPrelevement.toGestion();
			EOPlanComptableExer pco = PlancoExerBeCache.getSharedInstance().getPlancomptableExerOuBe(editingContext, ecdDebitPrelevement.toPlanComptableExer(), getExercice());

			EOEcritureDetail ecdEcheanceCredit = factory.creerEcritureDetailCreditPourEcheanceRejeteeOuConfirmee(getExercice(), Integer.valueOf(1), echeance, gestion, pco);
			EOEcritureDetail ecdEcheanceDebit = factory.creerEcritureDetailDebitPourEcheanceRejeteeOuConfirmee(getExercice(), Integer.valueOf(2), echeance, gestion, pcoDebit);

			String ecrLibelle = factory.creerLibelleEcriturePrelevement(echeance) + " : " + etat.toString();
			EOEcriture ecr = ecrFactory.creerEcriture(editingContext, getDateCreation(), ecrLibelle, getComptabilite(), getExercice(), getTypeJournal(), getTypeOperation(), getUtilisateur());
			ecr.addToToEcritureDetailsRelationship(ecdEcheanceCredit);
			ecr.addToToEcritureDetailsRelationship(ecdEcheanceDebit);
			ecrituresGenerees.add(ecr);

			if (ecdDebitPrelevement.ecdResteEmarger().equals(ecdDebitPrelevement.ecdMontant().abs())) {
				EOEmargement emargement = EOEmargementFactory.getSharedInstance().emargerEcritureDetailD1C1(ecdEcheanceCredit.editingContext(), getUtilisateur(), getTypeEmargement(), ecdEcheanceCredit.toExercice(), ecdEcheanceCredit, ecdDebitPrelevement, Integer.valueOf(0),
						ecdEcheanceCredit.toEcriture().toComptabilite(), ecdEcheanceCredit.ecdMontant());
				emargementsGeneres.add(emargement);
			}
			else {
				swMsg.append("Emargement non généré : L'écriture suivant eest déjà émargée : " + EcritureDetailHelper.getSharedInstance().getLibelleComplet(ecdDebitPrelevement));
			}
		}
	}

	public EOTypeEmargement getTypeEmargement() {
		return typeEmargement;
	}

	public EOTypeJournal getTypeJournal() {
		return typeJournal;
	}

	public EOTypeOperation getTypeOperation() {
		return typeOperation;
	}

}
