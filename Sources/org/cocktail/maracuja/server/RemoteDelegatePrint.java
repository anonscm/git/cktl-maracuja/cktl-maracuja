package org.cocktail.maracuja.server;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.server.remotes.CktlComptaRemoteDelegatePrint;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.maracuja.server.factory.FichierBDFUtils;
import org.cocktail.maracuja.server.finders.ZFinder;
import org.cocktail.maracuja.server.metier.EOPrelevementFichier;
import org.cocktail.maracuja.server.metier.EOVirementFichier;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/**
 * Gestion des appels distants (clients) concernant les impressions.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegatePrint extends CktlComptaRemoteDelegatePrint {
	public final static Logger logger = Logger.getLogger(RemoteDelegatePrint.class);
	private static final String CONST_REPORTS_LOCATION = "REPORTS_LOCATION";
	private static final String CONST_CONTENTS_RESOURCES = "/Contents/Resources/reports/";
	/** Les constantes sont aussi definies coté client dans ReportFactoryClient.java */
	private final static String JASPERFILENAME_DETAIL_VIREMENT_BDF = "detail_fichier_virement_bdf.jasper";
	private final static String JASPERFILENAME_DETAIL_VIREMENT_SEPA = "detail_fichier_virement_sepa.jasper";
	private final static String JASPERFILENAME_BORDEREAU_DISQUETTE = "b_disquette.jasper";
	private final static String JASPERFILENAME_BORDEREAU_SEPA_SCT = "bordereau_sepa_sct.jasper";
	private final static String JASPERFILENAME_BORDEREAU_SEPA_SDD = "bordereau_sepa_sdd.jasper";
	private final static String JASPERFILENAME_DETAIL_PRELEVEMENT_SEPA = "detail_fichier_prelevement_sepa.jasper";
	
	public RemoteDelegatePrint(Session session) {
		super(session);
	}
	public String getReportsLocation() {
		if ((getMyApplication().config().stringForKey(CONST_REPORTS_LOCATION) != null) && (getMyApplication().config().stringForKey(CONST_REPORTS_LOCATION).length() > 0)) {
			return getMyApplication().config().stringForKey(CONST_REPORTS_LOCATION);
		}
		final String aPath = getMyApplication().path().concat(CONST_CONTENTS_RESOURCES);
		logger.debug("Application.getReportsLocation() : ".concat(aPath));
		return aPath;
	}

	public String getRealJasperFileName(String rname) {
		String loc = null;
		EOEnterpriseObject obj = ZFinder.fetchObject(getMySession().defaultEditingContext(), "Report", "repId=%@", new NSArray(new Object[] {
			rname
		}), null, true);
		if (obj != null) {
			loc = (String) obj.valueForKey("repLocation");
			if (loc != null) {
				logger.debug("Report " + rname + " defini dans la table REPORT : " + loc);
				return loc;
			}
		}

		if (loc == null) {
			logger.debug("Report " + rname + " NON defini dans la table REPORT, utilisation du report par defaut");
			loc = getReportsLocation() + rname;
		}
		return loc;
	}
	
	@Override
	public NSData clientSideRequestPrintAndWait(String idreport, String sqlQuery, NSDictionary parametres) throws Exception {
		resetReporters();
		try {
			Map<String, Object> params = new HashMap<String, Object>();
			if (parametres != null) {
				params = new HashMap<String, Object>(parametres.hashtable());
				System.out.println("parametres : " + params);
			}
			String jasperId = idreport;
			final String realJasperFilName = getRealJasperFileName(jasperId);
			NSData res = null;
			if (JASPERFILENAME_DETAIL_VIREMENT_SEPA.equals(idreport)) {
				res = printReportForVirementSEPAXml(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			else if (JASPERFILENAME_BORDEREAU_SEPA_SCT.equals(idreport)) {
				res = printReportForVirementSEPAXml(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			else if (JASPERFILENAME_DETAIL_PRELEVEMENT_SEPA.equals(idreport)) {
				res = printReportForPrelevementSEPAXml(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			else if (JASPERFILENAME_BORDEREAU_SEPA_SDD.equals(idreport)) {
				res = printReportForPrelevementSEPAXml(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			else if (JASPERFILENAME_DETAIL_VIREMENT_BDF.equals(idreport)) {
				res = printDetailFichierVirementBDF(idreport, getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			else if (JASPERFILENAME_BORDEREAU_DISQUETTE.equals(idreport)) {
				res = printBordereauDisquetteReport(getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			else {
				reporter = new JrxmlReporter();
				res = reporter.printNow(getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, false, true, myListener);
			}
			return res;

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	
	
	protected InputStream createInputStreamFromString(String s) throws UnsupportedEncodingException {
		return new ByteArrayInputStream(s.getBytes("UTF-8"));
	}
	
	private NSData printReportForVirementSEPAXml(String idReport, Connection jdbcConnection, String sqlQuery, String realJasperFilName, Map<String, Object> params, int exportFormatPdf, boolean printIfEmpty, boolean tryCompilationWhenError, MyListener myListener) throws Throwable {
		Integer paiOrdre = (Integer) params.get("PAI_ORDRE");
		NSArray<EOVirementFichier> virements = EOVirementFichier.fetchAll(getMySession().defaultEditingContext(), new EOKeyValueQualifier(EOVirementFichier.PAI_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, paiOrdre), new NSArray(new Object[] {
				EOVirementFichier.SORT_DATE_CREATION_DESC
		}));

		if (virements.count() == 0) {
			throw new Exception("Contenu du fichier de virement non trouvé pour pai_ordre=" + paiOrdre);
		}
		String contenuFichier = ((EOVirementFichier) virements.objectAtIndex(0)).virContenu();
		InputStream xmlDataSource = createInputStreamFromString(contenuFichier);
		if (myListener != null) {
			myListener.afterDataSourceCreated();
		}
		reporter = new JrxmlReporterWithXmlDataSource();
		return ((JrxmlReporterWithXmlDataSource) reporter).printNow(idReport, xmlDataSource, "/Document", realJasperFilName, params, exportFormatPdf, printIfEmpty, tryCompilationWhenError, myListener);
	}

	private NSData printBordereauDisquetteReport(Connection jdbcConnection, String sqlQuery, String realJasperFilName, Map<String, Object> params, int exportFormatPdf, boolean b, boolean c, MyListener myListener) throws Throwable {
		String DEFAULT_SQL_QUERY = "select 1 from dual";
		if (sqlQuery == null) {
			sqlQuery = DEFAULT_SQL_QUERY;
			;
		}
		reporter = new JrxmlReporter();
		return reporter.printNow(getJDBCConnection(), sqlQuery, realJasperFilName, params, JrxmlReporter.EXPORT_FORMAT_PDF, true, true, myListener);
	}

	private NSData printReportForPrelevementSEPAXml(String idReport, Connection jdbcConnection, String sqlQuery, String realJasperFilName, Map<String, Object> params, int exportFormatPdf, boolean printIfEmpty, boolean tryCompilationWhenError, MyListener myListener) throws Throwable {
		Integer recoOrdre = (Integer) params.get("RECO_ORDRE");
		NSArray virements = EOPrelevementFichier.fetchAll(getMySession().defaultEditingContext(), new EOKeyValueQualifier(EOPrelevementFichier.RECO_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, recoOrdre), new NSArray(new Object[] {
				EOPrelevementFichier.SORT_DATE_CREATION_DESC
		}));

		if (virements.count() == 0) {
			throw new Exception("Contenu du fichier de prélèvement non trouvé pour reco_ordre=" + recoOrdre);
		}
		String contenuFichier = ((EOPrelevementFichier) virements.objectAtIndex(0)).ficpContenu();
		InputStream xmlDataSource = createInputStreamFromString(contenuFichier);
		if (myListener != null) {
			myListener.afterDataSourceCreated();
		}
		reporter = new JrxmlReporterWithXmlDataSource();
		return ((JrxmlReporterWithXmlDataSource) reporter).printNow(idReport, xmlDataSource, "/Document", realJasperFilName, params, exportFormatPdf, printIfEmpty, tryCompilationWhenError, myListener);

	}


	private NSData printDetailFichierVirementBDF(String idReport, Connection jdbcConnection, String sqlQuery, String realJasperFilName, Map<String, Object> params, int exportFormatPdf, boolean printIfEmpty, boolean tryCompilationWhenError, MyListener myListener) throws Throwable {
		StringWriter sw = FichierBDFUtils.createXmlForFichierBDFVirement(getMySession().defaultEditingContext(), getMyApplication().mainModelName(), (Number) params.get("PAI_ORDRE"));
		InputStream xmlDataSource = createInputStreamFromString(sw.toString());
		if (myListener != null) {
			myListener.afterDataSourceCreated();
		}
		reporter = new JrxmlReporterWithXmlDataSource();
		return ((JrxmlReporterWithXmlDataSource) reporter).printNow(idReport, xmlDataSource, "/virements/virement", realJasperFilName, params, exportFormatPdf, printIfEmpty, tryCompilationWhenError, myListener);
		//return new DefaultReport(jasperFileName, sw, "/virements/virement", parameters, listener);
	}
	//	//
	//	public static final DefaultReport createBordereauDisquetteReport(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		String DEFAULT_SQL_QUERY = "";
	//
	//		if (sqlQuery == null) {
	//			sqlQuery = DEFAULT_SQL_QUERY;
	//		}
	//		if (wantTrace) {
	//			System.out.println();
	//			System.out.println("Generation d'un report");
	//		}
	//		return new DefaultReport(jasperFileName, (StringWriter) null, null, parameters, listener);
	//	}
	//
	//	public static final DefaultReport createBordereauSEPASct(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		Integer paiOrdre = (Integer) parameters.get("PAI_ORDRE");
	//		NSArray virements = EOVirementFichier.fetchAll(ec, new EOKeyValueQualifier(EOVirementFichier.PAI_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, paiOrdre), new NSArray(new Object[] {
	//				EOVirementFichier.SORT_DATE_CREATION_DESC
	//		}));
	//
	//		if (virements.count() == 0) {
	//			throw new Exception("Contenu du fichier de virement non trouvé pour pai_ordre=" + paiOrdre);
	//		}
	//		String contenuFichier = ((EOVirementFichier) virements.objectAtIndex(0)).virContenu();
	//		StringWriter sw = new StringWriter();
	//		sw.write(contenuFichier);
	//
	//		reporter = new JrxmlReporter();
	//
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		return new DefaultReport(jasperFileName, sw, "/Document", parameters, listener);
	//	}
	//
	//	public static final DefaultReport createDetailFichierVirementBDF(final boolean wantTrace, final EOEditingContext ec, final String modelName, String SqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		StringWriter sw = FichierBDFUtils.createXmlForFichierBDFVirement(ec, modelName, (Number) parameters.get("PAI_ORDRE"));
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		if (wantTrace) {
	//			try {
	//				saveInTempFile(sw, "virements" + "_temp.xml");
	//			} catch (Exception e) {
	//				System.out.println("Impossible de creer le fichier " + "virements" + "_temp.xml");
	//				System.out.println(sw.toString());
	//			}
	//		}
	//		return new DefaultReport(jasperFileName, sw, "/virements/virement", parameters, listener);
	//	}
	//
	//	public static final DefaultReport createDetailFichierVirementSEPA(final boolean wantTrace, final EOEditingContext ec, final String modelName, String SqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		Integer paiOrdre = (Integer) parameters.get("PAI_ORDRE");
	//		NSArray virements = EOVirementFichier.fetchAll(ec, new EOKeyValueQualifier(EOVirementFichier.PAI_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, paiOrdre), new NSArray(new Object[] {
	//				EOVirementFichier.SORT_DATE_CREATION_DESC
	//		}));
	//
	//		if (virements.count() == 0) {
	//			throw new Exception("Contenu du fichier de virement non trouvé pour pai_ordre=" + paiOrdre);
	//		}
	//		String contenuFichier = ((EOVirementFichier) virements.objectAtIndex(0)).virContenu();
	//		StringWriter sw = new StringWriter();
	//		sw.write(contenuFichier);
	//
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		return new DefaultReport(jasperFileName, sw, "/Document", parameters, listener);
	//	}
	//
	//	public static final DefaultReport createReportForPrelevementSEPAXml(final boolean wantTrace, final EOEditingContext ec, final String modelName, String SqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		Integer recoOrdre = (Integer) parameters.get("RECO_ORDRE");
	//		NSArray virements = EOPrelevementFichier.fetchAll(ec, new EOKeyValueQualifier(EOPrelevementFichier.RECO_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, recoOrdre), new NSArray(new Object[] {
	//				EOPrelevementFichier.SORT_DATE_CREATION_DESC
	//		}));
	//
	//		if (virements.count() == 0) {
	//			throw new Exception("Contenu du fichier de prélèvement non trouvé pour reco_ordre=" + recoOrdre);
	//		}
	//		String contenuFichier = ((EOPrelevementFichier) virements.objectAtIndex(0)).ficpContenu();
	//		StringWriter sw = new StringWriter();
	//		sw.write(contenuFichier);
	//
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		return new DefaultReport(jasperFileName, sw, "/Document", parameters, listener);
	//	}
	//
	//	public static final DefaultReport createModePaiementReport(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		String ROOT_TAG = "modepaiements";
	//		String ELEMENT_TAG = "modepaiement";
	//		final String XML_RECORD_PATH = ROOT_TAG + "/" + ELEMENT_TAG;
	//		String DEFAULT_SQL_QUERY = "select * from MARACUJA.mode_paiement order by mode_paiement";
	//
	//		if (sqlQuery == null) {
	//			sqlQuery = DEFAULT_SQL_QUERY;
	//		}
	//		if (wantTrace) {
	//			System.out.println();
	//			System.out.println("Generation d'un report");
	//			System.out.println("SQL = " + sqlQuery);
	//		}
	//		StringWriter sw = createXMLDataSourceFromSqlQuery(ec, modelName, sqlQuery, ELEMENT_TAG, ROOT_TAG);
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		if (wantTrace) {
	//			try {
	//				saveInTempFile(sw, ROOT_TAG + "_temp.xml");
	//			} catch (Exception e) {
	//				System.out.println("Impossible de creer le fichier " + ROOT_TAG + "_temp.xml");
	//				System.out.println(sw.toString());
	//			}
	//		}
	//		return new DefaultReport(jasperFileName, sw, XML_RECORD_PATH, parameters, listener);
	//	}
	//
	//	public static final DefaultReport createPlanComptableReport(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		String ROOT_TAG = "plancomptables";
	//		String ELEMENT_TAG = "plancomptable";
	//		final String XML_RECORD_PATH = ROOT_TAG + "/" + ELEMENT_TAG;
	//		String DEFAULT_SQL_QUERY = "select * from MARACUJA.plan_comptable order by pco_num";
	//
	//		if (sqlQuery == null) {
	//			sqlQuery = DEFAULT_SQL_QUERY;
	//		}
	//		if (wantTrace) {
	//			System.out.println();
	//			System.out.println("Generation d'un report");
	//			System.out.println("SQL = " + sqlQuery);
	//		}
	//		StringWriter sw = createXMLDataSourceFromSqlQuery(ec, modelName, sqlQuery, ELEMENT_TAG, ROOT_TAG);
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		if (wantTrace) {
	//			try {
	//				saveInTempFile(sw, ROOT_TAG + "_temp.xml");
	//			} catch (Exception e) {
	//				System.out.println("Impossible de creer le fichier " + ROOT_TAG + "_temp.xml");
	//				System.out.println(sw.toString());
	//			}
	//		}
	//		return new DefaultReport(jasperFileName, sw, XML_RECORD_PATH, parameters, listener);
	//	}
	//
	//	public static final DefaultReport createUtilisateurReport(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		String ROOT_TAG = "utilisateurs";
	//		String ELEMENT_TAG = "utilisateur";
	//		final String XML_RECORD_PATH = ROOT_TAG + "/" + ELEMENT_TAG;
	//		String DEFAULT_SQL_QUERY = "select * from MARACUJA.utilisateur order by utl_nom, utl_prenom";
	//
	//		if (sqlQuery == null) {
	//			sqlQuery = DEFAULT_SQL_QUERY;
	//		}
	//		if (wantTrace) {
	//			System.out.println();
	//			System.out.println("Generation d'un report");
	//			System.out.println("SQL = " + sqlQuery);
	//		}
	//		StringWriter sw = createXMLDataSourceFromSqlQuery(ec, modelName, sqlQuery, ELEMENT_TAG, ROOT_TAG);
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		if (wantTrace) {
	//			try {
	//				saveInTempFile(sw, ROOT_TAG + "_temp.xml");
	//			} catch (Exception e) {
	//				System.out.println("Impossible de creer le fichier " + ROOT_TAG + "_temp.xml");
	//				System.out.println(sw.toString());
	//			}
	//		}
	//		return new DefaultReport(jasperFileName, sw, XML_RECORD_PATH, parameters, listener);
	//	}
	//
	//	public static final DefaultReport createDefaultReport(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener, final String rootTag, final String elementTag) throws Exception {
	//		return createDefaultReport(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener, rootTag, elementTag, true, true, true);
	//	}
	//
	//	public static final DefaultReport createDefaultReport(final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener, final String rootTag, final String elementTag,
	//			final boolean flat, final boolean compact, final boolean escapeCarriageReturn) throws Exception {
	//		String ROOT_TAG = rootTag;
	//		String ELEMENT_TAG = elementTag;
	//		final String XML_RECORD_PATH = ROOT_TAG + "/" + ELEMENT_TAG;
	//		if (wantTrace) {
	//			System.out.println();
	//			System.out.println("Generation d'un report");
	//			System.out.println("SQL = " + sqlQuery);
	//		}
	//		StringWriter sw = createXMLDataSourceFromSqlQuery(ec, modelName, sqlQuery, ELEMENT_TAG, ROOT_TAG, flat, compact, escapeCarriageReturn);
	//		if (listener != null) {
	//			listener.afterDataSourceCreated();
	//		}
	//		if (wantTrace) {
	//			try {
	//				saveInTempFile(sw, ROOT_TAG + "_temp.xml");
	//			} catch (Exception e) {
	//				System.out.println("Impossible de creer le fichier " + ROOT_TAG + "_temp.xml");
	//				System.out.println(sw.toString());
	//			}
	//		}
	//		return new DefaultReport(jasperFileName, sw, XML_RECORD_PATH, parameters, listener);
	//	}
	//
	//	public static final DefaultReport createDefaultReport(final boolean wantTrace, Connection connection, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		if (wantTrace) {
	//			System.out.println();
	//			System.out.println("Generation d'un report avec connection JDBC");
	//			System.out.println("SQL = " + sqlQuery);
	//		}
	//		return new DefaultReport(jasperFileName, connection, sqlQuery, parameters, listener);
	//	}
	//
	//	public static final DefaultReport createReport(final Connection connection, final String idreport, final boolean wantTrace, final EOEditingContext ec, final String modelName, String sqlQuery, final String jasperFileName, final Map parameters, IDefaultReportListener listener) throws Exception {
	//		if (DefaultReport.JASPERFILENAME_DETAIL_VIREMENT_BDF.equals(idreport)) {
	//			return DefaultReport.createDetailFichierVirementBDF(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//		else if (DefaultReport.JASPERFILENAME_DETAIL_VIREMENT_SEPA.equals(idreport)) {
	//			return DefaultReport.createDetailFichierVirementSEPA(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//		else if (DefaultReport.JASPERFILENAME_DETAIL_PRELEVEMENT_SEPA.equals(idreport)) {
	//			return DefaultReport.createReportForPrelevementSEPAXml(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//		else if (DefaultReport.JASPERFILENAME_BORDEREAU_DISQUETTE.equals(idreport)) {
	//			return DefaultReport.createBordereauDisquetteReport(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//		else if (DefaultReport.JASPERFILENAME_BORDEREAU_SEPA_SCT.equals(idreport)) {
	//			return DefaultReport.createBordereauSEPASct(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//		else if (DefaultReport.JASPERFILENAME_BORDEREAU_SEPA_SDD.equals(idreport)) {
	//			return DefaultReport.createReportForPrelevementSEPAXml(wantTrace, ec, modelName, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//		else {
	//			return DefaultReport.createDefaultReport(wantTrace, connection, sqlQuery, jasperFileName, parameters, listener);
	//		}
	//	}
	//
	//	public static final void saveInTempFile(StringWriter sw, final String fileName) throws Exception {
	//		String temporaryDir = System.getProperty("java.io.tmpdir");
	//		if (!temporaryDir.endsWith(File.separator)) {
	//			temporaryDir = temporaryDir + File.separator;
	//		}
	//		File f = new File(temporaryDir + fileName);
	//		if (f.exists()) {
	//			f.delete();
	//		}
	//		if (f.createNewFile()) {
	//			new FileWriter(f);
	//			BufferedWriter out = new BufferedWriter(new FileWriter(f));
	//			out.write(sw.toString());
	//			out.close();
	//		}
	//		System.out.println("Le fichier " + f.getAbsolutePath() + " a ete cree.");
	//	}


	
	
	
}
