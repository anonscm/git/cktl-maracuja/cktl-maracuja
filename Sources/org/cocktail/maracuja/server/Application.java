/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.helpers.ParamHelper;
import org.cocktail.fwkcktlcompta.server.metier.EOCoriandreInscription;
import org.cocktail.fwkcktlcompta.server.metier.EOLitchiEnteteFacture;
import org.cocktail.fwkcktlcompta.server.metier.EOScolarixInscription;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.server.CktlERXStaticResourceRequestHandler;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.maracuja.server.ctrl.UserAuthenticationEngine;
import org.cocktail.maracuja.server.ctrl.UserAuthenticationEngineLogin;
import org.cocktail.maracuja.server.ctrl.UserAuthenticationEngineSaut;
import org.cocktail.maracuja.server.ctrl.UserAuthenticationEngineSql;
import org.cocktail.maracuja.server.database.MyDatabaseContextDelegate;
import org.cocktail.maracuja.server.finders.ZFinder;
import org.cocktail.maracuja.server.metier.EOFonction;
import org.cocktail.zutil.server.ZDateUtil;
import org.cocktail.zutil.server.logging.ZBufferedLogger;
import org.cocktail.zutil.server.logging.ZLogger;
import org.cocktail.zutil.server.zwebapp.ZApplication;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.woinject.WOInject;

import er.extensions.eof.ERXEC;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class Application extends ZApplication {
	private static final String CONST_REPORTS_LOCATION = "REPORTS_LOCATION";
	private static final String CONST_CONTENTS_RESOURCES = "/Contents/Resources/reports/";
	private static final String CONST_LOG = "log_";
	private static final String CONST_SERVER = "_server_";
	private static final String CONST_1 = "1";
	public static final String ENCODING_COLLECTE = "ISO-8859-1";

	/** Derniere execution de la mise a jour des origines */
	public Date majOriginesLastExec = null;

	// Activation des logs
	public static final boolean SHOWSQLLOGS = false;
	/** Les cles des parametres obligatoires pour que l'application se lance. */
	public static final String[] MANDATORY_PARAMS = new String[] {
			"GRHUM_HOST_MAIL", "DEFAULT_NS_TIMEZONE", "ADMIN_MAIL", "SHOWSQLLOGS", "SHOWUSERNAMEINLOG", "SHOWIPADDRESSINLOG", "SHOWBDCONNEXIONSERVER", "SHOWBDCONNEXIONSERVERID", "LCL_USER_AUT_MODE"
	};

	/**
	 * Les cles des parametres optionnels (warning au lancement de l'application).
	 */
	public static final String[] OPTIONAL_PARAMS = new String[] {
			"SEND_MAIL_TO_ADMIN_AT_STARTUP", "ZTEST_MODE", "ZEMAIL_FOR_TEST"
	};

	public static final Format FORMAT_DATESHORT = new SimpleDateFormat("dd/MM/yyyy");

	// private boolean wantUserSAUT=true;
	private String etablissementName;
	private UserAuthenticationEngine myUserAuthenticationEngine;
	private NSDictionary allFonctionsDico;

	public NSArray sharedAllFonctionsList;
	private HelpCenter helpCenter;
	private HashMap mySessions = new HashMap();
	private String logPrefix;
	private Version _appVersion;

	private MyDatabaseContextDelegate databaseContextDelegate;

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.maracuja.server.Application", argv);
	}

	protected SAUTClient sautClient() {
		return sautClient;
	}

	/**
	 * Initialisation de l'application. S'execute une seule fois pour toutes les sessions
	 */
	public Application() {
		super();
		// fix pour que les WebServerResources marchent en javaclient
		if (isDirectConnectEnabled()) {
			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
		}
	}

	protected void initApplicationSpecial() {

		if (System.getProperty("CRIAppConfig") != null) {
			ZBufferedLogger.info("Fichier de configuration : ", System.getProperty("CRIAppConfig"));
		}
		else {
			ZBufferedLogger.info("Fichier de configuration par défaut");
		}

		ZBufferedLogger.logTitle("Methode d'authentification", ZLogger.LVL_INFO);
		try {
			final String autmode = config().stringForKey("LCL_USER_AUT_MODE");
			if (autmode.equals("SAUT")) {
				myUserAuthenticationEngine = new UserAuthenticationEngineSaut(config().stringForKey("SAUT_URL"), getApplicationInternalName());
			}
			else if (autmode.equals("SQL")) {
				myUserAuthenticationEngine = new UserAuthenticationEngineSql(dataBus());
			}
			else if (autmode.equals("LOGIN")) {
				myUserAuthenticationEngine = new UserAuthenticationEngineLogin(dataBus());
			}
			else {
				ZBufferedLogger.warning("Aucune méthode d'authentification n'a été définie dans le fichier de configuration. Les utilisateurs ne pourront pas se connecter à l'application.");
			}
			myUserAuthenticationEngine.initEngine();
			ZBufferedLogger.info("Connection JDBC : " + getJDBCConnection());
			databaseContextDelegate = new MyDatabaseContextDelegate();
			getDatabaseContext().setDelegate(databaseContextDelegate);

		} catch (Exception e5) {
			isInitialisationErreur = true;
			ZBufferedLogger.warning(e5.getMessage());
		}

		final String logLvl = config().stringForKey("LOG_LEVEL_SERVER");
		int lvl = ZLogger.LVL_INFO;
		if (ZLogger.VERBOSE.equals(logLvl)) {
			lvl = ZLogger.LVL_VERBOSE;
		}
		else if (ZLogger.DEBUG.equals(logLvl)) {
			lvl = ZLogger.LVL_DEBUG;
		}
		else if (ZLogger.INFO.equals(logLvl)) {
			lvl = ZLogger.LVL_INFO;
		}
		else if (ZLogger.WARNING.equals(logLvl)) {
			lvl = ZLogger.LVL_WARNING;
		}
		else if (ZLogger.ERROR.equals(logLvl)) {
			lvl = ZLogger.LVL_ERROR;
		}
		else if (ZLogger.NONE.equals(logLvl)) {
			lvl = ZLogger.LVL_NONE;
		}
		System.out.println("Niveau de log : " + logLvl);
		ZLogger.setCurrentLevel(lvl);
		ZLogger.verbose(System.getProperties());

		ZLogger.debug("isConcurrentRequestHandlingEnabled : " + isConcurrentRequestHandlingEnabled());
		initMoteurCompta();
	}

	public String getLitchiModCode(String parKey) {
		try {
			String sql = "select param_value from litchi.internat_parametres where param_key='" + parKey + "' and param_value<>'000'";
			NSArray res = EOUtilities.rawRowsForSQL(ERXEC.newEditingContext(), mainModelName(), sql, null);
			if (res.count() > 0) {
				return (String) ((NSDictionary) res.objectAtIndex(0)).valueForKey("PARAM_VALUE");
			}
		} catch (Throwable e) {
			// silent
			System.out.println("SI VOUS VOYEZ UN MESSAGE D'ERREUR INDIQUANT QUE LA TABLE LITCHI.INTERNAT_PARAMETRES N'EXISTE PAS ET QUE VOUS N'UTILISEZ PAS LITCHI, N'EN TENEZ PAS COMPTE.");
		}
		return null;
	}

	protected void initMoteurCompta() {
		FwkCktlComptaMoteurCtrl.getSharedInstance().init(FwkCktlComptaMoteurCtrl.Contexte.SERVEUR);
		FwkCktlComptaMoteurCtrl.getSharedInstance().setReportsBaseLocation(getReportsLocation());
		FwkCktlComptaMoteurCtrl.getSharedInstance().setRefApplicationCreation(VersionMe.APPLICATION_STRID);

		//init pour Litchi
		EOLitchiEnteteFacture.RESTAURATION_MODE_RECOUVREMENT_CODE = getLitchiModCode("RESTAURATION_MODE_RECOUVREMENT_CODE");
		EOLitchiEnteteFacture.HEBERGEMENT_MODE_RECOUVREMENT_CODE = getLitchiModCode("HEBERGEMENT_MODE_RECOUVREMENT_CODE");
		
        EOEditingContext ec = ERXEC.newEditingContext();

		//Init pour scolarix
		try {
            EOScolarixInscription.SCOLARIX_ECHEANCIER_MODE_RECOUVREMENT_CODE = (String) ParamHelper.getSharedInstance().getConfig(ec, IFwkCktlComptaParam.SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT);
		} catch (Exception e) {
			System.err.println("Impossible d'initialiser le code du mode de recouvrement à utiliser pour les prélèvements SEPA issus de SCOLARIX. Vérifiez la valeur du parametre " + IFwkCktlComptaParam.SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT);
			e.printStackTrace();
		}
		try {
            EOCoriandreInscription.CORIANDRE_ECHEANCIER_MODE_RECOUVREMENT_CODE = (String) ParamHelper.getSharedInstance().getConfig(ec, IFwkCktlComptaParam.SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT);
		} catch (Exception e) {
		    System.err.println("Impossible d'initialiser le code du mode de recouvrement à utiliser pour les prélèvements SEPA issus de Coriandre. Vérifiez la valeur du parametre " + IFwkCktlComptaParam.SEPASDDMANDAT_CORIANDRE_MODERECOUVREMENTCODEDEFAUT);
		    e.printStackTrace();
		}

	}

	/**
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#configTableName()
	 */
	public String configTableName() {
		return "FwkCktlWebApp_GrhumParametres";
	}

	/**
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#mainModelName()
	 */
	public String mainModelName() {
		return "Maracuja";
	}

	public CktlDataBus getDbManager() {
		return this.dataBus();
	}

	/**
	 * Renvoie la chaine de connexion à la base de données.
	 *
	 * @return
	 */
	public String bdConnexionName() {
		final EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		final EOModel vModel = vModelGroup.modelNamed(mainModelName());
		final NSDictionary vDico = vModel.connectionDictionary();
		// NSApp.appLog.trace(vDico);
		return (String) vDico.valueForKey("URL");
	}

	protected boolean showTrace() {
		return ZLogger.currentLevel >= ZLogger.LVL_DEBUG;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see zwebapp.ZApplication#showSqlLogs()
	 */
	protected boolean showSqlLogs() {
		if (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS;
		}
		return config().valueForKey("SHOWSQLLOGS").equals(CONST_1);
	}

	/**
	 * @see zwebapp.ZApplication#getApplicationFinalName()
	 */
	protected String getApplicationFinalName() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	/**
	 * Renvoie le nom de l'établissement en se basant sur la structure de type E
	 *
	 * @return
	 */
	public String getEtablissementName() {
		return etablissementName;
	}

	/**
	 * @return
	 */
	public UserAuthenticationEngine getMyUserAuthenticationEngine() {
		return myUserAuthenticationEngine;
	}

	/**
	 * Initialise la liste des fonctions disponibles.
	 */
	public void initFonctionList() {
		ZBufferedLogger.logTitle("Récupération des fonctions disponibles de l'application", ZLogger.LVL_INFO);
		updateAllFonctionsDico(mySharedEditingContext);
		final NSArray cles = allFonctionsDico.allKeys();
		for (int i = 0; i < cles.count(); i++) {
			ZBufferedLogger.info(cles.objectAtIndex(i).toString(), ((EOFonction) allFonctionsDico.valueForKey((String) cles.objectAtIndex(i))).fonLibelle());
		}
	}

	/**
	 * Met à jour la liste des fonctions disponibles de l'application.
	 */
	public void updateAllFonctionsDico(EOEditingContext ec) {
		final NSMutableDictionary tmpDico = new NSMutableDictionary();
		// Fetcher les fonctions disponibles
		sharedAllFonctionsList = ZFinder.fetchArray(ec, EOFonction.ENTITY_NAME, null, null, null, true);
		for (int i = 0; i < sharedAllFonctionsList.count(); i++) {
			final EOFonction tmpFonc = (EOFonction) sharedAllFonctionsList.objectAtIndex(i);
			tmpDico.takeValueForKey(tmpFonc, tmpFonc.fonIdInterne());
		}
		allFonctionsDico = tmpDico.immutableClone();
	}

	/**
	 * Renvoie un dictionnaire des fonctions disponibles de l'application. (En clé: l'identifiant inetrne de l'application, en valeur : l'objet
	 * fonction).
	 */
	public NSDictionary getAllFonctionsDico(EOEditingContext ec) {
		if (allFonctionsDico == null) {
			updateAllFonctionsDico(ec);
		}
		return allFonctionsDico;
	}

	/**
	 * @return
	 */
	public String getReportsLocation() {
		if ((config().stringForKey(CONST_REPORTS_LOCATION) != null) && (config().stringForKey(CONST_REPORTS_LOCATION).length() > 0)) {
			return config().stringForKey(CONST_REPORTS_LOCATION);
		}
		final String aPath = path().concat(CONST_CONTENTS_RESOURCES);
		ZBufferedLogger.debug("Application.getReportsLocation() : ".concat(aPath));
		return aPath;
	}

	public HelpCenter getHelpCenter() {
		if (helpCenter == null) {
			try {
				helpCenter = new HelpCenter();
			} catch (Exception e6) {
				e6.printStackTrace();
			}
		}
		return helpCenter;
	}

	/**
	 * Ecrit la liste des utilisateurs connetces dans la console.
	 */
	public final void listConnectedUsers() {
		ZLogger.info("");
		ZLogger.info("Liste des utilisateurs connectes");
		ZLogger.info("--------------------------------");

		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final Session element = (Session) iter.next();
			ZLogger.info(element.getInfoConnectedUser());
		}
		ZLogger.info("--------------------------------");
	}

	/**
     *
     */
	public final void cleanSessionsInactives() {
		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final Session element = (Session) iter.next();
			//Supprimer les sessions inactives
			//Si la session n'a pas recu d'info du client depuis plus de 180 secondes
			//on la detruit
			if (ZDateUtil.calcMillisecondsBetween(element.getConnectedUser().getDateLastHeartBeat(), new Date()) > 180000) {
				ZLogger.info(element.getInfoConnectedUser() + " inactive depuis plus de 180 secondes, supprimee.");
				element.terminate();
			}
		}
	}

	public final void cleanSessionsNonIdentifiees() {
		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final Session element = (Session) iter.next();
			//Si la session n'a pas d'utilisateur connecte depuis 60 secondes on la detruit
			if (element.getConnectedUser().getLogin() == null) {
				if (ZDateUtil.calcMillisecondsBetween(element.getConnectedUser().getDateConnection(), new Date()) > 60000) {
					ZLogger.info(element.getInfoConnectedUser() + " non identifie depuis plus de 60 secondes, supprimee.");
					element.terminate();
				}
			}
		}
	}

	public HashMap getMySessions() {
		return mySessions;
	}

	public void setMySessions(final HashMap mySessions) {
		this.mySessions = mySessions;
	}

	public WOComponent getSessionTimeoutPage(WOContext context) {
		return null;
	}

	/**
	 * @see org.cocktail.maracuja.server.zwebapp.ZApplication#getLogPrefix()
	 */
	public final String getLogPrefix() {
		if (logPrefix == null) {
			logPrefix = CONST_LOG + name() + CONST_SERVER;
		}
		return logPrefix;
	}

	protected String getApplicationInternalName() {
		return VersionMe.APPLICATIONINTERNALNAME;
	}

	public NSMutableDictionary appParametres() {
		if (appParametres == null) {
			appParametres = new NSMutableDictionary();
			appParametres.setObjectForKey(VersionMe.appliVersion(), "VERSIONNUM");
			appParametres.setObjectForKey(VersionMe.VERSIONDATE, "VERSIONDATE");
			appParametres.setObjectForKey(VersionMe.APPLICATIONFINALNAME, "APPLICATIONFINALNAME");
			appParametres.setObjectForKey(VersionMe.APPLICATIONINTERNALNAME, "APPLICATIONINTERNALNAME");
			appParametres.setObjectForKey(appCktlVersion().copyright(), "COPYRIGHT");
			appParametres.setObjectForKey(VersionMe.txtAppliVersion(), "TXTVERSION");
			appParametres.setObjectForKey(showBdConnexionInfo(), "BDCONNEXIONINFO");
			appParametres.setObjectForKey(NSTimeZone.defaultTimeZone(), "DEFAULTNSTIMEZONE");
			appParametres.setObjectForKey(TimeZone.getDefault(), "DEFAULTTIMEZONE");
			appParametres.setObjectForKey(getLogPrefix(), "SERVERLOGPREFIX");
			appParametres.setObjectForKey(getServerLogFile().getName(), "SERVERLOGNAME");
		}
		return appParametres;
	}

	public long getMajOrigineRefreshPeriod() {
		long period = 3600000;
		if (config().valueForKey("ORIGINE_REFRESH_PERIOD") != null) {
			period = config().intForKey("ORIGINE_REFRESH_PERIOD") * 1000;
		}
		ZLogger.verbose("ORIGINE_REFRESH_PERIOD = " + period / 1000 + " secondes");
		return period;
	}

	// controle de versions
	public A_CktlVersion appCktlVersion() {
		if (_appVersion == null) {
			_appVersion = new Version();
		}
		return _appVersion;
	}



	/*
	 * (non-Javadoc)
	 *
	 * @see zwebapp.ZApplication#versionTxt()
	 */
	public String versionTxt() {
		return VersionMe.txtAppliVersion();
	}

	public boolean appShouldSendCollecte() {
		return false;
	}

	public String[] configMandatoryKeys() {
		return MANDATORY_PARAMS;
	}

	public String[] configOptionalKeys() {
		return OPTIONAL_PARAMS;
	}

	public MyDatabaseContextDelegate getDatabaseContextDelegate() {
		return databaseContextDelegate;
	}
}
