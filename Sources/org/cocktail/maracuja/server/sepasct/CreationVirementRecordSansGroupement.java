package org.cocktail.maracuja.server.sepasct;

import java.util.Iterator;

import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;
import org.cocktail.zutil.server.ZStringUtil;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CreationVirementRecordSansGroupement implements IStrategieCreationVirementRecord {

	public NSArray<VirementSepaRecord> createRecords(SepaSCTFactory factory, int noTransaction, EORib rib, NSArray<ISupportVirement> elementsAPayer) throws Exception {
		NSMutableArray<VirementSepaRecord> records = new NSMutableArray<VirementSepaRecord>();
		int noTransactionLocal = noTransaction;
		Iterator<ISupportVirement> iterDeps = elementsAPayer.iterator();
		while (iterDeps.hasNext()) {
			noTransactionLocal++;
			records.addObject(createRecordFrom(factory, noTransactionLocal, iterDeps.next()));
		}
		return records;
	}

	private VirementSepaRecord createRecordFrom(SepaSCTFactory factory, int noTransaction, ISupportVirement elementAPayer) {
		VirementSepaRecord res = new VirementSepaRecord();
		//res.setAmount(object.odpMontantPaiement());
		res.setAmount(elementAPayer.montantAPayer());
		res.setCurrency(factory.getParamSepa().getDevise());
		//FIXME le controle doit s'effectuer avant
		res.setCreditorBIC(ZStringUtil.ifNull(elementAPayer.rib().ribBic(), "XXXXFRXXXXX"));
		res.setCreditorIBAN(elementAPayer.rib().ribIban());
		res.setCreditorName(elementAPayer.rib().ribTitco());
		res.setPaymentID(factory.buildTransactionId(noTransaction));
		res.setPurposeCode(null);
		res.setRemittanceInformation(factory.getLibelleTransaction(elementAPayer));
		res.setUltimateCreditorName(factory.getNomFournisseur(elementAPayer.rib().fournisseur()));
		return res;
	}

}
