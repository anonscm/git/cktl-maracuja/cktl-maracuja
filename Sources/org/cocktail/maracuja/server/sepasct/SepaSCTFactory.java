/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.server.sepasct;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlcompta.common.util.ZListUtil;
import org.cocktail.gfc.server.echanges.sepa.factories.ParametresSepa;
import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaFactory;
import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.maracuja.common.sepa.OptionsRegroupementLigneFichierVirement;
import org.cocktail.maracuja.server.metier.EODepense;
import org.cocktail.maracuja.server.metier.EOFournisseur;
import org.cocktail.maracuja.server.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;
import org.cocktail.zutil.server.ZStringUtil;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Genere des messages SEPA
 * 
 * @author rprin
 */
public class SepaSCTFactory {

	private ParametresSepa paramSepa;
	private String xsdLocation;
	private String paiNumero;
	private String exeOrdre;
	private List<String> codesPaysZoneSepaList;

	private static final String CODES_PAYS_SEPA_DEFAULT = "AT, BE, BG, CH, CY, CZ, DE, DK, EE, ES, FI, FR, GB, GF, GI, GR, GP, HR, HU, IE, IS, IT, LI, LT, LU, LV, MC, MQ, MT, NL, NO, PL, PM, PT, RE, RO, SE, SI, SK, YT";;

	/**
	 * @param paramSepa
	 * @param xsdLocation Emplacement du fichier xsd qui permet de valider le xml.
	 * @param exeOrdre
	 * @param paiNumero
	 */
	public SepaSCTFactory(ParametresSepa paramSepa, String xsdLocation, String exeOrdre, String paiNumero, String codesPaysZoneSepa) {
		this.paramSepa = paramSepa;
		this.xsdLocation = xsdLocation;
		this.paiNumero = paiNumero;
		this.exeOrdre = exeOrdre;
		if (codesPaysZoneSepa == null) {
			codesPaysZoneSepa = CODES_PAYS_SEPA_DEFAULT;
		}
		this.codesPaysZoneSepaList = ZListUtil.createListFromCommaSeparatedStringAndTrim(codesPaysZoneSepa);
	}




	public static Map<Integer, IStrategieCreationVirementRecord> strategies = new HashMap<Integer, IStrategieCreationVirementRecord>();
	static {
		strategies.put(OptionsRegroupementLigneFichierVirement.OPTION_REGROUPEMENT_LIGNES_SANS, new CreationVirementRecordSansGroupement());
		strategies.put(OptionsRegroupementLigneFichierVirement.OPTION_REGROUPEMENT_LIGNES_RIB, new CreationVirementRecordParRib());
		strategies.put(OptionsRegroupementLigneFichierVirement.OPTION_REGROUPEMENT_LIGNES_NO_FACTURE, new CreationVirementRecordParNoFacture());
	}

	/**
	 * Genere un message SEPA de type Virement SCT
	 * 
	 * @param dateTimeCreation
	 * @param lesDepenses
	 * @param lesOrdresDePaiment
	 * @param montantTotal
	 * @param nbTransactions
	 * @param dateExecutionDemandee
	 * @param numeroDeFichierDansLaJournee3Car
	 * @param identifiantFichierVirement
	 * @param optionRegroupementLignes
	 * @return
	 * @throws Exception
	 */
	public NSData genereMessageVirement(NSTimestamp dateTimeCreation, NSArray lesDepenses, NSArray lesOrdresDePaiment, BigDecimal montantTotal, Integer nbTransactions, NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car,
			String identifiantFichierVirement, int optionRegroupementLignes)
			throws Exception {

		if (!strategies.containsKey(optionRegroupementLignes)) {
			throw new Exception("L'option de regroupement de ligne n'existe pas (" + optionRegroupementLignes + ").");
		}

		//Nettoyer les elements à payer
		lesDepenses = supprimeElementsAvecMontantInferieurOuEgalAZero(lesDepenses);
		lesOrdresDePaiment = supprimeElementsAvecMontantInferieurOuEgalAZero(lesOrdresDePaiment);

		//Recuperer tous les ribs destinataires
		NSMutableArray<EORib> ribs = listerTousLesRibs(lesDepenses, lesOrdresDePaiment);

		NSMutableArray<VirementSepaRecord> records = new NSMutableArray<VirementSepaRecord>();
		Iterator<EORib> iterRib = ribs.iterator();
		int no_transaction = 0;
		while (iterRib.hasNext()) {
			EORib eoRib = (EORib) iterRib.next();
			EOQualifier qualDep = new EOKeyValueQualifier(EODepense.RIB_KEY, EOQualifier.QualifierOperatorEqual, eoRib);
			NSArray<ISupportVirement> deps = EOQualifier.filteredArrayWithQualifier(lesDepenses, qualDep);
			EOQualifier qualOdp = new EOKeyValueQualifier(EOOrdreDePaiement.RIB_KEY, EOQualifier.QualifierOperatorEqual, eoRib);
			NSArray<ISupportVirement> odps = EOQualifier.filteredArrayWithQualifier(lesOrdresDePaiment, qualOdp);

			NSMutableArray<ISupportVirement> objs = new NSMutableArray<ISupportVirement>();
			objs.addObjectsFromArray(deps);
			objs.addObjectsFromArray(odps);

			if (objs.count() > 0) {
				no_transaction++;
				IStrategieCreationVirementRecord creationVirement = strategies.get(optionRegroupementLignes);
				records.addObjectsFromArray(creationVirement.createRecords(this, no_transaction, eoRib, objs));
				no_transaction = records.count();
			}
		}

		checkBicInSEPA(records);
		//On corrige le nombre de transactions, qui peut varier suivant les options de regroupement et si des ordres 
		//de virement avec des montants à 0 sont passés (peut arriver dans le cas de retenues)
		nbTransactions = Integer.valueOf(no_transaction);

		if (Integer.valueOf(0).equals(nbTransactions)) {
			throw new Exception("Aucune transaction a écrire dans le fichier. Les montants sont peut-etre tous à 0.");
		}

		VirementSepaFactory virementSepaFactory = new VirementSepaFactory();
		return virementSepaFactory.genereMessage(dateTimeCreation, paramSepa, records, montantTotal, nbTransactions, dateExecutionDemandee, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, xsdLocation);

	}

	protected NSMutableArray<ISupportVirement> supprimeElementsAvecMontantInferieurOuEgalAZero(NSArray<ISupportVirement> objs) {
		NSMutableArray<ISupportVirement> res = new NSMutableArray<ISupportVirement>();
		Iterator<ISupportVirement> iterElements = objs.iterator();
		while (iterElements.hasNext()) {
			ISupportVirement iSupportVirement = (ISupportVirement) iterElements.next();
			if (BigDecimal.ZERO.compareTo(iSupportVirement.montantAPayer()) < 0) {
				res.addObject(iSupportVirement);
			}
		}
		return res;
	}

	private NSMutableArray<EORib> listerTousLesRibs(NSArray lesDepenses, NSArray lesOrdresDePaiment) {
		NSMutableArray<EORib> ribs = new NSMutableArray<EORib>();
		Iterator<EODepense> iter = lesDepenses.iterator();
		while (iter.hasNext()) {
			EODepense eoDepense = (EODepense) iter.next();
			if (ribs.indexOfIdenticalObject(eoDepense.rib()) == NSArray.NotFound) {
				ribs.addObject(eoDepense.rib());
			}
		}

		Iterator<EOOrdreDePaiement> iter2 = lesOrdresDePaiment.iterator();
		while (iter2.hasNext()) {
			EOOrdreDePaiement eoOrdreDePaiement = (EOOrdreDePaiement) iter2.next();
			if (ribs.indexOfIdenticalObject(eoOrdreDePaiement.rib()) == NSArray.NotFound) {
				ribs.addObject(eoOrdreDePaiement.rib());
			}
		}

		//Trier les ribs
		ribs = EOSortOrdering.sortedArrayUsingKeyOrderArray(ribs, new NSArray(new Object[] {
				EORib.SORT_BIC_ASC, EORib.SORT_IBAN_ASC
		})).mutableClone();

		return ribs;
	}

	/**
	 * @param paiement
	 * @param index
	 * @return Un id de transaction construit à partir du titulaire du compte + exercice + n° paiement + n° transaction
	 */
	protected String buildTransactionId(int index) {
		String emetteur = ZStringUtil.extendWithChars(paramSepa.getCompteDftTitulaire(), " ", 12, false);
		//String exeOrdre = paiement.exercice().exeExercice().toString();//4
		String lotNum = ZStringUtil.extendWithChars(paiNumero, "0", 5, true);
		String transactionNum = ZStringUtil.extendWithChars("" + index, "0", 5, true);
		String res = emetteur.concat(exeOrdre).concat(lotNum).concat(transactionNum);
		return res;
	}

	/**
	 * Verifie si tous les BIC dependent bien de la zone SEPA.
	 * 
	 * @param ribs
	 * @throws Exception
	 */
	public void checkBicInSEPA(NSArray virementRecords) throws Exception {
		Iterator<VirementSepaRecord> iterRibs = virementRecords.iterator();
		while (iterRibs.hasNext()) {
			VirementSepaRecord rec = (VirementSepaRecord) iterRibs.next();
			String bic = rec.getCreditorBIC();
			String codePaysISO = bic.substring(4, 6);
			if (!codesPaysZoneSepaList.contains(codePaysISO)) {
				throw new Exception("Le code BIC " + bic + " n'identifie pas une banque d'un pays de la zone SEPA (caractères 5 et 6 du code=" + codePaysISO + ")");
			}
		}
	}

	/**
	 * Max 140 caracteres.
	 * 
	 * @param elementAPayer
	 * @return
	 */
	protected String getLibelleTransaction(ISupportVirement elementAPayer) {
		String res = ZStringUtil.cut("Vir Sct " + ZStringUtil.cut(paramSepa.getCompteDftTitulaire(), 20), 28); //28
		res += ZStringUtil.cut(elementAPayer.virementReferenceFournisseur(), 79); //79
		res += ZStringUtil.cut(" " + elementAPayer.virementReferenceInterne(), 11); //11
		res += ZStringUtil.cut(" / Paiement " + getExeOrdre() + "-" + paiNumero, 22); //22
		return ZStringUtil.cut(res, 140);
	}

	protected String getNomFournisseur(EOFournisseur fournisseur) {
		String res = fournisseur.adrNom();
		if (fournisseur.adrPrenom() != null) {
			res += " " + fournisseur.adrPrenom();
		}
		res = ZStringUtil.extendWithChars(res, " ", 70, false);
		return res;
	}

	public ParametresSepa getParamSepa() {
		return paramSepa;
	}

	public String getPaiNumero() {
		return paiNumero;
	}

	public String getExeOrdre() {
		return exeOrdre;
	}

	public BigDecimal calculMontantPaiement(NSArray<ISupportVirement> elementsAPayer) {
		BigDecimal montant = new BigDecimal(0.00);
		Iterator<ISupportVirement> objs = elementsAPayer.iterator();
		while (objs.hasNext()) {
			montant = montant.add(objs.next().montantAPayer());
		}
		return montant;
	}


}
