package org.cocktail.maracuja.server.sepasct;

import java.math.BigDecimal;
import java.util.Iterator;

import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;
import org.cocktail.zutil.server.ZStringUtil;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Création d'une instruction de virement à partir d'éléments à payer regroupés par n° de facture (libellé).
 * 
 * @author rprin
 */
public class CreationVirementRecordParNoFacture implements IStrategieCreationVirementRecord {

	public NSArray<VirementSepaRecord> createRecords(SepaSCTFactory factory, int noTransaction, EORib rib, NSArray<ISupportVirement> elementsAPayer) throws Exception {
		return createRecordsFromArray(factory, noTransaction, rib, elementsAPayer);
	}

	protected NSArray<VirementSepaRecord> createRecordsFromArray(SepaSCTFactory factory, int noTransaction, EORib rib, NSArray<ISupportVirement> elementsAPayer) throws Exception {
		NSMutableArray<VirementSepaRecord> lesVirementRecords = new NSMutableArray<VirementSepaRecord>();
		boolean hasNullNoFacture = false;
		//ajouter controle sur elementsAPAyer n'aient pas d'autres ribs que celui passé en param
		checkRibDifferent(rib, elementsAPayer);

		//pb nbo transactiobn ?
		//récupérer tous les n°s facture
		NSMutableArray<String> allNoFacture = new NSMutableArray<String>();

		Iterator<ISupportVirement> objs = elementsAPayer.iterator();
		while (objs.hasNext()) {
			ISupportVirement elementAPayer = objs.next();
			if (elementAPayer.virementReferenceFournisseurNumero() == null) {
				hasNullNoFacture = true;
			}
			else if (allNoFacture.indexOf(elementAPayer.virementReferenceFournisseurNumero()) == NSArray.NotFound) {
				allNoFacture.addObject(elementAPayer.virementReferenceFournisseurNumero());
			}
		}

		Iterator<String> allNoFactureIt = allNoFacture.iterator();
		while (allNoFactureIt.hasNext()) {
			String referenceFournisseurNumero = (String) allNoFactureIt.next();
			//			EOQualifier qualForReferenceFournisseur = ERXQ.equals(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY, referenceFournisseurNumero);
			//			NSArray<ISupportVirement> elementsAPayerFiltres = EOQualifier.filteredArrayWithQualifier(elementsAPayer, qualForReferenceFournisseur);
			NSArray<ISupportVirement> elementsAPayerFiltres = filtreSupportVirementsByReferenceFournisseurNumero(referenceFournisseurNumero, elementsAPayer);
			BigDecimal montant = factory.calculMontantPaiement(elementsAPayerFiltres);

			VirementSepaRecord res = createVirementRecord(factory, montant, rib, factory.buildTransactionId(noTransaction), getLibelleTransaction(factory, referenceFournisseurNumero, elementsAPayerFiltres));
			lesVirementRecords.addObject(res);
		}

		if (hasNullNoFacture) {
			//			EOQualifier qualForReferenceFournisseur = ERXQ.equals(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY, null);
			//NSArray<ISupportVirement> elementsAPayerFiltres = EOQualifier.filteredArrayWithQualifier(elementsAPayer, qualForReferenceFournisseur);
			NSArray<ISupportVirement> elementsAPayerFiltres = filtreSupportVirementsByReferenceFournisseurNumero(null, elementsAPayer);
			BigDecimal montant = factory.calculMontantPaiement(elementsAPayerFiltres);
			VirementSepaRecord res = createVirementRecord(factory, montant, rib, factory.buildTransactionId(noTransaction), getLibelleTransaction(factory, "", elementsAPayerFiltres));
			lesVirementRecords.addObject(res);
		}

		return lesVirementRecords.immutableClone();
	}

	/**
	 * Verifie si les elements à payer contiennent un rib autre que celui specifié.
	 * 
	 * @param rib
	 * @param elementsAPayer
	 * @throws Exception
	 */
	protected void checkRibDifferent(EORib rib, NSArray<ISupportVirement> elementsAPayer) throws IllegalArgumentException {
		for (ISupportVirement iSupportVirement : elementsAPayer) {
			if (!rib.equals(iSupportVirement.rib())) {
				throw new IllegalArgumentException("Un element à payer ne contient pas le bon rib pour le regroupement");
			}
		}
	}

	private VirementSepaRecord createVirementRecord(SepaSCTFactory factory, BigDecimal montant, EORib rib, String paymentId, String libelleTransaction) {
		VirementSepaRecord res = new VirementSepaRecord();
		res.setAmount(montant);
		res.setCurrency(factory.getParamSepa().getDevise());
		res.setCreditorBIC(rib.ribBic());
		res.setCreditorIBAN(rib.ribIban());
		res.setCreditorName(rib.ribTitco());
		res.setPaymentID(paymentId);
		res.setPurposeCode(null);
		//			res.setRemittanceInformation("Vir Sct " + ZStringUtil.cut(factory.getParamSepa().getEmetteurNom(), 20) + " / multiple / Paiement " + ZStringUtil.extendWithChars("" + factory.getExeOrdre() + "-" + factory.getPaiNumero(), "0", 5, true));
		res.setRemittanceInformation(libelleTransaction);
		res.setUltimateCreditorName(factory.getNomFournisseur(rib.fournisseur()));
		return res;
	}

	/**
	 * @param noFacture
	 * @param elementsAPayer
	 * @return Le libellé de la transaction, sur 140 caracteres max
	 */
	protected String getLibelleTransaction(SepaSCTFactory factory, String referenceFournisseur, NSArray<ISupportVirement> elementsAPayer) {
		if (elementsAPayer.count() == 1) {
			return factory.getLibelleTransaction(elementsAPayer.objectAtIndex(0));
		}
		String res = "Vir Sct " + ZStringUtil.cut(factory.getParamSepa().getCompteDftTitulaire(), 20);
		if (referenceFournisseur != null) {
			res = res.concat(" / ").concat(referenceFournisseur.trim());
		}
		//	res += " / " + depense.depLigneBudgetaire();

		//		res = ZStringUtil.extendWithChars(res, " ", 97, false);
		res = ZStringUtil.cut(res, 97);

		res += " / Multiple"; //11
		res += " / Paiement " + factory.getExeOrdre() + "-" + factory.getPaiNumero(); //22

		return res;
	}

	protected NSArray<ISupportVirement> filtreSupportVirementsByReferenceFournisseurNumero(String referenceNumero, NSArray<ISupportVirement> elementsAPayer) {
		EOQualifier qualForReferenceFournisseur = ERXQ.equals(ISupportVirement.REFERENCE_FOURNISSEUR_NUMERO_KEY, referenceNumero);
		return EOQualifier.filteredArrayWithQualifier(elementsAPayer, qualForReferenceFournisseur);
	}

}
