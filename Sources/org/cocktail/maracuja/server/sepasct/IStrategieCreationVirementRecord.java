package org.cocktail.maracuja.server.sepasct;

import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;

import com.webobjects.foundation.NSArray;

public interface IStrategieCreationVirementRecord {
	public NSArray<VirementSepaRecord> createRecords(SepaSCTFactory factory, int noTransaction, EORib rib, NSArray<ISupportVirement> elementsAPayer) throws Exception;
}
