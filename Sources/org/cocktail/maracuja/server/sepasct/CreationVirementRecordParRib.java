package org.cocktail.maracuja.server.sepasct;

import java.math.BigDecimal;

import org.cocktail.gfc.server.echanges.sepa.factories.VirementSepaRecord;
import org.cocktail.maracuja.server.metier.EORib;
import org.cocktail.maracuja.server.metier.ISupportVirement;
import org.cocktail.zutil.server.ZStringUtil;

import com.webobjects.foundation.NSArray;

public class CreationVirementRecordParRib implements IStrategieCreationVirementRecord {

	public NSArray<VirementSepaRecord> createRecords(SepaSCTFactory factory, int noTransaction, EORib rib, NSArray<ISupportVirement> elementsAPayer) throws Exception {
		return new NSArray<VirementSepaRecord>(createRecordFromArray(factory, noTransaction, rib, elementsAPayer));
	}

	protected VirementSepaRecord createRecordFromArray(SepaSCTFactory factory, int noTransaction, EORib rib, NSArray<ISupportVirement> elementsAPayer) {
		BigDecimal montant = factory.calculMontantPaiement(elementsAPayer);
		//		Iterator<ISupportVirement> objs = elementsAPayer.iterator();
		//		while (objs.hasNext()) {
		//			montant = montant.add(objs.next().montantAPayer());
		//		}
		//		
		VirementSepaRecord res = new VirementSepaRecord();
		res.setAmount(montant);
		res.setCurrency(factory.getParamSepa().getDevise());
		res.setCreditorBIC(rib.ribBic());
		res.setCreditorIBAN(rib.ribIban());
		res.setCreditorName(rib.ribTitco());
		res.setPaymentID(factory.buildTransactionId(noTransaction));
		res.setPurposeCode(null);
		res.setRemittanceInformation("Vir Sct " + ZStringUtil.cut(factory.getParamSepa().getCompteDftTitulaire(), 20) + " / multiple / Paiement " + ZStringUtil.extendWithChars("" + factory.getExeOrdre() + "-" + factory.getPaiNumero(), "0", 5, true));
		res.setUltimateCreditorName(factory.getNomFournisseur(rib.fournisseur()));
		return res;

	}

}
