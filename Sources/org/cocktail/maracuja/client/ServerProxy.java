/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/* ServerProxy.java created by rprin on Thu 25-Sep-2003 */
package org.cocktail.maracuja.client;

import java.util.TimeZone;

import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimeZone;

/**
 * Rassemble tous les appels aux méthodes définies sur le serveur.
 */

public final class ServerProxy {
	private static final String CLIENT_SIDE_REQUEST_SAVE_PREFERENCE = "clientSideRequestSavePreference";
	private static final String CLIENT_SIDE_REQUEST_TEST_THREAD_PROGRESS = "clientSideRequestTestThreadProgress";
	private static final String CLIENT_SIDE_REQUEST_TEST_THREAD = "clientSideRequestTestThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_RESET = "clientSideRequestExportToExcelReset";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_BY_THREAD_CANCEL = "clientSideRequestExportToExcelByThreadCancel";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_GET_RESULT = "clientSideRequestExportToExcelGetResult";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_FETCH_SPEC_BY_THREAD = "clientSideRequestExportToExcelFromFetchSpecByThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_SQLBY_THREAD = "clientSideRequestExportToExcelFromSQLByThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS_BY_THREAD = "clientSideRequestExportToExcelFromEOsByThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS = "clientSideRequestExportToExcelFromEOs";
	private static final String CLIENT_SIDE_REQUEST_PrintByThreadJXls = "clientSideRequestPrintByThreadJXls";
	private static final String CLIENT_SIDE_REQUEST_GET_APPLICATION_SERVER_LOG_PREFIX = "clientSideRequestGetApplicationServerLogPrefix";
	private static final String CLIENT_SIDE_REQUEST_GET_APPLICATION_SERVER_LOG_NAME = "clientSideRequestGetApplicationServerLogName";
	private static final String CLIENT_SIDE_REQUEST_GET_CURRENT_SERVER_LOG_FILE = "clientSideRequestGetCurrentServerLogFile";
	private static final String CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_PAIEMENT = "clientSideRequestPapayePasserEcriturePaiement";
	private static final String CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_OPP_RET_BORD = "clientSideRequestPapayePasserEcritureOppRetBord";
	private static final String CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_SACDBORD = "clientSideRequestPapayePasserEcritureSACDBord";
	private static final String CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_VISABORD = "clientSideRequestPapayePasserEcritureVISABord";
	private static final String CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_PAIEMENT = "clientSideRequestPayePafPasserEcriturePaiement";
	private static final String CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_OPP_RET_BORD = "clientSideRequestPayePafPasserEcritureOppRetBord";
	private static final String CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_SACDBORD = "clientSideRequestPayePafPasserEcritureSACDBord";
	private static final String CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_VISABORD = "clientSideRequestPayePafPasserEcritureVISABord";
	private static final String CLIENT_SIDE_REQUEST_GET_PRINT_PROGRESSION = "clientSideRequestGetPrintProgression";
	private static final String CLIENT_SIDE_REQUEST_PRINT_DIFFERED_GET_PDF = "clientSideRequestPrintDifferedGetPdf";
	private static final String CLIENT_SIDE_REQUEST_PRINT_KILL_CURRENT_TASK = "clientSideRequestPrintKillCurrentTask";
	private static final String CLIENT_SIDE_REQUEST_SQL_QUERY = "clientSideRequestSqlQuery";
	private static final String CLIENT_SIDE_REQUEST_GET_CONNECTED_USERS = "clientSideRequestGetConnectedUsers";
	private static final String CLIENT_SIDE_REQUEST_MSG_TO_SERVER = "clientSideRequestMsgToServer";
	private static final String CLIENT_SIDE_REQUEST_HELLO_IM_ALIVE = "clientSideRequestHelloImAlive";
	private static final String CLIENT_SIDE_REQUEST_GET_URL_FOR_HELP_ID = "clientSideRequestGetUrlForHelpId";
	private static final String CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_RECOUVREMENT_RELEVE = "clientSideRequestAfaireApresTraitementRecouvrementReleve";
	private static final String CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_RECOUVREMENT = "clientSideRequestAfaireApresTraitementRecouvrement";
	private static final String CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_PAIEMENT = "clientSideRequestAfaireApresTraitementPaiement";
	private static final String CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_REIMPUTATION = "clientSideRequestAfaireApresTraitementReimputation";
	private static final String CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_VISA_BORDEREAU = "clientSideRequestAfaireApresTraitementVisaBordereau";
	private static final String CLIENT_SIDE_REQUEST_EXTOURNE_CREER_BORDEREAU = "clientSideRequestExtourneCreerBordereau";
	private static final String CLIENT_SIDE_REQUEST_NUMEROTATION = "clientSideRequestNumerotation";
	private static final String CLIENT_SIDE_REQUEST_PRINT_BY_THREAD = "clientSideRequestPrintByThread";
	private static final String CLIENT_SIDE_REQUEST_PRINT_AND_WAIT = "clientSideRequestPrintAndWait";
	private static final String CLIENT_SIDE_REQUEST_ALL_FONCTIONS = "clientSideRequestAllFonctions";
	private static final String CLIENT_SIDE_REQUEST_GET_APP_ALIAS = "clientSideRequestGetAppAlias";
	private static final String CLIENT_SIDE_REQUEST_GET_ETABLISSEMENT_NAME = "clientSideRequestGetEtablissementName";
	private static final String CLIENT_SIDE_REQUEST_GET_EMAIL_FOR_PERSONNE = "clientSideRequestGetEmailForPersonne";
	private static final String CLIENT_SIDE_REQUEST_SEND_MAIL_TO_ADMIN = "clientSideRequestSendMailToAdmin";
	private static final String CLIENT_SIDE_REQUEST_SEND_MAIL = "clientSideRequestSendMail";
	private static final String CLIENT_SIDE_REQUEST_GET_APPLICATION_NAME = "clientSideRequestGetApplicationName";
	private static final String CLIENT_SIDE_REQUEST_SET_CLIENT_PLATEFORME = "clientSideRequestSetClientPlateforme";
	private static final String CLIENT_SIDE_REQUEST_SESS_DECONNECT = "clientSideRequestSessDeconnect";
	private static final String CLIENT_SIDE_REQUEST_GET_USER_IDENTITY = "clientSideRequestGetUserIdentity";
	private static final String CLIENT_SIDE_REQUEST_REQUESTS_AUTHENTICATION_MODE = "clientSideRequestRequestsAuthenticationMode";
	private static final String CLIENT_SIDE_REQUEST_GET_GRHUM_PARAM = "clientSideRequestGetGrhumParam";
	private static final String CLIENT_SIDE_REQUEST_GET_CONFIG_PARAM = "clientSideRequestGetConfigParam";
	private static final String CLIENT_SIDE_REQUEST_DEFAULT_TIME_ZONE = "clientSideRequestDefaultTimeZone";
	private static final String CLIENT_SIDE_REQUEST_DEFAULT_NSTIME_ZONE = "clientSideRequestDefaultNSTimeZone";
	private static final String CLIENT_SIDE_REQUEST_BD_CONNEXION_NAME = "clientSideRequestBdConnexionName";
	private static final String CLIENT_SIDE_REQUEST_COPYRIGHT = "clientSideRequestCopyright";
	private static final String CLIENT_SIDE_REQUEST_VERSION = "clientSideRequestVersion";
	private static final String CLIENT_SIDE_REQUEST_VERSION_RAW = "clientSideRequestVersionRaw";
	private static final String CLIENT_SIDE_REQUEST_SET_USER_LOGIN = "clientSideRequestSetFwkCktlWebApp";
	private static final String CLIENT_SIDE_REQUEST_EXECUTE_STORED_PROCEDURE_NAMED = "clientSideRequestExecuteStoredProcedureNamed";
	private static final String CLIENT_SIDE_REQUEST_PRIMARY_KEY_FOR_OBJECT = "clientSideRequestPrimaryKeyForObject";
	private static final String CLIENT_SIDE_REQUEST_IS_TEST_MODE = "clientSideRequestIsTestMode";
	//	private static final String CLIENT_SIDE_REQUEST_GENERE_VIREMENT_SEPA = "clientSideRequestGenereVirementSepa";
	private static final String SESSION_STR = "session";
	private static final String DELEGATEPRINT_STR = "session.remoteDelegatePrint";
	//private static final String DELEGATEJXLS_STR = "session.remoteDelegateJxls";
	public static final String VERSIONNUM_KEY = "VERSIONNUM";
	public static final String VERSIONDATE_KEY = "VERSIONDATE";
	public static final String APPLICATIONFINALNAME_KEY = "APPLICATIONFINALNAME";
	public static final String APPLICATIONINTERNALNAME_KEY = "APPLICATIONINTERNALNAME";
	public static final String COPYRIGHT_KEY = "COPYRIGHT";
	public static final String TXTVERSION_KEY = "TXTVERSION";
	public static final String BDCONNEXIONINFO_KEY = "BDCONNEXIONINFO";
	public static final String DEFAULTNSTIMEZONE_KEY = "DEFAULTNSTIMEZONE";
	public static final String DEFAULTTIMEZONE_KEY = "DEFAULTTIMEZONE";
	public static final String APP_ALIAS_KEY = "APP_ALIAS";
	public static final String SERVERLOGPREFIX_KEY = "SERVERLOGPREFIX";
	public static final String SERVERLOGNAME_KEY = "SERVERLOGNAME";

	private static final String CLIENTSIDEREQUESTGETAPPPARAMETRES_NAME = "clientSideRequestGetAppParametres";
	private static final String CLIENT_SIDE_REQUEST_GET_WS_RESOURCE_URL = "clientSideRequestGetWsResourceUrl";

	//private static final String CLIENTSIDEREQUESTNEXTNUMEROPAIEMENT = "clientSideRequestNextNumeroPaiement";

	public static NSDictionary serverPrimaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PRIMARY_KEY_FOR_OBJECT, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				eo
		}, false);
	}

	public static NSDictionary clientSideRequestExecuteStoredProcedureNamed(EOEditingContext ec, String name, NSDictionary args) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXECUTE_STORED_PROCEDURE_NAMED, new Class[] {
				String.class, NSDictionary.class
		}, new Object[] {
				name, args
		}, false);
	}

	//    public static NSDictionary clientSideRequestExecuteJDBCStoredProcedureNamed(EOEditingContext ec,String name,NSDictionary args)  throws Exception {
	//        return(NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session", "clientSideRequestExecuteJDBCStoredProcedureNamed", new Class[] {String.class, NSDictionary.class}, new Object[] {name, args}, false);
	//    }

	public static void serverSetFwkCktlWebApp(EOEditingContext ec, String userLogin) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SET_USER_LOGIN, new Class[] {
				String.class
		}, new Object[] {
				userLogin
		}, false);
	}

	public static String serverVersion(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_VERSION, new Class[] {}, new Object[] {}, false);
	}

	public static String serverVersionRaw(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_VERSION_RAW, new Class[] {}, new Object[] {}, false);
	}

	public static String serverCopyright(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_COPYRIGHT, new Class[] {}, new Object[] {}, false);
	}

	public static String serverBdConnexionName(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_BD_CONNEXION_NAME, new Class[] {}, new Object[] {}, false);
	}

	public static NSTimeZone clientSideRequestDefaultNSTimeZone(EOEditingContext ec) {
		return (NSTimeZone) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_DEFAULT_NSTIME_ZONE, new Class[] {}, new Object[] {}, false);
	}

	public static TimeZone clientSideRequestDefaultTimeZone(EOEditingContext ec) {
		return (TimeZone) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_DEFAULT_TIME_ZONE, new Class[] {}, new Object[] {}, false);
	}

	public static Boolean clientSideRequestIsTestMode(EOEditingContext ec) {
		return (Boolean) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_IS_TEST_MODE, new Class[] {}, new Object[] {}, false);
	}

	/**
	 * Renvooie un parametre défini dans le fichier de config ou bien GRHUM.GRHUM_PARAMETRES mis en cache dans Maracuja (les valeurs ne sont chargées
	 * qu'une fois).
	 * 
	 * @param ec
	 * @param paramKey
	 * @return
	 */
	public static String clientSideRequestGetConfigParam(EOEditingContext ec, String paramKey) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_CONFIG_PARAM, new Class[] {
				String.class
		}, new Object[] {
				paramKey
		}, false);
	}

	/**
	 * Renvooie un parametre défini dans le fichier de config ou bien GRHUM.GRHUM_PARAMETRES non mis en cache (les infos sont relues régulièrement).
	 * 
	 * @param ec
	 * @param paramKey
	 * @return
	 */
	public static String clientSideRequestGetGrhumParam(EOEditingContext ec, String paramKey) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_GRHUM_PARAM, new Class[] {
				String.class
		}, new Object[] {
				paramKey
		}, false);
	}

	public static String clientSideRequestRequestsAuthenticationMode(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_REQUESTS_AUTHENTICATION_MODE, null, null, false);
	}

	public static NSDictionary clientSideRequestGetUserIdentity(EOEditingContext ec, String userName, String password, Boolean isCrypted, Boolean useCas) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_USER_IDENTITY, new Class[] {
				String.class, String.class, Boolean.class, Boolean.class
		}, new Object[] {
				userName, password, isCrypted, useCas
		},
				false);
	}

	/**
	 * Informe le serveur de la deconnexion d'un utilisateur
	 */
	public static void clientSideRequestSessDeconnect(EOEditingContext ec, String s) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SESS_DECONNECT, new Class[] {
				String.class
		}, new Object[] {
				s
		}, false);
	}

	public static void clientSideRequestSetClientPlateforme(EOEditingContext ec, String s) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SET_CLIENT_PLATEFORME, new Class[] {
				String.class
		}, new Object[] {
				s
		}, false);
	}

	public static String clientSideRequestGetApplicationName(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_APPLICATION_NAME, null, null, false);
	}

	/**
	 * Permet d'envoyer un mail à partir du client.
	 * 
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public static void clientSideRequestSendMail(EOEditingContext ec, String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, NSArray names, NSArray nsdatas) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SEND_MAIL, new Class[] {
				String.class, String.class, String.class, String.class, String.class, NSArray.class, NSArray.class
		}, new Object[] {
				mailFrom, mailTo, mailCC,
				mailSubject, mailBody, names, nsdatas
		}, false);
	}

	public static void clientSideRequestSendMailToAdmin(EOEditingContext ec, String mailSubject, String mailBody) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SEND_MAIL_TO_ADMIN, new Class[] {
				String.class, String.class
		}, new Object[] {
				mailSubject, mailBody
		}, false);
	}

	public static String clientSideRequestGetEmailForPersonne(EOEditingContext ec, EOGenericRecord personne) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_EMAIL_FOR_PERSONNE, new Class[] {
				EOGenericRecord.class
		}, new Object[] {
				personne
		}, false);
	}

	public static String clientSideRequestGetEtablissementName(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_ETABLISSEMENT_NAME, null, null, false);
	}

	/**
	 * Renvoie l'alias de l'application
	 */
	public static String clientSideRequestGetAppAlias(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_APP_ALIAS, null, null, false);
	}

	/**
	 * Renvoie le dictionnaires des fonctions dispos de l'appli pour le client.
	 * 
	 * @param ec
	 */
	public static NSDictionary clientSideRequestAllFonctions(EOEditingContext ec) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_ALL_FONCTIONS, null, null, false);
	}

	public static NSData clientSideRequestPrintAndWait(EOEditingContext ec, String report, String sql, NSDictionary parametres) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, DELEGATEPRINT_STR, CLIENT_SIDE_REQUEST_PRINT_AND_WAIT, new Class[] {
				String.class, String.class, NSDictionary.class
		}, new Object[] {
				report, sql, parametres
		}, false);
	}

	public static void clientSideRequestPrintByThread(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId, final Boolean printIfEmpty) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, DELEGATEPRINT_STR, CLIENT_SIDE_REQUEST_PRINT_BY_THREAD, new Class[] {
				String.class, String.class, NSDictionary.class, String.class, Boolean.class
		}, new Object[] {
				report, sql, parametres, customJasperId,
				printIfEmpty
		}, false);
	}

	//	public static void clientSideRequestPrintByThreadXls(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId) {
	//		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PRINT_BY_THREAD_XLS, new Class[] {
	//				String.class, String.class, NSDictionary.class, String.class
	//		}, new Object[] {
	//				report, sql, parametres, customJasperId
	//		}, false);
	//	}

	/**
	 * Fait appel à la méthode de numerotation server.
	 * 
	 * @param obj EOEnterpriseObject à numeroter.
	 */
	public static void clientSideRequestNumerotation(EOEditingContext ec, EOEnterpriseObject obj, String objClass) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_NUMEROTATION, new Class[] {
				EOEnterpriseObject.class, String.class
		}, new Object[] {
				obj, objClass
		}, false);
	}

	public static void clientSideRequestAfaireApresTraitementVisaBordereau(EOEditingContext ec, EOEnterpriseObject obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_VISA_BORDEREAU, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static Integer clientSideRequestExtourneCreerBordereau(EOEditingContext ec, EOEnterpriseObject obj) {
		return (Integer) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXTOURNE_CREER_BORDEREAU, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static void clientSideRequestAfaireApresTraitementReimputation(EOEditingContext ec, EOEnterpriseObject obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_REIMPUTATION, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static void clientSideRequestAfaireApresTraitementPaiement(EOEditingContext ec, EOEnterpriseObject obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_PAIEMENT, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static void clientSideRequestAfaireApresTraitementRecouvrement(EOEditingContext ec, EOEnterpriseObject obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_RECOUVREMENT, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static void clientSideRequestAfaireApresTraitementRecouvrementReleve(EOEditingContext ec, EOEnterpriseObject obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_AFAIRE_APRES_TRAITEMENT_RECOUVREMENT_RELEVE, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static String clientSideRequestGetUrlForHelpId(EOEditingContext ec, String id) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_URL_FOR_HELP_ID, new Class[] {
				String.class
		}, new Object[] {
				id
		}, false);
	}

	/**
	 * @param editingContext
	 * @param login
	 * @param ipAdress
	 */
	public static void clientSideRequestHelloImAlive(EOEditingContext ec, String login, String ipAdress) {
		try {
			((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_HELLO_IM_ALIVE, new Class[] {
					String.class, String.class
			}, new Object[] {
					login, ipAdress
			}, false);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void clientSideRequestMsgToServer(EOEditingContext ec, String msg, String ipAdress) {
		try {
			((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_MSG_TO_SERVER, new Class[] {
					String.class, String.class
			}, new Object[] {
					msg, ipAdress
			}, false);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param ec
	 * @return Un tableau de NSDictionary stockant les infos sur les utilisateurs connectés.
	 */
	public static NSArray clientSideRequestGetConnectedUsers(EOEditingContext ec) {
		return (NSArray) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_CONNECTED_USERS, new Class[] {}, new Object[] {}, false);
	}

	/**
	 * Execute une requete sql sur le serveu et renvoie le resultat.
	 * 
	 * @param ec
	 * @param sql
	 * @return
	 */
	public static NSArray clientSideRequestSqlQuery(EOEditingContext ec, String sql) {
		return (NSArray) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SQL_QUERY, new Class[] {
				String.class
		}, new Object[] {
				sql
		}, false);
	}

	/**
	 * @param ec
	 */
	public static void clientSideRequestPrintKillCurrentTask(EOEditingContext ec) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, DELEGATEPRINT_STR, CLIENT_SIDE_REQUEST_PRINT_KILL_CURRENT_TASK, new Class[] {}, new Object[] {}, false);

	}

	public static NSData clientSideRequestPrintDifferedGetPdf(EOEditingContext ec) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, DELEGATEPRINT_STR, CLIENT_SIDE_REQUEST_PRINT_DIFFERED_GET_PDF, new Class[] {}, new Object[] {}, false);
	}

	public static NSDictionary clientSideRequestGetPrintProgression(EOEditingContext ec) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, DELEGATEPRINT_STR, CLIENT_SIDE_REQUEST_GET_PRINT_PROGRESSION, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestPapayePasserEcritureVISABord(EOEditingContext ec, EOBordereau obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_VISABORD, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static void clientSideRequestPapayePasserEcritureSACDBord(EOEditingContext ec, EOBordereau obj) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_SACDBORD, new Class[] {
				EOEnterpriseObject.class
		}, new Object[] {
				obj
		}, false);
	}

	public static void clientSideRequestPapayePasserEcritureOppRetBord(EOEditingContext ec, EOBordereau obj, final String genEcritures) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_OPP_RET_BORD, new Class[] {
				EOEnterpriseObject.class, String.class
		}, new Object[] {
				obj, genEcritures
		}, false);
	}

	public static void clientSideRequestPapayePasserEcriturePaiement(EOEditingContext ec, String obj, final String genEcritures) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAPAYE_PASSER_ECRITURE_PAIEMENT, new Class[] {
				String.class, String.class
		}, new Object[] {
				obj, genEcritures
		}, false);
	}

	public static void clientSideRequestPayePafPasserEcritureVISABord(EOEditingContext ec, EOBordereau obj, EOUtilisateur util) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_VISABORD, new Class[] {
				EOEnterpriseObject.class, EOEnterpriseObject.class
		}, new Object[] {
				obj, util
		}, false);
	}

	public static void clientSideRequestPayePafPasserEcritureSACDBord(EOEditingContext ec, EOBordereau obj, EOUtilisateur util) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_SACDBORD, new Class[] {
				EOEnterpriseObject.class, EOEnterpriseObject.class
		}, new Object[] {
				obj, util
		}, false);
	}

	public static void clientSideRequestPayePafPasserEcritureOppRetBord(EOEditingContext ec, EOBordereau obj, final String genEcritures, EOUtilisateur util) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_OPP_RET_BORD, new Class[] {
				EOEnterpriseObject.class, String.class, EOEnterpriseObject.class
		}, new Object[] {
				obj, genEcritures, util
		}, false);
	}

	public static void clientSideRequestPayePafPasserEcriturePaiement(EOEditingContext ec, String obj, final String genEcritures, EOUtilisateur util) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PAYE_PAF_PASSER_ECRITURE_PAIEMENT, new Class[] {
				String.class, String.class, EOEnterpriseObject.class
		}, new Object[] {
				obj, genEcritures, util
		}, false);
	}

	public static NSData clientSideRequestGetCurrentServerLogFile(EOEditingContext ec) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_CURRENT_SERVER_LOG_FILE, new Class[] {}, new Object[] {}, false);
	}

	public static String clientSideRequestGetApplicationServerLogName(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_APPLICATION_SERVER_LOG_NAME, new Class[] {}, new Object[] {}, false);
	}

	public static String clientSideRequestGetApplicationServerLogPrefix(EOEditingContext ec) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_APPLICATION_SERVER_LOG_PREFIX, new Class[] {}, new Object[] {}, false);
	}

	public static NSData clientSideRequestExportToExcelFromEOs(EOEditingContext ec, NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS, new Class[] {
				NSArray.class, NSArray.class, NSArray.class, NSDictionary.class
		},
				new Object[] {
						keyNames, headers, values, formats
				}, false);
	}

	public static void clientSideRequestExportToExcelFromEOsByThread(EOEditingContext ec, NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS_BY_THREAD, new Class[] {
				NSArray.class, NSArray.class, NSArray.class, NSDictionary.class
		}, new Object[] {
				keyNames, headers, values, formats
		},
				false);
	}

	public static void clientSideRequestExportToExcelFromSQLByThread(EOEditingContext ec, NSArray keyNames, NSArray headers, NSArray keysToSum, String sql, NSDictionary parametres) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_SQLBY_THREAD, new Class[] {
				NSArray.class, NSArray.class, NSArray.class, String.class, NSDictionary.class
		}, new Object[] {
				keyNames, headers,
				keysToSum, sql, parametres
		}, false);
	}

	public static void clientSideRequestExportToExcelFromFetchSpecByThread(EOEditingContext ec, NSArray keyNames, NSArray headers, EOFetchSpecification fetchSpec, NSArray postSorts, NSDictionary parametres) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_FETCH_SPEC_BY_THREAD, new Class[] {
				NSArray.class, NSArray.class, EOFetchSpecification.class, NSArray.class, NSDictionary.class
		}, new Object[] {
				keyNames, headers, fetchSpec, postSorts, parametres
		}, false);
	}

	public static NSData clientSideRequestExportToExcelGetResult(EOEditingContext ec) {
		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_GET_RESULT, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestExportToExcelByThreadCancel(EOEditingContext ec) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_BY_THREAD_CANCEL, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestExportToExcelReset(EOEditingContext ec) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_RESET, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestTestThread(EOEditingContext ec, Integer duree) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_TEST_THREAD, new Class[] {
				Integer.class
		}, new Object[] {
				duree
		}, false);
	}

	public static Object clientSideRequestTestThreadProgress(EOEditingContext ec) {
		return ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_TEST_THREAD_PROGRESS, new Class[] {}, new Object[] {}, false);
	}

	public static EOEnterpriseObject clientSideRequestSavePreference(final EOEditingContext ec, final EOEnterpriseObject util, final String prefKey, final String newValue, final String defaultValue, final String description) throws Exception {
		ZLogger.verbose("clientSideRequestSavePreference");
		return (EOEnterpriseObject) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_SAVE_PREFERENCE, new Class[] {
				EOEnterpriseObject.class, String.class, String.class, String.class, String.class
		}, new Object[] {
				util, prefKey,
				newValue, defaultValue, description
		}, false);
	}

	//    
	public static NSDictionary clientSideRequestGetAppParametres(EOEditingContext ec) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENTSIDEREQUESTGETAPPPARAMETRES_NAME, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestPrintByThreadJXls(EOEditingContext ec, String idreport, String sqlQuery, NSDictionary parametres, String customJasperId) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, DELEGATEPRINT_STR, CLIENT_SIDE_REQUEST_PrintByThreadJXls, new Class[] {
				String.class, String.class, NSDictionary.class, String.class
		}, new Object[] {
				idreport, sqlQuery, parametres, customJasperId
		}, false);
	}

	public static void clientSideRequestAutowash(EOEditingContext ec) {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, "clientSideRequestAutowash", new Class[] {
				}, new Object[] {
				}, false);
	}

	public static String clientSideRequestGetWsResourceUrl(EOEditingContext ec, String fileName) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_GET_WS_RESOURCE_URL, new Class[] {
				String.class
		}, new Object[] {
				fileName
		}, false);
	}

	//	public static NSData clientSideRequestPrintByThreadJXlsGetFile(EOEditingContext ec) {
	//		return (NSData) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PrintByThreadJXlsGetFile, new Class[] {
	//
	//		}, new Object[] {
	//		}, false);
	//	}
	//
	//	public static NSDictionary clientSideRequestPrintByThreadJXlsGetPrintProgression(EOEditingContext ec) {
	//		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PrintByThreadJXlsGetProgression, new Class[] {
	//
	//		}, new Object[] {
	//		}, false);
	//	}

	//	public static void clientSideRequestPrintByThreadJXlsKillCurrentTask(EOEditingContext ec) {
	//		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, SESSION_STR, CLIENT_SIDE_REQUEST_PrintByThreadJXlsKillCurrentTask, new Class[] {
	//
	//		}, new Object[] {
	//		}, false);
	//	}

}
