/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.ListSelectionModel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.FournisseurSrchDialog;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessOrdrePaiement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessOrdrePaiementBrouillard;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard;
import org.cocktail.maracuja.client.metier.EOOrgan;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOTva;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel2;
import org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel;
import org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel2;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Le controleur de saisie
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class OdpSaisieCtrl2 extends CommonCtrl implements OdpSaisiePanel2.IOdpSaisiePanelListener {
	private static final String TITLE = "Saisie d'un ordre de paiement";
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_PAOPSA;
	private final String ACTION_ID_SAISIE_ORDONNATEUR = ZActionCtrl.IDU_PAOPO;

	private final int MODE_MODIF = 1;
	private final int MODE_CREATION = 0;
	private int modeSaisie = MODE_CREATION;

	private final NSArray gestions;
	private final NSArray modePaiements;
	private final NSArray planComptables;
	private final NSArray taux;

	private final HashMap odpDico;
	private EOOrdreDePaiement currentOdp;

	private FactoryProcessOrdrePaiement myFactoryProcessOrdrePaiement;
	private FactoryProcessOrdrePaiementBrouillard myFactoryProcessOrdrePaiementBrouillard;

	private final OdpSaisiePanel2 odpSaisiePanel;

	private final ActionAnnuler actionAnnuler = new ActionAnnuler();
	private final ActionSelecteRetenue actionSelectRetenue = new ActionSelecteRetenue();
	private final ActionValider actionValider = new ActionValider();
	private final ActionSelectFournis actionSelectFournis = new ActionSelectFournis();
	private final ActionSelectOrgan actionSelectOrgan = new ActionSelectOrgan();
	private final ActionActiverSaisieBrouillard actionActiverSaisieBrouillard = new ActionActiverSaisieBrouillard();
	private final ActionOdpBrouillardAdd actionOdpBrouillardAdd = new ActionOdpBrouillardAdd();
	private final ActionOdpBrouillardDelete actionOdpBrouillardDelete = new ActionOdpBrouillardDelete();

	private final OdpBrouillardListPanelListener odpBrouillardListPanelListener = new OdpBrouillardListPanelListener();

	private ZKarukeraDialog win;
	private final ZEOComboBoxModel origineModel;
	private final ZEOComboBoxModel typeOperationModel;
	private final ZEOComboBoxModel tauxModel;
	private final ZEOComboBoxModel modePaiementModel;
	private final ZEOComboBoxModel ribModel;
	private final EODisplayGroup dgPlancomptable;
	private final EODisplayGroup dgGestion;
	private PcoSelectDlg planComptableSelectionDialog;
	private ZEOSelectionDialog gestionSelectionDialog;

	private final Window parentWindow;
	private final String odpEtatDefaut;

	private final EODisplayGroup brouillardsDg;

	/** Liste des planco qui sont affectés en tant de plancovisa ou plancopaiement aux modes de paiement */
	private final NSMutableArray plancosPourModePaiement = new NSMutableArray();

	private final OdpSaisieFormPanelListener odpSaisieFormPanelListener = new OdpSaisieFormPanelListener();

	/**
	 * @throws Exception
	 */
	public OdpSaisieCtrl2(final EOEditingContext lec, Window win) throws Exception {
		super(lec);
		revertChanges();
		parentWindow = win;

		//Récupérer les droits
		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE)) {
			if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE_ORDONNATEUR)) {
				throw new DefaultClientException("Vous n'avez pas les droits nécessaires à la saisie des ordres de paiement.");
			}
			odpEtatDefaut = EOOrdreDePaiement.etatOrdonnateur;
		}
		else {
			odpEtatDefaut = EOOrdreDePaiement.etatValide;
		}

		myFactoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		myFactoryProcessOrdrePaiementBrouillard = new FactoryProcessOrdrePaiementBrouillard(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

		brouillardsDg = new EODisplayGroup();

		odpDico = new HashMap();
		try {
			planComptables = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), null, false);

			gestions = EOsFinder.getAllGestionsPourExercice(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
			//                comptabilites = EOsFinder.getAllComptabilites(getEditingContext());
			modePaiements = EOsFinder.getModePaiementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
			if (modePaiements.count() == 0) {
				throw new DefaultClientException("Aucun mode de paiement valide n'a été récupéré");
			}

			for (int i = 0; i < modePaiements.count(); i++) {
				final EOModePaiement element = (EOModePaiement) modePaiements.objectAtIndex(i);
				if (element.planComptablePaiement() != null && plancosPourModePaiement.indexOfObject(element.planComptablePaiement()) == NSArray.NotFound) {
					plancosPourModePaiement.addObject(element.planComptablePaiement());
				}
				if (element.planComptableVisa() != null && plancosPourModePaiement.indexOfObject(element.planComptableVisa()) == NSArray.NotFound) {
					plancosPourModePaiement.addObject(element.planComptableVisa());
				}
			}
			modePaiementModel = new ZEOComboBoxModel(modePaiements, "modLibelleLong", null, null);

			taux = EOsFinder.fetchArray(getEditingContext(), EOTva.ENTITY_NAME, null, null, new NSArray(EOSortOrdering.sortOrderingWithKey(EOTva.TVA_TAUX_KEY, EOSortOrdering.CompareAscending)), false);

			NSArray typesOperations = EOsFinder.getTypeOperationsPubliques(getEditingContext());
			typeOperationModel = new ZEOComboBoxModel(typesOperations, "topLibelle", null, null);
			typeOperationModel.setSelectedEObject(EOsFinder.fetchObject(getEditingContext(), EOTypeOperation.ENTITY_NAME, "topLibelle=%@", new NSArray(EOTypeOperation.TYPE_OPERATION_GENERIQUE), null, false));

			origineModel = new ZEOComboBoxModel(new NSArray(), "oriLibelle", "", null);
			ribModel = new ZEOComboBoxModel(new NSArray(), EORib.RIB_LIB_LONG_KEY, null, null);
			tauxModel = new ZEOComboBoxModel(taux, EOTva.TVA_TAUX_KEY, null, ZConst.FORMAT_DECIMAL);

			//recuperer le taux par defaut
			String tauxDef = (String) myApp.getParametres().valueForKey("TAU_CODE_DEFAUT");

			if (tauxDef != null) {
				final Number tauxDefNum = new BigDecimal(tauxDef.replaceAll(",", "."));
				ZLogger.debug("tauxDefNum", tauxDefNum);
				if (tauxDefNum != null) {
					EOTva tauxDefault = (EOTva) EOsFinder.fetchObject(getEditingContext(), EOTva.ENTITY_NAME, EOTva.TVA_TAUX_KEY + "=%@", new NSArray(tauxDefNum), null, false);
					ZLogger.debug("tauxDefault", tauxDefault);
					tauxModel.setSelectedEObject(tauxDefault);
					ZLogger.debug("getSelectedEObject", tauxModel.getSelectedEObject());
					ZLogger.debug("getSelectedItem", tauxModel.getSelectedItem());
				}
			}

			odpSaisiePanel = new OdpSaisiePanel2(this, odpSaisieFormPanelListener, odpBrouillardListPanelListener);
			odpSaisiePanel.initGUI();

			//Initialiser les fenetres de selection 
			dgPlancomptable = new EODisplayGroup();
			//On initilise les valeurs ici (le plancomptable n'a pas lieu de changer régulièrement).
			dgPlancomptable.setObjectArray(planComptables);

			dgGestion = new EODisplayGroup();
			dgGestion.setObjectArray(gestions);

		} catch (Exception e) {
			throw e;
		}

	}

	private final void initDicoWithDefault() {
		odpDico.put("odpDate", new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime()));
		odpDico.put("odpDateSaisie", new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime()));
		odpDico.put("odpEtat", odpEtatDefaut);
		odpDico.put("odpHt", null);
		odpDico.put("odpLibelle", null);
		odpDico.put("odpNumero", null);
		odpDico.put("odpTtc", null);
		odpDico.put("odpTva", null);
		odpDico.put("odpLibelleFournisseur", null);
		odpDico.put("comptabilite", getComptabilite());
		odpDico.put("exercice", myApp.appUserInfo().getCurrentExercice());
		odpDico.put("fournisseur", null);
		odpDico.put("modePaiement", modePaiements.objectAtIndex(0));
		//            odpDico.put("ordreDePaiementBrouillards", null);
		odpDico.put("organ", null);
		odpDico.put("origine", null);
		odpDico.put("rib", null);
		odpDico.put(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY, null);
		odpDico.put("utilisateur", myApp.appUserInfo().getUtilisateur());
		//            odpDico.put("virement", null);
	}

	private final void initDicoWithOdp(final EOOrdreDePaiement odp) {

		odpDico.put("odpDateSaisie", new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime()));
		odpDico.put("odpHt", odp.odpHt());
		odpDico.put("odpLibelle", odp.odpLibelle());
		odpDico.put("odpNumero", odp.odpNumero());
		odpDico.put("odpTtc", odp.odpTtc());
		odpDico.put("odpTva", odp.odpTva());
		odpDico.put("odpLibelleFournisseur", odp.odpLibelleFournisseur());
		odpDico.put("comptabilite", odp.comptabilite());
		odpDico.put("exercice", odp.exercice());
		odpDico.put("fournisseur", odp.fournisseur());
		odpDico.put("modePaiement", odp.modePaiement());
		//            odpDico.put("ordreDePaiementBrouillards", null);
		odpDico.put("organ", odp.organ());
		odpDico.put("origine", odp.origine());
		odpDico.put("rib", odp.rib());
		odpDico.put(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY, odp.odpReferencePaiement());

		odpDico.put("utilisateur", myApp.appUserInfo().getUtilisateur());

		//            odpDico.put("virement", odp.);

		//Préparer les brouillards
		final NSArray tmpBrouillards = odp.ordreDePaiementBrouillards();
		for (int i = 0; i < tmpBrouillards.count(); i++) {
			final EOOrdreDePaiementBrouillard element = (EOOrdreDePaiementBrouillard) tmpBrouillards.objectAtIndex(i);
			brouillardsDg.insertObjectAtIndex(createDicoFromBrouillard(element), i);
		}

		modePaiementModel.setSelectedEObject((NSKeyValueCoding) odpDico.get("modePaiement"));
		ribModel.setSelectedEObject((NSKeyValueCoding) odpDico.get("rib"));

		if (odpDico.get("origine") != null) {
			typeOperationModel.setSelectedEObject((NSKeyValueCoding) ((NSKeyValueCoding) odpDico.get("origine")).valueForKey("typeOperation"));
		}
		odpSaisieFormPanelListener.typeOperationChanged();
		origineModel.setSelectedEObject(odp.origine());

	}

	private final NSKeyValueCoding createDicoFromBrouillard(final EOOrdreDePaiementBrouillard element) {
		final NSKeyValueCoding dic = new NSMutableDictionary();
		dic.takeValueForKey(element.odbSens(), EOOrdreDePaiementBrouillard.ODB_SENS_KEY);
		dic.takeValueForKey(element.odbLibelle(), EOOrdreDePaiementBrouillard.ODB_LIBELLE_KEY);
		dic.takeValueForKey(element.odbMontant(), EOOrdreDePaiementBrouillard.ODB_MONTANT_KEY);
		dic.takeValueForKey(element.planComptable(), EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY);
		dic.takeValueForKey(element.gestion(), EOOrdreDePaiementBrouillard.GESTION_KEY);
		return dic;
	}

	private EOOrdreDePaiement createOdpFromDico() throws Exception {
		return myFactoryProcessOrdrePaiement.creerOrdreDePaiement(getEditingContext(),
				(EOComptabilite) odpDico.get("comptabilite"),
				(EOExercice) odpDico.get("exercice"),
				(EOFournisseur) odpDico.get("fournisseur"),
				(EOModePaiement) odpDico.get("modePaiement"),
				(BigDecimal) odpDico.get("odpHt"),
				(String) odpDico.get("odpLibelle"),
				(String) odpDico.get("odpLibelleFournisseur"),
				(Integer) odpDico.get("odpNumero"),
				(BigDecimal) odpDico.get("odpTtc"),
				(BigDecimal) odpDico.get("odpTva"),
				(EOOrigine) odpDico.get("origine"),
				(EORib) odpDico.get("rib"),
				(EOUtilisateur) odpDico.get("utilisateur"),
				(String) odpDico.get("odpEtat"),
				(NSTimestamp) odpDico.get("odpDateSaisie"),
				(EOOrgan) odpDico.get("organ"),
				(String) odpDico.get(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY));
	}

	private void updateOdpFromDico() throws Exception {
		myFactoryProcessOrdrePaiement.updateOrdreDePaiement(
				getEditingContext(),
				currentOdp,
				(EOFournisseur) odpDico.get("fournisseur"),
				(EOModePaiement) odpDico.get("modePaiement"),
				(BigDecimal) odpDico.get("odpHt"),
				(String) odpDico.get("odpLibelle"),
				(String) odpDico.get("odpLibelleFournisseur"),
				(BigDecimal) odpDico.get("odpTtc"),
				(BigDecimal) odpDico.get("odpTva"),
				(EOOrigine) odpDico.get("origine"),
				(EORib) odpDico.get("rib"),
				(EOUtilisateur) odpDico.get("utilisateur"),
				(NSTimestamp) odpDico.get("odpDateSaisie"),
				(EOOrgan) odpDico.get("organ"),
				(String) odpDico.get(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY));
	}

	private final NSKeyValueCoding odpBrouillardAdd(boolean wantFocus) {
		//            EOOrdreDePaiementBrouillard brouillard = myFactoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardVide(getEditingContext(), currentOdp, myApp.appUserInfo().getUtilisateur());
		final NSKeyValueCoding brouillard = creerOrdreDePaiementBrouillardVide();
		int row = odpSaisiePanel.ajouterBrouillardInDg(brouillard);
		if (wantFocus) {
			odpSaisiePanel.focusBrouillardAtRow(row);
		}
		return brouillard;
	}

	private final void odpBrouillardDelete() {
		final NSKeyValueCoding brouillard = odpSaisiePanel.selectedOdpBrouillard();
		if (brouillard != null) {
			//                myFactoryProcessOrdrePaiementBrouillard.annulerOrdreDePaiementBrouillard(getEditingContext(), brouillard, myApp.appUserInfo().getUtilisateur());
			supprimerOrdrePaiementBrouillard(brouillard);
			try {
				odpSaisiePanel.updateDataList();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	private final NSKeyValueCoding creerOrdreDePaiementBrouillardVide() {
		return new NSMutableDictionary();
	}

	private final void supprimerOrdrePaiementBrouillard(NSKeyValueCoding value) {
		final int i = brouillardsDg.allObjects().indexOfObject(value);
		if (i != NSArray.NotFound) {
			brouillardsDg.deleteObjectAtIndex(i);
		}
	}

	private final void updateData() {
		try {
			odpSaisiePanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	//        
	//        private final ZKarukeraDialog createModalDialog(Dialog dial ) {
	//            ZKarukeraDialog win = new ZKarukeraDialog(dial, TITLE, true);
	//            setMyDialog(win);
	//            odpSaisiePanel.setMyDialog(win);
	//            odpSaisiePanel.setPreferredSize(new Dimension(935,650));
	//            win.setContentPane(odpSaisiePanel);
	//            win.pack();
	//            return win;
	//        }
	//        
	//        private final ZKarukeraDialog createModalDialog(Frame dial ) {
	//            ZKarukeraDialog win = new ZKarukeraDialog(dial, TITLE, true);
	//            setMyDialog(win);
	//            odpSaisiePanel.setMyDialog(win);
	//            odpSaisiePanel.setPreferredSize(new Dimension(970,700));
	//            win.setContentPane(odpSaisiePanel);
	//            win.pack();
	//            return win;
	//        }        
	//        
	//        /**
	//         * Affiche une fenetre de saisie d'un nouvel ordre de paiement.
	//         * @return Null ou bien l'ordre de paiement créé.
	//         */
	//        public final EOOrdreDePaiement createNewOrdreDePaiementInWindow(Dialog dial) {
	//            win = createModalDialog(dial);
	////            setMyDialog(win);
	//            EOOrdreDePaiement res = null;
	//            try {
	//                newSaisie();
	//                if (win.open() == ZKarukeraDialog.MROK){
	//                    res = currentOdp;
	//                }
	//            }
	//            finally  {
	//                win.dispose();
	//            }
	//            return res; 
	//        }
	//        
	//
	//
	//        
	//        /**
	//         * Affiche une fenetre de saisie d'un nouvel ordre de paiement.
	//         * @return Null ou bien l'ordre de paiement créé.
	//         */
	//        public final EOOrdreDePaiement createNewOrdreDePaiementInWindow(Frame dial) {
	//            win = createModalDialog(dial);
	//            EOOrdreDePaiement res = null;
	//            try {
	//                newSaisie();
	////                win.show();
	//                if (win.open() == ZKarukeraDialog.MROK){
	//                    res = currentOdp;
	//                }
	//            }
	//            finally  {
	//                win.dispose();
	//            }
	//            return res; 
	//        }
	//
	//        
	//        
	//        
	//        

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		odpSaisiePanel.setMyDialog(win);
		odpSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
		odpSaisiePanel.initGUI();
		win.setContentPane(odpSaisiePanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final EOOrdreDePaiement openDialog(final Window dial, final EOOrdreDePaiement odp) {
		win = createModalDialog(dial);
		setMyDialog(win);

		planComptableSelectionDialog = createPlanComptableSelectionDialog();
		planComptableSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		gestionSelectionDialog = createGestionSelectionDialog();
		gestionSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		EOOrdreDePaiement res = null;
		if (odp != null) {
			currentOdp = odp;
			modeSaisie = MODE_MODIF;
			initDicoWithOdp(odp);
		}
		else {
			currentOdp = null;
			modeSaisie = MODE_CREATION;
			initDicoWithDefault();
		}
		if (EOOrdreDePaiement.etatOrdonnateur.equals(odpEtatDefaut)) {
			win.setTitle(win.getTitle() + " - Mode ordonnateur");
		}
		else {
			win.setTitle(win.getTitle() + " - Mode comptable");
		}

		initSaisie();
		try {
			setWaitCursor(false);
			if (win.open() == ZKarukeraDialog.MROK) {
				res = currentOdp;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			if (!showConfirmationDialog("Confirmation", "Vous n'avez pas validé l'ordre de paiement en cours de saisie, souhaitez-vous néammoins quitter cette fenêtre ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		getEditingContext().revert();
		win.onCancelClick();
	}

	private final void validerSaisie() {
		try {
			if (modeSaisie == MODE_CREATION) {
				validerSaisieForNew();
			}
			else {
				validerSaisieForModify();
			}
			getMyDialog().onOkClick();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void validerSaisieForNew() throws Exception {
		//            myApp.showinfoDialogFonctionNonImplementee();
		odpSaisiePanel.endSaisie();
		checkSaisieOdpDico();

		//Supprimer les details ecritures vides
		brouillardsNettoyer();

		//Vérifier la saisie
		checkSaisieBrouillards();

		//Transformer les dicos en EO
		currentOdp = createOdpFromDico();
		if (currentOdp == null) {
			throw new DataCheckException("L'ordre de paiement n'est pas valide.");
		}

		//Créer les odpBrouillards
		final NSArray brouillardsTmp = brouillardsDg.allObjects();
		final FactoryProcessOrdrePaiementBrouillard factoryProcessOrdrePaiementBrouillard = new FactoryProcessOrdrePaiementBrouillard(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		for (int i = 0; i < brouillardsTmp.count(); i++) {
			final NSKeyValueCoding element = (NSKeyValueCoding) brouillardsTmp.objectAtIndex(i);
			EOOrdreDePaiementBrouillard brouillard;
			if (EOOrdreDePaiementBrouillard.sensDebit.equals(element.valueForKey("odbSens"))) {
				brouillard = factoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardDebit(
						getEditingContext(),
						(BigDecimal) element.valueForKey("odbMontant"),
						currentOdp,
						(EOGestion) element.valueForKey(EOOrdreDePaiementBrouillard.GESTION_KEY),
						(EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY),
						(EOUtilisateur) element.valueForKey("utilisateur"));
			}
			else {
				brouillard = factoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardCredit(getEditingContext(), (BigDecimal) element.valueForKey("odbMontant"), currentOdp, (EOGestion) element.valueForKey("gestion"),
						(EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY), (EOUtilisateur) element.valueForKey("utilisateur"));
			}
			brouillard.setOdbLibelle((String) element.valueForKey("odbLibelle"));
		}

		try {
			odpSaisiePanel.updateDataList();
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

		ZLogger.debug(getEditingContext());

		//la sauvegarde
		//getEditingContext().saveChanges();            
		try {
			getEditingContext().saveChanges();

		}
		//Permet de traiter l'exception qui se produit de temps en temps...
		//pas terrible, mais pour l'instant je ne sais pas d'où vient cette exception
		catch (java.lang.IllegalStateException e) {
			//vérifier si l'objet est bien enregistré
			if ((getEditingContext().globalIDForObject(currentOdp) instanceof EOTemporaryGlobalID) || currentOdp.ordreDePaiementBrouillards() == null || currentOdp.ordreDePaiementBrouillards().count() == 0) {
				System.out.println("Erreur : objet non enregistre = " + currentOdp);
				throw e;
			}
			System.out.println("Erreur IllegalStateException mais objet enregistre = " + currentOdp);
			//sinon on invalide
			getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
					getEditingContext().globalIDForObject(currentOdp)
			}));
		}

		String msgFin = "Ordre de paiement enregistré. ";

		//Si c'est bien enregistre, on numerote et on croise les doigts
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
		try {

			myFactoryProcessOrdrePaiement.numeroterOrdreDePaiement(getEditingContext(), currentOdp, myKFactoryNumerotation);

			msgFin = msgFin + "\n\nL'ordre de paiement saisie porte le numéro " + currentOdp.odpNumero();

			myApp.showInfoDialog(msgFin);
		} catch (Exception e) {
			System.out.println("ERREUR LORS DE LA NUMEROTATION ORDRE DE PAIEMENT...");
			e.printStackTrace();
			throw new Exception(e);
		}

		//mettre à jour le dico
		initDicoWithOdp(currentOdp);

		//Desactiver le bouton valider
		actionValider.setEnabled(false);
	}

	private final void validerSaisieForModify() throws Exception {

		odpSaisiePanel.endSaisie();

		checkSaisieOdpDico();

		//Supprimer les details ecritures vides
		brouillardsNettoyer();

		//Vérifier la saisie
		checkSaisieBrouillards();

		updateOdpFromDico();
		if (currentOdp == null) {
			throw new DataCheckException("L'ordre de paiement n'est pas valide.");
		}

		//Créer les odpBrouillards
		final NSArray brouillardsTmp = brouillardsDg.allObjects();
		final FactoryProcessOrdrePaiementBrouillard factoryProcessOrdrePaiementBrouillard = new FactoryProcessOrdrePaiementBrouillard(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		//Supprimer les brouillards existants
		while (currentOdp.ordreDePaiementBrouillards().count() > 0) {
			factoryProcessOrdrePaiementBrouillard.annulerOrdreDePaiementBrouillard(getEditingContext(), (EOOrdreDePaiementBrouillard) currentOdp.ordreDePaiementBrouillards().objectAtIndex(0), getUtilisateur());

		}

		for (int i = 0; i < brouillardsTmp.count(); i++) {
			final NSKeyValueCoding element = (NSKeyValueCoding) brouillardsTmp.objectAtIndex(i);
			EOOrdreDePaiementBrouillard brouillard;
			if (EOOrdreDePaiementBrouillard.sensDebit.equals(element.valueForKey("odbSens"))) {
				brouillard = factoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardDebit(getEditingContext(), (BigDecimal) element.valueForKey("odbMontant"), currentOdp, (EOGestion) element.valueForKey("gestion"),
						(EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY), (EOUtilisateur) element.valueForKey("utilisateur"));
			}
			else {
				brouillard = factoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardCredit(getEditingContext(), (BigDecimal) element.valueForKey("odbMontant"), currentOdp, (EOGestion) element.valueForKey("gestion"),
						(EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY), (EOUtilisateur) element.valueForKey("utilisateur"));
			}
			brouillard.setOdbLibelle((String) element.valueForKey("odbLibelle"));
		}

		try {
			odpSaisiePanel.updateDataList();
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

		ZLogger.debug(getEditingContext());

		//la sauvegarde
		//getEditingContext().saveChanges();            
		try {
			getEditingContext().saveChanges();

		}
		//Permet de traiter l'exception qui se produit de temps en temps...
		//pas terrible, mais pour l'instant je ne sais pas d'où vient cette exception
		catch (java.lang.IllegalStateException e) {
			//vérifier si l'objet est bien enregistré
			if ((getEditingContext().globalIDForObject(currentOdp) instanceof EOTemporaryGlobalID) || currentOdp.ordreDePaiementBrouillards() == null || currentOdp.ordreDePaiementBrouillards().count() == 0) {
				System.out.println("Erreur : objet non enregistre = " + currentOdp);
				throw e;
			}
			System.out.println("Erreur IllegalStateException mais objet enregistre = " + currentOdp);
			//sinon on invalide
			getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
					getEditingContext().globalIDForObject(currentOdp)
			}));
		}

		//String msgFin = "Ordre de paiement enregistré. ";

		modeSaisie = MODE_MODIF;
		//mettre à jour le dico
		initDicoWithOdp(currentOdp);

		//Desactiver le bouton valider
		actionValider.setEnabled(false);
	}

	/**
	 * Vérifie si la saisie des details est correcte.
	 * 
	 * @throws Exception
	 */
	private final void checkSaisieBrouillards() throws Exception {

		//Vérifie qu'on a des details
		if (brouillardsDg.allObjects().count() == 0) {
			throw new DataCheckException("Aucun brouillard n'a été saisi.");
		}
		//            if (currentOdp.ordreDePaiementBrouillards()==null || currentOdp.ordreDePaiementBrouillards().count()==0) {
		//                throw new DataCheckException("Aucun brouillard n'a été saisi.");
		//            }

		//vérifier que l'écriture est équilibrée
		final NSArray debits = ZEOUtilities.getFilteredArrayByKeyValue(brouillardsDg.allObjects(), "odbSens", ZConst.SENS_DEBIT);
		final NSArray credits = ZEOUtilities.getFilteredArrayByKeyValue(brouillardsDg.allObjects(), "odbSens", ZConst.SENS_CREDIT);

		final BigDecimal sommeDebits = ZEOUtilities.calcSommeOfBigDecimals(debits, "odbMontant");
		final BigDecimal sommeCredits = ZEOUtilities.calcSommeOfBigDecimals(credits, "odbMontant");

		ZLogger.debug("sommeDebits", sommeDebits);
		ZLogger.debug("sommeCredits", sommeCredits);

		if (((sommeDebits == null) || (!sommeDebits.equals(sommeCredits)))) {
			throw new DataCheckException("L'écriture n'est pas équilibrée.");
		}

		//Vérifier que le montant de l'odp est cohérent avec le montant des brouillards
		if (((BigDecimal) odpDico.get("odpTtc")).compareTo(sommeDebits) != 0) {
			throw new DataCheckException("Le montant TTC de l'ordre de paiement n'est pas cohérent avec le montant des brouillards.");
		}
		//            if (currentOdp.odpMontantPaiement().compareTo( sommeDebits  )!= 0 ) {
		//                throw new DataCheckException("Le montant TTC de l'ordre de paiement n'est pas cohérent avec le montant des brouillards.");
		//            }

		//Balayer les brouillards et verifier que la classe des imputations est correcte
		//            NSArray brs = currentOdp.ordreDePaiementBrouillards();
		final NSArray brs = brouillardsDg.allObjects();
		for (int i = 0; i < brs.count(); i++) {
			final NSKeyValueCoding element = (NSKeyValueCoding) brs.objectAtIndex(i);
			if (element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY) == null) {
				throw new DataCheckException("Les comptes d'imputation sont obligatoires dans les brouillards");
			}
			if (element.valueForKey("gestion") == null) {
				throw new DataCheckException("Les codes gestion sont obligatoires dans les brouillards");
			}

			if (!EOPlanComptable.CLASSE4.equals(((EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)).getClasseCompte())
					&& !EOPlanComptable.CLASSE5.equals(((EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)).getClasseCompte())
					&& !"237".equals(((EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)).pcoNum().substring(0, 3))
					&& !"238".equals(((EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)).pcoNum().substring(0, 3))) {
				throw new DataCheckException("Les brouillards doivent être définis avec les comptes de classe " + EOPlanComptable.CLASSE4 + " ou " + EOPlanComptable.CLASSE5 + " ou bien comptes 237 ou 238");
			}

			final EOModePaiement mp = (EOModePaiement) odpDico.get("modePaiement");
			if (ZConst.SENS_CREDIT.equals(element.valueForKey("odbSens")) && (EOModePaiement.MODDOM_CAISSE.equals(mp.modDom()) || EOModePaiement.MODDOM_CHEQUE.equals(mp.modDom()) || EOModePaiement.MODDOM_VIREMENT.equals(mp.modDom()))) {
				if (mp.planComptablePaiement() != null)
					if (!mp.planComptablePaiement().equals((EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY))) {
						throw new DataCheckException("Les écritures de crédit doivent se faire sur le compte du mode de paiement choisi (" + mp.planComptablePaiement().pcoNum() + ")");
					}
			}

		}
	}

	private final void checkSaisieOdpDico() throws Exception {

		if (odpDico.get("odpLibelle") == null) {
			throw new DataCheckException("L'objet est obligatoire.");
		}
		if (odpDico.get("comptabilite") == null) {
			throw new DataCheckException("La comptabilité est obligatoire.");
		}
		if (odpDico.get("exercice") == null) {
			throw new DataCheckException("L'exercice est obligatoire.");
		}
		if ((odpDico.get("fournisseur") == null)) {
			throw new DataCheckException("Vous devez choisir un fournisseur");
		}
		if ((odpDico.get("modePaiement") == null)) {
			throw new DataCheckException("Vous devez choisir un mode de paiement");
		}

		if ((odpDico.get("odpHt") == null) || (((BigDecimal) odpDico.get("odpHt")).doubleValue() == 0)) {
			throw new DataCheckException("Le montant HT ne doit pas être nul");
		}

		if ((odpDico.get("odpTtc") == null) || (((BigDecimal) odpDico.get("odpTtc")).doubleValue() == 0)) {
			throw new DataCheckException("Le montant TTC ne doit pas être nul");
		}

		//Si la tva est nulle on l'initialise à 0
		if (odpDico.get("odpTva") == null) {
			odpDico.put("odpTva", new BigDecimal(0));
		}

		//Si pas mp interne et exercice!=tresorerie => bloquer
		if (!EOModePaiement.MODDOM_INTERNE.equals(((EOModePaiement) odpDico.get("modePaiement")).modDom()) &&
				!EOExercice.EXE_TYPE_TRESORERIE.equals(getExercice().exeType())) {
			throw new DataCheckException("Vous ne pouvez pas créer d'ordre de paiement qui vont réellement générer un paiement sur un exercice autre que l'exercice en cours.");
		}

		//Vérifier que le montant est correctement saisi
		ZLogger.debug("somme", ((BigDecimal) odpDico.get("odpHt")).add((BigDecimal) odpDico.get("odpTva")));

		if (((BigDecimal) odpDico.get("odpHt")).add((BigDecimal) odpDico.get("odpTva")).compareTo((BigDecimal) odpDico.get("odpTtc")) != 0) {
			throw new DataCheckException("Le montant TTC n'est pas égal à la somme du montant HT et de la TVA");
		}

		//Vérifier si le mode de paiement requiert un rib
		if (EOModePaiement.MODDOM_VIREMENT.equals(((EOModePaiement) odpDico.get("modePaiement")).modDom())) {
			if (odpDico.get("rib") == null) {
				throw new DataCheckException("Le RIB doit être précisé, car le mode de paiement que vous avez choisi entraine un virement.");
			}
		}

		//verifier que le mode de paiement a un plancopaiement défini
		if (EOModePaiement.MODDOM_VIREMENT.equals(((EOModePaiement) odpDico.get("modePaiement")).modDom()) ||
				EOModePaiement.MODDOM_CAISSE.equals(((EOModePaiement) odpDico.get("modePaiement")).modDom()) ||
				EOModePaiement.MODDOM_CHEQUE.equals(((EOModePaiement) odpDico.get("modePaiement")).modDom())) {
			if (((EOModePaiement) odpDico.get("modePaiement")).planComptablePaiement() == null) {
				throw new DataCheckException("Le compte à utiliser pour le paiement n'est pas spécifié pour le mode de paiement " + ((EOModePaiement) odpDico.get("modePaiement")).modLibelle()
						+ ", vous devez le définir avant de pouvoir utiliser ce mode de paiement (dans Administration/Modes de paiement)");
			}
		}

		if (odpDico.get(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY) != null && ((String) odpDico.get(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY)).length() > 32) {
			throw new DataCheckException("La référence paiement ne doit pas dépasser 32 caractères.");
		}

	}

	private final void saisieBrouillardActiver() {
		try {
			checkSaisieOdpDico();
			lockSaisieOdp(true);
			lockSaisieOdpBrouillard(false);
			actionActiverSaisieBrouillard.setCancelMode();

			//si on n'a pas déjà de brouillards saisis, on essaie de pré-remplir
			if (brouillardsDg.allObjects().count() == 0) {
				//On crée un nvel objet 
				final NSKeyValueCoding tmpBrouillard = odpBrouillardAdd(true);
				final EOModePaiement mp = ((EOModePaiement) odpDico.get("modePaiement"));
				EOPlanComptable pcoCredit = mp.planComptablePaiement();
				if (pcoCredit == null) {
					pcoCredit = mp.planComptableVisa();
				}

				if (pcoCredit != null) {
					final EOGestion gestion = getComptabilite().gestion();
					brouillardAffecteGestion(gestion, tmpBrouillard);
					brouillardAffectePlanComptable(pcoCredit, tmpBrouillard);
					brouillardAffecteCredit((BigDecimal) odpDico.get("odpTtc"), tmpBrouillard);
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void brouillardsNettoyer() {
		final NSArray tmp = brouillardsDg.allObjects();
		final ArrayList res = new ArrayList();
		for (int i = 0; i < tmp.count(); i++) {
			NSKeyValueCoding element = (NSKeyValueCoding) tmp.objectAtIndex(i);
			if (brouillardIsEmpty(element)) {
				res.add(element);
			}
		}
		if (res.size() > 0) {
			final Iterator iter = res.iterator();
			while (iter.hasNext()) {
				NSKeyValueCoding element = (NSKeyValueCoding) iter.next();
				supprimerOrdrePaiementBrouillard(element);
			}

		}
		//    	    if (currentOdp!=null) {
		//    	        NSArray tmp = currentOdp.ordreDePaiementBrouillards();
		//    	        ArrayList res=new ArrayList();
		//    	        for (int i = 0; i < tmp.count(); i++) {
		//    	            EOOrdreDePaiementBrouillard element = (EOOrdreDePaiementBrouillard) tmp.objectAtIndex(i);
		//    	            if (brouillardIsEmpty(element)) {
		//    	                res.add(element);
		//    	            }	        
		//    	        }
		//    	        if (res.size()>0) {
		//    	            Iterator iter = res.iterator();
		//    	            while (iter.hasNext()) {
		//    	                EOOrdreDePaiementBrouillard element = (EOOrdreDePaiementBrouillard) iter.next();
		//    	                myFactoryProcessOrdrePaiementBrouillard.annulerOrdreDePaiementBrouillard(getEditingContext(), element, myApp.appUserInfo().getUtilisateur());
		//    	            }
		//    	            
		//    	        }
		//    	    }
	}

	private boolean brouillardIsEmpty(NSKeyValueCoding detail) {
		//    	    return (detail.odbMontant()==null ) && (detail.gestion()==null) && (detail.planComptable()==null);
		return (detail.valueForKey("odbMontant") == null) && (detail.valueForKey("gestion") == null) && (detail.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY) == null);

	}

	private final void saisieBrouillardAnnuler() {
		//Nettoyer les details
		brouillardsNettoyer();
		try {
			odpSaisiePanel.updateDataList();
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

		//si details existants, demander confirmation
		NSArray details = brouillardsDg.allObjects();
		if (details.count() > 0) {
			if (!showConfirmationDialog("Confirmation", "Tous les brouillards de cet ordre de paiement vont être supprimés, est-ce vraiment ce que vous voulez faire ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		//supprimer tous les details
		while (brouillardsDg.allObjects().count() > 0) {
			brouillardsDg.deleteObjectAtIndex(0);
		}

		//On annule les modifications sur l'ec car lors de la prochaine activation de saisie 
		//du detail, un nveau  est créé
		getEditingContext().revert();

		try {
			updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
		lockSaisieOdp(false);
		lockSaisieOdpBrouillard(true);
		actionActiverSaisieBrouillard.setOkMode();
	}

	private void lockSaisieOdp(boolean locked) {
		odpSaisiePanel.lockSaisieOdp(locked);
	}

	/**
	 * Rend enebled/disabled les éléments de l'interface qui permettent de sasisir des details ecriture.
	 */
	private void lockSaisieOdpBrouillard(boolean locked) {
		odpSaisiePanel.lockSaisieOdpBrouillard(locked);
		actionOdpBrouillardAdd.setEnabled(!locked);
		actionOdpBrouillardDelete.setEnabled(!locked);
	}

	/**
	 * Initialise l'interface pour la saisie d'un nouvel odp.
	 */
	public final void initSaisie() {
		lockSaisieOdp(false);
		//            lockSaisieOdpBrouillard(true);
		lockSaisieOdpBrouillard(false);
		try {
			odpSaisiePanel.updateData();
			//                actionActiverSaisieBrouillard.setEnabled(true);
			//                actionActiverSaisieBrouillard.setOkMode();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void selectRetenue() {
		myApp.showinfoDialogFonctionNonImplementee();
	}

	private final void selectFournis() {
		FournisseurSrchDialog myFournisseurSrchDialog = new FournisseurSrchDialog(odpSaisiePanel.getMyDialog(), "Sélection d'un fournisseur", getEditingContext());
		if (myFournisseurSrchDialog.open() == ZEOSelectionDialog.MROK) {
			try {
				updateFournis((EOFournisseur) myFournisseurSrchDialog.getSelectedEOObject());
				updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		myFournisseurSrchDialog.dispose();
	}

	private final void selectOrgan() {
		final ZEOSelectionDialog dialog = createOrganSelectionDialog();
		if (dialog != null) {
			dialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			if (dialog.open() == ZEOSelectionDialog.MROK) {
				try {
					updateOrgan((EOOrgan) dialog.getSelectedEOObject());
					updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
			dialog.dispose();
		}
	}

	private final void updateOrgan(EOOrgan organ) {
		odpDico.put("organ", organ);
	}

	private final void updateFournis(EOFournisseur four) {
		odpDico.put("fournisseur", four);

		updateRibsForFournis();
	}

	/**
	 * Mets à jour les ribs en fonction du fournisseur et du mode de paiement.
	 */
	private final void updateRibsForFournis() {
		//          odpDico.put("rib", null);
		NSArray res = new NSArray();
		final EOModePaiement mp = (EOModePaiement) odpDico.get("modePaiement");

		if ((odpDico.get("fournisseur") != null)) {
			if ((mp != null) && EOModePaiement.MODDOM_VIREMENT.equals(mp.modDom())) {
				res = EOsFinder.getRibsValides(getEditingContext(), (EOFournisseur) odpDico.get("fournisseur"));

				//                    res = ZFinder.fetchArray(getEditingContext(), "Rib", "ribValide='O' and fournisseur=%@ and modCode=%@", new NSArray(new Object[]{ odpDico.get("fournisseur"), mp.modCode() } ), null, true);
				//                    ZLogger.debug("Ribs recuperes",res);
			}
		}

		try {
			ribModel.updateListWithData(res);
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}

	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel.IOdpSaisiePanelListener#actionClose()
	 */
	public Action actionAnnuler() {
		return actionAnnuler;
	}

	public final class ActionAnnuler extends AbstractAction {

		public ActionAnnuler() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionSelecteRetenue extends AbstractAction {

		public ActionSelecteRetenue() {
			super("Retenue");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner une retenue à partir de laquelle créer un ordre de paiement");
		}

		public void actionPerformed(ActionEvent e) {
			selectRetenue();
		}

	}

	public final class ActionSelectFournis extends AbstractAction {

		public ActionSelectFournis() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un fournisseur");
		}

		public void actionPerformed(ActionEvent e) {
			selectFournis();
		}

	}

	public final class ActionSelectOrgan extends AbstractAction {

		public ActionSelectOrgan() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner une ligne budgétaire");
		}

		public void actionPerformed(ActionEvent e) {
			selectOrgan();
		}

	}

	public final class ActionOdpBrouillardAdd extends AbstractAction {

		public ActionOdpBrouillardAdd() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un brouillard");
		}

		public void actionPerformed(ActionEvent e) {
			odpBrouillardAdd(true);
		}

	}

	public final class ActionOdpBrouillardDelete extends AbstractAction {

		public ActionOdpBrouillardDelete() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le brouillard sélectionné");
		}

		public void actionPerformed(ActionEvent e) {
			odpBrouillardDelete();
		}

	}

	public final class ActionActiverSaisieBrouillard extends AbstractAction {
		public final int MODE_OK = 0;
		public final int MODE_CANCEL = 1;
		private ImageIcon iconOk;
		private ImageIcon iconCancel;
		private int mode;

		public ActionActiverSaisieBrouillard() {
			super("");
			iconOk = ZIcon.getIconForName(ZIcon.ICON_OK_16);
			iconCancel = ZIcon.getIconForName(ZIcon.ICON_CANCEL_16);
			setOkMode();
			setEnabled(false);
		}

		public void setOkMode() {
			mode = MODE_OK;
			this.putValue(AbstractAction.NAME, "Saisir les brouillards");
			this.putValue(AbstractAction.SMALL_ICON, iconOk);
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Activer la saisie du brouillard");
		}

		public void setCancelMode() {
			mode = MODE_CANCEL;
			this.putValue(AbstractAction.NAME, "Supprimer les brouillards");
			this.putValue(AbstractAction.SMALL_ICON, iconCancel);
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer tous les brouillards");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if (mode == MODE_OK) {
				saisieBrouillardActiver();
			}
			else {
				saisieBrouillardAnnuler();
			}
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel.IOdpSaisiePanelListener#actionValider()
	 */
	public Action actionValider() {
		return actionValider;
	}

	private ZEOSelectionDialog createOrganSelectionDialog() {
		EODisplayGroup myDisplayGroup = new EODisplayGroup();
		//            myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "Organ"));
		Vector myCols = new Vector(2, 0);
		ZEOTableModelColumnWithProvider col1 = new ZEOTableModelColumnWithProvider("Ligne budgétaire", new OrganProvider(myDisplayGroup));
		myCols.add(col1);

		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, EOOrgan.ORG_LIBELLE_KEY, "Libellé");
		myCols.add(col2);

		try {

			Vector filters = new Vector(5, 0);
			filters.add(EOOrgan.ORG_ETAB_KEY);
			filters.add(EOOrgan.ORG_UB_KEY);
			filters.add(EOOrgan.ORG_CR_KEY);
			filters.add(EOOrgan.ORG_SOUSCR_KEY);
			filters.add(EOOrgan.ORG_LIBELLE_KEY);

			myDisplayGroup.setObjectArray(EOsFinder.getAllOrgan(getEditingContext(), getExercice()));
			ZEOSelectionDialog dialog = new ZEOSelectionDialog(win, "Sélection d'une ligne budgétaire", myDisplayGroup, myCols, "Veuillez sélectionner une ligne budgétaire dans la liste", filters);
			return dialog;
		} catch (Exception e) {
			this.showErrorDialog(e);
			return null;
		}

	}

	private final class OrganProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {
		private EODisplayGroup dg;

		/**
             * 
             */
		public OrganProvider(final EODisplayGroup dg1) {
			dg = dg1;
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			return ((EOOrgan) dg.displayedObjects().objectAtIndex(row)).getLongString();
		}

	}

	//        private ZEOSelectionDialog createPlanComptableSelectionDialog() {
	////    	    dgPlancomptable.setObjectArray(myListener.getPlanComptables());
	//    		Vector filterFields = new Vector(2,0);
	//    		filterFields.add(EOPlanComptable.PCO_NUM_KEY);
	////    		filterFields.add(EOPlanComptable.PCO_LIBELLE_KEY);
	//    		
	//    		Vector myCols = new Vector(2,0);
	//    		ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptable.PCO_NUM_KEY, "N°");
	//    		ZEOTableModelColumn col2 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptable.PCO_LIBELLE_KEY, "Libellé");
	//    		
	//    		myCols.add(col1);
	//    		myCols.add(col2);
	//    		
	//    		ZEOSelectionDialog dialog;
	//    		if (parentWindow instanceof Dialog) {
	//    		    dialog = new ZEOSelectionDialog( (Dialog)parentWindow, "Sélection d'une imputation" ,dgPlancomptable,myCols,"Veuillez sélectionner un compte dans la liste",filterFields);
	//    		}
	//    		else {
	//    		    dialog = new ZEOSelectionDialog( (Frame)parentWindow, "Sélection d'une imputation" ,dgPlancomptable,myCols,"Veuillez sélectionner un compte dans la liste",filterFields);
	//    		}
	//    		
	//    		dialog.setFilterPrefix("");
	//    		dialog.setFilterSuffix("*");
	//    		dialog.setFilterInstruction(" like %@");
	//    		
	//            return dialog;		
	//    	}    

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	}

	private ZEOSelectionDialog createGestionSelectionDialog() {
		//    	    dgGestion.setObjectArray(myListener.getPlanComptables());

		Vector myCols = new Vector(2, 0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgGestion, EOGestion.GES_CODE_KEY, "Code gestion");
		myCols.add(col1);
		ZEOSelectionDialog dialog;
		if (parentWindow instanceof Dialog) {
			dialog = new ZEOSelectionDialog((Dialog) parentWindow, "Sélection d'un code gestion", dgGestion, myCols, "Veuillez sélectionner un code gestion dans la liste", null);
		}
		else {
			dialog = new ZEOSelectionDialog((Frame) parentWindow, "Sélection d'un code gestion", dgGestion, myCols, "Veuillez sélectionner un code gestion dans la liste", null);
		}
		return dialog;
	}

	private final EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		if (planComptableSelectionDialog.open(planComptables) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;

		//    		setWaitCursor(true);
		//    		EOPlanComptable res = null;
		//    		planComptableSelectionDialog.getFilterTextField().setText(filter);
		//    		if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
		//    			NSArray selections = planComptableSelectionDialog.getSelectedEOObjects();
		//    			
		//    			if ((selections==null) || (selections.count()==0)) {
		//    			    res = null;
		//    			}
		//    			else {
		//    			    res = (EOPlanComptable) selections.objectAtIndex(0);
		//    			}
		//    		}
		//    		setWaitCursor(false);
		//    		return res;
	}

	private final EOGestion selectGestionIndDialog() {
		setWaitCursor(true);
		EOGestion res = null;
		if (gestionSelectionDialog.open() == ZEOSelectionDialog.MROK) {
			NSArray selections = gestionSelectionDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOGestion) selections.objectAtIndex(0);
			}
		}
		setWaitCursor(false);
		return res;
	}

	private final class OdpBrouillardListPanelListener implements OdpBrouillardListPanel2.IOdpBrouillardListPanelListener {

		//            /**
		//             * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#getData()
		//             */
		//            public NSArray getData() {
		//                if (currentOdp==null) {
		//                    return null;
		//                }
		//                else {
		//                    return currentOdp.ordreDePaiementBrouillards();
		//                }
		//            }
		//            

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#isRowEditable(int)
		 */
		public boolean isRowEditable(int row) {
			return true;
		}

		//            /**
		//             * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#getPlanComptables()
		//             */
		//            public NSArray getPlanComptables() {
		//                return planComptables;
		//            }

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#getGestions()
		 */
		public NSArray getGestions() {
			return gestions;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#addRow()
		 */
		public void addRow() {
			odpBrouillardAdd(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#deleteRow()
		 */
		public void deleteRow() {
			odpBrouillardDelete();
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#findPlanComptable(java.lang.String,
		 *      org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard)
		 */
		public EOPlanComptable findPlanComptable(String pconum) {
			EOPlanComptable res = null;
			NSArray tmp = EOQualifier.filteredArrayWithQualifier(planComptables, EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + pconum + "'", null));
			if (tmp.count() == 1) {
				//pas de doute sur le choix du pcoNum
				res = ((EOPlanComptableExer) tmp.objectAtIndex(0)).planComptable();
			}
			else {
				//on affiche une fenetre de recherche de PlanComptable
				res = selectPlanComptableIndDialog(pconum);
			}
			return res;
		}

		//            /**
		//             * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#odpBrouillardAffectePlanComptable(org.cocktail.maracuja.client.metier.EOPlanComptable)
		//             */
		//            public void odpBrouillardAffectePlanComptable(EOPlanComptable res, EOOrdreDePaiementBrouillard brouillard) {
		//                myFactoryProcessOrdrePaiementBrouillard.affecterPlanComptable(getEditingContext(), res, brouillard);
		//            }

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#findGestion(java.lang.String)
		 */
		public EOGestion findGestion(String gesCode) {
			EOGestion res = null;
			NSArray tmp = EOQualifier.filteredArrayWithQualifier(getGestions(), EOQualifier.qualifierWithQualifierFormat("gesCode like '" + gesCode + "*'", null));
			if (tmp.count() == 1) {
				//pas de doute sur le choix de la gestion
				res = (EOGestion) tmp.objectAtIndex(0);
			}
			else {
				//on affiche une fenetre de recherche de PlanComptable
				res = selectGestionIndDialog();
			}
			return res;
		}

		//
		//            /**
		//             * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#odpBrouillardAffecteGestion(org.cocktail.maracuja.client.metier.EOGestion, org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard)
		//             */
		//            public void odpBrouillardAffecteGestion(EOGestion res, EOOrdreDePaiementBrouillard brouillard) {
		//                myFactoryProcessOrdrePaiementBrouillard.affecterGestion(getEditingContext(), res, brouillard);
		//            }
		//
		//
		//            public void odpBrouillardAffecteCredit(BigDecimal decimal, EOOrdreDePaiementBrouillard tmp) {
		//                //On arrondit si besoin
		//                decimal = decimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		//                myFactoryProcessOrdrePaiementBrouillard.affecterMontantCredit(getEditingContext(), decimal, tmp);
		//            }
		//            
		public void odpBrouillardAffecteDebit(BigDecimal decimal, EOOrdreDePaiementBrouillard tmp) {
			//On arrondit si besoin
			decimal = decimal.setScale(2, BigDecimal.ROUND_HALF_UP);
			myFactoryProcessOrdrePaiementBrouillard.affecterMontantDebit(getEditingContext(), decimal, tmp);
		}

		public void odpBrouillardAffectePlanComptable(EOPlanComptable res, NSKeyValueCoding brouillard) {
			brouillardAffectePlanComptable(res, brouillard);
		}

		public void odpBrouillardAffecteGestion(EOGestion res, NSKeyValueCoding brouillard) {
			brouillardAffecteGestion(res, brouillard);
		}

		public void odpBrouillardAffecteCredit(BigDecimal decimal, NSKeyValueCoding tmp) {
			brouillardAffecteCredit(decimal, tmp);
		}

		public void odpBrouillardAffecteDebit(BigDecimal decimal, NSKeyValueCoding tmp) {
			brouillardAffecteDebit(decimal, tmp);
		}

		public EODisplayGroup getMyDisplayGroup() {
			return brouillardsDg;
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel.IOdpSaisiePanelListener#actionOdpBrouillardDelete()
	 */
	public Action actionOdpBrouillardDelete() {
		return actionOdpBrouillardDelete;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel.IOdpSaisiePanelListener#actionOdpBrouillardAdd()
	 */
	public Action actionOdpBrouillardAdd() {
		return actionOdpBrouillardAdd;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel.IOdpSaisiePanelListener#actionActiverSaisieBrouillard()
	 */
	public Action actionActiverSaisieBrouillard() {
		return actionActiverSaisieBrouillard;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisiePanel.IOdpSaisiePanelListener#actionSelectRetenue()
	 */
	public Action actionSelectRetenue() {
		return actionSelectRetenue;
	}

	private final class OdpSaisieFormPanelListener implements OdpSaisieFormPanel.IOdpSaisieFormPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getActionSelectFournis()
		 */
		public Action actionSelectFournis() {
			return actionSelectFournis;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getDateSaisie()
		 */
		public Date getDateSaisie() {
			return (Date) odpDico.get("odpDateSaisie");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getFournisseur()
		 */
		public EOFournisseur getFournisseur() {
			return (EOFournisseur) odpDico.get("fournisseur");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getLibelle()
		 */
		public String getLibelle() {
			return (String) odpDico.get("odpLibelle");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getModePaiement()
		 */
		public Object getModePaiement() {
			return odpDico.get("modePaiement");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getModePaiements()
		 */
		public NSArray getModePaiements() {
			return modePaiements;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getMontantHT()
		 */
		public Object getMontantHT() {
			return (BigDecimal) odpDico.get("odpHt");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getMontantTTC()
		 */
		public Object getMontantTTC() {
			return (BigDecimal) odpDico.get("odpTtc");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getMontantTVA()
		 */
		public Object getMontantTVA() {
			return (BigDecimal) odpDico.get("odpTva");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getNumero()
		 */
		public Object getNumero() {
			return odpDico.get("odpNumero");
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getOrganLibelle()
		 */
		public String getOrganLibelle() {
			EOOrgan tmp = (EOOrgan) odpDico.get("organ");
			if (tmp != null) {
				return tmp.getLongString();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getOrigineModel()
		 */
		public ZEOComboBoxModel getOrigineModel() {
			return origineModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getRib()
		 */
		public EORib getRib() {
			return (EORib) odpDico.get("rib");
		}

		public ZEOComboBoxModel getRibModel() {
			return ribModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getTypeOperationModel()
		 */
		public ZEOComboBoxModel getTypeOperationModel() {
			return typeOperationModel;
		}

		//            /**
		//             * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#setComptabilite(com.webobjects.foundation.NSKeyValueCoding)
		//             */
		//            public void setComptabilite(NSKeyValueCoding selectedEObject) {
		//                odpDico.put("comptabilite", (EOComptabilite)selectedEObject);
		//            }

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#setModePaiement(com.webobjects.foundation.NSKeyValueCoding)
		 */
		public void setModePaiement(NSKeyValueCoding selectedEObject) {
			final EOModePaiement mp = (EOModePaiement) odpDico.get("modePaiement");
			odpDico.put("modePaiement", (EOModePaiement) selectedEObject);
			updateRibsForFournis();
			updateData();
			EOPlanComptable pcoCreditOld = mp.planComptablePaiement();
			if (pcoCreditOld == null) {
				pcoCreditOld = mp.planComptableVisa();
			}

			EOPlanComptable pcoCreditNew = ((EOModePaiement) odpDico.get("modePaiement")).planComptablePaiement();
			if (pcoCreditNew == null) {
				pcoCreditNew = ((EOModePaiement) odpDico.get("modePaiement")).planComptableVisa();
			}

			final EOQualifier qualSens = EOQualifier.qualifierWithQualifierFormat("odbSens=%@", new NSArray(EOOrdreDePaiementBrouillard.sensCredit));
			final NSArray lesCredits = EOQualifier.filteredArrayWithQualifier(brouillardsDg.allObjects(), qualSens);

			if (pcoCreditNew != null) {
				if (lesCredits.count() > 0) {
					//Mettre a jour les brouillards en fonction du chgt du mode de paiement
					if (pcoCreditOld != null && !pcoCreditNew.equals(pcoCreditOld)) {
						boolean updated = false;
						final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("planComptable=%@", new NSArray(pcoCreditOld));
						final NSArray filteredBrouillards = EOQualifier.filteredArrayWithQualifier(lesCredits, qual);
						for (int i = 0; i < filteredBrouillards.count(); i++) {
							final NSKeyValueCoding element = (NSKeyValueCoding) filteredBrouillards.objectAtIndex(i);
							element.takeValueForKey(pcoCreditNew, EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY);
							updated = true;
						}
						if (updated) {
							showInfoDialog("Les brouillards en crédit définis sur le compte " + pcoCreditOld.pcoNum() + " ont été mis à jour avec le compte " + pcoCreditNew.pcoNum());
						}
					}
					else {
						//Vérifier s'il y a un brouillard avec un planco défini dans les modes de paiement
						for (int i = 0; i < lesCredits.count(); i++) {
							final NSKeyValueCoding element = (NSKeyValueCoding) lesCredits.objectAtIndex(i);
							if (!pcoCreditNew.equals(element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)) && plancosPourModePaiement.indexOfObject(element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)) != NSArray.NotFound) {
								if (showConfirmationDialog("Confirmation", "Voulez-vous remplacer le compte " + ((EOPlanComptable) element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)).pcoNum() + " utilisé pour le brouillard en crédit par le compte " + pcoCreditNew.pcoNum() + " ?",
										ZMsgPanel.BTLABEL_YES)) {
									element.takeValueForKey(pcoCreditNew, EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY);
								}
							}
						}
					}
				}
				else {
					//On ajoute un brouillard credit en auto
					final EOGestion gestion = getComptabilite().gestion();
					NSKeyValueCoding tmpBrouillard = odpBrouillardAdd(true);
					brouillardAffecteGestion(gestion, tmpBrouillard);
					brouillardAffectePlanComptable(pcoCreditNew, tmpBrouillard);
					if ((BigDecimal) odpDico.get("odpTtc") != null) {
						brouillardAffecteCredit((BigDecimal) odpDico.get("odpTtc"), tmpBrouillard);
					}
					else {
						brouillardAffecteCredit(ZConst.BIGDECIMAL_ZERO, tmpBrouillard);
					}

				}
			}
			//Mettre à jour la liste
			try {
				odpSaisiePanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		/**
		 * Met à jour les crédits en fonction suite à un changement de montant
		 */
		public void updateCreditsAfterMontantChanged() {
			final EOModePaiement mp = (EOModePaiement) odpDico.get("modePaiement");
			EOPlanComptable pcoCreditNew = ((EOModePaiement) odpDico.get("modePaiement")).planComptablePaiement();
			if (pcoCreditNew == null) {
				pcoCreditNew = ((EOModePaiement) odpDico.get("modePaiement")).planComptableVisa();
			}

			if (pcoCreditNew != null) {
				final EOQualifier qualSens = EOQualifier.qualifierWithQualifierFormat("odbSens=%@", new NSArray(EOOrdreDePaiementBrouillard.sensCredit));
				final EOQualifier qualPco = new EOKeyValueQualifier(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, pcoCreditNew);
				final NSArray lesCredits = EOQualifier.filteredArrayWithQualifier(brouillardsDg.allObjects(), new EOAndQualifier(new NSArray(new Object[] {
						qualSens, qualPco
				})));
				if (lesCredits.count() > 0) {
					if (lesCredits.count() == 1) {
						final NSKeyValueCoding element = (NSKeyValueCoding) lesCredits.objectAtIndex(0);
						if (pcoCreditNew.equals(element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)) && plancosPourModePaiement.indexOfObject(element.valueForKey(EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY)) != NSArray.NotFound) {
							brouillardAffecteCredit((BigDecimal) odpDico.get("odpTtc"), element);
						}
					}
				}
				else {
					//On ajoute un brouillard credit en auto
					final EOGestion gestion = getComptabilite().gestion();
					NSKeyValueCoding tmpBrouillard = odpBrouillardAdd(false);
					brouillardAffecteGestion(gestion, tmpBrouillard);
					brouillardAffectePlanComptable(pcoCreditNew, tmpBrouillard);
					if ((BigDecimal) odpDico.get("odpTtc") != null) {
						brouillardAffecteCredit((BigDecimal) odpDico.get("odpTtc"), tmpBrouillard);
					}
					else {
						brouillardAffecteCredit(ZConst.BIGDECIMAL_ZERO, tmpBrouillard);
					}
				}
			}

			try {
				odpSaisiePanel.updateDataList();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#selectedRibChanged()
		 */
		public void selectedRibChanged() {
			odpDico.put("rib", (EORib) ribModel.getSelectedEObject());
			//                ZLogger.debug("rib changé");
			//mettre à jour le panel
			odpSaisiePanel.updateRibInfo();
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#setLibelle(java.lang.String)
		 */
		public void setLibelle(String value) {
			odpDico.put("odpLibelle", value);
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#setMontantHT(java.lang.Object)
		 */
		public void setMontantHT(Object value) {
			if ((value == null) || !(value instanceof Number)) {
				odpDico.put("odpHt", null);
			}
			else {
				odpDico.put("odpHt", new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			updateTvaAndTtcFromHT();
			updateCreditsAfterMontantChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#setMontantTTC(java.lang.Object)
		 */
		public void setMontantTTC(Object value) {
			if ((value == null) || !(value instanceof Number)) {
				odpDico.put("odpTtc", null);
			}
			else {
				odpDico.put("odpTtc", new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			updateHtAndTvaFromTtc();
			updateCreditsAfterMontantChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#setMontantTVA(java.lang.Object)
		 */
		public void setMontantTVA(Object value) {
			if ((value == null) || !(value instanceof Number)) {
				odpDico.put("odpTva", null);
			}
			else {
				odpDico.put("odpTva", new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#typeOperationChanged()
		 */
		public void typeOperationChanged() {
			NSArray res = new NSArray();
			if (typeOperationModel.getSelectedEObject() != null) {
				ZLogger.debug("non null");
				res = ((EOTypeOperation) typeOperationModel.getSelectedEObject()).origines();
			}
			try {
				origineModel.updateListWithData(res);
				//                origineChanged();
			} catch (Exception e) {
				odpSaisiePanel.showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#origineChanged()
		 */
		public void origineChanged() {
			odpDico.put("origine", origineModel.getSelectedEObject());
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#actionSelectOrgan()
		 */
		public Action actionSelectOrgan() {
			return actionSelectOrgan;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#getTauxModel()
		 */
		public ZEOComboBoxModel getTauxModel() {
			return tauxModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpSaisieFormPanel.IOdpSaisieFormPanelListener#tauxChanged()
		 */
		public void tauxChanged() {
			//On met à jour le montant tva
			updateTvaAndTtcFromHT();
			updateCreditsAfterMontantChanged();
		}

		public ZEOComboBoxModel getModePaiementModel() {
			return modePaiementModel;
		}

		public Map getDicoValeurs() {
			return odpDico;
		}

		public String getOdpReferencePaiement() {
			return (String) odpDico.get(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY);
		}

		public void setOdpReferencePaiement(String value) {
			odpDico.put(EOOrdreDePaiement.ODP_REFERENCE_PAIEMENT_KEY, value);

		}

	}

	private final void updateTvaAndTtcFromHT() {
		if (odpDico.get("odpHt") != null) {
			EOTva letau = (EOTva) tauxModel.getSelectedEObject();
			if (letau != null) {
				BigDecimal ht = ((BigDecimal) odpDico.get("odpHt")).setScale(2);
				BigDecimal tauTaux = letau.tvaTaux().setScale(2);

				System.out.println("ht = " + ht);
				System.out.println("taux = " + tauTaux);

				BigDecimal tauxPourcent = tauTaux.setScale(4).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP);
				System.out.println("tauxPourcent = " + tauxPourcent);

				BigDecimal montantTva = tauxPourcent.multiply(ht).setScale(2, BigDecimal.ROUND_HALF_UP);

				BigDecimal res = ht.add(montantTva);
				odpDico.put("odpTva", montantTva);
				odpDico.put("odpTtc", res);
				System.out.println("tva = " + montantTva);
				System.out.println("ttc = " + res);
			}
		}
		else {
			odpDico.put("odpTva", null);
			odpDico.put("odpTtc", null);
		}

		odpSaisiePanel.updateDataTTC();
		odpSaisiePanel.updateDataTVA();

	}

	private final void updateHtAndTvaFromTtc() {
		if (odpDico.get("odpTtc") != null) {
			EOTva letau = (EOTva) tauxModel.getSelectedEObject();
			if (letau != null) {
				BigDecimal ttc = (BigDecimal) odpDico.get("odpTtc");
				BigDecimal tauTaux = letau.tvaTaux();
				BigDecimal ht = ttc.divide(((new BigDecimal(1)).add(tauTaux.setScale(4).divide(new BigDecimal(100), BigDecimal.ROUND_HALF_UP))), BigDecimal.ROUND_HALF_UP);
				BigDecimal montantTva = ttc.subtract(ht);

				odpDico.put("odpTva", montantTva);
				odpDico.put("odpHt", ht);
				//                    System.out.println("ttc = " + res);
			}
		}
		else {
			odpDico.put("odpTva", null);
			odpDico.put("odpHt", null);
		}

		odpSaisiePanel.updateDataHT();
		odpSaisiePanel.updateDataTVA();

	}

	public void brouillardAffectePlanComptable(EOPlanComptable res, NSKeyValueCoding brouillard) {
		brouillard.takeValueForKey(res, EOOrdreDePaiementBrouillard.PLAN_COMPTABLE_KEY);
	}

	public void brouillardAffecteGestion(EOGestion res, NSKeyValueCoding brouillard) {
		brouillard.takeValueForKey(res, "gestion");
	}

	public void brouillardAffecteCredit(BigDecimal decimal, NSKeyValueCoding tmp) {
		if (decimal != null) {
			decimal = decimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		tmp.takeValueForKey(decimal, "odbMontant");
		tmp.takeValueForKey(EOOrdreDePaiementBrouillard.sensCredit, "odbSens");
	}

	public void brouillardAffecteDebit(BigDecimal decimal, NSKeyValueCoding tmp) {
		if (decimal != null) {
			decimal = decimal.setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		tmp.takeValueForKey(decimal, "odbMontant");
		tmp.takeValueForKey(EOOrdreDePaiementBrouillard.sensDebit, "odbSens");
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return odpSaisiePanel;
	}

	public String title() {
		return TITLE;
	}

}
