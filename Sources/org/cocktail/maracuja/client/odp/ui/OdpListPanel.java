/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * Panel affichant une liste d'Ordres de paiement.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class OdpListPanel extends ZKarukeraPanel implements ZEOTableListener {
    protected ZEOTable myEOTable;
    protected ZEOTableModel myTableModel;
    protected EODisplayGroup myDisplayGroup;
    protected TableSorter myTableSorter;
    protected Vector myCols;
    private IOdpListPanelListener myListener;

    /**
     * @param editingContext
     */
    public OdpListPanel(IOdpListPanelListener listener) {
        super();
        myListener = listener;
        myDisplayGroup = new EODisplayGroup();
//        initDisplayGroup(editingContext);
    }


//    public void initDisplayGroup(EOEditingContext ec) {
//        myDisplayGroup = new EODisplayGroup();
//        myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(ec, "OrdreDePaiement"));
//        ((EODistributedDataSource) myDisplayGroup.dataSource()).setEditingContext(ec);
//    }




    private void initTableModel() {
        myCols = new Vector(8, 0);

        ZEOTableModelColumn col0 = new ZEOTableModelColumn(myDisplayGroup, "comptabilite.comLibelle", "Comptabilité", 130);
        col0.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, "odpNumero", "N°", 60);
        col5.setAlignment(SwingConstants.LEFT);
        col5.setColumnClass(Integer.class);

        ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, "odpLibelle", "Libellé", 95);
        col6.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col7 = new ZEOTableModelColumn(myDisplayGroup, "origine.oriLibelle", "Origine", 95);
        col7.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "modePaiement.modLibelle", "Mode de paiement", 100);
        col1.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn colUtilisateur = new ZEOTableModelColumn(myDisplayGroup, "utilisateur.nomAndPrenom", "Utilisateur", 110);
        colUtilisateur.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, "odpHt", "Montant HT", 90);
        col2.setAlignment(SwingConstants.RIGHT);
        col2.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col2.setColumnClass(BigDecimal.class);

        ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "odpTva", "TVA", 90);
        col3.setAlignment(SwingConstants.RIGHT);
        col3.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col3.setColumnClass(BigDecimal.class);

        ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "odpTtc", "Montant TTC", 90);
        col4.setAlignment(SwingConstants.RIGHT);
        col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col4.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colOdpDateSaisie = new ZEOTableModelColumn(myDisplayGroup,"odpDateSaisie","Date",60);
		colOdpDateSaisie.setAlignment(SwingConstants.CENTER);
		colOdpDateSaisie.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colOdpDateSaisie.setColumnClass(Date.class);


        ZEOTableModelColumn colEtat = new ZEOTableModelColumn(myDisplayGroup, "odpEtat", "Etat", 90);
        colEtat.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn colFour = new ZEOTableModelColumn(myDisplayGroup, "fournisseur.nomAndPrenom", "Fournisseur", 90);
        colFour.setAlignment(SwingConstants.LEFT);


        myCols.add(col5);
        myCols.add(colOdpDateSaisie);
        myCols.add(colFour);
//        myCols.add(col0);
        myCols.add(col6);
        myCols.add(col7);
        myCols.add(col1);
        myCols.add(col2);
        myCols.add(col3);
        myCols.add(col4);
        myCols.add(colEtat);
        myCols.add(colUtilisateur);

        myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
        myTableSorter = new TableSorter(myTableModel);
    }

    /**
     * Initialise la table à afficher (le modele doit exister)
     */
    private void initTable() {
        myEOTable = new ZEOTable(myTableSorter);
        myEOTable.addListener(this);
        myTableSorter.setTableHeader(myEOTable.getTableHeader());
        myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

    public void initGUI() {
        initTableModel();
        initTable();
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }



    public final EOEnterpriseObject selectedObject() {
        return (EOEnterpriseObject) myDisplayGroup.selectedObject();
    }
    public final NSArray selectedObjects() {
        return myDisplayGroup.selectedObjects();
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(myListener.getData());
        myEOTable.updateData();
//        myTableModel.updateInnerRowCount();
//        myTableModel.fireTableDataChanged();
    }

    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        myListener.onDbClick();
    }

    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.onSelectionChanged();
    }

    public interface IOdpListPanelListener {

        /**
         * @return
         */
        public NSArray getData();

        /**
         *
         */
        public void onSelectionChanged();

        /**
         *
         */
        public void onDbClick();

    }

}

