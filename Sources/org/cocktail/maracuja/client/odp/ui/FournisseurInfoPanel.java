/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;


import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class FournisseurInfoPanel extends JPanel {
    private ZTextField labelFouLibelle;
    private ZTextField labelAdrAdresse1;
    private ZTextField labelAdrAdresse2;
    private ZTextField labelAdrCp;
    private ZTextField labelAdrVille;
    private ZTextField labelFouCode;
    
    private IFournisseurInfoPanelListener myListener;
    
    
    /**
     * 
     */
    public FournisseurInfoPanel(IFournisseurInfoPanelListener listener) {
        super();
        myListener = listener;
    }
    
    
    public void initGui() {
        labelFouLibelle = new ZTextField(new FouLibelleProvider());
        labelAdrAdresse1 = new ZTextField(new FouAdresse1Provider());;
        labelAdrAdresse2 = new ZTextField(new FouAdresse2Provider());;;
        labelAdrCp = new ZTextField(new FouAdrCpProvider());
        labelAdrVille = new ZTextField(new FouAdrVilleProvider());
        labelFouCode = new ZTextField(new FouCodeProvider());
        
        labelFouCode.getMyTexfield().setColumns(6);
        labelFouLibelle.getMyTexfield().setColumns(26);
        labelAdrAdresse1.getMyTexfield().setColumns(35);
        labelAdrAdresse2.getMyTexfield().setColumns(35);
        labelAdrCp.getMyTexfield().setColumns(6);
        labelAdrVille.getMyTexfield().setColumns(26);        
        
        labelFouCode.setUIReadOnly();
        labelFouLibelle.setUIReadOnly();
        labelAdrAdresse1.setUIReadOnly();
        labelAdrAdresse2.setUIReadOnly();
        labelAdrCp.setUIReadOnly();
        labelAdrVille.setUIReadOnly();

        labelFouCode.setEnabled(false);
        labelFouLibelle.setEnabled(false);
        labelAdrAdresse1.setEnabled(false);
        labelAdrAdresse2.setEnabled(false);
        labelAdrCp.setEnabled(false);
        labelAdrVille.setEnabled(false);
        
        
        Box box1 = Box.createVerticalBox();
        box1.add(Box.createHorizontalStrut(340));
        box1.add(ZKarukeraPanel.buildLine(new Component[]{labelFouCode, labelFouLibelle}  ));
        box1.add(ZKarukeraPanel.buildLine(labelAdrAdresse1));
        box1.add(ZKarukeraPanel.buildLine(labelAdrAdresse2));
        box1.add(ZKarukeraPanel.buildLine(new Component[]{labelAdrCp, labelAdrVille}));
        
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(new Action[]{myListener.getActionSelectFournis()}), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createTitledBorder("Fournisseur"));
        this.add(box1, BorderLayout.CENTER);
        this.add(tmp, BorderLayout.EAST);
    }
    
    public void updateData() {
        labelFouLibelle.updateData();
        labelAdrAdresse1.updateData();
        labelAdrAdresse2.updateData();
        labelAdrCp.updateData();
        labelAdrVille.updateData();   
        labelFouCode.updateData();
    }

    
    public interface IFournisseurInfoPanelListener {

        /**
         * @return
         */
        public String getFouVille();

        /**
         * @return
         */
        public Action getActionSelectFournis();

        /**
         * @return
         */
        public String getFouCp();

        /**
         * @return
         */
        public String getFouAdresse2();

        /**
         * @return
         */
        public String getFouAdresse1();

        /**
         * @return
         */
        public String getFouPrenom();

        /**
         * @return
         */
        public String getFouNom();

        /**
         * @return
         */
        public Object getFouCode();
        
    }
    
    
    private final class FouLibelleProvider implements ZTextField.IZTextFieldModel {

        private static final String CHAINEVIDE = "";
        private static final String ESPACE = " ";

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            if (myListener.getFouNom()!=null) {
                return myListener.getFouNom() + ESPACE + ZStringUtil.ifNull(myListener.getFouPrenom(), CHAINEVIDE) ;
            }
            return null;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           return;
        }
        
    }
    
    private final class FouAdresse1Provider implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFouAdresse1();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           return;
        }
        
    }    
    
    private final class FouAdresse2Provider implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFouAdresse2();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           return;
        }
        
    }       
    
    private final class FouAdrCpProvider implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFouCp();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           return;
        }
        
    }
    
    private final class FouAdrVilleProvider implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFouVille();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           return;
        }
        
    }   
    private final class FouCodeProvider implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFouCode();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           return;
        }
        
    }   
}
