/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextField;

/**
 * Affiche le detail d'un rib.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RibInfoPanel extends JPanel {
	private IRibInfoPanelListener myListener;

	private ZTextField fRibBanque;
	private ZTextField fRibGuichet;
	private ZTextField fRibNumero;
	private ZTextField fRibCle;
	private ZTextField fRibBic;
	private ZTextField fRibIban;
	private ZTextField fRibAgence;
	private ZTextField fRibTitCo;

	/**
	 * @param listener
	 */
	public RibInfoPanel(IRibInfoPanelListener listener) {
		super();
		myListener = listener;
	}

	public void initGui() {
		fRibBanque = new ZTextField(new RibBanqueProvider());
		fRibGuichet = new ZTextField(new RibGuichetProvider());
		fRibNumero = new ZTextField(new RibNumeroProvider());
		fRibCle = new ZTextField(new RibCleProvider());
		fRibBic = new ZTextField(new RibBicProvider());
		fRibIban = new ZTextField(new RibIbanProvider());
		fRibTitCo = new ZTextField(new RibTitCoProvider());
		fRibAgence = new ZTextField(new RibAgenceProvider());

		//		fRibBanque.getMyTexfield().setColumns(5);
		//		fRibGuichet.getMyTexfield().setColumns(5);
		//		fRibNumero.getMyTexfield().setColumns(5);
		//		fRibCle.getMyTexfield().setColumns(2);
		//		fRibBic.getMyTexfield().setColumns(11);
		//		fRibIban.getMyTexfield().setColumns(10);
		//		fRibTitCo.getMyTexfield().setColumns(10);
		//		fRibAgence.getMyTexfield().setColumns(10);

		fRibBanque.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
		fRibGuichet.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
		fRibNumero.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
		fRibCle.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
		fRibBic.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fRibIban.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fRibTitCo.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fRibAgence.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);

		fRibBanque.setUIReadOnly();
		fRibGuichet.setUIReadOnly();
		fRibNumero.setUIReadOnly();
		fRibCle.setUIReadOnly();
		fRibBic.setUIReadOnly();
		fRibIban.setUIReadOnly();
		fRibTitCo.setUIReadOnly();
		fRibAgence.setUIReadOnly();

		fRibBanque.setEnabled(false);
		fRibGuichet.setEnabled(false);
		fRibNumero.setEnabled(false);
		fRibCle.setEnabled(false);
		fRibBic.setEnabled(false);
		fRibIban.setEnabled(false);
		fRibTitCo.setEnabled(false);
		fRibAgence.setEnabled(false);

		Box box = Box.createVerticalBox();

		//		box.add(ZKarukeraPanel.buildLine(new ZLabeledComponent("BIC/swift", fRibBic, ZLabeledComponent.LABELONLEFT, 40)));
		//		box.add(ZKarukeraPanel.buildLine(new ZLabeledComponent("Iban", fRibIban, ZLabeledComponent.LABELONLEFT, 40)));
		//		box.add(ZKarukeraPanel.buildLine(new ZLabeledComponent("Titulaire", fRibTitCo, ZLabeledComponent.LABELONLEFT, 45)));

		final ArrayList list = new ArrayList();
		list.add(new Component[] {
				new JLabel("BIC/swift : "), fRibBic
		});
		list.add(new Component[] {
				new JLabel("Agence : "), fRibAgence
		});
		list.add(new Component[] {
				new JLabel("IBAN : "), fRibIban
		});
		list.add(new Component[] {
				new JLabel("Titulaire : "), fRibTitCo
		});

		box.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);

		Box box1 = Box.createHorizontalBox();
		box1.add(new ZLabeledComponent("Banque", fRibBanque, ZLabeledComponent.LABELONTOP, -1));
		box1.add(new ZLabeledComponent("Guichet", fRibGuichet, ZLabeledComponent.LABELONTOP, -1));
		box1.add(new ZLabeledComponent("Compte", fRibNumero, ZLabeledComponent.LABELONTOP, -1));
		box1.add(new ZLabeledComponent("Clé", fRibCle, ZLabeledComponent.LABELONTOP, -1));
		box.add(ZKarukeraPanel.buildLine(box1));

		this.setBorder(BorderFactory.createTitledBorder("Détail du Rib"));
		this.setLayout(new BorderLayout());
		this.add(box, BorderLayout.CENTER);
	}

	public void updateData() {
		fRibBanque.updateData();
		fRibGuichet.updateData();
		fRibNumero.updateData();
		fRibCle.updateData();
		fRibBic.updateData();
		fRibIban.updateData();
		fRibTitCo.updateData();
	}

	public interface IRibInfoPanelListener {

		/**
		 * @return
		 */
		public String getRibGuichet();

		/**
		 * @return
		 */
		public String getRibNumero();

		/**
		 * @return
		 */
		public String getRibBanque();

		/**
		 * @return
		 */
		public String getRibCle();

		/**
		 * @return
		 */
		public String getRibBic();

		/**
		 * @return
		 */
		public String getRibIban();

		/**
		 * @return
		 */
		public Object getRibTitCo();

		public String getRibAgence();

	}

	private final class RibBanqueProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibBanque();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}
	}

	private final class RibGuichetProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibGuichet();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}
	}

	private final class RibNumeroProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibNumero();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}

	}

	private final class RibCleProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibCle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}

	}

	private final class RibBicProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibBic();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}

	}

	private final class RibIbanProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibIban();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}

	}

	private final class RibTitCoProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibTitCo();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}

	}

	private final class RibAgenceProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getRibAgence();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			return;
		}

	}

}
