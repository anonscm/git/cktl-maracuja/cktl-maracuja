/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;

import com.webobjects.foundation.NSArray;


/**
 * Panel permettant de rechercher et d'afficher des ordres de paiement ainsi que leurs brouillards associés.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class OdpRecherchePanel extends ZKarukeraPanel {
    private IOdpRecherchePanelListener myListener;

    private OdpRechercheFilterPanel filterPanel;
    private OdpListPanel odpListPanel;
    private OdpBrouillardListPanel odpBrouillardListPanel;



    /**
     * @param editingContext
     */
    public OdpRecherchePanel(IOdpRecherchePanelListener listener) {
        super();
        myListener = listener;

        filterPanel = new OdpRechercheFilterPanel(new OdpRechercheFilterPanelListener());
        odpListPanel = new OdpListPanel( new OdpListPanelListener());
        odpBrouillardListPanel = new OdpBrouillardListPanel( new OdpBrouillardListPanelListener(), true);
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        filterPanel.setMyDialog(getMyDialog());
        filterPanel.initGUI();
        odpListPanel.initGUI();
        odpBrouillardListPanel.initGUI();

        this.setLayout(new BorderLayout());
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.add(encloseInPanelWithTitle("Filtres de recherche",null,ZConst.BG_COLOR_TITLE,filterPanel,null, null), BorderLayout.NORTH);
        tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
                encloseInPanelWithTitle("Ordres de paiement",null,ZConst.BG_COLOR_TITLE,odpListPanel,null, null),
                encloseInPanelWithTitle("Brouillards",null,ZConst.BG_COLOR_TITLE,odpBrouillardListPanel,null, null)) , BorderLayout.CENTER);

        this.add(buildRightPanel(), BorderLayout.EAST);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);
        this.add(tmp, BorderLayout.CENTER);
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        filterPanel.updateData();
        odpListPanel.updateData();
        odpBrouillardListPanel.updateData();
    }

    private final JPanel buildRightPanel() {
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        ArrayList list = new ArrayList();
        list.add(myListener.actionNew());
        list.add(myListener.actionModify());
        list.add(myListener.actionDelete());
        list.add(myListener.actionValider());
        list.add(myListener.getActionViser());
        list.add(myListener.getActionImprimer());

        ArrayList buttons = ZKarukeraPanel.getButtonListFromActionList(list);

//        JLabel infoTip = new JLabel(ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16));
//        infoTip.setToolTipText("<html><b>Impression multiple</b><br>Vous pouvez imprimer plusieurs ordres de paiement en une seule fois en effectuant une sélection multiple dans la liste<br><i>N.B.: Si vous sélectionnez à la fois des OPs visés et des OPs non visés, deux fichiers différents seront générés.</i> </html>");


        JLabel infoTip = ZTooltip.getTooltip_IMPRODP();
      buttons.add(infoTip);

        tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }

    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionClose());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }



    /**
     * @return L'objet Ordre de paiement actuelment sélectionné.
     */
    public EOOrdreDePaiement getSelectedOdp() {
        return (EOOrdreDePaiement) odpListPanel.selectedObject();
    }

    public NSArray getSelectedOdps() {
        return odpListPanel.selectedObjects();
    }











    public interface IOdpRecherchePanelListener {

        /**
         * @return les brouillards en fonction de l'odp selectionne.
         */
        public NSArray getOdpBrouillards();

        /**
         * @return
         */
        public Action actionValider();

        /**
         * @return
         */
        public Action actionClose();

        /**
         * @return
         */
        public Action actionDelete();

        /**
         * @return
         */
        public Action actionModify();

        /**
         * @return
         */
        public Action actionNew();

        /**
         * @return Les odps à afficher
         */
        public NSArray getOdps();

        public Action getActionImprimer();
        public Action getActionViser();

        /**
         * @return un dictioniare contenant les filtres
         */
        public HashMap getFilters();

        /**
         * @return
         */
        public Action actionSrch();

        /**
         *
         */
        public void odpSelectionChanged();

    }


    private final class OdpRechercheFilterPanelListener implements OdpRechercheFilterPanel.IOdpRechercheFilterPanel {



        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpRechercheFilterPanel.IOdpRechercheFilterPanel#getFilters()
         */
        public HashMap getFilters() {
            return myListener.getFilters();
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpRechercheFilterPanel.IOdpRechercheFilterPanel#getActionSrch()
         */
        public Action getActionSrch() {
            return myListener.actionSrch();
        }

    }

    private final class OdpBrouillardListPanelListener implements OdpBrouillardListPanel.IOdpBrouillardListPanelListener {

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#getData()
         */
        public NSArray getData() {
            return myListener.getOdpBrouillards();
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#isRowEditable(int)
         */
        public boolean isRowEditable(int row) {
            return false;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#getPlanComptables()
         */
        public NSArray getPlanComptables() {
            return null;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#getGestions()
         */
        public NSArray getGestions() {
            return null;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#addRow()
         */
        public void addRow() {
            return;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#deleteRow()
         */
        public void deleteRow() {
            return;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#findPlanComptable(java.lang.String)
         */
        public EOPlanComptable findPlanComptable(String pconum) {
            return null;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#odpBrouillardAffectePlanComptable(org.cocktail.maracuja.client.metier.EOPlanComptable, org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard)
         */
        public void odpBrouillardAffectePlanComptable(EOPlanComptable res, EOOrdreDePaiementBrouillard brouillard) {

        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#findGestion(java.lang.String)
         */
        public EOGestion findGestion(String gesCode) {
            return null;
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#odpBrouillardAffecteGestion(org.cocktail.maracuja.client.metier.EOGestion, org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard)
         */
        public void odpBrouillardAffecteGestion(EOGestion res, EOOrdreDePaiementBrouillard brouillard) {
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#odpBrouillardAffecteCredit(java.math.BigDecimal, org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard)
         */
        public void odpBrouillardAffecteCredit(BigDecimal decimal, EOOrdreDePaiementBrouillard tmp) {

        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpBrouillardListPanel.IOdpBrouillardListPanelListener#odpBrouillardAffecteDebit(java.math.BigDecimal, org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard)
         */
        public void odpBrouillardAffecteDebit(BigDecimal decimal, EOOrdreDePaiementBrouillard tmp) {

        }

    }

    private final class OdpListPanelListener implements OdpListPanel.IOdpListPanelListener {

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpListPanel.IOdpListPanelListener#getData()
         */
        public NSArray getData() {
            return myListener.getOdps();
        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpListPanel.IOdpListPanelListener#onSelectionChanged()
         */
        public void onSelectionChanged() {
            try {
                myListener.odpSelectionChanged();
                odpBrouillardListPanel.updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }

        }

        /**
         * @see org.cocktail.maracuja.client.odp.ui.OdpListPanel.IOdpListPanelListener#onDbClick()
         */
        public void onDbClick() {
            if (myListener.actionModify().isEnabled()) {
                myListener.actionModify().actionPerformed(new ActionEvent(OdpRecherchePanel.this,ActionEvent.ACTION_PERFORMED, null));
            }
        }

    }



    public OdpListPanel getOdpListPanel() {
        return odpListPanel;
    }
}
