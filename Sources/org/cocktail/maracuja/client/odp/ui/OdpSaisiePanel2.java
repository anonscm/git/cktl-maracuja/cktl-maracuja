/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;

import com.webobjects.foundation.NSKeyValueCoding;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class OdpSaisiePanel2 extends ZKarukeraPanel {
	private IOdpSaisiePanelListener myListener;

	private OdpSaisieFormPanel saisieFormPanel;
	private OdpBrouillardListPanel2 odpBrouillardListPanel;

	//    private OdpSaisieCtrl myOdpSaisiePanelCtrl;

	/**
	 * @param editingContext
	 * @param listener Listener de ce panel
	 * @param listener2 Listener du panel form
	 */
	public OdpSaisiePanel2(IOdpSaisiePanelListener listener, OdpSaisieFormPanel.IOdpSaisieFormPanelListener listener2, OdpBrouillardListPanel2.IOdpBrouillardListPanelListener listener3) {
		super();
		//        myOdpSaisiePanelCtrl = new OdpSaisieCtrl(editingContext);
		myListener = listener;

		saisieFormPanel = new OdpSaisieFormPanel(listener2);
		odpBrouillardListPanel = new OdpBrouillardListPanel2(listener3, false);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		saisieFormPanel.initGUI();
		odpBrouillardListPanel.initGUI();

		JPanel p = new JPanel(new BorderLayout());
		p.add(saisieFormPanel, BorderLayout.NORTH);
		p.add(buildOdpBrouillardPanel(), BorderLayout.CENTER);

		this.setLayout(new BorderLayout());
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(p, BorderLayout.CENTER);
	}

	private JToolBar buildToolBarOdpBrouillard() {
		JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);
		//Initialiser les boutons à partir des iactions	
		myToolBar.add(myListener.actionOdpBrouillardAdd());
		myToolBar.add(myListener.actionOdpBrouillardDelete());
		return myToolBar;
	}

	private JPanel buildOdpBrouillardPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.add(buildToolBarOdpBrouillard(), BorderLayout.NORTH);
		p.add(odpBrouillardListPanel, BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Brouillards", null, ZConst.BG_COLOR_TITLE, p, null, null);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		saisieFormPanel.updateData();
		odpBrouillardListPanel.updateData();
	}

	public void endSaisie() {
		odpBrouillardListPanel.endSaisie();

	}

	/**
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public interface IOdpSaisiePanelListener {

		/**
		 * @return
		 */
		public Action actionSelectRetenue();

		/**
		 * @return
		 */
		public Action actionOdpBrouillardDelete();

		/**
		 * @return
		 */
		public Action actionOdpBrouillardAdd();

		public Action actionValider();

		public Action actionAnnuler();

		/**
		 * @return
		 */
		public Action actionActiverSaisieBrouillard();

	}

	private final JPanel buildRightPanel() {
		ArrayList list = new ArrayList();
		//        list.add(myListener.actionActiverSaisieBrouillard());
		list.add(myListener.actionSelectRetenue());

		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		tmp.setPreferredSize(new Dimension(150, 300));
		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionAnnuler());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @param locked
	 */
	public void lockSaisieOdp(boolean locked) {
		saisieFormPanel.lockSaisieOdp(locked);
	}

	/**
	 * @param locked
	 */
	public void lockSaisieOdpBrouillard(boolean locked) {
		odpBrouillardListPanel.lockSaisieOdpBrouillard(locked);
	}

	/**
	 * @param brouillard
	 */
	public int ajouterBrouillardInDg(NSKeyValueCoding brouillard) {
		return odpBrouillardListPanel.ajouterInDg(brouillard);
	}

	public void focusBrouillardAtRow(int row) {
		odpBrouillardListPanel.focusAtRow(row);
	}

	/**
	 * @return
	 */
	public NSKeyValueCoding selectedOdpBrouillard() {
		return (NSKeyValueCoding) odpBrouillardListPanel.myDisplayGroup.selectedObject();
	}

	/**
     * 
     */
	public void updateDataList() throws Exception {
		odpBrouillardListPanel.updateData();
	}

	public final void updateDataTTC() {
		saisieFormPanel.getMyMontantTTCField().updateData();
	}

	/**
     * 
     */
	public void updateDataTVA() {
		saisieFormPanel.getMyMontantTVAField().updateData();
	}

	/**
     * 
     */
	public void updateDataHT() {
		saisieFormPanel.getMyMontantHTField().updateData();

	}

	public void updateRibInfo() {
		saisieFormPanel.getMyRibInfoPanel().updateData();

	}

}
