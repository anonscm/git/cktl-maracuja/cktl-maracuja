/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import java.util.Date;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelComboBoxField;
import org.cocktail.maracuja.client.common.ui.ZLabelField;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class OdpSaisieFormPanel extends ZKarukeraPanel {

	private IOdpSaisieFormPanelListener myListener;
	private static final int DEFAULT_LABEL_WIDTH = 100;

	//    private ZEOComboBoxModel myComptabiliteModel;
	//    private ZEOComboBoxModel myModePaiementModel;

	//    private final ActionSelectFournis myActionSelectFournis = new ActionSelectFournis();
	//    private final ActionSelectOrgan myActionSelectOrgan = new ActionSelectOrgan();

	//    private ZActionField myFournisseurField;
	private ZActionField myOrganField;
	private ZLabelComboBoxField myModePaiementField;
	//    private ZLabelComboBoxField myComptabiliteField;
	private JComboBox myTypeOperationField;
	private JComboBox myOrigineField;
	private JComboBox myRibField;

	private JComponent myRibSelectBox;

	private ZTextField myLibelle;
	private ZTextField myOdpReferencePaiement;
	private ZTextField myDateSaisie;
	private ZTextField myNumero;
	private ZNumberField myMontantHTField;
	private ZNumberField myMontantTVAField;
	private ZNumberField myMontantTTCField;

	private FournisseurInfoPanel myFournisseurInfoPanel;
	private RibInfoPanel myRibInfoPanel;

	private JComboBox myTauxSelect;

	/**
	 * @param editingContext
	 */
	public OdpSaisieFormPanel(IOdpSaisieFormPanelListener listener) {
		super();
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		setLayout(new BorderLayout());
		//
		//        try {
		//            myComptabiliteModel =  new ZEOComboBoxModel(myListener.getComptabilites(), "comLibelle", null,null);
		//            myModePaiementModel = new ZEOComboBoxModel(myListener.getModePaiements(), "modLibelleLong", null,null);
		//
		//        } catch (Exception e) {
		//            showErrorDialog(e);
		//        }

		//        myComptabiliteField = new ZLabelComboBoxField("Comptabilité", ZLabelField.LABELONLEFT, DEFAULT_LABEL_WIDTH, myComptabiliteModel);
		//        myComptabiliteField.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
		//        ((JComboBox)myComptabiliteField.getContentComponent()).addActionListener((new ComptabiliteListener()));

		myRibField = new JComboBox(myListener.getRibModel());
		myRibField.addActionListener(new RibSelectListener());

		myTypeOperationField = new JComboBox(myListener.getTypeOperationModel());
		myTypeOperationField.addActionListener(new TypeOperationListener());

		myTauxSelect = new JComboBox(myListener.getTauxModel());
		myTauxSelect.addActionListener(new TauxSelectListener());

		myOrigineField = new JComboBox(myListener.getOrigineModel());
		myOrigineField.addActionListener(new OrigineListener());

		Box boxOrigine = Box.createHorizontalBox();
		boxOrigine.add(new ZLabeledComponent("Type Opération", myTypeOperationField, ZLabeledComponent.LABELONTOP, -1));
		boxOrigine.add(new ZLabeledComponent("Origine", myOrigineField, ZLabeledComponent.LABELONTOP, -1));
		boxOrigine.add(Box.createHorizontalGlue());

		myLibelle = new ZTextField(new LibelleModel());
		myLibelle.getMyTexfield().setColumns(35);

		myOdpReferencePaiement = new ZTextField(new OdpRefPaiementMdl());
		myOdpReferencePaiement.getMyTexfield().setColumns(32);
		myOdpReferencePaiement.setMaxChars(32);

		myDateSaisie = new ZTextField(new DateSaisieModel());
		myDateSaisie.getMyTexfield().setColumns(10);
		myDateSaisie.getMyTexfield().setEditable(false);
		myDateSaisie.setUIReadOnly();
		myDateSaisie.setFormat(ZConst.FORMAT_DATESHORT);
		myDateSaisie.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);

		myNumero = new ZTextField(new NumeroModel());
		myNumero.getMyTexfield().setColumns(10);
		myNumero.getMyTexfield().setEditable(false);
		myNumero.setUIReadOnly();
		myNumero.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);

		myMontantHTField = new ZNumberField(new MontantHTModel(), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL, ZConst.FORMAT_DISPLAY_NUMBER
		}, ZConst.FORMAT_DISPLAY_NUMBER);
		myMontantHTField.getMyTexfield().setColumns(10);

		myMontantTVAField = new ZNumberField(new MontantTVAModel(), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL, ZConst.FORMAT_DISPLAY_NUMBER
		}, ZConst.FORMAT_DISPLAY_NUMBER);
		myMontantTVAField.getMyTexfield().setColumns(10);
		myMontantTVAField.setEnabled(false);

		myMontantTTCField = new ZNumberField(new MontantTTCModel(), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL, ZConst.FORMAT_DISPLAY_NUMBER
		}, ZConst.FORMAT_DISPLAY_NUMBER);
		myMontantTTCField.getMyTexfield().setColumns(10);

		//        myFournisseurField = new ZActionField(new FournisseurLibelleModel(), myActionSelectFournis);
		//        myFournisseurField.getMyTexfield().setColumns(30);

		myOrganField = new ZActionField(new OrganLibelleModel(), getActionSelectOrgan());
		myOrganField.getMyTexfield().setColumns(30);

		//        myModePaiementField = new ZLabelComboBoxField("Mode de paiement", ZLabelField.LABELONLEFT, -1, myModePaiementModel);
		myModePaiementField = new ZLabelComboBoxField("Mode de paiement", ZLabelField.LABELONLEFT, -1, myListener.getModePaiementModel());
		((JComboBox) myModePaiementField.getContentComponent()).addActionListener(new ModePaiementListener());

		//        Box boxfournis = Box.createVerticalBox();
		//        boxfournis.add(buildLine(new ZLabeledComponent("Fournisseur",  myFournisseurField, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));

		Box boxOrgan = Box.createVerticalBox();
		boxOrgan.add(buildLine(new ZLabeledComponent("Ligne budgétaire", myOrganField, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));

		myRibSelectBox = new ZLabeledComponent("Rib", myRibField, ZLabeledComponent.LABELONLEFT, -1);

		myFournisseurInfoPanel = new FournisseurInfoPanel(new FournisseurInfoListener());
		myFournisseurInfoPanel.initGui();

		myRibInfoPanel = new RibInfoPanel(new RibInfoPanelListener());
		myRibInfoPanel.initGui();

		Box col1 = Box.createVerticalBox();
		//        col1.add(buildLine(myComptabiliteField));
		col1.add(buildLine(new ZLabeledComponent("Objet", myLibelle, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col1.add(buildLine(new ZLabeledComponent("Ref. Paiement", myOdpReferencePaiement, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col1.add(buildLine(boxOrgan));
		col1.add(buildLine(myFournisseurInfoPanel));
		col1.add(buildLine(new ZLabeledComponent("Montant HT", myMontantHTField, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col1.add(buildLine(new Component[] {
				new ZLabeledComponent("Montant TVA", myMontantTVAField, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH), new ZLabeledComponent("Taux", myTauxSelect, ZLabeledComponent.LABELONLEFT, 40)
		}));
		col1.add(buildLine(new ZLabeledComponent("Montant TTC", myMontantTTCField, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col1.add(Box.createVerticalGlue());

		Box col2 = Box.createVerticalBox();
		col2.add(buildLine(new ZLabeledComponent("Date de saisie", myDateSaisie, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col2.add(buildLine(new ZLabeledComponent("N°", myNumero, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col2.add(buildLine(myModePaiementField));
		col2.add(buildLine(myRibSelectBox));
		col2.add(buildLine(myRibInfoPanel));

		col2.add(boxOrigine);
		col2.add(Box.createVerticalGlue());

		Box boxH = Box.createHorizontalBox();
		boxH.add(col1);
		boxH.add(col2);
		boxH.add(Box.createHorizontalGlue());

		this.add(boxH, BorderLayout.WEST);
		this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		//TODO synchroniser avec la selection (vérifier mais ca doit être bon)
		//        myModePaiementField.updateData();
		//        myComptabiliteField.updateData();
		//        myFournisseurField.updateData();

		myLibelle.updateData();
		myOdpReferencePaiement.updateData();
		myDateSaisie.updateData();
		myNumero.updateData();
		myMontantHTField.updateData();
		myMontantTVAField.updateData();
		myMontantTTCField.updateData();
		myFournisseurInfoPanel.updateData();
		myRibInfoPanel.updateData();
		myOrganField.updateData();

		//        myListener.getModePaiementModel().setSelectedEObject((NSKeyValueCoding) myListener.getModePaiement());
		//        myListener.getRibModel().setSelectedEObject((NSKeyValueCoding) myListener.getDicoValeurs().get("rib"));
		//        
		//        if (myListener.getDicoValeurs().get("origine") != null) {
		//            myListener.getTypeOperationModel().setSelectedEObject((NSKeyValueCoding) ((NSKeyValueCoding) myListener.getDicoValeurs().get("origine")).valueForKey("typeOperation") );
		//        }
		//        
		//        myListener.getOrigineModel().setSelectedEObject((NSKeyValueCoding) myListener.getDicoValeurs().get("origine"));
		//        

		//        if (myListener.getDicoValeurs().get("odpTva") != null ) {
		//            myListener.getTauxModel().setSelectedEObject((NSKeyValueCoding) myListener.getDicoValeurs(). get());
		//        }

		if ((myListener.getFournisseur() == null) || (myListener.getModePaiement() == null)) {
			myRibSelectBox.setVisible(false);
		}
		else {
			myRibSelectBox.setVisible(true);
		}

	}

	public AbstractAction getActionSelectOrgan() {
		return (AbstractAction) myListener.actionSelectOrgan();
	}

	//    public ZEOComboBoxModel getMyComptabiliteModel() {
	//        return myComptabiliteModel;
	//    }

	private class NumeroModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getNumero();
		}

		public void setValue(Object value) {
		}

	}

	/**
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public interface IOdpSaisieFormPanelListener {

		public ZEOComboBoxModel getModePaiementModel();

		/**
		 * @return
		 */
		public ZEOComboBoxModel getTauxModel();

		/**
		 * @return
		 */
		public Object getModePaiement();

		/**
		 * @return
		 */
		public ZEOComboBoxModel getOrigineModel();

		public ZEOComboBoxModel getRibModel();

		/**
		 * @return
		 */
		public ZEOComboBoxModel getTypeOperationModel();

		/**
		 * @return
		 */
		public NSArray getModePaiements();

		/**
		 * @return
		 */
		public Object getNumero();

		//        /**
		//         * @param selectedEObject
		//         */
		//        public void setComptabilite(NSKeyValueCoding selectedEObject);

		public String getLibelle();

		public void setLibelle(String value);

		public String getOdpReferencePaiement();

		public void setOdpReferencePaiement(String value);

		/**
		 * @return
		 */
		public Date getDateSaisie();

		/**
		 * @return
		 */
		public Object getMontantHT();

		/**
		 * @param value
		 * @return
		 */
		public void setMontantHT(Object value);

		/**
		 * @return
		 */
		public Object getMontantTTC();

		/**
		 * @param value
		 */
		public void setMontantTTC(Object value);

		/**
		 * @return
		 */
		public Object getMontantTVA();

		/**
		 * @param value
		 */
		public void setMontantTVA(Object value);

		//        /**
		//         * @return
		//         */
		//        public String getFournisseurLibelle();

		/**
		 * @return
		 */
		public String getOrganLibelle();

		public EOFournisseur getFournisseur();

		public EORib getRib();

		/**
         *
         */
		public Action actionSelectFournis();

		public Action actionSelectOrgan();

		/**
		 * @param selectedEObject
		 */
		public void setModePaiement(NSKeyValueCoding selectedEObject);

		/**
         *
         */
		public void typeOperationChanged();

		/**
         *
         */
		public void origineChanged();

		/**
         *
         */
		public void selectedRibChanged();

		/**
         *
         */
		public void tauxChanged();

		public Map getDicoValeurs();
	}

	//    private final class ComptabiliteListener implements ActionListener {
	//
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            myListener.setComptabilite(getMyComptabiliteModel().getSelectedEObject());
	//        }
	//    }

	private final class ModePaiementListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.setModePaiement(myListener.getModePaiementModel().getSelectedEObject());
		}
	}

	private final class TypeOperationListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.typeOperationChanged();
		}
	}

	private final class TauxSelectListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.tauxChanged();
		}
	}

	private final class OrigineListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.origineChanged();
			//            myListener.setOrigine(myOrigineModel.getSelectedEObject());
		}
	}

	private final class RibSelectListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.selectedRibChanged();
		}
	}

	private class LibelleModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getLibelle();
		}

		public void setValue(Object value) {
			myListener.setLibelle((String) value);
		}
	}

	private class OdpRefPaiementMdl implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getOdpReferencePaiement();
		}

		public void setValue(Object value) {
			myListener.setOdpReferencePaiement((String) value);
		}
	}

	private final class OrganLibelleModel implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return myListener.getOrganLibelle();
		}

		public void setValue(Object value) {

		}
	}

	private final class DateSaisieModel implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return myListener.getDateSaisie();
		}

		public void setValue(Object value) {
		}
	}

	private final class MontantHTModel implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return myListener.getMontantHT();
		}

		public void setValue(Object value) {
			myListener.setMontantHT(value);
		}
	}

	private final class MontantTTCModel implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return myListener.getMontantTTC();
		}

		public void setValue(Object value) {
			myListener.setMontantTTC(value);
		}
	}

	private final class MontantTVAModel implements ZTextField.IZTextFieldModel {
		public Object getValue() {
			return myListener.getMontantTVA();
		}

		public void setValue(Object value) {
			myListener.setMontantTVA(value);
		}
	}

	private final class FournisseurInfoListener implements FournisseurInfoPanel.IFournisseurInfoPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouVille()
		 */
		public String getFouVille() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().adrVille();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getActionSelectFournis()
		 */
		public Action getActionSelectFournis() {
			return myListener.actionSelectFournis();
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouCp()
		 */
		public String getFouCp() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().adrCp();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouAdresse2()
		 */
		public String getFouAdresse2() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().adrAdresse2();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouAdresse1()
		 */
		public String getFouAdresse1() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().adrAdresse1();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouPrenom()
		 */
		public String getFouPrenom() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().adrPrenom();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouNom()
		 */
		public String getFouNom() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().adrNom();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.FournisseurInfoPanel.IFournisseurInfoPanelListener#getFouCode()
		 */
		public Object getFouCode() {
			if (myListener.getFournisseur() != null) {
				return myListener.getFournisseur().fouCode();
			}
			return null;
		}
	}

	private final class RibInfoPanelListener implements RibInfoPanel.IRibInfoPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibGuichet()
		 */
		public String getRibGuichet() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribGuich();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibNumero()
		 */
		public String getRibNumero() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribNum();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibBanque()
		 */
		public String getRibBanque() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribCodBanc();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibCle()
		 */
		public String getRibCle() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribCle();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibBic()
		 */
		public String getRibBic() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribBic();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibIban()
		 */
		public String getRibIban() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribIban();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibTitCo()
		 */
		public Object getRibTitCo() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribTitco();
			}
			return null;
		}

		public String getRibAgence() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribAgence();
			}
			return null;
		}

	}

	/**
	 * @param locked
	 */
	public void lockSaisieOdp(boolean locked) {
		//        myComptabiliteField.getContentComponent().setEnabled(!locked);
		myLibelle.getContentComponent().setEnabled(!locked);
		myOdpReferencePaiement.getContentComponent().setEnabled(!locked);
		myDateSaisie.getContentComponent().setEnabled(!locked);
		myMontantHTField.getContentComponent().setEnabled(!locked);
		myMontantTVAField.getContentComponent().setEnabled(!locked);
		myMontantTTCField.getContentComponent().setEnabled(!locked);
		myOrganField.getContentComponent().setEnabled(!locked);
		myModePaiementField.getContentComponent().setEnabled(!locked);
		myTypeOperationField.setEnabled(!locked);
		myOrigineField.setEnabled(!locked);
		myRibField.setEnabled(!locked);
		myListener.actionSelectFournis().setEnabled(!locked);
		myListener.actionSelectOrgan().setEnabled(!locked);

	}

	public ZNumberField getMyMontantTTCField() {
		return myMontantTTCField;
	}

	public ZNumberField getMyMontantTVAField() {
		return myMontantTVAField;
	}

	/**
	 * @return
	 */
	public ZNumberField getMyMontantHTField() {
		return myMontantHTField;
	}

	public RibInfoPanel getMyRibInfoPanel() {
		return myRibInfoPanel;
	}
}
