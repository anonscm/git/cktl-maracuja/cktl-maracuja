/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class OdpBrouillardListPanel2 extends ZKarukeraPanel implements ZEOTableListener, TableModelListener {
	private IOdpBrouillardListPanelListener myListener;

	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected EODisplayGroup myDisplayGroup;
	protected TableSorter myTableSorter;
	protected Vector myCols;
	private ZPanelBalance myZPanelBalance;

	private boolean isSortable;

	/**
	 * @param editingContext
	 */
	public OdpBrouillardListPanel2(IOdpBrouillardListPanelListener listener, boolean sortable) {
		super();
		isSortable = sortable;
		myListener = listener;
		myZPanelBalance = new ZPanelBalance(new ZPanelBalanceProvider(), true);
		myDisplayGroup = listener.getMyDisplayGroup();
	}

	private void initTableModel() {
		myCols = new Vector(6, 0);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "gestion.gesCode", "Code gestion", 90);
		col3.setAlignment(SwingConstants.CENTER);
		col3.setEditable(true);
		col3.setMyModifier(new GesCodeModifier());
		col3.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoNum", "Imp.", 130);
		col11.setAlignment(SwingConstants.LEFT);
		col11.setEditable(true);
		col11.setMyModifier(new pcoNumModifier());
		col11.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoLibelle", "Libellé", 260);
		col4.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colLibelle = new ZEOTableModelColumn(myDisplayGroup, "odbLibelle", "Libellé brouillard", 263);
		colLibelle.setAlignment(SwingConstants.LEFT);
		colLibelle.setEditable(true);
		colLibelle.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumnWithProvider colDebit = new ZEOTableModelColumnWithProvider("Débit", new colDebitProvider());
		colDebit.setAlignment(SwingConstants.RIGHT);
		//		colDebit.setFormatEdit( ZConst.FORMAT_EDIT_NUMBER);
		colDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colDebit.setEditable(true);
		colDebit.setMyModifier(new colDebitModifier());
		colDebit.setTableCellEditor(new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), ZConst.FORMAT_EDIT_NUMBER));
		colDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumnWithProvider colCredit = new ZEOTableModelColumnWithProvider("Crédit", new colCreditProvider());
		colCredit.setAlignment(SwingConstants.RIGHT);
		//		colCredit.setFormatEdit( ZConst.FORMAT_EDIT_NUMBER);
		colCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colCredit.setEditable(true);
		colCredit.setMyModifier(new colCreditModifier());
		colCredit.setTableCellEditor(new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), ZConst.FORMAT_EDIT_NUMBER));
		colCredit.setColumnClass(BigDecimal.class);

		myCols.add(col3);
		myCols.add(col11);
		myCols.add(col4);
		myCols.add(colLibelle);
		myCols.add(colDebit);
		myCols.add(colCredit);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myTableModel.addTableModelListener(this);
		//permet notammen,t d'ajouter la possibilité de controler l'autorisation de modifier au niveau du row
		myTableModel.setMyDelegate(new ZEOTableModelDelegate());

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		if (isSortable) {
			myEOTable = new ZEOTable(myTableSorter);
			myTableSorter.setTableHeader(myEOTable.getTableHeader());
		}
		else {
			myEOTable = new ZEOTable(myTableModel);
		}

		myEOTable.addListener(this);
		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		//Gestion du clavier
		myEOTable.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				if (!evt.isConsumed())
					doKeyPressed(evt);
			}
		});
	}

	public void initGUI() {
		initTableModel();
		initTable();
		myZPanelBalance.initGUI();

		setLayout(new BorderLayout());
		add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		add(myZPanelBalance, BorderLayout.SOUTH);
	}

	public interface IOdpBrouillardListPanelListener {

		public EODisplayGroup getMyDisplayGroup();

		/**
		 * @param row
		 * @return True si la ligne de brouillard doit être modifiable.
		 */
		public boolean isRowEditable(int row);

		/**
		 * @return
		 */
		public void addRow();

		public void deleteRow();

		/**
		 * @param pconum
		 * @param brouillard
		 * @return
		 */
		public EOPlanComptable findPlanComptable(String pconum);

		/**
		 * @param res
		 */
		public void odpBrouillardAffectePlanComptable(EOPlanComptable res, NSKeyValueCoding brouillard);

		/**
		 * @param gesCode
		 * @return
		 */
		public EOGestion findGestion(String gesCode);

		/**
		 * @param res
		 * @param brouillard
		 */
		public void odpBrouillardAffecteGestion(EOGestion res, NSKeyValueCoding brouillard);

		/**
		 * @param decimal
		 * @param tmp
		 */
		public void odpBrouillardAffecteCredit(BigDecimal decimal, NSKeyValueCoding tmp);

		public void odpBrouillardAffecteDebit(BigDecimal decimal, NSKeyValueCoding tmp);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		//        myDisplayGroup.setObjectArray(myListener.getData());
		//        myTableModel.updateInnerRowCount();        
		//        myTableModel.fireTableDataChanged();
		myEOTable.updateData();
	}

	public void endSaisie() {
		//si la table est en edition, on valide les modifs
		if (myEOTable.isEditing()) {
			if (!myEOTable.stopCellEditing()) {
				return;
			}
		}
	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		return;
	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		return;
	}

	private final class ZPanelBalanceProvider implements ZPanelBalance.IZPanelBalanceProvider {
		/**
		 * @return la valeur des debits.
		 */
		public BigDecimal getDebitValue() {
			NSArray tmp = myDisplayGroup.displayedObjects();
			BigDecimal val = new BigDecimal(0);

			for (int i = 0; i < tmp.count(); i++) {
				NSKeyValueCoding array_element = (NSKeyValueCoding) tmp.objectAtIndex(i);
				if (ZConst.SENS_DEBIT.equals(array_element.valueForKey("odbSens")) && array_element.valueForKey("odbMontant") != null) {
					val = val.add((BigDecimal) array_element.valueForKey("odbMontant"));
				}
			}
			return val;
		}

		public BigDecimal getCreditValue() {
			NSArray tmp = myDisplayGroup.displayedObjects();
			BigDecimal val = new BigDecimal(0);

			for (int i = 0; i < tmp.count(); i++) {
				NSKeyValueCoding array_element = (NSKeyValueCoding) tmp.objectAtIndex(i);
				if (ZConst.SENS_CREDIT.equals(array_element.valueForKey("odbSens")) && array_element.valueForKey("odbMontant") != null) {
					val = val.add((BigDecimal) array_element.valueForKey("odbMontant"));
				}
			}
			return val;
		}
	}

	private final class ZEOTableModelDelegate implements ZEOTableModel.IZEOTableModelDelegate {
		/**
		 * Le row est editable seulement s'il s'agit d'un credit.
		 * 
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate#isCellEditable(int, int)
		 */
		public boolean isCellEditable(int row, int col) {
			return myListener.isRowEditable(row);
			//			return ((EOTitreBrouillard)myDisplayGroup.displayedObjects().objectAtIndex(row)).tibSens().equals(ZConst.SENS_CREDIT);
		}
	}

	/**
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		myZPanelBalance.updateData();
	}

	/**
	 * @param locked
	 */
	public final void lockSaisieOdpBrouillard(boolean locked) {
		myEOTable.setEnabled(!locked);
	}

	public final int ajouterInDg(NSKeyValueCoding obj) {
		int row = myDisplayGroup.displayedObjects().count();
		myDisplayGroup.insertObjectAtIndex(obj, row);
		myDisplayGroup.setSelectedObject(obj);
		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();
		//	    myEOTable.updateData();		
		return row;

	}

	public void focusAtRow(int row) {
		myEOTable.requestFocusInWindow();
		if (myEOTable.editCellAt(row, 0)) {
			myEOTable.changeSelection(row, 0, false, false);
			myEOTable.getCellEditor().cancelCellEditing();
		}
	}

	/**
	 * @param pconum
	 * @param detail
	 */
	private void analysePcoNum(String pconum, NSKeyValueCoding brouillard) {
		EOPlanComptable res = myListener.findPlanComptable(pconum);
		myListener.odpBrouillardAffectePlanComptable(res, brouillard);
		if (res != null) {
			//    		  On arrete l'editon
			if (myEOTable.isEditing()) {
				myEOTable.getCellEditor().cancelCellEditing();
			}
		}
		myTableModel.fireTableDataChanged();
	}

	private void analyseGesCode(String gesCode, NSKeyValueCoding brouillard) {
		EOGestion res = myListener.findGestion(gesCode);
		myListener.odpBrouillardAffecteGestion(res, brouillard);
		if (res != null) {
			//    		  On arrete l'editon
			if (myEOTable.isEditing()) {
				myEOTable.getCellEditor().cancelCellEditing();
			}
		}
		myTableModel.fireTableDataChanged();
	}

	/**
	 * Down key moves down one row, appending a row if neccessary F4 key appends a row Up key moves up one row, pre-appending a row if neccessary F5
	 * key inserts a row before the currently selected row F6 & Delete key delete the current row F7 copies and appends to the bottom the current row
	 * F8 selects the first row
	 **/
	private void doKeyPressed(KeyEvent evt) {
		//	        int i, j;
		switch (evt.getKeyCode()) {
		case KeyEvent.VK_DOWN:
			if (myEOTable.getSelectedRow() != myEOTable.getRowCount() - 1) {
				break;
			}
			myListener.addRow();
			evt.consume();
			break;

		case KeyEvent.VK_INSERT:
			if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK) {
				myListener.addRow();
				evt.consume();
			}
			break;
		case KeyEvent.VK_UP:
			if (myEOTable.getSelectedRow() != 0) {
				break;
			}
			//		                myEOTable.removeEditor();
			myEOTable.setRowSelectionInterval(0, 0);

		case KeyEvent.VK_DELETE:
			if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK) {
				myListener.deleteRow();
				evt.consume();
			}
			break;

		default:
			break;
		}
	}

	private class pcoNumModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			NSKeyValueCoding tmp = (NSKeyValueCoding) myDisplayGroup.displayedObjects().objectAtIndex(row);
			ZLogger.debug("Objet en cours ", tmp);
			analysePcoNum((String) value, tmp);
		}

	}

	private class GesCodeModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			NSKeyValueCoding tmp = (NSKeyValueCoding) myDisplayGroup.displayedObjects().objectAtIndex(row);
			analyseGesCode((String) value, tmp);
		}

	}

	private class colDebitModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			//            EOOrdreDePaiementBrouillard tmp = (EOOrdreDePaiementBrouillard) myDisplayGroup.selectedObject();
			NSKeyValueCoding tmp = (NSKeyValueCoding) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (value instanceof Number) {
				myListener.odpBrouillardAffecteDebit(new BigDecimal(((Number) value).doubleValue()), tmp);
			}
		}

	}

	private class colCreditModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			NSKeyValueCoding tmp = (NSKeyValueCoding) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (value instanceof Number) {
				myListener.odpBrouillardAffecteCredit(new BigDecimal(((Number) value).doubleValue()), tmp);
			}
		}

	}

	private class colDebitProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			NSKeyValueCoding el = (NSKeyValueCoding) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (ZConst.SENS_DEBIT.equals(el.valueForKey("odbSens"))) {
				return el.valueForKey("odbMontant");
			}
			return null;
		}

	}

	private class colCreditProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			NSKeyValueCoding el = (NSKeyValueCoding) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (ZConst.SENS_CREDIT.equals(el.valueForKey("odbSens"))) {
				return el.valueForKey("odbMontant");
			}
			return null;
		}

	}

}
