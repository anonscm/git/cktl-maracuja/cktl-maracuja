/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.odp;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZAction;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.factory.process.FactoryProcessOrdrePaiement;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOVisaAvMission;
import org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class OdpRechercheCtrl extends CommonCtrl implements OdpRecherchePanel.IOdpRecherchePanelListener {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Gestion des ordres de paiement";

	private final String ACTION_ID_RECHERCHE = ZActionCtrl.IDU_PAOP;
	private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_PAOPSA;
	private final String ACTION_ID_SAISIE_ORDONNATEUR = ZActionCtrl.IDU_PAOPO;

	private final ZAction ACTION_SAISIE = myApp.getActionbyId(ZActionCtrl.IDU_PAOPSA);
	private final ZAction ACTION_SAISIE_ORDONNATEUR = myApp.getActionbyId(ZActionCtrl.IDU_PAOPO);
	private final ZAction ACTION_VISER = myApp.getActionbyId(ZActionCtrl.IDU_PAOPVI);
	private final ZAction ACTION_SUPPRESSION = myApp.getActionbyId(ZActionCtrl.IDU_PAOPSU);

	private final int TYPE_UTIL_COMPTABLE = 0;
	private final int TYPE_UTIL_ORDONNATEUR = 1;

	private OdpRecherchePanel myPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionDelete actionDelete = new ActionDelete();
	private final ActionModify actionModify = new ActionModify();
	private final ActionNew actionNew = new ActionNew();
	private final ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionValiderOP actionValiderOP = new ActionValiderOP();
	private final ActionViser actionViser = new ActionViser();
	private final ActionSrch actionSrch = new ActionSrch();

	private ZKarukeraDialog win;
	private HashMap odpFilters;

	private int typeUtilisateur = TYPE_UTIL_ORDONNATEUR;

	private boolean isNouveauEnabled = false;
	private boolean isViserEnabled = false;
	private boolean isAnnulerEnabled = false;
	private boolean hasDroitSaisie = false;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public OdpRechercheCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		odpFilters = new HashMap();

		//Récupérer les droits
		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_RECHERCHE) && !myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE_ORDONNATEUR)
				&& !myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE)) {
			throw new DefaultClientException("Vous n'avez pas les droits nécessaires à la consultation des ordres de paiement.");
		}
		else {
			typeUtilisateur = TYPE_UTIL_COMPTABLE;
		}

		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE_ORDONNATEUR)) {
			typeUtilisateur = TYPE_UTIL_ORDONNATEUR;
			hasDroitSaisie = true;
		}
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE)) {
			typeUtilisateur = TYPE_UTIL_COMPTABLE;
			hasDroitSaisie = true;
		}

		//        
		//        
		//        
		//        
		//        if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE )) {
		//            if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE_ORDONNATEUR )) {
		//                throw new DefaultClientException("Vous n'avez pas les droits nécessaires à la saisie des ordres de paiement.");
		//            }
		//            typeUtilisateur = TYPE_UTIL_ORDONNATEUR;
		//        }
		//        else {
		//            typeUtilisateur = TYPE_UTIL_COMPTABLE;
		//        }

		if (typeUtilisateur == TYPE_UTIL_COMPTABLE) {
			ZLogger.debug("Type comptable");
		}
		else {
			ZLogger.debug("Type Ordonnateur");
		}

		isNouveauEnabled = ((Action) myApp.getActionbyId(ZActionCtrl.IDU_PAOPSA)).isEnabled();
		isViserEnabled = ((Action) myApp.getActionbyId(ZActionCtrl.IDU_PAOPVI)).isEnabled();
		isAnnulerEnabled = ((Action) myApp.getActionbyId(ZActionCtrl.IDU_PAOPSU)).isEnabled();

		initSubObjects();
	}

	public void initSubObjects() {
		myPanel = new OdpRecherchePanel(this);
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#getOdpBrouillards()
	 */
	public NSArray getOdpBrouillards() {
		if (myPanel.getSelectedOdp() == null) {
			return null;
		}
		NSArray brouillards = myPanel.getSelectedOdp().ordreDePaiementBrouillards();
		if (brouillards != null) {
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("odbSens", EOSortOrdering.CompareDescending);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(brouillards, new NSArray(sort1));
		}
		return new NSArray();
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#getOdps()
	 * @return Renvoie une liste d'ordres de paiement en fonction des filtres.
	 */
	public final NSArray getOdps() {
		//Créer la condition à partir des filtres
		try {
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("odpNumero", EOSortOrdering.CompareDescending);
			return EOsFinder.fetchArray(getEditingContext(), EOOrdreDePaiement.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(odpFilters)), new NSArray(sort1), true);
		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRechercheFilterPanel.IOdpRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ and odpEtat<>%@", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice(), EOOrdreDePaiement.etatAnnule
		})));
		if (TYPE_UTIL_ORDONNATEUR == typeUtilisateur) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("odpEtat=%@", new NSArray(new Object[] {
					EOOrdreDePaiement.etatOrdonnateur
			})));
		}

		//Construire les qualifiers à partir des saisies utilisateur

		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("odpNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("odpNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("odpNumeroMin")).intValue()))));
		}
		if (dicoFiltre.get("odpNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("odpNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("odpNumeroMax")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		if (dicoFiltre.get("adrNom") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("fournisseur.adrNom caseInsensitiveLike %@ or fournisseur.fouCode=%@", new NSArray(new Object[] {
					"*" + dicoFiltre.get("adrNom") + "*", dicoFiltre.get("adrNom")
			})));
		}

		///Montant
		NSMutableArray qualsMontant = new NSMutableArray();
		if (dicoFiltre.get("odpHtMin") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("odpHt>=%@", new NSArray((Number) dicoFiltre.get("odpHtMin"))));
		}
		if (dicoFiltre.get("odpHtMax") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("odpHt<=%@", new NSArray((Number) dicoFiltre.get("odpHtMax"))));
		}
		if (qualsMontant.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMontant));
		}

		///Date
		NSMutableArray qualsDate = new NSMutableArray();
		if (dicoFiltre.get("odpDateSaisieMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("odpDateSaisie>=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("odpDateSaisieMin")))));
		}
		if (dicoFiltre.get("odpDateSaisieMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("odpDateSaisie<=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("odpDateSaisieMax")))));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		return quals;
	}

	private final void odpNew() {
		try {
			ZActionCtrl.checkActionWithExeStat(ACTION_SAISIE);
			//        OdpSaisieCtrl opdSaisieCtrl = new OdpSaisieCtrl( getEditingContext(),myPanel.getMyDialog());
			final OdpSaisieCtrl2 opdSaisieCtrl = new OdpSaisieCtrl2(getEditingContext(), myPanel.getMyDialog());
			final EOOrdreDePaiement odp = opdSaisieCtrl.openDialog(myPanel.getMyDialog(), null);
			if (odp != null) {
				odpFilters.clear();
				odpFilters.put("odpNumeroMin", odp.odpNumero());
				odpFilters.put("odpNumeroMax", odp.odpNumero());
				myPanel.updateData();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void odpModifier() {
		try {
			setWaitCursor(true);
			final EOOrdreDePaiement odp = myPanel.getSelectedOdp();
			if (odp == null) {
				throw new DataCheckException("Aucun ordre de paiement sélectionné");
			}

			if (isOpRattacheAvanceMission(odp)) {
				throw new DefaultClientException("Cet ordre de paiement correspond à une avance de mission, vous ne pouvez pas intervenir dessus à partir de cet écran, veuillez passer par l'interface de visa des avances de mission.");
			}

			if (!EOOrdreDePaiement.etatOrdonnateur.equals(odp.odpEtat()) && !EOOrdreDePaiement.etatValide.equals(odp.odpEtat())) {
				throw new DataCheckException("Cet ordre de paiement ne peut plus être modifié.");
			}

			final OdpSaisieCtrl2 opdSaisieCtrl = new OdpSaisieCtrl2(getEditingContext(), myPanel.getMyDialog());
			opdSaisieCtrl.openDialog(myPanel.getMyDialog(), odp);
			setWaitCursor(true);
			myPanel.updateData();

		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}

		//        showinfoDialogFonctionNonImplementee();
	}

	private final void odpAnnuler() {
		//Annulation d'un ordre de paiement
		try {
			ZActionCtrl.checkActionWithExeStat(ACTION_SUPPRESSION);
			EOOrdreDePaiement odp = myPanel.getSelectedOdp();
			if (odp == null) {
				throw new DataCheckException("Aucun ordre de paiement sélectionné");
			}

			//vérifier si l'ordre de paiement peut être annulé (seulement si etta valide)
			if (!EOOrdreDePaiement.etatValide.equals(odp.odpEtat()) && !EOOrdreDePaiement.etatOrdonnateur.equals(odp.odpEtat())) {
				throw new DataCheckException("Vous ne pouvez pas annuler un ordre de paiement qui a l'état " + odp.odpEtat());
			}

			if (TYPE_UTIL_ORDONNATEUR == typeUtilisateur && !EOOrdreDePaiement.etatOrdonnateur.equals(odp.odpEtat())) {
				throw new DefaultClientException("Vous n'avez le droit d'annuler cet ordre de paiement");
			}

			if (isOpRattacheAvanceMission(odp)) {
				throw new DefaultClientException("Cet ordre de paiement correspond à une avance de mission, vous ne pouvez pas intervenir dessus à partir de cet écran, veuillez passer par l'interface de visa des avances de mission.");
			}

			boolean goOn = showConfirmationDialog("Confirmation", "Souhaitez-vous réellement annuler l'ordre de paiement n°" + odp.odpNumero() + " ?", ZMsgPanel.BTLABEL_NO);
			if (goOn) {
				FactoryProcessOrdrePaiement myFactoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				myFactoryProcessOrdrePaiement.annulerOrdreDePaiement(getEditingContext(), odp, myApp.appUserInfo().getUtilisateur());
				getEditingContext().saveChanges();
				onSrch();
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
				onSrch();
			}
			showErrorDialog(e);
		}
	}

	private final void odpImprimer(final NSArray objs) {
		try {
			String filePath = ReportFactoryClient.imprimerOrdrePaiement(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), objs, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}


	private final void odpImprimer() {
		try {
			NSArray selectedObjs = myPanel.getSelectedOdps();
			if (selectedObjs.count() == 0) {
				throw new DefaultClientException("Aucun ordre de paiement à imprimer.");
			}
			odpImprimer(selectedObjs);
			/*
			 * 
			 * NSArray selectedOdpsVises = EOQualifier.filteredArrayWithQualifier(selectedObjs,
			 * EOQualifier.qualifierWithQualifierFormat("odpEtat=%@ or odpEtat=%@", new NSArray(new Object[] { EOOrdreDePaiement.etatVirement,
			 * EOOrdreDePaiement.etatVise }))); ZLogger.debug("selectedOdpsVises", selectedOdpsVises);
			 * 
			 * NSArray selectedOdpsNonVises = EOQualifier.filteredArrayWithQualifier(selectedObjs,
			 * EOQualifier.qualifierWithQualifierFormat("odpEtat=%@ or odpEtat=%@", new NSArray(new Object[] { EOOrdreDePaiement.etatValide,
			 * EOOrdreDePaiement.etatOrdonnateur }))); ZLogger.debug("selectedOdpsNonVises", selectedOdpsNonVises);
			 * 
			 * if (selectedOdpsNonVises.count() == 0 && selectedOdpsVises.count() == 0) { throw new
			 * DefaultClientException("Aucun ordre de paiement (valide ou visé) à imprimer."); }
			 * 
			 * if (selectedOdpsVises.count() > 0) { odpImprimer(selectedOdpsVises); }
			 * 
			 * if (selectedOdpsNonVises.count() > 0) { odpImprimerNonVise(selectedOdpsNonVises); }
			 */
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void odpViser() {
		EOOrdreDePaiement obj = myPanel.getSelectedOdp();
		try {
			if (obj != null) {
				if (TYPE_UTIL_ORDONNATEUR == typeUtilisateur) {
					throw new DefaultClientException("Vous n'avez le droit de viser les ordres de paiements");
				}

				ZActionCtrl.checkActionWithExeStat(ACTION_VISER);

				//verifier si domaine du mode de paiement est different cheque/caisse/virement
				String domaine = obj.modePaiement().modDom();
				if (EOModePaiement.MODDOM_CAISSE.equals(domaine) || EOModePaiement.MODDOM_CHEQUE.equals(domaine) || EOModePaiement.MODDOM_VIREMENT.equals(domaine)) {
					throw new DataCheckException("Le mode de paiement affecté à cet ordre de paiement ne lui permet pas d'être visé. Vous pouvez le payer en passant par l'interface de gestion des paiements.");
				}
				FactoryProcessOrdrePaiement factoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				NSArray lesEcritures = factoryProcessOrdrePaiement.viserOrdreDePaiementSansRib(getEditingContext(), obj, myApp.appUserInfo().getUtilisateur());

				ZLogger.debug(lesEcritures);
				getEditingContext().saveChanges();

				KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

				NSMutableArray numEcritures = new NSMutableArray();
				FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				if ((lesEcritures != null) && (lesEcritures.count() > 0)) {
					for (int i = 0; i < lesEcritures.count(); i++) {
						EOEcriture element = (EOEcriture) lesEcritures.objectAtIndex(i);
						factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
						numEcritures.addObject(element.ecrNumero());
					}
				}
				ZLogger.debug("Ecritures numerotees", numEcritures);

				String msgFin = "L'ordre de paiement " + obj.odpNumero() + " a été visé.\n";
				msgFin += "Les écritures suivantes ont été générées : " + numEcritures.componentsJoinedByString(",");
				showInfoDialog(msgFin);
			}
			else {
				throw new DataCheckException("Aucun ordre de paiement sélectionné.");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void odpValiderOP() {
		EOOrdreDePaiement obj = myPanel.getSelectedOdp();
		try {
			if (obj != null) {
				//              Vérifier les droits
				if (TYPE_UTIL_ORDONNATEUR == typeUtilisateur) {
					throw new DefaultClientException("Vous n'avez le droit de valider les ordres de paiements");
				}

				if (!EOOrdreDePaiement.etatOrdonnateur.equals(obj.odpEtat())) {
					throw new DataCheckException("L'état du bordereau ne lui permet pas d'être validé.");
				}
				if (isOpRattacheAvanceMission(obj)) {
					throw new DefaultClientException("Cet ordre de paiement correspond à une avance de mission, vous ne pouvez pas intervenir dessus à partir de cet écran, veuillez passer par l'interface de visa des avances de mission.");
				}

				FactoryProcessOrdrePaiement factoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				factoryProcessOrdrePaiement.validerOrdreDePaiement(getEditingContext(), obj, myApp.appUserInfo().getUtilisateur());

				getEditingContext().saveChanges();
				myPanel.getOdpListPanel().updateData();
			}
			else {
				throw new DataCheckException("Aucun ordre de paiement sélectionné.");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void fermer() {
		win.onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		//        win = new OdpRechercheDialog(dial);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private final ZKarukeraDialog createModalDialog(Frame dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		//        win = new OdpRechercheDialog(dial);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);

		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Dialog dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		try {
			refreshActions();
			win1.open();
		} finally {
			win1.dispose();
		}
	}

	public final void openDialog(Frame dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		try {
			refreshActions();
			win1.open();
			//            win.show();
		} finally {
			win1.dispose();
		}
	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouvel ordre de paiement");
			setEnabled(ACTION_SAISIE.isEnabled());
		}

		public void actionPerformed(ActionEvent e) {
			odpNew();
		}
	}

	private final class ActionModify extends AbstractAction {
		public ActionModify() {
			super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDIT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier l'ordre de paiement sélectionné");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {

			odpModifier();
		}
	}

	private final class ActionDelete extends AbstractAction {
		public ActionDelete() {
			super("Annuler OP");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDITDELETE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler l'ordre de paiement sélectionné");
			setEnabled(false);
			setEnabled(ACTION_SUPPRESSION.isEnabled());
		}

		public void actionPerformed(ActionEvent e) {
			odpAnnuler();
		}
	}

	private final class ActionImprimer extends AbstractAction {
		public ActionImprimer() {
			super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer l'ordre de paiement sélectionné");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			odpImprimer();
		}
	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SIGNATURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Viser l'ordre de paiement sélectionné");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			odpViser();
		}
	}

	private final class ActionValiderOP extends AbstractAction {
		public ActionValiderOP() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SIGNATURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider l'ordre de paiement pour qu'il puisse être visé ou payé");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			odpValiderOP();
		}
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#actionClose()
	 */
	public Action actionClose() {
		return actionClose;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#actionDelete()
	 */
	public Action actionDelete() {
		return actionDelete;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#actionModify()
	 */
	public Action actionModify() {
		return actionModify;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#actionNew()
	 */
	public Action actionNew() {
		return actionNew;
	}

	public Action getActionImprimer() {
		return actionImprimer;
	}

	public Action getActionViser() {
		return actionViser;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#getFilters()
	 */
	public HashMap getFilters() {
		return odpFilters;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#actionSrch()
	 */
	public Action actionSrch() {
		return actionSrch;
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#actionValider()
	 */
	public Action actionValider() {
		return actionValiderOP;
	}

	private final void refreshActions() {
		EOOrdreDePaiement obj = myPanel.getSelectedOdp();
		if (obj == null) {
			actionImprimer.setEnabled(false);
			actionModify.setEnabled(false);
			actionDelete.setEnabled(false);
			actionViser.setEnabled(false);
			actionValiderOP.setEnabled(false);
		}
		else {
			actionImprimer.setEnabled(true);
			if (myPanel.getSelectedOdps().count() == 1 && hasDroitSaisie) {
				actionModify.setEnabled(true && !(EOOrdreDePaiement.etatVise.equals(obj.odpEtat()) || EOOrdreDePaiement.etatVirement.equals(obj.odpEtat())) && isNouveauEnabled);
				actionDelete.setEnabled(true && !(EOOrdreDePaiement.etatVise.equals(obj.odpEtat()) || EOOrdreDePaiement.etatVirement.equals(obj.odpEtat())) && isAnnulerEnabled);
				actionViser.setEnabled(true && EOOrdreDePaiement.etatValide.equals(obj.odpEtat()) && TYPE_UTIL_COMPTABLE == typeUtilisateur && isViserEnabled);
				actionValiderOP.setEnabled(true && EOOrdreDePaiement.etatOrdonnateur.equals(obj.odpEtat()) && TYPE_UTIL_COMPTABLE == typeUtilisateur && isViserEnabled);
			}
			else {
				actionModify.setEnabled(false);
				actionDelete.setEnabled(false);
				actionViser.setEnabled(false);
				actionValiderOP.setEnabled(false);
			}

		}

		actionNew.setEnabled(ACTION_SAISIE_ORDONNATEUR.isEnabled() || ACTION_SAISIE.isEnabled());

		//        actionModify.setEnabled(false);
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.OdpRecherchePanel.IOdpRecherchePanelListener#odpSelectionChanged()
	 */
	public void odpSelectionChanged() {
		refreshActions();

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	public boolean isOpRattacheAvanceMission(EOOrdreDePaiement odp) {
		if (odp == null) {
			return false;
		}
		final NSArray res = EOsFinder.fetchArray(getEditingContext(), EOVisaAvMission.ENTITY_NAME, EOVisaAvMission.ORDRE_DE_PAIEMENT_KEY + "=%@", new NSArray(new Object[] {
				odp
		}), null, false);
		return (res.count() > 0);
	}

}
