/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client;

import java.awt.event.ActionEvent;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.KeyStroke;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ctrl.SepaSddMandatCtrl;
import org.cocktail.maracuja.client.ZAction.ZActionAdministration;
import org.cocktail.maracuja.client.ZAction.ZActionConsultation;
import org.cocktail.maracuja.client.ZAction.ZActionImpr;
import org.cocktail.maracuja.client.ZAction.ZActionJournal;
import org.cocktail.maracuja.client.ZAction.ZActionNavig;
import org.cocktail.maracuja.client.ZAction.ZActionTresorerie;
import org.cocktail.maracuja.client.ZAction.ZActionVisa;
import org.cocktail.maracuja.client.administration.ctrl.AdminBdfCalCtrl;
import org.cocktail.maracuja.client.administration.ctrl.GestionSaisieCtrl;
import org.cocktail.maracuja.client.administration.ctrl.ModPaiementRechercheCtrl;
import org.cocktail.maracuja.client.administration.ctrl.ModRecouvrementRechercheCtrl;
import org.cocktail.maracuja.client.administration.ctrl.TypeRetenueAdminCtrl;
import org.cocktail.maracuja.client.avmission.ctrl.AvMissionCtrl;
import org.cocktail.maracuja.client.cheques.BdChequeSrchCtrl;
import org.cocktail.maracuja.client.chgtex.ctrl.BalanceEntreeCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread.DefaultZHeartBeatListener;
import org.cocktail.maracuja.client.cptefi.ctrl.BilanAdminCtrl;
import org.cocktail.maracuja.client.cptefi.ctrl.CompteFiCtrl;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.emargements.ctrl.EmargementSrchCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.maracuja.client.impression.BalanceCtrl;
import org.cocktail.maracuja.client.impression.CoherenceSoldeCtrl;
import org.cocktail.maracuja.client.impression.DevSoldeCtrl;
import org.cocktail.maracuja.client.impression.FeuilletsBudCtrl;
import org.cocktail.maracuja.client.impression.GrandLivreCtrl;
import org.cocktail.maracuja.client.impression.JournalConventionRACtrl;
import org.cocktail.maracuja.client.impression.JournalDivCtrl;
import org.cocktail.maracuja.client.impression.JournalGeneralCtrl;
import org.cocktail.maracuja.client.impression.JournalRejetCtrl;
import org.cocktail.maracuja.client.impression.JournalTvaCtrl;
import org.cocktail.maracuja.client.impression.PlanComptableImprCtrl;
import org.cocktail.maracuja.client.impression.ResteRecouvrerCtrl;
import org.cocktail.maracuja.client.impression.SitFourDepenseCtrl;
import org.cocktail.maracuja.client.impression.SitfFourRecetteCtrl;
import org.cocktail.maracuja.client.impression.ToutEmargementCtrl;
import org.cocktail.maracuja.client.infocentre.ctrl.TransfertDgcpCtrl3;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.odp.OdpRechercheCtrl;
import org.cocktail.maracuja.client.paiement.PaiementChgtCtrl;
import org.cocktail.maracuja.client.paiement.PaiementSrchCtrl;
import org.cocktail.maracuja.client.papaye.ctrl.PapayeSrchCtrl;
import org.cocktail.maracuja.client.payepaf.ctrl.PayePafSrchCtrl;
import org.cocktail.maracuja.client.pco.PcoRechercheCtrl;
import org.cocktail.maracuja.client.recherches.DepRecCtrl;
import org.cocktail.maracuja.client.recouvrement.RecouvrementSrchCtrl;
import org.cocktail.maracuja.client.reimp.mandat.ReimpMandatRechercheCtrl;
import org.cocktail.maracuja.client.reimp.titre.ReimpTitreRechercheCtrl;
import org.cocktail.maracuja.client.relances.ctrl.RelanceSrchCtrl;
import org.cocktail.maracuja.client.relances.ctrl.TypeRelanceSrchCtrl;
import org.cocktail.maracuja.client.scol.ctrl.ScolBordereauCtrl;
import org.cocktail.maracuja.client.visa.VisaMandatDialog;
import org.cocktail.maracuja.client.visa.VisaTitreDialog;
import org.cocktail.maracuja.client.visa.taxe.ctrl.BordereauTaxeSrchCtrl;
import org.cocktail.maracuja.client.visabrouillard.ctrl.VisaBrouillardCtrl;
import org.cocktail.maracuja.client.visapi.ctrl.VisaPiCtrl;
import org.cocktail.maracuja.client.visapi.ctrl.VisaPiCtrl2;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.InitException;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZActionCtrl {
	private final ApplicationClient myApp;
	private NSMutableDictionary defaultActionDico;
	private NSMutableDictionary userActionDico;

	//Liste des identifiants actions par defaut (interne appli)
	public static final String ID_QUITTER = "LC0001";
	public static final String ID_AIDE = "LC0002";
	public static final String ID_APROPOS = "LC0003";
	public static final String ID_CHANGEREXERCICE = "LC0004";
	public static final String ID_OUTILS = "LC0005";
	public static final String ID_AIDE_CHANGELOG = "LC0008";
	public static final String ID_IMPR_BTMNA = "LC0009";
	public static final String ID_IMPR_UTILISATEUR = "LC0010";
	public static final String ID_IMPR_COMPTABILITEGESTION = "LC0011";
	public static final String ID_IMPR_BTTNA = "LC0013";
	public static final String ID_ICONIFY = "LC0015";
	public static final String ID_AIDE_LOGVIEWER = "LC0021";
	public static final String ID_OUTILS_REFRESHDATA = "LC0024";
	//public static final String ID_AIDE_SITEWEB = "LC0026";
	public static final String ID_IMPR_JOURNAL_REJET = "LC0027";
	public static final String ID_IMPR_JOURNAL_CONVRA = "LC0028";
	public static final String ID_IMPR_LISTE_OP_EXTOURNEES = "LC0029";

	public static final String IDU_SRCH01 = "SRCH01";
	/** Recherche depenses/recettes */
	public static final String IDU_SRCH02 = "SRCH02";
	/** Transfert DGCP */
	public static final String IDU_EPN = "EPN";
	/** Journal général */
	public static final String IDU_IMPR001 = "IMPR001";
	/** Grand livre */
	public static final String IDU_IMPR002 = "IMPR002";
	/** Balances */
	public static final String IDU_IMPR003 = "IMPR003";
	/** Etat dev soilde */
	public static final String IDU_IMPR004 = "IMPR004";
	/** Tout emargement */
	public static final String IDU_IMPR005 = "IMPR005";
	/** Plan comptable */
	public static final String IDU_IMPR007 = "IMPR007";
	/** Modes de paiement */
	public static final String IDU_IMPR008 = "IMPR008";
	/** Restes à recouvrer */
	public static final String IDU_IMPR009 = "IMPR009";
	/** Feuillets budgetaires */
	public static final String IDU_IMPR010 = "IMPR010";
	/** Journal TVA */
	public static final String IDU_IMPR011 = "IMPR011";
	/** Journal Divisionnaire */
	public static final String IDU_IMPR012 = "IMPR012";

	public static final String IDU_IMPR013 = "IMPR013";
	public static final String IDU_IMPR014 = "IMPR014";
	public static final String IDU_IMPR015 = "IMPR015";
	/** admin/comptabilité */
	public static final String IDU_ADCO = "ADCO"; //
	/** admin/Codes gestions */
	public static final String IDU_ADGE = "ADGE"; //
	/** admin/Types de relances */
	public static final String IDU_ADREL = "ADREL"; //
	/** admin/Types de retenues */
	public static final String IDU_ADRET = "ADRET"; //
	/** admin/Parametres */
	public static final String IDU_ADPA = "ADPA"; //
	/** Param/Gestion des types de credit */
	public static final String IDU_ADTC = "ADTC"; //
	//	/** admin/utilisateurs */
	//	public static final String IDU_ADUT="ADUT"; //
	/** Annulation ecriture */
	public static final String IDU_COAN = "COAN"; //compta/
	/** Journal / ecriture */
	public static final String IDU_COCE = "COCE"; //
	/** Journal / EMARGEMENT */
	public static final String IDU_COEM = "COEM"; //
	/** Saisie ecritures */
	public static final String IDU_JOECSA = "JOECSA"; //
	/** Saisie emargement */
	public static final String IDU_JOEMSA = "JOEMSA"; //
	/** Annulation EMARGEMENT */
	public static final String IDU_COEMAN = "COEMAN"; //compta/
	public static final String IDU_COMO = "COMO"; //compta/MODIF

	/** Saisie d'une suspension de DGP */
	public static final String IDU_IMSUSP = "IMSUSP"; //
	/** Creation des IMs */
	public static final String IDU_IMSAIS = "IMSAIS"; //

	/** GENERER UN PAIEMENT */
	public static final String IDU_PAGE = "PAGE"; //
	/** Paiements/Modifiers modes de paiements et ribs */
	public static final String IDU_PAMO = "PAMO"; //
	/** PARAMETRAGES/Modes de paiement */
	public static final String IDU_PAMP = "PAMP";
	/** PARAMETRAGES/Modes de recouvrement */
	public static final String IDU_PAMR = "PAMR";
	/** GESTION ORDRES PAIEMENT */
	public static final String IDU_PAOP = "PAOP";
	/** Ordres de paiement ordonnateurs */
	public static final String IDU_PAOPO = "PAOPO";
	/** GESTION PLAN COMPTABLE */
	public static final String IDU_PAPC = "PAPC"; //
	/** AVANCES ET RETENUES */
	public static final String IDU_PARE = "PARE"; //
	/** virement */
	public static final String IDU_PAVI = "PAVI"; //
	/** Bordereaux Papaye */
	public static final String IDU_PAYE = "PAYE"; //
	/** Bordereaux Paye PAF */
	public static final String IDU_PAYEPAF = "PAYEPAF"; //
	/** Visa/Depenses */
	public static final String IDU_VIDE = "VIDE"; //
	/** Visa/pRESTATIONS INTERNES */
	public static final String IDU_VIPR = "VIPR"; //
	/** Visa/Recettes */
	public static final String IDU_VIRE = "VIRE"; //
	/** Réimputation/Titres */
	public static final String IDU_RETI = "RETI";
	/** Réimputation/Mandats */
	public static final String IDU_REMA = "REMA";
	/** Saisie des bordereaux de cheques */
	public static final String IDU_CHESA = "CHESA";
	/** Visa des bordereaux de chèques */
	public static final String IDU_CHEVI = "CHEVI";
	/** gestion bordereaux scol */
	public static final String IDU_SCOL = "SCOL";
	/** Gestion des relances */
	public static final String IDU_RELANCE = "RELANCE";
	/** Visa/Taxe apprentissage */
	public static final String IDU_VITA = "VITA";
	/** Outils/Balance d'entrée */
	public static final String IDU_OUTBE = "OUTBE";
	/** Outils/Compte financier */
	public static final String IDU_OUTCFI = "OUTCFI";
	/** Paiements/saisie ops */
	public static final String IDU_PAOPSA = "PAOPSA";
	/** Paiements/visa ops */
	public static final String IDU_PAOPVI = "PAOPVI";
	/** Paiements/suppression ops */
	public static final String IDU_PAOPSU = "PAOPSU";
	/** Prélèvements */
	public static final String IDU_SEPASDD = "SEPASDD";
	/** Validation des mandats sepa sdd */
	public static final String IDU_SSDDVAL = "SSDDVAL";
	/** Prélèvements */
	public static final String IDU_PRELEV = "PRELEV";
	/** Import calendrier BDF */
	public static final String IDU_BDFCAL = "BDFCAL";
	/** Visa avances missions */
	public static final String IDU_VIAVMIS = "VIAVMIS";
	/** Administration bilan */
	public static final String IDU_ADBILAN = "ADBILAN";
	/** Visa des brouillards en attente */
	public static final String IDU_VISBRO = "VISBRO";

	/**
	 *
	 */
	public ZActionCtrl(final ApplicationClient app) {
		super();
		myApp = app;
		initAllActionDico();
	}

	public final void showAide() {
		myApp.showinfoDialogFonctionNonImplementee();
	}

	public final void quitter() {
		myApp.quitter();
	}

	public final void changerExercice() {
		myApp.changerExercice();
	}

	public final void rechercher() {
		try {
			final RechercheDialog myDial = new RechercheDialog(myApp.getMainFrame(), "Recherche", true);
			myDial.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			myDial.open();

		} catch (Exception e) {
			myApp.showErrorDialog(e);
		}
	}

	public final void gestionComptabilite() {
		// gestionComptabilite
		myApp.showinfoDialogFonctionNonImplementee();
	}

	/**
	 * Met à jour les actions utilisateurs (recupérées de la bd) en fonctions d'une liste d'identifiants d'actions "autorisées". Seules les actions
	 * dont les identifiants se trouvent dans la liste sont enabled/disabled.
	 */
	public final void setUserActionsWithIdListEnabled(final NSArray enabledList, final boolean b) {
		for (int i = 0; i < enabledList.count(); i++) {
			setUserActionEnabledById((String) enabledList.objectAtIndex(i), b);
		}
	}

	public final void setUserActionEnabledById(final String id, final boolean b) {
		if (getUserActionbyId(id) != null) {
			getUserActionbyId(id).setEnabled(b);
		}
	}

	/**
	 * Rend toutes les actions User enabled ou disabled.
	 */
	public final void setAllUserActionsEnabled(final boolean b) {
		setUserActionsWithIdListEnabled(userActionDico.allKeys(), b);
	}

	public final void setDefaultActionEnabledById(final String id, final boolean b) {
		if (getDefaultActionbyId(id) != null) {
			getDefaultActionbyId(id).setEnabled(b);
		}
	}

	public final void setDefaultActionsWithIdListEnabled(final NSArray enabledList, final boolean b) {
		for (int i = 0; i < enabledList.count(); i++) {
			setDefaultActionEnabledById((String) enabledList.objectAtIndex(i), b);
		}
	}

	/**
	 * Rend toutes les actions User enabled ou disabled.
	 */
	public final void setAllDefaultActionsEnabled(final boolean b) {
		setDefaultActionsWithIdListEnabled(defaultActionDico.allKeys(), b);
	}

	/**
	 * Initialise les actions par défaut accessibles à tous les utilisateurs (quitter, aide, etc.)
	 */
	private final void initDefaultActionDico() {
		defaultActionDico = new NSMutableDictionary();
		//		ZAction tmpAction;
		final ArrayList list = new ArrayList();
		list.add(new ActionQuitter());
		list.add(new ActionAide());
		list.add(new ActionAbout());
		list.add(new ActionChangerExercice());
		list.add(new ActionIMPR_BTMNA());
		list.add(new ActionIMPR_BTTNA());
		list.add(new ActionIMPR_COMPTABILITEGESTION());
		list.add(new ActionAIDE_CHANGELOG());
		list.add(new ActionICONIFY());
		list.add(new ActionAIDE_LOGVIEWER());
		list.add(new ActionOUTILS_REFRESHDATA());
		//	list.add(new ActionAIDE_SITEWEB());
		list.add(new ActionIMPR_JOURNAL_REJET());
		list.add(new Action_IMPR_LISTE_OP_EXTOURNEES());
		list.add(new ActionIMPR_JOURNAL_CONVRA());

		for (final Iterator iter = list.iterator(); iter.hasNext();) {
			final ZAction element = (ZAction) iter.next();
			defaultActionDico.takeValueForKey(element, element.getActionId());
		}

	}

	private final void initUserActionDico() throws Exception {
		try {
			userActionDico = new NSMutableDictionary();
			final NSArray res = EOsFinder.getAllFonctions(myApp.editingContext());
			String erreurs = "";
			for (int i = 0; i < res.count(); i++) {
				final EOFonction tmpfonc = (EOFonction) res.objectAtIndex(i);
				try {
					final Class tmpClass = Class.forName(this.getClass().getName() + "$Action" + tmpfonc.fonIdInterne());
					//Important : pour les classes internes, le premier parametre du constructeur est celui de la classe conteneur
					final Class[] consParamClasses = new Class[] {
							this.getClass(), EOFonction.class
					};
					final Constructor constructor = tmpClass.getDeclaredConstructor(consParamClasses);
					final Object[] params = {
							this, tmpfonc
					};
					final ZAction tmpaction = (ZAction) constructor.newInstance(params);
					userActionDico.takeValueForKey(tmpaction, tmpfonc.fonIdInterne());
				} catch (ClassNotFoundException e) {
					erreurs = erreurs + tmpfonc.fonIdInterne() + ",";
				}
			}
			if (erreurs.length() > 0) {
				System.out.println("Des fonctions sont définies dans la basees de données mais " + "non prises en charge par l'application : " + erreurs);
			}
		} catch (ZFinderException e1) {
			System.out.println(e1.getMessage());
		} catch (Exception e) {
			throw e;
		}
	}

	private final void initAllActionDico() {
		//		allActionDico = new NSMutableDictionary();
		initDefaultActionDico();
		try {
			initUserActionDico();
		} catch (Exception e) {
			e.printStackTrace();
			myApp.showErrorDialog(e);
		}
	}

	/**
	 * Vérifie si l'action est autorisée suivant l'état de l'exercice. Renvoie une exception sinon.
	 * 
	 * @param action
	 * @throws Exception
	 */
	public final static void checkActionWithExeStat(final ZAction action) throws Exception {

		final ApplicationClient app = (ApplicationClient) ApplicationClient.sharedApplication();

		final EOExercice exercice = app.appUserInfo().getCurrentExercice();
		boolean res = false;
		if (exercice.estPreparation()) {
			res = action.canExecuteOnExercicePreparation();
			if (!res) {
				throw new Exception("Cette action n'est pas permise sur l'exercice " + app.appUserInfo().getCurrentExercice().exeExercice().intValue() + " car il est en modePREPARATION");
			}
		}
		if (exercice.estOuvert()) {
			res = action.canExecuteOnExerciceOuvert();
			if (!res) {
				throw new Exception("Cette action n'est pas permise sur l'exercice " + app.appUserInfo().getCurrentExercice().exeExercice().intValue() + " car il est en mode OUVERT");
			}
		}
		else if (exercice.estRestreint()) {
			res = action.canExecuteOnExerciceRestreint();
			if (!res) {
				throw new Exception("Cette action n'est pas permise sur l'exercice " + app.appUserInfo().getCurrentExercice().exeExercice().intValue() + " car il est en mode RESTREINT");
			}
		}
		else if (exercice.estClos()) {
			res = action.canExecuteOnExerciceClos();
			if (!res) {
				throw new Exception("Cette action n'est pas permise sur l'exercice " + app.appUserInfo().getCurrentExercice().exeExercice().intValue() + " car il est en mode CLOS");
			}
		}

	}

	/**
	 * Vérifie si l'action est autorisée suivant l'état de l'exercice. Renvoie true si oui.
	 * 
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public final boolean isActionAllowedWithExeStat(final ZAction action) throws Exception {
		final EOExercice exercice = myApp.appUserInfo().getCurrentExercice();
		boolean res = false;
		if (exercice.estPreparation()) {
			res = action.canExecuteOnExercicePreparation();
		}
		if (exercice.estOuvert()) {
			res = action.canExecuteOnExerciceOuvert();
		}
		else if (exercice.estRestreint()) {
			res = action.canExecuteOnExerciceRestreint();
		}
		else if (exercice.estClos()) {
			res = action.canExecuteOnExerciceClos();
		}
		return res;
	}

	public final ZAction getActionbyId(final String id) {
		final ZAction tmp = getDefaultActionbyId(id);
		if (tmp != null) {
			return tmp;
		}
		return getUserActionbyId(id);
	}

	public final ZAction getDefaultActionbyId(final String id) {
		if (defaultActionDico.allKeys().indexOfObject(id) != NSArray.NotFound) {
			return (ZAction) defaultActionDico.valueForKey(id);
		}
		return null;
	}

	public final ZAction getUserActionbyId(final String id) {
		if (userActionDico.allKeys().indexOfObject(id) != NSArray.NotFound) {
			return (ZAction) userActionDico.valueForKey(id);
		}
		System.out.println("UserAction non trouvee:" + id);
		return null;
	}

	/**
	 * @return
	 */
	public final NSMutableDictionary getDefaultActionDico() {
		return defaultActionDico;
	}

	/**
	 * @return
	 */
	public final NSMutableDictionary getUserActionDico() {
		return userActionDico;
	}

	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////
	/**
	 * Action de quitter l'application.
	 */
	public final class ActionQuitter extends ZActionNavig {
		public ActionQuitter() {
			super(ID_QUITTER, "Quitter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXIT_16));
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			quitter();
		}
	}

	/**
	 * Action d'afficher l'aide.
	 */
	public final class ActionAide extends ZActionNavig {
		public ActionAide() {
			super(ID_AIDE, "Aide");
		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			showAide();
		}
	}

	/**
	 * Action pour changer d'exercice.
	 */
	public final class ActionChangerExercice extends ZActionNavig {
		public ActionChangerExercice() {
			super(ID_CHANGEREXERCICE, "Changer d'exercice...");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Changer l'exercice en cours");

		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			changerExercice();
		}
	}

	/**
	 * Action "A propos"
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public class ActionAbout extends ZActionNavig {
		public ActionAbout() {
			super(ID_APROPOS, "A propos...");
		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			myApp.showAboutDialog();
		}
	}

	/** Outil / Rechercher */
	public class ActionSRCH01 extends ZActionConsultation {
		public ActionSRCH01(EOFonction fonc) {
			super(fonc);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));

		}

		public final void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			rechercher();
		}
	}

	/** Outil / Suivi depense recette */
	public class ActionSRCH02 extends ZActionConsultation {
		public ActionSRCH02(EOFonction fonc) {
			super(fonc);
			putValue(Action.SHORT_DESCRIPTION, "Rechercher des mandats/titres et suivre les paiements");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				DepRecCtrl dial = new DepRecCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Outils / Transfert DGCP */
	public class ActionEPN extends ZAction {
		public ActionEPN(EOFonction fonc) {
			super(fonc);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXPORTDGCP_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final TransfertDgcpCtrl3 dial = new TransfertDgcpCtrl3(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Administration/Comptabilite */
	public class ActionADCO extends ZActionAdministration {
		public ActionADCO(EOFonction fonc) {
			super(fonc);

		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showinfoDialogFonctionNonImplementee();
		}
	}

	/** Administration / Codes gestion */
	public final class ActionADGE extends ZActionAdministration {
		public ActionADGE(final EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final GestionSaisieCtrl dial = new GestionSaisieCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Administration / Bilan */
	public final class ActionADBILAN extends ZActionAdministration {
		public ActionADBILAN(final EOFonction fonc) {
			super(fonc);
			setCanExecuteOnExercicePreparation(true);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(true);

		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				BilanAdminCtrl bp = new BilanAdminCtrl(myApp.getEditingContext());
				bp.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

	}

	/** Administration / Relances */
	public final class ActionADREL extends ZActionAdministration {
		public ActionADREL(final EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final TypeRelanceSrchCtrl typeRelanceSrchCtrl = new TypeRelanceSrchCtrl(myApp.editingContext());
				typeRelanceSrchCtrl.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Administration / Retenues */
	public final class ActionADRET extends ZActionAdministration {
		public ActionADRET(final EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(final ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final TypeRetenueAdminCtrl typeRetenueAdminCtrl = new TypeRetenueAdminCtrl(myApp.getEditingContext());
				typeRetenueAdminCtrl.openDialog(myApp.getMainWindow(), true);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Gestion des parametres.
	 */
	public final class ActionADPA extends ZActionAdministration {
		public ActionADPA(EOFonction fonc) {
			super(fonc);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showinfoDialogFonctionNonImplementee();
		}
	}

	/**
	 * Gestion des types de crédit.
	 */
	public class ActionADTC extends ZActionAdministration {
		public ActionADTC(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showinfoDialogFonctionNonImplementee();
		}
	}

	//	/**
	//	 * Action Gestion des utilisateurs.
	//	 */
	//	public final class ActionADUT extends ZActionAdministration {
	//		public ActionADUT(EOFonction fonc) {
	//			super(fonc);
	//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			super.actionPerformed(e);
	//            try {
	//                checkActionWithExeStat(this);
	//                final AdminUtilisateurDialog myAdminUtilisateurDialog = new AdminUtilisateurDialog(myApp.toSuperviseur);
	//                myAdminUtilisateurDialog.open();
	//            }
	//            catch (Exception e1) {
	//                myApp.showErrorDialog(e1);
	//            }            
	//
	//		}
	//        
	//        protected void initCanExecute() {
	//            super.initCanExecute();
	//            setCanExecuteOnExercicePreparation(true);
	//            setCanExecuteOnExerciceOuvert(true);
	//            setCanExecuteOnExerciceRestreint(true);
	//            setCanExecuteOnExerciceClos(true);             
	//        }
	//                
	//	}
	//    
	public final class ActionBDFCAL extends ZActionAdministration {
		public ActionBDFCAL(EOFonction fonc) {
			super(fonc);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final AdminBdfCalCtrl win = new AdminBdfCalCtrl(myApp.editingContext());
				win.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(true);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(true);
		}

	}

	/** Annulation ecriture */
	public final class ActionCOAN extends ZActionJournal {
		public ActionCOAN(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showinfoDialogFonctionNonImplementee();
		}
	}

	/**
	 * Saisie d'écritures.
	 */
	public final class ActionCOCE extends ZActionJournal {
		public ActionCOCE(EOFonction fonc) {
			super(fonc);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
				win.openDialog(myApp.getMainWindow(), null);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	public final class ActionJOECSA extends ZActionJournal {
		public ActionJOECSA(EOFonction fonc) {
			super(fonc);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);

		}
	}

	public final class ActionJOEMSA extends ZActionJournal {
		public ActionJOEMSA(EOFonction fonc) {
			super(fonc);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EMARGMENT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);

		}
	}

	/**
	 * Saisie emargements
	 */
	public final class ActionCOEM extends ZActionJournal {
		public ActionCOEM(EOFonction fonc) {
			super(fonc);
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control M"));
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EMARGMENT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				if (myApp.appUserInfo().getAuthorizedComptabilitesForActionID(myApp.editingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COEM).count() == 0) {
					throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour saisir un émargement. Demandez à votre administrateur de vous affecter les codes de gestions pour la saisie des émargements.");
				}
				final EmargementSrchCtrl win = new EmargementSrchCtrl(myApp.editingContext());
				try {
					win.openDialog(myApp.getMainWindow(), null);
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}

			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Annulation emargements */
	public class ActionCOEMAN extends ZActionJournal {
		public ActionCOEMAN(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
		}
	}

	/**
	 * Modification ecritures
	 */
	public class ActionCOMO extends ZActionJournal {
		public ActionCOMO(EOFonction fonc) {
			super(fonc);
			//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showinfoDialogFonctionNonImplementee();
		}
	}

	//
	//    /** pARAMETRAGES/EXERCICES */
	//	public class ActionPAEX extends ZActionAdministration {
	//		public ActionPAEX(EOFonction fonc) {
	//			super(fonc);
	////			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			super.actionPerformed(e);
	//            try {
	//                final AdminExercicesCtrl dial = new AdminExercicesCtrl(myApp.editingContext());
	//                dial.openDialog( myApp.getMainFrame());
	//                
	//                if (dial.hasChanged()) {
	//                    myApp.changeCurrentExercice(myApp.appUserInfo().getCurrentExercice());
	//                }
	//                
	//            }
	//            catch (Exception e1) {
	//                myApp.showErrorDialog(e1);
	//            }			
	//		}
	//	}

	/**
	 * Gestion des paiements
	 */
	public final class ActionPAGE extends ZActionTresorerie {
		public ActionPAGE(EOFonction fonc) {
			super(fonc);
			//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final PaiementSrchCtrl dial = new PaiementSrchCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Gestion des paiements
	 */
	public final class ActionVIAVMIS extends ZActionTresorerie {
		public ActionVIAVMIS(EOFonction fonc) {
			super(fonc);
			//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final AvMissionCtrl dial = new AvMissionCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame(), true);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Modifier modes de paiement et ribs
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class ActionPAMO extends ZAction {
		public ActionPAMO(EOFonction fonc) {
			super(fonc);
			//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final PaiementChgtCtrl dial = new PaiementChgtCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(false);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(false);
		}
	}

	/**
	 * Action correspondant à la gestion des modes de paiements.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class ActionPAMP extends ZActionAdministration {
		public ActionPAMP(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final ModPaiementRechercheCtrl dial = new ModPaiementRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(true);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(false);
		}
	}

	/**
	 * Action correspondant à la gestion des modes de recouvrement.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class ActionPAMR extends ZActionAdministration {
		public ActionPAMR(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final ModRecouvrementRechercheCtrl dial = new ModRecouvrementRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(true);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(false);
		}
	}

	/** gestion ordres de paiements */
	public final class ActionPAOP extends ZAction {
		public ActionPAOP(EOFonction fonc) {
			super(fonc);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ODP_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final OdpRechercheCtrl dial = new OdpRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Ordres de paiements ordonnateurs */
	public final class ActionPAOPO extends ZAction {
		public ActionPAOPO(EOFonction fonc) {
			super(fonc);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ODP_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final OdpRechercheCtrl dial = new OdpRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Gestion du plan comptable
	 */
	public final class ActionPAPC extends ZAction {
		public ActionPAPC(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				((ZFrame) myApp.getMainFrame()).setWaitCursor(true);
				final PcoRechercheCtrl dial = new PcoRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				((ZFrame) myApp.getMainFrame()).setWaitCursor(false);
				myApp.showErrorDialog(e1);
			} finally {
				((ZFrame) myApp.getMainFrame()).setWaitCursor(false);
			}
		}
	}

	/** Avances et retenues */
	public final class ActionPARE extends ZAction {
		public ActionPARE(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showInfoDialog("Vous pouvez créer des retenues sur des mandats lors de la phase de création d'un paiement.");
		}
	}

	/** Virements */
	public class ActionPAVI extends ZAction {
		public ActionPAVI(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showinfoDialogFonctionNonImplementee();
		}
	}

	/** Bordereaux Papaye */
	public final class ActionPAYE extends ZActionVisa {
		public ActionPAYE(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final PapayeSrchCtrl dial = new PapayeSrchCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Bordereaux Paye PAF */
	public final class ActionPAYEPAF extends ZActionVisa {
		public ActionPAYEPAF(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final PayePafSrchCtrl dial = new PayePafSrchCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Prélèvements */
	public final class ActionSEPASDD extends ZActionConsultation {
		public ActionSEPASDD(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				//checkActionWithExeStat(this);
				SepaSddMandatCtrl dial = new SepaSddMandatCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame(), true, false);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Prélèvements */
	public final class ActionPRELEV extends ZActionConsultation {
		public ActionPRELEV(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final RecouvrementSrchCtrl dial = new RecouvrementSrchCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Visa depense.
	 */
	public final class ActionVIDE extends ZActionVisa {
		public ActionVIDE(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);

			//maintenir la connection avec le serveur
			final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
				public void onBeat() {
					ServerProxy.clientSideRequestMsgToServer(myApp.getEditingContext(), "Visa depenses en cours...", myApp.getIpAdress());
				}

				public void mainTaskStart() {
				}

				public boolean isMainTaskFinished() {
					return false;
				};
			};
			final ZWaitingThread waitingThread = new ZWaitingThread(20000, 20000, defaultZHeartBeatListener);
			waitingThread.start();

			try {
				checkActionWithExeStat(this);
				myApp.getEditingContext().revert();
				final VisaMandatDialog myDial = new VisaMandatDialog(myApp.getMainFrame(), "Visa Dépenses", true);
				myDial.open(); //modal

				myDial.dispose();
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			} finally {
				waitingThread.interrupt();
			}
		}
	}

	/**
	 * visa recettes.
	 */
	public final class ActionVIRE extends ZActionVisa {
		public ActionVIRE(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);

			//maintenir la connection avec le serveur
			final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
				public void onBeat() {
					ServerProxy.clientSideRequestMsgToServer(myApp.getEditingContext(), "Visa recettes en cours...", myApp.getIpAdress());
				}

				public void mainTaskStart() {
				}

				public boolean isMainTaskFinished() {
					return false;
				};
			};
			final ZWaitingThread waitingThread = new ZWaitingThread(20000, 20000, defaultZHeartBeatListener);
			waitingThread.start();

			try {
				checkActionWithExeStat(this);
				myApp.getEditingContext().revert();

				final VisaTitreDialog myDial = new VisaTitreDialog(myApp.getMainFrame(), "Visa Recettes", true);
				myDial.open(); //modal

				myDial.dispose();
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			} finally {
				waitingThread.interrupt();
			}

		}
	}

	/** Visa prestations interne */
	public final class ActionVIPR extends ZActionVisa {
		public ActionVIPR(EOFonction fonc) {
			super(fonc);
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final EOExercice exercice = myApp.appUserInfo().getCurrentExercice();
				if (exercice.exeExercice().intValue() < ZConst.EXE_EXERCICE_2007) {
					final VisaPiCtrl dial = new VisaPiCtrl(myApp.editingContext());
					dial.openDialog(myApp.getMainFrame());
				}
				else {
					final VisaPiCtrl2 dial = new VisaPiCtrl2(myApp.editingContext());
					dial.openDialog(myApp.getMainFrame());
				}

			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Visa taxe apprentissage.
	 */
	public final class ActionVITA extends ZActionVisa {
		public ActionVITA(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final BordereauTaxeSrchCtrl dial = new BordereauTaxeSrchCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	public final class ActionVISBRO extends ZActionVisa {
		public ActionVISBRO(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final VisaBrouillardCtrl dial = new VisaBrouillardCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame(), true);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/**
	 * Outils/Balance d'entrée
	 */
	public final class ActionOUTBE extends ZAction {
		public ActionOUTBE(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final BalanceEntreeCtrl balanceEntreeCtrl = new BalanceEntreeCtrl(myApp.editingContext());
				balanceEntreeCtrl.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	public final class ActionOUTCFI extends ZAction {
		public ActionOUTCFI(EOFonction fonc) {
			super(fonc);
		}

		protected void initCanExecute() {
			setCanExecuteOnExercicePreparation(false);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(true);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final CompteFiCtrl compteFiCtrl = new CompteFiCtrl(myApp.editingContext());
				compteFiCtrl.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/**
	 * Reimputations / Titres
	 */
	public final class ActionRETI extends ZActionVisa {
		public ActionRETI(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final ReimpTitreRechercheCtrl dial = new ReimpTitreRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Reimputations / Mandats
	 */
	public final class ActionREMA extends ZActionVisa {
		public ActionREMA(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final ReimpMandatRechercheCtrl dial = new ReimpMandatRechercheCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Outils / Saisie de bordereaux de chèques
	 */
	public final class ActionCHESA extends ZAction {
		public ActionCHESA(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			//maintenir la connection avec le serveur
			final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
				public void onBeat() {
					ServerProxy.clientSideRequestMsgToServer(myApp.getEditingContext(), "Saisie cheques en cours...", myApp.getIpAdress());
				}

				public void mainTaskStart() {
				}

				public boolean isMainTaskFinished() {
					return false;
				};
			};
			final ZWaitingThread waitingThread = new ZWaitingThread(20000, 20000, defaultZHeartBeatListener);
			waitingThread.start();

			try {
				final BdChequeSrchCtrl dial = new BdChequeSrchCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame(), null);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			} finally {
				waitingThread.interrupt();
			}
		}
	}

	/**
	 * Outils/Relances
	 */
	public final class ActionRELANCE extends ZAction {
		public ActionRELANCE(EOFonction fonc) {
			super(fonc);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELANCE_16));
			setCanExecuteOnExerciceRestreint(true);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final RelanceSrchCtrl relanceSrchCtrl = new RelanceSrchCtrl(myApp.editingContext());
				relanceSrchCtrl.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/**
	 * Visa bordereaux scol
	 */
	public class ActionSCOL extends ZAction {
		public ActionSCOL(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final ScolBordereauCtrl dial = new ScolBordereauCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(false);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(true);
		}

	}

	/**
	 * Visa bordereaux cheques
	 */
	public final class ActionCHEVI extends ZAction {
		public ActionCHEVI(EOFonction fonc) {
			super(fonc);
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
		}
	}

	public final class ActionIMPR007 extends ZActionImpr {
		public ActionIMPR007(EOFonction fon) {
			super(fon);
			//			putValue(Action.SHORT_DESCRIPTION, "Impression du plan comptable");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				final PlanComptableImprCtrl dial = new PlanComptableImprCtrl(myApp.editingContext(), myApp.appUserInfo().currentExercice);
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression journal */
	public final class ActionIMPR001 extends ZActionImpr {
		public ActionIMPR001(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Impression du journal général");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final JournalGeneralCtrl dial = new JournalGeneralCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression du grand livre (fiche de compte) */
	public final class ActionIMPR002 extends ZActionImpr {
		public ActionIMPR002(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Impression du grand livre (fiche de compte)");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final GrandLivreCtrl dial = new GrandLivreCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression des balances */
	public final class ActionIMPR003 extends ZActionImpr {
		public ActionIMPR003(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Impression des balances");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final BalanceCtrl dial = new BalanceCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression Dev solde */
	public final class ActionIMPR004 extends ZActionImpr {
		public ActionIMPR004(EOFonction fon) {
			super(fon);
			//			putValue(Action.SHORT_DESCRIPTION, "Etat de développement de soldes");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final DevSoldeCtrl dial = new DevSoldeCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression Tout emargement */
	public final class ActionIMPR005 extends ZActionImpr {
		public ActionIMPR005(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Tout emargement");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final ToutEmargementCtrl dial = new ToutEmargementCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression Journal de TVA déductible et collectée */
	public final class ActionIMPR011 extends ZActionImpr {
		public ActionIMPR011(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Journal de TVA déductible et collectée");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final JournalTvaCtrl dial = new JournalTvaCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Impression Journal divisionnaire */
	public final class ActionIMPR012 extends ZActionImpr {
		public ActionIMPR012(EOFonction fon) {
			super(fon);
			//	        putValue(Action.SHORT_DESCRIPTION, "Journal de TVA déductible et collectée");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final JournalDivCtrl dial = new JournalDivCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Impression cohérence solde */
	public final class ActionIMPR013 extends ZActionImpr {
		public ActionIMPR013(EOFonction fon) {
			super(fon);
			//	        putValue(Action.SHORT_DESCRIPTION, "Journal de TVA déductible et collectée");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final CoherenceSoldeCtrl dial = new CoherenceSoldeCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Impression situation depense fournisseur */
	public final class ActionIMPR014 extends ZActionImpr {
		public ActionIMPR014(EOFonction fon) {
			super(fon);
			//	        putValue(Action.SHORT_DESCRIPTION, "Journal de TVA déductible et collectée");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				//	            checkActionWithExeStat(this);
				final SitFourDepenseCtrl dial = new SitFourDepenseCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Impression situation debiteur */
	public final class ActionIMPR015 extends ZActionImpr {
		public ActionIMPR015(EOFonction fon) {
			super(fon);
			//	        putValue(Action.SHORT_DESCRIPTION, "Journal de TVA déductible et collectée");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				//	            checkActionWithExeStat(this);
				final SitfFourRecetteCtrl dial = new SitfFourRecetteCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Impression Feuillets budgetaires */
	public final class ActionIMPR010 extends ZActionImpr {
		public ActionIMPR010(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Feuillets budgétaires");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				final FeuilletsBudCtrl dial = new FeuilletsBudCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression des comptabilités et codes gestions */
	public final class ActionIMPR_COMPTABILITEGESTION extends ZActionImpr {
		public ActionIMPR_COMPTABILITEGESTION() {
			super(ID_IMPR_COMPTABILITEGESTION, "Comptabilités / codes gestion");
			putValue(Action.SHORT_DESCRIPTION, "Impression des comptabilités et codes gestions");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				//		        ReportFactoryClient rf = new ReportFactoryClient();
				String filePath = ReportFactoryClient.imprimerComptabiliteGestion(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), myApp.getMainWindow());
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression des modes de paiement */
	public final class ActionIMPR008 extends ZActionImpr {
		public ActionIMPR008(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, "Impression des modes de paiements");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				//		        ReportFactoryClient rf = new ReportFactoryClient();
				final NSMutableDictionary params = new NSMutableDictionary(myApp.getParametres());
				params.takeValueForKey(myApp.appUserInfo().getCurrentExercice().exeExercice(), "EXEORDRE");

				String filePath = ReportFactoryClient.imprimerModePaiement(myApp.editingContext(), myApp.temporaryDir, params);
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression des restes à recouvrer */
	public final class ActionIMPR009 extends ZActionImpr {
		public ActionIMPR009(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, null);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				ResteRecouvrerCtrl dial = new ResteRecouvrerCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	/** Saisie OP */
	public final class ActionPAOPSA extends ZAction {
		public ActionPAOPSA(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, null);
			//            putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(false);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(false);
		}
	}

	/** Suppression OP */
	public final class ActionPAOPSU extends ZAction {
		public ActionPAOPSU(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, null);
			//          putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(false);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(false);
		}
	}

	/** visa OP */
	public final class ActionPAOPVI extends ZAction {
		public ActionPAOPVI(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, null);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				ResteRecouvrerCtrl dial = new ResteRecouvrerCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		protected void initCanExecute() {
			super.initCanExecute();
			setCanExecuteOnExercicePreparation(false);
			setCanExecuteOnExerciceOuvert(true);
			setCanExecuteOnExerciceRestreint(true);
			setCanExecuteOnExerciceClos(false);
		}
	}

	/**
	 * Saisie des suspensions de DGP
	 */
	public final class ActionIMSUSP extends ZAction {
		public ActionIMSUSP(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, null);
			//			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
		}

		protected void initCanExecute() {
			//super.initCanExecute();
		}
	}

	/**
	 * Saisie des intérêts moratoires
	 */
	public final class ActionIMSAIS extends ZAction {
		public ActionIMSAIS(EOFonction fon) {
			super(fon);
			putValue(Action.SHORT_DESCRIPTION, null);
			//			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
		}

		protected void initCanExecute() {
			//super.initCanExecute();
		}
	}

	public final class ActionICONIFY extends ZAction {
		public ActionICONIFY() {
			super(ID_ICONIFY, "Réduire la fenêtre");
			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("alt Z"));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				System.out.println("iconify");
				myApp.superviseur().iconify();
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression des Bordereau des mandats non admis */
	public final class ActionIMPR_BTMNA extends ZActionImpr {
		private EOBordereauRejet brj;

		public ActionIMPR_BTMNA() {
			super(ID_IMPR_BTMNA, "Bordereau des mandats non admis");
			putValue(Action.SHORT_DESCRIPTION, "Impression d'un bordereau");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				if (brj == null) {
					throw new DefaultClientException("Le bordereau n'est pas spécifié");
				}

				final String filePath = ReportFactoryClient.imprimerBtmna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(brj));
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		public void setBrj(EOBordereauRejet rejet) {
			brj = rejet;
		}
	}

	public final class ActionIMPR_JOURNAL_REJET extends ZActionImpr {

		public ActionIMPR_JOURNAL_REJET() {
			super(ID_IMPR_JOURNAL_REJET, "Journal des rejets");
			putValue(Action.SHORT_DESCRIPTION, "Impression du journal des rejets");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			final JournalRejetCtrl myDial;
			try {
				myDial = new JournalRejetCtrl(myApp.editingContext());
				myDial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	public final class ActionIMPR_JOURNAL_CONVRA extends ZActionImpr {

		public ActionIMPR_JOURNAL_CONVRA() {
			super(ID_IMPR_JOURNAL_CONVRA, "Journal / convention RA");
			putValue(Action.SHORT_DESCRIPTION, "Impression du journalpar convention RA");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			final JournalConventionRACtrl myDial;
			try {
				myDial = new JournalConventionRACtrl(myApp.editingContext());
				myDial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}

		}
	}

	/** Impression des Bordereau de titre non admis */
	public final class ActionIMPR_BTTNA extends ZActionImpr {
		private EOBordereauRejet brj;

		public ActionIMPR_BTTNA() {
			super(ID_IMPR_BTTNA, "Bordereau des titres non admis");
			putValue(Action.SHORT_DESCRIPTION, "Impression b'un bordereau");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				if (brj == null) {
					throw new DefaultClientException("Le bordereau n'est pas spécifié");
				}
				//		        ReportFactoryClient rf = new ReportFactoryClient();
				String filePath = ReportFactoryClient.imprimerBttna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(brj));
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		public void setBrj(EOBordereauRejet rejet) {
			brj = rejet;
		}
	}

	/** Impression des listes d'opérations extournées */
	public final class Action_IMPR_LISTE_OP_EXTOURNEES extends ZActionImpr {
		private EOBordereau bordereau;

		public Action_IMPR_LISTE_OP_EXTOURNEES() {
			super(ID_IMPR_LISTE_OP_EXTOURNEES, "Liste des opérations extournées");
			putValue(Action.SHORT_DESCRIPTION, "Impression de la liste");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			try {
				checkActionWithExeStat(this);
				if (bordereau == null) {
					throw new DefaultClientException("Le bordereau n'est pas spécifié");
				}
				String filePath = ReportFactoryClient.imprimerBordereauExtourne(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(bordereau));
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}

		public void setBordereau(EOBordereau bordereau) {
			this.bordereau = bordereau;
		}
	}

	public final class ActionAIDE_CHANGELOG extends ZActionNavig {
		public ActionAIDE_CHANGELOG() {
			super(ID_AIDE_CHANGELOG, "Historique des versions");
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showHelp("CHANGELOG");
		}
	}

	public final class ActionAIDE_LOGVIEWER extends ZActionNavig {
		public ActionAIDE_LOGVIEWER() {
			super(ID_AIDE_LOGVIEWER, "Visualiseur de logs");
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.showLogViewer();
		}
	}

	public final class ActionOUTILS_REFRESHDATA extends ZActionNavig {
		public ActionOUTILS_REFRESHDATA() {
			super(ID_OUTILS_REFRESHDATA, "Rafraîchir les données");
			putValue(Action.SHORT_DESCRIPTION, "Mise à jour des données depuis le serveur");
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
		}

		public void actionPerformed(ActionEvent e) {
			super.actionPerformed(e);
			myApp.refreshEoEnterprisesObjects(myApp.activeWindow());
			myApp.refreshInterface();
		}
	}

}
