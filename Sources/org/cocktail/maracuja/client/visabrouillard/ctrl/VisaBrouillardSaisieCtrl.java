/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visabrouillard.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBrouillard;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.visabrouillard.ui.VisaBrouillardSaisiePanel;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


public class VisaBrouillardSaisieCtrl extends CommonCtrl {
	//	private static final String TITLE = "Modification d'un brouillard";
	private static final Dimension WINDOW_DIMENSION = new Dimension(950, 580);

	private final VisaBrouillardSaisiePanel visaBrouillardSaisiePanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionValider actionValider = new ActionValider();

	private final Map currentDico;
	private EOBrouillard currentBrouillard;

	private BrouillardDetailListMdl brouillardDetailListMdl;
	private final BalanceMdl balanceMdl;

	/**
	 * @param editingContext
	 */
	public VisaBrouillardSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);
		currentDico = new HashMap();
		balanceMdl = new BalanceMdl();
		brouillardDetailListMdl = new BrouillardDetailListMdl();
		visaBrouillardSaisiePanel = new VisaBrouillardSaisiePanel(new VisaBrouillardSaisiePanelMdl());
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	private final void validerSaisie() {
		try {
			checkSaisie();
			updateObjectWithDico();

			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class VisaBrouillardSaisiePanelMdl implements VisaBrouillardSaisiePanel.IVisaBrouillardSaisiePanelMdl {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map getValues() {
			return currentDico;
		}

		public IZTablePanelMdl brouillardDetailListMdl() {
			return brouillardDetailListMdl;
		}

		public IZPanelBalanceProvider balanceMdl() {
			return balanceMdl;
		}

	}

	private NSArray getBrouillardDetails() {
		if (currentBrouillard != null) {
			return currentBrouillard.toBrouillardDetails();
		}
		return NSArray.EmptyArray;
	}

	private final class BrouillardDetailListMdl implements IZTablePanelMdl {

		public void selectionChanged() {
			//refreshActions();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return getBrouillardDetails();
		}

		public void onDbClick() {

		}

	}

	/**
	 * @param dial
	 * @param im
	 * @return
	 */
	public final int openDialog(Window dial, EOBrouillard im, String title) {
		final ZKarukeraDialog win = createDialog(dial, title, true);

		int res = ZKarukeraDialog.MRCANCEL;
		try {
			currentBrouillard = im;
			updateDicoWithObject();
			visaBrouillardSaisiePanel.initGUI();
			visaBrouillardSaisiePanel.updateData();

			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	//	private void updateImTaux() throws Exception {
	//		NSMutableArray res = new NSMutableArray();
	//		NSTimestamp dateFinDGPReelle = (NSTimestamp) currentDico.get(EOBrouillard.IM_DATE_FIN_DGP_REELLE_KEY);
	//		NSTimestamp dateDebutCalculIMs = FactoryIm.getDateDebutCalculIms(dateFinDGPReelle);
	//
	//		if (dateDebutCalculIMs != null) {
	//			//			//Récupérer les types de taux possibles pour une date
	//			//			NSArray typeTaux = EOBrouillardTypeTaux.fetchAllForDate(getEditingContext(), dateDebutCalculIMs);
	//			//			if (typeTaux.count() == 0) {
	//			//				throw new Exception("Aucun type de taux n'est défini pour la date de début de calcul des IM : " + ZConst.FORMAT_DATESHORT.format(dateDebutCalculIMs));
	//			//			}
	//			//
	//			//			for (int i = 0; i < typeTaux.count(); i++) {
	//			//				EOBrouillardTypeTaux tt = (EOBrouillardTypeTaux) typeTaux.objectAtIndex(i);
	//			//				res.addObjectsFromArray(EOBrouillardTaux.fetchAllForDateAndType(getEditingContext(), dateDebutCalculIMs, tt));
	//			//			}
	//			//			
	//			NSTimestamp dateDebutSemestre = new NSTimestamp(ZDateUtil.addDHMS(ZDateUtil.getDateDebutSemestre(dateDebutCalculIMs), -1, 0, 0, 0));
	//			res.addObjectsFromArray(EOBrouillardTaux.fetchAllForDateAndType(getEditingContext(), dateDebutSemestre, EOBrouillardTypeTaux.IMTT_CODE_TBCE));
	//			res.addObjectsFromArray(EOBrouillardTaux.fetchAllForDateAndType(getEditingContext(), dateDebutCalculIMs, EOBrouillardTypeTaux.IMTT_CODE_TIL));
	//			EOSortOrdering.sortArrayUsingKeyOrderArray(res, new NSArray(EOBrouillardTaux.SORT_IMTT_PRIORITE_DESC));
	//		}
	//
	//		//		System.out.println(res);
	//		tauxReferenceModel.updateListWithData(res);
	//		tauxReferenceModel.setSelectedEObject(null);
	//		if (res.indexOfObject(currentDico.get(EOBrouillard.IM_TAUX_KEY)) != NSArray.NotFound) {
	//			tauxReferenceModel.setSelectedEObject((NSKeyValueCoding) currentDico.get(EOBrouillard.IM_TAUX_KEY));
	//		}
	//
	//	}

	private void updateObjectWithDico() {
		currentBrouillard.setBroLibelle((String) currentDico.get(EOBrouillard.BRO_LIBELLE_KEY));
		currentBrouillard.setBroMotifRejet((String) currentDico.get(EOBrouillard.BRO_MOTIF_REJET_KEY));

	}

	private void updateDicoWithObject() {
		currentDico.clear();
		currentDico.put(EOBrouillard.BRO_LIBELLE_KEY, currentBrouillard.broLibelle());
		currentDico.put(EOBrouillard.TO_TYPE_JOURNAL_KEY + "." + EOTypeJournal.TJO_LIBELLE_KEY, currentBrouillard.toTypeJournal().tjoLibelle());
		currentDico.put(EOBrouillard.TO_TYPE_OPERATION_KEY + "." + EOTypeOperation.TOP_LIBELLE_KEY, currentBrouillard.toTypeOperation().topLibelle());
		currentDico.put(EOBrouillard.BRO_MOTIF_REJET_KEY, currentBrouillard.broMotifRejet());
		currentDico.put(EOBrouillard.TO_BORDEREAU_KEY + "." + EOBordereau.TYPE_BORDEREAU_KEY + "." + EOTypeBordereau.TBO_LIBELLE_KEY, (currentBrouillard.toBordereau() != null ? currentBrouillard.toBordereau().typeBordereau().tboLibelle() : null));
		currentDico.put(EOBrouillard.TO_ORIGINE_KEY + "." + EOOrigine.ORI_LIBELLE_KEY, (currentBrouillard.toOrigine() != null ? currentBrouillard.toOrigine().oriLibelle() : null));
		currentDico.put(EOBrouillard.TO_PERSONNE_CREATION_KEY + "." + EOPersonne.NOM_PRENOM_KEY, (currentBrouillard.toPersonneCreation() != null ? currentBrouillard.toPersonneCreation().getNomAndPrenom() : null));
		currentDico.put(EOBrouillard.DATE_CREATION_KEY, currentBrouillard.dateCreation());
		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return visaBrouillardSaisiePanel;
	}

	public String title() {
		return null;
	}

	private final class BalanceMdl implements IZPanelBalanceProvider {

		public BigDecimal getDebitValue() {
			return getSelectedDebitsMontant();
		}

		public BigDecimal getCreditValue() {
			return getSelectedCreditsMontant();
		}

	}

	private BigDecimal getSelectedDebitsMontant() {
		if (currentBrouillard == null) {
			return ZConst.BIGDECIMAL_ZERO;
		}
		return currentBrouillard.getMontantDebits();
	}

	private BigDecimal getSelectedCreditsMontant() {
		if (currentBrouillard == null) {
			return ZConst.BIGDECIMAL_ZERO;
		}
		return currentBrouillard.getMontantCredits();
	}
}
