/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visabrouillard.ctrl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBrouillard;
import org.cocktail.maracuja.client.metier.EOBrouillardDetail;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.visabrouillard.ui.VisaBrouillardFiltrePanel.IVisaBrouillardFiltrePanelCtrl;
import org.cocktail.maracuja.client.visabrouillard.ui.VisaBrouillardPanel;
import org.cocktail.maracuja.client.visabrouillard.ui.VisaBrouillardPanel.IVisaBrouillardPanelCtrl;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaBrouillardCtrl extends CommonCtrl implements IVisaBrouillardPanelCtrl {
	public static final String ACTION_ID = "";
	public static final String TOUS = "Tous";
	private static final Dimension WINDOW_DIMENSION = new Dimension(990, 700);
	private static final String TITLE = "Visa des brouillards en attente";

	private final VisaBrouillardPanel myPanel;
	private final ActionRejeter actionRejeter = new ActionRejeter();
	private final ActionViser actionViser = new ActionViser();
	private final ActionModifier actionModifier = new ActionModifier();
	private final ActionVoirEcritures actionVoirEcritures = new ActionVoirEcritures();

	private final BrouillardListMdl brouillardListMdl = new BrouillardListMdl();
	private final BrouillardDetailListMdl brouillardDetailListMdl = new BrouillardDetailListMdl();
	private final BordereauListMdl bordereauListMdl = new BordereauListMdl();
	private final VisaBrouillardFiltrePanelMdl visaBrouillardFiltrePanelMdl = new VisaBrouillardFiltrePanelMdl();

	private final DefaultComboBoxModel comboEtatBordereauModel;
	private final ZEOComboBoxModel comboGesCodeModel;
	private final ZEOComboBoxModel comboTypeBordereauModel;

	private final FiltreChangedListener filtreChangedListener;
	private final BalanceMdl balanceMdl;

	private final BrouillardDetailListTableCellRenderer brouillardDetailListTableCellRenderer = new BrouillardDetailListTableCellRenderer();

	public VisaBrouillardCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		//Récupérer les droits
		//		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID)) {
		//			throw new DefaultClientException("Vous n'avez pas les droits nécessaires pour cette fonctionnalité.");
		//		}

		comboEtatBordereauModel = new DefaultComboBoxModel();
		comboEtatBordereauModel.addElement(TOUS);
		comboEtatBordereauModel.addElement(EOBordereau.BordereauValide);
		comboEtatBordereauModel.addElement(EOBordereau.BordereauVise);
		comboEtatBordereauModel.addElement(EOBordereau.BordereauAnnule);
		comboEtatBordereauModel.setSelectedItem(EOBordereau.BordereauValide);

		//res = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_VIDE);
		NSArray allGestions = EOGestion.fetchAllForExercice(editingContext, getExercice());
		comboGesCodeModel = new ZEOComboBoxModel(allGestions, EOGestion.GES_CODE_KEY, "Tous", null);

		NSArray typeBordereaux = EOTypeBordereau.fetchAllForType(editingContext, EOTypeBordereau.TYPEBORDEREAU_BROUILLARDS);
		comboTypeBordereauModel = new ZEOComboBoxModel(typeBordereaux, EOTypeBordereau.TBO_LIBELLE_KEY, "Tous", null);
		comboTypeBordereauModel.setSelectedEObject((NSKeyValueCoding) typeBordereaux.objectAtIndex(0));

		filtreChangedListener = new FiltreChangedListener();
		balanceMdl = new BalanceMdl();
		myPanel = new VisaBrouillardPanel(this);

	}

	public IZTablePanelMdl brouillardListMdl() {
		return brouillardListMdl;
	}

	public IZKarukeraTablePanelListener bordereauListMdl() {
		return bordereauListMdl;
	}

	public IZTablePanelMdl brouillardDetailListMdl() {
		return brouillardDetailListMdl;
	}

	public Action actionClose() {
		return actionClose;
	}

	public Action actionRejeter() {
		return actionRejeter;
	}

	public Action actionViser() {
		return actionViser;
	}

	public Action actionModifier() {
		return actionModifier;
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public VisaBrouillardPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	private final class ActionModifier extends AbstractAction {
		public ActionModifier() {
			super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onModifier();
		}
	}

	private final class ActionRejeter extends AbstractAction {
		public ActionRejeter() {
			super("Rejeter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ERROR_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onRejeter();
		}
	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Accepter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
			//				putValue(AbstractAction.SHORT_DESCRIPTION, "Accepter la demande. Un Ordre de Paiement va être créé");
		}

		public void actionPerformed(ActionEvent e) {
			onAccepter();
		}
	}

	private final class ActionVoirEcritures extends AbstractAction {
		public ActionVoirEcritures() {
			super("Ecritures");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir le détail des écritures générées à partir du brouillard sélectionné");
			setEnabled(true);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			voirEcritures();
		}
	}

	private final void voirEcritures() {
		try {
			EOBrouillard brouillard = getSelectedBrouillard();
			if (brouillard == null) {
				throw new DefaultClientException("Aucun brouillard sélectionné.");
			}
			NSArray ecritures = brouillard.getEcrituresGenerees();

			NSMutableArray allEcritures = new NSMutableArray();
			allEcritures.addObjectsFromArray(ecritures);

			if (allEcritures.count() == 0) {
				showInfoDialog("Aucune écriture n'a été générée pour le brouillard sélectionné.");
				return;
			}
			EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
			win.openDialog(getMyDialog(), allEcritures);

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public EOBordereau getSelectedBordereau() {
		return (EOBordereau) myPanel.getBordereauListPanel().selectedObject();
	}

	private final void refreshActions() {
		final EOBordereau bordereau = getSelectedBordereau();
		actionModifier.setEnabled(false);
		actionRejeter.setEnabled(false);
		actionViser.setEnabled(false);
		actionVoirEcritures.setEnabled(false);

		if (bordereau != null) {
			actionModifier.setEnabled(bordereau.isValide());
			actionRejeter.setEnabled(bordereau.isValide());
			actionViser.setEnabled(bordereau.isValide());
			actionVoirEcritures.setEnabled(getSelectedBrouillard() != null && getSelectedBrouillard().isVise());
		}
	}

	private void onAccepter() {
		setWaitCursor(true);
		try {
			if (getSelectedBrouillards() == null || getSelectedBrouillards().count() == 0) {
				throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
			}
			NSArray brouillards = getSelectedBrouillards();
			String msg = "Souhaitez-vous valider le brouillard sélectionné ? ";
			if (brouillards.count() > 1) {
				msg = "Souhaitez-vous valider les " + brouillards.count() + " brouillards sélectionnés ? ";
			}
			if (showConfirmationDialog("Confirmation", msg, ZMsgPanel.BTLABEL_NO)) {
				FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				NSMutableArray newPlancos = new NSMutableArray();
				NSMutableArray newEcritures = new NSMutableArray();
				for (int i = 0; i < brouillards.count(); i++) {
					newPlancos.removeAllObjects();
					EOBrouillard brouillard = (EOBrouillard) brouillards.objectAtIndex(i);
					brouillard.initPlanComptableExer(getEditingContext());
					//Verifier si les comptes existent
					NSArray pcos = brouillard.getPlanComptablesNonValides();
					if (pcos.count() > 0) {

						String tmp = "";
						for (int j = 0; j < pcos.count(); j++) {
							NSDictionary pco = (NSDictionary) pcos.objectAtIndex(j);
							tmp = tmp.concat("<li><b>" + pco.valueForKey(EOBrouillardDetail.BROD_PCO_NUM_KEY) + "</b> " + pco.valueForKey(EOBrouillardDetail.BROD_PCO_LIBELLE_KEY)) + "</li>";
						}
						boolean creerAuto = showConfirmationDialog("Confirmation", "<html>Les comptes suivant spécifiés dans les brouillards n'existent pas, ne sont pas valides ou ont un libellé différents dans le plan comptable de " +
								brouillard.toExercice().exeExercice()
								+ ", souhaitez-vous les créer/valider automatiquement ? <ul>" + tmp
								+ "</ul></html>", ZMsgPanel.BTLABEL_YES);

						if (!creerAuto) {
							throw new DefaultClientException("Traitement annulé par l'utilisateur");
						}
						else {
							for (int j = 0; j < pcos.count(); j++) {
								NSDictionary pco = (NSDictionary) pcos.objectAtIndex(j);
								String newPcoNum = (String) pco.valueForKey(EOBrouillardDetail.BROD_PCO_NUM_KEY);
								String pcoLibelle = (String) pco.valueForKey(EOBrouillardDetail.BROD_PCO_LIBELLE_KEY);
								if (newPcoNum.length() < 3) {
									throw new Exception("Impossible de créer un compte de niveau <3");
								}

								EOPlanComptableExer compte = EOPlanComptableExer.getCompte(getEditingContext(), getExercice(), newPcoNum);
								if (compte == null) {
									//Récupérer le compte "pere"
									EOPlanComptableExer pcoExerPere = null;
									int x = 0;
									String pcoExerPereStr = newPcoNum;
									while (pcoExerPere == null && x <= 21) {
										pcoExerPereStr = pcoExerPereStr.substring(0, pcoExerPereStr.length() - 1);
										pcoExerPere = EOPlanComptableExer.getCompte(getEditingContext(), getExercice(), pcoExerPereStr);
										System.out.println("recherche : " + pcoExerPereStr);
										x++;
									}
									compte = EOPlanComptableExer.creerOuValiderPlanComptable(getEditingContext(), getExercice(), (String) pco.valueForKey(EOBrouillardDetail.BROD_PCO_NUM_KEY), (String) pco.valueForKey(EOBrouillardDetail.BROD_PCO_LIBELLE_KEY),
											pcoExerPere.pcoBudgetaire(),
											pcoExerPere.pcoEmargement(), pcoExerPere.pcoNature());

									compte.setPcoJBe(pcoExerPere.pcoJBe());
									compte.setPcoJExercice(pcoExerPere.pcoJExercice());
									compte.setPcoJFinExercice(pcoExerPere.pcoJFinExercice());
									compte.setPcoSensSolde(pcoExerPere.pcoSensSolde());

								}
								else {
									//si libelles differents ou invalide
									if (ZStringUtil.estVide(pcoLibelle) && !compte.pcoLibelle().toUpperCase().equals(pcoLibelle.toUpperCase())) {
										int choix = showConfirmationCancelDialog("Confirmation", "Le compte " + newPcoNum + " : " + pcoLibelle + " existe déjà dans le plan comptable mais avec un libellé différent (" + compte.pcoLibelle()
												+ "). Souhaitez-vous remplacer le libellé du plan comptable par le libellé indiqué dans le brouillard ? ", ZMsgPanel.BTLABEL_NO);
										if (choix == ZMsgPanel.MR_CANCEL) {
											throw new DefaultClientException("Traitement annulé par l'utilisateur");
										}
										if (choix == ZMsgPanel.MR_YES) {
											compte.setPcoLibelle(pcoLibelle);
										}
									}
									if (!compte.isValide()) {
										compte.setPcoValidite(EOPlanComptableExer.etatValide);
									}
								}
								newPlancos.addObject(compte);
							}
						}
					}
					EOEcriture ecr = brouillard.accepter(getUtilisateur().personne(), new NSTimestamp(myApp.getDateJourneeComptable()), newPlancos);
					newEcritures.addObject(ecr);

					NSArray ecrituresSACD = null;
					NSArray details185 = ZEOUtilities.getFilteredArrayByKeyValue(ecr.detailEcriture(), "planComptable.pcoNum", "185*", "like");
					if (details185 == null || details185.count() == 0) {
						try {

							ecrituresSACD = factoryProcessJournalEcriture.determinerLesEcrituresSACD(getEditingContext(), ecr);
							if (ecrituresSACD != null && ecrituresSACD.count() > 0) {
								newEcritures.addObjectsFromArray(ecrituresSACD);
							}
						} catch (Exception e) {
							e.printStackTrace();
							throw new Exception("Erreur lors de la détermination des éventuelles écritures SACD." + e.getMessage());
						}
					}

					getSelectedBordereau().setBorDateVisa(Factory.getNow());
					getSelectedBordereau().setBorEtat(EOBordereau.BordereauVise);
					getSelectedBordereau().setUtilisateurVisaRelationship(getUtilisateur());

				}

				if (getEditingContext().hasChanges()) {
					getEditingContext().saveChanges();
				}
				//la sauvegarde
				//			try {
				//				if (getEditingContext().hasChanges()) {
				//					getEditingContext().saveChanges();
				//				}
				//			}
				//			//Permet de traiter l'exception qui se produit de tant en temps...
				//			//pas terrible, mais pour l'instant je ne sais pas d'où vient cette exception
				//			catch (java.lang.IllegalStateException e) {
				//				//vérifier si l'objet est bien enregistré
				//				if ((getEditingContext().globalIDForObject(currentEcriture) instanceof EOTemporaryGlobalID) || currentEcriture.detailEcriture() == null || currentEcriture.detailEcriture().count() == 0) {
				//					System.out.println("Erreur : objet non enregistre = " + currentEcriture);
				//					throw e;
				//				}
				//				System.out.println("Erreur IllegalStateException mais objet enregistre = " + currentEcriture);
				//				//sinon on invalide
				//				getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				//						getEditingContext().globalIDForObject(currentEcriture)
				//				}));
				//			}

				String msgFin = "";
				msgFin = msgFin + "Nombre d'écritures générées : " + newEcritures.count() + "\n";

				//Numéroter
				try {
					KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
					NSMutableArray nbs = new NSMutableArray();
					if ((newEcritures != null) && (newEcritures.count() > 0)) {
						for (int i = 0; i < newEcritures.count(); i++) {
							EOEcriture element = (EOEcriture) newEcritures.objectAtIndex(i);
							factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
							nbs.addObject(element.ecrNumero());
						}
					}
					if (nbs.count() > 0) {
						msgFin = msgFin + "\nLes écritures ont été numérotées : " + nbs.componentsJoinedByString(",");
					}
				} catch (Exception e) {
					e.printStackTrace();
					msgFin = msgFin + "\nErreur lors de la numérotation des écritures : \n";
					msgFin = msgFin + e.getMessage();
				}
				myApp.showInfoDialog(msgFin);
			}

		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			updateData();
			setWaitCursor(false);
			refreshActions();
		}
	}

	private void onRejeter() {
		setWaitCursor(true);
		try {
			if (getSelectedBordereau() == null || getSelectedBrouillard() == null) {
				throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
			}

			String msg = "Souhaitez-vous rejeter le brouillard sélectionné ? ";
			if (showConfirmationDialog("Confirmation", msg, ZMsgPanel.BTLABEL_NO)) {

				boolean proceed = true;
				if (ZStringUtil.isEmpty(getSelectedBrouillard().broMotifRejet())) {
					final VisaBrouillardSaisieCtrl saisieCtrl = new VisaBrouillardSaisieCtrl(getEditingContext());
					int res = saisieCtrl.openDialog(getMyDialog(), getSelectedBrouillard(), "Rejet d'un brouillard");
					if (res == ZCommonDialog.MRCANCEL) {
						proceed = false;
					}
				}
				if (proceed) {
					getSelectedBrouillard().rejeter(getUtilisateur().personne());
					getSelectedBordereau().setBorDateVisa(Factory.getNow());
					getSelectedBordereau().setBorEtat(EOBordereau.BordereauAnnule);
					getSelectedBordereau().setUtilisateurVisaRelationship(getUtilisateur());
					if (getEditingContext().hasChanges()) {
						getEditingContext().saveChanges();
					}
				}
			}

		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			updateData();
			refreshActions();
		}
	}

	private void onModifier() {
		if (actionModifier.isEnabled()) {
			try {
				if (getSelectedBordereau() == null || getSelectedBrouillard() == null) {
					throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
				}

				final VisaBrouillardSaisieCtrl saisieCtrl = new VisaBrouillardSaisieCtrl(getEditingContext());
				saisieCtrl.openDialog(getMyDialog(), getSelectedBrouillard(), "Modification d'un brouillard");
				updateData();

			} catch (Exception e) {
				if (getEditingContext().hasChanges()) {
					getEditingContext().revert();
				}
				setWaitCursor(false);
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
				refreshActions();
			}
		}

	}

	private void updateData() {
		setWaitCursor(true);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}

	}

	private final class BordereauListMdl implements IZKarukeraTablePanelListener {

		public void selectionChanged() {
			try {
				myPanel.getBrouillardListPanel().updateData();
				refreshActions();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			NSArray res = getBordereaux();
			refreshActions();
			return res;
		}

		public void onDbClick() {

		}

	}

	private final class BrouillardListMdl implements IZTablePanelMdl {

		public void selectionChanged() {
			try {
				if (getSelectedBrouillard() != null) {
					getSelectedBrouillard().initPlanComptableExer(getEditingContext());
				}
				myPanel.getBrouillardDetailListPanel().updateData();
				myPanel.getMyZPanelBalanceNew().updateData();
				refreshActions();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return getBrouillards();
		}

		public void onDbClick() {

		}

	}

	private final class BrouillardDetailListMdl implements IZTablePanelMdl {

		public void selectionChanged() {
			refreshActions();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return getBrouillardDetails();
		}

		public void onDbClick() {

		}

	}

	private final class VisaBrouillardFiltrePanelMdl implements IVisaBrouillardFiltrePanelCtrl {

		public ComboBoxModel getComboEtatBordereauModel() {
			return comboEtatBordereauModel;
		}

		public ComboBoxModel getComboGesCodeModel() {
			return comboGesCodeModel;
		}

		public ComboBoxModel getComboTypeBordereauModel() {
			return comboTypeBordereauModel;
		}

		public ActionListener filtreChangedListener() {
			return filtreChangedListener;
		}

	}

	private final class FiltreChangedListener implements ActionListener {

		public void actionPerformed(ActionEvent arg0) {
			updateData();
		}
	}

	private NSArray getBordereaux() {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOBordereau.TYPE_BORDEREAU_KEY + "." + EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeBordereau.TYPEBORDEREAU_BROUILLARDS));
		quals.addObject(new EOKeyValueQualifier(EOBordereau.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, getExercice()));
		if (!TOUS.equals(comboEtatBordereauModel.getSelectedItem())) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.BOR_ETAT_KEY, EOQualifier.QualifierOperatorEqual, comboEtatBordereauModel.getSelectedItem()));
		}
		if (comboGesCodeModel.getSelectedEObject() != null) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.GESTION_KEY, EOQualifier.QualifierOperatorEqual, comboGesCodeModel.getSelectedEObject()));
		}
		if (comboTypeBordereauModel.getSelectedEObject() != null) {
			quals.addObject(new EOKeyValueQualifier(EOBordereau.TYPE_BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, comboTypeBordereauModel.getSelectedEObject()));
		}

		return EOBordereau.fetchAll(getEditingContext(), new EOAndQualifier(quals), new NSArray(new Object[] {
				EOBordereau.SORT_BOR_NUM_ASC
		}));
	}

	private NSArray getBrouillards() {
		if (getSelectedBordereau() != null) {
			return getSelectedBordereau().toBrouillards();
		}
		return NSArray.EmptyArray;
	}

	private NSArray getBrouillardDetails() {
		if (getSelectedBrouillard() != null) {
			return getSelectedBrouillard().toBrouillardDetails();
		}
		return NSArray.EmptyArray;
	}

	public IVisaBrouillardFiltrePanelCtrl visaBrouillardFiltrePanelMdl() {
		return visaBrouillardFiltrePanelMdl;
	}

	public EOBrouillard getSelectedBrouillard() {
		return (EOBrouillard) myPanel.getBrouillardListPanel().selectedObject();
	}

	public NSArray getSelectedBrouillards() {
		return (NSArray) myPanel.getBrouillardListPanel().selectedObjects();
	}

	public EOBrouillardDetail getSelectedBrouillardDetail() {
		return (EOBrouillardDetail) myPanel.getBrouillardDetailListPanel().selectedObject();
	}

	private final class BalanceMdl implements IZPanelBalanceProvider {

		public BigDecimal getDebitValue() {
			return getSelectedDebitsMontant();
		}

		public BigDecimal getCreditValue() {
			return getSelectedCreditsMontant();
		}

	}

	private BigDecimal getSelectedDebitsMontant() {
		if (getSelectedBordereau() == null) {
			return ZConst.BIGDECIMAL_ZERO;
		}
		NSArray selectedBrouillards = getSelectedBrouillards();
		BigDecimal res = ZEOUtilities.calcSommeOfBigDecimals(selectedBrouillards, EOBrouillard.MONTANT_DEBITS_KEY);
		return res;
	}

	private BigDecimal getSelectedCreditsMontant() {
		if (getSelectedBordereau() == null) {
			return ZConst.BIGDECIMAL_ZERO;
		}
		NSArray selectedBrouillards = getSelectedBrouillards();
		BigDecimal res = ZEOUtilities.calcSommeOfBigDecimals(selectedBrouillards, EOBrouillard.MONTANT_CREDITS_KEY);
		return res;
	}

	public IZPanelBalanceProvider balanceMdl() {
		return balanceMdl;
	}

	@Override
	protected void onClose() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		super.onClose();
	}

	private static final class BrouillardDetailListTableCellRenderer extends ZEOTableCellRenderer {
		private static final Color COL_PCO_ABSENT = Color.decode("#FFA099");
		private static final Color COL_PCO_INVALIDE = Color.decode("#F2C75F");

		public BrouillardDetailListTableCellRenderer() {
			super();
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object,
		 *      boolean, boolean, int, int)
		 */
		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {

			Component res = null;
			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOBrouillardDetail obj = (EOBrouillardDetail) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);

			final ZEOTableModelColumn column2 = ((ZEOTable) table).getDataModel().getColumn(table.convertColumnIndexToModel(column));
			res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			//////

			if (isSelected) {
				res.setBackground(table.getSelectionBackground());
				res.setForeground(table.getSelectionForeground());
			}
			else {
				if (EOBrouillardDetail.BROD_PCO_NUM_KEY.equals(column2.getAttributeName())) {
					if (obj.getPlanComptableExer() == null) {
						res.setBackground(COL_PCO_ABSENT);
						res.setForeground(table.getForeground());
					}
					else if (!obj.getPlanComptableExer().isValide()) {
						res.setBackground(COL_PCO_INVALIDE);
						res.setForeground(table.getForeground());
					}
					else {
						res.setBackground(table.getBackground());
						res.setForeground(table.getForeground());
					}
				}
			}
			return res;
		}
	}

	public IZEOTableCellRenderer brouillardDetailListPanelRenderer() {
		return brouillardDetailListTableCellRenderer;
	}

	public Action actionVoirEcritures() {
		return actionVoirEcritures;
	}
}
