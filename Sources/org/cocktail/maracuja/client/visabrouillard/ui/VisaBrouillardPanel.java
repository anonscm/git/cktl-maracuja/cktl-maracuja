/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visabrouillard.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ui.BordereauListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.visabrouillard.ui.VisaBrouillardFiltrePanel.IVisaBrouillardFiltrePanelCtrl;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


public class VisaBrouillardPanel extends ZKarukeraPanel {
	private final IVisaBrouillardPanelCtrl myCtrl;
	private final BrouillardListTablePanel brouillardListPanel;
	private final BrouillardDetailListTablePanel brouillardDetailListPanel;
	private final BordereauListPanel bordereauListPanel;
	private final VisaBrouillardFiltrePanel visaBrouillardFiltrePanel;
	private final ZPanelBalance myZPanelBalanceNew;

	public VisaBrouillardPanel(IVisaBrouillardPanelCtrl ctrl) {
		super();
		myCtrl = ctrl;
		bordereauListPanel = new BordereauListPanel(myCtrl.bordereauListMdl());
		brouillardListPanel = new BrouillardListTablePanel(myCtrl.brouillardListMdl());
		brouillardDetailListPanel = new BrouillardDetailListTablePanel(myCtrl.brouillardDetailListMdl());
		visaBrouillardFiltrePanel = new VisaBrouillardFiltrePanel(myCtrl.visaBrouillardFiltrePanelMdl());
		myZPanelBalanceNew = new ZPanelBalance(myCtrl.balanceMdl());

		setLayout(new BorderLayout());
		final JPanel b = buildRightPanel();
		b.setPreferredSize(new Dimension(160, 20));

		final JPanel d = new JPanel(new BorderLayout());
		//JSplitPane brouillards = buildVerticalSplitPane(brouillardListPanel, brouillardDetailListPanel);
		JSplitPane brouillards = buildVerticalSplitPane(encloseInPanelWithTitle("Brouillards", null, ZConst.BG_COLOR_TITLE, brouillardListPanel, null, null), encloseInPanelWithTitle("Détail du brouillard", null, ZConst.BG_COLOR_TITLE, brouillardDetailListPanel, null, null));
		JSplitPane tout = buildVerticalSplitPane(bordereauListPanel, brouillards);

		d.add(tout, BorderLayout.CENTER);
		d.add(myZPanelBalanceNew, BorderLayout.SOUTH);

		final JPanel c = new JPanel(new BorderLayout());
		c.add(d, BorderLayout.CENTER);
		c.add(b, BorderLayout.EAST);

		add(buildBottomPanel(), BorderLayout.SOUTH);
		add(c, BorderLayout.CENTER);
		add(buildTopPanel(), BorderLayout.NORTH);
		visaBrouillardFiltrePanel.initGUI();
		bordereauListPanel.initGUI();
		brouillardListPanel.initGUI();
		brouillardDetailListPanel.initGUI();
		myZPanelBalanceNew.initGUI();

		brouillardDetailListPanel.getMyEOTable().setMyRenderer(myCtrl.brouillardDetailListPanelRenderer());
	}

	@Override
	public void initGUI() {

	}

	@Override
	public void updateData() throws Exception {
		visaBrouillardFiltrePanel.updateData();
		bordereauListPanel.updateData();

	}

	public BrouillardListTablePanel getBrouillardListPanel() {
		return brouillardListPanel;
	}

	public BrouillardDetailListTablePanel getBrouillardDetailListPanel() {
		return brouillardDetailListPanel;
	}

	public BordereauListPanel getBordereauListPanel() {
		return bordereauListPanel;
	}

	private final JPanel buildRightPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(50, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(myCtrl.actionModifier());
		list.add(myCtrl.actionRejeter());
		list.add(myCtrl.actionViser());
		list.add(myCtrl.actionVoirEcritures());

		final ArrayList buttons = ZKarukeraPanel.getButtonListFromActionList(list);
		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myCtrl.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private JPanel buildTopPanel() {
		JPanel p = new JPanel(new BorderLayout());
		final ZCommentPanel commentPanel = new ZCommentPanel(
				"Visa des brouillards",
				"<html>Cet écran contient les brouillards d'écriture soumis à votre validation.<br>Les N° de comptes indiqués dans les détails des brouillards n'existent pas obligatoirement dans le plan comptable. Les comptes inexistant/non valides dans le plan comptable (apparaissant en rouge/orange) peuvent être créés/activés automatiquement lors de l'acceptation du brouillard.</html>",
				null);
		p.add(commentPanel, BorderLayout.NORTH);
		p.add(visaBrouillardFiltrePanel, BorderLayout.CENTER);
		//		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	public ZPanelBalance getMyZPanelBalanceNew() {
		return myZPanelBalanceNew;
	}

	public interface IVisaBrouillardPanelCtrl {
		public ZTablePanel.IZTablePanelMdl brouillardListMdl();

		public Action actionVoirEcritures();

		public IZEOTableCellRenderer brouillardDetailListPanelRenderer();

		public IVisaBrouillardFiltrePanelCtrl visaBrouillardFiltrePanelMdl();

		public IZKarukeraTablePanelListener bordereauListMdl();

		public ZTablePanel.IZTablePanelMdl brouillardDetailListMdl();

		public IZPanelBalanceProvider balanceMdl();

		public Action actionClose();

		public Action actionRejeter();

		public Action actionViser();

		public Action actionModifier();

	}
}
