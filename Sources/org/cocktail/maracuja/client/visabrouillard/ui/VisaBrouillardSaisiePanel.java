/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visabrouillard.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOBrouillard;
import org.cocktail.zutil.client.ui.IZDataComponent;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDefaultDataComponentModel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;


public class VisaBrouillardSaisiePanel extends ZKarukeraPanel {
	private final IVisaBrouillardSaisiePanelMdl myCtrl;

	private final ZLabel typeBordereau;
	private final ZLabel dateCreation;
	private final ZLabel persCreation;
	private final ZTextField broLibelle;
	//private final ZTextField broPostit;
	private final ZLabel origine;
	private final ZLabel operation;
	private final ZLabel typeJournal;
	private final ZTextField broMotifRejet;

	private final BrouillardDetailListTablePanel brouillardDetailListPanel;
	private final ZPanelBalance myZPanelBalanceNew;

	private final ArrayList allEditingComponents = new ArrayList();

	public VisaBrouillardSaisiePanel(IVisaBrouillardSaisiePanelMdl ctrl) {
		super();
		myCtrl = ctrl;

		typeBordereau = new ZLabel(new ZDefaultDataComponentModel(myCtrl.getValues(), EOBrouillard.TO_BORDEREAU_KEY + "." + EOBordereau.TYPE_BORDEREAU_KEY + "." + EOTypeBordereau.TBO_LIBELLE_KEY));
		origine = new ZLabel(new ZDefaultDataComponentModel(myCtrl.getValues(), EOBrouillard.TO_ORIGINE_KEY + "." + EOOrigine.ORI_LIBELLE_KEY));
		operation = new ZLabel(new ZDefaultDataComponentModel(myCtrl.getValues(), EOBrouillard.TO_TYPE_OPERATION_KEY + "." + EOTypeOperation.TOP_LIBELLE_KEY));
		persCreation = new ZLabel(new ZDefaultDataComponentModel(myCtrl.getValues(), EOBrouillard.TO_PERSONNE_CREATION_KEY + "." + EOPersonne.NOM_PRENOM_KEY));
		dateCreation = new ZLabel(new ZDefaultDataComponentModel(myCtrl.getValues(), EOBrouillard.DATE_CREATION_KEY), ZConst.FORMAT_DATESHORT);

		broLibelle = new ZTextField(new ZTextField.DefaultTextFieldModel(myCtrl.getValues(), EOBrouillard.BRO_LIBELLE_KEY));
		broLibelle.getMyTexfield().setColumns(60);
		broMotifRejet = new ZTextField(new ZTextField.DefaultTextFieldModel(myCtrl.getValues(), EOBrouillard.BRO_MOTIF_REJET_KEY));
		broMotifRejet.getMyTexfield().setColumns(60);

		typeJournal = new ZLabel(new ZDefaultDataComponentModel(myCtrl.getValues(), EOBrouillard.TO_TYPE_JOURNAL_KEY + "." + EOTypeJournal.TJO_LIBELLE_KEY));

		allEditingComponents.add(broLibelle);
		allEditingComponents.add(broMotifRejet);

		brouillardDetailListPanel = new BrouillardDetailListTablePanel(myCtrl.brouillardDetailListMdl());
		myZPanelBalanceNew = new ZPanelBalance(myCtrl.balanceMdl());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public void initGUI() {
		brouillardDetailListPanel.initGUI();
		myZPanelBalanceNew.initGUI();
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildFormPanel(), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);

	}

	private final JPanel buildFormPanel() {
		final ArrayList list = new ArrayList();
		list.add(new Component[] {
				new JLabel("Type : "), typeBordereau
		});

		list.add(new Component[] {
				new JLabel("Créé par : "), persCreation
		});
		list.add(new Component[] {
				new JLabel("Créé le : "), dateCreation
		});
		list.add(new Component[] {
				new JLabel("Origine : "), origine
		});
		list.add(new Component[] {
				new JLabel("Opération : "), operation
		});
		list.add(new Component[] {
				new JLabel("Journal : "), typeJournal
		});
		list.add(new Component[] {
				new JLabel("Libellé : "), broLibelle
		});
		list.add(new Component[] {
				new JLabel("Motif du rejet : "), broMotifRejet
		});

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);
		p.add(encloseInPanelWithTitle("Détail du brouillard", null, ZConst.BG_COLOR_TITLE, brouillardDetailListPanel, null, null), BorderLayout.CENTER);
		p.add(myZPanelBalanceNew, BorderLayout.SOUTH);

		return p;
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myCtrl.actionValider());
		a.add(myCtrl.actionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		typeBordereau.updateData();
		origine.updateData();
		operation.updateData();
		typeJournal.updateData();
		persCreation.updateData();
		dateCreation.updateData();
		broLibelle.updateData();
		broMotifRejet.updateData();
		brouillardDetailListPanel.updateData();
		myZPanelBalanceNew.updateData();
	}

	public void updateDataExcept(Object comp) {

		Iterator iterator = allEditingComponents.iterator();
		while (iterator.hasNext()) {
			IZDataComponent object = (IZDataComponent) iterator.next();
			if (!object.equals(comp)) {
				try {
					object.updateData();
				} catch (Exception e) {

				}
			}
		}
		//myCtrl.getTauxReferenceModel().setSelectedEObject((NSKeyValueCoding) myCtrl.getValues().get(EOIm.IM_TAUX_KEY));

	}

	public interface IVisaBrouillardSaisiePanelMdl {
		public Action actionClose();

		public IZPanelBalanceProvider balanceMdl();

		public IZTablePanelMdl brouillardDetailListMdl();

		public Action actionValider();

		public Map getValues();

	}

	//	private final class MontantModel implements ZTextField.IZTextFieldModel {
	//
	//		public Object getValue() {
	//			return myCtrl.getValues().get(EOIm.IM_MONTANT_KEY);
	//		}
	//
	//		public void setValue(Object value) {
	//			if (value != null) {
	//				myCtrl.getValues().put(EOIm.IM_MONTANT_KEY, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
	//			}
	//			else {
	//				myCtrl.getValues().put(EOIm.IM_MONTANT_KEY, null);
	//			}
	//
	//		}
	//
	//	}
	public ZPanelBalance getMyZPanelBalanceNew() {
		return myZPanelBalanceNew;
	}

}
