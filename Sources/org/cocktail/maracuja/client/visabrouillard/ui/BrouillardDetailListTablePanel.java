/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visabrouillard.ui;

import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOBrouillardDetail;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


public class BrouillardDetailListTablePanel extends ZTablePanel {

	public BrouillardDetailListTablePanel(IZTablePanelMdl brouillardDetailListMdl) {
		super(brouillardDetailListMdl);

		ZEOTableModelColumn brodIndex = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.BROD_INDEX_KEY, "#", 29);
		brodIndex.setAlignment(SwingConstants.LEFT);
		brodIndex.setColumnClass(Integer.class);

		ZEOTableModelColumn brodPcoNum = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.BROD_PCO_NUM_KEY, "Compte", 68);
		brodPcoNum.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn brodPcoLibelle = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.BROD_PCO_LIBELLE_KEY, "Libellé compte", 200);
		brodPcoLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.TO_GESTION_KEY + "." + EOGestion.GES_CODE_KEY, "Code gestion", 60);
		gesCode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn brodLibelle = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.BROD_LIBELLE_KEY, "Libellé", 228);
		brodLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn brodDebit = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.BROD_DEBIT_KEY, "Débit", 90);
		brodDebit.setAlignment(SwingConstants.RIGHT);
		brodDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		brodDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn brodCredit = new ZEOTableModelColumn(myDisplayGroup, EOBrouillardDetail.BROD_CREDIT_KEY, "Crédit", 90);
		brodCredit.setAlignment(SwingConstants.RIGHT);
		brodCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		brodCredit.setColumnClass(BigDecimal.class);

		colsMap.clear();
		colsMap.put(EOBrouillardDetail.BROD_INDEX_KEY, brodIndex);
		colsMap.put(EOBrouillardDetail.BROD_PCO_NUM_KEY, brodPcoNum);
		colsMap.put(EOBrouillardDetail.BROD_PCO_LIBELLE_KEY, brodPcoLibelle);
		colsMap.put(EOBrouillardDetail.TO_GESTION_KEY + "." + EOGestion.GES_CODE_KEY, gesCode);
		colsMap.put(EOBrouillardDetail.BROD_LIBELLE_KEY, brodLibelle);
		colsMap.put(EOBrouillardDetail.BROD_DEBIT_KEY, brodDebit);
		colsMap.put(EOBrouillardDetail.BROD_CREDIT_KEY, brodCredit);

	}

	public void initGUI() {
		super.initGUI();
		//getMyEOTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		//getMyEOTable().getColumnModel().getColumn(0).setCellRenderer(myCtrl.getImSrchTableCellRenderer());

		//			((ZEOTableModelColumn) colsMap.get(TYPE_ETAT_KEY)).get    setTableCellRenderer(myCtrl.getImSrchTableCellRenderer());
	}
}
