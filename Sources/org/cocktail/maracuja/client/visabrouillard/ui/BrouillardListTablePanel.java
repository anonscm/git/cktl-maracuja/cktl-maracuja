/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visabrouillard.ui;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOBrouillard;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


public class BrouillardListTablePanel extends ZTablePanel {

	public BrouillardListTablePanel(ZTablePanel.IZTablePanelMdl listener) {
		super(listener);

		//libelle
		//origine
		//cree par
		//date creation

		final ZEOTableModelColumn broLibelle = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.BRO_LIBELLE_KEY, "Libellé", 200);
		broLibelle.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn origine = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.TO_ORIGINE_KEY + "." + EOOrigine.ORI_LIBELLE_KEY, "Origine", 200);
		origine.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn persCreation = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.TO_PERSONNE_CREATION_KEY + "." + EOPersonne.NOM_PRENOM_KEY, "Créé par", 80);
		origine.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn persVisa = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.TO_PERSONNE_VISA_KEY + "." + EOPersonne.NOM_PRENOM_KEY, "Visé par", 80);
		persVisa.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn broEtat = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.BRO_ETAT_KEY, "Etat", 80);
		broEtat.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn dateCreation = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.DATE_CREATION_KEY, "Créé le", 80);
		dateCreation.setAlignment(SwingConstants.CENTER);
		dateCreation.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		final ZEOTableModelColumn dateVisa = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.BRO_DATE_VISA_KEY, "Visé le", 80);
		dateVisa.setAlignment(SwingConstants.CENTER);
		dateVisa.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		final ZEOTableModelColumn broMotifRejet = new ZEOTableModelColumn(myDisplayGroup, EOBrouillard.BRO_MOTIF_REJET_KEY, "Motif rejet", 80);
		broMotifRejet.setAlignment(SwingConstants.LEFT);

		//
		//		final ZEOTableModelColumn montantRegle = new ZEOTableModelColumn(myDisplayGroup, MONTANT_REGLE_KEY, "Montant réglé", 80);
		//		montantRegle.setAlignment(SwingConstants.RIGHT);
		//		montantRegle.setFormatDisplay(ZConst.FORMAT_DECIMAL_COURT);
		//		montantRegle.setColumnClass(BigDecimal.class);

		colsMap.clear();
		colsMap.put(EOBrouillard.BRO_LIBELLE_KEY, broLibelle);
		colsMap.put(EOBrouillard.TO_ORIGINE_KEY, origine);
		colsMap.put(EOBrouillard.TO_PERSONNE_CREATION_KEY, persCreation);
		colsMap.put(EOBrouillard.DATE_CREATION_KEY, dateCreation);
		colsMap.put(EOBrouillard.BRO_ETAT_KEY, broEtat);
		colsMap.put(EOBrouillard.TO_PERSONNE_VISA_KEY, persVisa);
		colsMap.put(EOBrouillard.BRO_DATE_VISA_KEY, dateVisa);
		colsMap.put(EOBrouillard.BRO_MOTIF_REJET_KEY, broMotifRejet);

	}

	public void initGUI() {
		super.initGUI();
		//getMyEOTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		//getMyEOTable().getColumnModel().getColumn(0).setCellRenderer(myCtrl.getImSrchTableCellRenderer());

		//			((ZEOTableModelColumn) colsMap.get(TYPE_ETAT_KEY)).get    setTableCellRenderer(myCtrl.getImSrchTableCellRenderer());
	}
}
