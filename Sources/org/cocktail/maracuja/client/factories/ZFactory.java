/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Fichier créé le 30 juin 2003
 * par rprin
 */
package org.cocktail.maracuja.client.factories;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import org.cocktail.zutil.client.IZProgressListener;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


//import com.webobjects.foundation.NSArray;
//import com.webobjects.foundation.NSMutableArray;

/**
 * Classe abstraite pour toutes les classes Factory.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 *
 */
public abstract class ZFactory {
    protected IZFactoryListener listener;
    
    
    
    public ZFactory(final IZFactoryListener _listener) {
        listener = _listener;
    }
    
    
    
	/**
	 * Méthode récupérée du framework CRIJavaClient52.
	 * Permet de créer une instance d'un EOEnterpriseObject.
	 * @param ec
	 * @param entity
	 * @return
	 */
	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) throws ZFactoryException {
		EODistributedClassDescription description = (EODistributedClassDescription)EOClassDescription.classDescriptionForEntityName(entity);
		if(description == null) {
			throw new ZFactoryException("Impossible de récupérer la description de l'entité  \""+ entity +"\" ");
		}

		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);
		// en 5.2.1, l'objet n'est pas insere dans l'editing context par la methode
		// createInstanceWithEditingContext(). Cela peut changer en 5.2.2.
		ec.insertObject(object);
		return object;
	}


	public static EOEnterpriseObject instanceForClass(EOEditingContext ec, Class laClasse) throws ZFactoryException {
		EODistributedClassDescription description = (EODistributedClassDescription)EOClassDescription.classDescriptionForClass(laClasse);
		if(description == null) {
			throw new ZFactoryException("Impossible de récupérer la description pour la classe  \""+ laClasse +"\" ");
		}

		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);
		// en 5.2.1, l'objet n'est pas insere dans l'editing context par la methode
		// createInstanceWithEditingContext(). Cela peut changer en 5.2.2.
		ec.insertObject(object);
		return object;
	}


	/**
	 * Instancie un EOEnterpriseObject dans un nested editingContext.
	 * Attention, childEc doit bien etre un nestedEditingContext.
	 * La méthode remonte dans les editingcontext jusqu'à trouver une instance existante.
	 *
	 * @param childEc
	 * @param object
	 * @return
	 */
	public static EOEnterpriseObject nestedInstanceForObject(EOEditingContext childEc, EOEnterpriseObject object) {

		EOEditingContext parentEc = (EOEditingContext)childEc.parentObjectStore();
		//Si on est arrivé en haut de l'arborescence, ca veut dire que l'objet n'a pas été trouvé, on renvoie nul.
		if (parentEc==null) {
			return null;
		}
		EOGlobalID gid = parentEc.globalIDForObject(object);

		//Si l'objet n'est pas connu, on remonte d'un editingContext
		if (gid==null ) {
			return nestedInstanceForObject(parentEc, object);
		}
        return childEc.faultForGlobalID(gid, childEc);
	}


	/**
	 * Instancie un tableau d'EOEnterpriseObject dans un nested editingContext.
	 *
	 * @param ec
	 * @param objects
	 * @return
	 */
	public static NSArray nestedInstancesForObjects(EOEditingContext childEc, NSArray objects) {
		int n = objects.count();
		Vector localObjs = new Vector(n,0);

		for (int i = 0; i < objects.count(); i++) {
			localObjs.add(  nestedInstanceForObject(childEc, (EOEnterpriseObject) objects.objectAtIndex(i)));
		}
		return new NSArray(localObjs.toArray());
	}

	/**
	 * Instancie un tableau d'EOEnterpriseObjects dans un editingContext particulier. (utile pour instancier des objets dans un nested editingcontext).
	 * @param ec
	 * @param objects
	 * @return
	 */
	public static Vector nestedInstancesForObjects(EOEditingContext childEc, Vector objects) {
		int n = objects.size();
		Vector localObjs = new Vector(n,0);
		Iterator iter = objects.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject rec = (EOEnterpriseObject) iter.next();
			EOEnterpriseObject tmp = nestedInstanceForObject(childEc, rec);
			localObjs.add( tmp );
		}
		return localObjs;
	}
	public static ArrayList nestedInstancesForObjects(EOEditingContext childEc, ArrayList objects) {
	    int n = objects.size();
        ArrayList localObjs = new ArrayList(n);
	    Iterator iter = objects.iterator();
	    while (iter.hasNext()) {
	        EOEnterpriseObject rec = (EOEnterpriseObject) iter.next();
	        EOEnterpriseObject tmp = nestedInstanceForObject(childEc, rec);
	        localObjs.add( tmp );
	    }
	    return localObjs;
	}


	/**
	 *  Permet de récupérer les instances d'objets d'un editongcontext dans un autre.
	 * Les objets doivent deja exister dans l'editingcontext. Sinon il faut efefctuer un fault.
	 *
	 * @param aEc
	 * @param objects
	 * @return
	 */
	public static Vector localInstancesForObjects(EOEditingContext aEc, Vector objects) {
		int n = objects.size();
		Vector localObjs = new Vector(n,0);
		Iterator iter = objects.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject rec = (EOEnterpriseObject) iter.next();
			EOEditingContext ec = rec.editingContext();
			EOEnterpriseObject tmp = aEc.objectForGlobalID(ec.globalIDForObject(rec) );
			localObjs.add( tmp );
		}
		return localObjs;
	}
	public static ArrayList localInstancesForObjects(EOEditingContext aEc, ArrayList objects) {
	    int n = objects.size();
        ArrayList localObjs = new ArrayList(n);
	    Iterator iter = objects.iterator();
	    while (iter.hasNext()) {
	        EOEnterpriseObject rec = (EOEnterpriseObject) iter.next();
	        EOEditingContext ec = rec.editingContext();
	        EOEnterpriseObject tmp = aEc.objectForGlobalID(ec.globalIDForObject(rec) );
	        localObjs.add( tmp );
	    }
	    return localObjs;
	}


	/**
	 * Sert uniquement à balayer le tableau, mettre les éléments dans un autre tableau.
	 * Apparemment ca permet à EOF de réellement instancier les objets dans un editingContext.
	 * @param aEc
	 * @param array
	 * @return
	 */
	public static NSArray enumererEObjets(NSArray array) {
	    EOEnterpriseObject[] objs = new EOEnterpriseObject[array.count()];
	    for (int i = 0; i < array.count(); i++) {
            objs[i] = (EOEnterpriseObject) array.objectAtIndex(i);
        }
	    return new NSArray(objs);
	}

	/**
	 * @return Un gregorianCalendar initialisé à la date du jour (sans les heures/minutes/etc).
	 * Utilisez getToday().getTime() pour recupérer la date du jour nettoyée des secondes sous forme de Date.
	 */
    private static final GregorianCalendar getToday() {
        final GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
        return gc;
    }



    /**
     * @return
     */
    public static final NSTimestamp getDateJour() {
        return new NSTimestamp(getToday().getTime());
    }

    
   
    /** A appeler par les descendants de ZFActory pour notifier les controileurs qui utilisent la factopry de l'évolution d'un traitement
     * @param max nombre d'étapes max (-1) si non défini 
     * */

protected void notifyProcessBegin(final int max) {
       if (listener != null) {
           listener.onProcessBegin(max); 
       }
   }
    
   /** A appeler par les descendants de ZFActory pour notifier les controileurs qui utilisent la factopry de l'évolution d'un traitement */
   protected void notifyProcessEnd() {
       if (listener != null) {
           listener.onProcessEnd(); 
       }
   }
   
   /** A appeler par les descendants de ZFActory pour notifier les controileurs qui utilisent la factopry de l'évolution d'un traitement 
    * 
    * @param max nombre d'étapes max (-1) si non défini 
    * */
   
   protected void notifyProcessStep(final int i, final int max) {
       if (listener != null) {
           listener.onProcessStep(i,max); 
       }
   }

   /**
    * Permets aux controleurs qui utilisent la factory de se tenir informer de l'évolution d'un traitement
    */
    public interface IZFactoryListener extends IZProgressListener {
        
    }
    
    

}
