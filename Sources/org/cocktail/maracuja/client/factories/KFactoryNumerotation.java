/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factories;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOReimputation;
import org.cocktail.maracuja.client.metier.EORetenue;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Classe servant a  effectuer la numerotation des differents objets.
 */
public class KFactoryNumerotation extends FactoryNumerotation {

	/**
	 * @param withLog
	 */
	public KFactoryNumerotation(boolean withLog) {
		super(withLog);
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOOrdreDePaiement(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.server.metier.EOOrdreDePaiement)
	 */
	public void getNumeroEOOrdreDePaiement(EOEditingContext ec, EOOrdreDePaiement monOrdreDePaiement) {
		ServerProxy.clientSideRequestNumerotation(ec, monOrdreDePaiement, EOOrdreDePaiement.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monOrdreDePaiement)
		}));
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOEcriture(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.client.metier.EOEcriture)
	 */
	public void getNumeroEOEcriture(EOEditingContext ec, EOEcriture monEcriture) {
		ServerProxy.clientSideRequestNumerotation(ec, monEcriture, EOEcriture.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monEcriture)
		}));
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOEcritureBrouillard(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.client.metier.EOEcriture)
	 */
	public void getNumeroEOEcritureBrouillard(EOEditingContext ec, EOEcriture monEcritureBrouillard) {
		ServerProxy.clientSideRequestNumerotation(ec, monEcritureBrouillard, "EOEcritureBrouillard");
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monEcritureBrouillard)
		}));
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOBordereauRejet(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.client.metier.EOBordereauRejet)
	 */
	public void getNumeroEOBordereauRejet(EOEditingContext ec, EOBordereauRejet monBordereauRejet) {
		ServerProxy.clientSideRequestNumerotation(ec, monBordereauRejet, EOBordereauRejet.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monBordereauRejet)
		}));
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOEmargement(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.client.metier.EOEmargement)
	 */
	public void getNumeroEOEmargement(EOEditingContext ec, EOEmargement monEmargement) {
		ServerProxy.clientSideRequestNumerotation(ec, monEmargement, EOEmargement.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monEmargement)
		}));
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOPaiement(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.client.metier.EOVirement)
	 */
	public void getNumeroEOPaiement(EOEditingContext ec, EOPaiement monPaiement) {
		ServerProxy.clientSideRequestNumerotation(ec, monPaiement, EOPaiement.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monPaiement)
		}));
	}

	/**
	 * @see org.cocktail.maracuja.client.factory.InterfaceFactoryNumerotation#getNumeroEOReimputation(com.webobjects.eocontrol.EOEditingContext,
	 *      org.cocktail.maracuja.client.metier.EOReimputation)
	 */
	public void getNumeroEOReimputation(EOEditingContext ec, EOReimputation monEOReimputation) {
		ServerProxy.clientSideRequestNumerotation(ec, monEOReimputation, EOReimputation.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monEOReimputation)
		}));
	}

	public void getNumeroEOBordereauCheque(EOEditingContext ec, EOBordereau bordereau) {
		ServerProxy.clientSideRequestNumerotation(ec, bordereau, EOBordereau.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(bordereau)
		}));
	}

	public void getNumeroEORecouvrement(EOEditingContext ec, EORecouvrement monRecouvrement) {
		ServerProxy.clientSideRequestNumerotation(ec, monRecouvrement, EORecouvrement.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(monRecouvrement)
		}));
	}

	public void getNumeroEORetenue(EOEditingContext ec, EORetenue retenue) {
		ServerProxy.clientSideRequestNumerotation(ec, retenue, EORetenue.ENTITY_NAME);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
				ec.globalIDForObject(retenue)
		}));
	}

}
