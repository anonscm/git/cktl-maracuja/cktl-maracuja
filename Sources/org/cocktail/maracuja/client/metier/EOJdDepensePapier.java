/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
package org.cocktail.maracuja.client.metier;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;


public class EOJdDepensePapier extends _EOJdDepensePapier {

	public static final String DISPLAY_STRING_KEY = "displayString";

	public EOJdDepensePapier() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static final NSArray fetchAllForMandat(EOEditingContext ec, EOMandat mandat, NSArray sortOrderings, boolean refresh) {
		if (mandat == null) {
			return NSArray.EmptyArray;
		}
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EODepenseDepensePapier.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, mandat));
		NSArray res = EODepenseDepensePapier.fetchAll(ec, new EOAndQualifier(quals), sortOrderings, refresh, true);

		if (res.count() == 0) {
			return NSArray.EmptyArray;
		}
		NSArray depensesPapier = (NSArray) res.valueForKeyPath(EODepenseDepensePapier.JD_DEPENSE_PAPIER_KEY);
		if (depensesPapier.count() == 0) {
			return NSArray.EmptyArray;
		}
		return depensesPapier;
	}

	public static final NSArray fetchAllForDepense(EOEditingContext ec, EODepense depense, NSArray sortOrderings, boolean refresh) {
		if (depense == null) {
			return NSArray.EmptyArray;
		}
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EODepenseDepensePapier.DEPENSE_KEY, EOQualifier.QualifierOperatorEqual, depense));
		NSArray res = EODepenseDepensePapier.fetchAll(ec, new EOAndQualifier(quals), sortOrderings, refresh, true);

		if (res.count() == 0) {
			return NSArray.EmptyArray;
		}
		NSArray depensesPapier = (NSArray) res.valueForKeyPath(EODepenseDepensePapier.JD_DEPENSE_PAPIER_KEY);
		if (depensesPapier.count() == 0) {
			return NSArray.EmptyArray;
		}
		return depensesPapier;
	}

	/**
	 * @return Les mandats associés
	 */
	public NSArray getMandats() {
		NSMutableArray res = new NSMutableArray();
		NSArray ddpps = depenseDepensePapiers();
		for (int i = 0; i < ddpps.count(); i++) {
			EODepenseDepensePapier ddpp = (EODepenseDepensePapier) ddpps.objectAtIndex(i);
			if (res.indexOfObject(ddpp.mandat()) == NSArray.NotFound) {
				res.addObject(ddpp.mandat());
			}
		}
		return res.immutableClone();

	}

	public String displayString() {
		return dppNumeroFacture() + " (" + ZConst.FORMAT_DATESHORT.format(dppDateFacture()) + ")";
	}

	public NSArray imSuspensionsValides() {
		NSArray res = super.imSuspensions();
		return EOQualifier.filteredArrayWithQualifier(res, EOImSuspension.QUAL_VALIDE);
	}

}
