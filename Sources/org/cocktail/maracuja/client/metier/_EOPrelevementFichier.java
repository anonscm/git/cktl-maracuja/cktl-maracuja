// _EOPrelevementFichier.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrelevementFichier.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrelevementFichier extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PrelevementFichier";
	public static final String ENTITY_TABLE_NAME = "maracuja.PRELEVEMENT_FICHIER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ficpOrdre";

	public static final String FICP_CODE_OP_KEY = "ficpCodeOp";
	public static final String FICP_COMPTE_KEY = "ficpCompte";
	public static final String FICP_CONTENU_KEY = "ficpContenu";
	public static final String FICP_DATE_CREATION_KEY = "ficpDateCreation";
	public static final String FICP_DATE_MODIF_KEY = "ficpDateModif";
	public static final String FICP_DATE_REGLEMENT_KEY = "ficpDateReglement";
	public static final String FICP_DATE_REMISE_KEY = "ficpDateRemise";
	public static final String FICP_LIBELLE_KEY = "ficpLibelle";
	public static final String FICP_MONTANT_KEY = "ficpMontant";
	public static final String FICP_NB_PRELEVEMENTS_KEY = "ficpNbPrelevements";
	public static final String FICP_NUMERO_KEY = "ficpNumero";

// Attributs non visibles
	public static final String FICP_ORDRE_KEY = "ficpOrdre";
	public static final String RECO_ORDRE_KEY = "recoOrdre";
	public static final String TREC_ORDRE_KEY = "trecOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String FICP_CODE_OP_COLKEY = "ficp_code_op";
	public static final String FICP_COMPTE_COLKEY = "FICP_COMPTE";
	public static final String FICP_CONTENU_COLKEY = "ficp_contenu";
	public static final String FICP_DATE_CREATION_COLKEY = "ficp_DATE_CREATION";
	public static final String FICP_DATE_MODIF_COLKEY = "ficp_DATE_MODIF";
	public static final String FICP_DATE_REGLEMENT_COLKEY = "ficp_DATE_reglement";
	public static final String FICP_DATE_REMISE_COLKEY = "ficp_DATE_REMISE";
	public static final String FICP_LIBELLE_COLKEY = "ficp_LIBELLE";
	public static final String FICP_MONTANT_COLKEY = "ficp_montant";
	public static final String FICP_NB_PRELEVEMENTS_COLKEY = "ficp_nb_prel";
	public static final String FICP_NUMERO_COLKEY = "ficp_NUMERO";

	public static final String FICP_ORDRE_COLKEY = "FICP_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "reco_ordre";
	public static final String TREC_ORDRE_COLKEY = "trec_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String RECOUVREMENT_KEY = "recouvrement";
	public static final String TYPE_RECOUVREMENT_KEY = "typeRecouvrement";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String ficpCodeOp() {
    return (String) storedValueForKey(FICP_CODE_OP_KEY);
  }

  public void setFicpCodeOp(String value) {
    takeStoredValueForKey(value, FICP_CODE_OP_KEY);
  }

  public String ficpCompte() {
    return (String) storedValueForKey(FICP_COMPTE_KEY);
  }

  public void setFicpCompte(String value) {
    takeStoredValueForKey(value, FICP_COMPTE_KEY);
  }

  public String ficpContenu() {
    return (String) storedValueForKey(FICP_CONTENU_KEY);
  }

  public void setFicpContenu(String value) {
    takeStoredValueForKey(value, FICP_CONTENU_KEY);
  }

  public NSTimestamp ficpDateCreation() {
    return (NSTimestamp) storedValueForKey(FICP_DATE_CREATION_KEY);
  }

  public void setFicpDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, FICP_DATE_CREATION_KEY);
  }

  public NSTimestamp ficpDateModif() {
    return (NSTimestamp) storedValueForKey(FICP_DATE_MODIF_KEY);
  }

  public void setFicpDateModif(NSTimestamp value) {
    takeStoredValueForKey(value, FICP_DATE_MODIF_KEY);
  }

  public NSTimestamp ficpDateReglement() {
    return (NSTimestamp) storedValueForKey(FICP_DATE_REGLEMENT_KEY);
  }

  public void setFicpDateReglement(NSTimestamp value) {
    takeStoredValueForKey(value, FICP_DATE_REGLEMENT_KEY);
  }

  public NSTimestamp ficpDateRemise() {
    return (NSTimestamp) storedValueForKey(FICP_DATE_REMISE_KEY);
  }

  public void setFicpDateRemise(NSTimestamp value) {
    takeStoredValueForKey(value, FICP_DATE_REMISE_KEY);
  }

  public String ficpLibelle() {
    return (String) storedValueForKey(FICP_LIBELLE_KEY);
  }

  public void setFicpLibelle(String value) {
    takeStoredValueForKey(value, FICP_LIBELLE_KEY);
  }

  public java.math.BigDecimal ficpMontant() {
    return (java.math.BigDecimal) storedValueForKey(FICP_MONTANT_KEY);
  }

  public void setFicpMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FICP_MONTANT_KEY);
  }

  public Integer ficpNbPrelevements() {
    return (Integer) storedValueForKey(FICP_NB_PRELEVEMENTS_KEY);
  }

  public void setFicpNbPrelevements(Integer value) {
    takeStoredValueForKey(value, FICP_NB_PRELEVEMENTS_KEY);
  }

  public Integer ficpNumero() {
    return (Integer) storedValueForKey(FICP_NUMERO_KEY);
  }

  public void setFicpNumero(Integer value) {
    takeStoredValueForKey(value, FICP_NUMERO_KEY);
  }

  public org.cocktail.maracuja.client.metier.EORecouvrement recouvrement() {
    return (org.cocktail.maracuja.client.metier.EORecouvrement)storedValueForKey(RECOUVREMENT_KEY);
  }

  public void setRecouvrementRelationship(org.cocktail.maracuja.client.metier.EORecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EORecouvrement oldValue = recouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeRecouvrement typeRecouvrement() {
    return (org.cocktail.maracuja.client.metier.EOTypeRecouvrement)storedValueForKey(TYPE_RECOUVREMENT_KEY);
  }

  public void setTypeRecouvrementRelationship(org.cocktail.maracuja.client.metier.EOTypeRecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeRecouvrement oldValue = typeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOPrelevementFichier createPrelevementFichier(EOEditingContext editingContext, String ficpCodeOp
, NSTimestamp ficpDateCreation
, NSTimestamp ficpDateModif
, NSTimestamp ficpDateReglement
, NSTimestamp ficpDateRemise
, String ficpLibelle
, java.math.BigDecimal ficpMontant
, Integer ficpNbPrelevements
, Integer ficpNumero
, org.cocktail.maracuja.client.metier.EORecouvrement recouvrement, org.cocktail.maracuja.client.metier.EOTypeRecouvrement typeRecouvrement, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOPrelevementFichier eo = (EOPrelevementFichier) createAndInsertInstance(editingContext, _EOPrelevementFichier.ENTITY_NAME);    
		eo.setFicpCodeOp(ficpCodeOp);
		eo.setFicpDateCreation(ficpDateCreation);
		eo.setFicpDateModif(ficpDateModif);
		eo.setFicpDateReglement(ficpDateReglement);
		eo.setFicpDateRemise(ficpDateRemise);
		eo.setFicpLibelle(ficpLibelle);
		eo.setFicpMontant(ficpMontant);
		eo.setFicpNbPrelevements(ficpNbPrelevements);
		eo.setFicpNumero(ficpNumero);
    eo.setRecouvrementRelationship(recouvrement);
    eo.setTypeRecouvrementRelationship(typeRecouvrement);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPrelevementFichier.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPrelevementFichier.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPrelevementFichier creerInstance(EOEditingContext editingContext) {
		  		EOPrelevementFichier object = (EOPrelevementFichier)createAndInsertInstance(editingContext, _EOPrelevementFichier.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOPrelevementFichier localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrelevementFichier)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPrelevementFichier localInstanceIn(EOEditingContext editingContext, EOPrelevementFichier eo) {
    EOPrelevementFichier localInstance = (eo == null) ? null : (EOPrelevementFichier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPrelevementFichier#localInstanceIn a la place.
   */
	public static EOPrelevementFichier localInstanceOf(EOEditingContext editingContext, EOPrelevementFichier eo) {
		return EOPrelevementFichier.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPrelevementFichier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPrelevementFichier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrelevementFichier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrelevementFichier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrelevementFichier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrelevementFichier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrelevementFichier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrelevementFichier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPrelevementFichier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrelevementFichier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrelevementFichier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrelevementFichier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
