/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOPaiement.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPaiement extends _EOPaiement {

	public static final EOSortOrdering SORT_PAI_DATE_DESC = EOSortOrdering.sortOrderingWithKey(EOPaiement.PAI_DATE_CREATION_KEY, EOSortOrdering.CompareDescending);

	public static final String PAI_ORDRE_KEY = "paiOrdre";

	public static final String MONTANT_RETENUES_KEY = "montantRetenues";

	public EOPaiement() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return Le montant des retenues (somme des montants des retenues des dépenses)
	 */
	public final BigDecimal montantRetenues() {
		return ZEOUtilities.calcSommeOfBigDecimals(mandats(), EOMandat.MONTANT_RETENUES_KEY);
	}

	public NSArray getDepenses() {
		NSMutableArray depenses = new NSMutableArray();
		NSArray mandats = mandats();
		for (int i = 0; i < mandats.count(); i++) {
			EOMandat mandat = (EOMandat) mandats.objectAtIndex(i);
			depenses.addObjectsFromArray(mandat.depenses());
		}
		return depenses.immutableClone();
	}

	public NSArray getIms() {
		NSMutableArray ims = new NSMutableArray();
		NSArray depenses = getDepenses();
		for (int i = 0; i < depenses.count(); i++) {
			EODepense depense = (EODepense) depenses.objectAtIndex(i);
			ims.addObjectsFromArray(EOIm.fetchAllForDepense(editingContext(), depense, null, false));
		}
		return ims.immutableClone();
	}

	/**
	 * @return Les ecritures details generes par un paiement.
	 */
	public NSArray getEcritureDetails() {
		NSMutableArray res = new NSMutableArray();

		NSArray tmp = getMandatDetailEcritures();
		for (int i = 0; i < tmp.count(); i++) {
			EOMandatDetailEcriture mde = (EOMandatDetailEcriture) tmp.objectAtIndex(i);
			if (EOMandatDetailEcriture.MDE_ORIGINE_VIREMENT.equals(mde.mdeOrigine()) || EOMandatDetailEcriture.MDE_ORIGINE_VIREMENT_RETENUE.equals(mde.mdeOrigine())) {
				res.addObject(mde.ecritureDetail());
			}
		}
		NSArray tmp2 = getOdpDetailEcritures();
		for (int i = 0; i < tmp2.count(); i++) {
			EOOrdrePaiementDetailEcriture mde = (EOOrdrePaiementDetailEcriture) tmp2.objectAtIndex(i);
			if (EOOrdrePaiementDetailEcriture.ORIGINE_VIREMENT.equals(mde.opeOrigine())) {
				res.addObject(mde.ecritureDetail());
			}
		}

		return res.immutableClone();
	}

	public NSArray getMandatDetailEcritures() {
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < mandats().count(); i++) {
			EOMandat man = (EOMandat) mandats().objectAtIndex(i);
			res.addObjectsFromArray(man.mandatDetailEcritures());
		}
		return res.immutableClone();
	}

	public NSArray getOdpDetailEcritures() {
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < ordreDePaiements().count(); i++) {
			EOOrdreDePaiement man = (EOOrdreDePaiement) ordreDePaiements().objectAtIndex(i);
			res.addObjectsFromArray(man.ordrePaiementDetailEcritures());
		}
		return res.immutableClone();
		//		return (NSArray) ordreDePaiements().valueForKey(EOOrdreDePaiement.ORDRE_PAIEMENT_DETAIL_ECRITURES_KEY);
	}

}
