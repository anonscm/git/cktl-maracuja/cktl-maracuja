// _EOPrelevementParamBdf.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrelevementParamBdf.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrelevementParamBdf extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PrelevementParamBdf";
	public static final String ENTITY_TABLE_NAME = "maracuja.prelevement_Param_Bdf";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ppbOrdre";

	public static final String PPB_C3_KEY = "ppbC3";
	public static final String PPB_C41_KEY = "ppbC41";
	public static final String PPB_C42_KEY = "ppbC42";
	public static final String PPB_C5_KEY = "ppbC5";
	public static final String PPB_C6_KEY = "ppbC6";
	public static final String PPB_COMPTE_TPG_KEY = "ppbCompteTpg";
	public static final String PPB_ETAT_KEY = "ppbEtat";
	public static final String PPB_NOM_REMETTANT_KEY = "ppbNomRemettant";
	public static final String PPB_NOM_TPG_KEY = "ppbNomTpg";

// Attributs non visibles
	public static final String PPB_ORDRE_KEY = "ppbOrdre";
	public static final String TREC_ORDRE_KEY = "trecOrdre";

//Colonnes dans la base de donnees
	public static final String PPB_C3_COLKEY = "ppb_C3";
	public static final String PPB_C41_COLKEY = "ppb_C41";
	public static final String PPB_C42_COLKEY = "ppb_C42";
	public static final String PPB_C5_COLKEY = "ppb_C5";
	public static final String PPB_C6_COLKEY = "ppb_C6";
	public static final String PPB_COMPTE_TPG_COLKEY = "ppb_compte_tpg";
	public static final String PPB_ETAT_COLKEY = "ppb_Etat";
	public static final String PPB_NOM_REMETTANT_COLKEY = "PPB_NOM_REMETTANT";
	public static final String PPB_NOM_TPG_COLKEY = "PPB_NOM_TPG";

	public static final String PPB_ORDRE_COLKEY = "ppb_Ordre";
	public static final String TREC_ORDRE_COLKEY = "trec_Ordre";


	// Relationships
	public static final String TYPE_RECOUVREMENT_KEY = "typeRecouvrement";



	// Accessors methods
  public String ppbC3() {
    return (String) storedValueForKey(PPB_C3_KEY);
  }

  public void setPpbC3(String value) {
    takeStoredValueForKey(value, PPB_C3_KEY);
  }

  public String ppbC41() {
    return (String) storedValueForKey(PPB_C41_KEY);
  }

  public void setPpbC41(String value) {
    takeStoredValueForKey(value, PPB_C41_KEY);
  }

  public String ppbC42() {
    return (String) storedValueForKey(PPB_C42_KEY);
  }

  public void setPpbC42(String value) {
    takeStoredValueForKey(value, PPB_C42_KEY);
  }

  public String ppbC5() {
    return (String) storedValueForKey(PPB_C5_KEY);
  }

  public void setPpbC5(String value) {
    takeStoredValueForKey(value, PPB_C5_KEY);
  }

  public String ppbC6() {
    return (String) storedValueForKey(PPB_C6_KEY);
  }

  public void setPpbC6(String value) {
    takeStoredValueForKey(value, PPB_C6_KEY);
  }

  public String ppbCompteTpg() {
    return (String) storedValueForKey(PPB_COMPTE_TPG_KEY);
  }

  public void setPpbCompteTpg(String value) {
    takeStoredValueForKey(value, PPB_COMPTE_TPG_KEY);
  }

  public String ppbEtat() {
    return (String) storedValueForKey(PPB_ETAT_KEY);
  }

  public void setPpbEtat(String value) {
    takeStoredValueForKey(value, PPB_ETAT_KEY);
  }

  public String ppbNomRemettant() {
    return (String) storedValueForKey(PPB_NOM_REMETTANT_KEY);
  }

  public void setPpbNomRemettant(String value) {
    takeStoredValueForKey(value, PPB_NOM_REMETTANT_KEY);
  }

  public String ppbNomTpg() {
    return (String) storedValueForKey(PPB_NOM_TPG_KEY);
  }

  public void setPpbNomTpg(String value) {
    takeStoredValueForKey(value, PPB_NOM_TPG_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOTypeRecouvrement typeRecouvrement() {
    return (org.cocktail.maracuja.client.metier.EOTypeRecouvrement)storedValueForKey(TYPE_RECOUVREMENT_KEY);
  }

  public void setTypeRecouvrementRelationship(org.cocktail.maracuja.client.metier.EOTypeRecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeRecouvrement oldValue = typeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RECOUVREMENT_KEY);
    }
  }
  

  public static EOPrelevementParamBdf createPrelevementParamBdf(EOEditingContext editingContext, String ppbC3
, String ppbC41
, String ppbC42
, String ppbC5
, String ppbC6
, String ppbCompteTpg
, String ppbEtat
, org.cocktail.maracuja.client.metier.EOTypeRecouvrement typeRecouvrement) {
    EOPrelevementParamBdf eo = (EOPrelevementParamBdf) createAndInsertInstance(editingContext, _EOPrelevementParamBdf.ENTITY_NAME);    
		eo.setPpbC3(ppbC3);
		eo.setPpbC41(ppbC41);
		eo.setPpbC42(ppbC42);
		eo.setPpbC5(ppbC5);
		eo.setPpbC6(ppbC6);
		eo.setPpbCompteTpg(ppbCompteTpg);
		eo.setPpbEtat(ppbEtat);
    eo.setTypeRecouvrementRelationship(typeRecouvrement);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPrelevementParamBdf.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPrelevementParamBdf.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPrelevementParamBdf creerInstance(EOEditingContext editingContext) {
		  		EOPrelevementParamBdf object = (EOPrelevementParamBdf)createAndInsertInstance(editingContext, _EOPrelevementParamBdf.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOPrelevementParamBdf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrelevementParamBdf)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPrelevementParamBdf localInstanceIn(EOEditingContext editingContext, EOPrelevementParamBdf eo) {
    EOPrelevementParamBdf localInstance = (eo == null) ? null : (EOPrelevementParamBdf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPrelevementParamBdf#localInstanceIn a la place.
   */
	public static EOPrelevementParamBdf localInstanceOf(EOEditingContext editingContext, EOPrelevementParamBdf eo) {
		return EOPrelevementParamBdf.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPrelevementParamBdf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPrelevementParamBdf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrelevementParamBdf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrelevementParamBdf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrelevementParamBdf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrelevementParamBdf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrelevementParamBdf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrelevementParamBdf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPrelevementParamBdf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrelevementParamBdf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrelevementParamBdf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrelevementParamBdf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
