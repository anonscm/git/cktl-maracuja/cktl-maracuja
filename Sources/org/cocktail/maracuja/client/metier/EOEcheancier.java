/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOEcheancier.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.common.echeancier.entities.IEcheancier;

import com.webobjects.foundation.NSValidation;

public class EOEcheancier extends _EOEcheancier implements IEcheancier {
	public static final String BORDEREAU_ASSOCIE_KEY = "bordereauAssocie";
	public static final String ETAT_PRELEVEMENT_VALIDE = "V";

	public EOEcheancier() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return Le bordereau associe a l'echeancier. (soit via la relation bordereau, soit en passant par la recette associee).
	 */
	public EOBordereau bordereauAssocie() {
		if (bordereau() != null) {
			return bordereau();
		}
		else if (recette() != null) {
			return recette().titre().bordereau();
		}
		return null;
	}

	public boolean hasTitreAssocie() {
		return (recette() != null);
	}



	public BigDecimal montantPreleve() {
		BigDecimal res = BigDecimal.valueOf(0.00);
		for (int i = 0; i < prelevements().count(); i++) {
			EOPrelevement prelev = (EOPrelevement) prelevements().objectAtIndex(i);
			if (EOPrelevement.ETAT_PRELEVE.equals(prelev.prelevEtatMaracuja()) || EOPrelevement.ETAT_CONFIRME.equals(prelev.prelevEtatMaracuja())) {
				res = res.add(prelev.prelevMontant());
			}
		}
		return res;
	}

	public BigDecimal montantAPrelever() {
		BigDecimal res = BigDecimal.valueOf(0.00);
		for (int i = 0; i < prelevements().count(); i++) {
			EOPrelevement prelev = (EOPrelevement) prelevements().objectAtIndex(i);
			if (EOPrelevement.ETAT_ATTENTE.equals(prelev.prelevEtatMaracuja())) {
				res = res.add(prelev.prelevMontant());
			}
		}
		return res;
	}

}
