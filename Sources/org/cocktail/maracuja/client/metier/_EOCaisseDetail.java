// _EOCaisseDetail.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCaisseDetail.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCaisseDetail extends  EOGenericRecord {
	public static final String ENTITY_NAME = "CaisseDetail";
	public static final String ENTITY_TABLE_NAME = "maracuja.caisse_detail";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cadOrdre";

	public static final String CAD_BEURO10_KEY = "cadBeuro10";
	public static final String CAD_BEURO100_KEY = "cadBeuro100";
	public static final String CAD_BEURO20_KEY = "cadBeuro20";
	public static final String CAD_BEURO200_KEY = "cadBeuro200";
	public static final String CAD_BEURO5_KEY = "cadBeuro5";
	public static final String CAD_BEURO50_KEY = "cadBeuro50";
	public static final String CAD_BEURO500_KEY = "cadBeuro500";
	public static final String CAD_DATE_KEY = "cadDate";
	public static final String CAD_PCENT1_KEY = "cadPcent1";
	public static final String CAD_PCENT10_KEY = "cadPcent10";
	public static final String CAD_PCENT2_KEY = "cadPcent2";
	public static final String CAD_PCENT20_KEY = "cadPcent20";
	public static final String CAD_PCENT5_KEY = "cadPcent5";
	public static final String CAD_PCENT50_KEY = "cadPcent50";
	public static final String CAD_PEURO1_KEY = "cadPeuro1";
	public static final String CAD_PEURO2_KEY = "cadPeuro2";

// Attributs non visibles
	public static final String CAD_ORDRE_KEY = "cadOrdre";
	public static final String CBA_ORDRE_KEY = "cbaOrdre";

//Colonnes dans la base de donnees
	public static final String CAD_BEURO10_COLKEY = "cad_Beuro10";
	public static final String CAD_BEURO100_COLKEY = "cad_Beuro100";
	public static final String CAD_BEURO20_COLKEY = "cad_Beuro20";
	public static final String CAD_BEURO200_COLKEY = "cad_Beuro200";
	public static final String CAD_BEURO5_COLKEY = "cad_Beuro5";
	public static final String CAD_BEURO50_COLKEY = "cad_Beuro50";
	public static final String CAD_BEURO500_COLKEY = "cad_Beuro500";
	public static final String CAD_DATE_COLKEY = "cad_date";
	public static final String CAD_PCENT1_COLKEY = "cad_Pcent1";
	public static final String CAD_PCENT10_COLKEY = "cad_Pcent10";
	public static final String CAD_PCENT2_COLKEY = "cad_Pcent2";
	public static final String CAD_PCENT20_COLKEY = "cad_Pcent20";
	public static final String CAD_PCENT5_COLKEY = "cad_Pcent5";
	public static final String CAD_PCENT50_COLKEY = "cad_Pcent50";
	public static final String CAD_PEURO1_COLKEY = "cad_Peuro1";
	public static final String CAD_PEURO2_COLKEY = "cad_Peuro2";

	public static final String CAD_ORDRE_COLKEY = "cad_ordre";
	public static final String CBA_ORDRE_COLKEY = "cba_Ordre";


	// Relationships
	public static final String COMPTE_BANCAIRE_KEY = "compteBancaire";



	// Accessors methods
  public Integer cadBeuro10() {
    return (Integer) storedValueForKey(CAD_BEURO10_KEY);
  }

  public void setCadBeuro10(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO10_KEY);
  }

  public Integer cadBeuro100() {
    return (Integer) storedValueForKey(CAD_BEURO100_KEY);
  }

  public void setCadBeuro100(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO100_KEY);
  }

  public Integer cadBeuro20() {
    return (Integer) storedValueForKey(CAD_BEURO20_KEY);
  }

  public void setCadBeuro20(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO20_KEY);
  }

  public Integer cadBeuro200() {
    return (Integer) storedValueForKey(CAD_BEURO200_KEY);
  }

  public void setCadBeuro200(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO200_KEY);
  }

  public Integer cadBeuro5() {
    return (Integer) storedValueForKey(CAD_BEURO5_KEY);
  }

  public void setCadBeuro5(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO5_KEY);
  }

  public Integer cadBeuro50() {
    return (Integer) storedValueForKey(CAD_BEURO50_KEY);
  }

  public void setCadBeuro50(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO50_KEY);
  }

  public Integer cadBeuro500() {
    return (Integer) storedValueForKey(CAD_BEURO500_KEY);
  }

  public void setCadBeuro500(Integer value) {
    takeStoredValueForKey(value, CAD_BEURO500_KEY);
  }

  public NSTimestamp cadDate() {
    return (NSTimestamp) storedValueForKey(CAD_DATE_KEY);
  }

  public void setCadDate(NSTimestamp value) {
    takeStoredValueForKey(value, CAD_DATE_KEY);
  }

  public Integer cadPcent1() {
    return (Integer) storedValueForKey(CAD_PCENT1_KEY);
  }

  public void setCadPcent1(Integer value) {
    takeStoredValueForKey(value, CAD_PCENT1_KEY);
  }

  public Integer cadPcent10() {
    return (Integer) storedValueForKey(CAD_PCENT10_KEY);
  }

  public void setCadPcent10(Integer value) {
    takeStoredValueForKey(value, CAD_PCENT10_KEY);
  }

  public Integer cadPcent2() {
    return (Integer) storedValueForKey(CAD_PCENT2_KEY);
  }

  public void setCadPcent2(Integer value) {
    takeStoredValueForKey(value, CAD_PCENT2_KEY);
  }

  public Integer cadPcent20() {
    return (Integer) storedValueForKey(CAD_PCENT20_KEY);
  }

  public void setCadPcent20(Integer value) {
    takeStoredValueForKey(value, CAD_PCENT20_KEY);
  }

  public Integer cadPcent5() {
    return (Integer) storedValueForKey(CAD_PCENT5_KEY);
  }

  public void setCadPcent5(Integer value) {
    takeStoredValueForKey(value, CAD_PCENT5_KEY);
  }

  public Integer cadPcent50() {
    return (Integer) storedValueForKey(CAD_PCENT50_KEY);
  }

  public void setCadPcent50(Integer value) {
    takeStoredValueForKey(value, CAD_PCENT50_KEY);
  }

  public Integer cadPeuro1() {
    return (Integer) storedValueForKey(CAD_PEURO1_KEY);
  }

  public void setCadPeuro1(Integer value) {
    takeStoredValueForKey(value, CAD_PEURO1_KEY);
  }

  public Integer cadPeuro2() {
    return (Integer) storedValueForKey(CAD_PEURO2_KEY);
  }

  public void setCadPeuro2(Integer value) {
    takeStoredValueForKey(value, CAD_PEURO2_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOCompteBancaire compteBancaire() {
    return (org.cocktail.maracuja.client.metier.EOCompteBancaire)storedValueForKey(COMPTE_BANCAIRE_KEY);
  }

  public void setCompteBancaireRelationship(org.cocktail.maracuja.client.metier.EOCompteBancaire value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOCompteBancaire oldValue = compteBancaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTE_BANCAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTE_BANCAIRE_KEY);
    }
  }
  

  public static EOCaisseDetail createCaisseDetail(EOEditingContext editingContext, NSTimestamp cadDate
, org.cocktail.maracuja.client.metier.EOCompteBancaire compteBancaire) {
    EOCaisseDetail eo = (EOCaisseDetail) createAndInsertInstance(editingContext, _EOCaisseDetail.ENTITY_NAME);    
		eo.setCadDate(cadDate);
    eo.setCompteBancaireRelationship(compteBancaire);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCaisseDetail.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCaisseDetail.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOCaisseDetail creerInstance(EOEditingContext editingContext) {
		  		EOCaisseDetail object = (EOCaisseDetail)createAndInsertInstance(editingContext, _EOCaisseDetail.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOCaisseDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCaisseDetail)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCaisseDetail localInstanceIn(EOEditingContext editingContext, EOCaisseDetail eo) {
    EOCaisseDetail localInstance = (eo == null) ? null : (EOCaisseDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCaisseDetail#localInstanceIn a la place.
   */
	public static EOCaisseDetail localInstanceOf(EOEditingContext editingContext, EOCaisseDetail eo) {
		return EOCaisseDetail.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCaisseDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCaisseDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCaisseDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCaisseDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCaisseDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCaisseDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCaisseDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCaisseDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCaisseDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCaisseDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCaisseDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCaisseDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
