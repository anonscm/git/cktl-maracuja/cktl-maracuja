/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.metier;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;


public class EOBilanPoste extends _EOBilanPoste {
	public static final EOSortOrdering SORT_BP_ORDRE_ASC = EOSortOrdering.sortOrderingWithKey(BP_ORDRE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray NS_SORT_BP_ORDRE_ASC = new NSArray(SORT_BP_ORDRE_ASC);
	//([ ]*[\+\-]?[ ]*S[DC]((\d++)|(\(\d++[ ]*([\+\-]?[ ]*\d++[ ]*)*\))))*
	//Permet de verifier les formules du type SD54-SC(40-4096+41)
	public static final String PATTERN_MATCHING_FORMULE = "([ ]*[\\+\\-]?[ ]*S[DC]((\\d++)|(\\(\\d++[ ]*([\\+\\-]?[ ]*\\d++[ ]*)*\\))))*";
	private static final EOQualifier QUAL_IS_SOUS_RUBRIQUE = new EOKeyValueQualifier("isGroupe2", EOQualifier.QualifierOperatorEqual, Boolean.TRUE);

	public static enum BPTypeElement {
		groupe1,
		groupe2,
		poste;
	}

	public EOBilanPoste() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (ZStringUtil.estVide(bpLibelle())) {
			throw new NSValidation.ValidationException("Le libelle ne peut être vide");
		}

		if (editingContext().insertedObjects().indexOfObject(this) != NSArray.NotFound) {
			checkLibelle(bpLibelle());
			String tmpBpStrId = creerBpStrIdFromLibelle(bpLibelle());
			setBpStrId(tmpBpStrId);
		}

		//Verifier les formules
		if (!ZStringUtil.estVide(bpFormuleMontant())) {
			validerFormule(editingContext(), bpFormuleMontant());
		}
		if (!ZStringUtil.estVide(bpFormuleAmortissement())) {
			validerFormule(editingContext(), bpFormuleAmortissement());
		}

	}

	public void checkLibelle(String libelle) {
		if (ZStringUtil.estVide(libelle)) {
			throw new NSValidation.ValidationException("Le libelle ne peut être vide");
		}
		String tmpBpStrId = creerBpStrIdFromLibelle(libelle);
		EOQualifier qual = new EOKeyValueQualifier(BP_STR_ID_KEY, EOQualifier.QualifierOperatorEqual, tmpBpStrId);
		if (EOQualifier.filteredArrayWithQualifier(toBilanPostePere().toBilanPosteEnfants(), qual).count() > 0) {
			throw new NSValidation.ValidationException("Le libellé [" + libelle + "] ou un libellé similaire est déjà utilisé pour un autre élément au même niveau.");
		}
	}

	public static void validerFormule(EOEditingContext editingContext, String formule) throws NSValidation.ValidationException {
		//validation 
		if (!ZStringUtil.estVide(formule)) {
			if (!formule.trim().matches(PATTERN_MATCHING_FORMULE)) {
				throw new NSValidation.ValidationException("La formule " + formule + " n'est pas valide.");
			}
		}
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.takeValueForKey(formule.trim(), "05_formule");
		ServerProxy.clientSideRequestExecuteStoredProcedureNamed(editingContext, "api_bilan.checkFormule", dico);
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return Le libellé sous forme arborescente
	 */
	public String displayLibelleComplet() {
		if (toBilanPostePere() == null) {
			return bpLibelle();
		}
		else {
			return toBilanPostePere().displayLibelleComplet() + "/" + bpLibelle();
		}
	}

	public static NSArray fetchAllForMode(EOEditingContext editingContext, EOBilanType selectedBilanType, EOExercice exercice, String mode) throws Exception {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOBilanPoste.TO_BILAN_TYPE_KEY, EOQualifier.QualifierOperatorEqual, selectedBilanType));
		quals.addObject(new EOKeyValueQualifier(EOBilanPoste.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice));
		quals.addObject(new EOKeyValueQualifier(EOBilanPoste.BP_STR_ID_KEY, EOQualifier.QualifierOperatorEqual, mode));

		//Recuperer la racine
		NSArray racines = fetchAll(editingContext, new EOAndQualifier(quals), null);
		if (racines.count() == 0) {
			throw new Exception("Aucune racine trouvée pour " + mode + "/" + exercice.exeExercice() + "/" + selectedBilanType.btLibelle());
		}
		EOBilanPoste bpRacine = (EOBilanPoste) racines.objectAtIndex(0);
		NSArray groupes = fetchAllRecursive(editingContext, bpRacine);
		return groupes;
	}

	public static NSArray fetchAllRecursive(EOEditingContext editingContext, EOBilanPoste bpRacine) throws Exception {
		NSMutableArray res = new NSMutableArray();
		NSArray groupes = fetchAll(editingContext, new EOKeyValueQualifier(EOBilanPoste.TO_BILAN_POSTE_PERE_KEY, EOQualifier.QualifierOperatorEqual, bpRacine), new NSArray(new Object[] {
				SORT_BP_ORDRE_ASC
		}));
		for (int i = 0; i < groupes.count(); i++) {
			EOBilanPoste bp = (EOBilanPoste) groupes.objectAtIndex(i);
			res.addObject(bp);
			NSArray fils = fetchAllRecursive(editingContext, bp);
			if (fils.count() > 0) {
				res.addObjectsFromArray(fils);
			}
		}
		return res.immutableClone();
	}

	public boolean isGroupe1() {
		return (bpNiveau().intValue() == 1 && bpGroupe().equals("O"));
	}

	public boolean isGroupe2() {
		return (bpNiveau().intValue() == 2 && bpGroupe().equals("O"));
	}

	public boolean isPoste() {
		return (bpGroupe().equals("N"));
	}

	public void setBpFormuleMontant(String value) {
		if (value != null) {
			value = value.trim();
		}
		super.setBpFormuleMontant(value);
		System.out.println("nouvelle formule montant " + value);
	}

	public void setBpFormuleAmortissement(String value) {
		if (value != null) {
			value = value.trim();
		}
		super.setBpFormuleAmortissement(value);
	}

	public boolean isUpdated() {
		return (editingContext().updatedObjects().indexOfObject(this) != NSArray.NotFound);
	}

	public static void validerLibelle(EOEditingContext editingContext, String libelle) throws NSValidation.ValidationException {
		//validation 
		if (ZStringUtil.estVide(libelle)) {
			throw new NSValidation.ValidationException("Le libellé ne peut être vide.");
		}
		//FIXME verifier que le libellé n'est pas deja present ?

	}

	public static EOBilanPoste createBilanPoste(EOEditingContext editingContext,
			EOBilanPoste bilanPostePere,
			String bpGroupe
			, String bpLibelle
			, String bpModifiable
			, Integer bpNiveau
			, EOBilanType toBilanType, EOExercice toExercice) {

		EOBilanPoste eo = (EOBilanPoste) createAndInsertInstance(editingContext, _EOBilanPoste.ENTITY_NAME);
		eo.setToBilanPostePereRelationship(bilanPostePere);
		eo.setBpLibelle(bpLibelle);
		eo.setBpGroupe(bpGroupe);
		eo.setBpLibelle(bpLibelle);
		eo.setBpModifiable(bpModifiable);
		eo.setBpNiveau(bpNiveau);
		eo.setBpOrdre(calcBpOrdreForBilanPostePere(bilanPostePere));
		eo.setToBilanTypeRelationship(toBilanType);
		eo.setToExerciceRelationship(toExercice);
		return eo;
	}

	private String creerBpStrIdFromLibelle(String libelle) {
		if (ZStringUtil.estVide(libelle)) {
			throw new NSValidation.ValidationException("Le libelle ne peut être vide");
		}
		String bpStrIdPere = "";
		if (toBilanPostePere()!=null) {
			 bpStrIdPere = toBilanPostePere().bpStrId()+"/";
		}
		
		String bpStrIdTmp = ZStringUtil.chaineSansCaracteresSpeciauxUpper(libelle).toLowerCase();
		return bpStrIdPere + bpStrIdTmp;
	}

	public static Integer calcBpOrdreForBilanPostePere(EOBilanPoste bilanPostePere) {
		Integer newBpOrdre = Integer.valueOf(0);
		NSArray enfants = bilanPostePere.toBilanPosteEnfants();

		for (int i = 0; i < enfants.count(); i++) {
			EOBilanPoste enfant = (EOBilanPoste) enfants.objectAtIndex(i);
			if (enfant.bpOrdre() != null && enfant.bpOrdre().compareTo(newBpOrdre) > 0) {
				newBpOrdre = enfant.bpOrdre();
			}
		}
		newBpOrdre = Integer.valueOf(newBpOrdre.intValue() + 1);
		return newBpOrdre;
	}

	/**
	 * @param niveau
	 * @return L'ancetre situé au niveau niveau.
	 */
	public EOBilanPoste getAncetreAtNiveau(Integer niveau) {
		if (niveau.compareTo(bpNiveau()) > 0) {
			return this;
		}
		if (bpNiveau().equals(Integer.valueOf(0))) {
			return this;
		}
		if (bpNiveau().equals(niveau)) {
			return this;
		}
		if (toBilanPostePere() == null) {
			return this;
		}
		return toBilanPostePere().getAncetreAtNiveau(niveau);
	}

	public EOBilanPoste getAncetreSousRubriqueOuRubrique() {
		if (toBilanPostePere() == null) {
			return this;
		}
		if (isGroupe2()) {
			return this;
		}
		if (isGroupe1()) {
			return this;
		}
		return toBilanPostePere().getAncetreSousRubriqueOuRubrique();
	}

	public EOBilanPoste getAncetreRubrique() {
		if (toBilanPostePere() == null) {
			return this;
		}
		if (isGroupe1()) {
			return this;
		}
		return toBilanPostePere().getAncetreRubrique();
	}

	public Integer numberOfSubObjects() {
		Integer nb = 0;
		NSArray enfants = toBilanPosteEnfants();
		if (enfants.count() == 0) {
			return 0;
		}
		else {
			for (int i = 0; i < enfants.count(); i++) {
				nb++;
				nb = nb + ((EOBilanPoste) enfants.objectAtIndex(i)).numberOfSubObjects();
			}
		}
		return nb;
	}

	public void reassignEnfantsBpOrdre() {
		NSArray enfants = toBilanPosteEnfants();
		for (int i = 0; i < enfants.count(); i++) {
			EOBilanPoste bp = (EOBilanPoste) enfants.objectAtIndex(i);
			if (bp.bpOrdre().intValue() != i + 1) {
				bp.setBpOrdre(i + 1);
			}
		}
	}

	public static void deleteObject(EOBilanPoste bilanPoste) {
		EOBilanPoste bilanPostePere = bilanPoste.toBilanPostePere();

		if (bilanPoste.toBilanPosteEnfants().count() > 0) {
			throw new NSValidation.ValidationException("L'élément contient des enfants, impossible de supprimer.");
		}
		bilanPoste.setToBilanPostePereRelationship(null);
		bilanPoste.editingContext().deleteObject(bilanPoste);
		bilanPostePere.reassignEnfantsBpOrdre();
	}

	@Override
	public NSArray toBilanPosteEnfants() {
		NSArray res = super.toBilanPosteEnfants();
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, NS_SORT_BP_ORDRE_ASC);
	}

	public NSArray toBilanPosteEnfantsRecursif() {
		NSMutableArray res = new NSMutableArray();
		NSArray enfants = toBilanPosteEnfants();
		if (enfants.count() > 0) {

			for (int i = 0; i < enfants.count(); i++) {
				res.addObject(enfants.objectAtIndex(i));
				res.addObjectsFromArray(((EOBilanPoste) enfants.objectAtIndex(i)).toBilanPosteEnfantsRecursif());
			}
		}
		return res.immutableClone();
	}

	public void echangerBpOrdreAvecEnfantPrecedent() {
		if (bpOrdre().intValue() <= 1) {
			return;
		}
		if (toBilanPostePere().toBilanPosteEnfants().count() == 1) {
			return;
		}
		int bpOrdre = bpOrdre();
		EOBilanPoste precedent = ((EOBilanPoste) toBilanPostePere()).getEnfantForBpOrdre(bpOrdre - 1);
		if (precedent != null) {
			precedent.setBpOrdre(bpOrdre);
		}
		setBpOrdre(bpOrdre - 1);
	}

	public void echangerBpOrdreAvecEnfantSuivant() {
		if (toBilanPostePere().toBilanPosteEnfants().count() == 1) {
			return;
		}
		int maxBpOrdre = ((EOBilanPoste) toBilanPostePere().toBilanPosteEnfants().lastObject()).bpOrdre();
		if (bpOrdre().intValue() == maxBpOrdre) {
			return;
		}
		int bpOrdre = bpOrdre();
		EOBilanPoste suivant = ((EOBilanPoste) toBilanPostePere()).getEnfantForBpOrdre(bpOrdre + 1);
		if (suivant != null) {
			suivant.setBpOrdre(bpOrdre);
		}
		setBpOrdre(bpOrdre + 1);
	}

	public EOBilanPoste getEnfantForBpOrdre(Integer bpOrdre) {
		EOQualifier qual = new EOKeyValueQualifier(EOBilanPoste.BP_ORDRE_KEY, EOQualifier.QualifierOperatorEqual, bpOrdre);
		NSArray res = EOQualifier.filteredArrayWithQualifier(toBilanPosteEnfants(), qual);
		if (res.count() == 0) {
			return null;
		}
		return (EOBilanPoste) res.objectAtIndex(0);
	}

	public boolean isPremierEnfant() {
		NSArray enfants = toBilanPostePere().toBilanPosteEnfants();
		return (enfants.indexOfObject(this) == 0);
	}

	public boolean isDernierEnfant() {
		NSArray enfants = toBilanPostePere().toBilanPosteEnfants();
		return (enfants.indexOfObject(this) == enfants.count() - 1);
	}

	public boolean hasSousRubriques() {
		return EOQualifier.filteredArrayWithQualifier(toBilanPosteEnfants(), QUAL_IS_SOUS_RUBRIQUE).count() > 0;
	}


}
