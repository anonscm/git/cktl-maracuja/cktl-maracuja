/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EORib.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.foundation.NSValidation;

public class EORib extends _EORib {
	private static final String LONGUEUR_DU_N_DE_COMPTE_INVALIDE = "Longueur du n° de compte invalide : ";
	private static final String LONGUEUR_DU_CODE_GUICHET_INVALIDE = "Longueur du code guichet invalide : ";
	private static final String AU_LIEU_DE = " au lieu de ";
	private static final String LONGUEUR_DU_CODE_BANQUE_INVALIDE = "Longueur du code banque invalide : ";
	private static final String NOM_DU_TITULAIRE_NON_DEFINI = "Nom du titulaire non défini.";
	private static final String COMPTE_NON_DEFINI = "Compte non défini.";
	private static final String DOMICILIATION_NON_DEFINIE = "Domiciliation non définie.";
	private static final String CODE_GUICHET_NON_DEFINI = "Code guichet non défini.";
	private static final String CODE_BANQUE_NON_DEFINI = "Code banque non défini.";
	public static final String RIB_VALIDE = "O";
	public static final String RIB_ANNULE = "A";
	public static final String RIB_NONVALIDE = "N";
	private static final int LONGUEUR_CODE_BANQUE = 5;
	private static final int LONGUEUR_GUICHET = 5;
	private static final int LONGUEUR_COMPTE_NUM = 11;
	public static final String RIB_INCOMPLET = "incomplet/erroné/invalide";
	public static final String RIB_LIB_LONG_KEY = "ribLibelleLong";
	public static final String RIBCOMPLET_KEY = "ribComplet";

	public EORib() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return Un libellé qui identifie le rib pour les listes déroulantes)
	 */
	public final String getRibLibelleLong() {
		if (!isRibFRComplet() && !isRibIntlComplet()) {
			return RIB_INCOMPLET;
		}
		return this.ribLib().concat("-").concat(this.getIbanOuCompteFr());
	}

	public final String getIbanOuCompteFr() {
		if (!ZStringUtil.estVide(ribIban())) {
			return ribIbanFormate();
		}
		return ribNum();
	}

	public final String getRibCompletFR() {
		if (!isRibFRComplet()) {
			return RIB_INCOMPLET;
		}
		return this.ribCodBanc().concat(" ").concat(this.ribGuich()).concat(" ").concat(ribNum()).concat(" ").concat(ribCle()).concat(" - ").concat(ribLib()).concat(" (").concat(ribTitco()).concat(")");
	}

	public final String getRibCompletIntl() {
		if (!isRibIntlComplet()) {
			return RIB_INCOMPLET;
		}
		return ribBicFormate() + " - " + ribIbanFormate().concat(" - ").concat(ribLib()).concat(" (").concat(ribTitco()).concat(")");
	}

	/**
	 * @return Un libellé avec Banque + guichet + compte + clé
	 */
	public final String getRibComplet() {
		if (isRibIntlComplet()) {
			return getRibCompletIntl();
		}
		else if (isRibFRComplet()) {
			return getRibCompletFR();
		}
		return RIB_INCOMPLET;
	}

	public final boolean isRibIncomplet() {
		return (!isRibFRComplet() && !isRibIntlComplet());
		//return (ribCodBanc() == null || ribGuich() == null || ribNum() == null || ribCle() == null || ribLib() == null || ribTitco() == null);
	}

	public boolean isRibFRComplet() {
		return (ribCodBanc() != null && ribGuich() != null && ribNum() != null && ribCle() != null);
	}

	public boolean isRibIntlComplet() {
		return (ribIban() != null && ribBic() != null);
	}

	/**
	 * Vérifie si le rib est bien construit (taille des champs etc.). Ne controle pas sir le rib est à l'état VALIDE. Sinon déclenche une exception.
	 * 
	 * @throws Exception
	 */
	public final void checkRibCorrect() throws Exception {
		if (ribCodBanc() == null) {
			throw new Exception(CODE_BANQUE_NON_DEFINI);
		}
		if (ribGuich() == null) {
			throw new Exception(CODE_GUICHET_NON_DEFINI);
		}
		if (ribLib() == null) {
			throw new Exception(DOMICILIATION_NON_DEFINIE);
		}
		if (ribNum() == null) {
			throw new Exception(COMPTE_NON_DEFINI);
		}
		if (ribTitco() == null) {
			throw new Exception(NOM_DU_TITULAIRE_NON_DEFINI);
		}
		if (ribCodBanc().length() != LONGUEUR_CODE_BANQUE) {
			throw new Exception(LONGUEUR_DU_CODE_BANQUE_INVALIDE + ribCodBanc().length() + AU_LIEU_DE + LONGUEUR_CODE_BANQUE);
		}
		if (ribGuich().length() != LONGUEUR_GUICHET) {
			throw new Exception(LONGUEUR_DU_CODE_GUICHET_INVALIDE + ribGuich().length() + AU_LIEU_DE + LONGUEUR_GUICHET);
		}
		if (ribNum().length() != LONGUEUR_COMPTE_NUM) {
			throw new Exception(LONGUEUR_DU_N_DE_COMPTE_INVALIDE + ribNum().length() + AU_LIEU_DE + LONGUEUR_COMPTE_NUM);
		}
	}

	public final void checkRibCorrectIntl() throws Exception {
		if (ribBic() == null) {
			throw new Exception("Le code BIC n'est pas défini");
		}
		if (ribIban() == null) {
			throw new Exception("Le code IBAN n'est pas défini");
		}
		if (ribLib() == null) {
			throw new Exception(DOMICILIATION_NON_DEFINIE);
		}
		if (ZStringUtil.estVide(ribTitco())) {
			throw new Exception(NOM_DU_TITULAIRE_NON_DEFINI);
		}

	}

	public String ribAgence() {
		return ribLib();
	}

	public String ribIbanFormate() {
		String res = formatIban(ribIban());
		if (ZStringUtil.estVide(res)) {
			return "????????";
		}
		return res;
	}

	public String ribBicFormate() {
		String res = ribBic();
		if (ZStringUtil.estVide(res)) {
			return "????????";
		}
		return res;
	}

	public static String formatIban(String iban) {
		if (iban == null || iban.length() == 0) {
			return "";
		}
		iban = iban.trim().replaceAll(" ", "");
		iban = iban.toUpperCase();
		String res = "";

		BigDecimal nbgrappes = BigDecimal.valueOf(iban.length()).divide(BigDecimal.valueOf(4));
		int max = nbgrappes.intValue();
		String reste = iban;
		for (int i = 0; i < max; i++) {
			res = res.concat(reste.substring(0, (reste.length() < 4 ? reste.length() : 4))).concat(" ");
			reste = reste.substring((reste.length() > 4 ? 4 : reste.length()));
		}
		res = res.concat(reste);
		res = res.trim();
		return res;
	}

}
