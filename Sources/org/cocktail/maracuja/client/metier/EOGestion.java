/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOGestion.java
// 
package org.cocktail.maracuja.client.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOGestion extends _EOGestion {

	public static final EOSortOrdering SORT_GES_CODE_ASC = EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending);

	public EOGestion() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static NSArray fetchAllForExercice(EOEditingContext editingContext, EOExercice exercice) {
		NSArray res = EOGestionExercice.fetchAll(editingContext, new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice), null);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray((NSArray) res.valueForKey(EOGestionExercice.GESTION_KEY), new NSArray(new Object[] {
				SORT_GES_CODE_ASC
		}));
	}

	/**
	 * Renvoie l'objet EOGestionExercice correspondant à un gestion et un exercice. Pas de fetch, seulement en filtre de gestion.gestionExercices
	 * 
	 * @param ed
	 * @param gestion
	 * @param exercice
	 * @return
	 * @throws Exception
	 */
	public final EOGestionExercice getGestionExercice(final EOExercice exercice) {
		//		final EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("exercice.exeExercice=%@", new NSArray(exercice.exeExercice()));
		final EOQualifier qualifier = new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice.exeExercice());
		final NSArray gestionExercices = EOQualifier.filteredArrayWithQualifier(gestionExercices(), qualifier);
		if (gestionExercices.count() == 0) {
			return null;
		}
		return (EOGestionExercice) gestionExercices.objectAtIndex(0);
	}

	/**
	 * @param exercice
	 * @return Le compte de liaison à utiliser pour les écritures SACD sur l'agence
	 */
	public final EOPlanComptable getPlanco185(final EOExercice exercice) {
		final EOGestionExercice gestionExercice = getGestionExercice(exercice);
		if (gestionExercice == null) {
			return null;
		}
		return gestionExercice.planComptable185();
	}

	/**
	 * @param exercice
	 * @return Le compte de liaison à utiliser pour les écritures SACD sur le SACD
	 */
	public final EOPlanComptable getPlanco185CtpSacd(final EOExercice exercice) {
		final EOGestionExercice gestionExercice = getGestionExercice(exercice);
		if (gestionExercice == null) {
			return null;
		}
		return gestionExercice.planComptable185CtpSacd();
	}

	public boolean isAgence() {
		return gesCode().equals(comptabilite().gestion().gesCode());
	}

	public boolean isSacd(EOExercice exercice) {
		final EOGestionExercice gestionExercice = getGestionExercice(exercice);
		if (gestionExercice == null) {
			return false;
		}
		return gestionExercice.isSacd();
	}

	public boolean isComposante(EOExercice exercice) {
		final EOGestionExercice gestionExercice = getGestionExercice(exercice);
		if (gestionExercice == null) {
			return false;
		}
		return gestionExercice.isComposante();
	}

}
