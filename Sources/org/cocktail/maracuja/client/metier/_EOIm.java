// _EOIm.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIm.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOIm extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Im";
	public static final String ENTITY_TABLE_NAME = "maracuja.IM";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "imId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String IM_COMMENTAIRES_KEY = "imCommentaires";
	public static final String IM_DATE_DEPART_DGP_KEY = "imDateDepartDgp";
	public static final String IM_DATE_FIN_DGP_REELLE_KEY = "imDateFinDgpReelle";
	public static final String IM_DATE_FIN_DGP_THEORIQUE_KEY = "imDateFinDgpTheorique";
	public static final String IM_DGP_KEY = "imDgp";
	public static final String IM_DUREE_SUSP_KEY = "imDureeSusp";
	public static final String IM_LIBELLE_TAUX_KEY = "imLibelleTaux";
	public static final String IM_MONTANT_KEY = "imMontant";
	public static final String IM_NB_JOURS_DEPASSEMENT_KEY = "imNbJoursDepassement";
	public static final String IM_NUMERO_KEY = "imNumero";
	public static final String IM_PENALITE_KEY = "imPenalite";
	public static final String IM_TAUX_APPLICABLE_KEY = "imTauxApplicable";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String IM_ID_KEY = "imId";
	public static final String IM_TAUX_REFERENCE_KEY = "imTauxReference";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String IM_COMMENTAIRES_COLKEY = "IM_COMMENTAIRES";
	public static final String IM_DATE_DEPART_DGP_COLKEY = "IM_DATE_DEPART_DGP";
	public static final String IM_DATE_FIN_DGP_REELLE_COLKEY = "IM_DATE_FIN_DGP_REELLE";
	public static final String IM_DATE_FIN_DGP_THEORIQUE_COLKEY = "IM_DATE_FIN_DGP_THEORIQUE";
	public static final String IM_DGP_COLKEY = "IM_DGP";
	public static final String IM_DUREE_SUSP_COLKEY = "IM_DUREE_SUSP";
	public static final String IM_LIBELLE_TAUX_COLKEY = "IM_LIBELLE_TAUX";
	public static final String IM_MONTANT_COLKEY = "IM_MONTANT";
	public static final String IM_NB_JOURS_DEPASSEMENT_COLKEY = "IM_NB_JOURS_DEPASSEMENT";
	public static final String IM_NUMERO_COLKEY = "IM_NUMERO";
	public static final String IM_PENALITE_COLKEY = "IM_penalite";
	public static final String IM_TAUX_APPLICABLE_COLKEY = "IM_TAUX_APPLICABLE";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String IM_ID_COLKEY = "IM_ID";
	public static final String IM_TAUX_REFERENCE_COLKEY = "IM_TAUX_REFERENCE";
	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String DEPENSE_KEY = "depense";
	public static final String IM_TAUX_KEY = "imTaux";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public String imCommentaires() {
    return (String) storedValueForKey(IM_COMMENTAIRES_KEY);
  }

  public void setImCommentaires(String value) {
    takeStoredValueForKey(value, IM_COMMENTAIRES_KEY);
  }

  public NSTimestamp imDateDepartDgp() {
    return (NSTimestamp) storedValueForKey(IM_DATE_DEPART_DGP_KEY);
  }

  public void setImDateDepartDgp(NSTimestamp value) {
    takeStoredValueForKey(value, IM_DATE_DEPART_DGP_KEY);
  }

  public NSTimestamp imDateFinDgpReelle() {
    return (NSTimestamp) storedValueForKey(IM_DATE_FIN_DGP_REELLE_KEY);
  }

  public void setImDateFinDgpReelle(NSTimestamp value) {
    takeStoredValueForKey(value, IM_DATE_FIN_DGP_REELLE_KEY);
  }

  public NSTimestamp imDateFinDgpTheorique() {
    return (NSTimestamp) storedValueForKey(IM_DATE_FIN_DGP_THEORIQUE_KEY);
  }

  public void setImDateFinDgpTheorique(NSTimestamp value) {
    takeStoredValueForKey(value, IM_DATE_FIN_DGP_THEORIQUE_KEY);
  }

  public Long imDgp() {
    return (Long) storedValueForKey(IM_DGP_KEY);
  }

  public void setImDgp(Long value) {
    takeStoredValueForKey(value, IM_DGP_KEY);
  }

  public Long imDureeSusp() {
    return (Long) storedValueForKey(IM_DUREE_SUSP_KEY);
  }

  public void setImDureeSusp(Long value) {
    takeStoredValueForKey(value, IM_DUREE_SUSP_KEY);
  }

  public String imLibelleTaux() {
    return (String) storedValueForKey(IM_LIBELLE_TAUX_KEY);
  }

  public void setImLibelleTaux(String value) {
    takeStoredValueForKey(value, IM_LIBELLE_TAUX_KEY);
  }

  public java.math.BigDecimal imMontant() {
    return (java.math.BigDecimal) storedValueForKey(IM_MONTANT_KEY);
  }

  public void setImMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IM_MONTANT_KEY);
  }

  public Long imNbJoursDepassement() {
    return (Long) storedValueForKey(IM_NB_JOURS_DEPASSEMENT_KEY);
  }

  public void setImNbJoursDepassement(Long value) {
    takeStoredValueForKey(value, IM_NB_JOURS_DEPASSEMENT_KEY);
  }

  public String imNumero() {
    return (String) storedValueForKey(IM_NUMERO_KEY);
  }

  public void setImNumero(String value) {
    takeStoredValueForKey(value, IM_NUMERO_KEY);
  }

  public java.math.BigDecimal imPenalite() {
    return (java.math.BigDecimal) storedValueForKey(IM_PENALITE_KEY);
  }

  public void setImPenalite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IM_PENALITE_KEY);
  }

  public java.math.BigDecimal imTauxApplicable() {
    return (java.math.BigDecimal) storedValueForKey(IM_TAUX_APPLICABLE_KEY);
  }

  public void setImTauxApplicable(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IM_TAUX_APPLICABLE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EODepense depense() {
    return (org.cocktail.maracuja.client.metier.EODepense)storedValueForKey(DEPENSE_KEY);
  }

  public void setDepenseRelationship(org.cocktail.maracuja.client.metier.EODepense value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EODepense oldValue = depense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DEPENSE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOImTaux imTaux() {
    return (org.cocktail.maracuja.client.metier.EOImTaux)storedValueForKey(IM_TAUX_KEY);
  }

  public void setImTauxRelationship(org.cocktail.maracuja.client.metier.EOImTaux value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOImTaux oldValue = imTaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, IM_TAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, IM_TAUX_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.maracuja.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.maracuja.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOIm createIm(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, NSTimestamp imDateDepartDgp
, NSTimestamp imDateFinDgpReelle
, NSTimestamp imDateFinDgpTheorique
, Long imDgp
, Long imDureeSusp
, String imLibelleTaux
, java.math.BigDecimal imMontant
, Long imNbJoursDepassement
, String imNumero
, java.math.BigDecimal imPenalite
, java.math.BigDecimal imTauxApplicable
, org.cocktail.maracuja.client.metier.EODepense depense, org.cocktail.maracuja.client.metier.EOImTaux imTaux, org.cocktail.maracuja.client.metier.EOTypeEtat typeEtat, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOIm eo = (EOIm) createAndInsertInstance(editingContext, _EOIm.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setImDateDepartDgp(imDateDepartDgp);
		eo.setImDateFinDgpReelle(imDateFinDgpReelle);
		eo.setImDateFinDgpTheorique(imDateFinDgpTheorique);
		eo.setImDgp(imDgp);
		eo.setImDureeSusp(imDureeSusp);
		eo.setImLibelleTaux(imLibelleTaux);
		eo.setImMontant(imMontant);
		eo.setImNbJoursDepassement(imNbJoursDepassement);
		eo.setImNumero(imNumero);
		eo.setImPenalite(imPenalite);
		eo.setImTauxApplicable(imTauxApplicable);
    eo.setDepenseRelationship(depense);
    eo.setImTauxRelationship(imTaux);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOIm.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOIm.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOIm creerInstance(EOEditingContext editingContext) {
		  		EOIm object = (EOIm)createAndInsertInstance(editingContext, _EOIm.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOIm localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIm)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOIm localInstanceIn(EOEditingContext editingContext, EOIm eo) {
    EOIm localInstance = (eo == null) ? null : (EOIm)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOIm#localInstanceIn a la place.
   */
	public static EOIm localInstanceOf(EOEditingContext editingContext, EOIm eo) {
		return EOIm.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOIm fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOIm fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIm eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIm)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIm fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIm fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIm eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIm)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOIm fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIm eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIm ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIm fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
