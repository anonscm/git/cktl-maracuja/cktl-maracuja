/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
package org.cocktail.maracuja.client.metier;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOImTaux extends _EOImTaux {
	public static final String TAUX_COMPLET_KEY = "tauxComplet";
	public static final EOSortOrdering SORT_IMTT_CODE_ASC = EOSortOrdering.sortOrderingWithKey(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final Object SORT_IMTT_PRIORITE_DESC = EOSortOrdering.sortOrderingWithKey(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_PRIORITE_KEY, EOSortOrdering.CompareDescending);;

	public EOImTaux() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static NSArray fetchAll(EOEditingContext ec, EOQualifier qual, NSArray sortOrderings, boolean refresh) {
		EOFetchSpecification spec = new EOFetchSpecification(ENTITY_NAME, qual, null, true, false, null);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static NSArray fetchAllForDate(EOEditingContext ec, NSTimestamp date) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(EOTypeEtat.ETAT_VALIDE)));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_DEBUT_KEY + "<=%@", new NSArray(date)));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(date)));
		return fetchAll(ec, new EOAndQualifier(quals), null, false);
	}

	public static NSArray fetchAllForDateAndType(EOEditingContext ec, NSTimestamp date, String typeTauxCode) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_CODE_KEY + "=%@", new NSArray(typeTauxCode)));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_DEBUT_KEY + "<=%@", new NSArray(date)));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(date)));
		return fetchAll(ec, new EOAndQualifier(quals), null, false);
	}

	public String tauxComplet() {
		return imTypeTaux().imttCode() + " " + ZConst.FORMAT_DECIMAL_COURT.format(imtaTaux()) + " du " + ZConst.FORMAT_DATESHORT.format(imtaDebut());
	}
}
