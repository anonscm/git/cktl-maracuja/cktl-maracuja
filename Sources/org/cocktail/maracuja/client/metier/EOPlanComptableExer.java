/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOPlanComptableExer.java
// 
package org.cocktail.maracuja.client.metier;

import org.cocktail.maracuja.client.factory.process.FactoryProcessPlanComptableEtVisa;
import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPlanComptableExer extends _EOPlanComptableExer {

	public static final String PCONUMETPCOLIBELLE_KEY = "pcoNumEtPcoLibelle";
	public static final String CLASSE_COMPTE_KEY = "classeCompte";

	public static final String PCO_NUM_KEY = "pcoNum";

	public static final String etatAnnule = "ANNULE";
	public static final String etatValide = "VALIDE";

	public static final String pcoBudgetaireOui = "O";
	public static final String pcoBudgetaireNon = "N";
	public static final String pcoEmargementOui = "O";
	public static final String pcoEmargementNon = "N";
	public static final String pcoNatureDepense = "D";
	public static final String pcoNatureRecette = "R";
	public static final String pcoNatureTiers = "T";

	public static final String pcoJBeOui = "O";
	public static final String pcoJBeNon = "N";
	public static final String pcoJExerciceOui = "O";
	public static final String pcoJExerciceNon = "N";
	public static final String pcoJFinExerciceOui = "O";
	public static final String pcoJFinExerciceNon = "N";

	public static final String PCOSENSSOLDE_DEBIT = "D";
	public static final String PCOSENSSOLDE_CREDIT = "C";
	public static final String PCOSENSSOLDE_LES2 = "2";
	public static final String PCOSENSSOLDE_ZERO = "0";

	public static final String CLASSE0 = "0";
	public static final String CLASSE1 = "1";
	public static final String CLASSE2 = "2";
	public static final String CLASSE3 = "3";
	public static final String CLASSE4 = "4";
	public static final String CLASSE5 = "5";
	public static final String CLASSE6 = "6";
	public static final String CLASSE7 = "7";
	public static final String CLASSE8 = "8";
	public static final String CLASSE9 = "9";

	public static EOQualifier QUAL_VALIDE = new EOKeyValueQualifier(EOPlanComptableExer.PCO_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual, etatValide);
	public static EOQualifier QUAL_CLASSE_4 = EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + CLASSE4 + "*'", null);
	public static EOQualifier QUAL_CLASSE_5 = EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + CLASSE5 + "*'", null);
	public static EOQualifier QUAL_EMARGEABLE = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_EMARGEMENT_KEY + "=%@", new NSArray(new Object[] {
			EOPlanComptableExer.pcoEmargementOui
	}));
	public static final EOQualifier QUAL_GRANDLIVREVALEURSINACTIVES = new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, "86*");

	public EOPlanComptableExer() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	//	public String pcoNum() {
	//		return (planComptable() != null ? planComptable().pcoNum():null);
	//	}

	/**
	 * @return La classe du compte (premier caractère du pcoNum ou null).
	 */
	public final String getClasseCompte() {
		if (pcoNum() != null) {
			return pcoNum().substring(0, 1);
		}
		return null;
	}

	/**
	 * @return Le chapitre du compte (deux premiers caractères du pcoNum ou null).
	 */
	public final String getChapitreCompte() {
		if (pcoNum() != null && pcoNum().length() >= 2) {
			return pcoNum().substring(0, 2);
		}
		return null;
	}

	public final String pcoNumEtPcoLibelle() {
		return pcoNum() + " : " + pcoLibelle();
	}

	public String toString() {
		return pcoNumEtPcoLibelle();
	}

	public boolean isAmortissementPossible() {
		return CLASSE2.equals(getClasseCompte());
	}

	public boolean isClasse() {
		return pcoNiveau().intValue() == 1;
	}

	public boolean isChapitre() {
		return pcoNiveau().intValue() == 2;
	}

	/// Setters

	public void setPcoBudgetaire(String value) {
		super.setPcoBudgetaire(value);
		planComptable().setPcoBudgetaire(value);
	}

	public void setPcoCompteBe(String value) {
		super.setPcoCompteBe(value);
		planComptable().setPcoCompteBe(value);
	}

	public void setPcoEmargement(String value) {
		super.setPcoEmargement(value);
		planComptable().setPcoEmargement(value);
	}

	public void setPcoJBe(String value) {
		super.setPcoJBe(value);
		planComptable().setPcoJBe(value);
	}

	public void setPcoJExercice(String value) {
		super.setPcoJExercice(value);
		planComptable().setPcoJExercice(value);
	}

	public void setPcoJFinExercice(String value) {
		super.setPcoJFinExercice(value);
		planComptable().setPcoJFinExercice(value);
	}

	public void setPcoLibelle(String value) {
		super.setPcoLibelle(value);
		planComptable().setPcoLibelle(value);
	}

	public void setPcoNature(String value) {
		super.setPcoNature(value);
		planComptable().setPcoNature(value);
	}

	public void setPcoNiveau(Integer value) {
		super.setPcoNiveau(value);
		planComptable().setPcoNiveau(value);
	}

	public void setPcoSensSolde(String value) {
		super.setPcoSensSolde(value);
		planComptable().setPcoSensSolde(value);
	}

	public void setPcoValidite(String value) {
		super.setPcoValidite(value);
		planComptable().setPcoValidite(value);
	}

	public static EOPlanComptableExer getCompte(EOEditingContext editingContext, EOExercice exercice, String pcoNum) {
		EOQualifier qual1 = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		EOQualifier qual2 = new EOKeyValueQualifier(EOPlanComptableExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return EOPlanComptableExer.fetchByQualifier(editingContext, new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})));
	}

	/**
	 * @param editingContext
	 * @param exercice
	 * @param pcoNum
	 * @return Le premier compte "parent" trouvé.
	 */
	public static EOPlanComptableExer getComptePere(EOEditingContext editingContext, EOExercice exercice, String pcoNum) {
		if (ZStringUtil.isEmpty(pcoNum)) {
			return null;
		}
		pcoNum = pcoNum.substring(0, pcoNum.length() - 1);
		//System.out.println(pcoNum);
		EOQualifier qual1 = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		EOQualifier qual2 = new EOKeyValueQualifier(EOPlanComptableExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		EOPlanComptableExer res = EOPlanComptableExer.fetchByQualifier(editingContext, new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})));
		if (res == null) {
			return getComptePere(editingContext, exercice, pcoNum);
		}
		return res;
	}

	public boolean isValide() {
		return etatValide.equals(pcoValidite());
	}

	public static EOPlanComptableExer creerOuValiderPlanComptable(EOEditingContext editingContext, EOExercice exercice, String pcoNum, String pcoLibelle, String pcoBudgetaire, String pcoEmargement, String pcoNature) throws Exception {
		EOPlanComptableExer pco = getCompte(editingContext, exercice, pcoNum);
		if (pco != null && ZStringUtil.estVide(pcoLibelle) && !pco.pcoLibelle().toUpperCase().equals(pcoLibelle.toUpperCase())) {
			throw new Exception("Le compte " + pcoNum + " existe déjà dans le plan comptable pour l'exercice " + exercice.exeExercice() + " avec un libellé différent (" + pco.pcoLibelle() + ")");
		}

		if (pco != null && pco.isValide()) {
			throw new Exception("Le compte " + pcoNum + " existe déjà et est valide dans le plan comptable pour l'exercice " + exercice.exeExercice());
		}

		if (pco == null) {
			FactoryProcessPlanComptableEtVisa factoryPco = new FactoryProcessPlanComptableEtVisa(IConst.WANT_EO_LOGS);
			EOPlanComptableExer res = factoryPco.creerPlanComptableExer(editingContext, exercice, pcoBudgetaire, pcoEmargement, pcoLibelle, pcoNature, pcoNum);
			return res;
		}
		else if (!pco.isValide()) {
			pco.setPcoValidite(EOPlanComptableExer.etatValide);
			return pco;
		}
		return null;

	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOExercice exercice) {
		EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return fetchAll(editingContext, qual, null, false);
	}

}
