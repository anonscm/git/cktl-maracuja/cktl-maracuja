// _EOBordereauBrouillard.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBordereauBrouillard.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBordereauBrouillard extends  EOGenericRecord {
	public static final String ENTITY_NAME = "BordereauBrouillard";
	public static final String ENTITY_TABLE_NAME = "maracuja.bordereau_brouillard";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bobOrdre";

	public static final String BOB_ETAT_KEY = "bobEtat";
	public static final String BOB_LIBELLE1_KEY = "bobLibelle1";
	public static final String BOB_LIBELLE2_KEY = "bobLibelle2";
	public static final String BOB_LIBELLE3_KEY = "bobLibelle3";
	public static final String BOB_MONTANT_KEY = "bobMontant";
	public static final String BOB_OPERATION_KEY = "bobOperation";
	public static final String BOB_SENS_KEY = "bobSens";

// Attributs non visibles
	public static final String BOB_ORDRE_KEY = "bobOrdre";
	public static final String BOR_ID_KEY = "borId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String BOB_ETAT_COLKEY = "bob_etat";
	public static final String BOB_LIBELLE1_COLKEY = "bob_libelle1";
	public static final String BOB_LIBELLE2_COLKEY = "bob_libelle2";
	public static final String BOB_LIBELLE3_COLKEY = "bob_libelle3";
	public static final String BOB_MONTANT_COLKEY = "bob_Montant";
	public static final String BOB_OPERATION_COLKEY = "bob_Operation";
	public static final String BOB_SENS_COLKEY = "bob_Sens";

	public static final String BOB_ORDRE_COLKEY = "bob_ordre";
	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String PCO_NUM_COLKEY = "pco_num";


	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String ECHEANCIER_BROUILLARDS_KEY = "echeancierBrouillards";
	public static final String EXERCICE_KEY = "exercice";
	public static final String GESTION_KEY = "gestion";
	public static final String PLAN_COMPTABLE_VISA_KEY = "planComptableVisa";
	public static final String TO_SEPA_SDD_ECHEANCIER_BOBS_KEY = "toSepaSddEcheancierBobs";



	// Accessors methods
  public String bobEtat() {
    return (String) storedValueForKey(BOB_ETAT_KEY);
  }

  public void setBobEtat(String value) {
    takeStoredValueForKey(value, BOB_ETAT_KEY);
  }

  public String bobLibelle1() {
    return (String) storedValueForKey(BOB_LIBELLE1_KEY);
  }

  public void setBobLibelle1(String value) {
    takeStoredValueForKey(value, BOB_LIBELLE1_KEY);
  }

  public String bobLibelle2() {
    return (String) storedValueForKey(BOB_LIBELLE2_KEY);
  }

  public void setBobLibelle2(String value) {
    takeStoredValueForKey(value, BOB_LIBELLE2_KEY);
  }

  public String bobLibelle3() {
    return (String) storedValueForKey(BOB_LIBELLE3_KEY);
  }

  public void setBobLibelle3(String value) {
    takeStoredValueForKey(value, BOB_LIBELLE3_KEY);
  }

  public java.math.BigDecimal bobMontant() {
    return (java.math.BigDecimal) storedValueForKey(BOB_MONTANT_KEY);
  }

  public void setBobMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BOB_MONTANT_KEY);
  }

  public String bobOperation() {
    return (String) storedValueForKey(BOB_OPERATION_KEY);
  }

  public void setBobOperation(String value) {
    takeStoredValueForKey(value, BOB_OPERATION_KEY);
  }

  public String bobSens() {
    return (String) storedValueForKey(BOB_SENS_KEY);
  }

  public void setBobSens(String value) {
    takeStoredValueForKey(value, BOB_SENS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOBordereau bordereau() {
    return (org.cocktail.maracuja.client.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.maracuja.client.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOPlanComptable planComptableVisa() {
    return (org.cocktail.maracuja.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_VISA_KEY);
  }

  public void setPlanComptableVisaRelationship(org.cocktail.maracuja.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOPlanComptable oldValue = planComptableVisa();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_VISA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_VISA_KEY);
    }
  }
  
  public NSArray echeancierBrouillards() {
    return (NSArray)storedValueForKey(ECHEANCIER_BROUILLARDS_KEY);
  }

  public NSArray echeancierBrouillards(EOQualifier qualifier) {
    return echeancierBrouillards(qualifier, null, false);
  }

  public NSArray echeancierBrouillards(EOQualifier qualifier, boolean fetch) {
    return echeancierBrouillards(qualifier, null, fetch);
  }

  public NSArray echeancierBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOEcheancierBrouillard.BORDEREAU_BROUILLARD_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOEcheancierBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = echeancierBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEcheancierBrouillardsRelationship(org.cocktail.maracuja.client.metier.EOEcheancierBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ECHEANCIER_BROUILLARDS_KEY);
  }

  public void removeFromEcheancierBrouillardsRelationship(org.cocktail.maracuja.client.metier.EOEcheancierBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ECHEANCIER_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOEcheancierBrouillard createEcheancierBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EcheancierBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ECHEANCIER_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.client.metier.EOEcheancierBrouillard) eo;
  }

  public void deleteEcheancierBrouillardsRelationship(org.cocktail.maracuja.client.metier.EOEcheancierBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ECHEANCIER_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEcheancierBrouillardsRelationships() {
    Enumeration objects = echeancierBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEcheancierBrouillardsRelationship((org.cocktail.maracuja.client.metier.EOEcheancierBrouillard)objects.nextElement());
    }
  }

  public NSArray toSepaSddEcheancierBobs() {
    return (NSArray)storedValueForKey(TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
  }

  public NSArray toSepaSddEcheancierBobs(EOQualifier qualifier) {
    return toSepaSddEcheancierBobs(qualifier, null);
  }

  public NSArray toSepaSddEcheancierBobs(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = toSepaSddEcheancierBobs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToSepaSddEcheancierBobsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
  }

  public void removeFromToSepaSddEcheancierBobsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
  }

  public org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob createToSepaSddEcheancierBobsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlCompta_SepaSddEcheancierBob");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
    return (org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob) eo;
  }

  public void deleteToSepaSddEcheancierBobsRelationship(org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_SEPA_SDD_ECHEANCIER_BOBS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToSepaSddEcheancierBobsRelationships() {
    Enumeration objects = toSepaSddEcheancierBobs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSepaSddEcheancierBobsRelationship((org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob)objects.nextElement());
    }
  }


  public static EOBordereauBrouillard createBordereauBrouillard(EOEditingContext editingContext, String bobEtat
, java.math.BigDecimal bobMontant
, String bobSens
, org.cocktail.maracuja.client.metier.EOBordereau bordereau, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOGestion gestion) {
    EOBordereauBrouillard eo = (EOBordereauBrouillard) createAndInsertInstance(editingContext, _EOBordereauBrouillard.ENTITY_NAME);    
		eo.setBobEtat(bobEtat);
		eo.setBobMontant(bobMontant);
		eo.setBobSens(bobSens);
    eo.setBordereauRelationship(bordereau);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBordereauBrouillard.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBordereauBrouillard.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOBordereauBrouillard creerInstance(EOEditingContext editingContext) {
		  		EOBordereauBrouillard object = (EOBordereauBrouillard)createAndInsertInstance(editingContext, _EOBordereauBrouillard.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOBordereauBrouillard localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBordereauBrouillard)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOBordereauBrouillard localInstanceIn(EOEditingContext editingContext, EOBordereauBrouillard eo) {
    EOBordereauBrouillard localInstance = (eo == null) ? null : (EOBordereauBrouillard)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBordereauBrouillard#localInstanceIn a la place.
   */
	public static EOBordereauBrouillard localInstanceOf(EOEditingContext editingContext, EOBordereauBrouillard eo) {
		return EOBordereauBrouillard.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOBordereauBrouillard fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBordereauBrouillard fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBordereauBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBordereauBrouillard)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBordereauBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBordereauBrouillard fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBordereauBrouillard eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBordereauBrouillard)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBordereauBrouillard fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBordereauBrouillard eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBordereauBrouillard ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBordereauBrouillard fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
