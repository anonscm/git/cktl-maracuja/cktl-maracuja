// _EORib.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORib.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORib extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Rib";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_RIB";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ribOrdre";

	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String RIB_BIC_KEY = "ribBic";
	public static final String RIB_CLE_KEY = "ribCle";
	public static final String RIB_COD_BANC_KEY = "ribCodBanc";
	public static final String RIB_GUICH_KEY = "ribGuich";
	public static final String RIB_IBAN_KEY = "ribIban";
	public static final String RIB_LIB_KEY = "ribLib";
	public static final String RIB_NUM_KEY = "ribNum";
	public static final String RIB_TITCO_KEY = "ribTitco";
	public static final String RIB_VALIDE_KEY = "ribValide";

// Attributs non visibles
	public static final String RIB_ORDRE_KEY = "ribOrdre";

//Colonnes dans la base de donnees
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String RIB_BIC_COLKEY = "RIB_BIC";
	public static final String RIB_CLE_COLKEY = "RIB_CLE";
	public static final String RIB_COD_BANC_COLKEY = "RIB_CODBANC";
	public static final String RIB_GUICH_COLKEY = "RIB_GUICH";
	public static final String RIB_IBAN_COLKEY = "RIB_IBAN";
	public static final String RIB_LIB_COLKEY = "RIB_DOMICIL";
	public static final String RIB_NUM_COLKEY = "RIB_NUM";
	public static final String RIB_TITCO_COLKEY = "RIB_TITCO";
	public static final String RIB_VALIDE_COLKEY = "RIB_VALIDE";

	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";


	// Relationships
	public static final String FOURNISSEUR_KEY = "fournisseur";



	// Accessors methods
  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String ribBic() {
    return (String) storedValueForKey(RIB_BIC_KEY);
  }

  public void setRibBic(String value) {
    takeStoredValueForKey(value, RIB_BIC_KEY);
  }

  public String ribCle() {
    return (String) storedValueForKey(RIB_CLE_KEY);
  }

  public void setRibCle(String value) {
    takeStoredValueForKey(value, RIB_CLE_KEY);
  }

  public String ribCodBanc() {
    return (String) storedValueForKey(RIB_COD_BANC_KEY);
  }

  public void setRibCodBanc(String value) {
    takeStoredValueForKey(value, RIB_COD_BANC_KEY);
  }

  public String ribGuich() {
    return (String) storedValueForKey(RIB_GUICH_KEY);
  }

  public void setRibGuich(String value) {
    takeStoredValueForKey(value, RIB_GUICH_KEY);
  }

  public String ribIban() {
    return (String) storedValueForKey(RIB_IBAN_KEY);
  }

  public void setRibIban(String value) {
    takeStoredValueForKey(value, RIB_IBAN_KEY);
  }

  public String ribLib() {
    return (String) storedValueForKey(RIB_LIB_KEY);
  }

  public void setRibLib(String value) {
    takeStoredValueForKey(value, RIB_LIB_KEY);
  }

  public String ribNum() {
    return (String) storedValueForKey(RIB_NUM_KEY);
  }

  public void setRibNum(String value) {
    takeStoredValueForKey(value, RIB_NUM_KEY);
  }

  public String ribTitco() {
    return (String) storedValueForKey(RIB_TITCO_KEY);
  }

  public void setRibTitco(String value) {
    takeStoredValueForKey(value, RIB_TITCO_KEY);
  }

  public String ribValide() {
    return (String) storedValueForKey(RIB_VALIDE_KEY);
  }

  public void setRibValide(String value) {
    takeStoredValueForKey(value, RIB_VALIDE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  

  public static EORib createRib(EOEditingContext editingContext, Integer fouOrdre
, String ribLib
, String ribTitco
, String ribValide
, org.cocktail.maracuja.client.metier.EOFournisseur fournisseur) {
    EORib eo = (EORib) createAndInsertInstance(editingContext, _EORib.ENTITY_NAME);    
		eo.setFouOrdre(fouOrdre);
		eo.setRibLib(ribLib);
		eo.setRibTitco(ribTitco);
		eo.setRibValide(ribValide);
    eo.setFournisseurRelationship(fournisseur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORib.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORib.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORib creerInstance(EOEditingContext editingContext) {
		  		EORib object = (EORib)createAndInsertInstance(editingContext, _EORib.ENTITY_NAME);
		  		return object;
			}


		
  	  public EORib localInstanceIn(EOEditingContext editingContext) {
	  		return (EORib)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORib localInstanceIn(EOEditingContext editingContext, EORib eo) {
    EORib localInstance = (eo == null) ? null : (EORib)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORib#localInstanceIn a la place.
   */
	public static EORib localInstanceOf(EOEditingContext editingContext, EORib eo) {
		return EORib.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORib fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORib fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORib eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORib)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORib fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORib fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORib eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORib)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORib fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORib eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORib ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORib fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
