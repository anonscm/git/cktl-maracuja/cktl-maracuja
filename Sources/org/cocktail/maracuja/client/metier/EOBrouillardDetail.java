/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;


public class EOBrouillardDetail extends _EOBrouillardDetail {

	public static final EOQualifier QUAL_DEBITS = new EOKeyValueQualifier(EOBrouillardDetail.BROD_SENS_KEY, EOQualifier.QualifierOperatorEqual, IConst.SENS_DEBIT);
	public static final EOQualifier QUAL_CREDITS = new EOKeyValueQualifier(EOBrouillardDetail.BROD_SENS_KEY, EOQualifier.QualifierOperatorEqual, IConst.SENS_CREDIT);

	private EOPlanComptableExer planComptableExer;

	public EOPlanComptableExer getPlanComptableExer() {
		return planComptableExer;
	}

	public void setPlanComptableExer(EOPlanComptableExer planComptableExer) {
		this.planComptableExer = planComptableExer;
	}

	public EOBrouillardDetail() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (toBrouillard() == null) {
			throw new NSValidation.ValidationException("Le brouillard doit etre renseigné.");
		}
		if ((IConst.SENS_CREDIT.equals(brodSens()) && brodDebit().doubleValue() != 0) || (IConst.SENS_DEBIT.equals(brodSens()) && brodCredit().doubleValue() != 0)) {
			throw new NSValidation.ValidationException("Sens (D/C) incohérent avec montants saisis en débit/crédit.");
		}

		if (toBrouillard().isVise() && toEcritureDetail() == null) {
			throw new NSValidation.ValidationException("Un detail d'ecriture doit être relié au détail du brouillard si celui-ci est accepté.");
		}
		if (!toBrouillard().isVise() && toEcritureDetail() != null) {
			throw new NSValidation.ValidationException("Un detail d'ecriture ne peut être relié au détail du brouillard si celui-ci n'est pas accepté.");
		}

		//verifier code gestion actif pour exercice
		EOQualifier qual1 = new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, toBrouillard().toExercice());
		EOQualifier qual2 = new EOKeyValueQualifier(EOGestionExercice.GESTION_KEY, EOQualifier.QualifierOperatorEqual, toGestion());
		if (EOGestionExercice.fetchAll(editingContext(), new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})), null).count() == 0) {
			throw new NSValidation.ValidationException("Le code gestion " + toGestion().gesCode() + " n'est pas valide pour l'exercice " + toBrouillard().toExercice().exeExercice());
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public EOEcritureDetail creerEcritureDetail(EOEcriture ecriture) throws Exception {
		//le brouillard doit etre en attente
		if (!toBrouillard().isAttente()) {
			throw new Exception("Le brouillard n'est pas à l'état ATTENTE (" + toBrouillard().broEtat() + ")");
		}
		checkCoherence();
		//EOPlanComptableExer pco = EOPlanComptableExer.getCompte(editingContext(), toBrouillard().toExercice(), brodPcoNum());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(IConst.WANT_EO_LOGS);

		BigDecimal ecdMontant = (IConst.SENS_DEBIT.equals(brodSens()) ? brodDebit() : brodCredit());

		EOEcritureDetail res = maFactoryEcritureDetail.creerEcritureDetail(editingContext(), brodCommentaire(), brodIndex(), brodLibelle(), ecdMontant, brodPostit(), ecdMontant.abs(), null, brodSens(), ecriture, toBrouillard().toExercice(), toGestion(), getPlanComptableExer().planComptable());
		setToEcritureDetailRelationship(res);
		return res;
	}

	public boolean isCompteExisteDansPlancomptableExer() throws Exception {
		if (getCompteDansPlanComptableExer() == null) {
			return false;
		}
		return true;
	}

	public EOPlanComptableExer getCompteDansPlanComptableExer() throws Exception {
		if (brodPcoNum() == null) {
			throw new NSValidation.ValidationException("Le compte n'est pas spécifié dans le brouillard");
		}
		EOPlanComptableExer pco = EOPlanComptableExer.getCompte(editingContext(), toBrouillard().toExercice(), brodPcoNum());
		return pco;
	}

	public void checkCoherence() throws NSValidation.ValidationException {
		//le compte doit etre valide sur l'exercice
		//		if (brodPcoNum() == null) {
		//			throw new NSValidation.ValidationException("Le compte n'est pas spécifié dans le brouillard");
		//		}
		if (getPlanComptableExer() == null) {
			throw new NSValidation.ValidationException("Le planComptableExer doit etre renseigné.");
		}
		if (!brodPcoNum().equals(getPlanComptableExer().pcoNum())) {
			throw new NSValidation.ValidationException("Numero de compte (" + brodPcoNum() + ") et planComptableExer (" + getPlanComptableExer().pcoNum() + ") incohérents");
		}
		if (!getPlanComptableExer().isValide()) {
			throw new NSValidation.ValidationException("Le compte " + brodPcoNum() + " existe mais n'est pas valide pas dans le plan comptable pour l'exercice " + toBrouillard().toExercice().exeExercice());
		}
	}

	public void initPlanComptableExer(EOEditingContext ec) {
		if (brodPcoNum() != null) {
			setPlanComptableExer(EOPlanComptableExer.getCompte(editingContext(), toBrouillard().toExercice(), brodPcoNum()));
		}
	}

	public static EOBrouillardDetail creer(EOEditingContext editingContext, EOBrouillard brouillard, Integer index, EOGestion gestion, String pcoNum, String pcoLibelle, BigDecimal debit, BigDecimal credit, String sens, String libelle, String postIt, String commentaire) {
		EOBrouillardDetail brod = EOBrouillardDetail.creerInstance(editingContext);
		brod.setBrodIndex(index);
		brod.setToGestionRelationship(gestion);
		brod.setBrodPcoNum(pcoNum);
		brod.setBrodPcoLibelle(pcoLibelle);
		brod.setBrodDebit(debit);
		brod.setBrodCredit(credit);
		brod.setBrodSens(sens);
		brod.setBrodLibelle(libelle);
		brod.setBrodPostit(postIt);
		brod.setBrodCommentaire(commentaire);
		return brod;
	}
}
