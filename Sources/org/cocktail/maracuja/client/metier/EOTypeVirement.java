/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOTypeVirement.java
// 
package org.cocktail.maracuja.client.metier;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTypeVirement extends _EOTypeVirement {
	public final static String MOD_DOM_CAISSE = "CAISSE";
	public final static String MOD_DOM_CHEQUE = "CHEQUE";
	public final static String MOD_DOM_VIREMENT = "VIREMENT";

	public final static String TVI_FORMAT_BDF = "BDF";
	public final static String TVI_FORMAT_ETEBAC = "ETEBAC";
	public final static String TVI_FORMAT_VINT = "VINT";
	public final static String TVI_FORMAT_SEPASCT = "SEPASCT";

	public final static String ETAT_VALIDE = "O";
	public final static EOQualifier QUAL_VALIDE = new EOKeyValueQualifier(EOTypeVirement.TVI_VALIDITE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeVirement.ETAT_VALIDE);
	public final static EOQualifier QUAL_DOM_VIREMENT = new EOKeyValueQualifier(EOTypeVirement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, EOTypeVirement.MOD_DOM_VIREMENT);
	public final static EOQualifier QUAL_HAS_VIREMENTPARAMS = new EOOrQualifier(new NSArray(new Object[] {
			new EOKeyValueQualifier(EOTypeVirement.TO_VIREMENT_PARAM_BDFS_KEY + "." + EOVirementParamBdf.VPB_ETAT_KEY, EOQualifier.QualifierOperatorNotEqual, null),
			new EOKeyValueQualifier(EOTypeVirement.TO_VIREMENT_PARAM_SEPAS_KEY + "." + EOVirementParamSepa.VPS_ETAT_KEY, EOQualifier.QualifierOperatorNotEqual, null)
	}));



	public EOTypeVirement() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public boolean isSepaSct() {
		return EOTypeVirement.TVI_FORMAT_SEPASCT.equals(tviFormat());
	}

	public boolean isBdf() {
		return EOTypeVirement.TVI_FORMAT_BDF.equals(tviFormat());
	}

	public String getFileExtension() {
		if (isSepaSct()) {
			return "xml";
		}
		else if (isBdf()) {
			return "txt";
		}
		return "txt";
	}

}
