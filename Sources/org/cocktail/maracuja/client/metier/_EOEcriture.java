// _EOEcriture.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcriture.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEcriture extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Ecriture";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ecriture";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ecrOrdre";

	public static final String ECR_DATE_KEY = "ecrDate";
	public static final String ECR_DATE_SAISIE_KEY = "ecrDateSaisie";
	public static final String ECR_ETAT_KEY = "ecrEtat";
	public static final String ECR_LIBELLE_KEY = "ecrLibelle";
	public static final String ECR_NUMERO_KEY = "ecrNumero";
	public static final String ECR_NUMERO_BROUILLARD_KEY = "ecrNumeroBrouillard";
	public static final String ECR_POSTIT_KEY = "ecrPostit";

// Attributs non visibles
	public static final String BRO_ORDRE_KEY = "broOrdre";
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String TJO_ORDRE_KEY = "tjoOrdre";
	public static final String TOP_ORDRE_KEY = "topOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ECR_DATE_COLKEY = "ecr_Date";
	public static final String ECR_DATE_SAISIE_COLKEY = "ecr_Date_Saisie";
	public static final String ECR_ETAT_COLKEY = "ecr_etat";
	public static final String ECR_LIBELLE_COLKEY = "ecr_Libelle";
	public static final String ECR_NUMERO_COLKEY = "ecr_numero";
	public static final String ECR_NUMERO_BROUILLARD_COLKEY = "ecr_numero_Brouillard";
	public static final String ECR_POSTIT_COLKEY = "ecr_Postit";

	public static final String BRO_ORDRE_COLKEY = "BRO_ORDRE";
	public static final String COM_ORDRE_COLKEY = "com_ordre";
	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_ordre";
	public static final String TJO_ORDRE_COLKEY = "TJO_ORDRE";
	public static final String TOP_ORDRE_COLKEY = "TOP_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String BROUILLARD_KEY = "brouillard";
	public static final String COMPTABILITE_KEY = "comptabilite";
	public static final String DETAIL_ECRITURE_KEY = "detailEcriture";
	public static final String ECRITURE_MONTANT_KEY = "ecritureMontant";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORIGINE_KEY = "origine";
	public static final String TYPE_JOURNAL_KEY = "typeJournal";
	public static final String TYPE_OPERATION_KEY = "typeOperation";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp ecrDate() {
    return (NSTimestamp) storedValueForKey(ECR_DATE_KEY);
  }

  public void setEcrDate(NSTimestamp value) {
    takeStoredValueForKey(value, ECR_DATE_KEY);
  }

  public NSTimestamp ecrDateSaisie() {
    return (NSTimestamp) storedValueForKey(ECR_DATE_SAISIE_KEY);
  }

  public void setEcrDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ECR_DATE_SAISIE_KEY);
  }

  public String ecrEtat() {
    return (String) storedValueForKey(ECR_ETAT_KEY);
  }

  public void setEcrEtat(String value) {
    takeStoredValueForKey(value, ECR_ETAT_KEY);
  }

  public String ecrLibelle() {
    return (String) storedValueForKey(ECR_LIBELLE_KEY);
  }

  public void setEcrLibelle(String value) {
    takeStoredValueForKey(value, ECR_LIBELLE_KEY);
  }

  public Integer ecrNumero() {
    return (Integer) storedValueForKey(ECR_NUMERO_KEY);
  }

  public void setEcrNumero(Integer value) {
    takeStoredValueForKey(value, ECR_NUMERO_KEY);
  }

  public Integer ecrNumeroBrouillard() {
    return (Integer) storedValueForKey(ECR_NUMERO_BROUILLARD_KEY);
  }

  public void setEcrNumeroBrouillard(Integer value) {
    takeStoredValueForKey(value, ECR_NUMERO_BROUILLARD_KEY);
  }

  public String ecrPostit() {
    return (String) storedValueForKey(ECR_POSTIT_KEY);
  }

  public void setEcrPostit(String value) {
    takeStoredValueForKey(value, ECR_POSTIT_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOBrouillard brouillard() {
    return (org.cocktail.maracuja.client.metier.EOBrouillard)storedValueForKey(BROUILLARD_KEY);
  }

  public void setBrouillardRelationship(org.cocktail.maracuja.client.metier.EOBrouillard value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBrouillard oldValue = brouillard();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BROUILLARD_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BROUILLARD_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOComptabilite comptabilite() {
    return (org.cocktail.maracuja.client.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
  }

  public void setComptabiliteRelationship(org.cocktail.maracuja.client.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOComptabilite oldValue = comptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOEcritureMontant ecritureMontant() {
    return (org.cocktail.maracuja.client.metier.EOEcritureMontant)storedValueForKey(ECRITURE_MONTANT_KEY);
  }

  public void setEcritureMontantRelationship(org.cocktail.maracuja.client.metier.EOEcritureMontant value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOEcritureMontant oldValue = ecritureMontant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_MONTANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_MONTANT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOrigine origine() {
    return (org.cocktail.maracuja.client.metier.EOOrigine)storedValueForKey(ORIGINE_KEY);
  }

  public void setOrigineRelationship(org.cocktail.maracuja.client.metier.EOOrigine value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOrigine oldValue = origine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeJournal typeJournal() {
    return (org.cocktail.maracuja.client.metier.EOTypeJournal)storedValueForKey(TYPE_JOURNAL_KEY);
  }

  public void setTypeJournalRelationship(org.cocktail.maracuja.client.metier.EOTypeJournal value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeJournal oldValue = typeJournal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_JOURNAL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_JOURNAL_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeOperation typeOperation() {
    return (org.cocktail.maracuja.client.metier.EOTypeOperation)storedValueForKey(TYPE_OPERATION_KEY);
  }

  public void setTypeOperationRelationship(org.cocktail.maracuja.client.metier.EOTypeOperation value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeOperation oldValue = typeOperation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_OPERATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_OPERATION_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray detailEcriture() {
    return (NSArray)storedValueForKey(DETAIL_ECRITURE_KEY);
  }

  public NSArray detailEcriture(EOQualifier qualifier) {
    return detailEcriture(qualifier, null, false);
  }

  public NSArray detailEcriture(EOQualifier qualifier, boolean fetch) {
    return detailEcriture(qualifier, null, fetch);
  }

  public NSArray detailEcriture(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOEcritureDetail.ECRITURE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOEcritureDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = detailEcriture();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToDetailEcritureRelationship(org.cocktail.maracuja.client.metier.EOEcritureDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DETAIL_ECRITURE_KEY);
  }

  public void removeFromDetailEcritureRelationship(org.cocktail.maracuja.client.metier.EOEcritureDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_ECRITURE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOEcritureDetail createDetailEcritureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EcritureDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DETAIL_ECRITURE_KEY);
    return (org.cocktail.maracuja.client.metier.EOEcritureDetail) eo;
  }

  public void deleteDetailEcritureRelationship(org.cocktail.maracuja.client.metier.EOEcritureDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DETAIL_ECRITURE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDetailEcritureRelationships() {
    Enumeration objects = detailEcriture().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDetailEcritureRelationship((org.cocktail.maracuja.client.metier.EOEcritureDetail)objects.nextElement());
    }
  }


  public static EOEcriture createEcriture(EOEditingContext editingContext, NSTimestamp ecrDate
, NSTimestamp ecrDateSaisie
, String ecrEtat
, String ecrLibelle
, org.cocktail.maracuja.client.metier.EOComptabilite comptabilite, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOTypeJournal typeJournal, org.cocktail.maracuja.client.metier.EOTypeOperation typeOperation, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOEcriture eo = (EOEcriture) createAndInsertInstance(editingContext, _EOEcriture.ENTITY_NAME);    
		eo.setEcrDate(ecrDate);
		eo.setEcrDateSaisie(ecrDateSaisie);
		eo.setEcrEtat(ecrEtat);
		eo.setEcrLibelle(ecrLibelle);
    eo.setComptabiliteRelationship(comptabilite);
    eo.setExerciceRelationship(exercice);
    eo.setTypeJournalRelationship(typeJournal);
    eo.setTypeOperationRelationship(typeOperation);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEcriture.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEcriture.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOEcriture creerInstance(EOEditingContext editingContext) {
		  		EOEcriture object = (EOEcriture)createAndInsertInstance(editingContext, _EOEcriture.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOEcriture localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcriture)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEcriture localInstanceIn(EOEditingContext editingContext, EOEcriture eo) {
    EOEcriture localInstance = (eo == null) ? null : (EOEcriture)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEcriture#localInstanceIn a la place.
   */
	public static EOEcriture localInstanceOf(EOEditingContext editingContext, EOEcriture eo) {
		return EOEcriture.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEcriture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEcriture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcriture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcriture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEcriture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcriture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcriture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcriture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
