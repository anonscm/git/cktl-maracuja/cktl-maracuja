// _EORecouvrement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecouvrement.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecouvrement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Recouvrement";
	public static final String ENTITY_TABLE_NAME = "maracuja.recouvrement";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recoOrdre";

	public static final String RECO_DATE_CREATION_KEY = "recoDateCreation";
	public static final String RECO_MONTANT_KEY = "recoMontant";
	public static final String RECO_NB_KEY = "recoNb";
	public static final String RECO_NUMERO_KEY = "recoNumero";

// Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String RECO_ORDRE_KEY = "recoOrdre";
	public static final String TREC_ORDRE_KEY = "trecOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String RECO_DATE_CREATION_COLKEY = "reco_Date_Creation";
	public static final String RECO_MONTANT_COLKEY = "reco_Montant";
	public static final String RECO_NB_COLKEY = "reco_nb";
	public static final String RECO_NUMERO_COLKEY = "reco_Numero";

	public static final String COM_ORDRE_COLKEY = "com_Ordre";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String RECO_ORDRE_COLKEY = "reco_ORDRE";
	public static final String TREC_ORDRE_COLKEY = "trec_Ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String COMPTABILITE_KEY = "comptabilite";
	public static final String EXERCICE_KEY = "exercice";
	public static final String PRELEVEMENTS_KEY = "prelevements";
	public static final String TYPE_RECOUVREMENT_KEY = "typeRecouvrement";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp recoDateCreation() {
    return (NSTimestamp) storedValueForKey(RECO_DATE_CREATION_KEY);
  }

  public void setRecoDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, RECO_DATE_CREATION_KEY);
  }

  public java.math.BigDecimal recoMontant() {
    return (java.math.BigDecimal) storedValueForKey(RECO_MONTANT_KEY);
  }

  public void setRecoMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RECO_MONTANT_KEY);
  }

  public Integer recoNb() {
    return (Integer) storedValueForKey(RECO_NB_KEY);
  }

  public void setRecoNb(Integer value) {
    takeStoredValueForKey(value, RECO_NB_KEY);
  }

  public Integer recoNumero() {
    return (Integer) storedValueForKey(RECO_NUMERO_KEY);
  }

  public void setRecoNumero(Integer value) {
    takeStoredValueForKey(value, RECO_NUMERO_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOComptabilite comptabilite() {
    return (org.cocktail.maracuja.client.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
  }

  public void setComptabiliteRelationship(org.cocktail.maracuja.client.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOComptabilite oldValue = comptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeRecouvrement typeRecouvrement() {
    return (org.cocktail.maracuja.client.metier.EOTypeRecouvrement)storedValueForKey(TYPE_RECOUVREMENT_KEY);
  }

  public void setTypeRecouvrementRelationship(org.cocktail.maracuja.client.metier.EOTypeRecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeRecouvrement oldValue = typeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray prelevements() {
    return (NSArray)storedValueForKey(PRELEVEMENTS_KEY);
  }

  public NSArray prelevements(EOQualifier qualifier) {
    return prelevements(qualifier, null, false);
  }

  public NSArray prelevements(EOQualifier qualifier, boolean fetch) {
    return prelevements(qualifier, null, fetch);
  }

  public NSArray prelevements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOPrelevement.RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOPrelevement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prelevements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrelevementsRelationship(org.cocktail.maracuja.client.metier.EOPrelevement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRELEVEMENTS_KEY);
  }

  public void removeFromPrelevementsRelationship(org.cocktail.maracuja.client.metier.EOPrelevement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOPrelevement createPrelevementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Prelevement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRELEVEMENTS_KEY);
    return (org.cocktail.maracuja.client.metier.EOPrelevement) eo;
  }

  public void deletePrelevementsRelationship(org.cocktail.maracuja.client.metier.EOPrelevement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRELEVEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrelevementsRelationships() {
    Enumeration objects = prelevements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrelevementsRelationship((org.cocktail.maracuja.client.metier.EOPrelevement)objects.nextElement());
    }
  }


  public static EORecouvrement createRecouvrement(EOEditingContext editingContext, NSTimestamp recoDateCreation
, java.math.BigDecimal recoMontant
, Integer recoNb
, Integer recoNumero
, org.cocktail.maracuja.client.metier.EOComptabilite comptabilite, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOTypeRecouvrement typeRecouvrement, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EORecouvrement eo = (EORecouvrement) createAndInsertInstance(editingContext, _EORecouvrement.ENTITY_NAME);    
		eo.setRecoDateCreation(recoDateCreation);
		eo.setRecoMontant(recoMontant);
		eo.setRecoNb(recoNb);
		eo.setRecoNumero(recoNumero);
    eo.setComptabiliteRelationship(comptabilite);
    eo.setExerciceRelationship(exercice);
    eo.setTypeRecouvrementRelationship(typeRecouvrement);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecouvrement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecouvrement.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecouvrement creerInstance(EOEditingContext editingContext) {
		  		EORecouvrement object = (EORecouvrement)createAndInsertInstance(editingContext, _EORecouvrement.ENTITY_NAME);
		  		return object;
			}


		
  	  public EORecouvrement localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecouvrement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORecouvrement localInstanceIn(EOEditingContext editingContext, EORecouvrement eo) {
    EORecouvrement localInstance = (eo == null) ? null : (EORecouvrement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecouvrement#localInstanceIn a la place.
   */
	public static EORecouvrement localInstanceOf(EOEditingContext editingContext, EORecouvrement eo) {
		return EORecouvrement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORecouvrement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecouvrement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecouvrement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecouvrement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecouvrement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecouvrement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecouvrement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecouvrement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecouvrement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecouvrement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
