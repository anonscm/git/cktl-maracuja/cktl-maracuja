/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOOrigine.java
// 
package org.cocktail.maracuja.client.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOOrigine extends _EOOrigine {

	public EOOrigine() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static EOAccordsContrat getAccordContratFromOrigine(EOEditingContext ec, EOOrigine origine) {
		if (origine == null) {
			return null;
		}
		EOAccordsContrat res = null;
		String maskExer = "YYYY";
		String maskConIndex = "xxxx";
		String mask = maskExer + "-" + maskConIndex;
		if (origine.oriLibelle() != null) {
			String exercice = null;
			String conIndex = null;
			String s = origine.oriLibelle();
			if (s.length() >= mask.length()) {
				s = s.substring(0, mask.length());
				if (s.substring(maskExer.length(), maskExer.length() + 1).equals("-")) {
					exercice = s.substring(0, maskExer.length());
					conIndex = s.substring(maskExer.length() + 1, maskExer.length() + 1 + maskConIndex.length());
				}
			}

			if (exercice != null && conIndex != null) {
				try {
					Integer iexer = Integer.valueOf(exercice);
					Integer iconindex = Integer.valueOf(conIndex);
					EOQualifier qual1 = new EOKeyValueQualifier(EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, iexer);
					EOQualifier qual2 = new EOKeyValueQualifier(EOAccordsContrat.CON_INDEX_KEY, EOQualifier.QualifierOperatorEqual, iconindex);
					res = EOAccordsContrat.fetchFirstByQualifier(ec, new EOAndQualifier(new NSArray(new Object[] {
							qual1, qual2
					})));
				} catch (NumberFormatException e) {
					//Dans le cas ou oriLibelle est <> exercice - index
					e.printStackTrace();
					return res;
				}
			}
		}
		return res;
	}

}
