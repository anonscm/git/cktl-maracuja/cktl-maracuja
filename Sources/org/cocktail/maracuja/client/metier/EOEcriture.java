/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOEcriture.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOEcriture extends _EOEcriture {
	private static final String MSG_VALIDATION_ECRITURE3 = ", C=";
	private static final String MSG_VALIDATION_ECRITURE2 = ") : D=";
	private static final String MSG_VALIDATION_ECRITURE1 = "        - Validation Ecriture avant enregistrement OK : ecriture equilibree (";

	public static final EOSortOrdering SORT_ECR_NUMERO_ASC = EOSortOrdering.sortOrderingWithKey(EOEcriture.ECR_NUMERO_KEY, EOSortOrdering.CompareAscending);

	public static final String[] entitesEnRelation = new String[] {
			"Brouillard", "Comptabilite", "EcritureDetail", "Exercice", "Origine", "TypeJournal", "TypeOperation", "Utilisateur"
	};
	public static final String ecritureValide = "VALIDE";
	public static final String ecritureAnnule = "ANNULE";
	public static final String ecritureBrouillard = "ATTENTE";
	public static final String tableName = "Ecriture";

	public static final int NUMERO_ECRITURE_TMP_SOLDE_6_7 = 99999999;

	public EOEcriture() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (exercice().estClos()) {
			throw new NSValidation.ValidationException("L'exercice " + exercice().exeExercice() + " est clos, impossible de saisir une ecriture.");
		}

		if (this.ecrNumero() == null) {
			if (this.ecrNumeroBrouillard() == null) {
				throw new NSValidation.ValidationException("    ecrNumero et ecrNumeroBrouillard ne peuvent pas etre null ");
			}

			if (ecritureValide.equals(this.ecrEtat())) {
				throw new NSValidation.ValidationException("    ecrNumero doir etre renseigne si l ecrEtat est VALIDE ");
			}
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		//On s'assure qu'il y a un detail
		if (this.detailEcriture() == null || this.detailEcriture().count() < 2) {
			throw new NSValidation.ValidationException("        L'ecriture doit avoir au moins deux details. (" + ecrLibelle() + ")");
		}

		//Verifier que l'ecriture est equilibree (debit=credit) ou (somme montants=0)
		final BigDecimal debits = Factory.calcSommeOfBigDecimals(detailEcriture(), EOEcritureDetail.ECD_DEBIT_KEY);
		final BigDecimal credits = Factory.calcSommeOfBigDecimals(detailEcriture(), EOEcritureDetail.ECD_CREDIT_KEY);
		final BigDecimal montants = Factory.calcSommeOfBigDecimals(detailEcriture(), EOEcritureDetail.ECD_MONTANT_KEY);
		//final BigDecimal montant0 = new BigDecimal(0).setScale(2);
		if (debits.add(credits.negate()).doubleValue() != (double) 0) {
			throw new NSValidation.ValidationException("        L'ecriture n'est pas equilibree (" + ecrLibelle() + MSG_VALIDATION_ECRITURE2 + debits + MSG_VALIDATION_ECRITURE3 + credits + ", D+C=" + montants);
		}
		System.out.println(MSG_VALIDATION_ECRITURE1 + ecrLibelle() + MSG_VALIDATION_ECRITURE2 + debits + MSG_VALIDATION_ECRITURE3 + credits);
	}

	/**
	 * @return Un tableau des planComptables utilisés dans l'écriture.
	 */
	public final NSArray getEcriturePlanComptables() {
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < detailEcriture().count(); i++) {
			final EOEcritureDetail element = (EOEcritureDetail) detailEcriture().objectAtIndex(i);
			if (res.indexOfObject(element.planComptable()) == NSArray.NotFound) {
				res.addObject(element.planComptable());
			}
		}
		return res;
	}

	/**
	 * Vérifie si une écriture peut être rectifiée (les comptes utilisés ne sont pas de la classe 6 ou 7).
	 * 
	 * @return true si l'ecriture peut être rectifiée.
	 */
	public final boolean isEcritureRectifiable() {
		NSArray plancos = getEcriturePlanComptables();
		for (int i = 0; i < plancos.count(); i++) {
			final EOPlanComptable element = (EOPlanComptable) plancos.objectAtIndex(i);
			if (EOPlanComptable.CLASSE6.equals(element.getClasseCompte()) || EOPlanComptable.CLASSE7.equals(element.getClasseCompte())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Vérifie si une écriture peut être annulée (les comptes utilisés ne sont pas de la classe 6 ou 7).
	 * 
	 * @return true si l'ecriture peut être rectifiée.
	 */
	public final boolean isEcritureAnnulable() {
		NSArray plancos = getEcriturePlanComptables();
		for (int i = 0; i < plancos.count(); i++) {
			final EOPlanComptable element = (EOPlanComptable) plancos.objectAtIndex(i);
			if (EOPlanComptable.CLASSE6.equals(element.getClasseCompte()) || EOPlanComptable.CLASSE7.equals(element.getClasseCompte())) {
				return false;
			}
		}
		return true;
	}

	public final boolean isEcritureEquilibree() {
		final BigDecimal sommeDebits = getDebitsMontant();
		final BigDecimal sommeCredits = getCreditsMontant();

		if (((sommeDebits == null) || (sommeDebits.compareTo(sommeCredits) != 0))) {
			return false;
		}
		return true;
	}

	/**
	 * @see org.cocktail.maracuja.client.modele.Ecriture#setEcrLibelle(java.lang.String)
	 */
	public void setEcrLibelle(String value) {
		if (value != null) {
			if (value.length() > 200) {
				value = value.substring(0, 200);
			}
		}
		super.setEcrLibelle(value);
	}

	/**
	 * @return Le montant de l'écriture (la somme des débits).
	 */
	public final BigDecimal getMontant() {
		return ecritureMontant().ecrMontant();
		//        return Factory.calcSommeOfBigDecimals(detailEcriture(), "ecdDebit");
	}

	public BigDecimal getDebitsMontant() {
		return ZEOUtilities.calcSommeOfBigDecimals(getDebits(), EOEcritureDetail.ECD_DEBIT_KEY);
	}

	public BigDecimal getCreditsMontant() {
		return ZEOUtilities.calcSommeOfBigDecimals(getCredits(), EOEcritureDetail.ECD_CREDIT_KEY);
	}

	public NSArray getDebits() {
		NSArray res = EOQualifier.filteredArrayWithQualifier(detailEcriture(), EOEcritureDetail.QUAL_DEBITS);
		return res;
	}

	public NSArray getCredits() {
		NSArray res = EOQualifier.filteredArrayWithQualifier(detailEcriture(), EOEcritureDetail.QUAL_CREDITS);
		return res;
	}

	/**
	 * Crée un minimum d'écritures à patrir d'un tableau de plusieurs écritures portant sur le même compte et le même sens. Les ecritures SACD sont
	 * préservées. Les détail d'écritures portant sur les mêmes comptes indiqués et sur le sens sont sommées. Ne fonctionne que sur des objets en
	 * mémoire.
	 * 
	 * @param ed
	 * @param ecritures
	 * @param pcoNums
	 * @param sens
	 * @param libelle
	 * @param mandats
	 * @param odps
	 * @param dateEcriture
	 * @return
	 */
	public static NSArray creerMinEcritureFrom(EOEditingContext ed, NSArray ecritures, NSArray pcoNums, String sens, String libelle, NSArray mandats, NSArray odps, NSTimestamp dateEcriture) {

		//On assemble en une seule écriture toutes les ecrituresDetail passés sur le code de gestion
		//logiquement les SACD restent à part, les retenues sont incluses.

		NSMutableArray ecrituresToKeep = new NSMutableArray();
		NSMutableArray ecrituresToMerge = new NSMutableArray();
		NSMutableArray qualsPcoNum = new NSMutableArray();

		for (int i = 0; i < pcoNums.count(); i++) {
			EOPlanComptableExer pco = (EOPlanComptableExer) pcoNums.objectAtIndex(i);
			qualsPcoNum.addObject(new EOKeyValueQualifier(EOEcritureDetail.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pco.pcoNum()));
		}

		EOQualifier qualPcoNum = new EOOrQualifier(qualsPcoNum);
		EOQualifier qualIsSacd = new EOKeyValueQualifier(EOEcritureDetail.IS_SACD_KEY, EOQualifier.QualifierOperatorEqual, Boolean.TRUE);
		EOQualifier qualSens = new EOKeyValueQualifier(EOEcritureDetail.ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, sens);
		Enumeration<EOEcriture> enumEcr = ecritures.objectEnumerator();

		//On balaye les ecritures a traiter pour separer celles à garder et celles qu'il faudra merger
		// toutes les écritures qui ne contiennent pas de lignes sur un sacd et qui contiennent une ligne sur un des comptes passé en paramètre doit être mergée 
		while (enumEcr.hasMoreElements()) {
			EOEcriture eoEcriture = (EOEcriture) enumEcr.nextElement();
			//si le compte est utilisé dans l'écriture
			NSArray dets = eoEcriture.detailEcriture(new EOAndQualifier(new NSArray(new Object[] {
					qualPcoNum, qualSens
			})), false);
			NSArray detsSaccd = eoEcriture.detailEcriture(qualIsSacd, false);
			if (dets.count() > 0 && detsSaccd.count() == 0) {
				ecrituresToMerge.addObject(eoEcriture);
			}
			else {
				ecrituresToKeep.addObject(eoEcriture);
			}
		}

		NSMutableArray ecrituresRes = new NSMutableArray();
		ecrituresRes.addObjectsFromArray(ecrituresToKeep);

		int ecdIndex = 1;
		//Regrouper les écritures details en une écriture
		if (ecrituresToMerge.count() > 0) {
			//On prend une uecriture de reference au hasard
			EOEcriture ecrRef = (EOEcriture) ecrituresToMerge.objectAtIndex(0);
			//On crée une nouvelle écriture  (si l'ecriture de ref a un eoorigine, on ne le garde pas)
			FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(ApplicationClient.wantShowTrace(), dateEcriture);
			EOEcriture nouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, ecrRef.ecrDateSaisie(), libelle, ecrRef.ecrNumero(), null, ecrRef.comptabilite(), ecrRef.exercice(), null, ecrRef
					.typeJournal(), ecrRef.typeOperation(), ecrRef.utilisateur());
			ecrituresRes.addObject(nouvelleEcriture);

			//chaque ecrituredetail à merger est basculeée sur la nouvelle ecriture, on supprime donc l'écriture initiale
			Enumeration<EOEcriture> enumEcrToMerge = ecrituresToMerge.objectEnumerator();
			while (enumEcrToMerge.hasMoreElements()) {
				EOEcriture eoEcriture = (EOEcriture) enumEcrToMerge.nextElement();
				while (eoEcriture.detailEcriture().count() > 0) {
					EOEcritureDetail eoEcritureDetail = (EOEcritureDetail) eoEcriture.detailEcriture().objectAtIndex(0);
					eoEcritureDetail.setEcdIndex(ecdIndex++);
					eoEcritureDetail.setEcritureRelationship(nouvelleEcriture);
				}
				ed.deleteObject(eoEcriture);
			}

			//Créer un ecritureDetail pour tous les ecritureDetail de même compte
			for (int i = 0; i < pcoNums.count(); i++) {
				EOPlanComptableExer pco = (EOPlanComptableExer) pcoNums.objectAtIndex(i);
				EOQualifier qualPcoNum2 = new EOKeyValueQualifier(EOEcritureDetail.PLAN_COMPTABLE_KEY + "." + EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pco.pcoNum());
				//On filtre les ecritures detail passés sur le pconum
				NSArray ecdsPcoNum = nouvelleEcriture.detailEcriture(new EOAndQualifier(new NSArray(new Object[] {
						qualPcoNum2, qualSens
				})), false);
				//on crée une ligne d'ecriture sur le compte en cours avec montant 0 
				FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(ApplicationClient.wantShowTrace());
				EOGestion gestion = ((EOEcritureDetail) ecdsPcoNum.objectAtIndex(0)).gestion();
				EOEcritureDetail newEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), libelle, ZConst.BIGDECIMAL_ZERO, "", ZConst.BIGDECIMAL_ZERO, null, sens, nouvelleEcriture, nouvelleEcriture
						.exercice(), gestion, pco.planComptable());

				//on balaye les ligne d'écriture à merger sur le compte en cours et on met à jour les montant de la nouvelle ligne
				for (int j = ecdsPcoNum.count() - 1; j >= 0; j--) {
					EOEcritureDetail eoEcritureDetail = (EOEcritureDetail) ecdsPcoNum.objectAtIndex(j);

					if (EOEcritureDetail.SENS_DEBIT.equals(sens)) {
						newEcritureDetail.setInitialDebit(newEcritureDetail.ecdDebit().add(eoEcritureDetail.ecdDebit()));
					}
					else {
						newEcritureDetail.setInitialCredit(newEcritureDetail.ecdCredit().add(eoEcritureDetail.ecdCredit()));
					}
					//memoriser le mandatDetailEcriture ou odpdetailecr (on change les references à la ligne initiale vers la nouvelle ligne)
					EOQualifier qualMandatEcds = new EOKeyValueQualifier(EOMandatDetailEcriture.ECRITURE_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, eoEcritureDetail);
					for (int k = 0; k < mandats.count(); k++) {
						EOMandat man = (EOMandat) mandats.objectAtIndex(k);
						NSArray res = man.mandatDetailEcritures(qualMandatEcds);
						if (res.count() > 0) {
							EOMandatDetailEcriture mde = ((EOMandatDetailEcriture) res.objectAtIndex(0));
							mde.setEcritureDetailRelationship(newEcritureDetail);
						}
					}

					EOQualifier qualOdpEcds = new EOKeyValueQualifier(EOOrdrePaiementDetailEcriture.ECRITURE_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, eoEcritureDetail);
					for (int k = 0; k < odps.count(); k++) {
						EOOrdreDePaiement man = (EOOrdreDePaiement) odps.objectAtIndex(k);
						NSArray res = man.ordrePaiementDetailEcritures(qualOdpEcds);
						if (res.count() > 0) {
							for (int l = 0; l < res.count(); l++) {
								EOOrdrePaiementDetailEcriture mde = (EOOrdrePaiementDetailEcriture) res.objectAtIndex(l);
								mde.setEcritureDetailRelationship(newEcritureDetail);
							}
							EOOrdrePaiementDetailEcriture mde = ((EOOrdrePaiementDetailEcriture) res.objectAtIndex(0));
							mde.setEcritureDetailRelationship(newEcritureDetail);
						}
					}

					//supprimer le ecrdetail initial
					eoEcritureDetail.removeObjectFromBothSidesOfRelationshipWithKey(eoEcritureDetail.ecriture(), EOEcritureDetail.ECRITURE_KEY);
					ed.deleteObject(eoEcritureDetail);
				}

			}

			//renumeroter les index
			NSArray edcs = nouvelleEcriture.detailEcriture(null, new NSArray(new Object[] {
					EOEcritureDetail.SORT_ECD_INDEX_ASC
			}), false);
			for (int i = 0; i < edcs.count(); i++) {
				((EOEcritureDetail) edcs.objectAtIndex(i)).setEcdIndex(Integer.valueOf(i));
			}
		}
		return ecrituresRes.immutableClone();

	}
}
