/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOBordereau.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOBordereau extends _EOBordereau {
	public static final String MONTANT_KEY = "montant";
	public static final String NB_KEY = "nb";
	public static final String BordereauValide = "VALIDE";
	public static final String BordereauVise = "VISE";
	public static final String BordereauAnnule = "ANNULE";
	public static final String BordereauAViser = "A VISER";
	public static final String BordereauEtatPaiement = "PAIEMENT";
	public static final String BordereauEtatRetenue = "RETENUES";
	public static final String BordereauEtatPaye = "PAYE";

	public static final String problemeBordereauDejaVise = "BORDEREAU DE CHEQUE  DEJA VISE !";
	public static final String problemeBordereauAnnule = "BORDEREAU DE CHEQUE  ANNULE !";
	public static final String problemeBordereauAViser = "BORDEREAU DE CHEQUE  A VISER !";

	public static final EOSortOrdering TRI_BOR_NUM_ASC = new EOSortOrdering(EOBordereau.BOR_NUM_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_DATE_CREATION_DESC = new EOSortOrdering(EOBordereau.BOR_DATE_CREATION_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_GES_CODE_ASC = new EOSortOrdering(EOBordereau.GES_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_BOR_NUM_ASC = TRI_BOR_NUM_ASC;

	// public static final String tableName       = "Bordereau";

	public EOBordereau() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return
	 */
	public String borLibelle() {
		if (bordereauInfo() != null) {
			return bordereauInfo().borLibelle();
		}
		return null;
	}

	public void setLibelle(String lib) {
		if (bordereauInfo() != null) {
			bordereauInfo().setBorLibelle(lib);
		}
	}

	public String gescodeAndNum() {
		return gestion().gesCode() + "/" + borNum();
	}

	/**
	 * @return true si le bordereau a des mandats associés.
	 */
	public boolean isBordereauDepenses() {
		return (mandats() != null && mandats().count() != 0);
	}

	/**
	 * @return true si le bordereau a des titres associes.
	 */
	public boolean isBordereauRecettes() {
		return (titres() != null && titres().count() != 0);
	}

	/**
	 * @return true si le bordereau a des cheques associes.
	 */
	public boolean isBordereauCheques() {
		return (cheques() != null && cheques().count() != 0);
	}

	private final BigDecimal calcSommeOfBigDecimals(final NSArray array, final String keyName) {
		BigDecimal res = new BigDecimal(0).setScale(2);
		for (int i = 0; i < array.count(); i++) {
			EOEnterpriseObject element = (EOEnterpriseObject) array.objectAtIndex(i);
			res = res.add((BigDecimal) element.valueForKey(keyName));
		}
		return res;
	}

	/**
	 * @return Le montant du bordereau (en fonction du type de bordereau)
	 * @see EOBordereau#isBordereauDepenses()
	 * @see EOBordereau#isBordereauRecettes()
	 * @see EOBordereau#isBordereauCheques()
	 */
	public BigDecimal getMontant() {
		if (isBordereauDepenses()) {
			return calcSommeOfBigDecimals(mandats(), "manTtc");
		}
		else if (isBordereauRecettes()) {
			return calcSommeOfBigDecimals(titres(), "titTtc");
		}
		else if (isBordereauBTBROUILLARD()) {
			return calcSommeOfBigDecimals(toBrouillards(), EOBrouillard.MONTANT_KEY);
		}
		else if (isBordereauCheques()) {
			BigDecimal res = new BigDecimal(0).setScale(2);
			NSArray array = cheques();
			for (int i = 0; i < array.count(); i++) {
				EOCheque element = (EOCheque) array.objectAtIndex(i);
				if (EOCheque.etatValide.equals(element.cheEtat())) {
					res = res.add(element.cheMontant());
				}
			}
			return res;
		}
		else if (isBordereauDroitsScol()) {
			return ZEOUtilities.calcSommeOfBigDecimals(bordereauBrouillards(), EOBordereauBrouillard.DEBIT_KEY);
		}
		else {
			return null;
		}
	}

	public Integer getNb() {
		if (isBordereauDepenses()) {
			return Integer.valueOf(mandats().count());
		}
		else if (isBordereauRecettes()) {
			return Integer.valueOf(titres().count());
		}
		else if (isBordereauCheques()) {
			return Integer.valueOf(cheques().count());
		}

		else {
			return null;
		}
	}

	/**
	 * @return true si tous les mandats associés au bordereau ont été payés ou pas (les madats rejetés ne sont pas pris en compte dans le résultat).
	 */
	public final boolean isBordereauDepensePaye() {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("manEtat<>%@", new NSArray(EOMandat.mandatAnnule));
		NSArray mans = EOQualifier.filteredArrayWithQualifier(mandats(), qual);
		if (mans.count() == 0) {
			return false;
		}
		boolean paye = true;
		for (int i = 0; i < mans.count() && paye; i++) {
			EOMandat element = (EOMandat) mans.objectAtIndex(i);
			paye = EOMandat.mandatVirement.equals(element.manEtat());
		}
		return paye;
	}

	public final Integer getNbMandats() {
		return new Integer(mandats().count());
	}

	public final Integer getNbTitres() {
		return new Integer(titres().count());
	}

	public boolean isBordereauBTME() {
		return EOTypeBordereau.TypeBordereauBTME.equals(typeBordereau().tboType());
	}

	public boolean isBordereauBTBROUILLARD() {
		return EOTypeBordereau.TYPEBORDEREAU_BROUILLARDS.equals(typeBordereau().tboType());
	}

	public boolean isBordereauDroitsScol() {
		return EOTypeBordereau.TypeBordereauDroitsUniversitaires.equals(typeBordereau().tboType());
	}

	public boolean isBordereauRembScol() {
		return EOTypeBordereau.TypeBordereauRembousrementsDroitsUniversitaires.equals(typeBordereau().tboType());
	}

	public NSArray allMandatBrouillards(EOQualifier qual) {
		if (isBordereauDepenses()) {
			NSMutableArray res = new NSMutableArray();
			for (int i = 0; i < mandats().count(); i++) {
				EOMandat mandat = (EOMandat) mandats().objectAtIndex(i);
				res.addObjectsFromArray(mandat.mandatBrouillards(qual));
			}
			return res.immutableClone();
		}
		return NSArray.EmptyArray;
	}

	public boolean isRejete() {
		return BordereauAnnule.equals(borEtat());
	}

	public boolean isValide() {
		return BordereauValide.equals(borEtat());
	}

	public boolean isVise() {
		return BordereauVise.equals(borEtat());
	}

	/**
	 * @return Les cheques valides triés par date de saisie.
	 */

	public final NSArray getChequesValides() {
		//		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("cheEtat=%@", new NSArray(EOCheque.etatValide));
		EOQualifier qual = new EOKeyValueQualifier(EOCheque.CHE_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOCheque.etatValide);
		NSArray res = EOQualifier.filteredArrayWithQualifier(this.cheques(), qual);
		res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				EOCheque.SORT_CHE_DATE_SAISIE_ASC
		}));
		return res;
	}

	public boolean isBordereauAExtourner() {
		return (this.mandats(new EOKeyValueQualifier(EOMandat.MODE_PAIEMENT_KEY + "." + EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, EOModePaiement.MODDOM_A_EXTOURNER)).count() > 0);
	}

}
