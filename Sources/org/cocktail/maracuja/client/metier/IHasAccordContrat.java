package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.NSArray;

/**
 * Interface permettant de savoir qu'un objet a des EOAccordContrat potentiellement liés.
 * 
 * @author rprin
 */
public interface IHasAccordContrat {

	/**
	 * @return Les EOAccordContrat lies a cet objet (potentiellement plusieurs)
	 */
	public NSArray getAccordContratLies();

}
