/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOMandat.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOMandat extends _EOMandat implements IHasAccordContrat {
	public static final EOSortOrdering SORT_MAN_NUMERO_ASC = EOSortOrdering.sortOrderingWithKey(EOMandat.MAN_NUMERO_KEY, EOSortOrdering.CompareAscending);

	public static final EOQualifier QUAL_PAYE = new EOKeyValueQualifier(EOMandat.MAN_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOMandat.mandatVirement);
	public static final EOQualifier QUAL_ANNULE = new EOKeyValueQualifier(EOMandat.MAN_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOMandat.mandatAnnule);

	public static final String MONTANT_A_PAYER_KEY = "montantAPayer";
	public static final String MONTANT_RETENUES_KEY = "montantRetenues";

	public static final String mandatAttente = "ATTENTE";
	//	public static final String mandatValide = "VALIDE";
	public static final String mandatVise = "VISE";
	public static final String mandatAnnule = "ANNULE";
	public static final String mandatVirement = "PAYE";
	public static final String problemeDejaPaye = "MANDAT DEJA PAYE";
	public static final String problemeAnnule = "MANDAT ANNULE";
	public static final String problemeNonVise = "MANDAT NON VISE";

	private NSArray _accordContratLiesCache;

	private BigDecimal montantAccordContratLies = BigDecimal.ZERO.setScale(0);

	private Boolean isSurCreditExtourne = Boolean.FALSE;

	public EOMandat() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return le premier objet depense trouvé dans la relation depenses.
	 */
	public final EODepense firstDepense() {
		if (depenses() != null && depenses().count() > 0) {
			return ((EODepense) depenses().objectAtIndex(0));
		}
		return null;
	}

	/**
	 * @return Un tableau contenant les types de crédits dont dépend le mandat (en fait ils sont récupérés à partir des depenses du mandat).
	 */
	public final NSArray getDistinctTypeCredits() {
		NSArray depenses = depenses();
		NSMutableArray tcs = new NSMutableArray();
		for (int i = 0; i < depenses.count(); i++) {
			EODepense element = (EODepense) depenses.objectAtIndex(i);
			if (element != null && element.typeCredit() != null && tcs.indexOfObject(element.typeCredit()) == NSArray.NotFound) {
				tcs.addObject(element.typeCredit());
			}
		}
		return tcs.immutableClone();
	}

	/**
	 * @return Le montant à payer (somme des montants à payer des dépenses)
	 */
	public final BigDecimal montantAPayer() {
		return ZEOUtilities.calcSommeOfBigDecimals(depenses(), EODepense.DEP_MONTANT_DISQUETTE_KEY);
	}

	/**
	 * @return Le montant des retenues (somme des montants des retenues des dépenses)
	 */
	public final BigDecimal montantRetenues() {
		return ZEOUtilities.calcSommeOfBigDecimals(depenses(), EODepense.MONTANT_RETENUES_KEY);
	}

	public boolean isPaye() {
		return (mandatVirement.equals(manEtat()));
	}

	public boolean isAnnule() {
		return (mandatAnnule.equals(manEtat()));
	}

	public String gescodeAndNum() {
		return gestion().gesCode() + "/" + manNumero();
	}

	public boolean isORV() {
		return (manHt().doubleValue() < 0 || manTtc().doubleValue() < 0);
	}

	public NSArray ecritureDetailVisa() {
		return (NSArray) mandatDetailEcritures(EOMandatDetailEcriture.QUAL_ALL_VISA).valueForKey(EOMandatDetailEcriture.ECRITURE_DETAIL_KEY);
	}

	/**
	 * @return un tableau avec les ecritures de contrepartie de visa ou modif mode de paiement)
	 */
	public NSArray ecritureDetailsVisaContrepartie() {
		NSMutableArray quals = new NSMutableArray();
		NSMutableArray qualsOr = new NSMutableArray();

		String sens = (isORV() ? EOEcritureDetail.SENS_DEBIT : EOEcritureDetail.SENS_CREDIT);
		qualsOr.addObject(new EOKeyValueQualifier(EOMandatDetailEcriture.MDE_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, EOMandatDetailEcriture.MDE_ORIGINE_VISA_CTP));
		qualsOr.addObject(new EOKeyValueQualifier(EOMandatDetailEcriture.MDE_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, EOMandatDetailEcriture.MDE_ORIGINE_MODIF_MODE_PAIEMENT));

		quals.addObject(new EOOrQualifier(qualsOr));
		quals.addObject(new EOKeyValueQualifier(EOMandatDetailEcriture.ECRITURE_DETAIL_KEY + "." + EOEcritureDetail.ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, sens));
		NSArray res = mandatDetailEcritures(new EOAndQualifier(quals));
		return (NSArray) res.valueForKey(EOMandatDetailEcriture.ECRITURE_DETAIL_KEY);
	}

	/**
	 * @return l'ecriture detail de visa non totalement emargee ou null
	 */
	public EOEcritureDetail ecritureDetailCtpNonEmargee() {
		EOQualifier qual = new EOKeyValueQualifier(EOEcritureDetail.ECD_RESTE_EMARGER_KEY, EOQualifier.QualifierOperatorGreaterThan, BigDecimal.valueOf(0.00));
		NSArray ecdCtp = EOQualifier.filteredArrayWithQualifier(ecritureDetailsVisaContrepartie(), qual);
		if (ecdCtp.count() == 1) {
			return (EOEcritureDetail) ecdCtp.objectAtIndex(0);
		}
		return null;
	}

	/**
	 * @param origine
	 * @param sens
	 * @return Le montant des ecritureDetail respectant l'origine et le sens
	 */
	public BigDecimal montantEcrituresParOrigineEtSens(String origine, String sens) {
		BigDecimal res = new BigDecimal(0.00);
		NSArray tmp = getEcritureDetailsParOrigineEtSens(origine, sens);
		res = ZEOUtilities.calcSommeOfBigDecimals(tmp, EOEcritureDetail.ECD_MONTANT_KEY);
		return res;
	}

	/**
	 * @param origine
	 * @param sens
	 * @return tableau d'ecritureDetail respectant l'origine et le sens
	 */
	public NSArray getEcritureDetailsParOrigineEtSens(String origine, String sens) {
		NSMutableArray res = new NSMutableArray();
		final EOQualifier qual = new EOKeyValueQualifier(EOMandatDetailEcriture.MDE_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, origine);
		final NSArray mdes = mandatDetailEcritures(qual, false);
		int j = 0;
		while (j < mdes.count()) {
			EOMandatDetailEcriture mde = (EOMandatDetailEcriture) mdes.objectAtIndex(j);
			if (sens.equals(mde.ecritureDetail().ecdSens())) {
				res.addObject(mde.ecritureDetail());
			}
			j++;
		}
		return res.immutableClone();
	}

	/**
	 * @param mandats
	 * @param origine
	 * @param sens
	 * @return Le montant des ecrituresDetail selon origine et sens.
	 */
	public static BigDecimal montantEcrituresParOrigineEtSens(NSArray mandats, String origine, String sens) {
		BigDecimal montantEcritures = new BigDecimal(0);
		int i = 0;
		while (i < mandats.count()) {
			EOMandat tmpMandat = (EOMandat) mandats.objectAtIndex(i);
			montantEcritures = montantEcritures.add(tmpMandat.montantEcrituresParOrigineEtSens(origine, sens));
			i++;
		}
		return montantEcritures;
	}

	/**
	 * @param mandats
	 * @return Le montant des ecrituresdetail de paiement en crédit (origine virement)
	 */
	public static BigDecimal montantEcrituresCreditsPaiement(NSArray mandats) {
		final String origine = EOMandatDetailEcriture.MDE_ORIGINE_VIREMENT;
		final String sens = EOEcritureDetail.SENS_CREDIT;
		return montantEcrituresParOrigineEtSens(mandats, origine, sens);
	}

	/**
	 * @param mandats
	 * @return Le montant des ecrituresdetail de retenues en crédit (origine virement)
	 */
	public static BigDecimal montantEcrituresCreditsRetenues(NSArray mandats) {
		final String origine = EOMandatDetailEcriture.MDE_ORIGINE_VIREMENT_RETENUE;
		final String sens = EOEcritureDetail.SENS_CREDIT;
		return montantEcrituresParOrigineEtSens(mandats, origine, sens);
	}

	public boolean isMandatAExtourner() {
		return EOModePaiement.QUAL_A_EXTOURNER.evaluateWithObject(modePaiement());
	}

	public EOExtourneMandat toExtourneMandatN() {
		return (EOExtourneMandat) (toExtourneMandatsN() != null && toExtourneMandatsN().count() > 0 ? toExtourneMandatsN().objectAtIndex(0) : null);
	}

	public NSArray getAccordContratLies() {

		if (_accordContratLiesCache != null) {
			return _accordContratLiesCache;
		}

		montantAccordContratLies = BigDecimal.ZERO.setScale(2);
		NSMutableArray res = new NSMutableArray();
		NSArray dpcos = EOJdDepenseCtrlPlanco.fetchAll(editingContext(), new EOKeyValueQualifier(EOJdDepenseCtrlPlanco.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, this), null);
		for (int j = 0; j < dpcos.count(); j++) {
			EOJdDepenseCtrlPlanco dpco = (EOJdDepenseCtrlPlanco) dpcos.objectAtIndex(j);
			if (dpco != null) {
				if (BigDecimal.ZERO.equals(dpco.dpcoMontantBudgetaire())) {
					isSurCreditExtourne = Boolean.TRUE;
				}
				NSArray rccs = dpco.toDepenseBudget().toDepenseCtrlConventions();
				for (int i = 0; i < rccs.count(); i++) {
					EOJDDepenseCtrlConvention depenseCtrlConvention = ((EOJDDepenseCtrlConvention) rccs.objectAtIndex(i));
					montantAccordContratLies = montantAccordContratLies.add(depenseCtrlConvention.dconMontantBudgetaire());
					EOAccordsContrat contrat = depenseCtrlConvention.toAccordsContrat();
					if (rccs.indexOfObject(contrat) == NSArray.NotFound) {
						res.addObject(contrat);
					}
				}
			}
		}

		_accordContratLiesCache = res.immutableClone();
		return _accordContratLiesCache;
	}

	/**
	 * @return Le montant positionné sur l'ensemble des conventions. Ce montant ne devrait pas etre diffrent du montant HT (budgetaire) du mandat
	 */
	public BigDecimal getMontantAccordContratLies() {
		return montantAccordContratLies;
	}

	public EOAccordsContrat getAccordContratUnique() {
		EOAccordsContrat contrat = null;
		NSArray res = getAccordContratLies();
		if (res.count() == 1) {
			contrat = (EOAccordsContrat) res.objectAtIndex(0);
		}
		return contrat;
	}

	/**
	 * @return le contrat relié au mandat s'il est unique et si la répartition est effectuée sur le montant total du mandat.
	 */
	public EOAccordsContrat getAccordContratUniqueAvecRepartitionTotale() {
		EOAccordsContrat contrat = null;
		NSArray res = getAccordContratLies();
		if (res.count() == 1 && this.manHt().equals(getMontantAccordContratLies())) {
			contrat = (EOAccordsContrat) res.objectAtIndex(0);
		}
		return contrat;
	}

	public boolean isSurCreditExtourne() {
		//pour initialiser le cache si besoin
		getAccordContratLies();
		return isSurCreditExtourne;
	}

}
