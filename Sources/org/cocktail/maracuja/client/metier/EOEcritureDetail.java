/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOEcritureDetail.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;
import java.util.Iterator;

import org.cocktail.fwkcktlcompta.common.entities.IEcriture;
import org.cocktail.fwkcktlcompta.common.entities.IEcritureDetail;
import org.cocktail.fwkcktlcompta.common.entities.IGestion;
import org.cocktail.fwkcktlcompta.common.entities.IJefyAdminExercice;
import org.cocktail.fwkcktlcompta.common.entities.IPlanComptableExer;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOEcritureDetail extends _EOEcritureDetail {

	public static final String resteEmargerNegatif = "LE RESTE A EMARGER NE PEUT PAS ETRE NEGATIF";
	public static final String MSG_ECDDEBIT_NUL = "ecdDebit ne peut pas etre NULL";
	public static final String MSG_CREDIT_NUL = "ecdCredit ne peut pas etre NULL";
	public static final String MSG_CREDIT_MONTANT = "ecdCredit doit etre egal a ecdMontant";
	public static final String MSG_DEBIT_MONTANT = "ecdDebit doit etre egal a ecdMontant";
	public static final String MSG_RESTEEMARGER_GRAND = "Le reste a emarger ne peut etre plus grand que le montant initial. Si vous êtes en train d'annuler un émargement, vérifiez qu'une autre personne n'est pas en train d'annuler le même.";
	public static final String MSG_RESTEEMARGER_0 = "Le reste a emarger ne peut etre inférieur à 0. Vérifiez q'une autre personne n'est pas en train de faire un émargement sur la même écriture.";
	public static final String MSG_GESTION_OBLIGATOIRE = "La relation gestion est obligatoire.";
	public static final String MSG_EXERCICE_OBLIGATOIRE = "La relation exercice est obligatoire.";
	public static final String MSG_ECRITURE_OBLIGATOIRE = "La relation ecriture est obligatoire.";
	public static final String MSG_PLANCOMPTABLE_OBLIGATOIRE = "La relation planComptable est obligatoire.";

	public static final String SENS_DEBIT = "D";
	public static final String SENS_CREDIT = "C";

	//	public static final EOQualifier QUAL_DEBITS = EOQualifier.qualifierWithQualifierFormat(ECD_SENS_KEY + "=%@", new NSArray(new Object[] { SENS_DEBIT }));
	//	public static final EOQualifier QUAL_CREDITS = EOQualifier.qualifierWithQualifierFormat(ECD_SENS_KEY + "=%@", new NSArray(new Object[] { SENS_CREDIT }));
	//public static final EOQualifier QUAL_VALIDE = EOQualifier.qualifierWithQualifierFormat(ECRITURE_KEY + "." + EOEcriture.ECR_ETAT_KEY + "=%@",			new NSArray(new Object[] { EOEcriture.ecritureValide }));

	public static final EOQualifier QUAL_DEBITS = new EOKeyValueQualifier(EOEcritureDetail.ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, SENS_DEBIT);
	public static final EOQualifier QUAL_CREDITS = new EOKeyValueQualifier(EOEcritureDetail.ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, SENS_CREDIT);
	public static final EOQualifier QUAL_VALIDE = new EOKeyValueQualifier(ECRITURE_KEY + "." + EOEcriture.ECR_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOEcriture.ecritureValide);
	public static final EOQualifier QUAL_NEGATIF = new EOKeyValueQualifier(EOEcritureDetail.ECD_MONTANT_KEY, EOQualifier.QualifierOperatorLessThan, ZConst.INTEGER_ZERO);
	public static final EOQualifier QUAL_POSITIF = new EONotQualifier(QUAL_NEGATIF);

	public static final String ECR_NUMERO_AND_ECD_INDEX_KEY = "ecrNumeroAndEcdIndex";
	public static final String ECD_FOR_EMARGEMENT_KEY = "ecdForEmargement";
	public static final String CALCULATED_RESTE_EMARGER_KEY = "calculatedEcdResteEmarger";
	public static final String IS_SACD_KEY = "isSacd";
	public static final EOSortOrdering SORT_ECD_INDEX_ASC = EOSortOrdering.sortOrderingWithKey(EOEcritureDetail.ECD_INDEX_KEY, EOSortOrdering.CompareAscending);

	private EOEcritureDetail ecdForEmargement;

	public EOEcritureDetail() {
		super();
	}

	public void setEcdResteEmarger(BigDecimal value) {
		super.setEcdResteEmarger(value);
		if (value.floatValue() < 0) {
			throw new NSValidation.ValidationException(MSG_RESTEEMARGER_0);
		}
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (SENS_DEBIT.equals(ecdSens()) && this.ecdDebit() == null) {
			throw new NSValidation.ValidationException(MSG_ECDDEBIT_NUL);
		}

		if (SENS_CREDIT.equals(ecdSens()) && this.ecdCredit() == null) {
			throw new NSValidation.ValidationException(MSG_CREDIT_NUL);
		}

		if (SENS_CREDIT.equals(ecdSens()) && !(this.ecdCredit().equals(this.ecdMontant()))) {
			throw new NSValidation.ValidationException(MSG_CREDIT_MONTANT + " " + ecdCredit() + " / " + ecdMontant());
		}

		if (SENS_DEBIT.equals(ecdSens()) && !(this.ecdDebit().equals(this.ecdMontant()))) {
			throw new NSValidation.ValidationException(MSG_DEBIT_MONTANT + " " + ecdDebit() + " / " + ecdMontant());
		}

		if (ecdResteEmarger().compareTo(ecdMontant().abs()) > 0) {
			throw new NSValidation.ValidationException(MSG_RESTEEMARGER_GRAND);
		}

		if (ecdResteEmarger().floatValue() < 0) {
			throw new NSValidation.ValidationException(MSG_RESTEEMARGER_0);
		}

		// relationShip Mandatory :
		if (this.gestion() == null) {
			throw new NSValidation.ValidationException(MSG_GESTION_OBLIGATOIRE);
		}

		if (this.exercice() == null) {
			throw new NSValidation.ValidationException(MSG_EXERCICE_OBLIGATOIRE);
		}

		if (this.ecriture() == null) {
			throw new NSValidation.ValidationException(MSG_ECRITURE_OBLIGATOIRE);
		}

		if (this.planComptable() == null) {
			throw new NSValidation.ValidationException(MSG_PLANCOMPTABLE_OBLIGATOIRE);
		}

	}

	public void setEcdIndex(Integer aValue) {
		if (aValue == null) {
			aValue = Integer.valueOf("0");
		}
		super.setEcdIndex(aValue);
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void setEcdLibelle(String value) {
		if (value != null) {
			if (value.length() > 200) {
				value = value.substring(0, 200);
			}
		}
		super.setEcdLibelle(value);
	}

	public EOEcritureDetail getEcdForEmargement() {
		return ecdForEmargement;
	}

	public void setEcdForEmargement(EOEcritureDetail ecdForEmargement) {
		this.ecdForEmargement = ecdForEmargement;
	}

	/**
	 * @return Une liste d'écritures details emargeables avec cette ecriture detail.
	 */
	public NSArray getEcritureDetailsEmargeables() {

		if (isEmargeable()) {
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, this.exercice()));
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.ECRITURE_KEY + "." + EOEcriture.ECR_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOEcriture.ecritureValide));
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.ECRITURE_KEY + "." + EOEcriture.COMPTABILITE_KEY, EOQualifier.QualifierOperatorEqual, this.ecriture().comptabilite()));
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.PLAN_COMPTABLE_KEY, EOQualifier.QualifierOperatorEqual, this.planComptable()));
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.GESTION_KEY, EOQualifier.QualifierOperatorEqual, this.gestion()));
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.ECD_RESTE_EMARGER_KEY, EOQualifier.QualifierOperatorGreaterThan, BigDecimal.valueOf(0)));
			quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.ECD_SENS_KEY, EOQualifier.QualifierOperatorNotEqual, this.ecdSens()));
			EOQualifier qual = new EOAndQualifier(quals);

			//classer les résultats par pertinence : partir de libellé identique, montant identique, etc.
			NSArray res1 = EOEcritureDetail.fetchAll(this.editingContext(), new EOAndQualifier(new NSArray(new Object[] {
					qual, new EOKeyValueQualifier(EOEcritureDetail.ECD_RESTE_EMARGER_KEY, EOQualifier.QualifierOperatorEqual, this.ecdMontant().abs())
			})), null);
			NSArray res2 = EOEcritureDetail.fetchAll(this.editingContext(), new EOAndQualifier(new NSArray(new Object[] {
					qual, new EOKeyValueQualifier(EOEcritureDetail.ECD_RESTE_EMARGER_KEY, EOQualifier.QualifierOperatorNotEqual, this.ecdMontant().abs())
			})), null);
			NSMutableArray res = new NSMutableArray();
			res.addObjectsFromArray(res1);
			res.addObjectsFromArray(res2);
			return res.immutableClone();
		}
		else {
			return NSArray.EmptyArray;
		}

	}

	public boolean isEmargeable() {
		return (ecriture() != null && gestion() != null && planComptable() != null && ecdMontant() != null && ecdResteEmarger().intValue() > 0 && planComptable().pcoEmargement().equals("O"));
	}

	public String ecrNumeroAndEcdIndex() {
		return ecriture().ecrNumero() + "/" + ecdIndex();
	}

	public boolean isDebit() {
		return SENS_DEBIT.equals(ecdSens());
	}

	public boolean isCredit() {
		return SENS_CREDIT.equals(ecdSens());
	}

	public boolean isSacd() {
		NSArray res = gestion().gestionExercices(new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice()));
		if (res.count() > 0) {
			return ((EOGestionExercice) res.objectAtIndex(0)).isSacd();
		}
		return false;
	}

	public void setInitialDebit(BigDecimal montant) {
		setEcdDebit(montant);
		setEcdCredit(ZConst.BIGDECIMAL_ZERO);
		setEcdMontant(montant);
		setEcdResteEmarger(montant.abs());
	}

	public void setInitialCredit(BigDecimal montant) {
		setEcdDebit(ZConst.BIGDECIMAL_ZERO);
		setEcdCredit(montant);
		setEcdMontant(montant);
		setEcdResteEmarger(montant.abs());
	}

	/**
	 * @return le montant calculé du reste à émarger, qui peut être différent de ecdResteEmarger en cas d'erreur de calcul.
	 */
	public BigDecimal calculatedEcdResteEmarger() {
		NSArray emds = emargementDetailsValides();
		BigDecimal montantEmargement = BigDecimal.valueOf(0.00);
		Iterator<EOEmargementDetail> ite = emds.iterator();
		while (ite.hasNext()) {
			EOEmargementDetail eoEmargementDetail = (EOEmargementDetail) ite.next();
			montantEmargement = montantEmargement.add(eoEmargementDetail.emdMontant());
		}
		BigDecimal resteCalcul = ecdMontant().abs().add(montantEmargement.negate());
		return resteCalcul;
	}

	/**
	 * @return Les emargements details valides créés à partir de cette ligne d'ecriture.
	 */
	public NSArray emargementDetailsValides() {
		EOQualifier qual1 = new EOKeyValueQualifier(EOEmargementDetail.EMARGEMENT_KEY + "." + EOEmargement.EMA_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOEmargement.emaEtatValide);
		EOQualifier qual21 = new EOKeyValueQualifier(EOEmargementDetail.DESTINATION_KEY, EOQualifier.QualifierOperatorEqual, this);
		EOQualifier qual22 = new EOKeyValueQualifier(EOEmargementDetail.SOURCE_KEY, EOQualifier.QualifierOperatorEqual, this);
		EOQualifier qual2 = new EOOrQualifier(new NSArray(new Object[] {
				qual21, qual22
		}));
		return EOEmargementDetail.fetchAll(editingContext(), new EOAndQualifier(new NSArray(new Object[] {
				qual1, qual2
		})), null, true);
	}


}
