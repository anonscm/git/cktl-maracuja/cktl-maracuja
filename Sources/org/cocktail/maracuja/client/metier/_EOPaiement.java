// _EOPaiement.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPaiement.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPaiement extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Paiement";
	public static final String ENTITY_TABLE_NAME = "maracuja.PAIEMENT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "paiOrdre";

	public static final String PAI_DATE_CREATION_KEY = "paiDateCreation";
	public static final String PAI_MONTANT_KEY = "paiMontant";
	public static final String PAI_NB_VIREMENTS_KEY = "paiNbVirements";
	public static final String PAI_NUMERO_KEY = "paiNumero";

// Attributs non visibles
	public static final String COM_ORDRE_KEY = "comOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PAI_ORDRE_KEY = "paiOrdre";
	public static final String TVI_ORDRE_KEY = "tviOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PAI_DATE_CREATION_COLKEY = "pai_Date_Creation";
	public static final String PAI_MONTANT_COLKEY = "pai_Montant";
	public static final String PAI_NB_VIREMENTS_COLKEY = "pai_Nb_Virements";
	public static final String PAI_NUMERO_COLKEY = "pai_Numero";

	public static final String COM_ORDRE_COLKEY = "com_Ordre";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PAI_ORDRE_COLKEY = "pai_ORDRE";
	public static final String TVI_ORDRE_COLKEY = "tvi_Ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String COMPTABILITE_KEY = "comptabilite";
	public static final String EXERCICE_KEY = "exercice";
	public static final String MANDATS_KEY = "mandats";
	public static final String ORDRE_DE_PAIEMENTS_KEY = "ordreDePaiements";
	public static final String TITRES_KEY = "titres";
	public static final String TYPE_VIREMENT_KEY = "typeVirement";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp paiDateCreation() {
    return (NSTimestamp) storedValueForKey(PAI_DATE_CREATION_KEY);
  }

  public void setPaiDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, PAI_DATE_CREATION_KEY);
  }

  public java.math.BigDecimal paiMontant() {
    return (java.math.BigDecimal) storedValueForKey(PAI_MONTANT_KEY);
  }

  public void setPaiMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PAI_MONTANT_KEY);
  }

  public Integer paiNbVirements() {
    return (Integer) storedValueForKey(PAI_NB_VIREMENTS_KEY);
  }

  public void setPaiNbVirements(Integer value) {
    takeStoredValueForKey(value, PAI_NB_VIREMENTS_KEY);
  }

  public Integer paiNumero() {
    return (Integer) storedValueForKey(PAI_NUMERO_KEY);
  }

  public void setPaiNumero(Integer value) {
    takeStoredValueForKey(value, PAI_NUMERO_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOComptabilite comptabilite() {
    return (org.cocktail.maracuja.client.metier.EOComptabilite)storedValueForKey(COMPTABILITE_KEY);
  }

  public void setComptabiliteRelationship(org.cocktail.maracuja.client.metier.EOComptabilite value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOComptabilite oldValue = comptabilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMPTABILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMPTABILITE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeVirement typeVirement() {
    return (org.cocktail.maracuja.client.metier.EOTypeVirement)storedValueForKey(TYPE_VIREMENT_KEY);
  }

  public void setTypeVirementRelationship(org.cocktail.maracuja.client.metier.EOTypeVirement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeVirement oldValue = typeVirement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_VIREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_VIREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray mandats() {
    return (NSArray)storedValueForKey(MANDATS_KEY);
  }

  public NSArray mandats(EOQualifier qualifier) {
    return mandats(qualifier, null, false);
  }

  public NSArray mandats(EOQualifier qualifier, boolean fetch) {
    return mandats(qualifier, null, fetch);
  }

  public NSArray mandats(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOMandat.PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOMandat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = mandats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToMandatsRelationship(org.cocktail.maracuja.client.metier.EOMandat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
  }

  public void removeFromMandatsRelationship(org.cocktail.maracuja.client.metier.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOMandat createMandatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Mandat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, MANDATS_KEY);
    return (org.cocktail.maracuja.client.metier.EOMandat) eo;
  }

  public void deleteMandatsRelationship(org.cocktail.maracuja.client.metier.EOMandat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, MANDATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllMandatsRelationships() {
    Enumeration objects = mandats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteMandatsRelationship((org.cocktail.maracuja.client.metier.EOMandat)objects.nextElement());
    }
  }

  public NSArray ordreDePaiements() {
    return (NSArray)storedValueForKey(ORDRE_DE_PAIEMENTS_KEY);
  }

  public NSArray ordreDePaiements(EOQualifier qualifier) {
    return ordreDePaiements(qualifier, null, false);
  }

  public NSArray ordreDePaiements(EOQualifier qualifier, boolean fetch) {
    return ordreDePaiements(qualifier, null, fetch);
  }

  public NSArray ordreDePaiements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOOrdreDePaiement.PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOOrdreDePaiement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = ordreDePaiements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrdreDePaiementsRelationship(org.cocktail.maracuja.client.metier.EOOrdreDePaiement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORDRE_DE_PAIEMENTS_KEY);
  }

  public void removeFromOrdreDePaiementsRelationship(org.cocktail.maracuja.client.metier.EOOrdreDePaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORDRE_DE_PAIEMENTS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOOrdreDePaiement createOrdreDePaiementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("OrdreDePaiement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORDRE_DE_PAIEMENTS_KEY);
    return (org.cocktail.maracuja.client.metier.EOOrdreDePaiement) eo;
  }

  public void deleteOrdreDePaiementsRelationship(org.cocktail.maracuja.client.metier.EOOrdreDePaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORDRE_DE_PAIEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrdreDePaiementsRelationships() {
    Enumeration objects = ordreDePaiements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrdreDePaiementsRelationship((org.cocktail.maracuja.client.metier.EOOrdreDePaiement)objects.nextElement());
    }
  }

  public NSArray titres() {
    return (NSArray)storedValueForKey(TITRES_KEY);
  }

  public NSArray titres(EOQualifier qualifier) {
    return titres(qualifier, null, false);
  }

  public NSArray titres(EOQualifier qualifier, boolean fetch) {
    return titres(qualifier, null, fetch);
  }

  public NSArray titres(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOTitre.PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOTitre.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = titres();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTitresRelationship(org.cocktail.maracuja.client.metier.EOTitre object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TITRES_KEY);
  }

  public void removeFromTitresRelationship(org.cocktail.maracuja.client.metier.EOTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRES_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOTitre createTitresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Titre");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TITRES_KEY);
    return (org.cocktail.maracuja.client.metier.EOTitre) eo;
  }

  public void deleteTitresRelationship(org.cocktail.maracuja.client.metier.EOTitre object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTitresRelationships() {
    Enumeration objects = titres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTitresRelationship((org.cocktail.maracuja.client.metier.EOTitre)objects.nextElement());
    }
  }


  public static EOPaiement createPaiement(EOEditingContext editingContext, NSTimestamp paiDateCreation
, java.math.BigDecimal paiMontant
, Integer paiNbVirements
, Integer paiNumero
, org.cocktail.maracuja.client.metier.EOComptabilite comptabilite, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOTypeVirement typeVirement, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOPaiement eo = (EOPaiement) createAndInsertInstance(editingContext, _EOPaiement.ENTITY_NAME);    
		eo.setPaiDateCreation(paiDateCreation);
		eo.setPaiMontant(paiMontant);
		eo.setPaiNbVirements(paiNbVirements);
		eo.setPaiNumero(paiNumero);
    eo.setComptabiliteRelationship(comptabilite);
    eo.setExerciceRelationship(exercice);
    eo.setTypeVirementRelationship(typeVirement);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPaiement.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPaiement.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPaiement creerInstance(EOEditingContext editingContext) {
		  		EOPaiement object = (EOPaiement)createAndInsertInstance(editingContext, _EOPaiement.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOPaiement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPaiement)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPaiement localInstanceIn(EOEditingContext editingContext, EOPaiement eo) {
    EOPaiement localInstance = (eo == null) ? null : (EOPaiement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPaiement#localInstanceIn a la place.
   */
	public static EOPaiement localInstanceOf(EOEditingContext editingContext, EOPaiement eo) {
		return EOPaiement.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
