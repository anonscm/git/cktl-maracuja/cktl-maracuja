// _EOEcritureAnalytique.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEcritureAnalytique.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEcritureAnalytique extends  EOGenericRecord {
	public static final String ENTITY_NAME = "EcritureAnalytique";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ecriture_analytique";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "eanOrdre";

	public static final String EAN_INDEX_KEY = "eanIndex";

// Attributs non visibles
	public static final String EAN_ORDRE_KEY = "eanOrdre";
	public static final String ECR_ORDRE_KEY = "ecrOrdre";
	public static final String PAN_ORDRE_KEY = "panOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String EAN_INDEX_COLKEY = "ean_index";

	public static final String EAN_ORDRE_COLKEY = "ean_Ordre";
	public static final String ECR_ORDRE_COLKEY = "ecr_ORDRE";
	public static final String PAN_ORDRE_COLKEY = "Pan_ordre";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String ECRITURE_KEY = "ecriture";
	public static final String PLAN_ANALYTIQUE_KEY = "planAnalytique";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public Integer eanIndex() {
    return (Integer) storedValueForKey(EAN_INDEX_KEY);
  }

  public void setEanIndex(Integer value) {
    takeStoredValueForKey(value, EAN_INDEX_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOEcriture ecriture() {
    return (org.cocktail.maracuja.client.metier.EOEcriture)storedValueForKey(ECRITURE_KEY);
  }

  public void setEcritureRelationship(org.cocktail.maracuja.client.metier.EOEcriture value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOEcriture oldValue = ecriture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOPlanAnalytique planAnalytique() {
    return (org.cocktail.maracuja.client.metier.EOPlanAnalytique)storedValueForKey(PLAN_ANALYTIQUE_KEY);
  }

  public void setPlanAnalytiqueRelationship(org.cocktail.maracuja.client.metier.EOPlanAnalytique value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOPlanAnalytique oldValue = planAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOEcritureAnalytique createEcritureAnalytique(EOEditingContext editingContext, Integer eanIndex
, org.cocktail.maracuja.client.metier.EOEcriture ecriture, org.cocktail.maracuja.client.metier.EOPlanAnalytique planAnalytique, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOEcritureAnalytique eo = (EOEcritureAnalytique) createAndInsertInstance(editingContext, _EOEcritureAnalytique.ENTITY_NAME);    
		eo.setEanIndex(eanIndex);
    eo.setEcritureRelationship(ecriture);
    eo.setPlanAnalytiqueRelationship(planAnalytique);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEcritureAnalytique.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEcritureAnalytique.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOEcritureAnalytique creerInstance(EOEditingContext editingContext) {
		  		EOEcritureAnalytique object = (EOEcritureAnalytique)createAndInsertInstance(editingContext, _EOEcritureAnalytique.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOEcritureAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEcritureAnalytique)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOEcritureAnalytique localInstanceIn(EOEditingContext editingContext, EOEcritureAnalytique eo) {
    EOEcritureAnalytique localInstance = (eo == null) ? null : (EOEcritureAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEcritureAnalytique#localInstanceIn a la place.
   */
	public static EOEcritureAnalytique localInstanceOf(EOEditingContext editingContext, EOEcritureAnalytique eo) {
		return EOEcritureAnalytique.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOEcritureAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEcritureAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEcritureAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEcritureAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEcritureAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEcritureAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEcritureAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEcritureAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEcritureAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEcritureAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEcritureAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEcritureAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
