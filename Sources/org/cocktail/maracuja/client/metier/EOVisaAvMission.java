/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EOVisaAvMission.java
// 
package org.cocktail.maracuja.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOVisaAvMission extends _EOVisaAvMission {
    public final static String FOU_NOM_AND_PRENOM_KEY = "nomAndPrenom";
    
    public EOVisaAvMission() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    public final String getNomAndPrenom() {
        return fournisseur_adrNom() + (fournisseur_adrPrenom()!=null ? " " + fournisseur_adrPrenom() : "");
    }
    
    /**
     * 
     * @param s
     * @return Un qualifier qui filtre sur le nom et le prenom du missionnaire (fournisseur).
     */
    public static final EOQualifier qualForNomAndPrenomLike(final String s) {
        if (s != null) {
            return new EOOrQualifier( 
            new NSArray(
                    new Object[]{EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.FOURNISSEUR_ADR_NOM_KEY + " caseInsensitiveLike %@ ", new NSArray(new Object[]{"*"+s+"*"})), 
                                EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.FOURNISSEUR_ADR_PRENOM_KEY+ " caseInsensitiveLike %@ ", new NSArray(new Object[]{"*"+s+"*"}))}
                    )        
            );
        }
        return new EOAndQualifier(new NSArray());
    }
    
    /**
     * @param s
     * @return Un qualifier qui filtre sur le motif de la mission (like).
     */
    public static final EOQualifier qualForMisMotifLike(final String s) {
        if (s != null) {
            return new EOOrQualifier( 
                    new NSArray(
                            new Object[]{EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.MISSION_MIS_MOTIF_KEY + " caseInsensitiveLike %@ ", new NSArray(new Object[]{"*"+s+"*"}))}
                    )        
            );
        }
        return new EOAndQualifier(new NSArray());
    }
    
    
    /**
     * @param s
     * @return Un qualifier qui filtre sur nom et prenom (like) du fournisseur et sur le fou_code (equals)
     */
    public static final EOQualifier qualForFournisseur(final String s) {
        if (s != null) {
            return new EOOrQualifier(new NSArray(new Object[]{qualForNomAndPrenomLike(s), qualForFouCodeEquals(s)})) ; 
        }
        return new EOAndQualifier(new NSArray());
    }
    
    
    /**
     * @param s
     * @return Un qualifier qui filtre sur le code du fournisseur de la mission.
     */
    public static final EOQualifier qualForFouCodeEquals(final String s) {
        if (s != null) {
            return EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.FOURNISSEUR_KEY +"."+EOFournisseur.FOU_CODE_KEY + "=%@", new NSArray(new Object[]{s}));
        }
        return new EOAndQualifier(new NSArray());
    }
    
    public static final EOQualifier qualForEtat(final EOTypeEtat etat) {
        if (etat != null) {
            return EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.TYPE_ETAT_KEY + "=%@", new NSArray(new Object[]{etat}));
        }
        return new EOAndQualifier(new NSArray());
    }
    
    
    /**
     * @param s
     * @return Un qualifier qui filtre sur le code gestion.
     */
    public static final EOQualifier qualForGesCodeEquals(final String s) {
        if (s != null) {
            return EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.GESTION_KEY +"."+EOGestion.GES_CODE_KEY + "=%@", new NSArray(new Object[]{s}));
        }
        return new EOAndQualifier(new NSArray());
    }
    
    /**
     * @param nMin Peut être null
     * @param nMax Peut être null
     * @return Un qualifier qui filtre sur le numero compris entre nMin et nMax.
     */
    public static final EOQualifier qualForMisNumeroBetween(final Number nMin, final Number nMax) {
        NSMutableArray quals = new NSMutableArray();
        if (nMin!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat( EOVisaAvMission.MISSION_MIS_NUMERO_KEY  + ">=%@", new NSArray( nMin )));
        }
        if (nMax!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.MISSION_MIS_NUMERO_KEY  +"<=%@", new NSArray( nMax)));
        }
        if (quals.count()>0) {
            return new EOAndQualifier(quals);
        }     
        return new EOAndQualifier(new NSArray());
    }
    
    /**
     * @param n 
     * @return Un qualifier qui filtre sur le n° de la mission.
     */
    public static final EOQualifier qualForMisNumeroEquals(final Number n) {
        if (n!=null) {
            return EOQualifier.qualifierWithQualifierFormat( EOVisaAvMission.MISSION_MIS_NUMERO_KEY  + "=%@", new NSArray( n ));
        }
        return new EOAndQualifier(new NSArray());
    }
    
    /**
     * @param montantMin
     * @param montantMax
     * @return Un qualifier qui filtre sur le MONTANT compris entre montantMin et montantMax.
     */
    public static final EOQualifier qualForVamMontantBetween(final Number montantMin, final Number montantMax) {
        NSMutableArray quals = new NSMutableArray();
        if (montantMin!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat( EOVisaAvMission.VAM_MONTANT_KEY  + ">=%@", new NSArray( montantMin )));
        }
        if (montantMax!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.VAM_MONTANT_KEY  +"<=%@", new NSArray( montantMax)));
        }
        if (quals.count()>0) {
            return new EOAndQualifier(quals);
        }     
        return new EOAndQualifier(new NSArray());
    }
    
    
    

}
