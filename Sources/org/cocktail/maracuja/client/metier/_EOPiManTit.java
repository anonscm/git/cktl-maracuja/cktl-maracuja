// _EOPiManTit.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPiManTit.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPiManTit extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PiManTit";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_PI_MAN_TIT";



	// Attributes



// Attributs non visibles
	public static final String BOR_ID_DEP_KEY = "borIdDep";
	public static final String BOR_ID_REC_KEY = "borIdRec";
	public static final String MAN_ID_KEY = "manId";
	public static final String PREST_ID_KEY = "prestId";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees

	public static final String BOR_ID_DEP_COLKEY = "bor_id_dep";
	public static final String BOR_ID_REC_COLKEY = "bor_id_rec";
	public static final String MAN_ID_COLKEY = "man_id";
	public static final String PREST_ID_COLKEY = "prest_id";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String BORDEREAU_DEP_KEY = "bordereauDep";
	public static final String BORDEREAU_REC_KEY = "bordereauRec";
	public static final String MANDAT_KEY = "mandat";
	public static final String TITRE_KEY = "titre";



	// Accessors methods
  public org.cocktail.maracuja.client.metier.EOBordereau bordereauDep() {
    return (org.cocktail.maracuja.client.metier.EOBordereau)storedValueForKey(BORDEREAU_DEP_KEY);
  }

  public void setBordereauDepRelationship(org.cocktail.maracuja.client.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBordereau oldValue = bordereauDep();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_DEP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_DEP_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOBordereau bordereauRec() {
    return (org.cocktail.maracuja.client.metier.EOBordereau)storedValueForKey(BORDEREAU_REC_KEY);
  }

  public void setBordereauRecRelationship(org.cocktail.maracuja.client.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBordereau oldValue = bordereauRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_REC_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOMandat mandat() {
    return (org.cocktail.maracuja.client.metier.EOMandat)storedValueForKey(MANDAT_KEY);
  }

  public void setMandatRelationship(org.cocktail.maracuja.client.metier.EOMandat value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOMandat oldValue = mandat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MANDAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MANDAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTitre titre() {
    return (org.cocktail.maracuja.client.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.maracuja.client.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  

  public static EOPiManTit createPiManTit(EOEditingContext editingContext, org.cocktail.maracuja.client.metier.EOBordereau bordereauDep, org.cocktail.maracuja.client.metier.EOBordereau bordereauRec, org.cocktail.maracuja.client.metier.EOMandat mandat, org.cocktail.maracuja.client.metier.EOTitre titre) {
    EOPiManTit eo = (EOPiManTit) createAndInsertInstance(editingContext, _EOPiManTit.ENTITY_NAME);    
    eo.setBordereauDepRelationship(bordereauDep);
    eo.setBordereauRecRelationship(bordereauRec);
    eo.setMandatRelationship(mandat);
    eo.setTitreRelationship(titre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPiManTit.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPiManTit.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPiManTit creerInstance(EOEditingContext editingContext) {
		  		EOPiManTit object = (EOPiManTit)createAndInsertInstance(editingContext, _EOPiManTit.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOPiManTit localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPiManTit)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPiManTit localInstanceIn(EOEditingContext editingContext, EOPiManTit eo) {
    EOPiManTit localInstance = (eo == null) ? null : (EOPiManTit)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPiManTit#localInstanceIn a la place.
   */
	public static EOPiManTit localInstanceOf(EOEditingContext editingContext, EOPiManTit eo) {
		return EOPiManTit.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPiManTit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPiManTit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPiManTit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPiManTit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPiManTit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPiManTit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPiManTit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPiManTit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPiManTit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPiManTit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPiManTit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPiManTit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
