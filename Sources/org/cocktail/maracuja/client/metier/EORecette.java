/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EORecette.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Vector;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.metier.recette.EOJRRecetteCtrlConvention;
import org.cocktail.maracuja.client.metier.recette.EOJRRecetteCtrlPlanco;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EORecette extends _EORecette implements IHasAccordContrat {
	private static final String S_RC = "\n";
	private static final String S_TIRET_SPACE = " - ";
	private static final String S_NUM = "N° ";
	private static final String QUAL_EQUALS = "=%@";
	public static final String REC_SUPPRESSION_OUI = "OUI";
	public static final String REC_SUPPRESSION_NON = "NON";

	public static final String NUM_LIBELLE_KEY = "numAndLibelle";
	public static final String ACCORD_CONTRAT_UNIQUE_KEY = "getAccordContratUnique";

	public static final EOQualifier QUAL_NON_SUPPRIME = EOQualifier.qualifierWithQualifierFormat(REC_SUPPRESSION_KEY + QUAL_EQUALS, new NSArray(new Object[] {
			REC_SUPPRESSION_NON
	}));
	public static final EOQualifier QUAL_VISE = EOQualifier.qualifierWithQualifierFormat("titre.titEtat=%@ ", new NSArray(new Object[] {
			EOTitre.titreVise
	}));
	public static final EOQualifier QUAL_NON_INTERNE = EOQualifier.qualifierWithQualifierFormat("titre.bordereau.typeBordereau.tboType<>%@ and titre.bordereau.typeBordereau.tboType<>%@", new NSArray(new Object[] {
			EOTypeBordereau.TypeBordereauPrestationInterne,
			EOTypeBordereau.TYPE_BORDEREAU_PRESTATION_INTERNE_R
	}));

	private String clientCache;
	private NSArray _accordContratLiesCache;

	public EORecette() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Renvoie le débiteur s'il est défini, sinon renvoi le fournisseur du titre, sinon la ligne budgétaire (PI)
	 * 
	 * @return
	 */
	public final String getClient() {
		if (clientCache == null) {
			String debiteur = recDebiteur();
			if (debiteur == null || debiteur.length() == 0) {
				if (titre().fournisseur() != null) {
					debiteur = titre().fournisseur().getNomAndPrenomAndCode();
				}
				else if (organInterne() != null) {
					debiteur = organInterne().getLongString();
				}
			}
			debiteur = debiteur.trim();
			if (debiteur.indexOf(S_RC) > 0) {
				debiteur = debiteur.substring(0, debiteur.indexOf(S_RC));
				//                System.out.println("debiteur corrige = " +clientCache);
			}
			clientCache = debiteur;
		}
		//        System.out.println("debiteur = " +clientCache);
		return clientCache;
	}

	/**
	 * Renvoie le débiteur s'il est défini, sinon renvoi le fournisseur du titre, sinon la ligne budgétaire (PI) Les retours chariots sont remplacés
	 * par des " - ".
	 * 
	 * @return
	 */
	public final String getClientAvecSeparateur() {
		String string = getClient();
		return (string == null ? null : string.replaceAll(S_RC, S_TIRET_SPACE));
	}

	//    public BigDecimal getResteRecouvrerReadOnly() {
	//        if (resteRecouvrerReadOnly==null) {
	//            resteRecouvrerReadOnly = calResteEmarger();
	//        }
	//        return resteRecouvrerReadOnly;
	//    }

	//    public void setResteRecouvrerReadOnly(final BigDecimal resteRecouvrer) {
	//        this.resteRecouvrerReadOnly = resteRecouvrer;
	//    }

	//
	//    private final BigDecimal calResteEmarger() {
	////        final NSArray titreDetailEcritures = titre().titreDetailEcritures();
	////        final NSMutableArray ecritureDetails = new NSMutableArray();
	////
	////        for (int i = 0; i < titreDetailEcritures.count(); i++) {
	////            EOTitreDetailEcriture element = (EOTitreDetailEcriture) titreDetailEcritures.objectAtIndex(i);
	////            if (ecritureDetails.indexOfObject(element.ecritureDetail())==NSArray.NotFound) {
	////                ecritureDetails.addObject(element.ecritureDetail());
	////            }
	////        }
	////        //Filtrer les details d'ecriture
	////        final NSMutableArray array = new NSMutableArray();
	////        array.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum like '4*' or planComptable.pcoNum like '5*' and ecdSens='D' and exercice.exeType=%@", new NSArray(EOExercice.EXE_TYPE_TRESORERIE)));
	////        array.addObject(new EONotQualifier(EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum like '445*')", null)));
	////        final EOQualifier qual = new EOAndQualifier(array);
	////
	////        final NSArray filteredEcritureDetails = EOQualifier.filteredArrayWithQualifier( ecritureDetails , qual);
	////        final BigDecimal montant = calcSommeOfBigDecimals(filteredEcritureDetails.vector(), "ecdResteEmarger");
	////        return montant;
	//        
	//        return titre().titreResteRecouvrerLight().resteRecouvrer();
	//        
	//    }

	public final BigDecimal calcSommeOfBigDecimals(final Vector array, final String keyName) {
		BigDecimal res = new BigDecimal(0).setScale(2);
		Iterator iter = array.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal) element.valueForKey(keyName));
		}
		return res;
	}

	public final NSArray recetteRelancesValides() {
		final NSArray relances = recetteRelances();
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORecetteRelance.RER_ETAT_KEY + QUAL_EQUALS, new NSArray(EORecetteRelance.etatValide));
		final NSArray relancesValides = EOQualifier.filteredArrayWithQualifier(relances, qual);
		return relancesValides;
	}

	public final NSTimestamp dateProchaineRelance() {
		return (recetteRelInfo() == null ? null : recetteRelInfo().dateProchaineRelance());
	}

	public EORecetteInfo recetteInfo() {
		if (recetteInfos() == null || recetteInfos().count() == 0) {
			return null;
		}
		return (EORecetteInfo) recetteInfos().objectAtIndex(0);
	}

	public final String getNumAndLibelle() {
		return S_NUM + recNum().intValue() + S_TIRET_SPACE + recLibelle();
	}

	/**
	 * Calcule et renvoie le reste a recouvrer associe a la recette.
	 * 
	 * @return Le montant est la somme des restes à émarger pour les débits associés à la recette.
	 */
	public BigDecimal resteARecouvrer() {
		final EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
				EOEcritureDetail.QUAL_VALIDE, EOEcritureDetail.QUAL_DEBITS
		}));
		final NSArray ecritureDetails = EOQualifier.filteredArrayWithQualifier((NSArray) titreDetailEcritures().valueForKey(EOTitreDetailEcriture.ECRITURE_DETAIL_KEY), qual);
		return ZEOUtilities.calcSommeOfBigDecimals(ecritureDetails, EOEcritureDetail.ECD_RESTE_EMARGER_KEY);
	}

	public NSArray getAccordContratLies() {
		if (_accordContratLiesCache != null) {
			return _accordContratLiesCache;
		}

		NSMutableArray res = new NSMutableArray();
		Integer titId = (Integer) ServerProxy.serverPrimaryKeyForObject(new EOEditingContext(), this.titre()).valueForKey(EOTitre.TIT_ID_KEY);
		NSMutableArray quals = new NSMutableArray();
		quals.addObjects(new EOKeyValueQualifier(EOJRRecetteCtrlPlanco.RPCO_ID_KEY, EOQualifier.QualifierOperatorEqual, this.recOrdre()));
		quals.addObjects(new EOKeyValueQualifier(EOJRRecetteCtrlPlanco.TIT_ID_KEY, EOQualifier.QualifierOperatorEqual, titId));
		EOJRRecetteCtrlPlanco dpco = EOJRRecetteCtrlPlanco.fetchByQualifier(editingContext(), new EOAndQualifier(quals));
		if (dpco != null) {
			NSArray rccs = dpco.toJRRecette().toRecetteCtrlConventions();
			for (int i = 0; i < rccs.count(); i++) {
				EOAccordsContrat contrat = ((EOJRRecetteCtrlConvention) rccs.objectAtIndex(i)).toAccordsContrat();
				if (rccs.indexOfObject(contrat) == NSArray.NotFound) {
					res.addObject(contrat);
				}
			}
		}
		_accordContratLiesCache = res.immutableClone();
		return _accordContratLiesCache;
	}

	public EOAccordsContrat getAccordContratUnique() {
		EOAccordsContrat contrat = null;
		NSArray res = getAccordContratLies();
		if (res.count() == 1) {
			contrat = (EOAccordsContrat) res.objectAtIndex(0);
		}
		return contrat;
	}

}
