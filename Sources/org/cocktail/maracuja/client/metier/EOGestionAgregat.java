/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.metier;

import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOGestionAgregat extends _EOGestionAgregat {
	public static final String RESERVE_ETAB = "ETAB";
	public static final String RESERVE_SACD = "SACD_";
	public static final String RESERVE_SACD2 = "SACD ";
	public static final String RESERVE_UB = "UB";
	public static final String RESERVE_ETABLISSEMENT = "ETABLISSEMENT";
	public static final String RESERVE_AGREGE = "AGREGE";

	public static final String TO_GESTION_EXERCICES_KEY = "toGestionExercices";

	public static final Object SORT_GA_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EOGestionAgregat.GA_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending);

	public EOGestionAgregat() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		valideLibelle(gaLibelle());

	}

	public static void valideLibelle(String libelle) {
		if (ZStringUtil.isEmpty(libelle)) {
			throw new NSValidation.ValidationException("Libellé obligatoire");
		}
		if (!libelle.toUpperCase().equals(libelle)) {
			throw new NSValidation.ValidationException("Le libellé doit être en majuscule");
		}
		if (libelle.equals(RESERVE_ETAB)) {
			throw new NSValidation.ValidationException(RESERVE_ETAB + " est un libellé réservé");
		}
		if (libelle.equals(RESERVE_UB)) {
			throw new NSValidation.ValidationException(RESERVE_UB + " est un libellé réservé");
		}
		if (libelle.startsWith(RESERVE_SACD)) {
			throw new NSValidation.ValidationException(RESERVE_SACD + " est un libellé réservé");
		}
		if (libelle.startsWith(RESERVE_SACD2)) {
			throw new NSValidation.ValidationException(RESERVE_SACD2 + " est un libellé réservé");
		}
		if (libelle.startsWith(RESERVE_ETABLISSEMENT)) {
			throw new NSValidation.ValidationException(RESERVE_ETABLISSEMENT + " est un libellé réservé");
		}
		if (libelle.startsWith(RESERVE_AGREGE)) {
			throw new NSValidation.ValidationException(RESERVE_AGREGE + " est un libellé réservé");
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public NSArray toGestionExercices() {
		NSMutableArray res = new NSMutableArray();
		NSArray tmp = toGestionAgregatReparts();
		for (int j = 0; j < tmp.count(); j++) {
			EOGestionAgregatRepart array_element = (EOGestionAgregatRepart) tmp.objectAtIndex(j);
			res.addObject(array_element.toGestionExercice());
		}
		return res.mutableClone();
	}

	public NSArray toGestionExercices(EOExercice exercice) {
		EOQualifier qual = new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		return EOQualifier.filteredArrayWithQualifier(toGestionExercices(), qual);
	}


	/**
	 * @param exercice
	 * @return Les agregats disponibles pour l'exercice.
	 */
	public static NSArray getGestionAgregatsForExercice(EOExercice exercice) {
		return EOGestionAgregat.fetchAll(exercice.editingContext(), new EOKeyValueQualifier(EOGestionAgregat.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice), new NSArray(new Object[] {
				EOGestionAgregat.SORT_GA_LIBELLE_ASC
		}));
	}

}
