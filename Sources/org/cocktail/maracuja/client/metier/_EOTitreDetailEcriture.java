// _EOTitreDetailEcriture.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTitreDetailEcriture.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTitreDetailEcriture extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TitreDetailEcriture";
	public static final String ENTITY_TABLE_NAME = "maracuja.Titre_Detail_Ecriture";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tdeOrdre";

	public static final String TDE_DATE_KEY = "tdeDate";
	public static final String TDE_ORIGINE_KEY = "tdeOrigine";

// Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORI_ORDRE_KEY = "oriOrdre";
	public static final String REC_ID_KEY = "recId";
	public static final String TDE_ORDRE_KEY = "tdeOrdre";
	public static final String TIT_ID_KEY = "titId";

//Colonnes dans la base de donnees
	public static final String TDE_DATE_COLKEY = "tde_date";
	public static final String TDE_ORIGINE_COLKEY = "TDE_ORIGINE";

	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORI_ORDRE_COLKEY = "ori_Ordre";
	public static final String REC_ID_COLKEY = "rec_id";
	public static final String TDE_ORDRE_COLKEY = "tde_Ordre";
	public static final String TIT_ID_COLKEY = "TIT_ID";


	// Relationships
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORIGINE_KEY = "origine";
	public static final String RECETTE_KEY = "recette";
	public static final String TITRE_KEY = "titre";



	// Accessors methods
  public NSTimestamp tdeDate() {
    return (NSTimestamp) storedValueForKey(TDE_DATE_KEY);
  }

  public void setTdeDate(NSTimestamp value) {
    takeStoredValueForKey(value, TDE_DATE_KEY);
  }

  public String tdeOrigine() {
    return (String) storedValueForKey(TDE_ORIGINE_KEY);
  }

  public void setTdeOrigine(String value) {
    takeStoredValueForKey(value, TDE_ORIGINE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOEcritureDetail ecritureDetail() {
    return (org.cocktail.maracuja.client.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
  }

  public void setEcritureDetailRelationship(org.cocktail.maracuja.client.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOEcritureDetail oldValue = ecritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOrigine origine() {
    return (org.cocktail.maracuja.client.metier.EOOrigine)storedValueForKey(ORIGINE_KEY);
  }

  public void setOrigineRelationship(org.cocktail.maracuja.client.metier.EOOrigine value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOrigine oldValue = origine();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORIGINE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORIGINE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EORecette recette() {
    return (org.cocktail.maracuja.client.metier.EORecette)storedValueForKey(RECETTE_KEY);
  }

  public void setRecetteRelationship(org.cocktail.maracuja.client.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EORecette oldValue = recette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTitre titre() {
    return (org.cocktail.maracuja.client.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.maracuja.client.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  

  public static EOTitreDetailEcriture createTitreDetailEcriture(EOEditingContext editingContext, NSTimestamp tdeDate
, org.cocktail.maracuja.client.metier.EOEcritureDetail ecritureDetail, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOTitre titre) {
    EOTitreDetailEcriture eo = (EOTitreDetailEcriture) createAndInsertInstance(editingContext, _EOTitreDetailEcriture.ENTITY_NAME);    
		eo.setTdeDate(tdeDate);
    eo.setEcritureDetailRelationship(ecritureDetail);
    eo.setExerciceRelationship(exercice);
    eo.setTitreRelationship(titre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTitreDetailEcriture.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTitreDetailEcriture.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOTitreDetailEcriture creerInstance(EOEditingContext editingContext) {
		  		EOTitreDetailEcriture object = (EOTitreDetailEcriture)createAndInsertInstance(editingContext, _EOTitreDetailEcriture.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOTitreDetailEcriture localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTitreDetailEcriture)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTitreDetailEcriture localInstanceIn(EOEditingContext editingContext, EOTitreDetailEcriture eo) {
    EOTitreDetailEcriture localInstance = (eo == null) ? null : (EOTitreDetailEcriture)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTitreDetailEcriture#localInstanceIn a la place.
   */
	public static EOTitreDetailEcriture localInstanceOf(EOEditingContext editingContext, EOTitreDetailEcriture eo) {
		return EOTitreDetailEcriture.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTitreDetailEcriture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTitreDetailEcriture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTitreDetailEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTitreDetailEcriture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTitreDetailEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTitreDetailEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTitreDetailEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTitreDetailEcriture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTitreDetailEcriture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTitreDetailEcriture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTitreDetailEcriture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTitreDetailEcriture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
