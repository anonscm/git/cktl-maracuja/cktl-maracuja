/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPlancoBilanPoste.java instead.
package org.cocktail.maracuja.client.metier;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public abstract class _EOPlancoBilanPoste extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PlancoBilanPoste";
	public static final String ENTITY_TABLE_NAME = "PLANCO_BILAN_POSTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pbpId";

	public static final String PBP_ID_COLONNE_KEY = "pbpIdColonne";
	public static final String PBP_SENS_KEY = "pbpSens";
	public static final String PBP_SIGNE_KEY = "pbpSigne";
	public static final String PBP_SUBDIV_KEY = "pbpSubdiv";

// Attributs non visibles
	public static final String PCOE_ID_KEY = "pcoeId";
	public static final String PBP_ID_KEY = "pbpId";
	public static final String BP_ID_KEY = "bpId";

//Colonnes dans la base de donnees
	public static final String PBP_ID_COLONNE_COLKEY = "PBP_ID_COLONNE";
	public static final String PBP_SENS_COLKEY = "PBP_SENS";
	public static final String PBP_SIGNE_COLKEY = "PBP_SIGNE";
	public static final String PBP_SUBDIV_COLKEY = "PBP_SUBDIV";

	public static final String PCOE_ID_COLKEY = "PCOE_ID";
	public static final String PBP_ID_COLKEY = "PBP_ID";
	public static final String BP_ID_COLKEY = "BP_ID";


	// Relationships
	public static final String TO_BILAN_POSTE_KEY = "toBilanPoste";
	public static final String TO_PLAN_COMPTABLE_EXER_KEY = "toPlanComptableExer";



	// Accessors methods
  public String pbpIdColonne() {
    return (String) storedValueForKey(PBP_ID_COLONNE_KEY);
  }

  public void setPbpIdColonne(String value) {
    takeStoredValueForKey(value, PBP_ID_COLONNE_KEY);
  }

  public String pbpSens() {
    return (String) storedValueForKey(PBP_SENS_KEY);
  }

  public void setPbpSens(String value) {
    takeStoredValueForKey(value, PBP_SENS_KEY);
  }

  public String pbpSigne() {
    return (String) storedValueForKey(PBP_SIGNE_KEY);
  }

  public void setPbpSigne(String value) {
    takeStoredValueForKey(value, PBP_SIGNE_KEY);
  }

  public String pbpSubdiv() {
    return (String) storedValueForKey(PBP_SUBDIV_KEY);
  }

  public void setPbpSubdiv(String value) {
    takeStoredValueForKey(value, PBP_SUBDIV_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOBilanPoste toBilanPoste() {
    return (org.cocktail.maracuja.client.metier.EOBilanPoste)storedValueForKey(TO_BILAN_POSTE_KEY);
  }

  public void setToBilanPosteRelationship(org.cocktail.maracuja.client.metier.EOBilanPoste value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBilanPoste oldValue = toBilanPoste();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BILAN_POSTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BILAN_POSTE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOPlanComptableExer toPlanComptableExer() {
    return (org.cocktail.maracuja.client.metier.EOPlanComptableExer)storedValueForKey(TO_PLAN_COMPTABLE_EXER_KEY);
  }

  public void setToPlanComptableExerRelationship(org.cocktail.maracuja.client.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOPlanComptableExer oldValue = toPlanComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PLAN_COMPTABLE_EXER_KEY);
    }
  }
  

  public static EOPlancoBilanPoste createPlancoBilanPoste(EOEditingContext editingContext, String pbpIdColonne
, String pbpSens
, String pbpSigne
, String pbpSubdiv
, org.cocktail.maracuja.client.metier.EOPlanComptableExer toPlanComptableExer) {
    EOPlancoBilanPoste eo = (EOPlancoBilanPoste) createAndInsertInstance(editingContext, _EOPlancoBilanPoste.ENTITY_NAME);    
		eo.setPbpIdColonne(pbpIdColonne);
		eo.setPbpSens(pbpSens);
		eo.setPbpSigne(pbpSigne);
		eo.setPbpSubdiv(pbpSubdiv);
    eo.setToPlanComptableExerRelationship(toPlanComptableExer);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPlancoBilanPoste.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPlancoBilanPoste.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOPlancoBilanPoste localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPlancoBilanPoste)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOPlancoBilanPoste localInstanceIn(EOEditingContext editingContext, EOPlancoBilanPoste eo) {
    EOPlancoBilanPoste localInstance = (eo == null) ? null : (EOPlancoBilanPoste)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPlancoBilanPoste#localInstanceIn a la place.
   */
	public static EOPlancoBilanPoste localInstanceOf(EOEditingContext editingContext, EOPlancoBilanPoste eo) {
		return EOPlancoBilanPoste.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOPlancoBilanPoste fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPlancoBilanPoste fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPlancoBilanPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPlancoBilanPoste)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPlancoBilanPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPlancoBilanPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPlancoBilanPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPlancoBilanPoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPlancoBilanPoste fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPlancoBilanPoste eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPlancoBilanPoste ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPlancoBilanPoste fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
