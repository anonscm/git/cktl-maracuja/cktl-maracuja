/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOPlanComptable.java
// 
package org.cocktail.maracuja.client.metier;

import java.util.HashMap;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPlanComptable extends _EOPlanComptable {

	private static HashMap<String, String> pcoLibelleMap = new HashMap<String, String>();

	//	public static EOExercice currentExercice = null;
	//	public static EOQualifier qualExer = null;

	public static final String PCONUMETPCOLIBELLE_KEY = "pcoNumEtPcoLibelle";

	//    
	public static final String CLASSE0 = "0";
	public static final String CLASSE1 = "1";
	public static final String CLASSE2 = "2";
	public static final String CLASSE3 = "3";
	public static final String CLASSE4 = "4";
	public static final String CLASSE5 = "5";
	public static final String CLASSE6 = "6";
	public static final String CLASSE7 = "7";
	public static final String CLASSE8 = "8";
	public static final String CLASSE9 = "9";

	public EOPlanComptable() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	//
	/**
	 * @return La classe du compte (premier caractère du pcoNum ou null).
	 */
	public final String getClasseCompte() {
		if (pcoNum() != null) {
			return pcoNum().substring(0, 1);
		}
		return null;
	}

	/**
	 * @return Le chapitre du compte (deux premiers caractères du pcoNum ou null).
	 */
	public final String getChapitreCompte() {
		if (pcoNum() != null && pcoNum().length() >= 2) {
			return pcoNum().substring(0, 2);
		}
		return null;
	}

	public final String pcoNumEtPcoLibelle() {
		return pcoNum() + " : " + pcoLibelle();
	}

	public String toString() {
		return pcoNumEtPcoLibelle();
	}

	public boolean isAmortissementPossible() {
		return CLASSE2.equals(getClasseCompte());
	}

	//
	//		public static EOExercice getCurrentExercice() {
	//			return currentExercice;
	//		}
	//
	//
	//		public static void setCurrentExercice(EOExercice currentExercice) {
	//			EOPlanComptable.currentExercice = currentExercice;
	//			qualExer = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + "=%@", new NSArray(new Object[]{getCurrentExercice()}));
	//			
	//		}
	//        
	/**
	 * on renvoit le libellé du compte sur l'exercice actif.
	 */
	public String pcoLibelle() {
		return (pcoLibelleMap.get(pcoNum()) != null ? pcoLibelleMap.get(pcoNum()) : super.pcoLibelle());
		//return super.pcoLibelle();
		//			return (currentPlancoExer()==null ? super.pcoLibelle() : currentPlancoExer().pcoLibelle());
	}

	//		/**
	//		 * 
	//		 * @return le plancomptablexer associé (en fonction de l'exercice en cours)
	//		 */
	//		public EOPlanComptableExer currentPlancoExer() {
	//			//si exercice changé, on reinitialise le cache
	//			if (_currentPlancoExer!=null && _currentPlancoExer.exercice()!=null && !_currentPlancoExer.exercice().equals(getCurrentExercice())) {
	//				_currentPlancoExer = null;
	//			}
	//			//si cache vide, on tente de l'initialiser
	//			if (_currentPlancoExer == null ) {
	//				if (getCurrentExercice()==null || planComptableExers().count()==0) {
	//					_currentPlancoExer = null;
	//				}
	//				else {
	//					NSArray res = EOQualifier.filteredArrayWithQualifier(planComptableExers(), qualExer);
	//					if (res.count()>0) {
	//						_currentPlancoExer = (EOPlanComptableExer) res.objectAtIndex(0);
	//					}
	//					else {
	//						_currentPlancoExer = null;
	//					}
	//				}
	//			}
	//			return _currentPlancoExer;
	//		}

	/**
	 * A appeler lors du lancement de l'appli, à chaque changement d'exercice ou à chaque modification de plancoExer
	 */
	public static void refreshPcoLibelleMap(EOEditingContext edc, EOExercice exercice) {
		pcoLibelleMap.clear();
		NSArray res = EOPlanComptableExer.fetchAll(edc, exercice);
		for (int i = 0; i < res.count(); i++) {
			EOPlanComptableExer pco = (EOPlanComptableExer) res.objectAtIndex(i);
			pcoLibelleMap.put(pco.pcoNum(), pco.pcoLibelle());
		}
	}

	public static HashMap<String, String> getPcoLibelleMap() {
		return pcoLibelleMap;
	}

}
