/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
package org.cocktail.maracuja.client.metier;

import java.util.Date;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.zutil.client.ZDateUtil;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOImSuspension extends _EOImSuspension {

	public static final String IMSUS_TYPE_FOR_DISPLAY_KEY = "typeForDisplay";
	public static final String TYPE_COMPTABLE_KEY = "C";
	public static final String TYPE_COMPTABLE_VALUE = "COMPTABLE";
	public static final String TYPE_ORDONNATEUR_KEY = "O";
	public static final String TYPE_ORDONNATEUR_VALUE = "ORDONNATEUR";

	public static final EOSortOrdering SORT_DEBUT_ASC = EOSortOrdering.sortOrderingWithKey(EOImSuspension.IMSUS_DEBUT_KEY, EOSortOrdering.CompareAscending);
	public static final EOQualifier QUAL_VALIDE = new EOKeyValueQualifier(EOImSuspension.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_VALIDE);

	public EOImSuspension() {
		super();
	}

	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDateCreation(ZDateUtil.now());
		setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (imsusType() == null) {
			setImsusType(TYPE_COMPTABLE_KEY);
		}

		if (utilisateur() == null) {
			throw new NSValidation.ValidationException("L'utilisateur est obligatoire");
		}
		if (jdDepensePapier() == null) {
			throw new NSValidation.ValidationException("Référence a la facture papier obligatoire");
		}
		if (imsusDebut() == null) {
			throw new NSValidation.ValidationException("La date de début est obligatoire");
		}
		if (imsusFin() != null && imsusDebut().after(imsusFin())) {
			throw new NSValidation.ValidationException("La date de début doit être antérieure à la date de fin.");
		}

		//Verifier que le mandat associé n'est pas payé ou annulé
		NSArray mandatsConcernes = jdDepensePapier().getMandats();
		for (int i = 0; i < mandatsConcernes.count(); i++) {
			EOMandat mandat = (EOMandat) mandatsConcernes.objectAtIndex(i);
			if (mandat.isPaye() && mandat.paiement().paiDateCreation().before(imsusFin())) {
				throw new NSValidation.ValidationException("Le mandat " + mandat.gestion().gesCode() + "/" + mandat.manNumero() + " a été payé le " + ZConst.FORMAT_DATESHORT.format(mandat.paiement().paiDateCreation()) + ", impossible d'ajouter une suspension qui se termine avant sur la facture "
						+ jdDepensePapier().dppNumeroFacture());
			}
			if (mandat.isAnnule()) {
				throw new NSValidation.ValidationException("Le mandat " + mandat.gestion().gesCode() + "/" + mandat.manNumero() + " a été rejeté, impossible d'ajouter une suspension sur la facture " + jdDepensePapier().dppNumeroFacture());
			}
		}

		checkChevauchement();
		checkContinuite();
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		setDateModification(ZDateUtil.now());
	}

	/**
	 * @return le type de la suspension sous forme affichable.
	 */
	public String typeForDisplay() {
		if (TYPE_COMPTABLE_KEY.equals(imsusType())) {
			return TYPE_COMPTABLE_VALUE;
		}
		if (TYPE_ORDONNATEUR_KEY.equals(imsusType())) {
			return TYPE_ORDONNATEUR_VALUE;
		}
		return null;
	}

	public static NSArray fetchAll(EOEditingContext ec, EOQualifier qual, NSArray sortOrderings, boolean refresh) {
		EOFetchSpecification spec = new EOFetchSpecification(ENTITY_NAME, qual, null, true, false, null);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(spec);
	}

	/**
	 * @param ec
	 * @param mandat
	 * @param sortOrderings
	 * @param refresh
	 * @return Les suspensions valides associees a un mandat (recuperees a
	 *         partir des depenses papiers associees au mandat).
	 */
	public static NSArray fetchAllForMandat(EOEditingContext ec, EOMandat mandat, NSArray sortOrderings, boolean refresh) {
		NSArray jdDepensePapiers = EOJdDepensePapier.fetchAllForMandat(ec, mandat, null, refresh);
		if (jdDepensePapiers.count() == 0) {
			return NSArray.EmptyArray;
		}

		NSMutableArray suspensions = new NSMutableArray();
		for (int i = 0; i < jdDepensePapiers.count(); i++) {
			EOJdDepensePapier jdDepensePapier = (EOJdDepensePapier) jdDepensePapiers.objectAtIndex(i);
			suspensions.addObjectsFromArray(jdDepensePapier.imSuspensionsValides());
		}
		//		EOQualifier.filterArrayWithQualifier(suspensions, QUAL_VALIDE);

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(suspensions, sortOrderings).immutableClone();
	}

	/**
	 * @return true si la suspension est supprimable.
	 */
	public boolean isSupprimable() {
		try {
			checkIfSupprimable();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void checkIfSupprimable() throws Exception {
		checkIfModifiable();

	}

	public void checkIfModifiable() throws Exception {
		if (!TYPE_COMPTABLE_KEY.equals(imsusType())) {
			throw new Exception("Impossible de modifier une suspension crééer par l'ordonnateur.");
		}
		EOQualifier qual = new EOOrQualifier(new NSArray(new Object[] { EOMandat.QUAL_PAYE, EOMandat.QUAL_ANNULE }));
		NSArray res = EOQualifier.filteredArrayWithQualifier(jdDepensePapier().getMandats(), qual);
		if (res.count() > 0) {
			throw new Exception("La facture reliée a cette suspension est rattachée à un mandat qui a déjà été payé ou annulé, impossible de modifier la suspension.");
		}

	}

	public boolean isModifiable() {
		try {
			checkIfModifiable();

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Verifier que les suspensions ne se chevauchent pas pour la même facture
	 * papier.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkChevauchement() throws NSValidation.ValidationException {
		EOJdDepensePapier depensePapier = jdDepensePapier();
		NSTimestamp deb = imsusDebut();
		NSTimestamp fin = imsusFin();

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOImSuspension.JD_DEPENSE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, depensePapier));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImSuspension.IMSUS_FIN_KEY + ">=%@ or " + EOImSuspension.IMSUS_FIN_KEY + "=nil", new NSArray(new Object[] { deb })));
		if (fin != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImSuspension.IMSUS_DEBUT_KEY + "<=%@", new NSArray(new Object[] { fin })));
		}

		NSArray susps = jdDepensePapier().imSuspensionsValides();
		NSArray res = EOQualifier.filteredArrayWithQualifier(susps, new EOAndQualifier(quals));
		boolean found = false;
		for (int i = 0; i < res.count() && !found; i++) {
			if (!editingContext().globalIDForObject((EOEnterpriseObject) res.objectAtIndex(i)).equals(editingContext().globalIDForObject(this))) {
				found = true;
			}
		}
		if (found) {
			throw new NSValidation.ValidationException("Une suspension existe déjà sur la période spécifiée. Changez la période.");
		}

	}

	/**
	 * Verifie que les suspensions sont bien saisies dans la continuite pour la
	 * même facture papier (pas de trous dans les dates).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkContinuite() throws NSValidation.ValidationException {
		NSArray susps = EOSortOrdering.sortedArrayUsingKeyOrderArray(jdDepensePapier().imSuspensionsValides(), new NSArray(new Object[] { EOImSuspension.SORT_DEBUT_ASC }));
		Date lastDate = null;
		for (int i = 0; i < susps.count(); i++) {
			EOImSuspension susp = (EOImSuspension) susps.objectAtIndex(i);
			if (lastDate == null) {
				lastDate = ZDateUtil.getDateOnly(susp.imsusFin());
			}
			else {
				Date deb = ZDateUtil.getDateOnly(susp.imsusDebut());
				if (ZDateUtil.addDHMS(lastDate, 1, 0, 0, 0).getTime() != deb.getTime()) {
					throw new NSValidation.ValidationException("Les suspensions saisies pour la facture '" + jdDepensePapier().dppNumeroFacture() + "' doivent l'être en continuité (pas de trou entre les dates). Problème décelé entre la suspension qui se termine le "
							+ ZConst.FORMAT_DATESHORT.format(lastDate) + " et celle qui commence le " + ZConst.FORMAT_DATESHORT.format(deb));
				}
			}

		}

	}
}
