// _EOImSuspension.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOImSuspension.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOImSuspension extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ImSuspension";
	public static final String ENTITY_TABLE_NAME = "maracuja.IM_SUSPENSION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "imsusId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_MODIFICATION_KEY = "dateModification";
	public static final String IMSUS_COMMENTAIRE_KEY = "imsusCommentaire";
	public static final String IMSUS_DEBUT_KEY = "imsusDebut";
	public static final String IMSUS_FIN_KEY = "imsusFin";
	public static final String IMSUS_TYPE_KEY = "imsusType";

// Attributs non visibles
	public static final String DPP_ID_KEY = "dppId";
	public static final String IMSUS_ETAT_KEY = "imsusEtat";
	public static final String IMSUS_ID_KEY = "imsusId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String IMSUS_COMMENTAIRE_COLKEY = "IMSUS_COMMENTAIRE";
	public static final String IMSUS_DEBUT_COLKEY = "IMSUS_DEBUT";
	public static final String IMSUS_FIN_COLKEY = "IMSUS_FIN";
	public static final String IMSUS_TYPE_COLKEY = "IMSUS_TYPE";

	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String IMSUS_ETAT_COLKEY = "IMSUS_ETAT";
	public static final String IMSUS_ID_COLKEY = "IMSUS_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String JD_DEPENSE_PAPIER_KEY = "jdDepensePapier";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public String imsusCommentaire() {
    return (String) storedValueForKey(IMSUS_COMMENTAIRE_KEY);
  }

  public void setImsusCommentaire(String value) {
    takeStoredValueForKey(value, IMSUS_COMMENTAIRE_KEY);
  }

  public NSTimestamp imsusDebut() {
    return (NSTimestamp) storedValueForKey(IMSUS_DEBUT_KEY);
  }

  public void setImsusDebut(NSTimestamp value) {
    takeStoredValueForKey(value, IMSUS_DEBUT_KEY);
  }

  public NSTimestamp imsusFin() {
    return (NSTimestamp) storedValueForKey(IMSUS_FIN_KEY);
  }

  public void setImsusFin(NSTimestamp value) {
    takeStoredValueForKey(value, IMSUS_FIN_KEY);
  }

  public String imsusType() {
    return (String) storedValueForKey(IMSUS_TYPE_KEY);
  }

  public void setImsusType(String value) {
    takeStoredValueForKey(value, IMSUS_TYPE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOJdDepensePapier jdDepensePapier() {
    return (org.cocktail.maracuja.client.metier.EOJdDepensePapier)storedValueForKey(JD_DEPENSE_PAPIER_KEY);
  }

  public void setJdDepensePapierRelationship(org.cocktail.maracuja.client.metier.EOJdDepensePapier value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOJdDepensePapier oldValue = jdDepensePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, JD_DEPENSE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, JD_DEPENSE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.maracuja.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.maracuja.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

  public static EOImSuspension createImSuspension(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, NSTimestamp imsusDebut
, String imsusType
, org.cocktail.maracuja.client.metier.EOJdDepensePapier jdDepensePapier, org.cocktail.maracuja.client.metier.EOTypeEtat typeEtat, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOImSuspension eo = (EOImSuspension) createAndInsertInstance(editingContext, _EOImSuspension.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setImsusDebut(imsusDebut);
		eo.setImsusType(imsusType);
    eo.setJdDepensePapierRelationship(jdDepensePapier);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOImSuspension.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOImSuspension.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOImSuspension creerInstance(EOEditingContext editingContext) {
		  		EOImSuspension object = (EOImSuspension)createAndInsertInstance(editingContext, _EOImSuspension.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOImSuspension localInstanceIn(EOEditingContext editingContext) {
	  		return (EOImSuspension)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOImSuspension localInstanceIn(EOEditingContext editingContext, EOImSuspension eo) {
    EOImSuspension localInstance = (eo == null) ? null : (EOImSuspension)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOImSuspension#localInstanceIn a la place.
   */
	public static EOImSuspension localInstanceOf(EOEditingContext editingContext, EOImSuspension eo) {
		return EOImSuspension.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOImSuspension fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOImSuspension fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOImSuspension eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOImSuspension)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOImSuspension fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOImSuspension fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOImSuspension eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOImSuspension)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOImSuspension fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOImSuspension eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOImSuspension ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOImSuspension fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
