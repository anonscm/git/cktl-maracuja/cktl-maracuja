/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EODepense.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EODepense extends _EODepense implements IHasAccordContrat {
	public static final String DEP_SUPPRESSION_OUI = "OUI";
	public static final String DEP_SUPPRESSION_NON = "NON";
	public static final String JD_DEPENSE_CTRL_PLANCO_KEY = "JdDepenseCtrlPlanco";
	public static final String ACCORD_CONTRAT_UNIQUE_KEY = "getAccordContratUnique";

	public static final String MONTANT_RETENUES_KEY = "montantRetenues";

	private NSArray _accordContratLiesCache;

	public EODepense() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return Le montant (calcule) de toutes les retenues affectees a la depense.
	 */

	public BigDecimal montantRetenues() {
		BigDecimal m = new BigDecimal(0).setScale(2);
		for (int i = 0; i < retenues().count(); i++) {
			EORetenue element = (EORetenue) retenues().objectAtIndex(i);
			m = m.add(element.retMontant());
		}
		//	ZLogger.verbose(">>montantRetenues() : " + m);
		return m;

	}

	/**
	 * Calcule le montant à payer en fonction des retenues.
	 */
	public BigDecimal calcMontantDisquette() throws Exception {
		final BigDecimal m = depTtc().add(montantRetenues().negate());
		if (m.signum() < 0) {
			throw new Exception("Le total des retenues ne peut être supérieur au montant de la dépense.");
		}
		ZLogger.verbose(">>calcMontantDisquette() : " + m);
		return m;
	}

	/**
	 * Calcule et met a jour le montant à payer en fonction des retenues.
	 */
	public void majMontantDisquette() throws Exception {
		ZLogger.verbose(">>majMontantDisquette()");
		setDepMontantDisquette(calcMontantDisquette());
	}

	/**
	 * renvoie le libelle 1 pour la disquette BDF (32 caractères)
	 * 
	 * @return
	 */
	public String getLibelleBdf1() {
		String lib = "";
		if (depNumero() != null) {
			lib = depNumero().trim();
		}
		return lib;
	}

	/**
	 * renvoie le libelle 2 pour la disquette BDF (32 caractères)
	 * 
	 * @return
	 */
	public String getLibelleBdf2() {
		String lib = "";
		if (depDateReception() != null) {
			lib += " DU " + new SimpleDateFormat("ddMMyy").format(depDateReception());
		}
		lib += " MAN. " + Integer.toString(mandat().manNumero().intValue());
		return lib;
	}

	public EOJdDepenseCtrlPlanco getJdDepenseCtrlPlanco() {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier("dpcoId", EOQualifier.QualifierOperatorEqual, depOrdre()));
		quals.addObject(new EOKeyValueQualifier(EOJdDepenseCtrlPlanco.MANDAT_KEY, EOQualifier.QualifierOperatorEqual, mandat()));
		NSArray res = EOJdDepenseCtrlPlanco.fetchAll(editingContext(), new EOAndQualifier(quals), null, false);
		if (res.count() > 0) {
			return (EOJdDepenseCtrlPlanco) res.objectAtIndex(0);
		}
		return null;

	}

	public NSArray getAccordContratLies() {
		if (_accordContratLiesCache != null) {
			return _accordContratLiesCache;
		}

		NSMutableArray res = new NSMutableArray();
		Integer manId = (Integer) ServerProxy.serverPrimaryKeyForObject(editingContext(), this.mandat()).valueForKey(EOMandat.MAN_ID_KEY);
		NSMutableArray quals = new NSMutableArray();
		quals.addObjects(new EOKeyValueQualifier(EOJdDepenseCtrlPlanco.DPCO_ID_KEY, EOQualifier.QualifierOperatorEqual, this.depOrdre()));
		quals.addObjects(new EOKeyValueQualifier(EOJdDepenseCtrlPlanco.MAN_ID_KEY, EOQualifier.QualifierOperatorEqual, manId));
		EOJdDepenseCtrlPlanco dpco = EOJdDepenseCtrlPlanco.fetchByQualifier(editingContext(), new EOAndQualifier(quals));
		if (dpco != null) {
			NSArray rccs = dpco.toDepenseBudget().toDepenseCtrlConventions();
			for (int i = 0; i < rccs.count(); i++) {
				EOAccordsContrat contrat = ((EOJDDepenseCtrlConvention) rccs.objectAtIndex(i)).toAccordsContrat();
				if (rccs.indexOfObject(contrat) == NSArray.NotFound) {
					res.addObject(contrat);
				}
			}
		}
		_accordContratLiesCache = res.immutableClone();
		return _accordContratLiesCache;
	}

	public EOAccordsContrat getAccordContratUnique() {
		EOAccordsContrat contrat = null;
		NSArray res = getAccordContratLies();
		if (res.count() == 1) {
			contrat = (EOAccordsContrat) res.objectAtIndex(0);
		}
		return contrat;
	}

}
