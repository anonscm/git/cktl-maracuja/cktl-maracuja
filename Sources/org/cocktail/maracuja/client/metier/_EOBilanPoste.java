// _EOBilanPoste.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBilanPoste.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBilanPoste extends  EOGenericRecord {
	public static final String ENTITY_NAME = "BilanPoste";
	public static final String ENTITY_TABLE_NAME = "maracuja.BILAN_POSTE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bpId";

	public static final String BP_FORMULE_AMORTISSEMENT_KEY = "bpFormuleAmortissement";
	public static final String BP_FORMULE_MONTANT_KEY = "bpFormuleMontant";
	public static final String BP_GROUPE_KEY = "bpGroupe";
	public static final String BP_ID_PERE_KEY = "bpIdPere";
	public static final String BP_LIBELLE_KEY = "bpLibelle";
	public static final String BP_MODIFIABLE_KEY = "bpModifiable";
	public static final String BP_NIVEAU_KEY = "bpNiveau";
	public static final String BP_ORDRE_KEY = "bpOrdre";
	public static final String BP_STR_ID_KEY = "bpStrId";

// Attributs non visibles
	public static final String BP_ID_KEY = "bpId";
	public static final String BT_ID_KEY = "btId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String BP_FORMULE_AMORTISSEMENT_COLKEY = "BP_FORMULE_AMORTISSEMENT";
	public static final String BP_FORMULE_MONTANT_COLKEY = "BP_FORMULE_MONTANT";
	public static final String BP_GROUPE_COLKEY = "BP_GROUPE";
	public static final String BP_ID_PERE_COLKEY = "BP_ID_PERE";
	public static final String BP_LIBELLE_COLKEY = "BP_LIBELLE";
	public static final String BP_MODIFIABLE_COLKEY = "BP_MODIFIABLE";
	public static final String BP_NIVEAU_COLKEY = "BP_NIVEAU";
	public static final String BP_ORDRE_COLKEY = "BP_ORDRE";
	public static final String BP_STR_ID_COLKEY = "BP_STR_ID";

	public static final String BP_ID_COLKEY = "BP_ID";
	public static final String BT_ID_COLKEY = "BT_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String TO_BILAN_POSTE_ENFANTS_KEY = "toBilanPosteEnfants";
	public static final String TO_BILAN_POSTE_PERE_KEY = "toBilanPostePere";
	public static final String TO_BILAN_TYPE_KEY = "toBilanType";
	public static final String TO_EXERCICE_KEY = "toExercice";



	// Accessors methods
  public String bpFormuleAmortissement() {
    return (String) storedValueForKey(BP_FORMULE_AMORTISSEMENT_KEY);
  }

  public void setBpFormuleAmortissement(String value) {
    takeStoredValueForKey(value, BP_FORMULE_AMORTISSEMENT_KEY);
  }

  public String bpFormuleMontant() {
    return (String) storedValueForKey(BP_FORMULE_MONTANT_KEY);
  }

  public void setBpFormuleMontant(String value) {
    takeStoredValueForKey(value, BP_FORMULE_MONTANT_KEY);
  }

  public String bpGroupe() {
    return (String) storedValueForKey(BP_GROUPE_KEY);
  }

  public void setBpGroupe(String value) {
    takeStoredValueForKey(value, BP_GROUPE_KEY);
  }

  public Integer bpIdPere() {
    return (Integer) storedValueForKey(BP_ID_PERE_KEY);
  }

  public void setBpIdPere(Integer value) {
    takeStoredValueForKey(value, BP_ID_PERE_KEY);
  }

  public String bpLibelle() {
    return (String) storedValueForKey(BP_LIBELLE_KEY);
  }

  public void setBpLibelle(String value) {
    takeStoredValueForKey(value, BP_LIBELLE_KEY);
  }

  public String bpModifiable() {
    return (String) storedValueForKey(BP_MODIFIABLE_KEY);
  }

  public void setBpModifiable(String value) {
    takeStoredValueForKey(value, BP_MODIFIABLE_KEY);
  }

  public Integer bpNiveau() {
    return (Integer) storedValueForKey(BP_NIVEAU_KEY);
  }

  public void setBpNiveau(Integer value) {
    takeStoredValueForKey(value, BP_NIVEAU_KEY);
  }

  public Integer bpOrdre() {
    return (Integer) storedValueForKey(BP_ORDRE_KEY);
  }

  public void setBpOrdre(Integer value) {
    takeStoredValueForKey(value, BP_ORDRE_KEY);
  }

  public String bpStrId() {
    return (String) storedValueForKey(BP_STR_ID_KEY);
  }

  public void setBpStrId(String value) {
    takeStoredValueForKey(value, BP_STR_ID_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOBilanPoste toBilanPostePere() {
    return (org.cocktail.maracuja.client.metier.EOBilanPoste)storedValueForKey(TO_BILAN_POSTE_PERE_KEY);
  }

  public void setToBilanPostePereRelationship(org.cocktail.maracuja.client.metier.EOBilanPoste value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBilanPoste oldValue = toBilanPostePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BILAN_POSTE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BILAN_POSTE_PERE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOBilanType toBilanType() {
    return (org.cocktail.maracuja.client.metier.EOBilanType)storedValueForKey(TO_BILAN_TYPE_KEY);
  }

  public void setToBilanTypeRelationship(org.cocktail.maracuja.client.metier.EOBilanType value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBilanType oldValue = toBilanType();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BILAN_TYPE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BILAN_TYPE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOExercice toExercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public NSArray toBilanPosteEnfants() {
    return (NSArray)storedValueForKey(TO_BILAN_POSTE_ENFANTS_KEY);
  }

  public NSArray toBilanPosteEnfants(EOQualifier qualifier) {
    return toBilanPosteEnfants(qualifier, null, false);
  }

  public NSArray toBilanPosteEnfants(EOQualifier qualifier, boolean fetch) {
    return toBilanPosteEnfants(qualifier, null, fetch);
  }

  public NSArray toBilanPosteEnfants(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOBilanPoste.TO_BILAN_POSTE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOBilanPoste.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toBilanPosteEnfants();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToBilanPosteEnfantsRelationship(org.cocktail.maracuja.client.metier.EOBilanPoste object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_BILAN_POSTE_ENFANTS_KEY);
  }

  public void removeFromToBilanPosteEnfantsRelationship(org.cocktail.maracuja.client.metier.EOBilanPoste object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BILAN_POSTE_ENFANTS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOBilanPoste createToBilanPosteEnfantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BilanPoste");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_BILAN_POSTE_ENFANTS_KEY);
    return (org.cocktail.maracuja.client.metier.EOBilanPoste) eo;
  }

  public void deleteToBilanPosteEnfantsRelationship(org.cocktail.maracuja.client.metier.EOBilanPoste object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_BILAN_POSTE_ENFANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToBilanPosteEnfantsRelationships() {
    Enumeration objects = toBilanPosteEnfants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToBilanPosteEnfantsRelationship((org.cocktail.maracuja.client.metier.EOBilanPoste)objects.nextElement());
    }
  }


  public static EOBilanPoste createBilanPoste(EOEditingContext editingContext, String bpGroupe
, String bpLibelle
, String bpModifiable
, Integer bpNiveau
, Integer bpOrdre
, String bpStrId
, org.cocktail.maracuja.client.metier.EOBilanType toBilanType, org.cocktail.maracuja.client.metier.EOExercice toExercice) {
    EOBilanPoste eo = (EOBilanPoste) createAndInsertInstance(editingContext, _EOBilanPoste.ENTITY_NAME);    
		eo.setBpGroupe(bpGroupe);
		eo.setBpLibelle(bpLibelle);
		eo.setBpModifiable(bpModifiable);
		eo.setBpNiveau(bpNiveau);
		eo.setBpOrdre(bpOrdre);
		eo.setBpStrId(bpStrId);
    eo.setToBilanTypeRelationship(toBilanType);
    eo.setToExerciceRelationship(toExercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBilanPoste.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBilanPoste.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOBilanPoste creerInstance(EOEditingContext editingContext) {
		  		EOBilanPoste object = (EOBilanPoste)createAndInsertInstance(editingContext, _EOBilanPoste.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOBilanPoste localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBilanPoste)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOBilanPoste localInstanceIn(EOEditingContext editingContext, EOBilanPoste eo) {
    EOBilanPoste localInstance = (eo == null) ? null : (EOBilanPoste)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBilanPoste#localInstanceIn a la place.
   */
	public static EOBilanPoste localInstanceOf(EOEditingContext editingContext, EOBilanPoste eo) {
		return EOBilanPoste.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOBilanPoste fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBilanPoste fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBilanPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBilanPoste)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBilanPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBilanPoste fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBilanPoste eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBilanPoste)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBilanPoste fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBilanPoste eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBilanPoste ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBilanPoste fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
