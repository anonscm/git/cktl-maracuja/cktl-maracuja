/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOModeRecouvrement.java
// 
package org.cocktail.maracuja.client.metier;

import java.util.HashMap;

import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;

import com.webobjects.foundation.NSValidation;

public class EOModeRecouvrement extends _EOModeRecouvrement {
	public static final String MOD_LIBELLE_LONG_KEY = "modLibelleLong";

	//    public static final String MODDOM_VIREMENT="VIREMENT";
	public static final String MODDOM_CHEQUE = "CHEQUE";
	//    public static final String MODDOM_CAISSE="CAISSE";
	public static final String MODDOM_INTERNE = "INTERNE";
	public static final String MODDOM_AUTRE = "AUTRE";

	public static final String etatValide = "VALIDE";
	public static final String etatInvalide = "ANNULE";

	public static final String MODDOM_ECHEANCIER = "ECHEANCIER";

	private HashMap<EOExercice, EOPlanComptableExer> _planCoPaiementSelonExercice = new HashMap<EOExercice, EOPlanComptableExer>();
	private HashMap<EOExercice, EOPlanComptableExer> _planCoVisaSelonExercice = new HashMap<EOExercice, EOPlanComptableExer>();

	public EOModeRecouvrement() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String modLibelleLong() {
		return modCode() + " - " + modLibelle();
	}

	/**
	 * @param exercicePaiement
	 * @return Le plancomptable exer à utiliser (sur l'exercice paiement) pour le debit du prelevement (si exercice de paiement différent exercice du
	 *         mode de paiement, on recupere le compte de BE affecté au compte initial).
	 * @throws Exception
	 */
	public EOPlanComptableExer getPlanCoPaiementSelonExercice(EOExercice exercicePaiement) throws Exception {
		if (_planCoPaiementSelonExercice.get(exercicePaiement) != null) {
			return _planCoPaiementSelonExercice.get(exercicePaiement);
		}
		EOPlanComptableExer res = null;
		if (planComptablePaiement() == null) {
			throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + " : Le mode de recouvrement " + modLibelleLong() + " devrait avoir un compte de paiement valide défini.");
		}
		EOPlanComptableExer pcoe = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercice(), planComptablePaiement().pcoNum(), false);
		if (pcoe == null || !pcoe.isValide()) {
			throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + ", mode de recouvrement " + modLibelleLong() + " : Le compte défini comme compte de paiement (" + planComptablePaiement().pcoNum() + ") n'est pas actif sur l'exercice "
					+ exercice().exeExercice() + ".");
		}

		if (exercicePaiement.exeExercice().intValue() == exercice().exeExercice().intValue()) {
			res = pcoe;
		}
		else {
			//prendre le compte de BE s'il existe sinon prendre le même compte s'il est actif su exercice en cours 
			if (pcoe.pcoCompteBe() != null) {
				EOPlanComptableExer pcoBE = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercicePaiement, pcoe.pcoCompteBe(), false);
				if (pcoBE == null || !pcoBE.isValide()) {
					throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + ", mode de recouvrement " + modLibelleLong() + ", compte de paiement (" + planComptablePaiement().pcoNum() + ") : Le compte de BE " + pcoe.pcoCompteBe()
							+ " n'est pas actif sur l'exercice " + exercicePaiement.exeExercice() + ".");
				}
				res = pcoBE;
			}
			else {
				EOPlanComptableExer pcoe2 = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercicePaiement, planComptablePaiement().pcoNum(), false);
				if (pcoe2 == null || !pcoe2.isValide()) {
					throw new Exception("Erreur paramétrage : Le compte " + planComptablePaiement().pcoNum() + " affecté au mode de recouvrement " + modLibelleLong() + " sur l'exercice " + exercice().exeExercice() + " n'est pas valide sur l'exercice " + exercicePaiement.exeExercice()
							+ ". Veuillez l'activer sur l'exercice " + exercicePaiement.exeExercice() + " ou indiquer un compte de BE sur l'exercice " + exercice().exeExercice() + ".");
				}
				res = pcoe2;
			}
		}
		_planCoPaiementSelonExercice.put(exercicePaiement, res);
		return res;
	}

	/**
	 * @param exercicePaiement
	 * @return Le plancomptable exer à utiliser (sur l'exercice paiement) pour le credit du prelevement (si exercice de paiement différent exercice du
	 *         mode de recouvrement, on recupere le compte de BE affecté au compte initial).
	 * @throws Exception
	 */
	public EOPlanComptableExer getPlanCoVisaSelonExercice(EOExercice exercicePaiement) throws Exception {
		if (_planCoVisaSelonExercice.get(exercicePaiement) != null) {
			return _planCoVisaSelonExercice.get(exercicePaiement);
		}
		EOPlanComptableExer res = null;
		if (planComptableVisa() == null) {
			throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + " : Le mode de recouvrement " + modLibelleLong() + " devrait avoir un compte de visa valide défini.");
		}
		EOPlanComptableExer pcoe = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercice(), planComptableVisa().pcoNum(), false);
		if (pcoe == null || !pcoe.isValide()) {
			throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + ", mode de recouvrement " + modLibelleLong() + " : Le compte défini comme compte de visa (" + planComptableVisa().pcoNum() + ") n'est pas actif sur l'exercice "
					+ exercice().exeExercice() + ".");
		}

		if (exercicePaiement.exeExercice().intValue() == exercice().exeExercice().intValue()) {
			res = pcoe;
		}
		else {
			//prendre le compte de BE s'il existe sinon prendre le même compte s'il est actif su exercice en cours 
			if (pcoe.pcoCompteBe() != null) {
				EOPlanComptableExer pcoBE = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercicePaiement, pcoe.pcoCompteBe(), false);
				if (pcoBE == null || !pcoBE.isValide()) {
					throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + ", mode de recouvrement " + modLibelleLong() + ", compte de paiement (" + planComptableVisa().pcoNum() + ") : Le compte de BE " + pcoe.pcoCompteBe()
							+ " n'est pas actif sur l'exercice " + exercicePaiement.exeExercice() + ".");
				}
				res = pcoBE;
			}
			else {
				EOPlanComptableExer pcoe2 = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercicePaiement, planComptableVisa().pcoNum(), false);
				if (pcoe2 == null || !pcoe2.isValide()) {
					throw new Exception("Erreur paramétrage : Le compte " + planComptableVisa().pcoNum() + " affecté au mode de recouvrement " + modLibelleLong() + " sur l'exercice " + exercice().exeExercice() + " n'est pas valide sur l'exercice " + exercicePaiement.exeExercice()
							+ ". Veuillez l'activer sur l'exercice " + exercicePaiement.exeExercice() + " ou indiquer un compte de BE sur l'exercice " + exercice().exeExercice() + ".");
				}
				res = pcoe2;
			}
		}
		_planCoVisaSelonExercice.put(exercicePaiement, res);
		return res;
	}

	public void cleanPlanCoPaiementSelonExercice() {
		_planCoPaiementSelonExercice.clear();
	}

	public void cleanPlanCoVisaSelonExercice() {
		_planCoVisaSelonExercice.clear();
	}

}
