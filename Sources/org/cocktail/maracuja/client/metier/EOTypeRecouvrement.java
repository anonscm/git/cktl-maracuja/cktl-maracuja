/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EOTypeRecouvrement.java
// 
package org.cocktail.maracuja.client.metier;


import org.cocktail.fwkcktlcompta.common.entities.ITypeRecouvrement;
import org.cocktail.fwkcktlcompta.common.helpers.TypeRecouvrementHelper;

import com.webobjects.foundation.NSValidation;

public class EOTypeRecouvrement extends _EOTypeRecouvrement implements ITypeRecouvrement {
    public final static String MOD_DOM_CAISSE="CAISSE"; 
    public final static String MOD_DOM_CHEQUE="CHEQUE"; 
    public final static String MOD_DOM_ECHEANCIER="ECHEANCIER"; 
    

    
    public final static String ETAT_VALIDE="O";
    
    

    public EOTypeRecouvrement() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


	public boolean isSepaSdd() {
		return TypeRecouvrementHelper.getSharedInstance().isSepaSdd(this);
	}

	public boolean isBdf() {
		return TypeRecouvrementHelper.getSharedInstance().isBdf(this);
	}

	public String getFileExtension() {
		return TypeRecouvrementHelper.getSharedInstance().getFileExtension(this);
	}

}
