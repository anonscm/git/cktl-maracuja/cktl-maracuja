/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.metier;

import java.util.HashMap;

import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOModePaiement extends _EOModePaiement {
	public static final String MODDOM_VIREMENT = "VIREMENT";
	public static final String MODDOM_CHEQUE = "CHEQUE";
	public static final String MODDOM_CAISSE = "CAISSE";
	public static final String MODDOM_INTERNE = "INTERNE";
	public static final String MODDOM_AUTRE = "AUTRE";
	public static final String MODDOM_A_EXTOURNER = "A EXTOURNER";

	public static final String etatValide = "VALIDE";
	public static final String etatInvalide = "ANNULE";

	public static final String EMA_AUTO_OUI = "1";
	public static final String EMA_AUTO_NON = "0";

	public static final String MOD_LIBELLE_LONG_KEY = "modLibelleLong";

	public static final EOQualifier QUAL_VIREMENT = new EOKeyValueQualifier(EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, MODDOM_VIREMENT);
	public static final EOQualifier QUAL_CHEQUE = new EOKeyValueQualifier(EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, MODDOM_CHEQUE);
	public static final EOQualifier QUAL_CAISSE = new EOKeyValueQualifier(EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, MODDOM_CAISSE);
	public static final EOQualifier QUAL_A_EXTOURNER = new EOKeyValueQualifier(EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, MODDOM_A_EXTOURNER);

	public static final String CONTREPARTIE_AGENCE = "AGENCE";
	public static final String CONTREPARTIE_COMPOSANTE = "COMPOSANTE";
	public static final Object PAIEMENT_HT_OUI = "O";
	public static final Object PAIEMENT_HT_NON = "N";
	private HashMap<EOExercice, EOPlanComptableExer> _planCoPaiementSelonExercice = new HashMap<EOExercice, EOPlanComptableExer>();

	public EOModePaiement() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (isPaiementHT()) {
			if (planComptableTvaCtp() == null) {
				throw new NSValidation.ValidationException("Les mandats passés avec ce mode de paiement seront payés au fournisseur en HT, vous devez donc indiquer le compte de TVA en crédit à utiliser pour l'écriture de TVA.");
			}
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String modLibelleLong() {
		return modCode() + " - " + modLibelle();
	}

	public boolean isPaiementHT() {
		return PAIEMENT_HT_OUI.equals(modPaiementHt());
	}

	public boolean isAExtourner() {
		return EOModePaiement.QUAL_A_EXTOURNER.evaluateWithObject(this);
	}

	public static final NSArray getModePaiementsValide(EOEditingContext ec, EOExercice exer) throws ZFinderException {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey(EOModePaiement.MOD_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOModePaiement.ENTITY_NAME, "modValidite=%@ and exercice=%@", new NSArray(new Object[] {
				EOModePaiement.etatValide, exer
		}), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static void checkAllModePaiementsValides(EOEditingContext edc, EOExercice exercice, String domaine) throws Exception {
		NSArray res = getModePaiementsValide(edc, exercice);
		if (domaine != null) {
			res = EOQualifier.filteredArrayWithQualifier(res, new EOKeyValueQualifier(MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, domaine));
		}

		for (int i = 0; i < res.count(); i++) {
			EOModePaiement element = (EOModePaiement) res.objectAtIndex(i);
			element.checkModePaiement();
		}
	}

	public void checkModePaiement() throws Exception {
		EOQualifier qualWithImputPaiement = new EOOrQualifier(new NSArray(new Object[] {
				QUAL_CAISSE, QUAL_CHEQUE, QUAL_VIREMENT
		}));

		if (qualWithImputPaiement.evaluateWithObject(this) && planComptablePaiement() == null) {
			throw new DefaultClientException("Exercice " + exercice().exeExercice().intValue() + " : Le mode de paiement " + modLibelleLong()
					+ " n'a pas d'imputation paiement associée, il sera impossible de générer les écritures de paiement, veuillez corriger ce problème avant d'accéder aux paiements.");
		}

		if (planComptablePaiement() != null) {
			final EOPlanComptableExer planco = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercice(), planComptablePaiement().pcoNum(), false);
			if (planco == null) {
				throw new DefaultClientException("Erreur de paramétrage sur exercice " + exercice().exeExercice().intValue() + ": Le compte " + planComptablePaiement().pcoLibelle() + " affecté comme compte de paiement pour le mode de paiement " + modLibelleLong()
						+ " n'est pas valide pour l'exercice "
						+ exercice().exeExercice() + ". Veuillez corriger cette erreur.");
			}
		}
	}

	/**
	 * @param exercicePaiement
	 * @return Le plancomptable exer à utiliser (sur l'exercice paiement) pour le credit du paiement (si exercice de paiement différent exercice du
	 *         mode de paiement, on recupere le compte de BE affecté au compte initial).
	 * @throws Exception
	 */
	public EOPlanComptableExer getPlanCoPaiementSelonExercice(EOExercice exercicePaiement) throws Exception {
		if (_planCoPaiementSelonExercice.get(exercicePaiement) != null) {
			return _planCoPaiementSelonExercice.get(exercicePaiement);
		}
		EOPlanComptableExer res = null;
		if (planComptablePaiement() == null) {
			throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + " : Le mode de paiement " + modLibelleLong() + " devrait avoir un compte de paiement valide défini.");
		}
		EOPlanComptableExer pcoe = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercice(), planComptablePaiement().pcoNum(), false);
		if (pcoe == null || !pcoe.isValide()) {
			throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + ", mode de paiement " + modLibelleLong() + " : Le compte défini comme compte de paiement (" + planComptablePaiement().pcoNum() + ") n'est pas actif sur l'exercice "
					+ exercice().exeExercice() + ".");
		}

		if (exercicePaiement.exeExercice().intValue() == exercice().exeExercice().intValue()) {
			res = pcoe;
		}
		else {
			//prendre le compte de BE s'il existe sinon prendre le même compte s'il est actif su exercice en cours 
			if (pcoe.pcoCompteBe() != null) {
				EOPlanComptableExer pcoBE = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercicePaiement, pcoe.pcoCompteBe(), false);
				if (pcoBE == null || !pcoBE.isValide()) {
					throw new Exception("Erreur paramétrage sur exercice " + exercice().exeExercice() + ", mode de paiement " + modLibelleLong() + ", compte de paiement (" + planComptablePaiement().pcoNum() + ") : Le compte de BE " + pcoe.pcoCompteBe()
							+ " n'est pas actif sur l'exercice " + exercicePaiement.exeExercice() + ".");
				}
				res = pcoBE;
			}
			else {
				EOPlanComptableExer pcoe2 = EOPlanComptableExerFinder.getPlancoExerForPcoNum(editingContext(), exercicePaiement, planComptablePaiement().pcoNum(), false);
				if (pcoe2 == null || !pcoe2.isValide()) {
					throw new Exception("Erreur paramétrage : Le compte " + planComptablePaiement().pcoNum() + " affecté au mode de paiement " + modLibelleLong() + " sur l'exercice " + exercice().exeExercice() + " n'est pas valide sur l'exercice " + exercicePaiement.exeExercice()
							+ ". Veuillez l'activer sur l'exercice " + exercicePaiement.exeExercice() + " ou indiquer un compte de BE sur l'exercice " + exercice().exeExercice() + ".");
				}
				res = pcoe2;
			}
		}
		_planCoPaiementSelonExercice.put(exercicePaiement, res);
		return res;
	}

	public void cleanPlanCoPaiementSelonExercice() {
		_planCoPaiementSelonExercice.clear();
	}

}
