/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EOTypeCredit.java
// 
package org.cocktail.maracuja.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOTypeCredit extends _EOTypeCredit {

    public static final String TCD_TYPE_RECETTE="RECETTE";
    public static final String TCD_TYPE_DEPENSE="DEPENSE";
    public static final String TCD_TYPE_RECETTE_SHORT="R";
    public static final String TCD_TYPE_DEPENSE_SHORT="D";
    public static final String TCD_BUDGET_BUDGETAIRE="BUDGETAIRE";
    public static final String TCD_BUDGET_EXECUTOIRE="EXECUTOIRE";
    public static final String TCD_BUDGET_RESERVE="RESERVE";
    
    public static final EOSortOrdering SORT_TCD_CODE_ASC = EOSortOrdering.sortOrderingWithKey(TCD_CODE_KEY, EOSortOrdering.CompareAscending);
    
    public static final EOQualifier QUAL_BUDGET_EXECUTOIRE = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_BUDGET_KEY + "=%@", new NSArray(new Object[]{TCD_BUDGET_EXECUTOIRE}));
    public static final EOQualifier QUAL_RECETTE = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY+"=%@", new NSArray(new Object[]{EOTypeCredit.TCD_TYPE_RECETTE}));
    public static final EOQualifier QUAL_DEPENSE = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TCD_TYPE_KEY+"=%@", new NSArray(new Object[]{EOTypeCredit.TCD_TYPE_DEPENSE}));
    
    public static final EOQualifier QUAL_DEPENSE_EXECUTOIRE = new EOAndQualifier(new NSArray(new Object[]{QUAL_BUDGET_EXECUTOIRE, QUAL_DEPENSE}));
    public static final EOQualifier QUAL_RECETTE_EXECUTOIRE = new EOAndQualifier(new NSArray(new Object[]{QUAL_BUDGET_EXECUTOIRE, QUAL_RECETTE}));
    
    
    public static final String TCD_SECT_1 = "1";
    public static final String TCD_SECT_2 = "2";
    
    
    
    public EOTypeCredit() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    public final String getTypeShort() {
        return ( TCD_TYPE_DEPENSE.equals(tcdType()) ? TCD_TYPE_DEPENSE_SHORT : TCD_TYPE_RECETTE_SHORT   );
    }


}
