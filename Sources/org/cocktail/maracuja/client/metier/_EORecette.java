// _EORecette.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecette.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecette extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Recette";
	public static final String ENTITY_TABLE_NAME = "maracuja.Recette";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "recId";

	public static final String MOD_CODE_KEY = "modCode";
	public static final String REC_DATE_KEY = "recDate";
	public static final String REC_DATE_LIMITE_PAIEMENT_KEY = "recDateLimitePaiement";
	public static final String REC_DEBITEUR_KEY = "recDebiteur";
	public static final String REC_IMPUTTVA_KEY = "recImputtva";
	public static final String REC_LIBELLE_KEY = "recLibelle";
	public static final String REC_LIGNE_BUDGETAIRE_KEY = "recLigneBudgetaire";
	public static final String REC_MONNAIE_KEY = "recMonnaie";
	public static final String REC_MONT_KEY = "recMont";
	public static final String REC_MONTANT_DISQUETTE_KEY = "recMontantDisquette";
	public static final String REC_MONT_TTC_KEY = "recMontTtc";
	public static final String REC_MONTTVA_KEY = "recMonttva";
	public static final String REC_NUM_KEY = "recNum";
	public static final String REC_ORDRE_KEY = "recOrdre";
	public static final String REC_PIECE_KEY = "recPiece";
	public static final String REC_REF_KEY = "recRef";
	public static final String REC_STAT_KEY = "recStat";
	public static final String REC_SUPPRESSION_KEY = "recSuppression";
	public static final String REC_TYPE_KEY = "recType";
	public static final String REC_VIREMENT_KEY = "recVirement";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MOD_ORDRE_KEY = "modOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String ORG_ORDRE_INTERNE_KEY = "orgOrdreInterne";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String REC_ID_KEY = "recId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TIT_ID_KEY = "titId";
	public static final String TIT_ORDRE_KEY = "titOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String MOD_CODE_COLKEY = "MOD_CODE";
	public static final String REC_DATE_COLKEY = "rec_DATE";
	public static final String REC_DATE_LIMITE_PAIEMENT_COLKEY = "rec_DATE_limite_paiement";
	public static final String REC_DEBITEUR_COLKEY = "rec_DEBITEUR";
	public static final String REC_IMPUTTVA_COLKEY = "rec_IMPUTTVA";
	public static final String REC_LIBELLE_COLKEY = "rec_libelle";
	public static final String REC_LIGNE_BUDGETAIRE_COLKEY = "rec_Ligne_Budgetaire";
	public static final String REC_MONNAIE_COLKEY = "rec_MONNAIE";
	public static final String REC_MONT_COLKEY = "rec_MONT";
	public static final String REC_MONTANT_DISQUETTE_COLKEY = "rec_Montant_Disquette";
	public static final String REC_MONT_TTC_COLKEY = "TIT_MONTTC";
	public static final String REC_MONTTVA_COLKEY = "rec_MONTTVA";
	public static final String REC_NUM_COLKEY = "rec_NUM";
	public static final String REC_ORDRE_COLKEY = "REC_ORDRE";
	public static final String REC_PIECE_COLKEY = "rec_PIECE";
	public static final String REC_REF_COLKEY = "rec_REF";
	public static final String REC_STAT_COLKEY = "rec_STAT";
	public static final String REC_SUPPRESSION_COLKEY = "rec_Suppression";
	public static final String REC_TYPE_COLKEY = "rec_TYPE";
	public static final String REC_VIREMENT_COLKEY = "rec_VIREMENT";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String GES_CODE_COLKEY = "ges_code";
	public static final String MOD_ORDRE_COLKEY = "MOD_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String ORG_ORDRE_COLKEY = "org_ordre";
	public static final String ORG_ORDRE_INTERNE_COLKEY = "rec_INTERNE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String RIB_ORDRE_COLKEY = "rib_ordre";
	public static final String TIT_ID_COLKEY = "TIT_ID";
	public static final String TIT_ORDRE_COLKEY = "tit_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String ECHEANCIER_KEY = "echeancier";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String GESTION_KEY = "gestion";
	public static final String MODE_PAIEMENT_KEY = "modePaiement";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String ORGAN_KEY = "organ";
	public static final String ORGAN_INTERNE_KEY = "organInterne";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String RECETTE_INFOS_KEY = "recetteInfos";
	public static final String RECETTE_RELANCES_KEY = "recetteRelances";
	public static final String RECETTE_REL_INFO_KEY = "recetteRelInfo";
	public static final String RECETTE_RESTE_RECOUVRER_KEY = "recetteResteRecouvrer";
	public static final String RIB_KEY = "rib";
	public static final String TITRE_KEY = "titre";
	public static final String TITRE_DETAIL_ECRITURES_KEY = "titreDetailEcritures";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String modCode() {
    return (String) storedValueForKey(MOD_CODE_KEY);
  }

  public void setModCode(String value) {
    takeStoredValueForKey(value, MOD_CODE_KEY);
  }

  public NSTimestamp recDate() {
    return (NSTimestamp) storedValueForKey(REC_DATE_KEY);
  }

  public void setRecDate(NSTimestamp value) {
    takeStoredValueForKey(value, REC_DATE_KEY);
  }

  public NSTimestamp recDateLimitePaiement() {
    return (NSTimestamp) storedValueForKey(REC_DATE_LIMITE_PAIEMENT_KEY);
  }

  public void setRecDateLimitePaiement(NSTimestamp value) {
    takeStoredValueForKey(value, REC_DATE_LIMITE_PAIEMENT_KEY);
  }

  public String recDebiteur() {
    return (String) storedValueForKey(REC_DEBITEUR_KEY);
  }

  public void setRecDebiteur(String value) {
    takeStoredValueForKey(value, REC_DEBITEUR_KEY);
  }

  public String recImputtva() {
    return (String) storedValueForKey(REC_IMPUTTVA_KEY);
  }

  public void setRecImputtva(String value) {
    takeStoredValueForKey(value, REC_IMPUTTVA_KEY);
  }

  public String recLibelle() {
    return (String) storedValueForKey(REC_LIBELLE_KEY);
  }

  public void setRecLibelle(String value) {
    takeStoredValueForKey(value, REC_LIBELLE_KEY);
  }

  public String recLigneBudgetaire() {
    return (String) storedValueForKey(REC_LIGNE_BUDGETAIRE_KEY);
  }

  public void setRecLigneBudgetaire(String value) {
    takeStoredValueForKey(value, REC_LIGNE_BUDGETAIRE_KEY);
  }

  public String recMonnaie() {
    return (String) storedValueForKey(REC_MONNAIE_KEY);
  }

  public void setRecMonnaie(String value) {
    takeStoredValueForKey(value, REC_MONNAIE_KEY);
  }

  public java.math.BigDecimal recMont() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONT_KEY);
  }

  public void setRecMont(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONT_KEY);
  }

  public java.math.BigDecimal recMontantDisquette() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONTANT_DISQUETTE_KEY);
  }

  public void setRecMontantDisquette(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONTANT_DISQUETTE_KEY);
  }

  public java.math.BigDecimal recMontTtc() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONT_TTC_KEY);
  }

  public void setRecMontTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONT_TTC_KEY);
  }

  public java.math.BigDecimal recMonttva() {
    return (java.math.BigDecimal) storedValueForKey(REC_MONTTVA_KEY);
  }

  public void setRecMonttva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, REC_MONTTVA_KEY);
  }

  public Integer recNum() {
    return (Integer) storedValueForKey(REC_NUM_KEY);
  }

  public void setRecNum(Integer value) {
    takeStoredValueForKey(value, REC_NUM_KEY);
  }

  public Integer recOrdre() {
    return (Integer) storedValueForKey(REC_ORDRE_KEY);
  }

  public void setRecOrdre(Integer value) {
    takeStoredValueForKey(value, REC_ORDRE_KEY);
  }

  public Integer recPiece() {
    return (Integer) storedValueForKey(REC_PIECE_KEY);
  }

  public void setRecPiece(Integer value) {
    takeStoredValueForKey(value, REC_PIECE_KEY);
  }

  public String recRef() {
    return (String) storedValueForKey(REC_REF_KEY);
  }

  public void setRecRef(String value) {
    takeStoredValueForKey(value, REC_REF_KEY);
  }

  public String recStat() {
    return (String) storedValueForKey(REC_STAT_KEY);
  }

  public void setRecStat(String value) {
    takeStoredValueForKey(value, REC_STAT_KEY);
  }

  public String recSuppression() {
    return (String) storedValueForKey(REC_SUPPRESSION_KEY);
  }

  public void setRecSuppression(String value) {
    takeStoredValueForKey(value, REC_SUPPRESSION_KEY);
  }

  public String recType() {
    return (String) storedValueForKey(REC_TYPE_KEY);
  }

  public void setRecType(String value) {
    takeStoredValueForKey(value, REC_TYPE_KEY);
  }

  public String recVirement() {
    return (String) storedValueForKey(REC_VIREMENT_KEY);
  }

  public void setRecVirement(String value) {
    takeStoredValueForKey(value, REC_VIREMENT_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOModePaiement modePaiement() {
    return (org.cocktail.maracuja.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_KEY);
  }

  public void setModePaiementRelationship(org.cocktail.maracuja.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOModePaiement oldValue = modePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.maracuja.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }

  public void setModeRecouvrementRelationship(org.cocktail.maracuja.client.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOrgan organ() {
    return (org.cocktail.maracuja.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.maracuja.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOrgan organInterne() {
    return (org.cocktail.maracuja.client.metier.EOOrgan)storedValueForKey(ORGAN_INTERNE_KEY);
  }

  public void setOrganInterneRelationship(org.cocktail.maracuja.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOrgan oldValue = organInterne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_INTERNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_INTERNE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.maracuja.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.maracuja.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EORecetteRelInfo recetteRelInfo() {
    return (org.cocktail.maracuja.client.metier.EORecetteRelInfo)storedValueForKey(RECETTE_REL_INFO_KEY);
  }

  public void setRecetteRelInfoRelationship(org.cocktail.maracuja.client.metier.EORecetteRelInfo value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EORecetteRelInfo oldValue = recetteRelInfo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_REL_INFO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_REL_INFO_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EORecetteResteRecouvrer recetteResteRecouvrer() {
    return (org.cocktail.maracuja.client.metier.EORecetteResteRecouvrer)storedValueForKey(RECETTE_RESTE_RECOUVRER_KEY);
  }

  public void setRecetteResteRecouvrerRelationship(org.cocktail.maracuja.client.metier.EORecetteResteRecouvrer value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EORecetteResteRecouvrer oldValue = recetteResteRecouvrer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_RESTE_RECOUVRER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_RESTE_RECOUVRER_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EORib rib() {
    return (org.cocktail.maracuja.client.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.client.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTitre titre() {
    return (org.cocktail.maracuja.client.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.maracuja.client.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray echeancier() {
    return (NSArray)storedValueForKey(ECHEANCIER_KEY);
  }

  public NSArray echeancier(EOQualifier qualifier) {
    return echeancier(qualifier, null, false);
  }

  public NSArray echeancier(EOQualifier qualifier, boolean fetch) {
    return echeancier(qualifier, null, fetch);
  }

  public NSArray echeancier(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOEcheancier.RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOEcheancier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = echeancier();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToEcheancierRelationship(org.cocktail.maracuja.client.metier.EOEcheancier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ECHEANCIER_KEY);
  }

  public void removeFromEcheancierRelationship(org.cocktail.maracuja.client.metier.EOEcheancier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ECHEANCIER_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOEcheancier createEcheancierRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Echeancier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ECHEANCIER_KEY);
    return (org.cocktail.maracuja.client.metier.EOEcheancier) eo;
  }

  public void deleteEcheancierRelationship(org.cocktail.maracuja.client.metier.EOEcheancier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ECHEANCIER_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEcheancierRelationships() {
    Enumeration objects = echeancier().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEcheancierRelationship((org.cocktail.maracuja.client.metier.EOEcheancier)objects.nextElement());
    }
  }

  public NSArray recetteInfos() {
    return (NSArray)storedValueForKey(RECETTE_INFOS_KEY);
  }

  public NSArray recetteInfos(EOQualifier qualifier) {
    return recetteInfos(qualifier, null, false);
  }

  public NSArray recetteInfos(EOQualifier qualifier, boolean fetch) {
    return recetteInfos(qualifier, null, fetch);
  }

  public NSArray recetteInfos(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EORecetteInfo.RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EORecetteInfo.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = recetteInfos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRecetteInfosRelationship(org.cocktail.maracuja.client.metier.EORecetteInfo object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_INFOS_KEY);
  }

  public void removeFromRecetteInfosRelationship(org.cocktail.maracuja.client.metier.EORecetteInfo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_INFOS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EORecetteInfo createRecetteInfosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteInfo");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_INFOS_KEY);
    return (org.cocktail.maracuja.client.metier.EORecetteInfo) eo;
  }

  public void deleteRecetteInfosRelationship(org.cocktail.maracuja.client.metier.EORecetteInfo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_INFOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteInfosRelationships() {
    Enumeration objects = recetteInfos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteInfosRelationship((org.cocktail.maracuja.client.metier.EORecetteInfo)objects.nextElement());
    }
  }

  public NSArray recetteRelances() {
    return (NSArray)storedValueForKey(RECETTE_RELANCES_KEY);
  }

  public NSArray recetteRelances(EOQualifier qualifier) {
    return recetteRelances(qualifier, null, false);
  }

  public NSArray recetteRelances(EOQualifier qualifier, boolean fetch) {
    return recetteRelances(qualifier, null, fetch);
  }

  public NSArray recetteRelances(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EORecetteRelance.RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EORecetteRelance.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = recetteRelances();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRecetteRelancesRelationship(org.cocktail.maracuja.client.metier.EORecetteRelance object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_RELANCES_KEY);
  }

  public void removeFromRecetteRelancesRelationship(org.cocktail.maracuja.client.metier.EORecetteRelance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_RELANCES_KEY);
  }

  public org.cocktail.maracuja.client.metier.EORecetteRelance createRecetteRelancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteRelance");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_RELANCES_KEY);
    return (org.cocktail.maracuja.client.metier.EORecetteRelance) eo;
  }

  public void deleteRecetteRelancesRelationship(org.cocktail.maracuja.client.metier.EORecetteRelance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_RELANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteRelancesRelationships() {
    Enumeration objects = recetteRelances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteRelancesRelationship((org.cocktail.maracuja.client.metier.EORecetteRelance)objects.nextElement());
    }
  }

  public NSArray titreDetailEcritures() {
    return (NSArray)storedValueForKey(TITRE_DETAIL_ECRITURES_KEY);
  }

  public NSArray titreDetailEcritures(EOQualifier qualifier) {
    return titreDetailEcritures(qualifier, null, false);
  }

  public NSArray titreDetailEcritures(EOQualifier qualifier, boolean fetch) {
    return titreDetailEcritures(qualifier, null, fetch);
  }

  public NSArray titreDetailEcritures(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOTitreDetailEcriture.RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOTitreDetailEcriture.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = titreDetailEcritures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTitreDetailEcrituresRelationship(org.cocktail.maracuja.client.metier.EOTitreDetailEcriture object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TITRE_DETAIL_ECRITURES_KEY);
  }

  public void removeFromTitreDetailEcrituresRelationship(org.cocktail.maracuja.client.metier.EOTitreDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_DETAIL_ECRITURES_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOTitreDetailEcriture createTitreDetailEcrituresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("TitreDetailEcriture");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TITRE_DETAIL_ECRITURES_KEY);
    return (org.cocktail.maracuja.client.metier.EOTitreDetailEcriture) eo;
  }

  public void deleteTitreDetailEcrituresRelationship(org.cocktail.maracuja.client.metier.EOTitreDetailEcriture object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TITRE_DETAIL_ECRITURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTitreDetailEcrituresRelationships() {
    Enumeration objects = titreDetailEcritures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTitreDetailEcrituresRelationship((org.cocktail.maracuja.client.metier.EOTitreDetailEcriture)objects.nextElement());
    }
  }


  public static EORecette createRecette(EOEditingContext editingContext, NSTimestamp recDate
, String recLibelle
, String recMonnaie
, java.math.BigDecimal recMont
, java.math.BigDecimal recMontantDisquette
, java.math.BigDecimal recMontTtc
, java.math.BigDecimal recMonttva
, Integer recOrdre
, String recStat
, String recSuppression
, String recType
, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOGestion gestion, org.cocktail.maracuja.client.metier.EOPlanComptable planComptable, org.cocktail.maracuja.client.metier.EOTitre titre, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EORecette eo = (EORecette) createAndInsertInstance(editingContext, _EORecette.ENTITY_NAME);    
		eo.setRecDate(recDate);
		eo.setRecLibelle(recLibelle);
		eo.setRecMonnaie(recMonnaie);
		eo.setRecMont(recMont);
		eo.setRecMontantDisquette(recMontantDisquette);
		eo.setRecMontTtc(recMontTtc);
		eo.setRecMonttva(recMonttva);
		eo.setRecOrdre(recOrdre);
		eo.setRecStat(recStat);
		eo.setRecSuppression(recSuppression);
		eo.setRecType(recType);
    eo.setExerciceRelationship(exercice);
    eo.setGestionRelationship(gestion);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTitreRelationship(titre);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecette.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecette.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecette creerInstance(EOEditingContext editingContext) {
		  		EORecette object = (EORecette)createAndInsertInstance(editingContext, _EORecette.ENTITY_NAME);
		  		return object;
			}


		
  	  public EORecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecette)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EORecette localInstanceIn(EOEditingContext editingContext, EORecette eo) {
    EORecette localInstance = (eo == null) ? null : (EORecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecette#localInstanceIn a la place.
   */
	public static EORecette localInstanceOf(EOEditingContext editingContext, EORecette eo) {
		return EORecette.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EORecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
