/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOCheque.java
// 
package org.cocktail.maracuja.client.metier;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSValidation;

public class EOCheque extends _EOCheque {

	public static final String tableName = "Cheque";

	public static final String etatValide = "VALIDE";
	public static final String etatAnnule = "ANNULE";
	public static final String etatVise = "VISE";

	public static final EOSortOrdering SORT_CHE_DATE_SAISIE_ASC = EOSortOrdering.sortOrderingWithKey(EOCheque.CHE_DATE_SAISIE_KEY, EOSortOrdering.CompareAscending);

	public static final String problemeEtatChequeAnnule = "IMPOSSIBLE ETAT ANNULE";

	public final String getNomPrenomTireur() {
		return (this.chePrenomTireur() == null ? this.cheNomTireur() : this.cheNomTireur().concat(" ").concat(this.chePrenomTireur()));
	}

	// Rod: utilisation de affecterBordereau dans FactoryCheque
	//    public void setBordereau(org.cocktail.maracuja.client.metier.EOBordereau value) {
	//        super.setBordereau(value);
	//        
	//        // degager ca de la ou bien faire un addtobothsides à la place de super... et appeler cette methode
	//        
	//        // le bordereau ne doit pas etre 
	//        // VISE
	//        if (EOBordereau.BordereauVise.equals(this.bordereau().borEtat()))
	//            throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
	//        
	//        // ANNULE
	//        if (EOBordereau.BordereauAnnule.equals(this.bordereau().borEtat()))
	//            throw new FactoryException(EOBordereau.problemeBordereauAnnule);
	//      
	//        // A VISER
	//        if (EOBordereau.BordereauAViser.equals(this.bordereau().borEtat()))
	//            throw new FactoryException(EOBordereau.problemeBordereauAViser);
	//              
	//// le cheque nedoit pas etre ANNULE
	//        if (this.cheEtat().equals(EOCheque.etatAnnule))
	//            throw new FactoryException(EOCheque.problemeEtatChequeAnnule);
	//
	//    }
	//    

	public EOCheque() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
