/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EOOrgan.java
// 
package org.cocktail.maracuja.client.metier;


import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSValidation;

public class EOOrgan extends _EOOrgan {

    public static final EOSortOrdering SORT_ORG_UNIV_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UNIV_KEY, EOSortOrdering.CompareAscending);
    public static final EOSortOrdering SORT_ORG_ETAB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_ETAB_KEY, EOSortOrdering.CompareAscending);
    public static final EOSortOrdering SORT_ORG_UB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UB_KEY, EOSortOrdering.CompareAscending);
    public static final EOSortOrdering SORT_ORG_CR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_CR_KEY, EOSortOrdering.CompareAscending);
    public static final EOSortOrdering SORT_ORG_SOUSCR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending);

    
    public EOOrgan() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

//	public final String getLongString() {
//		String tmp = orgUnit();
//
//		if (orgComp()!=null )
//			tmp = tmp + " / " + orgComp();
//
//		if (orgLbud()!=null) {
//			tmp = tmp + " / "+orgLbud();
//		}
//
//		if (orgUc()!=null) {
//			tmp = tmp + " / "+orgUc();
//		}
//		return tmp;
//
//	}
    
    public final String getLongString() {
//      String tmp =  orgUniv();
      String tmp =  orgEtab();

//      if (orgEtab()!=null )
//          tmp = tmp + " / " + orgEtab();
      
      if (orgUb()!=null )
          tmp = tmp + " / " + orgUb();

      if (orgCr()!=null) {
          tmp = tmp + " / "+orgCr();
      }
      
      if (orgSouscr()!=null) {
          tmp = tmp + " / "+orgSouscr();
      }

      return tmp;
  }

  
  /**
   * Renvoie selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
   * @param s
   */
  public final String getShortString() {
      final int niv = orgNiveau().intValue();
      String res = null;
      switch (niv) {
      case 0 :  
          res = orgUniv();
          break;

      case 1 :  
          res = orgEtab();
          break;
          
      case 2 :  
          res = orgUb();
          break;
          
      case 3 :  
          res = orgCr();
          break;
          
      case 4 :  
          res = orgSouscr();
          break;
          
      default:
          break;
      }
      return res;
  }    

}
