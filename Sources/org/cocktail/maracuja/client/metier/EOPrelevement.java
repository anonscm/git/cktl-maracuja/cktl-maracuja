/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOPrelevement.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;
import java.util.Date;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.zutil.client.ZDateUtil;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;


public class EOPrelevement extends _EOPrelevement {

	public static final String ETAT_INVALIDE = "INVALIDE";
	public static final String ETAT_ATTENTE = "ATTENTE";
	public static final String ETAT_PRELEVE = "PRELEVE";
	public static final String ETAT_CONFIRME = "CONFIRME";
	public static final String ETAT_REJETE = "REJETE";
	public static final String ETAT_SUPPRIME = "SUPPRIME";

	public static final String PRELEVMONTANTKEY = "prelevMontant";
	public static final String PRELEVEMENTNUMANDTOTAL_KEY = "prelevementNumAndTotal";
	public static final String ECHEANCIER_MONTANT_TOTAL_KEY = "echeancierMontantTotal";
	public static final String CLIENTNOMANDPRENOM_KEY = "clientNomAndPrenom";
	public static final String DATEPRELEVEMENTCORRIGEE_KEY = "datePrelevementCorrigee";

	public static final EOQualifier QUAL_ETAT_ATTENTE = EOQualifier.qualifierWithQualifierFormat(EOPrelevement.PRELEV_ETAT_MARACUJA_KEY + "=%@", new NSArray(new Object[] {
			ETAT_ATTENTE
	}));

	public EOPrelevement() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return numero du prelevement sous la forme (n/total)
	 */
	public String getPrelevementNumAndTotal() {
		return prelevIndex().intValue() + "/" + echeancier().nombrePrelevements().intValue();
	}

	public BigDecimal echeancierMontantTotal() {
		return echeancier().montant();
	}

	/**
	 * @return renvoie le nom et prenom du fournisseur s'il existe, sinon on essaie de recuperer le nom du cient associé à la recette associée à
	 *         l'échéancier, sinon le nom de la personne associee a l'echeancier.
	 */
	public String getClientNomAndPrenom() {
		if (fournisseur() != null) {
			return fournisseur().getNomAndPrenom();
		}
		else if (echeancier().recette() != null) {
			return echeancier().recette().getClient();
		}
		else if (echeancier().personne() != null) {
			return echeancier().personne().getNomAndPrenom();
		}
		return null;

	}

	/**
	 * @return date prélevement + 3 heures
	 */
	public Date getDatePrelevementCorrigee() {
		return ZDateUtil.addDHMS(prelevDatePrelevement(), 0, 3, 0, 0);
	}

	/**
	 * Verifier le nouveau montant (le total des prelevements ne doit pas depasser le montant de l'echeancier )
	 * 
	 * @param bigDecimal
	 * @throws NSValidation.ValidationException
	 */
	public void checkNewMontant(BigDecimal newMontant) throws NSValidation.ValidationException {
		if (newMontant == null) {
			throw new NSValidation.ValidationException("Le montant ne peut être nul");
		}
		if (!isAttente()) {
			throw new NSValidation.ValidationException("L'état de l'échéance est à " + prelevEtatMaracuja() + ", impossible de modifier le montant");
		}
		if (newMontant.doubleValue() <= 0) {
			throw new NSValidation.ValidationException("Le montant ne peut être nul ou négatif");
		}
		BigDecimal montantEcheancier = echeancier().montant();
		BigDecimal montantDejaPreleve = echeancier().montantPreleve();
		BigDecimal montantAPrelever = echeancier().montantAPrelever().add(prelevMontant().negate());
		BigDecimal montantCalcule = montantDejaPreleve.add(montantAPrelever).add(newMontant);

		System.out.println("montantEcheancier=" + montantEcheancier);
		System.out.println("montantDejaPreleve=" + montantDejaPreleve);
		System.out.println("montantAPrelever=" + montantAPrelever);
		System.out.println("montantCalcule=" + montantCalcule);

		if (montantCalcule.compareTo(montantEcheancier) > 0) {
			throw new NSValidation.ValidationException("Le montant total des prélèvements (" + ZConst.FORMAT_DECIMAL.format(montantCalcule) + ") ne peut excéder le montant de l'échéancier (" + ZConst.FORMAT_DECIMAL.format(montantEcheancier) + ")");
		}

	}

	/**
	 * Verifier la date (elle doit etre supérieure à la date du jour)
	 * 
	 * @param newDate
	 * @throws NSValidation.ValidationException
	 */
	public void checkNewDate(Date newDate) throws NSValidation.ValidationException {

		if (newDate == null) {
			throw new NSValidation.ValidationException("La date ne peut être nulle.");
		}
		if (!isAttente()) {
			throw new NSValidation.ValidationException("L'état de l'échéance est à " + prelevEtatMaracuja() + ", impossible de modifier la date");
		}

		if (newDate.before(ZDateUtil.nowAsDate())) {
			throw new NSValidation.ValidationException("La nouvelle date ne peut être antérieure à la date du jour.");
		}

	}

	public boolean isInvalide() {
		return ETAT_INVALIDE.equals(prelevEtatMaracuja());
	}

	public boolean isAttente() {
		return ETAT_ATTENTE.equals(prelevEtatMaracuja());
	}

	public boolean isPreleve() {
		return ETAT_PRELEVE.equals(prelevEtatMaracuja());
	}

	public boolean isConfirme() {
		return ETAT_CONFIRME.equals(prelevEtatMaracuja());
	}

	public boolean isRejete() {
		return ETAT_REJETE.equals(prelevEtatMaracuja());
	}

	public boolean isSupprime() {
		return ETAT_SUPPRIME.equals(prelevEtatMaracuja());
	}

}
