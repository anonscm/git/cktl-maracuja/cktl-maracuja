// _EOVisaAvMission.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVisaAvMission.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVisaAvMission extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VisaAvMission";
	public static final String ENTITY_TABLE_NAME = "maracuja.Visa_av_mission";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vamId";

	public static final String FOURNISSEUR_ADR_NOM_KEY = "fournisseur_adrNom";
	public static final String FOURNISSEUR_ADR_PRENOM_KEY = "fournisseur_adrPrenom";
	public static final String MISSION_MIS_DEBUT_KEY = "mission_misDebut";
	public static final String MISSION_MIS_MOTIF_KEY = "mission_misMotif";
	public static final String MISSION_MIS_NUMERO_KEY = "mission_misNumero";
	public static final String VAM_DATE_DEM_KEY = "vamDateDem";
	public static final String VAM_DATE_VISA_KEY = "vamDateVisa";
	public static final String VAM_MONTANT_KEY = "vamMontant";
	public static final String VAM_MOTIF_REJET_KEY = "vamMotifRejet";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String GES_CODE_KEY = "gesCode";
	public static final String MIS_ORDRE_KEY = "misOrdre";
	public static final String MOD_ORDRE_AVANCE_KEY = "modOrdreAvance";
	public static final String MOD_ORDRE_REGUL_KEY = "modOrdreRegul";
	public static final String ODP_ORDRE_KEY = "odpOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_DEMANDEUR_KEY = "utlOrdreDemandeur";
	public static final String UTL_ORDRE_VALIDEUR_KEY = "utlOrdreValideur";
	public static final String VAM_ID_KEY = "vamId";

//Colonnes dans la base de donnees
	public static final String FOURNISSEUR_ADR_NOM_COLKEY = "$attribute.columnName";
	public static final String FOURNISSEUR_ADR_PRENOM_COLKEY = "$attribute.columnName";
	public static final String MISSION_MIS_DEBUT_COLKEY = "$attribute.columnName";
	public static final String MISSION_MIS_MOTIF_COLKEY = "$attribute.columnName";
	public static final String MISSION_MIS_NUMERO_COLKEY = "$attribute.columnName";
	public static final String VAM_DATE_DEM_COLKEY = "VAM_DATE_DEM";
	public static final String VAM_DATE_VISA_COLKEY = "VAM_DATE_VISA";
	public static final String VAM_MONTANT_COLKEY = "vam_montant";
	public static final String VAM_MOTIF_REJET_COLKEY = "vam_motif_rejet";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "fou_Ordre";
	public static final String GES_CODE_COLKEY = "ges_Code";
	public static final String MIS_ORDRE_COLKEY = "mis_ORDRE";
	public static final String MOD_ORDRE_AVANCE_COLKEY = "MOD_ORDRE_AVANCE";
	public static final String MOD_ORDRE_REGUL_COLKEY = "MOD_ORDRE_regul";
	public static final String ODP_ORDRE_COLKEY = "odp_Ordre";
	public static final String ORG_ID_COLKEY = "ORG_id";
	public static final String RIB_ORDRE_COLKEY = "rib_Ordre";
	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String UTL_ORDRE_DEMANDEUR_COLKEY = "UTL_ORDRE_DEMANDEUR";
	public static final String UTL_ORDRE_VALIDEUR_COLKEY = "UTL_ORDRE_VALIDEUR";
	public static final String VAM_ID_COLKEY = "vam_id";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNISSEUR_KEY = "fournisseur";
	public static final String GESTION_KEY = "gestion";
	public static final String MISSION_KEY = "mission";
	public static final String MODE_PAIEMENT_AVANCE_KEY = "modePaiementAvance";
	public static final String MODE_PAIEMENT_REGUL_KEY = "modePaiementRegul";
	public static final String ORDRE_DE_PAIEMENT_KEY = "ordreDePaiement";
	public static final String ORGAN_KEY = "organ";
	public static final String RIB_KEY = "rib";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_DEMANDEUR_KEY = "utilisateurDemandeur";
	public static final String UTILISATEUR_VALIDEUR_KEY = "utilisateurValideur";



	// Accessors methods
  public String fournisseur_adrNom() {
    return (String) storedValueForKey(FOURNISSEUR_ADR_NOM_KEY);
  }

  public void setFournisseur_adrNom(String value) {
    takeStoredValueForKey(value, FOURNISSEUR_ADR_NOM_KEY);
  }

  public String fournisseur_adrPrenom() {
    return (String) storedValueForKey(FOURNISSEUR_ADR_PRENOM_KEY);
  }

  public void setFournisseur_adrPrenom(String value) {
    takeStoredValueForKey(value, FOURNISSEUR_ADR_PRENOM_KEY);
  }

  public NSTimestamp mission_misDebut() {
    return (NSTimestamp) storedValueForKey(MISSION_MIS_DEBUT_KEY);
  }

  public void setMission_misDebut(NSTimestamp value) {
    takeStoredValueForKey(value, MISSION_MIS_DEBUT_KEY);
  }

  public String mission_misMotif() {
    return (String) storedValueForKey(MISSION_MIS_MOTIF_KEY);
  }

  public void setMission_misMotif(String value) {
    takeStoredValueForKey(value, MISSION_MIS_MOTIF_KEY);
  }

  public Integer mission_misNumero() {
    return (Integer) storedValueForKey(MISSION_MIS_NUMERO_KEY);
  }

  public void setMission_misNumero(Integer value) {
    takeStoredValueForKey(value, MISSION_MIS_NUMERO_KEY);
  }

  public NSTimestamp vamDateDem() {
    return (NSTimestamp) storedValueForKey(VAM_DATE_DEM_KEY);
  }

  public void setVamDateDem(NSTimestamp value) {
    takeStoredValueForKey(value, VAM_DATE_DEM_KEY);
  }

  public NSTimestamp vamDateVisa() {
    return (NSTimestamp) storedValueForKey(VAM_DATE_VISA_KEY);
  }

  public void setVamDateVisa(NSTimestamp value) {
    takeStoredValueForKey(value, VAM_DATE_VISA_KEY);
  }

  public java.math.BigDecimal vamMontant() {
    return (java.math.BigDecimal) storedValueForKey(VAM_MONTANT_KEY);
  }

  public void setVamMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, VAM_MONTANT_KEY);
  }

  public String vamMotifRejet() {
    return (String) storedValueForKey(VAM_MOTIF_REJET_KEY);
  }

  public void setVamMotifRejet(String value) {
    takeStoredValueForKey(value, VAM_MOTIF_REJET_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOExercice exercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOFournisseur fournisseur() {
    return (org.cocktail.maracuja.client.metier.EOFournisseur)storedValueForKey(FOURNISSEUR_KEY);
  }

  public void setFournisseurRelationship(org.cocktail.maracuja.client.metier.EOFournisseur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOFournisseur oldValue = fournisseur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNISSEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNISSEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOGestion gestion() {
    return (org.cocktail.maracuja.client.metier.EOGestion)storedValueForKey(GESTION_KEY);
  }

  public void setGestionRelationship(org.cocktail.maracuja.client.metier.EOGestion value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOGestion oldValue = gestion();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, GESTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, GESTION_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOMission mission() {
    return (org.cocktail.maracuja.client.metier.EOMission)storedValueForKey(MISSION_KEY);
  }

  public void setMissionRelationship(org.cocktail.maracuja.client.metier.EOMission value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOMission oldValue = mission();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MISSION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MISSION_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOModePaiement modePaiementAvance() {
    return (org.cocktail.maracuja.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_AVANCE_KEY);
  }

  public void setModePaiementAvanceRelationship(org.cocktail.maracuja.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOModePaiement oldValue = modePaiementAvance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_AVANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_AVANCE_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOModePaiement modePaiementRegul() {
    return (org.cocktail.maracuja.client.metier.EOModePaiement)storedValueForKey(MODE_PAIEMENT_REGUL_KEY);
  }

  public void setModePaiementRegulRelationship(org.cocktail.maracuja.client.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOModePaiement oldValue = modePaiementRegul();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_PAIEMENT_REGUL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_PAIEMENT_REGUL_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOrdreDePaiement ordreDePaiement() {
    return (org.cocktail.maracuja.client.metier.EOOrdreDePaiement)storedValueForKey(ORDRE_DE_PAIEMENT_KEY);
  }

  public void setOrdreDePaiementRelationship(org.cocktail.maracuja.client.metier.EOOrdreDePaiement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOrdreDePaiement oldValue = ordreDePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORDRE_DE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORDRE_DE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOrgan organ() {
    return (org.cocktail.maracuja.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.maracuja.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EORib rib() {
    return (org.cocktail.maracuja.client.metier.EORib)storedValueForKey(RIB_KEY);
  }

  public void setRibRelationship(org.cocktail.maracuja.client.metier.EORib value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EORib oldValue = rib();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIB_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIB_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.maracuja.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.maracuja.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateurDemandeur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_DEMANDEUR_KEY);
  }

  public void setUtilisateurDemandeurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateurDemandeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_DEMANDEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_DEMANDEUR_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateurValideur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VALIDEUR_KEY);
  }

  public void setUtilisateurValideurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateurValideur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_VALIDEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VALIDEUR_KEY);
    }
  }
  

  public static EOVisaAvMission createVisaAvMission(EOEditingContext editingContext, NSTimestamp mission_misDebut
, String mission_misMotif
, Integer mission_misNumero
, NSTimestamp vamDateDem
, java.math.BigDecimal vamMontant
, org.cocktail.maracuja.client.metier.EOExercice exercice, org.cocktail.maracuja.client.metier.EOFournisseur fournisseur, org.cocktail.maracuja.client.metier.EOGestion gestion, org.cocktail.maracuja.client.metier.EOMission mission, org.cocktail.maracuja.client.metier.EOModePaiement modePaiementAvance, org.cocktail.maracuja.client.metier.EOModePaiement modePaiementRegul, org.cocktail.maracuja.client.metier.EOOrgan organ, org.cocktail.maracuja.client.metier.EOTypeEtat typeEtat, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateurDemandeur) {
    EOVisaAvMission eo = (EOVisaAvMission) createAndInsertInstance(editingContext, _EOVisaAvMission.ENTITY_NAME);    
		eo.setMission_misDebut(mission_misDebut);
		eo.setMission_misMotif(mission_misMotif);
		eo.setMission_misNumero(mission_misNumero);
		eo.setVamDateDem(vamDateDem);
		eo.setVamMontant(vamMontant);
    eo.setExerciceRelationship(exercice);
    eo.setFournisseurRelationship(fournisseur);
    eo.setGestionRelationship(gestion);
    eo.setMissionRelationship(mission);
    eo.setModePaiementAvanceRelationship(modePaiementAvance);
    eo.setModePaiementRegulRelationship(modePaiementRegul);
    eo.setOrganRelationship(organ);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setUtilisateurDemandeurRelationship(utilisateurDemandeur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVisaAvMission.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVisaAvMission.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOVisaAvMission creerInstance(EOEditingContext editingContext) {
		  		EOVisaAvMission object = (EOVisaAvMission)createAndInsertInstance(editingContext, _EOVisaAvMission.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOVisaAvMission localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVisaAvMission)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVisaAvMission localInstanceIn(EOEditingContext editingContext, EOVisaAvMission eo) {
    EOVisaAvMission localInstance = (eo == null) ? null : (EOVisaAvMission)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVisaAvMission#localInstanceIn a la place.
   */
	public static EOVisaAvMission localInstanceOf(EOEditingContext editingContext, EOVisaAvMission eo) {
		return EOVisaAvMission.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVisaAvMission fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVisaAvMission fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVisaAvMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVisaAvMission)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVisaAvMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVisaAvMission fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVisaAvMission eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVisaAvMission)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVisaAvMission fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVisaAvMission eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVisaAvMission ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVisaAvMission fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
