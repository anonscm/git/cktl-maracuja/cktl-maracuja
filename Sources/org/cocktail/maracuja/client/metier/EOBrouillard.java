/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOBrouillard.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOBrouillard extends _EOBrouillard {
	public static final String BRO_ETAT_ATTENTE = "ATTENTE";
	public static final String BRO_ETAT_VISE = "VISE";
	public static final String BRO_ETAT_REJETE = "REJETE";
	public static final String MONTANT_KEY = "montant";

	public static final String MONTANT_CREDITS_KEY = "montantCredits";
	public static final String MONTANT_DEBITS_KEY = "montantDebits";

	public EOBrouillard() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (isRejete()) {
			if (ZStringUtil.isEmpty(broMotifRejet())) {
				throw new NSValidation.ValidationException("Le motif de rejet est obligatoire si l'état du brouillard est à REJETE");
			}

		}
		if (isVise()) {
			if (!ZStringUtil.isEmpty(broMotifRejet())) {
				throw new NSValidation.ValidationException("Le brouillard ne peut pas être accepté si un motif de rejet est indiqué");
			}
		}
		checkCoherence();
		checkCoherenceVsBordereau();

		if (toExercice() == null) {
			throw new NSValidation.ValidationException("L'exercice est obligatoire");
		}
		if (toTypeOperation() == null) {
			throw new NSValidation.ValidationException("Le type d'operation est obligatoire");
		}
		if (toPersonneCreation() == null) {
			throw new NSValidation.ValidationException("Le createur est obligatoire");
		}
		if (toBordereau() == null) {
			throw new NSValidation.ValidationException("Le bordereau est obligatoire");
		}
		if (toTypeJournal() == null) {
			throw new NSValidation.ValidationException("Le journal est obligatoire");
		}
		if (toComptabilite() == null) {
			throw new NSValidation.ValidationException("La comptabilite est obligatoire");
		}

		if (toBrouillardDetails().count() == 0) {
			throw new NSValidation.ValidationException("Le brouillard doit comporter des détails");
		}
		if (getMontantCredits().compareTo(getMontantDebits()) != 0) {
			throw new NSValidation.ValidationException("Le montant des débits doit être égal au montant des crédits");
		}

	}

	public void checkCoherenceVsBordereau() throws NSValidation.ValidationException {
		if (toBordereau() != null) {
			if (toBordereau().isRejete()) {
				if (!BRO_ETAT_REJETE.equals(broEtat())) {
					throw new NSValidation.ValidationException("Si le bordereau est à l'état ANNULE, le brouillard doit être à l'état REJETE.");
				}
			}
			if (toBordereau().isVise()) {
				if (!BRO_ETAT_VISE.equals(broEtat())) {
					throw new NSValidation.ValidationException("Si le bordereau est à l'état VISE, le brouillard doit être à l'état VISE.");
				}
			}

		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void checkCoherence() throws NSValidation.ValidationException {
		//verifier si brouillards details equilibres
		if (toBrouillardDetails().count() == 0) {
			throw new NSValidation.ValidationException("Le brouillard ne contient aucune ligne de détail.");
		}

		BigDecimal totalDebit = getMontantDebits();
		BigDecimal totalCredit = getMontantCredits();

		if (totalDebit.compareTo(totalCredit) != 0) {
			throw new NSValidation.ValidationException("Brouillards non équilibrés (D=" + totalDebit.doubleValue() + " / C=" + totalCredit.doubleValue() + ")");
		}

		for (int i = 0; i < toBrouillardDetails().count(); i++) {
			EOBrouillardDetail array_element = (EOBrouillardDetail) toBrouillardDetails().objectAtIndex(i);
			array_element.checkCoherence();
		}
	}

	public BigDecimal getMontantDebits() {
		return ZEOUtilities.calcSommeOfBigDecimals(getDebits(), EOBrouillardDetail.BROD_DEBIT_KEY);
	}

	public BigDecimal getMontantCredits() {
		return ZEOUtilities.calcSommeOfBigDecimals(getCredits(), EOBrouillardDetail.BROD_CREDIT_KEY);
	}

	public NSArray getDebits() {
		return toBrouillardDetails(EOBrouillardDetail.QUAL_DEBITS);
	}

	public NSArray getCredits() {
		return toBrouillardDetails(EOBrouillardDetail.QUAL_CREDITS);
	}

	public EOEcriture accepter(EOPersonne personne, NSTimestamp dateJourneeComptable, NSArray newPlancos) throws Exception {
		if (!isAttente()) {
			throw new Exception("Le brouillard n'est pas à l'état ATTENTE");
		}
		//mettre à jour les plancoExer pour les brouillards
		for (int i = 0; i < toBrouillardDetails().count(); i++) {
			EOBrouillardDetail brod = (EOBrouillardDetail) toBrouillardDetails().objectAtIndex(i);
			EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, brod.brodPcoNum());
			NSArray res = (EOQualifier.filteredArrayWithQualifier(newPlancos, qual));
			if (res.count() > 0) {
				brod.setPlanComptableExer((EOPlanComptableExer) res.objectAtIndex(0));
			}
		}

		checkCoherence();
		setBroDateVisa(Factory.getNow());
		setToPersonneVisaRelationship(personne);
		EOEcriture ecr = creerEcriture(dateJourneeComptable);
		NSArray ecds = creerEcritureDetails(ecr);
		setBroEtat(BRO_ETAT_VISE);
		return ecr;
	}

	private EOEcriture creerEcriture(NSTimestamp dateJourneeComptable) {
		FactoryProcessJournalEcriture fp = new FactoryProcessJournalEcriture(IConst.WANT_EO_LOGS, dateJourneeComptable);
		EOUtilisateur utilisateur = EOUtilisateur.fetchByKeyValue(editingContext(), EOUtilisateur.PERSONNE_KEY, toPersonneVisa());
		EOEcriture res = fp.creerEcriture(editingContext(), dateJourneeComptable, broLibelle(), new Integer(0), broPostit(), toComptabilite(), toExercice(), toOrigine(), toTypeJournal(), toTypeOperation(), utilisateur);
		return res;
	}

	private NSArray creerEcritureDetails(EOEcriture ecr) throws Exception {
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < toBrouillardDetails().count(); i++) {
			EOBrouillardDetail brd = (EOBrouillardDetail) toBrouillardDetails().objectAtIndex(i);
			res.addObject(brd.creerEcritureDetail(ecr));
		}

		return res.immutableClone();
	}

	public void rejeter(EOPersonne personne) throws Exception {
		if (!isAttente()) {
			throw new Exception("Le brouillard n'est pas à l'état ATTENTE");
		}
		setBroDateVisa(Factory.getNow());
		setToPersonneVisaRelationship(personne);
		setBroEtat(BRO_ETAT_REJETE);
	}

	public boolean isAttente() {
		return BRO_ETAT_ATTENTE.equals(broEtat());
	}

	public boolean isVise() {
		return BRO_ETAT_VISE.equals(broEtat());
	}

	public boolean isRejete() {
		return BRO_ETAT_REJETE.equals(broEtat());
	}

	public BigDecimal getMontant() {
		return getMontantDebits();
	}

	/**
	 * @return un dictionnaire contenant les comptes indiqués dans les brouillards qui n'existent pas ou sont invalides dans le plan comptablde
	 *         l'exercice
	 */
	public NSArray getPlanComptablesNonValides() throws Exception {
		NSMutableArray res = new NSMutableArray();
		NSArray brouillardDetails = toBrouillardDetails();
		for (int i = 0; i < brouillardDetails.count(); i++) {
			EOBrouillardDetail brod = (EOBrouillardDetail) brouillardDetails.objectAtIndex(i);
			//if (!brod.isCompteExisteDansPlancomptableExer() || !brod.getCompteDansPlanComptableExer().isValide()) {
			if (brod.getPlanComptableExer() == null || !brod.getPlanComptableExer().isValide()) {
				NSMutableDictionary pco = new NSMutableDictionary();
				pco.takeValueForKey(brod.brodPcoNum(), EOBrouillardDetail.BROD_PCO_NUM_KEY);
				pco.takeValueForKey(brod.brodPcoLibelle(), EOBrouillardDetail.BROD_PCO_LIBELLE_KEY);
				res.addObject(pco);
			}
		}
		return res.immutableClone();
	}

	public void initPlanComptableExer(EOEditingContext ec) {
		for (int i = 0; i < toBrouillardDetails().count(); i++) {
			EOBrouillardDetail brod = (EOBrouillardDetail) toBrouillardDetails().objectAtIndex(i);
			brod.initPlanComptableExer(ec);
		}
	}

	/**
	 * @return Les écritures générées à partir du brouillard
	 */
	public NSArray getEcrituresGenerees() {
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < toBrouillardDetails().count(); i++) {
			EOBrouillardDetail brd = (EOBrouillardDetail) toBrouillardDetails().objectAtIndex(i);
			EOEcritureDetail ecd = brd.toEcritureDetail();
			if (ecd != null) {
				if (res.indexOfObject(ecd.ecriture()) == NSArray.NotFound) {
					res.addObject(ecd.ecriture());
				}
			}
		}
		return res.immutableClone();
	}

	/**
	 * @param exercice Exercice comptable (sur lequel l'écriture sera passée)
	 * @param libelle Libellé du brouillard (sera utilisé comme libellé de l'écriture)
	 * @param typeOperation Type d'opération
	 * @param personneCreation Le createur du brouillard
	 * @param bordereau Bordereau auquel est rattaché le brouillard
	 * @param postit informations complémentaires
	 * @param comptabilite facultatif
	 * @param typeJournal facultatif mais conseillé (sinon le type de journal "exercice" est affecté par defaut)
	 * @return
	 */
	public static EOBrouillard creerBrouillard(EOEditingContext editingContext, EOExercice exercice, String libelle, EOTypeOperation typeOperation, EOPersonne personneCreation, EOBordereau bordereau, String postit, EOComptabilite comptabilite, EOTypeJournal typeJournal) {
		EOBrouillard brouillard = (EOBrouillard) EOBrouillard.creerInstance(editingContext);

		if (comptabilite == null) {
			NSArray res = EOComptabilite.fetchAll(editingContext);
			if (res.count() == 1) {
				brouillard.setToComptabiliteRelationship((EOComptabilite) res.objectAtIndex(0));
			}
		}
		if (typeJournal == null) {
			NSArray res = EOTypeJournal.fetchAll(editingContext);
			if (res.count() == 1) {
				brouillard.setToTypeJournalRelationship((EOTypeJournal) res.objectAtIndex(0));
			}
		}
		brouillard.setToExerciceRelationship(exercice);
		brouillard.setBroLibelle(libelle);
		brouillard.setToTypeOperationRelationship(typeOperation);
		brouillard.setToPersonneCreationRelationship(personneCreation);
		brouillard.setToBordereauRelationship(bordereau);
		return brouillard;
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setBroNumero(Integer.valueOf(0));
	}
}
