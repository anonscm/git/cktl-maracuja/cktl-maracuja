// _EOCheque.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCheque.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCheque extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Cheque";
	public static final String ENTITY_TABLE_NAME = "maracuja.CHEQUE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cheOrdre";

	public static final String BANQ_AGENCE_KEY = "banqAgence";
	public static final String BANQ_CODE_KEY = "banqCode";
	public static final String BANQ_LIBELLE_KEY = "banqLibelle";
	public static final String CHE_DATE_CHEQUE_KEY = "cheDateCheque";
	public static final String CHE_DATE_SAISIE_KEY = "cheDateSaisie";
	public static final String CHE_ETAT_KEY = "cheEtat";
	public static final String CHE_MONTANT_KEY = "cheMontant";
	public static final String CHE_NOM_TIREUR_KEY = "cheNomTireur";
	public static final String CHE_NUMERO_CHEQUE_KEY = "cheNumeroCheque";
	public static final String CHE_NUMERO_COMPTE_KEY = "cheNumeroCompte";
	public static final String CHE_PRENOM_TIREUR_KEY = "chePrenomTireur";
	public static final String RCPT_CODE_KEY = "rcptCode";

// Attributs non visibles
	public static final String BOR_ID_KEY = "borId";
	public static final String CHE_ORDRE_KEY = "cheOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String BANQ_AGENCE_COLKEY = "banq_agence";
	public static final String BANQ_CODE_COLKEY = "banq_code";
	public static final String BANQ_LIBELLE_COLKEY = "banq_libelle";
	public static final String CHE_DATE_CHEQUE_COLKEY = "che_Date_cheque";
	public static final String CHE_DATE_SAISIE_COLKEY = "che_Date_saisie";
	public static final String CHE_ETAT_COLKEY = "che_etat";
	public static final String CHE_MONTANT_COLKEY = "che_Montant";
	public static final String CHE_NOM_TIREUR_COLKEY = "che_Nom_Tireur";
	public static final String CHE_NUMERO_CHEQUE_COLKEY = "che_numero_cheque";
	public static final String CHE_NUMERO_COMPTE_COLKEY = "che_numero_compte";
	public static final String CHE_PRENOM_TIREUR_COLKEY = "che_preNom_Tireur";
	public static final String RCPT_CODE_COLKEY = "rcpt_code";

	public static final String BOR_ID_COLKEY = "bor_id";
	public static final String CHE_ORDRE_COLKEY = "che_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "utl_ordre";


	// Relationships
	public static final String BORDEREAU_KEY = "bordereau";
	public static final String CHEQUE_BROUILLARDS_KEY = "chequeBrouillards";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String banqAgence() {
    return (String) storedValueForKey(BANQ_AGENCE_KEY);
  }

  public void setBanqAgence(String value) {
    takeStoredValueForKey(value, BANQ_AGENCE_KEY);
  }

  public String banqCode() {
    return (String) storedValueForKey(BANQ_CODE_KEY);
  }

  public void setBanqCode(String value) {
    takeStoredValueForKey(value, BANQ_CODE_KEY);
  }

  public String banqLibelle() {
    return (String) storedValueForKey(BANQ_LIBELLE_KEY);
  }

  public void setBanqLibelle(String value) {
    takeStoredValueForKey(value, BANQ_LIBELLE_KEY);
  }

  public NSTimestamp cheDateCheque() {
    return (NSTimestamp) storedValueForKey(CHE_DATE_CHEQUE_KEY);
  }

  public void setCheDateCheque(NSTimestamp value) {
    takeStoredValueForKey(value, CHE_DATE_CHEQUE_KEY);
  }

  public NSTimestamp cheDateSaisie() {
    return (NSTimestamp) storedValueForKey(CHE_DATE_SAISIE_KEY);
  }

  public void setCheDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, CHE_DATE_SAISIE_KEY);
  }

  public String cheEtat() {
    return (String) storedValueForKey(CHE_ETAT_KEY);
  }

  public void setCheEtat(String value) {
    takeStoredValueForKey(value, CHE_ETAT_KEY);
  }

  public java.math.BigDecimal cheMontant() {
    return (java.math.BigDecimal) storedValueForKey(CHE_MONTANT_KEY);
  }

  public void setCheMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CHE_MONTANT_KEY);
  }

  public String cheNomTireur() {
    return (String) storedValueForKey(CHE_NOM_TIREUR_KEY);
  }

  public void setCheNomTireur(String value) {
    takeStoredValueForKey(value, CHE_NOM_TIREUR_KEY);
  }

  public String cheNumeroCheque() {
    return (String) storedValueForKey(CHE_NUMERO_CHEQUE_KEY);
  }

  public void setCheNumeroCheque(String value) {
    takeStoredValueForKey(value, CHE_NUMERO_CHEQUE_KEY);
  }

  public String cheNumeroCompte() {
    return (String) storedValueForKey(CHE_NUMERO_COMPTE_KEY);
  }

  public void setCheNumeroCompte(String value) {
    takeStoredValueForKey(value, CHE_NUMERO_COMPTE_KEY);
  }

  public String chePrenomTireur() {
    return (String) storedValueForKey(CHE_PRENOM_TIREUR_KEY);
  }

  public void setChePrenomTireur(String value) {
    takeStoredValueForKey(value, CHE_PRENOM_TIREUR_KEY);
  }

  public String rcptCode() {
    return (String) storedValueForKey(RCPT_CODE_KEY);
  }

  public void setRcptCode(String value) {
    takeStoredValueForKey(value, RCPT_CODE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOBordereau bordereau() {
    return (org.cocktail.maracuja.client.metier.EOBordereau)storedValueForKey(BORDEREAU_KEY);
  }

  public void setBordereauRelationship(org.cocktail.maracuja.client.metier.EOBordereau value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOBordereau oldValue = bordereau();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BORDEREAU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BORDEREAU_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.maracuja.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.maracuja.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray chequeBrouillards() {
    return (NSArray)storedValueForKey(CHEQUE_BROUILLARDS_KEY);
  }

  public NSArray chequeBrouillards(EOQualifier qualifier) {
    return chequeBrouillards(qualifier, null, false);
  }

  public NSArray chequeBrouillards(EOQualifier qualifier, boolean fetch) {
    return chequeBrouillards(qualifier, null, fetch);
  }

  public NSArray chequeBrouillards(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOChequeBrouillard.CHEQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOChequeBrouillard.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = chequeBrouillards();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToChequeBrouillardsRelationship(org.cocktail.maracuja.client.metier.EOChequeBrouillard object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CHEQUE_BROUILLARDS_KEY);
  }

  public void removeFromChequeBrouillardsRelationship(org.cocktail.maracuja.client.metier.EOChequeBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CHEQUE_BROUILLARDS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOChequeBrouillard createChequeBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ChequeBrouillard");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CHEQUE_BROUILLARDS_KEY);
    return (org.cocktail.maracuja.client.metier.EOChequeBrouillard) eo;
  }

  public void deleteChequeBrouillardsRelationship(org.cocktail.maracuja.client.metier.EOChequeBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CHEQUE_BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllChequeBrouillardsRelationships() {
    Enumeration objects = chequeBrouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteChequeBrouillardsRelationship((org.cocktail.maracuja.client.metier.EOChequeBrouillard)objects.nextElement());
    }
  }


  public static EOCheque createCheque(EOEditingContext editingContext, NSTimestamp cheDateSaisie
, String cheEtat
, java.math.BigDecimal cheMontant
, String cheNomTireur
, String cheNumeroCheque
, String rcptCode
, org.cocktail.maracuja.client.metier.EOBordereau bordereau, org.cocktail.maracuja.client.metier.EOUtilisateur utilisateur) {
    EOCheque eo = (EOCheque) createAndInsertInstance(editingContext, _EOCheque.ENTITY_NAME);    
		eo.setCheDateSaisie(cheDateSaisie);
		eo.setCheEtat(cheEtat);
		eo.setCheMontant(cheMontant);
		eo.setCheNomTireur(cheNomTireur);
		eo.setCheNumeroCheque(cheNumeroCheque);
		eo.setRcptCode(rcptCode);
    eo.setBordereauRelationship(bordereau);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOCheque.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOCheque.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOCheque creerInstance(EOEditingContext editingContext) {
		  		EOCheque object = (EOCheque)createAndInsertInstance(editingContext, _EOCheque.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOCheque localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCheque)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOCheque localInstanceIn(EOEditingContext editingContext, EOCheque eo) {
    EOCheque localInstance = (eo == null) ? null : (EOCheque)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOCheque#localInstanceIn a la place.
   */
	public static EOCheque localInstanceOf(EOEditingContext editingContext, EOCheque eo) {
		return EOCheque.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOCheque fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOCheque fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCheque eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCheque)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCheque fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCheque fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCheque eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCheque)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOCheque fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCheque eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCheque ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCheque fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
