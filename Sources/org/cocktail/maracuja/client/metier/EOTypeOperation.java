/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOTypeOperation.java
// 
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOTypeOperation extends _EOTypeOperation {
	public static final String TYPE_OPERATION_GENERIQUE = "GENERIQUE";

	public static final String TYPE_OPERATION_LUCRATIVE = "OPERATION LUCRATIVE";

	public static final String TYPE_OPERATION_CONVENTION = "CONVENTION RESSOURCE AFFECTEE";
	public static final String TYPE_OPERATION_PAIEMENT = "PAIEMENT";

	public static final String TYPE_OPERATION_MODIF_PAIEMENT = "MODIF PAIEMENT";
	public static final String TYPE_OPERATION_ODP_VISA = "VISA ORDRE DE PAIEMENT";
	public static final String TYPE_OPERATION_VISA_PAYE = "VISA PAYE";
	public static final String TYPE_OPERATION_VISA_MANDAT = "VISA MANDAT";
	public static final String TYPE_OPERATION_VISA_TITRE = "VISA TITRE";
	public static final String TYPE_OPERATION_VISA_PRESTATION_INTERNE = "VISA PRESTATION INTERNE";
	public static final String TYPE_OPERATION_ECRITURE_SOLDE_6_ET_7 = "ECRITURE SOLDE 6 ET 7";

	public static final String TOP_TYPE_PUBLIC = "PUBLIC";
	public static final String TOP_TYPE_PRIVE = "PRIVE";

	public static final Object TYPE_OPERATION_RECOUVREMENT = "RECOUVREMENT";

	public EOTypeOperation() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
