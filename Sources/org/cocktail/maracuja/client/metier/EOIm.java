/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
// 
package org.cocktail.maracuja.client.metier;

import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.zutil.client.ZDateUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOIm extends _EOIm {

	public static final EOSortOrdering SORT_DEP_FOURNISSEUR_ASC = EOSortOrdering.sortOrderingWithKey(EOIm.DEPENSE_KEY + "." + EODepense.DEP_FOURNISSEUR_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_IM_NUMERO_ASC = EOSortOrdering.sortOrderingWithKey(EOIm.IM_NUMERO_KEY, EOSortOrdering.CompareAscending);

	public EOIm() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		if (depense() != null && (!isEnAttente())) {
			throw new NSValidation.ValidationException("L'intérêt moratoire pour la facture n°" + depense().depNumero() + " n'est pas dans l'état 'en attente', impossible de le supprimer.");
		}
		super.validateForDelete();
	}

	//	public void validateForSave() throws NSValidation.ValidationException {
	//		validateObjectMetier();
	//		validateBeforeTransactionSave();
	//		super.validateForSave();
	//
	//	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		System.out.println(this);

		if (utilisateur() == null) {
			throw new NSValidation.ValidationException("L'utilisateur est obligatoire");
		}
		if (depense() == null) {
			throw new NSValidation.ValidationException("Référence a la depense obligatoire");
		}
		if (imDateDepartDgp() == null) {
			throw new NSValidation.ValidationException("La date de depart DGP est obligatoire");
		}
		if (imDateFinDgpReelle() == null) {
			throw new NSValidation.ValidationException("La date de fin DGP reelle est obligatoire");
		}
		if (imDateFinDgpTheorique() == null) {
			throw new NSValidation.ValidationException("La date de fin DGP theorique est obligatoire");
		}
		if (imDgp() == null) {
			throw new NSValidation.ValidationException("Le DGP est obligatoire");
		}
		if (imDureeSusp() == null) {
			throw new NSValidation.ValidationException("La duree de suspension est obligatoire");
		}
		if (imMontant() == null) {
			throw new NSValidation.ValidationException("Le montant est obligatoire");
		}
		if (imNbJoursDepassement() == null) {
			throw new NSValidation.ValidationException("Le nombre de jours de dépassement est obligatoire");
		}
		if (imNumero() == null) {
			throw new NSValidation.ValidationException("Le numéro est obligatoire");
		}
		if (imTauxApplicable() == null) {
			throw new NSValidation.ValidationException("Le taux applicable est obligatoire");
		}
		if (imTaux() == null) {
			throw new NSValidation.ValidationException("Le taux de référence est obligatoire");
		}

		if (imDateFinDgpReelle().before(imDateDepartDgp())) {
			throw new NSValidation.ValidationException("La date de fin de DGP ne peut pas etre antérieure à la date de début de DGP.");
		}

		if (imDureeSusp().intValue() < 0) {
			throw new NSValidation.ValidationException("La durée de suspension doit être positive.");
		}

		if (imNbJoursDepassement().intValue() < 0) {
			throw new NSValidation.ValidationException("Le nombre de jours de dépassement doit être positif.");
		}

		if (imTauxApplicable().doubleValue() < 0) {
			throw new NSValidation.ValidationException("Le taux doit être positif.");
		}

		if (imMontant().doubleValue() < 0) {
			throw new NSValidation.ValidationException("Le montant doit être positif.");
		}

		setDateModification(ZDateUtil.now());
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public static NSArray fetchAll(EOEditingContext ec, EOQualifier qual, NSArray sortOrderings, boolean refresh) {
		EOFetchSpecification spec = new EOFetchSpecification(ENTITY_NAME, qual, null, true, false, null);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static NSArray fetchAllForDepense(EOEditingContext ec, EODepense depense, NSArray sortOrderings, boolean refresh) {
		EOQualifier qual = new EOKeyValueQualifier(EOIm.DEPENSE_KEY, EOQualifier.QualifierOperatorEqual, depense);
		return fetchAll(ec, qual, sortOrderings, refresh);
	}

	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setDateCreation(ZDateUtil.now());
		setImDureeSusp(new Long(0));
		mettreEnAttente();
	}

	public void rejeter() {
		setTypeEtatRelationship(ZFinderEtats.etat_ANNULE());
	}

	public void valider() {
		setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
	}

	public void mettreEnAttente() {
		setTypeEtatRelationship(ZFinderEtats.etat_EN_ATTENTE());
	}

	/**
	 * Nettoie les proprietes de l'im (a appeler pour une reinitialisation de l'IM).
	 */
	public void cleanIm() {
		if (typeEtat() == null || isEnAttente()) {
			setImCommentaires(null);
			setImDateDepartDgp(null);
			setImDateFinDgpReelle(null);
			setImDgp(null);
			setImDureeSusp(null);
			setImLibelleTaux(null);
			setImMontant(null);
			setImNbJoursDepassement(null);
			setImNumero(null);
			setImTauxApplicable(null);
			setImTauxRelationship(null);
			setImPenalite(null);
		}
	}

	public boolean isEnAttente() {
		return (EOTypeEtat.ETAT_EN_ATTENTE.equals(typeEtat().tyetLibelle()));
	}

	public boolean isValide() {
		return (EOTypeEtat.ETAT_VALIDE.equals(typeEtat().tyetLibelle()));
	}

	public boolean isAnnule() {
		return (EOTypeEtat.ETAT_ANNULE.equals(typeEtat().tyetLibelle()));
	}

	public boolean canReinitialiser() {
		return (isEnAttente() || isAnnule());
	}

	public void setImDgp(Number value) {
		Long res = null;
		if (value != null) {
			res = new Long(value.longValue());
		}
		super.setImDgp(res);
	}

	public void setImNbJoursDepassement(Number value) {
		Long res = null;
		if (value != null) {
			res = new Long(value.longValue());
		}
		super.setImNbJoursDepassement(res);
	}

	public void setImDureeSusp(Number value) {
		Long res = null;
		if (value != null) {
			res = new Long(value.longValue());
		}
		super.setImDureeSusp(res);
	}
}
