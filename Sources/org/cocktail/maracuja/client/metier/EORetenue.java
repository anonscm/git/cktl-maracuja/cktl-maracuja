/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */


// EORetenue.java
// 
package org.cocktail.maracuja.client.metier;


import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

public class EORetenue extends _EORetenue {

    public EORetenue() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }
    
    
    public void setRetMontant(BigDecimal aValue) {
        super.setRetMontant(aValue);
    }
    
    
    
//    
//    private BigDecimal computeSumForKey(
//            NSArray eo,
//            String key) {
//
//        BigDecimal total = new BigDecimal(0);
//        int i = 0;
//        while (i < eo.count()) {
//            total.add((BigDecimal) ((EOEnterpriseObject) eo.objectAtIndex(i)).valueForKey(key));
//            i = i + 1;
//        }
//        return total;
//    }

//
//    public void verifierNouveauMontant(
//            BigDecimal nouveauRetMontant) throws FactoryException {
//        NSArray lesRetenueDetails = null;
//        NSMutableArray lesDetails = null;
//        //float newMontantReste = 0;
//        int i = 0;
//        if (this.retenueDetails() != null) {
//
//            //if (nouveauRetMontant.floatValue() < this.retMontantReste()
//            //        .floatValue())
//                if (Factory.inferieur(nouveauRetMontant,(BigDecimal)retMontantReste()))
//                throw new FactoryException(EORetenue.nouveauMontantInfReste);
//
//            // si le nouveau montant est inf a la somme des RetenuesDetails !!!!
//            lesRetenueDetails = this.retenueDetails();
//            lesDetails = new NSMutableArray();
//            // enlever les details ANNULE
//            i = 0;
//            while (i < lesRetenueDetails.count()) { // si
//                // le
//                // detail
//                // est
//                // diff
//                // d
//                // annule
//                if (!(((EORetenue) lesRetenueDetails.objectAtIndex(i))
//                        .retEtat().equals(EORetenueDetail.etatAnnule)))
//                    lesDetails.addObject(lesRetenueDetails.objectAtIndex(i));
//                i++;
//            }
//            //if (nouveauRetMontant.floatValue() < (this.computeSumForKey(
//            //        lesDetails,
//            //        "redMontant")))
//                if (Factory.inferieur(nouveauRetMontant,this.computeSumForKey(lesDetails, "redMontant")))
//                throw new FactoryException(EORetenue.nouveauMontantInfReste);
//        }
//    }
//
//    public void evaluerMontantReste(
//            BigDecimal nouveauRetMontant) throws FactoryException {
//        NSArray lesRetenueDetails = null;
//        NSMutableArray lesDetails = null;
//        BigDecimal newMontantReste = new BigDecimal(0).setScale(2);
//        int i = 0;
//        if (this.retenueDetails() != null) {
//
//            // nouveau montantReste = newmontant - somme des details TERMINE
//            lesDetails = new NSMutableArray();
//
//            i = 0;
//            while (i < lesRetenueDetails.count()) { // on
//                // recupere
//                // les
//                // details
//                // TERMINE
//                if (((EORetenue) lesRetenueDetails.objectAtIndex(i)).retEtat()
//                        .equals(
//                                EORetenueDetail.etatTermine))
//                    lesDetails.addObject(lesRetenueDetails.objectAtIndex(i));
//                i++;
//            }
//            newMontantReste.add( nouveauRetMontant);
//            newMontantReste.add( this.computeSumForKey(lesDetails,"redMontant").negate());
//            
//        } else
//            newMontantReste = nouveauRetMontant;
//    }


}
