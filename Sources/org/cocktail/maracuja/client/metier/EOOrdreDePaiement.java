/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// EOOrdreDePaiement.java
// 
package org.cocktail.maracuja.client.metier;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOOrdreDePaiement extends _EOOrdreDePaiement {
	public static String etatValide = "VALIDE";

	public static String etatAnnule = "ANNULE";

	public static String etatVise = "VISE";

	public static String etatVirement = "PAYE";

	public static String etatOrdonnateur = "ORDONNATEUR";

	public static String problemeAnnulationEcriture = "IMPOSSIBLE DE SUPPRIMER UN ORDRE DE PAIEMENT AVEC ECRITURE";

	public static String problemeMontantEcriture = "L ECRITURE N AI PAS DU MONTANT DE L ORDRE DE PAIEMENT";

	public static String problemeDejaPaye = "ORDRE DE PAIEMENT DEJA PAYE";

	public static String problemeAnnule = "ORDRE DE PAIEMENT ANNULE";

	public static String problemeViserPasPossible = "L'ordre de paiement a déjà une écriture, ou bien il y a un rib affecté, ou bien il n'y a pas de brouillards";

	public EOOrdreDePaiement() {
		super();
	}

	// en cas de modif du TTC le montantPaiement prend  son montant.
	public void setOdpTtc(BigDecimal value) {
		//        this.setOdpTtc(value);
		//Rod : faut mieux mettre super sinon on a une belle recusivite infinie ;)        
		super.setOdpTtc(value);

		this.setOdpMontantPaiement(value);
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		//verifier qu'on a bien un broullard
		if (this.ordreDePaiementBrouillards() == null || this.ordreDePaiementBrouillards().count() < 2) {
			throw new NSValidation.ValidationException(
					"L'ordre de paiement doit avoir un brouillard associé.");
		}
	}

	/**
	 * renvoie le libelle 1 pour la disquette BDF (32 caractères)
	 * 
	 * @return odpReferencePaiement ou bien odpLibelle
	 */
	public String getLibelleBdf1() {
		String lib = odpReferencePaiement();
		if (lib == null || lib.trim().length() == 0) {
			lib = odpLibelle();
		}

		return lib;
	}

	/**
	 * renvoie le libelle 2 pour la disquette BDF (32 caractères)
	 * 
	 * @return
	 */
	public String getLibelleBdf2() {
		return odpNumero() + " DU " + new SimpleDateFormat("dd/MM/yy").format(odpDateSaisie());
	}

	/**
	 * @param origine
	 * @param sens
	 * @return Le montant des ecritureDetail respectant l'origine et le sens
	 */
	public BigDecimal montantEcrituresParOrigineEtSens(String origine, String sens) {
		BigDecimal res = new BigDecimal(0.00);
		NSArray tmp = getEcritureDetailsParOrigineEtSens(origine, sens);
		res = ZEOUtilities.calcSommeOfBigDecimals(tmp, EOEcritureDetail.ECD_MONTANT_KEY);
		return res;
	}

	/**
	 * @param origine
	 * @param sens
	 * @return tableau d'ecritureDetail respectant l'origine et le sens
	 */
	public NSArray getEcritureDetailsParOrigineEtSens(String origine, String sens) {
		NSMutableArray res = new NSMutableArray();
		final EOQualifier qual = new EOKeyValueQualifier(EOOrdrePaiementDetailEcriture.OPE_ORIGINE_KEY, EOQualifier.QualifierOperatorEqual, origine);
		final NSArray mdes = this.ordrePaiementDetailEcritures(qual, false);
		int j = 0;
		while (j < mdes.count()) {
			EOOrdrePaiementDetailEcriture mde = (EOOrdrePaiementDetailEcriture) mdes.objectAtIndex(j);
			if (sens.equals(mde.ecritureDetail().ecdSens())) {
				res.addObject(mde.ecritureDetail());
			}
			j++;
		}
		return res.immutableClone();
	}

	/**
	 * @param odps les ordres de paiement
	 * @param origine
	 * @param sens
	 * @return Le montant des ecrituresDetail selon origine et sens.
	 */
	public static BigDecimal montantEcrituresParOrigineEtSens(NSArray odps, String origine, String sens) {
		BigDecimal montantEcritures = new BigDecimal(0);
		int i = 0;
		while (i < odps.count()) {
			EOOrdreDePaiement tmp = (EOOrdreDePaiement) odps.objectAtIndex(i);
			montantEcritures = montantEcritures.add(tmp.montantEcrituresParOrigineEtSens(origine, sens));
			i++;
		}
		return montantEcritures;
	}

	/**
	 * @param odps les ordres de paiement
	 * @return Le montant des ecrituresdetail de paiement en crédit (origine virement)
	 */
	public static BigDecimal montantEcrituresCreditsPaiement(NSArray odps) {
		final String origine = EOOrdrePaiementDetailEcriture.ORIGINE_VIREMENT;
		final String sens = EOEcritureDetail.SENS_CREDIT;
		return montantEcrituresParOrigineEtSens(odps, origine, sens);
	}

}
