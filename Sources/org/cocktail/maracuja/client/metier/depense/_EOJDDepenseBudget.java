// _EOJDDepenseBudget.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJDDepenseBudget.java instead.
package org.cocktail.maracuja.client.metier.depense;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJDDepenseBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "JDDepenseBudget";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.DEPENSE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "depId";

	public static final String DEP_HT_SAISIE_KEY = "depHtSaisie";
	public static final String DEP_MONTANT_BUDGETAIRE_KEY = "depMontantBudgetaire";
	public static final String DEP_TTC_SAISIE_KEY = "depTtcSaisie";

// Attributs non visibles
	public static final String DEP_ID_KEY = "depId";
	public static final String DEP_ID_REVERSEMENT_KEY = "depIdReversement";
	public static final String DEP_TVA_SAISIE_KEY = "depTvaSaisie";
	public static final String DPP_ID_KEY = "dppId";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TAP_ID_KEY = "tapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String DEP_HT_SAISIE_COLKEY = "DEP_HT_SAISIE";
	public static final String DEP_MONTANT_BUDGETAIRE_COLKEY = "DEP_MONTANT_BUDGETAIRE";
	public static final String DEP_TTC_SAISIE_COLKEY = "DEP_TTC_SAISIE";

	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String DEP_ID_REVERSEMENT_COLKEY = "DEP_ID_REVERSEMENT";
	public static final String DEP_TVA_SAISIE_COLKEY = "DEP_TVA_SAISIE";
	public static final String DPP_ID_COLKEY = "DPP_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TO_DEPENSE_BUDGET_ORIGINE_REVERSEMENT_KEY = "toDepenseBudgetOrigineReversement";
	public static final String TO_DEPENSE_CTRL_CONVENTIONS_KEY = "toDepenseCtrlConventions";



	// Accessors methods
  public java.math.BigDecimal depHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_HT_SAISIE_KEY);
  }

  public void setDepHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal depMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDepMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal depTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DEP_TTC_SAISIE_KEY);
  }

  public void setDepTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DEP_TTC_SAISIE_KEY);
  }

  public org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget toDepenseBudgetOrigineReversement() {
    return (org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget)storedValueForKey(TO_DEPENSE_BUDGET_ORIGINE_REVERSEMENT_KEY);
  }

  public void setToDepenseBudgetOrigineReversementRelationship(org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget oldValue = toDepenseBudgetOrigineReversement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPENSE_BUDGET_ORIGINE_REVERSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPENSE_BUDGET_ORIGINE_REVERSEMENT_KEY);
    }
  }
  
  public NSArray toDepenseCtrlConventions() {
    return (NSArray)storedValueForKey(TO_DEPENSE_CTRL_CONVENTIONS_KEY);
  }

  public NSArray toDepenseCtrlConventions(EOQualifier qualifier) {
    return toDepenseCtrlConventions(qualifier, null, false);
  }

  public NSArray toDepenseCtrlConventions(EOQualifier qualifier, boolean fetch) {
    return toDepenseCtrlConventions(qualifier, null, fetch);
  }

  public NSArray toDepenseCtrlConventions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention.TO_DEPENSE_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDepenseCtrlConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDepenseCtrlConventionsRelationship(org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_DEPENSE_CTRL_CONVENTIONS_KEY);
  }

  public void removeFromToDepenseCtrlConventionsRelationship(org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DEPENSE_CTRL_CONVENTIONS_KEY);
  }

  public org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention createToDepenseCtrlConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("JDDepenseCtrlConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_DEPENSE_CTRL_CONVENTIONS_KEY);
    return (org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention) eo;
  }

  public void deleteToDepenseCtrlConventionsRelationship(org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_DEPENSE_CTRL_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToDepenseCtrlConventionsRelationships() {
    Enumeration objects = toDepenseCtrlConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDepenseCtrlConventionsRelationship((org.cocktail.maracuja.client.metier.depense.EOJDDepenseCtrlConvention)objects.nextElement());
    }
  }


  public static EOJDDepenseBudget createJDDepenseBudget(EOEditingContext editingContext, java.math.BigDecimal depHtSaisie
, java.math.BigDecimal depMontantBudgetaire
, java.math.BigDecimal depTtcSaisie
) {
    EOJDDepenseBudget eo = (EOJDDepenseBudget) createAndInsertInstance(editingContext, _EOJDDepenseBudget.ENTITY_NAME);    
		eo.setDepHtSaisie(depHtSaisie);
		eo.setDepMontantBudgetaire(depMontantBudgetaire);
		eo.setDepTtcSaisie(depTtcSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOJDDepenseBudget.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOJDDepenseBudget.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOJDDepenseBudget creerInstance(EOEditingContext editingContext) {
		  		EOJDDepenseBudget object = (EOJDDepenseBudget)createAndInsertInstance(editingContext, _EOJDDepenseBudget.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOJDDepenseBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJDDepenseBudget)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOJDDepenseBudget localInstanceIn(EOEditingContext editingContext, EOJDDepenseBudget eo) {
    EOJDDepenseBudget localInstance = (eo == null) ? null : (EOJDDepenseBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOJDDepenseBudget#localInstanceIn a la place.
   */
	public static EOJDDepenseBudget localInstanceOf(EOEditingContext editingContext, EOJDDepenseBudget eo) {
		return EOJDDepenseBudget.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOJDDepenseBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOJDDepenseBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJDDepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJDDepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJDDepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJDDepenseBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJDDepenseBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJDDepenseBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOJDDepenseBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJDDepenseBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJDDepenseBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJDDepenseBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
