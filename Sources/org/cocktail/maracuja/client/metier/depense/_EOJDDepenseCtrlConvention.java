// _EOJDDepenseCtrlConvention.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOJDDepenseCtrlConvention.java instead.
package org.cocktail.maracuja.client.metier.depense;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOJDDepenseCtrlConvention extends  EOGenericRecord {
	public static final String ENTITY_NAME = "JDDepenseCtrlConvention";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.DEPENSE_CTRL_CONVENTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "dconId";

	public static final String DCON_HT_SAISIE_KEY = "dconHtSaisie";
	public static final String DCON_MONTANT_BUDGETAIRE_KEY = "dconMontantBudgetaire";
	public static final String DCON_TTC_SAISIE_KEY = "dconTtcSaisie";
	public static final String DCON_TVA_SAISIE_KEY = "dconTvaSaisie";

// Attributs non visibles
	public static final String CONV_ORDRE_KEY = "convOrdre";
	public static final String DCON_ID_KEY = "dconId";
	public static final String DEP_ID_KEY = "depId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String DCON_HT_SAISIE_COLKEY = "DCON_HT_SAISIE";
	public static final String DCON_MONTANT_BUDGETAIRE_COLKEY = "DCON_MONTANT_BUDGETAIRE";
	public static final String DCON_TTC_SAISIE_COLKEY = "DCON_TTC_SAISIE";
	public static final String DCON_TVA_SAISIE_COLKEY = "DCON_TVA_SAISIE";

	public static final String CONV_ORDRE_COLKEY = "CONV_ORDRE";
	public static final String DCON_ID_COLKEY = "DCON_ID";
	public static final String DEP_ID_COLKEY = "DEP_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String TO_ACCORDS_CONTRAT_KEY = "toAccordsContrat";
	public static final String TO_DEPENSE_BUDGET_KEY = "toDepenseBudget";



	// Accessors methods
  public java.math.BigDecimal dconHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DCON_HT_SAISIE_KEY);
  }

  public void setDconHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DCON_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal dconMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(DCON_MONTANT_BUDGETAIRE_KEY);
  }

  public void setDconMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DCON_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal dconTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DCON_TTC_SAISIE_KEY);
  }

  public void setDconTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DCON_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal dconTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(DCON_TVA_SAISIE_KEY);
  }

  public void setDconTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, DCON_TVA_SAISIE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOAccordsContrat toAccordsContrat() {
    return (org.cocktail.maracuja.client.metier.EOAccordsContrat)storedValueForKey(TO_ACCORDS_CONTRAT_KEY);
  }

  public void setToAccordsContratRelationship(org.cocktail.maracuja.client.metier.EOAccordsContrat value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOAccordsContrat oldValue = toAccordsContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACCORDS_CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACCORDS_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget toDepenseBudget() {
    return (org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget)storedValueForKey(TO_DEPENSE_BUDGET_KEY);
  }

  public void setToDepenseBudgetRelationship(org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget oldValue = toDepenseBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPENSE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPENSE_BUDGET_KEY);
    }
  }
  

  public static EOJDDepenseCtrlConvention createJDDepenseCtrlConvention(EOEditingContext editingContext, java.math.BigDecimal dconHtSaisie
, java.math.BigDecimal dconMontantBudgetaire
, java.math.BigDecimal dconTtcSaisie
, java.math.BigDecimal dconTvaSaisie
, org.cocktail.maracuja.client.metier.EOAccordsContrat toAccordsContrat, org.cocktail.maracuja.client.metier.depense.EOJDDepenseBudget toDepenseBudget) {
    EOJDDepenseCtrlConvention eo = (EOJDDepenseCtrlConvention) createAndInsertInstance(editingContext, _EOJDDepenseCtrlConvention.ENTITY_NAME);    
		eo.setDconHtSaisie(dconHtSaisie);
		eo.setDconMontantBudgetaire(dconMontantBudgetaire);
		eo.setDconTtcSaisie(dconTtcSaisie);
		eo.setDconTvaSaisie(dconTvaSaisie);
    eo.setToAccordsContratRelationship(toAccordsContrat);
    eo.setToDepenseBudgetRelationship(toDepenseBudget);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOJDDepenseCtrlConvention.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOJDDepenseCtrlConvention.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOJDDepenseCtrlConvention creerInstance(EOEditingContext editingContext) {
		  		EOJDDepenseCtrlConvention object = (EOJDDepenseCtrlConvention)createAndInsertInstance(editingContext, _EOJDDepenseCtrlConvention.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOJDDepenseCtrlConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOJDDepenseCtrlConvention)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOJDDepenseCtrlConvention localInstanceIn(EOEditingContext editingContext, EOJDDepenseCtrlConvention eo) {
    EOJDDepenseCtrlConvention localInstance = (eo == null) ? null : (EOJDDepenseCtrlConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOJDDepenseCtrlConvention#localInstanceIn a la place.
   */
	public static EOJDDepenseCtrlConvention localInstanceOf(EOEditingContext editingContext, EOJDDepenseCtrlConvention eo) {
		return EOJDDepenseCtrlConvention.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOJDDepenseCtrlConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOJDDepenseCtrlConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOJDDepenseCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOJDDepenseCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOJDDepenseCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOJDDepenseCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOJDDepenseCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOJDDepenseCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOJDDepenseCtrlConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOJDDepenseCtrlConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOJDDepenseCtrlConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOJDDepenseCtrlConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
