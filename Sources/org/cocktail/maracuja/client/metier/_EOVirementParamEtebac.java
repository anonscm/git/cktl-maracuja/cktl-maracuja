// _EOVirementParamEtebac.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVirementParamEtebac.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOVirementParamEtebac extends  EOGenericRecord {
	public static final String ENTITY_NAME = "VirementParamEtebac";
	public static final String ENTITY_TABLE_NAME = "maracuja.Virement_Param_Etebac";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "vpeOrdre";

	public static final String VPE_DA_KEY = "vpeDA";
	public static final String VPE_D_B1_KEY = "vpeDB1";
	public static final String VPE_D_B3_KEY = "vpeDB3";
	public static final String VPE_EA_KEY = "vpeEA";
	public static final String VPE_E_B1_KEY = "vpeEB1";
	public static final String VPE_E_B3_KEY = "vpeEB3";
	public static final String VPE_E_C2_KEY = "vpeEC2";
	public static final String VPE_E_D21_KEY = "vpeED21";
	public static final String VPE_E_D3_KEY = "vpeED3";
	public static final String VPE_E_D4_KEY = "vpeED4";
	public static final String VPE_E_G1_KEY = "vpeEG1";
	public static final String VPE_ETAT_KEY = "vpeEtat";
	public static final String VPE_TA_KEY = "vpeTA";
	public static final String VPE_T_B1_KEY = "vpeTB1";
	public static final String VPE_T_B2_KEY = "vpeTB2";
	public static final String VPE_T_B3_KEY = "vpeTB3";

// Attributs non visibles
	public static final String TVI_ORDRE_KEY = "tviOrdre";
	public static final String VPE_ORDRE_KEY = "vpeOrdre";

//Colonnes dans la base de donnees
	public static final String VPE_DA_COLKEY = "VPE_DA";
	public static final String VPE_D_B1_COLKEY = "VPE_DB1";
	public static final String VPE_D_B3_COLKEY = "VPE_DB3";
	public static final String VPE_EA_COLKEY = "VPE_EA";
	public static final String VPE_E_B1_COLKEY = "VPE_EB1";
	public static final String VPE_E_B3_COLKEY = "VPE_EB3";
	public static final String VPE_E_C2_COLKEY = "VPE_EC2";
	public static final String VPE_E_D21_COLKEY = "VPE_ED21";
	public static final String VPE_E_D3_COLKEY = "VPE_ED3";
	public static final String VPE_E_D4_COLKEY = "VPE_ED4";
	public static final String VPE_E_G1_COLKEY = "VPE_EG1";
	public static final String VPE_ETAT_COLKEY = "vpe_Etat";
	public static final String VPE_TA_COLKEY = "VPE_TA";
	public static final String VPE_T_B1_COLKEY = "VPE_TB1";
	public static final String VPE_T_B2_COLKEY = "VPE_TB2";
	public static final String VPE_T_B3_COLKEY = "VPE_TB3";

	public static final String TVI_ORDRE_COLKEY = "tvi_Ordre";
	public static final String VPE_ORDRE_COLKEY = "vpe_Ordre";


	// Relationships
	public static final String TYPEVIREMENT_KEY = "typevirement";



	// Accessors methods
  public String vpeDA() {
    return (String) storedValueForKey(VPE_DA_KEY);
  }

  public void setVpeDA(String value) {
    takeStoredValueForKey(value, VPE_DA_KEY);
  }

  public String vpeDB1() {
    return (String) storedValueForKey(VPE_D_B1_KEY);
  }

  public void setVpeDB1(String value) {
    takeStoredValueForKey(value, VPE_D_B1_KEY);
  }

  public String vpeDB3() {
    return (String) storedValueForKey(VPE_D_B3_KEY);
  }

  public void setVpeDB3(String value) {
    takeStoredValueForKey(value, VPE_D_B3_KEY);
  }

  public String vpeEA() {
    return (String) storedValueForKey(VPE_EA_KEY);
  }

  public void setVpeEA(String value) {
    takeStoredValueForKey(value, VPE_EA_KEY);
  }

  public String vpeEB1() {
    return (String) storedValueForKey(VPE_E_B1_KEY);
  }

  public void setVpeEB1(String value) {
    takeStoredValueForKey(value, VPE_E_B1_KEY);
  }

  public String vpeEB3() {
    return (String) storedValueForKey(VPE_E_B3_KEY);
  }

  public void setVpeEB3(String value) {
    takeStoredValueForKey(value, VPE_E_B3_KEY);
  }

  public String vpeEC2() {
    return (String) storedValueForKey(VPE_E_C2_KEY);
  }

  public void setVpeEC2(String value) {
    takeStoredValueForKey(value, VPE_E_C2_KEY);
  }

  public String vpeED21() {
    return (String) storedValueForKey(VPE_E_D21_KEY);
  }

  public void setVpeED21(String value) {
    takeStoredValueForKey(value, VPE_E_D21_KEY);
  }

  public String vpeED3() {
    return (String) storedValueForKey(VPE_E_D3_KEY);
  }

  public void setVpeED3(String value) {
    takeStoredValueForKey(value, VPE_E_D3_KEY);
  }

  public String vpeED4() {
    return (String) storedValueForKey(VPE_E_D4_KEY);
  }

  public void setVpeED4(String value) {
    takeStoredValueForKey(value, VPE_E_D4_KEY);
  }

  public String vpeEG1() {
    return (String) storedValueForKey(VPE_E_G1_KEY);
  }

  public void setVpeEG1(String value) {
    takeStoredValueForKey(value, VPE_E_G1_KEY);
  }

  public String vpeEtat() {
    return (String) storedValueForKey(VPE_ETAT_KEY);
  }

  public void setVpeEtat(String value) {
    takeStoredValueForKey(value, VPE_ETAT_KEY);
  }

  public String vpeTA() {
    return (String) storedValueForKey(VPE_TA_KEY);
  }

  public void setVpeTA(String value) {
    takeStoredValueForKey(value, VPE_TA_KEY);
  }

  public String vpeTB1() {
    return (String) storedValueForKey(VPE_T_B1_KEY);
  }

  public void setVpeTB1(String value) {
    takeStoredValueForKey(value, VPE_T_B1_KEY);
  }

  public String vpeTB2() {
    return (String) storedValueForKey(VPE_T_B2_KEY);
  }

  public void setVpeTB2(String value) {
    takeStoredValueForKey(value, VPE_T_B2_KEY);
  }

  public String vpeTB3() {
    return (String) storedValueForKey(VPE_T_B3_KEY);
  }

  public void setVpeTB3(String value) {
    takeStoredValueForKey(value, VPE_T_B3_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOTypeVirement typevirement() {
    return (org.cocktail.maracuja.client.metier.EOTypeVirement)storedValueForKey(TYPEVIREMENT_KEY);
  }

  public void setTypevirementRelationship(org.cocktail.maracuja.client.metier.EOTypeVirement value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOTypeVirement oldValue = typevirement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPEVIREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPEVIREMENT_KEY);
    }
  }
  

  public static EOVirementParamEtebac createVirementParamEtebac(EOEditingContext editingContext, org.cocktail.maracuja.client.metier.EOTypeVirement typevirement) {
    EOVirementParamEtebac eo = (EOVirementParamEtebac) createAndInsertInstance(editingContext, _EOVirementParamEtebac.ENTITY_NAME);    
    eo.setTypevirementRelationship(typevirement);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOVirementParamEtebac.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOVirementParamEtebac.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOVirementParamEtebac creerInstance(EOEditingContext editingContext) {
		  		EOVirementParamEtebac object = (EOVirementParamEtebac)createAndInsertInstance(editingContext, _EOVirementParamEtebac.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOVirementParamEtebac localInstanceIn(EOEditingContext editingContext) {
	  		return (EOVirementParamEtebac)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOVirementParamEtebac localInstanceIn(EOEditingContext editingContext, EOVirementParamEtebac eo) {
    EOVirementParamEtebac localInstance = (eo == null) ? null : (EOVirementParamEtebac)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOVirementParamEtebac#localInstanceIn a la place.
   */
	public static EOVirementParamEtebac localInstanceOf(EOEditingContext editingContext, EOVirementParamEtebac eo) {
		return EOVirementParamEtebac.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOVirementParamEtebac fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOVirementParamEtebac fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOVirementParamEtebac eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOVirementParamEtebac)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOVirementParamEtebac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOVirementParamEtebac fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOVirementParamEtebac eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOVirementParamEtebac)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOVirementParamEtebac fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOVirementParamEtebac eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOVirementParamEtebac ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOVirementParamEtebac fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
