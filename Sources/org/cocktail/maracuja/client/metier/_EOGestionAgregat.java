// _EOGestionAgregat.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOGestionAgregat.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOGestionAgregat extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GestionAgregat";
	public static final String ENTITY_TABLE_NAME = "maracuja.gestion_agregat";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "gaId";

	public static final String GA_DESCRIPTION_KEY = "gaDescription";
	public static final String GA_LIBELLE_KEY = "gaLibelle";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String GA_ID_KEY = "gaId";

//Colonnes dans la base de donnees
	public static final String GA_DESCRIPTION_COLKEY = "ga_description";
	public static final String GA_LIBELLE_COLKEY = "ga_libelle";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String GA_ID_COLKEY = "ga_id";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final String TO_GESTION_AGREGAT_REPARTS_KEY = "toGestionAgregatReparts";



	// Accessors methods
  public String gaDescription() {
    return (String) storedValueForKey(GA_DESCRIPTION_KEY);
  }

  public void setGaDescription(String value) {
    takeStoredValueForKey(value, GA_DESCRIPTION_KEY);
  }

  public String gaLibelle() {
    return (String) storedValueForKey(GA_LIBELLE_KEY);
  }

  public void setGaLibelle(String value) {
    takeStoredValueForKey(value, GA_LIBELLE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOExercice toExercice() {
    return (org.cocktail.maracuja.client.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.maracuja.client.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }
  
  public NSArray toGestionAgregatReparts() {
    return (NSArray)storedValueForKey(TO_GESTION_AGREGAT_REPARTS_KEY);
  }

  public NSArray toGestionAgregatReparts(EOQualifier qualifier) {
    return toGestionAgregatReparts(qualifier, null, false);
  }

  public NSArray toGestionAgregatReparts(EOQualifier qualifier, boolean fetch) {
    return toGestionAgregatReparts(qualifier, null, fetch);
  }

  public NSArray toGestionAgregatReparts(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.maracuja.client.metier.EOGestionAgregatRepart.TO_GESTION_AGREGAT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.maracuja.client.metier.EOGestionAgregatRepart.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toGestionAgregatReparts();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToGestionAgregatRepartsRelationship(org.cocktail.maracuja.client.metier.EOGestionAgregatRepart object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_GESTION_AGREGAT_REPARTS_KEY);
  }

  public void removeFromToGestionAgregatRepartsRelationship(org.cocktail.maracuja.client.metier.EOGestionAgregatRepart object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GESTION_AGREGAT_REPARTS_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOGestionAgregatRepart createToGestionAgregatRepartsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("GestionAgregatRepart");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_GESTION_AGREGAT_REPARTS_KEY);
    return (org.cocktail.maracuja.client.metier.EOGestionAgregatRepart) eo;
  }

  public void deleteToGestionAgregatRepartsRelationship(org.cocktail.maracuja.client.metier.EOGestionAgregatRepart object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_GESTION_AGREGAT_REPARTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToGestionAgregatRepartsRelationships() {
    Enumeration objects = toGestionAgregatReparts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToGestionAgregatRepartsRelationship((org.cocktail.maracuja.client.metier.EOGestionAgregatRepart)objects.nextElement());
    }
  }


  public static EOGestionAgregat createGestionAgregat(EOEditingContext editingContext, String gaLibelle
, org.cocktail.maracuja.client.metier.EOExercice toExercice) {
    EOGestionAgregat eo = (EOGestionAgregat) createAndInsertInstance(editingContext, _EOGestionAgregat.ENTITY_NAME);    
		eo.setGaLibelle(gaLibelle);
    eo.setToExerciceRelationship(toExercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOGestionAgregat.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOGestionAgregat.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOGestionAgregat creerInstance(EOEditingContext editingContext) {
		  		EOGestionAgregat object = (EOGestionAgregat)createAndInsertInstance(editingContext, _EOGestionAgregat.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOGestionAgregat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOGestionAgregat)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOGestionAgregat localInstanceIn(EOEditingContext editingContext, EOGestionAgregat eo) {
    EOGestionAgregat localInstance = (eo == null) ? null : (EOGestionAgregat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOGestionAgregat#localInstanceIn a la place.
   */
	public static EOGestionAgregat localInstanceOf(EOEditingContext editingContext, EOGestionAgregat eo) {
		return EOGestionAgregat.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOGestionAgregat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOGestionAgregat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOGestionAgregat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOGestionAgregat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOGestionAgregat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOGestionAgregat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOGestionAgregat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOGestionAgregat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOGestionAgregat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOGestionAgregat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOGestionAgregat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOGestionAgregat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
