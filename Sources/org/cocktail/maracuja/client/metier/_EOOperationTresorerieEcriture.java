// _EOOperationTresorerieEcriture.java
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOperationTresorerieEcriture.java instead.
package org.cocktail.maracuja.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOperationTresorerieEcriture extends  EOGenericRecord {
	public static final String ENTITY_NAME = "OperationTresorerieEcriture";
	public static final String ENTITY_TABLE_NAME = "maracuja.Ope_Tresorerie_Ecriture";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "oteOrdre";

	public static final String OTE_DATE_KEY = "oteDate";
	public static final String OTE_ORIGINE_KEY = "oteOrigine";

// Attributs non visibles
	public static final String ECD_ORDRE_KEY = "ecdOrdre";
	public static final String OPT_ORDRE_KEY = "optOrdre";
	public static final String OTE_ORDRE_KEY = "oteOrdre";

//Colonnes dans la base de donnees
	public static final String OTE_DATE_COLKEY = "ote_date";
	public static final String OTE_ORIGINE_COLKEY = "ote_Origine";

	public static final String ECD_ORDRE_COLKEY = "ecd_ORDRE";
	public static final String OPT_ORDRE_COLKEY = "opt_Ordre";
	public static final String OTE_ORDRE_COLKEY = "ote_ordre";


	// Relationships
	public static final String ECRITURE_DETAIL_KEY = "ecritureDetail";
	public static final String OPERATION_TRESORERIE_KEY = "operationTresorerie";



	// Accessors methods
  public NSTimestamp oteDate() {
    return (NSTimestamp) storedValueForKey(OTE_DATE_KEY);
  }

  public void setOteDate(NSTimestamp value) {
    takeStoredValueForKey(value, OTE_DATE_KEY);
  }

  public String oteOrigine() {
    return (String) storedValueForKey(OTE_ORIGINE_KEY);
  }

  public void setOteOrigine(String value) {
    takeStoredValueForKey(value, OTE_ORIGINE_KEY);
  }

  public org.cocktail.maracuja.client.metier.EOEcritureDetail ecritureDetail() {
    return (org.cocktail.maracuja.client.metier.EOEcritureDetail)storedValueForKey(ECRITURE_DETAIL_KEY);
  }

  public void setEcritureDetailRelationship(org.cocktail.maracuja.client.metier.EOEcritureDetail value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOEcritureDetail oldValue = ecritureDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ECRITURE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ECRITURE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.maracuja.client.metier.EOOperationTresorerie operationTresorerie() {
    return (org.cocktail.maracuja.client.metier.EOOperationTresorerie)storedValueForKey(OPERATION_TRESORERIE_KEY);
  }

  public void setOperationTresorerieRelationship(org.cocktail.maracuja.client.metier.EOOperationTresorerie value) {
    if (value == null) {
    	org.cocktail.maracuja.client.metier.EOOperationTresorerie oldValue = operationTresorerie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, OPERATION_TRESORERIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, OPERATION_TRESORERIE_KEY);
    }
  }
  

  public static EOOperationTresorerieEcriture createOperationTresorerieEcriture(EOEditingContext editingContext, NSTimestamp oteDate
, String oteOrigine
, org.cocktail.maracuja.client.metier.EOEcritureDetail ecritureDetail, org.cocktail.maracuja.client.metier.EOOperationTresorerie operationTresorerie) {
    EOOperationTresorerieEcriture eo = (EOOperationTresorerieEcriture) createAndInsertInstance(editingContext, _EOOperationTresorerieEcriture.ENTITY_NAME);    
		eo.setOteDate(oteDate);
		eo.setOteOrigine(oteOrigine);
    eo.setEcritureDetailRelationship(ecritureDetail);
    eo.setOperationTresorerieRelationship(operationTresorerie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOOperationTresorerieEcriture.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOOperationTresorerieEcriture.fetch(editingContext, null, sortOrderings);
//  }

  
	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 * 
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOOperationTresorerieEcriture creerInstance(EOEditingContext editingContext) {
		  		EOOperationTresorerieEcriture object = (EOOperationTresorerieEcriture)createAndInsertInstance(editingContext, _EOOperationTresorerieEcriture.ENTITY_NAME);
		  		return object;
			}


		
  	  public EOOperationTresorerieEcriture localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOperationTresorerieEcriture)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOOperationTresorerieEcriture localInstanceIn(EOEditingContext editingContext, EOOperationTresorerieEcriture eo) {
    EOOperationTresorerieEcriture localInstance = (eo == null) ? null : (EOOperationTresorerieEcriture)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOOperationTresorerieEcriture#localInstanceIn a la place.
   */
	public static EOOperationTresorerieEcriture localInstanceOf(EOEditingContext editingContext, EOOperationTresorerieEcriture eo) {
		return EOOperationTresorerieEcriture.localInstanceIn(editingContext, eo);
	}
  
	


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOOperationTresorerieEcriture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOOperationTresorerieEcriture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOperationTresorerieEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOperationTresorerieEcriture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOperationTresorerieEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOperationTresorerieEcriture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOperationTresorerieEcriture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOperationTresorerieEcriture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOOperationTresorerieEcriture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOperationTresorerieEcriture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOperationTresorerieEcriture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOperationTresorerieEcriture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
