/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.helpers.TypeRecouvrementHelper;
import org.cocktail.fwkcktlcompta.common.util.CktlConfigUtil;
import org.cocktail.maracuja.client.EOConvertUtil;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPrelevementFichiersBdf;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOPrelevementParamBdf;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ctrl.RecoSepaSddCtrl;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ctrl.RecoSepaSddReleveCtrl;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.joda.time.LocalDate;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementSrchCtrl extends CommonCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	private static final String TITLE = "Recouvrements ";

	private RecouvrementSrchPanel myPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionNewRecouvrementBdf actionNewRecouvrementBdf = new ActionNewRecouvrementBdf();
	private final ActionNewRecouvrementSepaSdd actionNewRecouvrementSepaSdd = new ActionNewRecouvrementSepaSdd();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionRegenererVirement actionRegenererVirement = new ActionRegenererVirement();
	private final ActionImprimerContenuRecouvrement actionImprimerContenuRecouvrement = new ActionImprimerContenuRecouvrement();
	private final ActionImprimerBordereau actionImprimerBordereau = new ActionImprimerBordereau();
	private final ActionEnregistrer actionEnregistrer = new ActionEnregistrer();
	private final ActionEmargerReleve actionEmargerReleve = new ActionEmargerReleve();
	//    private final ActionVoirFichier actionVoirFichier = new ActionVoirFichier();

	private ZKarukeraDialog win;
	private HashMap filters;
	private Boolean prelevementNationalEnabled = Boolean.FALSE;
	private Boolean prelevementSepaEnabled = Boolean.FALSE;

	private RecouvrementSrchPanelListener recouvrementSrchPanelListener;

	/**
	 * @param editingContext
	 */
	public RecouvrementSrchCtrl(EOEditingContext editingContext) {
		super(editingContext);
		revertChanges();
		prelevementNationalEnabled = CktlConfigUtil.getBooleanValueForConfig((String) ServerCallCompta.clientSideRequestGetConfig(getEditingContext(), IFwkCktlComptaParam.PRELEVEMENTNATIONAL_ENABLED));
		prelevementSepaEnabled = CktlConfigUtil.getBooleanValueForConfig((String) ServerCallCompta.clientSideRequestGetConfig(getEditingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_ENABLED));

		filters = new HashMap();
		recouvrementSrchPanelListener = new RecouvrementSrchPanelListener();
		initSubObjects();

	}

	public void initSubObjects() {
		myPanel = new RecouvrementSrchPanel(recouvrementSrchPanelListener);
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice()
		})));

		//Construire les qualifiers à partir des saisies utilisateur

		NSMutableArray qualsOrdres = new NSMutableArray();
		if (dicoFiltre.get("recoOrdreMin") != null) {
			qualsOrdres.addObject(EOQualifier.qualifierWithQualifierFormat("recoOrdre>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("recoOrdreMin")).intValue()))));
		}
		if (dicoFiltre.get("recoOrdreMax") != null) {
			qualsOrdres.addObject(EOQualifier.qualifierWithQualifierFormat("recoOrdre<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("recoOrdreMax")).intValue()))));
		}
		if (qualsOrdres.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsOrdres));
		}

		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("recoNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("recoNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("recoNumeroMin")).intValue()))));
		}
		if (dicoFiltre.get("recoNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("recoNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("recoNumeroMax")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		///Montant
		NSMutableArray qualsMontant = new NSMutableArray();
		if (dicoFiltre.get("recoMontantMin") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("recoMontant>=%@", new NSArray((Number) dicoFiltre.get("recoMontantMin"))));
		}
		if (dicoFiltre.get("recoMontantMax") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("recoMontant<=%@", new NSArray((Number) dicoFiltre.get("recoMontantMax"))));
		}
		if (qualsMontant.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMontant));
		}

		///Date
		NSMutableArray qualsDate = new NSMutableArray();
		if (dicoFiltre.get("recoDateCreationMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("recoDateCreation>=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("recoDateCreationMin")))));
		}
		if (dicoFiltre.get("recoDateCreationMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("recoDateCreation<=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("recoDateCreationMax")))));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		return quals;
	}

	/**
	 * Vérifie si la date du jour correspond à l'exercice selectionné.
	 *
	 * @throws Exception
	 */
	private final void checkExerciceTermine() throws Exception {
		final int annee = ZDateUtil.getYear(ZDateUtil.nowAsDate());
		final int anneeSelectionnee = myApp.appUserInfo().getCurrentExercice().exeExercice().intValue();
		if (anneeSelectionnee < annee) {
			throw new DefaultClientException("Vous ne pouvez plus effectuer de recouvrements sur l'exercice " + anneeSelectionnee);
		}
	}

	private final void recouvrementNewNational() {
		if (!prelevementNationalEnabled) {
			showWarningDialog("Les prélèvements nationaux ne sont pas actifs (Cf. Paramètre de configuration org.cocktail.gfc.prelevementnational.enabled)");
			return;
		}

		try {
			// Vérifier si l'exercice en cours est dépassé et interdire de créer
			// un nouveau recouvrement
			checkExerciceTermine();

			RecouvrementCtrl recouvrementCtrl = new RecouvrementCtrl(getEditingContext());
			EORecouvrement odp = recouvrementCtrl.createNewRecouvrementInWindow(myPanel.getMyDialog(), ZDateUtil.addDHMS(ZDateUtil.now(), 5, 0, 0, 0));
			if (odp != null) {
				filters.clear();
				Integer paiOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), odp).valueForKey("recoOrdre");
				filters.put("recoOrdreMin", paiOrdre);
				filters.put("recoOrdreMax", paiOrdre);

				myPanel.updateData();
				filters.clear();

			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void recouvrementNewSepaSDD() {
		Boolean prelevementSepaEnabled = CktlConfigUtil.getBooleanValueForConfig((String) ServerCallCompta.clientSideRequestGetConfig(getEditingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_ENABLED));
		if (!prelevementSepaEnabled) {
			showWarningDialog("Les prélèvements SEPA ne sont pas actifs (Cf. Paramètre de configuration org.cocktail.gfc.sepasddmandat.enabled)");
			return;
		}

		try {
			// Vérifier si l'exercice en cours est dépassé et interdire de créer
			// un nouveau recouvrement
			checkExerciceTermine();

			RecoSepaSddCtrl ctrl = new RecoSepaSddCtrl(myApp.editingContext());
			Integer numeroJourMoisDateEcheanceDefaut = Integer.valueOf((String) ServerCallCompta.clientSideRequestGetConfig(myApp.editingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_NUMERO_JOUR));
			LocalDate dateMax = LocalDate.now();
			int year = dateMax.getYear();
			int month = dateMax.getMonthOfYear();
			int day = dateMax.getDayOfMonth();
			dateMax = new LocalDate(year, month, numeroJourMoisDateEcheanceDefaut + 1);
			if (day > numeroJourMoisDateEcheanceDefaut) {
				dateMax = dateMax.plusMonths(1);
			}
			NSArray recouvrementGeneres = ctrl.openDialog(myApp.getMainWindow(), dateMax);
			if (recouvrementGeneres != null && recouvrementGeneres.count() > 0) {
				filters.clear();
				NSTimestamp date = ((org.cocktail.fwkcktlcompta.client.metier.EORecouvrement) recouvrementGeneres.objectAtIndex(0)).recoDateCreation();
				filters.put("recoDateCreationMin", date);
				filters.put("recoDateCreationMax", date);
				myPanel.updateData();
				filters.clear();

			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSrch() {
		try {
			setWaitCursor(true);
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	/**
     *
     */
	private final void refreshActions() {
		EORecouvrement recouvrement = (EORecouvrement) myPanel.getSelectedObject();
		if (recouvrement == null) {
			actionImprimerContenuRecouvrement.setEnabled(false);
			actionRegenererVirement.setEnabled(false);
			actionEnregistrer.setEnabled(false);
			actionImprimerBordereau.setEnabled(false);
			actionEmargerReleve.setEnabled(false);
		}
		else {
			actionImprimerContenuRecouvrement.setEnabled(true);
			actionRegenererVirement.setEnabled(TypeRecouvrementHelper.getSharedInstance().isBdf(recouvrement.typeRecouvrement()));
			boolean isVirement = EOTypeRecouvrement.MOD_DOM_ECHEANCIER.equals(recouvrement.typeRecouvrement().modDom());
			actionEnregistrer.setEnabled(isVirement);
			actionImprimerBordereau.setEnabled(isVirement);
			actionEmargerReleve.setEnabled(isVirement);
		}
	}

	private final void regenererVirement() {
		try {
			if (!myApp.appUserInfo().getCurrentExercice().estTresorerie()) {
				throw new DefaultClientException("Cette opération n'est autorisée que sur l'exercice en cours.");
			}
			if (myPanel.getSelectedObject() == null) {
				throw new DefaultClientException("Veuillez sélectionner un recouvrement");
			}
			EORecouvrement recouvrementTmp = (EORecouvrement) myPanel.getSelectedObject();
			RecouvrementRegenererPrelevementCtrl recouvrementRegenererVirementCtrl = new RecouvrementRegenererPrelevementCtrl(getEditingContext());
			recouvrementRegenererVirementCtrl.createNewRegenererVirementInWindow(getMyDialog(), recouvrementTmp);
			myPanel.updateData();

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void fermer() {
		getMyDialog().onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}

		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private final String recupererFichier() throws Exception {
		checkExerciceTermine();
		String leContenuFichierVirement = null;
		EORecouvrement virement = (EORecouvrement) myPanel.getSelectedObject();
		//Récupérer le contenu
		String sql = "select FICP_CONTENU from MARACUJA.prelevement_fichier where RECO_ORDRE=" + ServerProxy.serverPrimaryKeyForObject(getEditingContext(), virement).valueForKey("recoOrdre") + " and rownum=1 order by ficp_Ordre desc";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		if (res.count() > 0) {
			NSDictionary dico = (NSDictionary) res.objectAtIndex(0);
			leContenuFichierVirement = (String) dico.valueForKey("FICP_CONTENU");
		}
		return leContenuFichierVirement;
	}

	private final void enregistrerFichier() {
		try {
			if (!myApp.appUserInfo().getCurrentExercice().estTresorerie()) {
				throw new DefaultClientException("Cette opération n'est autorisée que sur l'exercice en cours.");
			}
			EORecouvrement recouvrement = (EORecouvrement) myPanel.getSelectedObject();
			String leContenuFichier = recupererFichier();
			RecouvrementProxyCtrl.enregistrerFichier(this, leContenuFichier, recouvrement);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}


	public final void imprimerBordereau() {
		try {
			final String leContenuFichierVirement = recupererFichier();
			final EORecouvrement currentRecouvrement = (EORecouvrement) myPanel.getSelectedObject();
			if (leContenuFichierVirement == null || leContenuFichierVirement.length() == 0) {
				throw new DefaultClientException("Le contenu du fichier est vide !");
			}

			final int nbOperations = (leContenuFichierVirement.length() / FactoryProcessPrelevementFichiersBdf.LONGUEUR_LIGNE_BDF) - 2;
			final EOPrelevementParamBdf prelevementParamBdf = RecouvrementProxyCtrl.getPrelevementParamBDFForRecouvrement(getEditingContext(), currentRecouvrement);
			RecouvrementProxyCtrl.imprimerBordereau(this, currentRecouvrement, prelevementParamBdf);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		try {
			win1.open();
		} finally {
			win1.dispose();
		}
	}

	private final class ActionNewRecouvrementBdf extends AbstractAction {
		public ActionNewRecouvrementBdf() {
			super("Nouveau prélèvement national");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
		}

		public void actionPerformed(ActionEvent e) {
			recouvrementNewNational();
		}
	}

	private final class ActionNewRecouvrementSepaSdd extends AbstractAction {
		public ActionNewRecouvrementSepaSdd() {
			super("Nouveau prélèvement SEPA SDD");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
		}

		public void actionPerformed(ActionEvent e) {
			recouvrementNewSepaSDD();
		}
	}

	public class ActionImprimerContenuRecouvrement extends AbstractAction {
		public ActionImprimerContenuRecouvrement() {
			super("Détail du recouvrement");
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerContenuRecouvrement();

		}
	}

	public final void imprimerContenuRecouvrement() {
		RecouvrementProxyCtrl.imprimerContenuPrelevement(this, (EORecouvrement) myPanel.getSelectedObject());
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super("Enregistrer le fichier");
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			enregistrerFichier();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super("Bordereau d'accompagnement");
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.SHORT_DESCRIPTION, "Imprimer le bordereau d'accompagnement");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}


	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionRegenererVirement extends AbstractAction {
		public ActionRegenererVirement() {
			super("Regénérer");
			setEnabled(false);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			regenererVirement();
		}
	}

	private final class ActionEmargerReleve extends AbstractAction {
		public ActionEmargerReleve() {
			super("Gestion des rejets");
			setEnabled(false);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rejeter les échéances et créer les écritures correspondantes");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				if (myApp.appUserInfo().getCurrentExercice().estClos()) {
					throw new DefaultClientException("L'exercice " + myApp.appUserInfo().getCurrentExercice().exeExercice().intValue() + " est clos.");
				}

				if (myPanel.getSelectedObject()!=null) {
					if (((EORecouvrement) myPanel.getSelectedObject()).typeRecouvrement().isSepaSdd()) {
						Map<String, Object> filters = new HashMap<String, Object>();
						filters.put(EOSepaSddEcheance.TO_RECOUVREMENT_KEY, EOConvertUtil.fetchFrom((EORecouvrement) myPanel.getSelectedObject()));
						final RecoSepaSddReleveCtrl dial = new RecoSepaSddReleveCtrl(getEditingContext());
						dial.openDialog(myApp.getMainFrame(), filters);
					}
					else {
						final PrelevReleveCtrl dial = new PrelevReleveCtrl(myApp.editingContext());
						dial.openDialog(myApp.getMainFrame(), (EORecouvrement) myPanel.getSelectedObject());
					}
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	private final class RecouvrementSrchPanelListener implements RecouvrementSrchPanel.IRecouvrementSrchPanelListener {
		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#actionNew()
		 */
		public Action actionNewRecouvrementBdf() {
			if (!prelevementNationalEnabled) {
				return null;
			}
			return actionNewRecouvrementBdf;
		}


		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return filters;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}



		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#getRecouvrements()
		 */
		public NSArray getRecouvrements() {
			//Créer la condition à partir des filtres
			try {
				return ZFinder.fetchArray(getEditingContext(), EORecouvrement.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), new NSArray(EORecouvrement.SORT_DATE_DESC), true);
			} catch (Exception e) {
				showErrorDialog(e);
				return new NSArray();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#actionRegenererVirement()
		 */
		public Action actionRegenererVirement() {
			return actionRegenererVirement;
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#actionImprimerContenuRecouvrement()
		 */
		public Action actionImprimerContenuRecouvrement() {
			return actionImprimerContenuRecouvrement;
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			refreshActions();
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementSrchPanel.IRecouvrementSrchPanelListener#actionEnregistrerFichier()
		 */
		public Action actionEnregistrerFichier() {
			return actionEnregistrer;
		}

		public Action actionImprimerBordereau() {
			return actionImprimerBordereau;
		}

		public Action actionEmargerReleve() {
			return actionEmargerReleve;
		}

		public Action actionNewRecouvrementSepaSdd() {
			if (!prelevementSepaEnabled) {
				return null;
			}
			return actionNewRecouvrementSepaSdd;
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
