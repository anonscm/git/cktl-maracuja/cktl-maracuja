/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement;

import java.awt.Window;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.TimeZone;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import org.cocktail.fwkcktlcompta.common.entities.IPrelevementFichier;
import org.cocktail.fwkcktlcompta.common.entities.IPrelevementParam;
import org.cocktail.fwkcktlcompta.common.entities.ITypeRecouvrement;
import org.cocktail.fwkcktlcompta.common.helpers.TypeRecouvrementHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.ICommonCtrl;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOPrelevementFichier;
import org.cocktail.maracuja.client.metier.EOPrelevementParamBdf;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * Fourni des méthodes partagées par les différentes interfaces qui gèrent les
 * recouvrements.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementProxyCtrl {

	private RecouvrementProxyCtrl() {
        super();
    }


    /**
     * @param ec
     * @param recouvrement
     * @return Le virementParamBDF VALIDE associé au typeVirement du recouvrement (impossible de récupérer le param utilisé à l'époque du recouvrement)
     */
    public static EOPrelevementParamBdf getPrelevementParamBDFForRecouvrement(EOEditingContext ec, final EORecouvrement recouvrement) {
        final EOTypeRecouvrement tv = recouvrement.typeRecouvrement();
        final EOPrelevementParamBdf virementParamBdf =  (EOPrelevementParamBdf) EOsFinder.fetchObject(ec, EOPrelevementParamBdf.ENTITY_NAME, "typeRecouvrement=%@ and ppbEtat=%@", new NSArray(new Object[]{tv, EOPrelevementParamBdf.ETAT_VALIDE}), null, false);
        return virementParamBdf;
    }


	public final static void imprimerBordereau(final ICommonCtrl ctrl, final EORecouvrement currentRecouvrement, IPrelevementParam prelevementParam) {
		if (currentRecouvrement.typeRecouvrement().isBdf()) {
			imprimerBordereauBDF((CommonCtrl) ctrl, currentRecouvrement.recoNb(), currentRecouvrement, (EOPrelevementParamBdf) prelevementParam, ctrl.getMyDialog());
		}
		else if (currentRecouvrement.typeRecouvrement().isSepaSdd()) {
			imprimerBordereauSepaSDD(ctrl, currentRecouvrement);
		}
	}


    /**
     * Imprime et renvoie le chemin d'accès du bordereau d'accompagnement pour
     * les virements BDF
     *
     * @param ec
     * @param dico
     * @param nbOperations
     * @param currentRecouvrement
     * @param prelevementParamBdf
     * @param directory
     * @return
     * @throws Exception
     */
	private final static void imprimerBordereauBDF(final CommonCtrl ctrl, int nbOperations, final EORecouvrement currentRecouvrement, final EOPrelevementParamBdf prelevementParamBdf, Window win) {
        final ApplicationClient myApp = ctrl.getMyApp();

        try {

            //Récupérer le dernier virementFichierGénéré
            final NSArray sorts = new NSArray( new Object[]{EOSortOrdering.sortOrderingWithKey("ficpDateCreation", EOSortOrdering.CompareDescending), EOSortOrdering.sortOrderingWithKey("ficpOrdre", EOSortOrdering.CompareDescending)    });


            final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("recouvrement=%@", new NSArray(currentRecouvrement));
            final EOFetchSpecification spec = new EOFetchSpecification(EOPrelevementFichier.ENTITY_NAME,qual,sorts,false,false,null);
            spec.setRefreshesRefetchedObjects(true);
            final NSArray virementFichiers = ctrl.getEditingContext().objectsWithFetchSpecification(spec);


//          NSArray virementFichiers = EOsFinder.fetchArray(getEditingContext(), EOVirementFichier.ENTITY_NAME,,new NSArray(currentRecouvrement),sorts,true);
            if (virementFichiers.count()==0) {
                throw new DefaultClientException("Impossible de recuperer l'enregistrement correspondant au virement");
            }


            final NSMutableDictionary dico = new NSMutableDictionary(   myApp.getParametres()   );
            dico.takeValueForKey(  dico.valueForKey("DEVISE_LIBELLE").toString().toUpperCase()   ,"MONNAIE");
            dico.takeValueForKey(  new Integer(nbOperations)   ,"NBRE_OP");
            dico.takeValueForKey(  ((EOPrelevementFichier)virementFichiers.objectAtIndex(0)).ficpCodeOp()    ,"CODE_OP");
            dico.takeValueForKey(  prelevementParamBdf.ppbC41()   ,"ID_TPG");
            dico.takeValueForKey(  prelevementParamBdf.ppbNomRemettant()   ,"UNIV_TPG_REMETTANT");
            dico.takeValueForKey(  prelevementParamBdf.ppbNomTpg()   ,"NOM_TPG");
            dico.takeValueForKey(  prelevementParamBdf.ppbCompteTpg()    ,"COMPTE_TPG");
            dico.takeValueForKey(   ((EOPrelevementFichier)virementFichiers.objectAtIndex(0)).ficpDateReglement()  ,"DATE_REGLEMENT");
            dico.takeValueForKey(   ((EOPrelevementFichier)virementFichiers.objectAtIndex(0)).ficpDateRemise()   ,"DATE_REMISE");
            dico.takeValueForKey(   currentRecouvrement.recoMontant()  ,"MONTANT_CUMUL");
            dico.takeValueForKey(   currentRecouvrement.recoNumero().toString()  ,"DISK_NUM");


//          MONTANT_CUMUL


            String filePath = ReportFactoryClient.imprimerBordereauPrelevement(myApp.editingContext(), myApp.temporaryDir, dico, null, win);
            if (filePath!=null) {
                myApp.openPdfFile(filePath);
            }
        }
        catch (Exception e1) {
            ctrl.showErrorDialog(e1);
        }

    }

    
	private static final void imprimerBordereauSepaSDD(final ICommonCtrl ctrl, final EORecouvrement currentRecouvrement) {
		final ApplicationClient myApp = (ApplicationClient) ctrl.getMyApp();

		try {
			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(myApp.editingContext(), currentRecouvrement).valueForKey(EORecouvrement.RECO_ORDRE_KEY), EORecouvrement.RECO_ORDRE_COLKEY.toUpperCase());
			//dico.takeValueForKey(ServerProxy.clientSideRequestGetWsResourceUrl(myApp.editingContext(), "logo_sepa.jpg"), "LOGO_SEPA_URL");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String tz = ServerProxy.clientSideRequestGetGrhumParam(myApp.editingContext(), "GRHUM_TIME_ZONE");
			if (tz == null || tz.length() == 0) {
				tz = "Europe/Paris";
			}
			sdf.setTimeZone(TimeZone.getTimeZone(tz));
			String s = sdf.format(new Date());
			dico.takeValueForKey(s, "DATE_EXECUTION");

			String filePath = ReportFactoryClient.imprimerBordereauSepaSdd(myApp.editingContext(), myApp.temporaryDir, dico, new NSArray(currentRecouvrement));
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

	public final static void imprimerContenuPrelevement(final ICommonCtrl ctrl, final EORecouvrement currentRecouvrement) {
		if (currentRecouvrement.typeRecouvrement().isBdf()) {
			imprimerContenuPrelevementBDF((CommonCtrl) ctrl, currentRecouvrement);
		}
		else if (currentRecouvrement.typeRecouvrement().isSepaSdd()) {
			imprimerDetailFichierPrelevementSEPA(ctrl, currentRecouvrement);
		}
	}

    /**
     * Imprime le contenu du recouvrement.
     */
	private final static void imprimerContenuPrelevementBDF(final CommonCtrl ctrl, final EORecouvrement reco) {
        final ApplicationClient myApp = ctrl.getMyApp();
        try {
            String filePath = ReportFactoryClient.imprimerContenuPrelevement(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), reco, ctrl.getMyDialog());
            if (filePath != null) {
                myApp.openPdfFile(filePath);
            }
        } catch (Exception e1) {
            ctrl.showErrorDialog(e1);
        }
    }

	private static final void imprimerDetailFichierPrelevementSEPA(final ICommonCtrl ctrl, final EORecouvrement currentRecouvrement) {
		final ApplicationClient myApp = (ApplicationClient) ctrl.getMyApp();
		try {
			String filePath = ReportFactoryClient.imprimerDetailFichierPrelevementSEPA(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(currentRecouvrement), ctrl.getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

    /**
     * Imprime la liste des prelevements en attente.
     * @param ctrl
     * @param params Doit contenir prelevDatePrelevementMin et prelevDatePrelevementMax pour definir la periode. prelevDatePrelevementMin peut être null.
     * @param win
     */
    public final static void imprimerPrelevementsAttente(final CommonCtrl ctrl, Map params, Window win) {
    	final ApplicationClient myApp = ctrl.getMyApp();
    	try {
    		String filePath = ReportFactoryClient.imprimerPrelevementsAttente(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), params, win);
    		if (filePath != null) {
    			myApp.openPdfFile(filePath);
    		}
    	} catch (Exception e1) {
    		ctrl.showErrorDialog(e1);
    	}
    }

    /**
     * Imprime la liste des prelevements SEPA en attente.
     * @param app application client singleton instance
     * @param ctrl
     * @param params Doit contenir prelevDatePrelevementMin et prelevDatePrelevementMax pour definir la periode. prelevDatePrelevementMin peut être null.
     * @param win
     */
    public final static void imprimerSepaPrelevementsAttente(final ApplicationClient app, Map params, Window win) throws Exception {
   		String filePath = ReportFactoryClient.imprimerSepaPrelevementsAttente(app.editingContext(), app.temporaryDir, app.getParametres(), params, win);
   		if (filePath != null) {
   			app.openPdfFile(filePath);
   		}
    }


	/**
	 * Affiche une boite de dialogue d'enregistrement de fichier et l'enregistre.
	 * 
	 * @param ctrl
	 * @param leContenuFichier
	 * @param recouvrement
	 * @throws Exception
	 */
	public final static void enregistrerFichier(CommonCtrl ctrl, String leContenuFichier, final EORecouvrement recouvrement) throws Exception {
        File f;

            if (leContenuFichier == null || leContenuFichier.length() == 0) {
                throw new DefaultClientException("Le contenu du fichier est vide !");
            }
		EOPrelevementFichier prelevementFichier = getLastPrelevementFichier(recouvrement);
		String defaultFileName = createFileName(prelevementFichier);

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(null);
            f = new File(new File(fileChooser.getCurrentDirectory() , defaultFileName).getCanonicalPath());
            fileChooser.setSelectedFile(f);

		TxtFileFilter filter1 = new TxtFileFilter(TypeRecouvrementHelper.getSharedInstance().getFileExtension(recouvrement.typeRecouvrement()), "");
            filter1.setExtensionListInDescription(true);
            fileChooser.addChoosableFileFilter(filter1);
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileFilter(filter1);
            fileChooser.setDialogTitle("Enregistrer-sous..");

            int ret = fileChooser.showSaveDialog(ctrl.getMyDialog());
            File file = fileChooser.getSelectedFile();

            // System.out.println("ret="+ret);
            // System.out.println("file="+file);

            if ((ret != JFileChooser.APPROVE_OPTION) || (file == null)) {
                return;
            }

            if (file.exists()) {
                if (!ctrl.showConfirmationDialog("Confirmation", "Le fichier " + file + " existe déjà, voulez-vous l'écraser ?", ZMsgPanel.BTLABEL_NO)) {
                    return;
                }
            }

            FileWriter writer = new FileWriter(file);
            writer.write(leContenuFichier);
            writer.close();

            System.out.println("Fichier " + file + " enregistré.");
    }


	/**
	 * Enregistre le contenu du champ ficpContenu de prelemevementFichier dans le répertoire spécifié.
	 * 
	 * @param prelevementFichier
	 * @param directory
	 * @param fileName facultatif
	 * @return
	 * @throws Exception
	 */
	public static File enregistrerFichier(IPrelevementFichier prelevementFichier, String directory, String fileName) throws Exception {
		File f;
		if (fileName == null) {
			fileName = createFileName(prelevementFichier);
		}
		if (prelevementFichier.ficpContenu() == null || prelevementFichier.ficpContenu().length() == 0) {
			throw new DefaultClientException("Le contenu du fichier est vide !");
		}

		f = new File(directory, fileName);
		f = new File(new File(directory, fileName).getCanonicalPath());

		FileWriter writer = new FileWriter(f);
		writer.write(prelevementFichier.ficpContenu());
		writer.close();

		System.out.println("Fichier " + f + " enregistré.");
		return f;
	}

	public static String createFileName(IPrelevementFichier prelevementFichier) throws Exception {
		ITypeRecouvrement typeRecouvrement = prelevementFichier.toRecouvrement().toTypeRecouvrement();
		String typeShort = typeRecouvrement.trecFormat();
		if (typeShort == null) {
			throw new DefaultClientException("Type de recouvrement non pris en charge.");
		}
		return "Fichier_" + typeShort + "PREL" + "_" + ZStringUtil.extendWithChars("" + prelevementFichier.toRecouvrement().recoNumero(), "0", 10, true) + "." + TypeRecouvrementHelper.getSharedInstance().getFileExtension(typeRecouvrement);
	}

	public static EOPrelevementFichier getLastPrelevementFichier(EORecouvrement recouvrement) {
		EOQualifier qual = new EOKeyValueQualifier(EOPrelevementFichier.RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, recouvrement);
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EOPrelevementFichier.FICP_DATE_CREATION_KEY, EOSortOrdering.CompareDescending);

		return EOPrelevementFichier.fetchFirstByQualifier(recouvrement.editingContext(), qual, new NSArray(new Object[] {
			sort
		}));
	}

	public static class TxtFileFilter extends javax.swing.filechooser.FileFilter {

		// private static String TYPE_UNKNOWN = "Type Unknown";
		// private static String HIDDEN_FILE = "Hidden File";

		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		/**
		 * Creates a file filter. If no filters are added, then all files are accepted.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter() {
			filters = new Hashtable();
		}

		/**
		 * Creates a file filter that accepts files with the given extension. Example: new ExampleFileFilter("jpg");
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension) {
			this(extension, null);
		}

		/**
		 * Creates a file filter that accepts the given file type. Example: new ExampleFileFilter("jpg", "JPEG Image Images"); Note that the "."
		 * before the extension is not needed. If provided, it will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension, String description) {
			this();
			if (extension != null) {
				addExtension(extension);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Creates a file filter from the given string array. Example: new ExampleFileFilter(String {"gif", "jpg"}); Note that the "." before the
		 * extension is not needed adn will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters) {
			this(filters, null);
		}

		/**
		 * Creates a file filter from the given string array and description. Example: new ExampleFileFilter(String {"gif", "jpg"}, "Gif and JPG
		 * Images"); Note that the "." before the extension is not needed and will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Return true if this file should be shown in the directory pane, false if it shouldn't. Files that begin with "." are ignored.
		 * 
		 * @see #getExtension
		 * @see FileFilter#accepts
		 */
		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		/**
		 * Return the extension portion of the file's name .
		 * 
		 * @see #getExtension
		 * @see FileFilter#accept
		 */
		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		/**
		 * Adds a filetype "dot" extension to filter against. For example: the following code will create a filter that filters out all files except
		 * those that end in ".jpg" and ".tif": ExampleFileFilter filter = new ExampleFileFilter(); filter.addExtension("jpg");
		 * filter.addExtension("tif"); Note that the "." before the extension is not needed and will be ignored.
		 */
		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		/**
		 * Returns the human readable description of this filter. For example: "JPEG and GIF Image Files (*.jpg, *.gif)"
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 * @see FileFilter#getDescription
		 */
		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description + " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "." + (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ." + (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				}
				else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		/**
		 * Sets the human readable description of this filter. For example: filter.setDescription("Gif and JPG Images");
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 */
		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		/**
		 * Determines whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description
		 * was provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see isExtensionListInDescription
		 */
		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		/**
		 * Returns whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description was
		 * provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see setExtensionListInDescription
		 */
		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}
}
