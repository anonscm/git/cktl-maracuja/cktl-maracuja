package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import org.cocktail.fwkcktlcompta.common.sepasdd.remises.SepaSddEcheancesCombinaison;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel;

public class SepaSddEcheanceCombinaisonList extends ZTablePanel {

	private static final long serialVersionUID = 1L;

	public SepaSddEcheanceCombinaisonList(IZTablePanelMdl listener) {
		super(listener);

		final ZEOTableModelColumn typeOperation = new ZEOTableModelColumn(myDisplayGroup, SepaSddEcheancesCombinaison.TYPE_OPERATION_KEY, "Type opération", 10);
		final ZEOTableModelColumn datePrevue = new ZEOTableModelColumn(myDisplayGroup, SepaSddEcheancesCombinaison.DATE_PREVUE_KEY, "Date des échéances", 60);
		final ZEOTableModelColumn datePrelevement = new ZEOTableModelColumn(myDisplayGroup, SepaSddEcheancesCombinaison.DATE_PRELEVEMENT_KEY, "Date de prélèvement", 60);
		final ZEOTableModelColumn remarques = new ZEOTableModelColumn(myDisplayGroup, SepaSddEcheancesCombinaison.REMARQUES_KEY, "Remarques", 200);
		datePrevue.setFormatDisplay(ZConst.DATEISO_FORMAT_DATESHORT);
		datePrelevement.setFormatDisplay(ZConst.DATEISO_FORMAT_DATESHORT);

		colsMap.clear();
		colsMap.put(typeOperation.getAttributeName(), typeOperation);
		colsMap.put(datePrevue.getAttributeName(), datePrevue);
		colsMap.put(datePrelevement.getAttributeName(), datePrelevement);
		colsMap.put(remarques.getAttributeName(), remarques);

	}

}
