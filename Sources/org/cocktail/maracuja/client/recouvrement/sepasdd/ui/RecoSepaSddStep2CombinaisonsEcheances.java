/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraStepPanel2;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZLabelTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZPanelNbTotal;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.SepaSddEcheanceExtList.ISepaSddEcheanceSelectListListener;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.foundation.NSArray;

/**
 * Etape correspondant à l'affichage des prelevements selectionnes.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecoSepaSddStep2CombinaisonsEcheances extends ZKarukeraStepPanel2 {
	private static final long serialVersionUID = 1L;
	private static final String TITLE = "Fichier à générer";
	private static final String COMMENTAIRE = "Liste des prélèvements qui vont être effectués (regroupés par fichier).";

	private IRecoSepaSddStep2CombinaisonsEcheancesListener myListener;

	private SepaSddEcheanceCombinaisonList combinaisonList;
	private SepaSddEcheanceSelectionneesList echeanceSelectionneesList;
	private ZPanelNbTotal panelTotal1;

	/**
	 * @param listener
	 */
	public RecoSepaSddStep2CombinaisonsEcheances(IRecoSepaSddStep2CombinaisonsEcheancesListener listener) {
		super(listener);
		myListener = listener;
		combinaisonList = new SepaSddEcheanceCombinaisonList(new SepaSddEcheanceCombinaisonListListener());
		echeanceSelectionneesList = new SepaSddEcheanceSelectionneesList(new SepaSddEcheanceSelectionneesListListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {

		final JPanel resultPanel = new JPanel(new GridLayout(2, 1, 5, 5));

		JPanel p0 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des fichiers à créer", null, ZConst.BG_COLOR_TITLE, combinaisonList, null, null);
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des échéances par fichier", null, ZConst.BG_COLOR_TITLE, echeanceSelectionneesList, null, null);
		panelTotal1 = new ZPanelNbTotal("Total à recouvrer");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());

		JPanel p = new JPanel(new BorderLayout());
		p.add(p1, BorderLayout.CENTER);
		p.add(panelTotal1, BorderLayout.SOUTH);

		resultPanel.add(p0);
		resultPanel.add(p);
		return resultPanel;
	}

	private final class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(myListener.getEcheances(), EOSepaSddEcheance.MONTANT_KEY);
			return total;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			//  int i=0;
			return new Integer(myListener.getEcheances().count());
		}

		public void setValue(Object value) {
			return;
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		combinaisonList.initGUI();
		echeanceSelectionneesList.initGUI();
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		combinaisonList.updateData();
		echeanceSelectionneesList.updateData();
		panelTotal1.updateData();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface IRecoSepaSddStep2CombinaisonsEcheancesListener extends ZStepListener {
		public NSArray getEcheances();

		public NSArray getEcheanceCombinaisons();

		public void combinaisonHasChanged();
	}

	private final class SepaSddEcheanceSelectionneesListListener implements ISepaSddEcheanceSelectListListener {

		public void selectionChanged() {

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return myListener.getEcheances();
		}

		public void onDbClick() {

		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return null;
		}

		public Integer getFlagForEcheance(ISepaSddEcheance echeance) {
			return null;
		}

	}

	private final class SepaSddEcheanceCombinaisonListListener implements IZTablePanelMdl {

		public void selectionChanged() {
			myListener.combinaisonHasChanged();
		}

		public NSArray getData() throws Exception {
			return myListener.getEcheanceCombinaisons();
		}

		public void onDbClick() {

		}

	}

	public SepaSddEcheanceCombinaisonList getCombinaisonList() {
		return combinaisonList;
	}

	public SepaSddEcheanceSelectionneesList getEcheanceSelectionneesList() {
		return echeanceSelectionneesList;
	}

}
