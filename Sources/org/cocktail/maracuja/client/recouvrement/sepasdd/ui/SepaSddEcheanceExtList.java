/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.TracableColFactory;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZComptaSwingTablePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.sepasddmandat.ui.SepaSddEcheanceColFactory;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

/**
 * Affiche une liste d'échéances (dans une optique hors master/detail, donc avec reprise des informations sur l'échancier et le mandat sdd rattachés à
 * l'échéance. ).
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SepaSddEcheanceExtList extends ZComptaSwingTablePanel {
	private static final long serialVersionUID = 1L;
	private ISepaSddEcheanceSelectListListener myListener;

	private ZEOTableModelColumn colRib;
	private ZEOTableModelColumn colDatePrevue;
	private ZEOTableModelColumn colOrigine;
	private ZEOTableModelColumn colDebiteur;
	private ZEOTableModelColumn colMontant;
	private ZEOTableModelColumn colType;
	private ZEOTableModelColumn colEtat;
	private ZEOTableModelColumn colModifiePar;
	private ZEOTableModelColumn colModifieLe;


	/**
     * 
     */
	public SepaSddEcheanceExtList(ISepaSddEcheanceSelectListListener listener) {
		super(listener);
		myListener = listener;
		//		colNumero = SepaSddEcheanceColFactory.sharedInstance().getNumeroSurNombre(myDisplayGroup);
		//		if (myListener.getTableCellRenderer() != null) {
		//			colNumero.setTableCellRenderer(myListener.getTableCellRenderer());
		//		}

		colOrigine = SepaSddEcheanceColFactory.sharedInstance().getOrigine(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colOrigine.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colDebiteur = SepaSddEcheanceColFactory.sharedInstance().getDebiteur(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colDebiteur.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colMontant = SepaSddEcheanceColFactory.sharedInstance().getMontant(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colMontant.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colDatePrevue = SepaSddEcheanceColFactory.sharedInstance().getDatePrevue(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colDatePrevue.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colRib = SepaSddEcheanceColFactory.sharedInstance().getRib(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colRib.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colType = SepaSddEcheanceColFactory.sharedInstance().getType(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colType.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colEtat = SepaSddEcheanceColFactory.sharedInstance().getEtat(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colEtat.setTableCellRenderer(myListener.getTableCellRenderer());
		}


		colModifiePar = TracableColFactory.sharedInstance().getModifiePar(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colModifiePar.setTableCellRenderer(myListener.getTableCellRenderer());
		}

		colModifieLe = TracableColFactory.sharedInstance().getModifieLe(myDisplayGroup);
		if (myListener.getTableCellRenderer() != null) {
			colModifieLe.setTableCellRenderer(myListener.getTableCellRenderer());
		}
		
		

		colsMap.clear();
		initColsMap();

	}

	public ZEOTableModelColumn getColEtat() {
		return colEtat;
	}

	public void initColsMap() {

		colsMap.put(getColDatePrevue().getAttributeName(), getColDatePrevue());
		colsMap.put(getColDebiteur().getAttributeName(), getColDebiteur());
		colsMap.put(getColOrigine().getAttributeName(), getColOrigine());
		colsMap.put(getColMontant().getAttributeName(), getColMontant());
		colsMap.put(getColRib().getAttributeName(), getColRib());
		colsMap.put(getColEtat().getAttributeName(), getColEtat());
		colsMap.put(getColModifieLe().getAttributeName(), getColModifieLe());
		colsMap.put(getColModifiePar().getAttributeName(), getColModifiePar());
	}

	public ZEOTableModelColumn getColRib() {
		return colRib;
	}

	public ZEOTableModelColumn getColDatePrevue() {
		return colDatePrevue;
	}

	public ZEOTableModelColumn getColOrigine() {
		return colOrigine;
	}

	public ZEOTableModelColumn getColDebiteur() {
		return colDebiteur;
	}

	public ZEOTableModelColumn getColMontant() {
		return colMontant;
	}

	public ZEOTableModelColumn getColType() {
		return colType;
	}

	public ZEOTableModelColumn getColModifiePar() {
		return colModifiePar;
	}

	public ZEOTableModelColumn getColModifieLe() {
		return colModifieLe;
	}

	public interface ISepaSddEcheanceSelectListListener extends ZComptaSwingTablePanel.IZKarukeraTablePanelListener {
		public IZEOTableCellRenderer getTableCellRenderer();


	}

	public ISepaSddEcheanceSelectListListener getMyListener() {
		return myListener;
	}



}
