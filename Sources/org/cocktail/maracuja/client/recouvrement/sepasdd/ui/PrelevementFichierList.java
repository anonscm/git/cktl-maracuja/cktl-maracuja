package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier;
import org.cocktail.fwkcktlcompta.client.metier.EORecouvrement;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files.FileListTablePanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;

public class PrelevementFichierList extends FileListTablePanel {

	private static final long serialVersionUID = 1L;

	public PrelevementFichierList(IFileListTablePanelMdl listener) {
		super(listener);

		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevementFichier.TO_RECOUVREMENT_KEY + "." + EORecouvrement.RECO_NUMERO_KEY, "N°", 60);
		col5.setAlignment(SwingConstants.LEFT);
		col5.setColumnClass(Integer.class);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevementFichier.FICP_MONTANT_KEY, "Montant", 90);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn typeOperation = new ZEOTableModelColumn(myDisplayGroup, EOPrelevementFichier.FICP_CODE_OP_KEY, "Type opération", 90);
		typeOperation.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn nbOperation = new ZEOTableModelColumn(myDisplayGroup, EOPrelevementFichier.FICP_NB_PRELEVEMENTS_KEY, "Nombre d'opérations", 90);
		nbOperation.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn dateRemise = new ZEOTableModelColumn(myDisplayGroup, EOPrelevementFichier.FICP_DATE_REMISE_KEY, "Date remise", 90);
		dateRemise.setAlignment(SwingConstants.CENTER);
		dateRemise.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		ZEOTableModelColumn dateReglement = new ZEOTableModelColumn(myDisplayGroup, EOPrelevementFichier.FICP_DATE_REGLEMENT_KEY, "Date règlement", 90);
		dateReglement.setAlignment(SwingConstants.CENTER);
		dateReglement.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		
		//colsMap.clear();
		colsMap.put(col5.getAttributeName(), col5);
		colsMap.put(typeOperation.getAttributeName(), typeOperation);
		colsMap.put(dateRemise.getAttributeName(), dateRemise);
		colsMap.put(dateReglement.getAttributeName(), dateReglement);
		colsMap.put(nbOperation.getAttributeName(), nbOperation);
		colsMap.put(col4.getAttributeName(), col4);

	}

}
