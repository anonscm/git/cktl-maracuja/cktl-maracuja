/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.util.BooleanFormat;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumnDico;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * Affiche un panel de sélection d'échéances avec possibilité de sélectionner plusieurs échéances.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SepaSddEcheanceSelectList extends SepaSddEcheanceExtList {

	private static final long serialVersionUID = 1L;
	public static final String SELECTION_COLUMN_TITLE = "Sélect.";

	private JPopupMenu myPopupMenu;
	private ISepaSddEcheanceSelectListListener myListener;
	protected MyTableModelColumnDico col0;
	private ZEOTableModelColumn colErreurs;
	private ZPanelNbTotal panelTotal1;
	private ErreursCellRenderer erreursCellRenderer = new ErreursCellRenderer();

	public SepaSddEcheanceSelectList(ISepaSddEcheanceSelectListListener listener) {
		super(listener);
		myListener = listener;

		col0 = new MyTableModelColumnDico(myDisplayGroup, SELECTION_COLUMN_TITLE, 60, myListener.getAllCheckedPrelevements());
		col0.setEditable(true);
		col0.setResizable(false);
		col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher
		col0.setTableCellEditor(myListener.getTableCellEditorForCheck());

		colsMap.clear();
		colsMap.put("col0", col0);
		if (myListener.showColAnomalie()) {
			colErreurs = new ZEOTableModelColumnWithProvider("Anomalie", new FlagProvider(), 60);
			colErreurs.setAlignment(SwingConstants.CENTER);
			colErreurs.setColumnClass(Integer.class);
			colErreurs.setTableCellRenderer(erreursCellRenderer);
			colsMap.put(getColErreurs().getAttributeName(), getColErreurs());
		}

		super.initColsMap();

	}

	public void initGUI() {
		super.initGUI();
		initPopupMenu();
		if (myListener.showTotalPanel()) {
			add(buildTotalPanel(), BorderLayout.SOUTH);
		}
	}

	private final JPanel buildTotalPanel() {
		panelTotal1 = new ZPanelNbTotal("Total");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());
		return panelTotal1;
	}

	private final void initPopupMenu() {
		myPopupMenu = new JPopupMenu();
		myPopupMenu.add(new ActionCheckAll());
		myPopupMenu.add(new ActionUncheckAll());
		myEOTable.setPopup(myPopupMenu);
	}

	public interface ISepaSddEcheanceSelectListListener extends SepaSddEcheanceExtList.ISepaSddEcheanceSelectListListener {
		public HashMap getAllCheckedPrelevements();

		public boolean showColAnomalie();

		public TableCellEditor getTableCellEditorForCheck();

		public NSArray getEcheancesSelectionnees();

		public void afterCheck();

		public String getErreursForEcheance(ISepaSddEcheance echeance);

		public void checkAll(boolean b);

		public boolean showTotalPanel();

	}

	private final class ActionCheckAll extends AbstractAction {
		public ActionCheckAll() {
			super("Tout cocher");
		}

		public void actionPerformed(ActionEvent e) {
			myListener.checkAll(true);
		}
	}

	private final class ActionUncheckAll extends AbstractAction {
		public ActionUncheckAll() {
			super("Tout décocher");
		}

		public void actionPerformed(ActionEvent e) {
			myListener.checkAll(false);
		}
	}

	private final class MyTableModelColumnDico extends ZEOTableModelColumnDico {

		public MyTableModelColumnDico(EODisplayGroup vDg, String vTitle, int vpreferredWidth, HashMap aDico) {
			super(vDg, vTitle, vpreferredWidth, aDico);
		}

		public void setValueAtRow(final Object value, final int row) {
			super.setValueAtRow(value, row);
			myListener.afterCheck();

		}

	}

	private final class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(myListener.getEcheancesSelectionnees(), EOSepaSddEcheance.MONTANT_KEY);
			return total;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return new Integer(myListener.getEcheancesSelectionnees().count());
		}

		public void setValue(Object value) {
			return;
		}
	}

	public ZPanelNbTotal getPanelTotal1() {
		return panelTotal1;
	}

	@Override
	public void updateData() throws Exception {
		super.updateData();
		if (getPanelTotal1() != null) {
			getPanelTotal1().updateData();
		}
	}

	public ZEOTableModelColumn getColErreurs() {
		return colErreurs;
	}

	public final class FlagProvider implements ZEOTableModelColumnProvider {

		public FlagProvider() {
			super();
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			final ISepaSddEcheance echeance = (ISepaSddEcheance) myDisplayGroup.displayedObjects().objectAtIndex(row);
			return myListener.getErreursForEcheance(echeance);
		}

	}

	private final class ErreursCellRenderer extends ZEOTableCellRenderer {

		private static final long serialVersionUID = 1L;

		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			Component res = null;
			res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected) {
				res.setBackground(table.getSelectionBackground());
				res.setForeground(table.getSelectionForeground());
			}
			else {
				res.setBackground(table.getBackground());
				res.setForeground(table.getForeground());
			}

			String erreur = (String) value;
			if (erreur != null && erreur.length() > 0) {
				res.setBackground(ZConst.BGCOL_DESEQUILIBRE);
				res.setForeground(Color.BLACK);
			}

			return res;
		}
	}

}
