/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraStepPanel2;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files.FileListTablePanel.IFileListTablePanelMdl;

/**
 * Etape correspondant à l'affichage des prelevements selectionnes.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecoSepaSddStepFinalRecouvrements extends ZKarukeraStepPanel2 {
	private static final long serialVersionUID = 1L;
	private static final String TITLE = "Recouvrements générés";
	private static final String COMMENTAIRE = "Recouvrements générés";

	private IRecoSepaSddStepFinalRecouvrementsListener myListener;

	private PrelevementFichierList prelevementFichiersList;

	/**
	 * @param listener
	 */
	public RecoSepaSddStepFinalRecouvrements(IRecoSepaSddStepFinalRecouvrementsListener listener) {
		super(listener);
		myListener = listener;
		prelevementFichiersList = new PrelevementFichierList(myListener.getFileListTableModel());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p0 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des fichiers de prélèvement créés", null, ZConst.BG_COLOR_TITLE, prelevementFichiersList, null, null);

		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(p0, BorderLayout.CENTER);
		tmp.add(buildButtonsPanel(), BorderLayout.SOUTH);
		return tmp;
	}

	private final JPanel buildButtonsPanel() {
		final ArrayList<Action> list2 = new ArrayList<Action>(3);
		list2.add(myListener.actionEnregistrerFichiers());
		list2.add(myListener.actionImprimerBordereaux());
		list2.add(myListener.actionImprimerContenuFichiersPrelevement());

		final ArrayList list = ZKarukeraPanel.getButtonListFromActionList(list2);

		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(65, 150, 15, 150));
		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}



	public void initGUI() {
		prelevementFichiersList.initGUI();
		super.initGUI();
	}


	public void updateData() throws Exception {
		prelevementFichiersList.updateData();
		myListener.refreshActions();
	}

	public String getTitle() {
		return TITLE;
	}


	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface IRecoSepaSddStepFinalRecouvrementsListener extends ZStepListener {

		IFileListTablePanelMdl getFileListTableModel();

		void refreshActions();

		Action actionImprimerContenuFichiersPrelevement();

		Action actionImprimerBordereaux();

		Action actionEnregistrerFichiers();

	}



}
