/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.event.ListDataListener;
import javax.swing.table.TableCellEditor;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.DetailsTableCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraStepPanel2;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZUiUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.forms.ZTextField;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSArray;

/**
 * Etape correspondant a la selection des prelevements a recouvrer.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecoSepaSddStep1EcheancesSelection extends ZKarukeraStepPanel2 {
	private static final long serialVersionUID = 1L;

	private static final Dimension DETAILS_DIALOG_DIMENSION = new Dimension(600, 400);
	private static final String TITLE = "Sélection des échéances à partir desquelles construire la remise de prélèvements";
	private static final String COMMENTAIRE = "<html>Lancez la recherche puis sélectionnez dans la liste les échéances en attente partir desquelles générer la remise. <br/><b>Attention</b> : si des échances n'apparaissent pas alors qu'elles le devraient, vérifiez que les mandats SDD dont elles dépendent soient bien à l'état <b>VALIDE</b>.<br/>"
			+
			"D'autre part, l'écriture de prise en charge de l'échéancier correspondant doit être passée.</html>";

	private final SepaSddEcheanceSelectList sepaSddEcheanceSelectList;
	private final EcheancesSrchFilterPanel filterPanel;

	private final IRecoSepaSddStep1EcheancesSelectionListener myListener;

	/**
	 * @param listener
	 */
	public RecoSepaSddStep1EcheancesSelection(IRecoSepaSddStep1EcheancesSelectionListener listener) {
		super(listener);
		myListener = listener;
		sepaSddEcheanceSelectList = new SepaSddEcheanceSelectList(new SepaSddEcheanceSelectListListener());

		final IEcheancesSrchFilterListener list = new IEcheancesSrchFilterListener() {

			public Action getActionSrch() {
				return myListener.getActionSrch();
			}

			public HashMap getFilters() {
				return myListener.getFilters();
			}

			public Action getPrelevAttentePrint() {
				return myListener.getPrelevAttentePrint();
			}

            public ComboBoxModel getTypesOrigineModel() {
                return myListener.getTypesOrigineModel();
            }
		};

		filterPanel = new EcheancesSrchFilterPanel(list);

	}

	private void openDetailsDialog() {
		setWaitCursor(true);
		try {
			final DetailsTableCtrl ctrl = new DetailsTableCtrl(sepaSddEcheanceSelectList);
			ctrl.setDimension(DETAILS_DIALOG_DIMENSION);
			ctrl.openDialogModify(getMyDialog());
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	public JPanel getCenterPanel() {
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des échéances en attente", null, ZConst.BG_COLOR_TITLE, sepaSddEcheanceSelectList, null, null);
		JPanel p = new JPanel(new BorderLayout());
		p.add(p1, BorderLayout.CENTER);
		p.add(filterPanel, BorderLayout.NORTH);
		return p;
	}

	public void initGUI() {
		sepaSddEcheanceSelectList.initGUI();
		filterPanel.initGUI();
		//        sepaSddEcheanceSelectList.getMyEOTable().setMyRenderer(myListener.getTableCellRenderer());
		super.initGUI();
	}

	public void updateData() throws Exception {
		filterPanel.updateData();
		sepaSddEcheanceSelectList.updateData();
	}

	public String getTitle() {
		return TITLE;
	}

	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface IRecoSepaSddStep1EcheancesSelectionListener extends ZStepListener {
		public NSArray getAllPrelevements();

		public IZEOTableCellRenderer getTableCellRenderer();

		public HashMap getCheckedPrelevements();

		public Action getActionSrch();

		public Action getPrelevAttentePrint();

		public HashMap getFilters();

		public void selectionChanged();

		public void afterCheck();

		public NSArray getEcheancesARecouvrer();

		public String getErreursForEcheance(ISepaSddEcheance echeance);

		public void checkAll(boolean b);

		public TableCellEditor getTableCellEditorForCheck();
		
		public ComboBoxModel getTypesOrigineModel();
	}

	private final class SepaSddEcheanceSelectListListener implements SepaSddEcheanceSelectList.ISepaSddEcheanceSelectListListener {

		public HashMap getAllCheckedPrelevements() {
			return myListener.getCheckedPrelevements();
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return myListener.getTableCellRenderer();
		}

		public void afterCheck() {
			myListener.afterCheck();
		}

		public NSArray getData() throws Exception {
			return myListener.getAllPrelevements();
		}

		public void onDbClick() {
			openDetailsDialog();
		}

		public void selectionChanged() {
			myListener.selectionChanged();

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return myListener.getTableCellRenderer();
		}

		public NSArray getEcheancesSelectionnees() {
			return myListener.getEcheancesARecouvrer();
		}

		public String getErreursForEcheance(ISepaSddEcheance echeance) {
			return myListener.getErreursForEcheance(echeance);
		}

		public void checkAll(boolean b) {
			myListener.checkAll(b);

		}

		public TableCellEditor getTableCellEditorForCheck() {
			return myListener.getTableCellEditorForCheck();
		}

		public boolean showColAnomalie() {
			return true;
		}

		public boolean showTotalPanel() {
			return true;
		}

	}

	public interface IEcheancesSrchFilterListener {
		public Action getActionSrch();

		public Action getPrelevAttentePrint();

		public HashMap getFilters();
		
		public ComboBoxModel getTypesOrigineModel();
	}

	/**
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public class EcheancesSrchFilterPanel extends ZKarukeraPanel {
		private final Color BORDURE_COLOR = getBackground().brighter();

		private final IEcheancesSrchFilterListener myListener;

		private ZFormPanel datesPanel;

		/**
		 * @param editingContext
		 */
		public EcheancesSrchFilterPanel(IEcheancesSrchFilterListener listener) {
			super();
			myListener = listener;
			
			setLayout(new BorderLayout());
			setBorder(ZKarukeraPanel.createMargin());
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
		 */
		public void initGUI() {
			add(buildFilters(), BorderLayout.CENTER);
			add(buildButtons(), BorderLayout.EAST);
		}
		
		private final Component buildButtons() {
			return ZUiUtil.buildBoxLine(new Component[] {
					ZUiUtil.getButtonFromAction(myListener.getActionSrch()), ZUiUtil.getButtonFromAction(myListener.getPrelevAttentePrint())
			});
		}

		private final JPanel buildFilters() {
			datesPanel = buildDateFields();
			datesPanel.setDefaultAction(myListener.getActionSrch());

			ZFormPanel typeoriginePanel = ZFormPanel.buildLabelField("Type origine", new JComboBox(myListener.getTypesOrigineModel()));
		
			JPanel p = new JPanel(new BorderLayout());
			p.add(ZKarukeraPanel.buildLine(new Component[] {
					datesPanel, typeoriginePanel
			}), BorderLayout.WEST);
			p.add(new JPanel(new BorderLayout()));
			return p;
		}
		
		private final ZFormPanel buildDateFields() {
			return ZFormPanel.buildFourchetteStringDateFields("<= Date <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		}
		
		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
		 */
		public void updateData() throws Exception {
			datesPanel.updateData();
		}

		private final class NumeroMinModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("paiNumeroMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("paiNumeroMin", value);

			}

		}

		private final class NumeroMaxModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("odpNumeroMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("odpNumeroMax", value);

			}

		}

		private final class MontantHtMinModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("paiMontantMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("paiMontantMin", value);

			}

		}

		private final class MontantHtMaxModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("paiMontantMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("paiMontantMax", value);

			}

		}

		private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("prelevDatePrelevementMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("prelevDatePrelevementMax", value);
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

		private final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("prelevDatePrelevementMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("prelevDatePrelevementMin", value);
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

	}

	public EOSepaSddEcheance getSelectedPrelevement() {
		return (EOSepaSddEcheance) sepaSddEcheanceSelectList.selectedObject();
	}

	public final SepaSddEcheanceSelectList getSepaSddEcheanceSelectList() {
		return sepaSddEcheanceSelectList;
	}

}
