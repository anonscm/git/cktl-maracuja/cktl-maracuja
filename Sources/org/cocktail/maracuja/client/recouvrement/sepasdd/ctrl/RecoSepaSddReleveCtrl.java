/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.table.TableCellEditor;

import org.cocktail.fwkcktlcompta.client.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.client.metier.EOEcriture;
import org.cocktail.fwkcktlcompta.client.metier.EOEmargement;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.common.exception.DataCheckException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;
import org.cocktail.fwkcktlcompta.common.util.ZStringUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZMsgPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOUtilities;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.EOConvertUtil;
import org.cocktail.maracuja.client.metier.EOEcheancier;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddRelevePanel;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.SepaSddEcheanceSelectList.ISepaSddEcheanceSelectListListener;
import org.cocktail.maracuja.client.remotecall.ServerCallsepa;
import org.cocktail.maracuja.common.services.ISepaSddService;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class RecoSepaSddReleveCtrl extends CommonCtrl {
	private final static String TITLE = "Gestion des rejets de prélèvements";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);
	private static final String PCONUM_REJET_LIKE = "5*";
	private final Boolean PRESELECTION_OBJET = Boolean.FALSE;
	protected final Dimension PCOWINDOW_SIZE = new Dimension(300, 350);

	private final RecoSepaSddRelevePanel recoSepaSddRelevePanel;

	private final SepaSddEcheanceSelectListListener sepaSddEcheanceSelectListListener;
	private final RecoSepaSddRelevePanelListener recoSepaSddRelevePanelListener;
	private final ActionClose actionClose = new ActionClose();
	private final ActionEmargerReleve actionEmargerReleve = new ActionEmargerReleve();
	private final HashMap<ISepaSddEcheance, Boolean> checkedEcheances = new HashMap<ISepaSddEcheance, Boolean>();
	private NSArray allEcheances;

	private NSArray echeancesConfirmes = new NSArray();
	private NSArray echeancesRejetes = new NSArray();
	private LinkedHashMap pcoMap;
	private final PcoLookupModel pcoLookupModel;
	private final PcoLookupListener pcoLookupListener;
	private final PcoLookupModelRejet pcoLookupModelRejet;
	private final PcoLookupListenerRejet pcoLookupListenerRejet;
	private Map<String, Object> filters = new HashMap<String, Object>();
	private EOPlanComptableExer plancoDebitConfirme;
	private EOPlanComptableExer plancoDebitRejet;


	private String msgFin;
	private Exception lastException;
	private ApplicationClient myApp = (ApplicationClient) EOApplication.sharedApplication();

	ZWaitingPanelDialog waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));


	public RecoSepaSddReleveCtrl(final EOEditingContext ec) throws Exception {
		super(ec);
		revertChanges();

		final NSArray pcos;
		pcos = getPcoValides();
		pcoMap = new LinkedHashMap(pcos.count());
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptableExer element = (EOPlanComptableExer) pcos.objectAtIndex(i);
			pcoMap.put(ZStringUtil.extendWithChars(element.pcoNum(), " ", 15, false) + " " + element.pcoLibelle(), element);
		}

		pcoLookupModel = new PcoLookupModel();
		pcoLookupListener = new PcoLookupListener();
		pcoLookupModelRejet = new PcoLookupModelRejet();
		pcoLookupListenerRejet = new PcoLookupListenerRejet();
		sepaSddEcheanceSelectListListener = new SepaSddEcheanceSelectListListener();
		recoSepaSddRelevePanelListener = new RecoSepaSddRelevePanelListener();
		recoSepaSddRelevePanel = new RecoSepaSddRelevePanel(recoSepaSddRelevePanelListener);
	}

	protected final NSArray getPcoValides() {
		return EOPlanComptableExer.fetchAllValidesLike(getEditingContext(), getExercice(), PCONUM_REJET_LIKE);
	}

	@SuppressWarnings("rawtypes")
	protected NSMutableArray buildFilterQualifiers(Map<String, Object> dicoFiltre) {
		NSMutableArray quals = new NSMutableArray();
		if (dicoFiltre.get(EOSepaSddEcheance.TO_RECOUVREMENT_KEY) != null) {
			quals.addObject(new EOKeyValueQualifier(EOSepaSddEcheance.TO_RECOUVREMENT_KEY, EOQualifier.QualifierOperatorEqual, dicoFiltre.get(EOSepaSddEcheance.TO_RECOUVREMENT_KEY)));
		}
		return quals;
	}

	public final void updateData() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		checkedEcheances.clear();
		final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey(EOSepaSddEcheance.D_PREVUE_KEY, EOSortOrdering.CompareAscending);
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY + "." + EOSepaSddMandat.NUMERO_KEY, EOSortOrdering.CompareAscending);
		NSMutableArray andQuals = new NSMutableArray();

		EOQualifier qualEtat = new EOKeyValueQualifier(EOSepaSddEcheance.ETAT_KEY, EOQualifier.QualifierOperatorEqual, ISepaSddEcheance.Etat.PRELEVE.toString());
		andQuals.addObject(qualEtat);
		andQuals.addObjectsFromArray(buildFilterQualifiers(filters));

		NSMutableArray res = new NSMutableArray();

		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(sort0);
		sorts.addObject(sort1);
		res.addObjectsFromArray(EOSepaSddEcheance.fetchAll(getEditingContext(), new EOAndQualifier(andQuals), null, true));

		allEcheances = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, sorts).mutableClone();
		checkAllEcheances(PRESELECTION_OBJET);

		refreshTotaux();

		recoSepaSddRelevePanel.getPcoSelectButton().updateData();
		recoSepaSddRelevePanel.getPcoSelectButtonRejet().updateData();
		refreshActions();

	}

	private final void onValider() {
		try {
			//			final String pcoNum = (String) filters.get("pcoNum");
			//			if (pcoNum == null) {
			//				throw new DataCheckException("Vous devez indiquer un compte de confirmation");
			//			}
			//			plancoDebitConfirme = EOPlanComptableExer.getCompte(getEditingContext(), getExercice(), pcoNum);
			//			if (plancoDebitConfirme == null) {
			//				throw new DataCheckException("Le compte " + pcoNum + " que vous avez indiqué n'existe pas");
			//			}
			//			else if (!plancoDebitConfirme.isValide()) {
			//				throw new DataCheckException("Le compte " + pcoNum + " que vous avez indiqué n'est pas valide dans le plan comptable");
			//			}
			Boolean genererEcritures = (Boolean) filters.get("genererEcritures");
			if (echeancesRejetes.count() > 0) {
				if (genererEcritures.booleanValue()) {
					//Vérifier le compte selectionné
					final String pcoNumRejet = (String) filters.get("pcoNumRejet");
					if (pcoNumRejet == null) {
						throw new DataCheckException("Vous devez indiquer un compte de rejet");
					}
					plancoDebitRejet = EOPlanComptableExer.getCompte(getEditingContext(), getExercice(), pcoNumRejet);
					if (plancoDebitRejet == null) {
						throw new DataCheckException("Le compte de rejet " + pcoNumRejet + " que vous avez indiqué n'existe pas ou n'est pas valide dans le plan comptable");
					}
					if (!plancoDebitRejet.isValide()) {
						throw new DataCheckException("Le compte " + pcoNumRejet + " que vous avez indiqué n'est pas valide dans le plan comptable");
					}
				}
			}

			if (showConfirmationDialog("Confirmation", "Souhaitez-vous basculer les échéances sélectionnées à l'état REJETE ?\n" +
					(genererEcritures.booleanValue() ? "Les écritures de rejet seront générées." : "Les écritures de rejet NE SERONT PAS générées."), ZMsgPanel.BTLABEL_NO)) {
				waitingDialog.setTitle("Traitement en cours");
				waitingDialog.setTopText("Veuillez patienter ...");
				waitingDialog.setModal(true);
				lastException = null;
				updateLoadingMsg("Ce traitement peut être long...");
				try {

					final TraitementReleveThread thread = new TraitementReleveThread();
					thread.start();

					if (thread.isAlive()) {
						waitingDialog.setVisible(true);
					}
					if (lastException != null) {
						throw lastException;
					}

				} catch (Exception e) {
					showErrorDialog(e);
					getEditingContext().revert();
				} finally {
					waitingDialog.setVisible(false);
					if (waitingDialog != null) {
						waitingDialog.dispose();
					}
				}
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		recoSepaSddRelevePanel.initGUI();
		recoSepaSddRelevePanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(recoSepaSddRelevePanel);
		win.pack();
		return win;
	}

	public final void openDialog(Window dial, Map<String, Object> filters) {
		final ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		if (filters != null) {
			this.filters.putAll(filters);
		}
		this.filters.put("genererEcritures", Boolean.TRUE);
		updateData();

		try {
			recoSepaSddRelevePanel.updateData();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
	}

	private final NSArray getPrelevementSelectionnes(HashMap map, Boolean isSelected) {
		Iterator iter1 = map.keySet().iterator();
		NSMutableArray res = new NSMutableArray();
		while (iter1.hasNext()) {
			EOSepaSddEcheance element = (EOSepaSddEcheance) iter1.next();
			if (isSelected.equals(map.get(element))) {
				res.addObject(element);
			}
		}
		return res;
	}

	private void refreshTotaux() {
		echeancesConfirmes = getPrelevementSelectionnes(checkedEcheances, Boolean.FALSE);
		echeancesRejetes = getPrelevementSelectionnes(checkedEcheances, Boolean.TRUE);

		recoSepaSddRelevePanel.updateDataTotaux();

	}

	private final class RecoSepaSddRelevePanelListener implements RecoSepaSddRelevePanel.IRecoSepaSddRelevePanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionEmargerReleve() {
			return actionEmargerReleve;
		}

		public Integer getAcceptesNb() {
			return new Integer(echeancesConfirmes.count());
		}

		public BigDecimal getAcceptesTotal() {
			return ZEOUtilities.calcSommeOfBigDecimals(echeancesConfirmes, EOSepaSddEcheance.MONTANT_KEY);
		}

		public ISepaSddEcheanceSelectListListener getSepaSddEcheanceSelectListListener() {
			return sepaSddEcheanceSelectListListener;
		}

		public Integer getRejetesNb() {
			return new Integer(echeancesRejetes.count());
		}

		public BigDecimal getRejetesTotal() {
			return ZEOUtilities.calcSommeOfBigDecimals(echeancesRejetes, EOSepaSddEcheance.MONTANT_KEY);
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return null;
		}

		public void onSelectionChanged() {

		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public Map getValues() {
			return filters;
		}

		public IZLookupButtonListener getLookupButtonCompteRejetListener() {
			return pcoLookupListenerRejet;
		}

		public IZLookupButtonModel getLookupButtonCompteRejetModel() {
			return pcoLookupModelRejet;
		}


	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			getMyDialog().onCloseClick();
		}

	}

	public final class ActionEmargerReleve extends AbstractAction {

		public ActionEmargerReleve() {
			super("Rejeter");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onValider();
		}

	}

	private class PcoLookupListener implements IZLookupButtonListener {

		public void setNewValue(Object res) {
			final EOPlanComptableExer tmp = (EOPlanComptableExer) pcoMap.get(res);
			String s = (String) filters.get("pcoNum");
			s = tmp.pcoNum();

			System.out.println("PcoLookupListener.setNewValue() = " + s);
			filters.put("pcoNum", s);
			try {
				recoSepaSddRelevePanel.getPcoNum().updateData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onTextHasChanged() {

		}

		public void onDbClick() {

		}

	}

	private class PcoLookupModel implements IZLookupButtonModel {
		private final Collection pcosTmp;

		public PcoLookupModel() {
			pcosTmp = pcoMap.keySet();
		}

		public Collection getDatas() {
			return pcosTmp;
		}

		public String getActionDescription() {
			return "Sélectionner un compte";
		}

		public ImageIcon getIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
		}

		public Window getMyWindow() {
			return getMyDialog();
		}

		public String getTitle() {
			return "Sélectionnez un compte";
		}

		public Dimension getPreferredSize() {
			return PCOWINDOW_SIZE;
		}

	}

	private class PcoLookupListenerRejet implements IZLookupButtonListener {

		public void setNewValue(Object res) {
			final EOPlanComptableExer tmp = (EOPlanComptableExer) pcoMap.get(res);
			String s = (String) filters.get("pcoNumRejet");
			s = tmp.pcoNum();

			System.out.println("PcoLookupListener.setNewValue() = " + s);
			filters.put("pcoNumRejet", s);
			try {
				recoSepaSddRelevePanel.getPcoNumRejet().updateData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onTextHasChanged() {

		}

		public void onDbClick() {

		}

	}

	private class PcoLookupModelRejet implements IZLookupButtonModel {
		private final Collection pcosTmp;

		public PcoLookupModelRejet() {
			pcosTmp = pcoMap.keySet();
		}

		public Collection getDatas() {
			//            System.out.println("PcoLookupModel.getDatas()");
			//            ZLogger.debug(pcosTmp);
			return pcosTmp;
		}

		public String getActionDescription() {
			return "Sélectionner un compte";
		}

		public ImageIcon getIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
		}

		public Window getMyWindow() {
			return getMyDialog();
		}

		public String getTitle() {
			return "Sélectionnez un compte";
		}

		public Dimension getPreferredSize() {
			return PCOWINDOW_SIZE;
		}

	}

	private final class SepaSddEcheanceSelectListListener implements ISepaSddEcheanceSelectListListener {


		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public TableCellEditor getTableCellEditorForCheck() {
			return null;
		}

		public NSArray getEcheancesSelectionnees() {
			return echeancesRejetes;
		}

		public String getErreursForEcheance(ISepaSddEcheance echeance) {
			return null;
		}

		public void checkAll(boolean b) {
			checkAllEcheances(b);
			recoSepaSddRelevePanel.getSepaSddEcheanceSelectList().fireTableDataChanged();
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return null;
		}

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {
			return allEcheances;
		}

		public void onDbClick() {

		}

		public HashMap getAllCheckedPrelevements() {
			return checkedEcheances;
		}

		public void afterCheck() {
			refreshTotaux();
			refreshActions();
		}

		public boolean showColAnomalie() {
			return false;
		}

		public boolean showTotalPanel() {
			return false;
		}

	}

	private void checkAllEcheances(Boolean check) {
		for (int i = 0; i < allEcheances.count(); i++) {
			final EOSepaSddEcheance element = (EOSepaSddEcheance) allEcheances.objectAtIndex(i);
			checkedEcheances.put(element, Boolean.FALSE);
			if (check) {
				checkedEcheances.put(element, Boolean.TRUE);
			}
		}
	}

	public final class TraitementReleveThread extends Thread {

		public TraitementReleveThread() {
			super();
		}

		public void run() {
			msgFin = "";
			lastException = null;
			try {
				final EOComptabilite comptabilite = EOConvertUtil.fetchFrom(myApp.appUserInfo().getCurrentComptabilite());
				final EOJefyAdminExercice exercice = EOConvertUtil.fetchFrom(myApp.appUserInfo().getCurrentExercice());
				final EOJefyAdminUtilisateur utilisateur = EOConvertUtil.fetchFrom(myApp.appUserInfo().getUtilisateur());
				final NSTimestamp dateCreation = new NSTimestamp();
				final Map<ISepaSddEcheance.Etat, NSArray> echeancesRegroupeesParEtat = new HashMap<ISepaSddEcheance.Etat, NSArray>();
				//Rod : on ne traite pas les echeances confirmées (a priori pas besoin)
				//echeancesRegroupeesParEtat.put(ISepaSddEcheance.Etat.CONFIRME, echeancesConfirmes);
				echeancesRegroupeesParEtat.put(ISepaSddEcheance.Etat.REJETE, echeancesRejetes);
				
				Boolean genererEcritures = (Boolean) filters.get("genererEcritures");
				
				if (genererEcritures) {
    				checkGenerationEcrituresPossibles(echeancesRejetes);
				}
				
				NSDictionary res = ServerCallsepa.clientSideRequestSepaSddGenereEcrituresReleve(getEditingContext(), exercice, comptabilite, utilisateur, dateCreation, plancoDebitRejet, plancoDebitConfirme, echeancesRegroupeesParEtat, genererEcritures);

				NSArray ecrituresGenerees = CktlEOControlUtilities.faultsFromGlobalIds(getEditingContext(), (NSArray) res.valueForKey(ISepaSddService.ECRITURES_GENEREES_GIDS_KEY), EOEcriture.class);
				NSArray emargementsGeneres = CktlEOControlUtilities.faultsFromGlobalIds(getEditingContext(), (NSArray) res.valueForKey(ISepaSddService.EMARGEMENTS_GENEREES_GIDS_KEY), EOEmargement.class);
				String erreurs = (String) res.valueForKey(ISepaSddService.ERREURS_KEY);
				String msgs = (String) res.valueForKey(ISepaSddService.MSGS_KEY);
				
				String msg = "Traitement effectué\n";
				if (genererEcritures) {
					msg += "Nombre d'écriture(s) générée(s) : " + ecrituresGenerees.count() + "\n";
					msg += "Nombre d'émargement(s) généré(s) : " + emargementsGeneres.count() + "\n";
				} else {
					msg += "Aucune écriture n'a été générée suivant votre choix.\n";
				}

				if (!StringCtrl.isEmpty(msgs)) {
					msg += "\n" + msgs + "\n";
				}
				//Invalider les echeances
				CktlEOControlUtilities.invalidateObjects(allEcheances);
				waitingDialog.setVisible(false);
				//Si erreurs, les afficher
				if (!StringCtrl.isEmpty(erreurs)) {
					showWarningDialog(erreurs);
				}
				if (!StringCtrl.isEmpty(msg)) {
					showInfoDialog(msg);
				}

			} catch (Exception e) {
				getEditingContext().revert();
				e.printStackTrace();
				lastException = e;
			} finally {
				updateData();
				waitingDialog.setVisible(false);
			}

		}

        private void checkGenerationEcrituresPossibles(NSArray echeances) throws Exception {
            for (int idx = 0; idx < echeances.count(); idx++) {
                EOSepaSddEcheance echeance = (EOSepaSddEcheance) echeances.objectAtIndex(idx);
                if (!echeance.toSepaSddEcheancier().toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
                    throw new Exception("Action annulée car la génération des écritures n'est pas configurée pour toutes les échéances sélectionnées.");
                }
            }
        }

	}

	public void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	@Override
	public ZAbstractSwingPanel mainPanel() {
		return recoSepaSddRelevePanel;
	}

	public String title() {
		return TITLE;
	}

	private void refreshActions() {
		actionEmargerReleve.setEnabled(echeancesRejetes.count() > 0);

	}

}
