/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.sepasdd.ctrl;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import org.cocktail.fwkcktlcompta.client.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminTypeEtat;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.client.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlcompta.client.metier.EOPrelevementFichier;
import org.cocktail.fwkcktlcompta.client.metier.EORecouvrement;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddMandat;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigine;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddOrigineType;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam;
import org.cocktail.fwkcktlcompta.client.metier.finders.EOPlanComptableExerFinder;
import org.cocktail.fwkcktlcompta.client.sepasdd.helpers.SepaSddOrigineClientHelper;
import org.cocktail.fwkcktlcompta.common.entities.IGrhumRib;
import org.cocktail.fwkcktlcompta.common.exception.DataCheckException;
import org.cocktail.fwkcktlcompta.common.exception.DefaultClientException;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddParamHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.remises.SepaSddEcheancesCombinaison;
import org.cocktail.fwkcktlcompta.common.sepasdd.remises.SepaSddEcheancesRegroupementFactory;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheanceRule;
import org.cocktail.fwkcktlcompta.common.util.CktlArrayUtilities;
import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;
import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcompta.common.util.ZStringUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZIcon;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ctrl.CommonCtrl;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraStepPanel2;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ui.ZKarukeraStepPanel2.ZStepAction;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ZListUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files.DefaultFileListTablePanelMdl;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files.FileListTablePanel.IFileListTablePanelMdl;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.files.FileSaver;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.logging.ZLogger;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZAbstractSwingPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZHtmlUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZMsgPanel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTable;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.fwkcktlcomptaguiswing.client.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.EOConvertUtil;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.recouvrement.RecouvrementProxyCtrl;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStep0ParamSelection;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStep0ParamSelection.IRecoSepaSddStep0ParamSelectionListener;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStep1EcheancesSelection;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStep1EcheancesSelection.IRecoSepaSddStep1EcheancesSelectionListener;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStep2CombinaisonsEcheances;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStep2CombinaisonsEcheances.IRecoSepaSddStep2CombinaisonsEcheancesListener;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStepFinalRecouvrements;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.RecoSepaSddStepFinalRecouvrements.IRecoSepaSddStepFinalRecouvrementsListener;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ui.SepaSddEcheanceSelectList;
import org.cocktail.maracuja.client.remotecall.ServerCallsepa;
import org.joda.time.LocalDate;
import org.joda.time.ReadablePartial;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecoSepaSddCtrl extends CommonCtrl {
	public static final String ACTIONID = ZActionCtrl.IDU_PRELEV;
	public static final String PRELEV_DATE_PRELEVEMENT_MAX = "prelevDatePrelevementMax";
	public static final String PRELEV_DATE_PRELEVEMENT_MIN = "prelevDatePrelevementMin";

	private static final String STEP0 = "step0";
	/** Selection des elements */
	private static final String STEP1 = "step1";
	/** les combinaisons */
	private static final String STEP2 = "step2";
	private static final String STEP3 = "step3";

	private static final String TITLE = "Génération de remises de prélèvements SEPA SDD";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);
	private static final String TYPE_ORIGINE_TOUS_LABEL = "Tous";
	
	private final RecoSepaSddStep0ParamSelection step0ParamSelection;
	private final RecoSepaSddStep1EcheancesSelection step1EcheancesSelection;
	private final RecoSepaSddStep2CombinaisonsEcheances step2CombinaisonsEcheances;
	private final RecoSepaSddStepFinalRecouvrements stepFinalRecouvrements;

	private final RecoSepaSddStep0ParamSelectionListener step0ParamSelectionListener;
	private RecoSepaSddStep1EcheancesSelectionListener step1EcheancesSelectionListener;
	private RecoSepaSddStep2CombinaisonsEcheancesListener step2CombinaisonsEcheancesListener;
	private RecoSepaSddStepFinalRecouvrementsListener stepFinalRecouvrementsListener;

	private PrelevementFileListTablePanelMdl fileListTablePanelMdl = new PrelevementFileListTablePanelMdl();

	private JPanel cardPanel;
	private CardLayout cardLayout;
	private ZKarukeraStepPanel2 currentPanel;

	private final DefaultActionClose actionClose;

	private final HashMap<String, ZKarukeraStepPanel2> stepPanels = new HashMap<String, ZKarukeraStepPanel2>();

	/** jours ouvres de la BDF */
	@SuppressWarnings("rawtypes")
	private final NSArray calendrierBDF;

	protected Exception lastException = null;
	protected String msgFin = null;

	private final Map<EOPlanComptableExer, EOPlanComptableExer> comptesBE = new HashMap<EOPlanComptableExer, EOPlanComptableExer>();

	ZWaitingPanelDialog waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
	@SuppressWarnings("rawtypes")
	private Map<SepaSddEcheancesCombinaison, NSArray> echeancesRegroupements;
	@SuppressWarnings("rawtypes")
	private Map<SepaSddEcheancesCombinaison, Exception> recouvrementExceptions = new HashMap<SepaSddEcheancesCombinaison, Exception>();
	@SuppressWarnings("rawtypes")
	private NSArray echeancesCombinaisons;
	@SuppressWarnings("rawtypes")
	private NSArray sepaParams;
	@SuppressWarnings("rawtypes")
	private NSArray recouvrementGeneres = new NSArray();
	@SuppressWarnings("rawtypes")
	private final NSMutableArray prelevementFichierGeneres = new NSMutableArray();
	private ZEOComboBoxModel paramCompteModel;
	private ZEOComboBoxModel typesOrigineModel;
	
	private ApplicationClient myApp = (ApplicationClient) EOApplication.sharedApplication();
	private ActionImprimerBordereau actionImprimerBordereau = new ActionImprimerBordereau();
	private ActionEnregistrer actionEnregistrer = new ActionEnregistrer();
	private ActionImprimerContenuRecouvrement actionImprimerContenuRecouvrement = new ActionImprimerContenuRecouvrement();
	private Map<ISepaSddEcheance, String> erreursEcheances = new HashMap<ISepaSddEcheance, String>();


	/**
	 * @throws Exception
	 */
	public RecoSepaSddCtrl(EOEditingContext ec) throws Exception {
		super(ec);
		revertChanges();
		actionClose = new DefaultActionClose();
		actionClose.setEnabled(true);
		sepaParams = SepaSddParamHelper.getSharedInstance().getAllParamsValides(getEditingContext());
		if (sepaParams.count() == 0) {
			throw new Exception("Les paramètres SEPA ne sont pas remplis. Cf. table " + EOSepaSddParam.ENTITY_TABLE_NAME);
		}
		paramCompteModel = new ZEOComboBoxModel(sepaParams, ISepaSddParam.LIBELLE_KEY, null, null);
		
		initTypesOrigineModel();
		
		//Initialiser les comptes de BE
		@SuppressWarnings("rawtypes")
		//FIXME ameliorer ca en passant par le serveur (trop long)
		final NSArray pcos = EOPlanComptableExerFinder.getPlancoExerValidesWithCond(getEditingContext(), getExercice().getPrevEOExercice(), "pcoCompteBe<>nil", null, false);
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptableExer element = (EOPlanComptableExer) pcos.objectAtIndex(i);
			if (element.pcoCompteBe() != null) {
				final EOPlanComptableExer plancoBE = EOPlanComptableExerFinder.getPlancoExerForPcoNum(getEditingContext(), getExercice(), element.pcoCompteBe(), false);
				if (plancoBE == null) {
					throw new DefaultClientException("Erreur dans le plan comptable : Le compte de BE " + element.pcoCompteBe() + " (défini sur l'exercice " + getExercice().getPrevEOExercice().exeExercice() + ") pour le compte " + element.pcoNum() + " n'est pas valide pour l'exercice "
							+ getExercice().exeExercice() + ". Veuillez corriger cette erreur avant de faire un recouvrement.");
				}
				comptesBE.put(element, plancoBE);
			}
		}

		calendrierBDF = EOsFinder.getCalendrierBdfAutour(getEditingContext(), ZDateUtil.getDateOnly(ZDateUtil.now()));
		if (calendrierBDF.count() < 15) {
			showInfoDialog("Le calendrier de la Banque de France ne semble pas être complet. Il n'y aura peut-etre pas de contrôle possible pour les dates de réglement.");
		}

		step0ParamSelectionListener = new RecoSepaSddStep0ParamSelectionListener();
		step1EcheancesSelectionListener = new RecoSepaSddStep1EcheancesSelectionListener();
		step2CombinaisonsEcheancesListener = new RecoSepaSddStep2CombinaisonsEcheancesListener();
		stepFinalRecouvrementsListener = new RecoSepaSddStepFinalRecouvrementsListener();

		step0ParamSelection = new RecoSepaSddStep0ParamSelection(step0ParamSelectionListener);
		step1EcheancesSelection = new RecoSepaSddStep1EcheancesSelection(step1EcheancesSelectionListener);
		step2CombinaisonsEcheances = new RecoSepaSddStep2CombinaisonsEcheances(step2CombinaisonsEcheancesListener);
		stepFinalRecouvrements = new RecoSepaSddStepFinalRecouvrements(stepFinalRecouvrementsListener);

		stepPanels.put(STEP0, step0ParamSelection);
		stepPanels.put(STEP1, step1EcheancesSelection);
		stepPanels.put(STEP2, step2CombinaisonsEcheances);
		stepPanels.put(STEP3, stepFinalRecouvrements);

	}
	
	private void initTypesOrigineModel() {
	    NSArray typeOrigines = SepaSddOrigineClientHelper.getSharedInstance().fetchOrigineTypesValides(getEditingContext());
        typesOrigineModel = new ZEOComboBoxModel(typeOrigines, EOSepaSddOrigineType.TYPE_NOM_KEY, TYPE_ORIGINE_TOUS_LABEL, null);
	}

	private void initGUI() {
		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Iterator<String> iter = stepPanels.keySet().iterator();
		while (iter.hasNext()) {
			String element = iter.next();
			ZKarukeraStepPanel2 panel = (ZKarukeraStepPanel2) stepPanels.get(element);
			panel.setMyDialog(getMyDialog());
			panel.initGUI();
			cardPanel.add(panel, element);
		}

		changeStep(STEP0);
	}

	/**
	 * Ferme la fenetre.
	 */
	private final void close() {
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}



	private void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	/**
	 * Imprime le bordereau d'accompagnement
	 */
	private final void imprimerBordereau() {

		for (int i = 0; i < recouvrementGeneres.count(); i++) {
			EORecouvrement recouvrement = (EORecouvrement) recouvrementGeneres.objectAtIndex(i);
			RecouvrementProxyCtrl.imprimerBordereau(this, EOConvertUtil.fetchFrom(recouvrement), null);
		}
	}

	/**
	 * Imprime le contenu du paiement.
	 */
	private final void imprimerContenuRecouvrement() {
		for (int i = 0; i < recouvrementGeneres.count(); i++) {
			EORecouvrement recouvrement = (EORecouvrement) recouvrementGeneres.objectAtIndex(i);
			RecouvrementProxyCtrl.imprimerContenuPrelevement(this, EOConvertUtil.fetchFrom(recouvrement));
		}
	}

	private EOSepaSddParam getSelectedSepaSddParam() {
		return (EOSepaSddParam) paramCompteModel.getSelectedEObject();
	}

	private final void saveFiles() {
		try {
			List<File> files = CktlArrayUtilities.toList(fileListTablePanelMdl.getFiles());
			File dir = FileSaver.saveFiles(getMyDialog(), myApp.homeDir, files);
			//showInfoDialog("Les fichiers ont été enregistrés dans le répertoire " + dir.getAbsolutePath());
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Change le panel actif (en effectuant un updateData sur le nouveau).
	 * 
	 * @param newStep
	 */
	private final void changeStep(final String newStep) {
		ZLogger.debug(newStep);
		cardLayout.show(cardPanel, newStep);
		currentPanel = (ZKarukeraStepPanel2) stepPanels.get(newStep);
		try {
			currentPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prevSTEP1() {
		try {
			getEditingContext().revert();
			changeStep(STEP0);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prevSTEP2() {
		try {
			getEditingContext().revert();
			changeStep(STEP1);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void nextSTEP0() {
		try {

			changeStep(STEP1);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void nextSTEP1() {
		try {
			// verifier si prelevements selectionnés
			if (echeancesARecouvrer().count() > 0) {
				if (verifierEcheancesErreurs()) {
					return;
				}
				calculeRegroupements();
				changeStep(STEP2);
			}
			else {
				throw new DataCheckException("Veuillez cocher les échéances à prélever pour passer à l'étape suivante");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void calculeRegroupements() throws Exception {
		SepaSddEcheancesRegroupementFactory factory = new SepaSddEcheancesRegroupementFactory();
		getEditingContext().revert();
		echeancesRegroupements = factory.creerRegroupements(getEditingContext(), echeancesARecouvrer());
		echeancesCombinaisons = new NSArray(echeancesRegroupements.keySet().toArray());
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		initGUI();
		cardPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(cardPanel);
		win.pack();
		return win;
	}

	private class RecoSepaSddStep0ParamSelectionListener implements IRecoSepaSddStep0ParamSelectionListener {

		ZStepAction actionPrev = new DefaultActionPrev();
		private final ZStepAction actionSpecial1 = new DefaultActionPayer();

		private DefaultActionNext actionNext = new DefaultActionNext() {
			public void actionPerformed(ActionEvent e) {
				nextSTEP0();
			}
		};

		public RecoSepaSddStep0ParamSelectionListener() {
			actionNext.setEnabled(true);

		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		public ZEOComboBoxModel paramCompteModel() {
			return paramCompteModel;
		}

	}

	/**
	 * Selection des prelevements a effectuer.
	 */
	private class RecoSepaSddStep1EcheancesSelectionListener implements IRecoSepaSddStep1EcheancesSelectionListener {
		private final Boolean PRESELECTION_OBJET = Boolean.TRUE;
		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final ActionPrelevAttentePrint actionPrelevAttentePrint = new ActionPrelevAttentePrint();
		private final ActionSrch actionSrch = new ActionSrch();
		private final HashMap filters = new HashMap();
		private final HashMap<ISepaSddEcheance, Boolean> checkedPrelevements = new HashMap<ISepaSddEcheance, Boolean>();
		@SuppressWarnings("rawtypes")
		private NSMutableArray allPrelevements;
		private final SepaSddEcheanceSrchCellRenderer sepaSddEcheanceSrchCellRenderer = new SepaSddEcheanceSrchCellRenderer();
		private final CheckTableCellEditor checkTableCellEditor = new CheckTableCellEditor();

		public RecoSepaSddStep1EcheancesSelectionListener() {
			actionPrev = new DefaultActionPrev() {
				@Override
				public void actionPerformed(ActionEvent e) {
					prevSTEP1();
				}
			};
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionPrev.setEnabled(true);
			actionNext.setEnabled(true);
			//actionPrev.setEnabled(true);
			// updateData();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionPrelevAttentePrint extends AbstractAction {

			public ActionPrelevAttentePrint() {
				super();
				putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
				putValue(Action.NAME, "Imprimer");
				putValue(Action.SHORT_DESCRIPTION, "Imprimer la liste des prélèvements en attente");
				setEnabled(false);
			}

			public void actionPerformed(ActionEvent e) {
				prelevementsAttentePrint();
			}
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				nextSTEP1();

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep2PrelevementsSelectionListener.Step2Listener#getAllPrelevements()
		 */
		public NSArray getAllPrelevements() {
			ZLogger.verbose("getAllPrelevements()");
			ZLogger.verbose(allPrelevements);
			return allPrelevements;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep2PrelevementsSelectionListener.Step2Listener#getCheckedPrelevements()
		 */
		public HashMap getCheckedPrelevements() {
			return checkedPrelevements;
		}

		@SuppressWarnings("rawtypes")
		protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre, ZEOComboBoxModel typesOrigineModel) {
			NSMutableArray quals = new NSMutableArray();
			NSMutableArray qualsDate = new NSMutableArray();
			if (dicoFiltre.get(PRELEV_DATE_PRELEVEMENT_MIN) != null) {
				qualsDate.addObject(new EOKeyValueQualifier(EOSepaSddEcheance.D_PREVUE_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, DateConversionUtil.sharedInstance().formatDateWithoutTimeISO((ReadablePartial) dicoFiltre.get(PRELEV_DATE_PRELEVEMENT_MIN))));
			}
			
			if (dicoFiltre.get(PRELEV_DATE_PRELEVEMENT_MAX) != null) {
				qualsDate.addObject(new EOKeyValueQualifier(EOSepaSddEcheance.D_PREVUE_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, DateConversionUtil.sharedInstance().formatDateWithoutTimeISO((ReadablePartial) dicoFiltre.get(PRELEV_DATE_PRELEVEMENT_MAX))));
			}
			
			if (qualsDate.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsDate));
			}
	         
            if (typesOrigineModel.getSelectedEObject() != null) {
                quals.addObject(new EOKeyValueQualifier(
                    EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + 
                        EOSepaSddEcheancier.TO_SEPA_SDD_ORIGINE_KEY + "." + 
                        EOSepaSddOrigine.TO_SEPA_SDD_ORIGINE_TYPE_KEY,
                    EOQualifier.QualifierOperatorEqual, 
                    typesOrigineModel.getSelectedEObject()));
            }
			
			return quals;
		}

		@SuppressWarnings({
				"rawtypes", "unchecked"
		})
		public final void updateData() {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			try {
			checkedPrelevements.clear();

			NSMutableArray res = new NSMutableArray();

			final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey(EOSepaSddEcheance.D_PREVUE_KEY, EOSortOrdering.CompareAscending);
			final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY + "." + EOSepaSddMandat.NUMERO_KEY, EOSortOrdering.CompareAscending);

			NSMutableArray andQuals = new NSMutableArray();

			andQuals.addObjectsFromArray(buildFilterQualifiers(filters, typesOrigineModel));
			andQuals.addObject(new EOKeyValueQualifier(EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY + "." + EOSepaSddMandat.TO_TYPE_ETAT_KEY + "." + EOJefyAdminTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual,
					EOJefyAdminTypeEtat.ETAT_VALIDE));
			andQuals.addObject(EOSepaSddEcheance.QUAL_ETAT_ATTENTE);
			andQuals.addObject(new EOKeyValueQualifier(EOSepaSddEcheance.TO_SEPA_SDD_ECHEANCIER_KEY + "." + EOSepaSddEcheancier.TO_SEPA_SDD_MANDAT_KEY + "." + EOSepaSddMandat.TO_SEPA_SDD_PARAM_KEY, EOQualifier.QualifierOperatorEqual, getSelectedSepaSddParam()));
			NSMutableArray sorts = new NSMutableArray();
			sorts.addObject(sort0);
			sorts.addObject(sort1);
			res.addObjectsFromArray(EOSepaSddEcheance.fetchAll(getEditingContext(), new EOAndQualifier(andQuals), null, true));

			allPrelevements = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, sorts).mutableClone();
				for (int i = 0; i < allPrelevements.count(); i++) {
					final EOSepaSddEcheance element = (EOSepaSddEcheance) allPrelevements.objectAtIndex(i);
					//TODO a supprimer si ca ne regle pas les pbs de refresh
					element.editingContext().refaultObject(element);
					erreursEcheances.put(element, buildErreursForEcheance(element));
				}

			checkAllEcheances(PRESELECTION_OBJET);
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		private void checkAllEcheances(Boolean check) throws Exception {
			for (int i = 0; i < allPrelevements.count(); i++) {
				final EOSepaSddEcheance element = (EOSepaSddEcheance) allPrelevements.objectAtIndex(i);
				checkedPrelevements.put(element, Boolean.FALSE);
				if (check) {
					if (StringCtrl.isEmpty(erreursEcheances.get(element))) {
						checkedPrelevements.put(element, Boolean.TRUE);
					}
				}
			}
		}

		private final void onSrch() {
			try {
				setWaitCursor(true);
				updateData();
				step1EcheancesSelection.updateData();
				refreshActions();
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		private final void prelevementsAttentePrint() {
			if (getEditingContext().hasChanges()) {
				showWarningDialog("Attention : si vous avez supprimé des prélèvements en attente sur cet écran, ils apparaitront quand même sur l'édition.");
			}
			try {
				RecouvrementProxyCtrl.imprimerSepaPrelevementsAttente(myApp, filters, getMyDialog());
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		private final void refreshActions() {
			actionPrelevAttentePrint.setEnabled(getAllPrelevements().count() > 0);
		}

		private final class ActionSrch extends AbstractAction {
			public ActionSrch() {
				super();
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
			}

			/**
			 * Appelle updateData();
			 */
			public void actionPerformed(ActionEvent e) {
				onSrch();
			}
		}

		private final class CheckTableCellEditor extends DefaultCellEditor {

			public CheckTableCellEditor() {
				super(new JCheckBox());
			}

			@Override
			public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
				Component res = super.getTableCellEditorComponent(table, value, isSelected, row, column);
				final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
				final EOSepaSddEcheance obj = (EOSepaSddEcheance) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
				if (erreursEcheances.get(obj) != null && erreursEcheances.get(obj).length() > 0) {
					res.setEnabled(false);
				}
				else {
					res.setEnabled(true);
				}

				return res;
			}

		}

		private final class SepaSddEcheanceSrchCellRenderer extends ZEOTableCellRenderer {

			private static final long serialVersionUID = 1L;

			public SepaSddEcheanceSrchCellRenderer() {

			}

			public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
				Component res = null;
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				final ZEOTableModelColumn column2 = ((ZEOTable) table).getDataModel().getColumn(table.convertColumnIndexToModel(column));

				res.setEnabled(true);
				if (isSelected) {
					res.setBackground(table.getSelectionBackground());
					res.setForeground(table.getSelectionForeground());
				}
				else {
					res.setBackground(table.getBackground());
					res.setForeground(table.getForeground());
				}

				final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
				final EOSepaSddEcheance obj = (EOSepaSddEcheance) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
				
				if (!ZStringUtil.isEmpty(erreursEcheances.get(obj))) {
					//res.setBackground(ZConst.BGCOL_DESEQUILIBRE);
				    if (SepaSddEcheanceSelectList.SELECTION_COLUMN_TITLE.equals(column2.getTitle())) {
						res.setEnabled(false);
					}
					else {
						res.setEnabled(true);
					}
				}
				if (ISepaSddEcheance.Etat.ANNULE.equals(obj.etat())) {
					((JLabel) res).setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + ((JLabel) res).getText() + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX);
				}
				return res;
			}
		}

		public Action getActionSrch() {
			return actionSrch;
		}

		public HashMap getFilters() {
			return filters;
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return sepaSddEcheanceSrchCellRenderer;
		}

		public void selectionChanged() {
			refreshActions();

		}

		public void afterCheck() {
			step1EcheancesSelection.getSepaSddEcheanceSelectList().getPanelTotal1().updateData();
		}

		public Action getPrelevAttentePrint() {
			return actionPrelevAttentePrint;
		}

		public NSArray getEcheancesARecouvrer() {
			return echeancesARecouvrer();
		}


		private String buildErreursForEcheance(ISepaSddEcheance echeance) throws Exception {
			String erreurs = "";
			try {
				SepaSddEcheanceRule.getSharedInstance().checkEcheancePeutEtrePrelevee(echeance);
			} catch (Exception e) {
				erreurs += e.getMessage();
			}
			return erreurs;
		}

		public String getErreursForEcheance(ISepaSddEcheance echeance) {
			return erreursEcheances.get(echeance);
		}

		public void checkAll(boolean b) {
			try {
			checkAllEcheances(b);
			step1EcheancesSelection.getSepaSddEcheanceSelectList().fireTableDataChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		public TableCellEditor getTableCellEditorForCheck() {
			return new CheckTableCellEditor();
		}

        public ComboBoxModel getTypesOrigineModel() {
            return typesOrigineModel;
        }
	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionPayer extends ZStepAction {
		private static final long serialVersionUID = 1L;

		public DefaultActionPayer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Générer");
			putValue(Action.SHORT_DESCRIPTION, "Générer les fichiers");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			doProcess();
		}

	}

	private class DefaultActionNext extends ZStepAction {
		private static final long serialVersionUID = 1L;

		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le Bordereau d'accompagnement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.NAME, "Enregistrer le(s) fichier(s)");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			saveFiles();

		}
	}

	public class ActionImprimerContenuRecouvrement extends AbstractAction {
		public ActionImprimerContenuRecouvrement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail du paiement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerContenuRecouvrement();

		}
	}

	private NSArray sepaParams() {
		return sepaParams;

	}

	/**
	 * @return Les prelevements sélectionnes (et non supprimes) par l'utilisateur.
	 */
	private final NSArray echeancesARecouvrer() {
		final NSArray tmp = new NSArray(ZListUtil.getKeyListForValue(step1EcheancesSelectionListener.getCheckedPrelevements(), Boolean.TRUE).toArray());
		//On filtre sur les prelevements reellement en attente
		return EOQualifier.filteredArrayWithQualifier(tmp, EOSepaSddEcheance.QUAL_ETAT_ATTENTE);
	}

	public NSArray openDialog(final Window dial, LocalDate dateMax) {
		final ZKarukeraDialog win;
		NSArray res = null;
		win = createModalDialog(dial);
		step1EcheancesSelectionListener.getFilters().put(PRELEV_DATE_PRELEVEMENT_MAX, dateMax);
		try {
			currentPanel.updateData();
			win.open();
			res = recouvrementGeneres;
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	/**
	 * Vérifie d'éventuelles erreurs avant le recouvrement. - Dans le cas d'un prelevement, on vérifie si le rib est défini, s'il est cohérent avec la
	 * facture et affiche un warning si le rib n'est pas valide.
	 * 
	 * @throws Exception
	 */
	private final boolean verifierEcheancesErreurs() throws Exception {
		@SuppressWarnings("rawtypes")
		NSArray mds = echeancesARecouvrer();
		boolean res = false;
		for (int i = 0; i < mds.count(); i++) {
			EOSepaSddEcheance element = (EOSepaSddEcheance) mds.objectAtIndex(i);
			if (IGrhumRib.RIB_ANNULE.equals(element.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().ribValide())) {
				if (!showConfirmationDialog("Attention", "Le Rib affecté à l'échéance du " + ZConst.DATEISO_FORMAT_DATESHORT.format(element.datePrevue()) + " pour " + element.toSepaSddEcheancier().toSepaSddMandat().toDebiteurPersonne().getNomAndPrenom() + " ("
						+ element.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().getRibCompletIntl() + ") "
						+ "n'est pas valide, voulez-vous quand même continuer le prélèvement (avec ce rib invalide) ?", ZMsgPanel.BTLABEL_NO)) {
					return true;
				}
			}

			if (element.toSepaSddEcheancier().toSepaSddMandat().toDebiteurRib().ribTitco() == null) {
				throw new DefaultClientException("Le Rib affecté à l'échéance du " + ZConst.DATEISO_FORMAT_DATESHORT.format(element.datePrevue()) + " pour " + element.toSepaSddEcheancier().toSepaSddMandat().toDebiteurPersonne().getNomAndPrenom()
						+ " n'a pas de titulaire défini, veuillez corriger ce problème avant de générer le fichier de prélèvements.");
			}
		}
		return res;
	}

	/**
	 * Thread de traitement
	 */
	public final class RecouvrementThread extends Thread {
		public RecouvrementThread() {
			super();
		}

		public void run() {
			msgFin = null;
			lastException = null;
			try {
				prelevementFichierGeneres.removeAllObjects();
				fileListTablePanelMdl.clear();
				recouvrementExceptions.clear();

				if (getSelectedSepaSddParam() != null) {
					final EOComptabilite comptabilite = EOConvertUtil.fetchFrom(myApp.appUserInfo().getCurrentComptabilite());
					final EOJefyAdminExercice exercice = EOConvertUtil.fetchFrom(myApp.appUserInfo().getCurrentExercice());
					final EOJefyAdminUtilisateur utilisateur = EOConvertUtil.fetchFrom(myApp.appUserInfo().getUtilisateur());
					final NSTimestamp dateCreation = new NSTimestamp();

					//appeler generation sur le serveur
					NSDictionary recouvrementsDico = ServerCallsepa.clientSideRequestGenerePrelevementsSepa(getEditingContext(), exercice, comptabilite, utilisateur, dateCreation, getSelectedSepaSddParam(), echeancesRegroupements);
					recouvrementGeneres = CktlEOControlUtilities.faultsFromGlobalIds(getEditingContext(), (NSArray) recouvrementsDico.valueForKey("recouvrementsGeneresGids"), EORecouvrement.class);
					String msg = (String) recouvrementsDico.valueForKey("erreurs");

					//balayer les prelevements generes, creer les fichiers cote client
					for (int i = 0; i < recouvrementGeneres.count(); i++) {
						EORecouvrement recouvrement = (EORecouvrement) recouvrementGeneres.objectAtIndex(i);
						EOPrelevementFichier prelevementFichier = (EOPrelevementFichier) recouvrement.toPrelevementFichiers().objectAtIndex(0);
						//Enregistrement du fichier dans un rep temporaire
						File f = RecouvrementProxyCtrl.enregistrerFichier(prelevementFichier, myApp.getTemporaryDir(), null);
						fileListTablePanelMdl.addFile(f, prelevementFichier);

					}

					//afficher les exceptions eventuellement générées coté serveur
					if (msg != null) {
						throw new DefaultClientException(msg);
					}

				}
			} catch (Exception e) {
				getEditingContext().revert();
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractSwingPanel mainPanel() {
		return currentPanel;
	}

	public String title() {
		return TITLE;
	}

	private class RecoSepaSddStep2CombinaisonsEcheancesListener implements IRecoSepaSddStep2CombinaisonsEcheancesListener {

		private DefaultActionPrev actionPrev;
		private DefaultActionNext actionNext;
		private DefaultActionPayer actionSpecial1;

		public RecoSepaSddStep2CombinaisonsEcheancesListener() {
			actionSpecial1 = new DefaultActionPayer();
			actionNext = new DefaultActionNext();
			actionPrev = new DefaultActionPrev() {
				@Override
				public void actionPerformed(ActionEvent e) {
					prevSTEP2();
				}
			};

			actionSpecial1.setEnabled(true);
			actionPrev.setEnabled(true);
			actionNext.setEnabled(false);
		}

		public NSArray getEcheances() {
			SepaSddEcheancesCombinaison combinaison = (SepaSddEcheancesCombinaison) step2CombinaisonsEcheances.getCombinaisonList().selectedObject();

			if (combinaison == null) {
				return NSArray.EmptyArray;
			}
			return echeancesRegroupements.get(combinaison);
		}

		public NSArray getEcheanceCombinaisons() {
			return echeancesCombinaisons;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		public void combinaisonHasChanged() {
			try {
				step2CombinaisonsEcheances.getEcheanceSelectionneesList().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

	}

	private void doProcess() {
		try {
			//si modifs en attente, on enregistre
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
		} catch (Exception e) {
			showErrorDialog(e);
			return;
		}

		if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement effectuer le recouvrement ? \nCette opération est irréversible.", ZMsgPanel.BTLABEL_NO)) {
			waitingDialog.setTitle("Traitement du recouvrement");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setModal(true);

			updateLoadingMsg("Ce traitement peut être long...");
			try {

				final RecouvrementThread thread = new RecouvrementThread();
				thread.start();

				if (thread.isAlive()) {
					waitingDialog.setVisible(true);
				}

				ZLogger.debug("Recouvrement termine");

				if (lastException != null) {
					throw lastException;
				}

				if (msgFin != null) {
					msgFin = msgFin.replaceAll("\n", "<br>");
					showInfoDialog(msgFin);
				}

				// /retour
				if (lastException != null) {
					throw lastException;
				}

				changeStep(STEP3);

			} catch (Exception e) {
				showErrorDialog(e);
				getEditingContext().revert();
				close();
			} finally {
				waitingDialog.setVisible(false);
				if (waitingDialog != null) {
					waitingDialog.dispose();
				}
			}

		}
	}

	private final class RecoSepaSddStepFinalRecouvrementsListener implements IRecoSepaSddStepFinalRecouvrementsListener {

		public ZStepAction actionPrev() {
			return null;
		}

		public ZStepAction actionNext() {
			return null;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		public Action actionImprimerContenuFichiersPrelevement() {
			return actionImprimerContenuRecouvrement;
		}

		public Action actionImprimerBordereaux() {
			return actionImprimerBordereau;
		}

		public Action actionEnregistrerFichiers() {
			return actionEnregistrer;
		}

		public IFileListTablePanelMdl getFileListTableModel() {
			return fileListTablePanelMdl;
		}

		public void refreshActions() {
			boolean hasfiles = fileListTablePanelMdl.getFiles().count() > 0;
			actionEnregistrer.setEnabled(hasfiles);
			actionImprimerBordereau.setEnabled(hasfiles);
			actionImprimerContenuRecouvrement.setEnabled(hasfiles);
		}

	}

	private final class PrelevementFileListTablePanelMdl extends DefaultFileListTablePanelMdl {

		public void addFile(File file, EOPrelevementFichier prelevementFichier) {
			NSMutableDictionary dic = super.addFile(file);
			dic.takeValueForKey(prelevementFichier.toRecouvrement(), EOPrelevementFichier.TO_RECOUVREMENT_KEY);
			dic.takeValueForKey(prelevementFichier.ficpCodeOp(), EOPrelevementFichier.FICP_CODE_OP_KEY);
			dic.takeValueForKey(prelevementFichier.ficpNbPrelevements(), EOPrelevementFichier.FICP_NB_PRELEVEMENTS_KEY);
			dic.takeValueForKey(prelevementFichier.ficpDateRemise(), EOPrelevementFichier.FICP_DATE_REMISE_KEY);
			dic.takeValueForKey(prelevementFichier.ficpDateReglement(), EOPrelevementFichier.FICP_DATE_REGLEMENT_KEY);
			dic.takeValueForKey(prelevementFichier.ficpMontant(), EOPrelevementFichier.FICP_MONTANT_KEY);
			getFiles().addObject(dic);
			sort();
		}

	}


}
