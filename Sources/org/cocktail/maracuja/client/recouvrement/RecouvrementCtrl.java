/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryPrelevementFichier;
import org.cocktail.maracuja.client.factory.FactoryRecouvrement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessEcrituresPrelevements;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPrelevementFichiersBdf;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcheancier;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EOPrelevementParamBdf;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep1TypeRecouvrement;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep2PrelevementsSelection;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep3PrelevementsSelectionnes;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep7OptionsFichier;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep8Recapitulatif;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZListUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementCtrl extends CommonCtrl {
	public static final boolean RETENUE_ACTIVE = true;
	public static final String ACTIONID = ZActionCtrl.IDU_PAGE;

	/** Choix de la comptabilite + domaine de paiement */
	private static final String STEP1 = "step1";
	/** Selection des elements */
	private static final String STEP2 = "step2";
	/** les prelevements */
	private static final String STEP3 = "step3";

	/** Options de génération du fichier */
	private static final String STEP7 = "step7";
	/** Recap apres enregistrement */
	private static final String STEP8 = "step8";

	private static final String TITLE = "Génération d'un recouvrement";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);

	private final RecouvrementStep1TypeRecouvrement step1Panel;
	private final RecouvrementStep2PrelevementsSelection step2Panel;
	private final RecouvrementStep3PrelevementsSelectionnes step3Panel;
	private final RecouvrementStep7OptionsFichier step7Panel;
	private final RecouvrementStep8Recapitulatif step8Panel;

	private final RecouvrementPanelStep1Listener step1Listener;
	private final RecouvrementPanelStep2Listener step2Listener;
	private final RecouvrementPanelStep3Listener step3Listener;
	private final RecouvrementPanelStep7Listener step7Listener;
	private final RecouvrementPanelStep8Listener step8Listener;

	private JPanel cardPanel;
	private CardLayout cardLayout;
	private ZKarukeraStepPanel2 currentPanel;

	private final DefaultActionClose actionClose;
	private final ZEOComboBoxModel myComptabiliteModel;

	private final HashMap<String, ZKarukeraStepPanel2> stepPanels = new HashMap<String, ZKarukeraStepPanel2>();

	/** jours ouvres de la BDF */
	@SuppressWarnings("rawtypes")
	private final NSArray calendrierBDF;

	private File defaultFile;

	/**
	 * Les comptabilites autorisees.
	 */
	private final NSArray comptabilites;

	private EORecouvrement currentRecouvrement;
	private EOModeRecouvrement currentModeRecouvrement;

	private String leContenuFichier;

	private Date dateValeur;
	private Date dateOperation;

	private int nbOperations = -1;

	//    private File

	protected Exception lastException = null;
	protected String msgFin = null;

	private final Map comptesBE = new HashMap();

	ZWaitingPanelDialog waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));

	/**
	 * @throws Exception
	 */
	public RecouvrementCtrl(EOEditingContext ec) throws Exception {
		super(ec);
		revertChanges();
		actionClose = new DefaultActionClose();
		actionClose.setEnabled(true);
		comptabilites = EOsFinder.getAllComptabilites(getEditingContext());
		myComptabiliteModel = new ZEOComboBoxModel(comptabilites, "comLibelle", null, null);

		checkModesRecouvrements();

		//Initialiser les comptes de BE
		final NSArray pcos = EOPlanComptableExerFinder.getPlancoExerValidesWithCond(getEditingContext(), getExercice().getPrevEOExercice(), "pcoCompteBe<>nil", null, false);
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptableExer element = (EOPlanComptableExer) pcos.objectAtIndex(i);
			if (element.pcoCompteBe() != null) {
				final EOPlanComptableExer plancoBE = EOPlanComptableExerFinder.getPlancoExerForPcoNum(getEditingContext(), getExercice(), element.pcoCompteBe(), false);
				if (plancoBE == null) {
					throw new DefaultClientException("Erreur dans le plan comptable : Le compte de BE " + element.pcoCompteBe() + " (défini sur l'exercice " + getExercice().getPrevEOExercice().exeExercice() + ") pour le compte " + element.pcoNum() + " n'est pas valide pour l'exercice "
							+ getExercice().exeExercice() + ". Veuillez corriger cette erreur avant de faire un recouvrement.");
				}
				comptesBE.put(element, plancoBE.planComptable());
			}
		}

		calendrierBDF = EOsFinder.getCalendrierBdfAutour(getEditingContext(), ZDateUtil.getDateOnly(ZDateUtil.now()));
		if (calendrierBDF.count() < 15) {
			showInfoDialog("Le calendrier de la Banque de France ne semble pas être complet. Il n'y aura peut-etre pas de contrôle possible pour les dates d'échange et de réglement.");
		}

		ZLogger.debug(comptesBE);

		// creer les listeners
		step1Listener = new RecouvrementPanelStep1Listener();
		step2Listener = new RecouvrementPanelStep2Listener();
		step3Listener = new RecouvrementPanelStep3Listener();
		step7Listener = new RecouvrementPanelStep7Listener();
		step8Listener = new RecouvrementPanelStep8Listener();

		step1Panel = new RecouvrementStep1TypeRecouvrement(step1Listener);
		step2Panel = new RecouvrementStep2PrelevementsSelection(step2Listener);
		step3Panel = new RecouvrementStep3PrelevementsSelectionnes(step3Listener);
		step7Panel = new RecouvrementStep7OptionsFichier(step7Listener);
		step8Panel = new RecouvrementStep8Recapitulatif(step8Listener);

		stepPanels.put(STEP1, step1Panel);
		stepPanels.put(STEP2, step2Panel);
		stepPanels.put(STEP3, step3Panel);
		stepPanels.put(STEP7, step7Panel);
		stepPanels.put(STEP8, step8Panel);

	}

	/**
	 * Determine la date d'operation en fonction de la date d'echange, du nombre de jours de delai et du calendrier de la BDF. Renvoi null si une des
	 * deux dates n'est pas trouvée
	 *
	 * @param dateEchange
	 * @param nbJoursDelai
	 * @return
	 */
	private final Date determinerDateOperation(final Date dateEchange, final int nbJoursDelai) {
		ZLogger.debug("determination de la date +" + nbJoursDelai + " , " + dateEchange);
		final int i = calendrierBDF.indexOfObject(dateEchange);
		final int j = i + nbJoursDelai;
		if (i != NSArray.NotFound && j <= calendrierBDF.count() - 1) {
			return (Date) calendrierBDF.objectAtIndex(j);
		}
		return null;
	}

	private final boolean isDateDansCalendrierBdf(final Date dateEchange) {
		return (calendrierBDF.indexOfObject(dateEchange) != NSArray.NotFound);

	}

	private void initGUI() {

		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Iterator iter = stepPanels.keySet().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			ZKarukeraStepPanel2 panel = (ZKarukeraStepPanel2) stepPanels.get(element);
			panel.setMyDialog(getMyDialog());
			panel.initGUI();
			cardPanel.add(panel, element);
		}

		changeStep(STEP1);
	}

	/**
	 * Ferme la fenetre.
	 */
	private final void close() {
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}

	/**
	 * Vérifier si les modes de recouvrement sont bien paramétrés.
	 *
	 * @throws Exception
	 */
	private final void checkModesRecouvrements() throws Exception {
		NSArray res = EOsFinder.getModeRecouvrementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
		res = EOQualifier.filteredArrayWithQualifier(res, EOQualifier.qualifierWithQualifierFormat("modDom=%@", new NSArray(new Object[] {
				EOModeRecouvrement.MODDOM_ECHEANCIER
		})));

		//On ne doit avoir qu'un et un seul mode de recouvrement ECHEANCIER
		//on teste avec plusieurs
		//        if( res.count() > 1) {
		//            throw new DefaultClientException("Il y a plusieurs modes de recouvremets pour le domaine "  + EOModeRecouvrement.MODDOM_ECHEANCIER +". Ceci est anormal.");
		//        }
		if (res.count() == 0) {
			throw new DefaultClientException("Il n'y a pas de mode de recouvrement pour le domaine " + EOModeRecouvrement.MODDOM_ECHEANCIER + ". Veuillez en crééer un.");
		}

		EOModeRecouvrement element = (EOModeRecouvrement) res.objectAtIndex(0);
		if (element.planComptablePaiement() == null || element.planComptableVisa() == null) {
			throw new DefaultClientException("Le mode de recouvrement " + element.modCode() + " (" + element.modLibelle() + ")" + " n'a pas d'imputation visa ou recouvrement associée, il sera impossible de générer les écritures, veuillez corriger ce problème avant d'accéder aux paiements.");
		}

	}

	public void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	private final void payer() {

		try {
			if (dateValeur == null) {
				throw new DataCheckException("Vous devez indiquer une date d'échange.");
			}
			if (dateOperation == null) {
				throw new DataCheckException("Vous devez indiquer une date de prélèvement (date opération).");
			}

			//vérifier si les dates sont valides
			if (!isDateDansCalendrierBdf(dateValeur)) {
				if (!showConfirmationDialog("Confirmation", "La date d'échange que vous avez indiqué n'a pas été trouvée dans le calendrier de la BDF, souhaitez-vous quand même continuer ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}
			if (!isDateDansCalendrierBdf(dateOperation)) {
				if (!showConfirmationDialog("Confirmation", "La date de réglement que vous avez indiqué n'a pas été trouvée dans le calendrier de la BDF, souhaitez-vous quand même continuer ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}
			if (!dateOperation.equals(determinerDateOperation(dateValeur, step7Listener.getNbJoursDelai()))) {
				if (!showConfirmationDialog("Confirmation", "La date de réglement que vous avez indiqué n'est pas cohérente avec la date d'échange et la vitesse de prélèvement, souhaitez-vous quand même continuer ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}

		} catch (Exception e) {
			showErrorDialog(e);
			return;
		}

		if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement lancer la génération des prélèvements ? \nCette opération est irréversible.", ZMsgPanel.BTLABEL_NO)) {

			step7Listener.actionSpecial1().setEnabled(false);
			waitingDialog.setTitle("Traitement du recouvrement");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setModal(true);
			lastException = null;
			updateLoadingMsg("Ce traitement peut être long...");
			try {

				final RecouvrementPayerThread thread = new RecouvrementPayerThread();
				thread.start();

				if (thread.isAlive()) {
					waitingDialog.setVisible(true);
				}
				if (lastException != null) {
					throw lastException;
				}

				ZLogger.debug("Recouvrement termine");

				if (EOModeRecouvrement.MODDOM_ECHEANCIER.equals(step1Panel.getSelectedDomaine())) {
					prepareDefaultFile(currentRecouvrement);
				}

				if (msgFin != null) {
					showInfoDialog(msgFin);
				}

				//
				//                // /retour
				//                if (lastException != null) {
				//                    throw lastException;
				//                }

				if (EOModeRecouvrement.MODDOM_ECHEANCIER.equals(step1Panel.getSelectedDomaine())) {
					changeStep(STEP8);
				}

			} catch (Exception e) {
				showErrorDialog(e);
				getEditingContext().revert();
			} finally {
				waitingDialog.setVisible(false);
				if (waitingDialog != null) {
					waitingDialog.dispose();
				}
			}

		}
	}

	/**
	 * Imprime le bordereau d'accompagnement
	 */
	public final void imprimerBordereau() {
		RecouvrementProxyCtrl.imprimerBordereau(this, currentRecouvrement, getSelectedPrelevementParamBdf());
	}

	/**
	 * Imprime le contenu du paiement.
	 */
	public final void imprimerContenuRecouvrement() {
		RecouvrementProxyCtrl.imprimerContenuPrelevement(this, currentRecouvrement);
	}

	public final void prepareDefaultFile(final EORecouvrement virement) throws Exception {
		final String defaultFileName = "Fichier_" + getSelectedFormat() + "PREL" + "_" + ZStringUtil.extendWithChars("" + virement.recoNumero(), "0", 10, true) + ".txt";
		defaultFile = saveFile(leContenuFichier, defaultFileName);
	}

	/**
	 * @return renvoi le virementParam sélectionné (il faut le caster ensuite)
	 */
	public final EOEnterpriseObject getSelectedPrelevementParam() {
		final EOEnterpriseObject selected = (EOEnterpriseObject) step7Listener.getFormatsVirementModel().getSelectedEObject();
		return selected;
	}

	/**
	 * @return
	 */
	public final EOPrelevementParamBdf getSelectedPrelevementParamBdf() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeRecouvrement.TREC_FORMAT_BDF.equals(format)) {
			return (EOPrelevementParamBdf) getSelectedPrelevementParam();
		}
		return null;
	}

	public final EOTypeRecouvrement getSelectedTypeRecouvrement() {
		final EOEnterpriseObject selected = getSelectedPrelevementParam();
		if (selected == null) {
			return null;
		}
		return (EOTypeRecouvrement) selected.valueForKey("typeRecouvrement");
	}

	public final String getSelectedFormat() {
		final EOTypeRecouvrement tv = getSelectedTypeRecouvrement();
		if (tv == null) {
			return null;
		}
		return tv.trecFormat();
	}

	public final File saveFile(final String contenu, final String fileName) throws Exception {
		if (contenu == null || contenu.length() == 0) {
			throw new DefaultClientException("Le contenu du fichier est vide !");
		}
		final File f = new File(new File(myApp.temporaryDir + fileName).getCanonicalPath());
		final FileWriter writer = new FileWriter(f);
		writer.write(contenu);
		writer.close();
		return f;
	}

	/**
	 * Enregistre le fichier
	 */
	public final void enregistrerFichier(final EORecouvrement recouvrement) {
		try {
			RecouvrementProxyCtrl.enregistrerFichier(this, leContenuFichier, recouvrement);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Change le panel actif (en effectuant un updateData sur le nouveau).
	 *
	 * @param newStep
	 */
	private final void changeStep(final String newStep) {
		ZLogger.debug(newStep);
		cardLayout.show(cardPanel, newStep);
		currentPanel = (ZKarukeraStepPanel2) stepPanels.get(newStep);
		try {
			currentPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP2() {
		try {
			getEditingContext().revert();
			changeStep(STEP1);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP3() {
		try {
			changeStep(STEP2);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP4() {
		try {
			// verifier si prelevements selectionnés
			if (prelevementsARecouvrer().count() > 0) {
				changeStep(STEP3);
			}
			else {
				// si pas de prelevement sélectionnés on passe au 2
				prev_STEP3();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP7() {
		try {
			changeStep(STEP3);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Apres la selection du type de paiement.
	 */
	private final void next_STEP1() {
		try {
			ZLogger.debug(STEP2);
			step1Listener.domaineSelectionne = step1Panel.getSelectedDomaine();
			//TODO pb pour les echeanciers de la scol, le mode de recouvrement est pris par defaut
			NSArray res = EOsFinder.getModeRecouvrementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
			res = EOQualifier.filteredArrayWithQualifier(res, EOQualifier.qualifierWithQualifierFormat("modDom=%@", new NSArray(new Object[] {
					EOModeRecouvrement.MODDOM_ECHEANCIER
			})));
			currentModeRecouvrement = (EOModeRecouvrement) res.objectAtIndex(0);

			step2Listener.updateData();
			changeStep(STEP2);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Apres la selection des prelevements, titres et odps...
	 */
	private final void next_STEP2() {
		try {
			ZLogger.debug(STEP3);

			// verifier si prelevements selectionnés
			if (prelevementsARecouvrer().count() > 0) {

				if (verifierPrelevementsErreurs()) {
					return;
				}
				changeStep(STEP3);
				//                changeStep(STEP3);
			}
			else {
				throw new DataCheckException("Veuillez cocher les prélèvements à effectuer pour passer à l'étape suivante");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP3() {
		try {
			// verifier si titres selectionnés
			if (prelevementsARecouvrer().count() > 0) {
				changeStep(STEP7);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP7() {
		try {
			showinfoDialogFonctionNonImplementee();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP8() {
		try {
			showinfoDialogFonctionNonImplementee();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		// ZKarukeraDialog win = new ZKarukeraDialog(dial, TITLE,true);
		setMyDialog(win);
		initGUI();
		// step1Panel.setMyDialog(win);
		cardPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(cardPanel);
		win.pack();
		return win;
	}

	// //////////////////////////////////////////////////////////////////////////////

	private class RecouvrementPanelStep1Listener implements RecouvrementStep1TypeRecouvrement.Step1Listener {
		/** Domaine sélectionné par l'utilisateur */
		private String domaineSelectionne;

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final HashMap numOfObjByDomaine = new HashMap(3);

		/**
         *
         */
		public RecouvrementPanelStep1Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(true);
			domaineSelectionne = EOModeRecouvrement.MODDOM_ECHEANCIER;

			// Compter les objets par domaine de paiement
			HashMap objs = new HashMap(3);
			objs.put(PRELEVEMENTS, new Integer(getPrelevementsByDomaineRecouvrement(EOModeRecouvrement.MODDOM_ECHEANCIER).count()));
			numOfObjByDomaine.put(EOModeRecouvrement.MODDOM_ECHEANCIER, objs);

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP1();
			}

		}

		public String getSelectedDomaine() {
			return domaineSelectionne;
		}

		public HashMap getNumOfObjByDomaine() {
			return numOfObjByDomaine;
		}

		public HashMap getMontantByDomaine() {
			return null;
		}

		public NSArray getcomptabilites() {
			return comptabilites;
		}

		public ZEOComboBoxModel getcomptabiliteModel() {
			return myComptabiliteModel;
		}
	}

	/**
	 * Selection des prelevements a effectuer.
	 */
	private class RecouvrementPanelStep2Listener implements RecouvrementStep2PrelevementsSelection.RecouvrementStep2PrelevementsSelectionListener {
		private final Boolean PRESELECTION_OBJET = Boolean.TRUE;
		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final ActionPrelevementDelete actionPrelevementDelete = new ActionPrelevementDelete();
		private final ActionPrelevementModif actionPrelevementModif = new ActionPrelevementModif();
		private final ActionPrelevAttentePrint actionPrelevAttentePrint = new ActionPrelevAttentePrint();
		private final ActionSrch actionSrch = new ActionSrch();
		private final HashMap filters = new HashMap();
		private final HashMap checkedPrelevements = new HashMap();
		private NSMutableArray allPrelevements;
		private final PrelevementSrchCellRenderer prelevementSrchCellRenderer = new PrelevementSrchCellRenderer();

		public RecouvrementPanelStep2Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(true);
			actionPrev.setEnabled(true);
			// updateData();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionPrelevementDelete extends AbstractAction {

			public ActionPrelevementDelete() {
				super();
				putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
				putValue(Action.NAME, "Supprimer");
				putValue(Action.SHORT_DESCRIPTION, "Supprimer DEFINITIVEMENT l'échéance sélectionnée");
				setEnabled(false);
			}

			public void actionPerformed(ActionEvent e) {
				prelevementDelete();
			}
		}

		private final class ActionPrelevementModif extends AbstractAction {

			public ActionPrelevementModif() {
				super();
				putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
				putValue(Action.NAME, "Modifier");
				putValue(Action.SHORT_DESCRIPTION, "Modifier l'échéance sélectionnée");
				setEnabled(false);
			}

			public void actionPerformed(ActionEvent e) {
				prelevementModif();
			}
		}

		private final class ActionPrelevAttentePrint extends AbstractAction {

			public ActionPrelevAttentePrint() {
				super();
				putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
				putValue(Action.NAME, "Imprimer");
				putValue(Action.SHORT_DESCRIPTION, "Imprimer la liste des prélèvements en attente");
				setEnabled(false);
			}

			public void actionPerformed(ActionEvent e) {
				prelevementsAttentePrint();
			}
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP2();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP2();

			}

		}

		public String getSelectedDomaine() {
			return step1Panel.getSelectedDomaine();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep2PrelevementsSelectionListener.Step2Listener#getAllPrelevements()
		 */
		public NSArray getAllPrelevements() {
			ZLogger.verbose("getAllPrelevements()");
			ZLogger.verbose(allPrelevements);
			return allPrelevements;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep2PrelevementsSelectionListener.Step2Listener#getCheckedPrelevements()
		 */
		public HashMap getCheckedPrelevements() {
			return checkedPrelevements;
		}

		protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) {
			NSMutableArray quals = new NSMutableArray();
			//            quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[]{myApp.appUserInfo().getCurrentExercice()})));

			//Construire les qualifiers à partir des saisies utilisateur

			NSMutableArray qualsDate = new NSMutableArray();
			if (dicoFiltre.get("prelevDatePrelevementMin") != null) {
				qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("prelevDatePrelevement>=%@", new NSArray(new NSTimestamp(ZDateUtil.addDHMS((Date) dicoFiltre.get("prelevDatePrelevementMin"), 0, -2, 0, 0)))));
			}
			if (dicoFiltre.get("prelevDatePrelevementMax") != null) {
				qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("prelevDatePrelevement<=%@", new NSArray(new NSTimestamp(ZDateUtil.addDHMS((Date) dicoFiltre.get("prelevDatePrelevementMax"), 0, 2, 0, 0)))));
			}
			if (qualsDate.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsDate));
			}
			return quals;
		}

		public final void updateData() {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			checkedPrelevements.clear();

			//Les prelevements peuvent etre relies a des titres ou pas, on fait les deux fetchs independamment

			NSMutableArray res = new NSMutableArray();

			final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey(EOPrelevement.PRELEV_DATE_PRELEVEMENT_KEY, EOSortOrdering.CompareAscending);
			final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.LIBELLE_KEY, EOSortOrdering.CompareAscending);

			NSMutableArray andQuals = new NSMutableArray();
			andQuals.addObjectsFromArray(buildFilterQualifiers(filters));
			andQuals.addObject(new EOKeyValueQualifier(EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.ETAT_PRELEVEMENT_KEY, EOQualifier.QualifierOperatorEqual, "V"));
			andQuals.addObject(EOPrelevement.QUAL_ETAT_ATTENTE);

			// adapter les qualifier (en fonction du fait que l'echeancier peut etre relié à un titre ou pas.

			NSMutableArray qualTitres = new NSMutableArray();
			qualTitres.addObject(EOQualifier.qualifierWithQualifierFormat("echeancier.recette.titre.bordereau.borEtat=%@", new NSArray(EOBordereau.BordereauVise)));
			qualTitres.addObject(EOQualifier.qualifierWithQualifierFormat("echeancier.recette.titre.titEtat=%@", new NSArray(EOTitre.titreVise)));
			qualTitres.addObject(EOQualifier.qualifierWithQualifierFormat("echeancier.recette.titre.titTtc>0", null));
			EOQualifier qualTitre = new EOAndQualifier(qualTitres);

			NSMutableArray quals2 = new NSMutableArray();
			quals2.addObjectsFromArray(andQuals);
			quals2.addObject(qualTitre);
			res.addObjectsFromArray(EOsFinder.fetchArray(getEditingContext(), EOPrelevement.ENTITY_NAME, new EOAndQualifier(quals2), null, true));

			NSMutableArray qualScols = new NSMutableArray();
			qualScols.addObject(EOQualifier.qualifierWithQualifierFormat("echeancier.bordereau.borEtat=%@", new NSArray(new Object[] {
					EOBordereau.BordereauVise
			})));
			EOQualifier qualScol = new EOAndQualifier(qualScols);
			NSMutableArray quals3 = new NSMutableArray();
			quals3.addObjectsFromArray(andQuals);
			quals3.addObject(qualScol);
			res.addObjectsFromArray(EOsFinder.fetchArray(getEditingContext(), EOPrelevement.ENTITY_NAME, new EOAndQualifier(quals3), null, true));

			allPrelevements = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					sort0, sort1
			})).mutableClone();
			for (int i = 0; i < allPrelevements.count(); i++) {
				final EOPrelevement element = (EOPrelevement) allPrelevements.objectAtIndex(i);
				checkedPrelevements.put(element, PRESELECTION_OBJET);
			}
			ZLogger.verbose("RecouvrementPanelStep2Listener.updateData() - nb checkedPrelevements = " + checkedPrelevements.size());
		}

		private final void onSrch() {
			try {
				setWaitCursor(true);
				updateData();
				step2Panel.updateData();
				refreshActions();
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		private final void prelevementDelete() {
			try {
				final EOPrelevement prelevement = step2Panel.getSelectedPrelevement();
				if (prelevement != null) {
					if (showConfirmationDialog("Confirmation", "Voulez-vous réellement supprimer le prélèvement n°" + prelevement.getPrelevementNumAndTotal() + " pour " + prelevement.getClientNomAndPrenom()
							+ " Il n'apparaitra plus dans la liste des prélèvements à effectuer.<br> Ne supprimez que si vous avez régularisé les sommes dûes autrement.", ZMsgPanel.BTLABEL_NO)) {

						final FactoryRecouvrement factoryRecouvrement = new FactoryRecouvrement(myApp.wantShowTrace());
						factoryRecouvrement.supprimerPrelevements(new NSArray(new Object[] {
								prelevement
						}));
						if (getEditingContext().hasChanges()) {
							getEditingContext().saveChanges();
						}

						checkedPrelevements.put(prelevement, Boolean.FALSE);
						allPrelevements.removeObject(prelevement);
						checkedPrelevements.remove(prelevement);
						//                    step2Panel.getRecouvrementPanePrelevementSelect().fireTableDataChanged();
						step2Panel.getRecouvrementPanePrelevementSelect().updateData();

						refreshActions();
					}
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		private final void prelevementModif() {
			try {
				final EOPrelevement prelevement = step2Panel.getSelectedPrelevement();
				if (prelevement != null) {
					PrelevementModifCtrl ctrl = new PrelevementModifCtrl(getEditingContext());
					ctrl.openDialog(getMyDialog(), prelevement);
					step2Panel.getRecouvrementPanePrelevementSelect().fireSeletedTableRowUpdated();
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		private final void prelevementsAttentePrint() {
			if (getEditingContext().hasChanges()) {
				showWarningDialog("Attention : si vous avez supprimé des prélèvements en attente sur cet écran, ils apparaitront quand même sur l'édition.");
			}

			RecouvrementProxyCtrl.imprimerPrelevementsAttente(RecouvrementCtrl.this, filters, getMyDialog());
		}

		private final void refreshActions() {
			actionPrelevAttentePrint.setEnabled(getAllPrelevements().count() > 0);
			actionPrelevementDelete.setEnabled(step2Panel.getSelectedPrelevement() != null && EOPrelevement.ETAT_ATTENTE.equals(step2Panel.getSelectedPrelevement().prelevEtatMaracuja()));
			actionPrelevementModif.setEnabled(step2Panel.getSelectedPrelevement() != null && EOPrelevement.ETAT_ATTENTE.equals(step2Panel.getSelectedPrelevement().prelevEtatMaracuja()));
		}

		private final class ActionSrch extends AbstractAction {
			public ActionSrch() {
				super();
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
			}

			/**
			 * Appelle updateData();
			 */
			public void actionPerformed(ActionEvent e) {
				onSrch();
			}
		}

		private final class PrelevementSrchCellRenderer extends ZEOTableCellRenderer {
			/**
             *
             */
			public PrelevementSrchCellRenderer() {

			}

			public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
				Component res = null;
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (isSelected) {
					res.setBackground(table.getSelectionBackground());
					res.setForeground(table.getSelectionForeground());
				}
				else {
					res.setBackground(table.getBackground());
					res.setForeground(table.getForeground());
				}

				final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
				final EOPrelevement obj = (EOPrelevement) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
				if (obj.rib() == null || !obj.rib().isRibFRComplet() || obj.prelevDatePrelevement().before(ZDateUtil.nowAsDate())) {
					res.setBackground(ZConst.BGCOL_DESEQUILIBRE);
					res.setForeground(Color.WHITE);
				}
				if (EOPrelevement.ETAT_SUPPRIME.equals(obj.prelevEtatMaracuja())) {
					((JLabel) res).setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + ((JLabel) res).getText() + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX);
				}
				return res;
			}
		}

		public Action getActionSrch() {
			return actionSrch;
		}

		public HashMap getFilters() {
			return filters;
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return prelevementSrchCellRenderer;
		}

		public Action actionPrelevementDelete() {
			return actionPrelevementDelete;
		}

		public void selectionChanged() {
			refreshActions();

		}

		public void afterCheck() {

		}

		public Action getPrelevAttentePrint() {
			return actionPrelevAttentePrint;
		}

		public Action actionPrelevementModif() {
			return actionPrelevementModif;
		}

	}

	private class RecouvrementPanelStep3Listener implements RecouvrementStep3PrelevementsSelectionnes.RecouvrementStep3PrelevementsSelectionnesListener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private Action actionRetenueNew;

		/**
         *
         */
		public RecouvrementPanelStep3Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(true);
			actionPrev.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP3();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP3();

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep3PrelevementsSelectionnesListener.Step3Listener#getPrelevements()
		 */
		public NSArray getPrelevements() {
			return prelevementsARecouvrer();
		}

	}

	private class RecouvrementPanelStep7Listener implements RecouvrementStep7OptionsFichier.RecouvrementStep7OptionsFichierListener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final IZDatePickerFieldModel dateValeurModel;
		private final IZDatePickerFieldModel dateOperationModel;
		private ZEOComboBoxModel formatsVirementModel;
		private final DefaultComboBoxModel vitessePrelevementsModel;
		private final DocumentListener dateValeurListener;
		private final ActionCalculeDateOperation actionCalculeDateOperation = new ActionCalculeDateOperation();
		private final ActionAfficheCalendrierBdf actionAfficheCalendrierBdf = new ActionAfficheCalendrierBdf();
		private HashMap<String, Integer> vitessesPrelevementMap = new HashMap<String, Integer>();
		private final String libelleVitesseNormale;
		private final String libelleVitesseAcceleree;

		// private Date dateValeur;

		public RecouvrementPanelStep7Listener() throws Exception {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(true);
			actionSpecial1.setEnabled(true);
			dateValeurModel = new DateValeurModel();
			dateOperationModel = new DateOperationModel();

			String vitesseNormaleStr = (String) myApp.getParametres().valueForKey("org.cocktail.gfc.prelevement.vitesse.normale");
			String vitesseAccelereeStr = (String) myApp.getParametres().valueForKey("org.cocktail.gfc.prelevement.vitesse.acceleree");

			Integer vitesseNormale = null;
			Integer vitesseAcceleree = null;

			try {
				vitesseNormale = Integer.valueOf(vitesseNormaleStr);
				vitesseAcceleree = Integer.valueOf(vitesseAccelereeStr);
			} catch (Exception e) {
				e.printStackTrace();
				Exception e1 = new Exception("Erreur lors de la recuperation des parametres org.cocktail.gfc.prelevement.vitesse.normale et org.cocktail.gfc.prelevement.vitesse.acceleree : " + e.getMessage());
				throw e1;
				//showErrorDialog(e1);
			}

			vitessePrelevementsModel = new DefaultComboBoxModel();
			if (vitesseNormale != null && vitesseNormale.intValue() != 0) {
				libelleVitesseNormale = "Normal (réglement à J+" + vitesseNormale.intValue() + ")";
				vitessesPrelevementMap.put(libelleVitesseNormale, vitesseNormale);
				vitessePrelevementsModel.addElement(libelleVitesseNormale);
			}
			else {
				libelleVitesseNormale = null;
			}
			if (vitesseAcceleree != null && vitesseAcceleree.intValue() != 0) {
				libelleVitesseAcceleree = "Accéléré (réglement à J+" + vitesseAcceleree.intValue() + ")";
				vitessesPrelevementMap.put(libelleVitesseAcceleree, vitesseAcceleree);
				vitessePrelevementsModel.addElement(libelleVitesseAcceleree);
			}
			else {
				libelleVitesseAcceleree = null;
			}

			if (vitessesPrelevementMap.size() == 0) {
				throw new Exception("Aucune vitesse de prélèvement paramétrée valide (org.cocktail.gfc.prelevement.vitesse.normale ou org.cocktail.gfc.prelevement.vitesse.acceleree)");
			}

			dateValeurListener = new DocumentListener() {

				public void changedUpdate(DocumentEvent e) {
					dateOperation = null;
					step7Panel.updateDataDateOperation();

				}

				public void insertUpdate(DocumentEvent e) {
					dateOperation = null;
					step7Panel.updateDataDateOperation();

				}

				public void removeUpdate(DocumentEvent e) {
					dateOperation = null;
					step7Panel.updateDataDateOperation();

				}

			};

			try {
				final NSArray formatsPrelevements = EOsFinder.fetchArray(getEditingContext(), EOPrelevementParamBdf.ENTITY_NAME, "ppbEtat=%@ and typeRecouvrement.trecValidite=%@", new NSArray(new Object[] {
						EOPrelevementParamBdf.ETAT_VALIDE, EOTypeRecouvrement.ETAT_VALIDE
				}), null, false);
				formatsVirementModel = new ZEOComboBoxModel(formatsPrelevements, "libelle", null, null);

			} catch (Exception e) {
				formatsVirementModel = null;
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP7();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP7();

			}

		}

		private final class ActionCalculeDateOperation extends AbstractAction {
			public ActionCalculeDateOperation() {
				super("");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer la date de prélèvement (par rapport au calendrier BDF)");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				dateOperation = determinerDateOperation(dateValeur, getNbJoursDelai());
				step7Panel.updateDataDateOperation();

			}

		}

		private final class ActionAfficheCalendrierBdf extends AbstractAction {
			public ActionAfficheCalendrierBdf() {
				super("");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Afficher le calendrier BDF");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				try {
					final String filePath = ReportFactoryClient.imprimerBdfCalendrier(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), getMyDialog());
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep7OptionsFichierListener.Step7Listener#dateValeurModel()
		 */
		public IZDatePickerFieldModel dateValeurModel() {
			return dateValeurModel;
		}

		public Date getDateValeur() {
			return dateValeur;
		}

		public void setDateValeur(Date adateValeur) {
			dateValeur = adateValeur;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep7OptionsFichierListener.Step7Listener#montantRecouvrement()
		 */
		public BigDecimal montantRecouvrement() {
			final BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(step3Listener.getPrelevements(), "prelevMontant");
			return total;
		}

		public ZEOComboBoxModel getFormatsVirementModel() {
			return formatsVirementModel;
		}

		public ComboBoxModel getVitessePrelevementsModel() {
			return vitessePrelevementsModel;
		}

		public boolean prelevementAccelere() {
			return libelleVitesseAcceleree.equals(vitessePrelevementsModel.getSelectedItem());
		}

		public String getCodeOp() {
			return (libelleVitesseAcceleree.equals(vitessePrelevementsModel.getSelectedItem()) ? FactoryProcessPrelevementFichiersBdf.CODE_VITESSE_ACCELERE : FactoryProcessPrelevementFichiersBdf.CODE_VITESSE_NORMAL);
		}

		public NSArray getPrelevements() {
			return step3Listener.getPrelevements();
		}

		public Date getDateReglement() {
			return dateOperation;
			//            return ZDateUtil.addDHMS(getDateValeur(), (prelevementAccelere()? NBJOURS_ACCELERE: NBJOURS_NORMAL), 0, 0, 0);
		}

		public int getNbJoursDelai() {
			return vitessesPrelevementMap.get(vitessePrelevementsModel.getSelectedItem());
			//return prelevementAccelere() ? NBJOURS_ACCELERE : NBJOURS_NORMAL;
		}

		public IZDatePickerFieldModel dateOperationModel() {
			return dateOperationModel;
		}

		private final class DateValeurModel implements IZDatePickerFieldModel {
			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return dateValeur;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				System.out.println("DateValeurModel.setValue()");
				System.out.println(value);

				//On remet a jour la date d'operation
				dateOperation = null;
				if (value instanceof Date) {
					dateValeur = (Date) value;
				}
				else {
					dateValeur = null;
				}

			}

			public Window getParentWindow() {
				return getMyDialog();
			}

		}

		private final class DateOperationModel implements IZDatePickerFieldModel {
			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return dateOperation;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				System.out.println("DateOperationModel.setValue()");
				System.out.println(value);

				if (value instanceof Date) {
					dateOperation = (Date) value;
				}
				else {
					dateOperation = null;
				}

			}

			public Window getParentWindow() {
				return getMyDialog();
			}

		}

		public boolean isDateEchangeValide() {
			return isDateDansCalendrierBdf(dateValeur);
		}

		public Action actionCalculeDateOperation() {
			return actionCalculeDateOperation;
		}

		public DocumentListener dateValeurListener() {
			return dateValeurListener;
		}

		public Action actionAfficheCalendrierBdf() {
			return actionAfficheCalendrierBdf;
		}
	}

	private class RecouvrementPanelStep8Listener implements RecouvrementStep8Recapitulatif.RecouvrementStep8RecapitulatifListener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final ActionImprimerBordereau actionImprimerBordereau;
		private final ActionImprimerContenuRecouvrement actionImprimerContenuRecouvrement;
		private final ActionEnregistrer actionEnregistrer;

		/**
         *
         */
		public RecouvrementPanelStep8Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new DefaultActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(false);
			actionSpecial1.setEnabled(false);

			actionEnregistrer = new ActionEnregistrer();
			actionImprimerBordereau = new ActionImprimerBordereau();
			actionImprimerContenuRecouvrement = new ActionImprimerContenuRecouvrement();

			actionEnregistrer.setEnabled(true);
			actionImprimerBordereau.setEnabled(true);
			actionImprimerContenuRecouvrement.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep8RecapitulatifListener.Step8Listener#actionEnregistrer()
		 */
		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep8RecapitulatifListener.Step8Listener#actionImprimerBordereau()
		 */
		public Action actionImprimerBordereau() {
			return actionImprimerBordereau;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep8RecapitulatifListener.Step8Listener#actionImprimerContenuRecouvrement()
		 */
		public Action actionImprimerContenuRecouvrement() {
			return actionImprimerContenuRecouvrement;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep8RecapitulatifListener.Step8Listener#typeRecouvrement()
		 */
		public String typeRecouvrement() {
			String t = step1Panel.getSelectedDomaine();
			if (step1Panel.getSelectedDomaine().equals(EOModeRecouvrement.MODDOM_ECHEANCIER)) {
				t = t + "_" + getSelectedFormat();
			}
			return t;
		}

		public File getFilePrelevement() {
			return defaultFile;
		}

	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionPayer extends ZStepAction {
		public DefaultActionPayer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Générer");
			putValue(Action.SHORT_DESCRIPTION, "Générer le fichier de prélèvement");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			payer();
		}

	}

	private class DefaultActionNext extends ZStepAction {
		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le Bordereau d'accompagnement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.NAME, "Enregistrer le fichier");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			enregistrerFichier(currentRecouvrement);

		}
	}

	public class ActionImprimerContenuRecouvrement extends AbstractAction {
		public ActionImprimerContenuRecouvrement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail du paiement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerContenuRecouvrement();

		}
	}

	/**
	 * @return Les prelevements sélectionnes (et non supprimes) par l'utilisateur.
	 */
	private final NSArray prelevementsARecouvrer() {
		final NSArray tmp = new NSArray(ZListUtil.getKeyListForValue(step2Listener.checkedPrelevements, Boolean.TRUE).toArray());

		//On filtre sur les prelevements reellement en attente
		return EOQualifier.filteredArrayWithQualifier(tmp, EOPrelevement.QUAL_ETAT_ATTENTE);
	}

	/**
	 * @param domaine
	 * @return Les prelevements en fonction d'un domaine de mode de paiement.
	 */
	private final NSArray getPrelevementsByDomaineRecouvrement(final String domaine) {
		NSArray res;

		//        final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey("exercice.exeExercice", EOSortOrdering.CompareAscending);
		//        final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareAscending);
		//        final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("bordereau.borNum", EOSortOrdering.CompareAscending);
		//        final EOSortOrdering sort3 = EOSortOrdering.sortOrderingWithKey("manNumero", EOSortOrdering.CompareAscending);

		NSMutableArray andQuals = new NSMutableArray();
		//        andQuals.addObject(qualForGestion);
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("prelevEtatMaracuja=%@", new NSArray(EOPrelevement.ETAT_ATTENTE)));
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.comptabilite=%@", new NSArray(myComptabiliteModel.getSelectedEObject())));
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("modeRecouvrement.modDom=%@", new NSArray(domaine)));
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("manEtat=%@", new NSArray(EOPrelevement.prelevementVise)));
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borEtat=%@", new NSArray(EOBordereau.BordereauVise)));

		//On enleve la limitation d'exercice pour pouvoir payer des prelevements de l'exercice precedent
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(myApp.appUserInfo().getCurrentExercice())));

		EOQualifier qual = new EOAndQualifier(andQuals);

		// ZLogger.debug("qualifier prelevements",qual);

		res = EOsFinder.fetchArray(getEditingContext(), EOPrelevement.ENTITY_NAME, qual, null, true);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {}));
	}

	public EORecouvrement createNewRecouvrementInWindow(final Window dial, Date dateMax) {
		final ZKarukeraDialog win;
		EORecouvrement res = null;
		win = createModalDialog(dial);
		step2Listener.getFilters().put("prelevDatePrelevementMax", dateMax);
		try {
			currentPanel.updateData();
			win.open();
			res = currentRecouvrement;
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	/**
	 * Vérifie d'éventuelles erreurs avant le recouvrement. - Dans le cas d'un prelevement, on vérifie si le rib est défini, s'il est cohérent avec la
	 * facture et affiche un warning si le rib n'est pas valide.
	 *
	 * @throws Exception
	 */
	public final boolean verifierPrelevementsErreurs() throws Exception {
		NSArray mds = prelevementsARecouvrer();
		boolean res = false;
		for (int i = 0; i < mds.count(); i++) {
			EOPrelevement element = (EOPrelevement) mds.objectAtIndex(i);
			if (EORib.RIB_ANNULE.equals(element.rib().ribValide())) {
				if (!showConfirmationDialog("Attention", "Le Rib affecté au prelevement n°" + element.prelevIndex() + " pour " + element.getClientNomAndPrenom() + " (" + element.rib().getRibComplet() + ") "
						+ "n'est pas valide, voulez-vous quand même continuer le prélèvement (avec ce rib invalide) ?", ZMsgPanel.BTLABEL_NO)) {
					return true;
				}
			}

			if (element.rib().ribTitco() == null) {
				throw new DefaultClientException("Le Rib affecté au prelevement n°" + element.prelevIndex() + " pour " + element.getClientNomAndPrenom() + " n'a pas de titulaire défini, veuillez corriger ce problème avant de générer le fichier de prélèvement.");
			}
			if (element.rib().ribLib() == null) {
				throw new DefaultClientException("Le Rib affecté au prelevement n°" + element.prelevIndex() + " pour " + element.getClientNomAndPrenom() + " n'a pas de domiciliation définie, veuillez corriger ce problème avant de générer le fichier de prélèvement.");
			}
		}
		return res;
	}



	/**
	 * Thread de traitement pour le paiement.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class RecouvrementPayerThread extends Thread {
		//        private Exception lastException;

		/**
         *
         */
		public RecouvrementPayerThread() {
			super();
		}

		public void run() {
			msgFin = null;
			lastException = null;
			try {
				final boolean showTrace = myApp.wantShowTrace();

				final EOComptabilite comptabilite = getComptabilite();
				final EOExercice exercice = getExercice();
				final EOUtilisateur utilisateur = getUtilisateur();
				final EOTypeRecouvrement typeRecouvrement = getSelectedTypeRecouvrement();
				final NSTimestamp dateCreation = new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.now()));
				final NSTimestamp laDateValeur = new NSTimestamp(ZDateUtil.getDateOnly(step7Listener.getDateValeur()));
				final NSTimestamp laDateReglement = new NSTimestamp(ZDateUtil.getDateOnly(step7Listener.getDateReglement()));
				final BigDecimal montant = step7Listener.montantRecouvrement();
				final NSArray lesPrelevementsATraiter = step7Listener.getPrelevements();
				final Integer nbPrelevements = new Integer(lesPrelevementsATraiter.count());

				final EOPrelevementParamBdf prelevementParamBdf = getSelectedPrelevementParamBdf();
				final boolean prelevementAccelere = step7Listener.prelevementAccelere();
				final String codeOp = step7Listener.getCodeOp();

				final EOTypeJournal typeJournal = FinderJournalEcriture.leTypeJournalRecouvrement(getEditingContext());
				final EOTypeOperation typeOperation = FinderJournalEcriture.leTypeOperationRecouvrement(getEditingContext());

				final NSTimestamp dateJourneeComptable = new NSTimestamp(getDateJourneeComptable());

				NSMutableArray lesEcrituresQuiSerontGenerees = new NSMutableArray();

				leContenuFichier = null;

				//Créer un objet recouvrement
				final FactoryRecouvrement factoryRecouvrement = new FactoryRecouvrement(showTrace);
				currentRecouvrement = factoryRecouvrement.creerRecouvrement(getEditingContext(), comptabilite, exercice, utilisateur, typeRecouvrement, dateCreation, montant, nbPrelevements, new Integer(0));
				ZLogger.debug("Recouvrement cree : ", currentRecouvrement);

				if (EOModeRecouvrement.MODDOM_ECHEANCIER.equals(step1Panel.getSelectedDomaine())) {
					if (prelevementParamBdf == null) {
						throw new DefaultClientException("Le format du prélèvement n'est pas reconnu.");
					}

					//Associer les prélèvements au recouvrement
					factoryRecouvrement.associerPrelevements(currentRecouvrement, lesPrelevementsATraiter);
					ZLogger.debug("Prelevements rattaches au recouvrement");

					//                  //****************************************
					//Créer les écritures de recouvrement
					final FactoryProcessEcrituresPrelevements factoryProcessPrelevements = new FactoryProcessEcrituresPrelevements(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					lesEcrituresQuiSerontGenerees = factoryProcessPrelevements.genererEcrituresPrelevementsFichier(getEditingContext(), lesPrelevementsATraiter, typeJournal, typeOperation, currentModeRecouvrement, utilisateur, getExercice(), getComptabilite());

					//****************************************
					//gérer le contenu du fichier
					final FactoryProcessPrelevementFichiersBdf factoryProcessPrelevementFichiers = new FactoryProcessPrelevementFichiersBdf(showTrace, dateJourneeComptable);
					final String leContenuFichierTmp = factoryProcessPrelevementFichiers.genereContenuFichier(laDateValeur, laDateReglement, prelevementAccelere, lesPrelevementsATraiter, prelevementParamBdf);

					if (leContenuFichierTmp == null || leContenuFichierTmp.length() == 0) {
						throw new DefaultClientException("Le fichier généré est vide.");
					}

					final int nbLignes = (leContenuFichierTmp.length() / FactoryProcessPrelevementFichiersBdf.NB_CAR_PAR_ENTREE) - 2;
					if (nbLignes != nbPrelevements.intValue()) {
						throw new DefaultClientException("Nombre de lignes du fichier (" + nbLignes + ") incohérent avec le nombre de prélèvements (" + nbPrelevements + ")");
					}
					nbOperations = nbLignes;
					leContenuFichier = leContenuFichierTmp;

					//                  ****************************************
					//creer objet prelevementFichier
					final FactoryPrelevementFichier factorySauverVirement = new FactoryPrelevementFichier(myApp.wantShowTrace());
					factorySauverVirement.creerPrelevementFichier(getEditingContext(), currentRecouvrement, typeRecouvrement, utilisateur, leContenuFichier, dateCreation, laDateValeur, laDateReglement, "Fichier de prélèvement", montant, nbPrelevements, (Integer) currentRecouvrement.recoNumero(),
							codeOp);

					//***************************************
					// fermerPrelevements
					factoryRecouvrement.fermerPrelevements(currentRecouvrement);
					ZLogger.debug("Prelevements fermes");

				}

				// ZLogger.debug(getEditingContext());

				// Enregistrer les changements
				getEditingContext().saveChanges();
				waitingDialog.setBottomText("Recouvrement et écritures enregistrées");

				KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

				// numeroter les lesEcrituresQuiSerontGenerees
				NSMutableArray numEcritures = new NSMutableArray();
				FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				if ((lesEcrituresQuiSerontGenerees != null) && (lesEcrituresQuiSerontGenerees.count() > 0)) {
					for (int i = 0; i < lesEcrituresQuiSerontGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresQuiSerontGenerees.objectAtIndex(i);
						factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
					}

					final NSArray lesEcrituresTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcrituresQuiSerontGenerees, new NSArray(new EOSortOrdering("ecrNumero", EOSortOrdering.CompareAscending)));
					for (int i = 0; i < lesEcrituresTriees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresTriees.objectAtIndex(i);
						numEcritures.addObject(element.ecrNumero());
					}
				}

				// numeroter le recouvrement
				final NSMutableArray numRecouvrements = new NSMutableArray();
				myKFactoryNumerotation.getNumeroEORecouvrement(getEditingContext(), currentRecouvrement);
				numRecouvrements.addObject(currentRecouvrement.recoNumero());
				waitingDialog.setBottomText("Numérotation effectuée");

				// Message recapitulatif

				if (numRecouvrements.count() > 1) {
					msgFin = "Les recouvrements n° " + numRecouvrements.componentsJoinedByString(",") + " ont été générés.\n";
				}
				else {
					msgFin = "Le recouvrement n° " + numRecouvrements.componentsJoinedByString(",") + " a été généré.\n";
				}

				if (numEcritures.count() > 1) {
					msgFin += "Les écritures n° " + numEcritures.componentsJoinedByString(",") + " ont été générées.\n";
				}
				else {
					msgFin += "L'écriture n° " + numEcritures.componentsJoinedByString(",") + " a été générée.\n";
				}

				System.out.println(msgFin);

				final NSDictionary pdic = ServerProxy.serverPrimaryKeyForObject(getEditingContext(), currentRecouvrement);
				final Object pordre = pdic.valueForKey("recoOrdre");
				try {
					ZLogger.debug("Création des émargements");
					ServerProxy.clientSideRequestAfaireApresTraitementRecouvrement(getEditingContext(), currentRecouvrement);
					ZLogger.debug("Emargements crees");
					waitingDialog.setBottomText("Emargements crees");
					msgFin += "Les émargements automatiques ont été générée.\n";
				} catch (Exception e) {
					throw new DefaultClientException("Le recouvrement a bien été généré mais il y a eu une erreur lors de la génération des émargements automatiques pour recoOrdre= " + pordre + ". Vous devez transmettre ce message à un informaticien pour qu'il génère les émargements. : "
							+ e.getMessage());
				}

				if (lesEcrituresQuiSerontGenerees != null) {
					// On invalide tous les objets de l'éditingcontext pour être
					// sûr que les écritures details seront à jour
					waitingDialog.setBottomText("Mise à jour des données...");
					ZLogger.debug("Avant invalidate");
					final NSMutableArray lesEcr = new NSMutableArray();
					for (int i = 0; i < lesEcrituresQuiSerontGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresQuiSerontGenerees.objectAtIndex(i);

						//On invalidate les details
						final NSArray det = element.detailEcriture();
						for (int z = 0; z < det.count(); z++) {
							getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
									getEditingContext().globalIDForObject((EOEnterpriseObject) det.objectAtIndex(z))
							}));
							Thread.yield();
						}
						//on recupere les ecritures emargees
						final NSArray emarges = EOsFinder.getEcrituresEmargees(getEditingContext(), element);
						for (int j = 0; j < emarges.count(); j++) {
							final EOEcriture ecritureEmargee = (EOEcriture) emarges.objectAtIndex(j);
							final NSArray details = ecritureEmargee.detailEcriture();
							for (int k = 0; k < details.count(); k++) {
								final EOEcritureDetail detail = (EOEcritureDetail) details.objectAtIndex(k);
								if (lesEcr.indexOfObject(detail) == NSArray.NotFound) {
									lesEcr.addObject(detail);
								}
							}
						}
					}

					ZLogger.debug("Invalidate sur " + lesEcr.count() + " objets...");
					waitingDialog.getMyProgressBar().setMaximum(lesEcr.count());
					waitingDialog.getMyProgressBar().setIndeterminate(false);
					for (int i = 0; i < lesEcr.count(); i++) {
						//                         getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(lesEcr.objectAtIndex(i)));
						getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
								getEditingContext().globalIDForObject((EOEnterpriseObject) lesEcr.objectAtIndex(i))
						}));
						waitingDialog.getMyProgressBar().setValue(i);
						Thread.yield();
					}
					waitingDialog.getMyProgressBar().setIndeterminate(true);

					//                    getEditingContext().invalidateObjectsWithGlobalIDs(lesEcr)(ZEOUtilities.globalIDsForObjects(getEditingContext(), lesEcr));
					//getEditingContext().invalidateAllObjects();
					ZLogger.debug("Invalidate effectue");
				}

				// waitingDialog.setVisible(false);

			} catch (Exception e) {
				getEditingContext().revert();
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return currentPanel;
	}

	public String title() {
		return TITLE;
	}

}
