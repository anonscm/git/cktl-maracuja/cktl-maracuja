/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryRecouvrement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessEcrituresPrelevements;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.recouvrement.ui.PrelevRelevePanel;
import org.cocktail.maracuja.client.recouvrement.ui.PrelevementSelectList.PrelevementSelecListListener;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class PrelevReleveCtrl extends CommonCtrl {
	private final static String TITLE = "Gestion du relevé";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);
	private static final String PCONUM_REJET_LIKE = "5*";
	private final Boolean PRESELECTION_OBJET = Boolean.FALSE;
	protected final Dimension PCOWINDOW_SIZE = new Dimension(300, 350);

	private final PrelevRelevePanel prelevRelevePanel;

	private final PrelevementPanelSelectListener prelevementPanelSelectListener;
	private final PrelevRelevePanelListener prelevRelevePanelListener;
	private final ActionClose actionClose = new ActionClose();
	private final ActionEmargerReleve actionEmargerReleve = new ActionEmargerReleve();

	private final HashMap checkedPrelevements = new HashMap();
	private NSArray allPrelevements;
	private EORecouvrement currentRecouvrement;

	private NSArray prelevementAcceptes = new NSArray();
	private NSArray prelevementRejetes = new NSArray();
	private LinkedHashMap pcoMap;
	private final PcoLookupModel pcoLookupModel;
	private final PcoLookupListener pcoLookupListener;
	private final PcoLookupModelRejet pcoLookupModelRejet;
	private final PcoLookupListenerRejet pcoLookupListenerRejet;
	private final Map myValues = new HashMap();

	private EOPlanComptable planco = null;
	private EOPlanComptable plancoRejet = null;

	private String msgFin;
	private Exception lastException;

	ZWaitingPanelDialog waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));

	public PrelevReleveCtrl(final EOEditingContext ec) throws Exception {
		super(ec);
		revertChanges();

		final NSArray pcos;
		pcos = getPcoValides();
		pcoMap = new LinkedHashMap(pcos.count());
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptable element = (EOPlanComptable) pcos.objectAtIndex(i);
			pcoMap.put(ZStringUtil.extendWithChars(element.pcoNum(), " ", 15, false) + " " + element.pcoLibelle(), element);
		}

		pcoLookupModel = new PcoLookupModel();
		pcoLookupListener = new PcoLookupListener();
		pcoLookupModelRejet = new PcoLookupModelRejet();
		pcoLookupListenerRejet = new PcoLookupListenerRejet();
		prelevementPanelSelectListener = new PrelevementPanelSelectListener();
		prelevRelevePanelListener = new PrelevRelevePanelListener();
		prelevRelevePanel = new PrelevRelevePanel(prelevRelevePanelListener);
	}

	protected final NSArray getPcoValides() {
		return EOPlanComptableFinder.getPlancoValidesForPcoNumLike(getEditingContext(), PCONUM_REJET_LIKE, false);
	}

	public final void updateData() {
		checkedPrelevements.clear();
		NSArray res;

		final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey("prelevDatePrelevement", EOSortOrdering.CompareAscending);
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("echeancier.libelle", EOSortOrdering.CompareAscending);

		final NSMutableArray andQuals = new NSMutableArray();
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("recouvrement=%@", new NSArray(currentRecouvrement)));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("prelevEtatMaracuja=%@", new NSArray(EOPrelevement.ETAT_PRELEVE)));

		EOQualifier qual = new EOAndQualifier(andQuals);

		res = EOsFinder.fetchArray(getEditingContext(), EOPrelevement.ENTITY_NAME, qual, null, true);
		allPrelevements = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				sort0, sort1
		}));

		//        allPrelevements = getPrelevementsByDomaineRecouvrement(step1Panel.getSelectedDomaine());
		for (int i = 0; i < allPrelevements.count(); i++) {
			final EOPrelevement element = (EOPrelevement) allPrelevements.objectAtIndex(i);
			checkedPrelevements.put(element, PRESELECTION_OBJET);
		}
		System.out.println("RecouvrementPanelStep2Listener.updateData() - nb checkedPrelevements = " + checkedPrelevements.size());

		refreshTotaux();

		prelevRelevePanel.getPcoSelectButton().updateData();
		prelevRelevePanel.getPcoSelectButtonRejet().updateData();

	}

	private final void onValider() {
		try {
			final String pcoNum = (String) myValues.get("pcoNum");
			if (pcoNum == null) {
				throw new DataCheckException("Vous devez indiquer un compte TG");
			}
			planco = EOPlanComptableFinder.getPlancoValideForPcoNum(getEditingContext(), pcoNum, false);
			if (planco == null) {
				throw new DataCheckException("Le compte TG que vous avez indiqué n'existe pas ou n'est pas valide dans le plan comptable");
			}

			if (prelevementRejetes.count() > 0) {
				//Vérifier le compte selectionné
				final String pcoNumRejet = (String) myValues.get("pcoNumRejet");
				if (pcoNumRejet == null) {
					throw new DataCheckException("Vous devez indiquer un compte de rejet");
				}
				plancoRejet = EOPlanComptableFinder.getPlancoValideForPcoNum(getEditingContext(), pcoNumRejet, false);
				if (plancoRejet == null) {
					throw new DataCheckException("Le compte de rejet que vous avez indiqué n'existe pas ou n'est pas valide dans le plan comptable");
				}
			}

			if (showConfirmationDialog("Confirmation", "Souhaitez-vous valider le relevé ?\nLes écritures correspondantes seront générées.", ZMsgPanel.BTLABEL_NO)) {
				waitingDialog.setTitle("Traitement du recouvrement");
				waitingDialog.setTopText("Veuillez patienter ...");
				waitingDialog.setModal(true);
				lastException = null;
				updateLoadingMsg("Ce traitement peut être long...");
				try {

					final PrelevementReleveThread thread = new PrelevementReleveThread();
					thread.start();

					if (thread.isAlive()) {
						waitingDialog.setVisible(true);
					}
					if (lastException != null) {
						throw lastException;
					}

					if (msgFin != null) {
						showInfoDialog(msgFin);
					}

				} catch (Exception e) {
					showErrorDialog(e);
					getEditingContext().revert();
				} finally {
					waitingDialog.setVisible(false);
					if (waitingDialog != null) {
						waitingDialog.dispose();
					}
				}

			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		prelevRelevePanel.initGUI();
		prelevRelevePanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(prelevRelevePanel);
		win.pack();
		return win;
	}

	public final void openDialog(Window dial, final EORecouvrement recouvrement) {
		final ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		currentRecouvrement = recouvrement;
		updateData();

		try {
			prelevRelevePanel.updateData();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
	}

	private final NSArray getPrelevementSelectionnes(HashMap map, Boolean isSelected) {
		Iterator iter1 = map.keySet().iterator();
		NSMutableArray res = new NSMutableArray();
		while (iter1.hasNext()) {
			EOPrelevement element = (EOPrelevement) iter1.next();
			if (isSelected.equals(map.get(element))) {
				res.addObject(element);
			}
		}
		return res;
	}

	private void refreshTotaux() {
		prelevementAcceptes = getPrelevementSelectionnes(checkedPrelevements, Boolean.FALSE);
		prelevementRejetes = getPrelevementSelectionnes(checkedPrelevements, Boolean.TRUE);

		prelevRelevePanel.updateDataTotaux();

	}

	private final class PrelevRelevePanelListener implements PrelevRelevePanel.IPrelevRelevePanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionEmargerReleve() {
			return actionEmargerReleve;
		}

		public Integer getAcceptesNb() {
			return new Integer(prelevementAcceptes.count());
		}

		public BigDecimal getAcceptesTotal() {
			return ZEOUtilities.calcSommeOfBigDecimals(prelevementAcceptes, EOPrelevement.PRELEVMONTANTKEY);
		}

		public PrelevementSelecListListener getPrelevementPanelSelectListener() {
			return prelevementPanelSelectListener;
		}

		public Integer getRejetesNb() {
			return new Integer(prelevementRejetes.count());
		}

		public BigDecimal getRejetesTotal() {
			return ZEOUtilities.calcSommeOfBigDecimals(prelevementRejetes, EOPrelevement.PRELEVMONTANTKEY);
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return null;
		}

		public void onSelectionChanged() {

		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public Map getValues() {
			return myValues;
		}

		public IZLookupButtonListener getLookupButtonCompteRejetListener() {
			return pcoLookupListenerRejet;
		}

		public IZLookupButtonModel getLookupButtonCompteRejetModel() {
			return pcoLookupModelRejet;
		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			getMyDialog().onCloseClick();
		}

	}

	public final class ActionEmargerReleve extends AbstractAction {

		public ActionEmargerReleve() {
			super("Créer les écritures");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onValider();
		}

	}

	private class PcoLookupListener implements IZLookupButtonListener {

		public void setNewValue(Object res) {
			final EOPlanComptable tmp = (EOPlanComptable) pcoMap.get(res);
			String s = (String) myValues.get("pcoNum");
			s = tmp.pcoNum();

			System.out.println("PcoLookupListener.setNewValue() = " + s);
			myValues.put("pcoNum", s);
			try {
				prelevRelevePanel.getPcoNum().updateData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onTextHasChanged() {

		}

		public void onDbClick() {

		}

	}

	private class PcoLookupModel implements IZLookupButtonModel {
		private final Collection pcosTmp;

		public PcoLookupModel() {
			pcosTmp = pcoMap.keySet();
		}

		public Collection getDatas() {
			//            System.out.println("PcoLookupModel.getDatas()");
			//            ZLogger.debug(pcosTmp);
			return pcosTmp;
		}

		public String getActionDescription() {
			return "Sélectionner un compte";
		}

		public ImageIcon getIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
		}

		public Window getMyWindow() {
			return getMyDialog();
		}

		public String getTitle() {
			return "Sélectionnez un compte";
		}

		public Dimension getPreferredSize() {
			return PCOWINDOW_SIZE;
		}

	}

	private class PcoLookupListenerRejet implements IZLookupButtonListener {

		public void setNewValue(Object res) {
			final EOPlanComptable tmp = (EOPlanComptable) pcoMap.get(res);
			String s = (String) myValues.get("pcoNumRejet");
			s = tmp.pcoNum();

			System.out.println("PcoLookupListener.setNewValue() = " + s);
			myValues.put("pcoNumRejet", s);
			try {
				prelevRelevePanel.getPcoNumRejet().updateData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onTextHasChanged() {

		}

		public void onDbClick() {

		}

	}

	private class PcoLookupModelRejet implements IZLookupButtonModel {
		private final Collection pcosTmp;

		public PcoLookupModelRejet() {
			pcosTmp = pcoMap.keySet();
		}

		public Collection getDatas() {
			//            System.out.println("PcoLookupModel.getDatas()");
			//            ZLogger.debug(pcosTmp);
			return pcosTmp;
		}

		public String getActionDescription() {
			return "Sélectionner un compte";
		}

		public ImageIcon getIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
		}

		public Window getMyWindow() {
			return getMyDialog();
		}

		public String getTitle() {
			return "Sélectionnez un compte";
		}

		public Dimension getPreferredSize() {
			return PCOWINDOW_SIZE;
		}

	}

	private final class PrelevementPanelSelectListener implements PrelevementSelecListListener {

		public HashMap getAllCheckedPrelevements() {
			return checkedPrelevements;
		}

		public NSArray getAllPrelevements() {
			return allPrelevements;
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return null;
		}

		public void afterCheck() {
			refreshTotaux();

		}

		public Action actionPrelevementDelete() {
			return null;
		}

		public boolean autoriseSuppression() {
			return false;
		}

		public NSArray getData() throws Exception {
			return allPrelevements;
		}

		public void onDbClick() {

		}

		public void selectionChanged() {

		}

	}

	public final class PrelevementReleveThread extends Thread {

		/**
         * 
         */
		public PrelevementReleveThread() {
			super();
		}

		public void run() {
			msgFin = "";
			lastException = null;
			try {
				//Le mode recouvrement est pris à partir du titre
				final EOModeRecouvrement modeRecouvrement = null;
				final EOTypeJournal typeJournal = FinderJournalEcriture.leTypeJournalRecouvrement(getEditingContext());
				final EOTypeOperation typeOperation = FinderJournalEcriture.leTypeOperationRecouvrement(getEditingContext());

				ZLogger.debug("nb prelev acceptes = " + prelevementAcceptes.count());
				ZLogger.debug("nb prelev rejetes = " + prelevementRejetes.count());

				final FactoryProcessEcrituresPrelevements factoryProcessEcrituresPrelevements = new FactoryProcessEcrituresPrelevements(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				final NSMutableArray lesEcrituresQuiSerontGenerees = factoryProcessEcrituresPrelevements.genererEcrituresPrelevementsReleveBancaire(getEditingContext(), prelevementAcceptes, prelevementRejetes, typeJournal, typeOperation, getUtilisateur(), planco, plancoRejet, modeRecouvrement,
						getExercice(), getComptabilite());

				final FactoryRecouvrement factoryRecouvrement = new FactoryRecouvrement(myApp.wantShowTrace());
				factoryRecouvrement.confirmerPrelevements(prelevementAcceptes);
				factoryRecouvrement.rejeterPrelevements(prelevementRejetes);

				if (lesEcrituresQuiSerontGenerees.count() == 0) {
					throw new DefaultClientException("Erreur : Aucune écriture de générée. Les écritures de prélèvement ont peut-être été déjà émargées.");
				}

				ZLogger.debug("nb ecritures = " + lesEcrituresQuiSerontGenerees.count());
				// ZLogger.debug(getEditingContext());

				// Enregistrer les changements
				getEditingContext().saveChanges();
				waitingDialog.setBottomText("Ecritures enregistrées");

				KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

				// numeroter les lesEcrituresQuiSerontGenerees
				NSMutableArray numEcritures = new NSMutableArray();
				FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				if ((lesEcrituresQuiSerontGenerees != null) && (lesEcrituresQuiSerontGenerees.count() > 0)) {
					for (int i = 0; i < lesEcrituresQuiSerontGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresQuiSerontGenerees.objectAtIndex(i);
						factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
					}

					final NSArray lesEcrituresTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcrituresQuiSerontGenerees, new NSArray(new EOSortOrdering("ecrNumero", EOSortOrdering.CompareAscending)));
					for (int i = 0; i < lesEcrituresTriees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresTriees.objectAtIndex(i);
						numEcritures.addObject(element.ecrNumero());
					}
				}

				waitingDialog.setBottomText("Numérotation effectuée");

				// Message recapitulatif
				if (numEcritures.count() > 1) {
					msgFin += "Les écritures n° " + numEcritures.componentsJoinedByString(",") + " ont été générées.\n";
				}
				else {
					msgFin += "L'écriture n° " + numEcritures.componentsJoinedByString(",") + " a été générée.\n";
				}

				System.out.println(msgFin);

				final NSDictionary pdic = ServerProxy.serverPrimaryKeyForObject(getEditingContext(), currentRecouvrement);
				final Object pordre = pdic.valueForKey("recoOrdre");
				try {
					ZLogger.debug("Création des émargements");
					ServerProxy.clientSideRequestAfaireApresTraitementRecouvrementReleve(getEditingContext(), currentRecouvrement);
					ZLogger.debug("Emargements crees");
					waitingDialog.setBottomText("Emargements crees");
					msgFin += "Les émargements automatiques ont été générée.\n";
				} catch (Exception e) {
					throw new DefaultClientException("Ecritures créées mais il y a eu une erreur lors de la génération des émargements automatiques pour recoOrdre= " + pordre + ". Vous devez transmettre ce message à un informaticien pour qu'il génère les émargements. : " + e.getMessage());
				}

				if (lesEcrituresQuiSerontGenerees != null) {
					// On invalide tous les objets de l'éditingcontext pour être
					// sûr que les écritures details seront à jour
					waitingDialog.setBottomText("Mise à jour des données...");
					ZLogger.debug("Avant invalidate");
					final NSMutableArray lesEcr = new NSMutableArray();
					for (int i = 0; i < lesEcrituresQuiSerontGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresQuiSerontGenerees.objectAtIndex(i);

						//On invalidate les details 
						final NSArray det = element.detailEcriture();
						for (int z = 0; z < det.count(); z++) {
							getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
									getEditingContext().globalIDForObject((EOEnterpriseObject) det.objectAtIndex(z))
							}));
							Thread.yield();
						}
						//on recupere les ecritures emargees   
						final NSArray emarges = EOsFinder.getEcrituresEmargees(getEditingContext(), element);
						for (int j = 0; j < emarges.count(); j++) {
							final EOEcriture ecritureEmargee = (EOEcriture) emarges.objectAtIndex(j);
							final NSArray details = ecritureEmargee.detailEcriture();
							for (int k = 0; k < details.count(); k++) {
								final EOEcritureDetail detail = (EOEcritureDetail) details.objectAtIndex(k);
								if (lesEcr.indexOfObject(detail) == NSArray.NotFound) {
									lesEcr.addObject(detail);
								}
							}
						}
					}

					ZLogger.debug("Invalidate sur " + lesEcr.count() + " objets...");
					waitingDialog.getMyProgressBar().setMaximum(lesEcr.count());
					waitingDialog.getMyProgressBar().setIndeterminate(false);
					for (int i = 0; i < lesEcr.count(); i++) {
						//                         getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(lesEcr.objectAtIndex(i)));
						getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
								getEditingContext().globalIDForObject((EOEnterpriseObject) lesEcr.objectAtIndex(i))
						}));
						waitingDialog.getMyProgressBar().setValue(i);
						Thread.yield();
					}
					waitingDialog.getMyProgressBar().setIndeterminate(true);

					//                    getEditingContext().invalidateObjectsWithGlobalIDs(lesEcr)(ZEOUtilities.globalIDsForObjects(getEditingContext(), lesEcr));
					//getEditingContext().invalidateAllObjects();
					ZLogger.debug("Invalidate effectue");
				}

				// waitingDialog.setVisible(false);

			} catch (Exception e) {
				getEditingContext().revert();
				e.printStackTrace();
				lastException = e;
			} finally {
				updateData();
				waitingDialog.setVisible(false);
			}

		}

	}

	public void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return prelevRelevePanel;
	}

	public String title() {
		return TITLE;
	}

}
