/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.recouvrement.ui.PrelevementModifPanel;
import org.cocktail.maracuja.client.recouvrement.ui.PrelevementModifPanel.IPrelevementModifPanelListener;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PrelevementModifCtrl extends CommonCtrl {
	private static final String TITLE = "Modification d'une échéance";
	private static final Dimension WINDOW_DIMENSION = new Dimension(700, 300);

	private final HashMap currentDico;

	private final PrelevementModifPanel echeanceRibPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionEnregistrer actionValider = new ActionEnregistrer();

	private EOPrelevement currentPrelevement;

	private final ZEOComboBoxModel ribModel;

	/**
	 * @param editingContext
	 */
	public PrelevementModifCtrl(EOEditingContext editingContext) {
		super(editingContext);
		currentDico = new HashMap();
		ribModel = new ZEOComboBoxModel(NSArray.EmptyArray, EORib.RIBCOMPLET_KEY, null, null);
		echeanceRibPanel = new PrelevementModifPanel(new PrelevementModifPanelListener());
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionEnregistrer extends AbstractAction {

		public ActionEnregistrer() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);
		if (ribModel.getSelectedEObject() == null) {
			throw new DataCheckException("Vous devez obligatoirement sélectionner un rib.");
		}
		currentDico.put(EOPrelevement.RIB_KEY, ribModel.getSelectedEObject());

		//Verifier le nouveau montant (le total des prelevements ne doit pas depasser le montant de l'echeancier )
		if (currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY) != null && !currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY).equals(currentDico.get(EOPrelevement.PRELEV_MONTANT_KEY))) {
			currentPrelevement.checkNewMontant((BigDecimal) currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY));
		}

		//Verifier la date (elle doit etre supérieure à la date du jour)
		if (currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY) != null && !currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY).equals(currentDico.get(EOPrelevement.PRELEV_DATE_PRELEVEMENT_KEY))) {
			currentPrelevement.checkNewDate((Date) currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY));
		}

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	private final void validerSaisie() {
		try {
			checkSaisie();
			updateObjectWithDico();

			System.out.println(getEditingContext().updatedObjects());

			if (getEditingContext().hasChanges()) {

				getEditingContext().saveChanges();
			}
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class PrelevementModifPanelListener implements PrelevementModifPanel.IPrelevementModifPanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map getValues() {
			return currentDico;
		}

		public ZEOComboBoxModel getRibModel() {
			return ribModel;
		}

	}

	public static final NSArray getRibsValides(final EOEditingContext editingContext, final EOFournisseur fournis) {
		return ZFinder.fetchArray(editingContext, EORib.ENTITY_NAME, EORib.RIB_VALIDE_KEY + "='" + EORib.RIB_VALIDE + "' and " + EORib.FOURNISSEUR_KEY + "=%@", new NSArray(new Object[] {
				fournis
		}), null, true);
	}

	public final int openDialog(Window dial, EOPrelevement prelevement) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			currentPrelevement = prelevement;
			ribModel.updateListWithData(EOsFinder.getRibsValides(getEditingContext(), currentPrelevement.fournisseur()));
			ribModel.setSelectedEObject(currentPrelevement.rib());

			updateDicoWithObject();
			echeanceRibPanel.initGUI();
			echeanceRibPanel.updateData();

			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private void updateObjectWithDico() {
		System.out.println(currentDico);
		System.out.println(currentDico.get(EOPrelevement.RIB_KEY));

		currentPrelevement.setRibRelationship((EORib) currentDico.get(EOPrelevement.RIB_KEY));

		if (currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY) != null && !currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY).equals(currentDico.get(EOPrelevement.PRELEV_MONTANT_KEY))) {
			currentPrelevement.setPrelevMontant((BigDecimal) currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY));
		}

		if (currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY) != null && !currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY).equals(currentDico.get(EOPrelevement.PRELEV_DATE_PRELEVEMENT_KEY))) {
			currentPrelevement.setPrelevDatePrelevement((NSTimestamp) new NSTimestamp((Date) currentDico.get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY)));
		}

	}

	private void updateDicoWithObject() {
		currentDico.clear();
		currentDico.put(EOPrelevement.RIB_KEY, currentPrelevement.rib());
		currentDico.put(EOPrelevement.PRELEV_DATE_PRELEVEMENT_KEY, currentPrelevement.prelevDatePrelevement());
		currentDico.put(EOPrelevement.FOURNISSEUR_KEY + "." + EOFournisseur.NOM_AND_PRENOM_KEY, currentPrelevement.fournisseur().getNomAndPrenomAndCode());
		currentDico.put(EOPrelevement.PRELEV_MONTANT_KEY, currentPrelevement.prelevMontant());
		currentDico.put(EOPrelevement.PRELEVEMENTNUMANDTOTAL_KEY, currentPrelevement.getPrelevementNumAndTotal());
		currentDico.put(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY, currentPrelevement.prelevDatePrelevement());
		currentDico.put(IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY, currentPrelevement.prelevMontant());
		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return echeanceRibPanel;
	}

	public String title() {
		return TITLE;
	}

}
