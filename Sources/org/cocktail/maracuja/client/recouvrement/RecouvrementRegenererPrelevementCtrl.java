/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement;

import java.awt.CardLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction;
import org.cocktail.maracuja.client.factory.FactoryPrelevementFichier;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPrelevementFichiersBdf;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPrelevementParamBdf;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep7OptionsFichier;
import org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep8Recapitulatif;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementRegenererPrelevementCtrl extends CommonCtrl {
	public static final String ACTIONID = ZActionCtrl.IDU_PAGE;

	/** Le choix du format de virement */
	private static final String STEP7 = "step7";
	private static final String STEP8 = "step8";
	//    private static final String STEP9="step9";

	private static final String TITLE = "Regénération du fichier de prélèvement";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);

	private RecouvrementStep7OptionsFichier step7Panel;
	private RecouvrementStep8Recapitulatif step8Panel;

	private RecouvrementPanelStep7Listener step7Listener;
	private RecouvrementPanelStep8Listener step8Listener;

	private JPanel cardPanel;
	private CardLayout cardLayout;
	private ZKarukeraStepPanel2 currentPanel;

	private final DefaultActionClose actionClose;
	private ZEOComboBoxModel myComptabiliteModel;

	private final HashMap stepPanels = new HashMap();
	private EORecouvrement currentRecouvrement;
	private String leContenuFichierVirement;

	private Date dateValeur;
	private Date dateOperation;

	private int nbOperations = -1;

	private File defaultFile;

	private final NSArray calendrierBDF;

	/**
	 * @throws Exception
	 */
	public RecouvrementRegenererPrelevementCtrl(EOEditingContext ec) throws Exception {
		super(ec);

		actionClose = new DefaultActionClose();
		actionClose.setEnabled(true);

		calendrierBDF = EOsFinder.getCalendrierBdfAutour(getEditingContext(), ZDateUtil.getDateOnly(ZDateUtil.now()));

		if (calendrierBDF.count() < 15) {
			showInfoDialog("Le calendrier de la Banque de France ne semble pas être complet. Il n'y aura peut-etre pas de contrôle possible pour les dates d'échange et de réglement.");
		}

		try {
			//creer les listeners
			step7Listener = new RecouvrementPanelStep7Listener();
			step8Listener = new RecouvrementPanelStep8Listener();

			step7Panel = new RecouvrementStep7OptionsFichier(step7Listener);
			step8Panel = new RecouvrementStep8Recapitulatif(step8Listener);

			stepPanels.put(STEP7, step7Panel);
			stepPanels.put(STEP8, step8Panel);

			//            initGUI();

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void initGUI() {

		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Iterator iter = stepPanels.keySet().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			ZKarukeraStepPanel2 panel = (ZKarukeraStepPanel2) stepPanels.get(element);
			panel.setMyDialog(getMyDialog());
			panel.initGUI();
			cardPanel.add(panel, element);
		}

		changeStep(STEP7);
	}

	/**
	 * Ferme la fenetre.
	 */
	private final void close() {
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}

	/**
	 * Imprime le bordereau d'accompagnement
	 */
	public final void imprimerBordereau() {
		RecouvrementProxyCtrl.imprimerBordereau(this, currentRecouvrement, getSelectedPrelevementParamBdf());
	}

	/**
	 * Imprime le contenu du recouvrement.
	 */
	public final void imprimerContenuRecouvrement() {
		RecouvrementProxyCtrl.imprimerContenuPrelevement(this, currentRecouvrement);
	}

	private final boolean isDateDansCalendrierBdf(final Date dateEchange) {
		return (calendrierBDF.indexOfObject(dateEchange) != NSArray.NotFound);

	}

	public final void regenererVirement() {
		//appeler factory pour le virement
		try {
			if (step7Listener.getDateValeur() == null) {
				throw new DataCheckException("Vous devez indiquer une date de valeur.");
			}
			if (dateValeur == null) {
				throw new DataCheckException("Vous devez indiquer une date d'échange.");
			}
			if (dateOperation == null) {
				throw new DataCheckException("Vous devez indiquer une date de réglement (date opération).");
			}

			//vérifier si les dates sont valides
			if (!isDateDansCalendrierBdf(dateValeur)) {
				if (!showConfirmationDialog("Confirmation", "La date d'échange que vous avez indiqué n'a pas été trouvée dans le calendrier de la BDF, souhaitez-vous quand même continuer ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}
			if (!isDateDansCalendrierBdf(dateOperation)) {
				if (!showConfirmationDialog("Confirmation", "La date de réglement que vous avez indiqué n'a pas été trouvée dans le calendrier de la BDF, souhaitez-vous quand même continuer ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}
			if (!dateOperation.equals(determinerDateOperation(dateValeur, step7Listener.getNbJoursDelai()))) {
				if (!showConfirmationDialog("Confirmation", "La date de prélèvement que vous avez indiqué n'est pas cohérente avec la date d'échange et la vitesse de prélèvement, souhaitez-vous quand même continuer ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}

			setWaitCursor(true);

			String contenu = null;

			final FactoryProcessPrelevementFichiersBdf factoryProcessPrelevementFichiers = new FactoryProcessPrelevementFichiersBdf(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

			final EOPrelevementParamBdf prelevementParamBdf = getSelectedPrelevementParamBdf();
			//String leContenuFichier = null;

			// ZLogger.debug(step1Panel.getSelectedDomaine());

			final EOTypeRecouvrement leNewTypeVirement = getSelectedTypeVirement();

			if (prelevementParamBdf == null) {
				throw new DefaultClientException("Le format du recouvrement n'est pas reconnu.");
			}

			currentRecouvrement.prelevements();

			contenu = factoryProcessPrelevementFichiers.genereContenuFichier(new NSTimestamp(step7Listener.getDateValeur()), new NSTimestamp(dateOperation), step7Listener.prelevementAccelere(), currentRecouvrement.prelevements(), prelevementParamBdf);
			if (contenu == null) {
				throw new DefaultClientException("Le contenu du fichier est vide.");
			}

			nbOperations = (contenu.length() / FactoryProcessPrelevementFichiersBdf.LONGUEUR_LIGNE_BDF) - 2;

			leContenuFichierVirement = contenu;

			//On enregistre le contenu du fichier dans la base
			NSTimestamp laDateValeur = new NSTimestamp(ZDateUtil.getDateOnly(step7Listener.getDateValeur()));
			NSTimestamp laDateReglement = new NSTimestamp(ZDateUtil.getDateOnly(step7Listener.getDateReglement()));

			NSTimestamp laDateCreation = new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime());
			ZLogger.debug("datevaleur", laDateValeur);

			final FactoryPrelevementFichier factorySauverVirement = new FactoryPrelevementFichier(myApp.wantShowTrace());
			factorySauverVirement.creerPrelevementFichier(getEditingContext(), currentRecouvrement, leNewTypeVirement, myApp.appUserInfo().getUtilisateur(), leContenuFichierVirement, laDateCreation, laDateValeur, laDateReglement, "Fichier de prélèvement", currentRecouvrement.recoMontant(),
					(Integer) currentRecouvrement.recoNb(), (Integer) currentRecouvrement.recoNumero(), step7Listener.getCodeOp());

			getEditingContext().saveChanges();

			prepareDefaultFile(currentRecouvrement);

			changeStep(STEP8);

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}

	}

	/**
	 * Determine la date d'operation en fonction de la date d'echange, du nombre de jours de delai et du calendrier de la BDF. Renvoi null si une des
	 * deux dates n'est pas trouvée
	 * 
	 * @param dateEchange
	 * @param nbJoursDelai
	 * @return
	 */
	private final Date determinerDateOperation(final Date dateEchange, final int nbJoursDelai) {
		final int i = calendrierBDF.indexOfObject(dateEchange);
		final int j = i + nbJoursDelai;
		if (i != NSArray.NotFound && j <= calendrierBDF.count() - 1) {
			return (Date) calendrierBDF.objectAtIndex(j);
		}
		return null;
	}

	private final boolean isDateEchangeDansCalendrierBdf(final Date dateEchange) {
		return (calendrierBDF.indexOfObject(dateEchange) != NSArray.NotFound);

	}

	/**
	 * @return renvoi le prelevementParam sélectionné (il faut le caster ensuite)
	 */
	public final EOEnterpriseObject getSelectedPrelevementParam() {
		final EOEnterpriseObject selected = (EOEnterpriseObject) step7Listener.getFormatsVirementModel().getSelectedEObject();

		ZLogger.debug(selected);

		return selected;
	}

	/**
	 * @return
	 */
	public final EOPrelevementParamBdf getSelectedPrelevementParamBdf() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeRecouvrement.TREC_FORMAT_BDF.equals(format)) {
			return (EOPrelevementParamBdf) getSelectedPrelevementParam();
		}
		return null;
	}

	public final EOTypeRecouvrement getSelectedTypeVirement() {
		final EOEnterpriseObject selected = getSelectedPrelevementParam();
		if (selected == null) {
			return null;
		}
		return (EOTypeRecouvrement) selected.valueForKey("typeRecouvrement");
	}

	public final String getSelectedFormat() {
		final EOTypeRecouvrement tv = getSelectedTypeVirement();
		if (tv == null) {
			return null;
		}
		return tv.trecFormat();
	}

	/**
	 * Enregistre le fichier
	 */
	public final void enregistrerFichier(final EORecouvrement virement) {
		System.out.println("RecouvrementCtrl.enregistrerFichier()");
		//nommer le fichier en fonction du numéro de virement
		String defaultFileName = "Fichier_" + getSelectedFormat() + "PREL" + "_" + ZStringUtil.extendWithChars("" + virement.recoNumero(), "0", 10, true) + ".txt";

		File f;
		try {

			if (leContenuFichierVirement == null || leContenuFichierVirement.length() == 0) {
				throw new DefaultClientException("Le contenu du fichier est vide !");
			}

			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(null);
			f = new File(new File(fileChooser.getCurrentDirectory() + defaultFileName).getCanonicalPath());
			fileChooser.setSelectedFile(f);

			TxtFileFilter filter1 = new TxtFileFilter("txt", "Fichier texte");
			filter1.setExtensionListInDescription(true);
			fileChooser.addChoosableFileFilter(filter1);
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.setFileFilter(filter1);
			fileChooser.setDialogTitle("Enregistrer-sous..");

			int ret = fileChooser.showSaveDialog(getMyDialog());
			File file = fileChooser.getSelectedFile();

			//	        System.out.println("ret="+ret);
			//	        System.out.println("file="+file);

			if ((ret != JFileChooser.APPROVE_OPTION) || (file == null)) {
				return;
			}

			if (file.exists()) {
				if (!showConfirmationDialog("Confirmation", "Le fichier " + file + " existe déjà, voulez-vous l'écraser ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}
			}

			FileWriter writer = new FileWriter(file);
			writer.write(leContenuFichierVirement);
			writer.close();

			System.out.println("Fichier " + file + " enregistré.");

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Change le panel actif (en effectuant un updateData sur le nouveau).
	 * 
	 * @param newStep
	 */
	private final void changeStep(final String newStep) {
		cardLayout.show(cardPanel, newStep);
		currentPanel = (ZKarukeraStepPanel2) stepPanels.get(newStep);
		try {
			currentPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	//    private final void next_STEP7() {
	//        try {
	//            ZLogger.debug("Format virement", step7Panel.getSelectedFormat());
	//            showinfoDialogFonctionNonImplementee();
	//        }
	//        catch (Exception e) {
	//            showErrorDialog(e);
	//        }
	//    }

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		//        ZKarukeraDialog win = new ZKarukeraDialog(dial, TITLE,true);
		setMyDialog(win);
		initGUI();
		//        step7Panel.setMyDialog(win);
		cardPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(cardPanel);
		win.pack();
		return win;
	}

	////////////////////////////////////////////////////////////////////////////////

	private class RecouvrementPanelStep7Listener implements RecouvrementStep7OptionsFichier.RecouvrementStep7OptionsFichierListener {
		public final String VITESSE_NORMALE_LIBELLE = "Normal (réglement à J+4)";
		public final String VITESSE_ACCELERE_LIBELLE = "Accéléré (réglement à J+2)";

		public final int NBJOURS_NORMAL = 4;
		public final int NBJOURS_ACCELERE = 2;

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final IZDatePickerFieldModel dateValeurModel;
		private final IZDatePickerFieldModel dateOperationModel;
		private ZEOComboBoxModel formatsVirementModel;
		private final DefaultComboBoxModel vitessePrelevementsModel;
		private final DocumentListener dateValeurListener;
		private final ActionCalculeDateOperation actionCalculeDateOperation = new ActionCalculeDateOperation();
		private final ActionAfficheCalendrierBdf actionAfficheCalendrierBdf = new ActionAfficheCalendrierBdf();
		private HashMap<String, Integer> vitessesPrelevementMap = new HashMap<String, Integer>();

		private final String libelleVitesseNormale;
		private final String libelleVitesseAcceleree;

		// private Date dateValeur;

		/**
         * 
         */
		public RecouvrementPanelStep7Listener() throws Exception {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionGenererFichier();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(true);
			actionSpecial1.setEnabled(true);
			dateValeurModel = new DateValeurModel();
			dateOperationModel = new DateOperationModel();

			String vitesseNormaleStr = (String) myApp.getParametres().valueForKey("org.cocktail.gfc.prelevement.vitesse.normale");
			String vitesseAccelereeStr = (String) myApp.getParametres().valueForKey("org.cocktail.gfc.prelevement.vitesse.acceleree");

			Integer vitesseNormale = null;
			Integer vitesseAcceleree = null;

			try {
				vitesseNormale = Integer.valueOf(vitesseNormaleStr);
				vitesseAcceleree = Integer.valueOf(vitesseAccelereeStr);
			} catch (Exception e) {
				e.printStackTrace();
				Exception e1 = new Exception("Erreur lors de la recuperation des parametres org.cocktail.gfc.prelevement.vitesse.normale et org.cocktail.gfc.prelevement.vitesse.acceleree : " + e.getMessage());
				throw e1;
				//showErrorDialog(e1);
			}

			vitessePrelevementsModel = new DefaultComboBoxModel();
			if (vitesseNormale != null && vitesseNormale.intValue() != 0) {
				libelleVitesseNormale = "Normal (réglement à J+" + vitesseNormale.intValue() + ")";
				vitessesPrelevementMap.put(libelleVitesseNormale, vitesseNormale);
				vitessePrelevementsModel.addElement(libelleVitesseNormale);
			}
			else {
				libelleVitesseNormale = null;
			}
			if (vitesseAcceleree != null && vitesseAcceleree.intValue() != 0) {
				libelleVitesseAcceleree = "Accéléré (réglement à J+" + vitesseAcceleree.intValue() + ")";
				vitessesPrelevementMap.put(libelleVitesseAcceleree, vitesseAcceleree);
				vitessePrelevementsModel.addElement(libelleVitesseAcceleree);
			}
			else {
				libelleVitesseAcceleree = null;
			}

			if (vitessesPrelevementMap.size() == 0) {
				throw new Exception("Aucune vitesse de prélèvement paramétrée valide (org.cocktail.gfc.prelevement.vitesse.normale ou org.cocktail.gfc.prelevement.vitesse.acceleree)");
			}
			//			
			//			
			//			vitessePrelevementsModel = new DefaultComboBoxModel();
			//			vitessePrelevementsModel.addElement(VITESSE_NORMALE_LIBELLE);
			//			vitessePrelevementsModel.addElement(VITESSE_ACCELERE_LIBELLE);

			dateValeurListener = new DocumentListener() {

				public void changedUpdate(DocumentEvent e) {
					dateOperation = null;

				}

				public void insertUpdate(DocumentEvent e) {
					dateOperation = null;

				}

				public void removeUpdate(DocumentEvent e) {
					dateOperation = null;

				}

			};

			try {
				final NSArray formatsPrelevements = EOsFinder.fetchArray(getEditingContext(), EOPrelevementParamBdf.ENTITY_NAME, "ppbEtat=%@ and typeRecouvrement.trecValidite=%@", new NSArray(new Object[] {
						EOPrelevementParamBdf.ETAT_VALIDE, EOTypeRecouvrement.ETAT_VALIDE
				}), null, false);
				formatsVirementModel = new ZEOComboBoxModel(formatsPrelevements, "libelle", null, null);

			} catch (Exception e) {
				formatsVirementModel = null;
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionCalculeDateOperation extends AbstractAction {
			public ActionCalculeDateOperation() {
				super("");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Calculer la date de prélèvement (par rapport au calendrier BDF)");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				dateOperation = determinerDateOperation(dateValeur, getNbJoursDelai());
				step7Panel.updateDataDateOperation();

			}

		}

		private final class ActionAfficheCalendrierBdf extends AbstractAction {
			public ActionAfficheCalendrierBdf() {
				super("");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Afficher le calendrier BDF");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				try {
					final String filePath = ReportFactoryClient.imprimerBdfCalendrier(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), getMyDialog());
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}

		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				//                next_STEP7();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				//                prev_STEP7();

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep7OptionsFichierListener.Step7Listener#dateValeurModel()
		 */
		public IZDatePickerFieldModel dateValeurModel() {
			return dateValeurModel;
		}

		public Date getDateValeur() {
			return dateValeur;
		}

		public void setDateValeur(Date adateValeur) {
			dateValeur = adateValeur;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.RecouvrementStep7OptionsFichierListener.Step7Listener#montantRecouvrement()
		 */
		public BigDecimal montantRecouvrement() {
			return (currentRecouvrement != null ? ZEOUtilities.calcSommeOfBigDecimals(currentRecouvrement.prelevements(), "prelevMontant") : null);
		}

		public ZEOComboBoxModel getFormatsVirementModel() {
			return formatsVirementModel;
		}

		public ComboBoxModel getVitessePrelevementsModel() {
			return vitessePrelevementsModel;
		}

		public boolean prelevementAccelere() {
			return VITESSE_ACCELERE_LIBELLE.equals(vitessePrelevementsModel.getSelectedItem());
		}

		public String getCodeOp() {
			return (VITESSE_ACCELERE_LIBELLE.equals(vitessePrelevementsModel.getSelectedItem()) ? FactoryProcessPrelevementFichiersBdf.CODE_VITESSE_ACCELERE : FactoryProcessPrelevementFichiersBdf.CODE_VITESSE_NORMAL);
		}

		public NSArray getPrelevements() {
			return currentRecouvrement.prelevements();
		}

		public Date getDateReglement() {
			return dateOperation;
			//            return ZDateUtil.addDHMS(getDateValeur(), (prelevementAccelere()? NBJOURS_ACCELERE: NBJOURS_NORMAL), 0, 0, 0);
		}

		public int getNbJoursDelai() {
			//return prelevementAccelere() ? NBJOURS_ACCELERE : NBJOURS_NORMAL;
			return vitessesPrelevementMap.get(vitessePrelevementsModel.getSelectedItem());
		}

		public IZDatePickerFieldModel dateOperationModel() {
			return dateOperationModel;
		}

		private final class DateValeurModel implements IZDatePickerFieldModel {
			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return dateValeur;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				System.out.println("DateValeurModel.setValue()");
				System.out.println(value);

				//On remet a jour la date d'operation
				dateOperation = null;
				if (value instanceof Date) {
					dateValeur = (Date) value;

				}
				else {
					dateValeur = null;
				}
			}

			public Window getParentWindow() {
				return getMyDialog();
			}

		}

		private final class DateOperationModel implements IZDatePickerFieldModel {
			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return dateOperation;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				System.out.println("DateOperationModel.setValue()");
				System.out.println(value);

				if (value instanceof Date) {
					dateOperation = (Date) value;
				}
				else {
					dateOperation = null;
				}
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

		public boolean isDateEchangeValide() {
			return isDateEchangeDansCalendrierBdf(dateValeur);
		}

		public Action actionCalculeDateOperation() {
			return actionCalculeDateOperation;
		}

		public DocumentListener dateValeurListener() {
			return dateValeurListener;
		}

		public Action actionAfficheCalendrierBdf() {
			return actionAfficheCalendrierBdf;
		}
	}

	//    private class RecouvrementPanelStep7Listener implements RecouvrementPanelStep7.Step7Listener {
	//        public final String VITESSE_NORMALE_LIBELLE="Normal (réglement à J+4)";
	//        public final String VITESSE_ACCELERE_LIBELLE="Accéléré (réglement à J+2)";
	//
	//        
	//        public final int NBJOURS_NORMAL = 4;
	//        public final int NBJOURS_ACCELERE = 2;
	//        
	//        
	//        private ZStepAction actionPrev;
	//        private ZStepAction actionNext;
	//        private ZStepAction actionSpecial1;
	//        private IZTextFieldModel dateValeurModel;
	//        private Date dateValeur;
	//        private ZEOComboBoxModel formatsVirementModel;
	//        private final DefaultComboBoxModel vitessePrelevementsModel;
	//
	//        /**
	//         *
	//         */
	//        public RecouvrementPanelStep7Listener() {
	//            actionPrev = new ActionPrev();
	//            actionNext = new ActionNext();
	//            actionSpecial1 = new DefaultActionGenererFichier();
	//            actionNext.setEnabled(false);
	//            actionPrev.setEnabled(true);
	//            actionSpecial1.setEnabled(true);
	//            dateValeurModel = new DateValeurModel();
	//            
	//            
	//            vitessePrelevementsModel = new DefaultComboBoxModel();
	//            vitessePrelevementsModel.addElement(VITESSE_NORMALE_LIBELLE);
	//            vitessePrelevementsModel.addElement(VITESSE_ACCELERE_LIBELLE);
	//            
	//            try {
	//                final NSArray formatsVirements = EOsFinder.fetchArray(getEditingContext(), EOPrelevementParamBdf.ENTITY_NAME, "ppbEtat=%@", new NSArray(EOPrelevementParamBdf.ETAT_VALIDE), null, false); 
	//                formatsVirementModel = new ZEOComboBoxModel(formatsVirements, "libelle", null, null);
	//            } catch (Exception e) {
	//                formatsVirementModel = null;
	//                showErrorDialog(e);
	//            }            
	//            
	//            
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ZKarukeraStepPanel2.ZStepListener#actionPrev()
	//         */
	//        public ZStepAction actionPrev() {
	//            return actionPrev;
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ZKarukeraStepPanel2.ZStepListener#actionNext()
	//         */
	//        public ZStepAction actionNext() {
	//            return actionNext;
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ZKarukeraStepPanel2.ZStepListener#actionClose()
	//         */
	//        public ZStepAction actionClose() {
	//            return actionClose;
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
	//         */
	//        public ZStepAction actionSpecial1() {
	//            return actionSpecial1;
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
	//         */
	//        public ZStepAction actionSpecial2() {
	//            return null;
	//        }
	//
	//        private final class ActionNext extends  DefaultActionNext {
	//
	//            /**
	//             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//             */
	//            public void actionPerformed(ActionEvent e) {
	////                next_STEP7();
	//
	//            }
	//
	//        }
	//        private final class ActionPrev extends  DefaultActionPrev {
	//
	//            /**
	//             * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//             */
	//            public void actionPerformed(ActionEvent e) {
	////                prev_STEP7();
	//
	//            }
	//
	//            /**
	//             * @see org.cocktail.maracuja.client.recouvrement.RecouvrementRegenererVirementCtrl.DefaultActionPrev#isVisible()
	//             */
	//            public boolean isVisible() {
	//                return false;
	//            }
	//
	//        }
	//
	//
	//
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementPanelStep7.Step7Listener#dateValeurModel()
	//         */
	//        public IZTextFieldModel dateValeurModel() {
	//            return dateValeurModel;
	//        }
	//
	//        public Date getDateValeur() {
	//            return dateValeur;
	//        }
	//        public void setDateValeur(Date dateValeur) {
	//            this.dateValeur = dateValeur;
	//        }
	//
	////        private final class DateValeurModel implements IZTextFieldModel {
	////            /**
	////             * @see org.cocktail.maracuja.client.zutil.ui.forms.ZTextField.IZTextFieldModel#getValue()
	////             */
	////            public Object getValue() {
	////                return dateValeur;
	////            }
	////
	////            /**
	////             * @see org.cocktail.maracuja.client.zutil.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
	////             */
	////            public void setValue(Object value) {
	////                if (value instanceof Date) {
	////                    dateValeur = (Date) value;
	////                }
	////                else {
	////                    dateValeur = null;
	////                }
	////            }
	////
	////        }
	//        private final class DateValeurModel implements IZTextFieldModel {
	//            /**
	//             * @see org.cocktail.maracuja.client.zutil.ui.forms.ZTextField.IZTextFieldModel#getValue()
	//             */
	//            public Object getValue() {
	//                return dateValeur;
	//            }
	//
	//            /**
	//             * @see org.cocktail.maracuja.client.zutil.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
	//             */
	//            public void setValue(Object value) {
	//                System.out.println("DateValeurModel.setValue()");
	//                System.out.println(value);
	//                
	//                if (value instanceof Date) {
	//                    dateValeur = (Date) value;
	//                } else {
	//                    dateValeur = null;
	//                }
	//                
	//                System.out.println(dateValeur);
	//                System.out.println("*******");
	//
	//            }
	//
	//        }   
	//
	//
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementPanelStep7.Step7Listener#montantRecouvrement()
	//         */
	//        public BigDecimal montantRecouvrement() {
	//            if (currentRecouvrement!=null) {
	//                return currentRecouvrement.recoMontant();
	//            }
	//            return null;
	//
	//        }
	//
	//        public ZEOComboBoxModel getFormatsVirementModel() {
	//            return formatsVirementModel;
	//        }
	//        public ComboBoxModel getVitessePrelevementsModel() {
	//            return vitessePrelevementsModel;
	//        }
	//        
	//        public boolean prelevementAccelere() {
	//            return VITESSE_ACCELERE_LIBELLE.equals( vitessePrelevementsModel.getSelectedItem());
	//        }
	//
	//        public NSArray getPrelevements() {
	//            return currentRecouvrement.prelevements();
	//        }
	//
	//        public Date getDateReglement() {
	//            return ZDateUtil.addDHMS(getDateValeur(), (prelevementAccelere()? NBJOURS_ACCELERE: NBJOURS_NORMAL), 0, 0, 0);
	//        }
	//        public String getCodeOp() {
	//            return (VITESSE_ACCELERE_LIBELLE.equals( vitessePrelevementsModel.getSelectedItem()) ? FactoryProcessPrelevementFichiersBdf.CODE_VITESSE_ACCELERE : FactoryProcessPrelevementFichiersBdf.CODE_VITESSE_NORMAL) ;
	//        }
	//
	//        public IZTextFieldModel dateOperationModel() {
	//            return null;
	//        }
	//
	//        public boolean isDateEchangeValide() {
	//            return false;
	//        }
	//
	//    }
	private class RecouvrementPanelStep8Listener implements RecouvrementStep8Recapitulatif.RecouvrementStep8RecapitulatifListener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final ActionImprimerBordereau actionImprimerBordereau;
		private final ActionImprimerContenuRecouvrement actionImprimerContenuRecouvrement;
		private final ActionEnregistrer actionEnregistrer;

		/**
         *
         */
		public RecouvrementPanelStep8Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new DefaultActionNext();
			actionSpecial1 = new DefaultActionGenererFichier();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(false);
			actionSpecial1.setEnabled(false);

			actionEnregistrer = new ActionEnregistrer();
			actionImprimerBordereau = new ActionImprimerBordereau();
			actionImprimerContenuRecouvrement = new ActionImprimerContenuRecouvrement();

			actionEnregistrer.setEnabled(true);
			actionImprimerBordereau.setEnabled(true);
			actionImprimerContenuRecouvrement.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep8Recapitulatif.RecouvrementStep8RecapitulatifListener#actionEnregistrer()
		 */
		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep8Recapitulatif.RecouvrementStep8RecapitulatifListener#actionImprimerBordereau()
		 */
		public Action actionImprimerBordereau() {
			return actionImprimerBordereau;
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep8Recapitulatif.RecouvrementStep8RecapitulatifListener#actionImprimerContenuRecouvrement()
		 */
		public Action actionImprimerContenuRecouvrement() {
			return actionImprimerContenuRecouvrement;
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementStep8Recapitulatif.RecouvrementStep8RecapitulatifListener#typeRecouvrement()
		 */
		public String typeRecouvrement() {
			String t = EOModeRecouvrement.MODDOM_ECHEANCIER;
			t = t + "_" + getSelectedFormat();
			//            t = t + "_" + step7Panel.getSelectedFormat();
			return t;
		}

		public File getFilePrelevement() {
			return defaultFile;
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementPanelStep8.Step8Listener#getFile()
		//         */
		//        public File getFile() {
		//            return defaultFile;
		//        }
		//
		//        /**
		//         * @see org.cocktail.maracuja.client.recouvrement.ui.RecouvrementPanelStep8.Step8Listener#prepareFichierDefaut()
		//         */
		//        public void prepareFichierDefaut() {
		//            enregistrerFichierDefaut(currentRecouvrement);
		//        }

	}

	public final File saveFile(final String contenu, final String fileName) throws Exception {
		if (contenu == null || contenu.length() == 0) {
			throw new DefaultClientException("Le contenu du fichier est vide !");
		}
		final File f = new File(new File(myApp.temporaryDir + fileName).getCanonicalPath());
		final FileWriter writer = new FileWriter(f);
		writer.write(contenu);
		writer.close();
		return f;
	}

	public final void prepareDefaultFile(final EORecouvrement virement) throws Exception {
		final String defaultFileName = "Fichier_" + getSelectedFormat() + "_" + ZStringUtil.extendWithChars("" + virement.recoNumero(), "0", 10, true) + ".txt";
		defaultFile = saveFile(leContenuFichierVirement, defaultFileName);
	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionGenererFichier extends ZStepAction {
		public DefaultActionGenererFichier() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Générer");
			putValue(Action.SHORT_DESCRIPTION, "Générer le fichier");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			regenererVirement();
		}

	}

	private class DefaultActionNext extends ZStepAction {
		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le Bordereau d'accompagnement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.NAME, "Enregistrer le fichier");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			enregistrerFichier(currentRecouvrement);

		}
	}

	public class ActionImprimerContenuRecouvrement extends AbstractAction {
		public ActionImprimerContenuRecouvrement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail du recouvrement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerContenuRecouvrement();

		}
	}

	/**
	 * @param myDialog
	 * @return
	 */
	public void createNewRegenererVirementInWindow(Window dial, EORecouvrement leRecouvrement) {
		ZKarukeraDialog win;
		win = createModalDialog(dial);

		try {
			currentRecouvrement = leRecouvrement;
			if (currentRecouvrement == null) {
				throw new DefaultClientException("Le recouvrement à regénérer est nul.");
			}
			if (!EOTypeRecouvrement.MOD_DOM_ECHEANCIER.equals(currentRecouvrement.typeRecouvrement().modDom())) {
				throw new DefaultClientException("Vous ne pouvez pas générer de virement pour les recouvrement qui ne sont pas des virements");
			}

			//            if ( FinderTypeVirement.leTypeVirementCaisse(getEditingContext()).equals(currentRecouvrement.typeVirement()) || FinderTypeVirement.leTypeVirementCheque(getEditingContext()).equals(currentRecouvrement.typeVirement()) ) {
			//                throw new DefaultClientException("Vous ne pouvez pas générer de virement pour les recouvrement de type " + EOTypeVirement.typeVirementCaisse +" ou " + EOTypeVirement.typeVirementCheque);
			//            }
			//            

			currentPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return;
	}

	private static class TxtFileFilter extends javax.swing.filechooser.FileFilter {

		//        private static String TYPE_UNKNOWN = "Type Unknown";
		//        private static String HIDDEN_FILE = "Hidden File";

		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		/**
		 * Creates a file filter. If no filters are added, then all files are accepted.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter() {
			filters = new Hashtable();
		}

		/**
		 * Creates a file filter that accepts files with the given extension. Example: new ExampleFileFilter("jpg");
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension) {
			this(extension, null);
		}

		/**
		 * Creates a file filter that accepts the given file type. Example: new ExampleFileFilter("jpg", "JPEG Image Images"); Note that the "."
		 * before the extension is not needed. If provided, it will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension, String description) {
			this();
			if (extension != null) {
				addExtension(extension);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Creates a file filter from the given string array. Example: new ExampleFileFilter(String {"gif", "jpg"}); Note that the "." before the
		 * extension is not needed adn will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters) {
			this(filters, null);
		}

		/**
		 * Creates a file filter from the given string array and description. Example: new ExampleFileFilter(String {"gif", "jpg"},
		 * "Gif and JPG Images"); Note that the "." before the extension is not needed and will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Return true if this file should be shown in the directory pane, false if it shouldn't. Files that begin with "." are ignored.
		 * 
		 * @see #getExtension
		 * @see FileFilter#accepts
		 */
		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		/**
		 * Return the extension portion of the file's name .
		 * 
		 * @see #getExtension
		 * @see FileFilter#accept
		 */
		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		/**
		 * Adds a filetype "dot" extension to filter against. For example: the following code will create a filter that filters out all files except
		 * those that end in ".jpg" and ".tif": ExampleFileFilter filter = new ExampleFileFilter(); filter.addExtension("jpg");
		 * filter.addExtension("tif"); Note that the "." before the extension is not needed and will be ignored.
		 */
		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		/**
		 * Returns the human readable description of this filter. For example: "JPEG and GIF Image Files (*.jpg, *.gif)"
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 * @see FileFilter#getDescription
		 */
		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description + " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "." + (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ." + (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				}
				else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		/**
		 * Sets the human readable description of this filter. For example: filter.setDescription("Gif and JPG Images");
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 */
		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		/**
		 * Determines whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description
		 * was provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see isExtensionListInDescription
		 */
		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		/**
		 * Returns whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description was
		 * provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see setExtensionListInDescription
		 */
		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return currentPanel;
	}

	public String title() {
		return TITLE;
	}

}
