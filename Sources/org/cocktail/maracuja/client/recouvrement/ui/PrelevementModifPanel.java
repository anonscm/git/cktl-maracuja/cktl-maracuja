/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZDefaultDataComponentModel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PrelevementModifPanel extends ZKarukeraPanel {
	private final IPrelevementModifPanelListener myListener;
	private final JComboBox ribCombobox;

	private final ZLabel noEcheance;
	private final ZLabel fournisseur;
	private final ZLabel montantPrevu;
	private final ZLabel montantTotal;
	private final ZLabel dateEcheance;
	private final ZDatePickerField dateEcheanceNew;
	private final ZNumberField montantPrevuNew;

	/**
     * 
     */
	public PrelevementModifPanel(IPrelevementModifPanelListener listener) {
		super();
		myListener = listener;

		ribCombobox = new JComboBox(myListener.getRibModel());

		montantTotal = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), EOPrelevement.ECHEANCIER_MONTANT_TOTAL_KEY), ZConst.FORMAT_DECIMAL);
		noEcheance = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), EOPrelevement.PRELEVEMENTNUMANDTOTAL_KEY));
		fournisseur = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), EOPrelevement.FOURNISSEUR_KEY + "." + EOFournisseur.NOM_AND_PRENOM_KEY));
		dateEcheance = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), EOPrelevement.PRELEV_DATE_PRELEVEMENT_KEY), ZConst.FORMAT_DATESHORT);
		montantPrevu = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), EOPrelevement.PRELEV_MONTANT_KEY), ZConst.FORMAT_DECIMAL);

		dateEcheanceNew = new ZDatePickerField(new DateEcheanceFieldModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		montantPrevuNew = new ZNumberField(new ZNumberField.BigDecimalFieldModel(myListener.getValues(), IPrelevementModifPanelListener.PRELEVEMENT_MONTANT_NEW_KEY),
				ZConst.DECIMAL_EDIT_FORMATS
				, ZConst.FORMAT_DECIMAL);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildFormPanel(), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		//        updateInputMap();
	}

	private final JPanel buildFormPanel() {
		final ArrayList list = new ArrayList();
		final Component separator = Box.createRigidArea(new Dimension(4, 1));
		list.add(new Component[] {
				new JLabel("Client : "), fournisseur
		});
		list.add(new Component[] {
				new JLabel("N° échéance : "), noEcheance
		});
		list.add(new Component[] {
				new JLabel("Date prévue : "), ZUiUtil.buildBoxLine(new Component[] {
						dateEcheance, separator, dateEcheanceNew, new JPanel(new BorderLayout())
				})
		});
		list.add(new Component[] {
				new JLabel("Montant de l'échéancier : "), montantTotal
		});
		list.add(new Component[] {
				new JLabel("Montant de l'échéance : "), ZUiUtil.buildBoxLine(new Component[] {
						montantPrevu, separator, montantPrevuNew, new JPanel(new BorderLayout())
				})
		});
		list.add(new Component[] {
				new JLabel("Nouveau rib : "), ribCombobox
		});

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		fournisseur.updateData();
		noEcheance.updateData();
		dateEcheance.updateData();
		montantPrevu.updateData();
		dateEcheanceNew.updateData();
		montantPrevuNew.updateData();
		montantTotal.updateData();
	}

	public interface IPrelevementModifPanelListener {
		public static final String PRELEVEMENT_DATE_NEW_KEY = "prelDateNew";
		public static final String PRELEVEMENT_MONTANT_NEW_KEY = "prelMontantNew";

		public Action actionClose();

		public Action actionValider();

		public Map getValues();

		/** Modele pour la liste des depenses papier */
		public ZEOComboBoxModel getRibModel();
	}

	private final class DateEcheanceFieldModel implements IZDatePickerFieldModel {

		public Object getValue() {
			return myListener.getValues().get(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				value = new NSTimestamp((Date) value);
			}
			myListener.getValues().put(IPrelevementModifPanelListener.PRELEVEMENT_DATE_NEW_KEY, value);
			//updateDataExcept(dateDepartDgp);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

}
