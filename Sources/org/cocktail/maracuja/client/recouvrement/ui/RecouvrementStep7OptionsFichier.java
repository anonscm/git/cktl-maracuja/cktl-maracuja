/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.DateFormat;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSArray;

/**
 * Panel affichant le choix du type de virement.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementStep7OptionsFichier extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Veuillez sélectionner le format du fichier de prélèvement à générer ainsi que la date d'échange et la date de réglement";
	private static final String COMMENTAIRE =
			"Le délai entre ces deux dates correspond au nombre de jours ouvrés de la BDF Si vous sélectionnez comme date d'échange un vendredi, pour un prélèvement normal (J+4), \n" +
					"la date de réglement doit être le jeudi suivant et non le mardi. Utilisez le bouton pour calculer la date à partir du calendrier de la BDF.";

	public static final String FORMAT_NAME_BDF = "Disquette Banque de France";

	private RecouvrementStep7OptionsFichierListener myListener;

	private ZDatePickerField dateValeurField;
	private ZDatePickerField dateOperationField;
	private ZNumberField montantRecouvrementField;

	private JCheckBox grouperLignesParRibField;
	private JComboBox formatsVirement;
	private JComboBox vitessePrelevements;
	private JLabel labelDateReglement = new JLabel();

	//    private JComboBox 

	/**
	 * @param listener
	 */
	public RecouvrementStep7OptionsFichier(RecouvrementStep7OptionsFichierListener listener) {
		super(listener);
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		final Box col1 = Box.createVerticalBox();
		col1.add(buildInfosRecouvrementPanel());

		final JPanel panel2 = new JPanel(new BorderLayout());
		panel2.add(new JPanel(new BorderLayout()), BorderLayout.WEST);
		panel2.add(new JPanel(new BorderLayout()), BorderLayout.EAST);
		panel2.add(col1, BorderLayout.CENTER);

		p.add(panel2, BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return p;
	}

	private final JPanel buildInfosRecouvrementPanel() {
		final ZFormPanel p = new ZFormPanel();
		JLabel l = new JLabel("Montant du recouvrement");
		p.add(l);
		p.add(montantRecouvrementField);

		final ZFormPanel p2 = new ZFormPanel();
		p2.add(new JLabel("Date d'échange (SIT) / J"));
		p2.add(dateValeurField);

		final ZFormPanel p22 = new ZFormPanel();
		p22.add(new JLabel("Date de réglement"));
		p22.add(dateOperationField);

		final JButton b = ZKarukeraPanel.getButtonFromAction(myListener.actionCalculeDateOperation());
		b.setHorizontalAlignment(SwingConstants.CENTER);
		p22.add(b);

		final JButton b1 = ZKarukeraPanel.getButtonFromAction(myListener.actionAfficheCalendrierBdf());
		b1.setHorizontalAlignment(SwingConstants.CENTER);
		p22.add(b1);

		labelDateReglement.setFont(getFont().deriveFont(Font.BOLD));

		final ZLabeledComponent p3 = new ZLabeledComponent("Format du prélèvement ", formatsVirement, ZLabeledComponent.LABELONLEFT, -1);
		final ZLabeledComponent p4 = new ZLabeledComponent("Vitesse ", vitessePrelevements, ZLabeledComponent.LABELONLEFT, -1);
		//        final ZLabeledComponent p5 = new ZLabeledComponent("Réglement le  ", labelDateReglement, ZLabeledComponent.LABELONLEFT, -1);

		return ZKarukeraPanel.buildVerticalPanelOfComponent(new Component[] {
				p, p3, p2, p4, p22
		});
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		//        formatRadios = buildRadiosFormatPanel();

		formatsVirement = new JComboBox(myListener.getFormatsVirementModel());
		vitessePrelevements = new JComboBox(myListener.getVitessePrelevementsModel());

		dateValeurField = new ZDatePickerField(myListener.dateValeurModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateValeurField.getMyTexfield().setColumns(8);
		dateValeurField.getMyTexfield().getDocument().addDocumentListener(myListener.dateValeurListener());

		dateOperationField = new ZDatePickerField(myListener.dateOperationModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateOperationField.getMyTexfield().setColumns(8);

		montantRecouvrementField = new ZNumberField(new MontantProvider(), null, ZConst.FORMAT_DECIMAL);
		montantRecouvrementField.setUIReadOnly();
		montantRecouvrementField.getMyTexfield().setEditable(false);
		montantRecouvrementField.getMyTexfield().setColumns(10);

		vitessePrelevements.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					updateData();
				}
				catch (Exception e1) {
				}
			}

		});

		super.initGUI();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		montantRecouvrementField.updateData();
		dateValeurField.updateData();
		updateDataDateOperation();
	}

	public void updateDataDateOperation() {
		dateOperationField.updateData();
	}

	public final boolean grouperLignesParRib() {
		return grouperLignesParRibField.isSelected();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface RecouvrementStep7OptionsFichierListener extends ZStepListener {
		public IZDatePickerFieldModel dateValeurModel();

		public DocumentListener dateValeurListener();

		public IZDatePickerFieldModel dateOperationModel();

		public ComboBoxModel getVitessePrelevementsModel();

		public BigDecimal montantRecouvrement();

		public ZEOComboBoxModel getFormatsVirementModel();

		public NSArray getPrelevements();

		public boolean isDateEchangeValide();

		public Action actionCalculeDateOperation();

		public Action actionAfficheCalendrierBdf();
	}

	private final class MontantProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.montantRecouvrement();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
		}

	}

	public String getVitesseTraitement() {
		return (String) vitessePrelevements.getSelectedItem();
	}

}
