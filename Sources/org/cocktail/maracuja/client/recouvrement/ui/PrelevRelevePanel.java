/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.recouvrement.ui.PrelevementSelectList.PrelevementSelecListListener;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PrelevRelevePanel extends ZKarukeraPanel {
    private IPrelevRelevePanelListener myListener;

    private PrelevementSelectList prelevementPanelSelect;
    private final RejetesTotalModel rejetesTotalModel = new RejetesTotalModel();
    private final RejetesNbModel rejetesNbModel = new RejetesNbModel();
    private final AcceptesTotalModel acceptesTotalModel = new AcceptesTotalModel();
    private final AcceptesNbModel acceptesNbModel = new AcceptesNbModel();

    private ZPanelNbTotal panelTotalRejetes;
    private ZPanelNbTotal panelTotalAcceptes;

    private ZFormPanel pcoNum;
    private ZLookupButton pcoSelectButton;
    private ZFormPanel pcoNumRejet;
    private ZLookupButton pcoSelectButtonRejet;

    /**
     * @param editingContext
     */
    public PrelevRelevePanel(IPrelevRelevePanelListener listener) {
        super();
        myListener = listener;
        pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());
        pcoSelectButtonRejet = new ZLookupButton(myListener.getLookupButtonCompteRejetModel(), myListener.getLookupButtonCompteRejetListener());
        prelevementPanelSelect = new PrelevementSelectList(myListener.getPrelevementPanelSelectListener());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        
        pcoSelectButton.initGUI();
        pcoSelectButtonRejet.initGUI();
        prelevementPanelSelect.initGUI();

        this.setLayout(new BorderLayout());

        this.add(buildRightPanel(), BorderLayout.EAST);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);
        this.add(getCenterPanel(), BorderLayout.CENTER);
        this.add(buildTopPanel(), BorderLayout.NORTH);
    }

    private final JPanel buildTopPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("Relevé de prélèvements",
                "<html>Cochez les prélèvements de cette liste qui ont été rejetés, sélectionnez le compte de rejet, puis cliquez sur le bouton <b>Valider</b>." +
                "<br>Une écriture de rejet sera créée pour chaque prélèvement que vous avez coché.</html>",null);
        return commentPanel;        
    }    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        prelevementPanelSelect.updateData();
        updateDataTotaux();
    }

    
    
    public JPanel getCenterPanel() {
        panelTotalRejetes = new ZPanelNbTotal("Rejetés");
        panelTotalRejetes.setTotalProvider(rejetesTotalModel);
        panelTotalRejetes.setNbProvider(rejetesNbModel); 
        panelTotalAcceptes = new ZPanelNbTotal("Acceptés");
        panelTotalAcceptes.setTotalProvider(acceptesTotalModel);
        panelTotalAcceptes.setNbProvider(acceptesNbModel);
        
        
        final JPanel bas = new JPanel(new BorderLayout());
        ZKarukeraPanel.buildVerticalPanelOfComponent(new Component[]{panelTotalAcceptes, panelTotalRejetes});
        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(bas, BorderLayout.SOUTH);
        p.add(encloseInPanelWithTitle("Prélèvements",null,ZConst.BG_COLOR_TITLE,prelevementPanelSelect,null, null), BorderLayout.CENTER);
        return p;
    }
    
    
    private final JPanel buildRightPanel() {
        
        
        pcoNum = ZFormPanel.buildLabelField("Compte TG", new ZTextField.DefaultTextFieldModel(myListener.getValues(), "pcoNum") );
        ((ZTextField)pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(10);
        
        pcoNumRejet = ZFormPanel.buildLabelField("Compte rejet", new ZTextField.DefaultTextFieldModel(myListener.getValues(), "pcoNumRejet") );
        ((ZTextField)pcoNumRejet.getMyFields().get(0)).getMyTexfield().setColumns(10);
        
        JPanel panelPco = buildLine(new Component[]{pcoNum, pcoSelectButton, Box.createHorizontalStrut(15)});
        JPanel panelPcoRejet = buildLine(new Component[]{pcoNumRejet, pcoSelectButtonRejet, Box.createHorizontalStrut(15)});
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));

        JButton bt = ZKarukeraPanel.getButtonFromAction(myListener.actionEmargerReleve());
        
        
        ArrayList list = new ArrayList();
        list.add(panelPco);
        list.add(panelPcoRejet);        
        list.add(bt);        
        
        tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(list), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        
        
        
        return tmp;
    }

    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionClose());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }



    private final class RejetesTotalModel implements ZLabelTextField.IZLabelTextFieldModel {
        public Object getValue() {
            return  myListener.getRejetesTotal();
        }

        public void setValue(Object value) {
            return;
        }
    }

    private final class RejetesNbModel implements ZLabelTextField.IZLabelTextFieldModel {
        public Object getValue() {
            return myListener.getRejetesNb() ;
        }

        public void setValue(Object value) {
            return;
        }
    }  
    
    private final class AcceptesTotalModel implements ZLabelTextField.IZLabelTextFieldModel {
        public Object getValue() {
            return  myListener.getAcceptesTotal();
        }
        
        public void setValue(Object value) {
            return;
        }
    }
    
    private final class AcceptesNbModel implements ZLabelTextField.IZLabelTextFieldModel {
        public Object getValue() {
            return myListener.getAcceptesNb() ;
        }
        
        public void setValue(Object value) {
            return;
        }
    }  



    public interface IPrelevRelevePanelListener {
        public Action actionClose();
        public IZLookupButtonListener getLookupButtonCompteRejetListener();
        public IZLookupButtonModel getLookupButtonCompteRejetModel();
        public IZLookupButtonModel getLookupButtonCompteModel();
        public IZLookupButtonListener getLookupButtonCompteListener();
        public Map getValues();
        public Integer getRejetesNb();
        public BigDecimal getRejetesTotal();
        public Integer getAcceptesNb();
        public BigDecimal getAcceptesTotal();
        public Action actionEmargerReleve();
        public IZEOTableCellRenderer getTableCellRenderer();
        public void onSelectionChanged();
        public PrelevementSelecListListener getPrelevementPanelSelectListener();

    }



    public void updateDataTotaux() {
        panelTotalRejetes.updateData();
        panelTotalAcceptes.updateData();
    }

    public final ZFormPanel getPcoNum() {
        return pcoNum;
    }

    public final ZLookupButton getPcoSelectButton() {
        return pcoSelectButton;
    }
    
    public final ZFormPanel getPcoNumRejet() {
        return pcoNumRejet;
    }
    
    public final ZLookupButton getPcoSelectButtonRejet() {
        return pcoSelectButtonRejet;
    }



}
