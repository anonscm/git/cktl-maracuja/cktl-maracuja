/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;


import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.zutil.client.ui.ZRadioButton;
import org.cocktail.zutil.client.ui.ZRadioButtonPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSArray;


/**
 * Panel qui affiche la sélection du type de recouvrement (Virement, chèquen caisse)
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementStep1TypeRecouvrement extends ZKarukeraStepPanel2   {
    private static final String TITLE="Type de recouvrement";
    private static final String COMMENTAIRE="Veuiilez sélectionner le type de recouvrement que vous souhaitez générer.";
    
    private Step1Listener myListener;
    private ZRadioButtonPanel domainesRadios;
    private HashMap domainesObjects;
    private JComboBox myComptabiliteBox;
    
    /**
     * @param listener
     */
    public RecouvrementStep1TypeRecouvrement(Step1Listener listener) {
        super(listener);
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
     */
    public JPanel getCenterPanel() {
        return buildContentPanel();
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        domainesRadios.setselectedObject(myListener.getSelectedDomaine());
//        String svirements = Step1Listener.PRELEVEMENTS +" : " + ((HashMap)myListener.getNumOfObjByDomaine().get(EOModeRecouvrement.MODOM_ECHEANCIER)).get(Step1Listener.PRELEVEMENTS); 
        ((ZRadioButton.ZRadioButtonModel)domainesRadios.getButtonByObject(EOModeRecouvrement.MODDOM_ECHEANCIER).getModel()).setLibelle("Echéancier");
        domainesRadios.updateData();
        
    }
    
    
    
    public interface Step1Listener extends ZStepListener {
        public static final String PRELEVEMENTS="Echéances de prélèvement";
        /** Doit renvoyer le domaine à preselectionner */
        public String getSelectedDomaine();
        
        /** Doit renvoyer un dictionnaire avec en clé les domaines et en valeurs une autre dictionnaire avec en clés l'identifiant de l'objet (MANDATS, TITRES,ODPS - utiliser les constantes de cette interface) et en valeur le nombre d'objets restant à payer*/
        public HashMap getNumOfObjByDomaine();
        
        /** Doit renvoyer un dictionnaire avec en clé les domaines et en valeurs le montant restant à payer*/
        public HashMap getMontantByDomaine();

        /**
         * @return
         */
        public NSArray getcomptabilites();

        /**
         * @return
         */
        public ZEOComboBoxModel getcomptabiliteModel();
    }
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
     */
    public void initGUI() {
        domainesRadios = buildRadiosDomainePanel();
        super.initGUI();
        
        
    }
    
    
    
    private JPanel buildContentPanel() {
        Box box1 = Box.createVerticalBox();
        box1.add(Box.createVerticalStrut(60));
        box1.add(buildComptabilitePanel());
        box1.add(Box.createVerticalStrut(60));
        
        Box box = Box.createVerticalBox();
        box.add(Box.createVerticalGlue());
        box.add(domainesRadios);
        box.add(Box.createVerticalGlue());
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(box1, BorderLayout.NORTH);
        p.add(box, BorderLayout.CENTER);
        return p;
    }

    
    public String getSelectedDomaine() {
        return (String) domainesRadios.getSelectedObject();
    }
    public final EOComptabilite getSelectedComptabilite() {
        return (EOComptabilite) myListener.getcomptabiliteModel().getSelectedEObject();
    }
    
    
    
    
    
    private final ZRadioButtonPanel buildRadiosDomainePanel() {
        domainesObjects=new HashMap(2,1);
        domainesObjects.put( EOModeRecouvrement.MODDOM_ECHEANCIER, "Echéancier");
        return new ZRadioButtonPanel(domainesObjects, null,domainesObjects.values().toArray()[0], SwingConstants.VERTICAL);
    }
    
    
    private final JPanel buildComptabilitePanel() {
        JPanel p = new JPanel(new BorderLayout());
        try {
            
            myComptabiliteBox = new JComboBox(myListener.getcomptabiliteModel()   );
            p.add( new ZLabeledComponent("Comptabilité", myComptabiliteBox, ZLabeledComponent.LABELONLEFT, 150));
        } catch (Exception e) {
            showErrorDialog(e);
        }   
        return p;

    }
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
     */
    public String getTitle() {
        return TITLE;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
     */
    public String getCommentaire() {
        return COMMENTAIRE;
    }
    


}
