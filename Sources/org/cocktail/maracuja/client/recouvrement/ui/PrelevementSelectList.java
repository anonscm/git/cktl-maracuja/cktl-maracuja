/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOEcheancier;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

import com.webobjects.eointerface.EODisplayGroup;

/**
 * Affiche un panel de sélection de prélèvements (attente ou pour relevé)
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PrelevementSelectList extends ZTablePanel {
	private JPopupMenu myPopupMenu;
	private PrelevementSelecListListener myListener;
	protected ZEOTableModelColumn colRib;
	protected ZEOTableModelColumn colDateReception;
	protected ZEOTableModelColumn col4;
	protected ZEOTableModelColumn col2;
	protected ZEOTableModelColumn col11;
	protected ZEOTableModelColumn colBord;
	protected ZEOTableModelColumn col12;
	protected ZEOTableModelColumn colExer;
	protected ZEOTableModelColumn col1;
	protected MyTableModelColumnDico col0;

	/**
     * 
     */
	public PrelevementSelectList(PrelevementSelecListListener listener) {
		super(listener);
		myListener = listener;

		col0 = new MyTableModelColumnDico(myDisplayGroup, "  ", 60, myListener.getAllCheckedPrelevements());
		col0.setEditable(true);
		col0.setResizable(false);
		col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher

		col1 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.PRELEVEMENTNUMANDTOTAL_KEY, "N°", 50);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);
		col1.setTableCellRenderer(myListener.getTableCellRenderer());

		colExer = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, "Exercice", 60);
		colExer.setAlignment(SwingConstants.CENTER);
		colExer.setColumnClass(Integer.class);
		colExer.setTableCellRenderer(myListener.getTableCellRenderer());

		col12 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.LIBELLE_KEY, "Libellé", 150);
		col12.setAlignment(SwingConstants.CENTER);
		col12.setTableCellRenderer(myListener.getTableCellRenderer());

		//        colBord = new ZEOTableModelColumn(myDisplayGroup,  EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY,"Bord.",60);
		colBord = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.BORDEREAU_ASSOCIE_KEY + "." + EOBordereau.BOR_NUM_KEY, "Bord.", 60);
		colBord.setAlignment(SwingConstants.CENTER);
		colBord.setTableCellRenderer(myListener.getTableCellRenderer());

		col11 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.ECHEANCIER_KEY + "." + EOEcheancier.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.TIT_NUMERO_KEY, "Titre", 60);
		col11.setAlignment(SwingConstants.CENTER);
		col11.setTableCellRenderer(myListener.getTableCellRenderer());

		col2 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.CLIENTNOMANDPRENOM_KEY, "Client", 173);
		col2.setAlignment(SwingConstants.CENTER);
		col2.setTableCellRenderer(myListener.getTableCellRenderer());
		//      ZEOTableModelColumn col31 = new ZEOTableModelColumn(myDisplayGroup,"manNbPiece","Pièces",48);
		//      col31.setAlignment(SwingConstants.CENTER);

		col4 = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.PRELEV_MONTANT_KEY, "Montant", 80);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);
		col4.setTableCellRenderer(myListener.getTableCellRenderer());

		colDateReception = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.DATEPRELEVEMENTCORRIGEE_KEY, "Date prévue", 60);
		colDateReception.setAlignment(SwingConstants.CENTER);
		colDateReception.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colDateReception.setColumnClass(Date.class);
		colDateReception.setTableCellRenderer(myListener.getTableCellRenderer());

		colRib = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.RIB_KEY + "." + EORib.RIBCOMPLET_KEY, "Rib", 90);
		colRib.setAlignment(SwingConstants.CENTER);
		colRib.setTableCellRenderer(myListener.getTableCellRenderer());

		colsMap.clear();
		colsMap.put("col0", col0);
		colsMap.put("colDateReception", colDateReception);
		colsMap.put("colExer", colExer);
		colsMap.put("colBord", colBord);
		colsMap.put("col11", col11);
		colsMap.put("col12", col12);
		colsMap.put("col1", col1);
		colsMap.put("col2", col2);
		colsMap.put("colRib", colRib);
		colsMap.put("col4", col4);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		super.initGUI();
		initPopupMenu();
	}

	private final void initPopupMenu() {
		myPopupMenu = new JPopupMenu();
		myPopupMenu.add(new ActionCheckAll());
		myPopupMenu.add(new ActionUncheckAll());
		myEOTable.setPopup(myPopupMenu);
	}

	public interface PrelevementSelecListListener extends ZTablePanel.IZTablePanelMdl {
		public HashMap getAllCheckedPrelevements();

		public IZEOTableCellRenderer getTableCellRenderer();

		public void afterCheck();
	}

	private final class ActionCheckAll extends AbstractAction {
		public ActionCheckAll() {
			super("Tout cocher");
		}

		public void actionPerformed(ActionEvent e) {
			//			((ZEOTableModelColumnDico)myCols.get(0)).setValueForAllRows(Boolean.TRUE);
			((ZEOTableModelColumnDico) colsMap.get("col0")).setValueForAllRows(Boolean.TRUE);
			myTableModel.fireTableDataChanged();
		}
	}

	private final class ActionUncheckAll extends AbstractAction {
		public ActionUncheckAll() {
			super("Tout décocher");
		}

		public void actionPerformed(ActionEvent e) {
			//			((ZEOTableModelColumnDico)myCols.get(0)).setValueForAllRows(Boolean.FALSE);
			((ZEOTableModelColumnDico) colsMap.get("col0")).setValueForAllRows(Boolean.FALSE);
			myTableModel.fireTableDataChanged();
		}
	}

	private final class MyTableModelColumnDico extends ZEOTableModelColumnDico {

		public MyTableModelColumnDico(EODisplayGroup vDg, String vTitle, int vpreferredWidth, HashMap aDico) {
			super(vDg, vTitle, vpreferredWidth, aDico);
		}

		public void setValueAtRow(final Object value, final int row) {
			super.setValueAtRow(value, row);
			myListener.afterCheck();

		}

	}

}
