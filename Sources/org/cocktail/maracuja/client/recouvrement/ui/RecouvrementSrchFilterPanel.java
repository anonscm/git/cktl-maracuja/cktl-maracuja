/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementSrchFilterPanel extends ZKarukeraPanel {
    private final Color BORDURE_COLOR=getBackground().brighter();

    private IRecouvrementSrchFilterPanel myListener;

    private ZFormPanel numerosPanel;
    private ZFormPanel datesPanel;
    private ZFormPanel montantHtsPanel;



    /**
     * @param editingContext
     */
    public RecouvrementSrchFilterPanel(IRecouvrementSrchFilterPanel listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(new JButton(myListener.getActionSrch() ), BorderLayout.EAST);
    }





    private final JPanel buildFilters() {
        numerosPanel = buildNumeroFields();
        datesPanel = buildDateFields();
        montantHtsPanel = buildHtFields();

        numerosPanel.setDefaultAction(myListener.getActionSrch());
        datesPanel.setDefaultAction(myListener.getActionSrch());
        montantHtsPanel.setDefaultAction(myListener.getActionSrch());

        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraPanel.buildLine(new Component[]{numerosPanel, datesPanel, montantHtsPanel }), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;
    }


    /**
     * @return
     */
    private final ZFormPanel buildNumeroFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= N° <=", new NumeroMinModel(), new NumeroMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER );
    }



    private final ZFormPanel buildDateFields() {
        return ZFormPanel.buildFourchetteDateFields("<= Date <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16) );
    }


    private final ZFormPanel buildHtFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"recoMontantMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"recoMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        numerosPanel.updateData();
    }







    public interface IRecouvrementSrchFilterPanel {

        /**
         * @return
         */
        public Action getActionSrch();

        /**
         * @return un dictionnaire dans lequel sont stockés les filtres.
         */
        public HashMap getFilters();

    }


    private final class NumeroMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("recoNumeroMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("recoNumeroMin",value);

        }

    }
    private final class NumeroMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("recoNumeroMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("recoNumeroMax",value);

        }

    }



    private final class MontantHtMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("recoMontantMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("recoMontantMin",value);

        }

    }
    private final class MontantHtMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("recoMontantMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("recoMontantMax",value);

        }

    }


    private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("recoDateCreationMax");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("recoDateCreationMax", value);
        }

        public Window getParentWindow() {
            return getMyDialog();
        }
    }

    private final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("recoDateCreationMin");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("recoDateCreationMin", value);
        }

        public Window getParentWindow() {
            return getMyDialog();
        }        
    }



}
