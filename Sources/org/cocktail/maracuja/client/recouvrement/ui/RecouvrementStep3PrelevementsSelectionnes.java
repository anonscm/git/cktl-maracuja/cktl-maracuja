/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;

import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.foundation.NSArray;

/**
 * Etape correspondant à l'affichage des prelevements selectionnes.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementStep3PrelevementsSelectionnes extends ZKarukeraStepPanel2 {
	private static final long serialVersionUID = 1L;
	private static final String TITLE = "Les prélèvements sélectionnés";
	private static final String COMMENTAIRE = "Liste des prélèvements qui vont être effectués.";

	private RecouvrementStep3PrelevementsSelectionnesListener myListener;
	private PrelevementSelectionnesList recouvrementPanelPrelevementOk;
	private ZPanelNbTotal panelTotal1;

	//    private RecouvrementPanelDepenseList recouvrementPanelDepenseList;
	//    private RecouvrementPanelRetenueDetail recouvrementPanelRetenueDetail;

	/**
	 * @param listener
	 */
	public RecouvrementStep3PrelevementsSelectionnes(RecouvrementStep3PrelevementsSelectionnesListener listener) {
		super(listener);
		myListener = listener;
		recouvrementPanelPrelevementOk = new PrelevementSelectionnesList(new RecouvrementPanelPrelevementOkListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des prelevements sélectionnés", null, ZConst.BG_COLOR_TITLE, recouvrementPanelPrelevementOk, null, null);
		panelTotal1 = new ZPanelNbTotal("Total à recouvrer");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());

		JPanel p = new JPanel(new BorderLayout());
		p.add(p1, BorderLayout.CENTER);
		p.add(panelTotal1, BorderLayout.SOUTH);

		//        p.add(buildRightPanel(), BorderLayout.EAST);
		return p;
	}

	private final class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(myListener.getPrelevements(), "prelevMontant");
			return total;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			//  int i=0;
			return new Integer(myListener.getPrelevements().count());
		}

		public void setValue(Object value) {
			return;
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		recouvrementPanelPrelevementOk.initGUI();
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		recouvrementPanelPrelevementOk.updateData();
		panelTotal1.updateData();

	}

	public EOPrelevement getSelectedPrelevement() {
		return recouvrementPanelPrelevementOk.getSelectedObject();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface RecouvrementStep3PrelevementsSelectionnesListener extends ZStepListener {
		public NSArray getPrelevements();
	}

	private final class RecouvrementPanelPrelevementOkListener implements PrelevementSelectionnesList.PrelevementSelectionnesListListener {

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.PrelevementSelectionnesList.PrelevementSelectionnesListListener#getPrelevements()
		 */
		public NSArray getPrelevements() {
			return myListener.getPrelevements();
		}

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.PrelevementSelectionnesList.PrelevementSelectionnesListListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

		}

	}

}
