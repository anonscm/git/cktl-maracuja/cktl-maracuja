/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * Panel liste de prelevements.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PrelevementSelectionnesList extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {

	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;
	private TableSorter myTableSorter;
	private Vector myCols;

	private JPopupMenu myPopupMenu;

	private PrelevementSelectionnesListListener myListener;

	/**
     * 
     */
	public PrelevementSelectionnesList(PrelevementSelectionnesListListener listener) {
		super();
		myListener = listener;
		myDisplayGroup = new EODisplayGroup();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		initTableModel();
		initTable();
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4));
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myDisplayGroup.setObjectArray(myListener.getPrelevements());
		myEOTable.updateData();
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);

		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		//		initPopupMenu();
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private final void initTableModel() {
		myCols = new Vector(9, 1);

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "prelevementNumAndTotal", "N°", 50);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);

		ZEOTableModelColumn colExer = new ZEOTableModelColumn(myDisplayGroup, "echeancier.exercice.exeExercice", "Exercice", 60);
		colExer.setAlignment(SwingConstants.CENTER);
		colExer.setColumnClass(Integer.class);

		ZEOTableModelColumn col12 = new ZEOTableModelColumn(myDisplayGroup, "echeancier.libelle", "Libellé", 150);
		col12.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colBord = new ZEOTableModelColumn(myDisplayGroup, "echeancier.recette.titre.bordereau.borNum", "Bord.", 60);
		colBord.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup, "echeancier.recette.titre.titNumero", "Titre", 60);
		col11.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, "clientNomAndPrenom", "Client", 173);
		//      ZEOTableModelColumn col31 = new ZEOTableModelColumn(myDisplayGroup,"manNbPiece","Pièces",48);
		//      col31.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "prelevMontant", "Montant", 80);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colDateReception = new ZEOTableModelColumn(myDisplayGroup, "datePrelevementCorrigee", "Date prévue", 60);
		colDateReception.setAlignment(SwingConstants.CENTER);
		colDateReception.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colDateReception.setColumnClass(Date.class);

		ZEOTableModelColumn colRib = new ZEOTableModelColumn(myDisplayGroup, EOPrelevement.RIB_KEY + "." + EORib.RIBCOMPLET_KEY, "Rib", 90);
		colRib.setAlignment(SwingConstants.LEFT);

		myCols.add(colDateReception);
		myCols.add(colExer);
		myCols.add(colBord);
		myCols.add(col11);
		myCols.add(col12);
		myCols.add(col1);
		myCols.add(col2);
		myCols.add(colRib);
		myCols.add(col4);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter(myTableModel);

		//      myTableModel.addTableModelListener(this);
	}

	public interface PrelevementSelectionnesListListener {
		/**
		 * @return Les prelevements à afficher
		 */
		public NSArray getPrelevements();

		/**
         * 
         */
		public void onSelectionChanged();

	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		return;
	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.onSelectionChanged();

	}

	public EOPrelevement getSelectedObject() {
		return (EOPrelevement) myTableModel.getSelectedObject();
	}

	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}
}
