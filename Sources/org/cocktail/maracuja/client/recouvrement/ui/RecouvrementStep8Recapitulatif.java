/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.io.File;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.zutil.client.ui.ZFileBox;

/**
 * Panel de fin (permet d'imprimer et d'enregistrer les fichiers en fonction du format choisi)
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementStep8Recapitulatif extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Recouvrement effectué";
	private static final String COMMENTAIRE = "Utilisez les boutons pour effectuer les différentes opérations .";

	private final Icon ICONE_FICHIER = ZIcon.getIconForName(ZIcon.ICON_TEXTFILE_32);

	private RecouvrementStep8RecapitulatifListener myListener;
	private ZFileBox fileBox;

	private JPanel cardPanel;
	private CardLayout cardLayout;

	/**
	 * @param listener
	 */
	public RecouvrementStep8Recapitulatif(RecouvrementStep8RecapitulatifListener listener) {
		super(listener);
		myListener = listener;

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel t = new JPanel();
		//            t = buildButtonsPanelForCaisse();
		//            t = buildButtonsPanelForCheque();
		t = buildButtonsPanelForVirement();

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		p.add(t, BorderLayout.CENTER);

		return p;
	}

	private final JPanel buildButtonsPanelForVirement() {
		final ArrayList list2 = new ArrayList(3);
		list2.add(myListener.actionEnregistrer());
		list2.add(myListener.actionImprimerBordereau());
		list2.add(myListener.actionImprimerContenuRecouvrement());

		final ArrayList list = ZKarukeraPanel.getButtonListFromActionList(list2);

		final JPanel tmp = new JPanel(new BorderLayout());

		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		fileBox = new ZFileBox(new FileBoxModel());
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(fileBox, BorderLayout.NORTH);
		panel.add(tmp, BorderLayout.CENTER);
		panel.setBorder(BorderFactory.createEmptyBorder(65, 150, 15, 150));

		return centerInPanel(panel);

	}

	private final class FileBoxModel implements ZFileBox.IZFileBoxModel {

		/**
		 * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getFile()
		 */
		public File getFile() {
			return myListener.getFilePrelevement();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getText()
		 */
		public String getText() {
			return null;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getIcon()
		 */
		public Icon getIcon() {
			return ICONE_FICHIER;
		}

		public boolean canDrop() {
			return false;
		}

		public void setFile(File f) {

		}

	}

	private JPanel centerInPanel(Component c) {
		Box box = Box.createHorizontalBox();
		box.add(Box.createHorizontalGlue());
		box.add(c);
		box.add(Box.createHorizontalGlue());

		JPanel p = new JPanel(new BorderLayout());
		p.add(box, BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildButtonsPanelForCheque() {
		Action[] list = new Action[3];
		list[0] = myListener.actionEnregistrer();
		list[1] = myListener.actionImprimerBordereau();
		list[2] = myListener.actionImprimerContenuRecouvrement();
		JPanel p = ZKarukeraPanel.buildVerticalPanelOfButtonsFromActions(list);

		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalGlue());
		box.add(p);
		box.add(Box.createVerticalGlue());

		JPanel p1 = new JPanel(new BorderLayout());
		p1.add(box, BorderLayout.CENTER);
		return p1;

	}

	private final JPanel buildButtonsPanelForCaisse() {
		Action[] list = new Action[3];
		list[0] = myListener.actionEnregistrer();
		list[1] = myListener.actionImprimerBordereau();
		list[2] = myListener.actionImprimerContenuRecouvrement();
		JPanel p = ZKarukeraPanel.buildVerticalPanelOfButtonsFromActions(list);

		Box box = Box.createVerticalBox();
		box.add(Box.createVerticalGlue());
		box.add(p);
		box.add(Box.createVerticalGlue());

		JPanel p1 = new JPanel(new BorderLayout());
		p1.add(box, BorderLayout.CENTER);
		return p1;

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		//        cardPanel.add(buildButtonsPanelForCheque(), EOTypeVirement.MOD_DOM_CHEQUE);
		//        cardPanel.add(buildButtonsPanelForCaisse(), EOTypeVirement.MOD_DOM_CAISSE);

		JPanel panelVirement = buildButtonsPanelForVirement();
		cardPanel.add(panelVirement, EOTypeRecouvrement.MOD_DOM_ECHEANCIER + "_" + EOTypeRecouvrement.TREC_FORMAT_BDF);
		super.initGUI();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		cardLayout.show(cardPanel, myListener.typeRecouvrement());
		if (fileBox != null) {
			fileBox.updateData();
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface RecouvrementStep8RecapitulatifListener extends ZStepListener {
		public Action actionEnregistrer();

		public File getFilePrelevement();

		public Action actionImprimerBordereau();

		public Action actionImprimerContenuRecouvrement();

		/** Permet d'identifier le panel recapitulatif à afficher (depend du mod_dom + format). Par ex CHEQUE ou bien VIREMENT_ETEBAC */
		public String typeRecouvrement();

	}

}
