/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recouvrement.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.foundation.NSArray;


/**
 * Etape correspondant a la selection des prelevements a recouvrer.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecouvrementStep2PrelevementsSelection extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Sélection des éléments à partir desquels le fichier de prélèvement va être généré";
	private static final String COMMENTAIRE = "Veuillez cochez dans les listes ci-dessous les prélèvements en attente partir desquels générer le fichier.";

	private final PrelevementSelectModifList recouvrementPanePrelevementSelect;
	private final PrelevementSrchFilterPanel filterPanel;

	private final RecouvrementStep2PrelevementsSelectionListener myListener;

	/**
	 * @param listener
	 */
	public RecouvrementStep2PrelevementsSelection(RecouvrementStep2PrelevementsSelectionListener listener) {
		super(listener);
		myListener = listener;
		recouvrementPanePrelevementSelect = new PrelevementSelectModifList(new RecouvrementPanelPrelevementSelectListener());

		final IPrelevementSrchFilterListener list = new IPrelevementSrchFilterListener() {

			public Action getActionSrch() {
				return myListener.getActionSrch();
			}

			public HashMap getFilters() {
				return myListener.getFilters();
			}

			public Action getPrelevAttentePrint() {
				return myListener.getPrelevAttentePrint();
			}

		};

		filterPanel = new PrelevementSrchFilterPanel(list);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des prélèvements en attente", null, ZConst.BG_COLOR_TITLE, recouvrementPanePrelevementSelect, null, null);
		JPanel p = new JPanel(new BorderLayout());
		p.add(p1, BorderLayout.CENTER);
		p.add(filterPanel, BorderLayout.NORTH);
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		recouvrementPanePrelevementSelect.initGUI();
		filterPanel.initGUI();
		//        recouvrementPanePrelevementSelect.getMyEOTable().setMyRenderer(myListener.getTableCellRenderer());
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		recouvrementPanePrelevementSelect.updateData();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface RecouvrementStep2PrelevementsSelectionListener extends ZStepListener {
		public NSArray getAllPrelevements();

		public IZEOTableCellRenderer getTableCellRenderer();

		public HashMap getCheckedPrelevements();

		public Action getActionSrch();

		public Action getPrelevAttentePrint();

		public HashMap getFilters();

		public Action actionPrelevementDelete();

		public Action actionPrelevementModif();

		public void selectionChanged();

		public void afterCheck();
	}

	private final class RecouvrementPanelPrelevementSelectListener implements PrelevementSelectModifList.PrelevementSelectModifListListener {

		/**
		 * @see org.cocktail.maracuja.client.recouvrement.ui.PrelevementSelectList.PrelevementSelecListListener#getAllCheckedPrelevements()
		 */
		public HashMap getAllCheckedPrelevements() {
			return myListener.getCheckedPrelevements();
		}

		public IZEOTableCellRenderer getTableCellRenderer() {
			return myListener.getTableCellRenderer();
		}

		public void afterCheck() {
			myListener.afterCheck();
		}

		public NSArray getData() throws Exception {
			return myListener.getAllPrelevements();
		}

		public void onDbClick() {

		}

		public void selectionChanged() {
			myListener.selectionChanged();

		}

		public Action actionPrelevementDelete() {
			return myListener.actionPrelevementDelete();
		}

		public Action actionPrelevementModif() {
			return myListener.actionPrelevementModif();
		}

	}

	public interface IPrelevementSrchFilterListener {
		public Action getActionSrch();

		public Action getPrelevAttentePrint();

		public HashMap getFilters();
	}

	/**
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public class PrelevementSrchFilterPanel extends ZKarukeraPanel {
		private final Color BORDURE_COLOR = getBackground().brighter();

		private final IPrelevementSrchFilterListener myListener;

		private ZFormPanel datesPanel;

		/**
		 * @param editingContext
		 */
		public PrelevementSrchFilterPanel(IPrelevementSrchFilterListener listener) {
			super();
			myListener = listener;
			setLayout(new BorderLayout());
			setBorder(ZKarukeraPanel.createMargin());

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
		 */
		public void initGUI() {
			add(buildFilters(), BorderLayout.CENTER);
			//            add(new JButton(myListener.getActionSrch() ), BorderLayout.EAST);
			add(buildButtons(), BorderLayout.EAST);
		}

		private final Component buildButtons() {
			return ZUiUtil.buildBoxLine(new Component[] { ZUiUtil.getButtonFromAction(myListener.getActionSrch()), ZUiUtil.getButtonFromAction(myListener.getPrelevAttentePrint()) });
		}

		private final JPanel buildFilters() {
			datesPanel = buildDateFields();
			datesPanel.setDefaultAction(myListener.getActionSrch());

			JPanel p = new JPanel(new BorderLayout());
			p.add(ZKarukeraPanel.buildLine(new Component[] { datesPanel }), BorderLayout.WEST);
			p.add(new JPanel(new BorderLayout()));
			return p;
		}

		private final ZFormPanel buildDateFields() {
			return ZFormPanel.buildFourchetteDateFields("<= Date <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
		 */
		public void updateData() throws Exception {
			datesPanel.updateData();
		}

		private final class NumeroMinModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("paiNumeroMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("paiNumeroMin", value);

			}

		}

		private final class NumeroMaxModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("odpNumeroMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("odpNumeroMax", value);

			}

		}

		private final class MontantHtMinModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("paiMontantMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("paiMontantMin", value);

			}

		}

		private final class MontantHtMaxModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("paiMontantMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("paiMontantMax", value);

			}

		}

		private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("prelevDatePrelevementMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("prelevDatePrelevementMax", value);
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

		private final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get("prelevDatePrelevementMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put("prelevDatePrelevementMin", value);
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

	}

	public EOPrelevement getSelectedPrelevement() {
		return (EOPrelevement) recouvrementPanePrelevementSelect.selectedObject();
	}

	public final PrelevementSelectModifList getRecouvrementPanePrelevementSelect() {
		return recouvrementPanePrelevementSelect;
	}

}
