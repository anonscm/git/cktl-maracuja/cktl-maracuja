/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.im.ui;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EOImSuspension;
import org.cocktail.maracuja.client.metier.EOJdDepensePapier;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;


/**
 * Liste de suspensions du DGP.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ImSuspensionListPanel extends ZKarukeraTablePanel {

	public static final String COL_TYPE = EOImSuspension.IMSUS_TYPE_FOR_DISPLAY_KEY;
	public static final String COL_DEBUT = EOImSuspension.IMSUS_DEBUT_KEY;
	public static final String COL_FIN = EOImSuspension.IMSUS_FIN_KEY;
	public static final String COL_COMMENTAIRE = EOImSuspension.IMSUS_COMMENTAIRE_KEY;
	public static final String COL_ETAT = EOImSuspension.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY;
	public static final String COL_DPP_NUMERO_FACTURE = EOImSuspension.JD_DEPENSE_PAPIER_KEY + "." + EOJdDepensePapier.DPP_NUMERO_FACTURE_KEY;
	public static final String COL_DPP_DATE_FACTURE = EOImSuspension.JD_DEPENSE_PAPIER_KEY + "." + EOJdDepensePapier.DPP_DATE_FACTURE_KEY;
	public static final String COL_UTILISATEUR = EOImSuspension.UTILISATEUR_KEY + "." + EOUtilisateur.UTL_NOM_PRENOM_KEY;

	public ImSuspensionListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);

		ZEOTableModelColumn colType = new ZEOTableModelColumn(myDisplayGroup, COL_TYPE, "Origine", 80);
		colType.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colFacture = new ZEOTableModelColumn(myDisplayGroup, COL_DPP_NUMERO_FACTURE, "Facture concernée", 200);
		colFacture.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colDateFacture = new ZEOTableModelColumn(myDisplayGroup, COL_DPP_DATE_FACTURE, "Date facture", 80);
		colDateFacture.setAlignment(SwingConstants.CENTER);
		colDateFacture.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		ZEOTableModelColumn colDebut = new ZEOTableModelColumn(myDisplayGroup, COL_DEBUT, "Début", 80);
		colDebut.setAlignment(SwingConstants.CENTER);
		colDebut.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		ZEOTableModelColumn colFin = new ZEOTableModelColumn(myDisplayGroup, COL_FIN, "Fin", 80);
		colFin.setAlignment(SwingConstants.CENTER);
		colFin.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		ZEOTableModelColumn colCommentaires = new ZEOTableModelColumn(myDisplayGroup, COL_COMMENTAIRE, "Commentaires", 200);
		colCommentaires.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colUtilisateur = new ZEOTableModelColumn(myDisplayGroup, COL_UTILISATEUR, "Utilisateur", 150);
		colUtilisateur.setAlignment(SwingConstants.LEFT);

		colsMap.clear();
		colsMap.put(COL_TYPE, colType);
		colsMap.put(COL_DPP_NUMERO_FACTURE, colFacture);
		colsMap.put(COL_DPP_DATE_FACTURE, colDateFacture);
		colsMap.put(COL_DEBUT, colDebut);
		colsMap.put(COL_FIN, colFin);
		colsMap.put(COL_COMMENTAIRE, colCommentaires);
		colsMap.put(COL_UTILISATEUR, colUtilisateur);
	}

	public NSArray selectedObjects() {
		return myDisplayGroup.selectedObjects();
	}

}
