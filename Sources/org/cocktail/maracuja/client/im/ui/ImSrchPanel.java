/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.im.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.CommonFilterPanel.ICommonSrchCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField.IZLabelTextFieldModel;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOIm;
import org.cocktail.maracuja.client.metier.EOImTaux;
import org.cocktail.maracuja.client.metier.EOJdDepenseCtrlPlanco;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;

/**
 * Panel permettant de rechercher et d'afficher les IM associes a un paiement.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class ImSrchPanel extends ZKarukeraPanel {
	private final IImSrchPanelCtrl myCtrl;
	private final ImListTablePanel imListTablePanel;
	private ZPanelNbTotal panelTotal1;

	//	private final FilterPanel filterPanel;

	public ImSrchPanel(IImSrchPanelCtrl ctrl) {
		super();
		myCtrl = ctrl;
		imListTablePanel = new ImListTablePanel(myCtrl.imListMdl());
		imListTablePanel.initGUI();

		//		filterPanel = new FilterPanel(myCtrl.panelFilterCtrl());

		setLayout(new BorderLayout());
		final JPanel b = buildRightPanel();
		b.setPreferredSize(new Dimension(160, 20));

		final JPanel d = new JPanel(new BorderLayout());
		d.add(imListTablePanel, BorderLayout.CENTER);
		d.add(buildPanelTotaux(), BorderLayout.SOUTH);

		final JPanel c = new JPanel(new BorderLayout());
		c.add(d, BorderLayout.CENTER);
		c.add(b, BorderLayout.EAST);

		//		final JPanel tmp = new JPanel(new BorderLayout());
		//		tmp.add(encloseInPanelWithTitle("Recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		//		tmp.add(encloseInPanelWithTitle("IMs", null, ZConst.BG_COLOR_TITLE, c, null, null), BorderLayout.CENTER);

		add(buildBottomPanel(), BorderLayout.SOUTH);
		add(c, BorderLayout.CENTER);
		add(buildTopPanel(), BorderLayout.NORTH);
	}

	private JPanel buildPanelTotaux() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		panelTotal1 = new ZPanelNbTotal("Total des intérêts moratoires");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());

		mainPanel.add(panelTotal1, BorderLayout.EAST);
		return mainPanel;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		//		filterPanel.updateData();
		imListTablePanel.updateData();
		panelTotal1.updateData();
	}

	private final JPanel buildRightPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(50, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(myCtrl.actionReinitialiserTous());
		list.add(myCtrl.actionImprimerAll());
		list.add(myCtrl.actionReinitialiser());
		list.add(myCtrl.actionModifier());
		list.add(myCtrl.actionAttente());
		list.add(myCtrl.actionRejeter());
		list.add(myCtrl.actionViser());
		list.add(myCtrl.actionImprimer());

		final ArrayList buttons = ZKarukeraPanel.getButtonListFromActionList(list);
		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myCtrl.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private JPanel buildTopPanel() {
		JPanel p = new JPanel(new BorderLayout());
		final ZCommentPanel commentPanel = new ZCommentPanel("Gestion des intérêts moratoires", "<html>Vous avez la possibilité dans cette fenêtre de vérifier et modifier les intérêts moratoires calculés par l'application. Vous devez les valider pour qu'ils soient pris en compte.</html>", null);
		p.add(commentPanel, BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	public final class ImListTablePanel extends ZTablePanel {
		public static final String FOURNISSEUR_KEY = EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.FOURNISSEUR_KEY + "." + EOFournisseur.NOM_AND_PRENOM_CODE_KEY;
		public static final String NO_FACTURE_KEY = EOIm.DEPENSE_KEY + "." + EODepense.JD_DEPENSE_CTRL_PLANCO_KEY + "." + EOJdDepenseCtrlPlanco.DPP_NUMERO_FACTURE_KEY;
		public static final String DATE_FACTURE_KEY = EOIm.DEPENSE_KEY + "." + EODepense.JD_DEPENSE_CTRL_PLANCO_KEY + "." + EOJdDepenseCtrlPlanco.DPP_DATE_FACTURE_KEY;
		public static final String DATE_RECEPTION_KEY = EOIm.DEPENSE_KEY + "." + EODepense.JD_DEPENSE_CTRL_PLANCO_KEY + "." + EOJdDepenseCtrlPlanco.DPP_DATE_RECEPTION_KEY;
		public static final String DATE_SERVICE_FAIT_KEY = EOIm.DEPENSE_KEY + "." + EODepense.JD_DEPENSE_CTRL_PLANCO_KEY + "." + EOJdDepenseCtrlPlanco.DPP_DATE_SERVICE_FAIT_KEY;
		public static final String BOR_NUMERO_KEY = EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY;
		public static final String MAN_NUMERO_KEY = EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.MAN_NUMERO_KEY;
		public static final String PCO_NUM_KEY = EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY;

		public static final String NUMERO_KEY = EOIm.IM_NUMERO_KEY;
		public static final String GES_CODE_KEY = EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.GESTION_KEY + "." + EOGestion.GES_CODE_KEY;
		public static final String DEBUT_DGP_KEY = EOIm.IM_DATE_DEPART_DGP_KEY;
		public static final String FIN_DGP_REELLE_KEY = EOIm.IM_DATE_FIN_DGP_REELLE_KEY;
		public static final String FIN_DGP_THEORIQUE_KEY = EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY;
		public static final String NB_JOURS_DEPASSEMENT_KEY = EOIm.IM_NB_JOURS_DEPASSEMENT_KEY;
		public static final String TAUX_REFERENCE_KEY = EOIm.IM_TAUX_KEY + "." + EOImTaux.TAUX_COMPLET_KEY;
		public static final String TAUX_APPLICABLE_KEY = EOIm.IM_TAUX_APPLICABLE_KEY;
		public static final String MONTANT_KEY = EOIm.IM_MONTANT_KEY;
		public static final String MONTANT_REGLE_KEY = EOIm.DEPENSE_KEY + "." + EODepense.DEP_MONTANT_DISQUETTE_KEY;
		public static final String TYPE_ETAT_KEY = EOIm.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY;
		public static final String PENALITE_KEY = EOIm.IM_PENALITE_KEY;

		public ImListTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn typeEtat = new ZEOTableModelColumn(myDisplayGroup, TYPE_ETAT_KEY, "Etat", 20);
			typeEtat.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn fournisseur = new ZEOTableModelColumn(myDisplayGroup, FOURNISSEUR_KEY, "Fournisseur", 200);
			fournisseur.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup, GES_CODE_KEY, "Gestion", 60);
			gesCode.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn manNumero = new ZEOTableModelColumn(myDisplayGroup, MAN_NUMERO_KEY, "Mandat", 60);
			manNumero.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn pcoNum = new ZEOTableModelColumn(myDisplayGroup, PCO_NUM_KEY, "Imputation", 60);
			pcoNum.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn borNumero = new ZEOTableModelColumn(myDisplayGroup, BOR_NUMERO_KEY, "Bordereau", 60);
			borNumero.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn numero = new ZEOTableModelColumn(myDisplayGroup, NUMERO_KEY, "N° IM", 60);
			numero.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn nbJoursDepassement = new ZEOTableModelColumn(myDisplayGroup, NB_JOURS_DEPASSEMENT_KEY, "Dépassement", 60);
			nbJoursDepassement.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn colFacture = new ZEOTableModelColumn(myDisplayGroup, NO_FACTURE_KEY, "Facture concernée", 100);
			colFacture.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn tauxApplicable = new ZEOTableModelColumn(myDisplayGroup, TAUX_APPLICABLE_KEY, "Taux applicable", 80);
			tauxApplicable.setAlignment(SwingConstants.CENTER);
			tauxApplicable.setFormatDisplay(ZConst.FORMAT_DECIMAL_COURT);
			tauxApplicable.setColumnClass(BigDecimal.class);

			final ZEOTableModelColumn penalite = new ZEOTableModelColumn(myDisplayGroup, PENALITE_KEY, "Pénalité", 80);
			penalite.setAlignment(SwingConstants.RIGHT);
			penalite.setFormatDisplay(ZConst.FORMAT_DECIMAL_COURT);
			penalite.setColumnClass(BigDecimal.class);

			final ZEOTableModelColumn montant = new ZEOTableModelColumn(myDisplayGroup, MONTANT_KEY, "Montant Im", 80);
			montant.setAlignment(SwingConstants.RIGHT);
			montant.setFormatDisplay(ZConst.FORMAT_DECIMAL_COURT);
			montant.setColumnClass(BigDecimal.class);

			final ZEOTableModelColumn montantRegle = new ZEOTableModelColumn(myDisplayGroup, MONTANT_REGLE_KEY, "Montant réglé", 80);
			montantRegle.setAlignment(SwingConstants.RIGHT);
			montantRegle.setFormatDisplay(ZConst.FORMAT_DECIMAL_COURT);
			montantRegle.setColumnClass(BigDecimal.class);

			final ZEOTableModelColumn colDateFacture = new ZEOTableModelColumn(myDisplayGroup, DATE_FACTURE_KEY, "Date facture", 80);
			colDateFacture.setAlignment(SwingConstants.CENTER);
			colDateFacture.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn colDateReception = new ZEOTableModelColumn(myDisplayGroup, DATE_RECEPTION_KEY, "Receptionnée le", 80);
			colDateReception.setAlignment(SwingConstants.CENTER);
			colDateReception.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn colDateServiceFait = new ZEOTableModelColumn(myDisplayGroup, DATE_SERVICE_FAIT_KEY, "Service fait le", 80);
			colDateServiceFait.setAlignment(SwingConstants.CENTER);
			colDateServiceFait.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn colDebut = new ZEOTableModelColumn(myDisplayGroup, DEBUT_DGP_KEY, "Début DGP", 80);
			colDebut.setAlignment(SwingConstants.CENTER);
			colDebut.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn colFinTheorique = new ZEOTableModelColumn(myDisplayGroup, FIN_DGP_THEORIQUE_KEY, "Fin DGP théorique", 80);
			colFinTheorique.setAlignment(SwingConstants.CENTER);
			colFinTheorique.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn colFinReelle = new ZEOTableModelColumn(myDisplayGroup, FIN_DGP_REELLE_KEY, "Fin DGP réelle", 80);
			colFinReelle.setAlignment(SwingConstants.CENTER);
			colFinReelle.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn tauxReference = new ZEOTableModelColumn(myDisplayGroup, TAUX_REFERENCE_KEY, "Taux référence", 80);
			tauxReference.setAlignment(SwingConstants.CENTER);

			colsMap.clear();
			colsMap.put(TYPE_ETAT_KEY, typeEtat);
			colsMap.put(GES_CODE_KEY, gesCode);
			colsMap.put(MAN_NUMERO_KEY, manNumero);
			colsMap.put(PCO_NUM_KEY, pcoNum);
			colsMap.put(FOURNISSEUR_KEY, fournisseur);
			colsMap.put(NO_FACTURE_KEY, colFacture);
			colsMap.put(DATE_FACTURE_KEY, colDateFacture);
			colsMap.put(DATE_SERVICE_FAIT_KEY, colDateServiceFait);
			colsMap.put(DATE_RECEPTION_KEY, colDateReception);
			colsMap.put(DEBUT_DGP_KEY, colDebut);
			colsMap.put(FIN_DGP_THEORIQUE_KEY, colFinTheorique);
			colsMap.put(FIN_DGP_REELLE_KEY, colFinReelle);
			colsMap.put(NB_JOURS_DEPASSEMENT_KEY, nbJoursDepassement);
			colsMap.put(MONTANT_REGLE_KEY, montantRegle);
			colsMap.put(TAUX_REFERENCE_KEY, tauxReference);
			colsMap.put(TAUX_APPLICABLE_KEY, tauxApplicable);
			colsMap.put(PENALITE_KEY, penalite);
			colsMap.put(MONTANT_KEY, montant);

		}

		public void initGUI() {
			super.initGUI();
			getMyEOTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			getMyEOTable().getColumnModel().getColumn(0).setCellRenderer(myCtrl.getImSrchTableCellRenderer());
		}

	}

	public interface IImSrchPanelCtrl {
		public ZTablePanel.IZTablePanelMdl imListMdl();

		public TableCellRenderer getImSrchTableCellRenderer();

		public Action actionImprimer();

		public Action actionImprimerAll();

		public Action actionClose();

		public Action actionRejeter();

		public Action actionViser();

		public Action actionAttente();

		public Action actionModifier();

		public Action actionReinitialiser();

		public Action actionReinitialiserTous();

		public ICommonSrchCtrl panelFilterCtrl();

		public ComboBoxModel getEtatModel();

	}

	public void initGUI() {
		//obsolete

	}

	public final ImListTablePanel getImListTablePanel() {
		return imListTablePanel;
	}

	public EOIm selectedIm() {
		return (EOIm) imListTablePanel.selectedObject();
	}

	public NSArray selectedIms() {
		return imListTablePanel.selectedObjects();
	}

	private final class TotalModel1 implements IZLabelTextFieldModel {
		public Object getValue() {
			BigDecimal totalIm = ZEOUtilities.calcSommeOfBigDecimals(imListTablePanel.getMyDisplayGroup().allObjects(), EOIm.IM_MONTANT_KEY);
			return totalIm;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModel1 implements IZLabelTextFieldModel {
		public Object getValue() {
			Integer nbIms = new Integer(imListTablePanel.getMyDisplayGroup().allObjects().count());
			return nbIms;
		}

		public void setValue(Object value) {
			return;
		}
	}

	public ZPanelNbTotal getPanelTotal1() {
		return panelTotal1;
	}

}
