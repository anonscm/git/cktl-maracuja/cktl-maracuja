/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.im.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.im.ui.ImSrchPanel.ImListTablePanel;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOIm;
import org.cocktail.zutil.client.ui.IZDataComponent;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZDefaultDataComponentModel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextArea;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImSaisiePanel extends ZKarukeraPanel {
	private final IImSaisiePanelListener myListener;
	private final ZDatePickerField dateDepartDgp;
	private final ZDatePickerField dateFinDgpReelle;
	private final ZDatePickerField dateFinDgpTheorique;
	private final ZTextField dureeSuspension;
	private final ZNumberField dgp;
	private final ZNumberField nbJoursDepassements;
	private final ZTextField tauxApplicable;
	private final ZTextField penalite;
	private final ZTextField montant;
	private final ZTextArea commentaires;
	private final JComboBox comboTauxReference;
	private final ZLabel noFacture;
	private final ZLabel fournisseur;
	private final ZLabel montantRegle;
	private final ZLabel dateFacture;
	private final ZLabel dateReception;
	private final ZLabel dateServiceFait;
	private final ZLabel datePaiement;

	private final ArrayList allEditingComponents = new ArrayList();

	/**
     * 
     */
	public ImSaisiePanel(IImSaisiePanelListener listener) {
		super();
		myListener = listener;

		noFacture = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), ImListTablePanel.NO_FACTURE_KEY));
		fournisseur = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), ImListTablePanel.FOURNISSEUR_KEY));
		dateFacture = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), ImListTablePanel.DATE_FACTURE_KEY), ZConst.FORMAT_DATESHORT);
		dateReception = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), ImListTablePanel.DATE_RECEPTION_KEY), ZConst.FORMAT_DATESHORT);
		dateServiceFait = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), ImListTablePanel.DATE_SERVICE_FAIT_KEY), ZConst.FORMAT_DATESHORT);
		montantRegle = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), ImListTablePanel.MONTANT_REGLE_KEY), ZConst.FORMAT_DECIMAL);
		datePaiement = new ZLabel(new ZDefaultDataComponentModel(myListener.getValues(), EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.PAIEMENT_KEY + "." + EOPaiement.PAI_DATE_CREATION_KEY), ZConst.FORMAT_DATESHORT);

		commentaires = new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), EOIm.IM_COMMENTAIRES_KEY));
		commentaires.getMyTextArea().setColumns(30);
		commentaires.getMyTextArea().setRows(5);

		dateDepartDgp = new ZDatePickerField(new DateDepartDgpFieldModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateFinDgpReelle = new ZDatePickerField(new DateFinDgpReelleFieldModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateFinDgpTheorique = new ZDatePickerField(new DateFinDgpTheoriqueFieldModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));

		dgp = new ZNumberField(new DgpModel(), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT, ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT);
		nbJoursDepassements = new ZNumberField(new NbJoursDepassementModel(), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT, ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT);
		nbJoursDepassements.getMyTexfield().setEnabled(false);

		dureeSuspension = new ZNumberField(new DureeSuspensionModel(), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT, ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT);
		tauxApplicable = new ZNumberField(new TauxApplicableModel(), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL
		}, ZConst.FORMAT_DECIMAL);
		penalite = new ZNumberField(new PenaliteModel(), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL
		}, ZConst.FORMAT_DECIMAL);
		montant = new ZNumberField(new MontantModel(), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL
		}, ZConst.FORMAT_DECIMAL);
		montant.getMyTexfield().setColumns(10);

		comboTauxReference = new JComboBox(myListener.getTauxReferenceModel());

		allEditingComponents.add(dateDepartDgp);
		allEditingComponents.add(dateFinDgpReelle);
		allEditingComponents.add(dateFinDgpTheorique);
		allEditingComponents.add(dgp);
		allEditingComponents.add(nbJoursDepassements);
		allEditingComponents.add(dureeSuspension);
		allEditingComponents.add(tauxApplicable);
		allEditingComponents.add(penalite);
		allEditingComponents.add(montant);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public void initGUI() {

		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildFormPanel(), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		comboTauxReference.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent actionevent) {
				myListener.getValues().put(EOIm.IM_TAUX_KEY, myListener.getTauxReferenceModel().getSelectedEObject());
				nbJoursDepassements.updateData();
				tauxApplicable.updateData();
				montant.updateData();
			}

		});
		//        updateInputMap();
	}

	private final JPanel buildFormPanel() {
		final ArrayList list = new ArrayList();
		list.add(new Component[] {
				new JLabel("Fournisseur : "), fournisseur
		});
		list.add(new Component[] {
				new JLabel("N° facture : "), noFacture
		});
		list.add(new Component[] {
				new JLabel("Date facture : "), dateFacture
		});
		list.add(new Component[] {
				new JLabel("Service fait : "), dateServiceFait
		});
		list.add(new Component[] {
				new JLabel("Date de réception : "), dateReception
		});
		list.add(new Component[] {
				new JLabel("N° montant réglé : "), montantRegle
		});
		list.add(new Component[] {
				new JLabel("Date de paiement : "), datePaiement
		});
		list.add(new Component[] {
				new JLabel("Début d'application du DGP : "), dateDepartDgp
		});
		list.add(new Component[] {
				new JLabel("Nombre de jours pour le DGP : "), dgp
		});
		list.add(new Component[] {
				new JLabel("Fin théorique du DGP : "), dateFinDgpTheorique
		});
		list.add(new Component[] {
				new JLabel("Nombre de jours de suspension : "), dureeSuspension
		});
		list.add(new Component[] {
				new JLabel("Fin réelle du DGP : "), dateFinDgpReelle
		});
		list.add(new Component[] {
				new JLabel("Nombre de jours de dépassement : "), nbJoursDepassements
		});
		list.add(new Component[] {
				new JLabel("Taux de référence : "), comboTauxReference
		});
		list.add(new Component[] {
				new JLabel("Taux applicable : "), tauxApplicable
		});
		list.add(new Component[] {
				new JLabel("Montant pénalité : "), penalite
		});
		list.add(new Component[] {
				new JLabel("Montant : "), montant
		});
		list.add(new Component[] {
				new JLabel("Commentaires : "), commentaires
		});

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() {
		dateDepartDgp.updateData();
		dateFinDgpReelle.updateData();
		dateFinDgpTheorique.updateData();
		dureeSuspension.updateData();
		dgp.updateData();
		nbJoursDepassements.updateData();
		tauxApplicable.updateData();
		penalite.updateData();
		montant.updateData();
		commentaires.updateData();
		//		comboTauxReference;
		commentaires.updateData();
		noFacture.updateData();
		fournisseur.updateData();
		montantRegle.updateData();
		dateReception.updateData();
		dateFacture.updateData();
		dateServiceFait.updateData();
		datePaiement.updateData();

		myListener.getTauxReferenceModel().setSelectedEObject((NSKeyValueCoding) myListener.getValues().get(EOIm.IM_TAUX_KEY));

	}

	public void updateDataExcept(Object comp) {

		Iterator iterator = allEditingComponents.iterator();
		while (iterator.hasNext()) {
			IZDataComponent object = (IZDataComponent) iterator.next();
			if (!object.equals(comp)) {
				try {
					object.updateData();
				} catch (Exception e) {

				}
			}
		}
		myListener.getTauxReferenceModel().setSelectedEObject((NSKeyValueCoding) myListener.getValues().get(EOIm.IM_TAUX_KEY));

	}

	public interface IImSaisiePanelListener {
		public Action actionClose();

		public ZEOComboBoxModel getTauxReferenceModel();

		public Action actionValider();

		public Map getValues();

	}

	private final class DateDepartDgpFieldModel implements IZDatePickerFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_DATE_DEPART_DGP_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				value = new NSTimestamp((Date) value);
			}
			myListener.getValues().put(EOIm.IM_DATE_DEPART_DGP_KEY, value);
			updateDataExcept(dateDepartDgp);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	private final class DateFinDgpReelleFieldModel implements IZDatePickerFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				value = new NSTimestamp((Date) value);
			}
			myListener.getValues().put(EOIm.IM_DATE_FIN_DGP_REELLE_KEY, value);
			updateDataExcept(dateFinDgpReelle);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	private final class DateFinDgpTheoriqueFieldModel implements IZDatePickerFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				value = new NSTimestamp((Date) value);
			}
			myListener.getValues().put(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY, value);
			updateDataExcept(dateFinDgpTheorique);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	private final class DgpModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_DGP_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				myListener.getValues().put(EOIm.IM_DGP_KEY, new Integer(((Number) value).intValue()));
			}
			else {
				myListener.getValues().put(EOIm.IM_DGP_KEY, null);
			}
			updateDataExcept(dgp);
		}

	}

	private final class NbJoursDepassementModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				myListener.getValues().put(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY, new Integer(((Number) value).intValue()));
			}
			else {
				myListener.getValues().put(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY, null);
			}
			updateDataExcept(nbJoursDepassements);
		}

	}

	private final class DureeSuspensionModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_DUREE_SUSP_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				myListener.getValues().put(EOIm.IM_DUREE_SUSP_KEY, new Integer(((Number) value).intValue()));
			}
			else {
				myListener.getValues().put(EOIm.IM_DUREE_SUSP_KEY, null);
			}
			updateDataExcept(dureeSuspension);
		}

	}

	private final class TauxApplicableModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_TAUX_APPLICABLE_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				myListener.getValues().put(EOIm.IM_TAUX_APPLICABLE_KEY, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			else {
				myListener.getValues().put(EOIm.IM_TAUX_APPLICABLE_KEY, null);
			}
			updateDataExcept(tauxApplicable);
		}

	}

	private final class PenaliteModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_PENALITE_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				myListener.getValues().put(EOIm.IM_PENALITE_KEY, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			else {
				myListener.getValues().put(EOIm.IM_PENALITE_KEY, null);
			}
			updateDataExcept(penalite);
		}

	}

	private final class MontantModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOIm.IM_MONTANT_KEY);
		}

		public void setValue(Object value) {
			if (value != null) {
				myListener.getValues().put(EOIm.IM_MONTANT_KEY, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			else {
				myListener.getValues().put(EOIm.IM_MONTANT_KEY, null);
			}

		}

	}

}
