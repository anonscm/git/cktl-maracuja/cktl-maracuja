/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.im.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOImSuspension;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextArea;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImSuspensionSaisiePanel extends ZKarukeraPanel {
	private final IImSuspensionSaisiePanelListener myListener;
	private final ZDatePickerField debut;
	private final ZDatePickerField fin;
	private final ZTextArea commentaires;
	private final JComboBox dppsCombobox;

	/**
     * 
     */
	public ImSuspensionSaisiePanel(IImSuspensionSaisiePanelListener listener) {
		super();
		myListener = listener;

		commentaires = new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), EOImSuspension.IMSUS_COMMENTAIRE_KEY));
		commentaires.getMyTextArea().setColumns(50);
		commentaires.getMyTextArea().setRows(5);

		debut = new ZDatePickerField(new DateSaisieDebutFieldModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		fin = new ZDatePickerField(new DateSaisieFinFieldModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));

		dppsCombobox = new JComboBox(myListener.getDppModel());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public void initGUI() {

		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		this.add(buildFormPanel(), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);

		//        updateInputMap();
	}

	private final JPanel buildFormPanel() {
		final ArrayList list = new ArrayList();
		list.add(new Component[] { new JLabel("Facture concernée : "), dppsCombobox });
		list.add(new Component[] { new JLabel("Début : "), debut });
		list.add(new Component[] { new JLabel("Fin "), fin });
		list.add(new Component[] { new JLabel("Commentaires : "), commentaires });

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		debut.updateData();
		fin.updateData();
		commentaires.updateData();

		//		fournisseurMissionLabel.setText((String) myListener.getValues().get(EOVisaAvMission.FOU_NOM_AND_PRENOM_KEY));
		//		motifMissionLabel.setText((String) myListener.getValues().get(EOVisaAvMission.MISSION_MIS_MOTIF_KEY));
		//		numeroMissionLabel.setText("" + myListener.getValues().get(EOVisaAvMission.MISSION_MIS_NUMERO_KEY));
		//		debutMissionLabel.setText(ZConst.FORMAT_DATESHORT.format(myListener.getValues().get(EOVisaAvMission.MISSION_MIS_DEBUT_KEY)));
		//		//    	montantMissionLabel.setText(ZConst.FORMAT_DECIMAL.format( myListener.getValues().get(EOVisaAvMission.MISSION_)));
		//		montantAvanceLabel.setText(ZConst.FORMAT_DECIMAL.format(myListener.getValues().get(EOVisaAvMission.VAM_MONTANT_KEY)));
	}

	public interface IImSuspensionSaisiePanelListener {
		public Action actionClose();

		public Action actionValider();

		public Map getValues();

		/** Modele pour la liste des depenses papier */
		public ZEOComboBoxModel getDppModel();
	}

	private final class DateSaisieDebutFieldModel implements IZDatePickerFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOImSuspension.IMSUS_DEBUT_KEY);
		}

		public void setValue(Object value) {
			myListener.getValues().put(EOImSuspension.IMSUS_DEBUT_KEY, value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	private final class DateSaisieFinFieldModel implements IZDatePickerFieldModel {

		public Object getValue() {
			return myListener.getValues().get(EOImSuspension.IMSUS_FIN_KEY);
		}

		public void setValue(Object value) {
			myListener.getValues().put(EOImSuspension.IMSUS_FIN_KEY, value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

}
