/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.im.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.FactoryIm;
import org.cocktail.maracuja.client.factory.FactoryIm.IImMapListener;
import org.cocktail.maracuja.client.im.ui.ImSaisiePanel;
import org.cocktail.maracuja.client.im.ui.ImSrchPanel.ImListTablePanel;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOIm;
import org.cocktail.maracuja.client.metier.EOImTaux;
import org.cocktail.maracuja.client.metier.EOImTypeTaux;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImSaisieCtrl extends CommonCtrl implements IImMapListener {
	private static final String TITLE = "Modification d'un IM";
	private static final Dimension WINDOW_DIMENSION = new Dimension(700, 700);

	private final FactoryIm.ImMap currentDico;

	private final ImSaisiePanel ImSaisiePanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionValider actionValider = new ActionValider();

	private EOIm currentIm;

	private final ZEOComboBoxModel tauxReferenceModel;

	/**
	 * @param editingContext
	 */
	public ImSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);
		currentDico = new FactoryIm.ImMap(editingContext, this);
		tauxReferenceModel = new ZEOComboBoxModel(NSArray.EmptyArray, EOImTaux.TAUX_COMPLET_KEY, null, null);
		ImSaisiePanel = new ImSaisiePanel(new ImSaisiePanelListener());
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);
		if (tauxReferenceModel.getSelectedEObject() == null) {
			throw new DataCheckException("Vous devez obligatoirement indiquer un taux de référence.");
		}

		//currentDico.putSilent(EOIm.IM_TAUX_KEY, tauxReferenceModel.getSelectedEObject());

		if (currentDico.get(EOIm.IM_DATE_DEPART_DGP_KEY) == null) {
			throw new DataCheckException("La date de départ DGP est obligatoire.");
		}

		if (currentDico.get(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY) == null) {
			throw new DataCheckException("La date de fin théorique du DGP est obligatoire.");
		}

		if (currentDico.get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY) == null) {
			throw new DataCheckException("La date de fin réelle du DGP est obligatoire.");
		}

		if (currentDico.get(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY) == null) {
			throw new DataCheckException("Le nombre de jours de dépassement est obligatoire.");
		}

		if (currentDico.get(EOIm.IM_TAUX_APPLICABLE_KEY) == null) {
			throw new DataCheckException("Le taux applicable est obligatoire.");
		}

		if (currentDico.get(EOIm.IM_MONTANT_KEY) == null) {
			throw new DataCheckException("Le montant applicable est obligatoire.");
		}

		if (currentDico.get(EOIm.IM_DUREE_SUSP_KEY) == null) {
			currentDico.put(EOIm.IM_DUREE_SUSP_KEY, new Integer(0));
		}

		if (currentDico.get(EOIm.IM_PENALITE_KEY) == null) {
			currentDico.put(EOIm.IM_PENALITE_KEY, new Integer(0));
		}

		Date imDateFinDgpReelle = (Date) currentDico.get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY);
		Date imDateDepartDgp = (Date) currentDico.get(EOIm.IM_DATE_DEPART_DGP_KEY);
		Number imDureeSusp = (Number) currentDico.get(EOIm.IM_DUREE_SUSP_KEY);
		Number imNbJoursDepassement = (Number) currentDico.get(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY);
		Number imTauxApplicable = (Number) currentDico.get(EOIm.IM_TAUX_APPLICABLE_KEY);
		Number imMontant = (Number) currentDico.get(EOIm.IM_MONTANT_KEY);
		Number imPenalite = (Number) currentDico.get(EOIm.IM_PENALITE_KEY);

		if (imDateFinDgpReelle.before(imDateDepartDgp)) {
			throw new NSValidation.ValidationException("La date de fin de DGP ne peut pas etre antérieure à la date de début de DGP.");
		}

		if (imDureeSusp.intValue() < 0) {
			throw new NSValidation.ValidationException("La durée de suspension doit être positive.");
		}

		if (imNbJoursDepassement.intValue() < 0) {
			throw new NSValidation.ValidationException("Le nombre de jours de dépassement doit être positif.");
		}

		if (imTauxApplicable.doubleValue() < 0) {
			throw new NSValidation.ValidationException("Le taux doit être positif.");
		}

		if (imPenalite.doubleValue() < 0) {
			throw new NSValidation.ValidationException("Le montant de la pénalité doit être positif.");
		}
		if (imMontant.doubleValue() < 0) {
			throw new NSValidation.ValidationException("Le montant doit être positif.");
		}

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	private final void validerSaisie() {
		try {
			checkSaisie();
			updateObjectWithDico();
			currentIm.setUtilisateurRelationship(getUtilisateur());
			currentIm.setDateModification(ZDateUtil.now());

			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class ImSaisiePanelListener implements ImSaisiePanel.IImSaisiePanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map getValues() {
			return currentDico;
		}

		public ZEOComboBoxModel getTauxReferenceModel() {
			return tauxReferenceModel;
		}

	}

	/**
	 * @param dial
	 * @param im
	 * @return
	 */
	public final int openDialog(Window dial, EOIm im) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			currentIm = im;
			updateDicoWithObject();
			updateImTaux();
			ImSaisiePanel.initGUI();
			ImSaisiePanel.updateData();

			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private void updateImTaux() throws Exception {
		NSMutableArray res = new NSMutableArray();
		NSTimestamp dateFinDGPReelle = (NSTimestamp) currentDico.get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY);
		NSTimestamp dateDebutCalculIMs = FactoryIm.getDateDebutCalculIms(dateFinDGPReelle);

		if (dateDebutCalculIMs != null) {
			NSTimestamp dateDebutSemestre = new NSTimestamp(ZDateUtil.addDHMS(ZDateUtil.getDateDebutSemestre(dateDebutCalculIMs), -1, 0, 0, 0));
			res.addObjectsFromArray(EOImTaux.fetchAllForDateAndType(getEditingContext(), dateDebutSemestre, EOImTypeTaux.IMTT_CODE_TBCE));
			res.addObjectsFromArray(EOImTaux.fetchAllForDateAndType(getEditingContext(), dateDebutCalculIMs, EOImTypeTaux.IMTT_CODE_TIL));
			EOSortOrdering.sortArrayUsingKeyOrderArray(res, new NSArray(EOImTaux.SORT_IMTT_PRIORITE_DESC));
		}

		//		System.out.println(res);
		tauxReferenceModel.updateListWithData(res);
		tauxReferenceModel.setSelectedEObject(null);
		if (res.indexOfObject(currentDico.get(EOIm.IM_TAUX_KEY)) != NSArray.NotFound) {
			tauxReferenceModel.setSelectedEObject((NSKeyValueCoding) currentDico.get(EOIm.IM_TAUX_KEY));
		}

	}

	private void updateObjectWithDico() {
		currentIm.setImCommentaires((String) currentDico.get(EOIm.IM_COMMENTAIRES_KEY));
		currentIm.setImDgp((Number) currentDico.get(EOIm.IM_DGP_KEY));
		currentIm.setImDureeSusp((Number) currentDico.get(EOIm.IM_DUREE_SUSP_KEY));
		currentIm.setImPenalite((BigDecimal) currentDico.get(EOIm.IM_PENALITE_KEY));
		currentIm.setImMontant((BigDecimal) currentDico.get(EOIm.IM_MONTANT_KEY));
		currentIm.setImNbJoursDepassement((Number) currentDico.get(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY));
		currentIm.setImTauxRelationship((EOImTaux) currentDico.get(EOIm.IM_TAUX_KEY));
		currentIm.setImTauxApplicable((BigDecimal) currentDico.get(EOIm.IM_TAUX_APPLICABLE_KEY));

		if (currentDico.get(EOIm.IM_DATE_DEPART_DGP_KEY) != null) {
			currentIm.setImDateDepartDgp(new NSTimestamp((Date) currentDico.get(EOIm.IM_DATE_DEPART_DGP_KEY)));
		}
		else {
			currentIm.setImDateDepartDgp(null);
		}
		if (currentDico.get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY) != null) {
			currentIm.setImDateFinDgpReelle(new NSTimestamp((Date) currentDico.get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY)));
		}
		else {
			currentIm.setImDateFinDgpReelle(null);
		}
		if (currentDico.get(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY) != null) {
			currentIm.setImDateFinDgpTheorique(new NSTimestamp((Date) currentDico.get(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY)));
		}
		else {
			currentIm.setImDateFinDgpTheorique(null);
		}
	}

	private void updateDicoWithObject() {
		currentDico.clear();
		currentDico.putSilent(ImListTablePanel.NO_FACTURE_KEY, currentIm.depense().getJdDepenseCtrlPlanco().dppNumeroFacture());
		currentDico.putSilent(ImListTablePanel.FOURNISSEUR_KEY, currentIm.depense().mandat().fournisseur().getNomAndPrenomAndCode());
		currentDico.putSilent(ImListTablePanel.MONTANT_REGLE_KEY, currentIm.depense().depMontantDisquette());
		currentDico.putSilent(ImListTablePanel.DATE_RECEPTION_KEY, currentIm.depense().getJdDepenseCtrlPlanco().dppDateReception());
		currentDico.putSilent(ImListTablePanel.DATE_SERVICE_FAIT_KEY, currentIm.depense().getJdDepenseCtrlPlanco().dppDateServiceFait());
		currentDico.putSilent(ImListTablePanel.DATE_FACTURE_KEY, currentIm.depense().getJdDepenseCtrlPlanco().dppDateFacture());
		currentDico.putSilent(EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.PAIEMENT_KEY + "." + EOPaiement.PAI_DATE_CREATION_KEY, currentIm.depense().mandat().paiement().paiDateCreation());
		currentDico.putSilent(EOIm.IM_DGP_KEY, currentIm.imDgp());
		currentDico.putSilent(EOIm.IM_DUREE_SUSP_KEY, currentIm.imDureeSusp());
		currentDico.putSilent(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY, currentIm.imNbJoursDepassement());
		currentDico.putSilent(EOIm.IM_PENALITE_KEY, currentIm.imPenalite());
		currentDico.putSilent(EOIm.IM_MONTANT_KEY, currentIm.imMontant());
		currentDico.putSilent(EOIm.IM_TAUX_KEY, currentIm.imTaux());
		currentDico.putSilent(EOIm.IM_TAUX_APPLICABLE_KEY, currentIm.imTauxApplicable());
		currentDico.putSilent(EOIm.IM_DATE_DEPART_DGP_KEY, currentIm.imDateDepartDgp());
		currentDico.putSilent(EOIm.IM_DATE_FIN_DGP_REELLE_KEY, currentIm.imDateFinDgpReelle());
		currentDico.putSilent(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY, currentIm.imDateFinDgpTheorique());
		currentDico.putSilent(EOIm.IM_COMMENTAIRES_KEY, currentIm.imCommentaires());
		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return ImSaisiePanel;
	}

	public String title() {
		return TITLE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @seeorg.cocktail.maracuja.client.factory.FactoryIm.IImMapListener# onImDateFinReelleChanged()
	 */
	public void onImDateFinReelleChanged() {
		try {
			updateImTaux();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

}
