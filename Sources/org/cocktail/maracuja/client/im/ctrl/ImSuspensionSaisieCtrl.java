/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.im.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.im.ui.ImSuspensionSaisiePanel;
import org.cocktail.maracuja.client.metier.EOImSuspension;
import org.cocktail.maracuja.client.metier.EOJdDepensePapier;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImSuspensionSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'une suspension de DGP";
	private static final Dimension WINDOW_DIMENSION = new Dimension(700, 230);

	private final HashMap currentDico;

	private final ImSuspensionSaisiePanel ImSuspensionSaisiePanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionValider actionValider = new ActionValider();

	private EOImSuspension currentImSuspension;

	private final ZEOComboBoxModel dppModel;

	/**
	 * @param editingContext
	 */
	public ImSuspensionSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);
		currentDico = new HashMap();
		dppModel = new ZEOComboBoxModel(NSArray.EmptyArray, EOJdDepensePapier.DISPLAY_STRING_KEY, null, null);
		ImSuspensionSaisiePanel = new ImSuspensionSaisiePanel(new ImSuspensionSaisiePanelListener());
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);
		if (dppModel.getSelectedEObject() == null) {
			throw new DataCheckException("Vous devez obligatoirement affecter une facture du mandat à la suspension de DGP.");
		}

		currentDico.put(EOImSuspension.JD_DEPENSE_PAPIER_KEY, dppModel.getSelectedEObject());

		if (currentDico.get(EOImSuspension.IMSUS_DEBUT_KEY) == null) {
			throw new DataCheckException("La date de début est obligatoire.");
		}

		if (currentDico.get(EOImSuspension.IMSUS_FIN_KEY) != null) {
			if (((Date) currentDico.get(EOImSuspension.IMSUS_DEBUT_KEY)).after((Date) currentDico.get(EOImSuspension.IMSUS_FIN_KEY))) {
				throw new DataCheckException("La date de fin doit être ultérieure à la date de début.");
			}
		}

		//FIXME ajouter autres controles

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	private final void validerSaisie() {
		try {
			checkSaisie();
			updateObjectWithDico();
			currentImSuspension.setUtilisateurRelationship(getUtilisateur());
			currentImSuspension.setDateModification(ZDateUtil.now());

			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class ImSuspensionSaisiePanelListener implements ImSuspensionSaisiePanel.IImSuspensionSaisiePanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map getValues() {
			return currentDico;
		}

		public ZEOComboBoxModel getDppModel() {
			return dppModel;
		}

	}

	/**
	 * @param dial
	 * @param imSuspension
	 * @param depensePapiers
	 *            Les depensePapiers qu'il est possible d'affecter a la
	 *            suspension.
	 * @return
	 */
	public final int openDialog(Window dial, EOImSuspension imSuspension, NSArray depensePapiers) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			currentImSuspension = imSuspension;
			dppModel.setNullValue((depensePapiers.count() == 1 ? null : "Sélectionnez une facture dans la liste"));
			dppModel.updateListWithData(depensePapiers);

			dppModel.setSelectedEObject(imSuspension.jdDepensePapier());

			updateDicoWithObject();
			ImSuspensionSaisiePanel.initGUI();
			ImSuspensionSaisiePanel.updateData();

			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private void updateObjectWithDico() {
		currentImSuspension.setJdDepensePapierRelationship((EOJdDepensePapier) currentDico.get(EOImSuspension.JD_DEPENSE_PAPIER_KEY));
		currentImSuspension.setImsusCommentaire((String) currentDico.get(EOImSuspension.IMSUS_COMMENTAIRE_KEY));
		if (currentDico.get(EOImSuspension.IMSUS_DEBUT_KEY) != null) {
			currentImSuspension.setImsusDebut(new NSTimestamp((Date) currentDico.get(EOImSuspension.IMSUS_DEBUT_KEY)));
		}
		else {
			currentImSuspension.setImsusDebut(null);
		}
		if (currentDico.get(EOImSuspension.IMSUS_FIN_KEY) != null) {
			currentImSuspension.setImsusFin(new NSTimestamp((Date) currentDico.get(EOImSuspension.IMSUS_FIN_KEY)));
		}
		else {
			currentImSuspension.setImsusFin(null);
		}
	}

	private void updateDicoWithObject() {
		currentDico.clear();
		currentDico.put(EOImSuspension.IMSUS_COMMENTAIRE_KEY, currentImSuspension.imsusCommentaire());
		currentDico.put(EOImSuspension.IMSUS_TYPE_KEY, currentImSuspension.imsusType());
		currentDico.put(EOImSuspension.IMSUS_DEBUT_KEY, currentImSuspension.imsusDebut());
		currentDico.put(EOImSuspension.IMSUS_FIN_KEY, currentImSuspension.imsusFin());
		//		currentDico.put(EOImSuspension.TYPE_ETAT_KEY, currentImSuspension.typeEtat());
		currentDico.put(EOImSuspension.JD_DEPENSE_PAPIER_KEY, currentImSuspension.jdDepensePapier());
		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return ImSuspensionSaisiePanel;
	}

	public String title() {
		return TITLE;
	}

}
