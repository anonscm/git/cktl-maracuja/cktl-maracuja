/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.im.ctrl;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonSrchCtrl;
import org.cocktail.maracuja.client.common.ui.CommonFilterPanel.ICommonSrchCtrl;
import org.cocktail.maracuja.client.factory.FactoryIm;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.im.ui.ImSrchPanel;
import org.cocktail.maracuja.client.im.ui.ImSrchPanel.IImSrchPanelCtrl;
import org.cocktail.maracuja.client.metier.EOIm;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImSrchCtrl extends CommonSrchCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Intérêts moratoires";

	//	private final String ACTION_ID = ZActionCtrl.IDU_VIAVMIS;

	private final ImSrchPanel myPanel;
	private final ImListMdl imListMdl;
	private final ImSrchPanelCtrl imPanelCtrl;
	private final FilterPanelCtrl filterPanelCtrl;

	private final ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionImprimerAll actionImprimerAll = new ActionImprimerAll();
	private final ActionAttente actionAttente = new ActionAttente();
	private final ActionRejeter actionRejeter = new ActionRejeter();
	private final ActionViser actionViser = new ActionViser();
	private final ActionModifier actionModifier = new ActionModifier();
	private final ActionReinitialiser actionReinitialiser = new ActionReinitialiser();
	private final ActionReinitialiserTous actionReinitialiserTous = new ActionReinitialiserTous();

	private final FactoryIm factoryIm = new FactoryIm(myApp.wantShowTrace());

	private final DefaultComboBoxModel comboEtatsModel;
	private EOPaiement paiement;
	private final ImSrchTableCellRenderer imSrchTableCellRenderer = new ImSrchTableCellRenderer();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ImSrchCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		//Récupérer les droits
		//		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID)) {
		//			throw new DefaultClientException("Vous n'avez pas les droits nécessaires pour cette fonctionnalité.");
		//		}

		comboEtatsModel = new DefaultComboBoxModel();
		comboEtatsModel.addElement(ZFinderEtats.etat_EN_ATTENTE());
		comboEtatsModel.addElement(ZFinderEtats.etat_ACCEPTE());
		comboEtatsModel.addElement(ZFinderEtats.etat_REJETE());

		imListMdl = new ImListMdl();
		imPanelCtrl = new ImSrchPanelCtrl();
		filterPanelCtrl = new FilterPanelCtrl();
		myPanel = new ImSrchPanel(imPanelCtrl);

	}

	protected NSMutableArray buildFilterQualifiers() throws Exception {

		NSMutableArray quals = new NSMutableArray();
		//		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOIm.EXERCICE_KEY + "=%@", new NSArray(new Object[] { myApp.appUserInfo().getCurrentExercice() })));
		//
		//		quals.addObject(EOIm.qualForEtat((EOTypeEtat) comboEtatsModel.getSelectedItem()));
		//		quals.addObject(EOIm.qualForVamMontantBetween((Number) filters.get("montantMin"), (Number) filters.get("montantMax")));
		//		quals.addObject(EOIm.qualForMisNumeroEquals((Number) filters.get(EOIm.MISSION_KEY + "." + EOMission.MIS_NUMERO_KEY)));
		//		quals.addObject(EOIm.qualForFournisseur((String) filters.get(EOIm.FOURNISSEUR_KEY)));
		//		quals.addObject(EOIm.qualForMisMotifLike((String) filters.get(EOIm.MISSION_MIS_MOTIF_KEY)));
		//		quals.addObject(EOIm.qualForGesCodeEquals((String) filters.get(EOIm.GESTION_KEY + "." + EOGestion.GES_CODE_KEY)));
		return quals;
	}

	public void onAfterUpdateDataBeforeOpen() {
		super.onAfterUpdateDataBeforeOpen();
		refreshActions();
	}

	protected final NSArray getData() throws Exception {
		//Créer la condition à partir des filtres
		NSArray res = EOQualifier.filteredArrayWithQualifier(paiement.getIms(), new EOAndQualifier(buildFilterQualifiers()));
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(EOIm.SORT_DEP_FOURNISSEUR_ASC));
	}

	public void initData(EOPaiement paiement) {
		this.paiement = paiement;
		try {
			NSArray res = getData();
			//			System.out.println("res.count=" + res.count());
			if (res.count() == 0) {
				NSArray res2 = factoryIm.genereImsForPaiement(getEditingContext(), paiement, getUtilisateur());
				if (getEditingContext().hasChanges()) {
					getEditingContext().saveChanges();
				}
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			showErrorDialog(e);
		}

	}

	private void onAccepter() {
		setWaitCursor(true);
		try {
			if (getSelectedIm() == null) {
				throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
			}
			factoryIm.valideIm(getEditingContext(), getSelectedIm(), getUtilisateur());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}
	}

	private void onEnAttente() {
		setWaitCursor(true);
		try {
			if (getSelectedIm() == null) {
				throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
			}
			factoryIm.enAttenteIm(getEditingContext(), getSelectedIm(), getUtilisateur());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}
	}

	private void onRejeter() {
		setWaitCursor(true);
		try {
			if (getSelectedIm() == null) {
				throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
			}
			factoryIm.rejeteIm(getEditingContext(), getSelectedIm(), getUtilisateur());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}

	}

	private void onReinitialiser() {
		try {
			if (getSelectedIm() != null) {
				if (showConfirmationDialog("Réinitialiser l'IM ?", "Souhaitez-vous vraiment réinitialiser les IM pour la facture sélectionnée ? \n Vous allez perdre les modifications que vous avez pu faire manuellement pour cette facture.", ZMsgPanel.BTLABEL_YES)) {
					factoryIm.initialiseIm(getEditingContext(), getSelectedIm(), getUtilisateur(), paiement.paiDateCreation());
				}
			}
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			myPanel.getImListTablePanel().fireSeletedTableRowUpdated();
			myPanel.getPanelTotal1().updateData();

		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}
	}

	private void onReinitialiserTous() {
		try {
			if (paiement == null) {
				throw new Exception("Probleme : le paiement est null.");
			}

			setWaitCursor(true);

			NSArray res = factoryIm.genereImsForPaiement(getEditingContext(), paiement, getUtilisateur());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

			if (res.count() == 0) {
				showInfoDialog("Aucun intérêt moratoire n'a été généré. Aucune facture n'a été payée en retard.");
			}
			else {
				showInfoDialog(res.count() + " facture(s) donne(nt) lieu à des intérêts moratoires. Veuillez les valider ou les annuler.");
			}

		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			try {
				mainPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

			setWaitCursor(false);
			refreshActions();
		}
	}

	private void onImprimer(boolean onlyVises) {
		try {
			String filePath = ReportFactoryClient.imprimerImListe(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(paiement), getMyDialog(), onlyVises);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

	}

	private void onModifier() {
		if (actionModifier.isEnabled()) {
			try {
				if (getSelectedIm() == null) {
					throw new DefaultClientException("Aucune ligne n'est sélectionnée.");
				}

				final ImSaisieCtrl imSaisieCtrl = new ImSaisieCtrl(getEditingContext());
				imSaisieCtrl.openDialog(getMyDialog(), getSelectedIm());

				myPanel.getImListTablePanel().fireSeletedTableRowUpdated();
				myPanel.getPanelTotal1().updateData();

			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

	}

	private EOIm getSelectedIm() {
		return myPanel.selectedIm();
	}

	private final void refreshActions() {
		final EOIm im = getSelectedIm();
		actionModifier.setEnabled(false);
		actionReinitialiser.setEnabled(false);
		actionRejeter.setEnabled(false);
		actionViser.setEnabled(false);
		actionAttente.setEnabled(false);

		if (im != null) {
			actionModifier.setEnabled(im.isEnAttente());
			actionReinitialiser.setEnabled(true);
			actionRejeter.setEnabled(im.isEnAttente());
			actionViser.setEnabled(im.isEnAttente());
			actionAttente.setEnabled(im.isValide() || im.isAnnule());
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	private final class ImSrchPanelCtrl implements IImSrchPanelCtrl {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionRejeter() {
			return actionRejeter;
		}

		public Action actionViser() {
			return actionViser;
		}

		public Action actionModifier() {
			return actionModifier;
		}

		public IZTablePanelMdl imListMdl() {
			return imListMdl;
		}

		public ICommonSrchCtrl panelFilterCtrl() {
			return filterPanelCtrl;
		}

		public ComboBoxModel getEtatModel() {
			return comboEtatsModel;
		}

		public Action actionReinitialiser() {
			return actionReinitialiser;
		}

		public Action actionReinitialiserTous() {
			return actionReinitialiserTous;
		}

		public TableCellRenderer getImSrchTableCellRenderer() {
			return imSrchTableCellRenderer;
		}

		public Action actionAttente() {
			return actionAttente;
		}

		public Action actionImprimer() {
			return actionImprimer;
		}

		public Action actionImprimerAll() {
			return actionImprimerAll;
		}

	}

	private final class ImListMdl implements IZTablePanelMdl {

		public NSArray getData() throws Exception {
			return ImSrchCtrl.this.getData();
		}

		public void onDbClick() {
			onModifier();
		}

		public void selectionChanged() {
			refreshActions();
		}

	}

	private final class FilterPanelCtrl implements ICommonSrchCtrl {

		public Action actionSrch() {
			return actionSrch;
		}

		public Map filters() {
			return filters;
		}

	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
			//				putValue(AbstractAction.SHORT_DESCRIPTION, "Accepter la demande. Un Ordre de Paiement va être créé");
		}

		public void actionPerformed(ActionEvent e) {
			onAccepter();
		}
	}

	private final class ActionAttente extends AbstractAction {
		public ActionAttente() {
			super("Mettre en attente");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			//				putValue(AbstractAction.SHORT_DESCRIPTION, "Accepter la demande. Un Ordre de Paiement va être créé");
		}

		public void actionPerformed(ActionEvent e) {
			onEnAttente();
		}
	}

	//
	private final class ActionImprimer extends AbstractAction {
		public ActionImprimer() {
			super("Imprimer valides");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer les Intérêts moratoires valides.");
		}

		public void actionPerformed(ActionEvent e) {
			onImprimer(true);
		}
	}

	private final class ActionImprimerAll extends AbstractAction {
		public ActionImprimerAll() {
			super("Tout Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer tous les intérêts moratoires.");
		}

		public void actionPerformed(ActionEvent e) {
			onImprimer(false);
		}
	}

	//	private final class ActionAnnulerOp extends AbstractAction {
	//		public ActionAnnulerOp() {
	//			super("Annuler OP");
	//			//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
	//			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			onAnnulerOp();
	//		}
	//	}

	private final class ActionModifier extends AbstractAction {
		public ActionModifier() {
			super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onModifier();
		}
	}

	private final class ActionRejeter extends AbstractAction {
		public ActionRejeter() {
			super("Rejeter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ERROR_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onRejeter();
		}
	}

	private final class ActionReinitialiser extends AbstractAction {
		public ActionReinitialiser() {
			super("Reinitialiser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onReinitialiser();
		}
	}

	private final class ActionReinitialiserTous extends AbstractAction {
		public ActionReinitialiserTous() {
			super("Tout réinitialiser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			if (myPanel.getImListTablePanel().getMyDisplayGroup().allObjects().count() > 0) {
				if (showConfirmationDialog("Générer tous les intérêts moratoires", "Souhaitez-vous vraiment réinitialiser tous les intérêts moratoires pour ce paiement avec des valeurs pré-calculées ? \n Vous allez perdre toutes les modifications que vous avez pu faire manuellement.",
						ZMsgPanel.BTLABEL_YES)) {
					onReinitialiserTous();
				}

			}
			else {
				onReinitialiserTous();
			}
		}
	}

	//
	//	private boolean isCreationOpAutorisee() {
	//		try {
	//			return myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), "PAOPSA");
	//		} catch (Exception e) {
	//			return false;
	//		}
	//	}
	//
	//	private boolean isAnnulerOpAutorisee() {
	//		try {
	//			return myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), "PAOPSU");
	//		} catch (Exception e) {
	//			return false;
	//		}
	//	}

	private static final class ImSrchTableCellRenderer extends ZEOTableCellRenderer {
		private static final Image IMG_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16).getImage();
		private static final Image IMG_CANCEL = ZIcon.getIconForName(ZIcon.ICON_ERROR_16).getImage();
		private static final ZEOTable.ZImageCell CELL_OK = new ZEOTable.ZImageCell(new Image[] {
				IMG_OK
		});
		private static final ZEOTable.ZImageCell CELL_CANCEL = new ZEOTable.ZImageCell(new Image[] {
				IMG_CANCEL
		});
		static {
			CELL_OK.setCentered(true);
			CELL_CANCEL.setCentered(true);
		}

		//		private static final Icon ICON_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16);
		//		private static final JLabel LABEL_OK = new JLabel( ICON_OK);
		//		private static Color COL_CLASSE = Color.decode("#F9CC8A");
		//		private static Color COL_CHAPITRE = Color.decode("#DCEEA6");

		static {
			CELL_OK.setLayout(new BorderLayout());
			CELL_CANCEL.setLayout(new BorderLayout());
		}

		public ImSrchTableCellRenderer() {
			super();
		}

		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			Component res = null;
			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOIm obj = (EOIm) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);

			final ZEOTableModelColumn column2 = ((ZEOTable) table).getDataModel().getColumn(table.convertColumnIndexToModel(column));

			//System.out.println(column2.getAttributeName());

			if (ImSrchPanel.ImListTablePanel.TYPE_ETAT_KEY.equals(column2.getAttributeName())) {
				if (EOTypeEtat.ETAT_VALIDE.equals(value)) {
					res = CELL_OK;
				}
				else if (EOTypeEtat.ETAT_ANNULE.equals(value)) {
					res = CELL_CANCEL;
				}
				else {
					res = super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
				}
			}
			else {
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}

			//////

			if (isSelected) {
				res.setBackground(table.getSelectionBackground());
				res.setForeground(table.getSelectionForeground());
			}
			else {
				//				if (obj.isClasse()) {
				//					res.setBackground(COL_CLASSE);
				//					res.setForeground(table.getForeground());
				//				}
				//				else if (obj.isChapitre()) {
				//					res.setBackground(COL_CHAPITRE);
				//					res.setForeground(table.getForeground());
				//				}
				//				else {
				//					res.setBackground(table.getBackground());
				//					res.setForeground(table.getForeground());
				//				}

				res.setBackground(table.getBackground());
				res.setForeground(table.getForeground());
			}
			return res;
		}
	}
}
