/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.zutil.client.ui.ZToolBar;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RelanceListSrchPanel extends ZKarukeraPanel {

    private final IRelanceListSrchPanelListener myListener;
//    private final RelanceListSrchFilterPanel relanceListSrchFilterPanel;
    private final MyRelanceRecetteListPanel relanceRecetteListPanel;


    /**
     * @param editingContext
     */
    public RelanceListSrchPanel(IRelanceListSrchPanelListener listener) {
        super();
        myListener = listener;
        relanceRecetteListPanel = new MyRelanceRecetteListPanel(myListener.relanceRecetteListListener());
//        relanceListSrchFilterPanel = new RelanceListSrchFilterPanel(myListener.relanceSrchFilterPanelListener());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
//        relanceListSrchFilterPanel.initGUI();
        relanceRecetteListPanel.initGUI();

        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));


        this.add(encloseInPanelWithTitle("Toutes les relances correspondant aux critères",null,ZConst.BG_COLOR_TITLE,buildRelancePanel(),null, null), BorderLayout.CENTER);
        
//        this.add(buildRightPanel(), BorderLayout.EAST);
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        relanceRecetteListPanel.updateData();
        myListener.getActionRefreshRelances().setEnabled(false);
    }




    private final JPanel buildRelancePanel() {
        final ArrayList list = new ArrayList();
        list.add(myListener.getActionRefreshRelances());
        list.add(myListener.getActionRelanceImprimer());
        final ZToolBar toolBar = new ZToolBar(null, JToolBar.HORIZONTAL,list);
        final JPanel panel = new JPanel(new BorderLayout());
//        this.add(encloseInPanelWithTitle("Filtres de recherche",null,null,null,relanceListSrchFilterPanel,null,null), BorderLayout.NORTH);
        panel.add(toolBar, BorderLayout.NORTH);
        panel.add(relanceRecetteListPanel, BorderLayout.CENTER);
//        panel.add(toolBar, BorderLayout.NORTH);
        return panel;
    }





    public interface IRelanceListSrchPanelListener {
        public Action getActionRelanceImprimer();
        public Action getActionRefreshRelances();
        public IZKarukeraTablePanelListener relanceRecetteListListener();
    }






    

    public final class MyRelanceRecetteListPanel extends ZKarukeraTablePanel {
        public static final String COL_CONTACT="rerContact";
        public static final String COL_DATE_CREATION="rerDateCreation";
        public static final String COL_DATE_IMPRESSION="rerDateImpression";
        public static final String COL_LIBELLE="rerLibelle";
        public static final String COL_MONTANT="rerMont";
//        public static final String COL_NUMERO="rerNumero";
        public static final String COL_UTILISATEUR="utilisateur.nomAndPrenom";
        public static final String COL_TYPE_NIVEAU="typeRelance.trlNiveau";
        public static final String COL_TYPE_NOM="typeRelance.trlNom";
        public static final String COL_DATE_LIMITE_PAIEMENT="dateLimitePaiement";
        
        public static final String COL_BORNUM="recette.titre.bordereau.borNum";
        public static final String COL_TIT_NUMERO="recette.titre.titNumero";
        public static final String COL_GESTION="recette.titre.gestion.gesCode";
        public static final String COL_CLIENT="recette.clientAvecSeparateur";
        public static final String COL_TIT_LIBELLE="recette.recLibelle";
        


        /**
         * @param listener
         */
        public MyRelanceRecetteListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);

            ZEOTableModelColumn rerContact = new ZEOTableModelColumn(myDisplayGroup, COL_CONTACT, "Contact", 80);
            rerContact.setAlignment(SwingConstants.LEFT);

            ZEOTableModelColumn trlNom = new ZEOTableModelColumn(myDisplayGroup, COL_TYPE_NOM, "Nom de relance", 180);
            trlNom.setAlignment(SwingConstants.LEFT);

            ZEOTableModelColumn rerLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_LIBELLE, "Libellé", 180);
            rerLibelle.setAlignment(SwingConstants.LEFT);

            ZEOTableModelColumn utilisateur = new ZEOTableModelColumn(myDisplayGroup, COL_UTILISATEUR, "Créée/modifiée par", 110);
            utilisateur.setAlignment(SwingConstants.LEFT);

            ZEOTableModelColumn rerMont = new ZEOTableModelColumn(myDisplayGroup, COL_MONTANT, "Montant à recouvrer", 100);
            rerMont.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
            rerMont.setAlignment(SwingConstants.RIGHT);
            rerMont.setColumnClass(BigDecimal.class);

            ZEOTableModelColumn trlNiveau = new ZEOTableModelColumn(myDisplayGroup, COL_TYPE_NIVEAU, "Niveau", 50);
            trlNiveau.setFormatDisplay(ZConst.FORMAT_ENTIER_BRUT);
            trlNiveau.setAlignment(SwingConstants.CENTER);
            trlNiveau.setColumnClass(Integer.class);

            ZEOTableModelColumn rerDateCreation = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_CREATION, "Dernière modification le", 100);
            rerDateCreation.setFormatDisplay(ZConst.FORMAT_DATESHORT);
            rerDateCreation.setAlignment(SwingConstants.CENTER);
            rerDateCreation.setColumnClass(Date.class);

            ZEOTableModelColumn rerDateImpression = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_IMPRESSION, "Dernière impression le", 100);
            rerDateImpression.setFormatDisplay(ZConst.FORMAT_DATESHORT);
            rerDateImpression.setAlignment(SwingConstants.CENTER);
            rerDateImpression.setColumnClass(Date.class);


            ZEOTableModelColumn recDateLimitePaiement = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_LIMITE_PAIEMENT, "Limite de paiement", 80);
            recDateLimitePaiement.setFormatDisplay(ZConst.FORMAT_DATESHORT);
            recDateLimitePaiement.setAlignment(SwingConstants.CENTER);
            recDateLimitePaiement.setColumnClass(Date.class);


            ZEOTableModelColumn bornm = new ZEOTableModelColumn(myDisplayGroup, COL_BORNUM, "Bordereau", 70);
            bornm.setAlignment(SwingConstants.CENTER);
            bornm.setColumnClass(Integer.class);

            ZEOTableModelColumn coltitdat = new ZEOTableModelColumn(myDisplayGroup, COL_TIT_NUMERO, "N° titre", 70);
            coltitdat.setAlignment(SwingConstants.CENTER);
            coltitdat.setColumnClass(Integer.class);

//            ZEOTableModelColumn dateRemise = new ZEOTableModelColumn(myDisplayGroup, COL_TIT_DATE_REMISE, "Date remise", 80);
//            dateRemise.setFormatDisplay(ZConst.FORMAT_DATESHORT);
//            dateRemise.setAlignment(SwingConstants.CENTER);
//            dateRemise.setColumnClass(Date.class);
//
            ZEOTableModelColumn codeGestion = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code gestion", 70);
            codeGestion.setAlignment(SwingConstants.CENTER);


            ZEOTableModelColumn fournisseur = new ZEOTableModelColumn(myDisplayGroup, COL_CLIENT, "Client", 200);
            fournisseur.setAlignment(SwingConstants.LEFT);            
            
            ZEOTableModelColumn titLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_TIT_LIBELLE, "Libellé", 370);
            titLibelle.setAlignment(SwingConstants.LEFT);
            
            
    		colsMap.clear();
    		colsMap.put(COL_GESTION ,codeGestion);
    		colsMap.put(COL_BORNUM , bornm);
    		colsMap.put(COL_TIT_NUMERO , coltitdat);
    		colsMap.put(COL_TIT_LIBELLE , titLibelle);
    		colsMap.put(COL_TYPE_NIVEAU ,trlNiveau);
    		colsMap.put(COL_LIBELLE ,rerLibelle);
    		colsMap.put(COL_MONTANT ,rerMont);
    		colsMap.put(COL_DATE_CREATION ,rerDateCreation);
    		colsMap.put(COL_DATE_IMPRESSION ,rerDateImpression);
    		colsMap.put(COL_DATE_LIMITE_PAIEMENT,recDateLimitePaiement);
    		colsMap.put(COL_UTILISATEUR ,utilisateur);
        }


        public void initGUI() {
            super.initGUI();
            myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        }
        
        
        
        public final NSArray getSelectedObjects() {
            return myTableModel.getSelectedEOObjects();
        }
        
    }







    public final MyRelanceRecetteListPanel getRelanceRecetteListPanel() {
        return relanceRecetteListPanel;
    }


}
