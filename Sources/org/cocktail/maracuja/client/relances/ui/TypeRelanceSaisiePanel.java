/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextArea;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class TypeRelanceSaisiePanel extends ZKarukeraPanel {

	private final ITypeRelanceSaisiePanelListener myListener;

	private final ZFormPanel trlDelaiPaiement;
	private final ZFormPanel trlContactSuivi;
	private final ZFormPanel trlNom;
	private final ZFormPanel trlNiveau;
	private final ZFormPanel trlLibelle1;
	private final ZFormPanel trlLibelle2;
	private final ZFormPanel trlLibelle3;
	private final ZFormPanel trlSign;
	private final ZFormPanel trlReportId;

	private final PopupVariables menuVariablesPar1;
	private final PopupVariables menuVariablesPar2;
	private final PopupVariables menuVariablesPar3;

	/**
     *
     */
	public TypeRelanceSaisiePanel(ITypeRelanceSaisiePanelListener listener) {
		myListener = listener;

		//        System.out.println("TypeRelanceSaisiePanel.TypeRelanceSaisiePanel() 1");
		//        ZLogger.debug( myListener.getValues());

		trlNom = ZFormPanel.buildLabelField("Nom du type / Objet", new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlNom"), 110);
		((ZTextField) trlNom.getMyFields().get(0)).getMyTexfield().setColumns(30);

		trlContactSuivi = ZFormPanel.buildLabelField("Contact par défaut", new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlContactSuivi")), 110);
		((ZTextArea) trlContactSuivi.getMyFields().get(0)).getMyTextArea().setColumns(35);
		((ZTextArea) trlContactSuivi.getMyFields().get(0)).getMyTextArea().setRows(2);

		trlReportId = ZFormPanel.buildLabelField("Modèle d'impression", new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlReportId"), 110);
		((ZTextField) trlReportId.getMyFields().get(0)).getMyTexfield().setColumns(20);

		trlNiveau = ZFormPanel.buildLabelField("Niveau", new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.getValues(), "trlNiveau"), new Format[] {
			ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT));
		((ZTextField) trlNiveau.getMyFields().get(0)).getMyTexfield().setColumns(3);
		((ZTextField) trlNiveau.getMyFields().get(0)).getMyTexfield().setHorizontalAlignment(JTextField.CENTER);

		trlDelaiPaiement = ZFormPanel.buildLabelField("Délai de paiement par défaut", new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.getValues(), "trlDelaiPaiement"), new Format[] {
			ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT));
		((ZTextField) trlDelaiPaiement.getMyFields().get(0)).getMyTexfield().setColumns(3);
		((ZTextField) trlDelaiPaiement.getMyFields().get(0)).getMyTexfield().setHorizontalAlignment(JTextField.CENTER);

		trlLibelle1 = ZFormPanel.buildLabelField("Premier paragraphe", new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlLibelle1")), 110);
		((ZTextArea) trlLibelle1.getMyFields().get(0)).getMyTextArea().setRows(6);
		((ZTextArea) trlLibelle1.getMyFields().get(0)).getMyTextArea().setColumns(100);

		trlLibelle2 = ZFormPanel.buildLabelField("Deuxième paragraphe", new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlLibelle2")), 110);
		((ZTextArea) trlLibelle2.getMyFields().get(0)).getMyTextArea().setRows(6);
		((ZTextArea) trlLibelle2.getMyFields().get(0)).getMyTextArea().setColumns(100);

		trlLibelle3 = ZFormPanel.buildLabelField("Troisième paragraphe", new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlLibelle3")), 110);
		((ZTextArea) trlLibelle3.getMyFields().get(0)).getMyTextArea().setRows(6);
		((ZTextArea) trlLibelle3.getMyFields().get(0)).getMyTextArea().setColumns(100);

		trlSign = ZFormPanel.buildLabelField("Signature de la lettre", new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "trlSign")), 110);
		((ZTextArea) trlSign.getMyFields().get(0)).getMyTextArea().setRows(3);
		((ZTextArea) trlSign.getMyFields().get(0)).getMyTextArea().setColumns(50);

		menuVariablesPar1 = new PopupVariables(((ZTextArea) trlLibelle1.getMyFields().get(0)).getMyTextArea());
		menuVariablesPar2 = new PopupVariables(((ZTextArea) trlLibelle2.getMyFields().get(0)).getMyTextArea());
		menuVariablesPar3 = new PopupVariables(((ZTextArea) trlLibelle3.getMyFields().get(0)).getMyTextArea());

		menuVariablesPar1.selfRegister();
		menuVariablesPar2.selfRegister();
		menuVariablesPar3.selfRegister();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public final void initGUI() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		final JLabel labelNom = ZTooltip.createTooltipLabel("Nom du type / Objet", "Vous permet d'identifier le type de relance. Ce nom figurera en objet de la lettre de relance.");
		final JLabel labelContact = ZTooltip.createTooltipLabel("Contact",
				"Il s'agit des références de la personne dans l'établissement <br>qui doit être contactée à propos des relances de ce type. <br>Vous pouvez mettre les informations sur deux ou trois lignes.<br>(Ce texte peut être modifié ensuite pour chaque relance).");
		final JLabel labelReportId = ZTooltip.createTooltipLabel("Modèle d'impression", "Spécifie l'identifiant du modèle d'impression à utiliser pour<br>imprimer les relances de ce type.");
		final JLabel labelDelaiPaiement = ZTooltip.createTooltipLabel("Délai de paiement", "Spécifie le nombre de jours avant d'effectuer une nouvelle<br>relance. Ce délai peut ensuite être modifié pour chaque relance.");

		final Box box = Box.createVerticalBox();

		box.add(buildLine(new Component[] {
				trlNom, labelNom, trlNiveau, trlDelaiPaiement, labelDelaiPaiement
		}));
		box.add(buildLine(new Component[] {
				trlContactSuivi, labelContact
		}));
		box.add(buildLine(new Component[] {
			trlLibelle1
		}));
		box.add(buildLine(new Component[] {
			trlLibelle2
		}));
		box.add(buildLine(new Component[] {
			trlLibelle3
		}));
		box.add(buildLine(new Component[] {
			trlSign
		}));
		box.add(buildLine(new Component[] {
				trlReportId, labelReportId
		}));

		final Box col1 = Box.createVerticalBox();
		col1.add(box);
		col1.add(new JPanel(new BorderLayout()));

		//        Box boxH = Box.createHorizontalBox();
		//        boxH.add(col1);
		//        boxH.add(Box.createGlue());
		this.add(col1, BorderLayout.CENTER);

		//        this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public final void updateData() throws Exception {
		trlDelaiPaiement.updateData();
		trlContactSuivi.updateData();
		trlNom.updateData();
		trlNiveau.updateData();
		trlLibelle1.updateData();
		trlLibelle2.updateData();
		trlLibelle3.updateData();
		trlSign.updateData();
		trlReportId.updateData();
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public interface ITypeRelanceSaisiePanelListener {
		public HashMap getValues();

		public Action actionClose();

		public Action actionValider();
	}

	private final class PopupVariables extends JPopupMenu {
		private final JTextComponent _component;

		public PopupVariables(JTextComponent component) {
			_component = component;
			add(new ActionVariableMontant());
			add(new ActionVariableDelai());

		}

		public final void selfRegister() {
			_component.addMouseListener(new MouseAdapter() {
				public void mousePressed(MouseEvent evt) {
					if (evt.isPopupTrigger()) {
						show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}

				public void mouseReleased(MouseEvent evt) {
					if (evt.isPopupTrigger()) {
						show(evt.getComponent(), evt.getX(), evt.getY());
					}
				}
			});
		}

		private final class ActionVariableMontant extends AbstractAction {

			/**
             *
             */
			public ActionVariableMontant() {
				super("Insérer le montant");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(final ActionEvent e) {
				try {
					_component.getDocument().insertString(_component.getCaretPosition(), "#MONTANT#", null);
				} catch (BadLocationException e1) {
					e1.printStackTrace();
				}

			}

		}

		private final class ActionVariableDelai extends AbstractAction {

			/**
             *
             */
			public ActionVariableDelai() {
				super("Insérer le délai de paiement");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(final ActionEvent e) {
				try {
					_component.getDocument().insertString(_component.getCaretPosition(), "#DELAI#", null);
				} catch (BadLocationException e1) {
					e1.printStackTrace();
				}

			}

		}
	}

}
