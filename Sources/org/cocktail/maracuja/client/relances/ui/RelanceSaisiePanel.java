/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;


import java.awt.BorderLayout;
import java.awt.Component;
import java.math.BigDecimal;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextArea;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

import com.webobjects.eocontrol.EOEnterpriseObject;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class RelanceSaisiePanel extends ZKarukeraPanel {

    private final IRelanceSaisiePanelListener myListener;
    private final ZFormPanel rerContact;
    private final ZFormPanel rerLibelle;
    private final ZFormPanel rerMont;
    private final ZFormPanel rerPs;
    private final ZNumberField recTitreNum;
    private final ZFormPanel recTitreNum2;
    private final ZFormPanel gesCode;
    private final ZFormPanel recClient;
    private final ZFormPanel recLibelle;
    private final ZFormPanel recResteRecouvrer;
    private final ZFormPanel trlNiveau;
    private final ZFormPanel trlNom;
    private final ZFormPanel rerDelaiPaiement;
    private final JLabel rerDateLimitePaiement;
    private ZFormPanel dateLimitePaiementIntiale;
    private ZFormPanel recMontant;


    /**
     *
     */
    public RelanceSaisiePanel(IRelanceSaisiePanelListener listener) {
        myListener = listener;

//        System.out.println("RelanceSaisiePanel.RelanceSaisiePanel() 1");
//        ZLogger.debug( myListener.getValues());

        rerContact = ZFormPanel.buildLabelField("Contact",  new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "rerContact")) , 110);
        rerLibelle = ZFormPanel.buildLabelField("Libellé",  new ZTextField.DefaultTextFieldModel(myListener.getValues(), "rerLibelle") , 110);
        rerPs = ZFormPanel.buildLabelField("Post-Scriptum",   new ZTextField.DefaultTextFieldModel(myListener.getValues(), "rerPs")  ,110);
        rerMont = ZFormPanel.buildLabelField("Montant à recouvrer", new ZNumberField( new MontantModel(), new Format[]{ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL}, ZConst.FORMAT_DECIMAL )  , 110);
        rerDelaiPaiement = ZFormPanel.buildLabelField("Délai du paiement (jours)",  new ZNumberField( new DelaiModel(), new Format[]{ZConst.FORMAT_ENTIER}, ZConst.FORMAT_ENTIER ) );
        ((ZTextField)rerDelaiPaiement.getMyFields().get(0)).getMyTexfield().setColumns(3);
        rerDateLimitePaiement = new JLabel();


        ((ZTextField)rerDelaiPaiement.getMyFields().get(0)).addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
                updateDateLimitePaiement();
            }

            public void insertUpdate(DocumentEvent e) {
                updateDateLimitePaiement();
            }

            public void removeUpdate(DocumentEvent e) {
                updateDateLimitePaiement();
            }});



        recClient = ZFormPanel.buildLabelField("Client",  new ZTextArea(new ClientModel()) );
        ((ZTextArea)recClient.getMyFields().get(0)).getMyTextArea().setColumns(60);
        ((ZTextArea)recClient.getMyFields().get(0)).getMyTextArea().setRows(6);
        ((ZTextArea)recClient.getMyFields().get(0)).getMyTextComponent().setEditable(false);

//        recClient = ZFormPanel.buildLabelField("Client",  new ClientModel() );
//        ((ZTextField)recClient.getMyFields().get(0)).getMyTexfield().setColumns(60);
//        ((ZTextField)recClient.getMyFields().get(0)).getMyTexfield().setEditable(false);
//



        recLibelle = ZFormPanel.buildLabelField("Libellé",  new EOTextFieldModel( myListener.getValues(), "recette"  , EORecette.REC_LIBELLE_KEY) );
        ((ZTextField)recLibelle.getMyFields().get(0)).getMyTexfield().setColumns(60);
        ((ZTextField)recLibelle.getMyFields().get(0)).getMyTexfield().setEditable(false);

        recResteRecouvrer = ZFormPanel.buildLabelField("Reste à recouvrer", new ZNumberField(  new ZTextField.DefaultTextFieldModel(myListener.getValues(), "recResteRecouvrer"), new Format[]{ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL}, ZConst.FORMAT_DECIMAL )  );
        ((ZTextField)recResteRecouvrer.getMyFields().get(0)).getMyTexfield().setColumns(10);
        ((ZTextField)recResteRecouvrer.getMyFields().get(0)).getMyTexfield().setEditable(false);

        recMontant = ZFormPanel.buildLabelField("Montant initial", new ZNumberField(  new EOTextFieldModel( myListener.getValues(), "recette"  , EORecette.REC_MONT_TTC_KEY), new Format[]{ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL}, ZConst.FORMAT_DECIMAL )  );
        ((ZTextField)recMontant.getMyFields().get(0)).getMyTexfield().setColumns(10);
        ((ZTextField)recMontant.getMyFields().get(0)).getMyTexfield().setEditable(false);



        recTitreNum = new ZNumberField(new EOTextFieldModel( myListener.getValues(), "recette"  , "titre.titNumero"),new Format[]{ZConst.FORMAT_ENTIER_BRUT}, ZConst.FORMAT_ENTIER_BRUT );
        recTitreNum.setEnabled(false);
        recTitreNum.getMyTexfield().setColumns(10);
        recTitreNum.getMyTexfield().setEditable(false);
        recTitreNum2 = ZFormPanel.buildLabelField("N°", recTitreNum);

        gesCode = ZFormPanel.buildLabelField("Code gestion",  new EOTextFieldModel( myListener.getValues(), "recette"  , "titre.gestion.gesCode") );
        gesCode.setEnabled(false);
        ((ZTextField)gesCode.getMyFields().get(0)).getMyTexfield().setColumns(10);
        ((ZTextField)gesCode.getMyFields().get(0)).getMyTexfield().setEditable(false);

        dateLimitePaiementIntiale = ZFormPanel.buildLabelField("Date limite de paiement initiale",   new EOTextFieldModel( myListener.getValues(), "recette"  , EORecette.REC_DATE_LIMITE_PAIEMENT_KEY) );
        dateLimitePaiementIntiale.setEnabled(false);
        ((ZTextField)dateLimitePaiementIntiale.getMyFields().get(0)).setFormat(ZConst.FORMAT_DATESHORT);
        ((ZTextField)dateLimitePaiementIntiale.getMyFields().get(0)).getMyTexfield().setColumns(10);
        ((ZTextField)dateLimitePaiementIntiale.getMyFields().get(0)).getMyTexfield().setEditable(false);



        trlNiveau = ZFormPanel.buildLabelField("Niveau",  new EOTextFieldModel( myListener.getValues(), "typeRelance"  , "trlNiveau") );
        trlNiveau.setEnabled(false);
        ((ZTextField)trlNiveau.getMyFields().get(0)).getMyTexfield().setColumns(3);
        ((ZTextField)trlNiveau.getMyFields().get(0)).getMyTexfield().setEditable(false);
        ((ZTextField)trlNiveau.getMyFields().get(0)).getMyTexfield().setHorizontalAlignment(JTextField.CENTER);

        trlNom = ZFormPanel.buildLabelField("Type",  new EOTextFieldModel( myListener.getValues(), "typeRelance"  , "trlNom") );
        trlNom.setEnabled(false);
        ((ZTextField)trlNom.getMyFields().get(0)).getMyTexfield().setColumns(30);
        ((ZTextField)trlNom.getMyFields().get(0)).getMyTexfield().setEditable(false);

    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
     */
    public final void initGUI() {
        setLayout(new BorderLayout());

        ((ZTextField)recClient.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)recLibelle.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)recResteRecouvrer.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)recTitreNum2.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)gesCode.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)trlNom.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)trlNiveau.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)dateLimitePaiementIntiale.getMyFields().get(0)).setUIReadOnly();
        ((ZTextField)recMontant.getMyFields().get(0)).setUIReadOnly();


        final Box boxTitre = Box.createVerticalBox();
        boxTitre.setBorder(BorderFactory.createTitledBorder("Facture sur laquelle porte la relance"));
        boxTitre.add(buildLine(new Component[]{gesCode, recTitreNum2, recLibelle}));
        boxTitre.add(buildLine(recClient));
//        boxTitre.add(buildLine(recLibelle));
        boxTitre.add(buildLine(new Component[]{recMontant, recResteRecouvrer, dateLimitePaiementIntiale }));



        ((ZTextArea)rerContact.getMyFields().get(0)).getMyTextArea().setColumns(50);
        ((ZTextArea)rerContact.getMyFields().get(0)).getMyTextArea().setRows(2);

        ((ZTextField)rerLibelle.getMyFields().get(0)).getMyTexfield().setColumns(80);
        ((ZTextField)rerPs.getMyFields().get(0)).getMyTexfield().setColumns(80);
        ((ZTextField)rerMont.getMyFields().get(0)).getMyTexfield().setColumns(10);


        final JLabel labelContact = ZTooltip.createTooltipLabel("Contact", "Il s'agit des références de la personne dans l'établissement<br>qui doit être contactée à propos de la relance.");
        final JLabel labelLibelle = ZTooltip.createTooltipLabel("Libellé", "Libellé / Objet de la lettre de relance.");
        final JLabel labelPs = ZTooltip.createTooltipLabel("Post-Scriptum", "Post-Scriptum qui va figurer en bas de la lettre de relance.");
        final JLabel labelMont = ZTooltip.createTooltipLabel("Reste à recouvrer", "Montant restant à payer par le client pour cette facture. Ce montant est initialement calculé à partir des émargements, mais vous pouvez le modifier.");



        final Box box = Box.createVerticalBox();
        box.setBorder(BorderFactory.createTitledBorder("Détail de la relance"));

        box.add(buildLine(new Component[]{trlNom, trlNiveau}));
        box.add(buildLine(new Component[]{rerDelaiPaiement, rerDateLimitePaiement}));
        box.add(buildLine(new Component[]{rerContact, labelContact}));
        box.add(buildLine(new Component[]{rerLibelle, labelLibelle}));
        box.add(buildLine(new Component[]{rerPs, labelPs}));
        box.add(buildLine(new Component[]{rerMont, labelMont }));



        final Box col1 = Box.createVerticalBox();
        col1.add(boxTitre);
        col1.add(box);
        col1.add(new JPanel(new BorderLayout()));


//        Box boxH = Box.createHorizontalBox();
//        boxH.add(col1);
//        boxH.add(Box.createGlue());
        this.add(col1, BorderLayout.CENTER);

//        this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);

    }

    private final void updateDateLimitePaiement() {
        final String value = ((ZTextField)rerDelaiPaiement.getMyFields().get(0)).getMyTexfield().getText();
        Number delai = null;
        try {
            delai = (Number) ZConst.FORMAT_ENTIER.parseObject(value);
        }
        catch (Exception e) {

        }
        if (delai==null) {
            rerDateLimitePaiement.setText(" (Délai invalide)");
        }
        else {
            final Date dateRelance = (Date)myListener.getValues().get("rerDateCreation");
            final Date fin = ZDateUtil.addDHMS(dateRelance, delai.intValue(), 0,0,0);
            rerDateLimitePaiement.setText(" (".concat(ZConst.FORMAT_DATESHORT.format(fin)).concat(")"));
        }

        ZLogger.debug("delai",delai);

    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
     */
    public final void updateData() throws Exception {
//        ZLogger.debug(myListener.getValues());
        rerContact.updateData();
        rerLibelle.updateData();
        rerPs.updateData();
        rerMont.updateData();
        recMontant.updateData();

        recTitreNum.updateData();
        rerContact.updateData();
        rerLibelle.updateData();
        ((ZTextField)rerLibelle.getMyFields().get(0)).moveCaret(0);
        rerPs.updateData();
        rerMont.updateData();
        recClient.updateData();
        ((ZTextArea)recClient.getMyFields().get(0)).moveCaret(0);


        recLibelle.updateData();
        recResteRecouvrer.updateData();
        gesCode.updateData();

        trlNiveau.updateData();
        trlNom.updateData();
        rerDelaiPaiement.updateData();
        updateDateLimitePaiement();
        dateLimitePaiementIntiale.updateData();
    }



    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionClose());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }



    public interface IRelanceSaisiePanelListener {
        public HashMap getValues();
        public Action actionClose();
        public Action actionValider();
    }


    private final class MontantModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getValues().get("rerMont");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            if (value!=null ) {
                myListener.getValues().put("rerMont", new BigDecimal(((Number)value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP) );
            }
            else {
                myListener.getValues().put("rerMont", null);
            }

        }

    }

    private final class DelaiModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getValues().get("rerDelaiPaiement");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            if (value!=null ) {
                myListener.getValues().put("rerDelaiPaiement", new Integer(((Number)value).intValue() ));
            }
            else {
                myListener.getValues().put("rerDelaiPaiement", null);
            }

        }

    }





    private final class ClientModel implements IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            final EORecette rec = (EORecette) myListener.getValues().get("recette");
            String debiteur = null;
            if (rec != null) {
                debiteur = rec.recDebiteur();
                if (debiteur == null || debiteur.length()==0) {
                    debiteur = rec.titre().fournisseur().getNomAndPrenomAndCode();
                }
            }
            return debiteur;
        }

        /**
         * Non modifiable
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            //non modifiable
        }

    }



    public final class EOTextFieldModel implements IZTextFieldModel {
        private HashMap _map;
        private final String _keyInMap;
        private final String _keyInEo;


        /**
         *
         */
        public EOTextFieldModel(final HashMap map, final String keyInMap, final String keyInEo) {
            _map = map;
            _keyInMap = keyInMap;
            _keyInEo = keyInEo;

//            System.out.println("constr");
//            ZLogger.debug(_map);
        }

        private final EOEnterpriseObject getEo() {
//            System.out.println("ici");
////            ZLogger.debug(_map);
////            ZLogger.debug(_keyInMap);
            return (EOEnterpriseObject) _map.get(_keyInMap);
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
//            ZLogger.debug(getEo());
            final Object val = (_keyInEo.indexOf(".")>0 ? getEo().valueForKeyPath(  _keyInEo  ) : getEo().valueForKey(  _keyInEo  ));
            return val;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            if (_keyInEo.indexOf(".")>0) {
                getEo().takeValueForKeyPath(value, _keyInEo);
            }
            else {
                getEo().takeValueForKey(value, _keyInEo);
            }
        }

    }



}
