/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.RecetteListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.relances.ui.RelanceListSrchPanel.IRelanceListSrchPanelListener;
import org.cocktail.zutil.client.ui.ZToolBar;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RelanceSrchPanel extends ZKarukeraPanel {

	private final IRelanceSrchPanelListener myListener;
	private final RelanceSrchFilterPanel relanceSrchFilterPanel;
	private final MyRecetteListPanel recetteListPanel;
	private final MyRelanceRecetteListPanel relanceRecetteListPanel;
	private final RecetteInfoSaisiePanel recetteInfoSaisiePanel;
	private final RelanceListSrchPanel relanceListSrchPanel;

	/**
	 * @param editingContext
	 */
	public RelanceSrchPanel(IRelanceSrchPanelListener listener) {
		super();
		myListener = listener;
		recetteListPanel = new MyRecetteListPanel(myListener.recetteListListener());
		relanceRecetteListPanel = new MyRelanceRecetteListPanel(myListener.relanceRecetteListListener());
		relanceSrchFilterPanel = new RelanceSrchFilterPanel(myListener.relanceSrchFilterPanelListener());
		recetteInfoSaisiePanel = new RecetteInfoSaisiePanel(myListener.recetteInfoSaisiePanelListener());
		relanceListSrchPanel = new RelanceListSrchPanel(myListener.relanceListSrchPanelListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		relanceSrchFilterPanel.initGUI();
		recetteListPanel.initGUI();
		relanceRecetteListPanel.initGUI();
		recetteInfoSaisiePanel.initGUI();
		relanceListSrchPanel.initGUI();

		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));

		final JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.add("Relances", encloseInPanelWithTitle("Historique des relances pour la recette sélectionnée", null, ZConst.BG_COLOR_TITLE, buildRelancePanel(), null, null));
		tabbedPane.add("Suivi", recetteInfoSaisiePanel);

		final JSplitPane splitPane1 = ZKarukeraPanel.buildVerticalSplitPane(
				encloseInPanelWithTitle("Factures", null, ZConst.BG_COLOR_TITLE, recetteListPanel, null, null),
				tabbedPane);

		final JTabbedPane tabbedPane0 = new JTabbedPane();
		tabbedPane0.add("Factures", splitPane1);
		tabbedPane0.add("Relances", relanceListSrchPanel);

		this.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, relanceSrchFilterPanel, null, null), BorderLayout.NORTH);
		this.add(tabbedPane0, BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		//        this.add(buildRightPanel(), BorderLayout.EAST);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
	}

	//    private final Component buildRightPanel() {
	//        final ArrayList list = new ArrayList();
	//        list.add(myListener.getActionWizard());
	//        list.add(null);
	//        final ZToolBar toolBar = new ZToolBar(null, JToolBar.VERTICAL,list);
	//        return toolBar;
	//    }

	private final JPanel buildBottomPanel() {
		final ArrayList a = new ArrayList();
		a.add(myListener.getActionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private final JPanel buildRelancePanel() {
		final ArrayList list = new ArrayList();
		list.add(myListener.getActionRelanceNew());
		list.add(myListener.getActionRelanceNewAuto());
		list.add(myListener.getActionRelanceModifier());
		list.add(myListener.getActionRelanceSupprimer());
		list.add(myListener.getActionRelanceImprimer());

		final ZToolBar toolBar = new ZToolBar(null, JToolBar.HORIZONTAL, list);
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(relanceRecetteListPanel, BorderLayout.CENTER);
		panel.add(toolBar, BorderLayout.NORTH);
		return panel;
	}

	public interface IRelanceSrchPanelListener {
		public Action getActionWizard();

		public IRelanceListSrchPanelListener relanceListSrchPanelListener();

		public RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener recetteInfoSaisiePanelListener();

		public Action getActionImprimerTout();

		public Action getActionClose();

		public Action getActionRelanceImprimer();

		public Action getActionRelanceModifier();

		public Action getActionRelanceNew();

		public Action getActionRelanceNewAuto();

		public Action getActionRelanceSupprimer();

		public ZKarukeraTablePanel.IZKarukeraTablePanelListener relanceRecetteListListener();

		public ZKarukeraTablePanel.IZKarukeraTablePanelListener recetteListListener();

		public RelanceSrchFilterPanel.IRelanceSrchFilterPanelListener relanceSrchFilterPanelListener();

		//        public Date getDateLimitePaiementForRecette(final EORecette recette);
		/**
		 * @param recette
		 * @return
		 */
		public Object getFlagForRecette(EORecette recette);
	}

	public final class MyRecetteListPanel extends RecetteListPanel {
		public static final String COL_FLAG = "flag";
		public static final String COL_BORNUM = "titre.bordereau.borNum";
		public static final String COL_TIT_NUMERO = "titre.titNumero";
		public static final String COL_GESTION = "titre.gestion.gesCode";
		public static final String COL_CLIENT = "clientAvecSeparateur";
		public static final String COL_PCONUM = "titre.planComptable.pcoNum";
		public static final String COL_ETAT = "titre.titEtat";
		//        public static final String COL_TITTTC="titre.titTtc";
		public static final String COL_TITTTC = "titTtc";
		//        public static final String COL_TIT_LIBELLE="titre.titLibelle";
		public static final String COL_TIT_LIBELLE = "recLibelle";
		//        public static final String COL_RESTE_RECOUVRER="resteRecouvrerReadOnly";
		//        public static final String COL_RESTE_RECOUVRER= EORecette.RECETTE_RESTE_RECOUVRER_RESTE_RECOUVRER_KEY;
		public static final String COL_RESTE_RECOUVRER = "recetteResteRecouvrer.resteRecouvrer";
		//        public static final String COL_RESTE_RECOUVRER="titre.titreResteRecouvrerLight.resteRecouvrer";
		public static final String COL_DATE_LIMITE_PAIEMENT = "recDateLimitePaiement";
		private static final String COL_DATE_PRISE_EN_CHARGE = "titre.titDateRemise";
		private static final String COL_NB_RELANCES = "recetteRelInfo.nbRelances";
		//        private static final String COL_DATE_PROCHAINE_RELANCE = "prochaineDateRelance";
		private static final String COL_DATE_PROCHAINE_RELANCE = "dateProchaineRelance";

		/**
		 * @param listener
		 */
		public MyRecetteListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn bornm = new ZEOTableModelColumn(myDisplayGroup, COL_BORNUM, "Bordereau", 70);
			bornm.setAlignment(SwingConstants.CENTER);
			bornm.setColumnClass(Integer.class);

			ZEOTableModelColumn coltitdat = new ZEOTableModelColumn(myDisplayGroup, COL_TIT_NUMERO, "N° titre", 70);
			coltitdat.setAlignment(SwingConstants.CENTER);
			coltitdat.setColumnClass(Integer.class);

			//            ZEOTableModelColumn dateRemise = new ZEOTableModelColumn(myDisplayGroup, COL_TIT_DATE_REMISE, "Date remise", 80);
			//            dateRemise.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			//            dateRemise.setAlignment(SwingConstants.CENTER);
			//            dateRemise.setColumnClass(Date.class);
			//
			ZEOTableModelColumn codeGestion = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code gestion", 70);
			codeGestion.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn fournisseur = new ZEOTableModelColumn(myDisplayGroup, COL_CLIENT, "Client", 200);
			fournisseur.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn pcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imputation", 80);
			pcoNum.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn titLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_TIT_LIBELLE, "Libellé", 370);
			titLibelle.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn colEtat = new ZEOTableModelColumn(myDisplayGroup, COL_ETAT, "Etat", 80);
			colEtat.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn titTtc = new ZEOTableModelColumn(myDisplayGroup, COL_TITTTC, "Montant TTC", 90);
			titTtc.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			titTtc.setAlignment(SwingConstants.RIGHT);
			titTtc.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn titPriseEnCharge = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_PRISE_EN_CHARGE, "Prise en charge", 85);
			titPriseEnCharge.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			titPriseEnCharge.setAlignment(SwingConstants.CENTER);
			titPriseEnCharge.setColumnClass(Date.class);

			ZEOTableModelColumn recDateLimitePaiement = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_LIMITE_PAIEMENT, "Limite de paiement", 85);
			recDateLimitePaiement.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			recDateLimitePaiement.setAlignment(SwingConstants.CENTER);
			recDateLimitePaiement.setColumnClass(Date.class);

			ZEOTableModelColumn recProchaineRelance = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_PROCHAINE_RELANCE, "Prochaine relance", 85);
			recProchaineRelance.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			recProchaineRelance.setAlignment(SwingConstants.CENTER);
			recProchaineRelance.setColumnClass(Date.class);

			ZEOTableModelColumn resteEmarger = new ZEOTableModelColumn(myDisplayGroup, COL_RESTE_RECOUVRER, "Reste à recouvrer", 90);
			resteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			resteEmarger.setAlignment(SwingConstants.RIGHT);
			resteEmarger.setColumnClass(BigDecimal.class);

			final ZEOTableModelColumn nbRelances = new ZEOTableModelColumn(myDisplayGroup, COL_NB_RELANCES, "Nb relances", 90);
			nbRelances.setAlignment(SwingConstants.CENTER);
			//    		nbRelances.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER);
			nbRelances.setColumnClass(Integer.class);
			//    		
			//    		ZEOTableModelColumn nbRelances = new ZEOTableModelColumnWithProvider("Nb relances", new NbRecettesProvider(),70);
			//    		nbRelances.setAlignment(SwingConstants.CENTER);
			////  		nbRelances.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER);
			//    		nbRelances.setColumnClass(Integer.class);

			ZEOTableModelColumn flag = new ZEOTableModelColumnWithProvider("*", new FlagProvider(), 45);
			flag.setAlignment(SwingConstants.CENTER);
			flag.setColumnClass(Integer.class);

			LinkedHashMap newCols = new LinkedHashMap();
			newCols.put(COL_FLAG, flag);
			newCols.put(COL_GESTION, codeGestion);
			newCols.put(COL_BORNUM, bornm);
			newCols.put(COL_TIT_NUMERO, coltitdat);
			newCols.put(COL_PCONUM, pcoNum);
			newCols.put(COL_CLIENT, fournisseur);
			//    		newCols.put(COL_DEBITEUR ,colsMap.get(COL_DEBITEUR));
			newCols.put(COL_LIBELLE, colsMap.get(COL_LIBELLE));
			newCols.put(COL_DATE_PRISE_EN_CHARGE, titPriseEnCharge);
			newCols.put(COL_DATE_LIMITE_PAIEMENT, recDateLimitePaiement);
			newCols.put(COL_TTC, colsMap.get(COL_TTC));
			newCols.put(COL_RESTE_RECOUVRER, resteEmarger);
			//ATTENTION NE PAS MODIFIER L'ORDRE SANS MODIFIER LE RENDERER RecetteTableCellRenderer
			newCols.put(COL_NB_RELANCES, nbRelances);
			newCols.put(COL_DATE_PROCHAINE_RELANCE, recProchaineRelance);

			colsMap = newCols;
		}

		public void initGUI() {
			super.initGUI();
			myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		}

		/**
		 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
		 */
		public final class FlagProvider implements ZEOTableModelColumnProvider {

			public FlagProvider() {
				super();
			}

			/**
			 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
			 */
			public Object getValueAtRow(int row) {
				final EORecette recette = (EORecette) myDisplayGroup.displayedObjects().objectAtIndex(row);
				return RelanceSrchPanel.this.myListener.getFlagForRecette(recette);
			}

		}

	}

	public final class MyRelanceRecetteListPanel extends ZKarukeraTablePanel {
		public static final String COL_CONTACT = "rerContact";
		public static final String COL_DATE_CREATION = "rerDateCreation";
		public static final String COL_DATE_IMPRESSION = "rerDateImpression";
		public static final String COL_LIBELLE = "rerLibelle";
		public static final String COL_MONTANT = "rerMont";
		//        public static final String COL_NUMERO="rerNumero";
		public static final String COL_UTILISATEUR = "utilisateur.nomAndPrenom";
		public static final String COL_TYPE_NIVEAU = "typeRelance.trlNiveau";
		public static final String COL_TYPE_NOM = "typeRelance.trlNom";
		public static final String COL_DATE_LIMITE_PAIEMENT = "dateLimitePaiement";

		/**
		 * @param listener
		 */
		public MyRelanceRecetteListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn rerContact = new ZEOTableModelColumn(myDisplayGroup, COL_CONTACT, "Contact", 80);
			rerContact.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn trlNom = new ZEOTableModelColumn(myDisplayGroup, COL_TYPE_NOM, "Nom de relance", 180);
			trlNom.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn rerLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_LIBELLE, "Libellé", 180);
			rerLibelle.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn utilisateur = new ZEOTableModelColumn(myDisplayGroup, COL_UTILISATEUR, "Créée/modifiée par", 110);
			utilisateur.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn rerMont = new ZEOTableModelColumn(myDisplayGroup, COL_MONTANT, "Montant à recouvrer", 100);
			rerMont.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			rerMont.setAlignment(SwingConstants.RIGHT);
			rerMont.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn trlNiveau = new ZEOTableModelColumn(myDisplayGroup, COL_TYPE_NIVEAU, "Niveau", 50);
			trlNiveau.setFormatDisplay(ZConst.FORMAT_ENTIER_BRUT);
			trlNiveau.setAlignment(SwingConstants.CENTER);
			trlNiveau.setColumnClass(Integer.class);

			ZEOTableModelColumn rerDateCreation = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_CREATION, "Dernière modification le", 100);
			rerDateCreation.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			rerDateCreation.setAlignment(SwingConstants.CENTER);
			rerDateCreation.setColumnClass(Date.class);

			ZEOTableModelColumn rerDateImpression = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_IMPRESSION, "Dernière impression le", 100);
			rerDateImpression.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			rerDateImpression.setAlignment(SwingConstants.CENTER);
			rerDateImpression.setColumnClass(Date.class);

			ZEOTableModelColumn recDateLimitePaiement = new ZEOTableModelColumn(myDisplayGroup, COL_DATE_LIMITE_PAIEMENT, "Limite de paiement", 80);
			recDateLimitePaiement.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			recDateLimitePaiement.setAlignment(SwingConstants.CENTER);
			recDateLimitePaiement.setColumnClass(Date.class);

			colsMap.clear();
			colsMap.put(COL_TYPE_NIVEAU, trlNiveau);
			colsMap.put(COL_TYPE_NOM, trlNom);
			colsMap.put(COL_LIBELLE, rerLibelle);
			colsMap.put(COL_MONTANT, rerMont);
			colsMap.put(COL_DATE_CREATION, rerDateCreation);
			colsMap.put(COL_DATE_IMPRESSION, rerDateImpression);
			colsMap.put(COL_DATE_LIMITE_PAIEMENT, recDateLimitePaiement);
			colsMap.put(COL_UTILISATEUR, utilisateur);
		}

		public final NSArray getSelectedObjects() {
			return myTableModel.getSelectedEOObjects();
		}

	}

	public final MyRecetteListPanel getRecetteListPanel() {
		return recetteListPanel;
	}

	public final MyRelanceRecetteListPanel getRelanceRecetteListPanel() {
		return relanceRecetteListPanel;
	}

	public final RelanceSrchFilterPanel getRelanceSrchFilterPanel() {
		return relanceSrchFilterPanel;
	}

	public final RecetteInfoSaisiePanel getRecetteInfoSaisiePanel() {
		return recetteInfoSaisiePanel;
	}

	public RelanceListSrchPanel getRelanceListSrchPanel() {
		return relanceListSrchPanel;
	}

}
