/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZToolBar;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextArea;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class RecetteInfoSaisiePanel extends ZKarukeraPanel {

	private final IRecetteInfoSaisiePanelListener myListener;
	private final ZFormPanel recSuivi;
	private final JComboBox myEtatRelance;

	/**
     *
     */
	public RecetteInfoSaisiePanel(IRecetteInfoSaisiePanelListener listener) {
		myListener = listener;
		myEtatRelance = new JComboBox(myListener.getRecEtatRelanceComboBoxModel());

		recSuivi = ZFormPanel.buildLabelField("Historique", new ZTextArea(new ZTextField.DefaultTextFieldModel(myListener.getValues(), "recSuivi")), 110);
		((ZTextArea) recSuivi.getMyFields().get(0)).getMyTextArea().setColumns(80);
		((ZTextArea) recSuivi.getMyFields().get(0)).getMyTextArea().setRows(10);
		((ZTextArea) recSuivi.getMyFields().get(0)).getMyTextArea().getDocument().addDocumentListener(new MyDocumentListener());

		myEtatRelance.addActionListener(new MyComboBoxListener());

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public final void initGUI() {
		setLayout(new BorderLayout());

		final Box boxTitre = Box.createVerticalBox();

		//final JLabel labelMont = ZTooltip.createTooltipLabel("Suivi", "Vous pouvez saisir ici si vous le souhaitez <br>des informations concernant le suivi de la recette.");

		boxTitre.add(buildLine((new ZLabeledComponent("Création de relances", myEtatRelance, ZLabeledComponent.LABELONLEFT, -1))));
		boxTitre.add(buildLine(new Component[] {
			recSuivi
		}));

		final Box col1 = Box.createVerticalBox();
		col1.add(boxTitre);
		col1.add(new JPanel(new BorderLayout()));

		this.add(col1, BorderLayout.CENTER);
		this.add(buildRightPanel(), BorderLayout.NORTH);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public final void updateData() throws Exception {
		//        System.out.println(myListener.getValues());

		if (myListener.getValues().get("recette") == null) {
			((ZTextArea) recSuivi.getMyFields().get(0)).setEnabled(false);
			myEtatRelance.setSelectedItem(null);
			myEtatRelance.setEnabled(false);
			myListener.getActionEnregistrer().setEnabled(false);
			myListener.getActionAnnuler().setEnabled(false);
			//            ((ZTextArea)recSuivi.getMyFields().get(0)).getMyTextComponent().setText(null);
		}
		else {
			((ZTextArea) recSuivi.getMyFields().get(0)).setEnabled(true);
			myEtatRelance.setEnabled(true);
			myListener.getActionEnregistrer().setEnabled(true);
			myListener.getActionAnnuler().setEnabled(true);
			myEtatRelance.getModel().setSelectedItem(myListener.getValues().get("recEtatRelance"));
		}
		recSuivi.updateData();

	}

	private final Component buildRightPanel() {
		final ArrayList list = new ArrayList();
		list.add(myListener.getActionEnregistrer());
		list.add(myListener.getActionAnnuler());
		list.add(null);
		final ZToolBar toolBar = new ZToolBar(null, JToolBar.HORIZONTAL, list);
		return encloseInPanelWithTitle("Suivi de la recette", null, ZConst.BG_COLOR_TITLE, toolBar, null, null);
	}

	public interface IRecetteInfoSaisiePanelListener {
		public HashMap getValues();

		public Action getActionAnnuler();

		public Action getActionEnregistrer();

		public ComboBoxModel getRecEtatRelanceComboBoxModel();

		public void setModified(boolean b);
	}

	private final class MyDocumentListener implements DocumentListener {

		/**
		 * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
		 */
		public void changedUpdate(DocumentEvent e) {
			myListener.setModified(true);
		}

		/**
		 * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
		 */
		public void insertUpdate(DocumentEvent e) {
			myListener.setModified(true);
		}

		/**
		 * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
		 */
		public void removeUpdate(DocumentEvent e) {
			myListener.setModified(true);
		}

	}

	private final class MyComboBoxListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.setModified(true);

		}

	}

}
