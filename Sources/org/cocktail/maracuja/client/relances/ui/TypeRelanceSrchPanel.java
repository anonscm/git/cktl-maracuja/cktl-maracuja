/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.zutil.client.ui.ZToolBar;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TypeRelanceSrchPanel extends ZKarukeraPanel {

    private final ITypeRelanceSrchPanelListener myListener;
    private final MyTypeRelanceListPanel typeRelanceListPanel;


    /**
     * @param editingContext
     */
    public TypeRelanceSrchPanel(ITypeRelanceSrchPanelListener listener) {
        super();
        myListener = listener;
        typeRelanceListPanel = new MyTypeRelanceListPanel(myListener.typeRelanceListListener());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        typeRelanceListPanel.initGUI();

        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));

        this.add(encloseInPanelWithTitle("Types de relance",null,ZConst.BG_COLOR_TITLE,buildTypeRelancePanel(),null, null), BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
    }



    private final JPanel buildBottomPanel() {
        final ArrayList a = new ArrayList();
        a.add(myListener.getActionClose());
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }


    private final JPanel buildTypeRelancePanel() {
        final ArrayList list = new ArrayList();
        list.add(myListener.getActionTypeRelanceNew());
        list.add(myListener.getActionTypeRelanceModifier());
        list.add(myListener.getActionTypeRelanceSupprimer());

        final ZToolBar toolBar = new ZToolBar(null, JToolBar.HORIZONTAL,list);
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(typeRelanceListPanel, BorderLayout.CENTER);
        panel.add(toolBar, BorderLayout.NORTH);
        return panel;
    }





    public interface ITypeRelanceSrchPanelListener {
        public Action getActionClose();

        public Action getActionTypeRelanceModifier();
        public Action getActionTypeRelanceNew();
        public Action getActionTypeRelanceSupprimer();

        public ZKarukeraTablePanel.IZKarukeraTablePanelListener typeRelanceListListener();

    }






    public final class MyTypeRelanceListPanel extends ZKarukeraTablePanel {
        public static final String COL_TRL_NOM="trlNom";
        public static final String COL_TRL_NIVEAU="trlNiveau";
        public static final String COL_TRL_DELAI_PAIEMENT="trlDelaiPaiement";
        public static final String COL_TRL_REPORT_ID="trlReportId";


        /**
         * @param listener
         */
        public MyTypeRelanceListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);


            ZEOTableModelColumn trlNom = new ZEOTableModelColumn(myDisplayGroup, COL_TRL_NOM, "Nom", 200);
            trlNom.setAlignment(SwingConstants.LEFT);

            ZEOTableModelColumn trlReportId = new ZEOTableModelColumn(myDisplayGroup, COL_TRL_REPORT_ID, "Modèle d'impression", 150);
            trlReportId.setAlignment(SwingConstants.LEFT);

            ZEOTableModelColumn trlNiveau = new ZEOTableModelColumn(myDisplayGroup, COL_TRL_NIVEAU, "Niveau", 60);
            trlNiveau.setAlignment(SwingConstants.CENTER);
            trlNiveau.setColumnClass(Integer.class);

            ZEOTableModelColumn trlDelaiPaiement = new ZEOTableModelColumn(myDisplayGroup, COL_TRL_DELAI_PAIEMENT, "Délai de paiement", 60);
            trlDelaiPaiement.setAlignment(SwingConstants.CENTER);
            trlDelaiPaiement.setColumnClass(Integer.class);

    		colsMap.clear();
    		colsMap.put(COL_TRL_NIVEAU ,trlNom);
    		colsMap.put(COL_TRL_NOM ,trlNiveau);
    		colsMap.put(COL_TRL_DELAI_PAIEMENT ,trlDelaiPaiement);
    		colsMap.put(COL_TRL_REPORT_ID ,trlReportId);
        }

    }







    public MyTypeRelanceListPanel getTypeRelanceListPanel() {
        return typeRelanceListPanel;
    }
}
