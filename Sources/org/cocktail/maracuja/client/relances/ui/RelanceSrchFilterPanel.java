/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Window;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RelanceSrchFilterPanel extends ZKarukeraPanel {

	private IRelanceSrchFilterPanelListener myListener;

	private final ZFormPanel gesCode;
	private final ZFormPanel manNumero;
	private final ZFormPanel relDate;
	private final ZFormPanel fournisseur;
	private final ZFormPanel resteEmarger;
	private final ZFormPanel titMontTtc;
	private final ZFormPanel titLibelle;
	private final ZFormPanel pcoNumPanel;

	/**
	 * @param editingContext
	 */
	public RelanceSrchFilterPanel(IRelanceSrchFilterPanelListener listener) {
		super();
		myListener = listener;

		gesCode = ZFormPanel.buildLabelField("Code gestion", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOGestion.GES_CODE_KEY));

		manNumero = ZFormPanel.buildLabelField("N° titre", new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.getFilters(), "titNumero"), new Format[] {
			ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT));
		((ZTextField) manNumero.getMyFields().get(0)).getMyTexfield().setColumns(4);

		relDate = ZFormPanel.buildFourchetteDateFields("<= Date relance <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		//        relDate = ZFormPanel.buildFourchetteDateFields("<= Date relance <=", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "rerDateMin"), new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "rerDateMax"), BORDURE_COLOR, myListener.getDialog() );
		fournisseur = ZFormPanel.buildLabelField("Client", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "client"));
		((ZTextField) fournisseur.getMyFields().get(0)).getMyTexfield().setColumns(20);

		titMontTtc = ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "titMontTtcMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "titMontTtcMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER);
		((ZTextField) titMontTtc.getMyFields().get(0)).getMyTexfield().setColumns(8);
		((ZTextField) titMontTtc.getMyFields().get(1)).getMyTexfield().setColumns(8);

		resteEmarger = ZFormPanel.buildFourchetteNumberFields("<= Reste à recouvrer <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "ecdResteEmargerMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "ecdResteEmargerMax"), ZConst.DECIMAL_EDIT_FORMATS,
				ZConst.FORMAT_EDIT_NUMBER);
		((ZTextField) resteEmarger.getMyFields().get(0)).getMyTexfield().setColumns(8);
		((ZTextField) resteEmarger.getMyFields().get(1)).getMyTexfield().setColumns(8);

		titLibelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EORecette.REC_LIBELLE_KEY));
		((ZTextField) titLibelle.getMyFields().get(0)).getMyTexfield().setColumns(20);

		pcoNumPanel = ZFormPanel.buildLabelField("Compte de tiers", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOPlanComptable.PCO_NUM_KEY));
		((ZTextField) pcoNumPanel.getMyFields().get(0)).getMyTexfield().setColumns(10);

		gesCode.setDefaultAction(myListener.getActionSrch());
		manNumero.setDefaultAction(myListener.getActionSrch());
		relDate.setDefaultAction(myListener.getActionSrch());
		fournisseur.setDefaultAction(myListener.getActionSrch());
		resteEmarger.setDefaultAction(myListener.getActionSrch());
		titMontTtc.setDefaultAction(myListener.getActionSrch());
		titLibelle.setDefaultAction(myListener.getActionSrch());
		pcoNumPanel.setDefaultAction(myListener.getActionSrch());

		setSimpleLineBorder(gesCode);
		setSimpleLineBorder(manNumero);
		setSimpleLineBorder(relDate);
		setSimpleLineBorder(fournisseur);
		setSimpleLineBorder(resteEmarger);
		setSimpleLineBorder(titMontTtc);
		setSimpleLineBorder(titLibelle);
		setSimpleLineBorder(pcoNumPanel);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		setLayout(new BorderLayout());
		setBorder(ZKarukeraPanel.createMargin());
		add(buildFilters(), BorderLayout.CENTER);
		add(buildRightPanel(), BorderLayout.EAST);

	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.getActionSrch());

		ArrayList comps = ZKarukeraPanel.getButtonListFromActionList(list, 50, 50);
		//        JComponent comp = (JComponent) comps.get(0);

		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private final JPanel buildFilters() {

		final ArrayList list = new ArrayList();
		list.add(gesCode);
		list.add(manNumero);
		list.add(relDate);
		list.add(fournisseur);
		list.add(pcoNumPanel);

		final ArrayList list2 = new ArrayList();
		list2.add(titLibelle);
		list2.add(titMontTtc);
		list2.add(resteEmarger);

		Box b = Box.createVerticalBox();
		b.add(buildLine(list));
		b.add(buildLine(list2));
		b.add(new JPanel(new BorderLayout()));

		JPanel p = new JPanel(new BorderLayout());
		p.add(b, BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		gesCode.updateData();
		manNumero.updateData();
		relDate.updateData();
		fournisseur.updateData();
		resteEmarger.updateData();
		titMontTtc.updateData();
		titLibelle.updateData();
		pcoNumPanel.updateData();
	}

	private final class DateSaisieMaxFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get("rerDateMax");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put("rerDateMax", value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	private final class DateSaisieMinFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get("rerDateMin");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put("rerDateMin", value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	public interface IRelanceSrchFilterPanelListener {
		public Action getActionSrch();

		public Dialog getDialog();

		/**
		 * @return un dictionnaire dans lequel sont stockés les filtres.
		 */
		public HashMap getFilters();
	}

}
