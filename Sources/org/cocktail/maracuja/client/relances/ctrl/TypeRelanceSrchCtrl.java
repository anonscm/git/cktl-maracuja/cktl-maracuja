/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.factory.FactoryTypeRelance;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOTypeRelance;
import org.cocktail.maracuja.client.relances.ui.TypeRelanceSrchPanel;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class TypeRelanceSrchCtrl extends CommonCtrl {
	private static final String TITLE = "Gestion des types de relances";
	private static final Dimension WINDOW_DIMENSION = new Dimension(700, 330);

	private TypeRelanceSrchPanel typeRelanceSrchPanel;
	private TypeRelanceSrchPanelListener typeRelanceSrchPanelListener;
	private TypeRelanceListPanelListener typeRelanceListPanelListener;

	private final ActionClose actionClose = new ActionClose();

	private final ActionTypeRelanceModifier actionTypeRelanceModifier = new ActionTypeRelanceModifier();
	private final ActionTypeRelanceNew actionTypeRelanceNew = new ActionTypeRelanceNew();
	private final ActionTypeRelanceSupprimer actionTypeRelanceSupprimer = new ActionTypeRelanceSupprimer();

	//    private final HashMap resteEmargerCache = new HashMap();
	//    private final HashMap dateLimitePaiementRecetteCache = new HashMap();

	/**
	 * @param editingContext
	 */
	public TypeRelanceSrchCtrl(EOEditingContext editingContext) {
		super(editingContext);
	}

	private final void initSubObjects() {
		typeRelanceSrchPanelListener = new TypeRelanceSrchPanelListener();
		typeRelanceListPanelListener = new TypeRelanceListPanelListener();
		typeRelanceSrchPanel = new TypeRelanceSrchPanel(typeRelanceSrchPanelListener);
	}

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		final ZKarukeraDialog win = (dial instanceof Dialog ? new ZKarukeraDialog((Dialog) dial, TITLE, true) : new ZKarukeraDialog((Frame) dial, TITLE, true));

		setMyDialog(win);
		initSubObjects();
		typeRelanceSrchPanel.setPreferredSize(WINDOW_DIMENSION);
		typeRelanceSrchPanel.initGUI();
		win.setContentPane(typeRelanceSrchPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(final Window dial) {
		final ZKarukeraDialog win = createModalDialog(dial);
		try {
			typeRelanceSrchPanel.getTypeRelanceListPanel().updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class TypeRelanceSrchPanelListener implements TypeRelanceSrchPanel.ITypeRelanceSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.TypeRelanceSrchPanel.ITypeRelanceSrchPanelListener#getActionClose()
		 */
		public Action getActionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.TypeRelanceSrchPanel.ITypeRelanceSrchPanelListener#getActionTypeRelanceModifier()
		 */
		public Action getActionTypeRelanceModifier() {
			return actionTypeRelanceModifier;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.TypeRelanceSrchPanel.ITypeRelanceSrchPanelListener#getActionTypeRelanceNew()
		 */
		public Action getActionTypeRelanceNew() {
			return actionTypeRelanceNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.TypeRelanceSrchPanel.ITypeRelanceSrchPanelListener#getActionTypeRelanceSupprimer()
		 */
		public Action getActionTypeRelanceSupprimer() {
			return actionTypeRelanceSupprimer;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.TypeRelanceSrchPanel.ITypeRelanceSrchPanelListener#typeRelanceListListener()
		 */
		public IZKarukeraTablePanelListener typeRelanceListListener() {
			return typeRelanceListPanelListener;
		}
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final void supprimeTypeRelance() {
		try {
			final EOTypeRelance recetteTypeRelance = (EOTypeRelance) typeRelanceSrchPanel.getTypeRelanceListPanel().selectedObject();
			if (recetteTypeRelance != null) {
				if (showConfirmationDialog(
						"Confirmation",
						"Voulez-vous supprimer le type de relance " + recetteTypeRelance.trlNom() + " de niveau " + recetteTypeRelance.trlNiveau().intValue() + " ?",
						ZMsgPanel.BTLABEL_NO)) {
					deleteTypeRelance(recetteTypeRelance);
					ZLogger.debug(getEditingContext());
					try {
						getEditingContext().saveChanges();
					} catch (Exception e) {
						showErrorDialog(e);
						getEditingContext().revert();
					} finally {
						typeRelanceSrchPanel.getTypeRelanceListPanel().updateData();
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void modifTypeRelance() {
		try {
			final EOTypeRelance typeRelance = (EOTypeRelance) typeRelanceSrchPanel.getTypeRelanceListPanel().selectedObject();
			if (typeRelance != null) {
				final HashMap dico = createMapFromTypeRelance(typeRelance);
				final TypeRelanceSaisieCtrl typeRelanceSaisieCtrl = new TypeRelanceSaisieCtrl(getEditingContext(), dico);
				if (typeRelanceSaisieCtrl.openDialog(getMyDialog(), true) == ZKarukeraDialog.MROK) {
					updateTypeRelanceFromMap(typeRelance, dico);

					ZLogger.debug(getEditingContext());
					try {
						getEditingContext().saveChanges();
					} catch (Exception e) {
						showErrorDialog(e);
						getEditingContext().revert();
					} finally {
						typeRelanceSrchPanel.getTypeRelanceListPanel().updateData();
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void addTypeRelance() {
		try {
			final HashMap dico = createTypeRelanceMap();

			if (dico != null) {
				final TypeRelanceSaisieCtrl typeRelanceSaisieCtrl = new TypeRelanceSaisieCtrl(getEditingContext(), dico);
				if (typeRelanceSaisieCtrl.openDialog(getMyDialog(), false) == ZKarukeraDialog.MROK) {
					EOTypeRelance typeRelance = createTypeRelanceFromMap(dico);
					if (typeRelance != null) {
						ZLogger.debug(getEditingContext());
						try {
							getEditingContext().saveChanges();
						} catch (Exception e) {
							showErrorDialog(e);
							getEditingContext().revert();
						} finally {
							typeRelanceSrchPanel.getTypeRelanceListPanel().updateData();
						}
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void updateTypeRelanceFromMap(final EOTypeRelance typeRelance, final Map map) {
		final FactoryTypeRelance factoryTypeRelance = new FactoryTypeRelance(myApp.wantShowTrace());
		factoryTypeRelance.modifierTypeRelance(getEditingContext(), typeRelance,
				(Integer) map.get("trlDelaiPaiement"),
				(String) map.get("trlEtat"),
				(String) map.get("trlLibelle1"),
				(String) map.get("trlLibelle2"),
				(String) map.get("trlLibelle3"),
				(Integer) map.get("trlNiveau"),
				(String) map.get("trlNom"),
				(String) map.get("trlReportId"),
				(String) map.get("trlSign"),
				(String) map.get("trlContactSuivi"));
	}

	private final EOTypeRelance createTypeRelanceFromMap(final Map map) {
		final FactoryTypeRelance factoryTypeRelance = new FactoryTypeRelance(myApp.wantShowTrace());
		final EOTypeRelance typeRelance = factoryTypeRelance.ajouterTypeRelance(
				getEditingContext(),
				(Integer) map.get("trlDelaiPaiement"),
				(String) map.get("trlEtat"),
				(String) map.get("trlLibelle1"),
				(String) map.get("trlLibelle2"),
				(String) map.get("trlLibelle3"),
				(Integer) map.get("trlNiveau"),
				(String) map.get("trlNom"),
				(String) map.get("trlReportId"),
				(String) map.get("trlSign"),
				(String) map.get("trlContactSuivi"));
		return typeRelance;
	}

	private final HashMap createMapFromTypeRelance(final EOTypeRelance typeRelance) throws DefaultClientException {

		if (typeRelance == null) {
			throw new DefaultClientException("typeRelance nul");
		}

		final HashMap map = new HashMap();
		map.put("trlDelaiPaiement", typeRelance.trlDelaiPaiement());
		map.put("trlEtat", typeRelance.trlEtat());
		map.put("trlLibelle1", typeRelance.trlLibelle1());
		map.put("trlLibelle2", typeRelance.trlLibelle2());
		map.put("trlLibelle3", typeRelance.trlLibelle3());
		map.put("trlNiveau", typeRelance.trlNiveau());
		map.put("trlNom", typeRelance.trlNom());
		map.put("trlReportId", typeRelance.trlReportId());
		map.put("trlSign", typeRelance.trlSign());
		map.put("trlContactSuivi", typeRelance.trlContactSuivi());

		return map;
	}

	private final void deleteTypeRelance(final EOTypeRelance typeRelance) {
		final FactoryTypeRelance factoryTypeRelance = new FactoryTypeRelance(myApp.wantShowTrace());
		factoryTypeRelance.annulerTypeRelance(getEditingContext(), typeRelance);
	}

	private final HashMap createTypeRelanceMap() throws Exception {
		final HashMap map = new HashMap();

		return map;
	}

	private final NSArray getTypeRelances() throws Exception {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("trlNiveau", EOSortOrdering.CompareAscending);
		final NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("trlEtat=%@", new NSArray(EOTypeRelance.ETAT_VALIDE)));
		final NSArray res = ZFinder.fetchArray(getEditingContext(), EOTypeRelance.ENTITY_NAME, new EOAndQualifier(quals), new NSArray(sort1), true);
		return res;
	}

	private final class ActionTypeRelanceNew extends AbstractAction {

		public ActionTypeRelanceNew() {
			super("Nouveau");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouveau type de relance");

		}

		public void actionPerformed(final ActionEvent e) {
			addTypeRelance();
		}

	}

	private final class ActionTypeRelanceSupprimer extends AbstractAction {

		public ActionTypeRelanceSupprimer() {
			super("Supprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le type de relance sélectionné");

		}

		public void actionPerformed(final ActionEvent e) {
			supprimeTypeRelance();
		}

	}

	private final class ActionTypeRelanceModifier extends AbstractAction {

		public ActionTypeRelanceModifier() {
			super("Modifier");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le type de relance sélectionné");

		}

		public void actionPerformed(final ActionEvent e) {
			modifTypeRelance();
		}

	}

	private final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(final ActionEvent e) {
			fermer();
		}

	}

	private final class TypeRelanceListPanelListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getTypeRelances();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			modifTypeRelance();

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return typeRelanceSrchPanel;
	}

	public String title() {
		return TITLE;
	}
}
