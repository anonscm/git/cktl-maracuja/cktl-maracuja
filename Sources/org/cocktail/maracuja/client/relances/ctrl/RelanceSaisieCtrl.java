/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.relances.ui.RelanceSaisiePanel;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class RelanceSaisieCtrl extends CommonCtrl {
    private static final String TITLE_NEW = "Création d'une relance";
    private static final String TITLE_MODIFY = "Modification d'une relance";
    private static final Dimension WINDOW_DIMENSION=new Dimension(860,550);

//    private static final int MODE_NEW=0;
//    private static final int MODE_MODIF=1;
//    private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_CHESA;
//    private int modeSaisie=MODE_NEW;


    private final ActionAnnuler actionAnnuler = new ActionAnnuler();
    private final ActionValider actionValider = new ActionValider();


    private RelanceSaisiePanel relanceSaisiePanel;
//    private EORecetteRelance currentEORecetteRelance;
//    private boolean modified;

    private final HashMap values;

    /**
     * @param editingContext
     */
    public RelanceSaisieCtrl(final EOEditingContext editingContext, final HashMap _values) {
        super(editingContext);
        values = _values;
        relanceSaisiePanel = new RelanceSaisiePanel(new RelanceSaisiePanelListener());
    }



    private final ZKarukeraDialog createModalDialog(final Window dial ) {
        final ZKarukeraDialog win;
        
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE_NEW,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE_NEW,true);
        }
        setMyDialog(win);
        relanceSaisiePanel.setMyDialog(getMyDialog());
        relanceSaisiePanel.initGUI();
        relanceSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
        win.setContentPane(relanceSaisiePanel);
        win.pack();
        return win;
    }





    /**
     * Ouvre un dialog de saisie.
     */
    public final int openDialog(final Window dial, final boolean modify) {
        revertChanges();
        final ZKarukeraDialog win = createModalDialog(dial);
        if (modify) {
            win.setTitle(TITLE_MODIFY);
        }
        setMyDialog(win);
        int res=ZKarukeraDialog.MRCANCEL;
        try {
            initSaisie();
            res = win.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
        return res;
    }



    private final boolean checkSaisie() throws Exception {
        boolean res = true;
        if ( ZStringUtil.isEmpty( (String)values.get("rerLibelle") )   ) {
            throw new DataCheckException("Le libellé est obligatoire");
        }

        final BigDecimal mont = (BigDecimal)values.get("rerMont");
        if (mont==null || mont.doubleValue()==0) {
            throw new DataCheckException("Vous ne pouvez pas créer une relance d'un montant nul.");
        }

        final BigDecimal montReste = (BigDecimal) values.get("recResteRecouvrer");
        if (mont.compareTo(montReste)>0) {
            res = showConfirmationDialog("Confirmation", "Le montant qui va figurer sur la relance ("+ ZConst.FORMAT_DECIMAL.format(mont)  +") est supérieur au reste à encaisser calculé à partir des émargements ("+ ZConst.FORMAT_DECIMAL.format(montReste)  +").\nConfirmez-vous ce montant ?", ZMsgPanel.BTLABEL_NO);
        }
        return res;
    }


    private final void validerSaisie() {
        try {
            if (checkSaisie()) {
                getMyDialog().onOkClick();
            }
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
    }


    private final void initSaisie() throws Exception {
//        values = dico;

//        System.out.println();
//        System.out.println("RelanceSaisieCtrl.initSaisie()");
//        ZLogger.debug(values);
        relanceSaisiePanel.updateData();
//        refreshSaveAction();
    }

    public final class ActionValider extends AbstractAction {

	    public ActionValider() {
            super("Enregistrer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
        }

        public void actionPerformed(final ActionEvent e) {
            validerSaisie();

        }

	}

    public final class ActionAnnuler extends AbstractAction {

	    public ActionAnnuler() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
        }

        public void actionPerformed(final ActionEvent e) {
          getMyDialog().onCancelClick();
        }

	}


    private final class RelanceSaisiePanelListener implements RelanceSaisiePanel.IRelanceSaisiePanelListener {

        /**
         * @see org.cocktail.maracuja.client.relances.ui.RelanceSaisiePanel.IRelanceSaisiePanelListener#getValues()
         */
        public HashMap getValues() {
            return values;
        }

        /**
         * @see org.cocktail.maracuja.client.relances.ui.RelanceSaisiePanel.IRelanceSaisiePanelListener#actionClose()
         */
        public Action actionClose() {
            return actionAnnuler;
        }

        /**
         * @see org.cocktail.maracuja.client.relances.ui.RelanceSaisiePanel.IRelanceSaisiePanelListener#actionValider()
         */
        public Action actionValider() {
            return actionValider;
        }

    }
    
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return relanceSaisiePanel;
    }

    public String title() {
        return TITLE_NEW;
    }    

}
