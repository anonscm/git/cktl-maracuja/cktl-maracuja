/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOTypeRelance;
import org.cocktail.maracuja.client.relances.ui.TypeRelanceSaisiePanel;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class TypeRelanceSaisieCtrl extends CommonCtrl {
    private static final String TITLE_NEW = "Création d'un nouveau type de relance";
    private static final String TITLE_MODIFY = "Modification d'un type de relance";
    private static final Dimension WINDOW_DIMENSION=new Dimension(860,600);

    private static final int MODE_NEW=0;
    private static final int MODE_MODIF=1;
//    private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_CHESA;
    private int modeSaisie=MODE_NEW;


    private final ActionAnnuler actionAnnuler = new ActionAnnuler();
    private final ActionValider actionValider = new ActionValider();


    private TypeRelanceSaisiePanel typeRelanceSaisiePanel;





//    private EORecetteTypeRelance currentEORecetteTypeRelance;
//    private boolean modified;

    private final HashMap values;

    /**
     * @param editingContext
     */
    public TypeRelanceSaisieCtrl(final EOEditingContext editingContext, final HashMap _values) {
        super(editingContext);
        values = _values;
        typeRelanceSaisiePanel = new TypeRelanceSaisiePanel(new TypeRelanceSaisiePanelListener());
    }






    private final ZKarukeraDialog createModalDialog(Window dial ) {
        ZKarukeraDialog win;
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE_NEW,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE_NEW,true);
        }
        setMyDialog(win);
        typeRelanceSaisiePanel.setMyDialog(getMyDialog());
        typeRelanceSaisiePanel.initGUI();
        typeRelanceSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
        win.setContentPane(typeRelanceSaisiePanel);
        win.pack();
        return win;
    }





    /**
     * Ouvre un dialog de saisie.
     */
    public final int openDialog(Window dial, final boolean modify) {
        revertChanges();
        ZKarukeraDialog win = createModalDialog(dial);
        if (modify) {
            modeSaisie = MODE_MODIF;
        }
        else {
            modeSaisie = MODE_NEW;
        }
        if (modify) {
            win.setTitle(TITLE_MODIFY);
        }
        setMyDialog(win);
        int res=ZKarukeraDialog.MRCANCEL;
        try {
            initSaisie();
            res = win.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
        return res;
    }



    private final boolean checkSaisie() throws Exception {
        boolean res = true;
        //Vérifier qu'il n'y a pas déjà un type valide du même niveau
        Number trlNiveau = (Number)values.get("trlNiveau");
        if (trlNiveau == null) {
            throw new DataCheckException("Le niveau est obligatoire");
        }

        if (values.get("trlLibelle1") == null) {
            throw new DataCheckException("Le premier paragraphe est obligatoire");
        }

        if (values.get("trlNom") == null) {
            throw new DataCheckException("Le nom / objet est obligatoire");
        }

        if (values.get("trlDelaiPaiement") == null) {
            throw new DataCheckException("Le délai de paiement est obligatoire");
        }

        if (modeSaisie == MODE_NEW) {
	        final EOTypeRelance typeRelance = (EOTypeRelance) EOsFinder.fetchObject(getEditingContext(), EOTypeRelance.ENTITY_NAME, "trlEtat=%@ and trlNiveau=%@", new NSArray(new Object[]{EOTypeRelance.ETAT_VALIDE, trlNiveau}), null, false);
	        if (typeRelance != null) {
	            throw new DataCheckException("Il y a déjà un type de relance de niveau " + trlNiveau.intValue() + ". Vous ne pouvea pas en créer deux. Modifiez l'autre au besoin.");
	        }
        }


        return res;
    }


    private final void validerSaisie() {
        try {
            if (checkSaisie()) {
                getMyDialog().onOkClick();
            }
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
    }


    private final void initSaisie() throws Exception {
//        values = dico;

        System.out.println();
        System.out.println("TypeRelanceSaisieCtrl.initSaisie()");
        ZLogger.debug(values);
        typeRelanceSaisiePanel.updateData();
//        refreshSaveAction();
    }

    public final class ActionValider extends AbstractAction {

	    public ActionValider() {
            super("Enregistrer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
        }

        public void actionPerformed(ActionEvent e) {
            validerSaisie();

        }

	}

    public final class ActionAnnuler extends AbstractAction {

	    public ActionAnnuler() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
        }

        public void actionPerformed(ActionEvent e) {
          getMyDialog().onCancelClick();
        }

	}


    private final class TypeRelanceSaisiePanelListener implements TypeRelanceSaisiePanel.ITypeRelanceSaisiePanelListener {

        /**
         * @see org.cocktail.maracuja.client.typeRelances.ui.TypeRetenueSaisiePanel.ITypeRelanceSaisiePanelListener#getValues()
         */
        public HashMap getValues() {
            return values;
        }

        /**
         * @see org.cocktail.maracuja.client.typeRelances.ui.TypeRetenueSaisiePanel.ITypeRelanceSaisiePanelListener#actionClose()
         */
        public Action actionClose() {
            return actionAnnuler;
        }

        /**
         * @see org.cocktail.maracuja.client.typeRelances.ui.TypeRetenueSaisiePanel.ITypeRelanceSaisiePanelListener#actionValider()
         */
        public Action actionValider() {
            return actionValider;
        }

    }
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return typeRelanceSaisiePanel;
    }

    public String title() {
        return TITLE_NEW;
    }    

}
