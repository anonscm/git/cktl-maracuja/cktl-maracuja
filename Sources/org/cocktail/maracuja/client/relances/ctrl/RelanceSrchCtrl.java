/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.relances.ctrl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessRelance;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EORecetteInfo;
import org.cocktail.maracuja.client.metier.EORecetteRelance;
import org.cocktail.maracuja.client.metier.EORecetteResteRecouvrer;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeRelance;
import org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel;
import org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener;
import org.cocktail.maracuja.client.relances.ui.RelanceListSrchPanel.IRelanceListSrchPanelListener;
import org.cocktail.maracuja.client.relances.ui.RelanceSrchFilterPanel;
import org.cocktail.maracuja.client.relances.ui.RelanceSrchFilterPanel.IRelanceSrchFilterPanelListener;
import org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class RelanceSrchCtrl extends CommonCtrl {
	private static final String TITLE = "Gestion des relances";
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	private String delaiPaiementRecette = (String) myApp.getParametres().valueForKey("FACTURE_DELAI_PAIEMENT");

	private final NSTimestamp TODAY = new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.now()));
	private final Integer INTEGER_1 = new Integer(1);
	private final Integer INTEGER_2 = new Integer(2);
	private final Integer INTEGER_4 = new Integer(4);

	private RelanceSrchPanel relanceSrchPanel;
	private RelanceSrchPanelListener relanceSrchPanelListener;

	private RelanceRecetteListPanelListener relanceRecetteListPanelListener;
	private RecetteListPanelListener recetteListPanelListener;
	private RelanceSrchFilterPanelListener relanceSrchFilterPanelListener;

	private RelanceRecetteListSrchListener relanceRecetteListSrchListener;
	private RelanceListSrchPanelListener relanceListSrchPanelListener;

	private final ActionClose actionClose = new ActionClose();
	private final ActionWizard actionWizard = new ActionWizard();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionImprimerTout actionImprimerTout = new ActionImprimerTout();

	private final ActionRelanceImprimer actionRelanceImprimer = new ActionRelanceImprimer();
	private final ActionRelanceModifier actionRelanceModifier = new ActionRelanceModifier();
	private final ActionRelanceNew actionRelanceNew = new ActionRelanceNew();
	private final ActionRelanceNewAuto actionRelanceNewAuto = new ActionRelanceNewAuto();
	private final ActionRelanceSupprimer actionRelanceSupprimer = new ActionRelanceSupprimer();
	private final ActionRefreshRelances actionRefreshRelances = new ActionRefreshRelances();

	private final HashMap filters = new HashMap();

	private final NSArray typesRelances;

	private final RecetteInfoSaisieCtrl recetteInfoSaisieCtrl = new RecetteInfoSaisieCtrl();
	private final int nbMaxRelances;
	private final String nb_relances_str;

	private final RecetteTableCellRenderer recetteTableCellRenderer = new RecetteTableCellRenderer();

	//    private final HashMap resteEmargerCache = new HashMap();
	//    private final HashMap dateLimitePaiementRecetteCache = new HashMap();

	/**
	 * @param editingContext
	 */
	public RelanceSrchCtrl(EOEditingContext editingContext) {
		super(editingContext);

		if (delaiPaiementRecette == null) {
			delaiPaiementRecette = ZConst.DEFAULT_DELAI_RELANCE;
		}

		final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey("trlNiveau", EOSortOrdering.CompareAscending);
		typesRelances = EOsFinder.fetchArray(getEditingContext(), EOTypeRelance.ENTITY_NAME, "trlEtat=%@", new NSArray(new Object[] {
				EOTypeRelance.ETAT_VALIDE
		}), new NSArray(sortOrdering), true);
		nbMaxRelances = typesRelances.count();
		nb_relances_str = String.valueOf(nbMaxRelances);

	}

	private final void initSubObjects() {
		relanceRecetteListSrchListener = new RelanceRecetteListSrchListener();
		relanceListSrchPanelListener = new RelanceListSrchPanelListener();

		relanceSrchPanelListener = new RelanceSrchPanelListener();
		relanceRecetteListPanelListener = new RelanceRecetteListPanelListener();
		recetteListPanelListener = new RecetteListPanelListener();
		relanceSrchFilterPanelListener = new RelanceSrchFilterPanelListener();
		relanceSrchPanel = new RelanceSrchPanel(relanceSrchPanelListener);

		filters.put("ecdResteEmargerMin", new BigDecimal(0.01).setScale(2, BigDecimal.ROUND_HALF_UP));
		try {
			relanceSrchPanel.getRelanceSrchFilterPanel().updateData();
			refreshRecettesActions();
			refreshRelancesActions();
			relanceSrchPanel.getRecetteInfoSaisiePanel().updateData();
		} catch (Exception e) {
			//on ne fait rien, c pas grave
		}

	}

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		final ZKarukeraDialog win = (dial instanceof Dialog ? new ZKarukeraDialog((Dialog) dial, TITLE, true) : new ZKarukeraDialog((Frame) dial, TITLE, true));

		setMyDialog(win);
		initSubObjects();
		relanceSrchPanel.setPreferredSize(WINDOW_DIMENSION);
		relanceSrchPanel.initGUI();
		win.setContentPane(relanceSrchPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(final Window dial) {
		final ZKarukeraDialog win = createModalDialog(dial);
		try {
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final void refreshRecettesActions() {
		//final EORecette recette = (EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject();
		actionRelanceNew.setEnabled(relanceSrchPanel.getRecetteListPanel().selectedObjects().count() == 1);
		actionRelanceNewAuto.setEnabled(relanceSrchPanel.getRecetteListPanel().selectedObjects().count() > 0);
	}

	private final void refreshRelancesActions() {
		final EORecetteRelance recetteRelance = (EORecetteRelance) relanceSrchPanel.getRelanceRecetteListPanel().selectedObject();
		actionRelanceImprimer.setEnabled(recetteRelance != null);
		actionRelanceModifier.setEnabled(recetteRelance != null);
		actionRelanceSupprimer.setEnabled(recetteRelance != null);
	}

	private final class RelanceSrchPanelListener implements RelanceSrchPanel.IRelanceSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionWizard()
		 */
		public Action getActionWizard() {
			return actionWizard;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionClose()
		 */
		public Action getActionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#relanceRecetteListListener()
		 */
		public IZKarukeraTablePanelListener relanceRecetteListListener() {
			return relanceRecetteListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#recetteListListener()
		 */
		public IZKarukeraTablePanelListener recetteListListener() {
			return recetteListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#relanceSrchFilterPanelListener()
		 */
		public IRelanceSrchFilterPanelListener relanceSrchFilterPanelListener() {
			return relanceSrchFilterPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionImprimerTout()
		 */
		public Action getActionImprimerTout() {
			return actionImprimerTout;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionRelanceImprimer()
		 */
		public Action getActionRelanceImprimer() {
			return actionRelanceImprimer;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionRelanceModifier()
		 */
		public Action getActionRelanceModifier() {
			return actionRelanceModifier;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionRelanceNew()
		 */
		public Action getActionRelanceNew() {
			return actionRelanceNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getActionRelanceSupprimer()
		 */
		public Action getActionRelanceSupprimer() {
			return actionRelanceSupprimer;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#getFlagForRecette(org.cocktail.maracuja.client.metier.EORecette)
		 */
		public Object getFlagForRecette(final EORecette recette) {
			int res = 0;
			final EORecetteInfo recetteInfo = recette.recetteInfo();
			if (ZDateUtil.isBeforeEq(recette.dateProchaineRelance(), TODAY) && (recetteInfo == null || EORecetteInfo.REC_ETAT_RELANCE_POSSIBLE.equals(recetteInfo.recEtatRelance()))) {
				res = res + INTEGER_1.intValue();
			}

			if (recetteInfo != null) {
				if (EORecetteInfo.REC_ETAT_RELANCE_ANNULEE.equals(recetteInfo.recEtatRelance())) {
					res = res + INTEGER_2.intValue();
				}

				if (recetteInfo.recSuivi() != null) {
					res = res + INTEGER_4.intValue();
				}
			}
			return new Integer(res);
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchPanel.IRelanceSrchPanelListener#recetteInfoSaisiePanelListener()
		 */
		public IRecetteInfoSaisiePanelListener recetteInfoSaisiePanelListener() {
			return recetteInfoSaisieCtrl.getRecetteInfoSaisiePanelListener();
		}

		public IRelanceListSrchPanelListener relanceListSrchPanelListener() {
			return relanceListSrchPanelListener;
		}

		public Action getActionRelanceNewAuto() {
			return actionRelanceNewAuto;
		}

	}

	private final class RelanceRecetteListPanelListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			refreshRelancesActions();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public final NSArray getData() throws Exception {
			return getRecetteRelances();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			modifRelance();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	/**
	 * Listener de la vue des relances indépendantes des titres.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private final class RelanceListSrchPanelListener implements IRelanceListSrchPanelListener {

		public Action getActionRelanceImprimer() {
			return actionImprimerTout;
		}

		public IZKarukeraTablePanelListener relanceRecetteListListener() {
			return relanceRecetteListSrchListener;
		}

		public Action getActionRefreshRelances() {
			return actionRefreshRelances;
		}

	}

	/**
	 * Correspond au listener de la liste des relances indépendantes des titres.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private final class RelanceRecetteListSrchListener implements IZKarukeraTablePanelListener {

		protected final NSMutableArray buildFilterQualifiers(final HashMap dicoFiltre) throws Exception {
			final NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EORecetteRelance.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.TIT_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOTitre.titreVise));
			quals.addObject(new EOKeyValueQualifier(EORecetteRelance.RER_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EORecetteRelance.etatValide));

			if (dicoFiltre.get("client") != null) {
				final String s = "*" + (String) dicoFiltre.get("client") + "*";
				final NSMutableArray quals2 = new NSMutableArray();
				quals2.addObject(new EOKeyValueQualifier(EORecetteRelance.RECETTE_KEY + "." + EORecette.REC_DEBITEUR_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, s));
				quals2.addObject(new EOKeyValueQualifier(EORecetteRelance.RECETTE_KEY + "." + EORecette.FOURNISSEUR_KEY + "." + EOFournisseur.ADR_NOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, s));
				//quals2.addObject(new EOKeyValueQualifier(EORecetteRelance.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.FOURNISSEUR_KEY+"."+EOFournisseur.ADR_NOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, s));
				quals.addObject(new EOOrQualifier(quals2));
			}

			quals.addObject(ZEOUtilities.buildSimpleFilter(EORecetteRelance.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.TIT_LIBELLE_KEY, EOTitre.TIT_LIBELLE_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_CASEINSENSITIVELIKE, "*", "*"));
			quals.addObject(ZEOUtilities.buildSimpleFilter(EORecetteRelance.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.TIT_NUMERO_KEY, EOTitre.TIT_NUMERO_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
			quals.addObject(ZEOUtilities.buildSimpleFilter(EORecetteRelance.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.GESTION_KEY + "." + EOGestion.GES_CODE_KEY, EOGestion.GES_CODE_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
			quals.addObject(ZEOUtilities.buildSimpleFilter(EORecetteRelance.RECETTE_KEY + "." + EORecette.TITRE_KEY + "." + EOTitre.TITRE_DETAIL_ECRITURES_KEY + "." + EOTitreDetailEcriture.ECRITURE_DETAIL_KEY + "." + EOEcritureDetail.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY,
					EOPlanComptable.PCO_NUM_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
			quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecetteRelance.RECETTE_KEY + "." + EORecette.REC_MONT_TTC_KEY, "titMontTtcMin", "titMontTtcMax", dicoFiltre));
			quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecetteRelance.RER_DATE_CREATION_KEY, "rerDateMin", "rerDateMax", dicoFiltre));
			return quals;
		}

		public void selectionChanged() {
			//            ZLogger.debug(relanceSrchPanel.getRelanceListSrchPanel().getRelanceRecetteListPanel().selectedObject())

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return getRelances();
		}

		public void onDbClick() {

		}

		private final NSArray getRelances() throws Exception {
			final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EORecetteRelance.RER_DATE_CREATION_KEY, EOSortOrdering.CompareDescending);

			final NSMutableArray quals = new NSMutableArray();
			final NSMutableArray qualsReste = new NSMutableArray();
			if (filters.get("ecdResteEmargerMin") != null) {
				qualsReste.addObject(new EOKeyValueQualifier(EORecetteRelance.RECETTE_KEY + "." + EORecette.RECETTE_RESTE_RECOUVRER_KEY + "." + EORecetteResteRecouvrer.RESTE_RECOUVRER_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, filters.get("ecdResteEmargerMin")));

			}
			if (filters.get("ecdResteEmargerMax") != null) {
				qualsReste.addObject(new EOKeyValueQualifier(EORecetteRelance.RECETTE_KEY + "." + EORecette.RECETTE_RESTE_RECOUVRER_KEY + "." + EORecetteResteRecouvrer.RESTE_RECOUVRER_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, filters.get("ecdResteEmargerMax")));
			}
			if (qualsReste.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsReste));
			}

			NSArray res = ZFinder.fetchArray(getEditingContext(), EORecetteRelance.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), new NSArray(sort1), true);
			if (quals.count() > 0) {
				res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(quals));
			}
			return res;
		}

	}

	private final class RecetteListPanelListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			try {
				relanceSrchPanel.getRelanceRecetteListPanel().updateData();
				refreshRecettesActions();
				refreshRelancesActions();

				final EORecette recette = (EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject();
				recetteInfoSaisieCtrl.updateMap(recette);
				relanceSrchPanel.getRecetteInfoSaisiePanel().updateData();
				recetteInfoSaisieCtrl.setTouched(false);
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getRecettes();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return recetteTableCellRenderer;
		}

	}

	private final class RecetteTableCellRenderer extends ZEOTableCellRenderer {
		private final Image IMG_1_EXCLAMATION = ZIcon.getIconForName(ZIcon.ICON_EXCLAMATION_14).getImage();
		private final Image IMG_2_STOP_RELANCE = ZIcon.getIconForName(ZIcon.ICON_LOCKED_14).getImage();
		private final Image IMG_4_INFORMATION = ZIcon.getIconForName(ZIcon.ICON_INFORMATION_14).getImage();

		private final Image[] IMGS_1 = new Image[] {
				IMG_1_EXCLAMATION
		};
		private final Image[] IMGS_2 = new Image[] {
				IMG_2_STOP_RELANCE
		};
		private final Image[] IMGS_3 = new Image[] {
				IMG_1_EXCLAMATION, IMG_2_STOP_RELANCE
		};
		private final Image[] IMGS_4 = new Image[] {
				IMG_4_INFORMATION
		};
		private final Image[] IMGS_5 = new Image[] {
				IMG_1_EXCLAMATION, IMG_4_INFORMATION
		};
		private final Image[] IMGS_6 = new Image[] {
				IMG_2_STOP_RELANCE, IMG_4_INFORMATION
		};
		private final Image[] IMGS_7 = new Image[] {
				IMG_1_EXCLAMATION, IMG_2_STOP_RELANCE, IMG_4_INFORMATION
		};

		private final HashMap IMGS;
		private static final String nb_relances_sep = "/";

		/**
         *
         */
		public RecetteTableCellRenderer() {
			IMGS = new HashMap(8);
			IMGS.put(new Integer(0), new ZEOTable.ZImageCell(new Image[] {}));
			IMGS.put(new Integer(1), new ZEOTable.ZImageCell(IMGS_1));
			IMGS.put(new Integer(2), new ZEOTable.ZImageCell(IMGS_2));
			IMGS.put(new Integer(3), new ZEOTable.ZImageCell(IMGS_3));
			IMGS.put(new Integer(4), new ZEOTable.ZImageCell(IMGS_4));
			IMGS.put(new Integer(5), new ZEOTable.ZImageCell(IMGS_5));
			IMGS.put(new Integer(6), new ZEOTable.ZImageCell(IMGS_6));
			IMGS.put(new Integer(7), new ZEOTable.ZImageCell(IMGS_7));

		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object,
		 *      boolean, boolean, int, int)
		 */
		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			Component res = null;
			//            final ZEOTableModelColumn column2 = ((ZEOTable)table).getDataModel().getColumn( table.convertColumnIndexToModel(column)   );
			switch (table.convertColumnIndexToModel(column)) {
			case 0: //Flag
				res = (Component) IMGS.get(value);
				break;

			case 11: //Nb relances
				final int nbrel = ((Number) value).intValue();
				final String tmpValue = String.valueOf(nbrel).concat(nb_relances_sep).concat(nb_relances_str);

				res = super.getTableCellRendererComponent(table, tmpValue, isSelected, hasFocus, row, column);

				if (nbrel >= nbMaxRelances) {
					res.setBackground(Color.RED);
					res.setForeground(Color.WHITE);
				}
				else {
					if (isSelected) {
						res.setBackground(table.getSelectionBackground());
						res.setForeground(table.getSelectionForeground());
					}
					else {
						res.setBackground(table.getBackground());
						res.setForeground(table.getForeground());
					}
				}
				break;

			default:
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (isSelected) {
					res.setBackground(table.getSelectionBackground());
					res.setForeground(table.getSelectionForeground());
				}
				else {
					res.setBackground(table.getBackground());
					res.setForeground(table.getForeground());
				}
				break;
			}

			//
			//
			//            if (table.convertColumnIndexToModel(column)==0) {
			//
			//            }
			//            else {
			//                res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			//            }
			return res;
		}
	}

	private final class RelanceSrchFilterPanelListener implements RelanceSrchFilterPanel.IRelanceSrchFilterPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchFilterPanel.IRelanceSrchFilterPanelListener#getActionSrch()
		 */
		public Action getActionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchFilterPanel.IRelanceSrchFilterPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return filters;
		}

		/**
		 * @see org.cocktail.maracuja.client.relances.ui.RelanceSrchFilterPanel.IRelanceSrchFilterPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final void supprimeRelance() {
		try {
			final EORecetteRelance recetteRelance = (EORecetteRelance) relanceSrchPanel.getRelanceRecetteListPanel().selectedObject();
			if (recetteRelance != null) {
				if (showConfirmationDialog("Supression d'un relance", "Voulez-vous supprimer cette relance ? \nSi vous la supprimer, la prochaine lettre de relance que vous créerez sera du même niveau. Supprimez-la uniquement si vous ne l'avez pas envoyée.", ZMsgPanel.BTLABEL_NO)) {
					deleteRelance(recetteRelance);
					//                    ZLogger.debug(getEditingContext());
					try {
						getEditingContext().saveChanges();
						actionRefreshRelances.setEnabled(true);
					} catch (Exception e) {
						showErrorDialog(e);
						getEditingContext().revert();
					} finally {
						relanceSrchPanel.getRelanceRecetteListPanel().updateData();
						getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(((EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject()).recetteRelInfo())));
						relanceSrchPanel.getRecetteListPanel().fireTableRowUpdated(relanceSrchPanel.getRecetteListPanel().selectedRowIndex());
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void modifRelance() {
		try {
			final EORecetteRelance recetteRelance = (EORecetteRelance) relanceSrchPanel.getRelanceRecetteListPanel().selectedObject();
			if (recetteRelance != null) {
				final HashMap dico = createMapFromRelance(recetteRelance);
				final RelanceSaisieCtrl relanceSaisieCtrl = new RelanceSaisieCtrl(getEditingContext(), dico);
				if (relanceSaisieCtrl.openDialog(getMyDialog(), true) == ZKarukeraDialog.MROK) {
					updateRecetteRelanceFromMap(recetteRelance, dico);

					//                        ZLogger.debug(getEditingContext());
					try {
						getEditingContext().saveChanges();
					} catch (Exception e) {
						showErrorDialog(e);
						getEditingContext().revert();
					} finally {
						relanceSrchPanel.getRelanceRecetteListPanel().updateData();
						getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(((EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject()).recetteRelInfo())));
						relanceSrchPanel.getRecetteListPanel().fireTableRowUpdated(relanceSrchPanel.getRecetteListPanel().selectedRowIndex());
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void addRelance() {
		try {
			final EORecette recette = (EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject();
			final EORecetteInfo recetteInfo = recette.recetteInfo();
			if (recetteInfo != null && EORecetteInfo.REC_ETAT_RELANCE_ANNULEE.equals(recetteInfo.recEtatRelance())) {
				if (!showConfirmationDialog("Confirmation", "D'après les informations de suivi, vous ne devriez pas créer de nouvelle relance pour ce titre, souhaitez-vous néammoins en créer une ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}
			}

			//            if ( recette.getResteRecouvrerReadOnly().doubleValue()==0 ) {
			//fixme : recuperer le montant de la recette (reste à recouvrer)
			//fixme : changer vue recette_reste_recouvrer pour se baser sur type detail_ecriture (ctp)
			//			if (recette.titre().titreResteRecouvrerLight().resteRecouvrer().doubleValue() == 0) {
			if (recette.recetteResteRecouvrer().resteRecouvrer().doubleValue() == 0) {
				if (!showConfirmationDialog("Confirmation", "A priori la contrepartie de la recette a été émargé, souhaitez-vous néammoins créer une relance ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}
			}

			final HashMap dico = createRelanceMap(recette);
			if (dico != null) {
				final RelanceSaisieCtrl relanceSaisieCtrl = new RelanceSaisieCtrl(getEditingContext(), dico);
				if (relanceSaisieCtrl.openDialog(getMyDialog(), false) == ZKarukeraDialog.MROK) {
					final EORecetteRelance recetteRelance = createRecetteRelanceFromMap(dico);
					if (recetteRelance != null) {
						//                        ZLogger.debug(getEditingContext());
						try {
							getEditingContext().saveChanges();
							actionRefreshRelances.setEnabled(true);
						} catch (Exception e) {
							showErrorDialog(e);
							getEditingContext().revert();
						} finally {
							getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(((EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject()).recetteRelInfo())));
							relanceSrchPanel.getRecetteListPanel().fireTableRowUpdated(relanceSrchPanel.getRecetteListPanel().selectedRowIndex());
							relanceSrchPanel.getRelanceRecetteListPanel().updateData();
						}
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void addRelanceAuto() {
		try {
			NSArray recettes = relanceSrchPanel.getRecetteListPanel().selectedObjects();

			if (recettes.count() == 0) {
				throw new DefaultClientException("Veuillez sélectionner des recettes dans la liste pour lesquelles créer les relances.");
			}

			if (showConfirmationDialog("Création automatique des relances", "Souhaitez-vous réellement lancer la création automatique des relances pour les " + recettes.count()
					+ " recette que vous avez sélectionné ?<br>\nLes relances seront créées avec les options par défaut et seules les recettes dont la date de prochaine relance est atteinte ou dépassée seront créées.", ZMsgPanel.BTLABEL_NO)) {

				NSMutableArray relances = new NSMutableArray();

				for (int i = 0; i < recettes.count(); i++) {
					EORecette recette = (EORecette) recettes.objectAtIndex(i);
					EORecetteRelance recetteRelance = createRelanceAuto(recette);
					if (recetteRelance != null) {
						relances.addObject(recetteRelance);
					}
				}

				if (relances.count() == 0) {
					showInfoDialog("Aucune relance n'a été générée. Les dates de prochaine relance étaient peut-être postérieure à la date du jour.");
				}
				else if (relances.count() > 0) {
					try {
						getEditingContext().saveChanges();
						actionRefreshRelances.setEnabled(true);
					} catch (Exception e) {
						showErrorDialog(e);
						getEditingContext().revert();
						relances.clear();
					} finally {
						//mettre a jour tous les rows selectionnes
						NSMutableArray lesRecettesRelInfos = new NSMutableArray();
						for (int i = 0; i < recettes.count(); i++) {
							lesRecettesRelInfos.addObject(((EORecette) recettes.objectAtIndex(i)).recetteRelInfo());
						}
						getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), lesRecettesRelInfos.immutableClone()));

						for (int i = 0; i < relanceSrchPanel.getRecetteListPanel().getMyEOTable().getSelectedRows().length; i++) {
							relanceSrchPanel.getRecetteListPanel().fireTableRowUpdated(relanceSrchPanel.getRecetteListPanel().getMyEOTable().getSelectedRows()[i]);
						}
						relanceSrchPanel.getRelanceRecetteListPanel().updateData();
					}

					if (showConfirmationDialog("Relances créées", relances.count() + " relances ont été créées, souhaitez-vous les imprimer ?", ZMsgPanel.BTLABEL_YES)) {
						relancesImprimer(relances);
					}
				}
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	/**
	 * Cree une relance à partir des infos de la recette (sans passer par l'interface de creation).
	 * 
	 * @param recette
	 * @return
	 * @throws Exception
	 */
	private EORecetteRelance createRelanceAuto(EORecette recette) throws Exception {
		final EORecetteInfo recetteInfo = recette.recetteInfo();
		if (recetteInfo != null && EORecetteInfo.REC_ETAT_RELANCE_ANNULEE.equals(recetteInfo.recEtatRelance())) {
			return null;
		}

		if (recette.recetteResteRecouvrer().resteRecouvrer().doubleValue() == 0) {
			return null;
		}
		//		if (recette.titre().titreResteRecouvrerLight().resteRecouvrer().doubleValue() == 0) {
		//			return null;
		//		}
		if (ZDateUtil.nowAsDate().before(recette.dateProchaineRelance())) {
			return null;
		}

		final HashMap dico = createRelanceMap(recette);
		if (dico != null) {
			return createRecetteRelanceFromMap(dico);
		}
		return null;
	}

	private final void updateRecetteRelanceFromMap(final EORecetteRelance recetteRelance, final Map map) throws DefaultClientException {
		final FactoryProcessRelance factoryProcessRelance = new FactoryProcessRelance(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

		factoryProcessRelance.setRerContact(getEditingContext(), recetteRelance, (String) map.get("rerContact"));
		factoryProcessRelance.setRerDateCreation(getEditingContext(), recetteRelance, (NSTimestamp) map.get("rerDateCreation"));
		factoryProcessRelance.setRerDateImpression(getEditingContext(), recetteRelance, (NSTimestamp) map.get("rerDateImpression"));
		factoryProcessRelance.setRerLibelle(getEditingContext(), recetteRelance, (String) map.get("rerLibelle"));
		factoryProcessRelance.setRerMont(getEditingContext(), recetteRelance, (BigDecimal) map.get("rerMont"));
		factoryProcessRelance.setRerNumero(getEditingContext(), recetteRelance, (Integer) map.get("rerNumero"));
		factoryProcessRelance.setRerPs(getEditingContext(), recetteRelance, (String) map.get("rerPs"));
		factoryProcessRelance.setRerDelaiPaiement(getEditingContext(), recetteRelance, (Integer) map.get("rerDelaiPaiement"));
	}

	private final EORecetteRelance createRecetteRelanceFromMap(final Map map) throws DefaultClientException {
		final FactoryProcessRelance factoryProcessRelance = new FactoryProcessRelance(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		final EORecetteRelance recetteRelance2 = factoryProcessRelance.creerUneRelance(getEditingContext(), (EOTypeRelance) map.get("typeRelance"), getUtilisateur(), getExercice(), (EORecette) map.get("recette"), (String) map.get("rerContact"), (NSTimestamp) map.get("rerDateCreation"),
				(NSTimestamp) map.get("rerDateImpression"), (String) map.get("rerLibelle"), (BigDecimal) map.get("rerMont"), (Integer) map.get("rerNumero"), (String) map.get("rerPs"), (Integer) map.get("rerDelaiPaiement"));

		return recetteRelance2;
	}

	private final HashMap createMapFromRelance(final EORecetteRelance relance) {
		final HashMap map = new HashMap();

		map.put("typeRelance", relance.typeRelance());
		map.put("recette", relance.recette());
		map.put("rerMont", relance.rerMont());
		//        map.put("recResteRecouvrer", relance.recette().getResteRecouvrerReadOnly());
		//		map.put("recResteRecouvrer", relance.recette().titre().titreResteRecouvrerLight().resteRecouvrer());
		map.put("recResteRecouvrer", relance.recette().recetteResteRecouvrer().resteRecouvrer());
		map.put("rerDateCreation", new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.now())));
		map.put("rerDateImpression", null);
		map.put("rerPs", relance.rerPs());
		map.put("rerLibelle", relance.rerLibelle());
		map.put("rerContact", relance.rerContact());
		map.put("rerNumero", relance.rerNumero());
		map.put("rerDelaiPaiement", relance.rerDelaiPaiement());

		return map;
	}

	private final void deleteRelance(final EORecetteRelance recetteRelance) throws DefaultClientException {
		final FactoryProcessRelance factoryProcessRelance = new FactoryProcessRelance(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		factoryProcessRelance.supprimerUneRelance(getEditingContext(), recetteRelance);
	}

	private final HashMap createRelanceMap(final EORecette recette) throws Exception {
		if (recette == null) {
			throw new DefaultClientException("Recette nulle");
		}

		EORecetteRelance recetteRelance = null;
		final EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("typeRelance.trlNiveau", EOSortOrdering.CompareDescending);
		final NSArray recetteRelanceFinal = getRecetteRelancesValideForRecette(recette, new NSArray(sort));
		if (recetteRelanceFinal.count() > 0) {
			recetteRelance = (EORecetteRelance) recetteRelanceFinal.objectAtIndex(0);
		}

		//Recuperer le dernier niveau de relance
		Integer dernierNiveau = new Integer(0);
		//        Date dernierDate = recette.recDate();
		Date prochainDate = calDateLimitePaiementForRecette(recette);

		//      verifier si la date est atteinte
		if (recetteRelance != null) {
			dernierNiveau = recetteRelance.typeRelance().trlNiveau();
			//            dernierDate = recetteRelance.rerDateCreation();
			prochainDate = recetteRelance.dateLimitePaiement();
		}

		if (ZDateUtil.nowAsDate().compareTo(prochainDate) < 0) {
			//            throw new DefaultClientException("La date de la prochaine relance ("+ ZConst.FORMAT_DATESHORT.format(prochainDate)+") n'est pas atteinte pour le titre n°"+recette.titre().titNumero()+" de la composante " + recette.titre().gestion().gesCode());
			if (!showConfirmationDialog("Confirmation", "La date de la prochaine relance (" + ZConst.FORMAT_DATESHORT.format(prochainDate) + ") n'est pas atteinte pour le titre n°" + recette.titre().titNumero() + " de la composante " + recette.titre().gestion().gesCode()
					+ ".\nSouhaitez-vous néanmoins créer une nouvelle relance ?", ZMsgPanel.BTLABEL_NO)) {
				return null;
			}
		}

		//recuperer les prochains types classes
		final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOTypeRelance.TRL_NIVEAU_KEY, EOSortOrdering.CompareAscending);
		final NSArray nextTypeRelances = EOsFinder.fetchArray(getEditingContext(), EOTypeRelance.ENTITY_NAME, "trlNiveau>%@ and trlEtat=%@", new NSArray(new Object[] {
				dernierNiveau, EOTypeRelance.ETAT_VALIDE
		}), new NSArray(sortOrdering), false);

		if (nextTypeRelances == null || nextTypeRelances.count() == 0) {
			ZLogger.debug("Pas de relance à créer pour le titre " + recette.titre().titNumero());
			throw new DefaultClientException("Pas de type relance disponible pour le niveau > " + dernierNiveau);
		}
		final EOTypeRelance typeRelance = (EOTypeRelance) nextTypeRelances.objectAtIndex(0);

		//Créer une relance

		//récupérer le contact depuis les parametres
		final String contact = typeRelance.trlContactSuivi();
		final String libelle = typeRelance.trlNom();
		//        final BigDecimal montant = recette.getResteRecouvrerReadOnly();
		final BigDecimal montant = recette.recetteResteRecouvrer().resteRecouvrer();
		//fixme : recuperer le montant de la recette (reste à recouvrer)
		final Integer numero = new Integer(0);
		final String postscriptum = "";

		final HashMap recetteRelanceDico = new HashMap();
		recetteRelanceDico.put("typeRelance", typeRelance);
		recetteRelanceDico.put("recette", recette);
		recetteRelanceDico.put("rerMont", montant);
		recetteRelanceDico.put("recResteRecouvrer", montant);
		recetteRelanceDico.put("rerDateCreation", new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.now())));
		recetteRelanceDico.put("rerDateImpression", null);
		recetteRelanceDico.put("rerPs", postscriptum);
		recetteRelanceDico.put("rerLibelle", libelle);
		recetteRelanceDico.put("rerContact", contact);
		recetteRelanceDico.put("rerNumero", numero);
		recetteRelanceDico.put("rerDelaiPaiement", typeRelance.trlDelaiPaiement());

		//        final EORecetteRelance recetteRelance2 = factoryProcessRelance.creerUneRelance(getEditingContext(), typeRelance, getUtilisateur(), getExercice(),recette, contact, new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.now())), null,libelle,montant,numero,postscriptum );

		return recetteRelanceDico;
	}

	/**
	 * Imprime les relances sélectionnées par niveau
	 */
	private final void onRelancesImprimer() {
		final NSArray relances = relanceSrchPanel.getRelanceListSrchPanel().getRelanceRecetteListPanel().getSelectedObjects();
		relancesImprimer(relances);
	}

	private final void relancesImprimer(NSArray relances) {
		try {
			setWaitCursor(true);
			//final NSArray relances = relanceSrchPanel.getRelanceListSrchPanel().getRelanceRecetteListPanel().getSelectedObjects();

			//Faire un tri par niveau 
			final HashMap map = new HashMap(typesRelances.count());
			//            ZLogger.debug(map);

			for (int i = 0; i < typesRelances.count(); i++) {
				final EOTypeRelance element = (EOTypeRelance) typesRelances.objectAtIndex(i);
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("typeRelance=%@", new NSArray(new Object[] {
						element
				}));
				final NSArray array = EOQualifier.filteredArrayWithQualifier(relances, qual);
				if (array.count() > 0) {
					map.put(element, array);
				}
			}

			Iterator iter = map.keySet().iterator();
			while (iter.hasNext()) {
				final EOTypeRelance leType = (EOTypeRelance) iter.next();
				final NSArray rels = (NSArray) map.get(leType);
				final String filePath = ReportFactoryClient.imprimerRelances(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), rels, getMyDialog(), "relances_" + leType.trlNiveau().intValue() + ".pdf", leType.trlReportId());

				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
				if (showConfirmationDialog("Confirmation", "Voulez-vous mettre à jour la date d'impression pour les " + rels.count() + " relances de type " + leType.trlNiveau().intValue() + " (" + leType.trlNom() + ")"
						+ " que vous venez d'imprimer ? \nSi vous répondez oui, la date d'impression des relances que vous venez d'imprimer sera mise à aujourd'hui.", ZMsgPanel.BTLABEL_YES)) {
					final NSTimestamp tday = new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.nowAsDate()));
					for (int i = 0; i < rels.count(); i++) {
						final EORecetteRelance element = (EORecetteRelance) rels.objectAtIndex(i);
						element.setRerDateImpression(tday);
					}
					getEditingContext().saveChanges();

				}
			}

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}

	}

	private final void onRelanceImprimer() {
		try {
			setWaitCursor(true);
			//            final NSMutableArray valides = new NSMutableArray();
			final EORecetteRelance recetteRelance = (EORecetteRelance) relanceSrchPanel.getRelanceRecetteListPanel().selectedObject();
			final String filePath = ReportFactoryClient.imprimerRelances(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(recetteRelance), getMyDialog(), "relance.pdf", recetteRelance.typeRelance().trlReportId());

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}

			if (showConfirmationDialog("Confirmation", "Voulez-vous mettre à jour la date d'impression ? \nSi vous répondez oui, la date d'impression de la relance que " + "vous venez d'imprimer sera mise à aujourd'hui.", ZMsgPanel.BTLABEL_YES)) {
				final NSTimestamp tday = new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.nowAsDate()));
				recetteRelance.setRerDateImpression(tday);
				getEditingContext().saveChanges();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void onSrch() {
		try {
			setWaitCursor(true);
			relanceSrchPanel.getRecetteListPanel().updateData();
			relanceSrchPanel.getRelanceListSrchPanel().updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	/**
	 * Renvoie les relances valides pour la recette spécifiée.
	 * 
	 * @param recette
	 * @param sorts Tableau d'instances de EOSortOrderings. Si null, le résultat est trié par niveau , croissant.
	 * @return
	 */
	private final NSArray getRecetteRelancesValideForRecette(final EORecette recette, final NSArray sorts) {
		if (recette == null) {
			return NSARRAYVIDE;
		}
		final EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("typeRelance.trlNiveau", EOSortOrdering.CompareAscending);

		NSArray recetteRelanceFinal = recette.recetteRelancesValides();
		final NSArray recetteRelances = recette.recetteRelances();
		if (recetteRelances != null) {
			recetteRelanceFinal = EOSortOrdering.sortedArrayUsingKeyOrderArray(recetteRelanceFinal, (sorts == null ? new NSArray(sort) : sorts));
		}
		return recetteRelanceFinal;
	}

	/**
	 * @return Les relances valides associées à la recette selectionnee. Si plusieurs recettes selectionnees, renvoie un tableau vide.
	 */
	private final NSArray getRecetteRelances() {
		if (relanceSrchPanel.getRecetteListPanel().selectedObjects().count() > 1) {
			return NSArray.EmptyArray;
		}
		final EORecette recette = (EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject();
		return getRecetteRelancesValideForRecette(recette, null);
	}

	private final NSArray getRecettes() throws Exception {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("recDateLimitePaiement", EOSortOrdering.CompareAscending);

		//final NSMutableArray quals = new NSMutableArray();

		//		NSArray res = ZFinder.fetchArray(getEditingContext(), EORecette.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), new NSArray(sort1), true);
		NSArray res = ZFinder.fetchArray(getEditingContext(), EORecette.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), new NSArray(sort1), true, true, false, null);

		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("dateProchaineRelance", EOSortOrdering.CompareAscending);
		res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(sort2));

		//Invalidate sur les restes a recouvrer
		final NSMutableArray toInval = new NSMutableArray();

		for (int i = 0; i < res.count(); i++) {
			final EORecette element = (EORecette) res.objectAtIndex(i);
			toInval.addObject(element.recetteResteRecouvrer());
		}
		getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), toInval));

		return res;
	}

	/**
	 * @param element
	 * @return
	 */
	private final Date calDateLimitePaiementForRecette(final EORecette recette) {
		Date prochainDate = recette.recDateLimitePaiement();
		if (prochainDate == null) {
			prochainDate = ZDateUtil.addDHMS(recette.titre().titDateRemise(), new Integer(delaiPaiementRecette).intValue(), 0, 0, 0);
		}
		return prochainDate;
	}

	/**
	 * Creer le filtre pour la recherche des recettes.
	 * 
	 * @param dicoFiltre
	 * @return
	 * @throws Exception
	 */
	protected final NSMutableArray buildFilterQualifiers(final HashMap dicoFiltre) throws Exception {
		final NSMutableArray quals = new NSMutableArray();
		quals.addObject(EORecette.QUAL_NON_SUPPRIME);
		quals.addObject(EORecette.QUAL_VISE);
		quals.addObject(EORecette.QUAL_NON_INTERNE);
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("titreDetailEcritures.ecritureDetail.exercice=%@", new NSArray(new Object[] {
				getExercice()
		})));

		if (dicoFiltre.get("client") != null) {
			final String s = "*" + (String) dicoFiltre.get("client") + "*";
			final NSMutableArray quals2 = new NSMutableArray();
			quals2.addObject(new EOKeyValueQualifier(EORecette.REC_DEBITEUR_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, s));
			quals2.addObject(new EOKeyValueQualifier(EORecette.FOURNISSEUR_KEY + "." + EOFournisseur.ADR_NOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, s));
			quals.addObject(new EOOrQualifier(quals2));
		}

		quals.addObject(ZEOUtilities.buildSimpleFilter(EORecette.REC_LIBELLE_KEY, EORecette.REC_LIBELLE_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_CASEINSENSITIVELIKE, "*", "*"));
		quals.addObject(ZEOUtilities.buildSimpleFilter(EORecette.TITRE_KEY + "." + EOTitre.TIT_NUMERO_KEY, EOTitre.TIT_NUMERO_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
		quals.addObject(ZEOUtilities.buildSimpleFilter(EORecette.TITRE_KEY + "." + EOTitre.GESTION_KEY + "." + EOGestion.GES_CODE_KEY, EOGestion.GES_CODE_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
		quals.addObject(ZEOUtilities.buildSimpleFilter(EORecette.TITRE_KEY + "." + EOTitre.TITRE_DETAIL_ECRITURES_KEY + "." + EOTitreDetailEcriture.ECRITURE_DETAIL_KEY + "." + EOEcritureDetail.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY,
				EOPlanComptable.PCO_NUM_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
		quals.addObject(new EOKeyValueQualifier(EORecette.TITRE_KEY + "." + EOTitre.TITRE_DETAIL_ECRITURES_KEY + "." + EOTitreDetailEcriture.ECRITURE_DETAIL_KEY + "." + EOEcritureDetail.ECD_DEBIT_KEY, EOQualifier.QualifierOperatorGreaterThan, BigDecimal.valueOf(0d)));
		quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecette.REC_MONT_TTC_KEY, "titMontTtcMin", "titMontTtcMax", dicoFiltre));
		quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecette.RECETTE_RELANCES_KEY + "." + EORecetteRelance.RER_DATE_CREATION_KEY, "rerDateMin", "rerDateMax", dicoFiltre));
		quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecette.RECETTE_RESTE_RECOUVRER_KEY + "." + EORecetteResteRecouvrer.RESTE_RECOUVRER_KEY, "ecdResteEmargerMin", "ecdResteEmargerMax", dicoFiltre));

		//		
		//		quals.addObject(ZEOUtilities.buildSimpleFilter(EORecette.REC_LIBELLE_KEY, EORecette.REC_LIBELLE_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_CASEINSENSITIVELIKE, "*", "*"));
		//		quals.addObject(ZEOUtilities.buildSimpleFilter("titre.titNumero", "titNumero", dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
		//		quals.addObject(ZEOUtilities.buildSimpleFilter("titre.gestion.gesCode", EOGestion.GES_CODE_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
		//		quals.addObject(ZEOUtilities.buildSimpleFilter("titreDetailEcritures.ecritureDetail.pcoNum", EOPlanComptable.PCO_NUM_KEY, dicoFiltre, ZEOUtilities.OPERATEUR_EGAL));
		//
		//		//        quals.addObject(ZEOUtilities.buildFourchetteFilter("titre.titreResteRecouvrerLight.resteRecouvrer", "ecdResteEmargerMin", "ecdResteEmargerMax", dicoFiltre) );
		//		quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecette.RECETTE_RESTE_RECOUVRER_KEY + "." + EORecetteResteRecouvrer.RESTE_RECOUVRER_KEY, "ecdResteEmargerMin", "ecdResteEmargerMax", dicoFiltre));
		//		//        quals.addObject(ZEOUtilities.buildFourchetteFilter("titreDetailEcritures.ecritureDetail.ecdResteEmarger", "ecdResteEmargerMin", "ecdResteEmargerMax", dicoFiltre) );
		//
		//		quals.addObject(ZEOUtilities.buildFourchetteFilter(EORecette.REC_MONT_TTC_KEY, "titMontTtcMin", "titMontTtcMax", dicoFiltre));
		//		quals.addObject(ZEOUtilities.buildFourchetteFilter("recetteRelances.rerDateCreation", "rerDateMin", "rerDateMax", dicoFiltre));

		//        ZLogger.debug(quals);

		return quals;
	}

	private final class ActionWizard extends AbstractAction {

		public ActionWizard() {
			super("Assistant");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_WIZARD_16));
		}

		public void actionPerformed(final ActionEvent e) {
		}

	}

	private final class ActionRelanceNew extends AbstractAction {

		public ActionRelanceNew() {
			super("Nouveau");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer la prochaine relance");

		}

		public void actionPerformed(final ActionEvent e) {
			addRelance();
		}

	}

	private final class ActionRelanceNewAuto extends AbstractAction {

		public ActionRelanceNewAuto() {
			super("Créer (auto)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_WIZARD_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer les prochaines relances pour les recettes sélectionnées.");
		}

		public void actionPerformed(final ActionEvent e) {
			addRelanceAuto();
		}

	}

	private final class ActionRelanceSupprimer extends AbstractAction {

		public ActionRelanceSupprimer() {
			super("Supprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer la relance sélectionnée");

		}

		public void actionPerformed(final ActionEvent e) {
			supprimeRelance();
		}

	}

	private final class ActionRefreshRelances extends AbstractAction {

		public ActionRefreshRelances() {
			super("Mettre à jour la liste");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Mettre à jour la liste des relances (si vous avez créé/supprimé des relances)");
		}

		public void actionPerformed(final ActionEvent e) {
			try {
				relanceSrchPanel.getRelanceListSrchPanel().updateData();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class ActionRelanceImprimer extends AbstractAction {

		public ActionRelanceImprimer() {
			super("Imprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer la relance sélectionnée");

		}

		public void actionPerformed(final ActionEvent e) {
			onRelanceImprimer();
		}

	}

	private final class ActionRelanceModifier extends AbstractAction {

		public ActionRelanceModifier() {
			super("Modifier");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier la relance sélectionnée");

		}

		public void actionPerformed(final ActionEvent e) {
			modifRelance();
		}

	}

	private final class ActionImprimerTout extends AbstractAction {

		public ActionImprimerTout() {
			super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer la/les relance(s) sélectionnées");
		}

		public void actionPerformed(final ActionEvent e) {
			onRelancesImprimer();
		}

	}

	private final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(final ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(final ActionEvent e) {
			onSrch();
		}
	}

	private final class RecetteInfoSaisieCtrl {
		private final HashMap values = new HashMap();
		private final EtatRelanceComboBoxModel etatRelanceComboBoxModel = new EtatRelanceComboBoxModel();
		private final ActionAnnuler actionAnnuler = new ActionAnnuler();
		private final ActionEnregistrer actionEnregistrer = new ActionEnregistrer();
		private final RecetteInfoSaisiePanelListener recetteInfoSaisiePanelListener = new RecetteInfoSaisiePanelListener();

		private boolean touched = false;

		public final void updateMap(final EORecette recette) {
			values.clear();
			if (recette != null) {
				if (recette.recetteInfo() == null) {
					values.putAll(createRecetteInfoMap(recette));
				}
				else {
					values.putAll(createMapFromEO(recette.recetteInfo()));
				}
			}
		}

		/**
		 * @param b
		 */
		public void setTouched(final boolean b) {
			touched = b;
			refreshActions();
		}

		public void cleanValues() {
			values.clear();
		}

		private final HashMap createMapFromEO(final EORecetteInfo eo) {
			//            System.out.println("RecetteInfoSaisieCtrl.createMapFromEO()");
			final HashMap map = new HashMap();

			map.put("recSuivi", eo.recSuivi());
			map.put("recEtatRelance", etatRelanceComboBoxModel.getPlancoRelanceList().get(eo.recEtatRelance()));
			map.put("recette", eo.recette());
			return map;
		}

		private final HashMap createRecetteInfoMap(final EORecette recette) {
			//            System.out.println("RecetteInfoSaisieCtrl.createRecetteInfoMap()");
			final HashMap map = new HashMap();

			map.put("recSuivi", null);
			map.put("recEtatRelance", etatRelanceComboBoxModel.getPlancoRelanceList().get(EORecetteInfo.REC_ETAT_RELANCE_POSSIBLE));
			map.put("recette", recette);
			return map;
		}

		private final EORecetteInfo createEOFromMap(final HashMap map) {
			final FactoryRecetteInfo factoryRecetteInfo = new FactoryRecetteInfo(myApp.wantShowTrace());
			final EORecetteInfo recetteInfo = factoryRecetteInfo.creerRecetteInfo(getEditingContext(), (EORecette) map.get("recette"), (String) map.get("recSuivi"), (String) etatRelanceComboBoxModel.getSelectedKey());
			return recetteInfo;
		}

		private final void updateRecetteInfoFromMap(final EORecetteInfo recetteInfo, final HashMap map) {
			recetteInfo.setRecEtatRelance((String) etatRelanceComboBoxModel.getSelectedKey());
			recetteInfo.setRecSuivi((String) map.get("recSuivi"));
			//La recette doit deja être affectee
		}

		public HashMap getValues() {
			return values;
		}

		public EtatRelanceComboBoxModel getEtatRelanceComboBoxModel() {
			return etatRelanceComboBoxModel;
		}

		private final void refreshActions() {
			actionEnregistrer.setEnabled(touched);
			actionAnnuler.setEnabled(touched);
		}

		private final void enregistrerRecetteInfo() {
			try {
				final EORecette recette = (EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject();
				//                final EORecetteRelInfo recetteRelInfo = (EORecetteRelInfo)relanceSrchPanel.getRecetteListPanel().selectedObject();
				//                final EORecette recette = (recetteRelInfo==null?null:recetteRelInfo.recette());

				//                System.out.println("----> values ");
				//                ZLogger.debug(values);

				if (recette != null) {

					if (recette.recetteInfo() == null || recette.recetteInfo().recId() == null) {
						createEOFromMap(values);
					}
					else {
						updateRecetteInfoFromMap(recette.recetteInfo(), values);
					}
					//                    ZLogger.debug(getEditingContext());
					try {
						getEditingContext().saveChanges();
					} catch (Exception e) {
						showErrorDialog(e);
						getEditingContext().revert();
					} finally {
						updateMap(recette);
						relanceSrchPanel.getRecetteListPanel().fireTableRowUpdated(relanceSrchPanel.getRecetteListPanel().selectedRowIndex());
						relanceSrchPanel.getRecetteInfoSaisiePanel().updateData();
						setTouched(false);
					}
				}
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

		private final void annulerRecetteInfo() {
			final EORecette recette = (EORecette) relanceSrchPanel.getRecetteListPanel().selectedObject();
			//            final EORecetteRelInfo recetteRelInfo = (EORecetteRelInfo)relanceSrchPanel.getRecetteListPanel().selectedObject();
			//            final EORecette recette = (recetteRelInfo==null?null:recetteRelInfo.recette());

			updateMap(recette);
			try {
				relanceSrchPanel.getRecetteInfoSaisiePanel().updateData();
				setTouched(false);
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		private final class FactoryRecetteInfo extends Factory {
			/**
             *
             */
			public FactoryRecetteInfo(final boolean withLog) {
				super(withLog);
			}

			public final EORecetteInfo creerRecetteInfo(final EOEditingContext ed, final EORecette recette, final String recSuivi, final String recEtatRelance) {
				if (recette == null) {
					throw new FactoryException("Recette nulle");
				}
				final EORecetteInfo recetteInfo = (EORecetteInfo) Factory.instanceForEntity(ed, EORecetteInfo.ENTITY_NAME);
				ed.insertObject(recetteInfo);
				final EOKeyGlobalID gid = (EOKeyGlobalID) ed.globalIDForObject(recette);
				recetteInfo.setRecId((Integer) gid.keyValuesArray().objectAtIndex(0));
				recetteInfo.setRecEtatRelance(recEtatRelance);
				recetteInfo.setRecSuivi(recSuivi);

				//On nettoie les anciens (normalement on n'en a pas)
				while (recette.recetteInfos().count() > 0) {
					recette.removeObjectFromBothSidesOfRelationshipWithKey((EORecetteInfo) recette.recetteInfos().objectAtIndex(0), "recetteInfos");
				}

				recette.addToRecetteInfosRelationship(recetteInfo);
				//                recette.addObjectToBothSidesOfRelationshipWithKey(recetteInfo, "recetteInfos");

				return recetteInfo;
			}
		}

		private final class EtatRelanceComboBoxModel extends DefaultComboBoxModel {
			private final LinkedHashMap etatRelanceList = new LinkedHashMap();
			{
				etatRelanceList.put(EORecetteInfo.REC_ETAT_RELANCE_POSSIBLE, "Autoriser la création de nouvelles relances sur ce titre");
				etatRelanceList.put(EORecetteInfo.REC_ETAT_RELANCE_ANNULEE, "Ne plus créer de nouvelles relances sur ce titre");
			}

			/**
             *
             */
			public EtatRelanceComboBoxModel() {
				super();
				updateList();
			}

			private final void updateList() {
				final Iterator iter = etatRelanceList.values().iterator();
				while (iter.hasNext()) {
					addElement(iter.next());
				}
			}

			public final Object getSelectedKey() {
				final int ind = getIndexOf(getSelectedItem());
				return etatRelanceList.keySet().toArray()[ind];
			}

			public final LinkedHashMap getPlancoRelanceList() {
				return etatRelanceList;
			}
		}

		private final class ActionEnregistrer extends AbstractAction {

			public ActionEnregistrer() {
				super("Enregistrer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			}

			public void actionPerformed(final ActionEvent e) {
				enregistrerRecetteInfo();
			}

		}

		private final class ActionAnnuler extends AbstractAction {

			public ActionAnnuler() {
				super("Annuler");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
			}

			public void actionPerformed(final ActionEvent e) {
				annulerRecetteInfo();
			}

		}

		private final class RecetteInfoSaisiePanelListener implements RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener {

			/**
			 * @see org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener#getValues()
			 */
			public HashMap getValues() {
				return values;
			}

			/**
			 * @see org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener#getActionAnnuler()
			 */
			public Action getActionAnnuler() {
				return actionAnnuler;
			}

			/**
			 * @see org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener#getActionEnregistrer()
			 */
			public Action getActionEnregistrer() {
				return actionEnregistrer;
			}

			/**
			 * @see org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener#getRecEtatRelanceComboBoxModel()
			 */
			public ComboBoxModel getRecEtatRelanceComboBoxModel() {
				return etatRelanceComboBoxModel;
			}

			/**
			 * @see org.cocktail.maracuja.client.relances.ui.RecetteInfoSaisiePanel.IRecetteInfoSaisiePanelListener#onModified()
			 */
			public void setModified(final boolean b) {
				setTouched(b);
			}

		}

		public RecetteInfoSaisiePanelListener getRecetteInfoSaisiePanelListener() {
			return recetteInfoSaisiePanelListener;
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return relanceSrchPanel;
	}

	public String title() {
		return TITLE;
	}

}
