/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TitreListePanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener, TableModelListener {
	private static final String MOTIFREJETTITLE = "Motif de rejet";

	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;
	private TableSorter myTableSorter;
	private ITitreListePanelListener myListener;

	private Vector myCols;

	private JPopupMenu myPopupMenu;

	public TitreListePanel(ITitreListePanelListener vListener, EOEditingContext ec) {
		super();
		setEditingContext(ec);
		this.myListener = vListener;
		myDisplayGroup = new EODisplayGroup();
		myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), EOTitre.ENTITY_NAME));
		((EODistributedDataSource) myDisplayGroup.dataSource()).setEditingContext(getEditingContext());
	}

	/**
	 * Pour l'instant pas de popup de cohe/decoche, sinon pb avec synchro des coches recettes
	 */
	private void initPopupMenu() {
		myPopupMenu = new JPopupMenu();
		myPopupMenu.add(new ActionCheckAll());
		myPopupMenu.add(new ActionUncheckAll());
		myEOTable.setPopup(myPopupMenu);
	}

	private class ActionCheckAll extends AbstractAction {
		public ActionCheckAll() {
			super("Tout cocher");
		}

		public void actionPerformed(ActionEvent e) {
			((ZEOTableModelColumnDico) myCols.get(0)).setValueForAllRows(Boolean.valueOf(true));
			myTableModel.fireTableDataChanged();
		}
	}

	private class ActionUncheckAll extends AbstractAction {
		public ActionUncheckAll() {
			super("Tout décocher");
		}

		public void actionPerformed(ActionEvent e) {
			((ZEOTableModelColumnDico) myCols.get(0)).setValueForAllRows(Boolean.valueOf(false));
			myTableModel.fireTableDataChanged();
		}
	}

	//	/**
	//	 * Coche/decoche toutes les lignes
	//	 * @param checked
	//	 */
	//	private void checkAll(boolean checked) {
	//		NSArray res = myDisplayGroup.displayedObjects();
	//		for (int i = 0; i < res.count(); i++) {
	//			EOTitre array_element = (EOTitre) res.objectAtIndex(i);
	//			myListener.getAllCheckedMAndats().put(array_element, Boolean.valueOf(checked) );
	//			myListener.onTitreCheckedChanged(i,0,array_element);
	//		}
	//	}
	//

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);

		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		//		initPopupMenu();
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private void initTableModel() {
		myCols = new Vector(9, 1);

		ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDisplayGroup, "Viser", 60, myListener.getAllCheckedMAndats());
		col0.setEditable(true);
		col0.setResizable(false);
		col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_NUMERO_KEY, "N°", 64);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);

		ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.PLAN_COMPTABLE_KEY + ZConst.QUAL_POINT + EOPlanComptable.PCO_NUM_KEY, "Imp.", 88);
		col11.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.FOURNISSEUR_KEY + ZConst.QUAL_POINT + EOFournisseur.ADR_NOM_KEY, "Débiteur", 173);
		ZEOTableModelColumn col31 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_NB_PIECE_KEY, "Pièces", 48);
		col31.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_HT_KEY, "Montant HT", 80);
		col32.setAlignment(SwingConstants.RIGHT);
		col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col32.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_TVA_KEY, "Montant TVA", 80);
		col3.setAlignment(SwingConstants.RIGHT);
		col3.setColumnClass(BigDecimal.class);

		col3.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_TTC_KEY, "Montant TTC", 80);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);

		//		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup,"manDateRemise","Date remise",100);
		//		col5.setFormatDisplay(ZConst.FORMAT_DATESHORT ) ;
		//		ZEOTableModelColumn col6 = new ZEOTableModelColumnDico(myDisplayGroup,MOTIFREJETTITLE,100, myListener.dicoOfMotifsRejetsTitres());
		//		col6.setEditable(true);
		ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_MOTIF_REJET_KEY, MOTIFREJETTITLE, 300);
		col6.setEditable(true);

		ZEOTableModelColumn colTitLibelle = new ZEOTableModelColumn(myDisplayGroup, EOTitre.TIT_LIBELLE_KEY, "Libellé", 173);

		//On crée la collection des colonnes, ca défini l'ordre initial des colonnes.
		myCols.add(col0);
		myCols.add(col6);
		myCols.add(col1);
		myCols.add(col11);
		myCols.add(col2);
		myCols.add(col31);
		myCols.add(colTitLibelle);
		myCols.add(col32);
		myCols.add(col3);
		myCols.add(col4);
		//		myCols.add(col5);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myTableModel.addTableModelListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.onTitreSelectionChanged();
	}

	public void updateData() {
		try {
			if (myListener.getSelectedBordereau() != null) {
				NSArray res = myListener.getSelectedBordereau().titres();
				myDisplayGroup.setObjectArray(res);
			}
			else {
				myDisplayGroup.setObjectArray(new NSArray());
			}
			myEOTable.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		ZLogger.debug("");
		initTableModel();
		initTable();
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public interface ITitreListePanelListener {
		public EOBordereau getSelectedBordereau();

		public void onTitreSelectionChanged();

		public HashMap getAllCheckedMAndats();

		//		public HashMap dicoOfMotifsRejetsTitres();
		public void onTitreCheckedChanged(int row, int col, EOTitre updatedObject);
	}

	/**
	 * Renvoie les Titres cochés
	 */
	public ArrayList checkedTitres() {
		return myTableModel.getRowsForValueAtCol(Boolean.TRUE, 0);
	}

	/**
	 * Renvoie le titre sélectionné.
	 */
	public EOTitre getSelectedTitre() {
		return (EOTitre) myDisplayGroup.selectedObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		if (e.getType() == TableModelEvent.UPDATE) {
			int column = e.getColumn();
			int row = e.getFirstRow();
			if (column >= 0) {
				//Vérifier si on est sur la colonne des checks
				if (column == 0) {
					myListener.onTitreCheckedChanged(row, column, (EOTitre) myDisplayGroup.displayedObjects().objectAtIndex(row));
				}
				//Si le motif de rejet a été modifié, on décoche la ligne
				else if (((ZEOTableModelColumn) myCols.get(column)).getAttributeName().equals(EOTitre.TIT_MOTIF_REJET_KEY)) {
					if (myTableModel.getValueAt(row, 0).equals(Boolean.TRUE)) {
						myTableModel.setValueAt(Boolean.FALSE, row, 0);
						fireTableCellUpdated(row, 0);
					}
				}
			}
		}

		//		String columnName = model.getColumnName(column);
		//		Object data = model.getValueAt(row, column);
	}

	public NSArray getDisplayedObjects() {
		return myDisplayGroup.displayedObjects();
	}

	/**
	 * Méthode à appeler pour informer le modèle qu'il doit rafraichir l'affichage l'ensemble de la table à partir des données. L'appel à cette
	 * méthode entraine la perte de la ligne sélectionnée par l'utilisateur. Si les données mises à jour se limitent à quelques cellules, utiliser
	 * plutot fireTableCellUpdated.
	 */
	public void fireTableDataChanged() {
		myTableModel.fireTableDataChanged();
	}

	/**
	 * Méthode à utiliser pour informer le modele que la valeur affichée dans une cellule donnée doit être rafrachie (la donnée a été mide à jour).
	 * Cette méthode à l'avantage de conserver la sélection de l'utilisateur, contrairement à la méthode fireTableDataChanged().
	 */
	public void fireTableCellUpdated(int row, int col) {
		myTableModel.fireTableCellUpdated(row, col);
	}

	/**
	 * @return
	 */
	public Vector getMyCols() {
		return myCols;
	}

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}
}
