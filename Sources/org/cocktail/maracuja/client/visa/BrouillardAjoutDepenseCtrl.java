/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.Dimension;
import java.util.Map;

import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;


public class BrouillardAjoutDepenseCtrl extends BrouillardAjoutCtrl {
	private static final String TITLE = "Nouvelle contre-partie";
	private static final Dimension WINDOW_DIMENSION = new Dimension(320, 170);

	public BrouillardAjoutDepenseCtrl(EOEditingContext ec, final EOQualifier qualPco) throws Exception {
		super(ec, qualPco);
		saisiePanel = new BrouillardAjoutPanel(new SaisiePanelListener());
	}

	protected final void checkDicoSaisie() throws Exception {
		super.checkDicoSaisie();
	}

	private final class SaisiePanelListener implements BrouillardAjoutPanel.IBrouillardAjoutPanelListener {

		public Action actionValider() {
			return actionValider;
		}

		public Action actionClose() {
			return actionClose;

		}

		public Map getFilters() {
			return dicoSaisie;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public ZEOComboBoxModel getGestionModel() {
			return myGestionComboBoxModel;
		}

		public IZTextFieldModel getPcoNumModel() {
			return pcoNumFieldModel;
		}

		public DefaultComboBoxModel getSensModel() {
			return mySensComboBoxModel;
		}

	}

	protected String getTitle() {
		return TITLE;
	}

	protected Dimension getWindowSize() {
		return WINDOW_DIMENSION;
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return saisiePanel;
	}

	public String title() {
		return TITLE;
	}

	@Override
	protected String getActionId() {
		return ZActionCtrl.IDU_VIDE;
	}

}
