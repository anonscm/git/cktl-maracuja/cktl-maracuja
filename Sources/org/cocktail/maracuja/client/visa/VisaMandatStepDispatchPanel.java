/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZEOLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOLot;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMarche;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.IZDataComponent;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaMandatStepDispatchPanel extends ZKarukeraStepPanel {
	private static final String TOPTITLE = "Choix des mandats et dépenses à viser";
	private static final String TOPMESSAGE = "Veuillez cocher les mandats à accepter, et décocher les mandats à rejeter";

	private final int PREFEREDWIDTH = 1000;
	private final int PREFEREDHEIGHT1 = 65;
	private final int PREFEREDHEIGHT2 = 200;
	private final int PREFEREDHEIGHT3 = 200;

	private IBordereauAViserDispatchPanelListener myListener;
	private MandatListePanel myMandatListePanel;
	private DepenseListePanel myDepenseListePanel;
	private RecapBordereauInfos myRecapBordereau;

	private EOBordereau myBordereau;
	private ZPanelNbTotal panelTotal1;
	private ZPanelNbTotal panelTotal2;

	/**
	 * Contient en clés toutes les dépenses de tous les mandats dépendants du bordereau, et en valeurs TRUE ou FALSE.
	 */
	private HashMap dicoOfDepenses;

	/**
	 * Contient en clés tous les mandats dépendants du bordereau, et en valeurs TRUE ou FALSE.
	 */
	private HashMap dicoOfMandats;

	//	private HashMap dicoOfMotifsRejetsMandats;

	public VisaMandatStepDispatchPanel(IBordereauAViserDispatchPanelListener vListener) {
		super();
		myListener = vListener;
		myMandatListePanel = new MandatListePanel(new MandatListePanelListener(), getEditingContext());
		myDepenseListePanel = new DepenseListePanel(new DepenseListePanelListener(), getEditingContext());
		myRecapBordereau = new RecapBordereauInfos();
		dicoOfDepenses = new HashMap();
		dicoOfMandats = new LinkedHashMap();
	}

	/**
	 * Met à jour la infos.
	 * 
	 * @throws Exception
	 */
	public void updateData() throws Exception {
		//	  pas de nested, apparemment ca regle les problemes de revert (snapshot)
		//	    myBordereau = (EOBordereau)ZFactory.nestedInstanceForObject(getEditingContext(), myListener.getSelectedBordereau()); 

		myBordereau = myListener.getSelectedBordereau();
		//test pour eviter erreurs de double visa
		getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
				myBordereau
		})));
		getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), myBordereau.mandats()));

		myMandatListePanel.updateData();
		int max = myMandatListePanel.getDisplayedObjects().count();
		dicoOfMandats.clear();
		//		dicoOfMotifsRejetsMandats.clear();
		for (int i = 0; i < myMandatListePanel.getDisplayedObjects().count(); i++) {
			dicoOfMandats.put(myMandatListePanel.getDisplayedObjects().objectAtIndex(i), Boolean.TRUE);
			//			dicoOfMotifsRejetsMandats.put(myMandatListePanel.getDisplayedObjects().objectAtIndex(i), null);
		}

		//On prerempli le tableau sui sert à cocher/decocher les cases des dépenses
		dicoOfDepenses.clear();
		//		dicoOfDepenses = new HashMap();
		for (int i = 0; i < myMandatListePanel.getDisplayedObjects().count(); i++) {
			for (int j = 0; j < ((EOMandat) myMandatListePanel.getDisplayedObjects().objectAtIndex(i)).depenses().count(); j++) {
				dicoOfDepenses.put(((EOMandat) myMandatListePanel.getDisplayedObjects().objectAtIndex(i)).depenses().objectAtIndex(j), Boolean.TRUE);
			}
		}

		//On interdit le cochage/decochage des depenses par defaut
		myDepenseListePanel.getCol0().setEditable(false);

		myRecapBordereau.updateData();
		panelTotal1.updateData();
		panelTotal2.updateData();
		//		myDepenseListePanel.updateData();
	}

	public void initGUI() {
		myMandatListePanel.initGUI();
		myDepenseListePanel.initGUI();
		myRecapBordereau.initGUI();
		super.initGUI();
	}

	public boolean isPrevVisible() {
		return true;
	}

	public boolean isNextVisible() {
		return true;
	}

	public boolean isPrevEnabled() {
		return true;
	}

	public boolean isNextEnabled() {
		return true;
	}

	public boolean isEndEnabled() {
		return true;
	}

	public boolean isCloseEnabled() {
		return true;
	}

	public boolean isCloseVisible() {
		return true;
	}

	public void onPrev() {
		//	  Annuler les modifs
		myMandatListePanel.getMyEOTable().cancelCellEditing();

		ZLogger.debug("Arborescence EC:", ZEOUtilities.arborescenceOfEc(getEditingContext()));
		ZLogger.debug("EditingContext : " + getEditingContext() + "updatedObjects avant revert------->:" + getEditingContext().updatedObjects());
		getEditingContext().revert();
		ZLogger.debug("EditingContext : " + getEditingContext() + "update apres revert------->:" + getEditingContext().updatedObjects());
		//On detruit également l'editingcontext
		//		getEditingContext().dispose();	    
		myListener.onPrev();
	}

	/**
	 * @return la liste des mandats acceptés par l'utilisateur (sont cochés). Se base sur dociOfMandats.
	 */
	public ArrayList<EOMandat> getMandatsAViser() {
		ArrayList<EOMandat> tmp = new ArrayList<EOMandat>();
		Iterator<EOMandat> iter = dicoOfMandats.keySet().iterator();
		while (iter.hasNext()) {
			EOMandat element = iter.next();
			if (((Boolean) dicoOfMandats.get(element)).booleanValue()) {
				tmp.add(element);
			}
		}
		return tmp;
	}

	/**
	 * @return la liste des mandats acceptés par l'utilisateur (sont décochés). Se base sur dicoOfMandats.
	 */
	public ArrayList getMandatsARejeter() {
		ArrayList tmp = new ArrayList();
		Iterator iter = dicoOfMandats.keySet().iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			if (!((Boolean) dicoOfMandats.get(element)).booleanValue()) {
				tmp.add(element);
			}
		}
		return tmp;
	}

	/**
	 * @param obj
	 * @return La liste des dépenses associées à u nmandat qui sonr acceptées par l'utilisateur. Se base sur dicoOfDepenses.
	 */
	public Vector getDepensesARejeterForMandat(EOMandat obj) {
		Vector tmp = new Vector();
		//Récupérer la liste des dépenses associées au mandat
		Vector tmp2 = obj.depenses().vector();
		Iterator iter = tmp2.iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			if (!((Boolean) dicoOfDepenses.get(element)).booleanValue()) {
				tmp.add(element);
			}
		}
		return tmp;
	}

	public ArrayList getAllMandats() {
		ArrayList list = new ArrayList(dicoOfMandats.keySet());
		return list;
	}

	/**
	 * Valide la saisie de l'utilisateur et affecte la date de reception à tous les mandats.
	 * 
	 * @throws Exception
	 */
	public boolean valideSaisie() throws Exception {
		if (!myMandatListePanel.getMyEOTable().stopCellEditing()) {
			return false;
		}
		final ArrayList tmp2 = getMandatsARejeter();
		final Iterator iter = tmp2.iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			//			vérifier que pour tous les mandats décochés, un motif de rejet est saisi	
			if (ZStringUtil.ifNull(((EOMandat) element).manMotifRejet(), "").length() == 0) {
				throw new DataCheckException("Veuillez préciser le motif de rejet pour le mandat n° " + ((EOMandat) element).manNumero() + ", ou bien acceptez le mandat.", null);
			}
		}
		//Vérifier que la date de reception est valide
		if (myListener.getDateReception() == null) {
			throw new DataCheckException("Veuillez indiquer une date réception.");
		}

		//Affecter le champ suppression_depense
		//		Vector tmp = new Vector();
		//Récupérer la liste des dépenses associées au mandat
		final Iterator iter2 = dicoOfDepenses.keySet().iterator();
		while (iter2.hasNext()) {
			EODepense element = (EODepense) iter2.next();
			if (!((Boolean) dicoOfDepenses.get(element)).booleanValue()) {
				element.setDepSuppression("OUI");
			}
		}

		return true;

		//Affecter la date de reception a tous les mandats
		//C'est fait dans la factory...
		//		Iterator iter2  =getAllMandats().iterator();
		//		while (iter2.hasNext()) {
		//            EOMandat element = (EOMandat) iter2.next();
		//            element.setManDateRemise(new NSTimestamp(myListener.getDateReception()));
		//        }
	}

	public void onNext() {
		try {

			setWaitCursor(true);
			valideSaisie();
			myListener.onNext();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	public void onClose() {
		myListener.onClose();
	}

	public String getCommentaire() {
		return TOPMESSAGE;
	}

	public String getTitle() {
		return TOPTITLE;
	}

	public Dimension getPanelDimension() {
		return myListener.getPanelDimension();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.BordereauAViserPanel.StepPanel#getContentPanel()
	 */
	public JPanel getContentPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		mainPanel.add(buildPanelRecapBordereau(), BorderLayout.PAGE_START);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, buildPanelMandatListe(), buildPanelDepensesListe());
		splitPane.setOneTouchExpandable(false);
		splitPane.setDividerLocation(0.5);
		splitPane.setDividerSize(5);
		splitPane.setResizeWeight(0.5);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		mainPanel.add(splitPane, BorderLayout.CENTER);
		mainPanel.add(buildPanelTotaux(), BorderLayout.PAGE_END);
		return mainPanel;
	}

	private JPanel buildPanelRecapBordereau() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createTitledBorder("Bordereau sélectionné"));
		mainPanel.add(myRecapBordereau, BorderLayout.PAGE_START);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT1));
		return mainPanel;
	}

	private JPanel buildPanelMandatListe() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Listes des mandats du bordereau", null, ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);
		mainPanel.add(myMandatListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}

	private JPanel buildPanelDepensesListe() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Dépenses associées au mandat", null, ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);
		mainPanel.add(myDepenseListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT3));
		return mainPanel;
	}

	private JPanel buildPanelTotaux() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		panelTotal1 = new ZPanelNbTotal("Mandats acceptés");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());
		panelTotal2 = new ZPanelNbTotal("Mandats rejetés");
		panelTotal2.setTotalProvider(new TotalModel2());
		panelTotal2.setNbProvider(new NbModel2());
		mainPanel.add(panelTotal1, BorderLayout.LINE_START);
		mainPanel.add(panelTotal2, BorderLayout.LINE_END);
		return mainPanel;
	}

	/**
	 * @return
	 */
	public IBordereauAViserDispatchPanelListener getMyListener() {
		return myListener;
	}

	/**
	 * @param listener
	 */
	public void setMyListener(IBordereauAViserDispatchPanelListener listener) {
		myListener = listener;
	}

	/**
	 * Listener pour la liste des mandats
	 */
	public interface IBordereauAViserDispatchPanelListener extends ZKarukeraStepListener {
		public EOBordereau getSelectedBordereau();

		public Date getDateReception();

		public void setDateReception(Date newDate);

		public Dialog getDialog();
	}

	private class MandatListePanelListener implements MandatListePanel.IMandatListePanelListener {
		public EOBordereau getSelectedBordereau() {
			return myBordereau;
		}

		public void onMandatSelectionChanged() {
			myDepenseListePanel.updateData();
			if (dicoOfMandats != null) {
				//correction... 23/03/05
				//			    onMandatCheckedChanged(myMandatListePanel.getMyEOTable().getSelectedRow() , -1,myMandatListePanel.getSelectedMandat()   );
			}
		}

		/**
		 * Lors du cochage/decochage d'un mandat
		 * 
		 * @see org.cocktail.maracuja.client.visa.MandatListePanel.IMandatListePanelListener#onMandatCheckedChanged()
		 */
		public void onMandatCheckedChanged(int row, int col, EOMandat updatedObject) {
			if (updatedObject != null) {
				//Un mandat a été coché/décoché
				//Si le mandat est coché, on supprime l'éventuel motif de rejet
				if (Boolean.TRUE.equals(dicoOfMandats.get(updatedObject))) {
					((EOMandat) updatedObject).setManMotifRejet(null);
					myMandatListePanel.fireTableCellUpdated(row, 6);
					//On interdit le cochage/decochage des depenses
					myDepenseListePanel.getCol0().setEditable(false);
				}
				else {
					//On autorise le cochage/decochage des depenses
					myDepenseListePanel.getCol0().setEditable(true);
				}

				//on synchronise les valeurs de depenses associées au mandat
				NSArray lesdeps = updatedObject.depenses();
				for (int i = 0; i < lesdeps.count(); i++) {
					dicoOfDepenses.put(lesdeps.objectAtIndex(i), dicoOfMandats.get(updatedObject));
					System.out.println(((EODepense) lesdeps.objectAtIndex(i)).depNumero() + " = " + dicoOfMandats.get(updatedObject));

				}
				myDepenseListePanel.fireTableDataChanged();

				panelTotal1.updateData();
				panelTotal2.updateData();
			}

		}

		public HashMap getAllCheckedMAndats() {
			return dicoOfMandats;
		}

		//		public HashMap dicoOfMotifsRejetsMandats() {
		//			return dicoOfMotifsRejetsMandats;
		//		}
	}

	/**
	 * Listener pour la liste des dépenses
	 */
	private class DepenseListePanelListener implements DepenseListePanel.IDepenseListePanelListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.MandatListePanel.IMandatListePanelListener#onMandatCheckedChanged()
		 */
		public void onDepenseCheckedChanged() {
			//		   System.out.print("Valeurs Cochees : ");
			//		   Vector tmp =  myDepenseListePanel.checkedDepenses() ;
			//		   for (int i = 0; i < tmp.size(); i++) {
			//			   System.out.print(((EOEnterpriseObject)tmp.elementAt(i)).valueForKey("depNumero")  +", ");
			//		   }
			//		   System.out.println();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.DepenseListePanel.IDepenseListePanelListener#getSelectedMndat()
		 */
		public EOMandat getSelectedMandat() {
			return myMandatListePanel.getSelectedMandat();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.DepenseListePanel.IDepenseListePanelListener#onDepenseSelectionChanged()
		 */
		public void onDepenseSelectionChanged() {

		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.DepenseListePanel.IDepenseListePanelListener#getAllCheckedDepenses()
		 */
		public HashMap getAllCheckedDepenses() {
			return dicoOfDepenses;
		}

		/**
		 * @see org.cocktail.maracuja.client.visa.DepenseListePanel.IDepenseListePanelListener#getLotForLotOrdre(java.lang.Integer)
		 */
		public EOLot getLotForLotOrdre(Integer lotOrdre) {
			return EOsFinder.getLotForLotOrdre(getEditingContext(), lotOrdre);
		}

		/**
		 * @see org.cocktail.maracuja.client.visa.DepenseListePanel.IDepenseListePanelListener#getMarcheForMarOrdre(java.lang.Integer)
		 */
		public EOMarche getMarcheForMarOrdre(Integer marcheOrdre) {
			return EOsFinder.getMarcheForMarOrdre(getEditingContext(), marcheOrdre);
		}

	}

	private class RecapBordereauInfos extends JPanel {
		private Vector fields;
		private ZBordereauFieldsModel myZEOLabelTextFieldProvider;

		public RecapBordereauInfos() {
			super();
			myZEOLabelTextFieldProvider = new ZBordereauFieldsModel();
		}

		public void initGUI() {
			//Mettre une marge
			this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			this.setLayout(new BorderLayout());
			Box boxForm = Box.createVerticalBox();
			Box boxLine1 = Box.createHorizontalBox();

			ZLabelTextField f1 = new ZEOLabelTextField("N°", EOBordereau.BOR_NUM_KEY, myZEOLabelTextFieldProvider);
			f1.getMyTexfield().setEditable(false);
			f1.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
			f1.getMyTexfield().setColumns(6);
			f1.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
			f1.getMyTexfield().setBackground(Color.WHITE);

			ZLabelTextField f2 = new ZEOLabelTextField("Code gestion", "gestion.gesCode", myZEOLabelTextFieldProvider);
			f2.getMyTexfield().setEditable(false);
			f2.getMyTexfield().setColumns(4);
			f2.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
			f2.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
			f2.getMyTexfield().setBackground(Color.WHITE);
			//			ZLabelTextField f3 = new ZLabelTextField("Total à viser", new ZFieldTotalProvider());
			//			f3.getMyTexfield().setColumns(10);
			//			f3.setMyFormat( new DecimalFormat("0.00"));
			//			f3.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
			//			f3.getMyTexfield().setEditable(false);
			//			ZLabelTextField f4 = new ZLabelTextField("Date de visa", new ZFieldDateVisaProvider() );
			ZDatePickerField d = new ZDatePickerField(new ZFieldDateVisaProvider(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
			ZFormPanel f4 = ZFormPanel.buildLabelField("Date de réception", d);
			//			ZLabelTextField f4 = new ZDatePickerField("Date de réception", new ZFieldDateVisaProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16), myListener.getDialog());
			//			f4.getMyTexfield().setEditable(true);
			//			f4.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
			//			f4.getMyTexfield().setColumns(10);

			fields = new Vector(4, 0);
			fields.add(f1);
			fields.add(f2);
			//			fields.add(f3);
			fields.add(f4);

			boxLine1.add(f1);
			boxLine1.add(Box.createRigidArea(new Dimension(2, 2)));
			boxLine1.add(f2);
			//			boxLine1.add(Box.createRigidArea(new Dimension(2,2)));
			//			boxLine1.add(f3);
			boxLine1.add(Box.createRigidArea(new Dimension(2, 2)));
			boxLine1.add(f4);
			boxLine1.add(Box.createHorizontalGlue());

			//			box0.add(Box.createRigidArea(new Dimension(1,50))); //on fixe la hauteur au niveau supérieur
			boxForm.add(boxLine1);
			boxForm.add(Box.createVerticalGlue());
			this.add(boxForm, BorderLayout.LINE_START);
			this.add(Box.createGlue(), BorderLayout.CENTER);

			//			this.setPreferredSize(new Dimension(1,55));
		}

		/**
		 * Met à jour l'affichage des champs du panel.
		 * 
		 * @throws Exception
		 */
		public void updateData() throws Exception {
			Iterator iter = fields.iterator();
			while (iter.hasNext()) {
				IZDataComponent c = (IZDataComponent) iter.next();
				c.updateData();
				//                if (c instanceof ZLabelTextField) {
				//                    ((ZLabelTextField)iter.next()).updateData();
				//                }

			}
		}

		private class ZBordereauFieldsModel implements ZEOLabelTextField.IZEOLabelTextFieldModel {
			/**
			 * @see org.cocktail.zutil.client.wo.ZEOLabelTextField.IZEOLabelTextFieldModel#getEo()
			 */
			public EOEnterpriseObject getEo() {
				return myListener.getSelectedBordereau();
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return null;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				return;
			}
		}

		//		/**
		//		 * Classe quyi fournit les données au champ total
		//		 */
		//		private class ZFieldTotalProvider implements ZLabelTextField.IZLabelTextFieldModel {
		//			/* (non-Javadoc)
		//			 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldProvider#getValue()
		//			 */
		//			public Object getValue() {
		////				System.out.println("Update total ");
		//				Iterator iter = getMandatsAViser().iterator();
		//				BigDecimal total = new BigDecimal(0);
		//				while (iter.hasNext()) {
		////					System.out.println("Update total iter1");
		//					EOMandat tmp = (EOMandat)iter.next();
		//					total = total.add( (BigDecimal) tmp.manHt()  );
		//				}
		////				System.out.println("Update total res = " + total);
		//				return total;
		//			}
		//			
		//			/**
		//			 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
		//			 */
		//			public void setValue(Object value) {
		//				return;
		//			}
		//		}

		private class ZFieldDateVisaProvider implements IZDatePickerFieldModel {
			/*
			 * (non-Javadoc)
			 * 
			 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldProvider#getValue()
			 */
			public Object getValue() {
				return getMyListener().getDateReception();
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				if ((value instanceof Date)) {
					getMyListener().setDateReception((Date) value);
				}
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#isActionButton1Enabled()
	 */
	public boolean isActionButton1Enabled() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#isActionButton1Visible()
	 */
	public boolean isActionButton1Visible() {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#specialAction1()
	 */
	public AbstractAction specialAction1() {
		return myListener.specialAction1();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#onDisplay()
	 */
	public void onDisplay() {
		specialAction1().setEnabled(true);
	}

	private class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return ZEOUtilities.calcSommeOfBigDecimals(getMandatsAViser(), "manTtc");
		}

		public void setValue(Object value) {
			return;
		}
	}

	private class TotalModel2 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return ZEOUtilities.calcSommeOfBigDecimals(getMandatsARejeter(), "manTtc");
		}

		public void setValue(Object value) {
			return;
		}
	}

	private class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return new Integer(getMandatsAViser().size());
		}

		public void setValue(Object value) {
			return;
		}
	}

	private class NbModel2 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return new Integer(getMandatsARejeter().size());
		}

		public void setValue(Object value) {
			return;
		}
	}

}
