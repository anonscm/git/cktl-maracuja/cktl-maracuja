/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.factories.ZFactory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaModifMandat;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Le panneau permettant à l'utilisateur de saisir les infos nécessaires pour le visa des mandats (comptes, modes de paiement, etc.) Les modifs
 * effectuées ici le sont dans un editingcontext particulier (nested). Si on revient en arriere, on annule les modifs, si on va en avant, on valide
 * les modifs.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaMandatStepDetailPanel extends ZKarukeraStepPanel {
	private static final String TOPTITLE = "Mandats à viser";
	private static final String TOPMESSAGE = "Veuillez spécifier les informations pour chaque mandat à viser.";

	private final int PREFEREDWIDTH = 785;
	private final int PREFEREDHEIGHT1 = 60;
	private final int PREFEREDHEIGHT2 = 200;
	private final int PREFEREDHEIGHT3 = 200;

	private IBordereauDetailVisesListener myListener;
	private MandatAcceptesListePanel myMandatAcceptesListePanel;
	private MandatBrouillardListePanel myMandatBrouillardListePanel;

	private final ActionDeleteMandatBrouillard actionDeleteMandatBrouillard = new ActionDeleteMandatBrouillard();

	public VisaMandatStepDetailPanel(IBordereauDetailVisesListener vListener) {
		super();
		myListener = vListener;
		//On cree un nested de l'editingcontext de bordereauDispatch
		setEditingContext(new EOEditingContext(myListener.getParentEditingContext()));

		myMandatAcceptesListePanel = new MandatAcceptesListePanel(new MandatAcceptesListePanelListener(), getEditingContext());
		myMandatBrouillardListePanel = new MandatBrouillardListePanel(new MandatBrouillardListePanelListener(), getEditingContext());
	}

	/**
	 * Met à jour la infos.
	 */
	public void updateData() throws Exception {
		//on travaille sur un nested (dont le parent est l'editingcontext du panel précédent)

		myMandatAcceptesListePanel.updateData();
	}

	public void initGUI() {
		myMandatAcceptesListePanel.initGUI();
		myMandatBrouillardListePanel.initGUI();
		super.initGUI();
	}

	public boolean isPrevVisible() {
		return true;
	}

	public boolean isNextVisible() {
		return true;
	}

	public boolean isPrevEnabled() {
		return true;
	}

	public boolean isNextEnabled() {
		return true;
	}

	public boolean isEndEnabled() {
		return true;
	}

	public boolean isCloseEnabled() {
		return true;
	}

	public boolean isCloseVisible() {
		return true;
	}

	public void onPrev() {
		getEditingContext().revert();
		myListener.onPrev();
	}

	public void onNext() {
		try {
			setWaitCursor(true);
			valideSaisie();
			getEditingContext().saveChanges();
			//Si pas de pb, on change de panel 
			myListener.onNext();
			setWaitCursor(false);

		} catch (Exception e) {
			showErrorDialog(e);
			setWaitCursor(false);
			return;
		}

	}

	public void onClose() {
		myListener.onClose();
	}

	public String getCommentaire() {
		return TOPMESSAGE;
	}

	public String getTitle() {
		return TOPTITLE;
	}

	/**
	 * Effectue des vérifications sur la saisie de l'utilisateur.
	 * 
	 * @throws Exception
	 */
	private void checkData() throws DataCheckException {
		//Vérifier que toutes les écritures sont équilibrées
		checkMontantsBrouillards();
		checkEcrituresEquilibrees();
		checkComptesMP();

	}

	/**
	 * Vérifier que toutes les écritures associées au mandats sont équilibrées
	 * 
	 * @throws DataCheckException
	 */
	private void checkEcrituresEquilibrees() throws DataCheckException {
		NSArray res = myMandatAcceptesListePanel.getMyDisplayGroup().displayedObjects();
		for (int i = 0; i < res.count(); i++) {
			final EOMandat array_element = (EOMandat) res.objectAtIndex(i);
			final NSArray debits = getDebitsForMandat(array_element);
			final NSArray credits = getCreditsForMandat(array_element);

			final BigDecimal cred = ZEOUtilities.calcSommeOfBigDecimals(credits, "mabMontant");
			final BigDecimal deb = ZEOUtilities.calcSommeOfBigDecimals(debits, "mabMontant");

			if (!cred.equals(deb)) {
				throw new DataCheckException("L'écriture associée au mandat n° " + array_element.manNumero() + " n'est pas équilibrée. (" + cred.toString() + "/" + deb.toString() + ")");
			}
		}
	}

	private void checkMontantsBrouillards() throws DataCheckException {
		NSArray res = myMandatAcceptesListePanel.getMyDisplayGroup().displayedObjects();
		for (int i = 0; i < res.count(); i++) {
			final EOMandat array_element = (EOMandat) res.objectAtIndex(i);
			//final NSArray debits = getDebitsForMandat(array_element);
			final NSArray credits = getCreditsForMandat(array_element);

			final BigDecimal cred = ZEOUtilities.calcSommeOfBigDecimals(credits, "mabMontant");
			//final BigDecimal deb = ZEOUtilities.calcSommeOfBigDecimals(debits, "mabMontant");

			if (!cred.abs().equals(array_element.manTtc().abs())) {
				throw new DataCheckException("Le montant des crédits de l'écriture de prise en charge pour le mandat n° " + array_element.manNumero() + " doit êre égal au montant TTC du mandat. (" + cred.toString() + "/" + array_element.manTtc().toString() + ")");
			}
		}
	}

	private boolean checkComptesMP() throws DataCheckException {

		//        
		//        
		//        //Recuperer les comptes utilises comme pco_visa
		//        final NSArray mps = getModePaiementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
		//        final ArrayList pcoVisa = new ArrayList();
		//        final ArrayList pcoPaiement = new ArrayList();
		//        for (int i = 0; i < mps.count(); i++) {
		//            final EOModePaiement element = (EOModePaiement) mps.objectAtIndex(i);
		//            if (element.planComptableVisa() != null) {
		//                pcoVisa.add(element.planComptableVisa());
		//            }
		//            if (element.planComptablePaiement() != null) {
		//                pcoPaiement.add(element.planComptablePaiement());
		//            }
		//            
		//        }
		//        
		//	    final NSArray res = myMandatAcceptesListePanel.getMyDisplayGroup().displayedObjects();
		//	    for (int i = 0; i < res.count(); i++) {
		//	        final EOMandat array_element = (EOMandat) res.objectAtIndex(i);
		//	        final NSArray credits = getCreditsForMandat(array_element);
		//
		//            for (int j = 0; j < credits.count(); j++) {
		//                final EOMandatBrouillard element = (EOMandatBrouillard) credits.objectAtIndex(j);
		//                if (element.planComptable(). )
		//
		//            }
		//            
		//            
		//	        if (!cred.equals( deb  )    ) {
		//	            throw new DataCheckException("L'écriture associée au mandat n° "+array_element.manNumero() + " n'est pas équilibrée. ("+cred.toString()+"/"+ deb.toString() +")");
		//	        }
		//	    } 		
		//	    

		return true;
	}

	public Dimension getPanelDimension() {
		return myListener.getPanelDimension();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.BordereauAViserPanel.StepPanel#getContentPanel()
	 */
	public JPanel getContentPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		//		mainPanel.add(buildPanelRecapBordereau(), BorderLayout.PAGE_START);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, buildPanelMandatListe(), buildPanelMandatBrouillard());
		splitPane.setOneTouchExpandable(false);
		splitPane.setDividerLocation(0.5);
		splitPane.setDividerSize(5);
		splitPane.setResizeWeight(0.5);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		mainPanel.add(splitPane, BorderLayout.CENTER);
		return mainPanel;
	}

	private JPanel buildPanelMandatListe() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Listes des mandats que vous avez acceptés", null, ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);
		mainPanel.add(myMandatAcceptesListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}

	private JPanel buildPanelBottom() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		//		mainPanel.add(buildTitlePanel("Listes des mandats que vous avez sélectionné", null,ZConst.GRADIENTTITLESTARTCOLOR, ZConst.GRADIENTTITLEENDCOLOR), BorderLayout.PAGE_START);
		//		mainPanel.add(myMandatAcceptesListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}

	private JPanel buildPanelMandatBrouillard() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Les écritures associées au mandat", null, ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);

		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BorderLayout());
		tmpPanel.add(myMandatBrouillardListePanel, BorderLayout.CENTER);
		tmpPanel.add(buildMandatBrouillardListeToolBar(), BorderLayout.LINE_END);

		mainPanel.add(tmpPanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}

	//attention : mettre un warning si on est en MP virement et que on sélectionne 
	//un compte de classe 4 qui est utilisé comme pco_num_visa dans un MP interne  

	private class ActionAddMandatBrouillard extends AbstractAction {
		public ActionAddMandatBrouillard() {
			super("+");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter une contrepartie");
		}

		public void actionPerformed(ActionEvent e) {
			final EOMandatBrouillard mandatBrouillard = myMandatBrouillardListePanel.getSelectedMandatBrouillard();
			if (EOTypeBordereau.SOUS_TYPE_REVERSEMENTS.equals(mandatBrouillard.mandat().bordereau().typeBordereau().tboSousType())) {
				mandatBrouillardAddReversement();
			}
			else {
				mandatBrouillardAddDepense();
			}

		}
	}

	private class ActionDeleteMandatBrouillard extends AbstractAction {
		public ActionDeleteMandatBrouillard() {
			super("-");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer la contrepartie");
		}

		public void actionPerformed(ActionEvent e) {
			//		    showinfoDialogFonctionNonImplementee();			
			mandatBrouillardDelete();
		}
	}

	private JToolBar buildMandatBrouillardListeToolBar() {
		JToolBar tmpToolBar = new JToolBar("", JToolBar.VERTICAL);
		tmpToolBar.setFloatable(false);
		tmpToolBar.setRollover(false);
		tmpToolBar.add(Box.createRigidArea(new Dimension(2, 1)));
		tmpToolBar.add(new ActionAddMandatBrouillard());
		tmpToolBar.add(actionDeleteMandatBrouillard);
		return tmpToolBar;
	}

	/**
	 * @return
	 */
	public IBordereauDetailVisesListener getMyListener() {
		return myListener;
	}

	/**
	 * @param listener
	 */
	public void setMyListener(IBordereauDetailVisesListener listener) {
		myListener = listener;
	}

	/**
	 * Listener pour la liste des mandats
	 */
	public interface IBordereauDetailVisesListener extends ZKarukeraStepListener {
		public ArrayList getMandatsAViser();
		//		public Dialog getDialog();
	}

	public class MandatAcceptesListePanelListener implements MandatAcceptesListePanel.IMandatAcceptesListePanelListener {
		public ArrayList getMandatsAViser() {
			//Créer des instances locales pour les objets
			return ZFactory.nestedInstancesForObjects(getEditingContext(), myListener.getMandatsAViser());
			//			return   myListener.getMandatsAViser();
		}

		public void onMandatSelectionChanged() {
			myMandatBrouillardListePanel.updateData();
		}
	}

	public class MandatBrouillardListePanelListener implements MandatBrouillardListePanel.IMandatBrouillardListePanelListener {
		public EOMandat getSelectedMandat() {
			return myMandatAcceptesListePanel.getSelectedMandat();
		}

		public void onSelectionChanged() {
			refreshBrouillardAction();

		}
	}

	private void refreshBrouillardAction() {
		final EOMandatBrouillard mandatBrouillard = myMandatBrouillardListePanel.getSelectedMandatBrouillard();
		final boolean isOk = (mandatBrouillard != null && !EOMandatBrouillard.VISA_ANNULER.equals(mandatBrouillard.mabOperation())
				&& !mandatBrouillard.planComptable().pcoNum().equals(mandatBrouillard.mandat().planComptable().pcoNum()));

		//				
		//				&& ((EOMandatBrouillard.SENS_CREDIT.equals(mandatBrouillard.mabSens()) && !EOTypeBordereau.SOUS_TYPE_REVERSEMENTS.equals(mandatBrouillard.mandat().bordereau().typeBordereau().tboSousType()))
		//					|| (EOMandatBrouillard.SENS_DEBIT.equals(mandatBrouillard.mabSens()) && EOTypeBordereau.SOUS_TYPE_REVERSEMENTS.equals(mandatBrouillard.mandat().bordereau().typeBordereau().tboSousType()))
		//					||  mandatBrouillard.planComptable().pcoNum().startsWith("445") ) );
		actionDeleteMandatBrouillard.setEnabled(isOk);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#specialAction1()
	 */
	public AbstractAction specialAction1() {
		return myListener.specialAction1();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#onDisplay()
	 */
	public void onDisplay() {
		return;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel#valideSaisie()
	 */
	public boolean valideSaisie() throws Exception {
		checkData();
		return true;
	}

	public final NSArray getDebitsForMandat(final EOMandat mandat) {
		final EOQualifier qualDebit = EOQualifier.qualifierWithQualifierFormat("mabOperation<>%@ and mabSens=%@", new NSArray(new Object[] {
				EOMandatBrouillard.VISA_ANNULER, EOMandatBrouillard.SENS_DEBIT
		}));
		return EOQualifier.filteredArrayWithQualifier(mandat.mandatBrouillards(), qualDebit);
	}

	public final NSArray getCreditsForMandat(final EOMandat mandat) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("mabOperation<>%@ and mabSens=%@", new NSArray(new Object[] {
				EOMandatBrouillard.VISA_ANNULER, EOMandatBrouillard.SENS_CREDIT
		}));
		return EOQualifier.filteredArrayWithQualifier(mandat.mandatBrouillards(), qual);
	}

	/**
	 * Ajoute une contrepartie (crédit)
	 */
	private void mandatBrouillardAddDepense() {
		try {
			final EOMandat mandat = myMandatAcceptesListePanel.getSelectedMandat();
			if (mandat == null) {
				throw new DefaultClientException("Le mandat est nul.");
			}
			final NSArray debits = getDebitsForMandat(mandat);
			final NSArray credits = getCreditsForMandat(mandat);

			final BigDecimal montantDebit = ZEOUtilities.calcSommeOfBigDecimals(debits, "mabMontant");
			final BigDecimal montantCredit = ZEOUtilities.calcSommeOfBigDecimals(credits, "mabMontant");

			//			final EOQualifier qualPcoMandat = EOQualifier.qualifierWithQualifierFormat("(pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@)", new NSArray(new Object[] {
			//					"28*", "29*", "3*", "4*", "5*"
			//			}));

			final EOQualifier qualPcoMandat = EOsFinder.qualifierForPconumContrepartiesDepense();

			final BrouillardAjoutCtrl brouillardAjoutCtrl = new BrouillardAjoutDepenseCtrl(getEditingContext(), qualPcoMandat);
			String sensTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? BrouillardAjoutCtrl.SENS_CREDIT : BrouillardAjoutCtrl.SENS_DEBIT);
			BigDecimal montantTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? montantDebit.subtract(montantCredit) : montantCredit.subtract(montantDebit));
			final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantTmp, mandat.gestion(), sensTmp);

			if (res != null) {
				final FactoryProcessVisaModifMandat factoryProcessVisaModifMandat = new FactoryProcessVisaModifMandat(myApp.wantShowTrace(), null);

				//final String sens = EOMandatBrouillard.SENS_CREDIT;
				final String sens = ((String) res.get("sens")).substring(0, 1);
				final BigDecimal leMontant = (BigDecimal) res.get("montant");
				final EOGestion gestion = (EOGestion) res.get("gestion");
				final EOPlanComptable planComptable = (EOPlanComptable) res.get(EOMandatBrouillard.PLAN_COMPTABLE_KEY);

				if (leMontant == null) {
					throw new DefaultClientException("Le montant est obligatoire.");
				}

				if (gestion == null) {
					throw new DefaultClientException("Le code gestion est obligatoire.");
				}

				if (planComptable == null) {
					throw new DefaultClientException("Le compte est obligatoire.");
				}

				String mabOrigine = (planComptable.pcoNum().startsWith("445") ? EOMandatBrouillard.VISA_TVA : EOMandatBrouillard.VISA_CTP);

				final EOMandatBrouillard mandatBrouillard = factoryProcessVisaModifMandat.creerMandatBrouillard(getEditingContext(), myApp.appUserInfo().getUtilisateur(),
						mandat.exercice(), gestion, planComptable, mandat, mabOrigine, sens, leMontant);

				if (mandatBrouillard == null) {
					throw new DefaultClientException("Erreur lors de la création du brouillard.");
				}

				ZLogger.debug("Brouillard cree", mandatBrouillard);

				myMandatBrouillardListePanel.updateData();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void mandatBrouillardAddReversement() {
		try {
			final EOMandat mandat = myMandatAcceptesListePanel.getSelectedMandat();
			if (mandat == null) {
				throw new DefaultClientException("Le mandat est nul.");
			}
			final NSArray debits = getDebitsForMandat(mandat);
			final NSArray credits = getCreditsForMandat(mandat);

			final BigDecimal montantDebit = ZEOUtilities.calcSommeOfBigDecimals(debits, "mabMontant");
			final BigDecimal montantCredit = ZEOUtilities.calcSommeOfBigDecimals(credits, "mabMontant");

			final EOQualifier qualPcoMandat = EOQualifier.qualifierWithQualifierFormat("(pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@)", new NSArray(new Object[] {
					"28*", "29*", "3*", "4*", "59*", "58*"
			}));

			final BrouillardAjoutCtrl brouillardAjoutCtrl = new BrouillardAjoutDepenseCtrl(getEditingContext(), qualPcoMandat);
			String sensTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? BrouillardAjoutCtrl.SENS_CREDIT : BrouillardAjoutCtrl.SENS_DEBIT);
			BigDecimal montantTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? montantDebit.subtract(montantCredit) : montantCredit.subtract(montantDebit));
			final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantTmp, mandat.gestion(), sensTmp);

			//			final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantCredit.subtract(montantDebit), mandat.gestion(), BrouillardAjoutCtrl.SENS_DEBIT);

			if (res != null) {
				final FactoryProcessVisaModifMandat factoryProcessVisaModifMandat = new FactoryProcessVisaModifMandat(myApp.wantShowTrace(), null);

				//final String sens = EOMandatBrouillard.SENS_DEBIT;
				final String sens = ((String) res.get("sens")).substring(0, 1);
				final BigDecimal leMontant = (BigDecimal) res.get("montant");
				final EOGestion gestion = (EOGestion) res.get("gestion");
				final EOPlanComptable planComptable = (EOPlanComptable) res.get(EOMandatBrouillard.PLAN_COMPTABLE_KEY);

				if (leMontant == null) {
					throw new DefaultClientException("Le montant est obligatoire.");
				}

				if (gestion == null) {
					throw new DefaultClientException("Le code gestion est obligatoire.");
				}

				if (planComptable == null) {
					throw new DefaultClientException("Le compte est obligatoire.");
				}

				final EOMandatBrouillard mandatBrouillard = factoryProcessVisaModifMandat.creerMandatBrouillard(getEditingContext(), myApp.appUserInfo().getUtilisateur(),
						mandat.exercice(), gestion, planComptable, mandat, EOMandatBrouillard.VISA_CTP, sens, leMontant);

				if (mandatBrouillard == null) {
					throw new DefaultClientException("Erreur lors de la création du brouillard.");
				}

				ZLogger.debug("Brouillard cree", mandatBrouillard);

				myMandatBrouillardListePanel.updateData();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	/**
	 * Supprime la contrepartie (crédit) sélectionnée
	 */
	private void mandatBrouillardDelete() {
		try {
			final EOMandat mandat = myMandatAcceptesListePanel.getSelectedMandat();
			if (mandat == null) {
				throw new DefaultClientException("Le mandat est nul.");
			}

			final EOMandatBrouillard mandatBrouillard = myMandatBrouillardListePanel.getSelectedMandatBrouillard();
			if (mandatBrouillard == null) {
				throw new DefaultClientException("Aucun brouillard sélectionné");
			}

			final FactoryProcessVisaModifMandat factoryProcessVisaModifMandat = new FactoryProcessVisaModifMandat(myApp.wantShowTrace(), null);
			factoryProcessVisaModifMandat.supprimerMandatBrouillard(getEditingContext(), mandatBrouillard);

			myMandatBrouillardListePanel.updateData();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

}
