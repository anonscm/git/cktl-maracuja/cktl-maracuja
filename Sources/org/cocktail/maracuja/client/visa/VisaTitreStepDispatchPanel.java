/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZEOLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.IZDataComponent;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;



/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaTitreStepDispatchPanel extends ZKarukeraStepPanel {
	private static final String  TOPTITLE = "Choix des titres et recettes à viser";
	private static final String  TOPMESSAGE = "Veuillez cocher les titres à accepter, et décocher les titres à rejeter";
	
	private final int PREFEREDWIDTH=1000;
	private final int PREFEREDHEIGHT1=65;
	private final int PREFEREDHEIGHT2=200;
	private final int PREFEREDHEIGHT3=200;
	
	
	
	private IBordereauAViserDispatchPanelListener myListener;
	private TitreListePanel myTitreListePanel;
	private RecetteListePanel myRecetteListePanel;
	private RecapBordereauInfos myRecapBordereau;
	
	private EOBordereau myBordereau;
	private ZPanelNbTotal panelTotal1;
	private ZPanelNbTotal panelTotal2;	
	
	/**
	 * Contient en clés toutes les recettes de tous les titres dépendants du bordereau, et en valeurs TRUE ou FALSE.
	 */
	private HashMap dicoOfRecettes;
	
	/**
	 * Contient en clés tous les titres dépendants du bordereau, et en valeurs TRUE ou FALSE.
	 */
	private HashMap dicoOfTitres;
	

//	private HashMap dicoOfMotifsRejetsTitres;
	
	
	public VisaTitreStepDispatchPanel (IBordereauAViserDispatchPanelListener vListener) {
		super();
		myListener = vListener;
		//on ne cree pas de nested, pas besoin a ce niveau la... (sinon pb avec revert...) 
//		setEditingContext(new EOEditingContext( myListener.getParentEditingContext() ));
		myTitreListePanel = new TitreListePanel(new TitreListePanelListener(), getEditingContext());
		myRecetteListePanel = new RecetteListePanel(new RecetteListePanelListener(), getEditingContext());
		myRecapBordereau = new RecapBordereauInfos();
		dicoOfRecettes = new HashMap();
		dicoOfTitres = new HashMap();
	}
	

	/**
	 * Met à jour la infos.
	 * @throws Exception 
	 */
	public void updateData() throws Exception {
	    //a voir... apparemment ca regle les problemes de revert (snapshot)
//	    myBordereau = (EOBordereau)ZFactory.nestedInstanceForObject(getEditingContext(), myListener.getSelectedBordereau());
	    
	    myBordereau = myListener.getSelectedBordereau(); 
	    
		myTitreListePanel.updateData();
		int max = myTitreListePanel.getDisplayedObjects().count();
		dicoOfTitres.clear();
//		dicoOfMotifsRejetsTitres.clear();
		for (int i = 0; i < myTitreListePanel.getDisplayedObjects().count(); i++) {
			dicoOfTitres.put(myTitreListePanel.getDisplayedObjects().objectAtIndex(i), Boolean.TRUE);
//			dicoOfMotifsRejetsTitres.put(myTitreListePanel.getDisplayedObjects().objectAtIndex(i), null);
		}
		
		//On prerempli le tableau sui sert à cocher/decocher les cases des recettes
		dicoOfRecettes.clear();
//		dicoOfRecettes = new HashMap();
		for (int i = 0; i < myTitreListePanel.getDisplayedObjects().count(); i++) {
			for (int j = 0; j <  ((EOTitre)myTitreListePanel.getDisplayedObjects().objectAtIndex(i)).recettes().count()  ; j++) {
				dicoOfRecettes.put(((EOTitre)myTitreListePanel.getDisplayedObjects().objectAtIndex(i)).recettes().objectAtIndex(j), Boolean.TRUE);	
			}
		}
		
		//On interdit le cochage/decochage des recettes par defaut
		myRecetteListePanel.getCol0().setEditable(false); 		
		
		myRecapBordereau.updateData();
		panelTotal1.updateData();
		panelTotal2.updateData();
//		myRecetteListePanel.updateData();
	}		
		
		
	public void initGUI() {		
		myTitreListePanel.initGUI();
		myRecetteListePanel.initGUI();
		myRecapBordereau.initGUI();
		super.initGUI();
	}




	public boolean isPrevVisible() {
		return true;
	}

	public boolean isNextVisible() {
		return true;
	}


	public boolean isPrevEnabled() {
		return true;
	}


	public boolean isNextEnabled() {
		return true;
	}


	public boolean isEndEnabled() {
		return true;
	}
		
	public boolean isCloseEnabled() {
		return true;
	}


	public boolean isCloseVisible() {
		return true;
	}

	public void onPrev() {
//	  Annuler les modifs
        
        myTitreListePanel.getMyEOTable().cancelCellEditing();
                
	    ZLogger.debug("Arborescence EC:", ZEOUtilities.arborescenceOfEc(getEditingContext()));
	    
	    ZLogger.debug(getEditingContext());
	    
//	    ZLogger.debug("EditingContext : "+ getEditingContext() +  "updatedObjects avant revert------->:" + getEditingContext().updatedObjects());
		getEditingContext().revert();
//		ZLogger.debug("EditingContext : "+ getEditingContext()+"update apres revert------->:" + getEditingContext().updatedObjects());
		//On detruit également l'editingcontext
//		getEditingContext().dispose();	    
		myListener.onPrev();
	}


	/**
	 * @return la liste des titres acceptés par l'utilisateur (sont cochés). Se base sur dociOfTitres.
	 */
	public ArrayList getTitresAViser() {
//		Vector tmp = new Vector();
        ArrayList tmp = new ArrayList();
		Iterator iter = dicoOfTitres.keySet().iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			if (((Boolean)dicoOfTitres.get(element)).booleanValue() ) {
				tmp.add(element);
			}
		}
		return tmp;
	}

	/**
	 * @return la liste des titres acceptés par l'utilisateur (sont décochés). Se base sur dicoOfTitres.
	 */
	public ArrayList getTitresARejeter() {
		ArrayList tmp = new ArrayList();
		Iterator iter = dicoOfTitres.keySet().iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			if (!((Boolean)dicoOfTitres.get(element)).booleanValue() ) {
				tmp.add(element);
			}
		}
		return tmp;
	}

	
	


	/**
	 * @param obj 
	 * @return La liste des recettes associées à u ntitre qui sonr acceptées par l'utilisateur. Se base sur dicoOfRecettes.
	 */
	public Vector getRecettesARejeterForTitre(EOTitre obj) {
		Vector tmp = new Vector();
		//Récupérer la liste des recettes associées au titre
		Vector tmp2 = obj.recettes().vector();
		Iterator iter = tmp2.iterator();
		while (iter.hasNext()) {
			Object element = iter.next();
			if (!((Boolean)dicoOfRecettes.get(element)).booleanValue() ) {
				tmp.add(element);
			}
		}
		return tmp;
	}

	
	public ArrayList getAllTitres() {
	    ArrayList list = new ArrayList( dicoOfTitres.keySet());
	    return list;
	}
	

	/**
	 * Valide la saisie de l'utilisateur et affecte la date de reception à tous les titres.
	 * @throws Exception
	 */
	public boolean valideSaisie() throws Exception {
        if (!myTitreListePanel.getMyEOTable().stopCellEditing()) {
            return false;
        }
        
        final ArrayList tmp2 = getTitresARejeter();
		final Iterator iter = tmp2.iterator();
		while (iter.hasNext()) {
			final Object element = iter.next();
//			vérifier que pour tous les titres décochés, un motif de rejet est saisi	
			if ( ZStringUtil.ifNull( ((EOTitre)element).titMotifRejet() , "").length()==0) {
				throw new DataCheckException("Veuillez préciser le motif de rejet pour le titre n° "+((EOTitre)element).titNumero() +", ou bien acceptez le titre.",null);
			}
		}
		//Vérifier que la date de reception est valide
		if (myListener.getDateReception()==null) {
			throw new DataCheckException("Veuillez indiquer une date réception.");
		}
//		
//		if (  !(new SimpleDateFormat("yyyy")).format(myListener.getDateReception()) .equals( myApp.appUserInfo().getCurrentExercice().exeExercice().toString() )   ) {
//			throw new DataCheckException("La date de reception que vous avez indiqué n'est pas sur l'exercice .");
//		}
		
		//Affecter le champ suppression_recette
//		Vector tmp = new Vector();
		//Récupérer la liste des recettes associées au titre
		final Iterator iter2 = dicoOfRecettes.keySet().iterator();
		while (iter2.hasNext()) {
			final EORecette element = (EORecette)iter2.next();
			if (!((Boolean)dicoOfRecettes.get(element)).booleanValue() ) {
			    element.setRecSuppression("OUI");
			}
		}		
		
		
		return true;
		
		//Affecter la date de reception a tous les titres
		//C'est fait dans la factory...
//		Iterator iter2  =getAllTitres().iterator();
//		while (iter2.hasNext()) {
//            EOTitre element = (EOTitre) iter2.next();
//            element.setManDateRemise(new NSTimestamp(myListener.getDateReception()));
//        }
	}
	

	public void onNext() {	    
		try {

		    setWaitCursor(true);
		    valideSaisie();
		    myListener.onNext();
		} catch (Exception e) {
			showErrorDialog(e);
		}
		finally {
		    setWaitCursor(false);
		}
	}

	public void onClose() {
		myListener.onClose();
	}

	public String getCommentaire() {
		return TOPMESSAGE;
	}

	public String getTitle() {
		return TOPTITLE;
	}


	public Dimension getPanelDimension() {
		return myListener.getPanelDimension();
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.BordereauAViserPanel.StepPanel#getContentPanel()
	 */
	public JPanel getContentPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		mainPanel.add(buildPanelRecapBordereau(), BorderLayout.PAGE_START);
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, buildPanelTitreListe(), buildPanelRecettesListe());
		splitPane.setOneTouchExpandable(false);
		splitPane.setDividerLocation(0.5);
		splitPane.setDividerSize(5);
		splitPane.setResizeWeight(0.5);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		mainPanel.add(splitPane, BorderLayout.CENTER);
		mainPanel.add(buildPanelTotaux(), BorderLayout.PAGE_END);
		return mainPanel;	
	}			
		
	private JPanel buildPanelRecapBordereau() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createTitledBorder("Bordereau sélectionné"));
		mainPanel.add(myRecapBordereau, BorderLayout.PAGE_START);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT1));
		return mainPanel;
	}
	
	private JPanel buildPanelTitreListe() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Listes des titres du bordereau", null,ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);
		mainPanel.add(myTitreListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}
	
	private JPanel buildPanelRecettesListe() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Recettes associées au titre", null,ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);
		mainPanel.add(myRecetteListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT3));
		return mainPanel;
	}
	
	private JPanel buildPanelTotaux() {
	    JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		panelTotal1 = new ZPanelNbTotal("Titres acceptés");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());
		panelTotal2 = new ZPanelNbTotal("Titres rejetés");
		panelTotal2.setTotalProvider(new TotalModel2());
		panelTotal2.setNbProvider(new NbModel2());
		mainPanel.add(panelTotal1,BorderLayout.LINE_START);
		mainPanel.add(panelTotal2,BorderLayout.LINE_END);
		return mainPanel;	    
	}
		


		
		
		
		

	/**
	 * @return
	 */
	public IBordereauAViserDispatchPanelListener getMyListener() {
		return myListener;
	}

	/**
	 * @param listener
	 */
	public void setMyListener(IBordereauAViserDispatchPanelListener listener) {
		myListener = listener;
	}
	



	/**
	 * Listener pour la liste des titres
	 */
	public interface IBordereauAViserDispatchPanelListener extends ZKarukeraStepListener {
		public EOBordereau getSelectedBordereau();
		public Date getDateReception();
		public void setDateReception(Date newDate);
		public Dialog getDialog();
	}

	 private class TitreListePanelListener implements TitreListePanel.ITitreListePanelListener {
	     public EOBordereau getSelectedBordereau() {
			return myBordereau;
		}

		public void onTitreSelectionChanged() {
			myRecetteListePanel.updateData();
			if (dicoOfTitres!=null) {
			    //et faire comme pour les mandats (desactiver la ligne)
//			    onTitreCheckedChanged(myTitreListePanel.getMyEOTable().getSelectedRow() , -1,myTitreListePanel.getSelectedTitre()   );
			}			
		}

		/**
		 * Lors du cochage/decochage d'un titre
		 * @see org.cocktail.maracuja.client.visa.TitreListePanel.ITitreListePanelListener#onTitreCheckedChanged()
		 */
		public void onTitreCheckedChanged(int row, int col, EOTitre updatedObject) {
			//Un titre a été coché/décoché
		    if (updatedObject!=null) {
				//Si le titre est coché, on supprime l'éventuel motif de rejet
				if ( Boolean.TRUE.equals( dicoOfTitres.get(updatedObject))) {
					((EOTitre)updatedObject).setTitMotifRejet(null);
					myTitreListePanel.fireTableCellUpdated(row, 6);
					//On interdit le cochage/decochage des recettes
					myRecetteListePanel.getCol0().setEditable(false); 
				}
				else {
					//On autorise le cochage/decochage des recettes
					myRecetteListePanel.getCol0().setEditable(true); 
				}
				
				//on synchronise les valeurs de recettes associées au titre
				NSArray lesdeps = updatedObject.recettes();
				for (int i = 0; i < lesdeps.count() ; i++) {
					dicoOfRecettes.put(lesdeps.objectAtIndex(i) , dicoOfTitres.get( updatedObject));
				}
				myRecetteListePanel.fireTableDataChanged();
				
				panelTotal1.updateData();
				panelTotal2.updateData();			
		        
		    }
		}
		

		
		
		public HashMap getAllCheckedMAndats() {			
			return dicoOfTitres;
		}
		
//		public HashMap dicoOfMotifsRejetsTitres() {
//			return dicoOfMotifsRejetsTitres;
//		}
	 }
	
	
	/**
	 * Listener pour la liste des recettes
	 */
	private class RecetteListePanelListener implements RecetteListePanel.IRecetteListePanelListener {

	   /* (non-Javadoc)
		* @see org.cocktail.maracuja.client.TitreListePanel.ITitreListePanelListener#onTitreCheckedChanged()
		*/
	   public void onRecetteCheckedChanged() {
//		   System.out.print("Valeurs Cochees : ");
//		   Vector tmp =  myRecetteListePanel.checkedRecettes() ;
//		   for (int i = 0; i < tmp.size(); i++) {
//			   System.out.print(((EOEnterpriseObject)tmp.elementAt(i)).valueForKey("depNumero")  +", ");
//		   }
//		   System.out.println();
	   }

		/* (non-Javadoc)
		 * @see org.cocktail.maracuja.client.RecetteListePanel.IRecetteListePanelListener#getSelectedMndat()
		 */
		public EOTitre getSelectedTitre() {
			return myTitreListePanel.getSelectedTitre();
		}
	
		/* (non-Javadoc)
		 * @see org.cocktail.maracuja.client.RecetteListePanel.IRecetteListePanelListener#onRecetteSelectionChanged()
		 */
		public void onRecetteSelectionChanged() {
			
		}

		/* (non-Javadoc)
		 * @see org.cocktail.maracuja.client.RecetteListePanel.IRecetteListePanelListener#getAllCheckedRecettes()
		 */
		public HashMap getAllCheckedRecettes() {			
			return dicoOfRecettes;
		}
	 	
	}	
	
	
	private class RecapBordereauInfos extends JPanel {
		private Vector fields;
		private ZBordereauFieldsModel myZEOLabelTextFieldProvider;
		
		
		
		
		
		
		public RecapBordereauInfos() {
			super();
			myZEOLabelTextFieldProvider = new ZBordereauFieldsModel();
		}
		
		
		public void initGUI() {
			//Mettre une marge
			this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
			this.setLayout(new BorderLayout());
			Box boxForm = Box.createVerticalBox();			
			Box boxLine1 = Box.createHorizontalBox();
			
			ZLabelTextField f1 = new ZEOLabelTextField("N°", EOBordereau.BOR_NUM_KEY,myZEOLabelTextFieldProvider);
			f1.getMyTexfield().setEditable(false);
			f1.getMyTexfield().setColumns(6);
			f1.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
			f1.getMyTexfield().setBackground(Color.WHITE);	
			
			
			ZLabelTextField f2 = new ZEOLabelTextField("Code gestion", "gestion.gesCode", myZEOLabelTextFieldProvider);
			f2.getMyTexfield().setEditable(false);
			f2.getMyTexfield().setColumns(4);
			f2.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
			f2.getMyTexfield().setBorder(BorderFactory.createEmptyBorder());
			f2.getMyTexfield().setBackground(Color.WHITE);				
            ZFormPanel f4 = ZFormPanel.buildLabelField("Date de réception", new ZDatePickerField( new ZFieldDateVisaProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16))) ;

			
			fields = new Vector(4,0);
			fields.add(f1);
			fields.add(f2);
//			fields.add(f3);
			fields.add(f4);
			
			
			
			boxLine1.add(f1);
			boxLine1.add(Box.createRigidArea(new Dimension(2,2)));
			boxLine1.add(f2);
//			boxLine1.add(Box.createRigidArea(new Dimension(2,2)));
//			boxLine1.add(f3);
			boxLine1.add(Box.createRigidArea(new Dimension(2,2)));
			boxLine1.add(f4);
			boxLine1.add(Box.createHorizontalGlue());
			
//			box0.add(Box.createRigidArea(new Dimension(1,50))); //on fixe la hauteur au niveau supérieur
			boxForm.add(boxLine1);
			boxForm.add(Box.createVerticalGlue());
			this.add(boxForm, BorderLayout.LINE_START);
			this.add(Box.createGlue(), BorderLayout.CENTER);
			
//			this.setPreferredSize(new Dimension(1,55));
		}

		


		/**
		 * Met à jour l'affichage des champs du panel. 
		 */
		public void updateData() throws Exception  {
            Iterator iter = fields.iterator();
            while (iter.hasNext()) {
                IZDataComponent c = (IZDataComponent) iter.next();
                c.updateData();
//                Component c = (Component) iter.next();
//                if (c instanceof ZLabelTextField) {
//                    ((ZLabelTextField)iter.next()).updateData();
//                }
                
            }
		}

		
		
		private class ZBordereauFieldsModel implements ZEOLabelTextField.IZEOLabelTextFieldModel {
			/**
			 * @see org.cocktail.zutil.client.wo.ZEOLabelTextField.IZEOLabelTextFieldModel#getEo()
			 */
			public EOEnterpriseObject getEo() {				
				return myListener.getSelectedBordereau();
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {				
				return null;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				return;				
			}
		}
		
//		
//		/**
//		 * Classe quyi fournit les données au champ total
//		 */
//		private class ZFieldTotalProvider implements ZLabelTextField.IZLabelTextFieldModel {
//			/* (non-Javadoc)
//			 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldProvider#getValue()
//			 */
//			public Object getValue() {
////				System.out.println("Update total ");
//				Iterator iter = getTitresAViser().iterator();
//				BigDecimal total = new BigDecimal(0);
//				while (iter.hasNext()) {
////					System.out.println("Update total iter1");
//					EOTitre tmp = (EOTitre)iter.next();
//					total = total.add( (BigDecimal) tmp.titHt()  );
//				}
////				System.out.println("Update total res = " + total);
//				return total;
//			}
//			
//			/**
//			 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
//			 */
//			public void setValue(Object value) {
//				return;
//			}
//		}
		
		private class ZFieldDateVisaProvider implements IZDatePickerFieldModel {
			/* (non-Javadoc)
			 * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldProvider#getValue()
			 */
			public Object getValue() {
				return getMyListener().getDateReception() ;
			}
			
			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				if ((value instanceof Date)) {
					getMyListener().setDateReception((Date)value)	;
				}
			}

            public Window getParentWindow() {
                return getMyDialog();
            }
		}		
	}
	
	
	
	

	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#isActionButton1Enabled()
	 */
	public boolean isActionButton1Enabled() {
		return false;
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#isActionButton1Visible()
	 */
	public boolean isActionButton1Visible() {
		return false;
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#specialAction1()
	 */
	public AbstractAction specialAction1() {
		return myListener.specialAction1();
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#onDisplay()
	 */
	public void onDisplay() {
		specialAction1().setEnabled(true);
	}


	
	private class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  ZEOUtilities.calcSommeOfBigDecimals(getTitresAViser(), "titTtc")  ;
		}

		public void setValue(Object value) {
			return;
		}
	}
	
	
	private class TotalModel2 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  ZEOUtilities.calcSommeOfBigDecimals(getTitresARejeter(), "titTtc")  ;
		}

		public void setValue(Object value) {
			return;
		}
	}	
	
	private class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  new Integer(getTitresAViser().size());
		}

		public void setValue(Object value) {
			return;
		}
	}	
	private class NbModel2 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  new Integer(getTitresARejeter().size());
		}

		public void setValue(Object value) {
			return;
		}
	}


}
