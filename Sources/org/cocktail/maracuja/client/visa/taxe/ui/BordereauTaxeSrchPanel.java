/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa.taxe.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.BordereauBrouillardListPanel;
import org.cocktail.maracuja.client.common.ui.BordereauListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BordereauTaxeSrchPanel extends ZKarukeraPanel {
    private IBordereauTaxeSrchPanelListener myListener;

    private BordereauTaxeListPanel bordereauListPanel;
    private BordereauTaxeBrouillardListPanel bordereauBrouillardListPanel;

    private ZFormPanel codeGestion;
    private ZFormPanel borNum;
    private ZFormPanel comboBorEtat;
    
    


    /**
     * @param editingContext
     */
    public BordereauTaxeSrchPanel(IBordereauTaxeSrchPanelListener listener) {
        super();
        myListener = listener;

        bordereauListPanel = new BordereauTaxeListPanel(new BordereauTaxeListPanelListener());
        bordereauBrouillardListPanel = new BordereauTaxeBrouillardListPanel(new BordereauTaxeBrouillardListPanelListener());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
//        filterPanel.setMyDialog(getMyDialog());
//        filterPanel.initGUI();
        bordereauListPanel.initGUI();
        bordereauBrouillardListPanel.initGUI();

        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
        JPanel tmp = new JPanel(new BorderLayout());
//        tmp.add(encloseInPanelWithTitle("Filtres de recherche",null,null,null,filterPanel,null,null), BorderLayout.NORTH);
        tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
                encloseInPanelWithTitle("Bordereaux",null,ZConst.BG_COLOR_TITLE,bordereauListPanel,null, null),
                encloseInPanelWithTitle("Brouillards",null,ZConst.BG_COLOR_TITLE,bordereauBrouillardListPanel,null, null)) , BorderLayout.CENTER);

        tmp.add(buildFilterPanel(), BorderLayout.NORTH);
        this.add(buildRightPanel(), BorderLayout.EAST);
        this.add(tmp, BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);
        
        
    }

    
    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionClose());        
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    } 
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
//        filterPanel.updateData();
        bordereauListPanel.updateData();
        bordereauBrouillardListPanel.updateData();
    }

    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(50,10,15,10));
        ArrayList list = new ArrayList();
        list.add(myListener.getActionViser());
        list.add(myListener.getActionRejeter());

        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }


    private final JPanel buildFilterPanel() {
        codeGestion = ZFormPanel.buildLabelField("Code gestion", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOGestion.GES_CODE_KEY) );
        borNum = ZFormPanel.buildLabelField("N° Bordereau", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "borNum") );
        comboBorEtat = ZFormPanel.buildLabelComboBoxField("Etat", myListener.getBordereauEtatModel(), myListener.getFilters(), "borEtat",null );  
        
        final Component[] comps = new Component[3];
        comps[0] = codeGestion;
        comps[1] = borNum;
        comps[2] = comboBorEtat;     
        
        
        
        final JPanel panel = new JPanel(new BorderLayout());
        
        panel.add(buildLine(comps), BorderLayout.CENTER);
        panel.add(new JButton(myListener.actionSrch() ), BorderLayout.EAST);
        
        return encloseInPanelWithTitle( "Filtres de recherche",null,ZConst.BG_COLOR_TITLE,panel,null, null);
        
    }
    
    



    /**
     * @return L'objetactuelment sélectionné.
     */
    public EOBordereau getSelectedBordereau() {
        return (EOBordereau) bordereauListPanel.selectedObject();
    }







    public interface IBordereauTaxeSrchPanelListener {

        public Action getActionRejeter();
        public ComboBoxModel getBordereauEtatModel();
        public Action actionClose();
        public Action getActionViser();


        /**
         * @return un dictioniare contenant les filtres
         */
        public HashMap getFilters();
        public Action actionSrch();
        public void onBordereauSelectionChanged();
        public NSArray getBordereaux();
        public NSArray getBordereauxBrouillards();
//        public ZEOComboBoxModel getTypesBordereauxComboboxModel();
    }






    private final class BordereauTaxeListPanel extends BordereauListPanel {

        /**
         * @param listener
         */
        public BordereauTaxeListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);
            colsMap.remove(COL_UTILISATEUR);
        }

    }

    private final class BordereauTaxeBrouillardListPanel extends BordereauBrouillardListPanel {

        /**
         * @param listener
         */
        public BordereauTaxeBrouillardListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);
            colsMap.remove(COL_LIBELLE3);
            colsMap.remove(COL_OPERATION);
        }

    }


    private final class BordereauTaxeListPanelListener implements IZKarukeraTablePanelListener {

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
         */
        public void selectionChanged() {
            myListener.onBordereauSelectionChanged();

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
         */
        public NSArray getData() {
            return myListener.getBordereaux();
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
         */
        public void onDbClick() {
            return;
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
         */
        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }

    }


    private final class BordereauTaxeBrouillardListPanelListener implements IZKarukeraTablePanelListener {

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
         */
        public void selectionChanged() {
            return;
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
         */
        public NSArray getData() {
            return myListener.getBordereauxBrouillards();
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
         */
        public void onDbClick() {
            return;
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
         */
        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }

    }

//    private final class BordereauTaxeSrchFilterPanelListener implements BordereauTaxeSrchFilterPanel.IBordereauTaxeSrchFilterPanel {
//
//        /**
//         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchFilterPanel.IBordereauTaxeSrchFilterPanel#getActionSrch()
//         */
//        public Action getActionSrch() {
//            return myListener.actionSrch();
//        }
//
//        /**
//         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchFilterPanel.IBordereauTaxeSrchFilterPanel#getFilters()
//         */
//        public HashMap getFilters() {
//            return myListener.getFilters();
//        }
//
////        /**
////         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchFilterPanel.IBordereauTaxeSrchFilterPanel#getTypesBordereauxModel()
////         */
////        public ZEOComboBoxModel getTypesBordereauxModel() {
////            return myListener.getTypesBordereauxComboboxModel();
////        }
////
//    }

    /**
     * @throws Exception
     *
     */
    public void updateBrouillardList() throws Exception {
        bordereauBrouillardListPanel.updateData();

    }



}
