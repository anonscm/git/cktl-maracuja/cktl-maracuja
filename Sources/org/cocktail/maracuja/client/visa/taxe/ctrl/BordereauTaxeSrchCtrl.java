/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa.taxe.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessBordereauBrouillardGenerique;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.visa.taxe.ui.BordereauTaxeSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BordereauTaxeSrchCtrl extends CommonCtrl {
    private static final Dimension WINDOW_DIMENSION = new Dimension(970,700);

    private static final String TITLE = "Gestion des bordereaux de Taxes d'apprentissage";

    private final String ACTION_ID_VISA = ZActionCtrl.IDU_VITA;

    private BordereauTaxeSrchPanel panel;


    private final ActionSrch actionSrch = new ActionSrch();
    private final ActionViser actionViser = new ActionViser();
    private final ActionRejeter actionRejeter = new ActionRejeter();
    private final ActionClose actionClose = new ActionClose();

    private HashMap myFilters;


    private NSArray comptabilites;
    private NSArray gestionsforVisa;

    private final EOTypeBordereau typeBord;
    private final String typeBordType;
    private final EOTypeJournal typeJournal;
    
    private final DefaultComboBoxModel comboBorEtatModel; 
    
//    private ZEOComboBoxModel typeBordereauxScolModel;


    /**
     * @param editingContext
     * @throws Exception
     */
    public BordereauTaxeSrchCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        revertChanges();
        myFilters = new HashMap();
        
        typeBordType = EOTypeBordereau.TYPEBORDEREAU_TAXE_APPRENTISSAGE;
        typeBord = EOsFinder.getLeTypeBordereau(getEditingContext(), typeBordType) ;
        typeJournal = EOsFinder.getLeTypeJournal(getEditingContext(), EOTypeJournal.typeJournalVisaTaxe);
        
        if (typeJournal==null) {
            throw new DefaultClientException("Le Type journal "+EOTypeJournal.typeJournalVisaTaxe + " n'a pas été retrouvé dans la base de données.");
        }
        
        gestionsforVisa = EOsFinder.getAllGestionsPourExercice(getEditingContext(), getExercice());
        
//        gestionsforVisa = new NSArray();
////      origineModel = new ZEOComboBoxModel(new NSArray(),"oriLibelle","",null);
//      if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_VISA )) {
//          gestionsforVisa = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_VISA);
//      }
//
//      if (gestionsforVisa.count()==0) {
//          throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour accéder au visa de ces bordereaux. Demandez à votre administrateur " +
//                "de vous affecter les codes de gestions pour cette fonction");
//      }

      
        comboBorEtatModel = new DefaultComboBoxModel();
//        comboBorEtatModel.addElement(EOBordereau.BordereauAViser);
        comboBorEtatModel.addElement(EOBordereau.BordereauValide);
        comboBorEtatModel.addElement(EOBordereau.BordereauVise);
        
        
        panel = new BordereauTaxeSrchPanel(new BordereauTaxeSrchPanelListener());
      
        
    }





    public final NSArray getBordereaux() {
        //Créer la condition à partir des filtres
       try {
           EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("borNum", EOSortOrdering.CompareDescending);
           EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareDescending);
           NSArray res = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)),null, true);

           return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[]{sort2,sort1}));

	    } catch (Exception e) {
	        showErrorDialog(e);
	        return new NSArray();
	    }
    }



    public final EOBordereau getSelectedBordereau() {
        return (EOBordereau) panel.getSelectedBordereau();
    }


//
//    private final void onScolBordereauSelectionChanged() {
//        try {
//            scolBordereauAdmSrchPanel.updateBrouillardList();
//            refreshActions();
//        } catch (Exception e) {
//           showErrorDialog(e);
//        }
//    }

    /**
     * @see org.cocktail.maracuja.client.odp.ui.ScolBordereauRechercheFilterPanel.IScolBordereauRechercheFilterPanel#onSrch()
     */
    private final void onSrch() {
        try {
            panel.updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }


    private final NSArray bordereauxBrouillards() {
        if (panel.getSelectedBordereau()==null) {
            return new NSArray();
        }
        return panel.getSelectedBordereau().bordereauBrouillards();
    }






    protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

        NSMutableArray quals = new NSMutableArray();
        quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[]{myApp.appUserInfo().getCurrentExercice()})));
        quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeBordereau=%@", new NSArray(new Object[]{ typeBord  })));
//        quals.addObject(EOQualifier.qualifierWithQualifierFormat("borEtat=%@", new NSArray(new Object[]{ EOBordereau.BordereauValide  })));

        NSMutableArray lesgestions = new NSMutableArray();
        for (int i = 0; i < gestionsforVisa.count(); i++) {
            EOGestion element = (EOGestion) gestionsforVisa.objectAtIndex(i);
            lesgestions.addObject( EOQualifier.qualifierWithQualifierFormat("gestion=%@", new NSArray(new Object[]{ element }))  );
        }
        if (lesgestions.count()>0) {
            quals.addObject(new EOOrQualifier(lesgestions));
        }
        
        if (dicoFiltre.get(EOGestion.GES_CODE_KEY)!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.gesCode=%@", new NSArray( dicoFiltre.get(EOGestion.GES_CODE_KEY))));
        }

        if (dicoFiltre.get("borNum")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("borNum=%@", new NSArray( new Integer((String)dicoFiltre.get("borNum")))));
        }        
        
        if (dicoFiltre.get("borEtat")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("borEtat=%@", new NSArray( (String)dicoFiltre.get("borEtat"))));
        }        
        
        

        return quals;
    }




//	private final NSArray getChequesValidesOrVisesForBordereau(final EOBordereau bordereau) {
//        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("bobEtat=%@ or bobEtat=%@ ", new NSArray(new Object[]{EOBordereauBrouillard.bordereauBrouillardValide, EOBordereauBrouillard.bordereauBrouillardVise}));
//        return  EOQualifier.filteredArrayWithQualifier(bordereau.cheques(),qual);
//	}




    /**
     * Active/desactive les actions en fonction de l'ecriture selectionnee.
     */
    private final void refreshActions() {
        if (getSelectedBordereau()==null) {
            actionViser.setEnabled(false);
            actionRejeter.setEnabled(false);
        }
        else {
            actionRejeter.setEnabled(true && (gestionsforVisa.count()>0) && (EOBordereau.BordereauValide.equals(getSelectedBordereau().borEtat())));
            actionViser.setEnabled(true && (gestionsforVisa.count()>0) && (EOBordereau.BordereauValide.equals(getSelectedBordereau().borEtat())));
        }
    }






    private final void bordereauViser() {
        //Vérifier si l'utilisateur a bien les droits
        try {
            EOBordereau scolBordereau = getSelectedBordereau();
            if (scolBordereau!=null) {
                EOGestion gestion = scolBordereau.gestion();

                if (gestionsforVisa.indexOfObject( gestion ) == NSArray.NotFound) {
                    throw new DefaultClientException("Vous n'avez pas les droits de viser un bordereau pour le code de gestion " + gestion.gesCode());
                }

                if (!typeBordType.equals(scolBordereau.typeBordereau().tboType())) {
                    throw new DefaultClientException("Le bordereau n'est pas du type " + typeBordType);
                }



                if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement viser le bordereau n°" + scolBordereau.borNum() +" ?", ZMsgPanel.BTLABEL_NO)) {

                    if (!EOBordereau.BordereauValide.equals( scolBordereau.borEtat())) {
                        throw new DefaultClientException("Le bordereau doit être à l'état "+ EOBordereau.BordereauValide + " pour être visé.");
                    }


//
                    EOComptabilite compta = scolBordereau.gestion().comptabilite();
                    EOTypeOperation typeOperation = EOsFinder.getLeTypeOperation(getEditingContext(), EOTypeOperation.TYPE_OPERATION_GENERIQUE);
                    EOOrigine origine = null;
                    

                    FactoryProcessBordereauBrouillardGenerique  factoryProcessBordereau = new FactoryProcessBordereauBrouillardGenerique(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
                    FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
                    EOEcriture ecriture;
                    try {
                        Date dateVisa=ZDateUtil.getDateOnly(ZDateUtil.getTodayAsCalendar().getTime());
//                        //On force la date des écritures des bordereaux d'inscription (on prend la date du bordereau).
//                        //Le n° de bordereau dans le cas des bordereaux d'inscription est la date a l'envers
//                        String bornum = ZConst.FORMAT_ENTIER_BRUT.format(scolBordereau.borNum());
//
//                        dateVisa = (Date) ZConst.FORMAT_DATE_YYYYMMDD.parseObject(bornum);

                        ecriture = factoryProcessBordereau.viserUnBordereau(getEditingContext(), scolBordereau, compta, myApp.appUserInfo().getCurrentExercice(), origine, typeJournal, typeOperation,myApp.appUserInfo().getUtilisateur(), new NSTimestamp(dateVisa));
//                        lesEcritures = factoryProcessBordereauDeCheques.viserUnBordereau(getEditingContext(), scolBordereau, getUtilisateur() );
                    }
                    catch (Exception e) {
                        getEditingContext().revert();
                        throw e;
                    }
//
//
                    getEditingContext().saveChanges();
                    panel.updateData();
                    String msgFin = "";
                    msgFin = msgFin + "Le bordereau n° " + scolBordereau.borNum() +" a été visé.\n";
//
//                    //Numéroter
                    try {
                        KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
                        NSMutableArray nbs = new NSMutableArray();
                        if ((ecriture!=null) ) {
                                factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), ecriture, myKFactoryNumerotation);
                                nbs.addObject(ecriture.ecrNumero());
                        }
                        if (nbs.count()>0) {
                            msgFin = msgFin + "\nLes écritures ont été numérotées : " + nbs.componentsJoinedByString(",");
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        msgFin = msgFin + "\nErreur lors de la numérotation des écritures : \n";
                        msgFin = msgFin + e.getMessage();
                    }
                    myApp.showInfoDialog(msgFin);
                }


            }
        } catch (Exception e) {
            getEditingContext().revert();
            showErrorDialog(e);
        }

    }

    private final void bordereauRejeter() {
        //Vérifier si l'utilisateur a bien les droits
        try {
            EOBordereau bordereau = getSelectedBordereau();
            if (bordereau!=null) {
                EOGestion gestion = bordereau.gestion();

                if (gestionsforVisa.indexOfObject( gestion ) == NSArray.NotFound) {
                    throw new DefaultClientException("Vous n'avez pas les droits de viser/rejeter un bordereau pour le code de gestion " + gestion.gesCode());
                }

                if (!typeBordType.equals(bordereau.typeBordereau().tboType())) {
                    throw new DefaultClientException("Le bordereau n'est pas du type " + typeBordType);
                }

                if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement rejeter le bordereau n°" + bordereau.borNum() +" ?", ZMsgPanel.BTLABEL_NO)) {

                    if (!EOBordereau.BordereauValide.equals( bordereau.borEtat())) {
                        throw new DefaultClientException("Le bordereau doit être à l'état "+ EOBordereau.BordereauValide + " pour être rejeté.");
                    }

                    FactoryProcessBordereauBrouillardGenerique  factoryProcessBordereau = new FactoryProcessBordereauBrouillardGenerique(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
                    factoryProcessBordereau.rejeterUnBordereau(getEditingContext(), bordereau);
                }
                getEditingContext().saveChanges();
                panel.updateData();
            }




        } catch (Exception e) {
            getEditingContext().revert();
            showErrorDialog(e);
        }

    }





    public final void resetFilter() {
        myFilters.clear();
        myFilters.put("borEtat", EOBordereau.BordereauValide);
    }





    
    

    private final ZKarukeraDialog createModalDialog(Window dial ) {
        ZKarukeraDialog win;
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE,true);
        }
        panel.setMyDialog(win);
        panel.setPreferredSize(WINDOW_DIMENSION);
        panel.initGUI();
        win.setContentPane(panel);
        win.pack();
        return win;
    }    
    
   
    
    
    /**
     * Ouvre un dialog de recherche.
     */
    public final void openDialog(Window dial) {
        ZKarukeraDialog win = createModalDialog(dial);
        this.setMyDialog(win);
        try {
            resetFilter();
            onSrch();
            win.open();
        } catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
    }
        
    
    private final void fermer() {
        getMyDialog().onCloseClick();
    }
    
    



    private final class ActionSrch extends AbstractAction {
        public ActionSrch() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            onSrch();
        }
    }

    private final class ActionClose extends AbstractAction {

        public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
        

        public void actionPerformed(ActionEvent e) {
          fermer();
        }
        
    }  

    private final class ActionViser extends AbstractAction {
        public ActionViser() {
            super("Viser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Viser le bordereau");
			setEnabled(false);
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            bordereauViser();
        }
    }
    private final class ActionRejeter extends AbstractAction {
        public ActionRejeter() {
            super("Rejeter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Rejeter le bordereau");
			setEnabled(false);
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            bordereauRejeter();
        }
    }







    private final class BordereauTaxeSrchPanelListener implements BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener {


        /**
         * @see org.cocktail.maracuja.client.cheques.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#getBordereaux()
         */
        public NSArray getBordereaux() {
            return BordereauTaxeSrchCtrl.this.getBordereaux();
        }

        /**
         * @see org.cocktail.maracuja.client.cheques.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#getFilters()
         */
        public HashMap getFilters() {
            return myFilters;
        }

        /**
         * @see org.cocktail.maracuja.client.cheques.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#actionSrch()
         */
        public Action actionSrch() {
            return actionSrch;
        }



        /**
         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#getActionRejeter()
         */
        public Action getActionRejeter() {
            return actionRejeter;
        }

        /**
         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#getActionViser()
         */
        public Action getActionViser() {
            return actionViser;
        }

        /**
         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#onBordereauSelectionChanged()
         */
        public void onBordereauSelectionChanged() {
            try {
                refreshActions();
                panel.updateBrouillardList();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }

        /**
         * @see org.cocktail.maracuja.client.scol.ui.BordereauTaxeSrchPanel.IBordereauTaxeSrchPanelListener#getBordereauxBrouillards()
         */
        public NSArray getBordereauxBrouillards() {
            return bordereauxBrouillards();
        }

        public Action actionClose() {
            return actionClose;
        }

        public ComboBoxModel getBordereauEtatModel() {
            return comboBorEtatModel;
        }

    }


    public BordereauTaxeSrchPanel getBordereauTaxeSrchPanel() {
        return panel;
    }
    
    
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return panel;
    }

    public String title() {
        return TITLE;
    }    
    
}
