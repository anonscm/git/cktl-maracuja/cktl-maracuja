/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.Frame;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;


/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaMandatDialog extends ZKarukeraDialog implements VisaMandatPanel.ChangeStepListener {
	private VisaMandatPanel myPanel;

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws Exception
	 */
	public VisaMandatDialog(Frame owner, String title, boolean modal) throws Exception {
		super(owner, title, modal);
		//		revertChanges();
		myPanel = new VisaMandatPanel();
		initGUI();
	}

	private void initGUI() {

		myPanel.setMyDialog(this);
		myPanel.setMyChangeStepListener(this);
		myPanel.initGUI();
		updateGUI();
		updateData();
		this.pack(); // le pack doit etre fait une seule fois pour éviter le resize à chaque changement de step
		//		myPanel.updateData();//a deporter dans updategui

	}

	private void updateData() {
		try {
			myPanel.updateData();
		} catch (Exception e) {
			myApp.showErrorDialog(e, this);
			setVisible(false);
		}

	}

	private void updateGUI() {
		//		ZLogger.debug("ici");
		try {
			myPanel.updateGUI();
			this.setContentPane(myPanel);
		} catch (Exception e) {
			myApp.showErrorDialog(e, this);
			setVisible(false);
		}
		//		ZLogger.debug("ici");
	}

	/**
	 * Appelé par le panneau quand le panneau en cours doit changer - i.e la fenetre doit être redessinée(via interface)
	 */
	public void stepHasChanged() {
		updateGUI();
	}

	public void onCloseAsked() {
		setVisible(false);
	}

}
