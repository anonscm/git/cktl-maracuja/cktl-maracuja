/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTable;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.emargements.ctrl.FactoryEmargementProxy;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessEmargementVisa;
import org.cocktail.maracuja.client.finder.FinderEmargement;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeEmargement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZNumberUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementAutoCtrl extends CommonCtrl {
	private EmargementAutoPanel myPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionAccepter actionAccepter = new ActionAccepter();
	// private final ActionAccepterTous actionAccepterTous = new
	// ActionAccepterTous();
	private final ActionRefuser actionRefuser = new ActionRefuser();
	// private final ActionRefuserTous actionRefuserTous = new
	// ActionRefuserTous();

	private final CreditListListener creditListListener;
	private final DebitListListener debitListListener;
	private final DepenseListListener depenseListListener;

	private final EmargementAutoPanelListener emargementAutoPanelListener;

	private static final String TITLE = "Emargements ordonnateurs";
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	public static final Integer INTEGER_0 = new Integer(0);
	public static final Integer INTEGER_1 = new Integer(1);

	private final String DEBITS = "DEBITS";
	private final String CREDITS = "CREDITS";
	private final String ERREURMSG = "ERREURMSG";

	private final HashMap emargementsProposes = new HashMap();
	private final NSMutableArray depensesPourEmargement = new NSMutableArray();

	private final DepenseTableCellRenderer depenseTableCellRenderer = new DepenseTableCellRenderer();

	private EOComptabilite comptabilite;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public EmargementAutoCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		creditListListener = new CreditListListener();
		debitListListener = new DebitListListener();
		depenseListListener = new DepenseListListener();
		emargementAutoPanelListener = new EmargementAutoPanelListener();

		myPanel = new EmargementAutoPanel(emargementAutoPanelListener);

	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.EmargementRechercheFilterPanel.IEmargementRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			setWaitCursor(true);
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	public final void initData(final NSArray mandats) {
		emargementsProposes.clear();
		depensesPourEmargement.removeAllObjects();

		int i = 0;
		final int nb = mandats.count();
		while (i < nb) {
			final EOMandat mandat = (EOMandat) mandats.objectAtIndex(i);
			comptabilite = mandat.bordereau().gestion().comptabilite();
			final NSArray depenses = mandat.depenses();

			// Récupérer les ecritures en crédit générées lors du visa pour le
			// depense
			//			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("mdeOrigine=%@ and ecritureDetail.planComptable<>%@ and ecritureDetail.ecdSens=%@ and ecritureDetail.ecdResteEmarger>0",
			//					new NSArray(new Object[] {
			//							EOMandatDetailEcriture.MDE_ORIGINE_VISA, mandat.planComptable(), EOEcritureDetail.SENS_CREDIT
			//					}));
			//			
			//			final NSArray depenseDetailEcritures = EOQualifier.filteredArrayWithQualifier(mandat.mandatDetailEcritures(), qual);
			//			final NSMutableArray depenseCredits = new NSMutableArray();
			//			for (int k = 0; k < depenseDetailEcritures.count(); k++) {
			//				depenseCredits.addObject(((EOMandatDetailEcriture) depenseDetailEcritures.objectAtIndex(k)).ecritureDetail());
			//			}

			NSArray depenseCredits = mandat.ecritureDetailsVisaContrepartie();

			for (int j = 0; j < depenses.count(); j++) {
				final NSMutableArray depenseDebits = new NSMutableArray();
				final EODepense depense = (EODepense) depenses.objectAtIndex(j);
				if (depense.ecritureDetailEma() != null) {
					depenseDebits.addObject(depense.ecritureDetailEma());
				}

				System.out.println("EmargementAutoCtrl.initData()");
				System.out.println(depenseDebits);
				System.out.println(depenseCredits);

				// si des debits ont été spécifiés, on ajoute le depense dans la
				// liste
				if (depenseDebits.count() != 0 && depenseCredits.count() > 0) {

					depensesPourEmargement.addObject(depense);
					final HashMap debitsCredit = new HashMap(2);
					debitsCredit.put(DEBITS, depenseDebits);
					debitsCredit.put(CREDITS, depenseCredits);
					debitsCredit.put(ERREURMSG, null);

					// Detecter les erreurs
					// On verifie que les ecritures ont la même imputation
					final EOPlanComptable planComptable = ((EOEcritureDetail) depenseDebits.objectAtIndex(0)).planComptable();
					final EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat("planComptable<>%@", new NSArray(new Object[] {
							planComptable
					}));

					if (EOQualifier.filteredArrayWithQualifier(depenseDebits, qual2).count() != 0) {
						debitsCredit.put(ERREURMSG, ZStringUtil.concatStr((String) debitsCredit.get(ERREURMSG), "Mandat n° " + mandat.gestion().gesCode() + "/" + mandat.manNumero() + ", Facture n°"
								+ depense.depNumero() + " : Les débits sont faits sur des comptes différents.", "\n"));
					}

					if (EOQualifier.filteredArrayWithQualifier(depenseCredits, qual2).count() != 0) {
						debitsCredit.put(ERREURMSG, ZStringUtil.concatStr((String) debitsCredit.get(ERREURMSG), "Mandat n° " + mandat.gestion().gesCode() + "/" + mandat.manNumero() + ", Facture n°"
								+ depense.depNumero() + " :  Les crédits sont faits sur des comptes différents.", "\n"));
					}

					// //On verifie qu'on a le même reste a émarger des deux
					// cotes
					// final BigDecimal resteEmargerDebits =
					// ZEOUtilities.calcSommeOfBigDecimals(depenseDebits,
					// "ecdResteEmarger");
					// final BigDecimal resteEmargerCredits =
					// ZEOUtilities.calcSommeOfBigDecimals(depenseCredits,
					// "ecdResteEmarger");
					//                    
					// if (!resteEmargerDebits.equals(resteEmargerCredits)) {
					// debitsCredit.put(ERREURMSG,ZStringUtil.concatStr((String)debitsCredit.get(ERREURMSG),
					// "Depense n° "+
					// depense.gestion().gesCode()+"/"+depense.manNumero() +" :
					// Le reste à émarger pour les débits est différent du reste
					// à émarger des crédits", "\n"));
					// }
					//                    
					emargementsProposes.put(depense, debitsCredit);

				}
			}
			i++;
		}

	}

	private class FactoryEmargementSemiAuto extends FactoryProcessEmargementVisa {

		public FactoryEmargementSemiAuto(boolean withLog, final NSTimestamp dateJourComptable) {
			super(withLog, dateJourComptable);
		}

		/**
		 * Renvoi un tableau
		 * 
		 * @param ed
		 * @param utilisateur
		 * @param typeEmargement
		 * @param exercice
		 * @param depenses
		 * @param ecritureDetailCredit
		 * @return
		 * @throws Exception
		 */
		public final NSMutableArray emargerDepenseSemiAuto(final EOEditingContext ed, final EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, final EODepense depense,
				final EOEcritureDetail ecritureDetailCredit, final NSMutableArray lesEcrituresGenerees) throws Exception {
			// final HashMap montantEmargementParDepense = new HashMap();
			// final FactoryEmargementDetail factoryEmargementDetail = new
			// FactoryEmargementDetail(withLogs());
			// final NSMutableArray lesEmargements = new NSMutableArray();
			// EOEmargement emargement = null;
			NSMutableArray lesEmargementsGenerees = null;
			final EODepense element = depense;
			final EOEcritureDetail ecritureDetailDebit = element.ecritureDetailEma();

			if (ecritureDetailDebit != null) {
				// Vérifier l'imputation
				if (!ecritureDetailDebit.planComptable().equals(ecritureDetailCredit.planComptable())) {
					throw new FactoryException("L'imputation entre le debit et le credit n'est pas la meme.");
				}

				/*
				 * // verifier qu'on n'est pas entre un sacd et une composante final NSArray sacds = EOsFinder.getSACDGestion(getEditingContext(),
				 * exercice); final EOGestion agence = comptabilite.gestion();
				 * 
				 * 
				 * //si debit = composante if (!ecritureDetailDebit.gestion().equals(agence) &&
				 * sacds.indexOfObject(ecritureDetailDebit.gestion())==NSArray.NotFound) { //si credit=sacd if
				 * (sacds.indexOfObject(ecritureDetailCredit.gestion())!=NSArray.NotFound) { throw new
				 * FactoryException("L'émargement automatique entre une composante et un SACD n'est pas pris en charge."); } }
				 * 
				 * if (!ecritureDetailCredit.gestion().equals(agence) && sacds.indexOfObject(ecritureDetailCredit.gestion())==NSArray.NotFound) { if
				 * (sacds.indexOfObject(ecritureDetailDebit.gestion())!=NSArray.NotFound) { throw new
				 * FactoryException("L'émargement automatique entre une composante et un SACD n'est pas pris en charge."); } }
				 */

				//                
				//                
				//                if ((!ecritureDetailDebit.gestion().equals(agence) && sacds.indexOfObject(ecritureDetailCredit.gestion())!=NSArray.NotFound) || (!ecritureDetailCredit.gestion().equals(agence) && sacds.indexOfObject(ecritureDetailDebit.gestion())!=NSArray.NotFound)) {
				//                    throw new FactoryException("L'émargement automatique entre une composante et un SACD n'est pas pris en charge.");
				//                }

				//
				if (ecritureDetailDebit.ecdResteEmarger().doubleValue() <= 0) {
					throw new FactoryException("Le reste à émarger du débit est nul, impossible d'émarger.");
				}

				// On prend le montant le plus petit entre le montant de la
				// facture et le reste à émarger de l'écriture debit associée
				final BigDecimal montantEmargementDetail = ZNumberUtil.plusPetitValeurAbsolue(element.depTtc(), ecritureDetailDebit.ecdResteEmarger());
				trace("Montant emargement : " + montantEmargementDetail);

				if (montantEmargementDetail != null && montantEmargementDetail.doubleValue() > 0) {
					// creer l emargement
					// emargement = emargerEcritureDetailD1C1(ed, utilisateur,
					// typeEmargement, exercice,ecritureDetailCredit,
					// ecritureDetailDebit ,new Integer(-1),comptabilite,
					// montantEmargementDetail);
					//                    lesEmargementsGenerees = emargementMemeCodeGestionD1C1(ed, utilisateur, typeEmargement, exercice, ecritureDetailCredit, ecritureDetailDebit, new Integer(-1), comptabilite,
					//                            montantEmargementDetail, lesEcrituresGenerees);
					lesEmargementsGenerees = emargementAuVisa(ed, utilisateur, typeEmargement, exercice, ecritureDetailCredit, ecritureDetailDebit, new Integer(-1), comptabilite,
							montantEmargementDetail, lesEcrituresGenerees);

				}
			}
			return lesEmargementsGenerees;
		}

	}

	private void checkClose() throws Exception {
		// Vérifiez si tous les depenses sont traités
		if (depensesPourEmargement.count() > 0) {
			throw new Exception("");
		}
	}

	private final void fermer() {
		if (depensesPourEmargement.count() > 0) {
			if (!showConfirmationDialog("Confirmation", "Tous les depenses ne sont pas traités. Si vous fermez cette fenêtre,"
					+ " vous ne pourrez plus accepter les émargements proposés, vous devrez les faire manuellement.\n" + "Voulez-vous quand même fermer la fenêtre ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		getMyDialog().onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		this.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		final ZKarukeraDialog win2 = createModalDialog(dial);

		try {
			myPanel.updateData();
			win2.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win2.dispose();
		}
	}

	private final void accepterEmargement() {
		final NSArray depenses = getSelectedDepenses();
		final String lesDepensesNum = ZEOUtilities.getCommaSeparatedListOfValues(depenses, "depNumero");
		if (depenses != null && depenses.count() > 0) {

			if (depenses.count() == 1) {
				if (!showConfirmationDialog("Confirmation", "Voulez-vous créer un émargement pour les écritures associées à la dépense sélectionnée " + lesDepensesNum + " ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}
			else {
				if (!showConfirmationDialog("Confirmation", "Voulez-vous créer un émargement global pour les écritures associées aux depenses n°" + lesDepensesNum + " ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}

			try {
				EODepense depense = (EODepense) depenses.objectAtIndex(0);
				//                final NSArray emargements = creerEmargementsSemiAuto((EODepense) depenses.objectAtIndex(0));

				this.revertChanges();
				// final NSArray lesDebits = getDebitsForDepenses(depenses);
				// final NSArray lesCredits = getCreditsForDepenses(depenses);
				final FactoryEmargementProxy tmpFactoryEmargementProxy2 = new FactoryEmargementProxy(getEditingContext(), myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(),
						FinderEmargement.leTypeLettrage(getEditingContext()), myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				//
				// int modeEmargement =
				// tmpFactoryEmargementProxy2.getModeEmargement(lesDebits, lesCredits);

				final FactoryEmargementSemiAuto factoryEmargementSemiAuto = new FactoryEmargementSemiAuto(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				final NSMutableArray lesEcrituresGenerees = new NSMutableArray();

				final EOEcritureDetail ecritureDetailCredit = getCreditForDepense(depense);
				final NSArray newEmargements = factoryEmargementSemiAuto.emargerDepenseSemiAuto(getEditingContext(), getUtilisateur(), FinderEmargement.leTypeLettrageAutomatique(getEditingContext()),
						getExercice(), depense, ecritureDetailCredit, lesEcrituresGenerees);

				if (newEmargements == null || newEmargements.count() == 0) {
					throw new DefaultClientException("Aucun émargement créé.");
				}

				ZLogger.debug(newEmargements);
				// reactiver le savechanges
				getEditingContext().saveChanges();

				emargementsProposes.remove(depense);
				depensesPourEmargement.removeObject(depense);

				// emargementsCrees.addObject(newEmargement);

				// On numerote
				// KFactoryNumerotation myKFactoryNumerotation = new
				// KFactoryNumerotation(myApp.wantShowTrace());

				try {
					if (lesEcrituresGenerees != null) {
						for (int i = 0; i < lesEcrituresGenerees.count(); i++) {
							final EOEcriture element = (EOEcriture) lesEcrituresGenerees.objectAtIndex(i);
							final KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
							myKFactoryNumerotation.getNumeroEOEcriture(getEditingContext(), element);
							//                myKFactoryNumerotation.(getEditingContext(), element, myKFactoryNumerotation);
						}
					}

				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION ECRITURES...");
					e.printStackTrace();
					throw new Exception(e);
				}
				try {
					if (newEmargements != null) {
						for (int i = 0; i < newEmargements.count(); i++) {
							final EOEmargement element = (EOEmargement) newEmargements.objectAtIndex(i);
							tmpFactoryEmargementProxy2.numeroterEmargement(getEditingContext(), element);
						}

					}
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
					e.printStackTrace();

					throw new Exception(e);
				}

				String msg = "";

				msg += "Les émargements suivant ont été créés : " + ZEOUtilities.getCommaSeparatedListOfValues(newEmargements, "emaNumero") + "\n";

				if (lesEcrituresGenerees != null && lesEcrituresGenerees.count() > 0) {
					msg += "Les écritures suivantes ont été créés : " + ZEOUtilities.getCommaSeparatedListOfValues(lesEcrituresGenerees, "ecrNumero") + "\n";
				}

				showInfoDialog(msg);

			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				revertChanges();
				try {
					myPanel.updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	private final void refuserEmargement() {
		final NSArray depenses = getSelectedDepenses();
		final String lesDepensesNum = ZEOUtilities.getCommaSeparatedListOfValues(depenses, "depNumero");
		if (depenses != null && depenses.count() > 0) {

			if (depenses.count() == 1) {
				if (!showConfirmationDialog("Confirmation", "Voulez-vous refuser l'émargement pour les écritures associées à la dépense n°" + lesDepensesNum + " ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}
			else {
				if (!showConfirmationDialog("Confirmation", "Voulez-vous refuser les émargements pour les écritures associées aux depenses n°" + lesDepensesNum + " ?", ZMsgPanel.BTLABEL_YES)) {
					return;
				}
			}

			try {
				for (int i = 0; i < depenses.count(); i++) {
					final EODepense element = (EODepense) depenses.objectAtIndex(i);
					annulerEmargementSemiAuto(element);
				}

				myPanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

	}

	private final void annulerEmargementSemiAuto(final EODepense element) throws Exception {
		emargementsProposes.remove(element);
		depensesPourEmargement.removeObject(element);
	}

	//    private final NSArray creerEmargementsSemiAuto(EODepense depense) throws Exception {
	//        this.revertChanges();
	//        // final NSArray lesDebits = getDebitsForDepenses(depenses);
	//        // final NSArray lesCredits = getCreditsForDepenses(depenses);
	//        final FactoryEmargementProxy tmpFactoryEmargementProxy2 = new FactoryEmargementProxy(getEditingContext(), myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(),
	//                FinderEmargement.leTypeLettrage(getEditingContext()), myApp.wantShowTrace());
	//        //
	//        // int modeEmargement =
	//        // tmpFactoryEmargementProxy2.getModeEmargement(lesDebits, lesCredits);
	//
	//        final FactoryEmargementSemiAuto factoryEmargementSemiAuto = new FactoryEmargementSemiAuto(myApp.wantShowTrace());
	//
	//        final NSMutableArray lesEcrituresGenerees = new NSMutableArray();
	//
	//        final EOEcritureDetail ecritureDetailCredit = getCreditForDepense(depense);
	//        final NSArray newEmargements = factoryEmargementSemiAuto.emargerDepenseSemiAuto(getEditingContext(), getUtilisateur(), FinderEmargement.leTypeLettrageAutomatique(getEditingContext()),
	//                getExercice(), depense, ecritureDetailCredit, lesEcrituresGenerees);
	//
	//        ZLogger.debug(newEmargements);
	//        // reactiver le savechanges
	//        getEditingContext().saveChanges();
	//
	//        emargementsProposes.remove(depense);
	//        depensesPourEmargement.removeObject(depense);
	//
	//        // emargementsCrees.addObject(newEmargement);
	//
	//        // On numerote
	//        // KFactoryNumerotation myKFactoryNumerotation = new
	//        // KFactoryNumerotation(myApp.wantShowTrace());
	//
	//        try {
	//            if (lesEcrituresGenerees != null) {
	//                for (int i = 0; i < lesEcrituresGenerees.count(); i++) {
	//                    final EOEcriture element = (EOEcriture) lesEcrituresGenerees.objectAtIndex(i);
	//                    final KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
	//                    myKFactoryNumerotation.getNumeroEOEcriture(getEditingContext(), element);
	//                    //                myKFactoryNumerotation.(getEditingContext(), element, myKFactoryNumerotation);
	//                }
	//            }
	//            
	//        } catch (Exception e) {
	//            System.out.println("ERREUR LORS DE LA NUMEROTATION ECRITURES...");
	//            e.printStackTrace();
	//            throw new Exception(e);
	//        }
	//        try {
	//            if (newEmargements != null) {
	//                for (int i = 0; i < newEmargements.count(); i++) {
	//                    final EOEmargement element = (EOEmargement) newEmargements.objectAtIndex(i);
	//                    tmpFactoryEmargementProxy2.numeroterEmargement(getEditingContext(), element);
	//                }
	//                
	//            }
	//        } catch (Exception e) {
	//            System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
	//            e.printStackTrace();
	//
	//            throw new Exception(e);
	//        }
	//
	//        return newEmargements;
	//
	//    }

	//    
	// private final EOEmargement creerEmargementSemiAuto(final NSArray
	// depenses) throws Exception {
	// this.revertChanges();
	// final NSArray lesDebits = getDebitsForDepenses(depenses);
	// final NSArray lesCredits = getCreditsForDepenses(depenses);
	// final FactoryEmargementProxy tmpFactoryEmargementProxy2 = new
	// FactoryEmargementProxy(getEditingContext(),
	// myApp.appUserInfo().getUtilisateur(),
	// myApp.appUserInfo().getCurrentExercice(),FinderEmargement.leTypeLettrage(getEditingContext()),
	// myApp.wantShowTrace());
	//        
	// int modeEmargement =
	// tmpFactoryEmargementProxy2.getModeEmargement(lesDebits, lesCredits);
	// final EOEmargement newEmargement =
	// tmpFactoryEmargementProxy2.emarger(modeEmargement, lesDebits, lesCredits,
	// null, comptabilite);
	// ZLogger.debug(newEmargement);
	// //reactiver le savechanges
	// getEditingContext().saveChanges();
	// String msgFin = "Emargement effectué. ";
	//        
	//        
	// //Nettoyer les depenses de la liste
	// for (int i = 0; i < depenses.count(); i++) {
	// EODepense element = (EODepense) depenses.objectAtIndex(i);
	// emargementsProposes.remove(element);
	// depensesPourEmargement.removeObject(element);
	// }
	//        
	//        
	//        
	// // emargementsCrees.addObject(newEmargement);
	//        
	// //On numerote l'emargement
	// // KFactoryNumerotation myKFactoryNumerotation = new
	// KFactoryNumerotation(myApp.wantShowTrace());
	//        
	// try {
	// tmpFactoryEmargementProxy2.numeroterEmargement(getEditingContext(),
	// newEmargement);
	// myApp.showInfoDialog(msgFin + "\n\nL'émargement porte le numéro " +
	// newEmargement.emaNumero());
	// }
	// catch (Exception e) {
	// System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
	// e.printStackTrace();
	//            
	// throw new Exception(e);
	// }
	//        
	// return newEmargement;
	//        
	// }

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionAccepter extends AbstractAction {

		public ActionAccepter() {
			super("Accepter");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Accepter l'émargement sélectionné");
		}

		public void actionPerformed(ActionEvent e) {
			accepterEmargement();
		}

	}

	private final class ActionRefuser extends AbstractAction {

		public ActionRefuser() {
			super("Refuser");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDITDELETE_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Refuser l'émargement sélectionné");
		}

		public void actionPerformed(ActionEvent e) {
			refuserEmargement();
		}

	}

	// // private final class ActionAccepterTous extends AbstractAction {
	// //
	// // public ActionAccepterTous() {
	// // super("Tout accepter");
	// // this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_OK_16));
	// // this.putValue(AbstractAction.SHORT_DESCRIPTION, "Accepter tous les
	// émargements proposés");
	// // }
	// //
	// //
	// // public void actionPerformed(ActionEvent e) {
	// // accepterEmargementTous();
	// // }
	// //
	// // }
	//    
	// private final class ActionRefuserTous extends AbstractAction {
	//        
	// public ActionRefuserTous() {
	// super("Tout refuser");
	// this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_EDITDELETE_16));
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Refuser tous les
	// émargements proposés");
	// }
	//        
	//        
	// public void actionPerformed(ActionEvent e) {
	// refuserEmargementTous();
	// }
	//        
	// }

	private final class EmargementAutoPanelListener implements EmargementAutoPanel.IEmargementAutoPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementAutoPanel.IEmargementSrchPanelListener#actionFermer()
		 */
		public Action actionFermer() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementAutoPanel.IEmargementSrchPanelListener#getCreditsListListener()
		 */
		public IZKarukeraTablePanelListener getCreditsListListener() {
			return creditListListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementAutoPanel.IEmargementSrchPanelListener#getDebitsListListener()
		 */
		public IZKarukeraTablePanelListener getDebitsListListener() {
			return debitListListener;
		}

		public Action actionAccepter() {
			return actionAccepter;
		}

		public Action actionRefuser() {
			return actionRefuser;
		}

		//
		// public Action actionAccepterTous() {
		// return actionAccepterTous;
		// }
		//
		//
		// public Action actionRefuserTous() {
		// return actionRefuserTous;
		// }

		public IZKarukeraTablePanelListener getDepenseListListener() {
			return depenseListListener;
		}

		public Object getFlagForDepense(EODepense depense) {
			if (depense == null || emargementsProposes.get(depense) == null) {
				return null;
			}
			if (((HashMap) emargementsProposes.get(depense)).get(ERREURMSG) == null) {
				return INTEGER_0;
			}
			return INTEGER_1;
		}

	}

	private final void refreshActions() {
		actionAccepter.setEnabled(false);
		actionRefuser.setEnabled(false);
		// actionAccepterTous.setEnabled(false);
		// actionRefuserTous.setEnabled(false);
		myPanel.getErreurPanel().setCommentText(null);

		// if (depensesPourEmargement.count()>0) {
		// actionAccepterTous.setEnabled(true);
		// actionRefuserTous.setEnabled(true);
		// }

		String erreursMsg = null;
		if (getSelectedDepenses() != null) {

			for (int i = 0; i < getSelectedDepenses().count(); i++) {
				final EODepense element = (EODepense) getSelectedDepenses().objectAtIndex(i);
				if (((HashMap) emargementsProposes.get(element)).get(ERREURMSG) != null) {
					erreursMsg = ZStringUtil.concatStr(erreursMsg, (String) ((HashMap) emargementsProposes.get(element)).get(ERREURMSG), "\n");
				}
			}

			actionRefuser.setEnabled(true);
		}

		myPanel.getErreurPanel().invalidate();
		myPanel.getErreurPanel().setCommentText(erreursMsg);

		if (myPanel.getErreurPanel().getCommentText() == null || myPanel.getErreurPanel().getCommentText().length() == 0) {
			actionAccepter.setEnabled(true);
			myPanel.getErreurPanel().setVisible(false);
		}
		else {
			actionAccepter.setEnabled(false);
			myPanel.getErreurPanel().setVisible(true);
		}
		myPanel.validate();
	}

	private final EODepense getSelectedDepense() {
		return (EODepense) myPanel.getDepenseListPanel().selectedObject();
	}

	private final NSArray getSelectedDepenses() {
		return myPanel.getDepenseListPanel().selectedObjects();
	}

	/**
	 * @return Les depenses suceptibles d'avoir des ecritures à émarger.
	 */
	public final NSArray getDepensesPourEmargement() {
		return depensesPourEmargement;
	}

	private final NSArray getDebitsForDepense(final EODepense depense) {
		if (emargementsProposes.get(depense) == null) {
			return null;
		}
		return (NSArray) ((HashMap) emargementsProposes.get(depense)).get(DEBITS);
	}

	private final NSArray getCreditsForDepense(final EODepense depense) {
		if (emargementsProposes.get(depense) == null) {
			return null;
		}
		return (NSArray) ((HashMap) emargementsProposes.get(depense)).get(CREDITS);
	}

	private final EOEcritureDetail getCreditForDepense(final EODepense depense) {
		if (emargementsProposes.get(depense) == null) {
			return null;
		}
		NSArray credits = (NSArray) ((HashMap) emargementsProposes.get(depense)).get(CREDITS);
		if (credits != null && credits.count() > 0) {
			return (EOEcritureDetail) credits.objectAtIndex(0);
		}
		return null;

	}

	private final NSArray getDebitsForDepenses(final NSArray depenses) {
		final NSMutableArray debits = new NSMutableArray();
		for (int i = 0; i < depenses.count(); i++) {
			final EODepense element = (EODepense) depenses.objectAtIndex(i);
			debits.addObjectsFromArray(getDebitsForDepense(element));
		}
		return debits;
	}

	private final NSArray getCreditsForDepenses(final NSArray depenses) {
		final NSMutableArray credits = new NSMutableArray();
		for (int i = 0; i < depenses.count(); i++) {
			final EODepense element = (EODepense) depenses.objectAtIndex(i);
			credits.addObjectsFromArray(getCreditsForDepense(element));
		}
		return credits;
	}

	private final class CreditListListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			// final EODepense depense = (EODepense)
			// myPanel.getDepenseListPanel().selectedObject();
			// return getCreditsForDepense(depense);
			return getCreditsForDepenses(myPanel.getDepenseListPanel().selectedObjects());

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class DebitListListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			// final EODepense depense = (EODepense)
			// myPanel.getDepenseListPanel().selectedObject();
			// return getDebitsForDepense(depense);
			return getDebitsForDepenses(myPanel.getDepenseListPanel().selectedObjects());

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class DepenseListListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			try {
				myPanel.getCreditListPanel().updateData();
				myPanel.getDebitListPanel().updateData();
				refreshActions();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getDepensesPourEmargement();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return depenseTableCellRenderer;
		}

	}

	private final class DepenseTableCellRenderer extends ZEOTableCellRenderer {
		private final Image IMG_WARNING = ZIcon.getIconForName(ZIcon.ICON_WARNING_14).getImage();

		private final HashMap IMGS;

		/**
         * 
         */
		public DepenseTableCellRenderer() {
			IMGS = new HashMap(8);
			IMGS.put(new Integer(0), new ZEOTable.ZImageCell(new Image[] {}));
			IMGS.put(new Integer(1), new ZEOTable.ZImageCell(new Image[] {
					IMG_WARNING
			}));

		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object,
		 *      boolean, boolean, int, int)
		 */
		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			Component res = null;
			switch (table.convertColumnIndexToModel(column)) {
			case 0: // Flag
				res = (Component) IMGS.get(value);
				break;

			default:
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (isSelected) {
					res.setBackground(table.getSelectionBackground());
					res.setForeground(table.getSelectionForeground());
				}
				else {
					res.setBackground(table.getBackground());
					res.setForeground(table.getForeground());
				}
				break;
			}

			return res;
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
