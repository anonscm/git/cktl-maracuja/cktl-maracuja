/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.KTree;
import org.cocktail.maracuja.client.common.ui.KTreeCellRenderer;
import org.cocktail.maracuja.client.common.ui.KTreeModel;
import org.cocktail.maracuja.client.common.ui.KTreeNode;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;



/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaTitreStepRecapPanel extends ZKarukeraStepPanel {
	private static final String  TOPTITLE = "Récapitulatif";
	private static final String  TOPMESSAGE = "Vérifiez ici les informations avant de viser le bordereau.";
	
//	private final int PREFEREDWIDTH=785;
//	private final int PREFEREDHEIGHT1=60;
//	private final int PREFEREDHEIGHT2=200;
//	private final int PREFEREDHEIGHT3=200;

	private IBordereauRecapListener myListener;
	
	private KTreeNode rootNode;
	private KTreeModel myTreeModel;
	private KTree myTree;
	
	private KTreeNode rootNode2;
	private KTreeModel myTreeModel2;
	private KTree myTree2;
	
	private ZPanelNbTotal panelTotal1;
	private ZPanelNbTotal panelTotal2;
	

	
	public VisaTitreStepRecapPanel (IBordereauRecapListener vListener) {
		super();
		myListener = vListener;
	}
	

	/**
	 * Met à jour la infos.
	 */
	public void updateData() throws Exception {
//		ZLogger.debug("update ici");
		//on travaille sur un nested (dont le parent est l'editingcontext du panel précédent)
		setEditingContext(new EOEditingContext( myListener.getParentEditingContext() ));
		//TODO le passer aux enfants s'il y en a
		
		updateRootNode();
		updateTreeModel();
		panelTotal1.updateData();
		
		updateRootNode2();
		updateTreeModel2();
		panelTotal2.updateData();
	}		
		
		
	public void initGUI() {
		initTreeModel();
		initTree();
		initTreeModel2();
		initTree2();		
		super.initGUI();
	}


	/**
	 * 
	 */
	private void initTreeModel() {
//		myTreeModel = new ZTreeModel(rootNode);
	}

	/**
	 * 
	 */
	private void initTree() {
		myTree = new KTree();
		myTree.enableToolTips(false);
		myTree.setCellRenderer(new LeftTreeRenderer()  );
		
	}


	/**
	 * 
	 */
	private void initTreeModel2() {
//		myTreeModel = new ZTreeModel(rootNode);
	}

	/**
	 * 
	 */
	private void initTree2() {
		myTree2 = new KTree();
		myTree2.enableToolTips(false);
		myTree2.setCellRenderer(new RightTreeRenderer()  );
		
	}


	public boolean isPrevVisible() {
		return true;
	}

	public boolean isNextVisible() {
		return true;
	}


	public boolean isPrevEnabled() {
		return true;
	}


	public boolean isNextEnabled() {
		return false;
	}


	public boolean isEndEnabled() {
		return true;
	}
		
	public boolean isCloseEnabled() {
		return true;
	}


	public boolean isCloseVisible() {
		return true;
	}

	public void onPrev() {
//	    ZLogger.debug("Arborescence EC:", ZEOUtilities.arborescenceOfEc(getEditingContext()));	    
//		ZLogger.debug("update avant revert--------->:" + getEditingContext().updatedObjects());
		//Annuler les modifs (sur les modes de paiements, etc.)
		getEditingContext().revert();
//		ZLogger.debug("update apres revert------->:" + getEditingContext().updatedObjects());
		
		//On detruit également l'editingcontext
//		getEditingContext().dispose();
		myListener.onPrev();
	}


	public void onNext() {
		try {
			myListener.onNext();
		} catch (Exception e) {
			showErrorDialog(e);
			return;
		}
	}


	public void onClose() {
		myListener.onClose();
	}

	public String getCommentaire() {
		return TOPMESSAGE;
	}
	public String getTitle() {
		return TOPTITLE;
	}
	
//	/**
//	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#getParentEditingContext()
//	 */
//	public EOEditingContext getParentEditingContext() {
//		return myListener.getParentEditingContext();
//	}	
//	
	






	public Dimension getPanelDimension() {
		return myListener.getPanelDimension();
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.BordereauAViserPanel.StepPanel#getContentPanel()
	 */
	public JPanel getContentPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

//		mainPanel.add(buildPanelRecapBordereau(), BorderLayout.PAGE_START);
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, buildPanelRecapTree(), buildPanelRecapTree2());
		splitPane.setOneTouchExpandable(false);
		splitPane.setDividerLocation(0.5);
		splitPane.setDividerSize(5);
		splitPane.setResizeWeight(0.5);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		mainPanel.add(splitPane, BorderLayout.CENTER);
		return mainPanel;	
	}			
		




	
	
//	
//	/**
//	 * 
//	 */
//	private JPanel buildPanelRecapContent() {
//		return new JPanel();	
//	}


	/**
	 * 
	 */
	private JPanel buildPanelRecapTree() {
		JPanel tmp = new JPanel();
		tmp.setLayout(new BorderLayout());
		tmp.add( ZUiUtil.buildTitlePanel("Les titres acceptés", null,ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START );
		tmp.add(new JScrollPane(myTree), BorderLayout.CENTER);
		panelTotal1 = new ZPanelNbTotal(null);
		panelTotal1.setTotalProvider(new TotalProvider1());
		panelTotal1.setNbProvider(new NbProvider1());
		tmp.add(panelTotal1,BorderLayout.PAGE_END);
		return tmp;	
	}


	private JPanel buildPanelRecapTree2() {
		JPanel tmp = new JPanel();
		tmp.setLayout(new BorderLayout());
		tmp.add( ZUiUtil.buildTitlePanel("Les titres rejetés", null,ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START );		
		tmp.add(new JScrollPane(myTree2), BorderLayout.CENTER);
		panelTotal2 = new ZPanelNbTotal(null);
		panelTotal2.setTotalProvider(new TotalProvider2());
		panelTotal2.setNbProvider(new NbProvider2());
		tmp.add(panelTotal2,BorderLayout.PAGE_END);		
		return tmp;	
	}

//	private JPanel buildPanelBottom() {
//		JPanel mainPanel = new JPanel();
//		mainPanel.setLayout(new BorderLayout());
//		mainPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
////		mainPanel.add(buildTitlePanel("Listes des titres que vous avez sélectionné", null,ZConst.GRADIENTTITLESTARTCOLOR, ZConst.GRADIENTTITLEENDCOLOR), BorderLayout.PAGE_START);
////		mainPanel.add(myTitreAcceptesListePanel, BorderLayout.CENTER);
//		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
//		return mainPanel;
//	}		
//	
	
	
	


	public IBordereauRecapListener getMyListener() {
		return myListener;
	}

	public void setMyListener(IBordereauRecapListener listener) {
		myListener = listener;
	}
	



	/**
	 * Listener pour la liste des titres
	 */
	public interface IBordereauRecapListener extends ZKarukeraStepListener {
		public EOBordereau getBordereau();
		public ArrayList getTitresAViser();
		public ArrayList getTitresARejeter();
		 
	}



	/**
	 * @return
	 */
	public KTreeNode getRootNode() {
		return rootNode;
	}

	/**
	 * @param node
	 */
	public void setRootNode(KTreeNode node) {
		rootNode = node;
	}
	
	
	private void updateTreeModel() {
		myTreeModel = new KTreeModel(rootNode);
		myTree.setModel(myTreeModel);
		myTree.expandAllObjectsAtLevel(0,true);
//		myTree.expandAllObjectsAtLevel(1,true);
	}
	
	
	/**
	 * Construit l'arborescence des noeuds.
	 */
	private void updateRootNode() {
		KTreeNode tmpNode = new KTreeNode("Bordereau "+ myListener.getBordereau().borNum() + " du " + myListener.getBordereau().gestion().gesCode() ,myListener.getBordereau());
//		ZTreeNode tmpNode = new ZTreeNode("Bordereau "+ myListener.getBordereau().borNum(),myListener.getBordereau());
//		tmpRoot.addChild(buildNodeForTitresAcceptes());
        ArrayList tmp1 = myListener.getTitresAViser();
		Iterator iterator = tmp1.iterator();
		while (iterator.hasNext()) {
			EOTitre element = (EOTitre) iterator.next();
			tmpNode.addChild(buildNodeForTitreAccepte(element));
		}		
		rootNode = tmpNode;
	}
	
	////////////////////////////////////////
	
	
	
	/**
	 * @return
	 */
	public KTreeNode getRootNode2() {
		return rootNode2;
	}

	/**
	 * @param node
	 */
	public void setRootNode2(KTreeNode node) {
		rootNode2 = node;
	}
	
	
	private void updateTreeModel2() {
		myTreeModel2 = new KTreeModel(rootNode2);
		myTree2.setModel(myTreeModel2);
		myTree2.expandAllObjectsAtLevel(0,true);
//		myTree2.expandAllObjectsAtLevel(1,true);
//		myTree2.expandAllObjectsAtLevel(3,true);
//		myTree.expandAll(true);
	}
	
	
	/**
	 * Construit l'arborescence des noeuds.
	 */
	private void updateRootNode2() {
		KTreeNode tmpNode = new KTreeNode("Bordereau de rejet à créer" ,myListener.getBordereau());
//		tmpRoot.addChild(buildNodeForTitresRejetes());
        ArrayList tmp1 = myListener.getTitresARejeter();
		Iterator iterator = tmp1.iterator();
		while (iterator.hasNext()) {
			EOTitre element = (EOTitre) iterator.next();
			tmpNode.addChild(buildNodeForTitreRejete(element));
		}
		rootNode2 = tmpNode;
	}
		
	
	
	
	
//	private ZTreeNode buildNodeForTitresAcceptes() {
//		ZTreeNode tmpNode1 = new ZTreeNode("Titres acceptés",null);		
//		Vector tmp1 = myListener.getTitresAViser();
//		Iterator iterator = tmp1.iterator();
//		while (iterator.hasNext()) {
//			EOTitre element = (EOTitre) iterator.next();
//			tmpNode1.addChild(buildNodeForTitreAccepte(element));
//		}
//		return tmpNode1;
//	}
	
//	private ZTreeNode buildNodeForTitresRejetes() {
//		ZTreeNode tmpNode1 = new ZTreeNode("Titres rejetés",null);		
//		Vector tmp1 = myListener.getTitresARejeter();
//		Iterator iterator = tmp1.iterator();
//		while (iterator.hasNext()) {
//			EOTitre element = (EOTitre) iterator.next();
//			tmpNode1.addChild(buildNodeForTitreRejete(element));
//		}
//		return tmpNode1;
//	}	
	
	private KTreeNode buildNodeForTitreAccepte(EOTitre titre) {
		KTreeNode node = new KTreeNode(""+titre.titNumero() + titre.titOrigineLib() +" " ,titre);
//		ZTreeNode node = new ZTreeNode(formateTitreAccepte(titre) ,titre);
		node.addChild(buildNodeForTitreBrouillards(titre));
		return node;
	}	
	
	private KTreeNode buildNodeForTitreRejete(EOTitre titre) {
		KTreeNode node = new KTreeNode(""+titre.titNumero() ,titre);
		node.addChild(buildNodeForRecettes(titre) );
		return node;
	}		
	
	
	
	
	/**
	 * Renvoie un ZTreeNode construit à partir d'un titre.
	 * @param titre
	 * @return
	 */
	private KTreeNode buildNodeForTitreBrouillards(EOTitre titre) {
		KTreeNode node = new KTreeNode("Brouillards",null);
		NSArray array = EOsFinder.getTitreBrouilardsForTitre(getEditingContext(), titre);
		for (int i = 0; i < array.count(); i++) {
			EOTitreBrouillard element = (EOTitreBrouillard) array.objectAtIndex(i);
			KTreeNode tmp = buildNodeForTitreBrouillard(element);
			node.addChild(tmp);	
		}
		return node;
	}
	
	
	private KTreeNode buildNodeForTitreBrouillard(EOTitreBrouillard titreBrouillard) {
		KTreeNode node = new KTreeNode(titreBrouillard.tibSens()+" "+ titreBrouillard.planComptable().pcoNum() + "(" +titreBrouillard.tibMontant()+")" ,titreBrouillard);
		return node;
	}	
	
	
	private KTreeNode buildNodeForRecettes(EOTitre titre) {
		KTreeNode node = new KTreeNode("Recettes",null);
		NSArray array = titre.recettes();
		for (int i = 0; i < array.count(); i++) {
			EORecette element = (EORecette) array.objectAtIndex(i);
			KTreeNode tmp = buildNodeForRecette(element);
			node.addChild(tmp);	
		}
		return node;
	}	
	
	
	private KTreeNode buildNodeForRecette(EORecette recette) {
	    String s = recette.recNum()+" ("+ recette.recMontTtc()+")";
	    if ("OUI".equals(recette.recSuppression())) {
	        s = s + " A supprimer";
	    }
		KTreeNode node = new KTreeNode(s ,recette);
		return node;		
	}
	
	

	
	
	/**
	 * Composant pour l'affichage d'un Titre Accepté.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	class LeftTreeRenderer extends KTreeCellRenderer {
		Icon localIcon;
		Color bgMontant = Color.decode("#EEEEEE");
		

		public LeftTreeRenderer() {
			super();
			setBackgroundSelectionColor(new Color(0, 0, 128)); 
			setBorderSelectionColor(Color.black); 
			setTextSelectionColor(Color.white); 
			setTextNonSelectionColor(Color.black);			
		}

		public LeftTreeRenderer(Icon icon) {
			super();
			localIcon = icon;
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			superComponent = super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row,hasFocus);
			if (value instanceof KTreeNode) {
				if (((KTreeNode)value).getMyObject() instanceof EOTitre) {
					EOTitre titre = ((EOTitre) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForTitre(tree, titre, sel, expanded, leaf, row, hasFocus);
					return res;
				}
				else if (((KTreeNode)value).getMyObject() instanceof EOTitreBrouillard) {
					EOTitreBrouillard titreBrouillard = ((EOTitreBrouillard) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForTitreBrouillard(tree, titreBrouillard, sel, expanded, leaf, row, hasFocus);
					return res;
				}
			}
			
			//Par defaut			
			super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row,hasFocus);
			return this;
		}
		
		
		private Component getTreeCellRendererComponentForTitre(JTree tree, EOTitre titre, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(""+titre.titNumero(),5, ZLEFT, sel,null));
			tmpBox.add(buildTextField(titre.titOrigineLib() ,28, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format(titre.titTtc()) ,10 ,ZRIGHT,sel, bgMontant));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}	
		
		private Component getTreeCellRendererComponentForTitreBrouillard(JTree tree, EOTitreBrouillard titreBrouillard, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			Color tmpCol;
			if (titreBrouillard.tibSens().equals(ZConst.SENS_CREDIT) ){
				tmpCol = ZConst.BGCOLOR_CREDIT;
			}
			else {
				tmpCol = ZConst.BGCOLOR_DEBIT;
			}
			tmpBox.add(buildTextField(titreBrouillard.tibSens() ,1, ZLEFT, sel,null));
			tmpBox.add(buildTextField(titreBrouillard.gestion().gesCode(), 3, ZLEFT, sel,null));
			tmpBox.add(buildTextField(titreBrouillard.planComptable().pcoNum()  ,5, ZLEFT,sel,null));
			tmpBox.add(buildTextField(titreBrouillard.planComptable().pcoLibelle()  ,20, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format( titreBrouillard.tibMontant()  ) ,10 ,ZRIGHT,sel, tmpCol));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}	
		
	}
	
	
	
	class RightTreeRenderer extends KTreeCellRenderer {
		Icon localIcon;
		Color bgMontant = Color.decode("#EEEEEE");
		

		public RightTreeRenderer() {
			super();
			setBackgroundSelectionColor(new Color(0, 0, 128)); 
			setBorderSelectionColor(Color.black); 
			setTextSelectionColor(Color.white); 
			setTextNonSelectionColor(Color.black);			
		}

		public RightTreeRenderer(Icon icon) {
			super();
			localIcon = icon;
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			superComponent = super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row,hasFocus);
			if (value instanceof KTreeNode) {
				if (((KTreeNode)value).getMyObject() instanceof EOTitre) {
					EOTitre titre = ((EOTitre) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForTitre(tree, titre, sel, expanded, leaf, row, hasFocus);
					return res;
				}
				else if (((KTreeNode)value).getMyObject() instanceof EORecette) {
					EORecette recette= ((EORecette) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForRecette(tree, recette, sel, expanded, leaf, row, hasFocus);
					return res;
				}
			}
			
			//Par defaut			
			super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row,hasFocus);
			return this;
		}
		
		
		private Component getTreeCellRendererComponentForTitre(JTree tree, EOTitre titre, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(""+titre.titNumero(),5, ZLEFT, sel,null));
			tmpBox.add(buildTextField(titre.titOrigineLib() ,20, ZLEFT,sel,null));
			tmpBox.add(buildTextField( titre.titMotifRejet(),25, ZLEFT,sel,ZConst.BGCOLOR_POSTIT));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format(titre.titTtc()) ,10 ,ZRIGHT,sel, bgMontant));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}	
		
		
		private Component getTreeCellRendererComponentForRecette(JTree tree, EORecette recette, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(recette.recNum().toString()  ,15, ZLEFT, sel,null));
			tmpBox.add(buildTextField(recette.recLigneBudgetaire()  ,25, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format( recette.recMontTtc()   ) ,10 ,ZRIGHT,sel, bgMontant));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}			

	}	
	
	
	
	private class TotalProvider1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  ZEOUtilities.calcSommeOfBigDecimals(myListener.getTitresAViser(), "titTtc")  ;
		}

		public void setValue(Object value) {
			return;
		}
	}
	
	
	private class TotalProvider2 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  ZEOUtilities.calcSommeOfBigDecimals(myListener.getTitresARejeter(), "titTtc")  ;
		}

		public void setValue(Object value) {
			return;
		}
	}	
	
	private class NbProvider1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  new Integer(myListener.getTitresAViser().size());
		}

		public void setValue(Object value) {
			return;
		}
	}	
	private class NbProvider2 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return  new Integer(myListener.getTitresARejeter().size());
		}

		public void setValue(Object value) {
			return;
		}
	}



	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#specialAction1()
	 */
	public AbstractAction specialAction1() {
		return getMyListener().specialAction1() ;
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#onDisplay()
	 */
	public void onDisplay() {
		return;
	}


    /* (non-Javadoc)
     * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#valideSaisie()
     */
    public boolean valideSaisie() throws Exception {
        return true;
    }	
		
	
	
}
