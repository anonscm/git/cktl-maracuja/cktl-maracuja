/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class BrouillardAjoutCtrl extends CommonCtrl {
	private static final Dimension PCOWINDOW_SIZE = new Dimension(300, 420);
	public static final String SENS_DEBIT = "Débit";
	public static final String SENS_CREDIT = "Crédit";

	ActionClose actionClose = new ActionClose();
	ActionValider actionValider = new ActionValider();

	final HashMap dicoSaisie = new HashMap();

	protected BrouillardAjoutPanel saisiePanel;

	protected final PcoLookupModel pcoLookupModel;
	protected final PcoLookupListener pcoLookupListener = new PcoLookupListener();
	protected final HashMap pcoMap;
	protected final HashMap pcoMapReal;

	protected ZEOComboBoxModel myGestionComboBoxModel;
	protected DefaultComboBoxModel mySensComboBoxModel;
	final PcoNumFieldModel pcoNumFieldModel;

	private final EOQualifier _qualPco;

	private NSArray gestionsNonSacd = null;
	private NSArray gestionsSacd = null;

	public BrouillardAjoutCtrl(EOEditingContext ec, final EOQualifier qualPco) throws Exception {
		super(ec);

		_qualPco = qualPco;
		final NSArray pcos;
		pcos = getPcoValides();

		pcoMap = new LinkedHashMap(pcos.count());
		pcoMapReal = new LinkedHashMap(pcos.count());
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptable element = (EOPlanComptable) pcos.objectAtIndex(i);
			pcoMap.put(ZStringUtil.extendWithChars(element.pcoNum(), " ", 15, false) + " " + element.pcoLibelle(), element);
			pcoMapReal.put(element.pcoNum(), element);
		}
		pcoLookupModel = new PcoLookupModel();

		//final EOFonction fonc = myApp.getFonctionByActionId(getActionId());
		//        final NSArray res = EOUtilisateurFinder.getGestionsForUtilisateurAndExerciceAndFonction(getEditingContext(),myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(), fonc);
		//		final NSArray authorizedGestion = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_VIDE);
		//		if (authorizedGestion.count() == 0) {
		//			throw new DefaultClientException("Vous avez un droit sur cette fonctionnalité, mais aucun code gestion associé. Demandez à un administrateur de vous associer des codes gestions pour la fonctionnalité " + fonc.fonLibelle());
		//		}

		gestionsNonSacd = myApp.appUserInfo().getAllowedNonSacdsForFonction(getEditingContext(), myApp.getMyActionsCtrl(), getActionId());
		gestionsSacd = myApp.appUserInfo().getAllowedSacdsForFonction(getEditingContext(), myApp.getMyActionsCtrl(), getActionId());

		System.out.println("NB SACD = " + gestionsSacd.count());
		System.out.println("NB comp = " + gestionsNonSacd.count());

		System.out.println("getActionId() = " + getActionId());

		myGestionComboBoxModel = new ZEOComboBoxModel(gestionsNonSacd, EOGestion.GES_CODE_KEY, null, null);
		mySensComboBoxModel = new DefaultComboBoxModel(new String[] {
				SENS_DEBIT, SENS_CREDIT
		});

		pcoNumFieldModel = new PcoNumFieldModel(dicoSaisie, EOPlanComptable.PCO_NUM_KEY);
		saisiePanel = new BrouillardAjoutPanel(new SaisiePanelListener());

	}

	protected abstract String getActionId();

	protected void checkDicoSaisie() throws Exception {
		if (dicoSaisie.get("montant") == null) {
			throw new DataCheckException("Le montant est obligatoire");
		}
		if (dicoSaisie.get(EOMandatBrouillard.PLAN_COMPTABLE_KEY) == null) {
			throw new DataCheckException("Un compte valide est obligatoire");
		}

		if (dicoSaisie.get("gestion") == null) {
			throw new DataCheckException("Le code gestion est obligatoire");
		}
		if (dicoSaisie.get("sens") == null) {
			throw new DataCheckException("Le sens est obligatoire");
		}
	}

	protected final NSArray getPcoValides() {
		//        final EOQualifier qualPcoTitre = EOQualifier.qualifierWithQualifierFormat("pcoValidite=%@ and (pcoNum like %@ or pcoNum like %@ or pcoNum like %@", new NSArray(new Object[] { EOPlanComptable.etatValide, "4*", "139*", "59*"}));
		return EOPlanComptableFinder.getPlancoValidesWithQuals(getEditingContext(), _qualPco, false);
	}

	protected final void annulerSaisie() {
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	protected void validerSaisie() {
		try {
			if (dicoSaisie.get("montant") != null && dicoSaisie.get("montant") instanceof Number) {
				dicoSaisie.put("montant", new BigDecimal(((Number) dicoSaisie.get("montant")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			dicoSaisie.put("gestion", myGestionComboBoxModel.getSelectedEObject());
			dicoSaisie.put("sens", mySensComboBoxModel.getSelectedItem());
			checkDicoSaisie();
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void updateSelectedPlanComptable() {
		dicoSaisie.put(EOMandatBrouillard.PLAN_COMPTABLE_KEY, pcoMapReal.get(dicoSaisie.get(EOPlanComptable.PCO_NUM_KEY)));
		saisiePanel.updatePcoLibelle();
	}

	protected final ZKarukeraDialog createModalDialog(final Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, getTitle(), true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, getTitle(), true);
		}
		setMyDialog(win);
		saisiePanel.initGUI();
		saisiePanel.setPreferredSize(getWindowSize());
		win.setContentPane(saisiePanel);
		win.pack();
		return win;
	}

	protected abstract String getTitle();

	protected abstract Dimension getWindowSize();

	protected void updateCodeGestion(EOGestion gestion) throws Exception {
		NSArray res = NSArray.EmptyArray;
		if (gestion == null) {
			throw new Exception("Gestion non précisée");
		}

		System.out.println("NB SACD = " + gestionsSacd.count());
		System.out.println("NB comp = " + gestionsNonSacd.count());
		System.out.println("Gestion = " + gestion.gesCode());
		//si sacd, on ne propose que le sacd
		//		if (FinderGestion.gestionIsSacd(gestion, getExercice())) {
		if (gestion.isSacd(getExercice())) {
			System.out.println("BrouillardAjoutCtrl.updateCodeGestion() : SACD");
			if (gestionsSacd.indexOfObject(gestion) != NSArray.NotFound) {
				res = new NSArray(new Object[] {
						gestion
				});
			}
			else {
				throw new Exception("Vous n'avez pas de droit sur le code gestion " + gestion.gesCode());
			}
		}
		//sinon on propose tout sauf les sacds
		else {
			System.out.println("BrouillardAjoutCtrl.updateCodeGestion() : NON SACD");
			res = gestionsNonSacd;
		}
		if (res.count() == 0) {
			throw new Exception("Vous n'avez pas de droit sur un code gestion pour cette fonction");
		}
		System.out.println("codes gestions = " + res);
		myGestionComboBoxModel.updateListWithData(res);
		myGestionComboBoxModel.setSelectedEObject(gestion);

	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public Map openDialogNew(final Window dial, final BigDecimal montant, EOGestion gestion, String sens) {
		final ZKarukeraDialog win = createModalDialog(dial);
		Map res = null;
		try {
			//mettre a jour les codes gestions s
			updateCodeGestion(gestion);
			dicoSaisie.put("montant", ZConst.BIGDECIMAL_ZERO);
			if (montant != null) {
				dicoSaisie.put("montant", montant);
			}
			if (sens != null) {
				dicoSaisie.put("sens", sens);
				mySensComboBoxModel.setSelectedItem(sens);
			}
			saisiePanel.updateData();
			if (win.open() == ZKarukeraDialog.MROK) {
				res = dicoSaisie;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final class SaisiePanelListener implements BrouillardAjoutPanel.IBrouillardAjoutPanelListener {

		public Action actionValider() {
			return actionValider;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Map getFilters() {
			return dicoSaisie;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public ZEOComboBoxModel getGestionModel() {
			return myGestionComboBoxModel;
		}

		public IZTextFieldModel getPcoNumModel() {
			return pcoNumFieldModel;
		}

		public DefaultComboBoxModel getSensModel() {
			return mySensComboBoxModel;
		}

	}

	private class PcoLookupListener implements IZLookupButtonListener {

		public void setNewValue(Object res) {
			final EOPlanComptable tmp = (EOPlanComptable) pcoMap.get(res);
			dicoSaisie.put(EOMandatBrouillard.PLAN_COMPTABLE_KEY, tmp);
			if (tmp != null) {
				dicoSaisie.put(EOPlanComptable.PCO_NUM_KEY, tmp.pcoNum());
			}
			else {
				dicoSaisie.put(EOPlanComptable.PCO_NUM_KEY, null);
			}

			try {
				saisiePanel.updatePcoNum();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onTextHasChanged() {

		}

		public void onDbClick() {

		}

	}

	private class PcoLookupModel implements IZLookupButtonModel {
		private final Collection pcosTmp;

		public PcoLookupModel() {
			pcosTmp = pcoMap.keySet();
		}

		public Collection getDatas() {
			return pcosTmp;
		}

		public String getActionDescription() {
			return "Sélectionner un compte";
		}

		public ImageIcon getIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
		}

		public Window getMyWindow() {
			return getMyDialog();
		}

		public String getTitle() {
			return "Sélectionnez un compte";
		}

		public Dimension getPreferredSize() {
			return PCOWINDOW_SIZE;
		}

	}

	private final class PcoNumFieldModel extends ZTextField.DefaultTextFieldModel {
		public PcoNumFieldModel(final Map filter, final String key) {
			super(filter, key);
		}

		public void setValue(Object value) {
			super.setValue(value);
			updateSelectedPlanComptable();

		}

	}

}
