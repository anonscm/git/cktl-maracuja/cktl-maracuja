/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecetteListePanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;
	private TableSorter myTableSorter;
	private IRecetteListePanelListener myListener;

	private HashMap checkedRecettes;

	private ZEOTableModelColumnDico col0;

	/**
	 * @param context
	 */
	public RecetteListePanel(IRecetteListePanelListener vListener, EOEditingContext context) {
		super();
		setEditingContext(context);
		this.myListener = vListener;
		myDisplayGroup = new EODisplayGroup();

		myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "Recette"));
		((EODistributedDataSource) myDisplayGroup.dataSource()).setEditingContext(getEditingContext());

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private void initTableModel() {
		checkedRecettes = myListener.getAllCheckedRecettes();
		Vector myCols2 = new Vector(6, 0);
		col0 = new ZEOTableModelColumnDico(myDisplayGroup, "Viser", 60, checkedRecettes);
		//		ZEOTableModelColumnCheckBox col0 = new ZEOTableModelColumnCheckBox(myDisplayGroup,"Viser",60,Boolean.TRUE);
		col0.setEditable(true);
		col0.setResizable(false);
		col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "recNum", "N°", 70);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);

		//		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"depDateReception","Date réception",100);
		//		col2.setFormatDisplay(ZConst.FORMAT_DATESHORT) ;
		//		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"recRib","Rib",200);
		//		col2.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "recMont", "Montant HT", 80);
		col3.setAlignment(SwingConstants.RIGHT);
		col3.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col3.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "recMonttva", "Montant TVA", 80);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, "recMontTtc", "Montant TTC", 80);
		col5.setAlignment(SwingConstants.RIGHT);
		col5.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col5.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, "recLibelle", "Libellé", 200);
		col6.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn col7 = new ZEOTableModelColumn(myDisplayGroup, EORecette.ACCORD_CONTRAT_UNIQUE_KEY + "." + EOAccordsContrat.NUMERO_KEY, "Convention", 150);
		col7.setColumnClass(String.class);
		col7.setAlignment(SwingConstants.LEFT);

		myCols2.add(col0);
		myCols2.add(col1);
		myCols2.add(col6);
		myCols2.add(col7);
		myCols2.add(col3);
		myCols2.add(col4);
		myCols2.add(col5);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols2);
		myTableSorter = new TableSorter(myTableModel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		ZLogger.debug("selection recette changee");
		myListener.onRecetteSelectionChanged();
	}

	public void updateData() {
		//		ZLogger.debug("Mise a jour de la liste des recettes");
		try {
			if (myListener.getSelectedTitre() != null) {
				NSArray res = myListener.getSelectedTitre().recettes();
				//				myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "Recette"));
				myDisplayGroup.setObjectArray(res);
				//				ZLogger.debug ("Nb titres = "+res.count() );
			}
			else {
				myDisplayGroup.setObjectArray(new NSArray());
				//				ZLogger.debug("Pas de recette");
			}

			//			Indiquer au modele que le nombre d'enregistrements a changé
			//			myTableModel.updateInnerRowCount();
			//			myTableModel.fireTableDataChanged();
			myEOTable.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		initTableModel();
		initTable();
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public interface IRecetteListePanelListener {
		public EOTitre getSelectedTitre();

		public void onRecetteSelectionChanged();

		/**
		 * Doit renvoyer l'ensemble des recettes cochees (indépendamment du titre, ceci afin de garantir l'historique).
		 */
		public HashMap getAllCheckedRecettes();
	}

	/**
	 * Renvoie la dépense sélectionnée.
	 */
	public EORecette getSelectedRecette() {
		return (EORecette) myDisplayGroup.selectedObject();
	}

	/**
	 * Renvoie les recettes cochés
	 */
	public ArrayList checkedRecettes() {
		return myTableModel.getRowsForValueAtCol(Boolean.TRUE, 0);
	}

	public void fireTableDataChanged() {
		myTableModel.fireTableDataChanged();
	}

	/**
	 * @return
	 */
	public ZEOTableModelColumnDico getCol0() {
		return col0;
	}

}
