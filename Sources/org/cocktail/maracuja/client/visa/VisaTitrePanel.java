/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factories.ZFactory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaTitre;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSRange;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaTitrePanel extends ZKarukeraPanel {
	private ABordereauSelectPanel myBordereauSelectPanel;
	private VisaTitreStepDispatchPanel myBordereauDispatch;
	private VisaTitreStepDetailPanel myBordereauDetailVises;
	private VisaTitreStepRecapPanel myBordereauRecap;

	private BordereauSelectPanelListener myBordereauSelectPanelListener;
	private BordereauAViserDispatchPanelListener myBordereauAViserDispatchPanelListener;
	private BordereauDetailVisesListener myBordereauDetailVisesListener;
	private BordereauRecapListener myBordereauRecapListener;

	private ZKarukeraStepPanel currentPanel;

	public int panelWidth = 1000;
	public int topHeight = 100;
	public int minContentHeight;
	public int bottomHeight = 50;
	public static final Dimension MINDIMENSION = new Dimension(1000, 700);
	public ChangeStepListener myChangeStepListener;

	private EOGestion selectedGestion;
	private EOBordereau selectedBordereau;

	private Date dateVisa;

	private AbstractAction specialAction1;

	private NSArray plancoValides = null;

	public VisaTitrePanel() throws Exception {
		super();

		specialAction1 = new ActionViser();

		myBordereauSelectPanelListener = new BordereauSelectPanelListener();
		myBordereauDetailVisesListener = new BordereauDetailVisesListener();
		myBordereauAViserDispatchPanelListener = new BordereauAViserDispatchPanelListener();
		myBordereauRecapListener = new BordereauRecapListener();

		myBordereauSelectPanel = new VisaTitreStepBordereauSelectPanel();
		myBordereauSelectPanel.setMyListener(myBordereauSelectPanelListener);
		myBordereauDispatch = new VisaTitreStepDispatchPanel(myBordereauAViserDispatchPanelListener);
		myBordereauDetailVises = new VisaTitreStepDetailPanel(myBordereauDetailVisesListener);
		myBordereauRecap = new VisaTitreStepRecapPanel(myBordereauRecapListener);

		plancoValides = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), myApp.appUserInfo().getCurrentExercice(), null, false);
		if (plancoValides.count() == 0) {
			throw new DataCheckException("Erreur : la liste des plan comptable valides est vide, impossible de controler la validité des comptes. Cette erreur est vraisemblablement dûe a une perte de connexion temporaire. Veuillez relancer l'application");
		}

		//initGUI(); //appelé dans initGUI() parent
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		this.setPreferredSize(MINDIMENSION);
		//dateVisa = null;

		myBordereauSelectPanel.initGUI();
		myBordereauDispatch.initGUI();
		myBordereauDetailVises.initGUI();
		myBordereauRecap.initGUI();
		//        myBordereauRecap.initGUI();

		myBordereauSelectPanel.setMyDialog(getMyDialog());
		myBordereauDispatch.setMyDialog(getMyDialog());
		myBordereauDetailVises.setMyDialog(getMyDialog());
		myBordereauRecap.setMyDialog(getMyDialog());

		//Premier panel actif
		currentPanel = myBordereauSelectPanel;
	}

	/**
	 * Met à jour le panel actif
	 */
	public void updateGUI() {
		//Supprimer l'éventuel panel
		this.removeAll();

		this.setLayout(new BorderLayout());
		//Ajouter le panel en cours
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		this.add(currentPanel, BorderLayout.CENTER);
		currentPanel.onDisplay();
	}

	public void notifyListeners() {
		if (myChangeStepListener != null) {
			myChangeStepListener.stepHasChanged();
		}
		else {
			ZLogger.debug("Pas de listener... bizarre...");
		}
	}

	/**
	 * Interface pour les listeners du wizard (normalement le dialogue parent).
	 */
	public interface ChangeStepListener {
		public void stepHasChanged();

		public void onCloseAsked();
	}

	public ChangeStepListener getMyChangeStepListener() {
		return myChangeStepListener;
	}

	public void setMyChangeStepListener(ChangeStepListener listener) {
		myChangeStepListener = listener;
	}

	//    private void selectNewBordereau(EOBordereau newBord) {
	//
	//    }

	/**
	 * Listener pour le panneau de selection des bordereaux (premiere etape)
	 */
	private class BordereauSelectPanelListener implements ABordereauSelectPanel.IBordereauSelectPanelListener {
		public void onNewSelectedGestion(EOGestion newGestion) {
			selectedGestion = newGestion;
		}

		public void onNext() {
			// if (selectedBordereau != myBordereauSelectPanel.selectedBordereau()) {
			selectedBordereau = myBordereauSelectPanel.selectedBordereau();
			ZLogger.debug("Num bordereau selectionné", selectedBordereau.borNum());
			try {
				myBordereauDispatch.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
			//}
			currentPanel = myBordereauDispatch;
			myChangeStepListener.stepHasChanged();
		}

		public void onPrev() {
			return;

		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			return getSize();
		}

		public int etapeNumber() {
			return 1;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#getCommentaireDimension()
		 */
		public Dimension getCommentaireDimension() {
			return new Dimension(getSize().width, topHeight);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}
	}

	/**
	 * Listener pour le panneau de dispatching des bordereaux (deuxieme etape une fois le bordereau selectionne).
	 */
	private class BordereauAViserDispatchPanelListener implements VisaTitreStepDispatchPanel.IBordereauAViserDispatchPanelListener {
		public void onNext() {
			try {
				getMyDialog().setWaitCursor(true);
				currentPanel = myBordereauDetailVises;
				myBordereauDetailVises.updateData();
				myChangeStepListener.stepHasChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				getMyDialog().setWaitCursor(false);
			}
		}

		public void onPrev() {
			try {
				currentPanel = myBordereauSelectPanel;
				myChangeStepListener.stepHasChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			//			return MINDIMENSION;
			return getSize();

		}

		public int etapeNumber() {
			return 2;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.BordereauAViserDispatchPanel.IBordereauAViserDispatchPanelListener#getSelectedBordereau()
		 */
		public EOBordereau getSelectedBordereau() {
			return selectedBordereau;
		}

		public Date getDateReception() {
			return dateVisa;
		}

		public void setDateReception(Date newDate) {
			dateVisa = newDate;
			ZLogger.debug("dateVisa", newDate);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.BordereauAViserDispatchPanel.IBordereauAViserDispatchPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#getCommentaireDimension()
		 */
		public Dimension getCommentaireDimension() {
			return new Dimension(getSize().width, topHeight);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return myBordereauSelectPanel.getEditingContext();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}
	}

	private class BordereauDetailVisesListener implements VisaTitreStepDetailPanel.IBordereauDetailVisesListener {
		public ArrayList getTitresAViser() {
			return VisaTitrePanel.this.getTitresAViser();
		}

		//        public Dialog getDialog() {
		//            return null;
		//        }

		public void onNext() {
			try {
				getMyDialog().setWaitCursor(true);
				checkComptesValides(getTitresAViser());
				currentPanel = myBordereauRecap;
				myBordereauRecap.updateData();
				myChangeStepListener.stepHasChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				getMyDialog().setWaitCursor(false);
			}
		}

		public void onPrev() {
			getMyDialog().setWaitCursor(true);
			currentPanel = myBordereauDispatch;
			myChangeStepListener.stepHasChanged();
			getMyDialog().setWaitCursor(false);
		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			return getSize();
		}

		/**
		 * L'EditingContext du panel de dispatch
		 * 
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel.ZKarukeraStepListener#getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return myBordereauDispatch.getEditingContext();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}

	}

	private class BordereauRecapListener implements VisaTitreStepRecapPanel.IBordereauRecapListener {
		public ArrayList getTitresAViser() {
			return VisaTitrePanel.this.getTitresAViser();
		}

		//        public Dialog getDialog() {
		//            return null;
		//        }

		public void onNext() {
			//			currentPanel = myBordereauDispatch;
			myChangeStepListener.stepHasChanged();
		}

		public void onPrev() {
			currentPanel = myBordereauDetailVises;
			myChangeStepListener.stepHasChanged();
		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			return getSize();
		}

		/**
		 * L'EditingContext du panel précédent
		 * 
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel.ZKarukeraStepListener#getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return myBordereauDetailVises.getEditingContext();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.BordereauRecapPanel.IBordereauRecapListener#getBordrereau()
		 */
		public EOBordereau getBordereau() {
			return selectedBordereau;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.BordereauRecapPanel.IBordereauRecapListener#getTitresARejeter()
		 */
		public ArrayList getTitresARejeter() {
			return VisaTitrePanel.this.getTitresARejeter();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener#specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		currentPanel.updateData();
	}

	/**
	 * @return
	 */
	public ArrayList getTitresARejeter() {
		return myBordereauDispatch.getTitresARejeter();
	}

	/**
	 * @return
	 */
	public ArrayList getTitresAViser() {
		return myBordereauDispatch.getTitresAViser();
	}

	private class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Lancer le visa du bordereau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_TRAITEMENT_16));
		}

		public void actionPerformed(ActionEvent e) {
			traiterViser();
		}

	}

	public void checkComptesValides(ArrayList titresVises) throws Exception {
		//NSArray plancoValides = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), myApp.appUserInfo().getCurrentExercice(), null, false);
		if (plancoValides.count() == 0) {
			throw new DataCheckException("Erreur : la liste des plan comptable valides est vide, impossible de controler la validité des comptes. Cette erreur est vraisemblablement dûe a une perte de connexion temporaire. Veuillez relancer l'application");
		}

		for (int i = 0; i < titresVises.size(); i++) {
			final EOTitre element = (EOTitre) titresVises.get(i);

			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTitreBrouillard.TIB_OPERATION_KEY + "<>%@", new NSArray(new Object[] {
					EOTitreBrouillard.VISA_ANNULER
			}));
			NSArray mabs = EOQualifier.filteredArrayWithQualifier(element.titreBrouillards(), qual);

			for (int j = 0; j < mabs.count(); j++) {
				EOPlanComptable planco = ((EOTitreBrouillard) mabs.objectAtIndex(j)).planComptable();
				//				EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PLAN_COMPTABLE_KEY + "=%@", new NSArray(new Object[] {
				//					planco
				//				}));
				EOQualifier qual2 = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, planco.pcoNum());

				if (EOQualifier.filteredArrayWithQualifier(plancoValides, qual2).count() == 0) {
					ZLogger.verbose("Verification du compte " + planco.pcoNum() + " -> NON VALIDE");
					throw new DataCheckException("Le compte " + planco.pcoNum() + " associée au titre n° " + element.titNumero() + " n'est pas valide.");
				}
				ZLogger.verbose("Verification du compte " + planco.pcoNum() + " -> VALIDE");
			}
		}
	}

	public boolean checkPlusieursConventionsParTitre(ArrayList<EOTitre> titresVises) throws Exception {
		boolean goOn = true;
		for (int i = 0; goOn && i < titresVises.size(); i++) {
			final EOTitre titre = (EOTitre) titresVises.get(i);
			for (int j = 0; j < titre.recettes().count(); j++) {
				EORecette recette = (EORecette) titre.recettes().objectAtIndex(j);
				if (recette.getAccordContratLies().count() > 1) {
					goOn = myApp.showConfirmationDialog("Confirmation", "La recette '" + recette.recLibelle() + "' du titre n° " + titre.titNumero() + " fait référence à plusieurs conventions. Si vous visez ce titre, l'écriture de prise en charge de la recette ne sera pas reliée aux conventions. " +
							"Une recette doit être affecté à une seule convention pour que les écritures soient reliées à cette convention. \n Souhaitez-vous quand même accepter ce titre ?", ZMsgPanel.BTLABEL_NO);
				}
			}

		}
		return goOn;
	}

	private void traiterViser() {
		if (!myApp.showConfirmationDialog("Confirmation", "Souhaitez vous réellement viser le bordereau " + selectedBordereau.borNum() + "?\n cette opération est irréversible.", ZMsgPanel.BTLABEL_NO)) {
			return;
		}
		try {
			try {
				if (!currentPanel.valideSaisie()) {
					return;
				}
				//verifier si les comptes sont valides
				checkComptesValides(getTitresAViser());
				if (!checkPlusieursConventionsParTitre(getTitresAViser())) {
					return;
				}

			} catch (Exception e) {
				showErrorDialog(e);
				return;
			}
			setWaitCursor(true);

			//          Enregistrer les changements dans les nested editingContext jusqu'à l'editingContext
			//          On enregistre pas les changements de l'editingContext en cours
			if ((myBordereauRecap.getEditingContext() != null) && (myBordereauRecap.getEditingContext().hasChanges())) {
				ZLogger.debug("saveChanges sur myBordereauRecap");
				myBordereauRecap.getEditingContext().saveChanges();
			}
			if ((myBordereauDetailVises.getEditingContext() != null) && (myBordereauDetailVises.getEditingContext().hasChanges())) {
				ZLogger.debug("saveChanges sur myBordereauDetailVises");
				myBordereauDetailVises.getEditingContext().saveChanges();
			}
			//WARN pas de savechanges car on est sur l'ec prinipal
			//            if ((myBordereauDispatch.getEditingContext()!=null) && (myBordereauDispatch.getEditingContext().hasChanges())  ) {
			//                ZLogger.debug("saveChanges sur myBordereauDispatch" );
			//                myBordereauDispatch.getEditingContext().saveChanges();
			//            }

			EOTypeJournal myTypeJournal = FinderJournalEcriture.leTypeJournalVisaTitre(getEditingContext());
			EOTypeOperation myTypeOperation = FinderVisa.leTypeOperationVisaTitre(getEditingContext());

			ZLogger.debug("myTypeOperation", myTypeOperation);
			ZLogger.debug("myTypeJournal", myTypeJournal);

			FactoryProcessVisaTitre myFactoryProcessVisaTitre = new FactoryProcessVisaTitre(getEditingContext(), myTypeJournal, myTypeOperation, myApp.wantShowTrace(), new NSTimestamp(myApp.getDateJourneeComptable()));

			EOBordereauRejet myEOBordereauRejet;
			try {

				//Recuperer les titres pour qu'ils soient dans l'editingContext
				NSArray alltitresLocal = selectedBordereau.titres();
				ZFactory.enumererEObjets(alltitresLocal);
				ArrayList tmp = ZFactory.localInstancesForObjects(getEditingContext(), getTitresAViser());
				ZLogger.debug(tmp);
				NSArray titresVises = new NSArray(tmp.toArray(), new NSRange(0, tmp.size()));

				//Trier les titre par n°
				final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey("titNumero", EOSortOrdering.CompareAscending);
				titresVises = EOSortOrdering.sortedArrayUsingKeyOrderArray(titresVises, new NSArray(sortOrdering));

				tmp = getTitresARejeter();
				NSArray titresRejetes = new NSArray(tmp.toArray(), new NSRange(0, tmp.size()));
				titresRejetes = EOSortOrdering.sortedArrayUsingKeyOrderArray(titresRejetes, new NSArray(sortOrdering));

				//Recuperer le delai de paiement depuis les parametres
				String delaiStr = (String) myApp.getParametres().valueForKey(ZConst.FACTURE_DELAI_PAIEMENT_NAME);
				if (delaiStr == null) {
					delaiStr = ZConst.DEFAULT_DELAI_RELANCE;
				}
				final Integer delai = new Integer(delaiStr);
				final Date dateLimitePaiement = ZDateUtil.addDHMS(dateVisa, delai.intValue(), 0, 0, 0);
				myEOBordereauRejet = myFactoryProcessVisaTitre.viserUnBordereauDeTitreEtNumeroter(getEditingContext(), myApp.appUserInfo().getUtilisateur(), selectedBordereau, titresRejetes, titresVises, new NSTimestamp(dateVisa), new KFactoryNumerotation(myApp.wantShowTrace()), new NSTimestamp(
						dateLimitePaiement));

				//              Si tout s'est bien passé on enregistre les changements, enfin on essaye...
				try {
					this.myBordereauSelectPanel.getEditingContext().saveChanges();
				}
				//catcher exception
				catch (Exception e) {
					e.printStackTrace();
					throw new Exception(e);
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception(e);
			}

			NSArray lesEcritures = new NSArray();
			KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
			try {
				lesEcritures = myFactoryProcessVisaTitre.numeroterUnBordereauDeTitre(getEditingContext(), selectedBordereau, myKFactoryNumerotation, myApp.wantShowTrace());
			} catch (Exception e) {
				System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU...");
				e.printStackTrace();
				throw new DefaultClientException("Erreur lors de la numérotation des écritures du bordereau. Les écritures ont été générées mais non numérotées, et le bordereau n'a pas été passé coté ordonnateur. : \n" + e.getMessage());
			}

			boolean wantImprimer = false;

			String msgEcritures = "";
			if (lesEcritures != null && lesEcritures.count() > 0) {
				lesEcritures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcritures, new NSArray(new EOSortOrdering("ecrNumero", EOSortOrdering.CompareAscending)));
				NSMutableArray lesEcrituresNumeros = new NSMutableArray();
				for (int i = 0; i < lesEcritures.count(); i++) {
					EOEcriture element = (EOEcriture) lesEcritures.objectAtIndex(i);
					lesEcrituresNumeros.addObject(element.ecrNumero());
				}
				msgEcritures = "Les écritures suivantes ont été générées : " + lesEcrituresNumeros.componentsJoinedByString(",") + "\n";
			}

			String msgFin = "Opération de visa du bordereau réussie.\n\n";
			msgFin = msgFin + msgEcritures;

			ServerProxy.clientSideRequestAfaireApresTraitementVisaBordereau(getEditingContext(), selectedBordereau);

			if (myEOBordereauRejet != null) {
				getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
						myEOBordereauRejet
				})));
			}
			if (myEOBordereauRejet != null) {
				msgFin = msgFin + "Le bordereau de rejet généré porte le numéro " + myEOBordereauRejet.brjNum();
				msgFin = msgFin + " \nSouhaitez-vous l'imprimer ?";
				wantImprimer = showConfirmationDialog("Visa réussi", msgFin, ZMsgPanel.BTLABEL_YES);
			}
			else {
				msgFin = msgFin + "Aucun bordereau de rejet n'a été généré";
				showInfoDialog(msgFin);
			}

			if (wantImprimer) {
				try {
					String filePath = ReportFactoryClient.imprimerBttna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(myEOBordereauRejet));
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}

			getEditingContext().revert();

			selectedBordereau = null;

			currentPanel = myBordereauSelectPanel;
			myBordereauSelectPanel.updateData();
			myChangeStepListener.stepHasChanged();

			//on sort de l'assistant
			//            myChangeStepListener.onCloseAsked();
		} catch (Exception e) {
			showErrorDialog(e);
			myChangeStepListener.onCloseAsked();
		} finally {
			getEditingContext().revert();
			setWaitCursor(false);
		}
	}

}
