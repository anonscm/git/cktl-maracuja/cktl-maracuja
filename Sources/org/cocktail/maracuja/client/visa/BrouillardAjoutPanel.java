/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Component;
import java.text.Format;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;


public class BrouillardAjoutPanel extends ZKarukeraPanel {

	private final IBrouillardAjoutPanelListener _listener;

	protected ZFormPanel pcoNum;
	protected final ZLookupButton pcoSelectButton;

	protected ZFormPanel montant;

	protected JComboBox myGestionComboBox;
	protected JComboBox mySensComboBox;
	protected JLabel pcoLibelle = new JLabel(" ");

	public BrouillardAjoutPanel(final IBrouillardAjoutPanelListener listener) {
		_listener = listener;

		pcoSelectButton = new ZLookupButton(_listener.getLookupButtonCompteModel(), _listener.getLookupButtonCompteListener());
	}

	public void initGUI() {
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		pcoSelectButton.initGUI();

		this.add(buildFormPanel(), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
	}

	public void updateData() throws Exception {
		montant.updateData();
		pcoSelectButton.updateData();
		updatePcoNum();
		updatePcoLibelle();
	}

	public void updatePcoLibelle() {
		if ((EOPlanComptable) _listener.getFilters().get(EOMandatBrouillard.PLAN_COMPTABLE_KEY) != null) {
			pcoLibelle.setText(((EOPlanComptable) _listener.getFilters().get(EOMandatBrouillard.PLAN_COMPTABLE_KEY)).pcoLibelle());
		}
		else {
			pcoLibelle.setText("");
		}
	}

	public void updatePcoNum() throws Exception {
		//        System.out.println("dico=" + _listener.getFilters());
		pcoNum.updateData();
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(_listener.actionValider());
		a.add(_listener.actionClose());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraPanel.buildHorizontalButtonsFromActions(a));
		return p;
	}

	protected JPanel buildFormPanel() {
		pcoNum = ZFormPanel.buildLabelField("Compte", _listener.getPcoNumModel());
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(10);

		montant = ZFormPanel.buildLabelField("Montant", new ZNumberField(new ZTextField.DefaultTextFieldModel(_listener.getFilters(), "montant"), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER
		}, ZConst.FORMAT_DECIMAL_COURT));
		((ZTextField) montant.getMyFields().get(0)).getMyTexfield().setColumns(10);

		myGestionComboBox = new JComboBox(_listener.getGestionModel());
		mySensComboBox = new JComboBox(_listener.getSensModel());

		//        myGestionComboBox.addActionListener(new ComboGestionListener());

		final JPanel p = new JPanel(new BorderLayout());
		final Component[] comps = new Component[5];
		comps[0] = buildLine(new Component[] {
				new JLabel("Code gestion"), Box.createHorizontalStrut(4), myGestionComboBox
		});
		comps[1] = buildLine(new Component[] {
				new JLabel("Sens"), Box.createHorizontalStrut(4), mySensComboBox
		});
		comps[2] = buildLine(new Component[] {
				pcoNum, pcoSelectButton
		});
		comps[3] = buildLine(new Component[] {
				pcoLibelle
		});
		comps[4] = buildLine(montant);

		p.add(ZKarukeraPanel.buildColumn(comps), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()));

		final JPanel p2 = new JPanel(new BorderLayout());
		p2.add(p, BorderLayout.NORTH);
		p2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p2;

	}

	//    private final class ComboGestionListener implements ActionListener {
	//        public void actionPerformed(ActionEvent e) {
	//           _listener.getFilters().put("gestion"  , _listener.getGestionModel().getSelectedEObject()) ;
	//        }
	//    }    

	public interface IBrouillardAjoutPanelListener {
		public Action actionValider();

		public ZTextField.IZTextFieldModel getPcoNumModel();

		public ZEOComboBoxModel getGestionModel();

		public DefaultComboBoxModel getSensModel();

		public Action actionClose();

		public Map getFilters();

		public IZLookupButtonModel getLookupButtonCompteModel();

		public IZLookupButtonListener getLookupButtonCompteListener();
	}

}
