/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOLot;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMarche;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DepenseListePanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;
	private TableSorter myTableSorter;
	private IDepenseListePanelListener myListener;

	private HashMap checkedDepenses;

	private ZEOTableModelColumnDico col0;

	private final HashMap marchesProxy = new HashMap();
	private final HashMap lotsProxy = new HashMap();

	/**
	 * @param context
	 */
	public DepenseListePanel(IDepenseListePanelListener vListener, EOEditingContext context) {
		super();
		setEditingContext(context);
		this.myListener = vListener;
		myDisplayGroup = new EODisplayGroup();

		myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "Depense"));
		((EODistributedDataSource) myDisplayGroup.dataSource()).setEditingContext(getEditingContext());

	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private void initTableModel() {
		checkedDepenses = myListener.getAllCheckedDepenses();
		Vector myCols2 = new Vector(6, 0);
		col0 = new ZEOTableModelColumnDico(myDisplayGroup, "Viser", 50, checkedDepenses);
		//		ZEOTableModelColumnCheckBox col0 = new ZEOTableModelColumnCheckBox(myDisplayGroup,"Viser",60,Boolean.TRUE);
		col0.setEditable(true);
		col0.setResizable(false);
		col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_NUMERO_KEY, "N°", 70);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);

		//		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"depDateReception","Date réception",100);
		//		col2.setFormatDisplay(ZConst.FORMAT_DATESHORT) ;
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, EODepense.RIB_KEY + "." + EORib.RIBCOMPLET_KEY, "Rib", 340);
		col2.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colLot = new ZEOTableModelColumnWithProvider("Lot", new ColLotProvider(), 107);
		colLot.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colMarche = new ZEOTableModelColumnWithProvider("Marché", new ColMarcheProvider(), 137);
		colMarche.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_HT_KEY, "Montant HT", 90);
		col3.setAlignment(SwingConstants.RIGHT);
		col3.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col3.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_TVA_KEY, "Montant TVA", 90);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_TTC_KEY, "Montant TTC", 90);
		col5.setAlignment(SwingConstants.RIGHT);
		col5.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col5.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col7 = new ZEOTableModelColumn(myDisplayGroup, EODepense.ACCORD_CONTRAT_UNIQUE_KEY + "." + EOAccordsContrat.NUMERO_KEY, "Convention", 150);
		col7.setColumnClass(String.class);
		col7.setAlignment(SwingConstants.LEFT);

		myCols2.add(col0);
		myCols2.add(col1);
		myCols2.add(col2);
		myCols2.add(colMarche);
		myCols2.add(colLot);
		myCols2.add(col7);
		myCols2.add(col3);
		myCols2.add(col4);
		myCols2.add(col5);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols2);
		myTableSorter = new TableSorter(myTableModel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		return;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		//		ZLogger.debug("selection depense changee");
		myListener.onDepenseSelectionChanged();
	}

	public void updateData() {
		//		ZLogger.debug("Mise a jour de la liste des depenses");
		try {
			marchesProxy.clear();
			lotsProxy.clear();
			if (myListener.getSelectedMandat() != null) {
				EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("depNumero", EOSortOrdering.CompareAscending);
				NSArray res = myListener.getSelectedMandat().depenses();
				if (res != null) {
					res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(sort));
				}

				myDisplayGroup.setObjectArray(res);
				for (int i = 0; i < res.count(); i++) {
					EODepense element = (EODepense) res.objectAtIndex(i);
					if (!(element.depMarches() == null || element.depMarches().trim().length() == 0)) {
						Integer marcheOrdre = new Integer(element.depMarches());
						EOMarche marche = myListener.getMarcheForMarOrdre(marcheOrdre);
						marchesProxy.put(element, marche);
					}
					if (!(element.depLot() == null || element.depLot().trim().length() == 0)) {
						Integer lotOrdre = new Integer(element.depLot());
						EOLot lot = myListener.getLotForLotOrdre(lotOrdre);
						lotsProxy.put(element, lot);
					}
				}
			}
			else {
				myDisplayGroup.setObjectArray(new NSArray());
			}

			myEOTable.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		initTableModel();
		initTable();
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	public interface IDepenseListePanelListener {
		public EOMandat getSelectedMandat();

		public void onDepenseSelectionChanged();

		/**
		 * Doit renvoyer l'ensemble des depenses cochees (indépendamment du mandat, ceci afin de garantir l'historique).
		 */
		public HashMap getAllCheckedDepenses();

		/**
		 * @param lotOrdre
		 * @return
		 */
		public EOLot getLotForLotOrdre(Integer lotOrdre);

		/**
		 * @param marcheOrdre
		 * @return
		 */
		public EOMarche getMarcheForMarOrdre(Integer marcheOrdre);

	}

	/**
	 * Renvoie la dépense sélectionnée.
	 */
	public EODepense getSelectedDepense() {
		return (EODepense) myDisplayGroup.selectedObject();
	}

	/**
	 * Renvoie les depenses cochés
	 */
	public ArrayList checkedDepenses() {
		return myTableModel.getRowsForValueAtCol(Boolean.TRUE, 0);
	}

	public void fireTableDataChanged() {
		myTableModel.fireTableDataChanged();
	}

	/**
	 * @return
	 */
	public ZEOTableModelColumnDico getCol0() {
		return col0;
	}

	private final class ColLotProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			final EODepense dep = (EODepense) myDisplayGroup.displayedObjects().objectAtIndex(row);
			final EOLot lot = (EOLot) lotsProxy.get(dep);
			if (lot == null) {
				return "";
			}
			return lot.lotIndex() + " - " + lot.lotLibelle();
		}
	}

	private final class ColMarcheProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			EODepense dep = (EODepense) myDisplayGroup.displayedObjects().objectAtIndex(row);
			EOMarche marche = (EOMarche) marchesProxy.get(dep);
			if (marche == null) {
				return "";
			}
			return marche.marAnnee() + " - " + marche.marIndex();
		}
	}

}
