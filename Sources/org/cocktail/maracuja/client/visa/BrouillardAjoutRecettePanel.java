/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Component;
import java.text.Format;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;


public class BrouillardAjoutRecettePanel extends BrouillardAjoutPanel {

	private final IBrouillardAjoutRecettePanelListener _listener;
	private JComboBox myRecetteComboBox;

	public BrouillardAjoutRecettePanel(final IBrouillardAjoutRecettePanelListener listener) {
		super(listener);
		_listener = listener;
	}

	public void initGUI() {
		super.initGUI();
	}

	public void updateData() throws Exception {
		super.updateData();
	}

	protected final JPanel buildFormPanel() {
		pcoNum = ZFormPanel.buildLabelField("Compte", _listener.getPcoNumModel());
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(10);

		montant = ZFormPanel.buildLabelField("Montant", new ZNumberField(new ZTextField.DefaultTextFieldModel(_listener.getFilters(), "montant"), new Format[] {
			ZConst.FORMAT_EDIT_NUMBER
		}, ZConst.FORMAT_DECIMAL_COURT));
		((ZTextField) montant.getMyFields().get(0)).getMyTexfield().setColumns(10);

		myGestionComboBox = new JComboBox(_listener.getGestionModel());
		mySensComboBox = new JComboBox(_listener.getSensModel());
		myRecetteComboBox = new JComboBox(_listener.getRecettesModel());

		final JPanel p = new JPanel(new BorderLayout());
		final Component[] comps = new Component[6];
		comps[0] = buildLine(new Component[] {
				new JLabel("Code gestion"), Box.createHorizontalStrut(4), myGestionComboBox
		});
		comps[1] = buildLine(new Component[] {
				new JLabel("Sens"), Box.createHorizontalStrut(4), mySensComboBox
		});
		comps[2] = buildLine(new Component[] {
				pcoNum, pcoSelectButton
		});
		comps[3] = buildLine(new Component[] {
			pcoLibelle
		});
		comps[4] = buildLine(montant);
		comps[5] = buildLine(new Component[] {
				new JLabel("Recettte associée"), Box.createHorizontalStrut(4), myRecetteComboBox
		});

		p.add(ZKarukeraPanel.buildColumn(comps), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()));

		final JPanel p2 = new JPanel(new BorderLayout());
		p2.add(p, BorderLayout.NORTH);
		p2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p2;

	}

	public interface IBrouillardAjoutRecettePanelListener extends IBrouillardAjoutPanelListener {

		public ZEOComboBoxModel getRecettesModel();

	}

}
