/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * Liste des titres acceptés.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TitreAcceptesListePanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {
    private ZEOTable myEOTable;
    private ZEOTableModel myTableModel;
    private EODisplayGroup myDisplayGroup;
//    private EODisplayGroup dgCombo;

    private TableSorter myTableSorter;
    private ITitreAcceptesListePanelListener myListener;
    private EOEditingContext ec;

    private Vector myCols;
//    private ZEOTableModelColumnComboBoxWithRelation colCombo;

    public TitreAcceptesListePanel(ITitreAcceptesListePanelListener vListener, EOEditingContext ec) {
        super();
        setEditingContext(ec);
        this.myListener = vListener;
        myDisplayGroup = new EODisplayGroup();
//        dgCombo = new EODisplayGroup();
        myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "Titre"));        
        ((EODistributedDataSource)myDisplayGroup.dataSource()).setEditingContext(getEditingContext());
//        dgCombo.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "ModeRecouvrement"));
//        ((EODistributedDataSource)dgCombo.dataSource()).setEditingContext(getEditingContext());        
        
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cocktail.maracuja.client.ZKarukeraPanel#getEditingContext()
     */
    public EOEditingContext getEditingContext() {
        return ec;
    }

    public void setEditingContext(EOEditingContext pec) {
        ec = pec;
    }

    /**
     * Initialise la table à afficher (le modele doit exister)
     */
    private void initTable() {
        myEOTable = new ZEOTable(myTableSorter);

        myEOTable.addListener(this);
        myTableSorter.setTableHeader(myEOTable.getTableHeader());
    }

    /**
     * Initialise le modeele le la table à afficher.
     *  
     */
    private void initTableModel() {
        myCols = new Vector(8, 0);

        ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "titNumero", "N°", 64);
        col1.setAlignment(SwingConstants.CENTER);
        col1.setColumnClass(Integer.class);
        
        ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoNum", "Imp.", 88);
        col11.setAlignment(SwingConstants.LEFT);
        ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, "titOrigineLib", "Origine", 300);
        col2.setAlignment(SwingConstants.LEFT);
        ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup, "titHt", "Montant HT", 80);
        col32.setAlignment(SwingConstants.RIGHT);
        col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col32.setColumnClass(BigDecimal.class);
        
        ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "titTva", "Montant TVA", 80);
        col3.setAlignment(SwingConstants.RIGHT);
        col3.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col3.setColumnClass(BigDecimal.class);
        
        ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "titTtc", "Montant TTC", 80);
        col4.setAlignment(SwingConstants.RIGHT);
        col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col4.setColumnClass(BigDecimal.class);
        
//        ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, "manDateRemise", "Date remise", 100);
//        col5.setFormatDisplay(ZConst.FORMAT_DATESHORT);

        myCols.add(col1);
//        myCols.add(col5);

        ZEOTableModelColumn colModeRecouvrement =  new ZEOTableModelColumn(myDisplayGroup, "modeRecouvrement.modLibelle", "Mode recouvrement", 80);
        ZEOTableModelColumn colModePaiement =  new ZEOTableModelColumn(myDisplayGroup, "modePaiement.modLibelle", "Mode Paiement", 80);
        
        
//        EODisplayGroup dgCombo = new EODisplayGroup();

//        try {
//            colCombo = new ZEOTableModelColumnComboBoxWithRelation(myDisplayGroup, "modeRecouvrement.morLibelle", "Mode de recouvrement*", dgCombo, "morLibelle", "modeRecouvrement", null, null, 258);
//            colCombo.setEditable(true);
//            colCombo.setMyModifier(new ModifRecouvrementModifier());
//            myCols.add(colCombo);
//        } catch (Exception e) {
//            myApp.showErrorDialog(e);
//            return;
//        }
        
        
        
        myCols.add(col11);
        myCols.add(colModePaiement);
        myCols.add(colModeRecouvrement);
        
        myCols.add(col2);
        myCols.add(col32);
        myCols.add(col3);
        myCols.add(col4);
        
        
        
        myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
        myTableSorter = new TableSorter(myTableModel);

//        myTableModel.addTableModelListener(this);
    }

    public EOTitre getSelectedTitre() {
        return (EOTitre) myDisplayGroup.selectedObject();
    }



    /*
     * (non-Javadoc)
     * 
     * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        return;

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.onTitreSelectionChanged();
    }

    public interface ITitreAcceptesListePanelListener {
        public ArrayList getTitresAViser();

        public void onTitreSelectionChanged();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        ZLogger.debug("");
        initTableModel();
        initTable();

        this.setLayout(new BorderLayout());
        this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.cocktail.maracuja.client.ZKarukeraPanel#updateData()
     */
    public void updateData() throws DefaultClientException {
        myEOTable.removeEditor();
        ZLogger.debug("<------------ICI");
//        dgCombo.setObjectArray(EOsFinder.fetchArray(getEditingContext(), "ModeRecouvrement", "", null, null, false));
//        colCombo.updateComboWithData(dgCombo, "modLibelle", null, null);
        
        //Les titres sont stockés dans le nestedEditingcontext (local)
        ArrayList tmp = myListener.getTitresAViser();
        if (tmp.size() > 0) {
            myDisplayGroup.setObjectArray(new NSArray(tmp.toArray()));
        }
        ZLogger.debug("nb titres a viser :" + tmp.size());
//        myTableModel.updateInnerRowCount();
//        myTableModel.fireTableDataChanged();
        myEOTable.updateData();
    }

    /**
     * @return
     */
    public EODisplayGroup getMyDisplayGroup() {
        return myDisplayGroup;
    }
    
//    /**
//     * Utiliser pour la mide à jour des modes de recouvrement.  
//     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
//     */
//    private class ModifRecouvrementModifier implements ZEOTableModelColumn.Modifier {
//        /**
//         * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
//         */
//        public void setValueAtRow(Object value, int row) {
//            EOModeRecouvrement val = (EOModeRecouvrement)value;
//            EOTitre titre = (EOTitre) myDisplayGroup.displayedObjects().objectAtIndex(row);
//            try {
//                FactoryProcessVisaModifTitre myFactoryProcessVisaModifTitre = new FactoryProcessVisaModifTitre(myApp.showFactoryLogs());
//                myFactoryProcessVisaModifTitre.modifierModeDePaiement(getEditingContext(),myApp.appUserInfo().getUtilisateur(),titre,val);
//                ZLogger.debug("Mode de recouvrement modifie");
//            } catch (Exception e) {
//                myApp.showErrorDialog(e);
//            }
//        }
//        
//    }
    
    

}
