/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;

import org.cocktail.fwkcktlcompta.common.util.ZDateUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factories.ZFactory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaMandat;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOJdDepenseCtrlPlanco;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.remotecall.ServerCallDatabase;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSRange;
import com.webobjects.foundation.NSTimestamp;

/**
 * Affiche la liste des bordereaux à viser.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaMandatPanel extends ZKarukeraPanel {
	private ABordereauSelectPanel myBordereauSelectPanel;
	private VisaMandatStepDispatchPanel myBordereauDispatch;
	private VisaMandatStepDetailPanel myBordereauDetailVises;
	private VisaMandatStepRecapPanel myBordereauRecap;

	private BordereauSelectPanelListener myBordereauSelectPanelListener;
	private BordereauAViserDispatchPanelListener myBordereauAViserDispatchPanelListener;
	private BordereauDetailVisesListener myBordereauDetailVisesListener;
	private BordereauRecapListener myBordereauRecapListener;

	private ZKarukeraStepPanel currentPanel;

	public int panelWidth = 1000;
	public int topHeight = 100;
	public int minContentHeight;
	public int bottomHeight = 50;
	public static final Dimension MINDIMENSION = new Dimension(1000, 700);
	public ChangeStepListener myChangeStepListener;

	// private EOGestion selectedGestion;
	private EOBordereau selectedBordereau;

	private Date dateVisa;

	private AbstractAction specialAction1;

	public VisaMandatPanel() throws Exception {
		super();
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}

		specialAction1 = new ActionViser();


		myBordereauSelectPanelListener = new BordereauSelectPanelListener();
		myBordereauDetailVisesListener = new BordereauDetailVisesListener();
		myBordereauAViserDispatchPanelListener = new BordereauAViserDispatchPanelListener();
		myBordereauRecapListener = new BordereauRecapListener();

		myBordereauSelectPanel = new VisaMandatStepBordereauSelectPanel();
		myBordereauSelectPanel.setMyListener(myBordereauSelectPanelListener);
		myBordereauDispatch = new VisaMandatStepDispatchPanel(myBordereauAViserDispatchPanelListener);
		myBordereauDetailVises = new VisaMandatStepDetailPanel(myBordereauDetailVisesListener);
		myBordereauRecap = new VisaMandatStepRecapPanel(myBordereauRecapListener);
		myBordereauRecap.setEditingContext(myBordereauDetailVises.getEditingContext());

		// initGUI(); //appelé dans initGUI() parent
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		this.setPreferredSize(MINDIMENSION);
		myBordereauSelectPanel.initGUI();
		myBordereauDispatch.initGUI();
		myBordereauDetailVises.initGUI();
		myBordereauRecap.initGUI();
		// myBordereauRecap.initGUI();

		myBordereauSelectPanel.setMyDialog(getMyDialog());
		myBordereauDispatch.setMyDialog(getMyDialog());
		myBordereauDetailVises.setMyDialog(getMyDialog());
		myBordereauRecap.setMyDialog(getMyDialog());

		// Premier panel actif
		currentPanel = myBordereauSelectPanel;
	}

	/**
	 * Met à jour le panel actif
	 */
	public void updateGUI() {
		// Supprimer l'éventuel panel
		this.removeAll();

		this.setLayout(new BorderLayout());
		// Ajouter le panel en cours
		this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		this.add(currentPanel, BorderLayout.CENTER);
		currentPanel.onDisplay();
	}

	public void notifyListeners() {
		if (myChangeStepListener != null) {
			myChangeStepListener.stepHasChanged();
		}
		else {
			ZLogger.debug("Pas de listener... bizarre...");
		}
	}

	/**
	 * Interface pour les listeners du wizard (normalement le dialogue parent).
	 */
	public interface ChangeStepListener {
		public void stepHasChanged();

		public void onCloseAsked();
	}

	/**
	 * @return
	 */
	public ChangeStepListener getMyChangeStepListener() {
		return myChangeStepListener;
	}

	/**
	 * @param listener
	 */
	public void setMyChangeStepListener(ChangeStepListener listener) {
		myChangeStepListener = listener;
	}

	/**
	 * Listener pour le panneau de selection des bordereaux.
	 */
	private class BordereauSelectPanelListener implements ABordereauSelectPanel.IBordereauSelectPanelListener {
		public void onNewSelectedGestion(EOGestion newGestion) {
			// selectedGestion = newGestion;
		}

		public void onNext() {
			// if (selectedBordereau !=
			// myBordereauSelectPanel.selectedBordereau()) {
			selectedBordereau = myBordereauSelectPanel.selectedBordereau();
			ZLogger.debug("Num bordereau selectionné", selectedBordereau.borNum());

			if (!isBordereauPeutEtreVisePourExtourne(selectedBordereau)) {
				showInfoDialog("Ce bordereau contient des mandats avec des dépenses à extourner. Vous ne pourrez pas viser ces mandats tant que l'exercice " + (myApp.appUserInfo().getCurrentExercice().exeExercice().intValue() + 1) + " n'est pas créé. Vous pouvez par contre les rejeter.");
			}

			try {
				myBordereauDispatch.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
			currentPanel = myBordereauDispatch;
			myChangeStepListener.stepHasChanged();
		}

		public void onPrev() {
			return;

		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			return getSize();
		}

		public int etapeNumber() {
			return 1;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #getCommentaireDimension()
		 */
		public Dimension getCommentaireDimension() {
			return new Dimension(getSize().width, topHeight);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}
	}

	/**
	 * Listener pour le panneau de dispatching des bordereaux.
	 */
	private class BordereauAViserDispatchPanelListener implements VisaMandatStepDispatchPanel.IBordereauAViserDispatchPanelListener {
		public void onNext() {
			try {
				getMyDialog().setWaitCursor(true);
				currentPanel = myBordereauDetailVises;
				myBordereauDetailVises.updateData();
				myChangeStepListener.stepHasChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				getMyDialog().setWaitCursor(false);
			}
		}

		public void onPrev() {
			try {
				currentPanel = myBordereauSelectPanel;
				myChangeStepListener.stepHasChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			// return MINDIMENSION;
			return getSize();

		}

		public int etapeNumber() {
			return 2;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @seeorg.cocktail.maracuja.client.BordereauAViserDispatchPanel. IBordereauAViserDispatchPanelListener#getSelectedBordereau()
		 */
		public EOBordereau getSelectedBordereau() {
			return selectedBordereau;
		}

		public Date getDateReception() {
			return dateVisa;
		}

		public void setDateReception(Date newDate) {
			dateVisa = newDate;
			ZLogger.debug("dateVisa", newDate);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @seeorg.cocktail.maracuja.client.BordereauAViserDispatchPanel. IBordereauAViserDispatchPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #getCommentaireDimension()
		 */
		public Dimension getCommentaireDimension() {
			return new Dimension(getSize().width, topHeight);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}
	}

	private class BordereauDetailVisesListener implements VisaMandatStepDetailPanel.IBordereauDetailVisesListener {
		public ArrayList getMandatsAViser() {
			return VisaMandatPanel.this.getMandatsAViser();
		}

		// public Dialog getDialog() {
		// return null;
		// }

		public void onNext() {
			try {
				getMyDialog().setWaitCursor(true);
				checkComptesValides(getMandatsAViser());
				currentPanel = myBordereauRecap;
				myBordereauRecap.updateData();
				myChangeStepListener.stepHasChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				getMyDialog().setWaitCursor(false);
			}
		}

		public void onPrev() {
			getMyDialog().setWaitCursor(true);
			currentPanel = myBordereauDispatch;
			myChangeStepListener.stepHasChanged();
			getMyDialog().setWaitCursor(false);
		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			return getSize();
		}

		/**
		 * L'EditingContext du panel de dispatch
		 * 
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel.ZKarukeraStepListener#getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return myBordereauDispatch.getEditingContext();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}

	}

	private class BordereauRecapListener implements VisaMandatStepRecapPanel.IBordereauRecapListener {
		public ArrayList getMandatsAViser() {
			return VisaMandatPanel.this.getMandatsAViser();
		}

		// public Dialog getDialog() {
		// return null;
		// }

		public void onNext() {
			// currentPanel = myBordereauDispatch;
			myChangeStepListener.stepHasChanged();
		}

		public void onPrev() {
			currentPanel = myBordereauDetailVises;
			myChangeStepListener.stepHasChanged();
		}

		public void onClose() {
			myChangeStepListener.onCloseAsked();
		}

		public Dimension getPanelDimension() {
			return getSize();
		}

		/**
		 * L'EditingContext du panel précédent
		 * 
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel.ZKarukeraStepListener#getParentEditingContext()
		 */
		public EOEditingContext getParentEditingContext() {
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.BordereauRecapPanel.IBordereauRecapListener #getBordrereau()
		 */
		public EOBordereau getBordereau() {
			return selectedBordereau;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.BordereauRecapPanel.IBordereauRecapListener #getMandatsARejeter()
		 */
		public ArrayList getMandatsARejeter() {
			return VisaMandatPanel.this.getMandatsARejeter();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel.ZKarukeraStepListener #specialAction1()
		 */
		public AbstractAction specialAction1() {
			return specialAction1;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		currentPanel.updateData();
	}

	/**
	 * @return
	 */
	public ArrayList<EOMandat> getMandatsARejeter() {
		return myBordereauDispatch.getMandatsARejeter();
	}

	/**
	 * @return
	 */
	public ArrayList<EOMandat> getMandatsAViser() {
		return myBordereauDispatch.getMandatsAViser();
	}

	private class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SHORT_DESCRIPTION, "Lancer le visa du bordereau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_TRAITEMENT_16));
		}

		public void actionPerformed(ActionEvent e) {
			traiterViser();
		}

	}

	public void checkComptesValides(ArrayList mandatsVises) throws Exception {
		NSArray plancoValides = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), myApp.appUserInfo().getCurrentExercice(), null, false);
		if (plancoValides.count() == 0) {
			throw new DataCheckException("Erreur : la liste des plan comptable valides est vide, impossible de controler la validité des comptes. Cette erreur est vraisemblablement dûe a une perte de connexion temporaire. Veuillez relancer l'application");
		}

		for (int i = 0; i < mandatsVises.size(); i++) {
			final EOMandat element = (EOMandat) mandatsVises.get(i);

			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("mabOperation<>%@", new NSArray(new Object[] {
					EOMandatBrouillard.VISA_ANNULER
			}));
			NSArray mabs = EOQualifier.filteredArrayWithQualifier(element.mandatBrouillards(), qual);

			for (int j = 0; j < mabs.count(); j++) {
				EOPlanComptable planco = ((EOMandatBrouillard) mabs.objectAtIndex(j)).planComptable();
				EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PLAN_COMPTABLE_KEY + "=%@", new NSArray(new Object[] {
						planco
				}));
				if (EOQualifier.filteredArrayWithQualifier(plancoValides, qual2).count() == 0) {
					ZLogger.verbose("Verification du compte " + planco.pcoNum() + " -> NON VALIDE");
					throw new DataCheckException("Le compte " + planco.pcoNum() + " associée au mandat n° " + element.manNumero() + " n'est pas valide.");
				}
				ZLogger.verbose("Verification du compte " + planco.pcoNum() + " -> VALIDE");
			}
		}
	}

	/**
	 * Vérifie que les mandats avec un mode de paiement HT VIREMENT n'ont pas un montant HT à 0.
	 * 
	 * @param mandatsVises
	 * @throws Exception
	 */
	public void checkPaiementHT0(ArrayList<EOMandat> mandatsVises) throws Exception {
		for (int i = 0; i < mandatsVises.size(); i++) {
			final EOMandat element = (EOMandat) mandatsVises.get(i);
			if (element.modePaiement().isPaiementHT() && EOModePaiement.MODDOM_VIREMENT.equals(element.modePaiement().modDom()) && element.manHt().doubleValue() == 0d) {
				throw new DataCheckException("Le mandat n°" + element.manNumero() + " a un mode de paiement HT, or son montant HT est à 0.");
			}
		}
	}

	public boolean checkPlusieursConventionsParMandat(ArrayList<EOMandat> mandatsVises) throws Exception {
		if (!wantRecupConvention()) {
			return true;
		}

		boolean goOn = true;
		for (int i = 0; goOn && i < mandatsVises.size(); i++) {
			final EOMandat mandat = (EOMandat) mandatsVises.get(i);
			if (mandat.getAccordContratLies().count() > 1) {
				goOn = myApp.showConfirmationDialog("Confirmation", "Le mandat n° " + mandat.manNumero() + " contient des dépenses qui font référence à plusieurs conventions. Si vous visez ce mandat, les écritures ne seront pas reliées aux conventions. " +
						"Un mandat doit être affecté à une seule convention (pour le montant total du mandat) pour que les écritures soient reliées à cette convention. \n Souhaitez-vous quand même accepter ce mandat ?", ZMsgPanel.BTLABEL_NO);
			}
		}
		return goOn;
	}

	public boolean checkConventionsDateFinPaiementDepassee(ArrayList<EOMandat> mandatsVises) throws Exception {
		boolean goOn = true;
		String msg = "";
		for (int i = 0; goOn && i < mandatsVises.size(); i++) {
			final EOMandat mandat = (EOMandat) mandatsVises.get(i);
			if (!mandat.isSurCreditExtourne()) {
				for (int j = 0; j < mandat.getAccordContratLies().count(); j++) {
					EOAccordsContrat contrat = (EOAccordsContrat) mandat.getAccordContratLies().objectAtIndex(j);
					if (contrat.conDateFinPaiement() != null && contrat.conDateFinPaiement().before(ZDateUtil.addDHMS(new Date(), -1, 0, 0, 0))) {
						msg += "Le mandat n° " + mandat.gescodeAndNum() + " ne peut pas être visé car la convention associée n° " + contrat.numero() + "  a une date de fin de " +
								"paiement dépassée (" + ZConst.FORMAT_DATESHORT.format(contrat.conDateFinPaiement()) + ").";
					}
				}
			}
		}
		if (msg.length() > 0) {
			throw new Exception(msg);
		}
		return goOn;
	}

	private boolean wantRecupConvention() {
		String res = ServerProxy.clientSideRequestGetGrhumParam(getEditingContext(), "org.cocktail.gfc.maracuja.recupconventionpourmandat");
		if (res == null) {
			res = "N";
		}
		return "O".equals(res);
	}

	public boolean checkConventionAvecRepartitionPartielleParMandat(ArrayList<EOMandat> mandatsVises) throws Exception {
		if (!wantRecupConvention()) {
			return true;
		}
		boolean goOn = true;
		for (int i = 0; goOn && i < mandatsVises.size(); i++) {
			final EOMandat mandat = (EOMandat) mandatsVises.get(i);
			if (mandat.getAccordContratUnique() != null && !mandat.getMontantAccordContratLies().equals(mandat.manHt())) {
				goOn = myApp.showConfirmationDialog("Confirmation", "Le mandat n° " + mandat.manNumero() + " est affecté partiellement à la convention n° " + mandat.getAccordContratUnique().numero() + " (" + ZConst.FORMAT_DECIMAL.format(mandat.getMontantAccordContratLies()) + "/"
						+ ZConst.FORMAT_DECIMAL.format(mandat.manHt()) + "). Si vous visez ce mandat, les écritures ne seront pas reliées à la convention. " +
						"Un mandat doit être affecté à une seule convention (pour le montant total du mandat) pour que les écritures soient reliées à cette convention. \n Souhaitez-vous quand même accepter ce mandat ?", ZMsgPanel.BTLABEL_NO);
			}
		}
		return goOn;
	}


	/**
	 * Effectue les traitements de visa
	 */
	private final void traiterViser() {
		if (!myApp.showConfirmationDialog("Confirmation", "Souhaitez vous réellement viser le bordereau " + selectedBordereau.borNum() + "?\n cette opération est irréversible.", ZMsgPanel.BTLABEL_NO)) {
			return;
		}
		try {
			ServerCallDatabase.clientSideRequestCleanPendingOperation(getEditingContext());
			try {
				if (!currentPanel.valideSaisie()) {
					return;
				}
				// verifier si les comptes sont valides
				checkComptesValides(getMandatsAViser());
				checkPaiementHT0(getMandatsAViser());
				if (!checkPlusieursConventionsParMandat(getMandatsAViser())) {
					return;
				}
				if (!checkConventionAvecRepartitionPartielleParMandat(getMandatsAViser())) {
					return;
				}
				if (!checkConventionsDateFinPaiementDepassee(getMandatsAViser())) {
					return;
				}

			} catch (Exception e) {
				showErrorDialog(e);
				return;
			}

			setWaitCursor(true);
			//On enregistre les modifs sur les nested
			if ((myBordereauDetailVises.getEditingContext() != null) && (myBordereauDetailVises.getEditingContext().hasChanges())) {
				ZLogger.debug("saveChanges sur myBordereauDetailVises");
				myBordereauDetailVises.getEditingContext().saveChanges();
			}
			//			if ((myBordereauDispatch.getEditingContext() != null) && (myBordereauDispatch.getEditingContext().hasChanges())) {
			//				ZLogger.debug("saveChanges sur myBordereauDispatch");
			//				myBordereauDispatch.getEditingContext().saveChanges();
			//			}

			final EOTypeJournal myTypeJournal = FinderJournalEcriture.leTypeJournalVisaMandat(getEditingContext());
			final EOTypeOperation myTypeOperation = FinderVisa.leTypeOperationVisaMandat(getEditingContext());

			final FactoryProcessVisaMandat myFactoryProcessVisaMandat = new FactoryProcessVisaMandat(getEditingContext(), myTypeJournal, myTypeOperation, myApp.wantShowTrace(), new NSTimestamp(myApp.getDateJourneeComptable()), wantRecupConvention());

			EOBordereauRejet myEOBordereauRejet = null;
			EOBordereau bordExtourne = null;
			try {
				// Recuperer les mandats pour qu'ils soient dans
				// l'editingContext
				final NSArray allmandatsLocal = selectedBordereau.mandats();
				ZFactory.enumererEObjets(allmandatsLocal);
				ArrayList tmp = ZFactory.localInstancesForObjects(getEditingContext(), getMandatsAViser());
				ZLogger.debug(tmp);
				NSArray mandatsVises = new NSArray(tmp.toArray(), new NSRange(0, tmp.size()));
				// Trier les mandats par n°
				final EOSortOrdering sortOrdering = EOMandat.SORT_MAN_NUMERO_ASC;
				mandatsVises = EOSortOrdering.sortedArrayUsingKeyOrderArray(mandatsVises, new NSArray(sortOrdering));

				tmp = ZFactory.localInstancesForObjects(getEditingContext(), getMandatsARejeter());
				NSArray mandatsRejetes = new NSArray(tmp.toArray(), new NSRange(0, tmp.size()));
				// Trier les mandats par n°
				mandatsRejetes = EOSortOrdering.sortedArrayUsingKeyOrderArray(mandatsRejetes, new NSArray(sortOrdering));

				checkMandatsForExtourne(mandatsVises);

				myEOBordereauRejet = myFactoryProcessVisaMandat.viserUnBordereauDeMandatEtNumeroter(getEditingContext(), myApp.appUserInfo().getUtilisateur(), selectedBordereau, mandatsRejetes, mandatsVises, new NSTimestamp(dateVisa), new KFactoryNumerotation(myApp.wantShowTrace()));

				NSDictionary dicoPk = ServerProxy.serverPrimaryKeyForObject(getEditingContext(), selectedBordereau);
				String msgEcritures = "";
				try {
					//Ajouter la procedure stockee pour execution en fin de transaction sql Apres traitement (inclure extourne + num ecriture + num bd rejet)
					ServerCallDatabase.clientSideRequestAddStoredProcedureAsOperation(getEditingContext(), "afaireaprestraitement.apres_visa_bordereau", "afaireaprestraitement.apres_visa_bordereau", dicoPk);

					//SAVE EN BASE
					//					this.myBordereauSelectPanel.getEditingContext().saveChanges();
					getEditingContext().saveChanges();

					getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
							selectedBordereau
					})));

					NSArray lesEcritures = FinderVisa.lesEcrituresDuBordereauMandat(getEditingContext(), selectedBordereau, myApp.wantShowTrace());
					getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), lesEcritures));

					//invalidate des autres objets 
					if (myEOBordereauRejet != null) {
						getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
								myEOBordereauRejet
						})));
					}

					if (lesEcritures != null && lesEcritures.count() > 0) {
						lesEcritures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcritures, new NSArray(new Object[] {
								EOEcriture.SORT_ECR_NUMERO_ASC
						}));
						NSMutableArray lesEcrituresNumeros = new NSMutableArray();
						for (int i = 0; i < lesEcritures.count(); i++) {
							EOEcriture element = (EOEcriture) lesEcritures.objectAtIndex(i);
							lesEcrituresNumeros.addObject(element.ecrNumero());
						}
						msgEcritures = "Les écritures suivantes ont été générées sur l'exercice " + selectedBordereau.exercice().exeExercice().intValue() + ": " + lesEcrituresNumeros.componentsJoinedByString(",") + "\n";

						if (selectedBordereau.isBordereauAExtourner()) {
							int exerciceExtourne = 0;
							NSMutableArray mandatsExtourne = new NSMutableArray();
							NSMutableArray ecrituresExtourne = new NSMutableArray();

							//invalider les mandats
							getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), selectedBordereau.mandats()));

							//recuperer les mandats d'extourne, puis leurs ecritures
							for (int i = 0; i < selectedBordereau.mandats().count(); i++) {
								//Modif suite msg forum 14/01/2014 (NPE sur rejet de mandat à extourner)
								if (!((EOMandat) selectedBordereau.mandats().objectAtIndex(i)).isAnnule() && ((EOMandat) selectedBordereau.mandats().objectAtIndex(i)).isMandatAExtourner()) {
									EOMandat mandatExtourne = ((EOMandat) selectedBordereau.mandats().objectAtIndex(i)).toExtourneMandatN().toMandatN1();
									if (exerciceExtourne == 0) {
										exerciceExtourne = mandatExtourne.exercice().exeExercice().intValue();
									}
									mandatsExtourne.addObject(mandatExtourne);
									NSArray ecritures = (NSArray) mandatExtourne.ecritureDetailVisa().valueForKey(EOEcritureDetail.ECRITURE_KEY);
									ecritures = ZEOUtilities.getDistinctsOfNSArray(ecritures);
									ecrituresExtourne.addObjectsFromArray(ecritures);
									bordExtourne = mandatExtourne.bordereau();
								}
							}
							NSArray ecrituresExtourneTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(ecrituresExtourne, new NSArray(new Object[] {
									EOEcriture.SORT_ECR_NUMERO_ASC
							}));
							NSMutableArray lesEcrituresExtourneNum = new NSMutableArray();
							for (int i = 0; i < ecrituresExtourneTriees.count(); i++) {
								EOEcriture element = (EOEcriture) ecrituresExtourneTriees.objectAtIndex(i);
								lesEcrituresExtourneNum.addObject(element.ecrNumero());
							}
							msgEcritures += "\nLes écritures suivantes ont été générées sur l'exercice " + exerciceExtourne + " pour réserver les crédits d'extourne: " + lesEcrituresExtourneNum.componentsJoinedByString(",") + "\n";
						}
					}

					String msgFin = "Opération de visa du bordereau réussie.\n\n";
					msgFin = msgFin + msgEcritures;
					if (myEOBordereauRejet != null) {
						msgFin = msgFin + "Le bordereau de rejet généré porte le numéro " + myEOBordereauRejet.brjNum() + ".";
					}
					else {
						msgFin = msgFin + "Aucun bordereau de rejet n'a été généré.";
					}

					boolean wantImprimer = false;
					if (myEOBordereauRejet != null) {
						msgFin = msgFin + " \nSouhaitez-vous imprimer le bordereau de rejet ?";
						wantImprimer = showConfirmationDialog("Visa réussi", msgFin, ZMsgPanel.BTLABEL_YES);
					}
					else {
						showInfoDialog(msgFin);
					}
					if (wantImprimer) {
						try {
							String filePath = ReportFactoryClient.imprimerBtmna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(myEOBordereauRejet));
							if (filePath != null) {
								myApp.openPdfFile(filePath);
							}
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}

					//imprimer le bordereau d'extourne
					if (bordExtourne != null) {
						wantImprimer = showConfirmationDialog("Confirmation", "Souhaitez-vous imprimer la liste des opérations extournées ?", ZMsgPanel.BTLABEL_YES);
						if (wantImprimer) {
							try {
								String filePath = ReportFactoryClient.imprimerBordereauExtourne(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(bordExtourne));
								if (filePath != null) {
									myApp.openPdfFile(filePath);
								}
							} catch (Exception e1) {
								showErrorDialog(e1);
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}

			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}

			getEditingContext().revert();
			selectedBordereau = null;
			currentPanel = myBordereauSelectPanel;
			myBordereauSelectPanel.updateData();
			myChangeStepListener.stepHasChanged();

			// Emargements semi-auto
			final EmargementAutoCtrl emargementAutoCtrl = new EmargementAutoCtrl(getEditingContext());
			emargementAutoCtrl.initData(new NSArray(getMandatsAViser().toArray()));
			if (emargementAutoCtrl.getDepensesPourEmargement().count() > 0) {
				emargementAutoCtrl.openDialog(getMyDialog());
			}
			else {
				ZLogger.debug("Pas d'emargement semi-automatique detecte");
			}
		} catch (Exception e) {
			showErrorDialog(e);
			myChangeStepListener.onCloseAsked();
		} finally {

			myBordereauRecap.getEditingContext().revert();
			myBordereauDetailVises.getEditingContext().revert();
			//getEditingContext().revert();
			try {
				ServerCallDatabase.clientSideRequestCleanPendingOperation(getEditingContext());
			} catch (Exception e) {
				e.printStackTrace();
				showErrorDialog(e);
			}

			//revert pour annuler les modifs eventuellement en attentes sur le serveur
			getEditingContext().revert();
			if (selectedBordereau != null) {
				getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(),
						selectedBordereau.mandats()
						));
				getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
						selectedBordereau
				})));
			}

			setWaitCursor(false);

		}
	}

	/**
	 * Verifie si le bordereau peut etre traité. Si des mandats sont passés avec mode de paiement du domaine extourne, il faut verifier que l'exercice
	 * N+1 existe.
	 * 
	 * @param selectedBordereau2
	 */
	public boolean isBordereauPeutEtreVisePourExtourne(EOBordereau selectedBordereau2) {
		NSArray mandats = selectedBordereau2.mandats();
		EOQualifier qualExtourne = new EOKeyValueQualifier(EOMandat.MODE_PAIEMENT_KEY + "." + EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, EOModePaiement.MODDOM_A_EXTOURNER);
		NSArray res = EOQualifier.filteredArrayWithQualifier(mandats, qualExtourne);
		if (res.count() > 0) {
			if (myApp.appUserInfo().getCurrentExercice().getNextEOExercice() == null || !myApp.appUserInfo().getCurrentExercice().getNextEOExercice().estOuvert()) {
				return false;
			}
		}
		return true;
	}

	private void checkMandatsForExtourne(NSArray mandatsVises) throws DefaultClientException {
		EOQualifier qualExtourne = new EOKeyValueQualifier(EOMandat.MODE_PAIEMENT_KEY + "." + EOModePaiement.MOD_DOM_KEY, EOQualifier.QualifierOperatorEqual, EOModePaiement.MODDOM_A_EXTOURNER);
		NSArray res = EOQualifier.filteredArrayWithQualifier(mandatsVises, qualExtourne);
		if (res.count() > 0) {
			if (myApp.appUserInfo().getCurrentExercice().getNextEOExercice() == null || !myApp.appUserInfo().getCurrentExercice().getNextEOExercice().estOuvert()) {
				throw new DefaultClientException("Les mandats avec des dépenses à extourner ne peuvent pas être visés tant que l'exercice " + (myApp.appUserInfo().getCurrentExercice().exeExercice().intValue() + 1)
						+ " n'est pas ouvert, car le visa du mandat doit réserver des crédits sur " + (myApp.appUserInfo().getCurrentExercice().exeExercice().intValue() + 1) + ".");
			}
		}
	}

}
