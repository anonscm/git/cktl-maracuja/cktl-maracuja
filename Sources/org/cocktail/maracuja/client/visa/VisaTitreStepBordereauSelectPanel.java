/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaTitreStepBordereauSelectPanel extends ABordereauSelectPanel {

	/**
	 * @throws Exception
	 */
	public VisaTitreStepBordereauSelectPanel() throws Exception {
		super();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() {
		specialAction1().setEnabled(false);
		try {
			if (selectedGestion != null) {
				final NSArray res = FinderVisa.lesBordereauxTitreAViser(getEditingContext(), selectedGestion, myApp.appUserInfo().getCurrentExercice());
				//                ZLogger.debug(res);
				lesBordereauxDg.setObjectArray(res);
				bordCountPerGestion.put(selectedGestion.gesCode(), new Integer(lesBordereauxDg.displayedObjects().count()));
				bordMontantPerGestion.put(selectedGestion.gesCode(), ZEOUtilities.calcSommeOfBigDecimals(lesBordereauxDg.displayedObjects(), EOBordereau.MONTANT_KEY));
			}
			else {
				lesBordereauxDg.setObjectArray(new NSArray());
			}
			//				Indiquer au modele que le nombre d'enregistrements a changé
			//            myTableModel.updateInnerRowCount();
			//            myTableModel.fireTableDataChanged();
			refreshTotal();
			myEOTable.updateData();
			panelTotal1.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Initialise le modele le la table à afficher.
	 */
	protected void initTableModel() {
		Vector myCols2 = new Vector(6, 0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(lesBordereauxDg, "borNum", "N°", 69);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(lesBordereauxDg, "gestion.gesCode", "Code gestion", 81);
		col2.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(lesBordereauxDg, "typeBordereau.tboType", "Type", 79);
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(lesBordereauxDg, "typeBordereau.tboSousType", "Sous Type", 100);
		ZEOTableModelColumn col5 = new ZEOTableModelColumn(lesBordereauxDg, "typeBordereau.tboLibelle", "Libellé", 250);
		ZEOTableModelColumn col6 = new ZEOTableModelColumn(lesBordereauxDg, "utilisateur.nomAndPrenom", "Créé par", 180);
		ZEOTableModelColumn col7 = new ZEOTableModelColumn(lesBordereauxDg, "nbTitres", "Nb Titres", 60);
		col7.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col8 = new ZEOTableModelColumn(lesBordereauxDg, "borDateCreation", "Date création", 80);
		col8.setColumnClass(Date.class);
		col8.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		col8.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col9 = new ZEOTableModelColumn(lesBordereauxDg, "montant", "Montant", 80);
		col9.setColumnClass(BigDecimal.class);
		col9.setFormatDisplay(ZConst.FORMAT_DECIMAL);
		col9.setAlignment(SwingConstants.RIGHT);

		myCols2.add(col1);
		myCols2.add(col2);
		myCols2.add(col3);
		myCols2.add(col4);
		myCols2.add(col5);
		myCols2.add(col7);
		myCols2.add(col9);
		myCols2.add(col8);
		myCols2.add(col6);

		myTableModel = new ZEOTableModel(lesBordereauxDg, myCols2);
		myTableSorter = new TableSorter(myTableModel);
	}

	protected void initGestions() throws Exception {
		NSArray res;
		//res = EOsFinder.getGestionsForUtilisateur(getEditingContext(), myApp.appUserInfo().getUtilisateur());
		EOFonction fonc = myApp.getFonctionByActionId(ZActionCtrl.IDU_VIRE);
		if (fonc == null) {
			throw new Exception("La fonction IDU_VIRE devrait exister.");
		}
		//            res = EOUtilisateurFinder.getGestionsForUtilisateurAndExerciceAndFonction(getEditingContext(),myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(), fonc);
		res = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_VIRE);
		allGestions = res;
		if (res.count() == 0) {
			throw new DefaultClientException("Vous avez un droit sur cette fonctionnalité, mais aucun code gestion associé. Demandez à un administrateur de vous associer des codes gestions pour la fonctionnalité " + fonc.fonLibelle());
		}

		myGestionComboBoxModel = new ZEOComboBoxModel(res, EOGestion.GES_CODE_KEY, null, null);
		myGestionComboBox = new JComboBox(myGestionComboBoxModel);
		myGestionComboBox.setRenderer(myGestionRenderer);
		myGestionComboBox.addActionListener(new ComboFilterListener());
		myGestionComboBox.setMaximumRowCount(18);
		if (res.count() > 0) {
			selectedGestion = (EOGestion) res.objectAtIndex(0);
		}

		bordCountPerGestion.clear();
		final String sql;
		if (myApp.appUserInfo().getCurrentExercice().exeExercice().intValue() < ZConst.EXE_EXERCICE_2007) {
			sql = "SELECT t0.GES_CODE, COUNT(*) as NBBORD FROM MARACUJA.BORDEREAU t0, MARACUJA.TYPE_BORDEREAU T1 WHERE ((T1.TBO_TYPE = 'BTTE' OR T1.TBO_SOUS_TYPE = 'REVERSEMENTS') AND t0.bor_etat = 'VALIDE' AND t0.EXE_ORDRE = " + myApp.appUserInfo().getCurrentExercice().exeExercice().intValue()
					+ " ) AND t0.TBO_ORDRE = T1.TBO_ORDRE GROUP BY t0.ges_code";
		}
		else {
			sql = "select t0.ges_code, sum(tit_HT) as MONTANT, count(distinct t0.bor_id) as NBBORD FROM MARACUJA.titre m, MARACUJA.BORDEREAU t0, MARACUJA.TYPE_BORDEREAU T1 WHERE m.bor_id=t0.bor_id and ((T1.TBO_TYPE = 'BTTE') AND t0.bor_etat = 'VALIDE' AND t0.EXE_ORDRE = "
					+ myApp.appUserInfo().getCurrentExercice().exeExercice().intValue()
					+ " ) AND t0.TBO_ORDRE = T1.TBO_ORDRE  and " + buildSqlCondForGestions(allGestions) + " GROUP BY t0.ges_code";
		}

		NSArray resCount = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		for (int i = 0; i < resCount.count(); i++) {
			NSDictionary element = (NSDictionary) resCount.objectAtIndex(i);
			bordCountPerGestion.put(element.valueForKey("GES_CODE"), new Integer(((Number) element.valueForKey("NBBORD")).intValue()));
			bordMontantPerGestion.put(element.valueForKey("GES_CODE"), new BigDecimal(((Number) element.valueForKey("MONTANT")).doubleValue()));

		}

		//            int i = 0;
		//            bordCountPerGestion.clear();
		//            while (i<res.count()) {
		//                final EOGestion element = (EOGestion)res.objectAtIndex(i);
		//                final NSArray res1 = FinderVisa.lesBordereauxTitreAViser(getEditingContext(), (EOGestion)res.objectAtIndex(i), myApp.appUserInfo().getCurrentExercice());
		//                bordCountPerGestion.put(element.gesCode(), new Integer(res1.count()));
		//                i++;
		//            }

	}

	public void refreshTotal() {
		String sql = "select sum(tit_HT) as MONTANT, count(distinct t0.bor_id) as NBBORD FROM MARACUJA.titre m, MARACUJA.BORDEREAU t0, MARACUJA.TYPE_BORDEREAU T1 WHERE m.bor_id=t0.bor_id and ((T1.TBO_TYPE = 'BTTE'  ) AND t0.bor_etat = 'VALIDE' AND t0.EXE_ORDRE = "
				+ myApp.appUserInfo().getCurrentExercice().exeExercice().intValue()
				+ " ) AND t0.TBO_ORDRE = T1.TBO_ORDRE  and " + buildSqlCondForGestions(allGestions);
		NSArray resTotal = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		mapTotal.clear();
		if (resTotal.count() > 0) {
			NSDictionary element = (NSDictionary) resTotal.objectAtIndex(0);
			mapTotal.put("MONTANT", element.valueForKey("MONTANT"));
			mapTotal.put("NBBORD", element.valueForKey("NBBORD"));
		}
	}

}
