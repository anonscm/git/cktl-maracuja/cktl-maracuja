/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;


/**
 * Panel de sélection des bordereaux (premier panel de l'assistant).
 */
public abstract class ABordereauSelectPanel extends ZKarukeraStepPanel implements ZEOTable.ZEOTableListener {
	private static final String TOPTITLE = "Choix du bordereau";
	private static final String TOPMESSAGE = "Veuillez sélectionner un bordereau à viser dans la liste";

	protected ZEOTableModel myTableModel;
	protected ZEOTable myEOTable;
	protected TableSorter myTableSorter;

	private Vector _attributesToFilter;
	private JTextField filterTextField;
	protected ZEOComboBoxModel myGestionComboBoxModel;
	protected JComboBox myGestionComboBox;
	protected EODisplayGroup lesBordereauxDg;
	protected ZPanelNbTotal panelTotal1;
	protected EOGestion selectedGestion;
	private IBordereauSelectPanelListener myListener;

	protected final HashMap bordCountPerGestion = new HashMap();
	protected final HashMap bordMontantPerGestion = new HashMap();
	protected final HashMap mapTotal = new HashMap();
	protected final GestionRenderer myGestionRenderer = new GestionRenderer();
	protected NSArray allGestions = new NSArray();

	public ABordereauSelectPanel() throws Exception {
		lesBordereauxDg = new EODisplayGroup();
		initGestions();
		//refreshTotal();
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
	}

	protected abstract void initTableModel();

	/**
	 * Initialise la liste déroulantes des codes gestion.
	 * 
	 * @throws Exception
	 */
	protected abstract void initGestions() throws Exception;

	/**
	 * Met à jour la liste des bordereaux à viser.
	 */
	public abstract void updateData();

	public void initGUI() {

		initTableModel();
		initTable();
		super.initGUI();
	}

	public void onDbClick() {
		onNext();
	}

	public boolean isPrevVisible() {
		return true;
	}

	public boolean isNextVisible() {
		return true;
	}

	public boolean isPrevEnabled() {
		return false;
	}

	public boolean isNextEnabled() {
		return (selectedObject() != null);
	}

	public boolean isEndEnabled() {
		return true;
	}

	public EOEnterpriseObject selectedObject() {
		if (myTableModel.getSelectedEOObjects().count() > 0) {
			return (EOEnterpriseObject) myTableModel.getSelectedEOObjects().objectAtIndex(0);
		}
		return null;
	}

	/**
	 * Met à jour le filtre sur le displaygroup. Appelle {@link ZEOSelectionDialog#updateFilter(String)}
	 */
	public void filter() {
		updateFilter(filterTextField.getText());
	}

	/**
	 * Met à jour le qualifier du displaygroup. Le qualifier est un EOOrQualifier de qualifiers du type "nomAttribut caseInsensitiveLike *filter*". Il
	 * y a autant de qualifier que d'attributs définis dans le tableau indiqués dans le constructeur.
	 * 
	 * @param filter Texte du filtre
	 */
	private void updateFilter(String filter) {
		if (!ZStringUtil.isEmpty(filter)) {
			NSMutableArray lesQuals = new NSMutableArray();
			if (_attributesToFilter != null) {
				for (int i = 0; i < _attributesToFilter.size(); i++) {
					lesQuals.addObject(EOQualifier.qualifierWithQualifierFormat(_attributesToFilter.get(i).toString() + " caseInsensitiveLike %@", new NSArray("*" + filter + "*")));
				}
			}

			EOQualifier qual = new EOOrQualifier(lesQuals);
			myTableModel.getMyDg().setQualifier(qual);
		}
		else {
			myTableModel.getMyDg().setQualifier(null);
		}
		myTableModel.getMyDg().updateDisplayedObjects();
		myTableModel.setInnerRowCount(myTableModel.getMyDg().displayedObjects().count());
		myTableModel.fireTableDataChanged();

		myEOTable.updateData();
	}

	/**
	 * Permet de définir un listener sur le contenu du champ texte qui sert à filtrer la table. Dès que le contenu du champ change, on met à jour le
	 * filtre. Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();
		}

		public void removeUpdate(DocumentEvent e) {
			filter();
		}
	}

	/**
	 * LIstener pour gérer les changements de sélection dans la liste déroulante des codes de gestion.
	 */
	class ComboFilterListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			selectedGestion = (EOGestion) myGestionComboBoxModel.getSelectedEObject();
			updateData();
		}
	}

	public boolean isCloseEnabled() {
		return true;
	}

	public boolean isCloseVisible() {
		return true;
	}

	public void onPrev() {
		return;
	}

	public void onNext() {
		setWaitCursor(true);
		myListener.onNext();
		setWaitCursor(false);
	}

	public void onClose() {
		myListener.onClose();
	}

	public String getCommentaire() {
		return TOPMESSAGE;
	}

	public String getTitle() {
		return TOPTITLE;
	}

	public Dimension getPanelDimension() {
		return myListener.getPanelDimension();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.BordereauAViserPanel.StepPanel#getContentPanel()
	 */
	public JPanel getContentPanel() {
		final JPanel mainPanel = new JPanel();
		final Box topPanel = Box.createVerticalBox();
		final JScrollPane myJscrollPane = new JScrollPane(myEOTable);
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		//La boite d'entete
		final Box topBox = Box.createVerticalBox();
		final Box box2 = Box.createHorizontalBox();
		box2.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		box2.add(new JLabel("Code gestion : "));
		box2.add(myGestionComboBox);
		box2.add(Box.createHorizontalGlue());
		topBox.add(box2);
		topPanel.add(topBox);

		mainPanel.add(topPanel, BorderLayout.PAGE_START);
		mainPanel.add(myJscrollPane, BorderLayout.CENTER);
		mainPanel.add(buildPanelTotaux(), BorderLayout.PAGE_END);

		return mainPanel;
	}

	public interface IBordereauSelectPanelListener extends ZKarukeraStepListener {
		public void onNewSelectedGestion(EOGestion newGestion);

	}

	/**
	 * Renvoie le bordereau sélectionné en cours (ou null).
	 * 
	 * @return
	 */
	public EOBordereau selectedBordereau() {
		return (EOBordereau) selectedObject();
	}

	/**
	 * @return
	 */
	public IBordereauSelectPanelListener getMyListener() {
		return myListener;
	}

	/**
	 * @param listener
	 */
	public void setMyListener(IBordereauSelectPanelListener listener) {
		myListener = listener;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		getNextAction().setEnabled(selectedBordereau() != null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#specialAction1()
	 */
	public AbstractAction specialAction1() {
		return getMyListener().specialAction1();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#onDisplay()
	 */
	public void onDisplay() {
		specialAction1().setEnabled(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#valideSaisie()
	 */
	public boolean valideSaisie() throws Exception {
		return true;
	}

	private class GestionRenderer extends DefaultListCellRenderer {

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			String gestion = (String) value;
			final Integer nb = (Integer) bordCountPerGestion.get(gestion);
			final BigDecimal montant = (BigDecimal) bordMontantPerGestion.get(gestion);
			return super.getListCellRendererComponent(list, formatGestion(gestion, nb, montant), index, isSelected, cellHasFocus);
		}

		private String formatGestion(final String code, final Integer nb, final BigDecimal montant) {
			final StringBuffer sb = new StringBuffer();
			sb.append(code);
			sb.append("        ");
			sb.append((nb != null && nb.intValue() != 0 ? nb.toString() : ""));
			sb.append("        ");
			sb.append((montant != null && nb.intValue() != 0 ? ZConst.FORMAT_DECIMAL.format(montant) : ""));

			//le code html pose des pbs de refresh dans la liste déroulante... dommage
			//            sb.append("<html><table cellpadding=0 cellspacing=0><tr><td width=70>");
			//            sb.append(code);
			//            sb.append("</td><td>");
			//            sb.append((nb!=null && nb.intValue()!=0 ? nb : null));
			//            sb.append("</td></tr></table></html>");
			//            
			return sb.toString();
		}

	}

	private JPanel buildPanelTotaux() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		panelTotal1 = new ZPanelNbTotal("");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());
		mainPanel.add(panelTotal1, BorderLayout.LINE_START);
		return mainPanel;
	}

	private class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			if (mapTotal.get("MONTANT") instanceof NSKeyValueCoding.Null) {
				return BigDecimal.valueOf(0);
			}
			return BigDecimal.valueOf(((Number) mapTotal.get("MONTANT")).doubleValue());
		}

		public void setValue(Object value) {
			return;
		}
	}

	private class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			if (mapTotal.get("NBBORD") instanceof NSKeyValueCoding.Null) {
				return Integer.valueOf(0);
			}
			return Integer.valueOf(((Number) mapTotal.get("NBBORD")).intValue());
		}

		public void setValue(Object value) {
			return;
		}
	}

	protected String buildSqlCondForGestions(NSArray gestions) {
		String res = "";
		for (int i = 0; i < gestions.count(); i++) {
			EOGestion element = (EOGestion) gestions.objectAtIndex(i);
			if (res.length() > 0) {
				res = res + " or ";
			}
			res = res + " t0.ges_code = '" + element.gesCode() + "'";
		}
		res = "(" + res + ")";
		return res;
	}

	public abstract void refreshTotal();

}
