/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.DepenseListPanel;
import org.cocktail.maracuja.client.common.ui.EcritureDetailListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;


/**
 * Panneau principal de l'annulation des émargements.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementAutoPanel extends ZKarukeraPanel {
    private final IEmargementAutoPanelListener myListener;
    private final MyDepenseListPanel depenseListPanel;
    private final CreditListPanel creditListPanel;
    private final DebitListPanel debitListPanel;
    private final ZCommentPanel erreurPanel;


    /**
     * @param editingContext
     * @throws Exception
     */
    public EmargementAutoPanel(IEmargementAutoPanelListener listener) {
        super();
        myListener = listener;
        depenseListPanel = new MyDepenseListPanel(myListener.getDepenseListListener());
        creditListPanel = new CreditListPanel(myListener.getCreditsListListener());
        debitListPanel = new DebitListPanel(myListener.getDebitsListListener());
        erreurPanel = buildErreurPanel();
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        this.setLayout(new BorderLayout());
        depenseListPanel.initGUI();
        creditListPanel.initGUI();
        debitListPanel.initGUI();

        final JPanel debits = encloseInPanelWithTitle("Débits indiqués lors de la liquidation", null,ZConst.BG_COLOR_TITLE,debitListPanel,null, null );
        final JPanel credits = encloseInPanelWithTitle("Crédits créés lors du visa", null,ZConst.BG_COLOR_TITLE,creditListPanel,null, null );

        final JPanel p = new JPanel(new BorderLayout());
        p.add(encloseInPanelWithTitle("Depenses visées", null,ZConst.BG_COLOR_TITLE,buildVerticalSplitPane(depenseListPanel, buildVerticalSplitPane(debits, credits)),  null, null ), BorderLayout.CENTER);
        p.add(erreurPanel, BorderLayout.SOUTH);
        
        
        add(p, BorderLayout.CENTER);
        add(buildCommentPanel(), BorderLayout.NORTH );
        add(buildBottomPanel(), BorderLayout.SOUTH );
        add(buildRightPanel(), BorderLayout.EAST);

    }


    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(80,8,8,8));
        final ArrayList list = new ArrayList();
        list.add(myListener.actionAccepter());
        list.add(myListener.actionRefuser());
//        list.add(myListener.actionAccepterTous());
//        list.add(myListener.actionRefuserTous());

//        final ArrayList comps = getButtonListFromActionList(list);
        final ArrayList comps = getButtonListFromActionList(list,110,25);
        tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }


    
    private final ZCommentPanel buildErreurPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("",
                "", ZIcon.getIconForName(ZIcon.ICON_WARNING_32), Color.decode("#FF4040"), Color.decode("#FFFFFF"), BorderLayout.WEST );
        
        return commentPanel;
    }
    
    
    private final JPanel buildCommentPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("Gestion des émargements ordonnateurs",
                "Pour chaque depense que vous venez de viser, des émargements sont possibles.\n" +
                "Veuillez accepter ou refuser ces émargements.", ZIcon.getIconForName(ZIcon.ICON_EMARGEMENT_32));

        return commentPanel;
    }
    
    
    private final JPanel buildBottomPanel() {
        final ArrayList a = new ArrayList();
        a.add(myListener.actionFermer());
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraPanel.buildHorizontalButtonsFromActions(a), BorderLayout.EAST);
        return p;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        depenseListPanel.updateData();
    }



	public interface IEmargementAutoPanelListener {
        public Action actionFermer();
        public Action actionAccepter();
        public Action actionRefuser();
//        public Action actionAccepterTous();
//        public Action actionRefuserTous();
        public ZKarukeraTablePanel.IZKarukeraTablePanelListener getDepenseListListener();
        public ZKarukeraTablePanel.IZKarukeraTablePanelListener getCreditsListListener();
	    public ZKarukeraTablePanel.IZKarukeraTablePanelListener getDebitsListListener();
        
        public Object getFlagForDepense(EODepense depense);
	}








    public final class CreditListPanel extends EcritureDetailListPanel {
        public static final String COL_ECRNUMERO="ecriture.ecrNumero";
        /**
         * @param listener
         */
        public CreditListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);
    		final ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup,COL_ECRNUMERO,"N° Ecriture",68);
    		ecrNumero.setAlignment(SwingConstants.LEFT);


            LinkedHashMap colsNew = new LinkedHashMap();
            colsNew.put(COL_ECRNUMERO, ecrNumero);
            colsNew.put(COL_GESCODE , colsMap.get(COL_GESCODE));
            colsNew.put(COL_PCONUM , colsMap.get(COL_PCONUM));
    		colsNew.put(COL_PCOLIBELLE , colsMap.get(COL_PCOLIBELLE));
    		colsNew.put(COL_ECDLIBELLE , colsMap.get(COL_ECDLIBELLE));
//    		colsNew.put(COL_ECDDEBIT , colsMap.get(COL_ECDDEBIT));
    		colsNew.put(COL_ECDCREDIT , colsMap.get(COL_ECDCREDIT));
    		colsNew.put(COL_ECDRESTEEMARGER , colsMap.get(COL_ECDRESTEEMARGER));

    		colsMap = colsNew;
        }

    }

    public final class DebitListPanel extends EcritureDetailListPanel {
        public static final String COL_ECRNUMERO="ecriture.ecrNumero";
        /**
         * @param listener
         */
        public DebitListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);

    		final ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup,COL_ECRNUMERO,"N° Ecriture",68);
    		ecrNumero.setAlignment(SwingConstants.LEFT);


            final LinkedHashMap colsNew = new LinkedHashMap();
            colsNew.put(COL_ECRNUMERO, ecrNumero);
            colsNew.put(COL_GESCODE , colsMap.get(COL_GESCODE));
            colsNew.put(COL_PCONUM , colsMap.get(COL_PCONUM));
    		colsNew.put(COL_PCOLIBELLE , colsMap.get(COL_PCOLIBELLE));
    		colsNew.put(COL_ECDLIBELLE , colsMap.get(COL_ECDLIBELLE));
    		colsNew.put(COL_ECDDEBIT , colsMap.get(COL_ECDDEBIT));
//    		colsNew.put(COL_ECDCREDIT , colsMap.get(COL_ECDCREDIT));
    		colsNew.put(COL_ECDRESTEEMARGER , colsMap.get(COL_ECDRESTEEMARGER));

    		colsMap = colsNew;

        }

    }
    
    public final class MyDepenseListPanel extends DepenseListPanel {
        public static final String COL_MODEPAIEMENT="mandat.modePaiement.modLibelleLong";
        public static final String COL_FLAG="flag";
        public static final String COL_MAN_NUMERO="mandat.manNumero";
        public static final String COL_FOURNISSEUR="mandat.fournisseur.nomAndPrenomAndCode";
        public static final String COL_PCONUM="mandat.planComptable.pcoNum";

        
        
        public MyDepenseListPanel(IZKarukeraTablePanelListener listener) {
            super(listener);
            
            final ZEOTableModelColumn modePaiement = new ZEOTableModelColumn(myDisplayGroup, COL_MODEPAIEMENT, "Mode de paiement", 150);
            modePaiement.setAlignment(SwingConstants.LEFT);
            
            final ZEOTableModelColumn flag = new ZEOTableModelColumnWithProvider(" ", new FlagProvider(),18);
            flag.setAlignment(SwingConstants.CENTER);
            flag.setColumnClass(Integer.class);            
            
            final ZEOTableModelColumn colmandat = new ZEOTableModelColumn(myDisplayGroup, COL_MAN_NUMERO, "N° mandat", 80);
            colmandat.setAlignment(SwingConstants.CENTER);
            colmandat.setColumnClass(Integer.class);
            
            final ZEOTableModelColumn fournisseur = new ZEOTableModelColumn(myDisplayGroup, COL_FOURNISSEUR, "Fournisseur", 200);
            fournisseur.setAlignment(SwingConstants.LEFT);

            final ZEOTableModelColumn pcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imputation", 80);
            pcoNum.setAlignment(SwingConstants.CENTER);
            
            
            
            final LinkedHashMap newCols = new LinkedHashMap();
            newCols.put(COL_FLAG ,flag);
            newCols.put(COL_MAN_NUMERO ,colmandat);
            newCols.put(COL_FOURNISSEUR ,fournisseur);
            newCols.put(COL_MODEPAIEMENT , modePaiement);
            newCols.put(COL_NUMERO ,colsMap.get(COL_NUMERO));
            newCols.put(COL_TTC ,colsMap.get(COL_TTC));
            colsMap = newCols;       
            
            
           
        }
        public void initGUI() {
            super.initGUI();
            myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        }
        
        
        public final class FlagProvider implements ZEOTableModelColumnProvider {

            /**
             *
             */
            public FlagProvider() {
                super();
            }

            /**
             * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
             */
            public Object getValueAtRow(int row) {
                final EODepense depense = (EODepense) myDisplayGroup.displayedObjects().objectAtIndex(row);
                return EmargementAutoPanel.this.myListener.getFlagForDepense(depense);
            }

        }        
        
    }

    public final DepenseListPanel getDepenseListPanel() {
        return depenseListPanel;
    }

    public final CreditListPanel getCreditListPanel() {
        return creditListPanel;
    }
    public final DebitListPanel getDebitListPanel() {
        return debitListPanel;
    }

    public ZCommentPanel getErreurPanel() {
        return erreurPanel;
    }
}



