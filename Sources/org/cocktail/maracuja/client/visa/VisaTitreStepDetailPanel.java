/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel;
import org.cocktail.maracuja.client.factories.ZFactory;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaModifTitre;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Le panneau permettant à l'utilisateur de saisir les infos nécessaires pour le visa des titres (comptes, modes de paiement, etc.) Les modifs
 * efefctuées ici le sont dans un editingcontext particulier (nested). Si on revient en arriere, on annule les modifs, si on va en avant, on valide
 * les modifs.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaTitreStepDetailPanel extends ZKarukeraStepPanel {
	private static final String TOPTITLE = "Titres à viser";
	private static final String TOPMESSAGE = "Veuillez spécifier les informations pour chaque titre à viser.";

	private final int PREFEREDWIDTH = 785;
	private final int PREFEREDHEIGHT2 = 200;

	private IBordereauDetailVisesListener myListener;
	private TitreAcceptesListePanel myTitreAcceptesListePanel;
	private TitreBrouillardListePanel myTitreBrouillardListePanel;

	private final ActionDeleteTitreBrouillard actionDeleteTitreBrouillard = new ActionDeleteTitreBrouillard();

	public VisaTitreStepDetailPanel(IBordereauDetailVisesListener vListener) {
		super();
		myListener = vListener;
		myTitreAcceptesListePanel = new TitreAcceptesListePanel(new TitreAcceptesListePanelListener(), getEditingContext());
		myTitreBrouillardListePanel = new TitreBrouillardListePanel(new TitreBrouillardListePanelListener(), getEditingContext());

		setEditingContext(new EOEditingContext(myListener.getParentEditingContext()));

		//		myTitreBrouillardListePanel.setEditingContext(getEditingContext());
		//		myTitreAcceptesListePanel.setEditingContext(getEditingContext());		

	}

	/**
	 * Met à jour la infos.
	 */
	public void updateData() throws Exception {
		//on travaille sur un nested (dont le parent est l'editingcontext du panel précédent)

		myTitreAcceptesListePanel.updateData();
	}

	public void initGUI() {
		myTitreAcceptesListePanel.initGUI();
		myTitreBrouillardListePanel.initGUI();
		super.initGUI();
	}

	public boolean isPrevVisible() {
		return true;
	}

	public boolean isNextVisible() {
		return true;
	}

	public boolean isPrevEnabled() {
		return true;
	}

	public boolean isNextEnabled() {
		return true;
	}

	public boolean isEndEnabled() {
		return true;
	}

	public boolean isCloseEnabled() {
		return true;
	}

	public boolean isCloseVisible() {
		return true;
	}

	public void onPrev() {
		//	    ZLogger.debug("Arboresceence EC:", ZEOUtilities.arborescenceOfEc(getEditingContext()));
		//	    ZLogger.debug("EditingContext : "+ getEditingContext() +  "updatedObjects avant revert------->:" + getEditingContext().updatedObjects());
		getEditingContext().revert();
		//		ZLogger.debug("EditingContext : "+ getEditingContext()+"update apres revert------->:" + getEditingContext().updatedObjects());

		myListener.onPrev();
	}

	public void onNext() {
		try {
			setWaitCursor(true);
			valideSaisie();
			getEditingContext().saveChanges();
			//Si pas de pb, on change de panel 
			myListener.onNext();
			setWaitCursor(false);

		} catch (Exception e) {
			showErrorDialog(e);
			setWaitCursor(false);
			return;
		}

	}

	public void onClose() {
		myListener.onClose();
	}

	public String getCommentaire() {
		return TOPMESSAGE;
	}

	public String getTitle() {
		return TOPTITLE;
	}

	//	/**
	//	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#getParentEditingContext()
	//	 */
	//	public EOEditingContext getParentEditingContext() {
	//		return myListener.getParentEditingContext();
	//	}	
	//	

	/**
	 * Effectue des vérifications sur la saisie de l'utilisateur.
	 * 
	 * @throws Exception
	 */
	private void checkData() throws DataCheckException {
		//Vérifier que toutes les écritures sont équilibrées
		checkEcrituresEquilibrees();
		checkMontantsBrouillards();

	}

	/**
	 * Vérifier que toutes les écritures associées au titres sont équilibrées
	 * 
	 * @throws DataCheckException
	 */
	private void checkEcrituresEquilibrees() throws DataCheckException {
		NSArray res = myTitreAcceptesListePanel.getMyDisplayGroup().displayedObjects();
		for (int i = 0; i < res.count(); i++) {
			EOTitre array_element = (EOTitre) res.objectAtIndex(i);
			NSArray resDebit = getDebitsForTitre(array_element);//ZEOUtilities.getFilteredArrayByKeyValue(get, "tibSens", ZConst.SENS_DEBIT);
			NSArray resCredit = getCreditsForTitre(array_element); //ZEOUtilities.getFilteredArrayByKeyValue(array_element.titreBrouillards(), "tibSens", ZConst.SENS_CREDIT);
			//			NSArray resDebit = ZEOUtilities.getFilteredArrayByKeyValue(EOsFinder.getTitreBrouilardsForTitre(getEditingContext(), array_element), "tibSens", ZConst.SENS_DEBIT);
			//			NSArray resCredit = ZEOUtilities.getFilteredArrayByKeyValue(EOsFinder.getTitreBrouilardsForTitre(getEditingContext(), array_element), "tibSens", ZConst.SENS_CREDIT);

			BigDecimal cred = ZEOUtilities.calcSommeOfBigDecimals(resCredit, "tibMontant");
			BigDecimal deb = ZEOUtilities.calcSommeOfBigDecimals(resDebit, "tibMontant");
			System.out.println(cred + "/" + deb);
			if (!cred.equals(deb)) {
				throw new DataCheckException("L' écriture de prise en charge du titre n° " + array_element.titNumero() + " n'est pas équilibrée. (" + cred.toString() + "/" + deb.toString() + ")");
			}
		}

	}

	private void checkMontantsBrouillards() throws DataCheckException {
		NSArray res = myTitreAcceptesListePanel.getMyDisplayGroup().displayedObjects();
		for (int i = 0; i < res.count(); i++) {
			EOTitre array_element = (EOTitre) res.objectAtIndex(i);
			NSArray resDebit = getDebitsForTitre(array_element);//ZEOUtilities.getFilteredArrayByKeyValue(get, "tibSens", ZConst.SENS_DEBIT);
			//NSArray resCredit = getCreditsForTitre(array_element); //ZEOUtilities.getFilteredArrayByKeyValue(array_element.titreBrouillards(), "tibSens", ZConst.SENS_CREDIT);
			//NSArray resDebit = ZEOUtilities.getFilteredArrayByKeyValue(array_element.titreBrouillards(), "tibSens", ZConst.SENS_DEBIT);
			//			NSArray resDebit = ZEOUtilities.getFilteredArrayByKeyValue(EOsFinder.getTitreBrouilardsForTitre(getEditingContext(), array_element), "tibSens", ZConst.SENS_DEBIT);
			//NSArray resCredit = ZEOUtilities.getFilteredArrayByKeyValue(EOsFinder.getTitreBrouilardsForTitre(getEditingContext(), array_element), "tibSens", ZConst.SENS_CREDIT);

			//BigDecimal cred = ZEOUtilities.calcSommeOfBigDecimals(resCredit, "tibMontant");
			BigDecimal deb = ZEOUtilities.calcSommeOfBigDecimals(resDebit, "tibMontant");

			if (!deb.abs().equals(array_element.titTtc().abs())) {
				throw new DataCheckException("Le montant de l'écriture de prise en charge du titre n° " + array_element.titNumero() + " doit être égal au montant TTC du titre. (" + deb.toString() + "/" + array_element.titTtc().toString() + ")");
			}
		}

	}

	public Dimension getPanelDimension() {
		return myListener.getPanelDimension();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.BordereauAViserPanel.StepPanel#getContentPanel()
	 */
	public JPanel getContentPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		//		mainPanel.add(buildPanelRecapBordereau(), BorderLayout.PAGE_START);

		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, buildPanelTitreListe(), buildPanelTitreBrouillard());
		splitPane.setOneTouchExpandable(false);
		splitPane.setDividerLocation(0.5);
		splitPane.setDividerSize(5);
		splitPane.setResizeWeight(0.5);
		splitPane.setBorder(BorderFactory.createEmptyBorder());
		mainPanel.add(splitPane, BorderLayout.CENTER);
		return mainPanel;
	}

	private JPanel buildPanelTitreListe() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Listes des titres que vous avez sélectionné", null, ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);
		mainPanel.add(myTitreAcceptesListePanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}

	//	private JPanel buildPanelBottom() {
	//		JPanel mainPanel = new JPanel();
	//		mainPanel.setLayout(new BorderLayout());
	//		mainPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
	////		mainPanel.add(buildTitlePanel("Listes des titres que vous avez sélectionné", null,ZConst.GRADIENTTITLESTARTCOLOR, ZConst.GRADIENTTITLEENDCOLOR), BorderLayout.PAGE_START);
	////		mainPanel.add(myTitreAcceptesListePanel, BorderLayout.CENTER);
	//		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
	//		return mainPanel;
	//	}		

	private JPanel buildPanelTitreBrouillard() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		mainPanel.add(ZUiUtil.buildTitlePanel("Les écritures associées au titre", null, ZConst.BG_COLOR_TITLE, null, null), BorderLayout.PAGE_START);

		JPanel tmpPanel = new JPanel();
		tmpPanel.setLayout(new BorderLayout());
		tmpPanel.add(myTitreBrouillardListePanel, BorderLayout.CENTER);
		tmpPanel.add(buildTitreBrouillardListeToolBar(), BorderLayout.LINE_END);

		mainPanel.add(tmpPanel, BorderLayout.CENTER);
		mainPanel.setPreferredSize(new Dimension(PREFEREDWIDTH, PREFEREDHEIGHT2));
		return mainPanel;
	}

	private class ActionAddTitreBrouillard extends AbstractAction {
		public ActionAddTitreBrouillard() {
			super("+");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter une contrepartie");
			//			putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));

		}

		public void actionPerformed(ActionEvent e) {
			//Dans le cas d'un BTODP, interdire la modification
			final EOTitreBrouillard titreBrouillard = myTitreBrouillardListePanel.getSelectedTitreBrouillard();
			if (EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(titreBrouillard.titre().bordereau().typeBordereau().tboSousType())) {
				titreBrouillardAddReduction();
			}
			else {
				titreBrouillardAddRecette();
			}
		}
	}

	public final NSArray getDebitsForTitre(final EOTitre titre) {
		final EOQualifier qualDebit = EOQualifier.qualifierWithQualifierFormat("tibOperation<>%@ and tibSens=%@", new NSArray(new Object[] {
				EOTitreBrouillard.VISA_ANNULER, EOTitreBrouillard.SENS_DEBIT
		}));
		return EOQualifier.filteredArrayWithQualifier(titre.titreBrouillards(), qualDebit);
	}

	public final NSArray getCreditsForTitre(final EOTitre titre) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("tibOperation<>%@ and tibSens=%@", new NSArray(new Object[] {
				EOTitreBrouillard.VISA_ANNULER, EOTitreBrouillard.SENS_CREDIT
		}));
		return EOQualifier.filteredArrayWithQualifier(titre.titreBrouillards(), qual);
	}

	/**
	 * Ajoute une contrepartie (débit)
	 */
	private void titreBrouillardAddRecette() {
		try {
			final EOTitre titre = myTitreAcceptesListePanel.getSelectedTitre();
			if (titre == null) {
				throw new DefaultClientException("Le titre est nul.");
			}
			final NSArray debits = getDebitsForTitre(titre);
			final NSArray credits = getCreditsForTitre(titre);

			final BigDecimal montantDebit = ZEOUtilities.calcSommeOfBigDecimals(debits, "tibMontant");
			final BigDecimal montantCredit = ZEOUtilities.calcSommeOfBigDecimals(credits, "tibMontant");

			//			final EOQualifier qualPcoTitre = EOQualifier.qualifierWithQualifierFormat("(pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@)", new NSArray(new Object[] {
			//					"102*", "103*", "139*", "3*", "4*", "5*"
			//			}));

			final EOQualifier qualPcoTitre = EOsFinder.qualifierForPconumContrepartiesRecette();

			final BrouillardAjoutCtrl brouillardAjoutCtrl = new BrouillardAjoutRecetteCtrl(getEditingContext(), qualPcoTitre, titre.recettes());

			NSArray brouillardsCredits = EOQualifier.filteredArrayWithQualifier(titre.titreBrouillards(), new EOKeyValueQualifier(EOTitreBrouillard.TIB_SENS_KEY, EOQualifier.QualifierOperatorEqual, EOTitreBrouillard.SENS_CREDIT));
			EOGestion gesBro = titre.gestion();
			if (brouillardsCredits.count() > 0) {
				gesBro = ((EOTitreBrouillard) brouillardsCredits.objectAtIndex(0)).gestion();
				//brouillardAjoutCtrl.myGestionComboBoxModel.setSelectedEObject(gestion);
			}
			//FIXME voir si faire la meme chose pour les mandats (si ca regle les erreurs erratiques sur le fit que le planco n'est pas valide)

			String sensTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? BrouillardAjoutCtrl.SENS_CREDIT : BrouillardAjoutCtrl.SENS_DEBIT);
			BigDecimal montantTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? montantDebit.subtract(montantCredit) : montantCredit.subtract(montantDebit));
			final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantTmp, gesBro, sensTmp);
			//final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantCredit.subtract(montantDebit), titre.gestion(), BrouillardAjoutCtrl.SENS_DEBIT);

			if (res != null) {
				final FactoryProcessVisaModifTitre factoryProcessVisaModifTitre = new FactoryProcessVisaModifTitre(myApp.wantShowTrace(), null);

				//				final String sens = EOTitreBrouillard.SENS_DEBIT;
				final String sens = ((String) res.get("sens")).substring(0, 1);
				final BigDecimal leMontant = (BigDecimal) res.get("montant");
				final EOGestion gestion = (EOGestion) res.get("gestion");
				final EORecette recette = (EORecette) res.get("recette");
				final EOPlanComptable planComptable = (EOPlanComptable) res.get(EOTitreBrouillard.PLAN_COMPTABLE_KEY);

				if (leMontant == null) {
					throw new DefaultClientException("Le montant est obligatoire.");
				}

				if (gestion == null) {
					throw new DefaultClientException("Le code gestion est obligatoire.");
				}

				if (planComptable == null) {
					throw new DefaultClientException("Le compte est obligatoire.");
				}
				if (recette == null) {
					throw new DefaultClientException("La recette est obligatoire.");
				}
				String tibOrigine = (planComptable.pcoNum().startsWith("445") ? EOTitreBrouillard.VISA_TVA : EOTitreBrouillard.VISA_CTP);

				final EOTitreBrouillard titreBrouillard = factoryProcessVisaModifTitre.creerTitreBrouillard(getEditingContext(), myApp.appUserInfo().getUtilisateur(),
						titre.exercice(), gestion, planComptable, titre, tibOrigine, sens, leMontant, recette);

				if (titreBrouillard == null) {
					throw new DefaultClientException("Erreur lors de la création du brouillard.");
				}

				ZLogger.debug("Brouillard cree", titreBrouillard);

				myTitreBrouillardListePanel.updateData();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void titreBrouillardAddReduction() {
		try {
			final EOTitre titre = myTitreAcceptesListePanel.getSelectedTitre();
			if (titre == null) {
				throw new DefaultClientException("Le titre est nul.");
			}
			final NSArray debits = getDebitsForTitre(titre);
			final NSArray credits = getCreditsForTitre(titre);

			final BigDecimal montantDebit = ZEOUtilities.calcSommeOfBigDecimals(debits, "tibMontant");
			final BigDecimal montantCredit = ZEOUtilities.calcSommeOfBigDecimals(credits, "tibMontant");

			final EOQualifier qualPcoTitre = EOQualifier.qualifierWithQualifierFormat(" (pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@ or pcoNum like %@)", new NSArray(new Object[] {
					"102*", "103*", "139*", "3*", "4*", "5*"
			}));

			final BrouillardAjoutCtrl brouillardAjoutCtrl = new BrouillardAjoutRecetteCtrl(getEditingContext(), qualPcoTitre, titre.recettes());

			NSArray brouillardsCredits = EOQualifier.filteredArrayWithQualifier(titre.titreBrouillards(), new EOKeyValueQualifier(EOTitreBrouillard.TIB_SENS_KEY, EOQualifier.QualifierOperatorEqual, EOTitreBrouillard.SENS_DEBIT));
			EOGestion gesBro = titre.gestion();
			if (brouillardsCredits.count() > 0) {
				gesBro = ((EOTitreBrouillard) brouillardsCredits.objectAtIndex(0)).gestion();
				//brouillardAjoutCtrl.myGestionComboBoxModel.setSelectedEObject(gestion);
			}

			String sensTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? BrouillardAjoutCtrl.SENS_CREDIT : BrouillardAjoutCtrl.SENS_DEBIT);
			BigDecimal montantTmp = (montantDebit.abs().compareTo(montantCredit.abs()) > 0 ? montantDebit.subtract(montantCredit) : montantCredit.subtract(montantDebit));
			final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantTmp, gesBro, sensTmp);
			//final Map res = brouillardAjoutCtrl.openDialogNew(getMyDialog(), montantDebit.subtract(montantCredit), titre.gestion(), BrouillardAjoutCtrl.SENS_CREDIT);

			if (res != null) {
				final FactoryProcessVisaModifTitre factoryProcessVisaModifTitre = new FactoryProcessVisaModifTitre(myApp.wantShowTrace(), null);

				//final String sens = EOTitreBrouillard.SENS_CREDIT;
				final String sens = ((String) res.get("sens")).substring(0, 1);
				final BigDecimal leMontant = (BigDecimal) res.get("montant");
				final EOGestion gestion = (EOGestion) res.get("gestion");
				final EORecette recette = (EORecette) res.get("recette");
				final EOPlanComptable planComptable = (EOPlanComptable) res.get(EOTitreBrouillard.PLAN_COMPTABLE_KEY);

				if (leMontant == null) {
					throw new DefaultClientException("Le montant est obligatoire.");
				}

				if (gestion == null) {
					throw new DefaultClientException("Le code gestion est obligatoire.");
				}

				if (planComptable == null) {
					throw new DefaultClientException("Le compte est obligatoire.");
				}

				if (recette == null) {
					throw new DefaultClientException("La recette est obligatoire.");
				}
				String tibOrigine = (planComptable.pcoNum().startsWith("445") ? EOTitreBrouillard.VISA_TVA : EOTitreBrouillard.VISA_CTP);

				final EOTitreBrouillard titreBrouillard = factoryProcessVisaModifTitre.creerTitreBrouillard(getEditingContext(), myApp.appUserInfo().getUtilisateur(),
						titre.exercice(), gestion, planComptable, titre, tibOrigine, sens, leMontant, recette);

				if (titreBrouillard == null) {
					throw new DefaultClientException("Erreur lors de la création du brouillard.");
				}

				ZLogger.debug("Brouillard cree", titreBrouillard);

				myTitreBrouillardListePanel.updateData();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void titreBrouillardDelete() {
		try {
			final FactoryProcessVisaModifTitre factoryProcessVisaModifTitre = new FactoryProcessVisaModifTitre(myApp.wantShowTrace(), null);
			final EOTitre titre = myTitreAcceptesListePanel.getSelectedTitre();
			if (titre == null) {
				throw new DefaultClientException("Le mandat est nul.");
			}

			final EOTitreBrouillard titreBrouillard = myTitreBrouillardListePanel.getSelectedTitreBrouillard();
			if (titreBrouillard == null) {
				throw new DefaultClientException("Aucun brouillard sélectionné");
			}

			factoryProcessVisaModifTitre.supprimerTitreBrouillard(getEditingContext(), titreBrouillard);

			myTitreBrouillardListePanel.updateData();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void refreshBrouillardAction() {
		final EOTitreBrouillard titreBrouillard = myTitreBrouillardListePanel.getSelectedTitreBrouillard();

		final boolean isOk = (titreBrouillard != null && !EOTitreBrouillard.VISA_ANNULER.equals(titreBrouillard.tibOperation())
				&& !titreBrouillard.planComptable().pcoNum().equals(titreBrouillard.titre().planComptable().pcoNum()));
		//		final boolean isOk = (titreBrouillard != null && !EOTitreBrouillard.VISA_ANNULER.equals(titreBrouillard.tibOperation())
		//				&& ((EOTitreBrouillard.SENS_DEBIT.equals(titreBrouillard.tibSens())
		//						&& !EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(titreBrouillard.titre().bordereau().typeBordereau().tboSousType()))
		//						||
		//						(EOTitreBrouillard.SENS_CREDIT.equals(titreBrouillard.tibSens())
		//								&& EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(titreBrouillard.titre().bordereau().typeBordereau().tboSousType()))));

		actionDeleteTitreBrouillard.setEnabled(isOk);
	}

	private class ActionDeleteTitreBrouillard extends AbstractAction {
		public ActionDeleteTitreBrouillard() {
			super("-");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer la contrepartie");
		}

		public void actionPerformed(ActionEvent e) {
			//		  Dans le cas d'un BTODP, interdire la modification
			titreBrouillardDelete();
		}
	}

	private JToolBar buildTitreBrouillardListeToolBar() {
		JToolBar tmpToolBar = new JToolBar("", JToolBar.VERTICAL);
		tmpToolBar.setFloatable(false);
		tmpToolBar.setRollover(false);
		tmpToolBar.add(Box.createRigidArea(new Dimension(2, 1)));
		tmpToolBar.add(new ActionAddTitreBrouillard());
		tmpToolBar.add(actionDeleteTitreBrouillard);
		return tmpToolBar;
	}

	/**
	 * @return
	 */
	public IBordereauDetailVisesListener getMyListener() {
		return myListener;
	}

	/**
	 * @param listener
	 */
	public void setMyListener(IBordereauDetailVisesListener listener) {
		myListener = listener;
	}

	/**
	 * Listener pour la liste des titres
	 */
	public interface IBordereauDetailVisesListener extends ZKarukeraStepListener {
		public ArrayList getTitresAViser();
		//		public Dialog getDialog();
	}

	public class TitreAcceptesListePanelListener implements TitreAcceptesListePanel.ITitreAcceptesListePanelListener {
		public ArrayList getTitresAViser() {
			//Créer des instances locales pour les objets
			return ZFactory.nestedInstancesForObjects(getEditingContext(), myListener.getTitresAViser());
			//			return   myListener.getTitresAViser();
		}

		public void onTitreSelectionChanged() {
			myTitreBrouillardListePanel.updateData();

		}
	}

	public class TitreBrouillardListePanelListener implements TitreBrouillardListePanel.ITitreBrouillardListePanelListener {
		public EOTitre getSelectedTitre() {
			return myTitreAcceptesListePanel.getSelectedTitre();
		}

		public void onSelectionChanged() {
			refreshBrouillardAction();

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#specialAction1()
	 */
	public AbstractAction specialAction1() {
		return myListener.specialAction1();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraStepPanel#onDisplay()
	 */
	public void onDisplay() {
		return;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel#valideSaisie()
	 */
	public boolean valideSaisie() throws Exception {
		checkData();
		return true;
	}

}
