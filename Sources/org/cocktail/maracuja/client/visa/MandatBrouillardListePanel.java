/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visa;

import java.awt.BorderLayout;
import java.awt.Component;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.IEOBrouillard;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;


/**
 * Liste des écritures associées à un mandat.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MandatBrouillardListePanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener, TableModelListener {
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;
	private TableSorter myTableSorter;
	private IMandatBrouillardListePanelListener myListener;
	private ZPanelBalance myZPanelBalance;
	private EOEditingContext ec;
	private Vector myCols;
	private final BrouillardTableRenderer brouillardTableRenderer = new BrouillardTableRenderer();

	public MandatBrouillardListePanel(IMandatBrouillardListePanelListener vListener, EOEditingContext ec) {
		super();
		setEditingContext(ec);
		this.myListener = vListener;
		myDisplayGroup = new EODisplayGroup();

		myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), EOMandatBrouillard.ENTITY_NAME));
		((EODistributedDataSource) myDisplayGroup.dataSource()).setEditingContext(getEditingContext());

		myZPanelBalance = new ZPanelBalance(new ZPanelBalanceProvider());
	}

	public EOMandatBrouillard getSelectedMandatBrouillard() {
		return (EOMandatBrouillard) myDisplayGroup.selectedObject();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#getEditingContext()
	 */
	public EOEditingContext getEditingContext() {
		return ec;
	}

	public void setEditingContext(EOEditingContext pec) {
		ec = pec;
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter, brouillardTableRenderer);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private void initTableModel() {
		myCols = new Vector(4, 0);

		ZEOTableModelColumn colGescode = new ZEOTableModelColumn(myDisplayGroup, IEOBrouillard.GESTION_KEY + ZConst.QUAL_POINT + EOGestion.GES_CODE_KEY, "Code gestion", 84);
		colGescode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumnWithProvider colDebit = new ZEOTableModelColumnWithProvider("Débit", new colDebitProvider(), 80);
		colDebit.setAlignment(SwingConstants.RIGHT);
		colDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumnWithProvider colCredit = new ZEOTableModelColumnWithProvider("Crédit", new colCreditProvider(), 80);
		colCredit.setAlignment(SwingConstants.RIGHT);
		colCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colCredit.setColumnClass(BigDecimal.class);

		//		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"mabSens","Sens",80);
		//		col2.setAlignment(SwingConstants.CENTER); 		
		ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup, EOMandatBrouillard.PLAN_COMPTABLE_KEY + ZConst.QUAL_POINT + EOPlanComptable.PCO_NUM_KEY, "Imp.", 130);
		col11.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EOMandatBrouillard.PLAN_COMPTABLE_KEY + ZConst.QUAL_POINT + EOPlanComptable.PCO_LIBELLE_KEY, "Libellé", 409);
		col4.setAlignment(SwingConstants.LEFT);
		//		ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup,"mabMontant","Montant",125);		
		//		col32.setAlignment(SwingConstants.RIGHT);
		//		col32.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn mabOperation = new ZEOTableModelColumn(myDisplayGroup, EOMandatBrouillard.MAB_OPERATION_KEY, "Operation", 120);
		mabOperation.setAlignment(SwingConstants.LEFT);

		//		col32.setEditable(true);
		//		col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		//		col32.setFormatEdit( ZConst.FORMAT_EDIT_NUMBER);

		myCols.add(mabOperation);
		myCols.add(colGescode);
		//		myCols.add(col2);
		myCols.add(col11);
		myCols.add(col4);
		myCols.add(colDebit);
		myCols.add(colCredit);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter(myTableModel);

		myTableModel.addTableModelListener(this);
		//permet notammen,t d'ajouter la possibilité de controler l'autorisation de modifier au niveau du row
		myTableModel.setMyDelegate(new ZEOTableModelDelegate());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		myZPanelBalance.updateData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		return;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.onSelectionChanged();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		initTableModel();
		initTable();
		myZPanelBalance.initGUI();
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		this.add(myZPanelBalance, BorderLayout.PAGE_END);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.ZKarukeraPanel#updateData()
	 */
	public void updateData() {
		NSArray res = new NSArray();
		final EOMandat mandat = myListener.getSelectedMandat();
		if (mandat != null) {
			res = mandat.mandatBrouillards();

			// On tri en renvoyant d'abord les debits
			final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("mabSens", EOSortOrdering.CompareDescending);
			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				sort1
			}));
		}

		myDisplayGroup.setObjectArray(res);
		myEOTable.updateData();
	}

	private final class ZPanelBalanceProvider implements ZPanelBalance.IZPanelBalanceProvider {
		/**
		 * @return la valeur des debits.
		 */
		public BigDecimal getDebitValue() {
			final NSArray tmp = myDisplayGroup.displayedObjects();
			BigDecimal val = ZConst.BIGDECIMAL_ZERO;

			for (int i = 0; i < tmp.count(); i++) {
				final EOMandatBrouillard array_element = (EOMandatBrouillard) tmp.objectAtIndex(i);
				if (array_element.mabSens().equals(ZConst.SENS_DEBIT) && !EOMandatBrouillard.VISA_ANNULER.equals(array_element.mabOperation())) {
					val = val.add(array_element.mabMontant());
				}
			}
			return val;
		}

		public BigDecimal getCreditValue() {
			final NSArray tmp = myDisplayGroup.displayedObjects();
			BigDecimal val = ZConst.BIGDECIMAL_ZERO;

			for (int i = 0; i < tmp.count(); i++) {
				final EOMandatBrouillard array_element = (EOMandatBrouillard) tmp.objectAtIndex(i);
				if (array_element.mabSens().equals(ZConst.SENS_CREDIT) && !EOMandatBrouillard.VISA_ANNULER.equals(array_element.mabOperation())) {
					val = val.add(array_element.mabMontant());
				}
			}
			return val;
		}
	}

	private class ZEOTableModelDelegate implements ZEOTableModel.IZEOTableModelDelegate {
		/**
		 * Le row est editable seulement s'il s'agit d'un credit.
		 * 
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate#isCellEditable(int, int)
		 */
		public boolean isCellEditable(int row, int col) {
			return ((EOMandatBrouillard) myDisplayGroup.displayedObjects().objectAtIndex(row)).mabSens().equals(ZConst.SENS_CREDIT);
		}
	}

	public interface IMandatBrouillardListePanelListener {
		public EOMandat getSelectedMandat();

		public void onSelectionChanged();
	}

	private class colDebitProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			EOMandatBrouillard el = (EOMandatBrouillard) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (ZConst.SENS_DEBIT.equals(el.mabSens())) {
				return el.mabMontant();
			}
			return null;
		}

	}

	private class colCreditProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			EOMandatBrouillard el = (EOMandatBrouillard) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (ZConst.SENS_CREDIT.equals(el.mabSens())) {
				return el.mabMontant();
			}
			return null;
		}

	}

	private final class BrouillardTableRenderer extends ZEOTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

			//Recuperer le row
			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final NSKeyValueCoding obj = (NSKeyValueCoding) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
			final Component res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (EOMandatBrouillard.VISA_ANNULER.equals(obj.valueForKey(EOMandatBrouillard.MAB_OPERATION_KEY))) {
				((JLabel) res).setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + ((JLabel) res).getText() + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX);
				//                ((JLabel)res).setText("<html><strike>"+ ((JLabel)res).getText()  +"</strike></html>");
			}
			else {
				//                ((JLabel)res).setText("<html><strike>"+ ((JLabel)res).getText()  +"</stricke></html>");
			}

			return res;

		}

	}

}
