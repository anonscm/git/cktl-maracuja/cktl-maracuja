/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;

import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.files.ZFileFilter;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanel.ZWaitingPanelListener;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ExportFactoryClient extends CommonCtrl {

	/** sert aux threads */
	private Integer stateLock = new Integer(0);

	private ZWaitingPanelDialog waitingDialog;
	private ExportFactoryWaitThread exportThread;

	private Date debutTraitement, finTraitement;

	/**
     *
     */
	public ExportFactoryClient() {
		super();
		debutTraitement = null;
		finTraitement = null;
	}

	public final File showFileSelectDialog(String _extension, String description) {
		File dir = new File(myApp.homeDir);
		ZFileFilter filterXls = new ZFileFilter(_extension, description);
		filterXls.setExtensionListInDescription(true);

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(filterXls);
		fileChooser.addChoosableFileFilter(filterXls);
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setDialogTitle("Enregistrer le fichier sous...");
		fileChooser.setCurrentDirectory(dir);
		//            fileChooser.setSelectedFile(new File(dir, currentFile.getName()));
		if (fileChooser.showSaveDialog(getMyDialog()) == JFileChooser.APPROVE_OPTION) {
			File tmpFile = fileChooser.getSelectedFile();
			String extension = ZFileUtil.getExtension(tmpFile);
			if (extension == null || extension.length() == 0 || !extension.toLowerCase().equals("xls")) {
				fileChooser.setSelectedFile(new File(tmpFile.getAbsolutePath() + ".xls"));
			}
			File file = fileChooser.getSelectedFile();

			System.out.println("Export vers " + file.getAbsolutePath());

			if (file.exists()) {
				if (!showConfirmationDialog("Confirmation", "Le fichier " + file.getAbsolutePath() + " existe déjà, souhaitez-vous le remplacer ?", ZMsgPanel.BTLABEL_NO)) {
					return null;
				}
			}
			return file;
		}
		return null;

	}

	public final void exportToExcelFromFetchSpec(Window win, File fileToWrite, NSArray keyNames, NSArray headers, EOFetchSpecification fetchSpecification, NSArray postSorts, NSDictionary parametres) throws Exception {
		try {
			exportThread = new ExportFactoryWaitThreadExcelFromFetchSpec(getEditingContext(), fileToWrite, keyNames, headers, fetchSpecification, postSorts, parametres);

			final Action cancelAction = new ActionCancelImpression();

			final ZWaitingPanel.ZWaitingPanelListener waitingListener = new ZWaitingPanelListener() {
				public Action cancelAction() {
					return cancelAction;
				}
			};

			if (win instanceof Dialog) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Dialog) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else if (win instanceof Frame) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Frame) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else {
				throw new DefaultClientException("Fenetre parente non définie.");
			}
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Export en cours...");
			waitingDialog.setModal(true);

			debutTraitement = ZDateUtil.nowAsDate();
			exportThread.start();
			waitingDialog.setVisible(true);

			//Vérifier s'il y a eu une exception
			if (exportThread.getLastException() != null) {
				throw exportThread.getLastException();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible de réaliser l'export. " + e.getMessage());
		} finally {
			ServerProxy.clientSideRequestExportToExcelReset(getEditingContext());
		}
	}

	public final void exportToExcelFromSQL(Window win, File fileToWrite, NSArray keyNames, NSArray headers, NSArray keysToSum, String sqlQuery, NSDictionary parametres) throws Exception {
		try {
			exportThread = new ExportFactoryWaitThreadExcelFromSQL(getEditingContext(), fileToWrite, keyNames, headers, keysToSum, sqlQuery, parametres);

			final Action cancelAction = new ActionCancelImpression();

			final ZWaitingPanel.ZWaitingPanelListener waitingListener = new ZWaitingPanelListener() {
				public Action cancelAction() {
					return cancelAction;
				}
			};

			if (win instanceof Dialog) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Dialog) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else if (win instanceof Frame) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Frame) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else {
				throw new DefaultClientException("Fenetre parente non définie.");
			}
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Export en cours...");
			waitingDialog.setModal(true);

			debutTraitement = ZDateUtil.nowAsDate();
			exportThread.start();
			waitingDialog.setVisible(true);

			//Vérifier s'il y a eu une exception
			if (exportThread.getLastException() != null) {
				throw exportThread.getLastException();
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible de réaliser l'export. " + e.getMessage());
		} finally {
			ServerProxy.clientSideRequestExportToExcelReset(getEditingContext());
		}
	}

	//    private class ExportThread extends Thread {
	//
	//        /**
	//         * @throws Exception
	//         * @see java.lang.Thread#run()
	//         */
	//        public void run() {
	//
	//                try {
	//                    ServerProxy.clientSideRequestExportToExcelReset(getEditingContext());
	//
	//                    waitingPanelDialog.getMyProgressBar().setIndeterminate(true);
	//                    Thread.yield();
	//                    NSArray sorts = new NSArray (
	//                            new Object[]{
	//                                    new EOSortOrdering("ecriture.ecrNumero" , EOSortOrdering.CompareAscending),
	//                                    new EOSortOrdering("ecdIndex" , EOSortOrdering.CompareAscending)
	//                                    	}
	//                            );
	//
	//                    NSArray headers = new NSArray(new Object[]{"N° Ecriture", "Type journal", "Date", "Code gestion", "Compte", "Libelle compte", "Libelle ecriture", "Debit", "Credit", "Reste a emarger"   });
	//                    NSArray attributs = new NSArray(new Object[]{"ecriture.ecrNumero",
	//                            									"ecriture.typeJournal.tjoLibelle",
	//                            									"ecriture.ecrDateSaisie",
	//                            									"gestion.gesCode",
	//                            									"planComptable.pcoNum",
	//                            									"planComptable.pcoLibelle",
	//                            									"ecdLibelle",
	//                            									"ecdDebit",
	//                            									"ecdCredit",
	//                            									"ecdResteEmarger" });
	//
	//                    waitingPanelDialog.setBottomText("Ecriture des données au format Excel...");
	//
	//
	//
	//                    EOFetchSpecification fetchSpecification = new EOFetchSpecification(EOEcritureDetail.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters, "ecriture.")), null, true, true, null  );
	//                    fetchSpecification.setPrefetchingRelationshipKeyPaths(new NSArray("ecriture"));
	//
	//                    ServerProxy.clientSideRequestExportToExcelFromFetchSpecByThread(getEditingContext(), attributs, headers, fetchSpecification, sorts, null);
	//                    Thread.yield();
	//                } catch (Exception e) {
	//                    e.printStackTrace();
	//                    interrupt();
	//                }
	//        }
	//
	//        /**
	//         * @see java.lang.Thread#interrupt()
	//         */
	//        public void interrupt() {
	//            if (scruteur != null) {
	//                scruteur.cancel();
	//            }
	//            ServerProxy.clientSideRequestExportToExcelByThreadCancel(getEditingContext());
	//
	//            waitingPanelDialog.hide();
	//            super.interrupt();
	//        }
	//
	//    }

	//    private class ExportWaitTask extends TimerTask {
	//
	//        /**
	//         * @see java.util.TimerTask#run()
	//         */
	//        public void run() {
	//            NSData datas = ServerProxy.clientSideRequestExportToExcelGetResult(getEditingContext());
	//            if (datas != null) {
	//                if (scruteur!=null) {
	//                    scruteur.cancel();
	//                }
	//
	//                try {
	//
	//                    boolean goOn=true;
	//                    while (goOn) {
	//                        try {
	//                            System.out.println("ecriture du fichier..." + datas.length() + " octets");
	//                            FileOutputStream fileOutputStream = new FileOutputStream(file);
	//                            datas.writeToStream(fileOutputStream);
	//                            fileOutputStream.close();
	//                            goOn = false;
	//                            Date fin = new Date();
	//                            long duree = ZDateUtil.calcMillisecondsBetween(debutTraitement, fin);
	//                            System.out.println("Duree de l'export : " + ZDateUtil.formatDuree(duree) + " ("+ duree +"ms)");
	//
	//                        } catch (Exception exception) {
	//                            goOn = showConfirmationDialog("Impossible d'écrire le fichier", "Impossible d'écrire le fichier. Vérifiez qu'il n'est pas déjà ouvert dans une autre application, et fermez-le avant de cliquer sur Oui\n (cliquez sur Non si vous souhaitez annuler l'export).\n", ZMsgPanel.BTLABEL_YES);
	//                            if (!goOn){
	//                                throw new DefaultClientException("Export annulé par l'utilisateur.");
	//                            }
	//                        }
	//                    }
	//
	//
	//
	//                    //Vérifier que le fichier a bien ete cree
	//                    try {
	//                        if (!file.exists()) {
	//                            throw new Exception("Le fichier " + file.getAbsolutePath() + " n'existe pas.");
	//                        }
	//                    } catch (Exception e) {
	//                        throw new Exception(e.getMessage());
	//                    }
	//
	//                    if (!showConfirmationDialog("Confirmation", "Le fichier " +  file.getAbsolutePath() + " a été enregistré. Souhaitez-vous l'ouvrir ?","Non")) {
	//                        return;
	//                    }
	//                    else {
	//                        myApp.openFile(file.getAbsolutePath());
	//                    }
	//                } catch (Exception e) {
	//                    waitingPanelDialog.hide();
	//                    showErrorDialog(e);
	//                }
	//                finally {
	//                    waitingPanelDialog.hide();
	//                }
	//            }
	//
	//        }
	//
	//    }

	/**
	 * Tache qui scanne le serveur pour avoir le résultat
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private class ExportFactoryWaitTask extends TimerTask {
		private NSData datas;
		private Exception lastException;

		/**
         *
         */
		public ExportFactoryWaitTask() {
		}

		/**
		 * @see java.util.TimerTask#run()
		 */
		public void run() {
			System.out.println("Attente export...");
			Thread.yield();
			try {
				if (datas == null) {
					datas = ServerProxy.clientSideRequestExportToExcelGetResult(getEditingContext());
				}
				//                    waitingDialog.update(waitingDialog.getGraphics());
				if (datas != null) {
					//le report est rempli
					synchronized (getStateLock()) {
						//                          System.out.println("ReportFactoryPrintWaitTask.run() : report recu du serveur --> delock");
						getStateLock().notifyAll();
					}
				}
				else {
				}
			} catch (Exception e) {
				lastException = e;
				synchronized (getStateLock()) {
					getStateLock().notifyAll();
				}
			}
		}

		public NSData getDatas() {
			return datas;
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	/**
	 * Thread qui se charge du dialogue avec le serveur pour lancer le traitement
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public abstract class ExportFactoryWaitThread extends Thread {
		protected NSData result;
		protected Exception lastException;
		protected ExportFactoryWaitTask waitTask;
		protected Timer scruteur;
		protected File fileOut;

		/**
         *
         */
		public ExportFactoryWaitThread(EOEditingContext ec, final File fileToWrite) {
			super();
			fileOut = fileToWrite;
		}

		public abstract void startExport();

		public void run() {
			try {

				scruteur = new Timer();
				waitTask = new ExportFactoryWaitTask();
				scruteur.scheduleAtFixedRate(waitTask, 2000, 1000);

				//Lancer l'export
				startExport();

				//Attendre la fin de la tache d'impression
				synchronized (getStateLock()) {
					try {
						try {
							//On attend...
							getStateLock().wait();
							System.out.println("ExportFactoryWaitThread.run() state delocke");
							//a ce niveau on doit avoir le resultat en pdf ou
							// une exception
							NSData datas = waitTask.getDatas();
							if (datas == null) {
								if (waitTask.getLastException() != null) {
									throw waitTask.getLastException();
								}
								throw new Exception("Erreur : le resultat est null");
							}
							writeFile(datas);

							scruteur.cancel();

							finTraitement = ZDateUtil.nowAsDate();
							long duree = ZDateUtil.calcMillisecondsBetween(debutTraitement, finTraitement);
							System.out.println("Duree de l'export : " + ZDateUtil.formatDuree(duree) + " (" + duree + "ms)");
						} catch (InterruptedException e1) {
							e1.printStackTrace();
							throw new DefaultClientException("Export interrompue.");
						}

					} catch (Exception ie) {
						scruteur.cancel();
						throw ie;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}

		public void writeFile(NSData datas) throws Exception {
			boolean goOn = true;
			while (goOn) {
				try {
					System.out.println("ecriture du fichier..." + datas.length() + " octets");
					FileOutputStream fileOutputStream = new FileOutputStream(fileOut);
					datas.writeToStream(fileOutputStream);
					fileOutputStream.close();
					goOn = false;
					//                    Date fin = new Date();
					// long duree = ZDateUtil.calcMillisecondsBetween(debutTraitement, fin);
					//                    System.out.println("Duree de l'export : " + ZDateUtil.formatDuree(duree) + " ("+ duree +"ms)");

				} catch (Exception exception) {
					goOn = showConfirmationDialog("Impossible d'écrire le fichier", "Impossible d'écrire le fichier. Vérifiez qu'il n'est pas déjà ouvert dans une autre application, et fermez-le avant de cliquer sur Oui\n (cliquez sur Non si vous souhaitez annuler l'export).\n",
							ZMsgPanel.BTLABEL_YES);
					if (!goOn) {
						throw new DefaultClientException("Export annulé par l'utilisateur.");
					}
				}
			}
			//Vérifier que le fichier a bien ete cree
			try {
				File tmpFile = fileOut;
				if (!tmpFile.exists()) {
					throw new Exception("Le fichier " + fileOut.getAbsolutePath() + " n'existe pas.");
				}
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}

		}

		/**
		 * @see java.lang.Thread#interrupt()
		 */
		public void interrupt() {
			if (scruteur != null) {
				scruteur.cancel();
			}
			super.interrupt();
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	private final class ActionCancelImpression extends AbstractAction {
		/**
         *
         */
		public ActionCancelImpression() {
			super("Annuler");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interrompre l'export");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));

		}

		public void actionPerformed(ActionEvent e) {
			if (exportThread.isAlive()) {
				killCurrentExportTask(getEditingContext());
				exportThread.interrupt();
			}
		}

	}

	private final class ExportFactoryWaitThreadExcelFromFetchSpec extends ExportFactoryWaitThread {
		private EOFetchSpecification _fetchSpecification;
		private NSArray _postSorts;
		private NSDictionary _parametres;
		private NSArray _keyNames;
		private NSArray _headers;

		/**
		 * @param ec
		 * @param fileToWrite
		 */
		public ExportFactoryWaitThreadExcelFromFetchSpec(EOEditingContext ec, File fileToWrite, NSArray keyNames, NSArray headers, EOFetchSpecification fetchSpecification, NSArray postSorts, NSDictionary parametres) {
			super(ec, fileToWrite);
			_fetchSpecification = fetchSpecification;
			_postSorts = postSorts;
			_parametres = parametres;
			_keyNames = keyNames;
			_headers = headers;
		}

		/**
		 * @see org.cocktail.maracuja.client.ExportFactoryClient.ExportFactoryWaitThread#startExport()
		 */
		public void startExport() {
			ServerProxy.clientSideRequestExportToExcelFromFetchSpecByThread(getEditingContext(), _keyNames, _headers, _fetchSpecification, _postSorts, _parametres);
		}

	}

	private final class ExportFactoryWaitThreadExcelFromSQL extends ExportFactoryWaitThread {
		private String _sqlQuery;
		private NSDictionary _parametres;
		private NSArray _keyNames;
		private NSArray _headers;
		private NSArray _keysToSum;

		/**
		 * @param ec
		 * @param fileToWrite
		 */
		public ExportFactoryWaitThreadExcelFromSQL(EOEditingContext ec, File fileToWrite, NSArray keyNames, NSArray headers, NSArray keysToSum, String sqlQuery, NSDictionary parametres) {
			super(ec, fileToWrite);
			_sqlQuery = sqlQuery;
			_parametres = parametres;
			_keyNames = keyNames;
			_headers = headers;
			_keysToSum = keysToSum;
		}

		/**
		 * @see org.cocktail.maracuja.client.ExportFactoryClient.ExportFactoryWaitThread#startExport()
		 */
		public void startExport() {
			ServerProxy.clientSideRequestExportToExcelFromSQLByThread(getEditingContext(), _keyNames, _headers, _keysToSum, _sqlQuery, _parametres);
		}

	}

	private void killCurrentExportTask(EOEditingContext ec) {
		ServerProxy.clientSideRequestExportToExcelByThreadCancel(ec);
	}

	private Integer getStateLock() {
		return stateLock;
	}

	public Dimension defaultDimension() {
		return null;
	}

	public ZAbstractPanel mainPanel() {
		return null;
	}

	public String title() {
		return null;
	}

}
