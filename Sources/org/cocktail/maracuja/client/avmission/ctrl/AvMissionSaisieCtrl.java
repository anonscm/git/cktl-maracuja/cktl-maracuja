/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.avmission.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.avmission.ui.AvMissionSaisiePanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOVisaAvMission;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class AvMissionSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Modification de l'avance";
	private static final Dimension WINDOW_DIMENSION = new Dimension(600, 250);

	private HashMap currentDico;

	private AvMissionSaisiePanel AvMissionSaisiePanel;
	private ZEOComboBoxModel modePaiementModel;
	private ZEOComboBoxModel ribModel;

	private ActionClose actionClose = new ActionClose();
	private ActionValider actionValider = new ActionValider();

	private EOVisaAvMission currentVisaAvMission;

	/**
	 * @param editingContext
	 */
	public AvMissionSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);
		NSArray modePaiements = new NSArray();
		EOQualifier qual = new EOOrQualifier(new NSArray(new Object[] {
				EOModePaiement.QUAL_CAISSE, EOModePaiement.QUAL_CHEQUE, EOModePaiement.QUAL_VIREMENT
		}));
		try {
			modePaiements = EOQualifier.filteredArrayWithQualifier(EOsFinder.getModePaiementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice()), qual);

		} catch (ZFinderException e) {
			//ne rien faire
		}
		modePaiementModel = new ZEOComboBoxModel(modePaiements, EOModePaiement.MOD_LIBELLE_LONG_KEY, null, null);
		ribModel = new ZEOComboBoxModel(new NSArray(), EORib.RIB_LIB_LONG_KEY, null, null);
		currentDico = new HashMap();
		AvMissionSaisiePanel = new AvMissionSaisiePanel(new AvMissionSaisiePanelListener());
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			//            this.putValue(AbstractAction.SHORT_DESCRIPTION, "(F10)");
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisie() throws Exception {
		ZLogger.debug(currentDico);
		currentDico.put(EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY, modePaiementModel.getSelectedEObject());
		currentDico.put(EOVisaAvMission.RIB_KEY, ribModel.getSelectedEObject());

		if (currentDico.get(EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY) == null) {
			throw new DataCheckException("Le mode de paiement est obligatoire");
		}
		if (((EOModePaiement) currentDico.get(EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY)).planComptablePaiement() == null) {
			throw new DataCheckException("Le mode de paiement doit avoir une imputation paiement définie.");
		}

		if (EOModePaiement.MODDOM_VIREMENT.equals(((EOModePaiement) currentDico.get(EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY)).modDom())) {
			if ((EORib) currentDico.get(EOVisaAvMission.RIB_KEY) == null || !EORib.RIB_VALIDE.equals(((EORib) currentDico.get(EOVisaAvMission.RIB_KEY)).ribValide())) {
				throw new Exception("Le rib est non défini ou invalide.");
			}
		}
		else {
			currentDico.put(EOVisaAvMission.RIB_KEY, null);
		}

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	private final void validerSaisie() {
		try {
			checkSaisie();
			updateObjectWithDico();
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class AvMissionSaisiePanelListener implements AvMissionSaisiePanel.IAvMissionSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.AvMissionSaisiePanel.IAvMissionSaisiePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.AvMissionSaisiePanel.IAvMissionSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

		public DefaultComboBoxModel getModePaiementModel() {
			return modePaiementModel;
		}

		public Map getValues() {
			return currentDico;
		}

		public DefaultComboBoxModel getRibModel() {
			return ribModel;
		}

	}

	public final int openDialogModify(Window dial, EOVisaAvMission visaAvMission) {
		final ZKarukeraDialog win = createDialog(dial, true);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			//            modeSaisie=MODE_MODIF;
			currentVisaAvMission = visaAvMission;
			ribModel.updateListWithData(EOsFinder.getRibsValides(getEditingContext(), currentVisaAvMission.fournisseur()));
			updateDicoWithObject();

			modePaiementModel.setSelectedEObject(currentVisaAvMission.modePaiementAvance());
			ribModel.setSelectedEObject(currentVisaAvMission.rib());

			AvMissionSaisiePanel.initGUI();
			AvMissionSaisiePanel.updateData();
			res = win.open();
			//            if (win.open()==ZKarukeraDialog.MROK) {
			//                res = currentDico;
			//            }
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private void updateObjectWithDico() {
		currentVisaAvMission.setModePaiementAvanceRelationship((EOModePaiement) currentDico.get(EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY));
		currentVisaAvMission.setRibRelationship((EORib) currentDico.get(EOVisaAvMission.RIB_KEY));
		currentVisaAvMission.setVamMotifRejet((String) currentDico.get(EOVisaAvMission.VAM_MOTIF_REJET_KEY));
	}

	private void updateDicoWithObject() {
		currentDico.clear();
		currentDico.put(EOVisaAvMission.FOU_NOM_AND_PRENOM_KEY, currentVisaAvMission.getNomAndPrenom());
		currentDico.put(EOVisaAvMission.MISSION_MIS_MOTIF_KEY, currentVisaAvMission.mission_misMotif());
		currentDico.put(EOVisaAvMission.MISSION_MIS_NUMERO_KEY, currentVisaAvMission.mission_misNumero());
		currentDico.put(EOVisaAvMission.MISSION_MIS_DEBUT_KEY, currentVisaAvMission.mission_misDebut());
		currentDico.put(EOVisaAvMission.VAM_MONTANT_KEY, currentVisaAvMission.vamMontant());
		currentDico.put(EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY, currentVisaAvMission.modePaiementAvance());
		currentDico.put(EOVisaAvMission.VAM_MOTIF_REJET_KEY, currentVisaAvMission.vamMotifRejet());
		ZLogger.debug(currentDico);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return AvMissionSaisiePanel;
	}

	public String title() {
		return TITLE;
	}

}
