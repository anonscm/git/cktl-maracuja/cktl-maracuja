/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.avmission.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.avmission.ui.AvMissionPanel;
import org.cocktail.maracuja.client.avmission.ui.AvMissionPanel.IAvMissionPanelCtrl;
import org.cocktail.maracuja.client.common.ctrl.CommonSrchCtrl;
import org.cocktail.maracuja.client.common.ui.CommonFilterPanel.ICommonSrchCtrl;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessOrdrePaiement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaAvMission;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMission;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.maracuja.client.metier.EOVisaAvMission;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class AvMissionCtrl extends CommonSrchCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Prise en charge des avances de mission";

	private final String ACTION_ID = ZActionCtrl.IDU_VIAVMIS;

	private final AvMissionPanel myPanel;
	private final AvMissionListMdl avMissionListMdl;
	private final AvMissionPanelCtrl avMissionPanelCtrl;
	private final FilterPanelCtrl filterPanelCtrl;

	private final ActionRejeter actionRejeter = new ActionRejeter();
	private final ActionViser actionViser = new ActionViser();
	private final ActionVoirOp actionVoirOp = new ActionVoirOp();
	private final ActionImprimerOp actionImprimerOp = new ActionImprimerOp();
	private final ActionAnnulerOp actionAnnulerOp = new ActionAnnulerOp();
	private final ActionModifier actionModifier = new ActionModifier();

	private final FactoryProcessVisaAvMission factoryProcessVisaAvMission = new FactoryProcessVisaAvMission(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

	private final boolean canCreerOp;
	private final boolean canValiderOP;
	private NSArray gestionsforVisa;
	private NSArray gestionsforOp;
	private NSArray gestionsforOpSa;
	private NSArray gestionsforOpSu;
	private DefaultComboBoxModel comboEtatsModel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public AvMissionCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		gestionsforVisa = new NSArray();
		gestionsforOp = new NSArray();
		gestionsforOpSa = new NSArray();
		gestionsforOpSu = new NSArray();

		//Récupérer les droits
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID)) {
			gestionsforVisa = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID);
		} else {
			throw new DefaultClientException("Vous n'avez pas les droits nécessaires pour cette fonctionnalité.");
		}
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOP)) {
			gestionsforOp = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOP);
		}
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOPSA)) {
			//        	throw new DefaultClientException("Vous devez avoir tous les droits concernants les Ordres de paiement pour cette fonctionnalité");
			gestionsforOpSa = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOPSA);
		}
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOPSU)) {
			gestionsforOpSu = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOPSU);
		}

		canCreerOp = true;
		canValiderOP = true;

		if (gestionsforVisa.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour accéder à cette fonction. Demandez à votre administrateur " +
					"de vous affecter les codes de gestions pour cette fonction");
		}

		comboEtatsModel = new DefaultComboBoxModel();
		comboEtatsModel.addElement(ZFinderEtats.etat_EN_ATTENTE());
		comboEtatsModel.addElement(ZFinderEtats.etat_ACCEPTE());
		comboEtatsModel.addElement(ZFinderEtats.etat_REJETE());

		avMissionListMdl = new AvMissionListMdl();
		avMissionPanelCtrl = new AvMissionPanelCtrl();
		filterPanelCtrl = new FilterPanelCtrl();
		myPanel = new AvMissionPanel(avMissionPanelCtrl);
	}

	protected NSMutableArray buildFilterQualifiers() throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOVisaAvMission.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice()
		})));

		quals.addObject(EOVisaAvMission.qualForEtat((EOTypeEtat) comboEtatsModel.getSelectedItem()));
		quals.addObject(EOVisaAvMission.qualForVamMontantBetween((Number) filters.get("montantMin"), (Number) filters.get("montantMax")));
		quals.addObject(EOVisaAvMission.qualForMisNumeroEquals((Number) filters.get(EOVisaAvMission.MISSION_KEY + "." + EOMission.MIS_NUMERO_KEY)));
		quals.addObject(EOVisaAvMission.qualForFournisseur((String) filters.get(EOVisaAvMission.FOURNISSEUR_KEY)));
		quals.addObject(EOVisaAvMission.qualForMisMotifLike((String) filters.get(EOVisaAvMission.MISSION_MIS_MOTIF_KEY)));
		quals.addObject(EOVisaAvMission.qualForGesCodeEquals((String) filters.get(EOVisaAvMission.GESTION_KEY + "." + EOGestion.GES_CODE_KEY)));

		return quals;
	}

	public void onAfterUpdateDataBeforeOpen() {
		super.onAfterUpdateDataBeforeOpen();
		refreshActions();
	}

	protected final NSArray getData() throws Exception {
		//Créer la condition à partir des filtres
		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOVisaAvMission.VAM_DATE_DEM_KEY, EOSortOrdering.CompareDescending);
		return EOsFinder.fetchArray(getEditingContext(), EOVisaAvMission.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers()), new NSArray(sort1), true);

	}

	private void onAccepter() {
		setWaitCursor(true);
		try {
			if (getSelectedAvMission() == null) {
				throw new DefaultClientException("Aucune avance n'est sélectionnée.");
			}

			if (!isCreationOpAutorisee()) {
				throw new DefaultClientException("Vous n'avez pas le droit de saisie d'un OP comptable.");
			}

			if (gestionsforOpSa.indexOfObject(getSelectedAvMission().gestion()) == NSArray.NotFound) {
				throw new DefaultClientException("Vous n'avez pas les droits de saisie d'un OP comptable pour le code gestion " + getSelectedAvMission().gestion().gesCode());
			}

			factoryProcessVisaAvMission.accepterAvMission(getEditingContext(), getSelectedAvMission(), getComptabilite(), getExercice(), getUtilisateur(), canCreerOp, canValiderOP);
			getEditingContext().saveChanges();
			numeroterOdp(getSelectedAvMission().ordreDePaiement());
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}

	}

	private void numeroterOdp(EOOrdreDePaiement odp) throws Exception {
		if (odp.odpNumero().intValue() > 0) {
			return;
		}
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
		try {
			FactoryProcessOrdrePaiement factoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
			factoryProcessOrdrePaiement.numeroterOrdreDePaiement(getEditingContext(), odp, myKFactoryNumerotation);
		} catch (Exception e) {
			System.out.println("ERREUR LORS DE LA NUMEROTATION ORDRE DE PAIEMENT...");
			e.printStackTrace();
			throw new Exception(e);
		}
	}

	private void onRejeter() {
		setWaitCursor(true);
		try {
			if (getSelectedAvMission() == null) {
				throw new DefaultClientException("Aucune avance n'est sélectionnée.");
			}
			boolean wantAbort = false;
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement rejeter la demande d'avance sélectionnée ?", ZMsgPanel.BTLABEL_NO)) {
				if (getSelectedAvMission().vamMotifRejet() == null || getSelectedAvMission().vamMotifRejet().length() == 0) {
					final AvMissionSaisieCtrl avMissionSaisieCtrl = new AvMissionSaisieCtrl(getEditingContext());
					int res = avMissionSaisieCtrl.openDialogModify(getMyDialog(), getSelectedAvMission());
					if (res != ZCommonDialog.MROK) {
						wantAbort = true;
					}
				}
				if (!wantAbort) {
					factoryProcessVisaAvMission.rejeterAvMission(getEditingContext(), getSelectedAvMission(), getUtilisateur());
					getEditingContext().saveChanges();
				}
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
			refreshActions();
		}

	}

	private void onVoirOp() {
		// TODO Auto-generated method stub

	}

	private void onImprimerOp() {
		try {
			if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), "PAOP")) {
				throw new DefaultClientException("Vous devez avoir le droit Ordres de Paiement pour pouvoir imprimer l'OP.");
			}

			if (getSelectedAvMission() == null) {
				throw new DefaultClientException("Aucune avance n'est sélectionnée.");
			}
			NSArray selectedObjs = (NSArray) getSelectedAvMissions().valueForKeyPath(EOVisaAvMission.ORDRE_DE_PAIEMENT_KEY);
			if (selectedObjs.count() == 0) {
				throw new DefaultClientException("Aucun ordre de paiement à imprimer.");
			}

			odpImprimer(selectedObjs);

			/*
			 * NSArray selectedOdpsVises = EOQualifier.filteredArrayWithQualifier(selectedObjs,
			 * EOQualifier.qualifierWithQualifierFormat("odpEtat=%@ or odpEtat=%@", new NSArray(new Object[] { EOOrdreDePaiement.etatVirement,
			 * EOOrdreDePaiement.etatVise }))); ZLogger.debug("selectedOdpsVises", selectedOdpsVises);
			 * 
			 * NSArray selectedOdpsNonVises = EOQualifier.filteredArrayWithQualifier(selectedObjs,
			 * EOQualifier.qualifierWithQualifierFormat("odpEtat=%@ or odpEtat=%@", new NSArray(new Object[] { EOOrdreDePaiement.etatValide,
			 * EOOrdreDePaiement.etatOrdonnateur }))); ZLogger.debug("selectedOdpsNonVises", selectedOdpsNonVises);
			 * 
			 * if (selectedOdpsNonVises.count() == 0 && selectedOdpsVises.count() == 0) { throw new
			 * DefaultClientException("Aucun ordre de paiement (valide ou visé) à imprimer."); }
			 * 
			 * if (selectedOdpsVises.count() > 0) { odpImprimer(selectedOdpsVises); }
			 * 
			 * if (selectedOdpsNonVises.count() > 0) { odpImprimerNonVise(selectedOdpsNonVises); }
			 */

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void odpImprimer(final NSArray objs) {
		try {
			String filePath = ReportFactoryClient.imprimerOrdrePaiement(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), objs, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void odpImprimerNonVise(final NSArray objs) {
		try {
			String filePath = ReportFactoryClient.imprimerOrdrePaiement(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), objs, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private void onAnnulerOp() {
		try {
			if (getSelectedAvMission() == null) {
				throw new DefaultClientException("Aucune avance n'est sélectionnée.");
			}

			if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), "PAOPSU")) {
				throw new DefaultClientException("Vous devez avoir le droit de supprimer l'Ordres de paiement pour cette fonctionnalité");
			}

			if (gestionsforOpSu.indexOfObject(getSelectedAvMission().gestion()) == NSArray.NotFound) {
				throw new DefaultClientException("Vous n'avez pas les droits de suppression d'un OP pour le code gestion " + getSelectedAvMission().gestion().gesCode());
			}

			factoryProcessVisaAvMission.annulerOP(getEditingContext(), getSelectedAvMission(), getUtilisateur());
			getEditingContext().saveChanges();
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			showErrorDialog(e);
		}

	}

	private void onModifier() {
		try {
			if (getSelectedAvMission() == null) {
				throw new DefaultClientException("Aucune avance n'est sélectionnée.");
			}

			if (gestionsforOp.indexOfObject(getSelectedAvMission().gestion()) == NSArray.NotFound) {
				throw new DefaultClientException("Vous n'avez pas les droits de modification pour les OP pour le code gestion " + getSelectedAvMission().gestion().gesCode());
			}

			final AvMissionSaisieCtrl avMissionSaisieCtrl = new AvMissionSaisieCtrl(getEditingContext());
			avMissionSaisieCtrl.openDialogModify(getMyDialog(), getSelectedAvMission());

			myPanel.getAvMissionListTablePanel().fireSeletedTableRowUpdated();
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private EOVisaAvMission getSelectedAvMission() {
		return myPanel.selectedAvMission();
	}

	private NSArray getSelectedAvMissions() {
		return myPanel.selectedAvMissions();
	}

	private final void refreshActions() {
		if (ZFinderEtats.etat_EN_ATTENTE() == null) {
			ZLogger.verbose("en attente null!!");
		}

		final EOVisaAvMission visaAvMission = (EOVisaAvMission) myPanel.getAvMissionListTablePanel().selectedObject();
		actionRejeter.setEnabled(visaAvMission != null && !ZFinderEtats.etat_REJETE().equals(visaAvMission.typeEtat()) && visaAvMission.ordreDePaiement() != null && visaAvMission.ordreDePaiement().ecriture() == null);
		actionViser.setEnabled(visaAvMission != null && ZFinderEtats.etat_EN_ATTENTE().equals(visaAvMission.typeEtat()));
		actionModifier.setEnabled(visaAvMission != null && ZFinderEtats.etat_EN_ATTENTE().equals(visaAvMission.typeEtat()));
		actionVoirOp.setEnabled(visaAvMission != null && visaAvMission.ordreDePaiement() != null);
		actionImprimerOp.setEnabled(visaAvMission != null && visaAvMission.ordreDePaiement() != null);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	private final class AvMissionPanelCtrl implements IAvMissionPanelCtrl {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionRejeter() {
			return actionRejeter;
		}

		public Action actionViser() {
			return actionViser;
		}

		public Action actionVoirOp() {
			return actionVoirOp;
		}

		public IZTablePanelMdl avMissionListMdl() {
			return avMissionListMdl;
		}

		public ICommonSrchCtrl panelFilterCtrl() {
			return filterPanelCtrl;
		}

		public Action actionImprimerOp() {
			return actionImprimerOp;
		}

		public ComboBoxModel getEtatModel() {
			return comboEtatsModel;
		}

		public Action actionModifier() {
			return actionModifier;
		}

	}

	private final class AvMissionListMdl implements IZTablePanelMdl {

		public NSArray getData() throws Exception {
			return AvMissionCtrl.this.getData();
		}

		public void onDbClick() {

		}

		public void selectionChanged() {
			refreshActions();
		}

	}

	private final class FilterPanelCtrl implements ICommonSrchCtrl {

		public Action actionSrch() {
			return actionSrch;
		}

		public Map filters() {
			return filters;
		}

	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Accepter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Accepter la demande. Un Ordre de Paiement va être créé");
		}

		public void actionPerformed(ActionEvent e) {
			onAccepter();
		}
	}

	private final class ActionRejeter extends AbstractAction {
		public ActionRejeter() {
			super("Rejeter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rejeter la demande");
		}

		public void actionPerformed(ActionEvent e) {
			onRejeter();
		}
	}

	private final class ActionVoirOp extends AbstractAction {
		public ActionVoirOp() {
			super("Voir OP");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onVoirOp();
		}
	}

	private final class ActionImprimerOp extends AbstractAction {
		public ActionImprimerOp() {
			super("Imprimer OP");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onImprimerOp();
		}
	}

	private final class ActionAnnulerOp extends AbstractAction {
		public ActionAnnulerOp() {
			super("Annuler OP");
			//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onAnnulerOp();
		}
	}

	private final class ActionModifier extends AbstractAction {
		public ActionModifier() {
			super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			//            putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
		}

		public void actionPerformed(ActionEvent e) {
			onModifier();
		}
	}

	private boolean isCreationOpAutorisee() {
		try {
			return myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), "PAOPSA");
		} catch (Exception e) {
			return false;
		}
	}

	private boolean isAnnulerOpAutorisee() {
		try {
			return myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), "PAOPSU");
		} catch (Exception e) {
			return false;
		}
	}

}
