/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.avmission.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.CommonFilterPanel;
import org.cocktail.maracuja.client.common.ui.CommonFilterPanel.ICommonSrchCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMission;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.metier.EOVisaAvMission;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;


/**
 * Panel permettant de rechercher et d'afficher des demandes d'avances de mission.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class AvMissionPanel extends ZKarukeraPanel {
	private final IAvMissionPanelCtrl myCtrl;
	private final AvMissionListTablePanel avMissionListTablePanel;
	private final FilterPanel filterPanel;

	/**
	 * @param editingContext
	 */
	public AvMissionPanel(IAvMissionPanelCtrl ctrl) {
		super();
		myCtrl = ctrl;
		avMissionListTablePanel = new AvMissionListTablePanel(myCtrl.avMissionListMdl());
		avMissionListTablePanel.initGUI();

		filterPanel = new FilterPanel(myCtrl.panelFilterCtrl());

		setLayout(new BorderLayout());

		final JPanel c = new JPanel(new BorderLayout());
		c.add(avMissionListTablePanel, BorderLayout.CENTER);
		final JPanel b = buildRightPanel();
		b.setPreferredSize(new Dimension(160, 20));
		c.add(b, BorderLayout.EAST);

		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		tmp.add(encloseInPanelWithTitle("Demandes d'avances", null, ZConst.BG_COLOR_TITLE, c, null, null), BorderLayout.CENTER);

		add(buildBottomPanel(), BorderLayout.SOUTH);
		add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		avMissionListTablePanel.updateData();
	}

	private final JPanel buildRightPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(50, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(myCtrl.actionModifier());
		list.add(myCtrl.actionRejeter());
		list.add(myCtrl.actionViser());
		list.add(myCtrl.actionImprimerOp());

		final ArrayList buttons = ZKarukeraPanel.getButtonListFromActionList(list);
		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myCtrl.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public final class FilterPanel extends CommonFilterPanel {
		public final ZFormPanel fournisseur;
		public final ZFormPanel noMission;
		public final ZFormPanel montants;
		public final ZFormPanel motif;
		public final ZFormPanel gestion;
		public final ZFormPanel comboBorEtat;

		public FilterPanel(ICommonSrchCtrl ctrl) {
			super(ctrl);
			setLayout(new BorderLayout());
			fournisseur = ZFormPanel.buildLabelField("Missionnaire", new ZTextField.DefaultTextFieldModel(ctrl.filters(), EOVisaAvMission.FOURNISSEUR_KEY));
			motif = ZFormPanel.buildLabelField("Motif", new ZTextField.DefaultTextFieldModel(ctrl.filters(), EOVisaAvMission.MISSION_MIS_MOTIF_KEY));
			gestion = ZFormPanel.buildLabelField("Gestion", new ZTextField.DefaultTextFieldModel(ctrl.filters(), EOVisaAvMission.GESTION_KEY + "." + EOGestion.GES_CODE_KEY));
			noMission = ZFormPanel.buildLabelField("N° mission", new ZNumberField(new ZNumberField.IntegerFieldModel(ctrl.filters(), EOVisaAvMission.MISSION_KEY + "." + EOMission.MIS_NUMERO_KEY), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT
			}, ZConst.FORMAT_ENTIER_BRUT));
			montants = ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(ctrl.filters(), "montantMin"), new ZNumberField.BigDecimalFieldModel(ctrl.filters(), "montantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_DECIMAL);

			((ZTextField) fournisseur.getMyFields().get(0)).getMyTexfield().setColumns(20);
			((ZTextField) motif.getMyFields().get(0)).getMyTexfield().setColumns(20);

			fournisseur.setDefaultAction(ctrl.actionSrch());
			motif.setDefaultAction(ctrl.actionSrch());
			gestion.setDefaultAction(ctrl.actionSrch());
			noMission.setDefaultAction(ctrl.actionSrch());
			montants.setDefaultAction(ctrl.actionSrch());

			comboBorEtat = ZFormPanel.buildLabelComboBoxField("Etat", myCtrl.getEtatModel(), ctrl.filters(), EOVisaAvMission.TYPE_ETAT_KEY, null);

			final Component separator = Box.createRigidArea(new Dimension(4, 1));
			final ArrayList comps = new ArrayList();

			comps.add(ZUiUtil.buildBoxLine(new Component[] {
					gestion, separator, fournisseur, separator, noMission, motif
			}));
			comps.add(buildLine(new Component[] {
					montants, separator, comboBorEtat
			}));

			add(ZUiUtil.buildBoxLine(new Component[] {
				ZUiUtil.buildBoxColumn(comps)
			}), BorderLayout.WEST);
			add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

			final JButton b = ZUiUtil.getButtonFromAction(ctrl.actionSrch(), 50, 50);
			final ArrayList l = new ArrayList();
			l.add(b);
			l.add(Box.createHorizontalStrut(25));
			add(ZUiUtil.buildBoxLine(l), BorderLayout.EAST);

		}

		public void updateData() throws Exception {
			fournisseur.updateData();
			noMission.updateData();
			gestion.updateData();
			montants.updateData();
			motif.updateData();
			comboBorEtat.updateData();
		}
	}

	public final class AvMissionListTablePanel extends ZTablePanel {
		public static final String GES_CODE_KEY = EOVisaAvMission.GESTION_KEY + "." + EOGestion.GES_CODE_KEY;
		public static final String EXE_EXERCICE_KEY = EOVisaAvMission.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY;
		public static final String MISSION_MIS_NUMERO_KEY = EOVisaAvMission.MISSION_MIS_NUMERO_KEY;
		public static final String MISSION_MIS_MOTIF_KEY = EOVisaAvMission.MISSION_MIS_MOTIF_KEY;
		public static final String MISSION_MIS_DEBUT_KEY = EOVisaAvMission.MISSION_MIS_DEBUT_KEY;
		public static final String RIB_LIB_LONG_KEY = EOVisaAvMission.RIB_KEY + "." + EORib.RIB_LIB_LONG_KEY;
		public static final String VAM_DATE_DEM_KEY = EOVisaAvMission.VAM_DATE_DEM_KEY;
		public static final String VAM_MONTANT_KEY = EOVisaAvMission.VAM_MONTANT_KEY;
		public static final String FOURNISSEUR_KEY = EOVisaAvMission.FOU_NOM_AND_PRENOM_KEY;
		public static final String TYET_LIBELLE_KEY = EOVisaAvMission.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY;
		public static final String UTILISATEUR_DEMANDEUR_KEY = EOVisaAvMission.UTILISATEUR_DEMANDEUR_KEY + "." + EOUtilisateur.UTL_NOM_PRENOM_KEY;
		public static final String UTILISATEUR_VALIDEUR_KEY = EOVisaAvMission.UTILISATEUR_VALIDEUR_KEY + "." + EOUtilisateur.UTL_NOM_PRENOM_KEY;
		public static final String ODP_NUMERO_KEY = EOVisaAvMission.ORDRE_DE_PAIEMENT_KEY + "." + EOOrdreDePaiement.ODP_NUMERO_KEY;
		public static final String ODP_ETAT_KEY = EOVisaAvMission.ORDRE_DE_PAIEMENT_KEY + "." + EOOrdreDePaiement.ODP_ETAT_KEY;
		public static final String MODE_PAIEMENT_AVANCE = EOVisaAvMission.MODE_PAIEMENT_AVANCE_KEY + "." + EOModePaiement.MOD_LIBELLE_LONG_KEY;

		public AvMissionListTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup, GES_CODE_KEY, "Gestion", 60);
			gesCode.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn exeExercice = new ZEOTableModelColumn(myDisplayGroup, EXE_EXERCICE_KEY, "Exercice", 60);
			exeExercice.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn numero = new ZEOTableModelColumn(myDisplayGroup, MISSION_MIS_NUMERO_KEY, "N° Mission", 60);
			numero.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn motif = new ZEOTableModelColumn(myDisplayGroup, MISSION_MIS_MOTIF_KEY, "Motif Mission", 100);
			motif.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn debut = new ZEOTableModelColumn(myDisplayGroup, MISSION_MIS_DEBUT_KEY, "Début Mission", 100);
			debut.setColumnClass(Date.class);
			debut.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			debut.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn datedem = new ZEOTableModelColumn(myDisplayGroup, VAM_DATE_DEM_KEY, "Date demande", 100);
			datedem.setColumnClass(Date.class);
			datedem.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			datedem.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn rib = new ZEOTableModelColumn(myDisplayGroup, RIB_LIB_LONG_KEY, "Rib", 100);
			rib.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn vammontant = new ZEOTableModelColumn(myDisplayGroup, VAM_MONTANT_KEY, "Montant", 100);
			vammontant.setAlignment(SwingConstants.RIGHT);
			vammontant.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);

			final ZEOTableModelColumn fournisseur = new ZEOTableModelColumn(myDisplayGroup, FOURNISSEUR_KEY, "Missionnaire", 100);
			fournisseur.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn utilisateurDemandeur = new ZEOTableModelColumn(myDisplayGroup, UTILISATEUR_DEMANDEUR_KEY, "Demandée par", 100);
			utilisateurDemandeur.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn utilisateurValideur = new ZEOTableModelColumn(myDisplayGroup, UTILISATEUR_VALIDEUR_KEY, "Visée par", 100);
			utilisateurValideur.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn odpNumero = new ZEOTableModelColumn(myDisplayGroup, ODP_NUMERO_KEY, "N° OP", 84);
			odpNumero.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn odpEtat = new ZEOTableModelColumn(myDisplayGroup, ODP_ETAT_KEY, "Etat OP", 84);
			odpEtat.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn tyetLibelle = new ZEOTableModelColumn(myDisplayGroup, TYET_LIBELLE_KEY, "Etat", 84);
			tyetLibelle.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn modePaiement = new ZEOTableModelColumn(myDisplayGroup, MODE_PAIEMENT_AVANCE, "Mode de paiement", 84);
			modePaiement.setAlignment(SwingConstants.CENTER);

			colsMap.clear();
			//            colsMap.put(EXE_EXERCICE_KEY ,exeExercice);
			colsMap.put(GES_CODE_KEY, gesCode);
			colsMap.put(MISSION_MIS_NUMERO_KEY, numero);
			colsMap.put(FOURNISSEUR_KEY, fournisseur);
			colsMap.put(MISSION_MIS_MOTIF_KEY, motif);
			colsMap.put(MISSION_MIS_DEBUT_KEY, debut);

			//            colsMap.put(RIB_LIB_LONG_KEY,rib);
			colsMap.put(MODE_PAIEMENT_AVANCE, modePaiement);
			colsMap.put(VAM_MONTANT_KEY, vammontant);
			colsMap.put(UTILISATEUR_DEMANDEUR_KEY, utilisateurDemandeur);
			colsMap.put(TYET_LIBELLE_KEY, tyetLibelle);
			colsMap.put(ODP_NUMERO_KEY, odpNumero);
			colsMap.put(ODP_ETAT_KEY, odpEtat);

		}

	}

	public interface IAvMissionPanelCtrl {
		public ZTablePanel.IZTablePanelMdl avMissionListMdl();

		public Action actionModifier();

		public Action actionImprimerOp();

		public Action actionClose();

		public Action actionRejeter();

		public Action actionViser();

		public Action actionVoirOp();

		public ICommonSrchCtrl panelFilterCtrl();

		public ComboBoxModel getEtatModel();

	}

	public void initGUI() {
		//obsolete

	}

	public final AvMissionListTablePanel getAvMissionListTablePanel() {
		return avMissionListTablePanel;
	}

	public EOVisaAvMission selectedAvMission() {
		return (EOVisaAvMission) avMissionListTablePanel.selectedObject();
	}

	public NSArray selectedAvMissions() {
		return avMissionListTablePanel.selectedObjects();
	}

}
