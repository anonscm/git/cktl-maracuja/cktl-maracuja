/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.avmission.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOVisaAvMission;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class AvMissionSaisiePanel extends ZKarukeraPanel {
    private IAvMissionSaisiePanelListener myListener;
	private JComboBox myModePaiementField;
	private JLabel numeroMissionLabel;
	private JLabel motifMissionLabel;
	private JLabel montantMissionLabel;
	private JLabel montantAvanceLabel;
	private JLabel fournisseurMissionLabel;
	private JLabel debutMissionLabel;
	private JLabel finMissionLabel;
	private ZTextField motifRejetField;
	private JComboBox myRibField ;
    
    
    
    /**
     * 
     */
    public AvMissionSaisiePanel(IAvMissionSaisiePanelListener listener) {
        super();
        myListener = listener;
        numeroMissionLabel = new JLabel();
        motifMissionLabel = new JLabel();
        montantMissionLabel = new JLabel();
        montantAvanceLabel = new JLabel();
        fournisseurMissionLabel = new JLabel();
        debutMissionLabel = new JLabel();
        finMissionLabel = new JLabel();
        myModePaiementField = new JComboBox(myListener.getModePaiementModel());
        myRibField = new JComboBox(myListener.getRibModel() );
        motifRejetField = new ZTextField(new ZTextField.DefaultTextFieldModel(myListener.getValues(), EOVisaAvMission.VAM_MOTIF_REJET_KEY));
        
        numeroMissionLabel.setFont(getFont().deriveFont(Font.BOLD));
        motifMissionLabel.setFont(getFont().deriveFont(Font.BOLD));
        montantMissionLabel.setFont(getFont().deriveFont(Font.BOLD));
        fournisseurMissionLabel.setFont(getFont().deriveFont(Font.BOLD));
        finMissionLabel.setFont(getFont().deriveFont(Font.BOLD));
        debutMissionLabel.setFont(getFont().deriveFont(Font.BOLD));
        montantAvanceLabel.setFont(getFont().deriveFont(Font.BOLD));
        
        motifRejetField.getMyTexfield().setColumns(40);
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
     */
    public void initGUI() {
        
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
        this.add(buildFormPanel(), BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);             
        
//        updateInputMap();
    }

    
    
    private final JPanel buildFormPanel() {
    	final ArrayList list = new ArrayList();
    	list.add( new Component[]{new JLabel("Missionnaire : "), fournisseurMissionLabel});
    	list.add( new Component[]{new JLabel("Mission n° "), numeroMissionLabel});
    	list.add( new Component[]{new JLabel("Début : "), debutMissionLabel});
    	list.add( new Component[]{new JLabel("Motif : "), motifMissionLabel});
    	list.add( new Component[]{new JLabel("Montant avance : "), montantAvanceLabel});
    	list.add( new Component[]{new JLabel("Mode de paiement"), myModePaiementField});
    	list.add( new Component[]{new JLabel("Rib (virement uniquement)"), myRibField});
    	list.add( new Component[]{new JLabel("Motif de rejet"), motifRejetField});
    	
    	final JPanel p = new JPanel(new BorderLayout());
    	p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);
    	p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
    	return p;
    }    
    
    
    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionClose());        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }    
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
     */
    public void updateData() throws Exception {
    	fournisseurMissionLabel.setText((String) myListener.getValues().get(EOVisaAvMission.FOU_NOM_AND_PRENOM_KEY));
    	motifMissionLabel.setText((String) myListener.getValues().get(EOVisaAvMission.MISSION_MIS_MOTIF_KEY));
    	numeroMissionLabel.setText(""+ myListener.getValues().get(EOVisaAvMission.MISSION_MIS_NUMERO_KEY));
    	debutMissionLabel.setText(ZConst.FORMAT_DATESHORT.format( myListener.getValues().get(EOVisaAvMission.MISSION_MIS_DEBUT_KEY)));
//    	montantMissionLabel.setText(ZConst.FORMAT_DECIMAL.format( myListener.getValues().get(EOVisaAvMission.MISSION_)));
    	montantAvanceLabel.setText(ZConst.FORMAT_DECIMAL.format( myListener.getValues().get(EOVisaAvMission.VAM_MONTANT_KEY)));
    }

    
    public interface IAvMissionSaisiePanelListener {
        public Action actionClose();
        public DefaultComboBoxModel getModePaiementModel();
        public DefaultComboBoxModel getRibModel();
		public Action actionValider();
        public Map getValues();
        
    }


    
//    
//
//    /**
//     * Affecte les raccourcis claviers aux actions. Les raccourcis sonr ceux définis dans l'action (ACCELERATOR_KEY).
//     */
//    public void updateInputMap() {
//        getActionMap().clear();
//        getInputMap().clear();
//        
//        final KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
//        final KeyStroke f10 = KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0, false);
//        
//        getActionMap().put("ESCAPE", myListener.actionClose());          
//        getActionMap().put("F10", myListener.actionValider());
//        
//        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
//        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(f10, "F10");    
//                  
//        
//    }    
    
}
