/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZDropDownButton;

/**
 * Panel qui affiche les boutons de menu sur la fenetre principale.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MainMenuPanel extends ZKarukeraPanel {
	private JPanel cardPanel;
	private CardLayout cardLayout;

	private final Action actionJournal = new ActionJournal();
	private final Action actionPaiements = new ActionPaiements();
	private final Action actionRecouvrements = new ActionRecouvrements();
	private final Action actionVisa = new ActionVisa();
	private final Action actionAdministration = new ActionAdministration();
	private final Action actionImpressions = new ActionImpressions();
	private final Action actionOutils = new ActionOutils();

	private final Action actionQuitter = myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_QUITTER);

	private JPanel panelJournal;
	private JPanel panelPaiements;
	private JPanel panelRecouvrements;
	private JPanel panelVisa;
	private JPanel panelAdministration;
	private JPanel panelImpressions;
	private JPanel panelDefaut;
	private JPanel panelOutils;

	private static final String PANEL_JOURNAL = "Journal";
	private static final String PANEL_PAIEMENTS = "Paiements";
	private static final String PANEL_RECOUVREMENTS = "Recouvrements";
	private static final String PANEL_VISA = "Visa";
	private static final String PANEL_ADMINISTRATION = "Administration";
	private static final String PANEL_IMPRESSIONS = "Impressions";
	private static final String PANEL_DEFAUT = "Defaut";
	private static final String PANEL_OUTILS = "Outils";

	/**
     *
     */
	public MainMenuPanel() {
		super();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		panelJournal = buildPanelJournal();
		panelPaiements = buildPanelPaiements();
		panelRecouvrements = buildPanelRecouvrements();
		panelVisa = buildPanelVisa();
		panelAdministration = buildPanelAdministration();
		panelImpressions = buildPanelImpressions();
		panelDefaut = buildPanelDefaut();
		panelOutils = buildPanelOutils();

		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));

		cardPanel.add(panelJournal, PANEL_JOURNAL);
		cardPanel.add(panelPaiements, PANEL_PAIEMENTS);
		cardPanel.add(panelRecouvrements, PANEL_RECOUVREMENTS);
		cardPanel.add(panelVisa, PANEL_VISA);
		cardPanel.add(panelAdministration, PANEL_ADMINISTRATION);
		cardPanel.add(panelImpressions, PANEL_IMPRESSIONS);
		cardPanel.add(panelDefaut, PANEL_DEFAUT);
		cardPanel.add(panelOutils, PANEL_OUTILS);

		panelDefaut = new JPanel(new BorderLayout());

		setLayout(new BorderLayout());

		if (myApp.getDisplayMessageClient() != null && myApp.getDisplayMessageClient().length() > 0) {
			this.add(buildClientMessagePanel(myApp.getDisplayMessageClient()), BorderLayout.NORTH);
		}

		this.add(buildLeftButtonsPanel(), BorderLayout.WEST);
		this.add(cardPanel, BorderLayout.CENTER);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {

	}

	/**
	 * Construit un panel constitué d'un label
	 * 
	 * @param mes
	 * @return
	 */
	private final JPanel buildClientMessagePanel(String mes) {
		if (mes != null && mes.length() > 0) {
			JPanel p = new JPanel(new BorderLayout());
			p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 15));
			JLabel t = new JLabel(mes);
			t.setHorizontalAlignment(SwingConstants.CENTER);
			p.add(new JLabel(ZIcon.getIconForName(ZIcon.ICON_WARNING_32)), BorderLayout.WEST);
			p.add(t, BorderLayout.CENTER);
			return p;
		}
		return null;
	}

	private final JPanel buildLeftButtonsPanel() {

		Action[] actions = new Action[8];
		actions[0] = actionJournal;
		actions[1] = actionPaiements;
		actions[2] = actionRecouvrements;
		actions[3] = actionVisa;
		actions[4] = actionImpressions;
		actions[5] = actionAdministration;
		actions[6] = actionOutils;
		actions[7] = actionQuitter;

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 15));
		p.setPreferredSize(new Dimension(260, 450));
		p.add(ZKarukeraPanel.buildVerticalPanelOfButtonsFromActions(actions), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;

	}

	private final JPanel buildPanelAdministration() {
		final ArrayList list = new ArrayList();
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADCO), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADGE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAPC), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAMP), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAMR), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADREL), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADRET), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_BDFCAL), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADBILAN), list);
		final ArrayList listButtons = ZKarukeraPanel.getButtonListFromActionList(list);

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		p.add(buildVerticalPanelOfComponents(listButtons), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Administration", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionAdministration.getValue(Action.SMALL_ICON), null);
	}

	private final JPanel buildPanelVisa() {
		final ArrayList list = new ArrayList();
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_VIDE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_VIRE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_VIPR), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_VISBRO), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_REMA), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_RETI), list);

		final ArrayList listDivers = new ArrayList(3);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAYE), listDivers);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAYEPAF), listDivers);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_SCOL), listDivers);

		final ZDropDownButton bDivers = new ZDropDownButton("Autres visas...", ZIcon.getIconForName(ZIcon.ICON_PLAY_16));
		bDivers.addActions(listDivers);

		final ArrayList listButtons = ZKarukeraPanel.getButtonListFromActionList(list);
		listButtons.add(4, bDivers);

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		//	    p.setPreferredSize(new Dimension(150,350));
		p.add(buildVerticalPanelOfComponents(listButtons), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Visa", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionVisa.getValue(Action.SMALL_ICON), null);

	}

	private final JPanel buildPanelRecouvrements() {
		ArrayList list = new ArrayList();
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_SEPASDD), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PRELEV), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_CHESA), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_RELANCE), list);

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		p.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Recouvrements", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionRecouvrements.getValue(Action.SMALL_ICON), null);

	}
	private final JPanel buildPanelOutils() {
		ArrayList list = new ArrayList();

		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_SRCH01), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_SRCH02), list);

		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_EPN), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_OUTBE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_OUTCFI), list);

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		//	    p.setPreferredSize(new Dimension(150,350));
		p.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Outils", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionOutils.getValue(Action.SMALL_ICON), null);

	}

	private final void addObjectToListIfNotNull(final Object obj, final ArrayList list) {
		if (obj != null) {
			list.add(obj);
		}
	}

	private final JPanel buildPanelPaiements() {
		ArrayList list = new ArrayList(4);

		//	    Action[] actions = new Action[5];
		try {
			if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOP)) {
				addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAOP), list);
			}
			else if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOPO)) {
				//	                affecteActionByIDToMenu(ZActionCtrl.IDU_PAOPO , m);
				addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAOPO), list);
			}
			else {
				addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAOP), list);
			}
		} catch (DefaultClientException e) {
		}

		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PARE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAGE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_PAMO), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_VIAVMIS), list);

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		//	    p.setPreferredSize(new Dimension(150,350));
		p.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Paiements", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionPaiements.getValue(Action.SMALL_ICON), null);

	}

	private final JPanel buildPanelJournal() {
		ArrayList list = new ArrayList(2);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_COCE), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_COEM), list);
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		p.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Journal", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionJournal.getValue(Action.SMALL_ICON), null);
	}

	private final JPanel buildPanelImpressions() {
		ArrayList list = new ArrayList();
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_IMPR001), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_IMPR002), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_IMPR003), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_IMPR004), list);
		addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_IMPR005), list);

		ArrayList listDivers = new ArrayList(2);
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR007));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR008));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR009));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR015));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR010));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR011));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR012));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR013));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.IDU_IMPR014));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.ID_IMPR_JOURNAL_REJET));
		listDivers.add(myApp.getActionbyId(ZActionCtrl.ID_IMPR_JOURNAL_CONVRA));
		ZDropDownButton bImprimerDivers = new ZDropDownButton("Divers...", ZIcon.getIconForName(ZIcon.ICON_PLAY_16));
		bImprimerDivers.addActions(listDivers);

		ArrayList listButtons = ZKarukeraPanel.getButtonListFromActionList(list);
		listButtons.add(bImprimerDivers);

		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
		p.add(ZKarukeraPanel.buildVerticalPanelOfComponents(listButtons), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return ZKarukeraPanel.encloseInPanelWithTitle("Impressions", null, ZConst.BG_COLOR_TITLE, p, (ImageIcon) actionImpressions.getValue(Action.SMALL_ICON), null);
	}

	private final JPanel buildPanelDefaut() {
		JPanel p = new JPanel(new BorderLayout());
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final class ActionAdministration extends AbstractAction {
		public ActionAdministration() {
			super("Administration");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ADMINISTRATION_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_ADMINISTRATION);
		}
	}

	private final class ActionOutils extends AbstractAction {
		public ActionOutils() {
			super("Outils");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SETTINGS_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_OUTILS);
		}
	}

	private final class ActionVisa extends AbstractAction {
		public ActionVisa() {
			super("Visa/Réimputations");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SIGNATURE_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_VISA);
		}
	}

	private final class ActionImpressions extends AbstractAction {
		public ActionImpressions() {
			super("Impressions");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_IMPRESSIONS);
		}
	}

	private final class ActionJournal extends AbstractAction {
		public ActionJournal() {
			super("Journal");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JOURNAL_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_JOURNAL);
		}
	}

	private final class ActionPaiements extends AbstractAction {
		public ActionPaiements() {
			super("Paiements");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PAIEMENTS_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_PAIEMENTS);
		}
	}

	private final class ActionRecouvrements extends AbstractAction {
		public ActionRecouvrements() {
			super("Recouvrements");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PAIEMENTS_16));
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			cardLayout.show(cardPanel, PANEL_RECOUVREMENTS);
		}
	}

}
