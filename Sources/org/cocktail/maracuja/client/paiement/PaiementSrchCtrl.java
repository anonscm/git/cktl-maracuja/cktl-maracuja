/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZLogFileViewerCtrl;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread.DefaultZHeartBeatListener;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVirementFichiers;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.im.ctrl.ImSrchCtrl;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.metier.EOVirementParamBdf;
import org.cocktail.maracuja.client.paiement.PaiementCtrl.TxtFileFilter;
import org.cocktail.maracuja.client.paiement.ui.PaiementSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementSrchCtrl extends CommonCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	private static final String TITLE = "Paiements ";

	private PaiementSrchPanel myPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionNew actionNew = new ActionNew();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionRegenererVirement actionRegenererVirement = new ActionRegenererVirement();
	private final ActionNumeroterPaiement actionNumeroterPaiement = new ActionNumeroterPaiement();
	private final ActionImprimerContenuPaiement actionImprimerContenuPaiement = new ActionImprimerContenuPaiement();
	private final ActionImprimerDetailFichier actionImprimerDetailFichier = new ActionImprimerDetailFichier();
	private final ActionImprRetenues actionImprRetenues = new ActionImprRetenues();
	private final ActionImprimerBordereau actionImprimerBordereau = new ActionImprimerBordereau();
	private final ActionEnregistrer actionEnregistrer = new ActionEnregistrer();
	private final ActionIms actionIms = new ActionIms();
	//    private final ActionVoirFichier actionVoirFichier = new ActionVoirFichier();

	private ZKarukeraDialog win;
	private final HashMap filters;

	private final PaiementSrchPanelListener paiementSrchPanelListener;

	ZWaitingPanelDialog waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));

	private Exception lastException;

	/**
	 * @param editingContext
	 */
	public PaiementSrchCtrl(EOEditingContext editingContext) {
		super(editingContext);
		revertChanges();
		filters = new HashMap();
		paiementSrchPanelListener = new PaiementSrchPanelListener();
		initSubObjects();

	}

	public void initSubObjects() {
		myPanel = new PaiementSrchPanel(paiementSrchPanelListener);
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice()
		})));

		//Construire les qualifiers à partir des saisies utilisateur

		NSMutableArray qualsOrdres = new NSMutableArray();
		if (dicoFiltre.get("paiOrdreMin") != null) {
			qualsOrdres.addObject(EOQualifier.qualifierWithQualifierFormat("paiOrdre>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("paiOrdreMin")).intValue()))));
		}
		if (dicoFiltre.get("paiOrdreMax") != null) {
			qualsOrdres.addObject(EOQualifier.qualifierWithQualifierFormat("paiOrdre<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("paiOrdreMax")).intValue()))));
		}
		if (qualsOrdres.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsOrdres));
		}

		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("paiNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("paiNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("paiNumeroMin")).intValue()))));
		}
		if (dicoFiltre.get("paiNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("paiNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("paiNumeroMax")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		///Montant
		NSMutableArray qualsMontant = new NSMutableArray();
		if (dicoFiltre.get("paiMontantMin") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("paiMontant>=%@", new NSArray(dicoFiltre.get("paiMontantMin"))));
		}
		if (dicoFiltre.get("paiMontantMax") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("paiMontant<=%@", new NSArray(dicoFiltre.get("paiMontantMax"))));
		}
		if (qualsMontant.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMontant));
		}

		///Date
		NSMutableArray qualsDate = new NSMutableArray();
		if (dicoFiltre.get("paiDateCreationMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("paiDateCreation>=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("paiDateCreationMin")))));
		}
		if (dicoFiltre.get("paiDateCreationMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("paiDateCreation<=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("paiDateCreationMax")))));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		return quals;
	}

	/**
	 * Vérifie si la date du jour correspond à l'exercice selectionné.
	 * 
	 * @throws Exception
	 */
	private final void checkExerciceTermine() throws Exception {
		final int annee = ZDateUtil.getYear(ZDateUtil.nowAsDate());
		final int anneeSelectionnee = myApp.appUserInfo().getCurrentExercice().exeExercice().intValue();
		if (anneeSelectionnee < annee) {
			throw new DefaultClientException("Vous ne pouvez plus effectuer de paiements sur l'exercice " + anneeSelectionnee);
		}

	}

	private final void paiementNew() {

		//maintenir la connection avec le serveur
		final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
			public void onBeat() {
				ServerProxy.clientSideRequestMsgToServer(getDefaultEditingContext(), "Creation d'un paiement en cours...", myApp.getIpAdress());
			}

			public void mainTaskStart() {
			}

			public boolean isMainTaskFinished() {
				return false;
			};
		};
		final ZWaitingThread waitingThread = new ZWaitingThread(20000, 20000, defaultZHeartBeatListener);
		waitingThread.start();

		try {
			// Vérifier si l'exercice en cours est dépassé et interdire de créer
			// un nouveau paiement
			checkExerciceTermine();

			PaiementCtrl paiementCtrl = new PaiementCtrl(getEditingContext());
			EOPaiement odp = paiementCtrl.createNewPaiementInWindow(myPanel.getMyDialog());
			if (odp != null) {
				filters.clear();
				Integer paiOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), odp).valueForKey("paiOrdre");
				filters.put("paiOrdreMin", paiOrdre);
				filters.put("paiOrdreMax", paiOrdre);

				myPanel.updateData();
				filters.clear();

			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			//            win.dispose();
			waitingThread.interrupt();
		}

	}

	private final void onSrch() {
		try {
			setWaitCursor(true);
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	/**
     *
     */
	private final void refreshActions() {
		EOPaiement paiement = (EOPaiement) myPanel.getSelectedObject();
		if (paiement == null) {
			actionImprimerContenuPaiement.setEnabled(false);
			actionRegenererVirement.setEnabled(false);
			actionEnregistrer.setEnabled(false);
			actionImprimerBordereau.setEnabled(false);
			actionImprimerDetailFichier.setEnabled(false);
			actionImprRetenues.setEnabled(false);
			actionIms.setEnabled(false);
			actionNumeroterPaiement.setEnabled(false);
			//            actionVoirFichier.setEnabled(false);
		}
		else {
			actionImprimerContenuPaiement.setEnabled(true);
			actionRegenererVirement.setEnabled(true);
			//final String tv = paiement.typeVirement().tviLibelle();
			//            boolean isVirement = EOTypeVirement.typeVirementFichierDisquette.equals(tv) || EOTypeVirement.typeVirementFichierEtebac.equals(tv) || EOTypeVirement.typeVirementFichierVint.equals(tv) ;
			boolean isVirement = EOTypeVirement.MOD_DOM_VIREMENT.equals(paiement.typeVirement().modDom());
			actionEnregistrer.setEnabled(isVirement);
			actionImprimerBordereau.setEnabled(isVirement);
			actionImprimerDetailFichier.setEnabled(isVirement);
			actionIms.setEnabled(true);
			actionImprRetenues.setEnabled(true);
			actionNumeroterPaiement.setEnabled(true);
			//            actionVoirFichier.setEnabled(  isVirement  );
		}
	}

	private final void regenererVirement() {
		try {

			//checkExerciceTermine();

			if (myPanel.getSelectedObject() == null) {
				throw new DefaultClientException("Veuillez sélectionner un paiement (de type virement)");
			}
			EOPaiement paiementTmp = (EOPaiement) myPanel.getSelectedObject();

			if (!EOTypeVirement.MOD_DOM_VIREMENT.equals(paiementTmp.typeVirement().modDom())) {
				throw new DefaultClientException("Vous ne pouvez pas générer de virement pour les paiements qui ne sont pas des virements ");
			}
			PaiementRegenererVirementCtrl paiementRegenererVirementCtrl = new PaiementRegenererVirementCtrl(getEditingContext());
			paiementRegenererVirementCtrl.createNewRegenererVirementInWindow(getMyDialog(), paiementTmp);
			myPanel.updateData();

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void genererFichierEtebac() {
		//TODO Gérer la génération du fichier ETEBAC
	}

	private final void genererFichierVint() {
		//TODO Gérer la génération du fichier VINT
	}

	private void fermer() {
		getMyDialog().onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}

		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private final void voirFichier() {
		ZLogFileViewerCtrl win = new ZLogFileViewerCtrl(getEditingContext());
		try {
			checkExerciceTermine();
			String leContenuFichierVirement = recupererFichier();
			String defaultFileName = "Fichier_virement_tmp.txt";
			File file = new File(new File(myApp.temporaryDir + defaultFileName).getCanonicalPath());
			FileWriter writer = new FileWriter(file);
			writer.write(leContenuFichierVirement);
			writer.close();
			System.out.println("Fichier " + file + " enregistré.");
			win.openDialogFichier(getMyDialog(), file, 250);
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private void checkDroitsIms() throws Exception {
		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_IMSAIS)) {
			throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
		}
	}

	private final void ouvreIms() {
		try {
			checkDroitsIms();
			//checkExerciceTermine();
			setWaitCursor(true);
			final ImSrchCtrl dial = new ImSrchCtrl(myApp.editingContext());
			dial.initData(getSelectedPaiement());
			dial.openDialog(this.getMyDialog(), true);
		} catch (Exception e1) {
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}
	}

	private final String recupererFichier() throws Exception {
		checkExerciceTermine();
		String leContenuFichierVirement = null;
		EOPaiement virement = (EOPaiement) myPanel.getSelectedObject();
		//        EOTypeVirement typeVirement = virement.typeVirement();

		//Récupérer le contenu
		String sql = "select  vir_contenu  from (select vir_contenu from MARACUJA.virement_fichier where pai_ordre=" + ServerProxy.serverPrimaryKeyForObject(getEditingContext(), virement).valueForKey("paiOrdre") + " order by vir_ordre desc) where rownum=1";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		if (res.count() > 0) {
			NSDictionary dico = (NSDictionary) res.objectAtIndex(0);
			leContenuFichierVirement = (String) dico.valueForKey("VIR_CONTENU");
		}

		return leContenuFichierVirement;
	}

	private final void enregistrerFichier() {
		String leContenuFichierVirement = null;
		File f;
		try {
			leContenuFichierVirement = recupererFichier();
			EOPaiement virement = (EOPaiement) myPanel.getSelectedObject();
			EOTypeVirement typeVirement = virement.typeVirement();
			String typeShort = typeVirement.tviFormat();
			if (typeShort == null) {
				throw new DefaultClientException("Type de paiement non pris en charge.");
			}
			//            if ( EOTypeVirement.typeVirementFichierDisquette.equals(   typeVirement.tviLibelle())) {
			//                typeShort = EOTypeVirement.typeVirementFichierDisquetteShort;
			//            }
			//            else if (EOTypeVirement.typeVirementFichierEtebac.equals(   typeVirement.tviLibelle())) {
			//                typeShort = EOTypeVirement.typeVirementFichierEtebacShort;
			//            }else if (EOTypeVirement.typeVirementFichierVint.equals(   typeVirement.tviLibelle())) {
			//                typeShort = EOTypeVirement.typeVirementFichierVintShort;
			//            }
			//            else {
			//                throw new DefaultClientException("Type de paiement non pris en charge.");
			//            }
			//            if ( EOTypeVirement.typeVirementFichierDisquette.equals(   typeVirement.tviLibelle())) {
			//                typeShort = EOTypeVirement.typeVirementFichierDisquetteShort;
			//            }
			//            else if (EOTypeVirement.typeVirementFichierEtebac.equals(   typeVirement.tviLibelle())) {
			//                typeShort = EOTypeVirement.typeVirementFichierEtebacShort;
			//            }else if (EOTypeVirement.typeVirementFichierVint.equals(   typeVirement.tviLibelle())) {
			//                typeShort = EOTypeVirement.typeVirementFichierVintShort;
			//            }
			//            else {
			//                throw new DefaultClientException("Type de paiement non pris en charge.");
			//            }
			//nommer le fichier en fonction du numéro de virement
			String defaultFileName = "Fichier_" + typeShort + "_" + ZStringUtil.extendWithChars("" + virement.paiNumero(), "0", 10, true) + "." + virement.typeVirement().getFileExtension();

			if (leContenuFichierVirement == null || leContenuFichierVirement.length() == 0) {
				throw new DefaultClientException("Le contenu du fichier est vide !");
			}

			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(null);
			f = new File(new File(fileChooser.getCurrentDirectory() + File.separator + defaultFileName).getCanonicalPath());
			fileChooser.setSelectedFile(f);

			TxtFileFilter filter1 = new TxtFileFilter(virement.typeVirement().getFileExtension(), virement.typeVirement().getFileExtension());
			filter1.setExtensionListInDescription(true);
			fileChooser.addChoosableFileFilter(filter1);
			fileChooser.setAcceptAllFileFilterUsed(false);
			fileChooser.setFileFilter(filter1);
			fileChooser.setDialogTitle("Enregistrer-sous..");

			int ret = fileChooser.showSaveDialog(getMyDialog());
			File file = fileChooser.getSelectedFile();

			//	        System.out.println("ret="+ret);
			//	        System.out.println("file="+file);

			if ((ret != JFileChooser.APPROVE_OPTION) || (file == null)) {
				return;
			}

			if (file.exists()) {
				if (!showConfirmationDialog("Confirmation", "Le fichier " + file + " existe déjà, voulez-vous l'écraser ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}
			}

			FileWriter writer = new FileWriter(file);
			writer.write(leContenuFichierVirement);
			writer.close();

			System.out.println("Fichier " + file + " enregistré.");

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void numeroterPaiement() {
		try {
			EOPaiement paiement = (EOPaiement) myPanel.getSelectedObject();
			//if (paiement.paiNumero() == null || paiement.paiNumero().intValue() == 0) {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous lancer la numerotation du paiement du " + ZConst.FORMAT_DATESHORT.format(paiement.paiDateCreation()) + " ?<br> Les émargements liés au paiement seront également générés.", ZMsgPanel.BTLABEL_NO)) {
				waitingDialog.setTitle("Numerotation du paiement et generation des emargements");
				waitingDialog.setTopText("Veuillez patienter ...");
				waitingDialog.setModal(true);

				try {

					final PaiementNumeroterThread thread = new PaiementNumeroterThread();
					thread.start();

					if (thread.isAlive()) {
						waitingDialog.setVisible(true);
					}

					if (lastException != null) {
						throw lastException;
					}

					//rafraichir la ligne
					myPanel.getPaiementListPanel().fireSeletedTableRowUpdated();

				} catch (Exception e) {
					showErrorDialog(e);
					getEditingContext().revert();
				} finally {
					waitingDialog.setVisible(false);
					if (waitingDialog != null) {
						waitingDialog.dispose();
					}
				}
			}

			//			}
			//			else {
			//				showInfoDialog("Le paiement est déjà numéroté (" + paiement.paiNumero().intValue() + ").");
			//			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public final void imprimerBordereau() {
		try {
			final EOPaiement currentPaiement = (EOPaiement) myPanel.getSelectedObject();
			if (EOTypeVirement.TVI_FORMAT_BDF.equals(currentPaiement.typeVirement().tviFormat())) {
				final String leContenuFichierVirement = recupererFichier();

				if (leContenuFichierVirement == null || leContenuFichierVirement.length() == 0) {
					throw new DefaultClientException("Le contenu du fichier est vide !");
				}

				final int nbOperations = (leContenuFichierVirement.length() / FactoryProcessVirementFichiers.LONGUEUR_LIGNE_BDF) - 2;
				final EOVirementParamBdf virementParamBdf = PaiementProxyCtrl.getViremenParamBDFForPaiement(getEditingContext(), currentPaiement);
				if (virementParamBdf == null) {
					throw new DefaultClientException("Aucun paramètre de virement valide n'a été récupéré pour ce paiement.");
				}
				PaiementProxyCtrl.imprimerBordereau(this, nbOperations, currentPaiement, virementParamBdf);
			}
			else {
				PaiementProxyCtrl.imprimerBordereau(this, -1, currentPaiement, null);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}



	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		try {
			win1.open();
		} finally {
			win1.dispose();
		}
	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Générer un nouveau paiement");
		}

		public void actionPerformed(ActionEvent e) {
			paiementNew();
		}
	}

	public class ActionImprimerContenuPaiement extends AbstractAction {
		public ActionImprimerContenuPaiement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail du paiement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerContenuPaiement(PaiementSrchCtrl.this, (EOPaiement) myPanel.getSelectedObject());

		}
	}

	public class ActionImprimerDetailFichier extends AbstractAction {
		public ActionImprimerDetailFichier() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le contenu du fichier de virement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerDetailFichierVirement(PaiementSrchCtrl.this, (EOPaiement) myPanel.getSelectedObject());
		}
	}

	public class ActionImprRetenues extends AbstractAction {
		public ActionImprRetenues() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail des retenues");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerPaiementRetenues(PaiementSrchCtrl.this, (EOPaiement) myPanel.getSelectedObject());
		}
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.NAME, "Enregistrer le fichier");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			enregistrerFichier();

		}
	}

	public class ActionNumeroterPaiement extends AbstractAction {
		public ActionNumeroterPaiement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Numéroter le paiement");
			putValue(Action.SHORT_DESCRIPTION, "Numéroter le paiement et générer les émargements suite a incident lors de sa generation");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			numeroterPaiement();

		}
	}

	public class ActionIms extends AbstractAction {
		public ActionIms() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Intérêts moratoires");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			ouvreIms();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Bordereau d'accompagnement");
			putValue(Action.SHORT_DESCRIPTION, "Imprimer le bordereau d'accompagnement");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}

	//    public class ActionVoirFichier extends AbstractAction {
	//        public ActionVoirFichier() {
	//            super();
	//            setEnabled(false);
	//            putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_VIEWTEXT_16));
	//            putValue(Action.NAME, "Contenu fichier");
	//            putValue(Action.SHORT_DESCRIPTION, "");
	//        }
	//
	//
	//
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            voirFichier();
	//
	//        }
	//    }

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionRegenererVirement extends AbstractAction {
		public ActionRegenererVirement() {
			super("Regénérer");
			setEnabled(false);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Regénérer un fichier de virement");
		}

		public void actionPerformed(ActionEvent e) {
			regenererVirement();
		}
	}

	private final class PaiementSrchPanelListener implements PaiementSrchPanel.IPaiementSrchPanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionNew() {
			return actionNew;
		}

		public HashMap getFilters() {
			return filters;
		}

		public Action actionSrch() {
			return actionSrch;
		}

		public NSArray getPaiements() {
			//Créer la condition à partir des filtres
			try {
				return ZFinder.fetchArray(getEditingContext(), EOPaiement.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), new NSArray(new Object[] {
						EOPaiement.SORT_PAI_DATE_DESC
				}), true);
			} catch (Exception e) {
				showErrorDialog(e);
				return new NSArray();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementSrchPanel.IPaiementSrchPanelListener#actionRegenererVirement()
		 */
		public Action actionRegenererVirement() {
			return actionRegenererVirement;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementSrchPanel.IPaiementSrchPanelListener#actionImprimerContenuPaiement()
		 */
		public Action actionImprimerContenuPaiement() {
			return actionImprimerContenuPaiement;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementSrchPanel.IPaiementSrchPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			refreshActions();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementSrchPanel.IPaiementSrchPanelListener#actionEnregistrerFichier()
		 */
		public Action actionEnregistrerFichier() {
			return actionEnregistrer;
		}

		public Action actionImprimerBordereau() {
			return actionImprimerBordereau;
		}

		public Action actionImprRetenues() {
			return actionImprRetenues;
		}

		public Action actionImprimerDetailFichierBDF() {
			return actionImprimerDetailFichier;
		}

		public Action actionOuvreIms() {
			return actionIms;
		}

		public Action actionNumeroterPaiement() {
			return actionNumeroterPaiement;
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		//non gere en auto
		return null;
	}

	public String title() {
		return TITLE;
	}

	public EOPaiement getSelectedPaiement() {
		return (EOPaiement) myPanel.getSelectedObject();
	}

	public final class PaiementNumeroterThread extends Thread {

		/**
         * 
         */
		public PaiementNumeroterThread() {
			super();
		}

		public void run() {
			lastException = null;
			try {
				EOPaiement paiement = getSelectedPaiement();
				try {
					PaiementProxyCtrl.numeroterPaiement(getDateJourneeComptable(), paiement, ApplicationClient.wantShowTrace());
				} catch (Exception e) {
					e.printStackTrace();
				}
				//generation emargements
				PaiementProxyCtrl.genererEmargements(waitingDialog, paiement);

				//Synchroniser modifs sur client
				PaiementProxyCtrl.invaliderEcritureDetailsRelies(waitingDialog, paiement);

			} catch (Exception e) {
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}
	}

}
