/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.factory.process.FactoryProcessModificationRibModePaiement;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlancoVisa;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel;
import org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener;
import org.cocktail.maracuja.client.paiement.ui.PaiementChgtPanel;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementChgtCtrl extends CommonCtrl {

	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Modification des modes de paiement et ribs";
	private PaiementChgtPanel myPanel;
	private PaiementChgtMandatPanelListener paiementChgtMandatPanelListener;

	private NSArray modePaiements;
	private ActionClose actionClose = new ActionClose();

	/**
	 * @param editingContext
	 */
	public PaiementChgtCtrl(EOEditingContext editingContext) {
		super(editingContext);
		try {
			modePaiements = EOsFinder.getModePaiementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
			if (modePaiements.count() == 0) {
				throw new DefaultClientException("Aucun mode de paiement valide n'a été récupéré");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
		initGUI();
	}

	private void initGUI() {
		paiementChgtMandatPanelListener = new PaiementChgtMandatPanelListener();
		myPanel = new PaiementChgtPanel(new PaiementChgtPanelListener());
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}

		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private void fermer() {
		getMyDialog().onCloseClick();
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			win.open();
		} finally {
			win.dispose();
		}
	}

	private final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class PaiementChgtPanelListener implements PaiementChgtPanel.IPaiementChgtPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtPanel.IPaiementChgtPanelListener#mandatPanelListener()
		 */
		public IPaiementChgtMandatPanelListener mandatPanelListener() {
			return paiementChgtMandatPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtPanel.IPaiementChgtPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

	}

	private final class PaiementChgtMandatPanelListener implements PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener {
		private final HashMap filters = new HashMap();
		private ZEOComboBoxModel ribModel;
		private ZEOComboBoxModel modePaiementModel;

		private HashMap myDico = new HashMap(2);
		private Action actionAnnuler;
		private Action actionEnregistrer;

		/**
         *
         */
		public PaiementChgtMandatPanelListener() {
			actionAnnuler = new ActionAnnuler();
			actionEnregistrer = new ActionEnregistrer();

			try {
				ribModel = new ZEOComboBoxModel(new NSArray(), EORib.RIB_LIB_LONG_KEY, null, null);
				modePaiementModel = new ZEOComboBoxModel(modePaiements, "modLibelleLong", null, null);
			} catch (Exception e) {
				showErrorDialog(e);
			}

			enableActions(false);
		}

		private final void enableActions(final boolean enabled) {
			actionEnregistrer.setEnabled(enabled);
			actionAnnuler.setEnabled(enabled);
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#getMandats()
		 */
		public NSArray getMandats() {
			//Créer la condition à partir des filtres
			try {
				NSArray res = ZFinder.fetchArray(getEditingContext(), EOMandat.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), null, true);
				getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), res));
				return res;
			} catch (Exception e) {
				showErrorDialog(e);
				return new NSArray();
			}
		}

		private final NSArray getRibsForFournisAndModePaiement(EOFournisseur fournis, EOModePaiement modPaiement) {
			NSArray res = new NSArray();
			if (fournis != null) {
				if ((modPaiement != null) && EOModePaiement.MODDOM_VIREMENT.equals(modPaiement.modDom())) {
					res = EOsFinder.getRibsValides(getEditingContext(), fournis);
				}
			}
			return res;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#onMandatSelectionChanged()
		 */
		public void onMandatSelectionChanged() {
			resetInfos();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return filters;
		}

		private final void onSrch() {
			try {
				setWaitCursor(true);
				myPanel.updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		private final void onEnregistrer() {
			try {

				EOMandat mandat = myPanel.getPaiementChgtMandatPanel().getSelectedMandat();
				if (mandat == null) {
					throw new DataCheckException("Aucun mandat n'est sélectionné.");
				}
				//Memoriser etat du mandat
				//	String etat = mandat.manEtat();
				//	Integer manId = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), mandat).valueForKey(EOMandat.MAN_ID_KEY);

				EOModePaiement newModePaiement = (EOModePaiement) modePaiementModel.getSelectedEObject();
				EORib newRib = (EORib) myDico.get("rib");

				ZLogger.debug(mandat);
				ZLogger.debug(newModePaiement);
				ZLogger.debug(newRib);

				EOEcriture ecritureGeneree = null;
				if (newModePaiement == null) {
					throw new DataCheckException("Aucun mode de paiement n'est sélectionné.");
				}

				if (EOModePaiement.MODDOM_VIREMENT.equals(newModePaiement.modDom()) && newRib == null) {
					throw new DataCheckException("Vous devez obligatoirement définir un rib pour le mode de paiement " + newModePaiement.modLibelle());
				}

				ZLogger.debug(mandat.manNumero());
				ZLogger.debug(newModePaiement.modLibelle());
				ZLogger.debug(newRib);

				//implementer l'appel à la factory et faire un saveChanges
				FactoryProcessModificationRibModePaiement factoryProcessModificationRibModePaiement = new FactoryProcessModificationRibModePaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				//Si le mode de paiement a changé
				if (!mandat.modePaiement().equals(newModePaiement)) {
					if (mandat.modePaiement().isPaiementHT() != newModePaiement.isPaiementHT()) {
						throw new Exception("Il est impossible de passer d'un mode de paiement de type 'Paiement HT' vers un type 'Paiement TTC'");
					}

					if ((mandat.modePaiement().isAExtourner() && !newModePaiement.isAExtourner()) || (!mandat.modePaiement().isAExtourner() && newModePaiement.isAExtourner())) {
						throw new Exception("Il est impossible de passer d'un mode de paiement 'A extourner' vers un autre type de mode de paiement et inversement");
					}

					boolean goOn = true;
					boolean forceEcritureIfPcoNumNewModePaiementNull = false;
					boolean generateEcriture = true;
					//verifier si la ctp du mandat est déjà émargee
					if (mandat.ecritureDetailCtpNonEmargee() == null && newModePaiement.planComptableVisa() != null) {
						goOn = showConfirmationDialog("Confirmation", "L'écriture reliée au mandat pour le compte de tiers a été émargée. Souhaitez-vous quand même effectuer le changement de mode de paiement ? Si vous répondez Oui, aucune écriture automatique vers le compte "
								+ newModePaiement.planComptableVisa().pcoNum() + " ne sera générée, et le compte de tiers relié au mandat ne sera donc pas modifié.", ZMsgPanel.BTLABEL_NO);
					}

					if (!goOn) {
						return;
					}

					//Si le visa n'est pas emargé et que le mode de paiement de destination n'a pas de pconumvisa, on recupere le compte de visa par defaut de l'imputation budgetaire.
					if (mandat.ecritureDetailCtpNonEmargee() != null && newModePaiement.planComptableVisa() == null) {
						EOPlancoVisa pvi = EOPlancoVisa.getPlancoVisaForPcoOrdo(getEditingContext(), mandat.planComptable().pcoNum(), mandat.exercice());
						if (pvi != null && !mandat.ecritureDetailCtpNonEmargee().planComptable().pcoNum().equals(pvi.plancoExerCtrePartie().pcoNum())) {

							int rep = showConfirmationCancelDialog("Confirmation", "L'écriture reliée au mandat pour le compte de tiers a été passée sur le compte <b>" + mandat.ecritureDetailCtpNonEmargee().planComptable().pcoNum() + "</b>. Souhaitez-vous générer une écriture vers le compte <b>"
									+ pvi.plancoExerCtrePartie().pcoNum() + "</b> ? "
									, ZMsgPanel.BTLABEL_CANCEL);
							if (rep == ZMsgPanel.MR_CANCEL) {
								return;
							}
							forceEcritureIfPcoNumNewModePaiementNull = (rep == ZMsgPanel.MR_YES);
						}
					}
					//FIXME rod 10/01/2014  il y a un pb ici si mandat.ecritureDetailCtpNonEmargee() == null. repartir de la ligne 300 et ne pas passer ici 

					if (mandat.ecritureDetailCtpNonEmargee().ecdResteEmarger().abs().compareTo(mandat.ecritureDetailCtpNonEmargee().ecdMontant().abs()) != 0) {
						goOn = showConfirmationDialog("Confirmation", "L'écriture reliée au mandat pour le compte de tiers est partiellement émargée, il sera impossible de générer une écriture automatique, souhaitez-vous continuer quand même ?", ZMsgPanel.BTLABEL_NO);

						if (!goOn) {
							return;
						}
						forceEcritureIfPcoNumNewModePaiementNull = false;
						generateEcriture = false;
					}

					if (goOn) {
						ecritureGeneree = factoryProcessModificationRibModePaiement.modifierMandatModePaiement(getEditingContext(), mandat, newModePaiement, myApp.appUserInfo().getUtilisateur(), getExercice(), forceEcritureIfPcoNumNewModePaiementNull, generateEcriture);
					}
				}

				//si le rib a changé
				ZLogger.debug("newModePaiement.modDom()", newModePaiement.modDom());
				if (EOModePaiement.MODDOM_VIREMENT.equals(newModePaiement.modDom())) {
					if (!newRib.equals(mandat.rib())) {
						ZLogger.debug("Ton Rib change...");
						factoryProcessModificationRibModePaiement.modifierRibMandat(getEditingContext(), mandat, newRib);
					}
				}

				ZLogger.debug(getEditingContext());
				getEditingContext().saveChanges();

				//Numeroter
				if (ecritureGeneree != null) {
					//On numerote
					KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
					try {
						FactoryProcessJournalEcriture myFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
						myFactoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), ecritureGeneree, myKFactoryNumerotation);
						String msgFin = "\n\nL'écriture générée par le changement de mode de paiement porte le n° " + ecritureGeneree.ecrNumero();
						myApp.showInfoDialog(msgFin);
					} catch (Exception e) {
						System.out.println("ERREUR LORS DE LA NUMEROTATION ECRITURE...");
						e.printStackTrace();
						throw new Exception(e);
					}
				}
				myPanel.getPaiementChgtMandatPanel().getPaiementPanelMandatOk().fireTableRowUpdated(myPanel.getPaiementChgtMandatPanel().getPaiementPanelMandatOk().selectedRowIndex());
				resetInfos();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				getEditingContext().revert();
			}
		}

		private final void onAnnuler() {
			try {
				resetInfos();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		private final void resetInfos() {
			try {
				ZLogger.debug("mandat change");

				myDico.put(EOMandat.MODE_PAIEMENT_KEY, null);
				myDico.put(EOMandat.RIB_KEY, null);

				EOMandat man = myPanel.getPaiementChgtMandatPanel().getSelectedMandat();
				modePaiementModel.setSelectedEObject(null);
				ribModel.setSelectedEObject(null);

				ZLogger.debug("getSelectedMandat", man);

				if (man != null) {
					myDico.put(EOMandat.MODE_PAIEMENT_KEY, man.modePaiement());
					myDico.put(EOMandat.RIB_KEY, man.rib());
					modePaiementModel.setSelectedEObject(man.modePaiement());
					ribModel.setSelectedEObject(man.rib());
					myPanel.getPaiementChgtMandatPanel().getPaiementChgtFormPanel().getMyModePaiementField().setEnabled(true);
				}
				else {
					myPanel.getPaiementChgtMandatPanel().getPaiementChgtFormPanel().getMyModePaiementField().setEnabled(false);
				}

				myPanel.getPaiementChgtMandatPanel().getPaiementChgtFormPanel().updateData();
				enableActions(false);

			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {
			NSMutableArray quals = new NSMutableArray();

			//Les conditions obligatoires
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.BORDEREAU_KEY + "." + EOBordereau.BOR_ETAT_KEY + "=%@", new NSArray(EOBordereau.BordereauVise)));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.MAN_ETAT_KEY + "=%@", new NSArray(EOMandat.mandatVise)));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.EXERCICE_KEY + "=%@", new NSArray(getExercice())));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.MAN_HT_KEY + ">=0", new NSArray()));

			//ajouter filtre sur paiement non prestations internes

			//Construire les qualifiers à partir des saisies utilisateur
			///Numero
			NSMutableArray qualsNum = new NSMutableArray();
			if (dicoFiltre.get("manNumeroMin") != null) {
				qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.MAN_NUMERO_KEY + ">=%@", new NSArray(new Integer(((Number) dicoFiltre.get("manNumeroMin")).intValue()))));
			}
			if (dicoFiltre.get("manNumeroMax") != null) {
				qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.MAN_NUMERO_KEY + "<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("manNumeroMax")).intValue()))));
			}

			if (qualsNum.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsNum));
			}

			NSMutableArray qualsNumBord = new NSMutableArray();
			if (dicoFiltre.get("borNumMin") != null) {
				qualsNumBord.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY + ">=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNumMin")).intValue()))));
			}
			if (dicoFiltre.get("borNumMax") != null) {
				qualsNumBord.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY + "<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNumMax")).intValue()))));
			}
			if (qualsNumBord.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsNumBord));
			}

			if (dicoFiltre.get(EOGestion.GES_CODE_KEY) != null) {
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.GESTION_KEY + "." + EOGestion.GES_CODE_KEY + "=%@", new NSArray(dicoFiltre.get(EOGestion.GES_CODE_KEY))));
			}

			if (qualsNum.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsNum));
			}
			return quals;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return new ActionSrch();
		}

		private final class ActionSrch extends AbstractAction {
			public ActionSrch() {
				super();
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
			}

			/**
			 * Appelle updateData();
			 */
			public void actionPerformed(ActionEvent e) {
				onSrch();
			}
		}

		private final class ActionEnregistrer extends AbstractAction {
			public ActionEnregistrer() {
				super("Enregistrer");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les modifications");
			}

			/**
			 * Appelle updateData();
			 */
			public void actionPerformed(ActionEvent e) {
				onEnregistrer();
			}
		}

		private final class ActionAnnuler extends AbstractAction {
			public ActionAnnuler() {
				super("Annuler");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_UNDO_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler les modifications");
			}

			/**
			 * Appelle updateData();
			 */
			public void actionPerformed(ActionEvent e) {
				onAnnuler();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#getModePaiement()
		 */
		public Object getModePaiement() {
			return myDico.get(EOMandat.MODE_PAIEMENT_KEY);
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#getRibModel()
		 */
		public DefaultComboBoxModel getRibModel() {
			return ribModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#getRib()
		 */
		public EORib getRib() {
			return (EORib) myDico.get(EOMandat.RIB_KEY);
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#selectedRibChanged()
		 */
		public void selectedRibChanged() {
			myDico.put(EOMandat.RIB_KEY, ribModel.getSelectedEObject());
			myPanel.getPaiementChgtMandatPanel().getPaiementChgtFormPanel().getMyRibInfoPanel().updateData();
			enableActions(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#actionAnnuler()
		 */
		public Action actionAnnuler() {
			return actionAnnuler;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#actionEnregistrer()
		 */
		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#getModePaiementModel()
		 */
		public DefaultComboBoxModel getModePaiementModel() {
			return modePaiementModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener#selectedModPaiementChanged()
		 */
		public void selectedModPaiementChanged() {
			EOModePaiement mp = (EOModePaiement) modePaiementModel.getSelectedEObject();
			myDico.put(EOMandat.MODE_PAIEMENT_KEY, mp);
			try {
				if (myPanel.getPaiementChgtMandatPanel().getSelectedMandat() != null) {
					NSArray res = getRibsForFournisAndModePaiement(myPanel.getPaiementChgtMandatPanel().getSelectedMandat().fournisseur(), mp);
					ribModel.updateListWithData(res);
					if (ribModel.getSize() > 0) {
						ribModel.setSelectedEObject((NSKeyValueCoding) res.objectAtIndex(0));
					}
				}
				else {
					ribModel.updateListWithData(new NSArray());
				}
				myPanel.getPaiementChgtMandatPanel().getPaiementChgtFormPanel().updateData();
				enableActions(true);
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
