/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.awt.CardLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CalendrierBdfCtrl;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction;
import org.cocktail.maracuja.client.factory.FactorySauverVirement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVirementFichiers;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.metier.EOVirementParamBdf;
import org.cocktail.maracuja.client.metier.EOVirementParamEtebac;
import org.cocktail.maracuja.client.metier.EOVirementParamSepa;
import org.cocktail.maracuja.client.metier.EOVirementParamVint;
import org.cocktail.maracuja.client.metier.IVirementParam;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8;
import org.cocktail.maracuja.client.remotecall.ServerCallsepa;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementRegenererVirementCtrl extends CommonCtrl {
	public static final String ACTIONID = ZActionCtrl.IDU_PAGE;

	/** Le choix du format de virement */
	private static final String STEP7 = "step7";
	private static final String STEP8 = "step8";
	//    private static final String STEP9="step9";

	private static final String TITLE = "Regénération du virement";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);

	private PaiementPanelStep7 step7Panel;
	private PaiementPanelStep8 step8Panel;

	private PaiementPanelStep7Listener step7Listener;
	private PaiementPanelStep8Listener step8Listener;

	private JPanel cardPanel;
	private CardLayout cardLayout;
	private ZKarukeraStepPanel2 currentPanel;

	private final DefaultActionClose actionClose;
	private ZEOComboBoxModel myComptabiliteModel;

	private final HashMap stepPanels = new HashMap();
	private EOPaiement currentPaiement;
	private String leContenuFichierVirement;

	private int nbOperations = -1;

	private File defaultFile;
	//	private final NSArray calendrierBDF;
	private final CalendrierBdfCtrl calendrierBDF;

	/**
     *
     */
	public PaiementRegenererVirementCtrl(EOEditingContext ec) throws Exception {
		super(ec);

		actionClose = new DefaultActionClose();
		actionClose.setEnabled(true);

		//		calendrierBDF = EOsFinder.getCalendrierBdfAutour(getEditingContext(), ZDateUtil.getDateOnly(ZDateUtil.now()));
		calendrierBDF = new CalendrierBdfCtrl(getEditingContext());
		calendrierBDF.loadDatesAroundNow();

		if (calendrierBDF.count() < 15) {
			showInfoDialog("Le calendrier de la Banque de France ne semble pas être complet. Il n'y aura peut-etre pas de contrôle possible pour les dates d'échange et de réglement.");
		}

		try {

			//creer les listeners
			step7Listener = new PaiementPanelStep7Listener();
			step8Listener = new PaiementPanelStep8Listener();

			step7Panel = new PaiementPanelStep7(step7Listener);
			step8Panel = new PaiementPanelStep8(step8Listener);

			stepPanels.put(STEP7, step7Panel);
			stepPanels.put(STEP8, step8Panel);

			//            initGUI();

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void initGUI() {

		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Iterator iter = stepPanels.keySet().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			ZKarukeraStepPanel2 panel = (ZKarukeraStepPanel2) stepPanels.get(element);
			panel.setMyDialog(getMyDialog());
			panel.initGUI();
			cardPanel.add(panel, element);
		}

		//TODO activer format etebac et vint quand ils seront prêts
		//        step7Panel.enableFormat(Step7Listener.FORMAT_ETEBAC, false);
		//        step7Panel.enableFormat(Step7Listener.FORMAT_VINT, false);

		//Preselections
		//TODO activer format etebac et vint quand ils seront prêts
		//        step7Panel.preselectFormat(Step7Listener.FORMAT_BDF);

		changeStep(STEP7);
	}

	/**
	 * Ferme la fenetre.
	 */
	private final void close() {
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}

	/**
	 * Imprime le bordereau d'accompagnement
	 */
	public final void imprimerBordereau() {
		if (EOTypeVirement.TVI_FORMAT_BDF.equals(currentPaiement.typeVirement().tviFormat())) {
			PaiementProxyCtrl.imprimerBordereau(this, nbOperations, currentPaiement, getSelectedVirementParam());
		}
		else {
			PaiementProxyCtrl.imprimerBordereau(this, -1, currentPaiement, null);
		}

	}

	/**
	 * Imprime le contenu du paiement.
	 */
	public final void imprimerContenuPaiement() {
		PaiementProxyCtrl.imprimerContenuPaiement(this, currentPaiement);
	}

	public final void regenererVirement() {
		//appeler factory pour le virement
		try {
			if (step7Listener.getDateValeur() == null) {
				throw new DataCheckException("Vous devez indiquer une date de valeur.");
			}

			if (!calendrierBDF.isDateDansCalendrier(step7Listener.getDateValeur())) {
				Date nextDate = calendrierBDF.getFirstNextDate(step7Listener.getDateValeur());
				if (nextDate != null) {
					int choix = showConfirmationCancelDialog("Confirmation", "La date de valeur que vous avez indiqué (" + ZConst.FORMAT_DATESHORT.format(step7Listener.getDateValeur()) + ") n'est pas une date ouvrable de la banque de France, souhaitez-vous la remplacer par la date "
							+ ZConst.FORMAT_DATESHORT.format(nextDate) + " ?" + "<br><br>Cliquez sur Non pour générer le fichier à la la date du " + ZConst.FORMAT_DATESHORT.format(step7Listener.getDateValeur()) + "<br>Cliquez sur Annuler pour ne pas générer le fichier", ZMsgPanel.BTLABEL_YES);

					if (choix == ZMsgPanel.MR_CANCEL) {
						return;
					}
					else if (choix == ZMsgPanel.MR_YES) {
						step7Listener.setDateValeur(nextDate);
					}

				}
			}


			final EOVirementParamBdf virementParamBdf = getSelectedVirementParamBdf();
			final EOVirementParamEtebac virementParamEtebac = getSelectedVirementParamEtebac();
			final EOVirementParamVint virementParamVint = getSelectedVirementParamVint();
			final EOVirementParamSepa virementParamSepa = getSelectedVirementParamSepa();
			//	String leContenuFichier = null;

			final EOTypeVirement leNewTypeVirement = getSelectedTypeVirement();

			if (virementParamBdf == null && virementParamEtebac == null && virementParamVint == null && virementParamSepa == null) {
				throw new DefaultClientException("Le format du virement n'est pas reconnu.");
			}

			String contenu = null;
			String noCompte = null;
			if (virementParamBdf != null) {
				noCompte = virementParamBdf.vpbCompteTpg();
				FactoryProcessVirementFichiers factoryProcessVirementFichiers = new FactoryProcessVirementFichiers(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				contenu = factoryProcessVirementFichiers.genererFichierBanqueDeFrance(getEditingContext(), currentPaiement.getDepenses(), currentPaiement.ordreDePaiements(), virementParamBdf, new NSTimestamp(step7Listener.getDateValeur()), step7Panel.grouperLignesParRib());

				if (contenu == null) {
					throw new DefaultClientException("Le contenu du fichier est vide.");
				}
				nbOperations = (contenu.length() / FactoryProcessVirementFichiers.LONGUEUR_LIGNE_BDF) - 2;
				leContenuFichierVirement = contenu;
			}
			else if (virementParamSepa != null) {
				noCompte = virementParamSepa.vpsDftIban();
				NSArray depenses = EODepense.fetchAll(getEditingContext(), new EOKeyValueQualifier(EODepense.MANDAT_KEY + "." + EOMandat.PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, currentPaiement), null);
				NSArray odps = EOOrdreDePaiement.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOOrdreDePaiement.PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, currentPaiement), null);
				String identifiantFichierVirement = "Paiement numero " + currentPaiement.paiNumero() + " du " + ZConst.FORMAT_DATESHORT.format(currentPaiement.paiDateCreation());
				Integer nbPaiementPourJournee = ServerCallsepa.clientSideRequestNbPaiementJour(getEditingContext(), getSelectedVirementParamSepa(), currentPaiement.paiDateCreation());
				String numeroDeFichierDansLaJournee3Car = ZStringUtil.extendWithChars("" + nbPaiementPourJournee.intValue() + 1, "0", 3, true);

				NSDictionary res = ServerCallsepa.clientSideRequestGenereVirementSepa(currentPaiement.exercice().exeExercice().toString(), currentPaiement.paiNumero().toString(), getEditingContext(), virementParamSepa, new NSTimestamp(new Date()), depenses, odps, currentPaiement.paiMontant(),
						currentPaiement.paiNbVirements(), new NSTimestamp(step7Listener.getDateValeur()),
						numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, step7Panel.getOptionRegroupementLignes());

				contenu = (String) res.valueForKey("contenu");
				Integer nbOperationsOut = (Integer) res.valueForKey("nbTransactions");
				nbOperations = nbOperationsOut.intValue();

				if (contenu == null) {
					throw new DefaultClientException("Le contenu du fichier est vide.");
				}
				leContenuFichierVirement = contenu;
			}

			//On enregistre le contenu du fichier dans la base
			NSTimestamp laDateValeur = new NSTimestamp(ZDateUtil.getDateOnly(step7Listener.getDateValeur()));
			ZLogger.debug("datevaleur", laDateValeur);

			FactorySauverVirement factorySauverVirement = new FactorySauverVirement(myApp.wantShowTrace());
			factorySauverVirement.sauverFichier(getEditingContext(), currentPaiement, leNewTypeVirement, myApp.appUserInfo().getUtilisateur(), leContenuFichierVirement, laDateValeur, noCompte);

			getEditingContext().saveChanges();

			prepareDefaultFile(currentPaiement);

			changeStep(STEP8);

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	/**
	 * @return renvoi le virementParam sélectionné (il faut le caster ensuite)
	 */
	public final IVirementParam getSelectedVirementParam() {
		final IVirementParam selected = (IVirementParam) step7Listener.getFormatsVirementModel().getSelectedEObject();
		return selected;
	}

	/**
	 * @return
	 */
	public final EOVirementParamBdf getSelectedVirementParamBdf() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_BDF.equals(format)) {
			return (EOVirementParamBdf) getSelectedVirementParam();
		}
		return null;
	}

	public final EOVirementParamSepa getSelectedVirementParamSepa() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_SEPASCT.equals(format)) {
			return (EOVirementParamSepa) getSelectedVirementParam();
		}
		return null;
	}

	public final EOVirementParamEtebac getSelectedVirementParamEtebac() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_ETEBAC.equals(format)) {
			return (EOVirementParamEtebac) getSelectedVirementParam();
		}
		return null;
	}

	public final EOVirementParamVint getSelectedVirementParamVint() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_VINT.equals(format)) {
			return (EOVirementParamVint) getSelectedVirementParam();
		}
		return null;
	}

	public final EOTypeVirement getSelectedTypeVirement() {
		if (currentPaiement == null) {
			return null;
		}
		return currentPaiement.typeVirement();
		//		
		//		final EOEnterpriseObject selected = getSelectedVirementParam();
		//		if (selected == null) {
		//			return null;
		//		}
		//		return (EOTypeVirement) selected.valueForKey("typevirement");
	}

	public final String getSelectedFormat() {
		final EOTypeVirement tv = getSelectedTypeVirement();
		if (tv == null) {
			return null;
		}
		return tv.tviFormat();

	}

	/**
	 * Change le panel actif (en effectuant un updateData sur le nouveau).
	 * 
	 * @param newStep
	 */
	private final void changeStep(final String newStep) {
		cardLayout.show(cardPanel, newStep);
		currentPanel = (ZKarukeraStepPanel2) stepPanels.get(newStep);
		try {
			currentPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		//        ZKarukeraDialog win = new ZKarukeraDialog(dial, TITLE,true);
		setMyDialog(win);
		initGUI();
		//        step7Panel.setMyDialog(win);
		cardPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(cardPanel);
		win.pack();
		return win;
	}

	////////////////////////////////////////////////////////////////////////////////

	private class PaiementPanelStep7Listener implements PaiementPanelStep7.Step7Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final IZDatePickerFieldModel dateValeurModel;
		private Date dateValeur;
		private ZEOComboBoxModel formatsVirementModel;
		private final ActionAfficheCalendrierBdf actionAfficheCalendrierBdf = new ActionAfficheCalendrierBdf();

		/**
         *
         */
		public PaiementPanelStep7Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionGenererFichier();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(true);
			actionSpecial1.setEnabled(true);
			dateValeurModel = new DateValeurModel();

			try {
				formatsVirementModel = new ZEOComboBoxModel(NSARRAYVIDE, "libelle", null, null);
			} catch (Exception e) {
				formatsVirementModel = null;
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {

			}

			/**
			 * @see org.cocktail.maracuja.client.paiement.PaiementRegenererVirementCtrl.DefaultActionPrev#isVisible()
			 */
			public boolean isVisible() {
				return false;
			}

		}

		private final class ActionAfficheCalendrierBdf extends AbstractAction {
			public ActionAfficheCalendrierBdf() {
				super("");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Afficher le calendrier BDF");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				try {
					final String filePath = ReportFactoryClient.imprimerBdfCalendrier(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), getMyDialog());
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7.Step7Listener#dateValeurModel()
		 */
		public IZDatePickerFieldModel dateValeurModel() {
			return dateValeurModel;
		}

		public Date getDateValeur() {
			return dateValeur;
		}

		public void setDateValeur(Date dateValeur) {
			this.dateValeur = dateValeur;
		}

		//        private final class DateValeurModel implements IZTextFieldModel {
		//            /**
		//             * @see org.cocktail.maracuja.client.zutil.ui.forms.ZTextField.IZTextFieldModel#getValue()
		//             */
		//            public Object getValue() {
		//                return dateValeur;
		//            }
		//
		//            /**
		//             * @see org.cocktail.maracuja.client.zutil.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		//             */
		//            public void setValue(Object value) {
		//                if (value instanceof Date) {
		//                    dateValeur = (Date) value;
		//                }
		//                else {
		//                    dateValeur = null;
		//                }
		//            }
		//
		//        }
		private final class DateValeurModel implements IZDatePickerFieldModel {
			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return dateValeur;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				System.out.println("DateValeurModel.setValue()");
				System.out.println(value);

				if (value instanceof Date) {
					setDateValeur((Date) value);
				}
				else {
					//					dateValeur = null;
					setDateValeur(null);
				}

				System.out.println(dateValeur);
				System.out.println("*******");

				//				if (!calendrierBDF.isDateDansCalendrier(dateValeur)) {
				//					System.out.println("date pas dans calendrier");
				//					Date nextDate = calendrierBDF.getFirstNextDate(dateValeur);
				//					if (nextDate != null) {
				//						dateValeur = nextDate;
				//						System.out.println(dateValeur);
				//						System.out.println("*******");
				//					}
				//				}

			}

			public Window getParentWindow() {
				return getMyDialog();
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7.Step7Listener#montantPaiement()
		 */
		public BigDecimal montantPaiement() {
			if (currentPaiement != null) {
				return currentPaiement.paiMontant();
			}
			return null;

		}

		public ZEOComboBoxModel getFormatsVirementModel() {
			return formatsVirementModel;
		}

		public boolean isDateEchangeValide() {
			return calendrierBDF.isDateDansCalendrier(dateValeur);
		}

		public Action actionAfficheCalendrierBdf() {
			return actionAfficheCalendrierBdf;
		}

		public boolean unEcritureDetailCompte5FieldEnabled() {
			return false;
		}

		public boolean isGrouperLignesParNoFactureVisible() {
			return getSelectedVirementParamSepa() != null;
		}
	}

	private class PaiementPanelStep8Listener implements PaiementPanelStep8.Step8Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final ActionImprimerBordereau actionImprimerBordereau;
		private final ActionImprimerContenuPaiement actionImprimerContenuPaiement;
		private final ActionImprRetenues actionImprRetenues = new ActionImprRetenues();
		private final ActionImprimerDetailFichier actionImprimerDetailFichierBDF = new ActionImprimerDetailFichier();
		private final ActionEnregistrer actionEnregistrer;

		/**
         *
         */
		public PaiementPanelStep8Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new DefaultActionNext();
			actionSpecial1 = new DefaultActionGenererFichier();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(false);
			actionSpecial1.setEnabled(false);

			actionEnregistrer = new ActionEnregistrer();
			actionImprimerBordereau = new ActionImprimerBordereau();
			actionImprimerContenuPaiement = new ActionImprimerContenuPaiement();

			actionEnregistrer.setEnabled(true);
			actionImprimerBordereau.setEnabled(true);
			actionImprimerContenuPaiement.setEnabled(true);
			actionImprimerDetailFichierBDF.setEnabled(true);
			actionImprRetenues.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#actionEnregistrer()
		 */
		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#actionImprimerBordereau()
		 */
		public Action actionImprimerBordereau() {
			return actionImprimerBordereau;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#actionImprimerContenuPaiement()
		 */
		public Action actionImprimerContenuPaiement() {
			return actionImprimerContenuPaiement;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#typePaiement()
		 */
		public String typePaiement() {
			String t = EOModePaiement.MODDOM_VIREMENT;
			t = t + "_" + getSelectedFormat();
			//            t = t + "_" + step7Panel.getSelectedFormat();
			return t;
		}

		public File getFileVirement() {
			return defaultFile;
		}

		public Action actionImprRetenues() {
			return actionImprRetenues;
		}

		public Action actionImprimerDetailFichierBDF() {
			return actionImprimerDetailFichierBDF;
		}

	}

	public final void prepareDefaultFile(final EOPaiement virement) throws Exception {
		final String defaultFileName = "Fichier_" + getSelectedFormat() + "_" + ZStringUtil.extendWithChars("" + virement.paiNumero(), "0", 10, true) + "." + virement.typeVirement().getFileExtension();
		defaultFile = FilePaiementCtrl.saveFile(leContenuFichierVirement, myApp.temporaryDir, defaultFileName, nbOperations);
		//        defaultFile = saveFile(leContenuFichierVirement, defaultFileName);        
	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionGenererFichier extends ZStepAction {
		public DefaultActionGenererFichier() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Virement");
			putValue(Action.SHORT_DESCRIPTION, "Générer le fichier de virement");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			regenererVirement();
		}

	}

	private class DefaultActionNext extends ZStepAction {
		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le Bordereau d'accompagnement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}

	public class ActionImprimerDetailFichier extends AbstractAction {
		public ActionImprimerDetailFichier() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le contenu du fichier de virement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerDetailFichierVirement(PaiementRegenererVirementCtrl.this, currentPaiement);
			//PaiementProxyCtrl.imprimerDetailFichierVirementBDF(PaiementRegenererVirementCtrl.this, currentPaiement);
		}
	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.NAME, "Enregistrer le fichier");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				FilePaiementCtrl.enregistrerFichier(PaiementRegenererVirementCtrl.this, currentPaiement, leContenuFichierVirement, getSelectedFormat(), nbOperations);
				//                enregistrerFichier(currentPaiement);
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}
	}

	public class ActionImprimerContenuPaiement extends AbstractAction {
		public ActionImprimerContenuPaiement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail du paiement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerContenuPaiement();

		}
	}

	public class ActionImprRetenues extends AbstractAction {
		public ActionImprRetenues() {
			super();
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail des retenues");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerPaiementRetenues(PaiementRegenererVirementCtrl.this, currentPaiement);
		}
	}

	/**
	 * @param myDialog
	 * @return
	 */
	public void createNewRegenererVirementInWindow(Window dial, EOPaiement lePaiement) {
		ZKarukeraDialog win = null;
		try {
			win = createModalDialog(dial);
			currentPaiement = lePaiement;
			if (currentPaiement == null) {
				throw new DefaultClientException("Le paiement à regénrer est nul.");
			}
			if (!EOTypeVirement.MOD_DOM_VIREMENT.equals(currentPaiement.typeVirement().modDom())) {
				throw new DefaultClientException("Vous ne pouvez pas générer de virement pour les paiement qui ne sont pas des virements");
			}

			//            if ( FinderTypeVirement.leTypeVirementCaisse(getEditingContext()).equals(currentPaiement.typeVirement()) || FinderTypeVirement.leTypeVirementCheque(getEditingContext()).equals(currentPaiement.typeVirement()) ) {
			//                throw new DefaultClientException("Vous ne pouvez pas générer de virement pour les paiement de type " + EOTypeVirement.typeVirementCaisse +" ou " + EOTypeVirement.typeVirementCheque);
			//            }
			//            

			NSMutableArray res = new NSMutableArray();
			if (getSelectedTypeVirement() != null) {
				res.addObjectsFromArray(getSelectedTypeVirement().toVirementParamBdfs(new EOKeyValueQualifier(EOVirementParamBdf.VPB_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOVirementParamBdf.ETAT_VALIDE)));
				res.addObjectsFromArray(getSelectedTypeVirement().toVirementParamSepas(new EOKeyValueQualifier(EOVirementParamSepa.VPS_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOVirementParamSepa.ETAT_VALIDE)));
			}
			step7Listener.formatsVirementModel.updateListWithData(res);

			currentPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			if (win != null) {
				win.dispose();
			}

		}
		return;
	}

	private static class TxtFileFilter extends javax.swing.filechooser.FileFilter {

		//        private static String TYPE_UNKNOWN = "Type Unknown";
		//        private static String HIDDEN_FILE = "Hidden File";

		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		/**
		 * Creates a file filter. If no filters are added, then all files are accepted.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter() {
			filters = new Hashtable();
		}

		/**
		 * Creates a file filter that accepts files with the given extension. Example: new ExampleFileFilter("jpg");
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension) {
			this(extension, null);
		}

		/**
		 * Creates a file filter that accepts the given file type. Example: new ExampleFileFilter("jpg", "JPEG Image Images"); Note that the "."
		 * before the extension is not needed. If provided, it will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension, String description) {
			this();
			if (extension != null) {
				addExtension(extension);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Creates a file filter from the given string array. Example: new ExampleFileFilter(String {"gif", "jpg"}); Note that the "." before the
		 * extension is not needed adn will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters) {
			this(filters, null);
		}

		/**
		 * Creates a file filter from the given string array and description. Example: new ExampleFileFilter(String {"gif", "jpg"},
		 * "Gif and JPG Images"); Note that the "." before the extension is not needed and will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Return true if this file should be shown in the directory pane, false if it shouldn't. Files that begin with "." are ignored.
		 * 
		 * @see #getExtension
		 * @see FileFilter#accepts
		 */
		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		/**
		 * Return the extension portion of the file's name .
		 * 
		 * @see #getExtension
		 * @see FileFilter#accept
		 */
		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		/**
		 * Adds a filetype "dot" extension to filter against. For example: the following code will create a filter that filters out all files except
		 * those that end in ".jpg" and ".tif": ExampleFileFilter filter = new ExampleFileFilter(); filter.addExtension("jpg");
		 * filter.addExtension("tif"); Note that the "." before the extension is not needed and will be ignored.
		 */
		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		/**
		 * Returns the human readable description of this filter. For example: "JPEG and GIF Image Files (*.jpg, *.gif)"
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 * @see FileFilter#getDescription
		 */
		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description + " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "." + (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ." + (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				}
				else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		/**
		 * Sets the human readable description of this filter. For example: filter.setDescription("Gif and JPG Images");
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 */
		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		/**
		 * Determines whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description
		 * was provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see isExtensionListInDescription
		 */
		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		/**
		 * Returns whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description was
		 * provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see setExtensionListInDescription
		 */
		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		//non gere en auto
		return null;
	}

	public String title() {
		return TITLE;
	}

	//	private final boolean isDateDansCalendrierBdf(final Date dateEchange) {
	//		return (calendrierBDF.indexOfObject(dateEchange) != NSArray.NotFound);
	//		
	//	}

}
