/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.io.File;
import java.io.FileWriter;

import javax.swing.JFileChooser;

import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.paiement.PaiementCtrl.TxtFileFilter;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZMsgPanel;

public class FilePaiementCtrl {

	public static final File saveFile(final String contenu, final String dir, final String fileName, final int nbOperations) throws Exception {
		if (contenu == null || contenu.length() == 0) {
			throw new DefaultClientException("Le contenu du fichier est vide !");
		}

		if (nbOperations == 0) {
			throw new DefaultClientException("Le nombre de virements à effectuer est nul. Ceci est peut-être dû au fait que " +
					"vous avez saisi des retenues égales aux montant des factures à payer, " +
					"dans ce cas c'est normal, il n'y a pas de fichier à générer. Dans le cas contraire " +
					"il y a un problème. Sortez de l'application et vérifiez si le paiement apparait bien " +
					"dans la liste des paiements, et regénérez le fichier.");
		}

		final File f = new File(new File(dir + fileName).getCanonicalPath());
		final FileWriter writer = new FileWriter(f);
		writer.write(contenu);
		writer.close();
		return f;
	}

	/**
	 * Enregistre le fichier
	 */
	public static final void enregistrerFichier(final CommonCtrl owner, final EOPaiement virement, final String leContenuFichierVirement, final String format, final int nbOperations) throws Exception {

		// nommer le fichier en fonction du numéro de virement
		String defaultFileName = "Fichier_" + format + "_" + ZStringUtil.extendWithChars("" + virement.paiNumero(), "0", 10, true) + "." + virement.typeVirement().getFileExtension();

		File f;

		if (leContenuFichierVirement == null || leContenuFichierVirement.length() == 0) {
			throw new DefaultClientException("Le contenu du fichier est vide !");
		}

		if (nbOperations == 0) {
			throw new DefaultClientException("Le nombre de virements à effectuer est nul. Ceci est peut-être dû au fait que " +
						"vous avez saisi des retenues égales aux montant des factures à payer, " +
						"dans ce cas c'est normal, il n'y a pas de fichier à générer. Dans le cas contraire " +
						"il y a un problème. Sortez de l'application et vérifiez si le paiement apparait bien " +
						"dans la liste des paiements, et regénérez le fichier.");
		}

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(null);
		f = new File(new File(fileChooser.getCurrentDirectory() + File.separator + defaultFileName).getCanonicalPath());
		fileChooser.setSelectedFile(f);

		TxtFileFilter filter1 = new TxtFileFilter(virement.typeVirement().getFileExtension(), virement.typeVirement().getFileExtension());
		filter1.setExtensionListInDescription(true);
		fileChooser.addChoosableFileFilter(filter1);
		fileChooser.setAcceptAllFileFilterUsed(false);
		fileChooser.setFileFilter(filter1);
		fileChooser.setDialogTitle("Enregistrer-sous..");

		int ret = fileChooser.showSaveDialog(owner.getMyDialog());
		File file = fileChooser.getSelectedFile();

		// System.out.println("ret="+ret);
		// System.out.println("file="+file);

		if ((ret != JFileChooser.APPROVE_OPTION) || (file == null)) {
			return;
		}

		if (file.exists()) {
			if (!owner.showConfirmationDialog("Confirmation", "Le fichier " + file + " existe déjà, voulez-vous l'écraser ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		FileWriter writer = new FileWriter(file);
		writer.write(leContenuFichierVirement);
		writer.close();

		System.out.println("Fichier " + file + " enregistré.");

	}

}
