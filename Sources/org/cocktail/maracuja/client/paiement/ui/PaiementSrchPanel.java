/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementSrchPanel extends ZKarukeraPanel {
	private final IPaiementSrchPanelListener myListener;

	private final PaiementSrchFilterPanel filterPanel;
	private final PaiementSrchListPanel paiementListPanel;

	/**
	 * @param editingContext
	 */
	public PaiementSrchPanel(IPaiementSrchPanelListener listener) {
		super();
		myListener = listener;

		filterPanel = new PaiementSrchFilterPanel(new PaiementSrchFilterPanelListener());
		paiementListPanel = new PaiementSrchListPanel(new PaiementSrchListPanelListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		filterPanel.setMyDialog(getMyDialog());
		filterPanel.initGUI();
		paiementListPanel.initGUI();

		this.setLayout(new BorderLayout());
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		tmp.add(encloseInPanelWithTitle("Paiements déjà générés", null, ZConst.BG_COLOR_TITLE, paiementListPanel, null, null), BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		paiementListPanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionNew());
		list.add(myListener.actionRegenererVirement());
		list.add(myListener.actionImprimerContenuPaiement());
		list.add(myListener.actionImprimerDetailFichierBDF());
		list.add(myListener.actionEnregistrerFichier());
		list.add(myListener.actionImprimerBordereau());
		list.add(myListener.actionImprRetenues());
		list.add(myListener.actionNumeroterPaiement());
		list.add(myListener.actionOuvreIms());
		//        list.add(myListener.actionVoirFichier());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @return L'objet Ordre de paiement actuelment sélectionné.
	 */
	public EOEnterpriseObject getSelectedObject() {
		return paiementListPanel.selectedObject();
	}

	public interface IPaiementSrchPanelListener {
		public Action actionClose();

		//        public Action actionVoirFichier();
		public Action actionImprimerContenuPaiement();

		public Action actionImprimerBordereau();

		public Action actionImprimerDetailFichierBDF();

		public Action actionImprRetenues();

		public Action actionNumeroterPaiement();

		public Action actionEnregistrerFichier();

		public Action actionNew();

		public Action actionOuvreIms();

		public NSArray getPaiements();

		/**
		 * @return un dictioniare contenant les filtres
		 */
		public HashMap getFilters();

		public Action actionSrch();

		public Action actionRegenererVirement();

		public void onSelectionChanged();

	}

	private final class PaiementSrchFilterPanelListener implements PaiementSrchFilterPanel.IPaiementSrchFilterPanel {

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpRechercheFilterPanel.IOdpRechercheFilterPanel#getFilters()
		 */
		public HashMap getFilters() {
			return myListener.getFilters();
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.OdpRechercheFilterPanel.IOdpRechercheFilterPanel#getActionSrch()
		 */
		public Action getActionSrch() {
			return myListener.actionSrch();
		}

	}

	private final class PaiementSrchListPanelListener implements IZTablePanelMdl {
		public NSArray getData() {
			return myListener.getPaiements();
		}

		public void onDbClick() {

		}

		public void selectionChanged() {
			myListener.onSelectionChanged();
		}

	}

	public PaiementSrchListPanel getPaiementListPanel() {
		return paiementListPanel;
	}

}
