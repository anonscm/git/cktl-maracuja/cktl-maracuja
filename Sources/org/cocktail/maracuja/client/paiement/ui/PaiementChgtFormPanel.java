/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.odp.ui.RibInfoPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementChgtFormPanel extends ZKarukeraPanel {
	private IPaiementChgtFormPanelListener myListener;

	//    private ZLabelComboBoxField myModePaiementField;
	private JComboBox myModePaiementField;
	private JComboBox myRibField;
	private JComponent myRibSelectBox;

	private RibInfoPanel myRibInfoPanel;
	private static final int DEFAULT_LABEL_WIDTH = 100;

	//    private ZEOComboBoxModel myModePaiementModel;

	/**
     * 
     */
	public PaiementChgtFormPanel(IPaiementChgtFormPanelListener listener) {
		super();
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		setLayout(new BorderLayout());

		myRibField = new JComboBox(myListener.getRibModel());
		myRibField.addActionListener(new RibSelectListener());

		myModePaiementField = new JComboBox(myListener.getModePaiementModel());
		myModePaiementField.addActionListener(new ModePaiementListener());

		//        myModePaiementField = new ZLabelComboBoxField("Mode de paiement", ZLabelField.LABELONLEFT, -1, myModePaiementModel);
		//        ((JComboBox)myModePaiementField.getContentComponent()).addActionListener(new ModePaiementListener());

		myRibSelectBox = new ZLabeledComponent("Rib", myRibField, ZLabeledComponent.LABELONLEFT, -1);

		//  JLabel labelRib = new JLabel("Si le nouveau Rib n'est pas présent dans la liste, crééez-le dans l'annuaire.");

		myRibInfoPanel = new RibInfoPanel(new RibInfoPanelListener());
		myRibInfoPanel.initGui();

		Box col1 = Box.createVerticalBox();
		col1.add(buildLine(new ZLabeledComponent("Mode de paiement", myModePaiementField, ZLabeledComponent.LABELONLEFT, -1)));
		col1.add(Box.createVerticalGlue());

		Box col2 = Box.createVerticalBox();
		col2.add(buildLine(myRibSelectBox));
		col2.add(buildLine(myRibInfoPanel));

		JPanel p2 = new JPanel(new BorderLayout());
		p2.add(col1, BorderLayout.WEST);
		p2.add(col2, BorderLayout.CENTER);
		p2.add(buildRightPanel(), BorderLayout.EAST);

		setBorder(BorderFactory.createTitledBorder((String) null));
		add(p2, BorderLayout.NORTH);

		add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionEnregistrer());
		list.add(myListener.actionAnnuler());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		//        myModePaiementModel.setSelectedEObject((NSKeyValueCoding)myListener.getModePaiement());
		if ((myListener.getModePaiement() != null) && (EOModePaiement.MODDOM_VIREMENT.equals(((EOModePaiement) myListener.getModePaiement()).modDom()))) {
			myRibField.setEnabled(true);
			//            myRibInfoPanel.setVisible(true);
		}
		else {
			myRibField.setEnabled(false);
			//            myRibInfoPanel.setVisible(false);            
		}
		myRibInfoPanel.updateData();
	}

	//    public final void setSaisieEnabled(boolean enabled) {
	//        myListener.actionAnnuler().setEnabled(enabled);
	//        myListener.actionEnregistrer().setEnabled(enabled);
	//    }

	public interface IPaiementChgtFormPanelListener {
		/** le mode de paiement selectionne */
		public Object getModePaiement();

		public DefaultComboBoxModel getRibModel();

		public DefaultComboBoxModel getModePaiementModel();

		//        public NSArray getModePaiements();
		public EORib getRib();

		//        public void setModePaiement(NSKeyValueCoding selectedEObject);
		public void selectedRibChanged();

		public Action actionAnnuler();

		public Action actionEnregistrer();

		/**
         * 
         */
		public void selectedModPaiementChanged();

	}

	private final class RibSelectListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.selectedRibChanged();

		}
	}

	private final class ModePaiementListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//            myListener.setModePaiement(myModePaiementModel.getSelectedEObject());    

			myListener.selectedModPaiementChanged();

		}
	}

	private final class RibInfoPanelListener implements RibInfoPanel.IRibInfoPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibGuichet()
		 */
		public String getRibGuichet() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribGuich();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibNumero()
		 */
		public String getRibNumero() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribNum();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibBanque()
		 */
		public String getRibBanque() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribCodBanc();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibCle()
		 */
		public String getRibCle() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribCle();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibBic()
		 */
		public String getRibBic() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribBic();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibIban()
		 */
		public String getRibIban() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribIban();
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.odp.ui.RibInfoPanel.IRibInfoPanelListener#getRibTitCo()
		 */
		public Object getRibTitCo() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribTitco();
			}
			return null;
		}

		public String getRibAgence() {
			if (myListener.getRib() != null) {
				return myListener.getRib().ribAgence();
			}
			return null;
		}

	}

	public RibInfoPanel getMyRibInfoPanel() {
		return myRibInfoPanel;
	}

	public JComboBox getMyModePaiementField() {
		return myModePaiementField;
	}
}
