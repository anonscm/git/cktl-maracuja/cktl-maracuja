/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.text.DateFormat;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;

/**
 * Panel pour selection date valeur pour paiement cheque et caisse
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep9 extends ZKarukeraStepPanel2 {
	private Step9Listener myListener;
	private ZDatePickerField dateValeurField;
	private JCheckBox unEcritureDetailCompte5Field;

	/**
	 * @param listener
	 */
	public PaiementPanelStep9(Step9Listener listener) {
		super(listener);
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		p.add(buildDateValeurPanel(), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildDateValeurPanel() {
		unEcritureDetailCompte5Field = new JCheckBox("Créer une seule ligne d'écriture par compte de paiement");
		unEcritureDetailCompte5Field.setEnabled(myListener.unEcritureDetailCompte5FieldEnabled());
		unEcritureDetailCompte5Field.setSelected(true);

		ZFormPanel p = new ZFormPanel();
		JLabel l = new JLabel("Date de valeur");
		p.add(l);
		p.add(dateValeurField);
		p.add(unEcritureDetailCompte5Field);

		JPanel t = new JPanel(new BorderLayout());
		t.add(new JPanel(new BorderLayout()), BorderLayout.NORTH);
		t.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		t.add(p, BorderLayout.SOUTH);

		return t;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		dateValeurField = new ZDatePickerField(myListener.dateValeurModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateValeurField.getMyTexfield().setColumns(8);
		super.initGUI();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return myListener.getTile();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return myListener.getCommentaire();
	}

	public interface Step9Listener extends ZStepListener {
		public IZDatePickerFieldModel dateValeurModel();

		public boolean unEcritureDetailCompte5FieldEnabled();

		public String getTile();

		public String getCommentaire();

	}

	public final boolean unEcritureDetailCompte5() {
		return (unEcritureDetailCompte5Field.isEnabled() && unEcritureDetailCompte5Field.isSelected());
	}

}
