/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.text.Format;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;


public class RetenueAjoutPanel extends ZKarukeraPanel {
    private static final int LABEL_WIDTH = 80;
    private final IBrouillardAjoutPanelListener _listener;

    protected ZFormPanel montant; 
    protected ZFormPanel libelle; 
    protected JComboBox myTypeRetenueComboBox;
    
    
    
    public RetenueAjoutPanel(final IBrouillardAjoutPanelListener listener) {
        _listener = listener;
        initGUI();
        
    }
    
    public void initGUI() {
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        
        
        this.add(buildFormPanel(), BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);           
    }

    public void updateData() throws Exception {
        montant.updateData();
    }
    

    

    
    
    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(_listener.actionValider());
        a.add(_listener.actionClose());        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraPanel.buildHorizontalButtonsFromActions(a));
        return p;
    }    
    
    protected JPanel buildFormPanel() {

        montant = ZFormPanel.buildLabelField("Montant", new ZNumberField( new ZTextField.DefaultTextFieldModel(_listener.getFilters(), EORetenue.RET_MONTANT_KEY)  , new Format[]{ZConst.FORMAT_EDIT_NUMBER}, ZConst.FORMAT_DECIMAL_COURT) , LABEL_WIDTH);
        ((ZTextField)montant.getMyFields().get(0)).getMyTexfield().setColumns(10);
        
        
//        final ZTextArea t = new ZTextArea(new ZTextField.DefaultTextFieldModel(_listener.getFilters(), EORetenue.RET_LIBELLE_KEY));
//        t.getMyTextArea().setColumns(40);
//        t.getMyTextArea().setRows(5);
        
        libelle = ZFormPanel.buildLabelField("Détail", new ZTextField.DefaultTextFieldModel(_listener.getFilters(), EORetenue.RET_LIBELLE_KEY), LABEL_WIDTH);
        ((ZTextField)libelle.getMyFields().get(0)).getMyTexfield().setColumns(40);
        
        myTypeRetenueComboBox = new JComboBox(_listener.getTypeRetenueModel());
//        myGestionComboBox.addActionListener(new ComboGestionListener());
        
        ZFormPanel tr = ZFormPanel.buildLabelField("Type de retenue", myTypeRetenueComboBox, LABEL_WIDTH);
        
        
        
  
        
//        final JPanel p = new JPanel(new BorderLayout());
        final ArrayList comps = new ArrayList();
        comps.add(tr);
        comps.add(libelle);
        comps.add(montant);
//        
//        p.add(ZUiUtil.buildGridColumn(comps, 10), BorderLayout.WEST);
//        p.add(new JPanel(new BorderLayout()));
        
        
        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZUiUtil.buildGridColumn(comps, 5) , BorderLayout.NORTH);
        p.add(new JPanel(new BorderLayout()) , BorderLayout.CENTER);
        p.setBorder(ZUiUtil.createMargin());
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
        this.add(p, BorderLayout.CENTER);        
        
        final JPanel p2 = new JPanel(new BorderLayout());
        p2.add(p, BorderLayout.NORTH);
        p2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return p2;
        
    }
    
    

    
    
    
    public interface IBrouillardAjoutPanelListener {
        public Action actionValider();
        public ZEOComboBoxModel getTypeRetenueModel();
        public Action actionClose();
        public Map getFilters();
    }

}
