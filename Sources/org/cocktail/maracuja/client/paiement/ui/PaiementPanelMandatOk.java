/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.math.BigDecimal;
import java.util.Date;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


/**
 * Panel liste de mandats.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelMandatOk extends ZTablePanel {

	public static final String COL_MAN_NUMERO = EOMandat.MAN_NUMERO_KEY;
	public static final String COL_GESTION = EOMandat.GESTION_KEY + "." + EOGestion.GES_CODE_KEY;
	public static final String COL_BORDEREAU = EOMandat.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY;
	public static final String COL_PLAN_COMPTABLE = EOMandat.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY;
	public static final String COL_FOURNISSEUR = EOMandat.FOURNISSEUR_KEY + "." + EOFournisseur.NOM_AND_PRENOM_KEY;
	public static final String COL_MAN_HT = EOMandat.MAN_HT_KEY;
	public static final String COL_MAN_TVA = EOMandat.MAN_TVA_KEY;
	public static final String COL_MAN_TTC = EOMandat.MAN_TTC_KEY;
	public static final String COL_MONTANT_A_PAYER = EOMandat.MONTANT_A_PAYER_KEY;
	public static final String COL_MODE_PAIEMENT = EOMandat.MODE_PAIEMENT_KEY + "." + EOModePaiement.MOD_LIBELLE_KEY;
	public static final String COL_MAN_DATE_REMISE = EOMandat.MAN_DATE_REMISE_KEY;
	public static final String COL_MAN_ETAT = EOMandat.MAN_ETAT_KEY;

	protected ZEOTableModelColumn colManNumero;
	protected ZEOTableModelColumn colGestion;
	protected ZEOTableModelColumn colManEtat;
	protected ZEOTableModelColumn colBordereau;
	protected ZEOTableModelColumn colPlanComptable;
	protected ZEOTableModelColumn colFournisseur;
	protected ZEOTableModelColumn colManHt;
	protected ZEOTableModelColumn colManTva;
	protected ZEOTableModelColumn colManTtc;
	protected ZEOTableModelColumn colapayer;
	protected ZEOTableModelColumn colModePaiement;
	protected ZEOTableModelColumn colDateReception;

	public PaiementPanelMandatOk(ZTablePanel.IZTablePanelMdl listener) {
		super(listener);
		colManNumero = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_NUMERO_KEY, "N°", 64);
		colManNumero.setAlignment(SwingConstants.CENTER);
		colManNumero.setColumnClass(Integer.class);

		colGestion = new ZEOTableModelColumn(myDisplayGroup, EOMandat.GESTION_KEY + "." + EOGestion.GES_CODE_KEY, "Code gestion", 80);
		colGestion.setAlignment(SwingConstants.CENTER);

		colManEtat = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_ETAT_KEY, "Etat", 48);
		colManEtat.setAlignment(SwingConstants.CENTER);

		colBordereau = new ZEOTableModelColumn(myDisplayGroup, EOMandat.BORDEREAU_KEY + "." + EOBordereau.BOR_NUM_KEY, "Bordereau", 80);
		colBordereau.setAlignment(SwingConstants.LEFT);
		colBordereau.setColumnClass(Integer.class);

		colPlanComptable = new ZEOTableModelColumn(myDisplayGroup, EOMandat.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY, "Imp.", 88);
		colPlanComptable.setAlignment(SwingConstants.LEFT);

		colFournisseur = new ZEOTableModelColumn(myDisplayGroup, EOMandat.FOURNISSEUR_KEY + "." + EOFournisseur.NOM_AND_PRENOM_KEY, "Fournisseur", 173);

		colManHt = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_HT_KEY, "Montant HT", 80);
		colManHt.setAlignment(SwingConstants.RIGHT);
		colManHt.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colManHt.setColumnClass(BigDecimal.class);

		colManTva = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_TVA_KEY, "Montant TVA", 80);
		colManTva.setAlignment(SwingConstants.RIGHT);
		colManTva.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colManTva.setColumnClass(BigDecimal.class);

		colManTtc = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_TTC_KEY, "Montant TTC", 80);
		colManTtc.setAlignment(SwingConstants.RIGHT);
		colManTtc.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colManTtc.setColumnClass(BigDecimal.class);

		colapayer = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MONTANT_A_PAYER_KEY, "A Payer", 80);
		colapayer.setAlignment(SwingConstants.RIGHT);
		colapayer.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colapayer.setColumnClass(BigDecimal.class);

		colModePaiement = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MODE_PAIEMENT_KEY + "." + EOModePaiement.MOD_LIBELLE_KEY, "Mode de paiement", 48);
		colModePaiement.setAlignment(SwingConstants.CENTER);

		colDateReception = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_DATE_REMISE_KEY, "Date réception", 60);
		colDateReception.setAlignment(SwingConstants.CENTER);
		colDateReception.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colDateReception.setColumnClass(Date.class);

		colsMap.clear();
		colsMap.put(COL_GESTION, colGestion);
		colsMap.put(COL_BORDEREAU, colBordereau);
		colsMap.put(COL_MAN_NUMERO, colManNumero);
		colsMap.put(COL_PLAN_COMPTABLE, colPlanComptable);
		colsMap.put(COL_FOURNISSEUR, colFournisseur);
		colsMap.put(COL_MODE_PAIEMENT, colModePaiement);
		colsMap.put(COL_MAN_TTC, colManTtc);
		colsMap.put(COL_MONTANT_A_PAYER, colapayer);
	}

}
//public class PaiementPanelMandatOk extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {
//    
//    private ZEOTable myEOTable;
//    private ZEOTableModel myTableModel;
//    private EODisplayGroup myDisplayGroup;
//    private TableSorter myTableSorter;
//    private Vector myCols;
//    
//    
//    private IPaiementPanelMandatOkListener myListener;
//    
//    /**
//     * 
//     */
//    public PaiementPanelMandatOk(IPaiementPanelMandatOkListener listener) {
//        super();
//        myListener = listener;
//        myDisplayGroup = new EODisplayGroup();
//    }
//    
//    
//    
//    /**
//     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
//     */
//    public void initGUI() {
//        initTableModel();
//        initTable();
//        this.setLayout(new BorderLayout());
//        setBorder(BorderFactory.createEmptyBorder(0,4,4,4));
//        this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);        
//    }
//    
//    
//    
//    
//    
//    /**
//     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
//     */
//    public void updateData() throws Exception {
//        myDisplayGroup.setObjectArray(myListener.getMandats());
//        myEOTable.updateData();
//    }
//    
//    
//    
//    
//    
//    
//    
//    /**
//     * Initialise la table à afficher (le modele doit exister)
//     */
//    private void initTable() {
//        myEOTable = new ZEOTable(myTableSorter);
//        
//        myEOTable.addListener(this);
//        myTableSorter.setTableHeader(myEOTable.getTableHeader());
//        
////		initPopupMenu();
//    }
//    
//    /**
//     * Initialise le modeele le la table à afficher.
//     *
//     */
//    private final void initTableModel() {
//        myCols = new Vector(9,1);
//        ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup,"manNumero","N°",64);
//        col1.setAlignment(SwingConstants.CENTER);
//        col1.setColumnClass(Integer.class);
//        ZEOTableModelColumn col12 = new ZEOTableModelColumn(myDisplayGroup,"gestion.gesCode","Code gestion",80);
//        col12.setAlignment(SwingConstants.CENTER);
//        
//        ZEOTableModelColumn colBord = new ZEOTableModelColumn(myDisplayGroup,"bordereau.borNum","Bordereau",80);
//        colBord.setAlignment(SwingConstants.LEFT);
//        
//        ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup,"planComptable.pcoNum","Imp.",88);
//        col11.setAlignment(SwingConstants.LEFT);  
//        ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"fournisseur.nomAndPrenom","Fournisseur",173);
////		ZEOTableModelColumn col31 = new ZEOTableModelColumn(myDisplayGroup,"manNbPiece","Pièces",48);
////		col31.setAlignment(SwingConstants.CENTER);
//        ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup,"manHt","Montant HT",80);		
//        col32.setAlignment(SwingConstants.RIGHT);
//        col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
//        col32.setColumnClass(BigDecimal.class);
//        
//        ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup,"manTva","Montant TVA",80);		
//        col3.setAlignment(SwingConstants.RIGHT);
//        col3.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER ) ;
//        col3.setColumnClass(BigDecimal.class);
//        
//        ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup,"manTtc","Montant TTC",80);
//        col4.setAlignment(SwingConstants.RIGHT);
//        col4.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER) ;
//        col4.setColumnClass(BigDecimal.class);
//        
//        ZEOTableModelColumn colapayer = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MONTANT_A_PAYER_KEY,"A Payer",80);
//        colapayer.setAlignment(SwingConstants.RIGHT);
//        colapayer.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER) ;
//        colapayer.setColumnClass(BigDecimal.class);
//        
//        
//        ZEOTableModelColumn colModePaiement = new ZEOTableModelColumn(myDisplayGroup,"modePaiement.modLibelle","Mode de paiement",48);
//        colModePaiement.setAlignment(SwingConstants.CENTER);
//        
//        ZEOTableModelColumn colDateReception = new ZEOTableModelColumn(myDisplayGroup,"manDateRemise","Date réception",60);
//        colDateReception.setAlignment(SwingConstants.CENTER);
//        colDateReception.setFormatDisplay(ZConst.FORMAT_DATESHORT);
//        colDateReception.setColumnClass(Date.class);		
//        
//        
//        myCols.add(col12);
//        myCols.add(colBord);
//        myCols.add(col1);
//        myCols.add(col11);
////		myCols.add(colDateReception);
//        myCols.add(col2);
////		myCols.add(col31);
//        myCols.add(colModePaiement);
////		myCols.add(col32);
////		myCols.add(col3);
//        myCols.add(col4);
//        myCols.add(colapayer);
//        
//        
//        myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
//        myTableSorter = new TableSorter (myTableModel);
//        
////		myTableModel.addTableModelListener(this);
//    }
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    public interface IPaiementPanelMandatOkListener {
//        /**
//         * @return Les mandats à afficher
//         */
//        public NSArray getMandats();
//        
//        /**
//         * 
//         */
//        public void onSelectionChanged();
//        
//        
//    }
//    
//    
//    
//    
//    
//    
//    
//    /**
//     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
//     */
//    public void onDbClick() {
//        return;
//    }
//    
//    
//    
//    
//    
//    /**
//     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
//     */
//    public void onSelectionChanged() {
//        myListener.onSelectionChanged();
//        
//    }
//    
//    
//    /**
//     * Met à jour le row selectionne a partir des donnees.
//     */
//    public void selectedRowUpdateData() {
//        myTableModel.fireTableRowUpdated(myEOTable.getSelectedRow());        
//    }
//    
//    
//    
//    public EOMandat getSelectedObject() {
//        return (EOMandat) myTableModel.getSelectedObject();
//    }
//    
//    
//    public ZEOTableModel getMyTableModel() {
//        return myTableModel;
//    }
//    public ZEOTable getMyEOTable() {
//        return myEOTable;
//    }
//}
