/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZUIContainer;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * Affiche un panel de sélection de mandats.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelMandatSelect extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {
  
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;
	private TableSorter myTableSorter;
	private Vector myCols;
	
	private MandatSelectFilterPanel mandatSelectFilterPanel;
	private JPopupMenu myPopupMenu;
	
	private IPaiementPanelMandatSelectListener myListener;
    
	/**
     * 
     */
    public PaiementPanelMandatSelect(IPaiementPanelMandatSelectListener listener) {
        super();
        myListener = listener;
        myDisplayGroup = new EODisplayGroup();
    }
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        initTableModel();
        initTable();
        initPopupMenu();
		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0,4,4,4));
		
		mandatSelectFilterPanel = new MandatSelectFilterPanel(myListener.getMandatSelectFilterPanelListener());
		
//		JPanel p = new JPanel(new BorderLayout());
		this.add(mandatSelectFilterPanel, BorderLayout.NORTH);
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);        
    }

    
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(myListener.getAllMandats());
        myEOTable.updateData();
    }
    
    
    
    
    
    

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
	}
		
	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private final void initTableModel() {
		myCols = new Vector(9,1);
		
		final ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDisplayGroup,"Payer",60, myListener.getAllCheckedMAndats());
		col0.setEditable(true);
		col0.setResizable(false);
        col0.setMyModifier(myListener.checkPayerModifier());
		col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher
        
		final ZEOTableModelColumnDico col0b = new ZEOTableModelColumnDico(myDisplayGroup,"Différer",60, myListener.getAllDifferedMandats());
        col0b.setEditable(true);
        col0b.setResizable(false);
        col0b.setMyModifier(myListener.checkDifferedModifier());
        col0b.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher
        
        final ZEOTableModelColumnDico colManAttenteObjet = new ZEOTableModelColumnDico(myDisplayGroup,"Raison",100, myListener.getAllAttenteRaisonMandats());
        colManAttenteObjet.setAlignment(SwingConstants.LEFT);
        colManAttenteObjet.setAlignment(SwingConstants.LEFT);
        colManAttenteObjet.setEditable(true);
        colManAttenteObjet.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));
        
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_NUMERO_KEY,"N°",65);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);
        

		
		ZEOTableModelColumn colExer = new ZEOTableModelColumn(myDisplayGroup,"exercice.exeExercice","exercice",65);
        colExer.setAlignment(SwingConstants.CENTER);
        colExer.setColumnClass(Integer.class);
        
		ZEOTableModelColumn col12 = new ZEOTableModelColumn(myDisplayGroup,"gestion.gesCode","Code gestion",65);
		col12.setAlignment(SwingConstants.CENTER);
		
		ZEOTableModelColumn colBord = new ZEOTableModelColumn(myDisplayGroup,"bordereau.borNum","Bordereau",65);
		colBord.setAlignment(SwingConstants.CENTER);
		
		ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup,"planComptable.pcoNum","Imp.",65);
		col11.setAlignment(SwingConstants.LEFT);  
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"fournisseur.nomAndPrenom","Fournisseur",180);
//		ZEOTableModelColumn col31 = new ZEOTableModelColumn(myDisplayGroup,"manNbPiece","Pièces",48);
//		col31.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup,EOMandat.MAN_HT_KEY,"Montant HT",80);		
		col32.setAlignment(SwingConstants.RIGHT);
		col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col32.setColumnClass(BigDecimal.class);
		
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_TVA_KEY,"Montant TVA",80);		
		col3.setAlignment(SwingConstants.RIGHT);
		col3.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER ) ;
		col3.setColumnClass(BigDecimal.class);
		
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_TTC_KEY,"Montant TTC",80);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay( ZConst.FORMAT_DISPLAY_NUMBER) ;
		col4.setColumnClass(BigDecimal.class);
		
		ZEOTableModelColumn colModePaiement = new ZEOTableModelColumn(myDisplayGroup,"modePaiement.modLibelle","Mode de paiement",48);
		colModePaiement.setAlignment(SwingConstants.CENTER);
		
		ZEOTableModelColumn colDateReception = new ZEOTableModelColumn(myDisplayGroup, EOMandat.MAN_DATE_REMISE_KEY,"Date réception",60);
		colDateReception.setAlignment(SwingConstants.CENTER);
		colDateReception.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colDateReception.setColumnClass(Date.class);
		
		
//		col5.setFormat(new NSTimestampFormatter("%d/%m/%y") ) ;
		//On crée la collection des colonnes, ca défini l'ordre initial des colonnes. 			
		myCols.add(col0);
		myCols.add(col0b);
		myCols.add(colManAttenteObjet);
		myCols.add(colExer);
		myCols.add(col12);
		myCols.add(colBord);
		myCols.add(col1);
		myCols.add(col11);
		myCols.add(col2);
		myCols.add(colModePaiement);
		myCols.add(col4);
		


		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter (myTableModel);
        
        myTableModel.setMyDelegate(myListener.getMandatModelProvider());
        
        
		
//		myTableModel.addTableModelListener(this);
	}
			
	    
    
    
	private final void initPopupMenu() {
		myPopupMenu = new JPopupMenu();
		myPopupMenu.add(new ActionCheckAll());
		myPopupMenu.add(new ActionUncheckAll());
		myEOTable.setPopup(myPopupMenu);
	}	
    
    
    
    

    public interface IPaiementPanelMandatSelectListener {
        /**
         * @return Les mandats à afficher
         */
        public NSArray getAllMandats();
        
        public IMandatSelectFilterPanelListener getMandatSelectFilterPanelListener();

		public Map getAllAttenteRaisonMandats();

        public IZEOTableModelDelegate getMandatModelProvider();

        public Modifier checkDifferedModifier();

        public Modifier checkPayerModifier();

        /**
         * @return Les mandats préselectionnés.
         */
        public HashMap getAllCheckedMAndats();
        
        /** @return les mandats a differer */
        public HashMap getAllDifferedMandats();

    }







    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        return;
        
    }





    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        return;
        
    }





    
    
	private final class ActionCheckAll extends AbstractAction {
		public ActionCheckAll() {
			super("Tout cocher pour \"Payer\"");
		}
		public void actionPerformed(ActionEvent e) {
			((ZEOTableModelColumnDico)myCols.get(0)).setValueForAllRows(Boolean.TRUE);
			myTableModel.fireTableDataChanged();
		}
	}
	
	private final class ActionUncheckAll extends AbstractAction {
		public ActionUncheckAll() {
			super("Tout décocher pour \"Payer\"");
		}
		public void actionPerformed(ActionEvent e) {
			((ZEOTableModelColumnDico)myCols.get(0)).setValueForAllRows(Boolean.FALSE);
			myTableModel.fireTableDataChanged();
		}
	}






    public final EODisplayGroup getMyDisplayGroup() {
        return myDisplayGroup;
    }



    public final ZEOTable getMyEOTable() {
        return myEOTable;
    }



    public final ZEOTableModel getMyTableModel() {
        return myTableModel;
    }    
    
    
    
    
    private class MandatSelectFilterPanel extends ZUIContainer {
        
//        private ZFormPanel codeGestion;  
        private JComboBox typeBordereaux;
        protected IMandatSelectFilterPanelListener myListener;


        public MandatSelectFilterPanel(IMandatSelectFilterPanelListener listener) {
            super();
            setLayout(new BorderLayout());
            myListener = listener;
            typeBordereaux = new JComboBox(listener.getTypeBordereauxMdl());
            typeBordereaux.addActionListener(new ActionListener(){

				public void actionPerformed(ActionEvent actionevent) {
					myListener.getFilters().put(EOTypeBordereau.ENTITY_NAME, myListener.getTypeBordereauxMdl().getSelectedEObject());
					myListener.actionSrch().actionPerformed(actionevent);
					
				}});
            
            add(ZUiUtil.buildBoxLine(new Component[]{typeBordereaux}));
        }


        public void updateData() throws Exception {
        	
        	
        }
        
        /**
         * @see org.cocktail.maracuja.client.ZUIContainer#initGUI()
         */
        public void initGUI() {
            final ArrayList comps = new ArrayList();
            comps.add(typeBordereaux);
            add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
//            add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
            super.initGUI();
        }
        
        

        }    
        
            
        public interface IMandatSelectFilterPanelListener {
            /**
             * @return Les valeurs de chaque élément de filtre 
             */
            public HashMap getFilters();
            public ZEOComboBoxModel getTypeBordereauxMdl();
			public Action actionSrch();
        }
        
        
}
