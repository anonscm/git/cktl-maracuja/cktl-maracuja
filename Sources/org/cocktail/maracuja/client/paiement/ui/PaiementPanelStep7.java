/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.common.sepa.OptionsRegroupementLigneFichierVirement;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

/**
 * Panel affichant le choix du type de virement.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep7 extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Virement";
	private static final String COMMENTAIRE = "Veuillez sélectionner le format de virement à générer ainsi que la date de valeur .";

	private final Step7Listener myListener;

	private ZDatePickerField dateValeurField;
	private ZNumberField montantPaiementField;

	private JCheckBox grouperLignesParRibField;
	private JCheckBox grouperLignesParNoFactureField;
	private JCheckBox unEcritureDetailCompte5Field;
	private JComboBox formatsVirement;

	/**
	 * @param listener
	 */
	public PaiementPanelStep7(Step7Listener listener) {
		super(listener);
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		final Box col1 = Box.createVerticalBox();
		col1.add(buildInfosPaiementPanel());
		col1.add(new ZLabeledComponent("Format du virement ", formatsVirement, ZLabeledComponent.LABELONLEFT, -1));

		final JPanel panel2 = new JPanel(new BorderLayout());
		panel2.add(new JPanel(new BorderLayout()), BorderLayout.WEST);
		panel2.add(new JPanel(new BorderLayout()), BorderLayout.EAST);
		panel2.add(col1, BorderLayout.CENTER);

		p.add(panel2, BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return p;
	}

	private final JPanel buildInfosPaiementPanel() {
		ZFormPanel panelInfosPaiement = new ZFormPanel();
		JLabel l = new JLabel("Montant du paiement");
		panelInfosPaiement.add(l);
		panelInfosPaiement.add(montantPaiementField);

		ZFormPanel panelOptionDate = new ZFormPanel();
		panelOptionDate.add(new JLabel("Date de valeur"));
		panelOptionDate.add(dateValeurField);

		final JButton bCalendrier = ZKarukeraPanel.getButtonFromAction(myListener.actionAfficheCalendrierBdf());
		bCalendrier.setHorizontalAlignment(SwingConstants.CENTER);
		panelOptionDate.add(bCalendrier);

		ZFormPanel panelOptionFichier = new ZFormPanel();
		panelOptionFichier.add(grouperLignesParRibField);
		panelOptionFichier.add(grouperLignesParNoFactureField);

		ZFormPanel panelOptionEcriture = new ZFormPanel();
		panelOptionEcriture.add(unEcritureDetailCompte5Field);
		return ZKarukeraPanel.buildVerticalPanelOfComponent(new Component[] {
				panelInfosPaiement, panelOptionFichier, panelOptionEcriture, panelOptionDate
		});
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {

		formatsVirement = new JComboBox(myListener.getFormatsVirementModel());

		dateValeurField = new ZDatePickerField(myListener.dateValeurModel(), ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateValeurField.getMyTexfield().setColumns(8);

		montantPaiementField = new ZNumberField(new MontantProvider(), null, ZConst.FORMAT_DECIMAL);
		montantPaiementField.setUIReadOnly();
		montantPaiementField.getMyTexfield().setEditable(false);
		montantPaiementField.getMyTexfield().setColumns(10);

		grouperLignesParRibField = new JCheckBox("Fusionner les lignes du fichier par rib");
		grouperLignesParRibField.setToolTipText("Si vous cochez cette case, toutes les dépenses avec le même rib seront regroupées sur une seule ligne");
		grouperLignesParNoFactureField = new JCheckBox("Fusionner les lignes du fichier par numéro de facture");
		grouperLignesParNoFactureField.setToolTipText("Si vous cochez cette case, toutes les dépenses avec le même rib et le même n° de facture seront regroupées sur une seule ligne");

		grouperLignesParNoFactureField.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				if (grouperLignesParNoFactureField.isSelected()) {
					grouperLignesParRibField.setEnabled(false);
				}
				else {
					grouperLignesParRibField.setEnabled(true);
				}

			}
		});

		unEcritureDetailCompte5Field = new JCheckBox("Créer une seule ligne d'écriture par compte de paiement");
		unEcritureDetailCompte5Field.setEnabled(myListener.unEcritureDetailCompte5FieldEnabled());
		unEcritureDetailCompte5Field.setSelected(true);

		super.initGUI();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		montantPaiementField.updateData();
		if (!myListener.isGrouperLignesParNoFactureVisible()) {
			grouperLignesParNoFactureField.setSelected(false);
			grouperLignesParNoFactureField.setEnabled(false);
		}
		else {
			grouperLignesParNoFactureField.setEnabled(true);
		}
	}

	public final boolean grouperLignesParRib() {
		return grouperLignesParRibField.isSelected();
	}

	public final boolean grouperLignesParNoFacture() {
		return grouperLignesParNoFactureField.isSelected();
	}

	public final boolean unEcritureDetailCompte5() {
		return (unEcritureDetailCompte5Field.isEnabled() && unEcritureDetailCompte5Field.isSelected());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface Step7Listener extends ZStepListener {

		public IZDatePickerFieldModel dateValeurModel();

		public boolean isGrouperLignesParNoFactureVisible();

		public boolean unEcritureDetailCompte5FieldEnabled();

		public BigDecimal montantPaiement();

		public ZEOComboBoxModel getFormatsVirementModel();

		public boolean isDateEchangeValide();

		public Action actionAfficheCalendrierBdf();
	}

	private final class MontantProvider implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.montantPaiement();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
		}

	}

	public ZDatePickerField getDateValeurField() {
		return dateValeurField;
	}

	public Integer getOptionRegroupementLignes() {
		if (grouperLignesParNoFacture()) {
			return OptionsRegroupementLigneFichierVirement.OPTION_REGROUPEMENT_LIGNES_NO_FACTURE;
		}
		if (grouperLignesParRib()) {
			return OptionsRegroupementLigneFichierVirement.OPTION_REGROUPEMENT_LIGNES_RIB;
		}
		return OptionsRegroupementLigneFichierVirement.OPTION_REGROUPEMENT_LIGNES_SANS;
	}
}
