/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.math.BigDecimal;
import java.util.Date;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementSrchListPanel extends ZTablePanel {

    /**
     * @param editingContext
     */
    public PaiementSrchListPanel(IZTablePanelMdl listener) {
        super(listener);
        
        final ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.PAI_NUMERO_KEY, "N°", 40);
        col5.setAlignment(SwingConstants.LEFT);
        col5.setColumnClass(Integer.class);

        final ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.TYPE_VIREMENT_KEY + "." +EOTypeVirement.TVI_LIBELLE_KEY, "Type de paiement", 95);
        col6.setAlignment(SwingConstants.LEFT);

        final ZEOTableModelColumn colUtilisateur = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.UTILISATEUR_KEY + "." + EOUtilisateur.UTL_PRENOM_NOM_KEY , "Utilisateur", 110);
        colUtilisateur.setAlignment(SwingConstants.LEFT);

        final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.PAI_MONTANT_KEY, "Montant", 90);
        col4.setAlignment(SwingConstants.RIGHT);
        col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col4.setColumnClass(BigDecimal.class);
        
        final ZEOTableModelColumn colRetenues = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.MONTANT_RETENUES_KEY , "Retenues", 90);
        colRetenues.setAlignment(SwingConstants.RIGHT);
        colRetenues.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        colRetenues.setColumnClass(BigDecimal.class);
        
        final ZEOTableModelColumn colPaiementDateSaisie = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.PAI_DATE_CREATION_KEY,"Date",60);
        colPaiementDateSaisie.setAlignment(SwingConstants.CENTER);
        colPaiementDateSaisie.setFormatDisplay(ZConst.FORMAT_DATESHORT);
        colPaiementDateSaisie.setColumnClass(Date.class);
                

        final ZEOTableModelColumn colNb = new ZEOTableModelColumn(myDisplayGroup, EOPaiement.PAI_NB_VIREMENTS_KEY, "Nb lignes", 60);
        colNb.setAlignment(SwingConstants.CENTER);
        colNb.setFormatDisplay(ZConst.FORMAT_ENTIER);
        colNb.setColumnClass(Integer.class);
        
        
        colsMap.clear();
//      colsMap.put(EXE_EXERCICE_KEY ,exeExercice);
      colsMap.put("col5" ,col5);
      colsMap.put("colPaiementDateSaisie" ,colPaiementDateSaisie);
//      colsMap.put("col0" ,col0);
      colsMap.put("col6" ,col6);
      colsMap.put("colNb" ,colNb);
      colsMap.put("col4" ,col4);        
      //Affichage des retenues trop lent
//      colsMap.put("colRetenues" ,colRetenues);        
      colsMap.put("colUtilisateur" ,colUtilisateur);        
    }

}

