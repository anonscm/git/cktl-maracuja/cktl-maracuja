/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.KTree;
import org.cocktail.maracuja.client.common.ui.KTreeCellRenderer;
import org.cocktail.maracuja.client.common.ui.KTreeModel;
import org.cocktail.maracuja.client.common.ui.KTreeNode;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.foundation.NSArray;


/**
 * Panel recapitulatif des paiements
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep6 extends ZKarukeraStepPanel2 {
    private static final String TITLE="Récapitulatif";
    private static final String COMMENTAIRE="Vérifiez les élements qui vont être pris en compte dans le paiement.";

    private Step6Listener myListener;


	private KTreeNode rootNode;
	private KTreeModel myTreeModel;
	private KTree myTree;
    private ZPanelNbTotal panelTotal1;
    private ZPanelNbTotal panelTotalRetenues;
    private final TotalModelRetenues totalModelRetenues;

    /**
     * @param listener
     */
    public PaiementPanelStep6(Step6Listener listener) {
        super(listener);
        myListener = listener;
        totalModelRetenues = new TotalModelRetenues();
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
     */
    public JPanel getCenterPanel() {
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        p.add(new JScrollPane(myTree) , BorderLayout.CENTER);
        p.add(buildPanelTotaux(), BorderLayout.SOUTH);
        return p;
    }




    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
     */
    public void initGUI() {
        initTree();

        super.initGUI();

    }



	private JPanel buildPanelTotaux() {
	    JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		panelTotal1 = new ZPanelNbTotal("Total à payer ");
		panelTotal1.setTotalProvider(new TotalModel1());
		panelTotal1.setNbProvider(new NbModel1());
        
		panelTotalRetenues = new ZPanelNbTotal(" + retenues ");
		panelTotalRetenues.setTotalProvider(totalModelRetenues);
		panelTotalRetenues.setNbProvider(new NbModelRetenues());

		mainPanel.add(panelTotal1,BorderLayout.WEST);
		mainPanel.add(panelTotalRetenues,BorderLayout.EAST);
		return mainPanel;
	}




	private void initTree() {
		myTree = new KTree();
		myTree.enableToolTips(false);
		myTree.setCellRenderer(new RecapTreeRenderer()  );
		myTree.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
	}

	private void updateTreeModel() {
		myTreeModel = new KTreeModel(rootNode);
		myTree.setModel(myTreeModel);
//		myTree.expandAllObjectsAtLevel(0,true);
		myTree.expandAllObjectsAtLevel(1,true);
	}


	/**
	 * Construit l'arborescence des noeuds.
	 */
	private void updateRootNode() {
		KTreeNode tmpNode = new KTreeNode("Paiement à générer", null);

		if (myListener.getMandats()!=null && myListener.getMandats().count()>0) {
		    tmpNode.addChild(buildNodeForMandats(myListener.getMandats()));
		}

//		if (myListener.getTitres()!=null && myListener.getTitres().count()>0) {
//		    tmpNode.addChild(buildNodeForTitres(myListener.getTitres()));
//		}

		if (myListener.getOdps()!=null && myListener.getOdps().count()>0) {
		    tmpNode.addChild(buildNodeForOrdreDePaiement(myListener.getOdps()));
		}

		rootNode = tmpNode;
	}





	private final KTreeNode buildNodeForMandats(NSArray mandats) {
        KTreeNode node = new KTreeNode("Mandats",null);
        for (int i = 0; i < mandats.count(); i++) {
            EOMandat element = (EOMandat) mandats.objectAtIndex(i);
            node.addChild(buildNodeForMandat(element));
        }
		return node;
	}


	private final KTreeNode buildNodeForMandat(EOMandat mandat) {
		KTreeNode node = new KTreeNode(""+mandat.manNumero() + mandat.fournisseur().adrNom() +" " ,mandat);
		for (int i = 0; i < mandat.depenses().count(); i++) {
            EODepense element = (EODepense) mandat.depenses().objectAtIndex(i);
            node.addChild(buildNodeForDepense(element));
        }
		return node;
	}

	private final KTreeNode buildNodeForDepense(EODepense depense) {
	    KTreeNode node = new KTreeNode(depense.depNumero() ,depense);
		return node;
	}


//	private final KTreeNode buildNodeForTitres(NSArray titres) {
//        KTreeNode node = new KTreeNode("Titres (OP Ordonnateurs)",null);
//        for (int i = 0; i < titres.count(); i++) {
//            EOTitre element = (EOTitre) titres.objectAtIndex(i);
//            node.addChild(buildNodeForTitre(element));
//        }
//		return node;
//	}
//
//
//	private final KTreeNode buildNodeForTitre(EOTitre titre) {
//		KTreeNode node = new KTreeNode(titre.titNumero().toString() ,titre);
//		for (int i = 0; i < titre.recettes().count(); i++) {
//            EORecette element = (EORecette) titre.recettes().objectAtIndex(i);
//            node.addChild(buildNodeForRecette(element));
//        }
//		return node;
//	}

//	private final KTreeNode buildNodeForRecette(EORecette recette) {
//	    KTreeNode node = new KTreeNode(recette.recNum().toString() ,recette);
//		return node;
//	}

	private final KTreeNode buildNodeForOrdreDePaiement(NSArray odps) {
        KTreeNode node = new KTreeNode("Ordres de paiement",null);
        for (int i = 0; i < odps.count(); i++) {
            EOOrdreDePaiement element = (EOOrdreDePaiement) odps.objectAtIndex(i);
            node.addChild(buildNodeForOrdreDePaiement(element));
        }
		return node;
	}


	private final KTreeNode buildNodeForOrdreDePaiement(EOOrdreDePaiement odp) {
		KTreeNode node = new KTreeNode(odp.odpNumero() .toString() ,odp);
		return node;
	}



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        updateRootNode();
        updateTreeModel();
        panelTotal1.updateData();
        panelTotalRetenues.updateData();
        
        if (((Number)totalModelRetenues.getValue()).doubleValue()>0) {
            panelTotalRetenues.setBackgroundColor(ZConst.BGCOL_DESEQUILIBRE);
        }
        else {
            panelTotalRetenues.setBackgroundColor(Color.WHITE);
        }
    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
     */
    public String getTitle() {
        return TITLE;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
     */
    public String getCommentaire() {
        return COMMENTAIRE;
    }

    public interface Step6Listener extends ZStepListener {

        /**
         * @return
         */
        public NSArray getMandats();
//        public NSArray getTitres();
        public NSArray getOdps();

    }


    /**
	 * Composant pour l'affichage du recapitulatif du paiement.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	class RecapTreeRenderer extends KTreeCellRenderer {
		Icon localIcon;
		Color bgMontant = Color.decode("#EEEEEE");


		public RecapTreeRenderer() {
			super();
			setBackgroundSelectionColor(new Color(0, 0, 128));
			setBorderSelectionColor(Color.black);
			setTextSelectionColor(Color.white);
			setTextNonSelectionColor(Color.black);
		}

		public RecapTreeRenderer(Icon icon) {
			super();
			localIcon = icon;
		}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			superComponent = super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row,hasFocus);
			if (value instanceof KTreeNode) {
				if (((KTreeNode)value).getMyObject() instanceof EOMandat) {
					EOMandat mandat = ((EOMandat) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForMandat(tree, mandat, sel, expanded, leaf, row, hasFocus);
					return res;
				}
				else if (((KTreeNode)value).getMyObject() instanceof EODepense) {
				    EODepense dep = ((EODepense) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForDepense(tree, dep, sel, expanded, leaf, row, hasFocus);
					return res;
				}
				else if (((KTreeNode)value).getMyObject() instanceof EOTitre) {
				    EOTitre dep = ((EOTitre) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForTitre(tree, dep, sel, expanded, leaf, row, hasFocus);
					return res;
				}
				else if (((KTreeNode)value).getMyObject() instanceof EORecette) {
				    EORecette dep = ((EORecette) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForRecette(tree, dep, sel, expanded, leaf, row, hasFocus);
					return res;
				}
				else if (((KTreeNode)value).getMyObject() instanceof EOOrdreDePaiement) {
				    EOOrdreDePaiement dep = ((EOOrdreDePaiement) ((KTreeNode)value).getMyObject());
					Component res = getTreeCellRendererComponentForOrdreDePaiement(tree, dep, sel, expanded, leaf, row, hasFocus);
					return res;
				}
			}

			//Par defaut
			super.getTreeCellRendererComponent(tree, value, sel,expanded, leaf, row,hasFocus);
			return this;
		}


		private Component getTreeCellRendererComponentForMandat(JTree tree, EOMandat mandat, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(""+mandat.manNumero(),10, ZLEFT, sel,null));
			tmpBox.add(buildTextField(mandat.fournisseur().adrNom() ,60, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format(mandat.manTtc()) ,10 ,ZRIGHT,sel, bgMontant));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}

		private Component getTreeCellRendererComponentForDepense(JTree tree, EODepense depense, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(depense.depNumero() ,30, ZLEFT, sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DATESHORT.format(depense.depDateService())  ,10, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format( depense.depMontantDisquette()  ) ,10 ,ZRIGHT,sel, null));
			tmpBox.add(buildTextField( "(- " + ZConst.FORMAT_DISPLAY_NUMBER.format( depense.montantRetenues())+")" ,12 ,ZRIGHT,sel, null));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}


		private Component getTreeCellRendererComponentForTitre(JTree tree, EOTitre titre, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(""+titre.titNumero(),5, ZLEFT, sel,null));
			tmpBox.add(buildTextField(titre.titOrigineLib() ,28, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format(titre.titTtc()) ,10 ,ZRIGHT,sel, bgMontant));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}

		private Component getTreeCellRendererComponentForRecette(JTree tree, EORecette depense, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(depense.recNum().toString() ,10, ZLEFT, sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DATESHORT.format(depense.recDate() )  ,60, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format( depense.recMontTtc()  ) ,10 ,ZRIGHT,sel, null));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}

		private Component getTreeCellRendererComponentForOrdreDePaiement(JTree tree, EOOrdreDePaiement odp, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
			Box tmpBox = Box.createHorizontalBox();
			tmpBox.add(buildTextField(""+odp.odpNumero(),10, ZLEFT, sel,null));
			tmpBox.add(buildTextField(odp.odpLibelle()  ,60, ZLEFT,sel,null));
			tmpBox.add(buildTextField( ZConst.FORMAT_DISPLAY_NUMBER.format(odp.odpTtc()) ,10 ,ZRIGHT,sel, bgMontant));
			tmpBox.add(Box.createHorizontalGlue());
			return tmpBox;
		}


	}



	private final class TotalModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
		    BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(myListener.getMandats(), EOMandat.MONTANT_A_PAYER_KEY)  ;
//		    total = total.add(ZEOUtilities.calcSommeOfBigDecimals(myListener.getTitres(), "titTtc"));
		    total = total.add(ZEOUtilities.calcSommeOfBigDecimals(myListener.getOdps(), EOOrdreDePaiement.ODP_MONTANT_PAIEMENT_KEY));
			return  total;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModel1 implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
		    int i=0;
		    for (int j = 0; j < myListener.getMandats().count(); j++) {
                EOMandat element = (EOMandat) myListener.getMandats().objectAtIndex(j);
                i=i+element.depenses().count();
            }

//		    for (int j = 0; j < myListener.getTitres().count(); j++) {
//                EOTitre element = (EOTitre) myListener.getTitres().objectAtIndex(j);
//                i=i+element.recettes().count();
//            }
			return  new Integer(i + myListener.getOdps().count()   );
		}

		public void setValue(Object value) {
			return;
		}
	}
    
	private final class NbModelRetenues implements ZLabelTextField.IZLabelTextFieldModel {
	    public Object getValue() {
	        int i=0;
	        for (int j = 0; j < myListener.getMandats().count(); j++) {
	            EOMandat element = (EOMandat) myListener.getMandats().objectAtIndex(j);
                for (int k = 0; k < element.depenses().count(); k++) {
                    i=i+((EODepense) element.depenses().objectAtIndex(k)).retenues().count() ;
                }
	        }
	        return  new Integer(i);
	    }
	    
	    public void setValue(Object value) {
	        return;
	    }
	}
    
	private final class TotalModelRetenues implements ZLabelTextField.IZLabelTextFieldModel {
	    public Object getValue() {
	        BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(myListener.getMandats(), EOMandat.MONTANT_RETENUES_KEY)  ;
	        return  total;
	    }
	    
	    public void setValue(Object value) {
	        return;
	    }
	}
	
	
	
}
