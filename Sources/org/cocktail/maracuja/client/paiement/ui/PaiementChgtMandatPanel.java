/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.MandatFilterPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;


/**
 * Panel qui gere les modes modes de paiement et ribs sur les mandats.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementChgtMandatPanel extends ZKarukeraPanel {

	private PaiementChgtFormPanel paiementChgtFormPanel;
	private MyPaiementPanelMandatOk paiementPanelMandatOk;
	private MandatFilterPanel mandatFilterPanel;

	private IPaiementChgtMandatPanelListener myListener;

	/**
     * 
     */
	public PaiementChgtMandatPanel(IPaiementChgtMandatPanelListener listener) {
		super();
		myListener = listener;
		paiementPanelMandatOk = new MyPaiementPanelMandatOk(new PaiementPanelMandatOkListener());
		paiementChgtFormPanel = new PaiementChgtFormPanel(new PaiementChgtFormPanelListener());
		mandatFilterPanel = new MandatFilterPanel(new MandatFilterPanelListener());
	}

	private final JPanel buildTopPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		p.add(mandatFilterPanel, BorderLayout.CENTER);
		p.add(new JButton(myListener.actionSrch()), BorderLayout.EAST);
		return p;
	}

	public void initGUI() {
		mandatFilterPanel.initGUI();
		paiementPanelMandatOk.initGUI();
		paiementChgtFormPanel.initGUI();

		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		add(ZKarukeraPanel.encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, buildTopPanel(), null, null), BorderLayout.NORTH);
		add(ZKarukeraPanel.encloseInPanelWithTitle("Liste des mandats", null, ZConst.BG_COLOR_TITLE, paiementPanelMandatOk, null, null), BorderLayout.CENTER);
		add(ZKarukeraPanel.encloseInPanelWithTitle("Mode de paiement et Rib", null, ZConst.BG_COLOR_TITLE, paiementChgtFormPanel, null, null), BorderLayout.SOUTH);
		//        super.initGUI();
	}

	public EOMandat getSelectedMandat() {
		return (EOMandat) paiementPanelMandatOk.selectedObject();
	}

	public void updateData() throws Exception {
		//        super.updateData();

		mandatFilterPanel.updateData();
		paiementPanelMandatOk.updateData();
		paiementChgtFormPanel.updateData();

	}

	public interface IPaiementChgtMandatPanelListener {
		public NSArray getMandats();

		public void onMandatSelectionChanged();

		/**
		 * @return
		 */
		public HashMap getFilters();

		public Action actionSrch();

		/**
		 * @return
		 */
		public Object getModePaiement();

		/**
		 * @return
		 */
		public DefaultComboBoxModel getRibModel();

		//        /**
		//         * @return
		//         */
		//        public NSArray getModePaiements();
		/**
		 * @return
		 */
		public EORib getRib();

		//        /**
		//         * @param selectedEObject
		//         */
		//        public void setModePaiement(NSKeyValueCoding selectedEObject);
		/**
         * 
         */
		public void selectedRibChanged();

		/**
		 * @return
		 */
		public Action actionAnnuler();

		/**
		 * @return
		 */
		public Action actionEnregistrer();

		/**
		 * @return
		 */
		public DefaultComboBoxModel getModePaiementModel();

		/**
         * 
         */
		public void selectedModPaiementChanged();
	}

	private final class PaiementPanelMandatOkListener implements ZTablePanel.IZTablePanelMdl {

		public NSArray getData() {
			return myListener.getMandats();
		}

		public void onDbClick() {
			return;
		}

		public void selectionChanged() {
			myListener.onMandatSelectionChanged();
		}

	}

	private final class PaiementChgtFormPanelListener implements PaiementChgtFormPanel.IPaiementChgtFormPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#getModePaiement()
		 */
		public Object getModePaiement() {
			return myListener.getModePaiement();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#getRibModel()
		 */
		public DefaultComboBoxModel getRibModel() {
			return myListener.getRibModel();
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#getModePaiements()
		//         */
		//        public NSArray getModePaiements() {
		//            return myListener.getModePaiements();
		//        }

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#getRib()
		 */
		public EORib getRib() {
			return myListener.getRib();
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#setModePaiement(com.webobjects.foundation.NSKeyValueCoding)
		//         */
		//        public void setModePaiement(NSKeyValueCoding selectedEObject) {
		//            myListener.setModePaiement(selectedEObject);
		//        }

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#selectedRibChanged()
		 */
		public void selectedRibChanged() {
			myListener.selectedRibChanged();

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#actionCtrePartieAnnuler()
		 */
		public Action actionAnnuler() {
			return myListener.actionAnnuler();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#actionCtrePartieEnregistrer()
		 */
		public Action actionEnregistrer() {
			return myListener.actionEnregistrer();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#getModePaiementModel()
		 */
		public DefaultComboBoxModel getModePaiementModel() {
			return myListener.getModePaiementModel();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementChgtFormPanel.IPaiementChgtFormPanelListener#selectedModPaiementChanged()
		 */
		public void selectedModPaiementChanged() {
			myListener.selectedModPaiementChanged();
		}

	}

	private final class MandatFilterPanelListener implements MandatFilterPanel.IMandatFilterPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.MandatFilterPanel.IMandatFilterPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myListener.getFilters();
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return null;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return null;
		}

		public Action actionSrch() {
			return myListener.actionSrch();
		}

	}

	public PaiementChgtFormPanel getPaiementChgtFormPanel() {
		return paiementChgtFormPanel;
	}

	public PaiementPanelMandatOk getPaiementPanelMandatOk() {
		return paiementPanelMandatOk;
	}

	public class MyPaiementPanelMandatOk extends PaiementPanelMandatOk {

		public MyPaiementPanelMandatOk(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			colsMap.put(COL_MAN_ETAT, colManEtat);
		}
	}

}
