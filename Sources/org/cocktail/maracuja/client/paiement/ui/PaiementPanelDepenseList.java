/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelDepenseList extends ZTablePanel {
	private IPaiementPanelDepenseListener myListener;
	private ColRibProvider colRibProvider = new ColRibProvider();

	public PaiementPanelDepenseList(IPaiementPanelDepenseListener listener) {
		super(listener);
		myListener = listener;

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_NUMERO_KEY, "N°", 70);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);

		//      ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,"depDateReception","Date réception",100);
		//      col2.setFormatDisplay(ZConst.FORMAT_DATESHORT) ;
		ZEOTableModelColumn colRib = new ZEOTableModelColumnWithProvider("Rib", colRibProvider, 200);
		colRib.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_HT_KEY, "Montant HT", 80);
		col3.setAlignment(SwingConstants.RIGHT);
		col3.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col3.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_TVA_KEY, "Montant TVA", 80);
		col4.setAlignment(SwingConstants.RIGHT);
		col4.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col4.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_TTC_KEY, "Montant TTC", 80);
		col5.setAlignment(SwingConstants.RIGHT);
		col5.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		col5.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colRetenue = new ZEOTableModelColumn(myDisplayGroup, EODepense.MONTANT_RETENUES_KEY, "Retenues", 80);
		colRetenue.setAlignment(SwingConstants.RIGHT);
		colRetenue.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colRetenue.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colAPayer = new ZEOTableModelColumn(myDisplayGroup, EODepense.DEP_MONTANT_DISQUETTE_KEY, "A payer", 90);
		colAPayer.setAlignment(SwingConstants.RIGHT);
		colAPayer.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colAPayer.setColumnClass(BigDecimal.class);

		colsMap.clear();
		colsMap.put("col1", col1);
		colsMap.put("colRib", colRib);
		colsMap.put("col3", col3);
		colsMap.put("col4", col4);
		colsMap.put("col5", col5);
		colsMap.put("colRetenue", colRetenue);
		colsMap.put("colAPayer", colAPayer);

	}

	public interface IPaiementPanelDepenseListener extends IZTablePanelMdl {
		/**
		 * @return Le type virement qui va déterminer quel type de rib va être afficher (classique ou SEPA)
		 */
		public EOTypeVirement selectedTypeVirement();
	}

	private class ColRibProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {
		public Object getValueAtRow(int row) {
			EODepense el = (EODepense) myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (myListener.selectedTypeVirement() != null) {
				if (EOTypeVirement.TVI_FORMAT_BDF.equals(myListener.selectedTypeVirement().tviFormat())) {
					return el.rib().getRibCompletFR();
				}
				return (el.rib() != null ? el.rib().getRibCompletIntl() : "");
			}
			return "";
		}
	}

}
