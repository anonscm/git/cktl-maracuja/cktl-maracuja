/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;


/**
 * Fin d'un paiement caisse.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep10 extends ZKarukeraStepPanel2 {
    private Step10Listener myListener;
    private ZDatePickerField dateValeurField;

    
    /**
     * @param listener
     */
    public PaiementPanelStep10(Step10Listener listener) {
        super(listener);
        myListener = listener;
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
     */
    public JPanel getCenterPanel() {
        JPanel t = new JPanel();
      t = buildButtonsPanelForCaisse();
//      t = buildButtonsPanelForCheque();
//      t = buildButtonsPanelForVirement();
  
	  JPanel p = new JPanel(new BorderLayout());
	  p.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
	  p.add(t, BorderLayout.CENTER);
  
	  return p;

    }
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
     */
    public void initGUI() {
        super.initGUI();
        
    }
    
    
    private final JPanel buildButtonsPanelForCaisse() {
        Action[] list = new Action[2];
        list[0]  = myListener.actionImprimerContenuPaiement();
        list[1]  = myListener.actionImprRetenues();
        JPanel tmp = new JPanel(new BorderLayout());
        
        
        tmp.setBorder(BorderFactory.createEmptyBorder(65,150,15,150));
        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        return centerInPanel(tmp);   
        
        
    }
    
    
    private JPanel centerInPanel(Component c) {
        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        box.add(c);
        box.add(Box.createHorizontalGlue());
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(box, BorderLayout.NORTH);
        p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return p;        
    }
    
        

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
    }

    

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
     */
    public String getTitle() {
        return myListener.getTile();
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
     */
    public String getCommentaire() {
        return myListener.getCommentaire();
    }    
    
    public interface Step10Listener extends ZStepListener {
        public String getTile();
        public String getCommentaire();
        public Action actionImprimerContenuPaiement();
        public Action actionImprRetenues();

        
    }

    
 

}
