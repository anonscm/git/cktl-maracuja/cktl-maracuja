/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;

import javax.swing.Action;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelRetenue extends ZTablePanel {
//
//	private ZEOTable myEOTable;
//	private ZEOTableModel myTableModel;
//	private EODisplayGroup myDisplayGroup;
//	private TableSorter myTableSorter;
//	private Vector myCols;
//	
    private JToolBar myToolBar;
	private IPaiementPanelRetenueListener myListener;
    
	/**
     * 
     */
    public PaiementPanelRetenue(IPaiementPanelRetenueListener listener) {
        super(listener);
        myListener = listener;
        
        

        ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup,EORetenue.RET_LIBELLE_KEY,"Libellé de la retenue",300);
        col11.setAlignment(SwingConstants.LEFT);  

        ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup,EORetenue.RET_MONTANT_KEY,"Montant",80);     
        col32.setAlignment(SwingConstants.RIGHT);
        col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col32.setColumnClass(BigDecimal.class);
        
        ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup,EORetenue.UTILISATEUR_KEY+"."+EOUtilisateur.UTL_PRENOM_NOM_KEY ,"Saisie par",80);     
        col4.setAlignment(SwingConstants.LEFT);
        
        
        colsMap.clear();
        colsMap.put("col11" ,col11);
        colsMap.put("colRib" ,col32);
        colsMap.put("col4" ,col4);
        
  
        
    }
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        super.initGUI();
        
        myToolBar = new JToolBar();
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);
        
        //Initialiser les boutons à partir des actions 
        myToolBar.add(myListener.actionRetenueAdd());
        myToolBar.add(myListener.actionRetenueDelete());        
        
		this.add(myToolBar, BorderLayout.NORTH);        
    }

    
    
//    
//    
//    /**
//     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
//     */
//    public void updateData() throws Exception {
//        myDisplayGroup.setObjectArray(myListener.getRetenues());
//        myEOTable.updateData();
//    }
//    
//    
//    
//    
//    
//    
//
//	/**
//	 * Initialise la table à afficher (le modele doit exister)
//	 */
//	private void initTable() {
//		myEOTable = new ZEOTable(myTableSorter);
//		
//		myEOTable.addListener(this);
//		myTableSorter.setTableHeader(myEOTable.getTableHeader());
//		
////		initPopupMenu();
//	}
//		
//	/**
//	 * Initialise le modeele le la table à afficher.
//	 *
//	 */
//	private final void initTableModel() {
//		myCols = new Vector(9,1);
//		
//		
//		ZEOTableModelColumn col11 = new ZEOTableModelColumn(myDisplayGroup,EORetenue.RET_LIBELLE_KEY,"Libellé de la retenue",300);
//		col11.setAlignment(SwingConstants.LEFT);  
//
//		ZEOTableModelColumn col32 = new ZEOTableModelColumn(myDisplayGroup,EORetenue.RET_MONTANT_KEY,"Montant",80);		
//		col32.setAlignment(SwingConstants.RIGHT);
//		col32.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
//		col32.setColumnClass(BigDecimal.class);
//		
//		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup,EORetenue.UTILISATEUR_KEY+"."+EOUtilisateur.UTL_PRENOM_NOM_KEY ,"Saisie par",80);		
//        col4.setAlignment(SwingConstants.LEFT);
//		
//		myCols.add(col11);
//		myCols.add(col32);
//		myCols.add(col4);
//		
//		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
//		myTableSorter = new TableSorter (myTableModel);
//		
////		myTableModel.addTableModelListener(this);
//	}
//			
//	    
    
    

    public interface IPaiementPanelRetenueListener extends ZTablePanel.IZTablePanelMdl {
        public Action actionRetenueAdd();
        public Action actionRetenueDelete();
    }



    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        return;
    }



//
//
//    /**
//     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
//     */
//    public void onSelectionChanged() {
//        myListener.onSelectionChanged();
//    }
//
//
//    public EORetenue getSelectedObject() {
//        return (EORetenue) myTableModel.getSelectedObject();
//    }

    
    
}
