/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;

import com.webobjects.foundation.NSArray;

/**
 * Etape correspondant à l'affectation des retenues sur les Ordres de paiement à payer.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep5 extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Les Ordres de paiement sélectionnés";
	private static final String COMMENTAIRE = "Liste des Ordre de paiement qui vont être payés.";

	private Step5Listener myListener;
	private PaiementPanelOdpOk paiementPanelOdpOk;

	//    private PaiementPanelRetenue paiementPanelRetenueDetail;

	/**
	 * @param listener
	 */
	public PaiementPanelStep5(Step5Listener listener) {
		super(listener);
		myListener = listener;
		paiementPanelOdpOk = new PaiementPanelOdpOk(new PaiementPanelOdpOkListener());
		//        paiementPanelRetenueDetail = new PaiementPanelRetenue(new PaiementPanelRetenueDetailListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des ordres de paiement sélectionnés", null, ZConst.BG_COLOR_TITLE, paiementPanelOdpOk, null, null);
		//        JPanel p2 = ZKarukeraPanel.encloseInPanelWithTitle("Les détails des retenues par ordre de paiement",null,ZConst.BG_COLOR_TITLE,paiementPanelRetenueDetail,null, null);

		JPanel p = new JPanel(new BorderLayout());
		//        p.add(ZKarukeraPanel.buildVerticalSplitPane(p1,p2) , BorderLayout.CENTER);
		p.add(p1, BorderLayout.CENTER);
		p.add(buildRightPanel(), BorderLayout.EAST);
		return p;
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionRetenueNew());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		paiementPanelOdpOk.initGUI();
		//        paiementPanelRetenueDetail.initGUI();
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		paiementPanelOdpOk.updateData();
		//        paiementPanelRetenueDetail.updateData();
	}

	public EOOrdreDePaiement getSelectedOdp() {
		return paiementPanelOdpOk.getSelectedObject();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface Step5Listener extends ZStepListener {
		public NSArray getOdps();

		public Action actionRetenueNew();

		/**
		 * @return Les retenues pour le odp selectionné.
		 */
		public NSArray getRetenues();

		public EOTypeVirement selectedTypeVirement();
	}

	private final class PaiementPanelOdpOkListener implements PaiementPanelOdpOk.IPaiementPanelOdpOkListener {

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelOdpOk.IPaiementPanelOdpOkListener#getOdps()
		 */
		public NSArray getOdps() {
			return myListener.getOdps();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelOdpOk.IPaiementPanelOdpOkListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			try {
				//                paiementPanelRetenueDetail.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		public EOTypeVirement selectedTypeVirement() {
			return myListener.selectedTypeVirement();
		}

	}

	//    private final class PaiementPanelRetenueDetailListener implements PaiementPanelRetenue.IPaiementPanelRetenueListener {
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelRetenue.IPaiementPanelRetenueListener#getRetenues()
	//         */
	//        public NSArray getRetenues() {
	//            return myListener.getRetenues();
	//        }
	//        
	//    }
}
