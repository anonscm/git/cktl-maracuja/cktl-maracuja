/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.common.ui.ZPanelNbTotal;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelMandatSelect.IMandatSelectFilterPanelListener;
import org.cocktail.zutil.client.ZListUtil;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep2 extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Sélection des éléments à partir desquels le paiement va être généré";
	private static final String COMMENTAIRE = "Veuillez cochez dans les listes ci-dessous les mandats et ordres de paiements à partir desquels générer le paiement.";

	private PaiementPanelMandatSelect paiementPanelMandatSelect;
	private PaiementPanelOdpSelect paiementPanelOdpSelect;

	private Step2Listener myListener;
	private ZPanelNbTotal panelTotalAPayer;
	private ZPanelNbTotal panelTotalDifferes;

	//private final TotalModelDifferes totalModelDifferes;

	/**
	 * @param listener
	 */
	public PaiementPanelStep2(Step2Listener listener) {
		super(listener);
		myListener = listener;
		//totalModelDifferes = new TotalModelDifferes();
		paiementPanelMandatSelect = new PaiementPanelMandatSelect(new PaiementPanelMandatSelectListener());
		paiementPanelOdpSelect = new PaiementPanelOdpSelect(new PaiementPanelOdpSelectListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des mandats restant à payer", null, ZConst.BG_COLOR_TITLE, paiementPanelMandatSelect, null, null);
		JPanel p3 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des Ordres de paiement restant à payer", null, ZConst.BG_COLOR_TITLE, paiementPanelOdpSelect, null, null);

		JPanel p = new JPanel(new BorderLayout());

		JSplitPane split2 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, p1, p3);
		split2.setDividerLocation((double) 0.75);
		split2.setResizeWeight((double) 0.75);
		split2.setBorder(BorderFactory.createEmptyBorder());

		p.add(split2, BorderLayout.CENTER);
		p.add(buildPanelTotaux(), BorderLayout.SOUTH);
		return p;
	}

	private JPanel buildPanelTotaux() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		panelTotalAPayer = new ZPanelNbTotal("Total à payer ");
		panelTotalAPayer.setTotalProvider(new TotalModelMandatAPayer());
		panelTotalAPayer.setNbProvider(new NbModelMandatAPayer());

		panelTotalDifferes = new ZPanelNbTotal("Différés ");
		panelTotalDifferes.setTotalProvider(new TotalModelDifferes());
		panelTotalDifferes.setNbProvider(new NbModelDifferes());

		mainPanel.add(panelTotalAPayer, BorderLayout.WEST);
		mainPanel.add(panelTotalDifferes, BorderLayout.EAST);
		return mainPanel;
	}

	private final class TotalModelMandatAPayer implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(mandatsAPayer(), EOMandat.MAN_TTC_KEY);
			total = total.add(ZEOUtilities.calcSommeOfBigDecimals(odpsAPayer(), EOOrdreDePaiement.ODP_MONTANT_PAIEMENT_KEY));
			return total;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModelMandatAPayer implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return new Integer(mandatsAPayer().count() + odpsAPayer().count());
		}

		public void setValue(Object value) {
			return;
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		paiementPanelMandatSelect.initGUI();
		paiementPanelOdpSelect.initGUI();
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		paiementPanelMandatSelect.updateData();
		paiementPanelOdpSelect.updateData();
		updateTotaux();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface Step2Listener extends ZStepListener {
		public NSArray getAllMandats();

		public HashMap getCheckedMandats();

		public NSArray getAllTitres();

		public HashMap getCheckedTitres();

		public NSArray getAllOdps();

		public HashMap getCheckedOdps();

		public HashMap getAllDifferedMandats();

		public Modifier checkDifferedModifier();

		public Modifier checkPayerModifier();

		public IZEOTableModelDelegate getMandatModelProvider();

		public Map getAllAttenteRaisonMandats();

		public IMandatSelectFilterPanelListener getMandatSelectFilterPanelListener();

		public Modifier checkOdpPayerModifier();
	}

	private final class PaiementPanelMandatSelectListener implements PaiementPanelMandatSelect.IPaiementPanelMandatSelectListener {

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelMandatSelect.IPaiementPanelMandatSelectListener#getAllMandats()
		 */
		public NSArray getAllMandats() {
			return myListener.getAllMandats();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelMandatSelect.IPaiementPanelMandatSelectListener#getAllCheckedMAndats()
		 */
		public HashMap getAllCheckedMAndats() {
			return myListener.getCheckedMandats();
		}

		public HashMap getAllDifferedMandats() {
			return myListener.getAllDifferedMandats();
		}

		public Modifier checkDifferedModifier() {
			return myListener.checkDifferedModifier();
		}

		public Modifier checkPayerModifier() {
			return myListener.checkPayerModifier();
		}

		public IZEOTableModelDelegate getMandatModelProvider() {
			return myListener.getMandatModelProvider();
		}

		public Map getAllAttenteRaisonMandats() {
			return myListener.getAllAttenteRaisonMandats();
		}

		public IMandatSelectFilterPanelListener getMandatSelectFilterPanelListener() {
			return myListener.getMandatSelectFilterPanelListener();
		}

	}

	private final class PaiementPanelOdpSelectListener implements PaiementPanelOdpSelect.IPaiementPanelOdpSelectListener {

		public NSArray getAllOdps() {
			return myListener.getAllOdps();
		}

		public HashMap getCheckedOdps() {
			return myListener.getCheckedOdps();
		}

		public Modifier checkPayerModifier() {
			return myListener.checkOdpPayerModifier();
		}

	}

	public PaiementPanelMandatSelect getPaiementPanelMandatSelect() {
		return paiementPanelMandatSelect;
	}

	public PaiementPanelOdpSelect getPaiementPanelOdpSelect() {
		return paiementPanelOdpSelect;
	}

	/**
	 * @return Les mandats sélectionnés par l'utilisateur.
	 */
	private final NSArray mandatsAPayer() {
		final NSArray tmp = new NSArray(ZListUtil.getKeyListForValue(myListener.getCheckedMandats(), Boolean.TRUE).toArray());
		return tmp;
	}

	private final NSArray mandatsADifferer() {
		final NSArray tmp = new NSArray(ZListUtil.getKeyListForValue(myListener.getAllDifferedMandats(), Boolean.TRUE).toArray());
		return tmp;
	}

	/**
	 * @return Les odps sélectionnés par l'utilisateur.
	 */
	private final NSArray odpsAPayer() {
		return new NSArray(ZListUtil.getKeyListForValue(myListener.getCheckedOdps(), Boolean.TRUE).toArray());
	}

	private final class TotalModelDifferes implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(mandatsADifferer(), EOMandat.MAN_TTC_KEY);
			return total;
		}

		public void setValue(Object value) {
			return;
		}
	}

	private final class NbModelDifferes implements ZLabelTextField.IZLabelTextFieldModel {
		public Object getValue() {
			return Integer.valueOf(mandatsADifferer().count());
		}

		public void setValue(Object value) {
			return;
		}
	}

	public void updateTotaux() {
		panelTotalAPayer.updateData();
		panelTotalDifferes.updateData();

	}
}
