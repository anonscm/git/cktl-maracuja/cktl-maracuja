/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.zutil.client.ui.ZRadioButton;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSArray;

/**
 * Panel qui affiche la sélection du type de paiement (Virement, chèquen caisse)
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep1 extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Type de paiement";
	private static final String COMMENTAIRE = "Veuillez sélectionner le type de paiement que vous souhaitez générer.";

	private Step1Listener myListener;
	//private ZRadioButtonPanel domainesRadios;
	private HashMap domainesObjects;
	private JComboBox myComptabiliteBox;
	private JComboBox myTypeVirementsBox;

	private ZRadioButton btVirements;
	private ZRadioButton btCheques;
	private ZRadioButton btCaisse;
	private ButtonGroup btGroup = new ButtonGroup();
	private ActionListener btSelectListener = new BtSelectListener();

	/**
	 * @param listener
	 */
	public PaiementPanelStep1(Step1Listener listener) {
		super(listener);
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		return buildContentPanel();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		//domainesRadios.setselectedObject(myListener.getSelectedDomaine());

		String svirements = Step1Listener.MANDATS + " : " + ((HashMap) myListener.getNumOfObjByDomaine().get(EOModePaiement.MODDOM_VIREMENT)).get(Step1Listener.MANDATS);
		svirements = svirements + ", " + Step1Listener.ODPS + " : " + ((HashMap) myListener.getNumOfObjByDomaine().get(EOModePaiement.MODDOM_VIREMENT)).get(Step1Listener.ODPS);
		//((ZRadioButton.ZRadioButtonModel) domainesRadios.getButtonByObject(EOModePaiement.MODDOM_VIREMENT).getModel()).setLibelle("Virements (" + svirements + ")");
		((ZRadioButton.ZRadioButtonModel) btVirements.getModel()).setLibelle("Virements (" + svirements + ")");

		String scaisses = Step1Listener.MANDATS + " : " + ((HashMap) myListener.getNumOfObjByDomaine().get(EOModePaiement.MODDOM_CAISSE)).get(Step1Listener.MANDATS);
		scaisses = scaisses + ", " + Step1Listener.ODPS + " : " + ((HashMap) myListener.getNumOfObjByDomaine().get(EOModePaiement.MODDOM_CAISSE)).get(Step1Listener.ODPS);
		//((ZRadioButton.ZRadioButtonModel) domainesRadios.getButtonByObject(EOModePaiement.MODDOM_CAISSE).getModel()).setLibelle("Caisse (" + scaisses + ")");
		((ZRadioButton.ZRadioButtonModel) btCaisse.getModel()).setLibelle("Caisse (" + scaisses + ")");

		String scheques = Step1Listener.MANDATS + " : " + ((HashMap) myListener.getNumOfObjByDomaine().get(EOModePaiement.MODDOM_CHEQUE)).get(Step1Listener.MANDATS);
		scheques = scheques + ", " + Step1Listener.ODPS + " : " + ((HashMap) myListener.getNumOfObjByDomaine().get(EOModePaiement.MODDOM_CHEQUE)).get(Step1Listener.ODPS);
		//((ZRadioButton.ZRadioButtonModel) domainesRadios.getButtonByObject(EOModePaiement.MODDOM_CHEQUE).getModel()).setLibelle("Chèques (" + scheques + ")");
		((ZRadioButton.ZRadioButtonModel) btCheques.getModel()).setLibelle("Chèques (" + svirements + ")");

		//domainesRadios.updateData();

		btVirements.setSelected(EOModePaiement.MODDOM_VIREMENT.equals(myListener.getSelectedDomaine()));
		btCaisse.setSelected(EOModePaiement.MODDOM_CAISSE.equals(myListener.getSelectedDomaine()));
		btCheques.setSelected(EOModePaiement.MODDOM_CHEQUE.equals(myListener.getSelectedDomaine()));

		btVirements.updateData();
		btCaisse.updateData();
		btCaisse.updateData();
		refreshButtons();

	}

	public interface Step1Listener extends ZStepListener {
		public static final String MANDATS = "Mandats";
		public static final String TITRES = "Titres";
		public static final String ODPS = "Ordres de paiement";

		/** Doit renvoyer le domaine à preselectionner */
		public String getSelectedDomaine();

		/**
		 * Doit renvoyer un dictionnaire avec en clé les domaines et en valeurs une autre dictionnaire avec en clés l'identifiant de l'objet (MANDATS,
		 * TITRES,ODPS - utiliser les constantes de cette interface) et en valeur le nombre d'objets restant à payer
		 */
		public HashMap getNumOfObjByDomaine();

		/** Doit renvoyer un dictionnaire avec en clé les domaines et en valeurs le montant restant à payer */
		public HashMap getMontantByDomaine();

		/**
		 * @return
		 */
		public NSArray getcomptabilites();

		/**
		 * @return
		 */
		public ZEOComboBoxModel getcomptabiliteModel();

		public ZEOComboBoxModel getTypeVirementsModel();

		public void onTypeVirementSelected();

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		//domainesRadios = buildRadiosDomainePanel();
		super.initGUI();

	}

	private JPanel buildContentPanel() {
		btVirements = new ZRadioButton(EOModePaiement.MODDOM_VIREMENT, "Virement");
		btCheques = new ZRadioButton(EOModePaiement.MODDOM_CHEQUE, "Chèques");
		btCaisse = new ZRadioButton(EOModePaiement.MODDOM_CAISSE, "Caisse");
		myTypeVirementsBox = new JComboBox(myListener.getTypeVirementsModel());

		myTypeVirementsBox.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				myListener.onTypeVirementSelected();

			}
		});

		btGroup.add(btVirements);
		btGroup.add(btCheques);
		btGroup.add(btCaisse);

		btVirements.addActionListener(btSelectListener);
		btCheques.addActionListener(btSelectListener);
		btCaisse.addActionListener(btSelectListener);

		btVirements.setSelected(true);

		final ArrayList list = new ArrayList();
		list.add(new Component[] {
				btVirements
		});
		list.add(new Component[] {
				myTypeVirementsBox, new JPanel(new BorderLayout())
		});
		list.add(new Component[] {
				btCheques
		});
		list.add(new Component[] {
				btCaisse
		});

		//	box3.add(ZUiUtil.buildBoxColumn(list));

		Box box5 = Box.createHorizontalBox();
		box5.add(Box.createHorizontalStrut(100));
		box5.add(ZUiUtil.buildForm(list), (list));
		box5.add(Box.createHorizontalGlue());

		//box3.setBorder(BorderFactory.createTitledBorder("Mode de paiement"));

		Box box1 = Box.createVerticalBox();
		box1.add(Box.createVerticalStrut(60));
		box1.add(buildComptabilitePanel());
		box1.add(Box.createVerticalStrut(60));

		//		Box box = Box.createVerticalBox();
		//		box.add(Box.createVerticalGlue());
		//		box.add(domainesRadios);
		//		box.add(Box.createVerticalGlue());

		JPanel p = new JPanel(new BorderLayout());
		p.add(box1, BorderLayout.NORTH);
		p.add(box5, BorderLayout.CENTER);

		JPanel p1 = new JPanel(new BorderLayout());
		p1.add(p, BorderLayout.NORTH);
		p1.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		//	p.add(box, BorderLayout.SOUTH);

		return p;
	}

	private void refreshButtons() {
		myTypeVirementsBox.setEnabled(btVirements.isSelected());
	}

	public String getSelectedDomaine() {
		if (btGroup.getSelection() == null) {
			return null;
		}
		return (String) ((ZRadioButton.ZRadioButtonModel) btGroup.getSelection()).getAssociatedObject();
		//return (String) domainesRadios.getSelectedObject();
	}

	public EOTypeVirement getSelectedTypeVirement() {
		if (!EOTypeVirement.MOD_DOM_VIREMENT.equals(getSelectedDomaine())) {
			return null;
		}
		return (EOTypeVirement) myListener.getTypeVirementsModel().getSelectedEObject();
		//return (String) domainesRadios.getSelectedObject();
	}

	public final EOComptabilite getSelectedComptabilite() {
		return (EOComptabilite) myListener.getcomptabiliteModel().getSelectedEObject();
	}

	//	private final ZRadioButtonPanel buildRadiosDomainePanel() {
	//		domainesObjects = new HashMap(2, 1);
	//		domainesObjects.put(EOModePaiement.MODDOM_VIREMENT, "Virement : ");
	//		domainesObjects.put(EOModePaiement.MODDOM_CHEQUE, "Chèque");
	//		domainesObjects.put(EOModePaiement.MODDOM_CAISSE, "Caisse");
	//
	//		return new ZRadioButtonPanel(domainesObjects, null, domainesObjects.values().toArray()[0], SwingConstants.VERTICAL);
	//	}

	private final JPanel buildComptabilitePanel() {
		JPanel p = new JPanel(new BorderLayout());
		try {

			myComptabiliteBox = new JComboBox(myListener.getcomptabiliteModel());
			p.add(new ZLabeledComponent("Comptabilité", myComptabiliteBox, ZLabeledComponent.LABELONLEFT, 150));
		} catch (Exception e) {
			showErrorDialog(e);
		}
		return p;

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	private class BtSelectListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			refreshButtons();
		}

	}

}
