/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;

import java.awt.BorderLayout;

import javax.swing.Action;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelDepenseList.IPaiementPanelDepenseListener;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

import com.webobjects.foundation.NSArray;

/**
 * Etape correspondant à l'affectation des retenues sur les mandats à payer.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementPanelStep3 extends ZKarukeraStepPanel2 {
	private static final String TITLE = "Les mandats sélectionnés";
	private static final String COMMENTAIRE = "Liste des mandats qui vont être payés.";

	private Step3Listener myListener;
	private PaiementPanelMandatOk paiementPanelMandatOk;
	private PaiementPanelDepenseList paiementPanelDepenseList;
	private PaiementPanelRetenue paiementPanelRetenueDetail;

	/**
	 * @param listener
	 */
	public PaiementPanelStep3(Step3Listener listener) {
		super(listener);
		myListener = listener;
		paiementPanelMandatOk = new PaiementPanelMandatOk(new PaiementPanelMandatOkListener());
		paiementPanelDepenseList = new PaiementPanelDepenseList(new PaiementPanelDepenseListener());
		paiementPanelRetenueDetail = new PaiementPanelRetenue(new PaiementPanelRetenueDetailListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des mandats sélectionnés", null, ZConst.BG_COLOR_TITLE, paiementPanelMandatOk, null, null);
		JPanel p2 = ZKarukeraPanel.encloseInPanelWithTitle("Liste des dépenses pour le mandat sélectionné", null, ZConst.BG_COLOR_TITLE, paiementPanelDepenseList, null, null);
		JPanel p3 = ZKarukeraPanel.encloseInPanelWithTitle("Les détails des retenues pour la dépense sélectionnée", null, ZConst.BG_COLOR_TITLE, paiementPanelRetenueDetail, null, null);

		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraPanel.buildVerticalSplitPane(ZKarukeraPanel.buildVerticalSplitPane(p1, p2), p3), BorderLayout.CENTER);
		//        p.add(buildRightPanel(), BorderLayout.EAST);
		return p;
	}

	//    private final JPanel buildRightPanel() {
	//        JPanel tmp = new JPanel(new BorderLayout());
	//        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
	//        ArrayList list = new ArrayList();
	//        list.add(myListener.actionRetenueNew());
	//        
	//        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
	//        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	//        return tmp;
	//    }

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		paiementPanelMandatOk.initGUI();
		paiementPanelRetenueDetail.initGUI();
		paiementPanelDepenseList.initGUI();
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		updateDataMandat();
		updateDataDepense();
		updateDataRetenue();
	}

	public void updateDataMandat() throws Exception {
		paiementPanelMandatOk.updateData();
	}

	public void updateDataDepense() throws Exception {
		paiementPanelDepenseList.updateData();
	}

	public void updateDataRetenue() throws Exception {
		paiementPanelRetenueDetail.updateData();
	}

	public EOMandat getSelectedMandat() {
		return (EOMandat) paiementPanelMandatOk.selectedObject();
	}

	public EODepense getSelectedDepense() {
		return (EODepense) paiementPanelDepenseList.selectedObject();
	}

	public EORetenue getSelectedRetenue() {
		return (EORetenue) paiementPanelRetenueDetail.selectedObject();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return COMMENTAIRE;
	}

	public interface Step3Listener extends ZStepListener {
		public NSArray getMandats();

		/**
		 * @return Les retenues pour le mandat selectionné.
		 */
		public NSArray getRetenues();

		/**
		 * @return
		 */
		public NSArray getDepenses();

		public Action actionRetenueAdd();

		public Action actionRetenueDelete();

		public void onMandatSelectionChanged();

		public void onDepenseSelectionChanged();

		public void onRetenueSelectionChanged();

		public EOTypeVirement selectedTypeVirement();
	}

	private final class PaiementPanelMandatOkListener implements ZTablePanel.IZTablePanelMdl {

		public NSArray getData() {
			return myListener.getMandats();
		}

		public void selectionChanged() {
			myListener.onMandatSelectionChanged();
		}

		public void onDbClick() {
			return;
		}

	}

	private final class PaiementPanelDepenseListener implements IPaiementPanelDepenseListener {

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelMandatOk.IPaiementPanelMandatOkListener#onSelectionChanged()
		 */
		public void selectionChanged() {
			myListener.onDepenseSelectionChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelDepenseList.IPaiementPanelDepenseListListener#getDepenses()
		 */
		public NSArray getData() {
			return myListener.getDepenses();
		}

		public void onDbClick() {
			return;
		}

		public EOTypeVirement selectedTypeVirement() {
			return myListener.selectedTypeVirement();
		}

	}

	private final class PaiementPanelRetenueDetailListener implements PaiementPanelRetenue.IPaiementPanelRetenueListener {

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelRetenue.IPaiementPanelRetenueListener#getRetenues()
		 */
		public NSArray getData() {
			return myListener.getRetenues();
		}

		public Action actionRetenueAdd() {
			return myListener.actionRetenueAdd();
		}

		public Action actionRetenueDelete() {
			return myListener.actionRetenueDelete();
		}

		public void selectionChanged() {
			myListener.onRetenueSelectionChanged();

		}

		public void onDbClick() {

		}

	}

	public void fireSelectedMandatUpdated() {
		paiementPanelMandatOk.fireSeletedTableRowUpdated();
	}

	public void fireSelectedDepenseUpdated() {
		paiementPanelDepenseList.fireSeletedTableRowUpdated();

	}

}
