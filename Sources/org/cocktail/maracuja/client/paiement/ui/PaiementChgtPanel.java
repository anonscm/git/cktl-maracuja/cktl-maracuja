/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement.ui;


import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.cocktail.maracuja.client.ZUIContainer;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;


/**
 * Panel pour changer les modes de paiements et ribs.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementChgtPanel extends ZUIContainer {

    private PaiementChgtMandatPanel paiementChgtMandatPanel;
    private PaiementChgtTitrePanel paiementChgtTitrePanel;
    private IPaiementChgtPanelListener myListener;
    
    
    /**
     * 
     */
    public PaiementChgtPanel(IPaiementChgtPanelListener listener) {
        super();
        myListener = listener;
        
        paiementChgtMandatPanel = new PaiementChgtMandatPanel(myListener.mandatPanelListener());
        registerSubComp(paiementChgtMandatPanel);
        
        setLayout(new BorderLayout());
        add(buildTabbedPane(), BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);        
    }

    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionClose());        
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
//    public void initGUI() {
//        setLayout(new BorderLayout());
//        add(buildTabbedPane(), BorderLayout.CENTER);
//        add(buildBottomPanel(), BorderLayout.SOUTH);
//        super.initGUI();
//    }
    
    
    private final JTabbedPane buildTabbedPane() {
        JTabbedPane onglets = new JTabbedPane(); 
        onglets.addTab ("Mandats", paiementChgtMandatPanel) ;
        return onglets;
    }
    

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
       super.updateData();

    }
    
    public interface IPaiementChgtPanelListener {
        public PaiementChgtMandatPanel.IPaiementChgtMandatPanelListener mandatPanelListener();
        public Action actionClose();
    }

    
    
    
    
    public PaiementChgtMandatPanel getPaiementChgtMandatPanel() {
        return paiementChgtMandatPanel;
    }
}
