/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.awt.CardLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileFilter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CalendrierBdfCtrl;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactorySauverVirement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPaiement;
import org.cocktail.maracuja.client.factory.process.FactoryProcessRetenue;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVirementFichiers;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.metier.EOVirementParamBdf;
import org.cocktail.maracuja.client.metier.EOVirementParamSepa;
import org.cocktail.maracuja.client.metier.EOVirementParamVint;
import org.cocktail.maracuja.client.metier.IVirementParam;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelMandatSelect.IMandatSelectFilterPanelListener;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep1;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep10;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep3;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep5;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep6;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8;
import org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep9;
import org.cocktail.maracuja.client.remotecall.ServerCallsepa;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZListUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementCtrl extends CommonCtrl {
	public static final boolean RETENUE_ACTIVE = true;
	public static final String ACTIONID = ZActionCtrl.IDU_PAGE;

	/** Choix de la comptabilite + domaine de paiement */
	private static final String STEP1 = "step1";
	/** Selection des elements à payer */
	private static final String STEP2 = "step2";
	/** les mandats */
	private static final String STEP3 = "step3";
	/** Les titres */
	private static final String STEP4 = "step4";
	/** Les ODPs */
	private static final String STEP5 = "step5";
	/** L'arbre recapitulatif */
	private static final String STEP6 = "step6";
	/** Le choix du format de virement */
	private static final String STEP7 = "step7";
	/** Recap apres enregistrement VIREMENT */
	private static final String STEP8 = "step8";
	/** Choix avant paiement cheque ou caisse */
	private static final String STEP9 = "step9";
	/** recap apres enregistrement cheque ou caisse */
	private static final String STEP10 = "step10";

	private static final String TITLE = "Génération d'un paiement";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);

	private final PaiementPanelStep1 step1Panel;
	private final PaiementPanelStep2 step2Panel;
	private final PaiementPanelStep3 step3Panel;
	private final PaiementPanelStep5 step5Panel;
	private final PaiementPanelStep6 step6Panel;
	private final PaiementPanelStep7 step7Panel;
	private final PaiementPanelStep8 step8Panel;
	private final PaiementPanelStep9 step9Panel;
	private final PaiementPanelStep10 step10Panel;

	private final PaiementPanelStep1Listener step1Listener;
	private final PaiementPanelStep2Listener step2Listener;
	private final PaiementPanelStep3Listener step3Listener;
	private final PaiementPanelStep5Listener step5Listener;
	private final PaiementPanelStep6Listener step6Listener;
	private final PaiementPanelStep7Listener step7Listener;
	private final PaiementPanelStep8Listener step8Listener;
	private final PaiementPanelStep9Listener step9Listener;
	private final PaiementPanelStep10Listener step10Listener;

	private JPanel cardPanel;
	private CardLayout cardLayout;
	private ZKarukeraStepPanel2 currentPanel;

	private final DefaultActionClose actionClose;
	private final ZEOComboBoxModel myComptabiliteModel;
	/** Les types virements du domaine VIREMENT */
	private final ZEOComboBoxModel typeVirementsModel;

	private final HashMap stepPanels = new HashMap();

	private final EOQualifier qualForGestion;

	private File defaultFile;

	/**
	 * Les comptabilites autorisees.
	 */
	private final NSArray comptabilites;
	private EOPaiement currentPaiement;
	private String leContenuFichierVirement;
	private Date dateValeur;
	private int nbOperations = -1;

	/** jours ouvres de la BDF */
	//	private final NSArray calendrierBDF;
	private final CalendrierBdfCtrl calendrierBDF;

	//    private File 

	protected Exception lastException = null;
	protected String msgFin = null;

	private final Map comptesBE = new HashMap();

	ZWaitingPanelDialog waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
	private NSArray typeVirements;

	/**
	 * @throws Exception
	 */
	public PaiementCtrl(EOEditingContext ec) throws Exception {
		super(ec);
		revertChanges();

		actionClose = new DefaultActionClose();
		actionClose.setEnabled(true);

		comptabilites = myApp.appUserInfo().getAuthorizedComptabilitesForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTIONID);
		if (comptabilites.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour cette opération. Demandez à votre administrateur de vous affecter les codes de gestions pour la gestion des paiements.");
		}
		myComptabiliteModel = new ZEOComboBoxModel(comptabilites, "comLibelle", null, null);
		qualForGestion = myApp.appUserInfo().getQualifierForAutorizedGestionForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTIONID, "gestion");

		checkModesPaiements();

		//Recupérer les types de virements qui ont des parametres
		typeVirements = EOTypeVirement.fetchAll(getEditingContext(), new EOAndQualifier(new NSArray(new Object[] {
				EOTypeVirement.QUAL_VALIDE, EOTypeVirement.QUAL_DOM_VIREMENT, EOTypeVirement.QUAL_HAS_VIREMENTPARAMS
		})), null);
		typeVirementsModel = new ZEOComboBoxModel(typeVirements, EOTypeVirement.TVI_LIBELLE_KEY, null, null);

		//Initialiser les comptes de BE
		final NSArray pcos = EOPlanComptableExerFinder.getPlancoExerValidesWithCond(getEditingContext(), getExercice().getPrevEOExercice(), "pcoCompteBe<>nil", null, false);
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptableExer element = (EOPlanComptableExer) pcos.objectAtIndex(i);
			if (element.pcoCompteBe() != null) {
				final EOPlanComptableExer plancoBE = EOPlanComptableExerFinder.getPlancoExerForPcoNum(getEditingContext(), getExercice(), element.pcoCompteBe(), false);
				if (plancoBE == null) {
					throw new DefaultClientException("Erreur dans le plan comptable : Le compte de BE " + element.pcoCompteBe() + " (défini sur l'exercice " + getExercice().getPrevEOExercice().exeExercice() + ") pour le compte " + element.pcoNum() + " n'est pas valide pour l'exercice "
							+ getExercice().exeExercice() + ". Veuillez corriger cette erreur avant de faire un paiement.");
				}
				comptesBE.put(element.planComptable(), plancoBE.planComptable());
			}
		}

		ZLogger.debug(comptesBE);
		calendrierBDF = new CalendrierBdfCtrl(getEditingContext());
		calendrierBDF.loadDatesAroundNow();

		if (calendrierBDF.count() < 15) {
			showInfoDialog("Le calendrier de la Banque de France ne semble pas être complet. Il n'y aura peut-etre pas de contrôle possible pour les dates d'échange et de réglement.");
		}
		// creer les listeners
		step1Listener = new PaiementPanelStep1Listener();
		step2Listener = new PaiementPanelStep2Listener();
		step3Listener = new PaiementPanelStep3Listener();
		step5Listener = new PaiementPanelStep5Listener();
		step6Listener = new PaiementPanelStep6Listener();
		step7Listener = new PaiementPanelStep7Listener();
		step8Listener = new PaiementPanelStep8Listener();
		step9Listener = new PaiementPanelStep9Listener();
		step10Listener = new PaiementPanelStep10Listener();

		step1Panel = new PaiementPanelStep1(step1Listener);
		step2Panel = new PaiementPanelStep2(step2Listener);
		step3Panel = new PaiementPanelStep3(step3Listener);
		step5Panel = new PaiementPanelStep5(step5Listener);
		step6Panel = new PaiementPanelStep6(step6Listener);
		step7Panel = new PaiementPanelStep7(step7Listener);
		step8Panel = new PaiementPanelStep8(step8Listener);
		step9Panel = new PaiementPanelStep9(step9Listener);
		step10Panel = new PaiementPanelStep10(step10Listener);

		stepPanels.put(STEP1, step1Panel);
		stepPanels.put(STEP2, step2Panel);
		stepPanels.put(STEP3, step3Panel);
		stepPanels.put(STEP5, step5Panel);
		stepPanels.put(STEP6, step6Panel);
		stepPanels.put(STEP7, step7Panel);
		stepPanels.put(STEP8, step8Panel);
		stepPanels.put(STEP9, step9Panel);
		stepPanels.put(STEP10, step10Panel);

		final NSMutableArray formatsVirements = new NSMutableArray();
		if (getSelectedTypeVirement() != null) {
			formatsVirements.addObjectsFromArray(getSelectedTypeVirement().toVirementParamBdfs(new EOKeyValueQualifier(EOVirementParamBdf.VPB_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOVirementParamBdf.ETAT_VALIDE)));
			formatsVirements.addObjectsFromArray(getSelectedTypeVirement().toVirementParamSepas(new EOKeyValueQualifier(EOVirementParamSepa.VPS_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOVirementParamSepa.ETAT_VALIDE)));
		}
		step7Listener.formatsVirementModel.updateListWithData(formatsVirements);

	}

	private void initGUI() {

		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Iterator iter = stepPanels.keySet().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			ZKarukeraStepPanel2 panel = (ZKarukeraStepPanel2) stepPanels.get(element);
			panel.setMyDialog(getMyDialog());
			panel.initGUI();
			cardPanel.add(panel, element);
		}

		//  activer format etebac et vint quand ils seront prêts
		//        step7Panel.enableFormat(Step7Listener.FORMAT_ETEBAC, false);
		//        step7Panel.enableFormat(Step7Listener.FORMAT_VINT, false);

		// Preselections
		// activer format etebac et vint quand ils seront prêts
		//        step7Panel.preselectFormat(Step7Listener.FORMAT_BDF);

		changeStep(STEP1);
	}

	/**
	 * Ferme la fenetre.
	 */
	private final void close() {
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}

	/**
	 * Vérifier si les modes de paiement sont bien paramétrés.
	 * 
	 * @throws Exception
	 */
	private final void checkModesPaiements() throws Exception {
		EOModePaiement.checkAllModePaiementsValides(getEditingContext(), myApp.appUserInfo().getCurrentExercice(), null);
		//		NSArray res = EOsFinder.getModePaiementsValide(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
		//		res = EOQualifier.filteredArrayWithQualifier(res, EOQualifier.qualifierWithQualifierFormat("modDom=%@ or modDom=%@ or modDom=%@", new NSArray(new Object[] {
		//				EOModePaiement.MODDOM_CAISSE, EOModePaiement.MODDOM_CHEQUE, EOModePaiement.MODDOM_VIREMENT
		//		})));
		//
		//		for (int i = 0; i < res.count(); i++) {
		//			EOModePaiement element = (EOModePaiement) res.objectAtIndex(i);
		//			if (element.planComptablePaiement() == null) {
		//				throw new DefaultClientException("Le mode de paiement " + element.modCode() + " (" + element.modLibelle() + ")" + " n'a pas d'imputation paiement associée, il sera impossible de générer les écritures de paiement, veuillez corriger ce problème avant d'accéder aux paiements.");
		//			}
		//		}

	}

	public void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	private final void payer() {

		try {
			if (dateValeur == null) {
				throw new DataCheckException("Vous devez indiquer une date de valeur.");
			}
			if (!calendrierBDF.isDateDansCalendrier(step7Listener.getDateValeur())) {
				Date nextDate = calendrierBDF.getFirstNextDate(step7Listener.getDateValeur());
				if (nextDate != null) {
					int choix = showConfirmationCancelDialog("Confirmation", "La date de valeur que vous avez indiqué (" + ZConst.FORMAT_DATESHORT.format(step7Listener.getDateValeur()) + ") n'est pas une date ouvrable de la banque de France, souhaitez-vous la remplacer par la date "
							+ ZConst.FORMAT_DATESHORT.format(nextDate) + " ?" + "<br><br>Cliquez sur Non pour générer le fichier à la la date du " + ZConst.FORMAT_DATESHORT.format(step7Listener.getDateValeur()) + "<br>Cliquez sur Annuler pour ne pas générer le fichier", ZMsgPanel.BTLABEL_YES);

					if (choix == ZMsgPanel.MR_CANCEL) {
						return;
					}
					else if (choix == ZMsgPanel.MR_YES) {
						step7Listener.setDateValeur(nextDate);
					}

				}
			}
			//si modifs en attente, on enregistre
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

		} catch (Exception e) {
			showErrorDialog(e);
			return;
		}

		if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement effectuer le paiement ? \nCette opération est irréversible.", ZMsgPanel.BTLABEL_NO)) {

			step7Listener.actionSpecial1().setEnabled(false);
			waitingDialog.setTitle("Traitement du paiement");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setModal(true);

			updateLoadingMsg("Ce traitement peut être long...");
			try {

				final PaiementPayerThread thread = new PaiementPayerThread();
				thread.start();

				if (thread.isAlive()) {
					waitingDialog.setVisible(true);
				}

				ZLogger.debug("Paiement termine");

				if (lastException != null) {
					throw lastException;
				}
				if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
					prepareDefaultFile(currentPaiement);
				}

				if (msgFin != null) {
					msgFin = msgFin.replaceAll("\n", "<br>");
					showInfoDialog(msgFin);
				}

				// /retour
				if (lastException != null) {
					throw lastException;
				}

				if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
					changeStep(STEP8);
				}
				else {
					changeStep(STEP10);
				}

			} catch (Exception e) {
				showErrorDialog(e);
				getEditingContext().revert();
				//on ferme la fenetre pour eviter les erreurs...
				close();
			} finally {
				waitingDialog.setVisible(false);
				if (waitingDialog != null) {
					waitingDialog.dispose();
				}
			}

		}
	}

	/**
	 * Imprime le bordereau d'accompagnement
	 */
	public final void imprimerBordereau() {
		PaiementProxyCtrl.imprimerBordereau(this, nbOperations, currentPaiement, getSelectedVirementParamBdf());
	}

	public final void prepareDefaultFile(final EOPaiement virement) throws Exception {
		final String defaultFileName = "Fichier_" + getSelectedFormat() + "_" + ZStringUtil.extendWithChars("" + virement.paiNumero(), "0", 10, true) + "." + virement.typeVirement().getFileExtension();
		defaultFile = FilePaiementCtrl.saveFile(leContenuFichierVirement, myApp.temporaryDir, defaultFileName, nbOperations);
	}

	/**
	 * @return renvoi le virementParam sélectionné (il faut le caster ensuite)
	 */
	public final IVirementParam getSelectedVirementParam() {
		final IVirementParam selected = (IVirementParam) step7Listener.getFormatsVirementModel().getSelectedEObject();
		return selected;
	}

	/**
	 * @return
	 */
	public final EOVirementParamBdf getSelectedVirementParamBdf() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_BDF.equals(format)) {
			return (EOVirementParamBdf) getSelectedVirementParam();
		}
		return null;
	}

	//
	//	public final EOVirementParamEtebac getSelectedVirementParamEtebac() {
	//		final String format = getSelectedFormat();
	//		if (format == null) {
	//			return null;
	//		}
	//		if (EOTypeVirement.TVI_FORMAT_ETEBAC.equals(format)) {
	//			return (EOVirementParamEtebac) getSelectedVirementParam();
	//		}
	//		return null;
	//	}

	public final EOVirementParamVint getSelectedVirementParamVint() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_VINT.equals(format)) {
			return (EOVirementParamVint) getSelectedVirementParam();
		}
		return null;
	}

	public final EOVirementParamSepa getSelectedVirementParamSepa() {
		final String format = getSelectedFormat();
		if (format == null) {
			return null;
		}
		if (EOTypeVirement.TVI_FORMAT_SEPASCT.equals(format)) {
			return (EOVirementParamSepa) getSelectedVirementParam();
		}
		return null;
	}

	public final EOTypeVirement getSelectedTypeVirement() {
		return step1Panel.getSelectedTypeVirement();
		//		final EOEnterpriseObject selected = getSelectedVirementParam();
		//		if (selected == null) {
		//			return null;
		//		}
		//		return (EOTypeVirement) selected.valueForKey("typevirement");
	}

	public final String getSelectedFormat() {
		final EOTypeVirement tv = getSelectedTypeVirement();
		if (tv == null) {
			return null;
		}
		return tv.tviFormat();
	}

	/**
	 * Change le panel actif (en effectuant un updateData sur le nouveau).
	 * 
	 * @param newStep
	 */
	private final void changeStep(final String newStep) {
		ZLogger.debug(newStep);
		cardLayout.show(cardPanel, newStep);
		currentPanel = (ZKarukeraStepPanel2) stepPanels.get(newStep);
		try {
			currentPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP2() {
		try {
			getEditingContext().revert();
			changeStep(STEP1);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP3() {
		try {
			getEditingContext().revert();
			changeStep(STEP2);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP4() {
		try {
			// verifier si mandats selectionnés
			if (mandatsAPayer().count() > 0) {
				changeStep(STEP3);
			}
			else {
				// si pas de mandat sélectionnés on passe au 2
				prev_STEP3();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP5() {
		try {
			// verifier si titres selectionnés
			if (titresAPayer().count() > 0) {
				changeStep(STEP4);
			}
			else {
				// si pas de titre sélectionnés on passe au 3
				prev_STEP4();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP6() {
		try {
			// verifier si odps selectionnés
			if (odpsAPayer().count() > 0) {
				changeStep(STEP5);
			}
			else {
				// si pas de odp sélectionnés on passe au 4
				prev_STEP5();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP7() {
		try {
			changeStep(STEP6);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP8() {
		try {
			changeStep(STEP6);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Apres la selection du type de paiement.
	 */
	private final void next_STEP1() {
		try {
			setWaitCursor(true);
			ZLogger.debug(STEP2);
			step1Listener.domaineSelectionne = step1Panel.getSelectedDomaine();
			if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
				if (getSelectedTypeVirement() == null) {
					throw new Exception("Vous devez sélectionner un type de virement. Si Aucun type de virement n'est proposé, il s'agit d'un problème de paramétrage.");
				}

				final NSMutableArray formatsVirements = new NSMutableArray();
				if (getSelectedTypeVirement() != null) {
					formatsVirements.addObjectsFromArray(getSelectedTypeVirement().toVirementParamBdfs(new EOKeyValueQualifier(EOVirementParamBdf.VPB_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOVirementParamBdf.ETAT_VALIDE)));
					formatsVirements.addObjectsFromArray(getSelectedTypeVirement().toVirementParamSepas(new EOKeyValueQualifier(EOVirementParamSepa.VPS_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOVirementParamSepa.ETAT_VALIDE)));
				}
				step7Listener.formatsVirementModel.updateListWithData(formatsVirements);

			}
			step2Listener.updateData();

			//verifier la bonne recuperation du compte de paiement affecté au mode de paiement
			NSMutableArray mps = new NSMutableArray();
			mps.addObjectsFromArray(ZEOUtilities.getDistinctsOfNSArray((NSArray) step2Listener.getAllMandats().valueForKey(EOMandat.MODE_PAIEMENT_KEY)));
			mps.addObjectsFromArray(ZEOUtilities.getDistinctsOfNSArray((NSArray) step2Listener.getAllOdps().valueForKey(EOOrdreDePaiement.MODE_PAIEMENT_KEY)));
			for (int i = 0; i < mps.count(); i++) {
				EOModePaiement mp = (EOModePaiement) mps.objectAtIndex(i);
				mp.cleanPlanCoPaiementSelonExercice();
				mp.getPlanCoPaiementSelonExercice(getExercice());
			}

			changeStep(STEP2);

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	/**
	 * Apres la selection des mandats et odps...
	 */
	private final void next_STEP2() {
		try {
			ZLogger.debug(STEP3);

			if (!step2Panel.getPaiementPanelMandatSelect().getMyEOTable().stopCellEditing()) {
				return;
			}

			//Mettre a jour les mandats mis en attente
			final NSArray tmp = mandatsADifferer();
			for (int i = 0; i < tmp.count(); i++) {
				final EOMandat element = (EOMandat) tmp.objectAtIndex(i);
				//              Vérifier que les mandats différés ont une raison d'affectée
				if (ZStringUtil.isEmpty((String) step2Listener.objetsAttenteMandats.get(element))) {
					throw new DataCheckException("Veuillez indiquer la raison pour laquelle vous différez le paiement du mandat n°" + element.manNumero().intValue() + " du " + element.gestion().gesCode() + ".");
				}
				if (!ZFinderEtats.etat_OUI().equals(element.attentePaiement())) {
					element.setAttentePaiementRelationship(ZFinderEtats.etat_OUI());
					element.setManAttenteObjet((String) step2Listener.objetsAttenteMandats.get(element));
					element.setManAttenteDate(new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime()));
					element.setUtilisateurAttenteRelationship(getUtilisateur());
				}
			}

			final NSArray tmp2 = mandatsAPayer();
			for (int i = 0; i < tmp2.count(); i++) {
				final EOMandat element = (EOMandat) tmp2.objectAtIndex(i);
				element.setAttentePaiementRelationship(ZFinderEtats.etat_NON());
			}

			//Vérifier qu'il n'y a pas des mandats coches simultanement "Payer" et "Differer"
			final ArrayList list = new ArrayList();
			list.add(tmp);
			list.add(tmp2);
			if (ZEOUtilities.intersectionOfNSArray(list).count() > 0) {
				throw new DataCheckException("Certains mandats sont cochés à la fois en \"Payer\" et \"Différer\". Veuillez régler le problème avant de continuer.");
			}

			//S'il y a des changements sur les mises en différés, les enregistrer.
			//	System.out.println(getEditingContext().updatedObjects());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
				System.out.println("changements enregistrés");
			}

			if ("OUI".equals(myApp.getParametres().valueForKey("CTRL_DOUBLONS_PAIEMENT"))) {
				Integer nbJours = null;
				if (myApp.getParametres().valueForKey("CTRL_DOUBLONS_PAIEMENT_NB_JOURS_DEPUIS") != null) {

					try {
						nbJours = Integer.valueOf((String) myApp.getParametres().valueForKey("CTRL_DOUBLONS_PAIEMENT_NB_JOURS_DEPUIS"));
					} catch (NumberFormatException e) {
						System.err.println("Parametre CTRL_DOUBLONS_PAIEMENT_NB_JOURS_DEPUIS : doit etre un entier");
						e.printStackTrace();
					}
				}
				checkDoublonsDepenses(nbJours);
			}

			// verifier si mandats selectionnés
			if (mandatsAPayer().count() > 0) {
				if (verifierMandatsErreurs()) {
					return;
				}

				changeStep(STEP3);
			}
			else {
				// si pas de mandat sélectionnés on passe au 4
				next_STEP3();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void checkDoublonsDepenses(Integer nbJours) {
		//verifier s'il y a des doublons
		final NSArray mandatsAPayer = mandatsAPayer();
		final NSArray odpsAPayer = odpsAPayer();
		//String sql = "";
		String sqlDeps = "";
		String sqlOdps = "";
		String critDepid = "";
		String critOdpOrdre = "";
		String critDepDateMin = "";
		if (mandatsAPayer.count() > 0 || odpsAPayer().count() > 0) {
			for (int i = 0; i < mandatsAPayer.count(); i++) {
				final EOMandat element = (EOMandat) mandatsAPayer.objectAtIndex(i);
				for (int j = 0; j < element.depenses().count(); j++) {
					EODepense depense = (EODepense) element.depenses().objectAtIndex(j);
					if (critDepid.length() > 0) {
						critDepid += " or ";
					}
					critDepid += "d1.dep_id=" + ServerProxy.serverPrimaryKeyForObject(getEditingContext(), depense).valueForKey(EODepense.DEP_ID_KEY);
				}
			}

			for (int i = 0; i < odpsAPayer.count(); i++) {
				final EOOrdreDePaiement element = (EOOrdreDePaiement) odpsAPayer.objectAtIndex(i);
				if (critOdpOrdre.length() > 0) {
					critOdpOrdre += " or ";
				}
				critOdpOrdre += "d1.odp_ordre=" + ServerProxy.serverPrimaryKeyForObject(getEditingContext(), element).valueForKey(EOOrdreDePaiement.ODP_ORDRE_KEY);
			}

			if (nbJours != null) {
				critDepDateMin = " and p2.pai_date_creation >= sysdate-" + nbJours.intValue();
			}

			if (mandatsAPayer.count() > 0) {

				sqlDeps =
						"select 'MANDAT' d1_type, d1.dep_id, d1.dep_date_reception, d1.dep_numero, d1.dep_montant_disquette, " +
								"m1.man_numero, m1.ges_code, " +
								"f1.fou_code, f1.nom adr_nom, f1.prenom adr_prenom,  r1.rib_codbanc, r1.rib_guich, r1.rib_num, r1.rib_cle, r1.rib_titco ,r1.rib_bic, r1.rib_iban, "
								+ " d2.dep_id d2_id,d2.dep_date_reception d2_date_reception, d2.dep_montant_disquette d2_dep_montant_disquette, " +
								"'MANDAT' d2_type,m2.man_numero||' (fact. '|| d2.dep_numero ||')' d2_numero, "
								+ " m2.ges_code d2_ges_code,f2.fou_code d2_fou_code, f2.nom d2_adr_nom, f2.prenom d2_adr_prenom,  r2.rib_codbanc d2_rib_codbanc, " +
								"r2.rib_guich d2_rib_guich, r2.rib_num d2_rib_num, r2.rib_cle d2_rib_cle, r2.rib_titco d2_rib_titco, r2.rib_bic d2_rib_bic, r2.rib_iban d2_rib_iban,"
								+ " p2.pai_numero, p2.pai_date_creation, p2.exe_ordre " +
								" from maracuja.depense d1, maracuja.mandat m1, maracuja.v_rib r1, maracuja.v_fournis_light f1, " +
								" maracuja.depense d2, maracuja.mandat m2,  maracuja.v_rib r2, maracuja.v_fournis_light f2 , maracuja.paiement p2 "
								+ " where "
								+ " d1.man_id=m1.man_id " +
								" and m1.rib_ordre_comptable=r1.rib_ordre " +
								" and m1.fou_ordre=f1.fou_ordre " +
								" and d2.man_id=m2.man_id " +
								" and m2.rib_ordre_comptable=r2.rib_ordre " +
								" and m2.fou_ordre=f2.fou_ordre " +
								" and m2.pai_ordre=p2.pai_ordre "
								+ " and d1.dep_id<>d2.dep_id " +
								" and d1.dep_montant_disquette = d2.dep_montant_disquette " +
								" and ( " +
								" (r1.rib_num is not null and (r1.rib_num = r2.rib_num " + " and r1.rib_cle = r2.rib_cle " + " and r1.rib_codbanc = r2.rib_codbanc " + " and r1.rib_guich = r2.rib_guich " + " )) " +
								" or "
								+ " (r1.rib_iban is not null and (r1.rib_iban= r2.rib_iban)) " +
								" or "
								+ " (f1.nom= f2.nom) " +

								" ) " +
								" and m2.man_etat='PAYE' "
								+ " and (" + critDepid + ") "
								+ critDepDateMin
								+ " UNION ALL "
								+ " select 'MANDAT' d1_type, d1.dep_id, d1.dep_date_reception, d1.dep_numero, d1.dep_montant_disquette, " +
								"m1.man_numero, m1.ges_code, " +
								"f1.fou_code, f1.nom adr_nom, f1.prenom adr_prenom,  r1.rib_codbanc, r1.rib_guich, r1.rib_num, r1.rib_cle, r1.rib_titco ,r1.rib_bic, r1.rib_iban, "
								+ " m2.odp_ordre d2_id,m2.odp_date_saisie d2_date_reception, m2.odp_montant_paiement d2_dep_montant_disquette, " +
								"'ORDRE DE PAIEMENT' d2_type,m2.odp_numero || ' '|| m2.odp_libelle d2_numero, "
								+ " '' d2_ges_code,f2.fou_code d2_fou_code, f2.nom d2_adr_nom, f2.prenom d2_adr_prenom,  " +
								"r2.rib_codbanc d2_rib_codbanc, r2.rib_guich d2_rib_guich, r2.rib_num d2_rib_num, r2.rib_cle d2_rib_cle, r2.rib_titco d2_rib_titco , r2.rib_bic d2_rib_bic, r2.rib_iban d2_rib_iban,"
								+ " p2.pai_numero, p2.pai_date_creation, p2.exe_ordre " +
								" from maracuja.depense d1, maracuja.mandat m1, maracuja.v_rib r1, maracuja.v_fournis_light f1, " +
								" maracuja.ordre_de_paiement m2, maracuja.v_rib r2, maracuja.v_fournis_light f2 , maracuja.paiement p2 " +
								" where "
								+ " d1.man_id=m1.man_id " +
								" and m1.rib_ordre_comptable=r1.rib_ordre " +
								" and m1.fou_ordre=f1.fou_ordre " +
								" and m2.rib_ordre=r2.rib_ordre " +
								" and m2.fou_ordre=f2.fou_ordre " +
								" and m2.pai_ordre=p2.pai_ordre " +
								" and d1.dep_montant_disquette = m2.odp_montant_paiement " +
								" and ( " +
								" (r1.rib_num is not null and (r1.rib_num = r2.rib_num " + " and r1.rib_cle = r2.rib_cle " + " and r1.rib_codbanc = r2.rib_codbanc " + " and r1.rib_guich = r2.rib_guich " + " )) " +
								" or "
								+ " (r1.rib_iban is not null and (r1.rib_iban= r2.rib_iban)) " +
								" or "
								+ " (f1.nom= f2.nom) " +

								" ) " +
								" and m2.odp_etat='PAYE' "
								+ " and (" + critDepid + ") "
								+ critDepDateMin;

			}
			if (odpsAPayer.count() > 0) {

				sqlOdps =
						"select 'ORDRE DE PAIEMENT' d1_type, d1.odp_ordre dep_id, d1.odp_date_saisie dep_date_reception, d1.odp_numero||' '||d1.odp_libelle dep_numero, d1.odp_montant_paiement dep_montant_disquette, "
								+
								"0 man_numero, ' ' ges_code, "
								+
								"f1.fou_code, f1.nom adr_nom, f1.prenom adr_prenom,  r1.rib_codbanc, r1.rib_guich, r1.rib_num, r1.rib_cle, r1.rib_titco ,r1.rib_bic, r1.rib_iban,"
								+ " d2.dep_id d2_id,d2.dep_date_reception d2_date_reception, d2.dep_montant_disquette d2_dep_montant_disquette, "
								+
								"'MANDAT' d2_type,m2.man_numero||' (fact. '|| d2.dep_numero ||')' d2_numero, "
								+ " m2.ges_code d2_ges_code,f2.fou_code d2_fou_code, f2.nom d2_adr_nom, f2.prenom d2_adr_prenom,  r2.rib_codbanc d2_rib_codbanc, r2.rib_guich d2_rib_guich, r2.rib_num d2_rib_num, r2.rib_cle d2_rib_cle, r2.rib_titco d2_rib_titco, r2.rib_bic d2_rib_bic, r2.rib_iban d2_rib_iban,"
								+ " p2.pai_numero, p2.pai_date_creation, p2.exe_ordre " +
								" from maracuja.ordre_de_paiement d1, maracuja.v_rib r1, maracuja.v_fournis_light f1, " + " maracuja.depense d2, maracuja.mandat m2,  maracuja.v_rib r2, maracuja.v_fournis_light f2 , maracuja.paiement p2 "
								+ " where " +
								" d1.rib_ordre=r1.rib_ordre " +
								" and d1.fou_ordre=f1.fou_ordre " +
								" and d2.man_id=m2.man_id " +
								" and m2.rib_ordre_comptable=r2.rib_ordre " +
								" and m2.fou_ordre=f2.fou_ordre " +
								" and m2.pai_ordre=p2.pai_ordre " +
								" and d1.odp_montant_paiement = d2.dep_montant_disquette " +
								" and ( " +
								" (r1.rib_num is not null and (r1.rib_num = r2.rib_num " + " and r1.rib_cle = r2.rib_cle " + " and r1.rib_codbanc = r2.rib_codbanc " + " and r1.rib_guich = r2.rib_guich " + " )) " +
								" or "
								+ " (r1.rib_iban is not null and (r1.rib_iban= r2.rib_iban)) " +
								" or "
								+ " (f1.nom= f2.nom) " +

								" ) " +
								" and m2.man_etat='PAYE' "
								+ " and ("
								+ critOdpOrdre + ") "
								+ critDepDateMin
								+ " UNION ALL " +
								"select 'ORDRE DE PAIEMENT' d1_type, d1.odp_ordre dep_id, d1.odp_date_saisie dep_date_reception, d1.odp_numero||' '||d1.odp_libelle dep_numero, d1.odp_montant_paiement dep_montant_disquette, " +
								"0 man_numero, ' ' ges_code, " +
								"f1.fou_code, f1.nom adr_nom, f1.prenom adr_prenom,  r1.rib_codbanc, r1.rib_guich, r1.rib_num, r1.rib_cle, r1.rib_titco , r1.rib_bic, r1.rib_iban,"
								+ " m2.odp_ordre d2_id,m2.odp_date_saisie d2_date_reception, m2.odp_montant_paiement d2_dep_montant_disquette, " +
								"'ORDRE DE PAIEMENT' d2_type,m2.odp_numero || ' '|| m2.odp_libelle d2_numero, "
								+ " '' d2_ges_code,f2.fou_code d2_fou_code, f2.nom d2_adr_nom, f2.prenom d2_adr_prenom,  " +
								"r2.rib_codbanc d2_rib_codbanc, r2.rib_guich d2_rib_guich, r2.rib_num d2_rib_num, r2.rib_cle d2_rib_cle, r2.rib_titco d2_rib_titco , r2.rib_bic d2_rib_bic, r2.rib_iban d2_rib_iban,"
								+ " p2.pai_numero, p2.pai_date_creation, p2.exe_ordre " +
								" from maracuja.ordre_de_paiement d1, maracuja.v_rib r1, maracuja.v_fournis_light f1, " +
								" maracuja.ordre_de_paiement m2, maracuja.v_rib r2, maracuja.v_fournis_light f2 , maracuja.paiement p2 " +
								" where " +
								" d1.rib_ordre=r1.rib_ordre " +
								" and d1.fou_ordre=f1.fou_ordre " +
								" and m2.rib_ordre=r2.rib_ordre " +
								" and m2.fou_ordre=f2.fou_ordre " +
								" and m2.pai_ordre=p2.pai_ordre " +
								" and d1.odp_ordre<>m2.odp_ordre " +
								" and d1.odp_montant_paiement = m2.odp_montant_paiement " +
								" and ( " +
								" (r1.rib_num is not null and (r1.rib_num = r2.rib_num " + " and r1.rib_cle = r2.rib_cle " + " and r1.rib_codbanc = r2.rib_codbanc " + " and r1.rib_guich = r2.rib_guich " + " )) " +
								" or "
								+ " (r1.rib_iban is not null and (r1.rib_iban= r2.rib_iban)) " +
								" or "
								+ " (f1.nom= f2.nom) " +

								" ) " +
								" and m2.odp_etat='PAYE' "
								+ " and (" + critOdpOrdre + ") "
								+ critDepDateMin;

			}

			String sqlTmp = sqlOdps;
			if (sqlOdps.length() > 0 && sqlDeps.length() > 0) {
				sqlTmp += " UNION ALL ";
			}
			sqlTmp += sqlDeps;

			String sql = "select * from (" + sqlTmp + ")  order by d1_type, dep_id, d2_id";
			String sqlNb = "select count(*) as NB from (" + sqlTmp + ")";

			NSArray res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sqlNb);
			if (res.count() > 0 && ((Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB")).intValue() > 0) {

				if (showConfirmationDialog("Confirmation", "Certaines factures que vous souhaitez payer ont peut-être déjà fait l'objet d'un paiement, souhaitez-vous consulter la liste des doublons potentiels avant d'effectuer le paiement ? ", ZMsgPanel.BTLABEL_YES)) {
					try {
						final String filePath = ReportFactoryClient.imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), "paiement_doublons_dep.jasper", "paiement_doublons_dep.pdf", sql, getMyDialog(), null);
						if (filePath != null) {
							myApp.openPdfFile(filePath);
						}
					} catch (Exception e1) {
						showErrorDialog(e1);
					}

				}
			}
		}

	}

	private final void next_STEP3() {
		try {
			ZLogger.debug(STEP4);
			//si modifs en attente, on enregistre
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}

			// verifier si titres selectionnés
			if (titresAPayer().count() > 0) {
				changeStep(STEP4);
			}
			else {
				// si pas de titre sélectionnés on passe au 5
				next_STEP4();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP4() {
		try {
			ZLogger.debug(STEP5);

			// verifier si odps selectionnés
			if (odpsAPayer().count() > 0) {
				if (verifierOdpsErreurs()) {
					return;
				}
				changeStep(STEP5);
			}
			else {
				// si pas de odp sélectionnés on passe au 6
				next_STEP5();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP5() {
		try {
			changeStep(STEP6);
			step6Listener.actionSpecial1().setEnabled(false);
			step6Listener.actionNext().setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP6() {
		try {
			if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
				changeStep(STEP7);
				step7Listener.actionSpecial1().setEnabled(true);
			}
			else {
				changeStep(STEP9);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP7() {
		try {
			showinfoDialogFonctionNonImplementee();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP8() {
		try {
			showinfoDialogFonctionNonImplementee();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		// ZKarukeraDialog win = new ZKarukeraDialog(dial, TITLE,true);
		setMyDialog(win);
		initGUI();
		// step1Panel.setMyDialog(win);
		cardPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(cardPanel);
		win.pack();
		return win;
	}

	// //////////////////////////////////////////////////////////////////////////////

	private class PaiementPanelStep1Listener implements PaiementPanelStep1.Step1Listener {
		/** Domaine sélectionné par l'utilisateur */
		private String domaineSelectionne;

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final HashMap numOfObjByDomaine = new HashMap(3);

		/**
         * 
         */
		public PaiementPanelStep1Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(true);
			domaineSelectionne = EOModePaiement.MODDOM_VIREMENT;

			// Compter les objets par domaine de paiement
			HashMap objs = new HashMap(3);
			objs.put(MANDATS, new Integer(getMandatsByDomainePaiement(EOModePaiement.MODDOM_VIREMENT).count()));
			//objs.put(TITRES, new Integer(getTitresByDomainePaiement(EOModePaiement.MODDOM_VIREMENT).count()));
			objs.put(ODPS, new Integer(getOdpByDomainePaiement(EOModePaiement.MODDOM_VIREMENT).count()));
			numOfObjByDomaine.put(EOModePaiement.MODDOM_VIREMENT, objs);

			HashMap objs2 = new HashMap(3);
			objs2.put(MANDATS, new Integer(getMandatsByDomainePaiement(EOModePaiement.MODDOM_CHEQUE).count()));
			objs2.put(TITRES, new Integer(getTitresByDomainePaiement(EOModePaiement.MODDOM_CHEQUE).count()));
			objs2.put(ODPS, new Integer(getOdpByDomainePaiement(EOModePaiement.MODDOM_CHEQUE).count()));
			numOfObjByDomaine.put(EOModePaiement.MODDOM_CHEQUE, objs2);

			HashMap objs3 = new HashMap(3);
			objs3.put(MANDATS, new Integer(getMandatsByDomainePaiement(EOModePaiement.MODDOM_CAISSE).count()));
			objs3.put(TITRES, new Integer(getTitresByDomainePaiement(EOModePaiement.MODDOM_CAISSE).count()));
			objs3.put(ODPS, new Integer(getOdpByDomainePaiement(EOModePaiement.MODDOM_CAISSE).count()));
			numOfObjByDomaine.put(EOModePaiement.MODDOM_CAISSE, objs3);

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP1();
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep1.Step1Listener#getSelectedDomaine()
		 */
		public String getSelectedDomaine() {
			return domaineSelectionne;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep1.Step1Listener#getNumOfObjByDomaine()
		 */
		public HashMap getNumOfObjByDomaine() {
			return numOfObjByDomaine;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep1.Step1Listener#getMontantByDomaine()
		 */
		public HashMap getMontantByDomaine() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep1.Step1Listener#getcomptabilites()
		 */
		public NSArray getcomptabilites() {
			return comptabilites;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep1.Step1Listener#getcomptabiliteModel()
		 */
		public ZEOComboBoxModel getcomptabiliteModel() {
			return myComptabiliteModel;
		}

		public ZEOComboBoxModel getTypeVirementsModel() {
			return typeVirementsModel;
		}

		public void onTypeVirementSelected() {

		}
	}

	private class PaiementPanelStep2Listener implements PaiementPanelStep2.Step2Listener {
		private final Boolean PRESELECTION_OBJET = Boolean.TRUE;
		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final HashMap checkedMandats = new HashMap();
		private final HashMap differedMandats = new HashMap();
		private final HashMap objetsAttenteMandats = new HashMap();
		private NSArray allMandats;

		private final HashMap checkedTitres = new HashMap();
		private NSArray allTitres;

		private final HashMap checkedOdps = new HashMap();
		private NSArray allOdps;

		private final CheckOdpPayerModifier checkOdpPayerModifier = new CheckOdpPayerModifier();
		private final CheckPayerModifier checkPayerModifier = new CheckPayerModifier();
		private final CheckDiffereredModifier checkDiffereredModifier = new CheckDiffereredModifier();

		private final MandatTableModelDelegate mandatTableModelProvider = new MandatTableModelDelegate();

		private final HashMap mandatFilters = new HashMap();
		private final ZEOComboBoxModel typesBordereauxMandatMdl;
		private final NSArray typesBordereaux;

		private final MandatSelectFilterPanelListener mandatSelectFilterPanelListener = new MandatSelectFilterPanelListener();

		/**
         * 
         */
		public PaiementPanelStep2Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(true);
			actionPrev.setEnabled(true);

			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeBordereau.TBO_TYPE_KEY + "=%@ and tboSousType<>%@ and tboSousType<>%@", new NSArray(new Object[] {
					EOTypeBordereau.TypeBordereauBTME, EOTypeBordereau.SOUS_TYPE_PAPAYE, EOTypeBordereau.SOUS_TYPE_REVERSEMENTS
			}));
			typesBordereaux = EOsFinder.getLesTypeBordereauxByQual(getEditingContext(), qual);

			typesBordereauxMandatMdl = new ZEOComboBoxModel(typesBordereaux, EOTypeBordereau.TBO_LIBELLE_KEY, "Tous", null);

			// updateData();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP2();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP2();

			}

		}

		private class CheckDiffereredModifier implements ZEOTableModelColumn.Modifier {
			/**
			 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
			 */
			public void setValueAtRow(Object value, int row) {
				System.out.println(row + ": " + value);
				final Boolean val = (Boolean) value;
				final EOMandat mandat = (EOMandat) step2Panel.getPaiementPanelMandatSelect().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				try {
					if (val.booleanValue()) {
						checkedMandats.put(mandat, Boolean.FALSE);
					}
					differedMandats.put(mandat, Boolean.TRUE);
					step2Panel.getPaiementPanelMandatSelect().getMyTableModel().fireTableRowUpdated(row);
					step2Panel.updateTotaux();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}

		private class CheckOdpPayerModifier implements ZEOTableModelColumn.Modifier {

			public void setValueAtRow(Object value, int row) {
				final Boolean val = (Boolean) value;
				final EOOrdreDePaiement odp = (EOOrdreDePaiement) step2Panel.getPaiementPanelOdpSelect().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				try {
					checkedOdps.put(odp, val);
					step2Panel.getPaiementPanelOdpSelect().getMyTableModel().fireTableRowUpdated(row);
					step2Panel.updateTotaux();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}

		private class CheckPayerModifier implements ZEOTableModelColumn.Modifier {
			/**
			 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
			 */
			public void setValueAtRow(Object value, int row) {
				System.out.println(row + ": " + value);
				final Boolean val = (Boolean) value;
				final EOMandat mandat = (EOMandat) step2Panel.getPaiementPanelMandatSelect().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				try {
					if (val.booleanValue()) {
						differedMandats.put(mandat, Boolean.FALSE);
					}
					checkedMandats.put(mandat, val);
					step2Panel.getPaiementPanelMandatSelect().getMyTableModel().fireTableRowUpdated(row);
					step2Panel.updateTotaux();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}

		private final class MandatTableModelDelegate implements IZEOTableModelDelegate {

			public boolean isCellEditable(int row, int col) {
				if (col < 2) {
					return true;
				}
				final EOMandat mandat = (EOMandat) step2Panel.getPaiementPanelMandatSelect().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				return ((Boolean) differedMandats.get(mandat)).booleanValue();
			}

		}

		private final class MandatSelectFilterPanelListener implements IMandatSelectFilterPanelListener {

			public Action actionSrch() {
				return new AbstractAction() {

					public void actionPerformed(ActionEvent actionevent) {
						updateData();
						try {
							currentPanel.updateData();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				};
			}

			public HashMap getFilters() {
				return mandatFilters;
			}

			public ZEOComboBoxModel getTypeBordereauxMdl() {
				return typesBordereauxMandatMdl;
			}

		}

		public String getSelectedDomaine() {
			return step1Panel.getSelectedDomaine();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2.Step2Listener#getAllMandats()
		 */
		public NSArray getAllMandats() {
			return allMandats;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2.Step2Listener#getCheckedMandats()
		 */
		public HashMap getCheckedMandats() {
			return checkedMandats;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2.Step2Listener#getAllTitres()
		 */
		public NSArray getAllTitres() {
			return allTitres;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2.Step2Listener#getCheckedTitres()
		 */
		public HashMap getCheckedTitres() {
			return checkedTitres;
		}

		protected NSMutableArray buildFilterQualifiersForMandats(HashMap dicoFiltre) throws Exception {
			NSMutableArray quals = new NSMutableArray();

			if (dicoFiltre.get(EOTypeBordereau.ENTITY_NAME) != null) {
				quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.BORDEREAU_KEY + "." + EOBordereau.TYPE_BORDEREAU_KEY + "=%@", new NSArray(dicoFiltre.get(EOTypeBordereau.ENTITY_NAME))));
			}

			return quals;
		}

		public final void updateData() {
			checkedMandats.clear();
			differedMandats.clear();
			objetsAttenteMandats.clear();
			checkedTitres.clear();
			checkedOdps.clear();

			// fetcher les mandats, et odps en fonction du domaine
			// selectionne.

			allMandats = getMandatsByDomainePaiement(step1Panel.getSelectedDomaine());
			//pour résoudre le bug qui entraine un update du mandat avec un man_etat à attente
			getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), allMandats));
			NSArray filters = null;
			try {
				filters = buildFilterQualifiersForMandats(mandatFilters);
			} catch (Exception e) {
				e.printStackTrace();
				filters = null;
			}
			if (filters != null && filters.count() > 0) {
				allMandats = EOQualifier.filteredArrayWithQualifier(allMandats, new EOAndQualifier(filters));
			}

			for (int i = 0; i < allMandats.count(); i++) {
				final EOMandat element = (EOMandat) allMandats.objectAtIndex(i);
				differedMandats.put(element, (ZFinderEtats.etat_OUI().equals(element.attentePaiement()) ? Boolean.TRUE : Boolean.FALSE));
				checkedMandats.put(element, (ZFinderEtats.etat_OUI().equals(element.attentePaiement()) ? Boolean.FALSE : Boolean.TRUE));
				objetsAttenteMandats.put(element, element.manAttenteObjet());
			}

			allTitres = getTitresByDomainePaiement(step1Panel.getSelectedDomaine());

			for (int i = 0; i < allTitres.count(); i++) {
				final EOTitre element = (EOTitre) allTitres.objectAtIndex(i);
				checkedTitres.put(element, PRESELECTION_OBJET);
			}

			allOdps = getOdpByDomainePaiement(step1Panel.getSelectedDomaine());
			for (int i = 0; i < allOdps.count(); i++) {
				final EOOrdreDePaiement element = (EOOrdreDePaiement) allOdps.objectAtIndex(i);
				checkedOdps.put(element, PRESELECTION_OBJET);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2.Step2Listener#getAllOdps()
		 */
		public NSArray getAllOdps() {
			return allOdps;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep2.Step2Listener#getCheckedOdps()
		 */
		public HashMap getCheckedOdps() {
			return checkedOdps;
		}

		public HashMap getAllDifferedMandats() {
			return differedMandats;
		}

		public Modifier checkDifferedModifier() {
			return checkDiffereredModifier;
		}

		public Modifier checkPayerModifier() {
			return checkPayerModifier;
		}

		public IZEOTableModelDelegate getMandatModelProvider() {
			return mandatTableModelProvider;
		}

		public Map getAllAttenteRaisonMandats() {
			return objetsAttenteMandats;
		}

		public IMandatSelectFilterPanelListener getMandatSelectFilterPanelListener() {
			return mandatSelectFilterPanelListener;
		}

		public Modifier checkOdpPayerModifier() {
			return checkOdpPayerModifier;
		}

	}

	/**
	 * ajout des retenues
	 */
	private class PaiementPanelStep3Listener implements PaiementPanelStep3.Step3Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final Action actionRetenueAdd;
		private final Action actionRetenueDelete;

		/**
         * 
         */
		public PaiementPanelStep3Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionRetenueAdd = new ActionRetenueAdd();
			actionRetenueDelete = new ActionRetenueDelete();
			actionNext.setEnabled(true);
			actionPrev.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP3();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP3();

			}

		}

		private final class ActionRetenueAdd extends AbstractAction {

			public ActionRetenueAdd() {
				super();
				setEnabled(false);
				putValue(Action.NAME, "");
				putValue(Action.SHORT_DESCRIPTION, "Créer une nouvelle retenue pour la dépense sélectionnée");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			}

			public void actionPerformed(ActionEvent e) {
				retenueAdd();
			}
		}

		private final class ActionRetenueDelete extends AbstractAction {

			public ActionRetenueDelete() {
				super();
				setEnabled(false);
				putValue(Action.NAME, "");
				putValue(Action.SHORT_DESCRIPTION, "Supprimer la retenue sélectionnée");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				retenueDelete();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep3.Step3Listener#getMandats()
		 */
		public NSArray getMandats() {
			return mandatsAPayer();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep3.Step3Listener#getRetenues()
		 */
		public NSArray getRetenues() {
			EODepense dep = step3Panel.getSelectedDepense();
			if (dep != null) {
				return dep.retenues();
			}
			return new NSArray();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep3.Step3Listener#getDepenses()
		 */
		public NSArray getDepenses() {
			EOMandat man = step3Panel.getSelectedMandat();
			if (man != null) {
				return man.depenses();
			}
			return new NSArray();
		}

		public Action actionRetenueAdd() {
			return actionRetenueAdd;
		}

		public Action actionRetenueDelete() {
			return actionRetenueDelete;
		}

		/**
		 * Ajoute la retenue (mise à jour du montant des retenues + ouverture dialogue saisi + mise à jour de l'affichage).
		 */
		private final void retenueAdd() {

			try {
				final EODepense depense = step3Panel.getSelectedDepense();
				if (depense == null) {
					return;
				}
				depense.majMontantDisquette();
				if (depense.depMontantDisquette().compareTo(ZConst.BIGDECIMAL_ZERO) <= 0) {
					throw new DefaultClientException("Le total des retenues affectées à la dépense est déjà équivalent au montant de la dépense.");
				}
				final RetenueAjoutCtrl retenueAjoutCtrl = new RetenueAjoutCtrl(getEditingContext(), depense);
				retenueAjoutCtrl.openDialog(getMyDialog(), true);
				depense.majMontantDisquette();

				//mise a jour affichage
				step3Panel.fireSelectedMandatUpdated();
				step3Panel.fireSelectedDepenseUpdated();
				step3Listener.onDepenseSelectionChanged();

			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		private final void retenueDelete() {
			final EORetenue retenue = step3Panel.getSelectedRetenue();
			if (retenue != null) {

				try {
					FactoryProcessRetenue factoryProcessRetenue = new FactoryProcessRetenue(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					factoryProcessRetenue.supprimerRetenue(getEditingContext(), retenue, getUtilisateur());
					step3Panel.updateDataDepense();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}

		}

		public void onDepenseSelectionChanged() {
			try {
				step3Panel.updateDataRetenue();
				actionRetenueAdd.setEnabled(step3Panel.getSelectedDepense() != null);
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public void onMandatSelectionChanged() {
			try {
				step3Panel.updateDataDepense();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public void onRetenueSelectionChanged() {
			try {
				final EORetenue retenue = step3Panel.getSelectedRetenue();
				actionRetenueDelete.setEnabled(retenue != null);
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public EOTypeVirement selectedTypeVirement() {
			return getSelectedTypeVirement();
		}

	}

	private class PaiementPanelStep5Listener implements PaiementPanelStep5.Step5Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final Action actionRetenueNew;

		/**
         * 
         */
		public PaiementPanelStep5Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionRetenueNew = new ActionRetenueNew();
			actionNext.setEnabled(true);
			actionPrev.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP5();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP5();

			}

		}

		private final class ActionRetenueNew extends AbstractAction {

			public ActionRetenueNew() {
				super();
				setEnabled(false);
				putValue(Action.NAME, "Retenue");
				putValue(Action.SHORT_DESCRIPTION, "Créer une nouvelle retenue pour le mandat sélectionné");
			}

			public void actionPerformed(ActionEvent e) {
				// TODO gérer la création d'une retenue
				showinfoDialogFonctionNonImplementee();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep3.Step3Listener#actionRetenueNew()
		 */
		public Action actionRetenueNew() {
			return actionRetenueNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep3.Step3Listener#getRetenues()
		 */
		public NSArray getRetenues() {
			ArrayList rets = new ArrayList();
			EOMandat man = step3Panel.getSelectedMandat();
			if (man != null) {
				Object[] subs = man.depenses().objects();
				// ZLogger.debug(subs);
				for (int i = 0; i < subs.length; i++) {
					EODepense depense = (EODepense) subs[i];
					depense.retenues().objects();
					rets.addAll(depense.retenues().vector());
				}

			}
			return new NSArray(rets.toArray());
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep4.Step4Listener#getTitres()
		 */
		public NSArray getOdps() {
			return odpsAPayer();
		}

		public EOTypeVirement selectedTypeVirement() {
			return getSelectedTypeVirement();
		}

	}

	/**
	 * Recapitulatif (arbre)
	 */
	private class PaiementPanelStep6Listener implements PaiementPanelStep6.Step6Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		/**
         * 
         */
		public PaiementPanelStep6Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(true);
			actionPrev.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP6();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP6();

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep4.Step4Listener#getTitres()
		 */
		public NSArray getOdps() {
			return odpsAPayer();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep6.Step6Listener#getMandats()
		 */
		public NSArray getMandats() {
			return mandatsAPayer();
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep6.Step6Listener#getTitres()
		 */
		public NSArray getTitres() {
			return titresAPayer();
		}

	}

	/**
	 * Panel affichant le choix du type de virement.
	 */
	private class PaiementPanelStep7Listener implements PaiementPanelStep7.Step7Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final IZDatePickerFieldModel dateValeurModel;
		private ZEOComboBoxModel formatsVirementModel;
		//		private final DocumentListener dateValeurListener;
		private final ActionAfficheCalendrierBdf actionAfficheCalendrierBdf = new ActionAfficheCalendrierBdf();

		// private Date dateValeur;

		/**
         * 
         */
		public PaiementPanelStep7Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(true);
			actionSpecial1.setEnabled(true);
			dateValeurModel = new DateValeurModel();

			try {
				formatsVirementModel = new ZEOComboBoxModel(NSARRAYVIDE, "libelle", null, null);
			} catch (Exception e) {
				formatsVirementModel = null;
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP7();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP7();

			}

		}

		private final class ActionAfficheCalendrierBdf extends AbstractAction {
			public ActionAfficheCalendrierBdf() {
				super("");
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Afficher le calendrier BDF");
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				try {
					final String filePath = ReportFactoryClient.imprimerBdfCalendrier(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), getMyDialog());
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7.Step7Listener#dateValeurModel()
		 */
		public IZDatePickerFieldModel dateValeurModel() {
			return dateValeurModel;
		}

		public Date getDateValeur() {
			return dateValeur;
		}

		public void setDateValeur(Date adateValeur) {
			dateValeur = adateValeur;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7.Step7Listener#montantPaiement()
		 */
		public BigDecimal montantPaiement() {
			BigDecimal total = ZEOUtilities.calcSommeOfBigDecimals(step6Listener.getMandats(), EOMandat.MONTANT_A_PAYER_KEY);
			total = total.add(ZEOUtilities.calcSommeOfBigDecimals(step6Listener.getOdps(), EOOrdreDePaiement.ODP_TTC_KEY));
			return total;
		}

		public ZEOComboBoxModel getFormatsVirementModel() {
			return formatsVirementModel;
		}

		public boolean isDateEchangeValide() {
			return calendrierBDF.isDateDansCalendrier(dateValeur);
		}

		public Action actionAfficheCalendrierBdf() {
			return actionAfficheCalendrierBdf;
		}

		public boolean unEcritureDetailCompte5FieldEnabled() {
			return true;
		}

		public boolean isGrouperLignesParNoFactureVisible() {
			return getSelectedVirementParamSepa() != null;
		}

	}

	/**
	 * Panel de fin (permet d'imprimer et d'enregistrer les fichiers en fonction du format choisi)
	 */
	private class PaiementPanelStep8Listener implements PaiementPanelStep8.Step8Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;

		private final ActionImprimerBordereau actionImprimerBordereau;
		private final ActionImprimerContenuPaiement actionImprimerContenuPaiement;
		private final ActionImprimerDetailFichier actionImprimerDetailFichierBDF = new ActionImprimerDetailFichier();
		private final ActionImprRetenues actionImprRetenues = new ActionImprRetenues();
		private final ActionEnregistrer actionEnregistrer;

		/**
         * 
         */
		public PaiementPanelStep8Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new DefaultActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(false);
			actionSpecial1.setEnabled(false);

			actionEnregistrer = new ActionEnregistrer();
			actionImprimerBordereau = new ActionImprimerBordereau();
			actionImprimerContenuPaiement = new ActionImprimerContenuPaiement();

			actionEnregistrer.setEnabled(true);
			actionImprimerBordereau.setEnabled(true);
			actionImprimerContenuPaiement.setEnabled(true);
			actionImprimerDetailFichierBDF.setEnabled(true);
			actionImprRetenues.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#actionEnregistrer()
		 */
		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#actionImprimerBordereau()
		 */
		public Action actionImprimerBordereau() {
			return actionImprimerBordereau;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#actionImprimerContenuPaiement()
		 */
		public Action actionImprimerContenuPaiement() {
			return actionImprimerContenuPaiement;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep8.Step8Listener#typePaiement()
		 */
		public String typePaiement() {
			String t = step1Panel.getSelectedDomaine();
			if (step1Panel.getSelectedDomaine().equals(EOModePaiement.MODDOM_VIREMENT)) {
				t = t + "_" + getSelectedFormat();
			}
			return t;
		}

		public File getFileVirement() {
			return defaultFile;
		}

		public Action actionImprRetenues() {
			return actionImprRetenues;
		}

		public Action actionImprimerDetailFichierBDF() {
			return actionImprimerDetailFichierBDF;
		}
	}

	/**
	 * Panel pour selection date valeur pour paiement cheque et caisse
	 */
	private class PaiementPanelStep9Listener implements PaiementPanelStep9.Step9Listener {

		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final IZDatePickerFieldModel dateValeurModel;

		/**
         * 
         */
		public PaiementPanelStep9Listener() {
			actionPrev = new ActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(true);
			actionSpecial1.setEnabled(true);
			dateValeurModel = new DateValeurModel();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class ActionNext extends DefaultActionNext {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				next_STEP8();

			}

		}

		private final class ActionPrev extends DefaultActionPrev {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				prev_STEP8();

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep7.Step7Listener#dateValeurModel()
		 */
		public IZDatePickerFieldModel dateValeurModel() {
			return dateValeurModel;
		}

		public Date getDateValeur() {
			return dateValeur;
		}

		public void setDateValeur(Date adateValeur) {
			dateValeur = adateValeur;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep9.Step9Listener#getTile()
		 */
		public String getTile() {
			return "Date de valeur";
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep9.Step9Listener#getCommentaire()
		 */
		public String getCommentaire() {
			return null;
		}

		public boolean unEcritureDetailCompte5FieldEnabled() {
			return true;
		}

	}

	private final class DateValeurModel implements IZDatePickerFieldModel {
		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return dateValeur;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			System.out.println("DateValeurModel.setValue()");
			System.out.println(value);

			if (value instanceof Date) {
				dateValeur = (Date) value;
			}
			else {
				dateValeur = null;
			}

			System.out.println(dateValeur);
			System.out.println("*******");

		}

		public Window getParentWindow() {
			return getMyDialog();
		}

	}

	private class PaiementPanelStep10Listener implements PaiementPanelStep10.Step10Listener {
		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final ActionImprimerContenuPaiement actionImprimerContenuPaiement;
		private final ActionImprRetenues actionImprRetenues = new ActionImprRetenues();

		/**
         * 
         */
		public PaiementPanelStep10Listener() {
			actionPrev = new DefaultActionPrev();
			actionNext = new DefaultActionNext();
			actionSpecial1 = new DefaultActionPayer();
			actionNext.setEnabled(false);
			actionPrev.setEnabled(false);
			actionSpecial1.setEnabled(false);
			actionImprimerContenuPaiement = new ActionImprimerContenuPaiement();

			actionImprimerContenuPaiement.setEnabled(true);
			actionImprRetenues.setEnabled(true);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep10.Step10Listener#getTile()
		 */
		public String getTile() {
			return "Paiement effectué";
		}

		public String getCommentaire() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.paiement.ui.PaiementPanelStep10.Step10Listener#actionImprimerContenuPaiement()
		 */
		public Action actionImprimerContenuPaiement() {
			return actionImprimerContenuPaiement;
		}

		public Action actionImprRetenues() {
			return actionImprRetenues;
		}

	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionPayer extends ZStepAction {
		public DefaultActionPayer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Payer");
			putValue(Action.SHORT_DESCRIPTION, "Générer le paiement");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			payer();
		}

	}

	private class DefaultActionNext extends ZStepAction {
		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	public class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le Bordereau d'accompagnement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerBordereau();

		}
	}

	public class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(Action.NAME, "Enregistrer le fichier");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				FilePaiementCtrl.enregistrerFichier(PaiementCtrl.this, currentPaiement, leContenuFichierVirement, getSelectedFormat(), nbOperations);
				//                enregistrerFichier(currentPaiement);
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}
	}

	public class ActionImprimerContenuPaiement extends AbstractAction {
		public ActionImprimerContenuPaiement() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail du paiement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerContenuPaiement(PaiementCtrl.this, currentPaiement);
		}
	}

	public class ActionImprimerDetailFichier extends AbstractAction {
		public ActionImprimerDetailFichier() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le contenu du fichier de virement");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//	PaiementProxyCtrl.imprimerDetailFichierVirementBDF(PaiementCtrl.this, currentPaiement);
			PaiementProxyCtrl.imprimerDetailFichierVirement(PaiementCtrl.this, currentPaiement);
		}
	}

	public class ActionImprRetenues extends AbstractAction {
		public ActionImprRetenues() {
			super();
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(Action.NAME, "Imprimer le détail des retenues");
			putValue(Action.SHORT_DESCRIPTION, "");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			PaiementProxyCtrl.imprimerPaiementRetenues(PaiementCtrl.this, currentPaiement);
		}
	}

	/**
	 * @return Les mandats sélectionnés par l'utilisateur.
	 */
	private final NSArray mandatsAPayer() {
		final NSArray tmp = new NSArray(ZListUtil.getKeyListForValue(step2Listener.checkedMandats, Boolean.TRUE).toArray());
		return tmp;
	}

	private final NSArray mandatsADifferer() {
		final NSArray tmp = new NSArray(ZListUtil.getKeyListForValue(step2Listener.differedMandats, Boolean.TRUE).toArray());
		return tmp;
	}

	/**
	 * @return Les titres sélectionnés par l'utilisateur.
	 */
	private final NSArray titresAPayer() {
		return new NSArray(ZListUtil.getKeyListForValue(step2Listener.checkedTitres, Boolean.TRUE).toArray());
	}

	/**
	 * @return Les odps sélectionnés par l'utilisateur.
	 */
	private final NSArray odpsAPayer() {
		return new NSArray(ZListUtil.getKeyListForValue(step2Listener.checkedOdps, Boolean.TRUE).toArray());
	}

	/**
	 * @param domaine
	 * @return Les mandats visés en fonction d'un domaine de mode de paiement.
	 */
	private final NSArray getMandatsByDomainePaiement(final String domaine) {
		NSArray res;

		final EOSortOrdering sort00 = EOSortOrdering.sortOrderingWithKey(EOMandat.ATTENTE_PAIEMENT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, EOSortOrdering.CompareDescending);
		final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey("exercice.exeExercice", EOSortOrdering.CompareAscending);
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareAscending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("bordereau.borNum", EOSortOrdering.CompareAscending);
		final EOSortOrdering sort3 = EOSortOrdering.sortOrderingWithKey(EOMandat.MAN_NUMERO_KEY, EOSortOrdering.CompareAscending);

		NSMutableArray andQuals = new NSMutableArray();
		andQuals.addObject(qualForGestion);
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.comptabilite=%@", new NSArray(myComptabiliteModel.getSelectedEObject())));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("modePaiement.modDom=%@", new NSArray(domaine)));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.MAN_ETAT_KEY + "=%@", new NSArray(EOMandat.mandatVise)));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borEtat=%@", new NSArray(EOBordereau.BordereauVise)));
		EOQualifier qualMontant = new EOOrQualifier(new NSArray(new EOQualifier[] {
				new EOKeyValueQualifier(EOMandat.MAN_HT_KEY, EOQualifier.QualifierOperatorGreaterThan, BigDecimal.valueOf(0)), new EOKeyValueQualifier(EOMandat.MAN_TTC_KEY, EOQualifier.QualifierOperatorGreaterThan, BigDecimal.valueOf(0))
		}));
		andQuals.addObject(qualMontant);
		//On enleve la limitation d'exercice pour pouvoir payer des mandats de l'exercice precedent
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(myApp.appUserInfo().getCurrentExercice())));

		EOQualifier qual = new EOAndQualifier(andQuals);

		// ZLogger.debug("qualifier mandats",qual);

		res = EOsFinder.fetchArray(getEditingContext(), EOMandat.ENTITY_NAME, qual, null, true);
		res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				sort00, sort0, sort1, sort2, sort3
		}));

		return res;
	}

	/**
	 * @param domaine
	 * @return Les titres visés en fonction d'un domaine de mode de paiement.
	 */
	private final NSArray getTitresByDomainePaiement(final String domaine) {
		NSArray res;

		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareAscending);
		EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("bordereau.borNum", EOSortOrdering.CompareAscending);

		NSMutableArray andQuals = new NSMutableArray();
		andQuals.addObject(qualForGestion);
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.comptabilite=%@", new NSArray(myComptabiliteModel.getSelectedEObject())));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("modePaiement.modDom=%@", new NSArray(domaine)));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("titEtat=%@", new NSArray(EOTitre.titreVise)));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borEtat=%@", new NSArray(EOBordereau.BordereauVise)));
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(myApp.appUserInfo().getCurrentExercice())));

		EOQualifier qual = new EOAndQualifier(andQuals);
		// res = EOsFinder.fetchArray(getEditingContext(), EOTitre.ENTITY_NAME,
		// qual,null,true);

		res = EOsFinder.fetchArray(getEditingContext(), EOTitre.ENTITY_NAME, qual, null, true);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				sort1, sort2
		}));
	}

	/**
	 * @param domaine
	 * @return Les Ordres de paiements visés en fonction d'un domaine de mode de paiement et d'une comptabilité
	 */
	private final NSArray getOdpByDomainePaiement(final String domaine) {
		NSArray res;

		NSMutableArray andQuals = new NSMutableArray();
		// andQuals.addObject(qualForGestion);

		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("odpNumero", EOSortOrdering.CompareAscending);

		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("comptabilite=%@", new NSArray(myComptabiliteModel.getSelectedEObject())));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("modePaiement.modDom=%@", new NSArray(domaine)));
		andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("odpEtat=%@", new NSArray(EOOrdreDePaiement.etatValide)));
		//on ne peut pas payer des OP de l'exercice antérieur
		//        andQuals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(myApp.appUserInfo().getCurrentExercice())));

		EOQualifier qual = new EOAndQualifier(andQuals);
		res = EOsFinder.fetchArray(getEditingContext(), EOOrdreDePaiement.ENTITY_NAME, qual, new NSArray(sort1), true);

		return res;
	}

	/**
	 * @param myDialog
	 * @return
	 */
	public EOPaiement createNewPaiementInWindow(final Window dial) {
		final ZKarukeraDialog win;
		EOPaiement res = null;
		win = createModalDialog(dial);
		try {
			currentPanel.updateData();

			win.open();
			res = currentPaiement;
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	/**
	 * Vérifie d'éventuelles erreurs avant le paiement. - Dans le cas d'un virement, on vérifie si le rib est défini, s'il est cohérent avec la
	 * facture et affiche un warning si le rib n'est pas valide.
	 * 
	 * @return
	 * @throws Exception
	 */
	public final boolean verifierMandatsErreurs() throws Exception {
		NSArray mds = mandatsAPayer();
		boolean res = false;
		for (int i = 0; i < mds.count(); i++) {
			// verifier les retenues
			//Verifier RIB
			if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
				EOMandat element = (EOMandat) mds.objectAtIndex(i);
				if (element.rib() == null || element.rib().equals(NSKeyValueCoding.NullValue)) {
					throw new DefaultClientException("Coordonnées bancaires non défini pour le mandat n°" + element.manNumero());
				}
				NSArray deps = element.depenses();
				for (int j = 0; j < deps.count(); j++) {
					EODepense dep = (EODepense) deps.objectAtIndex(j);
					if (dep.rib() == null) {
						throw new DefaultClientException("Le rib est manquant pour la facture associée au mandat (n°" + element.manNumero() + ")");
					}
					// Verifier la coherence rib mandat / rib depense
					if (!dep.rib().equals(element.rib())) {
						throw new DefaultClientException("Incohérence de rib entre les factures et le mandat (n°" + element.manNumero() + ")");
					}

				}

				if (EORib.RIB_ANNULE.equals(element.rib().ribValide())) {
					if (!showConfirmationDialog("Attention", "Le Rib affecté au mandat n°" + element.manNumero() + " du " + element.gestion().gesCode() + " (" + element.rib().getRibComplet() + ") " +
							"n'est pas valide, voulez-vous quand même continuer le paiement (avec ce rib invalide) ?",
							ZMsgPanel.BTLABEL_NO)) {
						return true;
					}
				}
				try {
					if (EOTypeVirement.TVI_FORMAT_BDF.equals(getSelectedTypeVirement().tviFormat())) {
						element.rib().checkRibCorrect();
					}
					else {
						element.rib().checkRibCorrectIntl();
					}
				} catch (Exception e) {
					throw new DefaultClientException("Les coordonnées bancaires affectées au mandat n°" + element.manNumero() + " du " + element.gestion().gesCode() + " (fournisseur : " + element.fournisseur().getNomAndPrenomAndCode() + ") contiennent une erreur : " + e.getMessage()
							+ ", veuillez corriger ce problème avant de générer le paiement.");
				}
			}
		}
		return res;
	}

	public final boolean verifierOdpsErreurs() throws Exception {
		NSArray mds = odpsAPayer();
		boolean res = false;
		for (int i = 0; i < mds.count(); i++) {
			EOOrdreDePaiement element = (EOOrdreDePaiement) mds.objectAtIndex(i);
			if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
				if (element.rib() == null || element.rib().equals(NSKeyValueCoding.NullValue)) {
					throw new DefaultClientException("Rib non défini pour l'ordre de paiement n°" + element.odpNumero());
				}
				if (EORib.RIB_ANNULE.equals(element.rib().ribValide())) {
					if (!showConfirmationDialog("Attention", "Le Rib affecté à l'ordre de paiement n°" + element.odpNumero() + " (" + element.rib().getRibComplet() + ") " + " n'est pas valide dans l'annuaire, voulez-vous quand même continuer le paiement ?", ZMsgPanel.BTLABEL_NO)) {
						return true;
					}
				}
				try {
					if (EOTypeVirement.TVI_FORMAT_BDF.equals(getSelectedTypeVirement().tviFormat())) {
						element.rib().checkRibCorrect();
					}
					else {
						element.rib().checkRibCorrectIntl();
					}

				} catch (Exception e) {
					throw new DefaultClientException("Les coordonnées bancaires affectées à l'ordre de paiement n°" + element.odpNumero() + " (fournisseur : " + element.fournisseur().getNomAndPrenomAndCode() + ") contiennent une erreur : " + e.getMessage()
							+ ", veuillez corriger ce problème avant de générer le paiement.");
				}
			}
		}
		return res;
	}

	public boolean wantVerifierDoublons() {
		return true;
	}

	public static class TxtFileFilter extends javax.swing.filechooser.FileFilter {

		// private static String TYPE_UNKNOWN = "Type Unknown";
		// private static String HIDDEN_FILE = "Hidden File";

		private Hashtable filters = null;
		private String description = null;
		private String fullDescription = null;
		private boolean useExtensionsInDescription = true;

		/**
		 * Creates a file filter. If no filters are added, then all files are accepted.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter() {
			filters = new Hashtable();
		}

		/**
		 * Creates a file filter that accepts files with the given extension. Example: new ExampleFileFilter("jpg");
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension) {
			this(extension, null);
		}

		/**
		 * Creates a file filter that accepts the given file type. Example: new ExampleFileFilter("jpg", "JPEG Image Images"); Note that the "."
		 * before the extension is not needed. If provided, it will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String extension, String description) {
			this();
			if (extension != null) {
				addExtension(extension);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Creates a file filter from the given string array. Example: new ExampleFileFilter(String {"gif", "jpg"}); Note that the "." before the
		 * extension is not needed adn will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters) {
			this(filters, null);
		}

		/**
		 * Creates a file filter from the given string array and description. Example: new ExampleFileFilter(String {"gif", "jpg"}, "Gif and JPG
		 * Images"); Note that the "." before the extension is not needed and will be ignored.
		 * 
		 * @see #addExtension
		 */
		public TxtFileFilter(String[] filters, String description) {
			this();
			for (int i = 0; i < filters.length; i++) {
				// add filters one by one
				addExtension(filters[i]);
			}
			if (description != null) {
				setDescription(description);
			}
		}

		/**
		 * Return true if this file should be shown in the directory pane, false if it shouldn't. Files that begin with "." are ignored.
		 * 
		 * @see #getExtension
		 * @see FileFilter#accepts
		 */
		public boolean accept(File f) {
			if (f != null) {
				if (f.isDirectory()) {
					return true;
				}
				String extension = getExtension(f);
				if (extension != null && filters.get(getExtension(f)) != null) {
					return true;
				}
				;
			}
			return false;
		}

		/**
		 * Return the extension portion of the file's name .
		 * 
		 * @see #getExtension
		 * @see FileFilter#accept
		 */
		public String getExtension(File f) {
			if (f != null) {
				String filename = f.getName();
				int i = filename.lastIndexOf('.');
				if (i > 0 && i < filename.length() - 1) {
					return filename.substring(i + 1).toLowerCase();
				}
				;
			}
			return null;
		}

		/**
		 * Adds a filetype "dot" extension to filter against. For example: the following code will create a filter that filters out all files except
		 * those that end in ".jpg" and ".tif": ExampleFileFilter filter = new ExampleFileFilter(); filter.addExtension("jpg");
		 * filter.addExtension("tif"); Note that the "." before the extension is not needed and will be ignored.
		 */
		public void addExtension(String extension) {
			if (filters == null) {
				filters = new Hashtable(5);
			}
			filters.put(extension.toLowerCase(), this);
			fullDescription = null;
		}

		/**
		 * Returns the human readable description of this filter. For example: "JPEG and GIF Image Files (*.jpg, *.gif)"
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 * @see FileFilter#getDescription
		 */
		public String getDescription() {
			if (fullDescription == null) {
				if (description == null || isExtensionListInDescription()) {
					fullDescription = description == null ? "(" : description + " (";
					// build the description from the extension list
					Enumeration extensions = filters.keys();
					if (extensions != null) {
						fullDescription += "." + (String) extensions.nextElement();
						while (extensions.hasMoreElements()) {
							fullDescription += ", ." + (String) extensions.nextElement();
						}
					}
					fullDescription += ")";
				}
				else {
					fullDescription = description;
				}
			}
			return fullDescription;
		}

		/**
		 * Sets the human readable description of this filter. For example: filter.setDescription("Gif and JPG Images");
		 * 
		 * @see setDescription
		 * @see setExtensionListInDescription
		 * @see isExtensionListInDescription
		 */
		public void setDescription(String description) {
			this.description = description;
			fullDescription = null;
		}

		/**
		 * Determines whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description
		 * was provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see isExtensionListInDescription
		 */
		public void setExtensionListInDescription(boolean b) {
			useExtensionsInDescription = b;
			fullDescription = null;
		}

		/**
		 * Returns whether the extension list (.jpg, .gif, etc) should show up in the human readable description. Only relevent if a description was
		 * provided in the constructor or using setDescription();
		 * 
		 * @see getDescription
		 * @see setDescription
		 * @see setExtensionListInDescription
		 */
		public boolean isExtensionListInDescription() {
			return useExtensionsInDescription;
		}
	}

	/**
	 * Thread de traitement pour le paiement.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class PaiementPayerThread extends Thread {
		//        private Exception lastException;

		/**
         * 
         */
		public PaiementPayerThread() {
			super();
		}

		public void run() {
			msgFin = null;
			lastException = null;
			try {

				// faire les traitements liés au paiement (appel factory)
				final FactoryProcessPaiement factoryProcessVirement = new FactoryProcessPaiement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				NSMutableArray lesEcrituresQuiSerontGenerees = new NSMutableArray();
				NSMutableArray lesVirementsQuiSerontGenerees = new NSMutableArray();
				NSMutableArray lesOPTresorerieQuiSerontGenerees = new NSMutableArray();
				EOEmargement emargementQuiSeraGenere = null;
				leContenuFichierVirement = null;

				NSTimestamp laDateValeur = new NSTimestamp(ZDateUtil.getDateOnly(step7Listener.getDateValeur()));
				NSTimestamp laDateCreation = new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime());

				// ///////////////////////////////////////////////////////////////////////////////
				// Si on est sur un virement
				if (EOModePaiement.MODDOM_VIREMENT.equals(step1Panel.getSelectedDomaine())) {
					final EOVirementParamBdf virementParamBdf = getSelectedVirementParamBdf();
					final EOVirementParamVint virementParamVint = getSelectedVirementParamVint();
					final EOVirementParamSepa virementParamSepa = getSelectedVirementParamSepa();

					String noCompte = null;
					if (virementParamBdf == null && virementParamVint == null && virementParamSepa == null) {
						throw new DefaultClientException("Le format du virement n'est pas défini.");
					}

					factoryProcessVirement.genererVirements(getEditingContext(), mandatsAPayer(), odpsAPayer(), laDateValeur, myApp.appUserInfo().getUtilisateur(), lesEcrituresQuiSerontGenerees,
							lesVirementsQuiSerontGenerees, emargementQuiSeraGenere, lesOPTresorerieQuiSerontGenerees, step1Panel.getSelectedComptabilite(), getSelectedTypeVirement(), myApp.appUserInfo().getCurrentExercice(), comptesBE);
					currentPaiement = (EOPaiement) lesVirementsQuiSerontGenerees.objectAtIndex(0);

					if (step7Panel.unEcritureDetailCompte5()) {
						//Générer un seule écriture par compte 5
						//récupérer les comptes de paiement utilisés
						NSMutableArray pcoPaiements = new NSMutableArray();
						Enumeration<EOMandat> enumMAndats = mandatsAPayer().objectEnumerator();
						while (enumMAndats.hasMoreElements()) {
							EOMandat eoMandat = (EOMandat) enumMAndats.nextElement();
							if (pcoPaiements.indexOfObject(eoMandat.modePaiement().getPlanCoPaiementSelonExercice(getExercice())) == NSArray.NotFound) {
								pcoPaiements.addObject(eoMandat.modePaiement().getPlanCoPaiementSelonExercice(getExercice()));
							}
						}
						Enumeration<EOOrdreDePaiement> enumOdp = odpsAPayer().objectEnumerator();
						while (enumOdp.hasMoreElements()) {
							EOOrdreDePaiement eoOdp = (EOOrdreDePaiement) enumOdp.nextElement();
							if (pcoPaiements.indexOfObject(eoOdp.modePaiement().getPlanCoPaiementSelonExercice(getExercice())) == NSArray.NotFound) {
								pcoPaiements.addObject(eoOdp.modePaiement().getPlanCoPaiementSelonExercice(getExercice()));
							}
						}
						lesEcrituresQuiSerontGenerees = EOEcriture.creerMinEcritureFrom(getEditingContext(), lesEcrituresQuiSerontGenerees, pcoPaiements, EOEcritureDetail.SENS_CREDIT,
								"Paiement numero " + currentPaiement.paiNumero() + " du " + ZConst.FORMAT_DATESHORT.format(currentPaiement.paiDateCreation()),
								mandatsAPayer(), odpsAPayer(), new NSTimestamp(getDateJourneeComptable())).mutableClone();

					}

					String leContenuFichier = null;

					//générer contenu fichier
					FactoryProcessVirementFichiers factoryProcessVirementFichiers = new FactoryProcessVirementFichiers(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					if (virementParamSepa != null) {
						noCompte = virementParamSepa.vpsDftIban();
						Integer nbPaiementPourJournee = ServerCallsepa.clientSideRequestNbPaiementJour(getEditingContext(), getSelectedVirementParamSepa(), laDateCreation);
						nbPaiementPourJournee = Integer.valueOf(nbPaiementPourJournee.intValue() + 1);
						String numeroDeFichierDansLaJournee3Car = ZStringUtil.extendWithChars("" + nbPaiementPourJournee.intValue(), "0", 3, true);
						String identifiantFichierVirement = "Paiement numero " + currentPaiement.paiNumero() + " du " + ZConst.FORMAT_DATESHORT.format(currentPaiement.paiDateCreation());

						NSDictionary res = ServerCallsepa.clientSideRequestGenereVirementSepa(currentPaiement.exercice().exeExercice().toString(), currentPaiement.paiNumero().toString(), getEditingContext(), virementParamSepa, new NSTimestamp(new Date()),
								currentPaiement.getDepenses(), currentPaiement.ordreDePaiements(), currentPaiement.paiMontant(), currentPaiement.paiNbVirements(), laDateValeur, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, step7Panel.getOptionRegroupementLignes()
								);

						leContenuFichier = (String) res.valueForKey("contenu");
						Integer nbOperationsOut = (Integer) res.valueForKey("nbTransactions");
						nbOperations = nbOperationsOut.intValue();
					}
					else if (virementParamBdf != null) {
						noCompte = virementParamBdf.vpbCompteTpg();
						leContenuFichier = factoryProcessVirementFichiers.genererFichierBanqueDeFrance(getEditingContext(), currentPaiement.getDepenses(), currentPaiement.ordreDePaiements(), virementParamBdf, laDateValeur, step7Panel.grouperLignesParRib());
						nbOperations = (leContenuFichier.length() / FactoryProcessVirementFichiers.LONGUEUR_LIGNE_BDF) - 2;

					}
					if (leContenuFichier == null || leContenuFichier.length() == 0) {
						throw new DefaultClientException("Le fichier généré est vide.");
					}
					FactorySauverVirement fsv = new FactorySauverVirement(myApp.wantShowTrace());
					fsv.sauverFichier(getEditingContext(), currentPaiement, getSelectedTypeVirement(), getUtilisateur(), leContenuFichier, laDateValeur, noCompte);

					leContenuFichierVirement = leContenuFichier;

				}
				// //////////////////////////////////////////////////////////////////////////////
				// Si on est sur un paiement caisse
				else if (EOModePaiement.MODDOM_CAISSE.equals(step1Panel.getSelectedDomaine())) {
					// ZLogger.debug(step1Panel.getSelectedDomaine());
					factoryProcessVirement.genererCaisses(getEditingContext(), mandatsAPayer(), odpsAPayer(), titresAPayer(), null, null, null, laDateValeur, myApp.appUserInfo().getUtilisateur(), lesEcrituresQuiSerontGenerees, lesVirementsQuiSerontGenerees, emargementQuiSeraGenere,
							lesOPTresorerieQuiSerontGenerees, step1Panel.getSelectedComptabilite(), myApp.appUserInfo().getCurrentExercice(), comptesBE);
					currentPaiement = (EOPaiement) lesVirementsQuiSerontGenerees.objectAtIndex(0);

					if (step9Panel.unEcritureDetailCompte5()) {
						//Générer un seule écriture par compte 5
						//récupérer les comptes de paiement utilisés
						NSMutableArray pcoPaiements = new NSMutableArray();
						Enumeration<EOMandat> enumMAndats = mandatsAPayer().objectEnumerator();
						while (enumMAndats.hasMoreElements()) {
							EOMandat eoMandat = (EOMandat) enumMAndats.nextElement();
							if (pcoPaiements.indexOfObject(eoMandat.modePaiement().getPlanCoPaiementSelonExercice(getExercice())) == NSArray.NotFound) {
								pcoPaiements.addObject(eoMandat.modePaiement().getPlanCoPaiementSelonExercice(getExercice()));
							}
						}
						Enumeration<EOOrdreDePaiement> enumOdp = odpsAPayer().objectEnumerator();
						while (enumOdp.hasMoreElements()) {
							EOOrdreDePaiement eoOdp = (EOOrdreDePaiement) enumOdp.nextElement();
							if (pcoPaiements.indexOfObject(eoOdp.modePaiement().getPlanCoPaiementSelonExercice(getExercice())) == NSArray.NotFound) {
								pcoPaiements.addObject(eoOdp.modePaiement().getPlanCoPaiementSelonExercice(getExercice()));
							}
						}
						lesEcrituresQuiSerontGenerees = EOEcriture.creerMinEcritureFrom(getEditingContext(), lesEcrituresQuiSerontGenerees, pcoPaiements, EOEcritureDetail.SENS_CREDIT,
								"Paiement numero " + currentPaiement.paiNumero() + " du " + ZConst.FORMAT_DATESHORT.format(currentPaiement.paiDateCreation()),
								mandatsAPayer(), odpsAPayer(), new NSTimestamp(getDateJourneeComptable())).mutableClone();

					}

				}
				// //////////////////////////////////////////////////////////////////////////
				// //
				// Si on est sur un paiement cheque
				else if (EOModePaiement.MODDOM_CHEQUE.equals(step1Panel.getSelectedDomaine())) {
					// ZLogger.debug(step1Panel.getSelectedDomaine());
					factoryProcessVirement.genererCheques(getEditingContext(), mandatsAPayer(), odpsAPayer(), titresAPayer(), null, null, null, laDateValeur, myApp.appUserInfo().getUtilisateur(), lesEcrituresQuiSerontGenerees, lesVirementsQuiSerontGenerees, emargementQuiSeraGenere,
							lesOPTresorerieQuiSerontGenerees, step1Panel.getSelectedComptabilite(), myApp.appUserInfo().getCurrentExercice(), comptesBE);
					currentPaiement = (EOPaiement) lesVirementsQuiSerontGenerees.objectAtIndex(0);

					if (step9Panel.unEcritureDetailCompte5()) {
						//Générer un seule écriture par compte 5
						//récupérer les comptes de paiement utilisés
						NSMutableArray pcoPaiements = new NSMutableArray();
						Enumeration<EOMandat> enumMAndats = mandatsAPayer().objectEnumerator();
						while (enumMAndats.hasMoreElements()) {
							EOMandat eoMandat = (EOMandat) enumMAndats.nextElement();
							if (pcoPaiements.indexOfObject(eoMandat.modePaiement().getPlanCoPaiementSelonExercice(getExercice())) == NSArray.NotFound) {
								pcoPaiements.addObject(eoMandat.modePaiement().getPlanCoPaiementSelonExercice(getExercice()));
							}
						}
						Enumeration<EOOrdreDePaiement> enumOdp = odpsAPayer().objectEnumerator();
						while (enumOdp.hasMoreElements()) {
							EOOrdreDePaiement eoOdp = (EOOrdreDePaiement) enumOdp.nextElement();
							if (pcoPaiements.indexOfObject(eoOdp.modePaiement().getPlanCoPaiementSelonExercice(getExercice())) == NSArray.NotFound) {
								pcoPaiements.addObject(eoOdp.modePaiement().getPlanCoPaiementSelonExercice(getExercice()));
							}
						}
						lesEcrituresQuiSerontGenerees = EOEcriture.creerMinEcritureFrom(getEditingContext(), lesEcrituresQuiSerontGenerees, pcoPaiements, EOEcritureDetail.SENS_CREDIT,
								"Paiement numero " + currentPaiement.paiNumero() + " du " + ZConst.FORMAT_DATESHORT.format(currentPaiement.paiDateCreation()),
								mandatsAPayer(), odpsAPayer(), new NSTimestamp(getDateJourneeComptable())).mutableClone();

					}
				}

				else {
					throw new DefaultClientException("Format non reconnu");
				}

				if (lesVirementsQuiSerontGenerees == null || lesVirementsQuiSerontGenerees.count() == 0) {
					throw new DefaultClientException("Erreur : Aucun objet paiement renvoyé.");
				}
				currentPaiement = (EOPaiement) lesVirementsQuiSerontGenerees.objectAtIndex(0);

				// ZLogger.debug(getEditingContext());

				// Enregistrer les changements
				getEditingContext().saveChanges();
				waitingDialog.setBottomText("Paiement et écritures enregistrées");

				KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

				// numeroter les lesEcrituresQuiSerontGenerees
				NSMutableArray numEcritures = new NSMutableArray();
				FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

				// ZLogger.debug("lesEcrituresQuiSerontGenerees",lesEcrituresQuiSerontGenerees);

				if ((lesEcrituresQuiSerontGenerees != null) && (lesEcrituresQuiSerontGenerees.count() > 0)) {
					for (int i = 0; i < lesEcrituresQuiSerontGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresQuiSerontGenerees.objectAtIndex(i);
						factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
					}

					final NSArray lesEcrituresTriees = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcrituresQuiSerontGenerees, new NSArray(new EOSortOrdering("ecrNumero", EOSortOrdering.CompareAscending)));
					for (int i = 0; i < lesEcrituresTriees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresTriees.objectAtIndex(i);
						numEcritures.addObject(element.ecrNumero());
					}
				}

				///Numeroter les eventuelles retenues
				final NSMutableArray numRetenues = new NSMutableArray();
				for (int i = 0; i < mandatsAPayer().count(); i++) {
					final EOMandat mandat = (EOMandat) mandatsAPayer().objectAtIndex(i);
					for (int j = 0; j < mandat.depenses().count(); j++) {
						final EODepense depense = (EODepense) mandat.depenses().objectAtIndex(j);
						for (int k = 0; k < depense.retenues().count(); k++) {
							final EORetenue retenue = (EORetenue) depense.retenues().objectAtIndex(k);
							myKFactoryNumerotation.getNumeroEORetenue(getEditingContext(), retenue);
							numRetenues.addObject(retenue.retNumero());
						}
					}
				}

				waitingDialog.setBottomText("Numérotation effectuée");

				msgFin = "Le paiement n° " + currentPaiement.paiNumero().intValue() + " a été généré.\n";

				if (numRetenues.count() > 0) {
					if (numRetenues.count() > 1) {
						msgFin += "Les retenues n° " + numRetenues.componentsJoinedByString(",") + " ont été générées.\n";
					}
					else {
						msgFin += "La retenue n° " + numRetenues.componentsJoinedByString(",") + " a été générée.\n";
					}
				}

				if (numEcritures.count() > 1) {
					msgFin += "Les écritures n° " + numEcritures.componentsJoinedByString(",") + " ont été générées.\n";
				}
				else {
					msgFin += "L'écriture n° " + numEcritures.componentsJoinedByString(",") + " a été générée.\n";
				}

				System.out.println(msgFin);

				if ((lesVirementsQuiSerontGenerees != null) && (lesVirementsQuiSerontGenerees.count() > 0)) {
					PaiementProxyCtrl.genererEmargements(waitingDialog, (EOPaiement) lesVirementsQuiSerontGenerees.objectAtIndex(0));
				}

				if (lesEcrituresQuiSerontGenerees != null) {
					// On invalide tous les objets de l'éditingcontext pour être
					// sûr que les écritures details seront à jour
					waitingDialog.setBottomText("Mise à jour des données...");
					ZLogger.debug("Avant invalidate");

					final NSMutableArray lesEcr = new NSMutableArray();
					for (int i = 0; i < lesEcrituresQuiSerontGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) lesEcrituresQuiSerontGenerees.objectAtIndex(i);
						final NSArray emarges = EOsFinder.getEcrituresEmargees(getEditingContext(), element);
						for (int j = 0; j < emarges.count(); j++) {
							final EOEcriture ecritureEmargee = (EOEcriture) emarges.objectAtIndex(j);
							final NSArray details = ecritureEmargee.detailEcriture();
							for (int k = 0; k < details.count(); k++) {
								final EOEcritureDetail detail = (EOEcritureDetail) details.objectAtIndex(k);
								if (lesEcr.indexOfObject(detail) == NSArray.NotFound) {
									lesEcr.addObject(detail);
								}
							}
						}
					}

					ZLogger.debug("Invalidate sur " + lesEcr.count() + " objets...");
					waitingDialog.getMyProgressBar().setMaximum(lesEcr.count());
					waitingDialog.getMyProgressBar().setIndeterminate(false);
					for (int i = 0; i < lesEcr.count(); i++) {
						//                         getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(lesEcr.objectAtIndex(i)));
						getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
								getEditingContext().globalIDForObject((EOEnterpriseObject) lesEcr.objectAtIndex(i))
						}));
						waitingDialog.getMyProgressBar().setValue(i);
						Thread.yield();
					}
					waitingDialog.getMyProgressBar().setIndeterminate(true);

					//                    getEditingContext().invalidateObjectsWithGlobalIDs(lesEcr)(ZEOUtilities.globalIDsForObjects(getEditingContext(), lesEcr));
					//getEditingContext().invalidateAllObjects();
					ZLogger.debug("Invalidate effectue");
				}

				// waitingDialog.setVisible(false);

			} catch (Exception e) {
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		//non gere en auto
		return null;
	}

	public String title() {
		return TITLE;
	}

}
