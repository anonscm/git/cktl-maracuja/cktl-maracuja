/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.factory.process.FactoryProcessRetenue;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.maracuja.client.metier.EOTypeRetenue;
import org.cocktail.maracuja.client.paiement.ui.RetenueAjoutPanel;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class RetenueAjoutCtrl extends CommonCtrl {
	protected static final String TITLE = "Création d'une retenue";
	protected static final Dimension WINDOW_DIMENSION = new Dimension(800, 300);

	private ActionClose actionClose = new ActionClose();
	private ActionValider actionValider = new ActionValider();

	final HashMap dicoSaisie = new HashMap();
	protected RetenueAjoutPanel saisiePanel;
	protected ZEOComboBoxModel myTypeRetenueComboBoxModel;
	private EODepense _depense;

	public RetenueAjoutCtrl(EOEditingContext ec, EODepense depense) throws Exception {
		super(ec);

		_depense = depense;
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeRetenue.TYPE_ETAT_KEY + "=%@", new NSArray(new Object[] {
			ZFinderEtats.etat_VALIDE()
		}));
		final NSArray lesTypeRetenues = EOsFinder.fetchArray(ec, EOTypeRetenue.ENTITY_NAME, qual, new NSArray(new Object[] {
			EOSortOrdering.sortOrderingWithKey(EOTypeRetenue.TRE_LIBELLE_KEY, EOSortOrdering.CompareAscending)
		}), false);

		if (lesTypeRetenues.count() == 0) {
			throw new DefaultClientException("Aucun type de retenues trouvé, veuillez en saisir au moins un dans Administration/Gestion des types de retenue.");
		}

		myTypeRetenueComboBoxModel = new ZEOComboBoxModel(lesTypeRetenues, EOTypeRetenue.TRE_LIBELLE_KEY, null, null);
		saisiePanel = new RetenueAjoutPanel(new SaisiePanelListener());
	}

	protected void checkDicoSaisie() throws Exception {
		if (dicoSaisie.get(EORetenue.RET_MONTANT_KEY) == null) {
			throw new DataCheckException("Le montant est obligatoire");
		}
		if (dicoSaisie.get(EORetenue.TYPE_RETENUE_KEY) == null) {
			throw new DataCheckException("Le type de retenue est obligatoire");
		}
		if (dicoSaisie.get(EORetenue.RET_LIBELLE_KEY) == null) {
			throw new DataCheckException("Le libellé est obligatoire");
		}

		if (((BigDecimal) dicoSaisie.get(EORetenue.RET_MONTANT_KEY)).compareTo(ZConst.BIGDECIMAL_ZERO) <= 0) {
			throw new DefaultClientException("Le montant de la retenue doit être supérieur à 0.");
		}

		_depense.majMontantDisquette();
		BigDecimal totalRetenues = _depense.montantRetenues().add((BigDecimal) dicoSaisie.get(EORetenue.RET_MONTANT_KEY));
		if (totalRetenues.compareTo(_depense.depTtc()) > 0) {
			throw new DefaultClientException("Le montant total des retenues (" + ZConst.FORMAT_DECIMAL.format(totalRetenues) + ") ne peut être supérieur au montant de la dépense (" + ZConst.FORMAT_DECIMAL.format(_depense.depTtc()) + ")");
		}

	}

	protected final void annulerSaisie() {
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	protected void validerSaisie() {
		try {
			if (dicoSaisie.get(EORetenue.RET_MONTANT_KEY) != null && dicoSaisie.get(EORetenue.RET_MONTANT_KEY) instanceof Number) {
				dicoSaisie.put(EORetenue.RET_MONTANT_KEY, new BigDecimal(((Number) dicoSaisie.get(EORetenue.RET_MONTANT_KEY)).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			dicoSaisie.put(EORetenue.TYPE_RETENUE_KEY, myTypeRetenueComboBoxModel.getSelectedEObject());
			checkDicoSaisie();

			final FactoryProcessRetenue factoryProcessRetenue = new FactoryProcessRetenue(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
			final EORetenue retenue = factoryProcessRetenue.creerRetenue(getEditingContext(),
					(String) dicoSaisie.get(EORetenue.RET_LIBELLE_KEY),
					(BigDecimal) dicoSaisie.get(EORetenue.RET_MONTANT_KEY),
					(EOTypeRetenue) dicoSaisie.get(EORetenue.TYPE_RETENUE_KEY),
					_depense,
					getComptabilite(),
					getExercice(),
					getUtilisateur());

			getMyDialog().onOkClick();
		} catch (Exception e) {

			showErrorDialog(e);
		}
	}

	//    protected final ZKarukeraDialog createModalDialog(final Window dial ) {
	//        ZKarukeraDialog win;
	//        if (dial instanceof Dialog) {
	//            win = new ZKarukeraDialog((Dialog)dial, getTitle(),true);
	//        }
	//        else {
	//            win = new ZKarukeraDialog((Frame)dial, getTitle(),true);
	//        }        
	//        setMyDialog(win);
	//        saisiePanel.initGUI();
	//        saisiePanel.setPreferredSize(getWindowSize());
	//        win.setContentPane(saisiePanel);
	//        win.pack();
	//        return win;
	//    }            
	//        
	//    protected String getTitle() {
	//        return TITLE;
	//    }
	//    
	//    protected Dimension getWindowSize() {
	//        return WINDOW_DIMENSION;
	//    }

	// 
	//    public Map openDialogNew(final Window dial, final BigDecimal montant) {
	//        final ZKarukeraDialog win = createDialog(dial);
	//        Map res=null;
	//        try {
	//            dicoSaisie.put("montant", ZConst.BIGDECIMAL_ZERO);
	//            if (montant != null) {
	//                dicoSaisie.put("montant", montant);    
	//            }
	//            saisiePanel.updateData();
	//            if (win.open()==ZKarukeraDialog.MROK) {
	//                res = dicoSaisie;
	//            }
	//        }
	//        catch (Exception e) {
	//            showErrorDialog(e);
	//        }
	//        finally  {
	//            win.dispose();
	//        }
	//        return res;
	//    }        
	//    

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final class SaisiePanelListener implements RetenueAjoutPanel.IBrouillardAjoutPanelListener {

		public Action actionValider() {
			return actionValider;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Map getFilters() {
			return dicoSaisie;
		}

		public ZEOComboBoxModel getTypeRetenueModel() {
			return myTypeRetenueComboBoxModel;
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return saisiePanel;
	}

	public String title() {
		return TITLE;
	}

}
