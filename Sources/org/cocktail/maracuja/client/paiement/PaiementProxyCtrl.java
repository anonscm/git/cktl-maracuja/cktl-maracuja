/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.paiement;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPaiement;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.metier.EOVirementFichier;
import org.cocktail.maracuja.client.metier.EOVirementParamBdf;
import org.cocktail.maracuja.client.metier.IVirementParam;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Fourni des méthodes partagées par les différentes interfaces qui gèrent les paiements.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PaiementProxyCtrl {

	public PaiementProxyCtrl() {
		super();
	}

	/**
	 * @param ec
	 * @param paiement
	 * @return Le virementParamBDF VALIDE associé au typeVirement du paiement (impossible de récupérer le param utilisé à l'époque du paiement)
	 */
	public static EOVirementParamBdf getViremenParamBDFForPaiement(EOEditingContext ec, final EOPaiement paiement) {
		final EOTypeVirement tv = paiement.typeVirement();
		final EOVirementParamBdf virementParamBdf = (EOVirementParamBdf) EOsFinder.fetchObject(ec, EOVirementParamBdf.ENTITY_NAME, "toTypeVirement=%@ and vpbEtat=%@", new NSArray(new Object[] {
				tv, EOVirementParamBdf.ETAT_VALIDE
		}), null, false);

		return virementParamBdf;
	}

	public static final void imprimerBordereau(final CommonCtrl ctrl, int nbOperations, final EOPaiement currentPaiement, final IVirementParam virementParam) {
		if (EOTypeVirement.TVI_FORMAT_BDF.equals(currentPaiement.typeVirement().tviFormat())) {
			imprimerBordereauBDF(ctrl, nbOperations, currentPaiement, (EOVirementParamBdf) virementParam);
		}
		else if (EOTypeVirement.TVI_FORMAT_SEPASCT.equals(currentPaiement.typeVirement().tviFormat())) {
			imprimerBordereauSepaSCT(ctrl, currentPaiement);
		}
	}

	/**
	 * Imprime et renvoie le chemin d'accès du bordereau d'accompagnement pour les virements BDF
	 * 
	 * @param ec
	 * @param dico
	 * @param nbOperations
	 * @param currentPaiement
	 * @param virementParamBdf
	 * @param directory
	 * @return
	 * @throws Exception
	 */
	private static final void imprimerBordereauBDF(final CommonCtrl ctrl, int nbOperations, final EOPaiement currentPaiement, final EOVirementParamBdf virementParamBdf) {
		final ApplicationClient myApp = ctrl.getMyApp();

		try {

			//Récupérer le dernier virementFichierGénéré
			final NSArray sorts = new NSArray(new Object[] {
					EOSortOrdering.sortOrderingWithKey("virDateCreation", EOSortOrdering.CompareDescending), EOSortOrdering.sortOrderingWithKey("virOrdre", EOSortOrdering.CompareDescending)
			});

			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("paiement=%@", new NSArray(currentPaiement));
			final EOFetchSpecification spec = new EOFetchSpecification(EOVirementFichier.ENTITY_NAME, qual, sorts, false, false, null);
			spec.setRefreshesRefetchedObjects(true);
			final NSArray virementFichiers = ctrl.getEditingContext().objectsWithFetchSpecification(spec);

			if (virementFichiers.count() == 0) {
				throw new DefaultClientException("Impossible de recuperer l'enregistrement correspondant au virement");
			}

			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey(dico.valueForKey("DEVISE_LIBELLE").toString().toUpperCase(), "MONNAIE");
			dico.takeValueForKey(new Integer(nbOperations), "NBRE_OP");
			dico.takeValueForKey(virementParamBdf.vpbB1(), "CODE_OP");
			dico.takeValueForKey(virementParamBdf.vpbC41(), "ID_TPG");
			dico.takeValueForKey(virementParamBdf.vpbNomRemettant(), "UNIV_TPG_REMETTANT");
			dico.takeValueForKey(virementParamBdf.vpbNomTpg(), "NOM_TPG");
			dico.takeValueForKey(virementParamBdf.vpbCompteTpg(), "COMPTE_TPG");
			dico.takeValueForKey(ZConst.FORMAT_DATESHORT.format(((EOVirementFichier) virementFichiers.objectAtIndex(0)).virDateValeur()), "DATE_REG");
			dico.takeValueForKey(currentPaiement.paiMontant(), "MONTANT_CUMUL");
			dico.takeValueForKey(currentPaiement.paiNumero().toString(), "DISK_NUM");


			String filePath = ReportFactoryClient.imprimerBordereauDisquette(myApp.editingContext(), myApp.temporaryDir, dico, null);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}

	}

	private static final void imprimerBordereauSepaSCT(final CommonCtrl ctrl, final EOPaiement currentPaiement) {
		final ApplicationClient myApp = ctrl.getMyApp();
		try {
			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(myApp.editingContext(), currentPaiement).valueForKey(EOPaiement.PAI_ORDRE_KEY), EOPaiement.PAI_ORDRE_COLKEY.toUpperCase());
			//dico.takeValueForKey(ServerProxy.clientSideRequestGetWsResourceUrl(myApp.editingContext(), "logo_sepa.jpg"), "LOGO_SEPA_URL");
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String tz = ServerProxy.clientSideRequestGetGrhumParam(myApp.editingContext(), "GRHUM_TIME_ZONE");
			if (tz == null || tz.length() == 0) {
				tz = "Europe/Paris";
			}
			sdf.setTimeZone(TimeZone.getTimeZone(tz));
			String s = sdf.format(new Date());
			dico.takeValueForKey(s, "DATE_EXECUTION");

			String filePath = ReportFactoryClient.imprimerBordereauSepaSct(myApp.editingContext(), myApp.temporaryDir, dico, null);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

	/**
	 * Imprime le contenu du paiement.
	 */
	public static final void imprimerContenuPaiement(final CommonCtrl ctrl, final EOPaiement currentPaiement) {
		final ApplicationClient myApp = ctrl.getMyApp();
		try {
			String filePath = ReportFactoryClient.imprimerContenuPaiement(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(currentPaiement), ctrl.getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

	public static final void imprimerDetailFichierVirement(final CommonCtrl ctrl, final EOPaiement currentPaiement) {
		//recuperer le dernier fichier généré
		EOVirementFichier fichier = EOVirementFichier.fetchFirstByQualifier(ctrl.getEditingContext(), new EOKeyValueQualifier(EOVirementFichier.PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, currentPaiement), new NSArray(new Object[] {
				EOVirementFichier.SORT_DATE_CREATION_DESC
		}));

		if (currentPaiement.typeVirement().isBdf()) {
			imprimerDetailFichierVirementBDF(ctrl, currentPaiement);
		}
		else if (currentPaiement.typeVirement().isSepaSct()) {
			imprimerDetailFichierVirementSEPA(ctrl, currentPaiement);
		}

	}

	public static final void imprimerDetailFichierVirementBDF(final CommonCtrl ctrl, final EOPaiement currentPaiement) {
		final ApplicationClient myApp = ctrl.getMyApp();
		try {
			String filePath = ReportFactoryClient.imprimerDetailFichierVirementBDF(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(currentPaiement), ctrl.getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

	public static final void imprimerDetailFichierVirementSEPA(final CommonCtrl ctrl, final EOPaiement currentPaiement) {
		final ApplicationClient myApp = ctrl.getMyApp();
		try {
			String filePath = ReportFactoryClient.imprimerDetailFichierVirementSEPA(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(currentPaiement), ctrl.getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

	/**
	 * Imprime les retenues associées aux paiement.
	 * 
	 * @param ctrl
	 * @param currentPaiement
	 */
	public static final void imprimerPaiementRetenues(final CommonCtrl ctrl, final EOPaiement currentPaiement) {
		final ApplicationClient myApp = ctrl.getMyApp();
		try {
			//Verifier s'il y a des retenues
			if (currentPaiement.montantRetenues().compareTo(ZConst.BIGDECIMAL_ZERO) <= 0) {
				throw new DefaultClientException("Aucune retenue générée lors de ce paiement.");
			}
			Integer paiOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(ctrl.getEditingContext(), currentPaiement).valueForKey(EOPaiement.PAI_ORDRE_KEY);

			NSMutableDictionary dic = myApp.getParametres().mutableClone();
			dic.takeValueForKey(ctrl.getExercice().exeExercice().toString(), "EXERCICE");
			dic.takeValueForKey(paiOrdre, "PAI_ORDRE");

			String filePath = ReportFactoryClient.imprimerPaiementRetenues(myApp.editingContext(), myApp.temporaryDir, dic);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			ctrl.showErrorDialog(e1);
		}
	}

	/**
	 * Numerote un paiement qui est à 0.
	 * 
	 * @param dateJourneeComptable
	 * @param paiement
	 * @param showTrace
	 * @throws Exception
	 */
	public static Integer numeroterPaiement(Date dateJourneeComptable, EOPaiement paiement, boolean showTrace) throws Exception {
		if (!(paiement.paiNumero() == null || paiement.paiNumero().intValue() == 0)) {
			throw new Exception("Le paiement est deja numerote (" + paiement.paiNumero().intValue() + ")");
		}

		EOEditingContext ec = paiement.editingContext();
		final FactoryProcessPaiement factoryProcessVirement = new FactoryProcessPaiement(showTrace, new NSTimestamp(dateJourneeComptable));
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(showTrace);

		factoryProcessVirement.numeroterVirement(ec, paiement, myKFactoryNumerotation);
		return paiement.paiNumero();

	}

	/**
	 * Genere les emargements relies a un paiment (appel procedure stockee).
	 * 
	 * @param dateJourneeComptable
	 * @param paiement
	 * @param showTrace
	 * @throws Exception
	 */
	public static void genererEmargements(ZWaitingPanelDialog waitingDialog, EOPaiement paiement) throws Exception {
		//generation emargements
		final NSDictionary pdic = ServerProxy.serverPrimaryKeyForObject(paiement.editingContext(), paiement);
		final Object pordre = pdic.valueForKey(EOPaiement.PAI_ORDRE_KEY);
		try {
			ZLogger.debug("Création des émargements");
			if (waitingDialog != null) {
				waitingDialog.setBottomText("Creation des emargements");
				waitingDialog.getMyProgressBar().setIndeterminate(true);
			}
			ServerProxy.clientSideRequestAfaireApresTraitementPaiement(paiement.editingContext(), paiement);
			ZLogger.debug("Emargements crees");
		} catch (Exception e) {
			throw new DefaultClientException("Le paiement a bien été généré mais il y a eu une erreur lors de la génération des émargements automatiques pour pai_ordre= " + pordre + ". Vous devez transmettre ce message à un informaticien pour qu'il génère les émargements. : "
					+ e.getMessage());
		}

	}

	public static void invaliderEcritureDetailsRelies(ZWaitingPanelDialog waitingDialog, EOPaiement paiement) {
		NSArray ecds = paiement.getEcritureDetails();
		if (ecds != null) {
			// On invalide tous les objets de l'éditingcontext pour être
			// sûr que les écritures details seront à jour
			ZLogger.debug("Avant invalidate");
			final NSMutableArray lesEcr = new NSMutableArray();
			for (int i = 0; i < ecds.count(); i++) {
				NSArray res = EOsFinder.getEcritureDetailsEmargees(paiement.editingContext(), (EOEcritureDetail) ecds.objectAtIndex(i));
				for (int j = 0; j < res.count(); j++) {
					EOEcritureDetail ecd = (EOEcritureDetail) res.objectAtIndex(j);
					if (lesEcr.indexOfObject(ecd) == NSArray.NotFound) {
						lesEcr.addObject(ecd);
					}
				}
			}

			ZLogger.debug("Invalidate sur " + lesEcr.count() + " objets...");
			if (waitingDialog != null) {
				waitingDialog.setBottomText("Synchronisation...");
				waitingDialog.getMyProgressBar().setMaximum(lesEcr.count());
				waitingDialog.getMyProgressBar().setIndeterminate(false);
			}
			for (int i = 0; i < lesEcr.count(); i++) {
				paiement.editingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
						paiement.editingContext().globalIDForObject((EOEnterpriseObject) lesEcr.objectAtIndex(i))
				}));
				if (waitingDialog != null) {
					waitingDialog.getMyProgressBar().setValue(i);
				}
				Thread.yield();
			}
			if (waitingDialog != null) {
				waitingDialog.getMyProgressBar().setIndeterminate(true);
			}
			ZLogger.debug("Invalidate effectue");
		}
	}
}
