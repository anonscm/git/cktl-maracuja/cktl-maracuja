/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;


public class CreationEcrSolde67Panel extends ZKarukeraPanel {
    private static String COMMENT = "<html>Option et libellé pour la création de l'écriture des soldes des classes 6 et 7.</html>";
    

    public final static String OPTION_COMPOSANTE = "Résultat par composante"; 
    public final static String OPTION_ETABLISSEMENT = "Résultat par établissement/SACD"; 
    

    
    
    private final ZCommentPanel commentPanel;
    
    private final JRadioButton button1 = new JRadioButton(OPTION_COMPOSANTE);
    private final JRadioButton button2 = new JRadioButton(OPTION_ETABLISSEMENT);
    private final ButtonGroup group = new ButtonGroup();
    
    
    
//   private final BtInfoPanel btInfoPanel;
    
    
    private final ICreationEcrSolde67PanelListener _listener;

    private ZFormPanel libelle;
    
    public CreationEcrSolde67Panel(final ICreationEcrSolde67PanelListener listener) {
        commentPanel = new ZCommentPanel("Solde des comptes de classe 6 et 7", COMMENT, null, Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.WEST);
//        btInfoPanel = new BtInfoPanel();
        _listener = listener;
        
        group.add(button1);
        group.add(button2);
        
        ButtonOptionListener buttonOptionListener = new ButtonOptionListener();
        
        button1.addActionListener(buttonOptionListener);
        button2.addActionListener(buttonOptionListener);
    }

    
    
    
    
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        add(commentPanel, BorderLayout.NORTH);
        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(buildContentPanel(), BorderLayout.NORTH);
        p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        add(p, BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);
    }

    private final Component buildContentPanel() {
        libelle = ZFormPanel.buildLabelField("Libellé écriture", new ZTextField.DefaultTextFieldModel(_listener.getFilters(), "ecrLibelle") );
        ((ZTextField)libelle.getMyFields().get(0)).getMyTexfield().setColumns(50);
        
        
        final Box box = Box.createVerticalBox();
        box.add(button1);
        box.add(button2);
        box.setBorder(BorderFactory.createTitledBorder("Option"));
        
        final ArrayList comps = new ArrayList();
        comps.add(libelle);
        comps.add(box);
        
        return ZKarukeraPanel.buildVerticalPanelOfComponents(comps);
    }
    
    
    private final JPanel buildBottomPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(10, 10, 15, 10));
        final ArrayList list = new ArrayList();
        list.add(_listener.actionOk());
        list.add(_listener.actionClose());

        tmp.add(ZKarukeraPanel.buildHorizontalPanelOfComponent(ZKarukeraPanel.getButtonListFromActionList(list)), BorderLayout.EAST);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }
    

    public void updateData() throws Exception {
       libelle.updateData();
       if (OPTION_COMPOSANTE.equals(_listener.getFilters().get("rtatComp"))) {
           button1.setSelected(true);
       }
       else {
           button2.setSelected(true);
       }

    }

    
    private final class ButtonOptionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
//            if (button1.isSelected()) {
//                _listener.getFilters().put("rtatComp", OPTION_COMPOSANTE);
//            }
//            else {
//                _listener.getFilters().put("rtatComp", OPTION_ETABLISSEMENT);
//            }
//            
        }
            
    }
    
    public static interface ICreationEcrSolde67PanelListener {
        public static final String ZDESCRIPTION_KEY="ZDESCRIPTION";
        public Action actionClose();
        public Map getFilters();
        public Action actionOk();
        
    }
    
    
    public String getOptionSelected() {
        if (button1.isSelected()  ) {
            return OPTION_COMPOSANTE;
        }
        return OPTION_ETABLISSEMENT;
    }
    
}
