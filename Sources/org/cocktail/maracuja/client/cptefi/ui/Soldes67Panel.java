/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;


public class Soldes67Panel extends ZKarukeraPanel {
	private static String COMMENT = "<html>Vous pouvez ici créer automatiquement une écriture pour solder les comptes des classes 6 et 7.<br> Une fois générée, "
			+ "vous pouvez vérifier l'écriture, calculer le bilan, le compte de résultat et les autres documents et revenir la supprimer (pour la regénérer une autre fois) ou la <b>valider</b>.<br><b>Une fois validée, vous ne pourrez plus annuler cette écriture.</html>";

	private final JLabel labelEtat = new JLabel("");

	private final ZCommentPanel commentPanel;
	//   private final BtInfoPanel btInfoPanel;

	private final ISoldes67PanelListener _listener;

	public Soldes67Panel(final ISoldes67PanelListener listener) {
		commentPanel = new ZCommentPanel("Solde des comptes de classe 6 et 7", COMMENT, null, Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.WEST);
		//        btInfoPanel = new BtInfoPanel();
		_listener = listener;
	}

	public void initGUI() {
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		add(commentPanel, BorderLayout.NORTH);

		final JPanel p = new JPanel(new BorderLayout());
		p.add(buildEtapes(), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		add(p, BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);
	}

	private final Component buildEtapes() {
		final ArrayList lesBoutonsExt = new ArrayList();
		int i = 0;
		final Iterator iter = _listener.etapeActions().values().iterator();
		while (iter.hasNext()) {
			i++;
			final Action element = (Action) iter.next();
			final JButton bt = new JButton(element);
			//mise en forme
			final JPanel ligne = new JPanel(new BorderLayout());
			final JPanel btPanel = new JPanel(new BorderLayout());
			btPanel.add(bt, BorderLayout.CENTER);
			btPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
			ligne.add(btPanel, BorderLayout.CENTER);
			//             ligne.add(ZTooltip.createTooltipLabel((String) element.getValue(Action.NAME), (String)element.getValue(ISoldes67PanelListener.ZDESCRIPTION_KEY)), BorderLayout.EAST);
			lesBoutonsExt.add(ligne);
		}

		lesBoutonsExt.add(0, labelEtat);
		labelEtat.setHorizontalAlignment(SwingConstants.CENTER);

		final JPanel panel = ZKarukeraPanel.buildVerticalPanelOfComponents(lesBoutonsExt);

		panel.setBorder(BorderFactory.createEmptyBorder(20, 20, 2, 20));

		return panel;

	}

	private final JPanel buildBottomPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(10, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(_listener.actionClose());

		tmp.add(ZKarukeraPanel.buildLine(ZKarukeraPanel.getButtonListFromActionList(list)), BorderLayout.EAST);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	public void updateData() throws Exception {
		labelEtat.setText(_listener.getEtatEcritureSolde());

	}

	public static interface ISoldes67PanelListener {
		public static final String ZDESCRIPTION_KEY = "ZDESCRIPTION";

		public Action actionClose();

		public Map etapeActions();

		public String getEtatEcritureSolde();

	}

}
