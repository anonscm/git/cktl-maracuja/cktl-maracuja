/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;


public class CompteFiPanel extends ZKarukeraPanel {
	private static String COMMENT = "<html>Cet écran présente les étapes prises en charge par maracuja pour la génération du compte financier.</html>";
	private static String COLOR_NUM_STR = "#D27715";
	private static String COLOR_TITLE_STR = "#FAEA73";
	private static String COLOR_COMMENT_STR = "#D27715";
	private static String BGCOLOR_NUM_STR = "#FAEA73";
	private static String BGCOLOR_TITLE_STR = "#D28028";
	private static String BGCOLOR_COMMENT_STR = "#FAEA73";
	private static Color COLOR_NUM = Color.decode(COLOR_NUM_STR);
	private static Color BGCOLOR_NUM = Color.decode(BGCOLOR_NUM_STR);
	private static Dimension SIZE_NUM = new Dimension(40, 26);

	private final ZCommentPanel commentPanel;
	private final BtInfoPanel btInfoPanel;

	private final ICompteFiPanelListener _listener;

	public CompteFiPanel(final ICompteFiPanelListener listener) {
		commentPanel = new ZCommentPanel("Génération du compte financier", COMMENT, null, Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.WEST);
		btInfoPanel = new BtInfoPanel();
		_listener = listener;
	}

	public void initGUI() {
		setLayout(new BorderLayout());
		//setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		add(commentPanel, BorderLayout.NORTH);

		final JPanel p = new JPanel(new BorderLayout());
		p.add(buildEtapes(), BorderLayout.NORTH);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		//        p.add(buildInfoPanel(), BorderLayout.CENTER);

		add(p, BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);
	}

	//	private final JPanel buildInfoPanel() {
	//		final JPanel panel = new JPanel(new BorderLayout());
	//		panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	//		panel.add(btInfoPanel, BorderLayout.CENTER);
	//		return panel;
	//	}

	private final Component buildEtapes() {
		final Font fontNum = getFont().deriveFont((float) 20);
		final ArrayList lesBoutonsExt = new ArrayList();

		int i = 0;

		final Iterator iter = _listener.etapeActions().values().iterator();
		while (iter.hasNext()) {
			i++;
			final Action element = (Action) iter.next();
			final JButton bt = new JButton(element);
			final JLabel num = new JLabel("" + i);
			num.setPreferredSize(SIZE_NUM);
			num.setHorizontalAlignment(SwingConstants.CENTER);
			num.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 5));
			num.setFont(fontNum);
			num.setForeground(COLOR_NUM);
			num.setBackground(BGCOLOR_NUM);
			num.setOpaque(true);
			//mise en forme
			final JPanel ligne = new JPanel(new BorderLayout());
			ligne.add(num, BorderLayout.WEST);
			final JPanel btPanel = new JPanel(new BorderLayout());
			btPanel.add(bt, BorderLayout.CENTER);
			btPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
			ligne.add(btPanel, BorderLayout.CENTER);
			//             ligne.add(ZTooltip.createTooltipLabel((String) element.getValue(Action.NAME), (String)element.getValue(ICompteFiPanelListener.ZDESCRIPTION_KEY)), BorderLayout.EAST);
			lesBoutonsExt.add(ligne);
			//infos
			btInfoPanel.registerForComponent(buildHtmlForComment(i, (String) element.getValue(Action.NAME), (String) element.getValue(ICompteFiPanelListener.ZDESCRIPTION_KEY)), bt);
		}

		int moitie = (_listener.etapeActions().size() % 2 == 0 ? _listener.etapeActions().size() : _listener.etapeActions().size() + 1) / 2;

		final GridLayout gridLayout = new GridLayout(moitie, 1);
		gridLayout.setHgap(50);
		gridLayout.setVgap(10);

		final GridLayout gridLayout2 = new GridLayout(moitie, 1);
		gridLayout2.setHgap(50);
		gridLayout2.setVgap(10);

		final JPanel panel1 = new JPanel();
		panel1.setLayout(gridLayout);
		panel1.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 50));

		final JPanel panel2 = new JPanel();
		panel2.setLayout(gridLayout2);
		panel2.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 50));

		for (int j = 0; j < moitie; j++) {
			final Component element = (Component) lesBoutonsExt.get(j);
			panel1.add(element);
		}

		for (int j = moitie; j < _listener.etapeActions().size(); j++) {
			final Component element = (Component) lesBoutonsExt.get(j);
			panel2.add(element);
		}

		btInfoPanel.registerForComponent(null, panel1);
		btInfoPanel.registerForComponent(null, panel2);
		btInfoPanel.registerForComponent(null, this);

		final Box b = Box.createHorizontalBox();
		b.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		b.add(panel1);
		b.add(panel2);
		return b;

	}

	private final String buildHtmlForComment(final int num, final String title, final String comment) {
		final StringBuffer str = new StringBuffer();
		str.append("<html><table width=\"685\" style=\"background-color: " + BGCOLOR_COMMENT_STR + "\"  ><tr><td width=\"50\"  style=\"font-weight: bold; text-align: center; color:" + COLOR_NUM_STR + "; font-size: 16px;background-color: " + BGCOLOR_NUM_STR + "\"> " + num + "</td>");
		str.append("<td width=\"635\" style=\"font-weight: bold; padding-left:4px; color:" + COLOR_TITLE_STR + "; font-size: 12px;background-color: " + BGCOLOR_TITLE_STR + "\">");
		if (title != null) {
			str.append(title.replaceAll("&", "&amp;"));
		}
		str.append("</td></tr>");
		str.append("<tr><td colspan=2 style=\"font-size: 10px; padding-left:40px; padding-top: 5px; padding-right: 5px; padding-bottom: 5px; color=" + COLOR_COMMENT_STR + ";\">");
		str.append(comment);
		str.append("</td></tr></table></html>");

		return str.toString();
	}

	private final JPanel buildBottomPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(10, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(_listener.actionClose());

		tmp.add(ZKarukeraPanel.buildLine(ZKarukeraPanel.getButtonListFromActionList(list)), BorderLayout.EAST);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	public void updateData() throws Exception {

	}

	public static interface ICompteFiPanelListener {
		public static final String ZDESCRIPTION_KEY = "ZDESCRIPTION";

		public Action actionClose();

		public Map etapeActions();
	}

	//    public static abstract class EtapeAction extends AbstractAction {
	//        
	//        
	//    }

	private final static class BtInfoPanel extends JPanel {
		private final JPanel contentPanel;
		private final JLabel _contentLabel;

		public BtInfoPanel() {
			super(new BorderLayout());
			_contentLabel = new JLabel();

			contentPanel = new JPanel(new BorderLayout());
			contentPanel.add(_contentLabel, BorderLayout.CENTER);

			add(contentPanel, BorderLayout.NORTH);
			add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		}

		public final void registerForComponent(final String comment, final Component comp) {
			comp.addMouseMotionListener(new MyMouseMotionListener(comment));
		}

		//		public final void setText(final String text) {
		//			_contentLabel.setText(text);
		//		}

		private final class MyMouseMotionListener extends MouseMotionAdapter {
			private final String _text;

			public MyMouseMotionListener(final String text) {
				super();
				_text = text;
			}

			public void mouseMoved(MouseEvent e) {
				super.mouseMoved(e);

				_contentLabel.setText(_text);
			}

		}

	}

}
