/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ui;

import javax.swing.SwingConstants;
import javax.swing.table.TableCellEditor;

import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EOBilanPoste;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BilanPosteListPanel extends ZKarukeraTablePanel {
	private IBilanPosteListPanelListener myListener;
	public String name;

	/**
	 * @param editingContext
	 */
	public BilanPosteListPanel(String name, IBilanPosteListPanelListener listener) {
		super(listener, false);
		this.name = name;
		myListener = listener;

		final ZEOTableModelColumn col0 = new ZEOTableModelColumn(myDisplayGroup, EOBilanPoste.BP_LIBELLE_KEY, "Poste", 300);
		col0.setAlignment(SwingConstants.LEFT);
		col0.setEditable(listener.isEditable());
		col0.setTableCellEditor(myListener.getTableCellLibelleEditor());
		col0.setMyModifier(myListener.getTableCellLibelleModifier());

		final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EOBilanPoste.BP_FORMULE_MONTANT_KEY, "Montant", 200);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setEditable(listener.isEditable());
		col1.setTableCellEditor(myListener.getTableCellFormuleEditor());
		col1.setMyModifier(myListener.getTableCellMontantModifier());

		final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, EOBilanPoste.BP_FORMULE_AMORTISSEMENT_KEY, "Amortissement", 200);
		col2.setAlignment(SwingConstants.CENTER);
		col2.setEditable(listener.isEditable());
		col2.setTableCellEditor(myListener.getTableCellFormuleEditor());
		col2.setMyModifier(myListener.getTableCellAmortissementModifier());

		colsMap.clear();
		colsMap.put(EOBilanPoste.BP_LIBELLE_KEY, col0);
		colsMap.put(EOBilanPoste.BP_FORMULE_MONTANT_KEY, col1);
		colsMap.put(EOBilanPoste.BP_FORMULE_AMORTISSEMENT_KEY, col2);
	}

	public interface IBilanPosteListPanelListener extends IZKarukeraTablePanelListener {
		public EOExercice getExercice();

		public Modifier getTableCellLibelleModifier();

		public TableCellEditor getTableCellLibelleEditor();

		public boolean isEditable();

		public Modifier getTableCellAmortissementModifier();

		public Modifier getTableCellMontantModifier();

		public TableCellEditor getTableCellFormuleEditor();

	}


	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

	public void ajouterInDg(EOBilanPoste detail, int row) {
		myDisplayGroup.insertObjectAtIndex(detail, row);
		myDisplayGroup.setSelectedObject(detail);

		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();

		myEOTable.requestFocusInWindow();
		if (myEOTable.editCellAt(row, 0)) {
			myEOTable.changeSelection(row, 0, false, false);
		}

	}

}
