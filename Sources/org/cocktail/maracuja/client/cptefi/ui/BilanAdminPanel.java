/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeListener;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZDropDownButton;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BilanAdminPanel extends ZKarukeraPanel {
	private IBilanAdminPanelListener myListener;

	private BilanPosteListPanel actifPostePanel;
	private BilanPosteListPanel passifPostePanel;
	private JComboBox comboBilanType;
	private JTabbedPane tabs;

	/**
	 * @param editingContext
	 */
	public BilanAdminPanel(IBilanAdminPanelListener listener) {
		super();
		myListener = listener;
		actifPostePanel = new BilanPosteListPanel("Actif", myListener.getBilanPosteListPanelListenerActif());
		passifPostePanel = new BilanPosteListPanel("Passif", myListener.getBilanPosteListPanelListenerPassif());
	}

	public void initGUI() {
		actifPostePanel.initGUI();
		passifPostePanel.initGUI();
		System.out.println("BilanAdminPanel.initGUI()");
		tabs = createTabs();

		this.setLayout(new BorderLayout());
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(tabs, BorderLayout.CENTER);

		this.add(buildTopPanel(), BorderLayout.NORTH);
		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(tmp, BorderLayout.CENTER);
	}

	public JTabbedPane getTabs() {
		return tabs;
	}

	public JTabbedPane createTabs() {
		final JTabbedPane p = new JTabbedPane();
		p.addTab("Actif " + myListener.getExercice().exeExercice(), actifPostePanel);
		p.addTab("Passif " + myListener.getExercice().exeExercice(), passifPostePanel);
		return p;
	}

	public void updateData() throws Exception {
		actifPostePanel.updateData();
		passifPostePanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList<Action> list = new ArrayList<Action>();
		list.add(myListener.actionDelete());
		list.add(myListener.actionGoUp());
		list.add(myListener.actionGoDown());
		list.add(myListener.actionEnregistrer());
		list.add(myListener.actionAnnuler());
		list.add(myListener.actionImprimer());

		final List<Component> listButtons = ZKarukeraPanel.getButtonListFromActionList(list);
		final ZDropDownButton bNew = new ZDropDownButton("Créer...", ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
		bNew.addActions(myListener.actionsNew());
		listButtons.add(0, bNew);

		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(listButtons, 5, 20), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	public BilanPosteListPanel getActifPostePanel() {
		return actifPostePanel;
	}

	public BilanPosteListPanel getPassifPostePanel() {
		return passifPostePanel;
	}

	private JPanel buildTopPanel() {
		final ZCommentPanel commentPanel = new ZCommentPanel("Paramétrage du bilan ",
				"<html><ul><li>Vous pouvez modifier ici les formules de calcul du bilan. Exemple de formule acceptée : SD4096 + SD(472-4729)</li>" +
						"<li>La modification dans cet outil d'une rubrique, d'une sous-rubrique ou d'un poste doit correspondre à une modification métier réelle " +
						"et ne doit pas être détournée pour gérer de nouveaux éléments puisque pour ces éléments sont présentées les situations sur 2 exercices différents.</li>" +
						"<li>Les éléments présents sur un exercice N-1 et ne devant plus apparaître au bilan de l'exercice N doivent être supprimés et ne doivent pas être réutilisés " +
						"pour la définition de nouveaux éléments qui eux doivent être réellement ajoutés.</li>" +
						"</ul></html>"
				, null);

		comboBilanType = new JComboBox(myListener.getComboBilanTypeModel());
		comboBilanType.addActionListener(myListener.getComboBilanTypeListener());

		JPanel p = new JPanel(new BorderLayout());
		p.add(commentPanel, BorderLayout.NORTH);
		p.add(new ZLabeledComponent("Type :", comboBilanType, ZLabeledComponent.LABELONLEFT, -1));
		return p;
	}

	private JPanel buildBottomPanel() {
		ArrayList<Action> a = new ArrayList<Action>();
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public interface IBilanAdminPanelListener {
		public Action actionClose();

		public Action actionGoDown();

		public Action actionGoUp();

		public Action actionDelete();

		public Action actionAnnuler();

		public Action actionEnregistrer();

		public ActionListener getComboBilanTypeListener();

		public List<Action> actionsNew();

		public ComboBoxModel getComboBilanTypeModel();

		public Action actionImprimer();

		public void onSelectionChanged();

		public void onDbClick();

		public EOExercice getExercice();

		public IZEOTableCellRenderer getBilanPosteRenderer();

		public BilanPosteListPanel.IBilanPosteListPanelListener getBilanPosteListPanelListenerActif();

		public BilanPosteListPanel.IBilanPosteListPanelListener getBilanPosteListPanelListenerPassif();

	}

	public boolean stopEditing() {
		return getCurrentBilanPosteListPanel().stopCellEditing();
	}

	public void cancelEditing() {
		getCurrentBilanPosteListPanel().cancelCellEditing();
	}

	public JComboBox getComboBilanType() {
		return comboBilanType;
	}

	public BilanPosteListPanel getCurrentBilanPosteListPanel() {
		return (BilanPosteListPanel) getTabs().getSelectedComponent();
	}

}
