/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.cptefi.ui.ListeActionGenPanel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;


public class ListeActionGenCtrl extends CommonCtrl {
    public static final Dimension WINDOW_DIMENSION = new Dimension(300, 180);
    
    private final ActionClose actionClose = new ActionClose();
    private final ListeActionGenPanel listeActionGenPanel;

    private final ArrayList etapeActions;
    private final String _title;
    

    public ListeActionGenCtrl(final EOEditingContext editingContext, final ArrayList actions, final String title) {
        super(editingContext);
        etapeActions = actions;
        _title = title;
        listeActionGenPanel = new ListeActionGenPanel(new ListeActionGenPanelListener());
    }

    private final ZKarukeraDialog createModalDialog(final Window dial ) {
        final ZKarukeraDialog win;
        
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, _title,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, _title,true);
        }
        setMyDialog(win);
        listeActionGenPanel.setMyDialog(getMyDialog());
        listeActionGenPanel.initGUI();
        listeActionGenPanel.setPreferredSize( WINDOW_DIMENSION );
        win.setModal(true);
        win.setContentPane(listeActionGenPanel);
        win.pack();
        return win;
    }





    public final void openDialog(final Window dial) {
        revertChanges();
        final ZKarukeraDialog win = createModalDialog(dial);
        setMyDialog(win);
        try {
            win.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
    }



    
    private final class ListeActionGenPanelListener implements ListeActionGenPanel.IListeActionGenPanelListener {

        public Action actionClose() {
            return actionClose;
        }

        public ArrayList etapeActions() {
            return etapeActions;
        }


        
    }
    
    
    public final class ActionClose extends AbstractAction {

        public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }

        public void actionPerformed(final ActionEvent e) {
          getMyDialog().onCancelClick();
        }

    }
    

    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return listeActionGenPanel;
    }

    public String title() {
        return _title;
    }
    
    
    
}

