/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.cptefi.ui.CompteFiPanel;
import org.cocktail.maracuja.client.cptefi.ui.CompteFiPanel.ICompteFiPanelListener;
import org.cocktail.maracuja.client.impression.BalanceCtrlCpteFi;
import org.cocktail.maracuja.client.impression.BilanImprCtrl;
import org.cocktail.maracuja.client.impression.CoherenceSoldeCtrlCpteFi;
import org.cocktail.maracuja.client.impression.CompteFiGenCtrl;
import org.cocktail.maracuja.client.impression.DevDepRecCtrl;
import org.cocktail.maracuja.client.impression.DevSoldeCtrlCpteFi;
import org.cocktail.maracuja.client.impression.ExecBudGestCtrl;
import org.cocktail.maracuja.client.impression.FeuilletsBudCtrl;
import org.cocktail.maracuja.client.impression.ui.CompteFiCadre5Panel;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;

public class CompteFiCtrl extends CommonCtrl {
	public static final Dimension WINDOW_DIMENSION = new Dimension(870, 490);
	public static final String TITLE_NEW = "Etapes du compte financier";
	public static final String ID_CPTEFI_VERIF_SOLDES = "Vérification des soldes";
	public static final String ID_CPTEFI_CADRE1_AVT_SOLDE = "Cadre 1 : Balance avant cloture 6 & 7";
	public static final String ID_CPTEFI_CADRE2_BUD_DEP = "Cadre 2 : Développement des dépenses budgétaires";
	public static final String ID_CPTEFI_CADRE3_BUD_REC = "Cadre 3 : Développement des recettes budgétaires";
	public static final String ID_CPTEFI_FEUILLETSBUD = "Feuillets budgétaires";
	public static final String ID_CPTEFI_SOLDES_COMPTES = "Soldes des comptes de classe 6 & 7";
	public static final String ID_CPTEFI_CADRE1_APRES_SOLDE = "Cadre 1 : Balance définitive";
	public static final String ID_CPTEFI_BILAN = "Bilan";
	public static final String ID_CPTEFI_COMPTE_RESULTAT = "Compte de résultat";
	public static final String ID_CPTEFI_SIG = "Soldes intermédiaires de gestion";
	public static final String ID_CPTEFI_DETER_CAF = "Détermination de la capacité d'autofinancement";
	//    public static final String ID_CPTEFI_VAR_FONDS_ROULEMENT = "Variation du fonds de roulement";
	public static final String ID_CPTEFI_DEV_SOLDE = "Etat de développement des soldes";
	public static final String ID_CPTEFI_TAB_FINANCEMENT = "Tableau de financement global";
	public static final String ID_CPTEFI_TAB_IMMO = "Tableau des immobilisations";
	public static final String ID_CPTEFI_TAB_AMORT = "Tableau des amortissements";
	public static final String ID_CPTEFI_TAB_PROV = "Tableau des provisions";
	public static final String ID_CPTEFI_TAB_EMPLOIS = "Tableau des emplois/ressources";
	public static final String ID_CPTEFI_CADRE4_EXEC_BUDGET = "Cadre 4 : Exécution du budget";
	public static final String ID_CPTEFI_CADRE5_TAB_CONCORDANCE = "Cadre 5 : Tableau de concordance...";
	public static final String ID_CPTEFI_CADRE6_BALANCE_COMPTES = "Cadre 6 : Balance des comptes de valeur inactive";
	public static final String ID_CPTEFI_EXECBUDGES = "Execution du budget de gestion";
	public static final String ID_CPTEFI_LISTE_OP = "Liste des ordres de paiement";

	private final ActionClose actionClose = new ActionClose();
	private final CompteFiPanel compteFiPanel;
	private final LinkedHashMap etapeActions = new LinkedHashMap();

	public CompteFiCtrl(final EOEditingContext editingContext) {
		super(editingContext);
		initEtapeActions();
		compteFiPanel = new CompteFiPanel(new CompteFiPanelListener());
	}

	private final ZKarukeraDialog createDialog(final Window dial) {
		final ZKarukeraDialog win;

		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE_NEW, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE_NEW, true);
		}
		setMyDialog(win);
		compteFiPanel.setMyDialog(getMyDialog());
		compteFiPanel.initGUI();
		compteFiPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setModal(false);
		win.setContentPane(compteFiPanel);
		win.pack();
		return win;
	}

	private final void initEtapeActions() {
		etapeActions.put(ID_CPTEFI_VERIF_SOLDES, new Action_CPTEFI_VERIF_SOLDES());
		etapeActions.put(ID_CPTEFI_CADRE1_AVT_SOLDE, new Action_CPTEFI_CADRE1_AVT_SOLDE());
		etapeActions.put(ID_CPTEFI_CADRE2_BUD_DEP, new Action_CPTEFI_CADRE2_BUD_DEP());
		etapeActions.put(ID_CPTEFI_CADRE3_BUD_REC, new Action_CPTEFI_CADRE3_BUD_REC());

		etapeActions.put(ID_CPTEFI_FEUILLETSBUD, new Action_CPTEFI_FEUILLETSBUD());
		etapeActions.put(ID_CPTEFI_SOLDES_COMPTES, new Action_CPTEFI_SOLDES_COMPTES());
		etapeActions.put(ID_CPTEFI_CADRE1_APRES_SOLDE, new Action_CPTEFI_CADRE1_APRES_SOLDE());
		etapeActions.put(ID_CPTEFI_BILAN, new Action_CPTEFI_BILAN());
		etapeActions.put(ID_CPTEFI_COMPTE_RESULTAT, new Action_CPTEFI_COMPTE_RESULTAT());
		etapeActions.put(ID_CPTEFI_SIG, new Action_CPTEFI_SIG());
		etapeActions.put(ID_CPTEFI_DETER_CAF, new Action_CPTEFI_DETER_CAF());
		etapeActions.put(ID_CPTEFI_DEV_SOLDE, new Action_CPTEFI_DEV_SOLDE());
		etapeActions.put(ID_CPTEFI_TAB_FINANCEMENT, new Action_CPTEFI_TAB_FINANCEMENT());
		//Attente
		//        etapeActions.put(ID_CPTEFI_TAB_IMMO, new Action_CPTEFI_TAB_IMMO());
		//        etapeActions.put(ID_CPTEFI_TAB_AMORT, new Action_CPTEFI_TAB_AMORT());
		//        etapeActions.put(ID_CPTEFI_TAB_PROV, new Action_CPTEFI_TAB_PROV());
		// etapeActions.put(ID_CPTEFI_TAB_EMPLOIS, new
		// Action_CPTEFI_TAB_EMPLOIS());
		etapeActions.put(ID_CPTEFI_CADRE4_EXEC_BUDGET, new Action_CPTEFI_CADRE4_EXEC_BUDGET());
		etapeActions.put(ID_CPTEFI_CADRE5_TAB_CONCORDANCE, new Action_CPTEFI_CADRE5_TAB_CONCORDANCE());
		etapeActions.put(ID_CPTEFI_CADRE6_BALANCE_COMPTES, new Action_CPTEFI_CADRE6_BALANCE_COMPTES());
		etapeActions.put(ID_CPTEFI_EXECBUDGES, new Action_CPTEFI_EXECBUDGES());
		etapeActions.put(ID_CPTEFI_LISTE_OP, new Action_CPTEFI_LISTE_OP());

	}

	public final void openDialog(final Window dial) {
		revertChanges();
		final ZKarukeraDialog win = createDialog(dial);
		setMyDialog(win);
		try {
			refresh();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class CompteFiPanelListener implements CompteFiPanel.ICompteFiPanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Map etapeActions() {
			return etapeActions;
		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(final ActionEvent e) {
			getMyDialog().onCancelClick();
		}

	}

	private final class Action_CPTEFI_VERIF_SOLDES extends ACtionCpteFi {

		public Action_CPTEFI_VERIF_SOLDES() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_VERIF_SOLDES);
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "Lancez cette vérification pour vous aider à détecter des erreurs éventuelles dans les comptes.\n " + "Cette vérification se base sur les indications données dans le plan comptable, dans le champ \"Solde de ce compte\". "
					+ "Si le sens du solde ne respecte pas ce qui est indiqué, une erreur sera affichée en face du compte sur cette édition. " + "Si aucune erreur n'est détectée, rien ne sera affiché.");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final CoherenceSoldeCtrlCpteFi dial = new CoherenceSoldeCtrlCpteFi(myApp.editingContext());
				dial.getFilters().put("exercice", getExercice());
				dial.getFilters().put("comptabilite", getComptabilite());
				dial.getFilters().put("showall", Boolean.FALSE);
				// dial.imprimerSilent(getMyDialog());
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_EXECBUDGES extends ACtionCpteFi {

		public Action_CPTEFI_EXECBUDGES() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_EXECBUDGES);
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final ExecBudGestCtrl dial = new ExecBudGestCtrl(myApp.editingContext());
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_CADRE1_AVT_SOLDE extends ACtionCpteFi {

		public Action_CPTEFI_CADRE1_AVT_SOLDE() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE1_AVT_SOLDE);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				// checkActionWithExeStat(this);
				final BalanceCtrlCpteFi dial = new BalanceCtrlCpteFi(myApp.editingContext());
				dial.openDialogForCpteFiCadre1(getMyDialog(), true);
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}

	}

	private final class Action_CPTEFI_CADRE2_BUD_DEP extends ACtionCpteFi {

		public Action_CPTEFI_CADRE2_BUD_DEP() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE2_BUD_DEP);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final DevDepRecCtrl dial = new DevDepRecCtrl(myApp.editingContext(), DevDepRecCtrl.MODE_CADRE2);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_CADRE3_BUD_REC extends ACtionCpteFi {

		public Action_CPTEFI_CADRE3_BUD_REC() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE3_BUD_REC);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final DevDepRecCtrl dial = new DevDepRecCtrl(myApp.editingContext(), DevDepRecCtrl.MODE_CADRE3);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_FEUILLETSBUD extends ACtionCpteFi {

		public Action_CPTEFI_FEUILLETSBUD() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_FEUILLETSBUD);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				// checkActionWithExeStat(this);
				final FeuilletsBudCtrl dial = new FeuilletsBudCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_SOLDES_COMPTES extends ACtionCpteFi {

		public Action_CPTEFI_SOLDES_COMPTES() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.NAME, ID_CPTEFI_SOLDES_COMPTES);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final Soldes67Ctrl dial = new Soldes67Ctrl(myApp.editingContext());
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
			refresh();
		}

	}

	private final class Action_CPTEFI_CADRE1_APRES_SOLDE extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_CADRE1_APRES_SOLDE() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE1_APRES_SOLDE);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}

			try {
				// checkActionWithExeStat(this);
				final BalanceCtrlCpteFi dial = new BalanceCtrlCpteFi(myApp.editingContext());
				dial.openDialogForCpteFiCadre1(getMyDialog(), false);
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	private final class Action_CPTEFI_BILAN extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_BILAN() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_BILAN);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			try {
				final BilanImprCtrl dial = new BilanImprCtrl(myApp.editingContext(), CompteFiGenCtrl.BILAN_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	private final class Action_CPTEFI_COMPTE_RESULTAT extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_COMPTE_RESULTAT() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_COMPTE_RESULTAT);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.CPTERESULTAT_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_SIG extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_SIG() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_SIG);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}

			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.SIG_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_DETER_CAF extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_DETER_CAF() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_DETER_CAF);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.CAF_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_TAB_FINANCEMENT extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_TAB_FINANCEMENT() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_TAB_FINANCEMENT);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.TAB_FINANCEMENT_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_LISTE_OP extends ACtionCpteFi {

		public Action_CPTEFI_LISTE_OP() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_LISTE_OP);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.LISTE_OP_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_DEV_SOLDE extends ACtionCpteFi {

		public Action_CPTEFI_DEV_SOLDE() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_DEV_SOLDE);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			try {
				final DevSoldeCtrlCpteFi dial = new DevSoldeCtrlCpteFi(myApp.editingContext());
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class Action_CPTEFI_TAB_IMMO extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_TAB_IMMO() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_TAB_IMMO);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			showinfoDialogFonctionNonImplementee();
		}

	}

	private final class Action_CPTEFI_TAB_AMORT extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_TAB_AMORT() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_TAB_AMORT);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			showinfoDialogFonctionNonImplementee();
		}

	}

	private final class Action_CPTEFI_TAB_PROV extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_TAB_PROV() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_TAB_PROV);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			showinfoDialogFonctionNonImplementee();
		}

	}

	private final class Action_CPTEFI_TAB_EMPLOIS extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_TAB_EMPLOIS() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_TAB_EMPLOIS);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}
			showinfoDialogFonctionNonImplementee();
		}

	}

	private final class Action_CPTEFI_CADRE4_EXEC_BUDGET extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_CADRE4_EXEC_BUDGET() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE4_EXEC_BUDGET);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}

			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.CADRE4_ID, false);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}

	}

	private final class Action_CPTEFI_CADRE5_TAB_CONCORDANCE extends ACtionCpteFiApresSolde {

		public Action_CPTEFI_CADRE5_TAB_CONCORDANCE() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE5_TAB_CONCORDANCE);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "Tableau de concordance entre la balance définitive des comptes du grand livre " +
					"et le développement des dépenses et recettes de la section des opérations en capital.<br>Y figurent l'intégralité "
					+ "des comptes de classe 1, 2 et 3. Pour les classes 4 et 5, ne sont repris que les comptes 481 (charges à étaler sur plusieurs exercices) " + "et 50 (valeurs mobilières de placement).");
		}

		public void actionPerformed(ActionEvent e) {
			if (!checkEcritureSolde()) {
				return;
			}

			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.CADRE5_ID, true);
				dial.setMyPanel(new CompteFiCadre5Panel(dial.new CompteFiGenPanelListener()));
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}
	}

	private final class Action_CPTEFI_CADRE6_BALANCE_COMPTES extends ACtionCpteFi {

		public Action_CPTEFI_CADRE6_BALANCE_COMPTES() {
			super();
			putValue(AbstractAction.NAME, ID_CPTEFI_CADRE6_BALANCE_COMPTES);
			putValue(ICompteFiPanelListener.ZDESCRIPTION_KEY, "");
		}

		public void actionPerformed(ActionEvent e) {

			try {
				final CompteFiGenCtrl dial = new CompteFiGenCtrl(myApp.editingContext(), CompteFiGenCtrl.CADRE6_ID, true);
				dial.openDialog(getMyDialog());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private abstract class ACtionCpteFi extends AbstractAction {
		public ACtionCpteFi() {

		}

		public boolean canEnable() {
			return true;
		}
	}

	private abstract class ACtionCpteFiAvantSolde extends ACtionCpteFi {
		public ACtionCpteFiAvantSolde() {
			super();
		}

		/**
		 * Vérifier si l'écriture de solde est créée
		 * 
		 * @return
		 */
		public boolean checkEcritureSolde() {
			//
			try {
				if (!canEnable()) {
					throw new DataCheckException("L'écriture de solde des classe 6 et 7 est générée, vous ne pouvez plus utiliser cette fonction, sauf apres avoir supprimé cette écriture.");
				}
				return true;
			}

			catch (Exception e1) {
				showErrorDialog(e1);
				return false;
			}

		}

		/**
		 * Vérifie si l'écriture de solde est créée.
		 * 
		 * @return
		 */
		public final boolean canEnable() {
			final EOEcriture ecriture = Soldes67Ctrl.getEcritureSolde(getEditingContext(), getExercice());
			return (ecriture == null);
		}

	}

	private abstract class ACtionCpteFiApresSolde extends ACtionCpteFi {
		public ACtionCpteFiApresSolde() {
			super();
		}

		/**
		 * Vérifier si l'écriture de solde est créée
		 * 
		 * @return
		 */
		public boolean checkEcritureSolde() {
			//
			try {
				if (!canEnable()) {
					throw new DataCheckException("Vous devez générer l'écriture de solde des classe 6 et 7 avant de pour voir utiliser cette fonction.");
				}
				return true;
			}

			catch (Exception e1) {
				showErrorDialog(e1);
				return false;
			}

		}

		/**
		 * Vérifie si l'écriture de solde est créée.
		 * 
		 * @return
		 */
		public final boolean canEnable() {
			final EOEcriture ecriture = Soldes67Ctrl.getEcritureSolde(getEditingContext(), getExercice());
			return (ecriture != null);
		}

	}

	private final void refresh() {
		final Iterator iter = etapeActions.values().iterator();
		while (iter.hasNext()) {
			final ACtionCpteFi element = (ACtionCpteFi) iter.next();
			element.setEnabled(element.canEnable());
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return compteFiPanel;
	}

	public String title() {
		return TITLE_NEW;
	}

}
