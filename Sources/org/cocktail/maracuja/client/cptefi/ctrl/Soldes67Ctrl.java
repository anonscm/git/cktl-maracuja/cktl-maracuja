/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.cptefi.ui.Soldes67Panel;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;


public class Soldes67Ctrl extends CommonCtrl {
	public static final Dimension WINDOW_DIMENSION = new Dimension(560, 430);
	public static final String TITLE = "Soldes des classes 6 et 7";

	public static final String ID_GENERER = "Générer l'écriture";
	public static final String ID_VOIR = "Voir l' écriture";
	public static final String ID_SUPPRIMER = "Supprimer l'écriture";
	public static final String ID_VALIDER = "Valider l'écriture";

	private static final String ETAT_NON_GENERE = "Ecriture de solde non générée";
	private static final String ETAT_GENERE = "Ecriture de solde générée (non validée)";
	private static final String ETAT_VALIDE = "Ecriture de solde validée ";

	private final ActionClose actionClose = new ActionClose();
	private final Soldes67Panel soldes67Panel;
	private final LinkedHashMap etapeActions = new LinkedHashMap();

	private final Action_GENERER action_GENERER = new Action_GENERER();
	private final Action_VOIR action_VOIR = new Action_VOIR();
	private final Action_SUPPRIMER action_SUPPRIMER = new Action_SUPPRIMER();
	private final Action_VALIDER action_VALIDER = new Action_VALIDER();

	private EOEcriture ecrSolde = null;

	public Soldes67Ctrl(final EOEditingContext editingContext) {
		super(editingContext);
		initEtapeActions();
		soldes67Panel = new Soldes67Panel(new Soldes67PanelListener());
	}

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		final ZKarukeraDialog win;

		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		soldes67Panel.setMyDialog(getMyDialog());
		soldes67Panel.initGUI();
		soldes67Panel.setPreferredSize(WINDOW_DIMENSION);
		win.setModal(true);
		win.setContentPane(soldes67Panel);
		win.pack();
		return win;
	}

	private final void initEtapeActions() {
		etapeActions.put(ID_GENERER, action_GENERER);
		etapeActions.put(ID_VOIR, action_VOIR);
		etapeActions.put(ID_SUPPRIMER, action_SUPPRIMER);
		etapeActions.put(ID_VALIDER, action_VALIDER);
	}

	public final void openDialog(final Window dial) {
		revertChanges();
		final ZKarukeraDialog win = createModalDialog(dial);
		setMyDialog(win);
		try {
			refresh();

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * @return L'écriture DE SOLDE 6 / 7 (de type ECRITURE SOLDE 6 ET 7)
	 */
	public static final EOEcriture getEcritureSolde(final EOEditingContext editingContext, final EOExercice exercice) {
		final EOEcriture ecr = (EOEcriture) EOsFinder.fetchObject(editingContext, EOEcriture.tableName, "exercice=%@ and ecrEtat=%@ and ecrNumero>%@ and typeOperation.topLibelle=%@", new NSArray(new Object[] {
				exercice, EOEcriture.ecritureValide, new Integer(0), EOTypeOperation.TYPE_OPERATION_ECRITURE_SOLDE_6_ET_7
		}), null, true);
		return ecr;
	}

	private final String ecrSoldeEtat() {

		if (ecrSolde == null) {
			return ETAT_NON_GENERE;
		}
		else if (ecrSolde.ecrNumero().intValue() == EOEcriture.NUMERO_ECRITURE_TMP_SOLDE_6_7) {
			return ETAT_GENERE;
		}
		else {
			return ETAT_VALIDE + " (n° " + ecrSolde.ecrNumero() + ")";
		}

	}

	private final void refresh() {
		try {
			ecrSolde = getEcritureSolde(getEditingContext(), getExercice());
			System.out.println("ecriture solde=" + ecrSolde);
			if (ecrSolde == null) {
				action_GENERER.setEnabled(true);
				action_VOIR.setEnabled(false);
				action_SUPPRIMER.setEnabled(false);
				action_VALIDER.setEnabled(false);
			}
			else if (ecrSolde.ecrNumero().intValue() == EOEcriture.NUMERO_ECRITURE_TMP_SOLDE_6_7) {
				action_GENERER.setEnabled(false);
				action_VOIR.setEnabled(true);
				action_SUPPRIMER.setEnabled(true);
				action_VALIDER.setEnabled(true);
			}
			else {
				action_GENERER.setEnabled(false);
				action_VOIR.setEnabled(true);
				action_SUPPRIMER.setEnabled(false);
				action_VALIDER.setEnabled(false);
			}

			soldes67Panel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class Soldes67PanelListener implements Soldes67Panel.ISoldes67PanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Map etapeActions() {
			return etapeActions;
		}

		public String getEtatEcritureSolde() {

			return ecrSoldeEtat();
		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(final ActionEvent e) {
			getMyDialog().onCancelClick();
		}

	}

	private void ecritureSoldeCreer() {
		final CreationEcrSolde67Ctrl creationEcrSolde67Ctrl = new CreationEcrSolde67Ctrl(getEditingContext());
		if (creationEcrSolde67Ctrl.openDialog(getMyDialog()) == CommonCtrl.MROK) {

		}
		refresh();
	}

	private void ecritureSoldeVoir() {
		try {
			if (ecrSolde == null) {
				throw new UserActionException("Ecriture non définie.");
			}

			if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {
				final EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
				win.openDialog(getMyDialog(), new NSArray(ecrSolde));
			}
			else {
				throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void ecritureSoldeSupprimer() {
		try {
			setWaitCursor(true);
			final Integer ecrOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), ecrSolde).valueForKey("ecrOrdre");
			final NSMutableDictionary params = new NSMutableDictionary();
			final String spName = "cptefiutil.annuler_ecriture";
			params.takeValueForKey(ecrOrdre, "10_ecrordre");
			ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), spName, params);
			refresh();
			setWaitCursor(false);
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		}
	}

	private void ecritureSoldeValider() {
		try {
			if (showConfirmationDialog("Confirmation", "La génération des documents (Bilan, Compte de résultat etc.) ne nécessite pas la validation de l'écriture, souhaitez-vous réellement valider l'écriture de solde ? Une fois validée vous ne pourrez plus revenir en arrière...",
					ZMsgPanel.BTLABEL_NO)) {
				setWaitCursor(true);
				final Integer ecrOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), ecrSolde).valueForKey("ecrOrdre");
				final NSMutableDictionary params = new NSMutableDictionary();
				final String spName = "cptefiutil.numeroter_ecriture";
				params.takeValueForKey(ecrOrdre, "10_ecrordre");
				ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), spName, params);
				refresh();
				setWaitCursor(false);
			}
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		}
	}

	private final class Action_GENERER extends AbstractAction {

		public Action_GENERER() {
			putValue(AbstractAction.NAME, ID_GENERER);
		}

		public void actionPerformed(ActionEvent e) {
			ecritureSoldeCreer();
		}

	}

	private final class Action_VOIR extends AbstractAction {

		public Action_VOIR() {
			putValue(AbstractAction.NAME, ID_VOIR);
		}

		public void actionPerformed(ActionEvent e) {
			ecritureSoldeVoir();
		}

	}

	private final class Action_SUPPRIMER extends AbstractAction {

		public Action_SUPPRIMER() {
			putValue(AbstractAction.NAME, ID_SUPPRIMER);
		}

		public void actionPerformed(ActionEvent e) {
			ecritureSoldeSupprimer();

		}

	}

	private final class Action_VALIDER extends AbstractAction {

		public Action_VALIDER() {
			putValue(AbstractAction.NAME, ID_VALIDER);
		}

		public void actionPerformed(ActionEvent e) {
			ecritureSoldeValider();

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return soldes67Panel;
	}

	public String title() {
		return TITLE;
	}

}
