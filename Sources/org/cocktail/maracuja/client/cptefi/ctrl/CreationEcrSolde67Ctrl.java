/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.cptefi.ui.CreationEcrSolde67Panel;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;


public class CreationEcrSolde67Ctrl extends CommonCtrl {
	public static final Dimension WINDOW_DIMENSION = new Dimension(560, 430);
	public static final String TITLE_NEW = "Création de l'écriture des soldes des classes 6 et 7";

	private final ActionClose actionClose = new ActionClose();
	private final ActionOk actionOk = new ActionOk();

	private final CreationEcrSolde67Panel creationEcrSolde67Panel;

	private final Map valeurs = new HashMap();
	private int mrRes = MRCANCEL;

	public CreationEcrSolde67Ctrl(final EOEditingContext editingContext) {
		super(editingContext);
		creationEcrSolde67Panel = new CreationEcrSolde67Panel(new CreationEcrSolde67PanelListener());
	}

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		final ZKarukeraDialog win;

		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE_NEW, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE_NEW, true);
		}
		setMyDialog(win);
		creationEcrSolde67Panel.setMyDialog(getMyDialog());
		creationEcrSolde67Panel.initGUI();
		creationEcrSolde67Panel.setPreferredSize(WINDOW_DIMENSION);
		win.setModal(true);
		win.setContentPane(creationEcrSolde67Panel);
		win.pack();
		return win;
	}

	private final void initValeurs() {
		valeurs.put("ecrLibelle", "Ecriture de solde classes 6 et 7");
		valeurs.put("rtatComp", CreationEcrSolde67Panel.OPTION_COMPOSANTE);
	}

	public final int openDialog(final Window dial) {
		revertChanges();
		final ZKarukeraDialog win = createModalDialog(dial);
		setMyDialog(win);
		try {
			initValeurs();
			creationEcrSolde67Panel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		}
		return mrRes;

	}

	private final class CreationEcrSolde67PanelListener implements CreationEcrSolde67Panel.ICreationEcrSolde67PanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionOk() {
			return actionOk;
		}

		public Map getFilters() {
			return valeurs;
		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		public void actionPerformed(final ActionEvent e) {
			getMyDialog().onCancelClick();
		}

	}

	private final class ActionOk extends AbstractAction {

		public ActionOk() {
			super("Ok");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
		}

		public void actionPerformed(ActionEvent e) {
			try {
				setWaitCursor(true);
				ecritureSoldeCreer();
				mrRes = MROK;
				showInfoDialog("Ecriture créée.");
				setWaitCursor(false);
			} catch (Exception e1) {
				setWaitCursor(false);
				showErrorDialog(e1);
			} finally {
				getMyDialog().setVisible(false);
			}

		}

	}

	private void ecritureSoldeCreer() throws Exception {
		final NSMutableDictionary params = new NSMutableDictionary();
		final String spName = "cptefiutil.solde_6_7";

		final Integer exeOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), getExercice()).valueForKey("exeOrdre");
		final Integer utlOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), getUtilisateur()).valueForKey("utlOrdre");
		final String ecrLibelle = (String) valeurs.get("ecrLibelle");
		final String rtatComp;

		System.out.println("selection : " + creationEcrSolde67Panel.getOptionSelected());

		if (CreationEcrSolde67Panel.OPTION_COMPOSANTE.equals(creationEcrSolde67Panel.getOptionSelected())) {
			rtatComp = "O";
		}
		else {
			rtatComp = "N";
		}

		if (ecrLibelle == null || ecrLibelle.length() == 0) {
			throw new DataCheckException("Vous devez spécifier un libellé pour l'écriture");
		}
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(ecrLibelle, "20_libelle");
		params.takeValueForKey(rtatComp, "25_rtat_comp");
		ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), spName, params);
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return creationEcrSolde67Panel;
	}

	public String title() {
		return TITLE_NEW;
	}

}
