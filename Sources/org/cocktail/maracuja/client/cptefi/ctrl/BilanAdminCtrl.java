/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cptefi.ctrl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.cptefi.ui.BilanAdminPanel;
import org.cocktail.maracuja.client.cptefi.ui.BilanPosteListPanel;
import org.cocktail.maracuja.client.cptefi.ui.BilanPosteListPanel.IBilanPosteListPanelListener;
import org.cocktail.maracuja.client.metier.EOBilanPoste;
import org.cocktail.maracuja.client.metier.EOBilanPoste.BPTypeElement;
import org.cocktail.maracuja.client.metier.EOBilanType;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;

import com.lowagie.text.Font;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BilanAdminCtrl extends CommonCtrl {

	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Paramétrage du bilan";
	private final ActionClose actionClose = new ActionClose();
	private final ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionEnregistrer actionEnregistrer = new ActionEnregistrer();
	private final ActionAnnuler actionAnnuler = new ActionAnnuler();

	private TableCellFormuleEditor tableCellFormuleEditor = new TableCellFormuleEditor(new JTextField());
	private BilanPosteTableCellRenderer bilanPosteTableCellRenderer = new BilanPosteTableCellRenderer();
	private BilanPassifListPanelListener bilanPassifListPanelListener = new BilanPassifListPanelListener();
	private BilanActifListPanelListener bilanActifListPanelListener = new BilanActifListPanelListener();
	private TableCellAmortissementModifierActif tableCellAmortissementModifierActif = new TableCellAmortissementModifierActif();
	private TableCellMontantModifier tableCellMontantModifier = new TableCellMontantModifier();
	private Modifier tableCellLibelleModifier = new TableCellLibelleModifier();
	private TableCellEditor tableCellLibelleEditor = new TableCellLibelleEditor(new JTextField());

	private Action actionUp = new ActionUp();
	private Action actionDown = new ActionDown();
	private Action actionDelete = new ActionDelete();
	private ActionNewRubrique actionNewRubrique = new ActionNewRubrique();
	private ActionNewSousRubrique actionNewSousRubrique = new ActionNewSousRubrique();
	private ActionNewPoste actionNewPoste = new ActionNewPoste();
	private List<Action> actionsNew = new ArrayList<Action>();

	private ZKarukeraDialog win;

	private BilanAdminPanel myPanel;

	private ZEOComboBoxModel bilanTypeComboboxModel;
	private EOBilanType selectedBilanType;

	/**
	 * @param editingContext
	 */
	public BilanAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);
		actionsNew.add(actionNewRubrique);
		actionsNew.add(actionNewSousRubrique);
		actionsNew.add(actionNewPoste);
		revertChanges();
		initSubObjects();

	}

	public void initSubObjects() {
		myPanel = new BilanAdminPanel(new BilanAdminPanelListener());
		NSArray lesBilanTypes = EOBilanType.fetchAll(getEditingContext());

		bilanTypeComboboxModel = new ZEOComboBoxModel(lesBilanTypes, EOBilanType.BT_LIBELLE_KEY, null, null);
		selectedBilanType = (EOBilanType) lesBilanTypes.objectAtIndex(0);
	}

	private void initGUI() {
		myPanel.initGUI();
	}

	private void onFermer() {
		if (getEditingContext().hasChanges()) {
			showInfoDialog("Vous avez des modifications non enregistrées, veuillez les enregistrer ou les annuler.");
			return;
		}
		getEditingContext().revert();
		win.onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();

		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private final ZKarukeraDialog createModalDialog(Frame dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();

		win.setContentPane(myPanel);

		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Dialog dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		win1.setTitle(win1.getTitle() + " - " + getExercice().exeExercice().intValue());
		try {
			if (getExercice().estClos()) {
				showInfoDialog("L'exercice " + getExercice().exeExercice().intValue() + " est clos, vous ne pouvez plus modifier les formules de calcul du bilan.");
			}
			myPanel.updateData();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
	}

	public final void openDialog(Frame dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		win1.setTitle(win1.getTitle() + " - " + getExercice().exeExercice().intValue());
		try {
			if (getExercice().estClos()) {
				showInfoDialog("L'exercice " + getExercice().exeExercice().intValue() + " est clos, vous ne pouvez plus modifier les formules de calcul du bilan.");
			}
			myPanel.updateData();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
	}

	private final void refreshActions() {
		actionEnregistrer.setEnabled(getEditingContext().hasChanges());
		actionAnnuler.setEnabled(getEditingContext().hasChanges());
		actionDelete.setEnabled(!getEditingContext().hasChanges() && getCurrentBilanPoste() != null);
		actionUp.setEnabled(!getEditingContext().hasChanges() && getCurrentBilanPoste() != null);
		actionDown.setEnabled(!getEditingContext().hasChanges() && getCurrentBilanPoste() != null);
		actionNewPoste.setEnabled(getCurrentBilanPoste() != null);
		actionNewRubrique.setEnabled(getCurrentBilanPoste() != null);
		actionNewSousRubrique.setEnabled(getCurrentBilanPoste() != null);
		myPanel.getComboBilanType().setEnabled(!getEditingContext().hasChanges());
	}

	private final class BilanAdminPanelListener implements BilanAdminPanel.IBilanAdminPanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionImprimer() {
			return actionImprimer;
		}

		public void onSelectionChanged() {
			refreshActions();
		}

		public void onDbClick() {
			//pcoModifier();

		}

		public EOExercice getExercice() {
			return BilanAdminCtrl.this.getExercice();
		}

		public IZEOTableCellRenderer getBilanPosteRenderer() {
			return bilanPosteTableCellRenderer;
		}

		public IBilanPosteListPanelListener getBilanPosteListPanelListenerActif() {
			return bilanActifListPanelListener;
		}

		public IBilanPosteListPanelListener getBilanPosteListPanelListenerPassif() {
			return bilanPassifListPanelListener;
		}

		public ActionListener getComboBilanTypeListener() {
			return bilanTypeComBoListener;
		}

		public ComboBoxModel getComboBilanTypeModel() {
			return bilanTypeComboboxModel;
		}

		public Action actionAnnuler() {
			return actionAnnuler;
		}

		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		public List<Action> actionsNew() {
			return actionsNew;
		}

		public Action actionDelete() {
			return actionDelete;
		}

		public Action actionGoDown() {
			return actionDown;
		}

		public Action actionGoUp() {
			return actionUp;
		}

	}

	private final class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			super("Enregistrer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les modifications.");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			onEnregistrer();
		}
	}

	private final class ActionAnnuler extends AbstractAction {
		public ActionAnnuler() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler les modifications.");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			onAnnuler();
		}
	}

	private final class ActionUp extends AbstractAction {
		public ActionUp() {
			super("Monter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_UP_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Monter");
			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onGoUpDown(true);
		}

	}

	private final class ActionDown extends AbstractAction {
		public ActionDown() {
			super("Descendre");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_DOWN_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Descendre");
			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onGoUpDown(false);
		}

	}
	private final class ActionDelete extends AbstractAction {
		public ActionDelete() {
			super("Supprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer");
			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onDelete();
		}

	}

	private final class ActionNewRubrique extends AbstractAction {
		public ActionNewRubrique() {
			super("Rubrique");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CARRE_ORANGE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Créer une nouvelle rubrique.");
			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onCreerNewRubrique();
		}

	}

	private final class ActionNewSousRubrique extends AbstractAction {
		public ActionNewSousRubrique() {
			super("Sous-rubrique");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CARRE_VERT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Créer une nouvelle sous-rubrique.");
			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onCreerNewSousRubrique();
		}
	}

	private final class ActionNewPoste extends AbstractAction {
		public ActionNewPoste() {
			super("Poste");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CARRE_BLANC_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouveau poste.");
			setEnabled(true);
		}

		public void actionPerformed(ActionEvent e) {
			onCreerNewPoste();
		}
	}

	private final class ActionImprimer extends AbstractAction {
		public ActionImprimer() {
			super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le plan comptable");
		}

		public void actionPerformed(ActionEvent e) {
			onImprimer();
		}
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onFermer();
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	private static final class BilanPosteTableCellRenderer extends ZEOTableCellRenderer {
		private static final Image IMG_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16).getImage();
		private static final ZEOTable.ZImageCell CELL_OK = new ZEOTable.ZImageCell(new Image[] {
				IMG_OK
		});
		static {
			CELL_OK.setCentered(true);
		}

		private static final Icon ICON_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16);
		private static final JLabel LABEL_OK = new JLabel(ICON_OK);
		private static Color COL_CLASSE = Color.decode("#F9CC8A");
		private static Color COL_CHAPITRE = Color.decode("#DCEEA6");

		static {
			CELL_OK.setLayout(new BorderLayout());
			LABEL_OK.setOpaque(false);
		}

		public BilanPosteTableCellRenderer() {
			super();
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object,
		 *      boolean, boolean, int, int)
		 */
		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			Component res = null;
			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOBilanPoste obj = (EOBilanPoste) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);

			final ZEOTableModelColumn column2 = ((ZEOTable) table).getDataModel().getColumn(table.convertColumnIndexToModel(column));

			res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			if (isSelected) {
				res.setBackground(table.getSelectionBackground());
				res.setForeground(table.getSelectionForeground());
				if (!column2.getAttributeName().equals(EOBilanPoste.BP_LIBELLE_KEY)) {
					if (obj.isGroupe1()) {
						res.setBackground(COL_CLASSE.darker());
						res.setForeground(table.getForeground());
					}
					else if (obj.isGroupe2()) {
						res.setBackground(COL_CHAPITRE.darker());
						res.setForeground(table.getForeground());
					}
				}
			}
			else {
				if (obj.isGroupe1()) {
					res.setBackground(COL_CLASSE);
					res.setForeground(table.getForeground());
				}
				else if (obj.isGroupe2()) {
					res.setBackground(COL_CHAPITRE);
					res.setForeground(table.getForeground());
				}
				else {
					res.setBackground(table.getBackground());
					res.setForeground(table.getForeground());
				}
			}
			if (obj.isUpdated()) {
				res.setFont(res.getFont().deriveFont(this.getFont().getStyle() + Font.BOLD));
			}
			else {
				res.setFont(this.getFont());
			}
			return res;
		}
	}

	private final class BilanActifListPanelListener implements BilanPosteListPanel.IBilanPosteListPanelListener {

		public NSArray getData() throws Exception {
			return getBilanPostesActif();
		}

		public void onDbClick() {

		}

		public EOExercice getExercice() {
			return BilanAdminCtrl.this.getExercice();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return bilanPosteTableCellRenderer;
		}

		public void selectionChanged() {

		}

		public TableCellEditor getTableCellFormuleEditor() {
			return tableCellFormuleEditor;
		}

		public Modifier getTableCellAmortissementModifier() {
			return tableCellAmortissementModifierActif;
		}

		public Modifier getTableCellMontantModifier() {
			return tableCellMontantModifier;
		}

		public boolean isEditable() {
			return (!getExercice().estClos());
		}

		public Modifier getTableCellLibelleModifier() {
			return tableCellLibelleModifier;
		}

		public TableCellEditor getTableCellLibelleEditor() {
			return tableCellLibelleEditor;
		}

	}

	private final class BilanPassifListPanelListener implements BilanPosteListPanel.IBilanPosteListPanelListener {

		public NSArray getData() throws Exception {
			return getBilanPostesPassif();
		}

		public void onDbClick() {

		}

		public EOExercice getExercice() {
			return BilanAdminCtrl.this.getExercice();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return bilanPosteTableCellRenderer;
		}

		public void selectionChanged() {

		}

		public TableCellEditor getTableCellFormuleEditor() {
			return tableCellFormuleEditor;
		}

		public Modifier getTableCellAmortissementModifier() {
			return null;
		}

		public Modifier getTableCellMontantModifier() {
			return tableCellMontantModifier;
		}

		public boolean isEditable() {
			return (!getExercice().estClos());
		}

		public Modifier getTableCellLibelleModifier() {
			return tableCellLibelleModifier;
		}

		public TableCellEditor getTableCellLibelleEditor() {
			return tableCellLibelleEditor;
		}
	}

	public class TableCellFormuleEditor extends DefaultCellEditor {
		private final JTextField myTextField;

		public TableCellFormuleEditor(JTextField textField) {
			super(textField);
			myTextField = textField;
			myTextField.setBorder(BorderFactory.createLineBorder(Color.decode("#EE8827")));
		}

		/**
		 * @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
		 */
		public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {

			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOBilanPoste obj = (EOBilanPoste) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
			final JTextField tmp = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
			//Si l'objet selectionne est un groupe, seul le libelle est modifiable
			;
			if (obj.isGroupe1() || obj.isGroupe2()) {
				tmp.setEnabled(false);
				return null;
			}
			tmp.setEnabled(true);
			tmp.selectAll();
			return tmp;
		}

		/**
		 * @see javax.swing.DefaultCellEditor#shouldSelectCell(java.util.EventObject)
		 */
		public boolean shouldSelectCell(final EventObject anEvent) {
			myTextField.selectAll();
			return super.shouldSelectCell(anEvent);
		}

		public JTextField getMyTextField() {
			return myTextField;
		}

		public boolean stopCellEditing() {
			try {
				EOBilanPoste.validerFormule(getEditingContext(), myTextField.getText());
			} catch (Exception e) {
				showErrorDialog(e);
				return false;
			}
			return super.stopCellEditing();
		}
	}

	public class TableCellLibelleEditor extends DefaultCellEditor {
		private final JTextField myTextField;

		public TableCellLibelleEditor(JTextField textField) {
			super(textField);
			myTextField = textField;
			myTextField.setBorder(BorderFactory.createLineBorder(Color.decode("#EE8827")));
		}

		/**
		 * @see javax.swing.DefaultCellEditor#getTableCellEditorComponent(javax.swing.JTable, java.lang.Object, boolean, int, int)
		 */
		public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected, final int row, final int column) {

			//verifier si l'objet selectionne est un groupe
			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOBilanPoste obj = (EOBilanPoste) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);
			final JTextField tmp = (JTextField) super.getTableCellEditorComponent(table, value, isSelected, row, column);
			tmp.setEnabled(true);
			tmp.selectAll();
			return tmp;
		}

		/**
		 * @see javax.swing.DefaultCellEditor#shouldSelectCell(java.util.EventObject)
		 */
		public boolean shouldSelectCell(final EventObject anEvent) {
			myTextField.selectAll();
			return super.shouldSelectCell(anEvent);
		}

		public JTextField getMyTextField() {
			return myTextField;
		}

		public boolean stopCellEditing() {
			try {
				EOBilanPoste.validerLibelle(getEditingContext(), myTextField.getText());
			} catch (Exception e) {
				showErrorDialog(e);
				return false;
			}
			return super.stopCellEditing();
		}
	}

	private void setBilanPosteLibelle(BilanPosteListPanel panel, Object value, int row) {
		EOBilanPoste tmp = (EOBilanPoste) panel.selectedObject();
		int col = panel.getMyEOTable().getSelectedColumn();
		try {
			tmp.checkLibelle((String) value);
			tmp.setBpLibelle((String) value);
			panel.getMyTableModel().fireTableCellUpdated(row, col);
			refreshActions();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void setBilanFormuleMontant(BilanPosteListPanel panel, Object value, int row) {
		EOBilanPoste tmp = (EOBilanPoste) panel.selectedObject();
		int col = panel.getMyEOTable().getSelectedColumn();
		try {
			tmp.setBpFormuleMontant((String) value);
			panel.getMyTableModel().fireTableCellUpdated(row, col);
			refreshActions();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void setBilanFormuleAmortissement(BilanPosteListPanel panel, Object value, int row) {
		EOBilanPoste tmp = (EOBilanPoste) panel.selectedObject();
		int col = panel.getMyEOTable().getSelectedColumn();
		try {
			tmp.setBpFormuleAmortissement((String) value);
			panel.getMyTableModel().fireTableCellUpdated(row, col);
			refreshActions();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private class TableCellAmortissementModifierActif implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			setBilanFormuleAmortissement(myPanel.getActifPostePanel(), value, row);
		}
	}

	private class TableCellLibelleModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			setBilanPosteLibelle(myPanel.getCurrentBilanPosteListPanel(), value, row);
		}
	}

	private class TableCellMontantModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			setBilanFormuleMontant(myPanel.getCurrentBilanPosteListPanel(), value, row);
		}
	}

	private NSArray getBilanPostesPassif() throws Exception {
		return EOBilanPoste.fetchAllForMode(getEditingContext(), selectedBilanType, getExercice(), "passif");
	}

	private NSArray getBilanPostesActif() throws Exception {
		NSArray res = EOBilanPoste.fetchAllForMode(getEditingContext(), selectedBilanType, getExercice(), "actif");
		return res;
	}

	private final ActionListener bilanTypeComBoListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			selectedBilanType = (EOBilanType) bilanTypeComboboxModel.getSelectedEObject();
			try {
				myPanel.updateData();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
			refreshActions();
		}
	};

	private void onEnregistrer() {
		try {
			if (myPanel.stopEditing() && getEditingContext().hasChanges()) {
				if (!estCeQueLeParametrageDuBilanEstModifiable()) {
					return;
				}
				getEditingContext().saveChanges();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			try {
				myPanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		refreshActions();
	}

	private void onAnnuler() {
		try {
			myPanel.cancelEditing();

			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			try {
				myPanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		refreshActions();
	}

	private void onImprimer() {
		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			dico.addEntriesFromDictionary(myApp.getParametres());
			dico.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(getEditingContext(), myApp.appUserInfo().getCurrentExercice()).valueForKey("exeOrdre"), "EXE_ORDRE");
			dico.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(getEditingContext(), selectedBilanType).valueForKey(EOBilanType.BT_ID_KEY), "BT_ID");
			dico.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(getEditingContext(), selectedBilanType).valueForKey(EOBilanType.BT_ID_KEY), "BT_ID");

			String filePath = ReportFactoryClient.imprimerBilanFormules(myApp.editingContext(), myApp.temporaryDir, dico);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private EOBilanPoste getCurrentBilanPoste() {
		return (EOBilanPoste) myPanel.getCurrentBilanPosteListPanel().getMyDisplayGroup().selectedObject();
	}


	
	private void creerNewLigne(BPTypeElement typeElt) {
		if (!estCeQueLeParametrageDuBilanEstModifiable()) {
			return;
		}

		try {
			if (myPanel.getCurrentBilanPosteListPanel().isEditing()) {
				myPanel.getCurrentBilanPosteListPanel().stopCellEditing();
			}
			String bpGroupe = "N";

			if (typeElt != BPTypeElement.poste) {
				bpGroupe = "O";
			}

			//papaouté ?
			EOBilanPoste bilanPostePere;
			bilanPostePere = getCurrentBilanPoste().getAncetreAtNiveau(0);
			if (typeElt.equals(EOBilanPoste.BPTypeElement.poste)) {
				if (getCurrentBilanPoste().isPoste()) {
					bilanPostePere = getCurrentBilanPoste().getAncetreSousRubriqueOuRubrique();
				}
				else {
					bilanPostePere = getCurrentBilanPoste();
				}

				if (bilanPostePere.isGroupe1() && bilanPostePere.hasSousRubriques()) {
					if (!showConfirmationDialog("Confirmation", "La rubrique " + bilanPostePere.bpLibelle() + "possède des sous-rubriques, souhaitez-vous vraiment créer un poste directement sous " + bilanPostePere.bpLibelle() + " plutôt que de le créer dans une sous-rubrique ?",
							ZMsgPanel.BTLABEL_NO)) {
						return;
					}
				}

			}
			else if (typeElt.equals(EOBilanPoste.BPTypeElement.groupe2)) {
				if (getCurrentBilanPoste().isGroupe2() || getCurrentBilanPoste().isPoste()) {
					bilanPostePere = getCurrentBilanPoste().getAncetreRubrique();
				}
				else {
					bilanPostePere = getCurrentBilanPoste();
				}
			}

			//EOBilanPoste bilanPostePere = getCurrentBilanPoste().getAncetreAtNiveau(niveau - 1);
			EOBilanPoste bilanPostePrevious = bilanPostePere;
			if (bilanPostePere.toBilanPosteEnfants().count() > 0) {
				bilanPostePrevious = (EOBilanPoste) bilanPostePere.toBilanPosteEnfantsRecursif().lastObject();
			}
			Integer row = myPanel.getCurrentBilanPosteListPanel().getMyDisplayGroup().displayedObjects().indexOfObject(bilanPostePrevious) + 1;
			//			Integer row = bilanPostePrevious.numberOfSubObjects();
			//Integer row = myPanel.getCurrentBilanPosteListPanel().selectedRowIndex() + 1;
			EOBilanPoste newBilanPoste = EOBilanPoste.createBilanPoste(getEditingContext(), bilanPostePere, bpGroupe, null, "O", bilanPostePere.bpNiveau() + 1, (EOBilanType) bilanTypeComboboxModel.getSelectedEObject(), getExercice());
			//row = row + newBilanPoste.bpOrdre();
			myPanel.getCurrentBilanPosteListPanel().ajouterInDg(newBilanPoste, row);
			//myPanel.getCurrentBilanPosteListPanel().getMyDisplayGroup().redisplay();

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			refreshActions();
		}

	}

	private void onCreerNewRubrique() {
		creerNewLigne(EOBilanPoste.BPTypeElement.groupe1);
	}

	private void onCreerNewSousRubrique() {
		creerNewLigne(EOBilanPoste.BPTypeElement.groupe2);
	}

	private void onCreerNewPoste() {
		creerNewLigne(EOBilanPoste.BPTypeElement.poste);
	}

	private void onDelete() {
		if (!estCeQueLeParametrageDuBilanEstModifiable()) {
			return;
		}
		try {
			if (myPanel.getCurrentBilanPosteListPanel().isEditing()) {
				myPanel.getCurrentBilanPosteListPanel().stopCellEditing();
			}
			if (getCurrentBilanPoste() == null) {
				return;
			}
			if (getEditingContext().hasChanges()) {
				showInfoDialog("Vous avez des modifications non enregistrées. Veuillez les enregistrer ou les annuler avant de poursuivre.");
				return;
			}

			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous supprimer \n" + getCurrentBilanPoste().displayLibelleComplet() + "?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
			EOBilanPoste.deleteObject(getCurrentBilanPoste());
			onEnregistrer();

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			refreshActions();
		}
	}

	private void onGoUpDown(boolean up) {
		if (!estCeQueLeParametrageDuBilanEstModifiable()) {
			return;
		}
		try {
			if (myPanel.getCurrentBilanPosteListPanel().isEditing()) {
				myPanel.getCurrentBilanPosteListPanel().stopCellEditing();
			}
			if (getCurrentBilanPoste() == null) {
				return;
			}
			if (getEditingContext().hasChanges()) {
				showInfoDialog("Vous avez des modifications non enregistrées. Veuillez les enregistrer ou les annuler avant de poursuivre.");
				return;
			}
			if (up) {
				if (getCurrentBilanPoste().isPremierEnfant()) {
					showInfoDialog("Vous ne pouvez pas changer un poste de sous-rubrique (ou changer une sous-rubrique de rubrique).");
				}
				else {
					getCurrentBilanPoste().echangerBpOrdreAvecEnfantPrecedent();
				}
			}
			else {
				if (getCurrentBilanPoste().isDernierEnfant()) {
					showInfoDialog("Vous ne pouvez pas changer un poste de sous-rubrique (ou changer une sous-rubrique de rubrique).");
				}
				else {
					getCurrentBilanPoste().echangerBpOrdreAvecEnfantSuivant();
				}
			}

			onEnregistrer();

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			refreshActions();
		}
	}

	private boolean estCeQueLeParametrageDuBilanEstModifiable() {
		Integer btId = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), selectedBilanType).valueForKey(EOBilanType.BT_ID_KEY);
		String sql = "select ba_ordre from comptefi.bilan_actif where exe_ordre=" + getExercice().exeExercice() + " and bt_id=" + btId;
		sql = sql.concat(" union all select bp_ordre from comptefi.bilan_passif where exe_ordre=" + getExercice().exeExercice() + " and bt_id=" + btId);
		ZLogger.verbose(sql);
		NSArray res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		if (res.count() > 0) {
			if (!showConfirmationDialog("Confirmation", "Le bilan a déjà été calculé pour l'exercice " + getExercice().exeExercice()
					+ ". Si vous effectuez des modifications de paramétrage les valeurs calculées seront effacées, vous pourrez ensuite les calculer à nouveau. Souhaitez-vous continuer ?", ZMsgPanel.BTLABEL_NO)) {
				return false;
			}
			sql = "delete from comptefi.bilan_actif where exe_ordre=" + getExercice().exeExercice() + " and bt_id=" + btId;
			res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
			sql = "delete from comptefi.bilan_passif where exe_ordre=" + getExercice().exeExercice() + " and bt_id=" + btId;
			res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		}
		return true;
	}


}
