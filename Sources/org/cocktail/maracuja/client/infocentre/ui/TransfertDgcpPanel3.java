/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.infocentre.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.basic.BasicTableUI;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eoapplication.EOController.Enumeration;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TransfertDgcpPanel3 extends ZKarukeraPanel {
	private final ImageIcon iconExport32 = ZIcon.getIconForName(ZIcon.ICON_EXPORTDGCP_32);

	public static final String PREFIXE_BAL = "BAL";
	public static final String PREFIXE_EDDB = "EDDB";
	public static final String PREFIXE_EDRB = "EDRB";
	public static final String PREFIXE_ECCB = "ECCB";

	public static final String RANG_J1 = "J1";
	public static final String RANG_F1 = "F1";
	public static final String RANG_01 = "01";
	public static final String RANG_A2 = "A2";
	public static final String RANG_M2 = "M2";
	public static final String RANG_02 = "02";
	public static final String RANG_J3 = "J3";
	public static final String RANG_A3 = "A3";
	public static final String RANG_03 = "03";
	public static final String RANG_O4 = "O4";
	public static final String RANG_N4 = "N4";
	public static final String RANG_04 = "04";
	public static final String RANG_05 = "05";
	public static final String RANG_06 = "06";

	public static final String RANG_J1_LIBELLE = "J1 : Janvier";
	public static final String RANG_F1_LIBELLE = "F1 : Février";
	public static final String RANG_01_LIBELLE = "01 : Mars (1er trimestre)";
	public static final String RANG_A2_LIBELLE = "A2 : Avril";
	public static final String RANG_M2_LIBELLE = "M2 : Mai";
	public static final String RANG_02_LIBELLE = "02 : Juin (2ème trimestre)";
	public static final String RANG_J3_LIBELLE = "J3 : Juillet";
	public static final String RANG_A3_LIBELLE = "A3 : Août";
	public static final String RANG_03_LIBELLE = "03 : Septembre (3ème trimestre)";
	public static final String RANG_O4_LIBELLE = "O4 : Octobre";
	public static final String RANG_N4_LIBELLE = "N4 : Novembre";
	public static final String RANG_04_LIBELLE = "04 : Décembre (4ème trimestre)";
	public static final String RANG_05_LIBELLE = "05 : Etats provisoires";
	public static final String RANG_06_LIBELLE = "06 : Etats définitifs";
	public static final String RANG_MENSUELS = "Etats mensuels";

	public static final Map<String, String> RANGS_MAP = new LinkedHashMap<String, String>();
	static {
		RANGS_MAP.put(RANG_J1_LIBELLE, RANG_J1);
		RANGS_MAP.put(RANG_F1_LIBELLE, RANG_F1);
		RANGS_MAP.put(RANG_01_LIBELLE, RANG_01);
		RANGS_MAP.put(RANG_A2_LIBELLE, RANG_A2);
		RANGS_MAP.put(RANG_M2_LIBELLE, RANG_M2);
		RANGS_MAP.put(RANG_02_LIBELLE, RANG_02);
		RANGS_MAP.put(RANG_J3_LIBELLE, RANG_J3);
		RANGS_MAP.put(RANG_A3_LIBELLE, RANG_A3);
		RANGS_MAP.put(RANG_03_LIBELLE, RANG_03);
		RANGS_MAP.put(RANG_O4_LIBELLE, RANG_O4);
		RANGS_MAP.put(RANG_N4_LIBELLE, RANG_N4);
		RANGS_MAP.put(RANG_04_LIBELLE, RANG_04);
		RANGS_MAP.put(RANG_05_LIBELLE, RANG_05);
		RANGS_MAP.put(RANG_06_LIBELLE, RANG_06);
	}

	private ITransfertDgcpPanelModel myModel;

	private final JRadioButton radioMensuel = new JRadioButton(RANG_MENSUELS);
	private final JRadioButton radio05 = new JRadioButton(RANG_05_LIBELLE);
	private final JRadioButton radio06 = new JRadioButton(RANG_06_LIBELLE);


	private final DefaultComboBoxModel comboRangModel;
	private final JComboBox comboRang;

	private ZDatePickerField dateRang05Field;
	private ZDatePickerField dateRang06Field;

	private final ButtonGroup groupRang = new ButtonGroup();
	private JComboBox myTypeFichier;
	private JComboBox myCodeBudget;

	private ButtonsListener buttonsListener = new ButtonsListener();
	private FileListTablePanel fileListTablePanel;

	private JCheckBox myBALCheckBox;
	private JCheckBox myEDDBCheckBox;
	private JCheckBox myEDRBCheckBox;
	private JCheckBox myECCBCheckBox;

	/**
     *
     */
	public TransfertDgcpPanel3(ITransfertDgcpPanelModel model) {
		super();
		myModel = model;
		fileListTablePanel = new FileListTablePanel(model.getFileListTablePanelMdl());

		comboRangModel = new DefaultComboBoxModel(RANGS_MAP.keySet().toArray());
		comboRangModel.removeElement(RANG_05_LIBELLE);
		comboRangModel.removeElement(RANG_06_LIBELLE);
		comboRang = new JComboBox(comboRangModel);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
	 */
	public void initGUI() {
		setLayout(new BorderLayout());
		
		dateRang06Field = new ZDatePickerField(new MyDatePickerFieldModel(myModel.getDicoValeurs(), "dateBalanceDef"), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateRang05Field = new ZDatePickerField(new MyDatePickerFieldModel(myModel.getDicoValeurs(), "dateBalanceProv"), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));

		dateRang06Field.getMyTexfield().setColumns(8);
		dateRang05Field.getMyTexfield().setColumns(8);

		dateRang05Field.setEnabled(false);
		dateRang06Field.setEnabled(false);

		groupRang.add(radioMensuel);
		groupRang.add(radio05);
		groupRang.add(radio06);



		radioMensuel.addActionListener(buttonsListener);
		radio05.addActionListener(buttonsListener);
		radio06.addActionListener(buttonsListener);

		radioMensuel.addActionListener(myModel.getComptabiliteListener());
		radio05.addActionListener(myModel.getComptabiliteListener());
		radio06.addActionListener(myModel.getComptabiliteListener());

		radioMensuel.setSelected(true);

		comboRang.addActionListener(buttonsListener);
		comboRang.addActionListener(myModel.getComptabiliteListener());

		JPanel panelRang;
		JPanel panelOptions;

		myTypeFichier = new JComboBox(myModel.getTypeFichiersModel());
		myTypeFichier.addActionListener(buttonsListener);
		myTypeFichier.addActionListener(myModel.getComptabiliteListener());
		myCodeBudget = new JComboBox(myModel.getCodeBudgetModel());
		myCodeBudget.addActionListener(myModel.getComptabiliteListener());

		myBALCheckBox = new JCheckBox("BAL");
		myEDDBCheckBox = new JCheckBox("EDDB");
		myEDRBCheckBox = new JCheckBox("EDRB");
		myECCBCheckBox = new JCheckBox("ECCB");

		myBALCheckBox.setToolTipText("Balance comptable");
		myEDDBCheckBox.setToolTipText("Développement des Dépenses Budgétaires");
		myEDRBCheckBox.setToolTipText("Développement des Recettes Budgétaires");
		myECCBCheckBox.setToolTipText("Consommation des Crédits Budgétaires");

		myBALCheckBox.setSelected(true);
		myEDDBCheckBox.setSelected(true);
		myEDRBCheckBox.setSelected(true);
		myECCBCheckBox.setSelected(true);

		myBALCheckBox.addActionListener(myModel.getComptabiliteListener());
		myEDDBCheckBox.addActionListener(myModel.getComptabiliteListener());
		myEDRBCheckBox.addActionListener(myModel.getComptabiliteListener());
		myECCBCheckBox.addActionListener(myModel.getComptabiliteListener());

		ArrayList listForm = new ArrayList();
		listForm.add(new Component[] {
				new JLabel("Type de fichier"), myTypeFichier
		});
		listForm.add(new Component[] {
				new JLabel("Code budget"), myCodeBudget
		});

		panelOptions = new JPanel(new BorderLayout());
		panelOptions.setBorder(BorderFactory.createTitledBorder(""));
		panelOptions.add(ZUiUtil.buildBoxLine(new Component[] {
				myBALCheckBox, myEDDBCheckBox, myEDRBCheckBox, myECCBCheckBox
		}), BorderLayout.NORTH);
		panelOptions.add(ZUiUtil.buildForm(listForm), BorderLayout.CENTER);

		///////////////////////////////////

		panelRang = new JPanel(new GridLayout(3, 0));
		panelRang.setBorder(BorderFactory.createTitledBorder("Rang"));
		panelRang.add(ZKarukeraPanel.buildLine(new Component[] {
				radioMensuel, Box.createHorizontalStrut(4), comboRang
		}));

		panelRang.add(ZKarukeraPanel.buildLine(new Component[] {
				radio05, Box.createHorizontalStrut(4), dateRang05Field
		}));

		panelRang.add(ZKarukeraPanel.buildLine(new Component[] {
				radio06, Box.createHorizontalStrut(4), dateRang06Field
		}));

		Component criteres = ZUiUtil.buildBoxLine(new Component[] {
				panelRang, panelOptions
		});
		JPanel criteresTop = new JPanel(new BorderLayout());
		final ZCommentPanel commentPanel = new ZCommentPanel("Génération des fichiers pour le transfert infocentre vers la DGCP - " + "Exercice " + ((EOExercice) myModel.getDicoValeurs().get("exercice")).exeExercice().intValue(),
				"<html>Indiquez les critères nécessaires à la génération des fichiers puis cliquez sur le bouton <b>\"Générer les fichiers\"</b>.<br>" +
						"Une fois les fichiers générés, enregistrez-les dans un répertoire de votre disque dur et utilisez la procédure de transfert</html>.", iconExport32);

		criteresTop.add(commentPanel, BorderLayout.NORTH);
		criteresTop.add(ZKarukeraPanel.encloseInPanelWithTitle("Options pour la génération des fichiers", null, ZConst.BG_COLOR_TITLE, criteres, null, null), BorderLayout.CENTER);

		ArrayList list = new ArrayList(3);
		list.add(myModel.actionGenererFichier());
		list.add(myModel.actionEnregistrerFichier());
		list.add(myModel.actionEnregistrerFichierCsv());

		JPanel btPanel = ZKarukeraPanel.buildHorizontalPanelOfComponent(ZKarukeraPanel.getButtonListFromActionList(list));

		btPanel.setBorder(BorderFactory.createEmptyBorder(4, 100, 4, 100));
		criteresTop.add(btPanel, BorderLayout.SOUTH);
		fileListTablePanel.initGUI();
		JPanel resPanel = ZKarukeraPanel.encloseInPanelWithTitle("Fichiers générés", null, ZConst.BG_COLOR_TITLE, fileListTablePanel, null, null);

		add(criteresTop, BorderLayout.NORTH);
		add(resPanel, BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);

	}

	/**
	 * @return le rang selectionné
	 */
	public final String getSelectedRang() {
		String res = null;
		JRadioButton sel = ZUiUtil.getSelectedRadioButton(groupRang);
		if (sel != null) {
			String key;
			if (RANG_MENSUELS.equals(sel.getText())) {
				key = (String) comboRangModel.getSelectedItem();
			}
			else {
				key = sel.getText();
			}
			res = (String) RANGS_MAP.get(key);
		}

		return res;
	}

	/**
	 * Renvoie la liste des prefixes cochés (EDDB, BAL etc.)
	 */
	public final ArrayList getSelectedPrefixes() {
		final ArrayList res = new ArrayList();

		if (myBALCheckBox.isSelected()) {
			res.add(PREFIXE_BAL);
		}
		if (myEDDBCheckBox.isSelected()) {
			res.add(PREFIXE_EDDB);
		}
		if (myEDRBCheckBox.isSelected()) {
			res.add(PREFIXE_EDRB);
		}
		if (myECCBCheckBox.isSelected()) {
			res.add(PREFIXE_ECCB);
		}
		return res;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myModel.actionFermer());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a), BorderLayout.CENTER);
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		System.out.println("TransfertDgcpPanel3.updateData()");
		dateRang05Field.updateData();
		dateRang06Field.updateData();
		fileListUpdateData();
		refreshButtons();

	}

	public void fileListUpdateData() throws Exception {
		fileListTablePanel.updateData();
	}

	private final void refreshButtons() {
		myCodeBudget.removeActionListener(myModel.getComptabiliteListener());
		if ("03".equals(myModel.getTypeFichierLibellesMap().get(myTypeFichier.getSelectedItem()))) {
			myCodeBudget.setEnabled(true);
		}
		else {
			myCodeBudget.setSelectedIndex(0);
			myCodeBudget.setEnabled(false);
		}

		ButtonModel selectedType = groupRang.getSelection();
		comboRang.setEnabled(false);
		dateRang05Field.setEnabled(false);
		dateRang06Field.setEnabled(false);
		if (selectedType.equals(radioMensuel.getModel())) {
			comboRang.setEnabled(true);
		}
		else if (selectedType.equals(radio05.getModel())) {
			dateRang05Field.setEnabled(true);
		}
		else if (selectedType.equals(radio06.getModel())) {
			dateRang06Field.setEnabled(true);
		}
		myCodeBudget.addActionListener(myModel.getComptabiliteListener());
	}

	public interface ITransfertDgcpPanelModel {
		public DefaultComboBoxModel getCodeBudgetModel();

		public Object actionEnregistrerFichierCsv();

		public IFileListTablePanelMdl getFileListTablePanelMdl();

		public String getNomFichier(String prefix);

		public EOExercice getExercice();

		public String getIdentifiantDGCP();

		public Map getTypeFichierLibellesMap();

		public Map getCodeBudgetLibellesMap();

		public DefaultComboBoxModel getTypeFichiersModel();

		public Dialog getDialog();

		public Action actionFermer();

		public Action actionGenererFichier();

		public Action actionEnregistrerFichier();

		public ActionListener getComptabiliteListener();

		public Map getDicoValeurs();
	}

	private final class ButtonsListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			refreshButtons();
		}

	}

	private class MyDatePickerFieldModel extends DefaultTextFieldModel implements IZDatePickerFieldModel {
		public MyDatePickerFieldModel(final Map filter, final String key) {
			super(filter, key);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}

	}

	public static final class FileListTablePanel extends ZTablePanel implements DragSourceListener, DragGestureListener {
		public static DataFlavor FILEFLAVOR = DataFlavor.javaFileListFlavor;
		public static final String NOM_FICHIER_KEY = "FILE_NAME";
		public static final String FICHIER_KEY = "FILE";
		public ZFileTransferHandler transferHandler;
		private DragSource dragSource = DragSource.getDefaultDragSource();

		public FileListTablePanel(IFileListTablePanelMdl listener) {
			super(listener);
			transferHandler = new ZFileTransferHandler();
			final ZEOTableModelColumn nomFichier = new ZEOTableModelColumn(myDisplayGroup, NOM_FICHIER_KEY, "Nom", 200);
			colsMap.clear();
			colsMap.put(NOM_FICHIER_KEY, nomFichier);
		}

		public void updateData() throws Exception {
			super.updateData();
		}

		public void initGUI() {
			super.initGUI();
			myEOTable.setTransferHandler(transferHandler);
			myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
			myEOTable.getTableHeader().setReorderingAllowed(false);
			dragSource.createDefaultDragGestureRecognizer(myEOTable, DnDConstants.ACTION_COPY, this);
			myEOTable.setUI(new MyTableUI());
		}

		/**
		 * @see java.awt.dnd.DragSourceListener#dragEnter(java.awt.dnd.DragSourceDragEvent)
		 */
		public void dragEnter(DragSourceDragEvent dsde) {
		}

		/**
		 * @see java.awt.dnd.DragSourceListener#dragOver(java.awt.dnd.DragSourceDragEvent)
		 */
		public void dragOver(DragSourceDragEvent dsde) {

		}

		/**
		 * @see java.awt.dnd.DragSourceListener#dropActionChanged(java.awt.dnd.DragSourceDragEvent)
		 */
		public void dropActionChanged(DragSourceDragEvent dsde) {

		}

		/**
		 * @see java.awt.dnd.DragSourceListener#dragDropEnd(java.awt.dnd.DragSourceDropEvent)
		 */
		public void dragDropEnd(DragSourceDropEvent dsde) {

		}

		/**
		 * @see java.awt.dnd.DragSourceListener#dragExit(java.awt.dnd.DragSourceEvent)
		 */
		public void dragExit(DragSourceEvent dse) {
		}

		/**
		 * @see java.awt.dnd.DragGestureListener#dragGestureRecognized(java.awt.dnd.DragGestureEvent)
		 */
		public void dragGestureRecognized(DragGestureEvent dge) {
			if (selectedObjects() == null) {
				return;
			}
			final FileSelection transferable = new FileSelection(selectedObjects());
			dge.startDrag(DragSource.DefaultCopyDrop, transferable, this);
		}

		public class FileSelection extends Vector implements Transferable {
			final static int FILE = 0;
			final static int STRING = 1;
			DataFlavor flavors[] = {
					DataFlavor.javaFileListFlavor, DataFlavor.stringFlavor
			};

			public FileSelection(NSArray files) {
				for (int i = 0; i < files.count(); i++) {
					NSKeyValueCoding array_element = (NSKeyValueCoding) files.objectAtIndex(i);
					addElement(array_element.valueForKey(FICHIER_KEY));
				}
			}

			/* Returns the array of flavors in which it can provide the data. */
			public synchronized DataFlavor[] getTransferDataFlavors() {
				return flavors;
			}

			/* Returns whether the requested flavor is supported by this object. */
			public boolean isDataFlavorSupported(final DataFlavor flavor) {
				System.out.println("FileSelection.isDataFlavorSupported()");
				boolean b = false;
				b |= flavor.equals(flavors[FILE]);
				b |= flavor.equals(flavors[STRING]);
				return (b);
			}

			/**
			 * If the data was requested in the "java.lang.String" flavor, return the String representing the selection.
			 */
			public synchronized Object getTransferData(final DataFlavor flavor) throws UnsupportedFlavorException, IOException {
				if (flavor.equals(flavors[FILE])) {
					return this;
				}
				else if (flavor.equals(flavors[STRING])) {
					return ((File) elementAt(0)).getAbsolutePath();
				}
				else {
					throw new UnsupportedFlavorException(flavor);
				}
			}
		}

		public final class ZFileTransferHandler extends TransferHandler {

			public ZFileTransferHandler() {
				System.out.println("ZFileTransferHandler.ZFileTransferHandler()");
			}

			/**
			 * @see javax.swing.TransferHandler#exportDone(javax.swing.JComponent, java.awt.datatransfer.Transferable, int)
			 */
			protected void exportDone(JComponent source, Transferable data, int action) {
				super.exportDone(source, data, action);
				//                System.out.println("ZFileTransferHandler.exportDone()");
			}

			/**
			 * @see javax.swing.TransferHandler#createTransferable(javax.swing.JComponent)
			 */
			protected Transferable createTransferable(JComponent c) {
				//                System.out.println("ZFileTransferHandler.createTransferable()");
				return super.createTransferable(c);
			}

			public boolean importData(JComponent c, Transferable t) {
				return false;
			}

			protected boolean hasFileFlavor(DataFlavor[] flavors) {
				//                System.out.println("ZFileTransferHandler.hasFileFlavor()");
				for (int i = 0; i < flavors.length; i++) {
					if (FILEFLAVOR.equals(flavors[i])) {
						return true;
					}
				}
				return false;
			}
		}

		public class MyTableUI extends BasicTableUI {

			protected MouseInputListener createMouseInputListener() {
				return new MyMouseInputHandler();
			}

			/*
			 * This class listens for selections on table rows. It prevents table row selections when mouse is dragged (default behaviour for multi
			 * row interval selections) and instead fires row drag (DND) events.
			 */
			class MyMouseInputHandler extends MouseInputHandler {

				private boolean ignoreDrag = false;

				public void mousePressed(MouseEvent e) {

					// Cancel the selected area if the user
					// clicks twice, otherwise if the user had
					// selected all of the cells they couldn't
					// de-select them with the mouse
					if (e.getClickCount() == 2) {
						table.clearSelection();
					}

					// Always set this to true - it will be
					// set if the user has clicked on a cell
					// that is already selected.
					ignoreDrag = true;

					// Found out whether the user has clicked
					// on a cell that it is already selected
					Point origin = e.getPoint();
					int row = table.rowAtPoint(origin);
					int column = table.columnAtPoint(origin);
					if (row != -1 && column != -1) {
						if (table.isCellSelected(row, column)) {
							// The cell is selected, so we
							// need to ignore the mouse drag
							// events completely.
							ignoreDrag = true;

						}
						else {
							// Only perform the usual mouse pressed
							// operation of the user has not clicked
							// on a cell that is already selected.
							super.mousePressed(e);
						}
					}
				}

				public void mouseDragged(MouseEvent e) {
					if (!ignoreDrag) {
						// We only call this method when a user has NOT clicked on a cell that is already selected 
						super.mouseDragged(e);
					}
					else {

						// Start a table row drag operation
						table.getTransferHandler().exportAsDrag(table, e, DnDConstants.ACTION_COPY);

					}
				}

				public void mouseReleased(MouseEvent e) {
					if (ignoreDrag) {
						// When clicking on an already selected row and releasing mouse => clear selection on table
						table.clearSelection();
					}

					super.mousePressed(e);
				}
			}

		}
	}

	public interface IFileListTablePanelMdl extends IZTablePanelMdl {
	}

}
