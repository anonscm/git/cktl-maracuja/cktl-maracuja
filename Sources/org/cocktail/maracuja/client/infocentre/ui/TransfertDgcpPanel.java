/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.infocentre.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZFileBox;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TransfertDgcpPanel extends ZKarukeraPanel {
    private final ImageIcon iconExport32=ZIcon.getIconForName(ZIcon.ICON_EXPORTDGCP_32);
    public static final String TRIMESTRE_1="1";
    public static final String TRIMESTRE_2="2";
    public static final String TRIMESTRE_3="3";
    public static final String TRIMESTRE_4="4";

    public static final String BALANCE_TRIMESTRE="0";
    public static final String BALANCE_PROVISOIRE="5";
    public static final String BALANCE_DEFINITIVE="6";


    private final Icon ICONE_FICHIER = ZIcon.getIconForName(ZIcon.ICON_TEXTFILE_32);
    private ITransfertDgcpPanelModel myModel;

    private final JRadioButton trimestre1 = new JRadioButton("1er trimestre");
    private final JRadioButton trimestre2 = new JRadioButton("2ème trimestre");
    private final JRadioButton trimestre3 = new JRadioButton("3ème trimestre");
    private final JRadioButton trimestre4 = new JRadioButton("4ème trimestre");

    private final JRadioButton balance_trimestre = new JRadioButton("Balances / Etats budgétaires trimestriels");
    private final JRadioButton balance_provisoire = new JRadioButton("Balance provisoire");
    private final JRadioButton balance_definitive = new JRadioButton("Balances définitive");

    private  ZDatePickerField dateBalanceProvField;
    private  ZDatePickerField dateBalanceDefField;
    
    
    private final ButtonGroup groupTrimestre = new ButtonGroup();
    private final ButtonGroup groupTypeExport = new ButtonGroup();

    private JComboBox myCodeGestionSacd;

    private ZFileBox fileBoxBalanceComptable;
    private ZFileBox fileBoxDepenseBudgetaire;
    private ZFileBox fileBoxRecetteBudgetaire;
    private ZFileBox fileBoxCreditBudgetaire;

    private ButtonTypeExpListener buttonTrimestreListener = new ButtonTypeExpListener();
    
    
    
    

    /**
     *
     */
    public TransfertDgcpPanel(ITransfertDgcpPanelModel model) {
        super();
        myModel = model;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());

//        dateBalanceDefField = new ZDatePickerField( new ZTextField.DefaultTextFieldModel(myModel.getDicoValeurs(), "dateBalanceDef"), (DateFormat)ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16),myModel.getDialog() );
//        dateBalanceProvField = new ZDatePickerField( new ZTextField.DefaultTextFieldModel(myModel.getDicoValeurs(), "dateBalanceProv"), (DateFormat)ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16),myModel.getDialog() );
        
        dateBalanceDefField = new ZDatePickerField( new MyDatePickerFieldModel(myModel.getDicoValeurs(), "dateBalanceDef"), (DateFormat)ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16) );
        dateBalanceProvField = new ZDatePickerField( new MyDatePickerFieldModel(myModel.getDicoValeurs(), "dateBalanceProv"), (DateFormat)ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16) );
        
        dateBalanceDefField.getMyTexfield().setColumns(8);
        dateBalanceProvField.getMyTexfield().setColumns(8);
        
        dateBalanceProvField.setEnabled(false);
        dateBalanceDefField.setEnabled(false);
        
        
        balance_trimestre.addActionListener(buttonTrimestreListener);
        balance_provisoire.addActionListener(buttonTrimestreListener);
        balance_definitive.addActionListener(buttonTrimestreListener);

        trimestre1.setName(TRIMESTRE_1);
        trimestre2.setName(TRIMESTRE_2);
        trimestre3.setName(TRIMESTRE_3);
        trimestre4.setName(TRIMESTRE_4);

        groupTrimestre.add(trimestre1);
        groupTrimestre.add(trimestre2);
        groupTrimestre.add(trimestre3);
        groupTrimestre.add(trimestre4);

        groupTypeExport.add(balance_trimestre);
        groupTypeExport.add(balance_provisoire);
        groupTypeExport.add(balance_definitive);

        
        trimestre1.setSelected(true);
        balance_trimestre.setSelected(true);

        JPanel panelPeriode;
        JPanel panelTypeExport;
        JPanel panelTypeComptabilite;

        panelPeriode = new JPanel(new GridLayout(4,1));
        panelPeriode.setBorder(BorderFactory.createTitledBorder("Période"));
        panelPeriode.add( ZKarukeraPanel.buildLine(trimestre1));
        panelPeriode.add(ZKarukeraPanel.buildLine(trimestre2));
        panelPeriode.add(ZKarukeraPanel.buildLine(trimestre3));
        panelPeriode.add(ZKarukeraPanel.buildLine(trimestre4));

        panelTypeExport = new JPanel(new GridLayout(4,1));
        panelTypeExport.setBorder(BorderFactory.createTitledBorder("Type d'export"));
        panelTypeExport.add(ZKarukeraPanel.buildLine(balance_trimestre));
        panelTypeExport.add(ZKarukeraPanel.buildLine(new Component[]{balance_provisoire, Box.createHorizontalStrut(4),dateBalanceProvField}));
        panelTypeExport.add(ZKarukeraPanel.buildLine(new Component[]{balance_definitive, Box.createHorizontalStrut(4),dateBalanceDefField}));
        panelTypeExport.add(new JPanel());

        myCodeGestionSacd = new JComboBox(myModel.getCodeGestionSacdModel());

        myCodeGestionSacd.addActionListener(myModel.getComptabiliteListener());

        System.out.println("listener : "+myCodeGestionSacd.getActionListeners());

        panelTypeComptabilite = new JPanel(new GridLayout(4,1));
        panelTypeComptabilite.setBorder(BorderFactory.createTitledBorder("Comptabilité"));
        panelTypeComptabilite.add(myCodeGestionSacd);
        panelTypeComptabilite.add(new JPanel());
        panelTypeComptabilite.add(new JPanel());
        panelTypeComptabilite.add(new JPanel());


        JPanel criteres = ZKarukeraPanel.buildHorizontalPanelOfComponent(new Component[]{panelPeriode, panelTypeExport,panelTypeComptabilite    });
        JPanel criteresTop = new JPanel(new BorderLayout());
        final ZCommentPanel commentPanel = new ZCommentPanel("Génération des fichiers pour le transfert infocentre vers la DGCP - " + "Exercice " + ((EOExercice)myModel.getDicoValeurs().get("exercice")).exeExercice().intValue() ,
        		"<html>Indiquez les critères nécessaires à la génération des fichiers puis cliquez sur le bouton <b>\"Générer les fichiers\"</b>.<br>" +
        		"Une fois les fichiers générés, enregistrez-les dans un répertoire de votre disque dur et utilisez la procédure de transfert</html>.", iconExport32);

        criteresTop.add(commentPanel, BorderLayout.NORTH);
        criteresTop.add(ZKarukeraPanel.encloseInPanelWithTitle("Critères",null,ZConst.BG_COLOR_TITLE,criteres, null, null), BorderLayout.CENTER);





        ArrayList list = new ArrayList(2);
        list.add(myModel.actionGenererFichier());
        list.add(myModel.actionEnregistrerFichier());


        JPanel btPanel = ZKarukeraPanel.buildHorizontalPanelOfComponent( ZKarukeraPanel.getButtonListFromActionList(list)  );

        btPanel.setBorder(BorderFactory.createEmptyBorder(4,100,4,100));
        criteresTop.add(btPanel,BorderLayout.SOUTH);

        ///////////////////

        fileBoxBalanceComptable = new ZFileBox(new FileBoxBalanceComptableModel());
        fileBoxDepenseBudgetaire = new ZFileBox(new FileBoxDepenseBudgetaireModel());
        fileBoxRecetteBudgetaire = new ZFileBox(new FileBoxRecetteBudgetaireModel());
        fileBoxCreditBudgetaire = new ZFileBox(new FileBoxCreditBudgetaireModel());


        JPanel panelBalanceComptable = new JPanel(new BorderLayout());
        JPanel panelDepenseBudgetaire = new JPanel(new BorderLayout());
        JPanel panelRecetteBudgetaire = new JPanel(new BorderLayout());
        JPanel panelCreditBudgetaire = new JPanel(new BorderLayout());

        panelBalanceComptable.setBorder(BorderFactory.createTitledBorder("Balance comptable"));
        panelDepenseBudgetaire.setBorder(BorderFactory.createTitledBorder("Dépenses budgétaire"));
        panelRecetteBudgetaire.setBorder(BorderFactory.createTitledBorder("Recettes budgétaires "));
        panelCreditBudgetaire.setBorder(BorderFactory.createTitledBorder("Crédit budgétaire"));

        panelBalanceComptable.add(fileBoxBalanceComptable);
        panelDepenseBudgetaire.add(fileBoxDepenseBudgetaire);
        panelRecetteBudgetaire.add(fileBoxRecetteBudgetaire);
        panelCreditBudgetaire.add(fileBoxCreditBudgetaire);



        JPanel bottomPanel = new JPanel(new GridLayout(1,4));
        bottomPanel.add(panelBalanceComptable);
        bottomPanel.add(panelDepenseBudgetaire);
        bottomPanel.add(panelRecetteBudgetaire);
        bottomPanel.add(panelCreditBudgetaire);


        JPanel resPanel = ZKarukeraPanel.encloseInPanelWithTitle("Fichiers générés", null,ZConst.BG_COLOR_TITLE,bottomPanel,null, null);


        add(criteresTop, BorderLayout.NORTH);
        add(resPanel, BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);

    }


    /**
     * @return le trimestre selectionné (constantes TRIMESTRE_1, TRIMESTRE_2, TRIMESTRE_3, TRIMESTRE_4)
     */
    public final String getSelectedTrimestre() {
        if (balance_trimestre.isSelected()) {
            if (trimestre1.isSelected()) {
                return TRIMESTRE_1;
            }
            else if (trimestre2.isSelected() ) {
                return TRIMESTRE_2;
            }
            else if (trimestre3.isSelected() ) {
                return TRIMESTRE_3;
            }
            else if (trimestre4.isSelected() ) {
                return TRIMESTRE_4;
            }
            else {
                return null;
            }
        }
        return TRIMESTRE_4;
    }

    public final String getSelectedTypeExport() {
        if (balance_trimestre.isSelected()) {
            return BALANCE_TRIMESTRE;
        }
        else if (balance_provisoire.isSelected() ) {
            return BALANCE_PROVISOIRE;
        }
        else if (balance_definitive.isSelected() ) {
            return BALANCE_DEFINITIVE;
        }
        else {
            return null;
        }
    }



    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(  myModel.actionFermer());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a), BorderLayout.CENTER);
        return p;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
     */
    public void updateData() throws Exception {
        fileBoxBalanceComptable.updateData();
        fileBoxDepenseBudgetaire.updateData();
        fileBoxRecetteBudgetaire.updateData();
        fileBoxCreditBudgetaire.updateData();
        dateBalanceProvField.updateData();
        dateBalanceDefField.updateData();
        refreshButtons();

    }





    private final void refreshButtons() {
        ButtonModel selectedType = groupTypeExport.getSelection();
        if (selectedType == null) {
            trimestre1.setEnabled(false);
            trimestre2.setEnabled(false);
            trimestre3.setEnabled(false);
            trimestre4.setEnabled(false);
        }
        else {
            if (selectedType.equals( balance_trimestre.getModel()  )) {
                trimestre1.setEnabled(true);
                trimestre2.setEnabled(true);
                trimestre3.setEnabled(true);
                trimestre4.setEnabled(true);
            }
            else {
                trimestre1.setEnabled(false);
                trimestre2.setEnabled(false);
                trimestre3.setEnabled(false);
                trimestre4.setEnabled(false);
            }
        }
        
        
        if (selectedType.equals( balance_provisoire.getModel()  )) {
            dateBalanceProvField.setEnabled(true);
            dateBalanceDefField.setEnabled(false);
        }
        else if (selectedType.equals( balance_definitive.getModel()  )) {
            dateBalanceProvField.setEnabled(false);
            dateBalanceDefField.setEnabled(true);
        }
        else {
            dateBalanceProvField.setEnabled(false);
            dateBalanceDefField.setEnabled(false);            
        }
        
    }




    public interface ITransfertDgcpPanelModel {
        public DefaultComboBoxModel getCodeGestionSacdModel();
        public Dialog getDialog();
        public Action actionFermer();
        public Action actionGenererFichier();
        public Action actionEnregistrerFichier();
        public File getFileCreditBudgetaire();
        public File getFileRecetteBudgetaire();
        public File getFileDepenseBudgetaire();
        public File getFileBalanceComptable();
        public ActionListener getComptabiliteListener();
        public Map getDicoValeurs();
    }



    private final class FileBoxBalanceComptableModel implements ZFileBox.IZFileBoxModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getFile()
         */
        public File getFile() {
            return myModel.getFileBalanceComptable();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getText()
         */
        public String getText() {
            return null;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getIcon()
         */
        public Icon getIcon() {
            return ICONE_FICHIER;
        }

        public boolean canDrop() {
            return false;
        }

        public void setFile(File f) {
            
        }

    }

    private final class FileBoxDepenseBudgetaireModel implements ZFileBox.IZFileBoxModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getFile()
         */
        public File getFile() {
            return myModel.getFileDepenseBudgetaire();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getText()
         */
        public String getText() {
            return null;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getIcon()
         */
        public Icon getIcon() {
            return ICONE_FICHIER;
        }

        public boolean canDrop() {
            return false;
        }

        public void setFile(File f) {
            
        }

    }

    private final class FileBoxRecetteBudgetaireModel implements ZFileBox.IZFileBoxModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getFile()
         */
        public File getFile() {
            return myModel.getFileRecetteBudgetaire();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getText()
         */
        public String getText() {
            return null;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getIcon()
         */
        public Icon getIcon() {
            return ICONE_FICHIER;
        }

        public boolean canDrop() {
            return false;
        }

        public void setFile(File f) {
            
        }

    }
    private final class FileBoxCreditBudgetaireModel implements ZFileBox.IZFileBoxModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getFile()
         */
        public File getFile() {
            return myModel.getFileCreditBudgetaire();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getText()
         */
        public String getText() {
            return null;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel#getIcon()
         */
        public Icon getIcon() {
            return ICONE_FICHIER;
        }

        public boolean canDrop() {
            return false;
        }

        public void setFile(File f) {
            
        }

    }




    private final class ButtonTypeExpListener implements ActionListener {

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            refreshButtons();
        }

    }

    private class MyDatePickerFieldModel extends DefaultTextFieldModel implements IZDatePickerFieldModel {
        public MyDatePickerFieldModel(final Map filter, final String key) {
            super(filter, key);
        }
        
        public Window getParentWindow() {
            return getMyDialog();
        }
        
    }   

}
