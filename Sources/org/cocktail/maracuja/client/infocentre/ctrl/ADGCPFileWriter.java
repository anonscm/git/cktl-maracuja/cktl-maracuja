/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright COCKTAIL, 1995-2008 This software is governed by the CeCILL license
 * under French law and abiding by the rules of distribution of free software.
 * You can use, modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.infocentre.ctrl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.zutil.client.ZFlatTextFilewriter;

import com.webobjects.foundation.NSKeyValueCoding;

/**
*
* @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
* Classe abstraire pour l'ecriture des fichiers de transfert DGCP.
*/

abstract class ADGCPFileWriter extends ZFlatTextFilewriter {
    /**
	 * 
	 */
	protected Format FORMAT_DATE= new SimpleDateFormat("ddMMyyyy");
    protected Format FORMAT_NUMBER= new DecimalFormat("0000");

    /**
     * @param file
     * @throws IOException
     */
    public ADGCPFileWriter(File file) throws IOException {
        super(file);
    }
    

    public ADGCPFileWriter(File file, String colSeparator) throws IOException {
		super(file, colSeparator);
	}


	/**
     * Les nombres sont formates sans separateur de decimales, alignes à droite avec le format "signe + 00000 +nnnn"
     * @param value
     * @param nbCharacters
     * @return
     * @throws Exception
     */
    public String encodeNumber(Object value, final int nbCharacters) throws Exception {
        if (NSKeyValueCoding.NullValue.equals(value) || value ==null || !(value instanceof Number)) {
            value = new Integer(0);
        }

        BigDecimal nb = new BigDecimal( ((Number)value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
        nb = nb.multiply(new BigDecimal(100));
        String res = FORMAT_NUMBER.format(nb.abs());
        String signe= (nb.signum()<0 ? "-" : "0") ;
        if (res.length()>nbCharacters-1) {
            throw new Exception("ERREUR : Le nombre " + value +" est trop grand pour être formaté sur " + nbCharacters +" caractères.");
        }
        return signe + encodeStringAlignRight(res,nbCharacters-1, "0");
    }

    /**
     * Les dates sont formatees en JJMMAAAA.
     * @throws Exception
     * @see org.cocktail.zutil.client.ZFlatTextFilewriter#encodeDate(java.util.Date, int)
     */
    public String encodeDate(final Date value, final int nbCharacters) throws Exception {
        return encodeStringAlignLeft(FORMAT_DATE.format(value), nbCharacters);
    }

}
