/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.infocentre.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3;
import org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3.IFileListTablePanelMdl;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestionAgregat;
import org.cocktail.maracuja.client.metier.EOGestionExercice;
import org.cocktail.zutil.client.StringCtrl;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TransfertDgcpCtrl3 extends CommonCtrl {
	private static final String MSG_VEUILLEZ_PATIENTER = "Veuillez patienter...";

	private static final String TITLE = "Transfert DGFIP - Remontées Infocentre";
	private static final Dimension WINDOW_DIMENSION = new Dimension(820, 450);
	/** Nom du user dans lequel se trouvent les procedures tables à interroger */
	private final String USER_EPN = "EPN.";

	private static final String PARAM_REGROUPEMENT_SACD = "org.cocktail.gfc.maracuja.epn.traiterregroupementssuivantcommesacdaveccodebuget";

	private static final String CODE_BUDGET_TOUS = "??";
	private static final String CODE_BUDGET_01 = "01";
	private static final String CODE_BUDGET_TOUS_LIBELLE = "Tous";
	private static final String CODE_BUDGET_01_LIBELLE = "Comptabilité principale";

	private static final String TYPE_FICHIER_TOUS = "??";
	private static final String TYPE_FICHIER_01 = "01";
	private static final String TYPE_FICHIER_02 = "02";
	private static final String TYPE_FICHIER_03 = "03";
	private static final String TYPE_FICHIER_TOUS_LIBELLE = "Tous";
	private static final String TYPE_FICHIER_01_LIBELLE = "01 (Consolidé)";
	private static final String TYPE_FICHIER_02_LIBELLE = "02 (Agrégé : Etab + SACDs)";
	private static final String TYPE_FICHIER_03_LIBELLE = "03 (Détaillé : Etab ou SACD)";

	private static final String KEY_IDENTIFIANT_DGCP = "05_identifiantDGCP";
	private static final String KEY_TYPE_FICHIER = "10_typeFichier";
	private static final String KEY_CODE_BUDGET = "15_codeBudget";
	private static final String KEY_EXE_ORDRE = "20_exeOrdre";
	private static final String KEY_RANG = "25_rang";
	private static final String KEY_GES_CODE_SACD = "30_gesCodeSACD";
	private static final String KEY_V_DATE = "35_v_date";

	private static final String FICHIER_BAL = "BAL";
	private static final String FICHIER_EDDB = "EDDB";
	private static final String FICHIER_EDRB = "EDRB";
	private static final String FICHIER_ECCB = "ECCB";

	public static final ImageIcon iconExport16 = ZIcon.getIconForName(ZIcon.ICON_EXPORTDGCP_16);

	private final ActionClose actionClose = new ActionClose();
	private final ActionGenererFichier actionGenererFichier = new ActionGenererFichier();
	private final ActionEnregistrerFichierCSV actionGenererFichierCsv = new ActionEnregistrerFichierCSV();
	private final ActionEnregistrerFichier actionEnregistrerFichier = new ActionEnregistrerFichier();
	private TransfertDgcpPanel3 myPanel;
	private CodeBudgetModel codeBudgetModel;
	private TypeFichiersModel typeFichiersModel;

	private final HashMap<String, Date> datesFinMois = new HashMap<String, Date>(4);
	private ZWaitingPanelDialog waitingPanelDialog;

	private Map<String, Serializable> dicoValeurs = new HashMap<String, Serializable>();

	private Integer exeOrdre;
	private String identifiantDGCP;

	private final ComptabiliteListener comptabiliteListener = new ComptabiliteListener();

	private HashMap<String, String> codeBudgetMap = new LinkedHashMap<String, String>();
	private HashMap<String, String> codeBudgetLibellesMap = new LinkedHashMap<String, String>();
	private HashMap<String, String> typeFichierLibellesMap = new LinkedHashMap<String, String>();

	private FileListTablePanelMdl fileListTablePanelMdl = new FileListTablePanelMdl();

	private Date laDateBalance;
	private NSArray eoagregats;

	/**
	 * @param editingContext
	 */
	public TransfertDgcpCtrl3(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		construireCodeBudgetMap();

		codeBudgetModel = new CodeBudgetModel();
		typeFichiersModel = new TypeFichiersModel();

		int exer = getExercice().exeExercice().intValue();
		List<Date> finMois = DateCtrl.getLastDaysOfMonth(exer);
		datesFinMois.put(TransfertDgcpPanel3.RANG_J1, finMois.get(0));
		datesFinMois.put(TransfertDgcpPanel3.RANG_F1, finMois.get(1));
		datesFinMois.put(TransfertDgcpPanel3.RANG_01, finMois.get(2));
		datesFinMois.put(TransfertDgcpPanel3.RANG_A2, finMois.get(3));
		datesFinMois.put(TransfertDgcpPanel3.RANG_M2, finMois.get(4));
		datesFinMois.put(TransfertDgcpPanel3.RANG_02, finMois.get(5));
		datesFinMois.put(TransfertDgcpPanel3.RANG_J3, finMois.get(6));
		datesFinMois.put(TransfertDgcpPanel3.RANG_A3, finMois.get(7));
		datesFinMois.put(TransfertDgcpPanel3.RANG_03, finMois.get(8));
		datesFinMois.put(TransfertDgcpPanel3.RANG_O4, finMois.get(9));
		datesFinMois.put(TransfertDgcpPanel3.RANG_N4, finMois.get(10));
		datesFinMois.put(TransfertDgcpPanel3.RANG_04, finMois.get(11));

		dicoValeurs.put("exercice", getExercice());
		dicoValeurs.put("dateBalanceDef", ZDateUtil.stringToDate("30/04/" + (getExercice().exeExercice().intValue() + 1)));
		dicoValeurs.put("dateBalanceProv", ZDateUtil.stringToDate("21/01/" + (getExercice().exeExercice().intValue() + 1)));

		exeOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), getExercice()).valueForKey("exeOrdre");
		identifiantDGCP = (String) myApp.getParametres().valueForKey("IDENTIFIANT_DGCP");

		if (identifiantDGCP == null || identifiantDGCP.length() == 0) {
			throw new DataCheckException("Le parametre IDENTIFIANT_DGCP n'est pas défini dans la table PARAMETRE de la base MARACUJA pour l'exercice " + getExercice().exeExercice()
					+ " . Veuillez demander à un informaticien de le renseigner. Cet identifiant est fourni par la DGCP et identifie votre établissement.");
		}
		if (identifiantDGCP.length() != 10) {
			throw new DataCheckException("Le parametre IDENTIFIANT_DGCP dans la table PARAMETRE de la base MARACUJA pour l'exercice " + getExercice().exeExercice()
					+ " doit comporter 10 caracteres. Veuillez demander à un informaticien de le corriger. Cet identifiant est fourni par la DGCP et identifie votre établissement.");
		}

		myPanel = new TransfertDgcpPanel3(new TransfertDgcpModel());
	}

	/**
	 * @return Les code budgets des SACD pour l'exercice. Sous la forme d'un tableeau de NSDIctionary.
	 */
	private NSArray getCodeBudgetsSACD() throws Exception {
		String sql = "select g.GES_CODE, c.COD_BUD from maracuja.gestion_exercice g, epn.code_budget_sacd c " +
				"where g.exe_ordre=c.exe_ordre(+) and " +
				"g.pco_num_185 is not null and " +
				"g.exe_ordre=" + getExercice().exeExercice().intValue() + " and " +
				"g.ges_code=c.sacd(+)";
		ZLogger.verbose(sql);
		return ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final void checkChoix() throws Exception {
		laDateBalance = null;
		Date ojourdui = ZDateUtil.getTodayAsCalendar().getTime();
		String rang = myPanel.getSelectedRang();
		if (datesFinMois.get(rang) != null) {
			if (ojourdui.before(datesFinMois.get(rang))) {
				throw new DataCheckException("Cet export ne peut être effectué avant la fin du mois ( " + ZConst.FORMAT_DATESHORT.format(datesFinMois.get(rang)) + "  )");
			}
		}

		//        String rang = myPanel.getSelectedRang();
		Date dateBalance = null;
		if (TransfertDgcpPanel3.RANG_05.equals(rang)) {
			dateBalance = (Date) dicoValeurs.get("dateBalanceProv");
			//Vérifier que l'exercice est bien en mode Restreint
			if (!getExercice().estRestreint()) {
				throw new DataCheckException("Il n'est pas possible de sortir une balance provisoire car l'exercice " + getExercice().exeExercice().intValue() + " n'est pas en mode " + EOExercice.EXE_ETAT_RESTREINT_LIBELLE);
			}
			if (dateBalance == null) {
				throw new DataCheckException("La date est obligatoire.");
			}
		}
		else if (TransfertDgcpPanel3.RANG_06.equals(rang)) {
			dateBalance = (Date) dicoValeurs.get("dateBalanceDef");
			if (!getExercice().estClos()) {
				throw new DataCheckException("Il n'est pas possible de sortir une balance définitive car l'exercice " + getExercice().exeExercice().intValue() + " n'est pas en mode " + EOExercice.EXE_ETAT_CLOS_LIBELLE);
			}
			if (dateBalance == null) {
				throw new DataCheckException("La date est obligatoire.");
			}
		}
		else {
			dateBalance = datesFinMois.get(rang);
		}

		if (dateBalance != null && dateBalance.getTime() > ZDateUtil.getDateOnly(ZDateUtil.nowAsDate()).getTime()) {
			throw new DataCheckException("Vous ne pouvez pas utiliser une date de balance (" + ZConst.FORMAT_DATESHORT.format(dateBalance) + ") ultérieure à la date du jour.");
		}

		laDateBalance = dateBalance;

	}

	private final void enregistrerFichiers() {
		try {
			File dir = new File(myApp.homeDir);
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setDialogTitle("Sélectionnez le répertoire de destination ...");
			fileChooser.setCurrentDirectory(dir);
			if (fileChooser.showSaveDialog(getMyDialog()) == JFileChooser.APPROVE_OPTION) {
				dir = fileChooser.getCurrentDirectory();
				if (dir.exists()) {
					//On copie les fichiers
					File dest;
					NSArray files = fileListTablePanelMdl.getFiles();

					for (int i = 0; i < files.count(); i++) {
						File f = (File) files.objectAtIndex(i);
						dest = new File(dir, f.getName());
						ZFileUtil.fileCopy(f, dest);
					}

					showInfoDialog("Les fichiers ont été enregistrés dans le répertoire " + dir.getAbsolutePath());
				}
				else {
					throw new DefaultClientException("Le répertoire " + dir.getAbsolutePath() + " n'existe pas.");
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void enregistrerFichiersCSV() {
		try {
			File dir = new File(myApp.homeDir);
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fileChooser.setMultiSelectionEnabled(false);
			fileChooser.setDialogTitle("Sélectionnez le répertoire de destination ...");
			fileChooser.setCurrentDirectory(dir);
			if (fileChooser.showSaveDialog(getMyDialog()) == JFileChooser.APPROVE_OPTION) {
				dir = fileChooser.getCurrentDirectory();
				if (dir.exists()) {
					File dest;
					NSArray files = fileListTablePanelMdl.getFiles();
					for (int i = 0; i < files.count(); i++) {
						File f = (File) files.objectAtIndex(i);
						PreparationFichier preparationFichier = fileListTablePanelMdl.getFilePreparationMap().get(f);
						f = preparationFichier.genereFichierXls();
						dest = new File(dir, f.getName());
						ZFileUtil.fileCopy(f, dest);
					}

					showInfoDialog("Les fichiers ont été enregistrés dans le répertoire " + dir.getAbsolutePath());
				}
				else {
					throw new DefaultClientException("Le répertoire " + dir.getAbsolutePath() + " n'existe pas.");
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void clearFiles() {
		try {
			fileListTablePanelMdl.clear();
			actionEnregistrerFichier.setEnabled(false);
			actionGenererFichierCsv.setEnabled(false);
			myPanel.fileListUpdateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void genererFichiers() {
		if (myPanel.getSelectedPrefixes().size() == 0) {
			showInfoDialog("Vous devez cocher au moins un fichier à générer (BAL, EDDB, etc.)");
			return;
		}

		if (fileListTablePanelMdl.getFiles().count() > 0) {
			if (!showConfirmationDialog("Confirmation", "Des fichiers ont déjà été générés, souhaitez-vous les écraser ? ", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		try {
			clearFiles();
			checkChoix();

			waitingPanelDialog = new ZWaitingPanelDialog(null, this.getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingPanelDialog.setTitle(MSG_VEUILLEZ_PATIENTER);
			waitingPanelDialog.setTopText(MSG_VEUILLEZ_PATIENTER);
			waitingPanelDialog.setBottomText("Génération des fichiers en cours");
			waitingPanelDialog.getMyProgressBar().setIndeterminate(false);
			waitingPanelDialog.getMyProgressBar().setMaximum(4);
			waitingPanelDialog.setModal(true);

			PreparationFichiers preparationFichiers = new PreparationFichiers();
			preparationFichiers.start();
			//            Thread.yield();
			waitingPanelDialog.setVisible(true);
			//On attend...

			if (preparationFichiers.getLastException() != null) {
				throw preparationFichiers.getLastException();
			}
			actionEnregistrerFichier.setEnabled(true);
			actionGenererFichierCsv.setEnabled(true);
			try {
				myPanel.updateData();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

			showInfoDialog("Les fichiers ont été générés. Vous pouvez effectuer un glisser-déposer des fichiers vers le bureau par exemple pour les transférer.");
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			if (waitingPanelDialog != null) {
				waitingPanelDialog.setVisible(false);
			}
			try {
				myPanel.updateData();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	public final class ActionGenererFichier extends AbstractAction {

		public ActionGenererFichier() {
			super("Générer les fichiers");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			genererFichiers();
		}
	}

	public final class ActionEnregistrerFichier extends AbstractAction {

		public ActionEnregistrerFichier() {
			super("Enregistrer les fichiers...");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			enregistrerFichiers();
		}

	}

	public final class ActionEnregistrerFichierCSV extends AbstractAction {

		public ActionEnregistrerFichierCSV() {
			super("Enregistrer (CSV)...");

			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les fichiers au format CSV, vous pourrez ainsi les controler dans Excel.");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			enregistrerFichiersCSV();
		}

	}

	private final class TransfertDgcpModel implements TransfertDgcpPanel3.ITransfertDgcpPanelModel {

		/**
		 * @see org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3.ITransfertDgcpPanelModel#getCodeGestionSacdModel()
		 */
		public DefaultComboBoxModel getCodeBudgetModel() {
			return codeBudgetModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3.ITransfertDgcpPanelModel#actionGenererFichier()
		 */
		public Action actionGenererFichier() {
			return actionGenererFichier;
		}

		/**
		 * @see org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3.ITransfertDgcpPanelModel#actionFermer()
		 */
		public Action actionFermer() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3.ITransfertDgcpPanelModel#actionEnregistrerFichier()
		 */
		public Action actionEnregistrerFichier() {
			return actionEnregistrerFichier;
		}

		/**
		 * @see org.cocktail.maracuja.client.infocentre.ui.TransfertDgcpPanel3.ITransfertDgcpPanelModel#getComptabiliteListener()
		 */
		public ActionListener getComptabiliteListener() {
			return comptabiliteListener;
		}

		public Dialog getDialog() {
			return getMyDialog();
		}

		public Map<String, Serializable> getDicoValeurs() {
			return dicoValeurs;
		}

		public DefaultComboBoxModel getTypeFichiersModel() {
			return typeFichiersModel;
		}

		public Map<String, String> getCodeBudgetLibellesMap() {
			return codeBudgetLibellesMap;
		}

		public EOExercice getExercice() {
			return TransfertDgcpCtrl3.this.getExercice();
		}

		public String getIdentifiantDGCP() {
			return identifiantDGCP;
		}

		public Map<String, String> getTypeFichierLibellesMap() {
			return typeFichierLibellesMap;
		}

		public String getNomFichier(String prefix) {
			String typeFichier = typeFichierLibellesMap.get(typeFichiersModel.getSelectedItem());
			String codeBudget = (String) codeBudgetLibellesMap.get(codeBudgetModel.getSelectedItem());
			String exercice = getExercice().exeExercice().toString();
			String rang = myPanel.getSelectedRang();
			return prefix + "_" + identifiantDGCP + "_" + typeFichier + "_" + codeBudget + "_" + exercice + "_" + rang;
		}

		public IFileListTablePanelMdl getFileListTablePanelMdl() {
			return fileListTablePanelMdl;
		}

		public Object actionEnregistrerFichierCsv() {
			return actionGenererFichierCsv;
		}

	}

	private final class PreparationFichiers extends Thread {
		private NSMutableDictionary dico = new NSMutableDictionary();
		private Exception lastException;

		public PreparationFichiers() {

		}

		public void faireTraitement() throws Exception {
			ArrayList<String> typeFichiers = typeFichiersModel.getSelectedTypeFichiers();
			ArrayList<Object> codeBudgets = codeBudgetModel.getSelectedCodeBudgets();
			Date dateBalance = laDateBalance;
			String exercice = getExercice().exeExercice().toString();
			String rang = myPanel.getSelectedRang();

			int maxStep = typeFichiers.size() * codeBudgets.size() * myPanel.getSelectedPrefixes().size();
			int step = 0;
			waitingPanelDialog.getMyProgressBar().setMaximum(maxStep);
			Iterator<String> iterTypes = typeFichiers.iterator();
			while (iterTypes.hasNext()) {
				String typeFichier = iterTypes.next();
				Iterator<Object> iterCodeBudgets = codeBudgets.iterator();

				while (iterCodeBudgets.hasNext()) {
					String codeBudget = (String) iterCodeBudgets.next();
					//si on n'est pas sur un fichier detaille et que le code budget est different de l'agence principale, on ne genere rien
					if (!TYPE_FICHIER_03.equals(typeFichier) && !CODE_BUDGET_01.equals(codeBudget)) {
						ZLogger.verbose("!TYPE_FICHIER_03.equals(typeFichier) && !CODE_BUDGET_01.equals(codeBudget)");
						waitingPanelDialog.getMyProgressBar().setValue(step++);
					}
					else {
						String selectedSACD = null;
						if (!CODE_BUDGET_01.equals(codeBudget)) {
							selectedSACD = codeBudgetMap.get(codeBudget);
						}

						dico.removeAllObjects();
						dico.takeValueForKey(identifiantDGCP, KEY_IDENTIFIANT_DGCP);
						dico.takeValueForKey(typeFichier, KEY_TYPE_FICHIER);
						dico.takeValueForKey(codeBudget, KEY_CODE_BUDGET);
						dico.takeValueForKey(exeOrdre, KEY_EXE_ORDRE);
						dico.takeValueForKey(rang, KEY_RANG);
						dico.takeValueForKey(selectedSACD, KEY_GES_CODE_SACD);
						if (dateBalance != null) {
							dico.takeValueForKey(new SimpleDateFormat("ddMMyyyy").format(dateBalance), KEY_V_DATE);
						}
						ZLogger.debug(dico);

						Iterator iterPrefixes = myPanel.getSelectedPrefixes().iterator();
						while (iterPrefixes.hasNext()) {
							String prefixe = (String) iterPrefixes.next();

							String nomFichier = prefixe + "_" + identifiantDGCP + "_" + typeFichier + "_" + codeBudget + "_" + exercice + "_" + rang;

							PreparationFichier preparationFichier = null;
							if (FICHIER_BAL.equals(prefixe)) {
								preparationFichier = new PreparationFichierBAL(dico, nomFichier);
							}
							else if (FICHIER_EDDB.equals(prefixe)) {
								preparationFichier = new PreparationFichierEDDB(dico, nomFichier);
							}
							else if (FICHIER_EDRB.equals(prefixe)) {
								preparationFichier = new PreparationFichierEDRB(dico, nomFichier);
							}
							else if (FICHIER_ECCB.equals(prefixe)) {
								preparationFichier = new PreparationFichierECCB(dico, nomFichier);
							}
							preparationFichier.faireTraitement();
							File fichier = preparationFichier.genereFichierText();
							System.out.println("Fichier genere : " + fichier.getPath());

							//				            preparationFichier.genereFichierXls();
							fileListTablePanelMdl.addFile(fichier);
							fileListTablePanelMdl.getFilePreparationMap().put(fichier, preparationFichier);
							waitingPanelDialog.getMyProgressBar().setValue(step++);
						}
					}
				}
			}
		}

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			lastException = null;
			try {
				faireTraitement();
			} catch (Exception e) {
				lastException = e;
			} finally {
				if (waitingPanelDialog != null) {
					waitingPanelDialog.setVisible(false);
				}
			}
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	private final NSArray execSQL(final String sql) throws Exception {
		ZLogger.debug("sql", sql);
		NSArray res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sql);
		if (res == null) {
			throw new DefaultClientException("ERREUR : La requete sql n'a rien renvoyé");
		}
		return res;
	}

	private abstract class PreparationFichier {
		public static final String FORMAT_DGCP = "DGCP";
		public static final String FORMAT_CSV = "CSV";

		public abstract void faireTraitement() throws Exception;

		public abstract File genereFichierText() throws Exception;

		public abstract File genereFichierXls() throws Exception;

		public abstract NSDictionary getDico();

	}

	private final class PreparationFichierBAL extends PreparationFichier {
		private final String PROC_NAME = "Epn3_epn_genere_bal";
		private NSDictionary dico;
		private String nomFichier;

		public PreparationFichierBAL(NSDictionary _dico, String nomFichier) {
			this.nomFichier = nomFichier;
			dico = _dico.mutableClone();
		}

		public void faireTraitement() throws Exception {
			//appel procedure pour balance
			System.out.println("PreparationFichierBalance.faireTraitement()");
			System.out.println("exec de la procedure " + PROC_NAME);
			ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), PROC_NAME, dico);
		}

		private File genererFichier(String format) throws Exception {
			File fichier;
			ADGCPFileWriter fileWriter;
			if ("CSV".equals(format)) {
				fichier = new File(myApp.temporaryDir, nomFichier + ".csv");
				fileWriter = new DGCPBALFileWriter(fichier, ",");
			}
			else {
				fichier = new File(myApp.temporaryDir, nomFichier + ".bdf");
				fileWriter = new DGCPBALFileWriter(fichier);
			}

			//Récupérer les enregistrements de la table resultats
			String sql1 = "select * from " + USER_EPN + "epn_bal_1 where EPNB1_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNB1_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNB1_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'";
			NSArray res = execSQL(sql1);
			if (res.count() > 1) {
				throw new DefaultClientException("ERREUR : La requete sql a renvoyé plus d'un enregistrement");
			}
			Map mapHead = ((NSDictionary) res.objectAtIndex(0)).hashtable();
			String sql2 = "select * from " + USER_EPN + "epn_bal_2 where EPNB2_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNB2_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNB2_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "' order by EPNB2_NUMERO";
			NSArray res2 = execSQL(sql2);
			if (res2.count() == 0) {
				showWarningDialog("Attention, le fichier Balance est vide pour le code budget " + dico.valueForKey(KEY_CODE_BUDGET) + ". Vérifiez si c'est normal avant d'envoyer les fichiers.");
			}
			ArrayList lignes = ZEOUtilities.convertNSArrayOfNSDictionaryToArrayListOfMap(res2);
			//          creation du fichier
			try {
				fileWriter.writeHeader(mapHead);
				fileWriter.flush();
				fileWriter.writeLines(lignes);
				fileWriter.flush();
				fileWriter.writeFooter(mapHead);
				fileWriter.flush();
				fileWriter.close();
				if (fichier.length() <= 0) {
					throw new DefaultClientException("Le fichier est vide.");
				}
			} catch (Exception e) {
				fileWriter.close();
				throw e;
			}
			return fichier;
		}

		public File genereFichierText() throws Exception {
			return genererFichier(FORMAT_DGCP);

		}

		public File genereFichierXls() throws Exception {
			return genererFichier(FORMAT_CSV);
		}

		public NSDictionary getDico() {
			return dico;
		}

	}

	private final class PreparationFichierEDDB extends PreparationFichier {
		private final String PROC_NAME = "Epn3_epn_genere_eddb";
		private NSDictionary dico;
		private String nomFichier;

		public PreparationFichierEDDB(NSDictionary _dico, String nomFichier) {
			this.nomFichier = nomFichier;
			dico = _dico.mutableClone();
		}

		public void faireTraitement() throws Exception {
			System.out.println("exec de la procedure " + PROC_NAME);
			ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), PROC_NAME, dico);

		}

		public File genererFichier(String format) throws Exception {
			File fichier;
			ADGCPFileWriter fileWriter;
			if ("CSV".equals(format)) {
				fichier = new File(myApp.temporaryDir, nomFichier + ".csv");
				fileWriter = new DGCPEDDBFileWriter(fichier, ",");
			}
			else {
				fichier = new File(myApp.temporaryDir, nomFichier + ".bdf");
				fileWriter = new DGCPEDDBFileWriter(fichier);
			}
			//Récupérer les enregistrements de la table resultats
			String sql1 = "select * from " + USER_EPN + "epn_dep_bud_1 where EPNdB1_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNdB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNdB1_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNdB1_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'";
			NSArray res = execSQL(sql1);
			if (res.count() > 1) {
				throw new DefaultClientException("ERREUR : La requete sql a renvoyé plus d'un enregistrement");
			}
			Map mapHead = ((NSDictionary) res.objectAtIndex(0)).hashtable();

			String sql2 = "select * from " + USER_EPN + "epn_dep_bud_2 where EPNdB2_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNdB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNdB2_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNdB2_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'" + " order by EPNDB2_NUMERO";
			NSArray res2 = execSQL(sql2);
			if (res2.count() == 0) {
				showWarningDialog("Attention, le fichier Dépense budgétaire est vide pour le code budget " + dico.valueForKey(KEY_CODE_BUDGET) + ". Vérifiez si c'est normal avant d'envoyer les fichiers.");
			}
			ArrayList lignes = ZEOUtilities.convertNSArrayOfNSDictionaryToArrayListOfMap(res2);

			//          creation du fichier
			try {
				fileWriter.writeHeader(mapHead);
				fileWriter.flush();
				fileWriter.writeLines(lignes);
				fileWriter.flush();
				fileWriter.writeFooter(mapHead);
				fileWriter.flush();
				fileWriter.close();
				if (fichier.length() <= 0) {
					throw new DefaultClientException("Le fichier est vide.");
				}
			} catch (Exception e) {
				fileWriter.close();
				throw e;
			}
			return fichier;
		}

		public File genereFichierText() throws Exception {
			return genererFichier(FORMAT_DGCP);

		}

		public File genereFichierXls() throws Exception {
			return genererFichier(FORMAT_CSV);
		}

		public NSDictionary getDico() {
			return dico;
		}

	}

	private final class PreparationFichierEDRB extends PreparationFichier {
		private final String PROC_NAME = "Epn3_epn_genere_edrb";
		private NSDictionary dico;
		private String nomFichier;

		public PreparationFichierEDRB(NSDictionary _dico, String nomFichier) {
			this.nomFichier = nomFichier;
			dico = _dico.mutableClone();
		}

		public void faireTraitement() throws Exception {
			//appel procedure pour balance
			//appel procedure pour balance
			System.out.println("exec de la procedure " + PROC_NAME);
			ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), PROC_NAME, dico);

		}

		public File genererFichier(String format) throws Exception {
			File fichier;
			ADGCPFileWriter fileWriter;
			if ("CSV".equals(format)) {
				fichier = new File(myApp.temporaryDir, nomFichier + ".csv");
				fileWriter = new DGCPEDRBFileWriter(fichier, ",");
			}
			else {
				fichier = new File(myApp.temporaryDir, nomFichier + ".bdf");
				fileWriter = new DGCPEDRBFileWriter(fichier);
			}
			//Récupérer les enregistrements de la table resultats
			//			String sql1 = "select * from "+USER_EPN+"epn_rec_bud_1 where epnrb1_ges_code='"+ dico.valueForKey("10_comp") + "' and EPNRB_ORDRE ="+dico.valueForKey("05_ordre") + " and EPNRB1_EXERCICE=" +dico.valueForKey("15_exe_ordre");
			String sql1 = "select * from " + USER_EPN + "epn_rec_bud_1 where EPNrB1_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNrB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNrB1_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNrB1_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'";
			NSArray res = execSQL(sql1);
			if (res.count() > 1) {
				throw new DefaultClientException("ERREUR : La requete sql a renvoyé plus d'un enregistrement");
			}
			Map mapHead = ((NSDictionary) res.objectAtIndex(0)).hashtable();

			String sql2 = "select * from " + USER_EPN + "epn_rec_bud_2 where EPNrB2_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNrB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNrB2_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNrB2_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'" + " order by EPNRB2_NUMERO";
			NSArray res2 = execSQL(sql2);
			if (res2.count() == 0) {
				showWarningDialog("Attention, le fichier Recette budgétaire est vide pour le code budget " + dico.valueForKey(KEY_CODE_BUDGET) + ". Vérifiez si c'est normal avant d'envoyer les fichiers.");
			}

			ArrayList lignes = ZEOUtilities.convertNSArrayOfNSDictionaryToArrayListOfMap(res2);

			//          creation du fichier
			//            DGCPEDRBFileWriter fileWriter = new DGCPEDRBFileWriter(fichier);
			try {
				fileWriter.writeHeader(mapHead);
				fileWriter.flush();
				fileWriter.writeLines(lignes);
				fileWriter.flush();
				fileWriter.writeFooter(mapHead);
				fileWriter.flush();
				fileWriter.close();
				if (fichier.length() <= 0) {
					throw new DefaultClientException("Le fichier est vide.");
				}
			} catch (Exception e) {
				fileWriter.close();
				throw e;
			}
			return fichier;
		}

		public File genereFichierText() throws Exception {
			return genererFichier(FORMAT_DGCP);

		}

		public File genereFichierXls() throws Exception {
			return genererFichier(FORMAT_CSV);
		}

		public NSDictionary getDico() {
			return dico;
		}

	}

	private final class PreparationFichierECCB extends PreparationFichier {
		private final String PROC_NAME = "Epn3_epn_genere_eccb";
		private NSDictionary dico;
		private String nomFichier;

		public PreparationFichierECCB(NSDictionary _dico, String nomFichier) {
			this.nomFichier = nomFichier;
			dico = _dico.mutableClone();
		}

		public void faireTraitement() throws Exception {
			//appel procedure pour balance
			System.out.println("exec de la procedure " + PROC_NAME);
			ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), PROC_NAME, dico);

		}

		public File genererFichier(String format) throws Exception {
			File fichier;
			ADGCPFileWriter fileWriter;
			if ("CSV".equals(format)) {
				fichier = new File(myApp.temporaryDir, nomFichier + ".csv");
				fileWriter = new DGCPECCBFileWriter(fichier, ",");
			}
			else {
				fichier = new File(myApp.temporaryDir, nomFichier + ".bdf");
				fileWriter = new DGCPECCBFileWriter(fichier);
			}
			//Récupérer les enregistrements de la table resultats
			//			String sql1 = "select * from "+USER_EPN+"epn_cre_bud_1 where epnccb1_ges_code='"+ dico.valueForKey("10_comp") + "' and EPNCCB_ORDRE ="+dico.valueForKey("05_ordre") + " and EPNCCB1_EXERCICE=" +dico.valueForKey("15_exe_ordre");
			String sql1 = "select * from " + USER_EPN + "epn_cre_bud_1 where EPNccB1_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNccB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNccB1_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNccB1_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'";
			NSArray res = execSQL(sql1);
			if (res.count() > 1) {
				throw new DefaultClientException("ERREUR : La requete sql a renvoyé plus d'un enregistrement");
			}
			Map mapHead = ((NSDictionary) res.objectAtIndex(0)).hashtable();

			//			String sql2 = "select * from "+USER_EPN+"epn_cre_bud_2 where epnccb2_ges_code='"+ dico.valueForKey("10_comp") + "' and EPNCCB_ORDRE ="+dico.valueForKey("05_ordre")+ " and EPNCCB2_EXERCICE=" +dico.valueForKey("15_exe_ordre") +" order by EPNCCB2_NUMERO";
			String sql2 = "select * from " + USER_EPN + "epn_cre_bud_2 where EPNccB2_COD_BUD='" + dico.valueForKey(KEY_CODE_BUDGET) + "' and EPNccB_ORDRE ='" + dico.valueForKey(KEY_RANG) + "' and EPNccB2_EXERCICE = " + dico.valueForKey(KEY_EXE_ORDRE) + " and EPNccB2_TYPE_DOC='"
					+ dico.valueForKey(KEY_TYPE_FICHIER) + "'" + " order by EPNCCB2_NUMERO";
			NSArray res2 = execSQL(sql2);
			if (res2.count() == 0) {
				showWarningDialog("Attention, le fichier Crédits budgétaires est vide pour le code budget " + dico.valueForKey(KEY_CODE_BUDGET) + ". Vérifiez si c'est normal avant d'envoyer les fichiers.");
			}

			ArrayList lignes = ZEOUtilities.convertNSArrayOfNSDictionaryToArrayListOfMap(res2);

			ZLogger.debug(lignes);

			//          creation du fichier
			//            DGCPECCBFileWriter fileWriter = new DGCPECCBFileWriter(fichier);
			try {
				fileWriter.writeHeader(mapHead);
				fileWriter.flush();
				fileWriter.writeLines(lignes);
				fileWriter.flush();
				fileWriter.writeFooter(mapHead);
				fileWriter.flush();
				fileWriter.close();
				if (fichier.length() <= 0) {
					throw new DefaultClientException("Le fichier est vide.");
				}
			} catch (Exception e) {
				fileWriter.close();
				throw e;
			}
			return fichier;
		}

		public File genereFichierText() throws Exception {
			return genererFichier(FORMAT_DGCP);

		}

		public File genereFichierXls() throws Exception {
			return genererFichier(FORMAT_CSV);
		}

		public NSDictionary getDico() {
			return dico;
		}

	}

	private final class CodeBudgetModel extends DefaultComboBoxModel {
		private ArrayList<Object> allCodeBudgets = new ArrayList<Object>();

		public CodeBudgetModel() {
			addElement(CODE_BUDGET_TOUS_LIBELLE);
			codeBudgetLibellesMap.put(CODE_BUDGET_TOUS_LIBELLE, CODE_BUDGET_TOUS);
			Iterator<String> iter = codeBudgetMap.keySet().iterator();
			while (iter.hasNext()) {
				String codeBud = iter.next();
				String lib = codeBud + " (" + (CODE_BUDGET_01.equals(codeBud) ? "" : "SACD ") + codeBudgetMap.get(codeBud) + ")";
				codeBudgetLibellesMap.put(lib, codeBud);
				addElement(lib);
				allCodeBudgets.add(codeBud);
			}
		}

		public ArrayList<Object> getAllCodeBudgets() {
			return allCodeBudgets;
		}

		public ArrayList<Object> getSelectedCodeBudgets() {
			if (CODE_BUDGET_TOUS_LIBELLE.equals(getSelectedItem())) {
				return getAllCodeBudgets();
			}
			ArrayList<Object> res = new ArrayList<Object>();
			res.add(codeBudgetLibellesMap.get(getSelectedItem()));
			return res;
		}

	}

	private final class TypeFichiersModel extends DefaultComboBoxModel {
		private ArrayList<String> alltypeFichiers = new ArrayList<String>();

		public TypeFichiersModel() {
			NSArray sacds;
			try {
				sacds = getCodeBudgetsSACD();
			} catch (Exception e) {
				sacds = new NSArray();
			}
			if (sacds.count() > 0) {
				alltypeFichiers.add(TYPE_FICHIER_02);
			}
			alltypeFichiers.add(TYPE_FICHIER_03);
			typeFichierLibellesMap.put(TYPE_FICHIER_TOUS_LIBELLE, TYPE_FICHIER_TOUS);
			addElement(TYPE_FICHIER_TOUS_LIBELLE);

			if (alltypeFichiers.contains(TYPE_FICHIER_02)) {
				typeFichierLibellesMap.put(TYPE_FICHIER_02_LIBELLE, TYPE_FICHIER_02);
				addElement(TYPE_FICHIER_02_LIBELLE);
			}

			typeFichierLibellesMap.put(TYPE_FICHIER_03_LIBELLE, TYPE_FICHIER_03);
			addElement(TYPE_FICHIER_03_LIBELLE);
		}

		/**
		 * @return les types de fichiers possibles dans le cas ou l'utilisateur selectionne l'option Tous.
		 */
		public ArrayList<String> getAllTypeFichiers() {
			return alltypeFichiers;
		}

		public ArrayList<String> getSelectedTypeFichiers() {
			if (TYPE_FICHIER_TOUS_LIBELLE.equals(getSelectedItem())) {
				return getAllTypeFichiers();
			}
			ArrayList<String> res = new ArrayList<String>();
			res.add(typeFichierLibellesMap.get(getSelectedItem()));
			return res;
		}
	}

	private final class ComptabiliteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			//System.out.println("ComptabiliteListener.actionPerformed()");
			clearFiles();
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	/**
	 * Modele pour la liste des fichiers.
	 */
	private final class FileListTablePanelMdl implements IFileListTablePanelMdl {
		public final String FILE_NAME_KEY = "FILE_NAME";
		public final String FILE_KEY = "FILE";
		private final EOSortOrdering SORT_FILE_NAME = EOSortOrdering.sortOrderingWithKey(FILE_NAME_KEY, EOSortOrdering.CompareAscending);
		private final NSMutableArray<NSMutableDictionary> files = new NSMutableArray<NSMutableDictionary>();
		private Map<File, PreparationFichier> filePreparationMap = new HashMap<File, PreparationFichier>();

		public NSArray<NSMutableDictionary> getData() throws Exception {
			ZLogger.verbose("fichiers :");
			ZLogger.verbose(files);
			return files;
		}

		public void onDbClick() {

		}

		public void selectionChanged() {

		}

		public void clear() {
			files.removeAllObjects();
			filePreparationMap.clear();
		}

		public void addFile(File file) {
			NSMutableDictionary dic = new NSMutableDictionary();
			dic.takeValueForKey(file.getName(), FILE_NAME_KEY);
			dic.takeValueForKey(file, FILE_KEY);
			files.addObject(dic);
			sort();
		}

		public void sort() {
			EOSortOrdering.sortArrayUsingKeyOrderArray(files, new NSArray(new Object[] {
					SORT_FILE_NAME
			}));
		}

		public NSMutableArray getFiles() {
			return (NSMutableArray) files.valueForKey(FILE_KEY);
		}

		public Map<File, PreparationFichier> getFilePreparationMap() {
			return filePreparationMap;
		}
	}

	private void construireCodeBudgetMap() throws Exception {
		codeBudgetMap.put(CODE_BUDGET_01, CODE_BUDGET_01_LIBELLE);
		//recuperer le parametre (ex. CONSOLIDE SAIC:02,TOTO:03)
		String regroupementSACDParam = (String) myApp.getParametres().valueForKey(PARAM_REGROUPEMENT_SACD);
		if (!StringCtrl.isEmpty(regroupementSACDParam)) {

			eoagregats = EOGestionAgregat.getGestionAgregatsForExercice(myApp.appUserInfo().getCurrentExercice());

			NSMutableArray paires = new NSMutableArray(NSArray.componentsSeparatedByString(regroupementSACDParam, ","));

			Map<String, String> regroupementsSACDCodesMap = new HashMap<String, String>();

			for (int i = 0; i < paires.count(); i++) {
				String paire = (String) paires.objectAtIndex(i);
				NSArray codes = NSArray.componentsSeparatedByString(paire, ":");
				if (codes.count() == 2) {
					regroupementsSACDCodesMap.put(((String) codes.objectAtIndex(0)).trim(), ((String) codes.objectAtIndex(1)).trim());
				}
			}

			for (String libelle : regroupementsSACDCodesMap.keySet()) {
				EOQualifier qual = new EOKeyValueQualifier(EOGestionAgregat.GA_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, libelle);

				if (EOQualifier.filteredArrayWithQualifier(eoagregats, qual).count() != 1) {
					throw new Exception("Le libellé " + libelle + " indiqué dans le paramètre " + PARAM_REGROUPEMENT_SACD + " ne correspond pas à un regroupement de codes gestions.");
				}
				if ("01".equals(regroupementsSACDCodesMap.get(libelle))) {
					throw new Exception("Le code budget 01 est défini pour le regroupement " + libelle + " pour l'exercice " + getExercice().exeExercice().intValue()
							+ ". Ce code 01 est réservé au code budget de l'établissement principal, vous devez modifier ca dans le parametre " + PARAM_REGROUPEMENT_SACD + " de la table maracuja.parametre.");
				}
				EOGestionAgregat ga = (EOGestionAgregat) EOQualifier.filteredArrayWithQualifier(eoagregats, qual).objectAtIndex(0);
				NSArray ges = ga.toGestionExercices(getExercice());
				String gestionCsv = "";
				for (int i = 0; i < ges.count(); i++) {
					EOGestionExercice gestionExercice = (EOGestionExercice) ges.objectAtIndex(i);
					if (!gestionExercice.isSacd()) {
						throw new Exception("Le code gestion " + gestionExercice.gesCode() + " indiqué dans le paramètre " + PARAM_REGROUPEMENT_SACD + " ne correspond pas à un SACD.");
					}
					//gestionCsv += (gestionCsv.length() > 0 ? "+" : "");
					gestionCsv += "+" + gestionExercice.gesCode();
				}
				codeBudgetMap.put(regroupementsSACDCodesMap.get(libelle), "+" + ga.gaLibelle());
			}
		}

		NSArray codeBudgets = getCodeBudgetsSACD();
		if (codeBudgets.count() > 0) {
			for (int i = 0; i < codeBudgets.count(); i++) {
				NSDictionary element = (NSDictionary) codeBudgets.objectAtIndex(i);

				boolean traitee = false;
				for (String val : codeBudgetMap.values()) {
					NSArray vals = NSArray.componentsSeparatedByString(val, "+");
					if (val.startsWith("+")) {
						val = val.substring(1);

						EOQualifier qual = new EOKeyValueQualifier(EOGestionAgregat.GA_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, val);
						NSArray res = EOQualifier.filteredArrayWithQualifier(eoagregats, qual);
						if (res.count() > 0) {
							EOGestionAgregat ga = (EOGestionAgregat) res.objectAtIndex(0);
							NSArray gestions = ga.toGestionExercices(getExercice());
							EOQualifier qual2 = new EOKeyValueQualifier(EOGestionExercice.GES_CODE_KEY, EOQualifier.QualifierOperatorEqual, element.valueForKey("GES_CODE"));
							if (EOQualifier.filteredArrayWithQualifier(gestions, qual2).count() > 0) {
								traitee = true;
							}
						}
					}
				}

				//si le code budget est deja intégré via les parametres, on passe
				if (!traitee) {
					if (NSKeyValueCoding.NullValue.equals(element.valueForKey("COD_BUD")) || element.valueForKey("COD_BUD") == null) {
						throw new Exception("Le code budget n'est pas defini pour le SACD " + element.valueForKey("GES_CODE") + " pour l'exercice " + getExercice().exeExercice().intValue() + ". Vous devez le definir dans la table "
								+ " EPN.CODE_BUDGET_SACD. Ce code doit vous etre fourni par la DGCP.");
					}

					if (!codeBudgetMap.containsKey(element.valueForKey("COD_BUD"))) {
						if ("01".equals(element.valueForKey("COD_BUD").toString())) {
							throw new Exception("Le code budget 01 est défini pour le SACD " + element.valueForKey("GES_CODE") + " pour l'exercice " + getExercice().exeExercice().intValue()
									+ ". Ce code 01 est réservé au code budget de l'établissement principal, vous devez modifier ca dans la table EPN.CODE_BUDGET_SACD.");
						}
						codeBudgetMap.put((String) element.valueForKey("COD_BUD"), (String) element.valueForKey("GES_CODE"));
					}
				}
			}
		}

	}

}
