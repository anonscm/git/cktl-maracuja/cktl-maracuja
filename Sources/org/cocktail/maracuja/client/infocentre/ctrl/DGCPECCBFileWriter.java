/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Copyright COCKTAIL, 1995-2008 This software is governed by the CeCILL license
 * under French law and abiding by the rules of distribution of free software.
 * You can use, modify and/or redistribute the software under the terms of the
 * CeCILL license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". As a counterpart to the access to the source code
 * and rights to copy, modify and redistribute granted by the license, users are
 * provided only with a limited warranty and the software's author, the holder
 * of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks
 * associated with loading, using, modifying and/or developing or reproducing
 * the software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also therefore
 * means that it is reserved for developers and experienced professionals having
 * in-depth computer knowledge. Users are therefore encouraged to load and test
 * the software's suitability as regards their requirements in conditions
 * enabling the security of their systems and/or data to be ensured and, more
 * generally, to use and operate it in the same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.infocentre.ctrl;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.cocktail.zutil.client.ZStringUtil;

/**
*
* @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
*/

final class DGCPECCBFileWriter extends ADGCPFileWriter {
    /**
	 * 
	 */
	public final int LEN_HEADER_TYPE=1;
    public final int LEN_HEADER_NUMERO=5;
    public final int LEN_HEADER_IDENTIFIANT=10;
    public final int LEN_HEADER_TYPEDOCUMENT=2;
    public final int LEN_HEADER_CODENOMENCLATURE=2;
    public final int LEN_HEADER_CODEBUDGET=2;
    public final int LEN_HEADER_EXERCICE=4;
    public final int LEN_HEADER_RANG=2;
    public final int LEN_HEADER_DATE=8;
    public final int LEN_HEADER_SIREN=9;
    public final int LEN_HEADER_SIRET=14;
    public final int LEN_HEADER_FILLER=24;

    public final int LEN_DETAIL_TYPE=1;
    public final int LEN_DETAIL_NUMERO=5;
    public final int LEN_DETAIL_TYPECOMPTE=1;
    public final int LEN_DETAIL_COMPTE=15;
    public final int LEN_DETAIL_CREDPREVOUVERT=15;
    public final int LEN_DETAIL_CREDPREVOB=15;
    public final int LEN_DETAIL_CREDPREVNONEMPL=15;
    public final int LEN_DETAIL_CREDPREVEXTNONEMPL=15;
    public final int LEN_DETAIL_SENSCOMPTE=1;

    public final int LEN_FOOTER_TYPE=1;
    public final int LEN_FOOTER_NUMERO=5;
    public final int LEN_FOOTER_FILLER=77;




    /**
     * @param file
     * @throws IOException
     */
    public DGCPECCBFileWriter(File file) throws IOException {
        super(file);
    }
    
    public DGCPECCBFileWriter(File file, String colSeparator) throws IOException {
    	super(file, colSeparator);
    }
    

    /**
     * @see org.cocktail.zutil.client.ZFlatTextFilewriter#writeHeader(java.util.Map)
     */
    public void writeHeader(final Map map) throws Exception {
        write( encodeStringAlignLeft((String) map.get("EPNCCB1_TYPE"),LEN_HEADER_TYPE));
        write( encodeStringAlignRight(map.get("EPNCCB1_NUMERO").toString(),LEN_HEADER_NUMERO, "0"));
        write( encodeStringAlignRight(map.get("EPNCCB1_IDENTIFIANT").toString(),LEN_HEADER_IDENTIFIANT, "0"));
        write( encodeStringAlignLeft(map.get("EPNCCB1_TYPE_DOC").toString(),LEN_HEADER_TYPEDOCUMENT));
        write( encodeStringAlignLeft(map.get("EPNCCB1_COD_NOMEN").toString(),LEN_HEADER_CODENOMENCLATURE));
        write( encodeStringAlignLeft(map.get("EPNCCB1_COD_BUD").toString(),LEN_HEADER_CODEBUDGET));
        write( encodeStringAlignLeft(map.get("EPNCCB1_EXERCICE").toString(),LEN_HEADER_EXERCICE));
        write( encodeStringAlignLeft(map.get("EPNCCB1_RANG").toString(),LEN_HEADER_RANG));
        write( encodeStringAlignLeft(map.get("EPNCCB1_DATE").toString(),LEN_HEADER_DATE));
        write( encodeStringAlignRight(map.get("EPNCCB1_SIREN").toString(),LEN_HEADER_SIREN));
        write( encodeStringAlignRight(map.get("EPNCCB1_SIRET").toString(),LEN_HEADER_SIRET));
        write( ZStringUtil.extendWithChars("",SPACER, LEN_HEADER_FILLER,false));
        write(CRLF);
    }

    /**
     * @see org.cocktail.zutil.client.ZFlatTextFilewriter#writeLine(java.util.Map)
     */
    public void writeLine(final Map map)  throws Exception {
        write( encodeStringAlignLeft(map.get("EPNCCB2_TYPE").toString(),LEN_DETAIL_TYPE));
        write( encodeStringAlignRight(map.get("EPNCCB2_NUMERO").toString(),LEN_DETAIL_NUMERO, "0"));
        write( encodeStringAlignRight(map.get("EPNCCB2_TYPE_CPT").toString(),LEN_DETAIL_TYPECOMPTE));
        write( encodeStringAlignLeft(map.get("EPNCCB2_COMPTE").toString(),LEN_DETAIL_COMPTE));
        write( encodeNumber( map.get("EPNCCB2_CREPREVOUV"), LEN_DETAIL_CREDPREVOUVERT));
        write( encodeNumber( map.get("EPNCCB2_CREPREVOB"),LEN_DETAIL_CREDPREVOB));
        write( encodeNumber( map.get("EPNCCB2_CREPREVNOEMPL"),LEN_DETAIL_CREDPREVNONEMPL));
        write( encodeNumber( map.get("EPNCCB2_CREPREVEXTNOEMPL"),LEN_DETAIL_CREDPREVEXTNONEMPL));
        write( encodeStringAlignLeft( map.get("EPNCCB2_SENSCPT").toString(),LEN_DETAIL_SENSCOMPTE));
        write(CRLF);
    }

    /**
     * @see org.cocktail.zutil.client.ZFlatTextFilewriter#writeFooter(java.util.Map)
     */
    public void writeFooter(final Map map)  throws Exception {
        write( ZStringUtil.extendWithChars("9",SPACER, LEN_FOOTER_TYPE,false));
        write( encodeStringAlignRight(map.get("EPNCCB1_NBENREG").toString(),LEN_FOOTER_NUMERO, "0"));
        write( ZStringUtil.extendWithChars("",SPACER, LEN_FOOTER_FILLER,false));
        write(CRLF);
    }



}
