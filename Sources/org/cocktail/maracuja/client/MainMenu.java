/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/* MainMenu.java created by tsaivre on Fri 20-Jun-2003 */
package org.cocktail.maracuja.client;

import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;

import org.cocktail.maracuja.client.administration.ConnectedUsersCtrl;
import org.cocktail.maracuja.client.administration.ParametresSaisieCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread.DefaultZHeartBeatListener;
import org.cocktail.maracuja.client.cptefi.ctrl.BilanAdminCtrl;
import org.cocktail.maracuja.client.impression.ZKarukeraImprCtrl;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSArray;

public final class MainMenu extends JMenuBar {

	private static final String MENU_FICHIER = "Fichier";
	private static final String MENU_JOURNAL = "Journal";
	private static final String MENU_PAIEMENTS = "Paiements";
	private static final String MENU_RECOUVREMENTS = "Recouvrements";
	private static final String MENU_VISA = "Visa";
	private static final String MENU_ADMINISTRATION = "Administration";
	private static final String MENU_IMPRESSIONS = "Impressions";
	private static final String MENU_OUTILS = "Outils";
	private static final String MENU_aide = "?";

	private static final String SMENU_comptabilites = "Comptabilité";


	private ApplicationClient myApp;
	protected JMenu menuFichier, menuAide, menuAdmin, menuVisa, menuOutils;
	private ZActionCtrl myActionCtrl;
	private JMenu menuJournal;
	private JMenu menuImpressions;
	private JMenu menuPaiement;
	private JMenu menuRecouvrement;
	private IMainMenuModel myModel;

	public MainMenu(IMainMenuModel model) {
		myModel = model;
		myApp = (ApplicationClient) EOApplication.sharedApplication();
		myActionCtrl = myApp.getMyActionsCtrl();
		this.buildAllMenu();

		this.add(menuFichier);
		this.add(menuJournal);
		this.add(menuPaiement);
		this.add(menuRecouvrement);
		this.add(menuVisa);
		this.add(menuImpressions);
		this.add(menuAdmin);
		this.add(menuOutils);
		this.add(menuAide);
	}

	public final void buildAllMenu() {
		menuFichier = buildMenuFichier();
		menuVisa = buildMenuVisa();
		menuJournal = buildMenuJournal();
		menuOutils = buildMenuOutils();
		menuImpressions = buildMenuImpressions();
		menuAdmin = buildMenuAdministration();
		menuAide = buildMenuAide();
		menuPaiement = buildMenuPaiement();
		menuRecouvrement = buildMenuRecouvrements();
	}

	/**
	 * Construit le menu Journal.
	 * 
	 * @return
	 */
	public JMenu buildMenuJournal() {
		final JMenu menuComptabilite = new JMenu(MENU_JOURNAL);
		affecteActionByIDToMenu(ZActionCtrl.IDU_COCE, menuComptabilite);
		menuComptabilite.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.IDU_COEM, menuComptabilite);
		return menuComptabilite;
	}

	/**
	 * Construit le menu Paiement.
	 * 
	 * @return
	 */
	public JMenu buildMenuPaiement() {
		final JMenu m = new JMenu(MENU_PAIEMENTS);

		try {
			if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOP)) {
				affecteActionByIDToMenu(ZActionCtrl.IDU_PAOP, m);
			}
			else if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_PAOPO)) {
				affecteActionByIDToMenu(ZActionCtrl.IDU_PAOPO, m);
			}
		} catch (DefaultClientException e) {
		}

		affecteActionByIDToMenu(ZActionCtrl.IDU_PARE, m);
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAGE, m);
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAMO, m);
		affecteActionByIDToMenu(ZActionCtrl.IDU_VIAVMIS, m);

		return m;
	}

	/**
	 * Construit le menu Outils.
	 */
	public JMenu buildMenuOutils() {
		final JMenu menuOutilsTmp = new JMenu(MENU_OUTILS);
		affecteActionByIDToMenu(ZActionCtrl.IDU_SRCH01, menuOutilsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_SRCH02, menuOutilsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_EPN, menuOutilsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_OUTBE, menuOutilsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_OUTCFI, menuOutilsTmp);
		affecteActionByIDToMenu(ZActionCtrl.ID_OUTILS_REFRESHDATA, menuOutilsTmp);
		menuOutilsTmp.add(new ActionAnalyseProblemes());
		menuOutilsTmp.add(new ActionConnectedUsers());
		return menuOutilsTmp;
	}

	public JMenu buildMenuRecouvrements() {
		final JMenu menu = new JMenu(MENU_RECOUVREMENTS);
		affecteActionByIDToMenu(ZActionCtrl.IDU_SEPASDD, menu);
		affecteActionByIDToMenu(ZActionCtrl.IDU_PRELEV, menu);
		affecteActionByIDToMenu(ZActionCtrl.IDU_CHESA, menu);
		affecteActionByIDToMenu(ZActionCtrl.IDU_RELANCE, menu);
		return menu;
	}

	/**
	 * Construit le menu d'aide.
	 * 
	 * @return
	 */
	public JMenu buildMenuAide() {
		final JMenu menuAideTmp = new JMenu(MENU_aide);
		affecteActionByIDToMenu(ZActionCtrl.ID_AIDE_LOGVIEWER, menuAideTmp);
		menuAideTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.ID_APROPOS, menuAideTmp);
		return menuAideTmp;

	}

	public JMenu buildMenuImpressions() {
		final JMenu menuImpressionsTmp = new JMenu(MENU_IMPRESSIONS);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR007, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR008, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.ID_IMPR_COMPTABILITEGESTION, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR009, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR015, menuImpressionsTmp);
		menuImpressionsTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR001, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR002, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR003, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR004, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR005, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR010, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR011, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR012, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR013, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMPR014, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.ID_IMPR_JOURNAL_REJET, menuImpressionsTmp);
		affecteActionByIDToMenu(ZActionCtrl.ID_IMPR_JOURNAL_CONVRA, menuImpressionsTmp);
		return menuImpressionsTmp;
	}

	public JMenu buildMenuFichier() {
		final JMenu menuFichierTmp = new JMenu(MENU_FICHIER);
		affecteActionByIDToMenu(ZActionCtrl.ID_CHANGEREXERCICE, menuFichierTmp);
		menuFichierTmp.add(buildMenuComptabilites());
		menuFichierTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.ID_QUITTER, menuFichierTmp);
		return menuFichierTmp;
	}

	private final JMenu buildMenuComptabilites() {
		final NSArray comptabilites = myModel.getComptabilites();
		final JMenu menuComptabilites = new JMenu(SMENU_comptabilites);

		final ButtonGroup group = new ButtonGroup();

		for (int i = 0; i < comptabilites.count(); i++) {
			final EOComptabilite element = (EOComptabilite) comptabilites.objectAtIndex(i);
			final JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem();
			final Action action = new AbstractAction(element.comLibelle() + " (" + element.gestion().gesCode() + ")") {
				public void actionPerformed(ActionEvent e) {
					myModel.onComptabiliteSelected(element);
					menuItem.setSelected(true);
				}
			};
			menuItem.setAction(action);
			menuComptabilites.add(menuItem);
			group.add(menuItem);
			if (element.equals(myApp.appUserInfo().getCurrentComptabilite())) {
				menuItem.setSelected(true);
			}
		}
		return menuComptabilites;
	}

	public JMenu buildMenuVisa() {
		final JMenu menuVisaTmp = new JMenu(MENU_VISA);
		affecteActionByIDToMenu(ZActionCtrl.IDU_VIDE, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_VIRE, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_VIPR, menuVisaTmp);
		menuVisaTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAYE, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAYEPAF, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_SCOL, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_VITA, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_VISBRO, menuVisaTmp);
		menuVisaTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.IDU_REMA, menuVisaTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_RETI, menuVisaTmp);

		//		menuVisa.addSeparator();
		return menuVisaTmp;
	}

	public JMenu buildMenuAdministration() {
		final JMenu menuAdminTmp = new JMenu(MENU_ADMINISTRATION);
		//		affecteActionByIDToMenu(ZActionCtrl.IDU_ADUT, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADCO, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADGE, menuAdminTmp);
		//		affecteActionByIDToMenu(ZActionCtrl.IDU_PAEX, menuAdminTmp);
		menuAdminTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAPC, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAMP, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_PAMR, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADREL, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADRET, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_BDFCAL, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADBILAN, menuAdminTmp);

		//        menuAdminTmp.add(new ActionParametres());
		return menuAdminTmp;
	}

	private void affecteActionByIDToMenu(String id, JMenu menu) {
		final ZAction tmpAction = myActionCtrl.getActionbyId(id);
		if (tmpAction != null) {
			final JMenuItem item = menu.add(tmpAction);
			item.setToolTipText(null);
		}
	}

	private final class ActionConnectedUsers extends AbstractAction {
		public ActionConnectedUsers() {
			super("Utilisateurs connectés");
			//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			ConnectedUsersCtrl win = new ConnectedUsersCtrl(myApp.editingContext());
			try {
				win.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}



	private final class ActionAnalyseProblemes extends AbstractAction {
		public ActionAnalyseProblemes() {
			super("Analyse des problèmes de données");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			try {
				HashMap paramRequete = new HashMap();
				paramRequete.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());

				final String filePath = ReportFactoryClient.imprimerZanalyseProbleme(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), paramRequete, myApp.getMainFrame(), true);
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	//    
	private final class ActionTest extends AbstractAction {
		public ActionTest() {
			super("Test Bilan");
			//	        putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			try {
				BilanAdminCtrl bp = new BilanAdminCtrl(myApp.getEditingContext());
				bp.openDialog(myApp.getMainFrame());

			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	private final class ActionTestThread extends AbstractAction {
		public ActionTestThread() {
			super("Test thread");
			//	        putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			try {

				final ZWaitingPanelDialog waitingPanelDialog = new ZWaitingPanelDialog(null, myApp.getMainFrame(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
				waitingPanelDialog.setTitle("Veuillez patienter...");
				waitingPanelDialog.setTopText("Veuillez patienter ...");
				waitingPanelDialog.setBottomText("Génération des fichiers en cours");
				waitingPanelDialog.getMyProgressBar().setIndeterminate(false);
				waitingPanelDialog.setModal(false);

				//maintenir la connection avec le serveur
				final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
					Integer progress;

					public void onBeat() {
						progress = (Integer) ServerProxy.clientSideRequestTestThreadProgress(myApp.editingContext());
						System.out.println("Progress = " + progress);

						waitingPanelDialog.setBottomText("Traitement de " + progress);
						waitingPanelDialog.getMyProgressBar().setValue(progress.intValue());
					}

					public void mainTaskStart() {
						waitingPanelDialog.getMyProgressBar().setMaximum(50);
						waitingPanelDialog.setVisible(true);
						ServerProxy.clientSideRequestTestThread(myApp.editingContext(), new Integer(30000));
					}

					public boolean isMainTaskFinished() {
						return (progress.longValue() >= 50);
					};
				};
				final ZWaitingThread waitingThread = new ZWaitingThread(1000, 500, defaultZHeartBeatListener);
				try {
					waitingThread.start();
					synchronized (waitingThread.getStateLock()) {
						//                  On attend que ce soit terminé
						waitingThread.getStateLock().wait();
						waitingPanelDialog.setVisible(false);
						System.out.println("tout fini");
					}
				} catch (Exception e0) {
					throw e0;
				} finally {
					waitingThread.interrupt();
				}
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	private final class ActionParametres extends AbstractAction {
		public ActionParametres() {
			super("Parametres");
		}

		public void actionPerformed(ActionEvent e) {
			final ParametresSaisieCtrl win = new ParametresSaisieCtrl(myApp.editingContext());
			try {
				win.openDialog(myApp.getMainWindow());
			} catch (Exception e1) {
				myApp.showErrorDialog(e1);
			}
		}
	}

	//    private final class LFChangerItemListener implements ItemListener {
	//
	//        /**
	//         * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	//         */
	//        public void itemStateChanged(ItemEvent e) {
	//            final JRadioButtonMenuItem menuItem = (JRadioButtonMenuItem)e.getItem();
	//            if (menuItem.isSelected()) {
	//                final String s = menuItem.getName();
	//                try {
	//                    UIManager.setLookAndFeel(s);
	//                    myApp.getMainFrame().validate();
	//                } catch (Exception e1) {
	//                    myApp.showErrorDialog(e1);
	//                }
	//            }
	//        }
	//
	//    }

	public interface IMainMenuModel {
		public NSArray getComptabilites();

		public void onComptabiliteSelected(EOComptabilite comptabilite);

	}

}
