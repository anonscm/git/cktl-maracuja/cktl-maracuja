/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.ClassesComboBoxModel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPlanComptableEtVisa;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoVisa;
import org.cocktail.maracuja.client.pco.ui.OptionsBilanTable.IOptionsBilanTableListener;
import org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel;
import org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel;
import org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoRechercheCtrl extends CommonCtrl {

	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Gestion du plan comptable";
	private final ActionClose actionClose = new ActionClose();
	private final ActionCompteDevalider actionCompteInvalider = new ActionCompteDevalider();
	private final ActionCompteValider actionCompteValider = new ActionCompteValider();
	private final ActionNew actionNew = new ActionNew();
	private final ActionSupprimer actionSupprimer = new ActionSupprimer();
	private final ActionModify actionModify = new ActionModify();
	private final ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionPlancoCtrePartieSelect actionPlancoCtrePartieSelect = new ActionPlancoCtrePartieSelect();
	private final ActionPlancoTvaSelect actionPlancoTvaSelect = new ActionPlancoTvaSelect();
	private final ActionCtrePartieAnnuler actionCtrePartieAnnuler = new ActionCtrePartieAnnuler();
	private final ActionCtrePartieEnregistrer actionCtrePartieEnregistrer = new ActionCtrePartieEnregistrer();
	private final ActionDeletePlancoBilan actionDeletePlancoBilan = new ActionDeletePlancoBilan();
	private final ActionAddPlancoBilan actionAddPlancoBilan = new ActionAddPlancoBilan();
	private final ActionCtrePartieSupprimer actionCtrePartieSupprimer = new ActionCtrePartieSupprimer();

	//	private final ActionCtrePartieDevalider actionCtrePartieDevalider = new ActionCtrePartieDevalider();
	//	private final ActionCtrePartieValider actionCtrePartieValider = new ActionCtrePartieValider();

	private DefaultComboBoxModel contrePartieGestionMdl = new DefaultComboBoxModel(new String[] {
			"AGENCE", "COMPOSANTE"
	});

	private PcoTableCellRenderer pcoTableCellRenderer = new PcoTableCellRenderer();

	private ZKarukeraDialog win;
	private HashMap pcoFilters;
	private PcoRecherchePanel myPanel;

	private PcoSaisieContrePartiePanelListener myPcoSaisieContrePartiePanelListener = new PcoSaisieContrePartiePanelListener();
	private OptionsBilanTableListener optionsBilanTableListener = new OptionsBilanTableListener();

	private HashMap dicoCtrePartie;
	private EOPlancoVisa currentPcoVisa;

	private EODisplayGroup dgPlancomptable;
	private ZEOSelectionDialog planComptableSelectionDialog;

	private FactoryProcessPlanComptableEtVisa myFactoryProcessPlanComptableEtVisa;

	private NSArray pcoValideClasse4 = getPcoValidesContrepartie();
	private NSArray pcoValideClasse445 = getPcoValidesClasse445();

	private final ClassesComboBoxModel classesComboboxModel = new ClassesComboBoxModel();
	private final ActionListener classesComBoListener = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			pcoFilters.put(EOPlanComptableExer.CLASSE_COMPTE_KEY, classesComboboxModel.getSelectedClasse());
			onSrch();
		}
	};

	//private NSArray allPlancoExers = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), null, false);
	private NSArray pcosValidesNextExer = EOPlanComptableExer.fetchAll(getEditingContext(), new EOAndQualifier(new NSArray(new Object[] {
			EOPlanComptableExer.QUAL_VALIDE, new EOKeyValueQualifier(EOPlanComptableExer.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, getExercice().getNextEOExercice())
	})), null);

	/**
	 * @param editingContext
	 */
	public PcoRechercheCtrl(EOEditingContext editingContext) {
		super(editingContext);
		revertChanges();
		pcoFilters = new HashMap();
		initSubObjects();

	}

	public void initSubObjects() {
		dicoCtrePartie = new HashMap();
		dgPlancomptable = new EODisplayGroup();
		myPanel = new PcoRecherchePanel(new PcoRecherchePanelListener());
		myFactoryProcessPlanComptableEtVisa = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
	}

	private void initGUI() {
		myPanel.initGUI();
		planComptableSelectionDialog = createPlanComptableSelectionDialog();
	}

	private final void checkSaisieCtrPartieDico() throws Exception {
		ZLogger.debug(dicoCtrePartie);

		dicoCtrePartie.put(EOPlancoVisa.PVI_CONTREPARTIE_GESTION_KEY, contrePartieGestionMdl.getSelectedItem());

		if (dicoCtrePartie.get(EOPlancoVisa.PVI_CONTREPARTIE_GESTION_KEY) == null) {
			throw new DataCheckException("Veuillez indiquer comment traiter le code gestion pour la contrepartie.");
		}

		if (dicoCtrePartie.get("pviLibelle") == null) {
			throw new DataCheckException("Le libellé est obligatoire.");
		}
		if (dicoCtrePartie.get("ordonnateur") == null) {
			throw new DataCheckException("Erreur anormale. La partie ordonnateur n'est pas précisée. Il doit s'agir d'un bug...");
		}
		if (dicoCtrePartie.get("ctrepartie") == null) {
			throw new DataCheckException("Vous devez spécifier un compte pour la contrepartie");
		}
		if (dicoCtrePartie.get("tva") == null) {
			throw new DataCheckException("Vous devez spécifier un compte pour la TVA");
		}

	}

	private final void onValiderContrePartie() {
		if (currentPcoVisa != null) {
			myFactoryProcessPlanComptableEtVisa.validerPlanComptableVisa(getEditingContext(), currentPcoVisa);
			updateDataCtrePartie();
		}
	}

	//	private EOPlanComptableExer getPlancoExerForplanco(EOPlanComptable planComptable) {
	//		try {
	//			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("planComptable=%@", new NSArray(new Object[] {
	//					planComptable
	//			}));
	//			NSArray res = EOQualifier.filteredArrayWithQualifier(allPlancoExers, qual);
	//			if (res.count() == 0) {
	//				throw new Exception("Le compte " + planComptable.pcoNum() + " n'est pas valide pour l'exercice " + getExercice().exeExercice());
	//			}
	//			return (EOPlanComptableExer) res.objectAtIndex(0);
	//
	//		} catch (Exception e) {
	//			showErrorDialog(e);
	//			return null;
	//		}
	//	}

	private final void onEnregistrerContrePartie() {
		try {
			checkSaisieCtrPartieDico();
			if (myPanel.getSelectedPco() == null) {
				throw new DataCheckException("Le plan comptable n'est pas sélectionné");
			}

			// Vérifier s'il s'agit d'une modification
			if (currentPcoVisa != null) {
				// Vérifier que la contrepartie en cours est bien celle du
				// plancomptable selectionne
				myFactoryProcessPlanComptableEtVisa.modifierPlanComptableVisa(getEditingContext(),
						currentPcoVisa,
						(EOPlanComptableExer) dicoCtrePartie.get("ordonnateur"),
						(EOPlanComptableExer) dicoCtrePartie.get("tva"),
						(EOPlanComptableExer) dicoCtrePartie.get("ctrepartie"),
						(String) dicoCtrePartie.get(EOPlancoVisa.PVI_CONTREPARTIE_GESTION_KEY));
			}
			else {
				currentPcoVisa = myFactoryProcessPlanComptableEtVisa.creerPlanComptableVisa(getEditingContext(),
						((EOPlanComptableExer) dicoCtrePartie.get("ordonnateur")),
						((EOPlanComptableExer) dicoCtrePartie.get("ctrepartie")),
						((EOPlanComptableExer) dicoCtrePartie.get("tva")),
						(String) dicoCtrePartie.get("pviLibelle"),
						(String) dicoCtrePartie.get(EOPlancoVisa.PVI_CONTREPARTIE_GESTION_KEY),
						getExercice());
			}

			getEditingContext().saveChanges();
			updateDataCtrePartie();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Annule les eventuelles mofifs effectuées sur le plancovisa
	 */
	private final void onAnnulerContrePartie() {
		updateDataCtrePartie();
	}

	private final void onSupprimerContrePartie() {
		try {
			if (currentPcoVisa != null) {
				getEditingContext().deleteObject(currentPcoVisa);
			}
			getEditingContext().saveChanges();
			updateDataCtrePartie();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.pco.ui.PcoRechercheFilterPanel.IPcoRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		setWaitCursor(true);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	/**
	 * Gere la selection d'une contre-partie
	 */
	private final void onSelectPlancoCtrePartie() {
		EOPlanComptableExer res = null;
		// on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(pcoValideClasse4);
		res = selectPlanComptableIndDialog((String) dicoCtrePartie.get("pcoNumCtrepartie"));

		dicoCtrePartie.put("ctrepartie", res);
		if (res != null) {
			dicoCtrePartie.put("pcoNumCtrepartie", res.planComptable().pcoNum());
		}

		try {
			myPanel.getCtrePartiePanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Gere la selection d'une TVA
	 */
	private final void onSelectPlancoTva() {
		EOPlanComptableExer res = null;
		// on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(pcoValideClasse445);
		res = selectPlanComptableIndDialog((String) dicoCtrePartie.get("pcoNumTva"));

		dicoCtrePartie.put("tva", res);
		if (res != null) {
			dicoCtrePartie.put("pcoNumTva", res.planComptable().pcoNum());
		}

		try {
			myPanel.getCtrePartiePanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private ZEOSelectionDialog createPlanComptableSelectionDialog() {

		Vector filterFields = new Vector(2, 0);
		filterFields.add(EOPlanComptableExer.PCO_NUM_KEY);
		// filterFields.add(EOPlanComptableExer.PCO_LIBELLE_KEY);

		Vector myCols = new Vector(2, 0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptableExer.PCO_NUM_KEY, "N°");
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptableExer.PCO_LIBELLE_KEY, "Libellé");

		myCols.add(col1);
		myCols.add(col2);

		ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMyDialog(), "Sélection d'une imputation", dgPlancomptable, myCols, "Veuillez sélectionner un compte dans la liste", filterFields);
		dialog.setFilterPrefix("");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" like %@");

		return dialog;
	}

	private EOPlanComptableExer selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptableExer res = null;

		planComptableSelectionDialog.getFilterTextField().setText(filter);
		planComptableSelectionDialog.filter();
		if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
			NSArray selections = planComptableSelectionDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOPlanComptableExer) selections.objectAtIndex(0);
			}
		}
		setWaitCursor(false);
		return res;
	}

	/**
	 * @param pco
	 * @return Le pviLibelle adapté au plancomptable passé en paramètre (en fonctionde la classe du compte) ou null
	 */
	private final String getPviLibelleForPco(EOPlanComptableExer pco) {
		if (pco == null) {
			return null;
		}
		String s = pco.getClasseCompte();
		// ZLogger.debug("Classe du compte "+ pco.pcoNum(), s);
		if ("2".equals(s)) {
			return EOPlancoVisa.pviLibelleMandat2;
		}
		else if ("6".equals(s)) {
			return EOPlancoVisa.pviLibelleMandat6;
		}
		else if ("7".equals(s)) {
			return EOPlancoVisa.pviLibelleTitre7;
		}
		else if ("1".equals(s)) {
			return EOPlancoVisa.pviLibelleTitre1;
		}
		else {
			return null;
		}
	}

	private final void updateOptionBilanTable() {
		try {
			//myPanel.getOptionsBilanTable().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Met à jour le dictionaire en fonction du pco courant. Appelé lors des changemnts de selection du pco.
	 */
	private final void updateDataCtrePartie() {
		currentPcoVisa = null;
		dicoCtrePartie.clear();
		EOPlanComptableExer pco = (EOPlanComptableExer) myPanel.getSelectedPco();
		// Si le pco ne peut pas avoir de contrepartie (à cause de sa classe),
		// on désactive le panel de saisie
		boolean pcoCtrePartieActif = getPviLibelleForPco(pco) != null;
		myPanel.getCtrePartiePanel().setSaisieEnabled(pcoCtrePartieActif);
		if (pco != null) {
			dicoCtrePartie.put(EOPlancoVisa.ORDONNATEUR_KEY, pco);
			currentPcoVisa = (EOPlancoVisa) ZFinder.fetchObject(getEditingContext(), EOPlancoVisa.ENTITY_NAME, "ordonnateur=%@ and exercice=%@", new NSArray(new Object[] {
					pco, getExercice()
			}), null, true);
			dicoCtrePartie.put(EOPlancoVisa.PVI_LIBELLE_KEY, (getPviLibelleForPco(pco) == null ? "Contrepartie non autorisée pour ce compte" : getPviLibelleForPco(pco)));

			if (currentPcoVisa != null) {
				dicoCtrePartie.put("pcoNumCtrepartie", currentPcoVisa.ctrepartie().pcoNum());
				dicoCtrePartie.put("pcoNumTva", currentPcoVisa.tva().pcoNum());
				dicoCtrePartie.put(EOPlancoVisa.CTREPARTIE_KEY, currentPcoVisa.plancoExerCtrePartie());
				dicoCtrePartie.put(EOPlancoVisa.TVA_KEY, currentPcoVisa.plancoExerTva());
				//				dicoCtrePartie.put(EOPlancoVisa.CTREPARTIE_KEY, getPlancoExerForplanco(currentPcoVisa.ctrepartie()));
				//				dicoCtrePartie.put(EOPlancoVisa.TVA_KEY, getPlancoExerForplanco(currentPcoVisa.tva()));
				//				dicoCtrePartie.put(EOPlancoVisa.CTREPARTIE_KEY, currentPcoVisa.ctrepartie().currentPlancoExer());
				//				dicoCtrePartie.put(EOPlancoVisa.TVA_KEY, currentPcoVisa.tva().currentPlancoExer());
				dicoCtrePartie.put(EOPlancoVisa.PVI_LIBELLE_KEY, currentPcoVisa.pviLibelle());
				dicoCtrePartie.put(EOPlancoVisa.PVI_CONTREPARTIE_GESTION_KEY, currentPcoVisa.pviContrepartieGestion());
			}
		}
		try {
			myPanel.getCtrePartiePanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {
		NSMutableArray quals = new NSMutableArray();
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get(EOPlanComptableExer.CLASSE_COMPTE_KEY) != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("pcoNum like %@", new NSArray(new Object[] {
					"" + ((Integer) dicoFiltre.get(EOPlanComptableExer.CLASSE_COMPTE_KEY)).intValue() + "*"
			})));
		}
		if ((dicoFiltre.get(EOPlanComptableExer.PCO_NUM_KEY) != null) && (((String) dicoFiltre.get(EOPlanComptableExer.PCO_NUM_KEY)).length() > 0)) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("pcoNum like %@", new NSArray(((String) dicoFiltre.get(EOPlanComptableExer.PCO_NUM_KEY)) + "*")));
		}
		if ((dicoFiltre.get(EOPlanComptableExer.PCO_LIBELLE_KEY) != null) && (((String) dicoFiltre.get(EOPlanComptableExer.PCO_LIBELLE_KEY)).length() > 0)) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("pcoLibelle caseInsensitiveLike %@", new NSArray("*" + ((String) dicoFiltre.get(EOPlanComptableExer.PCO_LIBELLE_KEY)) + "*")));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}
		return quals;
	}

	private final NSArray getPcoValidesContrepartie() {
		//		return EOPlanComptableExerFinder.getPlancoExerValidesWithCond(getEditingContext(), getExercice(), "pcoNum like %@ or pcoNum like %@", new NSArray(new Object[] {
		//				"4*", "139*"
		//		}), false);

		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), new EOOrQualifier(new NSArray(new Object[] {
				EOsFinder.qualifierForPconumContrepartiesDepense(), EOsFinder.qualifierForPconumContrepartiesRecette()
		})), false);
	}

	private final NSArray getPcoValidesClasse445() {
		return EOPlanComptableExerFinder.getPlancoExerValidesWithCond(getEditingContext(), getExercice(), "pcoNum like %@", new NSArray(new Object[] {
				"445*"
		}), false);
	}

	private final void pcoNew() {
		PcoSaisieCtrl opdSaisieCtrl = new PcoSaisieCtrl(getEditingContext(), myPanel.getMyDialog(), pcosValidesNextExer);
		EOPlanComptableExer pco = opdSaisieCtrl.openDialogNew(getMyDialog(), (EOPlanComptableExer) myPanel.getSelectedPco());
		if (pco != null) {
			try {
				myPanel.updateData();
				myPanel.getPcoListPanel().setSelection(pco);
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	/**
	 * @param fils
	 * @return Le compte pere d'un compte d'imputation, null si le compte n'a pas de père.
	 * @throws DefaultClientException
	 */
	private final EOPlanComptableExer getCompteParent(EOPlanComptableExer fils) throws DefaultClientException {
		if (fils == null) {
			throw new DefaultClientException("le compte fils est nul.");
		}
		EOPlanComptableExer res = EOPlanComptableExer.getComptePere(getEditingContext(), getExercice(), fils.pcoNum());
		return res;
	}

	private final void pcoModifier() {

		PcoSaisieCtrl opdSaisieCtrl = new PcoSaisieCtrl(getEditingContext(), myPanel.getMyDialog(), pcosValidesNextExer);
		try {
			EOPlanComptableExer pco = opdSaisieCtrl.openDialogModify(getMyDialog(), getCompteParent((EOPlanComptableExer) myPanel.getSelectedPco()), (EOPlanComptableExer) myPanel.getSelectedPco());
			if (pco != null) {
				try {
					myPanel.updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void pcoSupprimer() {
		try {
			EOPlanComptableExer pco = (EOPlanComptableExer) myPanel.getSelectedPco();
			if (pco == null) {
				throw new DataCheckException("Aucun compte sélectionné");
			}

			//Verifier qu'il n'y a pas de compte enfants
			NSArray enfants = EOPlanComptableExerFinder.getPlancoExersForPcoNumLike(getEditingContext(), getExercice(), pco.pcoNum(), false);
			if (enfants != null && enfants.count() > 1) {
				throw new DataCheckException("Le compte " + pco.pcoNum() + " a des comptes subdivisés, vous ne pouvez pas le supprimer. Désactivez-le pour qu'il ne soit plus utilisable par les utilisateurs.");
			}

			Number res = FactoryProcessPlanComptableEtVisa.nbEcritureDetailsForPlanco(getEditingContext(), pco, getExercice());
			if (res != null && res.intValue() > 0) {
				throw new DataCheckException("Le compte " + pco.pcoNum() + " est utilisé dans les écritures, vous ne pouvez pas le supprimer. Désactivez-le pour qu'il ne soit plus utilisable par les utilisateurs.");
			}
			res = FactoryProcessPlanComptableEtVisa.nbEcritureDetailsForPlancoLike(getEditingContext(), pco, getExercice());
			if (res != null && res.intValue() > 0) {
				throw new DataCheckException("Des subdivisions du compte " + pco.pcoNum() + " sont utilisées dans les écritures, vous ne pouvez pas le supprimer. Désactivez-le pour qu'il ne soit plus utilisable par les utilisateurs.");
			}
			FactoryProcessPlanComptableEtVisa.checkPlancoUtiliseDansBudget(getEditingContext(), pco, getExercice());
			FactoryProcessPlanComptableEtVisa.checkPlancoUtilisePlancoVisa(getEditingContext(), pco, getExercice());

			boolean goOn = showConfirmationDialog("Confirmation", "Souhaitez-vous supprimer le compte n°" + pco.pcoNum() + " pour l'exercice " + getExercice().exeExercice().intValue() + " ?", ZMsgPanel.BTLABEL_NO);
			if (goOn) {
				FactoryProcessPlanComptableEtVisa myFactoryProcessPlanComptableEtVisa1 = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
				myFactoryProcessPlanComptableEtVisa1.supprimerPlanComptableExer(getEditingContext(), pco);
				ZLogger.verbose("deleted : ");
				ZLogger.verbose(getEditingContext().deletedObjects());
				for (int i = 0; i < getEditingContext().deletedObjects().count(); i++) {
					System.out.println(getEditingContext().deletedObjects().objectAtIndex(i).getClass().getName());
				}

				getEditingContext().saveChanges();
				myPanel.updateData();
				refreshActions();
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			showErrorDialog(e);
		}

	}

	private final void pcoInvalider() {
		try {
			EOPlanComptableExer pco = (EOPlanComptableExer) myPanel.getSelectedPco();
			if (pco == null) {
				throw new DataCheckException("Aucun compte sélectionné");
			}

			// TODO vérifier si le planco peut être annulé

			boolean goOn = showConfirmationDialog("Confirmation", "Souhaitez-vous désactiver le compte n°" + pco.pcoNum() + " ?", ZMsgPanel.BTLABEL_NO);
			if (goOn) {
				FactoryProcessPlanComptableEtVisa myFactoryProcessPlanComptableEtVisa1 = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
				myFactoryProcessPlanComptableEtVisa1.annulerPlanComptable(getEditingContext(), pco);
				getEditingContext().saveChanges();
				refreshActions();
				myPanel.getPcoListPanel().fireTableRowUpdated(myPanel.getPcoListPanel().selectedRowIndex());
				//				myPanel.getPcoListPanel().getMyTableModel().fireTableCellUpdated(((Integer) myPanel.getPcoListPanel().getMyTableModel().getMyDg().selectionIndexes().objectAtIndex(0)).intValue(),
				//						myPanel.getPcoListPanel().getMyTableModel().findColumn("pcoValidite"));
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			showErrorDialog(e);
		}
	}

	private final void pcoValider() {
		try {
			EOPlanComptableExer pco = (EOPlanComptableExer) myPanel.getSelectedPco();
			if (pco == null) {
				throw new DataCheckException("Aucun compte sélectionné");
			}

			boolean goOn = showConfirmationDialog("Confirmation", "Souhaitez-vous activer le compte n°" + pco.pcoNum() + " ?", ZMsgPanel.BTLABEL_NO);
			if (goOn) {
				FactoryProcessPlanComptableEtVisa myFactoryProcessPlanComptableEtVisa1 = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
				myFactoryProcessPlanComptableEtVisa1.ValiderPlanComptable(getEditingContext(), pco);
				getEditingContext().saveChanges();
				refreshActions();
				myPanel.getPcoListPanel().fireTableRowUpdated(myPanel.getPcoListPanel().selectedRowIndex());
				//				myPanel.getPcoListPanel().getMyTableModel().fireTableCellUpdated(((Integer) myPanel.getPcoListPanel().getMyTableModel().getMyDg().selectionIndexes().objectAtIndex(0)).intValue(),myPanel.getPcoListPanel().getMyTableModel().findColumn("pcoValidite"));
			}
		} catch (Exception e) {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}
			showErrorDialog(e);
		}
	}

	private void pcoImprimer() {

		// try {
		// String filePath =
		// ReportFactoryClient.imprimerPlanComptable(myApp.editingContext(),
		// myApp.temporaryDir , null, win);
		// if (filePath!=null) {
		// myApp.openPdfFile(filePath);
		// }
		// }
		// catch (Exception e1) {
		// showErrorDialog(e1);
		// }
	}

	private void fermer() {
		getEditingContext().revert();
		win.onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();

		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private final ZKarukeraDialog createModalDialog(Frame dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();

		win.setContentPane(myPanel);

		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Dialog dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		win1.setTitle(win1.getTitle() + " - " + getExercice().exeExercice().intValue());
		try {
			myPanel.updateData();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
	}

	public final void openDialog(Frame dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		win1.setTitle(win1.getTitle() + " - " + getExercice().exeExercice().intValue());
		try {
			myPanel.updateData();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
	}

	private final void refreshActions() {
		EOPlanComptableExer pco = (EOPlanComptableExer) myPanel.getSelectedPco();
		actionModify.setEnabled(pco != null);
		actionNew.setEnabled(pco != null);
		actionSupprimer.setEnabled(pco != null);
		if (pco == null) {
			actionCompteValider.setEnabled(false);
			actionCompteInvalider.setEnabled(false);
		}
		else {
			actionCompteValider.setEnabled(!EOPlanComptableExer.etatValide.equals(pco.pcoValidite()));
			actionCompteInvalider.setEnabled(!actionCompteValider.isEnabled());
			refreshActionOptionsBilan();

		}

	}

	private void refreshActionOptionsBilan() {
		//actionDeletePlancoBilan.setEnabled(myPanel.getOptionsBilanTable().selectedObject() != null);
		//actionAddPlancoBilan.setEnabled(myPanel.getSelectedPco() != null);
	}

	private final class PcoRecherchePanelListener implements PcoRecherchePanel.IPcoRecherchePanelListener {

		private final ColAmortissementProvider colAmortissementProvider = new ColAmortissementProvider();

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#actionInvalider()
		 */
		public Action actionInvalider() {
			return actionCompteInvalider;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#actionNew()
		 */
		public Action actionNew() {
			return actionNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#getPcos()
		 */
		public NSArray getPcos() {
			try {
				//				NSMutableArray sort = new NSMutableArray();
				//				sort.addObject(EOSortOrdering.sortOrderingWithKey(EOPlanComptableExer.PCO_NUM_KEY, EOSortOrdering.CompareAscending));
				return EOPlanComptableExerFinder.getPlancoExersWithQual(getEditingContext(), getExercice(), new EOAndQualifier(buildFilterQualifiers(pcoFilters)), true);
			} catch (Exception e) {
				showErrorDialog(e);
				return new NSArray();
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#getActionImprimer()
		 */
		public Action getActionImprimer() {
			return actionImprimer;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return pcoFilters;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#actionCompteValider()
		 */
		public Action actionCompteValider() {
			return actionCompteValider;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			refreshActions();
			updateDataCtrePartie();
			updateOptionBilanTable();
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#getPcoSaisieContrePartiePanelListener()
		 */
		public IPcoSaisieContrePartiePanelListener getPcoSaisieContrePartiePanelListener() {
			return myPcoSaisieContrePartiePanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#actionModify()
		 */
		public Action actionModify() {
			return actionModify;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRecherchePanel.IPcoRecherchePanelListener#onDbClick()
		 */
		public void onDbClick() {
			pcoModifier();

		}

		public ZEOTableModelColumnProvider getAmortissementProvider() {
			return colAmortissementProvider;
		}

		public ActionListener getComboClasseListener() {
			return classesComBoListener;
		}

		public DefaultComboBoxModel getComboClassesModel() {
			return classesComboboxModel;
		}

		public EOExercice getExercice() {
			return PcoRechercheCtrl.this.getExercice();
		}

		public Action actionSupprimer() {
			return actionSupprimer;
		}

		public IZEOTableCellRenderer getPcoRenderer() {
			return pcoTableCellRenderer;
		}

		public IOptionsBilanTableListener getOptionsBilanTableListener() {
			return optionsBilanTableListener;
		}
	}

	private final class ColAmortissementProvider implements ZEOTableModelColumnProvider {

		public Object getValueAtRow(int row) {
			final EOPlanComptableExer obj = (EOPlanComptableExer) myPanel.getPcoListPanel().getMyTableModel().getMyDg().displayedObjects().objectAtIndex(row);
			//			final EOPlanComptableExer obj = (EOPlanComptableExer) myPanel.getPcoListPanel().selectedObject();
			if (obj != null && obj.isAmortissementPossible()) {
				return EOsFinder.getPlancomptableAmoForPlanco(getEditingContext(), obj.planComptable(), getExercice());
			}
			return null;
		}

	}

	private final class PcoSaisieContrePartiePanelListener implements PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#dicoValues()
		 */
		public HashMap dicoValues() {
			return dicoCtrePartie;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#actionPlancoCtrePartieSelect()
		 */
		public AbstractAction actionPlancoCtrePartieSelect() {
			return actionPlancoCtrePartieSelect;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#actionPlancoTvaSelect()
		 */
		public AbstractAction actionPlancoTvaSelect() {
			return actionPlancoTvaSelect;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#actionCtrePartieAnnuler()
		 */
		public AbstractAction actionCtrePartieAnnuler() {
			return actionCtrePartieAnnuler;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#actionCtrePartieEnregistrer()
		 */
		public AbstractAction actionCtrePartieEnregistrer() {
			return actionCtrePartieEnregistrer;
		}

		public AbstractAction actionCtrePartieSupprimer() {
			return actionCtrePartieSupprimer;
		}

		//		/**
		//		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#actionCtrePartieDevalider()
		//		 */
		//		public AbstractAction actionCtrePartieDevalider() {
		//			return actionCtrePartieDevalider;
		//		}
		//
		//		/**
		//		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener#actionCtrePartieValider()
		//		 */
		//		public AbstractAction actionCtrePartieValider() {
		//			return actionCtrePartieValider;
		//		}

		public DefaultComboBoxModel getContrePartieGestionMdl() {
			return contrePartieGestionMdl;
		}

		public void onPconumCtrePartieUpdated() {
			final String pcoNumSaisie = (String) dicoCtrePartie.get("pcoNumCtrepartie");
			if (pcoNumSaisie != null) {
				final NSArray tmp = EOQualifier.filteredArrayWithQualifier(pcoValideClasse4, EOQualifier.qualifierWithQualifierFormat("pcoNum = '" + pcoNumSaisie + "'", null));
				EOPlanComptableExer res = null;
				if (tmp.count() == 1) {
					// pas de doute sur le choix du pcoNum
					res = (EOPlanComptableExer) tmp.objectAtIndex(0);
				}
				dicoCtrePartie.put("ctrepartie", res);
				myPanel.getCtrePartiePanel().updateDataLibellePconumCtrePartie();
			}
		}

		public void onPconumTvaUpdated() {
			final String pcoNumSaisie = (String) dicoCtrePartie.get("pcoNumTva");
			if (pcoNumSaisie != null) {
				final NSArray tmp = EOQualifier.filteredArrayWithQualifier(pcoValideClasse445, EOQualifier.qualifierWithQualifierFormat("pcoNum = '" + pcoNumSaisie + "'", null));
				EOPlanComptableExer res = null;
				if (tmp.count() == 1) {
					// pas de doute sur le choix du pcoNum
					res = (EOPlanComptableExer) tmp.objectAtIndex(0);
				}
				dicoCtrePartie.put("tva", res);
				myPanel.getCtrePartiePanel().updateDataLibelleTvaCtrePartie();
			}
		}

		public EOExercice getExercice() {
			return PcoRechercheCtrl.this.getExercice();
		}
	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouvel compte à partir du compte sélectionné");
		}

		public void actionPerformed(ActionEvent e) {
			pcoNew();
		}
	}

	private final class ActionModify extends AbstractAction {
		public ActionModify() {
			super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le compte sélectionné");
		}

		public void actionPerformed(ActionEvent e) {
			pcoModifier();
		}
	}

	private final class ActionSupprimer extends AbstractAction {
		public ActionSupprimer() {
			super("Supprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le compte sélectionné pour l'exercice " + getExercice().exeExercice().intValue());
		}

		public void actionPerformed(ActionEvent e) {
			pcoSupprimer();
		}
	}

	private final class ActionCompteDevalider extends AbstractAction {
		public ActionCompteDevalider() {
			super("Désactiver");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REDLED_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Désactiver le compte sélectionné, il ne sera plus utilisable pour passer des écritures. Vous Pourrez le réactiver ultérieurement.");
		}

		public void actionPerformed(ActionEvent e) {
			pcoInvalider();
		}
	}

	private final class ActionCompteValider extends AbstractAction {
		public ActionCompteValider() {
			super("Activer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_GREENLED_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Activer le compte sélectionné, il deviendra utilisable pour passer des écritures.");
		}

		public void actionPerformed(ActionEvent e) {
			pcoValider();
		}
	}

	private final class ActionImprimer extends AbstractAction {
		public ActionImprimer() {
			super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le plan comptable");
		}

		public void actionPerformed(ActionEvent e) {
			((ZActionCtrl.ActionIMPR007) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.IDU_IMPR007)).actionPerformed(e);
		}
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionPlancoCtrePartieSelect extends AbstractAction {
		public ActionPlancoCtrePartieSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour la contrepartie");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoCtrePartie();
		}
	}

	private final class ActionPlancoTvaSelect extends AbstractAction {
		public ActionPlancoTvaSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour la TVA");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoTva();
		}
	}

	private final class ActionCtrePartieValider extends AbstractAction {
		public ActionCtrePartieValider() {
			super("Valider");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_GREENLED_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Valider la contrepartie");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onValiderContrePartie();
		}
	}

	private final class ActionCtrePartieEnregistrer extends AbstractAction {
		public ActionCtrePartieEnregistrer() {
			super("Enregistrer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les modifications concernant la contrepartie");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onEnregistrerContrePartie();
		}
	}

	private final class ActionCtrePartieAnnuler extends AbstractAction {
		public ActionCtrePartieAnnuler() {
			super("Annuler");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_UNDO_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler les modifications concernant la contrepartie");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onAnnulerContrePartie();
		}
	}

	private final class ActionCtrePartieSupprimer extends AbstractAction {
		public ActionCtrePartieSupprimer() {
			super("Supprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer les informations concernant la contrepartie");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSupprimerContrePartie();
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

	private static final class PcoTableCellRenderer extends ZEOTableCellRenderer {
		private static final Image IMG_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16).getImage();
		private static final ZEOTable.ZImageCell CELL_OK = new ZEOTable.ZImageCell(new Image[] {
				IMG_OK
		});
		static {
			CELL_OK.setCentered(true);
		}

		private static final Icon ICON_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16);
		private static final JLabel LABEL_OK = new JLabel(ICON_OK);
		private static Color COL_CLASSE = Color.decode("#F9CC8A");
		private static Color COL_CHAPITRE = Color.decode("#DCEEA6");

		static {
			CELL_OK.setLayout(new BorderLayout());
			LABEL_OK.setOpaque(false);
		}

		public PcoTableCellRenderer() {
			super();
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableCellRenderer#getTableCellRendererComponent(javax.swing.JTable, java.lang.Object,
		 *      boolean, boolean, int, int)
		 */
		public final Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
			Component res = null;
			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final EOPlanComptableExer obj = (EOPlanComptableExer) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);

			final ZEOTableModelColumn column2 = ((ZEOTable) table).getDataModel().getColumn(table.convertColumnIndexToModel(column));
			if (EOPlanComptableExer.PCO_VALIDITE_KEY.equals(column2.getAttributeName())) {
				if (EOPlanComptableExer.etatValide.equals(value)) {
					//            		res = LABEL_OK;
					res = CELL_OK;
				}
				else {
					res = super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
				}
			}
			else {
				res = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}

			//////

			if (isSelected) {
				res.setBackground(table.getSelectionBackground());
				res.setForeground(table.getSelectionForeground());
			}
			else {
				if (obj.isClasse()) {
					res.setBackground(COL_CLASSE);
					res.setForeground(table.getForeground());
				}
				else if (obj.isChapitre()) {
					res.setBackground(COL_CHAPITRE);
					res.setForeground(table.getForeground());
				}
				else {
					res.setBackground(table.getBackground());
					res.setForeground(table.getForeground());
				}
			}
			return res;
		}
	}

	private class OptionsBilanTableListener implements IOptionsBilanTableListener {

		public void selectionChanged() {
			refreshActionOptionsBilan();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			// TODO Auto-generated method stub
			return null;
		}

		public NSArray getData() throws Exception {
			EOPlanComptableExer pco = (EOPlanComptableExer) myPanel.getSelectedPco();
			if (pco == null) {
				return NSArray.EmptyArray;
			}
			else {
				return NSArray.EmptyArray;
				//return pco.toPlancoBilanPostes();
			}

		}

		public void onDbClick() {

		}

		public Action getActionAddPlancoBilan() {
			return actionAddPlancoBilan;
		}

		public Action getActionDeletePlancoBilan() {
			return actionDeletePlancoBilan;
		}

	}

	private class ActionAddPlancoBilan extends AbstractAction {
		public ActionAddPlancoBilan() {
			super("+");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter une entrée dans le bilan");

		}

		public void actionPerformed(ActionEvent e) {
			addPlancoBilan();
		}

	}

	private class ActionDeletePlancoBilan extends AbstractAction {
		public ActionDeletePlancoBilan() {
			super("-");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'entrée dans le bilan");

		}

		public void actionPerformed(ActionEvent e) {
			deletePlancoBilan();
		}

	}

	private void addPlancoBilan() {
		showinfoDialogFonctionNonImplementee();

	}

	private void deletePlancoBilan() {
		showinfoDialogFonctionNonImplementee();

	}

}
