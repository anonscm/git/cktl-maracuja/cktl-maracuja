/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoVisa;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldListener;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoSaisieContrePartiePanel extends ZKarukeraPanel {
	//    private static final int DEFAULT_LABEL_WIDTH = 100;

	private IPcoSaisieContrePartiePanelListener myListener;

	private ZActionField pcoNumCtrePartieField;
	private ZTextField pcoLibelleCtrePartieField;
	private ZActionField pcoNumTvaField;
	private ZTextField pcoLibelleTvaField;
	private ZLabel pviLibelleField;

	private JComboBox myContrePartieGestionField;

	/**
     * 
     */
	public PcoSaisieContrePartiePanel(IPcoSaisieContrePartiePanelListener listener) {
		super();
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		myContrePartieGestionField = new JComboBox(myListener.getContrePartieGestionMdl());

		pcoNumCtrePartieField = new ZActionField(new PcoNumCtrePartieModel(), myListener.actionPlancoCtrePartieSelect());
		pcoNumCtrePartieField.getMyTexfield().setColumns(10);
		pcoNumCtrePartieField.setListener(new IZTextFieldListener() {
			public void afterUpdate() {
				myListener.onPconumCtrePartieUpdated();
			}
		});

		pcoLibelleCtrePartieField = new ZTextField(new PcoLibelleCtrePartieModel());
		pcoLibelleCtrePartieField.getMyTexfield().setColumns(50);
		pcoLibelleCtrePartieField.getMyTexfield().setEditable(false);

		pcoNumTvaField = new ZActionField(new PcoNumTvaModel(), myListener.actionPlancoTvaSelect());
		pcoNumTvaField.getMyTexfield().setColumns(10);
		pcoNumTvaField.setListener(new IZTextFieldListener() {
			public void afterUpdate() {
				myListener.onPconumTvaUpdated();
			}
		});

		pcoLibelleTvaField = new ZTextField(new PcoLibelleTvaModel());
		pcoLibelleTvaField.getMyTexfield().setColumns(50);
		pcoLibelleTvaField.getMyTexfield().setEditable(false);     

		pviLibelleField = new ZLabel(new PviLibelleModel());
		//        pviLibelleField.getMyTexfield().setColumns(30);
		//        pviLibelleField.getMyTexfield().setEditable(false);

		final ArrayList list = new ArrayList();
		list.add(new Component[] {
				new JLabel("Contrepartie :"), buildLine(new Component[] {
						pcoNumCtrePartieField, pcoLibelleCtrePartieField
				})
		});
		list.add(new Component[] {
				new JLabel("TVA :"), buildLine(new Component[] {
						pcoNumTvaField, pcoLibelleTvaField
				})
		});
		list.add(new Component[] {
				new JLabel("Code gestion pour la contrepartie :"), myContrePartieGestionField
		});
		list.add(new Component[] {
				new JLabel("Type :"), pviLibelleField
		});

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);

		JPanel p2 = new JPanel(new BorderLayout());
		p2.add(p, BorderLayout.CENTER);
		p2.add(buildRightPanel(), BorderLayout.EAST);

		setLayout(new BorderLayout());

		setBorder(BorderFactory.createTitledBorder((String) null));
		add(encloseInPanelWithTitle("<html>Contrepartie pour l'exercice <b>" + myListener.getExercice().exeExercice() + "</b></html>", null, ZConst.BG_COLOR_TITLE, p2, null, null), BorderLayout.NORTH);
		add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		//        list.add(myListener.actionCtrePartieValider());
		//        list.add(myListener.actionCtrePartieDevalider());
		list.add(myListener.actionCtrePartieEnregistrer());
		list.add(myListener.actionCtrePartieAnnuler());
		list.add(myListener.actionCtrePartieSupprimer());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		pcoNumCtrePartieField.updateData();
		pcoLibelleCtrePartieField.updateData();
		pcoNumTvaField.updateData();
		pcoLibelleTvaField.updateData();
		pviLibelleField.updateData();
		pcoLibelleCtrePartieField.moveCaret(0);
		pcoLibelleTvaField.moveCaret(0);
		myContrePartieGestionField.setSelectedItem(myListener.dicoValues().get(EOPlancoVisa.PVI_CONTREPARTIE_GESTION_KEY));
	}

	public void updateDataLibellePconumCtrePartie() {
		pcoLibelleCtrePartieField.updateData();
	}

	public void updateDataLibelleTvaCtrePartie() {
		pcoLibelleTvaField.updateData();
	}

	public interface IPcoSaisieContrePartiePanelListener {
		public HashMap dicoValues();

		public EOExercice getExercice();

		public void onPconumTvaUpdated();

		public void onPconumCtrePartieUpdated();

		public DefaultComboBoxModel getContrePartieGestionMdl();

		public AbstractAction actionCtrePartieAnnuler();

		public AbstractAction actionCtrePartieSupprimer();

		public AbstractAction actionCtrePartieEnregistrer();

		//        public AbstractAction actionCtrePartieDevalider();
		//        public AbstractAction actionCtrePartieValider();
		public AbstractAction actionPlancoTvaSelect();

		public AbstractAction actionPlancoCtrePartieSelect();
	}

	private class PcoNumCtrePartieModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get("pcoNumCtrepartie");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoNumCtrepartie", value);
		}

	}

	private class PcoLibelleCtrePartieModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			if (myListener.dicoValues().get("ctrepartie") == null) {
				return null;
			}
			return ((EOPlanComptableExer) myListener.dicoValues().get("ctrepartie")).pcoNum() + " " + ((EOPlanComptableExer) myListener.dicoValues().get("ctrepartie")).pcoLibelle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {

		}

	}

	private class PcoNumTvaModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get("pcoNumTva");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoNumTva", value);
		}

	}

	private class PcoLibelleTvaModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			if (myListener.dicoValues().get("tva") == null) {
				return null;
			}
			return ((EOPlanComptableExer) myListener.dicoValues().get("tva")).pcoNum() + " " + ((EOPlanComptableExer) myListener.dicoValues().get("tva")).pcoLibelle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {

		}

	}

	private class PviLibelleModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get("pviLibelle");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pviLibelle", value);
		}

	}

	/**
	 * @param pcoCtrePartieActif
	 */
	public final void setSaisieEnabled(boolean enabled) {
		pcoNumCtrePartieField.getMyTexfield().setEnabled(enabled);
		pcoNumTvaField.getMyTexfield().setEnabled(enabled);
		myListener.actionCtrePartieAnnuler().setEnabled(enabled);
		//        myListener.actionCtrePartieDevalider().setEnabled(enabled);
		myListener.actionCtrePartieEnregistrer().setEnabled(enabled);
		myListener.actionCtrePartieSupprimer().setEnabled(enabled);
		//        myListener.actionCtrePartieValider().setEnabled(enabled);
		myListener.actionPlancoCtrePartieSelect().setEnabled(enabled);
		myListener.actionPlancoTvaSelect().setEnabled(enabled);
		myContrePartieGestionField.setEnabled(enabled);
	}

}
