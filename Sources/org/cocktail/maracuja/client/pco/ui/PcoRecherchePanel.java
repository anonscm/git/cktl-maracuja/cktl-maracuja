/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.pco.ui.OptionsBilanTable.IOptionsBilanTableListener;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoRecherchePanel extends ZKarukeraPanel {
	private IPcoRecherchePanelListener myListener;

	private PcoRechercheFilterPanel filterPanel;
	private PcoListPanel pcoListPanel;
	private PcoSaisieContrePartiePanel ctrePartiePanel;

	//private OptionsBilanTable optionsBilanTable;

	/**
	 * @param editingContext
	 */
	public PcoRecherchePanel(IPcoRecherchePanelListener listener) {
		super();
		myListener = listener;
		filterPanel = new PcoRechercheFilterPanel(new PcoRechercheFilterPanelListener());
		pcoListPanel = new PcoListPanel(new PcoListPanelListener());
		ctrePartiePanel = new PcoSaisieContrePartiePanel(myListener.getPcoSaisieContrePartiePanelListener());
		//optionsBilanTable = new OptionsBilanTable(myListener.getOptionsBilanTableListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		filterPanel.setMyDialog(getMyDialog());
		filterPanel.initGUI();
		pcoListPanel.initGUI();
		ctrePartiePanel.initGUI();
		//optionsBilanTable.initGUI();

		JTabbedPane t = getTabs();
		JSplitPane splitPane = buildVerticalSplitPane(pcoListPanel, t);
		splitPane.setDividerLocation(0.95);
		splitPane.setResizeWeight(0.95);

		this.setLayout(new BorderLayout());
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(filterPanel, BorderLayout.NORTH);
		//	tmp.add(pcoListPanel, BorderLayout.CENTER);
		tmp.add(splitPane, BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(tmp, BorderLayout.CENTER);
	}

	public JTabbedPane getTabs() {
		final JTabbedPane p = new JTabbedPane();
		p.addTab("Contreparties " + myListener.getExercice().exeExercice(), ctrePartiePanel);
		//p.addTab("Bilan " + myListener.getExercice().exeExercice(), optionsBilanTable);
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		pcoListPanel.updateData();
		ctrePartiePanel.updateData();
		//optionsBilanTable.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionNew());
		list.add(myListener.actionModify());
		list.add(myListener.actionSupprimer());

		list.add(myListener.actionCompteValider());
		list.add(myListener.actionInvalider());
		list.add(myListener.getActionImprimer());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public Object getSelectedPco() {
		return pcoListPanel.selectedObject();
	}

	public interface IPcoRecherchePanelListener {
		public Action actionClose();

		public IOptionsBilanTableListener getOptionsBilanTableListener();

		public PcoSaisieContrePartiePanel.IPcoSaisieContrePartiePanelListener getPcoSaisieContrePartiePanelListener();

		public Action actionInvalider();

		public Action actionCompteValider();

		public Action actionNew();

		public Action actionSupprimer();

		public Action actionModify();

		/**
		 * @return Les pcos à afficher
		 */
		public NSArray getPcos();

		public Action getActionImprimer();

		/**
		 * @return un dictioniare contenant les filtres
		 */
		public HashMap getFilters();

		public Action actionSrch();

		public void onSelectionChanged();

		public void onDbClick();

		public ZEOTableModelColumnProvider getAmortissementProvider();

		public ActionListener getComboClasseListener();

		public DefaultComboBoxModel getComboClassesModel();

		public EOExercice getExercice();

		public IZEOTableCellRenderer getPcoRenderer();
	}

	private final class PcoRechercheFilterPanelListener implements PcoRechercheFilterPanel.IPcoRechercheFilterPanel {
		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRechercheFilterPanel.IPcoRechercheFilterPanel#getFilters()
		 */
		public HashMap getFilters() {
			return myListener.getFilters();
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoRechercheFilterPanel.IPcoRechercheFilterPanel#getActionSrch()
		 */
		public Action getActionSrch() {
			return myListener.actionSrch();
		}

		public ActionListener getComboClasseListener() {
			return myListener.getComboClasseListener();
		}

		public DefaultComboBoxModel getComboClassesModel() {
			return myListener.getComboClassesModel();
		}

	}

	private final class PcoListPanelListener implements PcoListPanel.IPcoListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoListPanel.IPcoListPanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getPcos();
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoListPanel.IPcoListPanelListener#onDbClick()
		 */
		public void onDbClick() {
			myListener.onDbClick();
		}

		public ZEOTableModelColumnProvider getAmortissementProvider() {
			return myListener.getAmortissementProvider();
		}

		public EOExercice getExercice() {
			return myListener.getExercice();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return myListener.getPcoRenderer();
		}

		public void selectionChanged() {
			myListener.onSelectionChanged();
		}

	}

	public PcoSaisieContrePartiePanel getCtrePartiePanel() {
		return ctrePartiePanel;
	}

	public PcoListPanel getPcoListPanel() {
		return pcoListPanel;
	}

	//	public OptionsBilanTable getOptionsBilanTable() {
	//		return optionsBilanTable;
	//	}
}
