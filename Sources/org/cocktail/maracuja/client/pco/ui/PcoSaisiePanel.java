/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableAmo;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoAmortissement;
import org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldListener;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoSaisiePanel extends ZKarukeraPanel {
	private final ZTextField myLibelle;
	private final ZTextField myNumero;
	private final ZTextField myLibelleParent;
	private final ZTextField myNumeroParent;
	private final ZTextField myPcoCompteBe;
	private final ZTextField myPcoNumAmo;
	private final ZTextField myPcoLibelleAmo;
	private final ZTextField myPcaDuree;

	//    private ZTextField 		myEmargement;
	//    private ZTextField 		myBudgetaire;
	//    private ZTextField 		myNature;

	private ZRadioButtonGroupPanel myBudgetaire;
	private ZRadioButtonGroupPanel myNature;
	private ZRadioButtonGroupPanel myEmargement;
	private ZRadioButtonGroupPanel mySensSolde;
	private ZRadioButtonGroupPanel myJBe;
	private ZRadioButtonGroupPanel myJExercice;
	//private ZRadioButtonGroupPanel myJFinExercice;

	private final PlancoCreditPanel plancoCreditPanelDep;
	private final PlancoCreditPanel plancoCreditPanelRec;

	private IPcoSaisiePanelListener myListener;
	private static final int DEFAULT_LABEL_WIDTH = 100;
	private static final String TITRE_TYPE_CREDITS_DEPENSE = "Dépenses";
	private static final String TITRE_TYPE_CREDITS_RECETTE = "Recettes";

	/**
     *
     */
	public PcoSaisiePanel(IPcoSaisiePanelListener listener) {
		super();
		myListener = listener;
		plancoCreditPanelDep = new PlancoCreditPanel(myListener.getPcoSaisiePanelDepListener());
		plancoCreditPanelRec = new PlancoCreditPanel(myListener.getPcoSaisiePanelRecListener());

		myPcoNumAmo = new ZTextField(new ZTextField.DefaultTextFieldModel(myListener.dicoValues(), EOPlanComptableAmo.PCOA_NUM_KEY));
		myPcoNumAmo.getMyTexfield().setColumns(10);
		myPcoNumAmo.setListener(new IZTextFieldListener() {

			public void afterUpdate() {
				myListener.onPconumAmoUpdated();
			}
		});

		myPcoLibelleAmo = new ZTextField(new ZTextField.DefaultTextFieldModel(myListener.dicoValues(), EOPlanComptableAmo.PCOA_LIBELLE_KEY));
		myPcoLibelleAmo.getMyTexfield().setColumns(25);
		myPcoLibelleAmo.getMyTexfield().setHorizontalAlignment(JTextField.LEADING);

		//        myPcaDuree = new ZTextField( new ZTextField.DefaultTextFieldModel (myListener.dicoValues(), EOPlancoAmortissement.PCA_DUREE_KEY)   );
		myPcaDuree = new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.dicoValues(), EOPlancoAmortissement.PCA_DUREE_KEY), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT);
		myPcaDuree.getMyTexfield().setColumns(2);

		myLibelleParent = new ZTextField(new LibelleParentModel());
		myLibelleParent.getMyTexfield().setColumns(40);
		myLibelleParent.getMyTexfield().setEditable(false);
		myLibelleParent.setUIReadOnly();

		myNumeroParent = new ZTextField(new NumeroParentModel());
		myNumeroParent.getMyTexfield().setColumns(20);
		myNumeroParent.getMyTexfield().setEditable(false);
		myNumeroParent.setUIReadOnly();

		myPcoCompteBe = new ZTextField(new PcoCompteBeModel());
		myPcoCompteBe.getMyTexfield().setColumns(20);

		myLibelle = new ZTextField(new LibelleModel());
		myLibelle.getMyTexfield().setColumns(40);

		myNumero = new ZTextField(new NumeroModel());
		myNumero.getMyTexfield().setColumns(20);

		myBudgetaire = new ZRadioButtonGroupPanel(new BudgetaireRadioModel());
		myNature = new ZRadioButtonGroupPanel(new NatureRadioModel());
		myEmargement = new ZRadioButtonGroupPanel(new EmargementRadioModel());
		mySensSolde = new ZRadioButtonGroupPanel(new SensSoldeRadioModel());
		myJBe = new ZRadioButtonGroupPanel(new JBeRadioModel());
		myJExercice = new ZRadioButtonGroupPanel(new JExerciceRadioModel());
		//myJFinExercice = new ZRadioButtonGroupPanel(new JFinExerciceRadioModel());

		final JComponent tc = buildPcoCreditsPanel();
		final JPanel journalPanel = buildJournalPanel();
		journalPanel.setBorder(BorderFactory.createTitledBorder((String) null));
		final JComponent amoPanel = buildPcoAmortissementPanel();
		amoPanel.setBorder(BorderFactory.createTitledBorder((String) null));
		final Box leftBottom = ZUiUtil.buildBoxColumn(new Component[] {
				journalPanel, amoPanel
		});

		final JPanel lig = new JPanel(new BorderLayout());
		lig.add(ZUiUtil.alignInNewPanel(leftBottom, BorderLayout.NORTH), BorderLayout.WEST);
		lig.add(tc, BorderLayout.CENTER);

		JPanel p1 = new JPanel(new GridLayout(3, 2));
		//        p1.setLayout();
		p1.add(buildLine(new ZLabeledComponent("Emargement", myEmargement, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		p1.add(buildLine(new ZLabeledComponent("Nature", myNature, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		p1.add(buildLine(new ZLabeledComponent("Budget par nature", myBudgetaire, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		p1.add(buildLine(new Component[] {
				new ZLabeledComponent("Solde fin exercice", mySensSolde, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH),
				Box.createHorizontalStrut(15),
				ZTooltip.createTooltipLabel("Solde du compte", "Indiquez ici comment doit être le solde de ce compte en fin d'année. <br>Cette indication sera utilisée pour les contrôles.")
		}));
		p1.add(buildLine(new Component[] {
				new ZLabeledComponent("Compte de BE", myPcoCompteBe, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH),
				Box.createHorizontalStrut(15),
				ZTooltip.createTooltipLabel("Compte BE", "Indiquez ici le n° du compte sur lequel créer l'écriture de BE pour ce compte.<br><i>Par exemple indiquez <b>4011</b> pour le compte <b>4012</b>.</i>")
		}));

		Box col1 = Box.createVerticalBox();
		col1.add(buildLine(new ZLabeledComponent("Compte parent", buildLine(new Component[] {
				myNumeroParent, myLibelleParent
		}), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		col1.add(buildLine(new Component[] {
				new ZLabeledComponent("N° compte", myNumero, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH), new ZLabeledComponent("Libellé", myLibelle, ZLabeledComponent.LABELONLEFT, -1)
		}));
		col1.add(p1);
		//        col1.add(buildLine(new Component[]{new ZLabeledComponent("Nature", myNature, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH), new ZLabeledComponent("Budget par nature", myBudgetaire, ZLabeledComponent.LABELONLEFT, -1)}));
		//        col1.add(buildLine((new ZLabeledComponent("Relance", myRelance, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
		//        col1.add(buildLine(new Component[]{new JLabel("Solde de ce compte"), mySensSolde, Box.createHorizontalStrut(15), ZTooltip.createTooltipLabel("Solde du compte", "Indiquez ici comment doit être le solde de ce compte en fin d'année. <br>Cette indication sera utilisée pour les contrôles." ), new ZLabeledComponent("Compte de BE", myPcoCompteBe, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH), Box.createHorizontalStrut(15), ZTooltip.createTooltipLabel("Compte BE", "Indiquez ici le n° du compte sur lequel créer l'écriture de BE pour ce compte.<br><i>Par exemple indiquez <b>4011</b> pour le compte <b>4012</b>.</i>" ) }));

		col1.add(lig);
		col1.add(Box.createVerticalGlue());

		setLayout(new BorderLayout());
		add(col1, BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {

	}

	private final JPanel buildJournalPanel() {
		final ArrayList comps = new ArrayList();
		comps.add(buildLine(new ZLabeledComponent("Journal exercice", buildLine(new Component[] {
				myJExercice
		}), ZLabeledComponent.LABELONLEFT, 150)));
		//		comps.add(buildLine(new ZLabeledComponent("Journal fin d'exercice", buildLine(new Component[] {
		//				myJFinExercice
		//}),
		//ZLabeledComponent.LABELONLEFT, 150)));
		comps.add(buildLine(new ZLabeledComponent("Journal balance d'entrée", buildLine(new Component[] {
				myJBe
		}), ZLabeledComponent.LABELONLEFT, 150)));

		final Box col1 = ZUiUtil.buildBoxColumn(comps);
		//        col1.setBorder(BorderFactory.createEmptyBorder(1,2,1,2));
		return encloseInPanelWithTitle("Journaux dans lequel le compte est utilisable", null, ZConst.BG_COLOR_TITLE, col1, null, null);
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionAnnuler());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private JComponent buildPcoCreditsPanel() {

		final JTabbedPane p = new JTabbedPane();
		p.addTab(TITRE_TYPE_CREDITS_DEPENSE, buildPcoCreditDepPanel());
		p.addTab(TITRE_TYPE_CREDITS_RECETTE, buildPcoCreditRecPanel());
		final JPanel tc = encloseInPanelWithTitle("Types de crédit (pour " + myListener.getExeercice().exeExercice().intValue() + ")",
				null, ZConst.BG_COLOR_TITLE, p, null, null);
		tc.setBorder(BorderFactory.createTitledBorder((String) null));
		return tc;
	}

	private JComponent buildPcoCreditDepPanel() {
		plancoCreditPanelDep.initGUI();
		//        final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_SIGNATAIRES, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16),ZConst.BGCOLOR_YELLOW, Color.BLACK, BorderLayout.WEST);
		final JPanel p00 = new JPanel(new BorderLayout());
		p00.add(plancoCreditPanelDep, BorderLayout.CENTER);
		//        p00.add( ZUiUtil.buildTitlePanel(labelSignataireTc, null, ZConst.GRADIENTTITLESTARTCOLOR, null, null) , BorderLayout.NORTH);        
		final JPanel p7 = new JPanel(new BorderLayout());
		p7.add(p00, BorderLayout.CENTER);
		//        p7.add(commentPanel, BorderLayout.NORTH);
		return p7;
	}

	private JComponent buildPcoCreditRecPanel() {
		plancoCreditPanelRec.initGUI();
		//        final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_SIGNATAIRES, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16),ZConst.BGCOLOR_YELLOW, Color.BLACK, BorderLayout.WEST);
		final JPanel p00 = new JPanel(new BorderLayout());
		p00.add(plancoCreditPanelRec, BorderLayout.CENTER);
		//        p00.add( ZUiUtil.buildTitlePanel(labelSignataireTc, null, ZConst.GRADIENTTITLESTARTCOLOR, null, null) , BorderLayout.NORTH);        
		final JPanel p7 = new JPanel(new BorderLayout());
		p7.add(p00, BorderLayout.CENTER);
		//        p7.add(commentPanel, BorderLayout.NORTH);
		return p7;
	}

	private JComponent buildPcoAmortissementPanel() {
		final ArrayList comps = new ArrayList();
		//        final JButton b1 = ZUiUtil.getSmallButtonFromAction(myListener.actionSrchPlancomptableAmo()); 
		final JLabel label1 = new JLabel("N°");
		final JLabel label2 = new JLabel("Libellé");
		final JLabel label3 = new JLabel("Durée amortissement (années)");
		label1.setPreferredSize(label2.getPreferredSize());

		comps.add(ZUiUtil.buildBoxLine(new Component[] {
				label1, Box.createHorizontalStrut(4), myPcoNumAmo
		}, BorderLayout.WEST));
		comps.add(ZUiUtil.buildBoxLine(new Component[] {
				label2, Box.createHorizontalStrut(4), myPcoLibelleAmo
		}, BorderLayout.WEST));
		comps.add(ZUiUtil.buildBoxLine(new Component[] {
				label3, Box.createHorizontalStrut(4), myPcaDuree
		}, BorderLayout.WEST));
		final Box b = ZUiUtil.buildBoxColumn(comps);
		return ZUiUtil.encloseInPanelWithTitle("Compte d'amortissement (pour " + myListener.getExeercice().exeExercice().intValue() + ")", null, ZConst.BG_COLOR_TITLE, ZUiUtil.alignInNewPanel(b, BorderLayout.NORTH), null, null);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		plancoCreditPanelDep.updateData();
		plancoCreditPanelRec.updateData();
		myLibelle.updateData();
		myNumero.updateData();
		myLibelleParent.updateData();
		myNumeroParent.updateData();
		mySensSolde.updateData();
		myNature.updateData();
		myBudgetaire.updateData();
		myEmargement.updateData();
		myPcoNumAmo.updateData();
		updateDataAmortissementLibelle();
		myPcaDuree.updateData();

		myJBe.updateData();
		myJExercice.updateData();
		//myJFinExercice.updateData();
		myPcoCompteBe.updateData();

	}

	public void updateDataAmortissementLibelle() {
		myPcoLibelleAmo.updateData();
		myPcoLibelleAmo.moveCaret(0);
	}

	private final class LibelleModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get(EOPlanComptable.PCO_LIBELLE_KEY);
		}

		public void setValue(Object value) {
			myListener.dicoValues().put(EOPlanComptable.PCO_LIBELLE_KEY, value);
		}

	}

	private final class LibelleParentModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get("pcoLibelleParent");
		}

		public void setValue(Object value) {
			myListener.dicoValues().put("pcoLibelleParent", value);
		}

	}

	private final class NumeroModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get(EOPlanComptable.PCO_NUM_KEY);
		}

		public void setValue(Object value) {
			myListener.dicoValues().put(EOPlanComptable.PCO_NUM_KEY, value);
		}

	}

	private final class NumeroParentModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get("pcoNumParent");
		}

		public void setValue(Object value) {
			myListener.dicoValues().put("pcoNumParent", value);
		}

	}

	private final class PcoCompteBeModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get("pcoCompteBe");
		}

		public void setValue(Object value) {
			myListener.dicoValues().put("pcoCompteBe", value);
		}

	}

	private final class BudgetaireRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public BudgetaireRadioModel() {
			values = new HashMap();
			values.put("Oui", EOPlanComptableExer.pcoBudgetaireOui);
			values.put("Non", EOPlanComptableExer.pcoBudgetaireNon);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoBudgetaire");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoBudgetaire", value);
		}

	}

	private final class JBeRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public JBeRadioModel() {
			values = new HashMap();
			values.put("Oui", EOPlanComptableExer.pcoJBeOui);
			values.put("Non", EOPlanComptableExer.pcoJBeNon);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoJBe");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoJBe", value);
		}

	}

	private final class JExerciceRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public JExerciceRadioModel() {
			values = new HashMap();
			values.put("Oui", EOPlanComptableExer.pcoJExerciceOui);
			values.put("Non", EOPlanComptableExer.pcoJExerciceNon);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoJExercice");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoJExercice", value);
		}

	}

	private final class JFinExerciceRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public JFinExerciceRadioModel() {
			values = new HashMap();
			values.put("Oui", EOPlanComptableExer.pcoJFinExerciceOui);
			values.put("Non", EOPlanComptableExer.pcoJFinExerciceNon);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoJFinExercice");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoJFinExercice", value);
		}

	}

	private final class NatureRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public NatureRadioModel() {
			values = new HashMap();
			values.put("Dépense", EOPlanComptableExer.pcoNatureDepense);
			values.put("Recette", EOPlanComptableExer.pcoNatureRecette);
			values.put("Tiers", EOPlanComptableExer.pcoNatureTiers);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoNature");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoNature", value);
		}

	}

	private final class EmargementRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public EmargementRadioModel() {
			values = new HashMap();
			values.put("Oui", EOPlanComptableExer.pcoEmargementOui);
			values.put("Non", EOPlanComptableExer.pcoEmargementNon);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoEmargement");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoEmargement", value);
		}

	}

	private final class SensSoldeRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public SensSoldeRadioModel() {
			values = new HashMap();
			values.put("Créditeur", EOPlanComptableExer.PCOSENSSOLDE_CREDIT);
			values.put("Débiteur", EOPlanComptableExer.PCOSENSSOLDE_DEBIT);
			values.put("Non défini", EOPlanComptableExer.PCOSENSSOLDE_LES2);
			values.put("Nul", EOPlanComptableExer.PCOSENSSOLDE_ZERO);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("pcoSensSolde");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoSensSolde", value);
		}

	}

	/**
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public interface IPcoSaisiePanelListener {
		public HashMap dicoValues();

		public void onPconumAmoUpdated();

		public Action actionSrchPlancomptableAmo();

		public IPlancoCreditPanelListener getPcoSaisiePanelRecListener();

		public IPlancoCreditPanelListener getPcoSaisiePanelDepListener();

		public Action actionValider();

		public Action actionAnnuler();

		public EOExercice getExeercice();
	}

	/**
     *
     */
	public final void lockForModify() {
		myNumero.setEnabled(false);
	}

}
