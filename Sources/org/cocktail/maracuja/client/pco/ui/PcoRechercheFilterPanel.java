/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoRechercheFilterPanel extends ZKarukeraPanel {
    private final Color BORDURE_COLOR=getBackground().brighter();
    
    private IPcoRechercheFilterPanel myListener;
    
    
    private ZTextField  fNumero;
    private ZTextField  fLibelle;
    private JComboBox comboClasse;
    
    
    
    
    /**
     * @param editingContext
     */
    public PcoRechercheFilterPanel(IPcoRechercheFilterPanel listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(new JButton(myListener.getActionSrch() ), BorderLayout.EAST);
    }

    
    


    private final JPanel buildFilters() {
    	comboClasse = new JComboBox(myListener.getComboClassesModel());
    	comboClasse.addActionListener(myListener.getComboClasseListener());
    	
        fNumero = new ZTextField(new NumeroModel());
        fNumero.getMyTexfield().setColumns(10);
        
        fLibelle = new ZTextField(new LibelleModel());
        fLibelle.getMyTexfield().setColumns(30);
      
        
        JPanel p = new JPanel(new BorderLayout());
        ZFormPanel[] comps = new ZFormPanel[3];
        comps[0] = ZFormPanel.buildLabelField("Classe", comboClasse);
        comps[1] = ZFormPanel.buildLabelField("N°", fNumero);
        comps[2] = ZFormPanel.buildLabelField("Libellé", fLibelle);
        
        comps[1].setDefaultAction(myListener.getActionSrch());
        comps[2].setDefaultAction(myListener.getActionSrch());
        
//        setSimpleLineBorder(comps[0]);
//        setSimpleLineBorder(comps[1]);        
        
        
//        comps[1] = new ZLabeledComponent("Libellé", fLibelle, ZLabeledComponent.LABELONLEFT, -1) ;
//        comps[0] = new ZLabeledComponent("N°", fNumero, ZLabeledComponent.LABELONLEFT, -1) ;
//        comps[1] = new ZLabeledComponent("Libellé", fLibelle, ZLabeledComponent.LABELONLEFT, -1) ;
        
        p.add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;
    }
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        fNumero.updateData();
    }

    public interface IPcoRechercheFilterPanel {
        public Action getActionSrch();
        public DefaultComboBoxModel getComboClassesModel();
        public ActionListener getComboClasseListener();

		/**
         * @return un dictionnaire dans lequel sont stockés les filtres.
         */
        public HashMap getFilters();
        
    }
    
    private final class NumeroModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get(EOPlanComptable.PCO_NUM_KEY) ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put(EOPlanComptable.PCO_NUM_KEY,value);
            
        }
        
    }
    private final class LibelleModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get(EOPlanComptable.PCO_LIBELLE_KEY) ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put(EOPlanComptable.PCO_LIBELLE_KEY,value);
            
        }
    }
}
