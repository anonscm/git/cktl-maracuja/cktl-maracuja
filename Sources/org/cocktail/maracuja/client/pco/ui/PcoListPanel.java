/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoListPanel extends ZKarukeraTablePanel {
    private IPcoListPanelListener myListener;

    /**
     * @param editingContext
     */
    public PcoListPanel(IPcoListPanelListener listener) {
        super(listener);
        myListener = listener;
        
        final ZEOTableModelColumn col0 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_NUM_KEY, "N° compte", 90);
        col0.setAlignment(SwingConstants.LEFT);
        col0.setColumnClass(Integer.class);

        final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_LIBELLE_KEY, "Libellé", 250);
        col1.setAlignment(SwingConstants.LEFT);

        final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_NATURE_KEY, "Nature", 30);
        col2.setAlignment(SwingConstants.CENTER);

        final ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_NIVEAU_KEY, "Niveau", 30);
        col3.setAlignment(SwingConstants.CENTER);
        col3.setColumnClass(Integer.class);

        final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_BUDGETAIRE_KEY, "Budgétaire", 30);
        col4.setAlignment(SwingConstants.CENTER);

        final ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_EMARGEMENT_KEY, "Emargement", 30);
        col5.setAlignment(SwingConstants.CENTER);

        final ZEOTableModelColumn col7 = new ZEOTableModelColumn(myDisplayGroup, EOPlanComptableExer.PCO_VALIDITE_KEY, "Etat", 40);
        col7.setAlignment(SwingConstants.CENTER);
        
        final ZEOTableModelColumn col8 = new ZEOTableModelColumnWithProvider("Amortissement "+ myListener.getExercice().exeExercice(), myListener.getAmortissementProvider(),50);
        col8.setAlignment(SwingConstants.CENTER);        
        
        colsMap.clear();
        colsMap.put(EOPlanComptableExer.PCO_NUM_KEY, col0);
        colsMap.put(EOPlanComptableExer.PCO_LIBELLE_KEY, col1);
        colsMap.put(EOPlanComptableExer.PCO_NIVEAU_KEY, col3);
        colsMap.put(EOPlanComptableExer.PCO_NATURE_KEY, col2);
        colsMap.put(EOPlanComptableExer.PCO_BUDGETAIRE_KEY, col4);
        colsMap.put(EOPlanComptableExer.PCO_EMARGEMENT_KEY, col5);
        colsMap.put("Amortissement", col8);
        colsMap.put(EOPlanComptableExer.PCO_VALIDITE_KEY, col7);
    }

    
    public interface IPcoListPanelListener extends IZKarukeraTablePanelListener {
        public EOExercice getExercice();
		public ZEOTableModelColumnProvider getAmortissementProvider();
    }
    
    public void setSelection(EOPlanComptableExer planComptableExer) {
    	getMyEOTable().scrollToSelection(planComptableExer);
    }
    
    public ZEOTableModel getMyTableModel() {
    	return myTableModel;
    }
    
}
