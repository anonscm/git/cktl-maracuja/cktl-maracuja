/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOPlancoCredit;
import org.cocktail.maracuja.client.metier.EOTypeCredit;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PlancoCreditPanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {

	private static final String CONST_LIBELLE = "Libellé";
    private static final String CONST_CODE = "Code";
    private static final String CONST_AUTORISE = "Autorisé";

    private JPopupMenu myPopupMenu;
//	private boolean valueHasChanged;

	private ZEOTableModel 			myTableModel;
	private ZEOTable 				myEOTable;
	private TableSorter					myTableSorter;
	private CBTableModelListener		myCBTableModelListener;	
	
	private IPlancoCreditPanelListener myListener;
	
//	private HashMap						dicoValeurs;
	private Vector 						myCols;
	
	private EODisplayGroup myDg; 
	private JComboBox plaQuoiBox;


    public PlancoCreditPanel(IPlancoCreditPanelListener listener) {
        super();
        myListener = listener;
        myDg = new EODisplayGroup();
    }



	public void initGUI() {
	    setLayout(new BorderLayout());
	    plaQuoiBox = new JComboBox();
	    plaQuoiBox.addItem(EOPlancoCredit.quoiDepense);
	    plaQuoiBox.addItem(EOPlancoCredit.quoiRecette);
	    
	    
	    initTableModel();
		initTable();
		initPopupMenu();
		
		
		
		JScrollPane scrollPane = new JScrollPane(myEOTable);
		add(scrollPane, BorderLayout.CENTER);		
	}
	
    

	
	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		myEOTable.addListener(this);
	}
		
	/**
	 * Initialise le modeele le la table à afficher.
	 *
	 */
	private void initTableModel() {
		myCols = new Vector(2,0);
		ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDg,CONST_AUTORISE,80,myListener.dicoOfTypeCredits()); 
		col0.setColumnClass(Boolean.class);
		col0.setResizable(false);
		col0.setEditable(true);
//		col0.setMyModifier(new CheckAutorisationModifier());
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, EOTypeCredit.TCD_CODE_KEY,CONST_CODE, 70);
		col1.setAlignment(SwingConstants.CENTER);
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg, EOTypeCredit.TCD_LIBELLE_KEY,CONST_LIBELLE, 250);
		col3.setAlignment(SwingConstants.LEFT);
//		ZEOTableModelColumn col4 = new ZEOTableModelColumnWithProvider("Type", new PlaQuoiProvider(), 115);
//		col4.setAlignment(SwingConstants.CENTER);
//		col4.setEditable(true);
//		col4.setMyModifier(new PlaQuoiModifier());
//		col4.setTableCellEditor( new PlaquoiCellEditor( plaQuoiBox  ));
		
		myCols.add(col0);
		myCols.add(col1);
		myCols.add(col3);		
//		myCols.add(col4);		
		
		myCBTableModelListener = new CBTableModelListener();
		myTableModel = new ZEOTableModel(myDg,myCols);
		myTableModel.addTableModelListener(myCBTableModelListener);						
		myTableSorter = new TableSorter (myTableModel);
	}
				
	
	public void updateData() {
		try {
//			valueHasChanged = false;
//			ZLogger.debug(myListener.getAllTypeCredits());
			myDg.setObjectArray(myListener.getAllTypeCredits() );
			
			myTableModel.removeTableModelListener(myCBTableModelListener);
			myTableModel.updateInnerRowCount();
			myTableModel.fireTableDataChanged();
			myTableModel.addTableModelListener(myCBTableModelListener);
//			valueHasChanged = false;
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}


	

	

	
	public class CBTableModelListener implements TableModelListener {
		public void tableChanged(TableModelEvent e) {
//			valueHasChanged = true;
			if (myListener!=null) {
				myListener.changeEditMode(true);				
			}			
		}
	}
	

	
	

	
	
	public interface IPlancoCreditPanelListener {
		public void changeEditMode(boolean editMode);
		/**
         * @return
         */
        public NSArray getAllTypeCredits();
        public boolean isEditingState();
		public void onSelectionChanged();
		public void onCheck();
		/** Un dictionnaire contenant en valeur un enregistrement de TypeCedit et en valeur Boolea.TRUE ou Boolean.FALSE*/
		public HashMap dicoOfTypeCredits();
		/** Un dictionnaire contenant en valeur un enregistrement de TypeCedit et en valeur D ou R*/
//		public HashMap dicoOfPlaQuoi();
		
		
		
	}

	
	
	



	private void initPopupMenu() {
		myPopupMenu = new JPopupMenu();
		myPopupMenu.add(new ActionCheckAll());
		myPopupMenu.add(new ActionUncheckAll());
		myEOTable.setPopup(myPopupMenu);
	}
	
	
	private class ActionCheckAll extends AbstractAction {
		private static final String CONST_TOUT_COCHER = "Tout cocher";
        public ActionCheckAll() {
			super(CONST_TOUT_COCHER);
		}
		public void actionPerformed(ActionEvent e) {
			((ZEOTableModelColumnDico)myCols.get(0)).setValueForAllRows(Boolean.valueOf(true));
			myTableModel.fireTableDataChanged();
		}
	}
	
	private class ActionUncheckAll extends AbstractAction {
		private static final String CONST_TOUT_DECOCHER = "Tout décocher";
        public ActionUncheckAll() {
			super(CONST_TOUT_DECOCHER);
		}
		public void actionPerformed(ActionEvent e) {
			((ZEOTableModelColumnDico)myCols.get(0)).setValueForAllRows(Boolean.valueOf(false));
			myTableModel.fireTableDataChanged();
		}
	}






    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        return;
        
    }



    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
       myListener.onSelectionChanged();
    }	
	


}
