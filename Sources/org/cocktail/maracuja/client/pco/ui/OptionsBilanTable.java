/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoBilanPoste;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


public class OptionsBilanTable extends ZKarukeraTablePanel {
	private final ImageIcon iconInfo = ZIcon.getIconForName(ZIcon.ICON_INFORMATION_32);

	private IOptionsBilanTableListener myListener;

	/**
	 * @param editingContext
	 */
	public OptionsBilanTable(IOptionsBilanTableListener listener) {
		super(listener);
		myListener = listener;

		final ZEOTableModelColumn col0 = new ZEOTableModelColumn(myDisplayGroup, EOPlancoBilanPoste.DISPLAY_POSTE_KEY, "Poste", 300);
		col0.setAlignment(SwingConstants.LEFT);
		col0.setColumnClass(Integer.class);

		final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, EOPlancoBilanPoste.DISPLAY_SIGNEETSENS_KEY, "Prise en compte du solde", 30);
		col1.setAlignment(SwingConstants.CENTER);

		final ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, EOPlancoBilanPoste.DISPLAY_COLONNE_KEY, "Colonne", 20);
		col3.setAlignment(SwingConstants.CENTER);

		final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, EOPlancoBilanPoste.DISPLAY_SUBDIVISIONS_KEY, "Prise en compte des subdivisions", 40);
		col2.setAlignment(SwingConstants.CENTER);

		colsMap.clear();
		colsMap.put(EOPlancoBilanPoste.DISPLAY_SIGNEETSENS_KEY, col1);
		colsMap.put(EOPlancoBilanPoste.DISPLAY_POSTE_KEY, col0);
		colsMap.put(EOPlancoBilanPoste.DISPLAY_COLONNE_KEY, col3);
	}

	public interface IOptionsBilanTableListener extends IZKarukeraTablePanelListener {

		public Action getActionAddPlancoBilan();

		public Action getActionDeletePlancoBilan();

	}

	public void setSelection(EOPlanComptableExer planComptableExer) {
		getMyEOTable().scrollToSelection(planComptableExer);
	}

	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

	public void initGUI() {
		super.initGUI();
		final ZCommentPanel commentPanel = new ZCommentPanel("Paramétrage du bilan",
				"<html>Indiquez les postes du bilan pour lesquels ce compte et ses subdivisions doivent être prises en compte.<br>"
				, iconInfo);

		this.add(commentPanel, BorderLayout.NORTH);
		this.add(buildActionToolBar(), BorderLayout.EAST);

		//myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}

	private JToolBar buildActionToolBar() {
		JToolBar tmpToolBar = new JToolBar("", JToolBar.VERTICAL);
		tmpToolBar.setFloatable(false);
		tmpToolBar.setRollover(false);
		tmpToolBar.add(Box.createRigidArea(new Dimension(2, 1)));
		tmpToolBar.add(myListener.getActionAddPlancoBilan());
		tmpToolBar.add(myListener.getActionDeletePlancoBilan());
		return tmpToolBar;
	}

}
