/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.pco;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPlanComptableEtVisa;
import org.cocktail.maracuja.client.factory.process.FactoryProcessPlanComptableTypeCredit;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableAmo;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoAmortissement;
import org.cocktail.maracuja.client.metier.EOPlancoCredit;
import org.cocktail.maracuja.client.metier.EOTypeCredit;
import org.cocktail.maracuja.client.pco.ui.PcoSaisiePanel;
import org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel;
import org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PcoSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'un compte";

	/** Caractères autorises pour la creation des comptes */
	public static final String CARACTERES_AUTORISES = "0123456789abcdefghijklmnopqrstuvwxyz";
	// private Window parentWindow;
	private final HashMap pcoDico;
	private final PcoSaisiePanel myPanel;

	private final ActionAnnuler actionAnnuler = new ActionAnnuler();
	private final ActionValider actionValider = new ActionValider();
	private final ActionSrchPlancomptableAmo actionSrchPlancomptableAmo = new ActionSrchPlancomptableAmo();

	private ZKarukeraDialog win;
	private EOPlanComptableExer currentPco;
	private int mode;
	private static final int MODE_MODIF = 2;
	private static final int MODE_NEW = 1;
	private static final Dimension WINDOW_DIMENSION = new Dimension(850, 600);

	private final PlancoCreditDepPanelListener plancoCreditDepPanelListener = new PlancoCreditDepPanelListener();
	private final HashMap dicoOfTypeCreditsDep = new HashMap();
	private final NSArray allTypeCreditsDep;

	private final PlancoCreditRecPanelListener plancoCreditRecPanelListener = new PlancoCreditRecPanelListener();
	private final HashMap dicoOfTypeCreditsRec = new HashMap();
	private final NSArray allTypeCreditsRec;
	private final NSArray allPlancoValidesForBE;
	private final NSArray allPlancomptableAmos;


	private NSArray initialPcoCredits;

	/**
	 * Permet de spécifier les nombre de caractères max pour chaque classe de compte (à vérifier lors de la saisie)
	 */
	public static final HashMap PCONUM_MAX_CHARS_PER_CLASS = new HashMap(10);
	{

		//on passe a 10 caracteres pour les classes budgetaires (au lieu de 10)
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE0, new Integer(20));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE1, new Integer(10));

		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE2, new Integer(10));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE3, new Integer(20));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE4, new Integer(20));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE5, new Integer(20));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE6, new Integer(10));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE7, new Integer(10));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE8, new Integer(20));
		PCONUM_MAX_CHARS_PER_CLASS.put(EOPlanComptableExer.CLASSE9, new Integer(20));
	};

	/**
	 * @param editingContext
	 */
	public PcoSaisieCtrl(EOEditingContext editingContext, Window win, final NSArray _allPlancoValidesForBE) {
		super(editingContext);
		revertChanges();
		// parentWindow = win;
		allPlancoValidesForBE = _allPlancoValidesForBE;
		pcoDico = new HashMap();
		myPanel = new PcoSaisiePanel(new PcoSaisiePanelListener());

		// Charger les types de crédit
		allTypeCreditsDep = EOsFinder.getTypesCreditsDepenseExecutoire(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
		ZLogger.debug(allTypeCreditsDep);
		for (int i = 0; i < allTypeCreditsDep.count(); i++) {
			dicoOfTypeCreditsDep.put(allTypeCreditsDep.objectAtIndex(i), Boolean.FALSE);
		}
		allTypeCreditsRec = EOsFinder.getTypesCreditsRecette(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
		ZLogger.debug(allTypeCreditsRec);
		for (int i = 0; i < allTypeCreditsRec.count(); i++) {
			dicoOfTypeCreditsRec.put(allTypeCreditsRec.objectAtIndex(i), Boolean.FALSE);
		}

		allPlancomptableAmos = EOsFinder.fetchAllPlancomptableAmo(getEditingContext());

	}

	private final void annulerSaisie() {
		revertChanges();
		getMyDialog().onCancelClick();
	}

	private final void validerSaisie() {
		try {
			if (mode == MODE_NEW) {
				validerSaisieForNew();
			}
			else if (mode == MODE_MODIF) {
				validerSaisieForModify();
			}
			else {
				throw new DefaultClientException("Erreur anormale. Mode de saisie non défini.");
			}
			EOPlanComptable.refreshPcoLibelleMap(getEditingContext(), getExercice());
			getMyDialog().onOkClick();

		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		}
	}

	/**
	 * @throws Exception
	 */
	private void validerSaisieForNew() throws Exception {
		checkSaisieDico();
		final FactoryProcessPlanComptableEtVisa myFactoryProcessPlanComptableEtVisa = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
		final EOPlanComptableExer pco = myFactoryProcessPlanComptableEtVisa.creerPlanComptableExer(getEditingContext(), getExercice(), (String) pcoDico.get(EOPlanComptableExer.PCO_BUDGETAIRE_KEY), (String) pcoDico.get(EOPlanComptableExer.PCO_EMARGEMENT_KEY), (String) pcoDico
				.get(EOPlanComptableExer.PCO_LIBELLE_KEY), (String) pcoDico.get(EOPlanComptableExer.PCO_NATURE_KEY), (String) pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY));
		pco.setPcoJBe((String) pcoDico.get(EOPlanComptableExer.PCO_J_BE_KEY));
		pco.setPcoJExercice((String) pcoDico.get(EOPlanComptableExer.PCO_J_EXERCICE_KEY));
		pco.setPcoJFinExercice((String) pcoDico.get(EOPlanComptableExer.PCO_J_FIN_EXERCICE_KEY));
		pco.setPcoCompteBe((String) pcoDico.get(EOPlanComptableExer.PCO_COMPTE_BE_KEY));
		pco.setPcoSensSolde((String) pcoDico.get(EOPlanComptableExer.PCO_SENS_SOLDE_KEY));

		// gérer la sauvegarde des plancocredits

		// NSArray plancoCredits = getPlancoCreditsForPlanco(pco);
		// NSArray typeCreditsForPco = getTypesCreditsForPlanco(pco);

		final FactoryProcessPlanComptableTypeCredit factoryProcessPlanComptableTypeCredit = new FactoryProcessPlanComptableTypeCredit(myApp.wantShowTrace());
		final Iterator iter = dicoOfTypeCreditsDep.keySet().iterator();

		//  ZLogger.debug("inserted", getEditingContext().insertedObjects());

		//on balaye tous les types credits et on verifie s'ils sont coches
		while (iter.hasNext()) {
			EOTypeCredit typeCredit = (EOTypeCredit) iter.next();
			ZLogger.debug("analyse typecredit " + typeCredit.tcdCode());
			// Vérifier si l'association existe deja (n'existe jamais dans le cas d'un nouveau compte)
			//            EOPlancoCredit pcoCredit = getPlancoCreditForTypeCreditAndPlanco(typeCredit, pco);
			EOPlancoCredit pcoCredit = null;
			//si type credit coche
			if (Boolean.TRUE.equals(dicoOfTypeCreditsDep.get(typeCredit))) {
				//// si objet inexistant, on le crée
				if (pcoCredit == null) {
					ZLogger.debug("nveau pcocredit");
					pcoCredit = factoryProcessPlanComptableTypeCredit.associerCreditCompte(getEditingContext(), typeCredit, pco.planComptable(), typeCredit.getTypeShort());
				}
				//				else {
				//					//// si objet existant, on le valide
				//					ZLogger.debug("pcocredit existe");
				//					pcoCredit.setPccEtat(EOPlancoCredit.etatValide);
				//					pcoCredit.setPlaQuoi(typeCredit.getTypeShort());
				//				}
			}
			// Si decoche et qu'il existe, on le devalide
			if (pcoCredit != null && Boolean.FALSE.equals(dicoOfTypeCreditsDep.get(typeCredit))) {
				factoryProcessPlanComptableTypeCredit.annulerCreditCompte(getEditingContext(), pcoCredit);
			}
		}
		final Iterator iter2 = dicoOfTypeCreditsRec.keySet().iterator();
		while (iter2.hasNext()) {
			EOTypeCredit typeCredit = (EOTypeCredit) iter2.next();
			ZLogger.debug("analyse typecredit " + typeCredit.tcdCode());
			// Vérifier si l'association existe deja (n'existe jamais dans le cas d'un nouveau compte)
			//          EOPlancoCredit pcoCredit = getPlancoCreditForTypeCreditAndPlanco(typeCredit, pco);
			EOPlancoCredit pcoCredit = null;
			// si objet inexistant et qu'il est coché, on le crée
			if (Boolean.TRUE.equals(dicoOfTypeCreditsRec.get(typeCredit))) {
				if (pcoCredit == null) {
					ZLogger.debug("nveau pcocredit");
					pcoCredit = factoryProcessPlanComptableTypeCredit.associerCreditCompte(getEditingContext(), typeCredit, pco.planComptable(), typeCredit.getTypeShort());
				}
				//				else {
				//					ZLogger.debug("pcocredit existe");
				//					pcoCredit.setPccEtat(EOPlancoCredit.etatValide);
				//					pcoCredit.setPlaQuoi(typeCredit.getTypeShort());
				//				}
			}

			// Si annule
			if (pcoCredit != null && Boolean.FALSE.equals(dicoOfTypeCreditsRec.get(typeCredit))) {
				factoryProcessPlanComptableTypeCredit.annulerCreditCompte(getEditingContext(), pcoCredit);
			}
		}

		// gestion de plancorelance
		// EOPlancoRelance plancoRelance =
		// EOsFinder.getPlancoRelanceForPlanco(getEditingContext(), currentPco);
		//obnsolete
		//        final FactoryPlancoRelance factoryPlancoRelance = new FactoryPlancoRelance(myApp.wantShowTrace());
		//        EOPlancoRelance plancoRelance = factoryPlancoRelance.creerEOPlancoRelance(getEditingContext(), new NSTimestamp(ZDateUtil.getToday().getTime()), pco.planComptable(), getUtilisateur());
		//        plancoRelance.setPlrEtat((String) plancoRelanceComboBoxModel.getSelectedKey());

		// gestion des plancoamortissements
		// recupérer le plancomptableAmortissement selectionné
		final FactoryProcessPlanComptableEtVisa factoryProcessPlanComptableEtVisa = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
		// supprimer la relation systematiquement
		String pcoANum = (String) pcoDico.get(EOPlanComptableAmo.PCOA_NUM_KEY);
		String pcoALibelle = (String) pcoDico.get(EOPlanComptableAmo.PCOA_LIBELLE_KEY);
		final Integer pcaDuree = (Integer) pcoDico.get(EOPlancoAmortissement.PCA_DUREE_KEY);
		if (pcoANum != null) {
			pcoANum = pcoANum.trim().toUpperCase();
			if (ZStringUtil.isEmpty(pcoALibelle)) {
				pcoALibelle = (String) pcoDico.get(EOPlanComptableExer.PCO_LIBELLE_KEY);
			}
			pcoALibelle = pcoALibelle.trim();
			//vérifier si le pcoa_num existe 
			EOPlanComptableAmo planComptableAmo = EOsFinder.getPlanComptableAmoForPcoANum(getEditingContext(), pcoANum);
			if (planComptableAmo == null) {
				//si l'objet n''existe pas on le crée
				planComptableAmo = factoryProcessPlanComptableEtVisa.creerPlanComptableAmo(getEditingContext(), pcoANum, pcoALibelle);
			}
			else {
				if (planComptableAmo.pcoaLibelle() != null && !planComptableAmo.pcoaLibelle().equals(pcoALibelle)) {
					if (!showConfirmationDialog("Confirmation", "<html>Voulez-vous remplacer le libellé du compte " + pcoANum + "<br>\"" + planComptableAmo.pcoaLibelle() + "\"<br> par \"<b>" + pcoALibelle + "\"</b> ?</html>", ZMsgPanel.BTLABEL_YES)) {
						planComptableAmo.setPcoaLibelle(pcoALibelle);
					}
				}
			}
			factoryProcessPlanComptableEtVisa.creerPlancoAmortissement(getEditingContext(), planComptableAmo, pco.planComptable(), myApp.appUserInfo().getCurrentExercice(), pcaDuree);
		}

		ZLogger.verbose(getEditingContext());

		// la sauvegarde
		//		System.out.println(getEditingContext().insertedObjects().count());
		//		for (int i = 0; i < getEditingContext().insertedObjects().count(); i++) {
		//			EOEnterpriseObject array_element = (EOEnterpriseObject) getEditingContext().insertedObjects().objectAtIndex(i);
		//			System.out.println("->new");
		//			System.out.println(array_element.getClass().getName());
		//			System.out.println(array_element);
		//			System.out.println("");
		//
		//		}

		getEditingContext().saveChanges();
		currentPco = pco;
	}

	private void validerSaisieForModify() throws Exception {
		ZLogger.debug("");
		checkSaisieDico();

		if (currentPco == null) {
			throw new DefaultClientException("Impossible d'enregistrer les modifications. L'objet à modifier est nul.");
		}

		final FactoryProcessPlanComptableTypeCredit factoryProcessPlanComptableTypeCredit = new FactoryProcessPlanComptableTypeCredit(myApp.wantShowTrace());
		Iterator iter = dicoOfTypeCreditsDep.keySet().iterator();
		while (iter.hasNext()) {
			final EOTypeCredit typeCredit = (EOTypeCredit) iter.next();
			ZLogger.debug("analyse typecredit " + typeCredit.tcdCode());
			// Vérifier si l'association existe deja
			EOPlancoCredit pcoCredit = getPlancoCreditForTypeCreditAndPlanco(typeCredit, currentPco);
			// si objet inexistant et qu'il est coché, on le crée
			if (Boolean.TRUE.equals(dicoOfTypeCreditsDep.get(typeCredit))) {
				if (pcoCredit == null) {
					ZLogger.debug("nveau pcocredit");
					pcoCredit = factoryProcessPlanComptableTypeCredit.associerCreditCompte(getEditingContext(), typeCredit, currentPco.planComptable(), typeCredit.getTypeShort());
				}
				else {
					ZLogger.debug("pcocredit existe");
					pcoCredit.setPccEtat(EOPlancoCredit.etatValide);
					pcoCredit.setPlaQuoi(typeCredit.getTypeShort());
				}
			}

			// Si annule
			if (pcoCredit != null && Boolean.FALSE.equals(dicoOfTypeCreditsDep.get(typeCredit))) {
				factoryProcessPlanComptableTypeCredit.annulerCreditCompte(getEditingContext(), pcoCredit);
			}
		}

		Iterator iter2 = dicoOfTypeCreditsRec.keySet().iterator();
		while (iter2.hasNext()) {
			final EOTypeCredit typeCredit = (EOTypeCredit) iter2.next();
			ZLogger.debug("analyse typecredit " + typeCredit.tcdCode());
			// Vérifier si l'association existe deja
			EOPlancoCredit pcoCredit = getPlancoCreditForTypeCreditAndPlanco(typeCredit, currentPco);
			// si objet inexistant et qu'il est coché, on le crée
			if (Boolean.TRUE.equals(dicoOfTypeCreditsRec.get(typeCredit))) {
				if (pcoCredit == null) {
					ZLogger.debug("nveau pcocredit");
					pcoCredit = factoryProcessPlanComptableTypeCredit.associerCreditCompte(getEditingContext(), typeCredit, currentPco.planComptable(), typeCredit.getTypeShort());
				}
				else {
					ZLogger.debug("pcocredit existe");
					pcoCredit.setPccEtat(EOPlancoCredit.etatValide);
					pcoCredit.setPlaQuoi(typeCredit.getTypeShort());
				}
			}

			// Si annule
			if (pcoCredit != null && Boolean.FALSE.equals(dicoOfTypeCreditsRec.get(typeCredit))) {
				factoryProcessPlanComptableTypeCredit.annulerCreditCompte(getEditingContext(), pcoCredit);
			}
		}

		currentPco.setPcoJBe((String) pcoDico.get("pcoJBe"));
		currentPco.setPcoJExercice((String) pcoDico.get("pcoJExercice"));
		currentPco.setPcoJFinExercice((String) pcoDico.get("pcoJFinExercice"));
		currentPco.setPcoBudgetaire((String) pcoDico.get("pcoBudgetaire"));
		currentPco.setPcoEmargement((String) pcoDico.get("pcoEmargement"));
		currentPco.setPcoLibelle((String) pcoDico.get(EOPlanComptableExer.PCO_LIBELLE_KEY));
		currentPco.setPcoNature((String) pcoDico.get("pcoNature"));
		currentPco.setPcoCompteBe((String) pcoDico.get("pcoCompteBe"));
		currentPco.setPcoSensSolde((String) pcoDico.get("pcoSensSolde"));

		ZLogger.debug("currentPco", currentPco);
		ZLogger.debug(currentPco.editingContext());
		ZLogger.debug(pcoDico);

		// gérer la sauvegarde des plancocredits
		ZLogger.debug(dicoOfTypeCreditsDep);
		// ZLogger.debug(dicoOfPlaQuoi);

		//
		// NSArray plancoCredits = getPlancoCreditsForPlanco(currentPco);
		// NSArray typeCreditsForPco = getTypesCreditsForPlanco(currentPco);

		// gestion des plancoamortissements
		// recupérer le plancomptableAmortissement selectionné
		final FactoryProcessPlanComptableEtVisa factoryProcessPlanComptableEtVisa = new FactoryProcessPlanComptableEtVisa(myApp.wantShowTrace());
		// supprimer la relation systematiquement
		factoryProcessPlanComptableEtVisa.supprimerPlancoAmortissements(getEditingContext(), currentPco.planComptable(), myApp.appUserInfo().getCurrentExercice());
		String pcoANum = (String) pcoDico.get(EOPlanComptableAmo.PCOA_NUM_KEY);
		String pcoALibelle = (String) pcoDico.get(EOPlanComptableAmo.PCOA_LIBELLE_KEY);
		final Integer pcaDuree = (Integer) pcoDico.get(EOPlancoAmortissement.PCA_DUREE_KEY);
		if (pcoANum != null) {
			pcoANum = pcoANum.trim().toUpperCase();
			if (ZStringUtil.isEmpty(pcoALibelle)) {
				pcoALibelle = (String) pcoDico.get(EOPlanComptableExer.PCO_LIBELLE_KEY);
			}
			pcoALibelle = pcoALibelle.trim();
			//vérifier si le pcoa_num existe 
			EOPlanComptableAmo planComptableAmo = EOsFinder.getPlanComptableAmoForPcoANum(getEditingContext(), pcoANum);
			if (planComptableAmo == null) {
				//si l'objet n''existe pas on le crée
				planComptableAmo = factoryProcessPlanComptableEtVisa.creerPlanComptableAmo(getEditingContext(), pcoANum, pcoALibelle);
			}
			else {
				if (planComptableAmo.pcoaLibelle() != null && !planComptableAmo.pcoaLibelle().equals(pcoALibelle)) {
					if (showConfirmationDialog("Confirmation", "<html>Voulez-vous remplacer le libellé du compte " + pcoANum + "<br>\"" + planComptableAmo.pcoaLibelle() + "\"<br> par \"<b>" + pcoALibelle + "</b>\" ?</html>", ZMsgPanel.BTLABEL_YES)) {
						planComptableAmo.setPcoaLibelle(pcoALibelle);
						//	planComptableAmo.willChange();
					}
				}
			}
			//creer systematiquement
			factoryProcessPlanComptableEtVisa.creerPlancoAmortissement(getEditingContext(), planComptableAmo, currentPco.planComptable(), myApp.appUserInfo().getCurrentExercice(), pcaDuree);

		}

		ZLogger.verbose(getEditingContext().updatedObjects());

		// la sauvegarde
		getEditingContext().saveChanges();

	}

	/**
	 * Vérifie la validité de la saisie d'un compte. <br>
	 * <ul>
	 * <li>Libellé obligatoire</li>
	 * <li>N° de compte obligatoire</li>
	 * <li>Héritage depuis le père (sur n° de compte)</li>
	 * <li>Nombre de caractères max du compte suivant la classe</li>
	 * </ul>
	 * 
	 * @throws Exception
	 */
	private final void checkSaisieDico() throws Exception {
		ZLogger.debug(pcoDico);

		if (pcoDico.get(EOPlanComptableExer.PCO_LIBELLE_KEY) == null) {
			throw new DataCheckException("Le libellé est obligatoire.");
		}
		if (pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY) == null) {
			throw new DataCheckException("Le n° de compte est obligatoire.");
		}
		pcoDico.put(EOPlanComptableExer.PCO_NUM_KEY, ((String) pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY)).trim());

		//Vérifier que les caracteres composant le comptes sont autorises
		String pconum = (String) pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY);
		char[] pconumchar = pconum.toCharArray();
		for (int i = 0; i < pconumchar.length; i++) {
			char array_element = pconumchar[i];
			if (CARACTERES_AUTORISES.indexOf(new String(new char[] {
					array_element
			}).toLowerCase()) < 0) {
				throw new DataCheckException("Le caractère " + array_element + " n'est pas autorisé dans les numéros de comptes. Veuillez utiliser uniquement des caractères alphanumériques.");
			}
		}

		// Vérifier que le compte est different de son pere
		if (pcoDico.get("pcoNumParent") != null && pcoDico.get("pcoNumParent").equals(pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY))) {
			throw new DataCheckException("Vous ne pouvez pas créer un nouveau compte avec un n° de compte identique à son compte parent (" + pcoDico.get("pcoNumParent") + ")");
		}

		// verifier que le compte descend bien de son pere
		if (pcoDico.get("pcoNumParent") != null && !pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY).toString().startsWith((String) pcoDico.get("pcoNumParent"))) {
			throw new DataCheckException("Le n° de compte doit obligatoirement débuter par le n° de son père.");
		}

		// Suivant la classe du compte, on vérifie le nombre maximum de
		// caractères.
		String classeCompte = pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY).toString().substring(0, 1);
		if (PCONUM_MAX_CHARS_PER_CLASS.get(classeCompte) != null) {
			if (pcoDico.get(EOPlanComptableExer.PCO_NUM_KEY).toString().length() > ((Integer) PCONUM_MAX_CHARS_PER_CLASS.get(classeCompte)).intValue()) {
				throw new DataCheckException("Les n° de compte pour la classe " + classeCompte + " ne doivent pas dépasser " + PCONUM_MAX_CHARS_PER_CLASS.get(classeCompte) + " caractères.");
			}
		}

		//Verifier duree 
		if (pcoDico.get(EOPlanComptableAmo.PCOA_NUM_KEY) != null) {
			if (pcoDico.get(EOPlancoAmortissement.PCA_DUREE_KEY) == null) {
				throw new DataCheckException("Vous devez spécifier une durée d'amortissement si vous associez un compte d'amortissement.");
			}
		}

		//Verifier si pco_budgetaire est a N et que le compte est utilise dans le budget
		if (getCurrentPco() != null && "N".equals(pcoDico.get(EOPlanComptable.PCO_BUDGETAIRE_KEY))) {
			FactoryProcessPlanComptableEtVisa.checkPlancoUtiliseDansBudget(getEditingContext(), getCurrentPco(), getExercice());
		}

		// Vérifier que le compte BE existe
		String pcoBe = (String) pcoDico.get("pcoCompteBe");
		if (pcoBe == null || pcoBe.trim().length() == 0) {
			pcoDico.put("pcoCompteBe", null);
		}
		else {
			pcoDico.put("pcoCompteBe", ((String) pcoDico.get("pcoCompteBe")).trim());
			Integer exeOrdreNext = Integer.valueOf(getExercice().exeExercice().intValue() + 1);
			EOExercice exerciceNext = EOExercice.fetchByKeyValue(getEditingContext(), EOExercice.EXE_EXERCICE_KEY, exeOrdreNext);
			if (exerciceNext != null) {
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum=%@", new NSArray(pcoBe));
				if ((EOQualifier.filteredArrayWithQualifier(allPlancoValidesForBE, qual)).count() == 0) {
					throw new DataCheckException("Le n° de compte spécifié pour la BE " + pcoBe.trim() + " ne correspond pas à un compte valide sur l'exercice " + exeOrdreNext.intValue());
				}
			}
			else {
				showInfoDialog("Impossible de vérifier la validité du compte de BE spécifié (" + pcoBe.trim() + ") car l'exercice " + exeOrdreNext.intValue() + " n'est pas créé. Ceci n'est pas bloquant pour l'enregistrement du compte.");
			}
		}
	}

	public final void initDicoWithDefault(EOPlanComptableExer pcoParent) {
		if (pcoParent != null) {
			pcoDico.put("pcoNumParent", pcoParent.pcoNum());
			pcoDico.put("pcoLibelleParent", pcoParent.pcoLibelle());
			pcoDico.put(EOPlanComptableExer.PCO_NUM_KEY, pcoParent.pcoNum());
			pcoDico.put("pcoBudgetaire", pcoParent.pcoBudgetaire());
			pcoDico.put("pcoEmargement", pcoParent.pcoEmargement());
			pcoDico.put("pcoNature", pcoParent.pcoNature());
			pcoDico.put("pcoJBe", pcoParent.pcoJBe());
			pcoDico.put("pcoJExercice", pcoParent.pcoJExercice());
			pcoDico.put("pcoJFinExercice", pcoParent.pcoJFinExercice());
			pcoDico.put("pcoSensSolde", pcoParent.pcoSensSolde());
			pcoDico.put(EOPlanComptableAmo.PCOA_NUM_KEY, null);
			pcoDico.put(EOPlanComptableAmo.PCOA_LIBELLE_KEY, null);
			pcoDico.put(EOPlancoAmortissement.PCA_DUREE_KEY, null);
			preselectPlancoCredits(pcoParent);
		}
		else {
			pcoDico.put("pcoNumParent", null);
			pcoDico.put("pcoLibelleParent", null);
			pcoDico.put(EOPlanComptableExer.PCO_NUM_KEY, null);
			pcoDico.put("pcoBudgetaire", null);
			pcoDico.put("pcoEmargement", null);
			pcoDico.put("pcoNature", null);
			pcoDico.put("pcoJBe", null);
			pcoDico.put("pcoJExercice", null);
			pcoDico.put("pcoJFinExercice", null);
			pcoDico.put("pcoSensSolde", null);
			pcoDico.put(EOPlanComptableAmo.PCOA_NUM_KEY, null);
			pcoDico.put(EOPlanComptableAmo.PCOA_LIBELLE_KEY, null);
			pcoDico.put(EOPlancoAmortissement.PCA_DUREE_KEY, null);
		}
	}

	public final void initDicoWithObject(EOPlanComptableExer pcoParent) {

		if (pcoParent != null) {
			pcoDico.put("pcoNumParent", pcoParent.pcoNum());
			pcoDico.put("pcoLibelleParent", pcoParent.pcoLibelle());
		}
		else {
			pcoDico.put("pcoNumParent", null);
			pcoDico.put("pcoLibelleParent", null);
		}
		pcoDico.put(EOPlanComptableExer.PCO_NUM_KEY, currentPco.pcoNum());
		pcoDico.put(EOPlanComptableExer.PCO_LIBELLE_KEY, currentPco.pcoLibelle());
		pcoDico.put("pcoBudgetaire", currentPco.pcoBudgetaire());
		pcoDico.put("pcoEmargement", currentPco.pcoEmargement());
		pcoDico.put("pcoNature", currentPco.pcoNature());
		pcoDico.put("pcoJBe", currentPco.pcoJBe());
		pcoDico.put("pcoJExercice", currentPco.pcoJExercice());
		pcoDico.put("pcoJFinExercice", currentPco.pcoJFinExercice());
		pcoDico.put("pcoCompteBe", currentPco.pcoCompteBe());
		pcoDico.put("pcoSensSolde", currentPco.pcoSensSolde());

		final EOPlancoAmortissement plancoa = EOsFinder.getPlancoAmortissementForPlanco(getEditingContext(), currentPco.planComptable(), myApp.appUserInfo().getCurrentExercice());
		final EOPlanComptableAmo planComptableAmo = EOsFinder.getPlancomptableAmoForPlanco(getEditingContext(), currentPco.planComptable(), myApp.appUserInfo().getCurrentExercice());
		pcoDico.put(EOPlanComptableAmo.PCOA_NUM_KEY, (planComptableAmo != null ? planComptableAmo.pcoaNum() : null));
		pcoDico.put(EOPlanComptableAmo.PCOA_LIBELLE_KEY, (planComptableAmo != null ? planComptableAmo.pcoaLibelle() : null));
		pcoDico.put(EOPlancoAmortissement.PCA_DUREE_KEY, (plancoa != null ? plancoa.pcaDuree() : null));


		preselectPlancoCredits(currentPco);
	}

	private final void preselectPlancoCredits(final EOPlanComptableExer pco) {
		final NSArray res = getPlancoCreditsForPlanco(pco);
		initialPcoCredits = res;
		ZLogger.debug(res);
		ZLogger.debug("nb plancocredits recuperes " + res.count());
		for (int i = 0; i < res.count(); i++) {
			final EOPlancoCredit element = ((EOPlancoCredit) res.objectAtIndex(i));
			if (EOTypeCredit.TCD_TYPE_DEPENSE.equals(element.typeCredit().tcdType())) {
				dicoOfTypeCreditsDep.put(element.typeCredit(), Boolean.valueOf(EOPlancoCredit.etatValide.equals(element.pccEtat())));
			}
			else {
				dicoOfTypeCreditsRec.put(element.typeCredit(), Boolean.valueOf(EOPlancoCredit.etatValide.equals(element.pccEtat())));
			}
		}
	}

	private final NSArray getPlancoCreditsForPlanco(EOPlanComptableExer pco) {
		return EOsFinder.fetchArray(getEditingContext(), EOPlancoCredit.ENTITY_NAME, EOPlancoCredit.PLAN_COMPTABLE_KEY + ZFinder.QUAL_EQUALS + " and " + EOPlancoCredit.TYPE_CREDIT_KEY + "." + EOTypeCredit.EXERCICE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				pco, getExercice()
		}), null,
				false);
	}

	private final EOPlancoCredit getPlancoCreditForTypeCreditAndPlanco(EOTypeCredit tc, EOPlanComptableExer pco) {
		return (EOPlancoCredit) EOsFinder.fetchObject(getEditingContext(), EOPlancoCredit.ENTITY_NAME, EOPlancoCredit.PLAN_COMPTABLE_KEY + ZFinder.QUAL_EQUALS + ZFinder.QUAL_AND + EOPlancoCredit.TYPE_CREDIT_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				pco, tc
		}), null, false);
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	//    private final ZKarukeraDialog createModalDialog(Frame dial) {
	//        win = new ZKarukeraDialog(dial, "Saisie d'un compte", true);
	//        myPanel.setMyDialog(win);
	//        myPanel.setPreferredSize(WINDOW_SIZE);
	//        myPanel.initGUI();
	//        win.setContentPane(myPanel);
	//
	//        win.pack();
	//        return win;
	//    }

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final EOPlanComptableExer openDialogNew(Dialog dial, final EOPlanComptableExer pcoParent) {
		currentPco = null;
		try {
			if (pcoParent == null) {
				throw new DefaultClientException("Vous ne pouvez pas créer de compte sans compte parent.");
			}
		} catch (Exception e) {
			showErrorDialog(e);
			return currentPco;
		}

		ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		try {
			mode = MODE_NEW;
			newSaisie(pcoParent);
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
		return currentPco;
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final EOPlanComptableExer openDialogModify(Dialog dial, final EOPlanComptableExer pcoParent, EOPlanComptableExer pco) {
		// currentPco=pco;
		try {
			if (pco == null) {
				throw new DefaultClientException("Le compte à modifier n'est pas spécifié.");
			}
		}

		catch (Exception e) {
			showErrorDialog(e);
			return currentPco;
		}

		ZKarukeraDialog win1 = createModalDialog(dial);
		this.setMyDialog(win1);
		try {
			mode = MODE_MODIF;
			modifySaisie(pcoParent, pco);
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
		return currentPco;
	}

	/**
	 * @param pcoParent
	 * @param pco
	 */
	private void modifySaisie(EOPlanComptableExer pcoParent, EOPlanComptableExer pco) {
		try {
			currentPco = pco;
			initDicoWithObject(pcoParent);
			myPanel.updateData();
			myPanel.lockForModify();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public final void newSaisie(EOPlanComptableExer pcoParent) {
		try {
			currentPco = null;
			initDicoWithDefault(pcoParent);
			myPanel.updateData();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public final class ActionAnnuler extends AbstractAction {

		public ActionAnnuler() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	public final class ActionSrchPlancomptableAmo extends AbstractAction {

		public ActionSrchPlancomptableAmo() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
		}

		public void actionPerformed(ActionEvent e) {
			onSrchPlancomptableAmo();
		}

	}

	private final class PcoSaisiePanelListener implements PcoSaisiePanel.IPcoSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisiePanel.IPcoSaisiePanelListener#dicoValues()
		 */
		public HashMap dicoValues() {
			return pcoDico;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisiePanel.IPcoSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisiePanel.IPcoSaisiePanelListener#actionAnnuler()
		 */
		public Action actionAnnuler() {
			return actionAnnuler;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PcoSaisiePanel.IPcoSaisiePanelListener#getPcoSaisiePanelDepListener()
		 */
		public IPlancoCreditPanelListener getPcoSaisiePanelDepListener() {
			return plancoCreditDepPanelListener;
		}

		public EOExercice getExeercice() {
			return getExercice();
		}

		public IPlancoCreditPanelListener getPcoSaisiePanelRecListener() {
			return plancoCreditRecPanelListener;
		}

		public Action actionSrchPlancomptableAmo() {
			return actionSrchPlancomptableAmo;
		}

		public void onPconumAmoUpdated() {
			updatePcoLibelleAmo();

		}

	}

	private final class PlancoCreditDepPanelListener implements PlancoCreditPanel.IPlancoCreditPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#changeEditMode(boolean)
		 */
		public void changeEditMode(boolean editMode) {

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#isEditingState()
		 */
		public boolean isEditingState() {
			return false;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#onCheck()
		 */
		public void onCheck() {

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#dicoOfTypeCredits()
		 */
		public HashMap dicoOfTypeCredits() {
			return dicoOfTypeCreditsDep;
		}

		// /**
		// * @see
		// org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#dicoOfPlaQuoi()
		// */
		// public HashMap dicoOfPlaQuoi() {
		// return dicoOfPlaQuoi;
		// }

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#getAllTypeCredits()
		 */
		public NSArray getAllTypeCredits() {
			return allTypeCreditsDep;
		}

	}

	private final class PlancoCreditRecPanelListener implements PlancoCreditPanel.IPlancoCreditPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#changeEditMode(boolean)
		 */
		public void changeEditMode(boolean editMode) {

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#isEditingState()
		 */
		public boolean isEditingState() {
			return false;
		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#onCheck()
		 */
		public void onCheck() {

		}

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#dicoOfTypeCredits()
		 */
		public HashMap dicoOfTypeCredits() {
			return dicoOfTypeCreditsRec;
		}

		// /**
		// * @see
		// org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#dicoOfPlaQuoi()
		// */
		// public HashMap dicoOfPlaQuoi() {
		// return dicoOfPlaQuoi;
		// }

		/**
		 * @see org.cocktail.maracuja.client.pco.ui.PlancoCreditPanel.IPlancoCreditPanelListener#getAllTypeCredits()
		 */
		public NSArray getAllTypeCredits() {
			return allTypeCreditsRec;
		}

	}

	public EOPlanComptableExer getCurrentPco() {
		return currentPco;
	}


	private void onSrchPlancomptableAmo() {
		//On ne fait rien (tant que l'action n'est pas implementee)
	}

	private void updatePcoLibelleAmo() {
		String s = (String) pcoDico.get(EOPlanComptableAmo.PCOA_NUM_KEY);
		System.out.println("->" + s);

		if (s == null) {
			pcoDico.put(EOPlanComptableAmo.PCOA_LIBELLE_KEY, null);
		}
		else {
			s = s.toUpperCase();
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableAmo.PCOA_NUM_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
					s
			}));
			final NSArray found = EOQualifier.filteredArrayWithQualifier(allPlancomptableAmos, qual);
			if (found.count() > 0) {
				pcoDico.put(EOPlanComptableAmo.PCOA_LIBELLE_KEY, ((EOPlanComptableAmo) found.objectAtIndex(0)).pcoaLibelle());
			}
			else {
				pcoDico.put(EOPlanComptableAmo.PCOA_LIBELLE_KEY, null);
			}
		}

		myPanel.updateDataAmortissementLibelle();
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}
}
