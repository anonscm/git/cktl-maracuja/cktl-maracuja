package org.cocktail.maracuja.client;

import org.cocktail.fwkcktlcompta.client.metier.EOComptabilite;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminExercice;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyAdminUtilisateur;
import org.cocktail.fwkcktlcompta.client.metier.EORecouvrement;
import org.cocktail.fwkcktlcompta.client.util.ClientCktlEOControlUtilities;

import com.webobjects.eocontrol.EOEditingContext;

public class EOConvertUtil {


	public static EOComptabilite fetchFrom(org.cocktail.maracuja.client.metier.EOComptabilite comptabilite) {
		EOEditingContext edc = comptabilite.editingContext();
		Object pkValue = ClientCktlEOControlUtilities.primaryKeyObjectForObject(comptabilite);
		EOComptabilite res = EOComptabilite.fetchByKeyValue(edc, EOComptabilite.ENTITY_PRIMARY_KEY, pkValue);
		return res;
	}

	public static EOJefyAdminExercice fetchFrom(org.cocktail.maracuja.client.metier.EOExercice object) {
		EOEditingContext edc = object.editingContext();
		Object pkValue = ClientCktlEOControlUtilities.primaryKeyObjectForObject(object);
		EOJefyAdminExercice res = EOJefyAdminExercice.fetchByKeyValue(edc, EOJefyAdminExercice.ENTITY_PRIMARY_KEY, pkValue);
		return res;
	}

	public static EOJefyAdminUtilisateur fetchFrom(org.cocktail.maracuja.client.metier.EOUtilisateur object) {
		EOEditingContext edc = object.editingContext();
		Object pkValue = ClientCktlEOControlUtilities.primaryKeyObjectForObject(object);
		EOJefyAdminUtilisateur res = EOJefyAdminUtilisateur.fetchByKeyValue(edc, EOJefyAdminUtilisateur.ENTITY_PRIMARY_KEY, pkValue);
		return res;
	}

	public static org.cocktail.maracuja.client.metier.EORecouvrement fetchFrom(EORecouvrement object) {
		EOEditingContext edc = object.editingContext();
		Object pkValue = ClientCktlEOControlUtilities.primaryKeyObjectForObject(object);
		org.cocktail.maracuja.client.metier.EORecouvrement res = org.cocktail.maracuja.client.metier.EORecouvrement.fetchByKeyValue(edc, EORecouvrement.ENTITY_PRIMARY_KEY, pkValue);
		return res;
	}

	public static EORecouvrement fetchFrom(org.cocktail.maracuja.client.metier.EORecouvrement object) {
		EOEditingContext edc = object.editingContext();
		Object pkValue = ClientCktlEOControlUtilities.primaryKeyObjectForObject(object);
		EORecouvrement res = EORecouvrement.fetchByKeyValue(edc, org.cocktail.maracuja.client.metier.EORecouvrement.ENTITY_PRIMARY_KEY, pkValue);
		return res;
	}

}
