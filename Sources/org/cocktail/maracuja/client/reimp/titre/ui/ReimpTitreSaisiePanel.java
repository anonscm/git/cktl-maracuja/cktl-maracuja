/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.reimp.titre.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextField;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ReimpTitreSaisiePanel extends ZKarukeraPanel {
    private ZTextField 		myLibelle;
    
    private ZActionField pcoNumNouveauField;
    private ZTextField pcoLibelleNouveauField;
    private ZTextField titNumero;
    private ZTextField titDateRemise;
    private ZTextField titFournisseur;
    private ZTextField titMontantTTC;
    
    
    
    private IReimpSaisiePanelListener myListener;
    private static final int DEFAULT_LABEL_WIDTH = 65;

    private ZTextField pcoNumAncien;

    private ZTextField pcoLibelleAncien;
    
    
    /**
     * 
     */
    public ReimpTitreSaisiePanel(IReimpSaisiePanelListener listener) {
        super();
        myListener = listener;
    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        add(buildPanelInfos(), BorderLayout.NORTH);
        add(buildPanelForm(), BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);
        
    }

    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionAnnuler());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }
    
    
    
    
    private final JPanel buildPanelForm() {
        myLibelle = new ZTextField(new LibelleModel());
        myLibelle.getMyTexfield().setColumns(40);
        
        pcoNumNouveauField = new ZActionField(new PcoNumNewModel(),myListener.actionPlancomptableNouveauSelect() );
        pcoNumNouveauField.getMyTexfield().setColumns(10);
        
        pcoLibelleNouveauField = new ZTextField(new PcoLibelleNewModel());
        pcoLibelleNouveauField.getMyTexfield().setColumns(30);
        pcoLibelleNouveauField .getMyTexfield().setEditable(false);

      
        
        Box col1 = Box.createVerticalBox();
        col1.add(buildLine((new ZLabeledComponent("Libellé", myLibelle, ZLabeledComponent.LABELONLEFT, 120))));
        col1.add(buildLine(new ZLabeledComponent("Nouvelle imputation", buildLine( new Component[]{pcoNumNouveauField, pcoLibelleNouveauField} ), ZLabeledComponent.LABELONLEFT, 120)));
        col1.add(Box.createVerticalGlue());        
        
        return encloseInPanelWithTitle("Réimputation", null,ZConst.BG_COLOR_TITLE,col1,null, null);
    }
    
    private final JPanel buildPanelInfos() {
        titDateRemise = new ZTextField(new TitDateRemiseModel());
        titDateRemise.getMyTexfield().setColumns(10);
        titDateRemise.getMyTexfield().setEditable(false);
        titDateRemise.setUIReadOnly();
        titDateRemise.getMyTexfield().setHorizontalAlignment(SwingConstants.CENTER);
        titDateRemise.setFormat(ZConst.FORMAT_DATESHORT);
        
        pcoNumAncien =  new ZTextField(new PcoNumOldModel());
        pcoNumAncien.setUIReadOnly();
        pcoNumAncien.getMyTexfield().setColumns(15);
        pcoNumAncien.getMyTexfield().setEditable(false);
        pcoNumAncien.getMyTexfield().setFont(pcoNumAncien.getMyTexfield().getFont().deriveFont(Font.BOLD));
        
        pcoLibelleAncien =  new ZTextField(new PcoLibelleOldModel());
        pcoLibelleAncien.setUIReadOnly();
        pcoLibelleAncien.getMyTexfield().setColumns(50);
        pcoLibelleAncien.getMyTexfield().setEditable(false);          
        pcoLibelleAncien.getMyTexfield().setFont(pcoLibelleAncien.getMyTexfield().getFont().deriveFont(Font.BOLD));
        
        titNumero = new ZTextField(new TitNumeroModel());
        titNumero.getMyTexfield().setColumns(10);
        titNumero.getMyTexfield().setEditable(false);
        titNumero.setUIReadOnly();
        titNumero.getMyTexfield().setHorizontalAlignment(SwingConstants.CENTER);
        
        
        titFournisseur = new ZTextField(new TitFournisseurModel());
        titFournisseur.getMyTexfield().setColumns(50);
        titFournisseur.getMyTexfield().setEditable(false);
        titFournisseur.setUIReadOnly();
        
        titMontantTTC = new ZTextField(new TitMontantModel());
        titMontantTTC.getMyTexfield().setColumns(10);
        titMontantTTC.getMyTexfield().setEditable(false);
        titMontantTTC.getMyTexfield().setHorizontalAlignment(SwingConstants.RIGHT);
        titMontantTTC.setUIReadOnly();   
        titMontantTTC.setFormat(ZConst.FORMAT_DISPLAY_NUMBER);
        
        
        Box col1 = Box.createVerticalBox();
        col1.add(buildLine((new ZLabeledComponent("N° titre", titNumero, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
        col1.add(buildLine(new ZLabeledComponent("Imputation", buildLine( new Component[]{pcoNumAncien, pcoLibelleAncien} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
        col1.add(buildLine(new ZLabeledComponent("Date remise", buildLine( new Component[]{titDateRemise} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
        col1.add(buildLine(new ZLabeledComponent("Fournisseur", buildLine( new Component[]{titFournisseur} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
        col1.add(buildLine(new ZLabeledComponent("Montant TTC", buildLine( new Component[]{titMontantTTC} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
        col1.add(Box.createVerticalGlue());        
        
        return encloseInPanelWithTitle("Titre", null,ZConst.BG_COLOR_TITLE,col1,null, null);
    }
    
    
    
    
    
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        titDateRemise.updateData();
        titFournisseur.updateData();
        titMontantTTC.updateData();
        titNumero.updateData();
        pcoLibelleAncien.updateData();
        pcoNumAncien.updateData();        
        myLibelle.updateData();
        pcoNumNouveauField.updateData();
        pcoLibelleNouveauField.updateData();
        
    }
    
  
    
    
    
    
    private final class LibelleModel implements ZTextField.IZTextFieldModel {

        public Object getValue() {
            return myListener.dicoValues().get("reiLibelle");
        }

        public void setValue(Object value) {
            myListener.dicoValues().put("reiLibelle", value);
        }
        
    }    
    
    
    
    /**
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public interface IReimpSaisiePanelListener {
        public HashMap dicoValues();
        public Action actionValider();
        public Action actionAnnuler();
        public AbstractAction actionPlancomptableNouveauSelect();
    }

    private final class PcoNumNewModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.dicoValues().get("pcoNumNouveau");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.dicoValues().put("pcoNumNouveau", value);
        }
        
    }     

    private final class PcoLibelleNewModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            if (myListener.dicoValues().get("planComptableNouveau")==null) {
                return null;
            }
            return ((EOPlanComptable)myListener.dicoValues().get("planComptableNouveau")).pcoNum()+" "+ ((EOPlanComptable)myListener.dicoValues().get("planComptableNouveau")).pcoLibelle();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     
    
    private final class PcoNumOldModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return ((EOPlanComptable)myListener.dicoValues().get("planComptableAncien")).pcoNum();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
        }
        
    }     

    private final class PcoLibelleOldModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return ((EOPlanComptable)myListener.dicoValues().get("planComptableAncien")).pcoLibelle();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }    
    
    
    private final class TitFournisseurModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            if (myListener.dicoValues().get("fournisseur")==null) {
                return null;
            }
            return ((EOFournisseur)myListener.dicoValues().get("fournisseur")).adrNom()+" "+ ZStringUtil.ifNull(((EOFournisseur)myListener.dicoValues().get("fournisseur")).adrPrenom());
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     
  

    private final class TitNumeroModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.dicoValues().get("titNumero");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     
  

    private final class TitDateRemiseModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.dicoValues().get("titDateRemise");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     

    private final class TitMontantModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.dicoValues().get("titTtc");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     
  
  
}
