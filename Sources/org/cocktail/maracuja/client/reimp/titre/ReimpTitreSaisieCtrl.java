/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.reimp.titre;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.factory.process.FactoryProcessReimputation;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOReimputation;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.reimp.titre.ui.ReimpTitreSaisiePanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ReimpTitreSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'une reimputation titre";

	private final Dimension WINDOW_DIMENSION = new Dimension(600, 360);

	public final static int MODE_NEW = 1;
	public final static int MODE_MODIF = 2;
	//    private int mode;

	//    private Window parentWindow;
	private HashMap reimpDico;
	private ReimpTitreSaisiePanel myPanel;

	private final ActionAnnuler actionAnnuler = new ActionAnnuler();
	private final ActionValider actionValider = new ActionValider();
	private final ActionPlancoReimpSelect actionPlancoReimpSelect = new ActionPlancoReimpSelect();

	private ZKarukeraDialog win;
	private EOReimputation currentReimp;
	private EOTitre currentTitre;

	private FactoryProcessReimputation myFactoryProcessReimputation;

	private EODisplayGroup dgPlancomptable;
	private ZEOSelectionDialog planComptableSelectionDialog;

	/**
	 * @param editingContext
	 * @throws DefaultClientException
	 */
	public ReimpTitreSaisieCtrl(EOEditingContext editingContext, Window win) throws DefaultClientException {
		super(editingContext);
		revertChanges();
		//        parentWindow = win;
		initSubObjects();
	}

	public void initSubObjects() throws DefaultClientException {
		dgPlancomptable = new EODisplayGroup();

		myFactoryProcessReimputation = new FactoryProcessReimputation(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		reimpDico = new HashMap();
		myPanel = new ReimpTitreSaisiePanel(new ReimpTitreSaisiePanelListener());
	}

	private final void annulerSaisie() {
		getEditingContext().revert();
		getMyDialog().onCancelClick();
	}

	private final void validerSaisie() {
		try {
			validerSaisieForNew();
			getMyDialog().onOkClick();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Imprime les reimputations passï¿½es en parametre.
	 * 
	 * @param objs
	 */
	private final void imprimerReimputation(final NSArray objs) {
		try {
			String filePath = ReportFactoryClient.imprimerReimputationTitre(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), objs);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	/**
	 * @throws Exception
	 */
	private void validerSaisieForNew() throws Exception {
		ZLogger.debug("");
		checkSaisieDico();
		final EOPlanComptable pcoOld = currentTitre.planComptable();
		final EOPlanComptable pcoNew = (EOPlanComptable) reimpDico.get("planComptableNouveau");
		EOPlanComptable pcoOldEcr = pcoOld;
		EOPlanComptable pcoNewEcr = pcoNew;

		if (EOTypeBordereau.TypeBordereauPrestationInterne.equals(currentTitre.bordereau().typeBordereau().tboType()) || EOTypeBordereau.TYPE_BORDEREAU_PRESTATION_INTERNE_R.equals(currentTitre.bordereau().typeBordereau().tboType())) {
			//            
			pcoOldEcr = EOPlanComptableFinder.getPlancoValideForPcoNum(getEditingContext(), "18" + pcoOld.pcoNum(), false);
			pcoNewEcr = EOPlanComptableFinder.getPlancoValideForPcoNum(getEditingContext(), "18" + pcoNew.pcoNum(), false);

			if (pcoOldEcr == null) {
				throw new DefaultClientException("Le compte " + "18" + pcoOld.pcoNum() + " n'a pas été trouvé dans le plan comptable. Vous devez le créer pour pouvoir créer une réimputation sur le compte " + pcoOld.pcoNum());
			}
			if (pcoNewEcr == null) {
				throw new DefaultClientException("Le compte " + "18" + pcoNew.pcoNum() + " n'a pas été trouvé dans le plan comptable. Vous devez le créer pour pouvoir créer une réimputation sur le compte " + pcoNew.pcoNum());
			}

		}

		final EOReimputation reimp = myFactoryProcessReimputation.reimputerTitre(getEditingContext(), currentTitre.exercice(), pcoOld, pcoNew, pcoOldEcr, pcoNewEcr, new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime()), (String) reimpDico.get("reiLibelle"), currentTitre, myApp.appUserInfo()
				.getUtilisateur());

		ZLogger.debug(getEditingContext());

		EOEcriture ecriture = reimp.ecriture();
		if (ecriture == null) {
			throw new DefaultClientException("Aucune écriture générée");
		}

		//la sauvegarde
		getEditingContext().saveChanges();

		String msgFin = "Réimputation enregistrée. \n Souhaitez-vous l'imprimer ?";

		//Si c'est bien enregistre, on numerote
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

		try {
			myFactoryProcessReimputation.numeroterReimputation(getEditingContext(), reimp, myKFactoryNumerotation);

			//            System.out.println("Reimputation="+reimp);
			msgFin = msgFin + "\n\nLa réimputation porte le numéro " + reimp.reiNumero();
			FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
			factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), ecriture, myKFactoryNumerotation);

			msgFin = msgFin + "\nEcriture de réimputation " + ecriture.ecrNumero();
			ZLogger.debug("Ecriture de réimputation : ", ecriture.ecrNumero());

			ServerProxy.clientSideRequestAfaireApresTraitementReimputation(getEditingContext(), reimp);

			if (showConfirmationDialog("Information", msgFin, ZMsgPanel.BTLABEL_YES)) {
				imprimerReimputation(new NSArray(reimp));
			}
		} catch (Exception e) {
			System.out.println("ERREUR LORS DE LA NUMEROTATION REIMPUTATION...");
			e.printStackTrace();
			throw new Exception(e);
		}

		currentReimp = reimp;
	}

	private final void checkSaisieDico() throws Exception {
		ZLogger.debug(reimpDico);

		if (reimpDico.get("planComptableNouveau") == null) {
			throw new DataCheckException("Vous devez sélectionner une nouvelle imputation.");
		}

	}

	public final void initDicoWithDefault(EOTitre tit) {
		reimpDico.put("fournisseur", tit.fournisseur());
		reimpDico.put("titNumero", tit.titNumero());
		reimpDico.put("titTtc", tit.titTtc());
		reimpDico.put("titDateRemise", tit.titDateRemise());
		reimpDico.put("planComptableAncien", tit.planComptable());
		reimpDico.put("reiLibelle", null);
		reimpDico.put("planComptableNouveau", null);
		reimpDico.put("pcoNumNouveau", null);
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	//    private final ZKarukeraDialog createModalDialog(Frame dial ) {
	//        win = new ZKarukeraDialog(dial, "Saisie d'une rï¿½imputation titre", true);
	//        setMyDialog(win);
	//        myPanel.setMyDialog(win);
	//        myPanel.setPreferredSize(WINDOW_DIMENSION);
	//        initGUI();
	//        win.setContentPane(myPanel);
	//        win.pack();
	//        return win;
	//    }

	/**
     *
     */
	private void initGUI() {
		planComptableSelectionDialog = createPlanComptableSelectionDialog();
		myPanel.initGUI();
	}

	private ZEOSelectionDialog createPlanComptableSelectionDialog() {

		Vector filterFields = new Vector(2, 0);
		filterFields.add(EOPlanComptable.PCO_NUM_KEY);
		filterFields.add(EOPlanComptable.PCO_LIBELLE_KEY);

		Vector myCols = new Vector(2, 0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptable.PCO_NUM_KEY, "N°");
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptable.PCO_LIBELLE_KEY, "Libellé");

		myCols.add(col1);
		myCols.add(col2);

		ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMyDialog(), "Sélection d'une imputation", dgPlancomptable, myCols, "Veuillez sélectionner un compte dans la liste", filterFields);
		dialog.setFilterPrefix("");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" caseInsensitiveLike %@");

		return dialog;
	}

	private final NSArray getPcoValidesForReimp() {
		//        return ZFinder.fetchArray(getEditingContext(), EOPlanComptable.ENTITY_NAME, "pcoValidite=%@ and pcoNature=%@ and pcoNum<>%@", new NSArray(new Object[]{EOPlanComptable.etatValide, EOPlanComptable.pcoNatureRecette, currentTitre.planComptable().pcoNum()} ),new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending)), false );
		return EOPlanComptableFinder.getPlancoValidesWithCond(getEditingContext(), "pcoNature=%@ and pcoNum<>%@", new NSArray(new Object[] {
				EOPlanComptableExer.pcoNatureRecette, currentTitre.planComptable().pcoNum()
		}), false);
	}

	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		planComptableSelectionDialog.filter();
		if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
			NSArray selections = planComptableSelectionDialog.getSelectedEOObjects();
			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOPlanComptable) selections.objectAtIndex(0);
			}
		}
		setWaitCursor(false);
		return res;
	}

	/**
	 * Gere la selection d'un planco
	 */
	private final void onSelectPlancoReimp() {
		EOPlanComptable res = null;
		//on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(getPcoValidesForReimp());
		res = selectPlanComptableIndDialog((String) reimpDico.get("pcoNumNouveau"));

		reimpDico.put("planComptableNouveau", res);
		if (res != null) {
			reimpDico.put("pcoNumNouveau", res.pcoNum());
		}
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final EOReimputation openDialogNew(Dialog dial, EOTitre tit) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		//        this.setMyDialog(win);
		try {
			//            mode = MODE_NEW;
			newSaisie(tit);
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
		return currentReimp;
	}

	public final void newSaisie(EOTitre tit) {
		try {
			currentTitre = tit;
			currentReimp = null;
			initDicoWithDefault(tit);
			myPanel.updateData();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public final class ActionAnnuler extends AbstractAction {

		public ActionAnnuler() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {
		public ActionValider() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final class ActionPlancoReimpSelect extends AbstractAction {
		public ActionPlancoReimpSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour la nouvelle imputation");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoReimp();
		}
	}

	private final class ReimpTitreSaisiePanelListener implements ReimpTitreSaisiePanel.IReimpSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#dicoValues()
		 */
		public HashMap dicoValues() {
			return reimpDico;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionAnnuler()
		 */
		public Action actionAnnuler() {
			return actionAnnuler;
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.titre.ui.ReimpTitreSaisiePanel.IReimpSaisiePanelListener#actionPlancomptableNouveauSelect()
		 */
		public AbstractAction actionPlancomptableNouveauSelect() {
			return actionPlancoReimpSelect;
		}

	}

	public EOReimputation getCurrentReimp() {
		return currentReimp;
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}
}
