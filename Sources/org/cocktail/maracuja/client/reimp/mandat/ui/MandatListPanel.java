/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.reimp.mandat.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOTypeCredit;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * Panel affichant une liste de mandats.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MandatListPanel extends ZKarukeraPanel implements ZEOTableListener {
    protected ZEOTable myEOTable;
    protected ZEOTableModel myTableModel;
    protected EODisplayGroup myDisplayGroup;
    protected TableSorter myTableSorter;
    protected Vector myCols;
    private IMandatListPanelListener myListener;

    /**
     * @param editingContext
     */
    public MandatListPanel(IMandatListPanelListener listener) {
        super();
        myListener = listener;
        myDisplayGroup = new EODisplayGroup();

//        initDisplayGroup(editingContext);
    }

    
//    public void initDisplayGroup(EOEditingContext ec) {
//        myDisplayGroup = new EODisplayGroup();
////        myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(ec, "Mandat"));
////        ((EODistributedDataSource) myDisplayGroup.dataSource()).setEditingContext(ec);
//    }
    
    
    
    
    private void initTableModel() {
        myCols = new Vector(7, 0);

        ZEOTableModelColumn typeBord = new ZEOTableModelColumn(myDisplayGroup, "bordereau.typeBordereau.tboSousType", "Type", 80);
        typeBord.setAlignment(SwingConstants.LEFT);        
        
        ZEOTableModelColumn col0 = new ZEOTableModelColumn(myDisplayGroup, "bordereau.borNum", "Bordereau", 80);
        col0.setAlignment(SwingConstants.LEFT);
        col0.setColumnClass(Integer.class);

        ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "manNumero", "N° mandat", 80);
        col1.setAlignment(SwingConstants.LEFT);
        col1.setColumnClass(Integer.class);

        ZEOTableModelColumn col7 = new ZEOTableModelColumn(myDisplayGroup, "manDateRemise", "Date remise", 80);
        col7.setFormatDisplay(ZConst.FORMAT_DATESHORT);
        col7.setAlignment(SwingConstants.LEFT);
        col7.setColumnClass(Date.class);

        ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, "gestion.gesCode", "Code gestion", 80);
        col2.setAlignment(SwingConstants.CENTER);

        ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "fournisseur.adrNom", "Fournisseur", 220);
        col3.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoNum", "Imputation", 80);
        col4.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumnWithProvider typeCredit = new ZEOTableModelColumnWithProvider("Type(s) de crédit",new TypeCreditProvider(), 80);
        typeCredit.setAlignment(SwingConstants.CENTER);

        ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, "manEtat", "Etat", 80);
        col5.setAlignment(SwingConstants.CENTER);

        ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, "manTtc", "Montant TTC", 90);
        col6.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        col6.setAlignment(SwingConstants.RIGHT);
        col6.setColumnClass(BigDecimal.class);


		        

        myCols.add(typeBord);
        myCols.add(col0);
        myCols.add(col1);
        myCols.add(col7);        
        myCols.add(col2);
        myCols.add(col4);
        myCols.add(typeCredit);
        myCols.add(col3);        
        myCols.add(col6);
        myCols.add(col5);

        myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
        myTableSorter = new TableSorter(myTableModel);
    }

    /**
     * Initialise la table à afficher (le modele doit exister)
     */
    private void initTable() {
        myEOTable = new ZEOTable(myTableSorter);
        myEOTable.addListener(this);
        myTableSorter.setTableHeader(myEOTable.getTableHeader());
        myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void initGUI() {
        setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        initTableModel();
        initTable();
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }

    
    
    public final Object selectedObject() {
        return myDisplayGroup.selectedObject();
    }
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(myListener.getData());
        myEOTable.updateData();
        
//        myTableModel.updateInnerRowCount();
//        myTableModel.fireTableDataChanged();
    }

    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        myListener.onDbClick();
    }

    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.onSelectionChanged();
    }

    public interface IMandatListPanelListener {

        /**
         * @return
         */
        public NSArray getData();

        /**
         *  
         */
        public void onSelectionChanged();

        /**
         *  
         */
        public void onDbClick();
        

    }
    public ZEOTableModel getMyTableModel() {
        return myTableModel;
    }
    
    
    
    
    private final class TypeCreditProvider implements ZEOTableModelColumnProvider {

        /**
         * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
         */
        public Object getValueAtRow(int row) {
            EOMandat mandat = (EOMandat) myDisplayGroup.displayedObjects().objectAtIndex(row); 
            if (mandat!=null) {
                NSArray tcs1 = mandat.getDistinctTypeCredits();
                NSMutableArray tcs = new NSMutableArray();
                for (int i = 0; i < tcs1.count(); i++) {
                    EOTypeCredit element = (EOTypeCredit) tcs1.objectAtIndex(i);
                    tcs.addObject(element.tcdCode() );
                }
                return tcs.componentsJoinedByString(", ");
            }
            return null;
        }
        
    }    
    
}
