/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.reimp.mandat.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;

import org.cocktail.maracuja.client.common.ui.MandatFilterPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOReimputation;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class MandatReimpFilterPanel extends MandatFilterPanel {
	public static String AVEC_REIMPUTATION = "AVEC_REIMPUTATION";
	 private ZFormPanel pcoNumAncien;  
	 private ZFormPanel reimpNumero;  
	 private JCheckBox avecReimputation = new JCheckBox("Voir Seulement les mandats réimputés");
	 
	public MandatReimpFilterPanel(IMandatFilterPanelListener listener) {
		super(listener);
	}
	
    public void initGUI() {
    	avecReimputation.addActionListener(new ActionListener(){
    		public void actionPerformed(ActionEvent e) {
    			myListener.getFilters().put(AVEC_REIMPUTATION, Boolean.valueOf(avecReimputation.isSelected()));
    	}});
        pcoNumAncien = ZFormPanel.buildLabelField("Ancienne Imputation", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOReimputation.PLAN_COMPTABLE_ANCIEN_KEY+"."+EOPlanComptable.PCO_NUM_KEY) );
        pcoNumAncien.setDefaultAction(myListener.actionSrch());
        setSimpleLineBorder(pcoNumAncien);
        reimpNumero =  ZFormPanel.buildLabelField("N° reimputation", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOReimputation.REI_NUMERO_KEY) );
        reimpNumero.setDefaultAction(myListener.actionSrch());
        setSimpleLineBorder(reimpNumero);
        final ArrayList comps = new ArrayList();
        comps.add( pcoNumAncien);
        comps.add( avecReimputation);
        comps.add( reimpNumero);
        add(ZKarukeraPanel.buildLine(comps), BorderLayout.SOUTH);
        super.initGUI();
    }

}
