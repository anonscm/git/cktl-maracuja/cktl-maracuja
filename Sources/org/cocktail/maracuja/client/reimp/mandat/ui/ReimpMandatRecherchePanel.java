/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.reimp.mandat.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.MandatFilterPanel.IMandatFilterPanelListener;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.reimp.ui.ReimputationListPanel;

import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ReimpMandatRecherchePanel extends ZKarukeraPanel {
	private final Color BORDURE_COLOR = getBackground().brighter();
	private IReimpRecherchePanelListener myListener;
	private MandatListPanel mandatListPanel;
	private ReimputationListPanel reimputationListPanel;
	private ZKarukeraPanel filterPanel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ReimpMandatRecherchePanel(IReimpRecherchePanelListener listener) throws Exception {
		super();
		myListener = listener;
		mandatListPanel = new MandatListPanel(new MandatListPanelListener());
		reimputationListPanel = new ReimputationListPanel(new ReimputationListPanelListener());

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		mandatListPanel.initGUI();
		reimputationListPanel.initGUI();

		JSplitPane split1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, encloseInPanelWithTitle("Mandats", null, ZConst.BG_COLOR_TITLE, mandatListPanel, null, null), encloseInPanelWithTitle("Réimputations effectuées sur le mandat sélectionné", null, ZConst.BG_COLOR_TITLE, reimputationListPanel, null,
				null));
		split1.setDividerLocation(0.66);
		split1.setResizeWeight(0.5);
		split1.setBorder(BorderFactory.createEmptyBorder());

		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, buildTopPanel(), null, null), BorderLayout.NORTH);
		tmp.add(split1, BorderLayout.CENTER);
		//        tmp.add(, BorderLayout.SOUTH);

		this.setLayout(new BorderLayout());
		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		mandatListPanel.updateData();
		reimputationListPanel.updateData();

	}

	private final JPanel buildRightPanel() {
		ArrayList list = new ArrayList();
		list.add(myListener.actionNew());
		list.add(myListener.actionImprimer());

		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionClose());

		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private JPanel buildTopPanel() {
		filterPanel = buildFilters();
		JPanel p = new JPanel(new BorderLayout());
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		p.add(filterPanel, BorderLayout.CENTER);
		p.add(new JButton(myListener.actionSrch()), BorderLayout.EAST);
		return p;
	}

	/**
	 * @return L'objet Ordre de paiement actuelment sélectionné.
	 */
	public Object getSelectedMandat() {
		return mandatListPanel.selectedObject();
	}

	private final ZKarukeraPanel buildFilters() {
		ZKarukeraPanel p = new MandatReimpFilterPanel(new MandatFilterPanelListener());
		p.initGUI();
		return p;
	}

	//    private final NSArray getReimputationForMandat(EOMandat mandat){
	//        setWaitCursor(true);
	//        NSArray res = new NSArray();
	//        try {
	//            if (mandat!=null) {
	//                res = EOsFinder.fetchArray(getEditingContext(), EOReimputation.ENTITY_NAME, "mandat=%@", new NSArray(mandat), new NSArray(EOSortOrdering.sortOrderingWithKey("reiDate", EOSortOrdering.CompareAscending)), true);
	//            }
	//        }
	//        finally {
	//            setWaitCursor(false);
	//        }
	//        return res;
	//    }

	private final class MandatFilterPanelListener implements IMandatFilterPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.MandatFilterPanel.IMandatFilterPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myListener.getFilters();
		}

		public Action actionSrch() {
			return myListener.actionSrch();
		}

	}

	public interface IReimpRecherchePanelListener {

		/**
		 * @return
		 */
		public Action actionClose();

		/**
		 * @return
		 */
		public Action actionImprimer();

		/**
		 * @return
		 */
		public Action actionNew();

		/**
		 * @return Les mods à afficher
		 */
		public NSArray getMandats();

		/**
		 * @return un dictionaire contenant les filtres
		 */
		public HashMap getFilters();

		/**
		 * @return
		 */
		public Action actionSrch();

		/**
         * 
         */
		public void onSelectionChanged();

		/**
         * 
         */
		public void onDbClick();

		/**
		 * @return
		 */
		public NSArray getReimputations();

		/**
         * 
         */
		public void onReimputationSelectionChanged();

	}

	private final class MandatListPanelListener implements MandatListPanel.IMandatListPanelListener {

		public NSArray getData() {
			return myListener.getMandats();
		}

		public void onSelectionChanged() {
			myListener.onSelectionChanged();
			try {
				reimputationListPanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public void onDbClick() {
			myListener.onDbClick();

		}

	}

	private final class ReimputationListPanelListener implements ReimputationListPanel.IReimputationListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.reimp.ui.ReimputationListPanel.IReimputationListPanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getReimputations();
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.ui.ReimputationListPanel.IReimputationListPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			myListener.onReimputationSelectionChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.ui.ReimputationListPanel.IReimputationListPanelListener#onDbClick()
		 */
		public void onDbClick() {
			return;
		}

	}

	public MandatListPanel getReimpListPanel() {
		return mandatListPanel;
	}

	/**
	 * @return
	 */
	public final Object getSelectedReimputation() {
		return reimputationListPanel.selectedObject();
	}
}
