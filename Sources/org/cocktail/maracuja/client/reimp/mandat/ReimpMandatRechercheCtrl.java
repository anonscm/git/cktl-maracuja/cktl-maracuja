/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.reimp.mandat;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOReimputation;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.reimp.mandat.ui.MandatReimpFilterPanel;
import org.cocktail.maracuja.client.reimp.mandat.ui.ReimpMandatRecherchePanel;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ReimpMandatRechercheCtrl extends CommonCtrl {
	private static final String TITLE = "Réimputations des mandats visés";

	private final Dimension WINDOW_DIMENSION = new Dimension(900, 600);

	private final ActionClose actionClose = new ActionClose();
	private final ActionNew actionNew = new ActionNew();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionImprimer actionImprimer = new ActionImprimer();

	//    private final ActionSrch actionSrch = new ActionSrch();

	private ZKarukeraDialog win;
	private final HashMap filters = new HashMap();
	private final ReimpMandatRecherchePanel myPanel;

	/**
	 * @param editingContext
	 * @throws Exception
	 * @throws
	 */
	public ReimpMandatRechercheCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		//cette instruction pose un pb de perf si on vient du visa des mandats
		//normalement pas besoin de faire ca...  les recherches se font avec un setRefreshesRefetchedObjects
		//editingContext.invalidateAllObjects();

		myPanel = new ReimpMandatRecherchePanel(new ReimpRecherchePanelListener());
		actionImprimer.setEnabled(false);
		actionNew.setEnabled(false);

	}

	private void initGUI() {
		myPanel.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.mod.ui.ModRechercheFilterPanel.IModRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		setWaitCursor(true);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {
		NSMutableArray quals = new NSMutableArray();

		//Construire les qualifiers à partir des saisies utilisateur
		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("manNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("manNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("manNumeroMin")).intValue()))));
		}
		if (dicoFiltre.get("manNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("manNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("manNumeroMax")).intValue()))));
		}

		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		NSMutableArray qualsNumBord = new NSMutableArray();
		if (dicoFiltre.get("borNumMin") != null) {
			qualsNumBord.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borNum>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNumMin")).intValue()))));
		}
		if (dicoFiltre.get("borNumMax") != null) {
			qualsNumBord.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borNum<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNumMax")).intValue()))));
		}
		if (qualsNumBord.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNumBord));
		}

		if (!ZStringUtil.isEmpty((String) dicoFiltre.get(EOGestion.GES_CODE_KEY))) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("gesCode=%@", new NSArray(dicoFiltre.get(EOGestion.GES_CODE_KEY))));
		}

		if (!ZStringUtil.isEmpty((String) dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum=%@", new NSArray(dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))));
		}

		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		if (dicoFiltre.get(MandatReimpFilterPanel.AVEC_REIMPUTATION) != null && ((Boolean) dicoFiltre.get(MandatReimpFilterPanel.AVEC_REIMPUTATION)).booleanValue()) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.REIMPUTATIONS_KEY + "." + EOReimputation.REI_NUMERO_KEY + "<>nil", null));
		}

		if (!ZStringUtil.isEmpty((String) dicoFiltre.get(EOReimputation.PLAN_COMPTABLE_ANCIEN_KEY + "." + EOPlanComptable.PCO_NUM_KEY))) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.REIMPUTATIONS_KEY + "." + EOReimputation.PLAN_COMPTABLE_ANCIEN_KEY + "." + EOPlanComptable.PCO_NUM_KEY + "=%@", new NSArray(new Object[] {
					dicoFiltre.get(EOReimputation.PLAN_COMPTABLE_ANCIEN_KEY + "."
							+ EOPlanComptable.PCO_NUM_KEY)
			})));
		}

		if (!ZStringUtil.isEmpty((String) dicoFiltre.get(EOReimputation.REI_NUMERO_KEY))) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOMandat.REIMPUTATIONS_KEY + "." + EOReimputation.REI_NUMERO_KEY + "=%@", new NSArray(new Object[] {
					new Integer((String) dicoFiltre.get(EOReimputation.REI_NUMERO_KEY))
			})));
		}

		return quals;
	}

	private final void reimpNew() {
		EOMandat man = (EOMandat) myPanel.getSelectedMandat();
		try {
			if (man == null) {
				throw new DefaultClientException("Veuillez sélectionner un mandat à réimputer");
			}

			NSArray autGestions = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_REMA);
			if (autGestions.indexOfObject(man.gestion()) == NSArray.NotFound) {
				throw new DefaultClientException("Vous n'avez pas les droits de reimputer des mandats pour le code gestion " + man.gestion().gesCode());
			}

			ReimpMandatSaisieCtrl mySaisieCtrl = new ReimpMandatSaisieCtrl(getEditingContext(), myPanel.getMyDialog());
			EOReimputation mod = mySaisieCtrl.openDialogNew(getMyDialog(), man);
			if (mod != null) {
				myPanel.updateData();
			}

		} catch (Exception e1) {
			showErrorDialog(e1);
			return;
		}

	}

	private final void reimpPrint() {
		try {
			if (myPanel.getSelectedReimputation() == null) {
				throw new DefaultClientException("Aucune réimputation n'est sélectionnée");
			}

			String filePath = ReportFactoryClient.imprimerReimputationMandat(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(myPanel.getSelectedReimputation()), getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

	}

	private void fermer() {
		getEditingContext().revert();
		win.onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();

		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	private final ZKarukeraDialog createModalDialog(Frame dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();

		win.setContentPane(myPanel);

		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Dialog dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			//            myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final void openDialog(Frame dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			//            myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final void refreshActions() {
		actionNew.setEnabled(myPanel.getSelectedMandat() != null);
	}

	private final class ReimpRecherchePanelListener implements ReimpMandatRecherchePanel.IReimpRecherchePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementRecherchePanel.IModRecherchePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementRecherchePanel.IModRecherchePanelListener#actionNew()
		 */
		public Action actionNew() {
			return actionNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementRecherchePanel.IModRecherchePanelListener#getMods()
		 */
		public NSArray getMandats() {
			try {
				NSMutableArray qualsGlob = new NSMutableArray();
				qualsGlob.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(getExercice())));
				qualsGlob.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.typeBordereau.tboType=%@ or bordereau.typeBordereau.tboType=%@ or bordereau.typeBordereau.tboType=%@", new NSArray(new Object[] {
						EOTypeBordereau.TypeBordereauBTME, EOTypeBordereau.TypeBordereauBTMS,
						EOTypeBordereau.TYPE_BORDEREAU_PRESTATION_INTERNE_D
				})));
				qualsGlob.addObject(EOQualifier.qualifierWithQualifierFormat("(manEtat=%@ or manEtat=%@)", new NSArray(new Object[] {
						EOMandat.mandatVise, EOMandat.mandatVirement
				})));

				final EOAndQualifier qualFinal = new EOAndQualifier(new NSArray(new Object[] {
						new EOAndQualifier(qualsGlob), new EOAndQualifier(buildFilterQualifiers(filters))
				}));
				final NSArray res = ZFinder.fetchArrayWithPrefetching(getEditingContext(), EOMandat.ENTITY_NAME, qualFinal, null, true, new NSArray("bordereau"), 100);
				getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), res));

				NSArray res2;
				if (res.count() > 0) {
					if (res.count() == 100) {
						showInfoDialog("Seuls les 100 premiers mandats ont été récupérés.");
					}

					NSMutableArray sort = new NSMutableArray();
					sort.addObject(EOSortOrdering.sortOrderingWithKey("bordereau.borNum", EOSortOrdering.CompareAscending));
					sort.addObject(EOSortOrdering.sortOrderingWithKey("manNumero", EOSortOrdering.CompareAscending));
					res2 = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, sort);
				}
				else {
					res2 = new NSArray();
				}

				return res2;

			} catch (Exception e) {
				showErrorDialog(e);
				return new NSArray();
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementRecherchePanel.IModRecherchePanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return filters;
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.mod.ui.ModRecherchePanel.IModRecherchePanelListener#actionSrch()
		//         */
		//        public Action actionSrch() {
		//            return actionSrch;
		//        }

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementRecherchePanel.IModRecherchePanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			refreshActions();
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementRecherchePanel.IModRecherchePanelListener#onDbClick()
		 */
		public void onDbClick() {
			reimpNew();
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.mandat.ui.ReimpMandatRecherchePanel.IReimpRecherchePanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.mandat.ui.ReimpMandatRecherchePanel.IReimpRecherchePanelListener#getReimputations()
		 */
		public NSArray getReimputations() {
			EOMandat mandat = (EOMandat) myPanel.getSelectedMandat();
			NSArray res = new NSArray();
			if (mandat != null) {
				res = EOsFinder.fetchArray(getEditingContext(), EOReimputation.ENTITY_NAME, "mandat=%@", new NSArray(mandat), new NSArray(EOSortOrdering.sortOrderingWithKey("reiDate", EOSortOrdering.CompareAscending)), true);
			}
			return res;
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.mandat.ui.ReimpMandatRecherchePanel.IReimpRecherchePanelListener#onReimputationSelectionChanged()
		 */
		public void onReimputationSelectionChanged() {
			actionImprimer.setEnabled(myPanel.getSelectedReimputation() != null);
		}

		/**
		 * @see org.cocktail.maracuja.client.reimp.mandat.ui.ReimpMandatRecherchePanel.IReimpRecherchePanelListener#actionImprimer()
		 */
		public Action actionImprimer() {
			return actionImprimer;
		}

	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Créer une nouvelle réimputation à partir du mandat sélectionné");
		}

		public void actionPerformed(ActionEvent e) {
			reimpNew();
		}
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionImprimer extends AbstractAction {
		public ActionImprimer() {
			super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer la réimputation sélectionnée");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			reimpPrint();
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
