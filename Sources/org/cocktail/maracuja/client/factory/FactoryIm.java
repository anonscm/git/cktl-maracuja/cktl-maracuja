/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOIm;
import org.cocktail.maracuja.client.metier.EOImTaux;
import org.cocktail.maracuja.client.metier.EOImTypeTaux;
import org.cocktail.maracuja.client.metier.EOJdDepensePapier;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZDateUtil;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class FactoryIm extends Factory {
	/**
	 * Date à partir de laquelle il faut ajouter 8 points au taux BCE (au lieu de 7 avant)
	 */
	protected static final Date DATE_TBCE_8POINTS = ZDateUtil.stringToDate("01/05/2013");
	protected static final BigDecimal BIG_DECIMAL_8 = BigDecimal.valueOf(8d).setScale(0);
	protected static final BigDecimal BIG_DECIMAL_7 = BigDecimal.valueOf(7d).setScale(0);
	protected static final BigDecimal BIG_DECIMAL_2 = BigDecimal.valueOf(2d).setScale(0);

	private static NSArray allTaux = null;

	public FactoryIm(boolean withLog) {
		super(withLog);
	}

	public EOIm newObject(EOEditingContext ed, EODepense depense, EOUtilisateur utilisateur) {
		final EOIm newObject = (EOIm) Factory.instanceForEntity(ed, EOIm.ENTITY_NAME);
		ed.insertObject(newObject);
		newObject.setUtilisateurRelationship(utilisateur);
		newObject.setDepenseRelationship(depense);
		return newObject;
	}

	/**
	 * Genere les im pour un paiement.
	 * 
	 * @param ed
	 * @param paiement
	 * @param utilisateur
	 * @return un NSArray d'IMs.
	 * @throws Exception
	 */
	public NSArray genereImsForPaiement(EOEditingContext ed, EOPaiement paiement, EOUtilisateur utilisateur) throws Exception {
		//Nettoyer tous les ims existant

		NSMutableArray res = new NSMutableArray();
		NSArray depenses = paiement.getDepenses();
		for (int i = 0; i < depenses.count(); i++) {
			EODepense depense = (EODepense) depenses.objectAtIndex(i);
			EOIm im = genereImForDepense(ed, depense, utilisateur);
			if (im != null) {
				res.addObject(im);
			}
		}
		return res;
	}

	/**
	 * @param ed
	 * @param depense
	 * @param utilisateur
	 * @return un IM s'il a lieu d'être genere. L'im existant est supprimé.
	 * @throws Exception
	 */
	public EOIm genereImForDepense(EOEditingContext ed, EODepense depense, EOUtilisateur utilisateur) throws Exception {
		NSArray imExistants = EOIm.fetchAllForDepense(ed, depense, null, false);
		for (int i = 0; i < imExistants.count(); i++) {
			deleteIm(ed, (EOIm) imExistants.objectAtIndex(i));
		}

		if (shouldGenererIm(depense)) {
			EOIm im = newObject(ed, depense, utilisateur);
			initialiseIm(ed, im, utilisateur, depense.mandat().paiement().paiDateCreation());
			return im;
		}
		return null;
	}

	/**
	 * @param depense
	 * @return true si la date de fin de DGP de la depense est antérieure a la date du paiement de la depense et si la depense est issue d'un
	 *         bordereau de type BTME.
	 * @throws Exception
	 */
	public boolean shouldGenererIm(EODepense depense) throws Exception {
		try {
			if (depense == null) {
				throw new Exception("La depense est nulle");
			}

			//Limiter aux mandats de type BTME 
			if (!depense.mandat().bordereau().isBordereauBTME()) {
				return false;
			}

			if (depense.getJdDepenseCtrlPlanco() == null) {
				throw new Exception("Immpossible de relier la depense payee a un depense_ctrl_planco de jefy_depense.");
			}
			EOJdDepensePapier dpp = depense.getJdDepenseCtrlPlanco().jdDepensePapier();
			if (dpp == null) {
				throw new Exception("La dpp est nulle");
			}
			if (dpp.imDepenseTauxRef() == null) {
				throw new Exception("Determination impossible des informations sur les IM pour la facture n° " + dpp.dppNumeroFacture() + " du " + ZConst.FORMAT_DATESHORT.format(dpp.dppDateFacture()) + ". Verifiez que les parametres (DGP et taux sont bien paramétrés pour la date de la facture).");
			}
			if (dpp.imDepenseTauxRef().finDgpReelle() == null) {
				throw new Exception("Determination impossible du DGP pour la facture n° " + dpp.dppNumeroFacture() + " du " + ZConst.FORMAT_DATESHORT.format(dpp.dppDateFacture()) + ". Verifiez que le parametrage du DGP inclut la date de debut de DGP ("
						+ ZConst.FORMAT_DATESHORT.format(dpp.dateDebutDgp()) + ".");
			}
			if (dpp.imDepenseTauxRef().imTypeTaux() == null) {
				throw new Exception("Determination impossible du Taux de reference pour la facture n° " + dpp.dppNumeroFacture() + " du " + ZConst.FORMAT_DATESHORT.format(dpp.dppDateFacture()) + ". Verifiez que le parametrage des taux inclut la date de la facture.");
			}

			return (dpp.imDepenseTauxRef().finDgpReelle().before(depense.mandat().paiement().paiDateCreation()));
		} catch (Exception e) {
			System.err.println(depense);
			throw e;
		}
	}

	/**
	 * Initialise les proprietes de l'IM en fonction de la depense qui lui est attachée.
	 * 
	 * @param ed
	 * @param im
	 * @param utilisateur
	 * @throws Exception
	 */
	public void initialiseIm(EOEditingContext ed, EOIm im, EOUtilisateur utilisateur, Date dateGenerationIms) throws Exception {
		try {
			if (im != null) {
				if (im.isValide()) {
					throw new Exception("L'intérêt moratoire est valide, vous devez changer son état à 'en attente' avant de pouvoir le réinitialiser.");
				}
				if (im.isAnnule()) {
					throw new Exception("L'intérêt moratoire est annulé, vous devez changer son état à 'en attente' avant de pouvoir le réinitialiser.");
				}

				if (!(im.isEnAttente())) {
					throw new Exception("L'intérêt moratoire doit etre en attente ou annulé pour que vous puissiez le réinitialiser.");
				}

				im.cleanIm();
				im.setUtilisateurRelationship(utilisateur);

				EOJdDepensePapier dpp = im.depense().getJdDepenseCtrlPlanco().jdDepensePapier();

				//Ce taux est calculé en amont via une vue
				im.setImTauxRelationship(dpp.imDepenseTauxRef().imTaux());
				im.setImLibelleTaux(dpp.imDepenseTauxRef().imTypeTaux().imttLibelle());
				im.setImDateDepartDgp(dpp.imDepenseTauxRef().dateDebutDgp());
				im.setImDgp(dpp.imDepenseTauxRef().imdgDgp());
				im.setImDateFinDgpTheorique(dpp.imDepenseTauxRef().finDgpTheorique());
				im.setImDureeSusp(dpp.imDepenseTauxRef().dureeSuspension());
				im.setImDateFinDgpReelle(dpp.imDepenseTauxRef().finDgpReelle());

				NSTimestamp datePaiement = im.depense().mandat().paiement().paiDateCreation();
				im.setImNbJoursDepassement(calculeNbJoursDepassement(dpp.imDepenseTauxRef().finDgpReelle(), datePaiement));

				im.setImTauxApplicable(determineTauxApplicable(im.imTaux(), dateGenerationIms));
				im.setImPenalite(determinePenalite(im));

				//recuperer le montant regle
				BigDecimal montantRegle = im.depense().depMontantDisquette().setScale(2, BigDecimal.ROUND_HALF_UP);
				BigDecimal montantIm = calculeMontantIm(montantRegle, im.imTauxApplicable(), im.imNbJoursDepassement(), im.imPenalite());
				im.setImMontant(montantIm);

				im.setImNumero("0");

			}
		} catch (Exception e) {
			System.err.println(im);
			throw e;
		}

	}

	public void deleteIm(EOEditingContext ed, EOIm im) throws Exception {
		im.validateForDelete();
		im.setUtilisateurRelationship(null);
		im.setDepenseRelationship(null);
		ed.deleteObject(im);
	}

	public void rejeteIm(EOEditingContext ed, EOIm im, EOUtilisateur utilisateur) {
		im.setUtilisateurRelationship(utilisateur);
		im.rejeter();
	}

	public void valideIm(EOEditingContext ed, EOIm im, EOUtilisateur utilisateur) {
		if (im.isEnAttente()) {
			im.setUtilisateurRelationship(utilisateur);
			im.valider();
		}
	}

	public void enAttenteIm(EOEditingContext ed, EOIm im, EOUtilisateur utilisateur) {
		if (im.isValide() || im.isAnnule()) {
			im.setUtilisateurRelationship(utilisateur);
			im.mettreEnAttente();
		}
	}

	public static BigDecimal determineTauxApplicable(EOImTaux tauxReference, Date date) {
		BigDecimal tauxApplicable = null;
		if (tauxReference != null) {
			tauxApplicable = tauxReference.imtaTaux().add(determineTauxCompensation(tauxReference.imTypeTaux(), date).setScale(2, BigDecimal.ROUND_HALF_UP));
		}
		return tauxApplicable;

	}

	/**
	 * le taux de reference est calculé dans la vue maracuja.v_depense_im_taux_ref
	 * 
	 * @deprecated
	 */
	public static EOImTaux determineTauxReference(EOEditingContext ec, NSTimestamp dateFinReelle, String typeTauxReferenceCode) throws Exception {
		EOImTaux tauxReference = null;

		if (EOImTypeTaux.IMTT_CODE_TBCE.equals(typeTauxReferenceCode)) {
			NSTimestamp debutDepassement = new NSTimestamp(ZDateUtil.addDHMS(dateFinReelle, 1, 0, 0, 0));
			NSTimestamp debutSemestre = null;
			if (ZDateUtil.getMonth(debutDepassement) < 6) {
				debutSemestre = new NSTimestamp(ZDateUtil.getFirstDayOfYear(debutDepassement));
			}
			else {
				debutSemestre = new NSTimestamp((Date) ZDateUtil.getFirstDaysOfMonth(ZDateUtil.getYear(debutDepassement)).get(6));
			}

			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_CODE_KEY, EOQualifier.QualifierOperatorEqual, typeTauxReferenceCode));
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IMTA_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, debutSemestre));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(debutSemestre)));

			NSArray res = EOQualifier.filteredArrayWithQualifier(allTaux(ec), new EOAndQualifier(quals));
			if (res.count() > 0) {
				tauxReference = (EOImTaux) res.objectAtIndex(0);
			}
			else {
				throw new Exception("Aucun taux de type " + typeTauxReferenceCode + " pour la date " + ZConst.FORMAT_DATESHORT.format(debutDepassement));
			}
		}
		else {
			NSTimestamp debutDepassement = new NSTimestamp(ZDateUtil.addDHMS(dateFinReelle, 1, 0, 0, 0));
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_CODE_KEY, EOQualifier.QualifierOperatorEqual, typeTauxReferenceCode));
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IMTA_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, debutDepassement));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(debutDepassement)));

			NSArray res = EOQualifier.filteredArrayWithQualifier(allTaux(ec), new EOAndQualifier(quals));
			if (res.count() > 0) {
				tauxReference = (EOImTaux) res.objectAtIndex(0);
			}
			else {
				throw new Exception("Aucun taux de type " + typeTauxReferenceCode + " pour la date " + ZConst.FORMAT_DATESHORT.format(debutDepassement));
			}
		}
		if (tauxReference == null) {
			throw new Exception("Aucun taux de type " + typeTauxReferenceCode + " pour la date " + ZConst.FORMAT_DATESHORT.format(dateFinReelle));
		}
		return tauxReference;
	}

	/**
	 * @deprecated
	 */
	public static BigDecimal determineTauxApplicable(EOEditingContext ec, NSTimestamp dateFinReelle, String typeTauxReferenceCode) throws Exception {
		BigDecimal tauxApplicable = null;

		if (EOImTypeTaux.IMTT_CODE_TBCE.equals(typeTauxReferenceCode)) {
			NSTimestamp debutDepassement = new NSTimestamp(ZDateUtil.addDHMS(dateFinReelle, 1, 0, 0, 0));
			NSTimestamp debutSemestre = new NSTimestamp(ZDateUtil.addDHMS(ZDateUtil.getDateDebutSemestre(debutDepassement), -1, 0, 0, 0));
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_CODE_KEY, EOQualifier.QualifierOperatorEqual, typeTauxReferenceCode));
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IMTA_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, debutSemestre));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(debutSemestre)));

			NSArray res = EOQualifier.filteredArrayWithQualifier(allTaux(ec), new EOAndQualifier(quals));
			if (res.count() > 0) {
				EOImTaux imTaux = (EOImTaux) res.objectAtIndex(0);
				//pas normal d'avoir ca ici
				//				tauxApplicable = imTaux.imtaTaux().add(new BigDecimal(7).setScale(2, BigDecimal.ROUND_HALF_UP));
				tauxApplicable = determineTauxApplicable(imTaux, debutDepassement);

			}
			else {
				throw new Exception("Aucun taux de type " + typeTauxReferenceCode + " trouvé pour la date du " + ZConst.FORMAT_DATESHORT.format(debutSemestre));
			}

		}
		else {
			NSTimestamp debutDepassement = new NSTimestamp(ZDateUtil.addDHMS(dateFinReelle, 1, 0, 0, 0));
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IM_TYPE_TAUX_KEY + "." + EOImTypeTaux.IMTT_CODE_KEY, EOQualifier.QualifierOperatorEqual, typeTauxReferenceCode));
			quals.addObject(new EOKeyValueQualifier(EOImTaux.IMTA_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, debutDepassement));
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + "=nil or " + EOImTaux.IMTA_FIN_KEY + ">=%@", new NSArray(debutDepassement)));

			NSArray res = EOQualifier.filteredArrayWithQualifier(allTaux(ec), new EOAndQualifier(quals));
			if (res.count() > 0) {
				EOImTaux imTaux = (EOImTaux) res.objectAtIndex(0);
				tauxApplicable = imTaux.imtaTaux().add(new BigDecimal(2).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
			else {
				throw new Exception("Aucun taux de type " + typeTauxReferenceCode + " pour la date " + ZConst.FORMAT_DATESHORT.format(debutDepassement));
			}

		}
		if (tauxApplicable == null) {
			throw new Exception("Aucun taux de type " + typeTauxReferenceCode + " pour la date " + ZConst.FORMAT_DATESHORT.format(dateFinReelle));
		}
		return tauxApplicable;
	}

	/**
	 * @deprecated
	 */
	public static BigDecimal determineTauxApplicable(EOIm im) throws Exception {
		return determineTauxApplicable(im.editingContext(), im.imDateFinDgpReelle(), im.imTaux().imTypeTaux().imttCode());
	}

	public BigDecimal determinePenalite(EOIm im) {
		return (im.imTaux().imtaPenalite() != null ? im.imTaux().imtaPenalite() : BigDecimal.ZERO);
	}

	private static NSArray allTaux(EOEditingContext ec) {
		if (allTaux == null) {
			allTaux = EOImTaux.fetchAll(ec, null, null, false);
		}
		return allTaux;
	}

	public static NSTimestamp getDateDebutCalculIms(NSTimestamp dateFinDgpReelle) {
		if (dateFinDgpReelle == null) {
			return null;
		}
		return new NSTimestamp(ZDateUtil.addDHMS(dateFinDgpReelle, 1, 0, 0, 0));
	}

	public static BigDecimal calculeMontantIm(BigDecimal montantRegle, BigDecimal tauxApplicable, Number nbJoursDepassements, BigDecimal penalite) {
		BigDecimal montantIm = null;
		if (tauxApplicable != null && nbJoursDepassements != null) {
			//Montant IM = (montant réglé)*(taux applicable IM/100).*[(nombre total de jours de dépassement DGP) / 365
			BigDecimal montantTotal = montantRegle.multiply(tauxApplicable.setScale(2, BigDecimal.ROUND_HALF_UP)).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal montantAvecTauxAnnee = montantTotal.divide(new BigDecimal(100).setScale(2), 2, BigDecimal.ROUND_HALF_UP);
			BigDecimal nbJours = new BigDecimal(nbJoursDepassements.doubleValue()).setScale(2);
			BigDecimal nbJoursAnnee = new BigDecimal(365.0).setScale(2);
			montantIm = montantAvecTauxAnnee.multiply(nbJours).setScale(2, BigDecimal.ROUND_HALF_UP).divide(nbJoursAnnee, 2, BigDecimal.ROUND_HALF_UP);
			montantIm = montantIm.add(penalite);
		}
		return montantIm;
	}

	public static Number calculeNbJoursDepassement(NSTimestamp dateFinDgpReelle, NSTimestamp datePaiement) {
		Long nbJours = null;
		if (dateFinDgpReelle != null && datePaiement != null) {
			nbJours = new Long(ZDateUtil.calcDaysBetween(dateFinDgpReelle, datePaiement));
			if (nbJours.doubleValue() < 0) {
				nbJours = new Long(0);
			}
		}
		return nbJours;
	}

	/**
	 * @return le nombre de points à rajouter au taux pour le calcul (exemple tbce + 8 points)
	 */
	public static BigDecimal determineTauxCompensation(EOImTypeTaux typeTaux, Date date) {
		if (EOImTypeTaux.IMTT_CODE_TBCE.equals(typeTaux.imttCode())) {
			if (date.equals(DATE_TBCE_8POINTS) || date.after(DATE_TBCE_8POINTS)) {
				return BIG_DECIMAL_8;
			}
			return BIG_DECIMAL_7;
		}
		return BIG_DECIMAL_2;
	}

	/**
	 * Map pour les objets IM. Permet de faire du traitement metier en fonction des entrees.
	 */
	public static class ImMap extends HashMap {
		//private final EOEditingContext ec;
		private final IImMapListener listener;

		public ImMap(EOEditingContext ec, IImMapListener listener) {
			//this.ec = ec;
			this.listener = listener;
		}

		public Object put(Object key, Object value) {
			if (EOIm.IM_DATE_DEPART_DGP_KEY.equals(key)) {
				setImDateDepartDgp((NSTimestamp) value);
			}
			else if (EOIm.IM_DATE_FIN_DGP_REELLE_KEY.equals(key)) {
				setImDateFinDgpReelle((NSTimestamp) value);
			}
			else if (EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY.equals(key)) {
				setImDateFinDgpTheorique((NSTimestamp) value);
			}
			else if (EOIm.IM_DGP_KEY.equals(key)) {
				setImDgp((Number) value);
			}
			else if (EOIm.IM_DUREE_SUSP_KEY.equals(key)) {
				setImDureeSusp((Number) value);
			}
			else if (EOIm.IM_MONTANT_KEY.equals(key)) {
				setImMontant((BigDecimal) value);
			}
			else if (EOIm.IM_PENALITE_KEY.equals(key)) {
				setImPenalite((BigDecimal) value);
			}
			else if (EOIm.IM_NB_JOURS_DEPASSEMENT_KEY.equals(key)) {
				setImNbJoursDepassement((Number) value);
			}
			else if (EOIm.IM_TAUX_KEY.equals(key)) {
				setImTaux((EOImTaux) value);
			}
			else if (EOIm.IM_TAUX_APPLICABLE_KEY.equals(key)) {
				setImTauxApplicable((BigDecimal) value);
			}
			else {
				super.put(key, value);
			}
			return get(key);
		}

		public Object putSilent(Object key, Object value) {
			return super.put(key, value);
		}

		public void setImDateDepartDgp(NSTimestamp aValue) {
			super.put(EOIm.IM_DATE_DEPART_DGP_KEY, aValue);
			setImDateFinDgpTheorique(calculeDateFinTheorique());
		}

		public void setImDgp(Number aValue) {
			super.put(EOIm.IM_DGP_KEY, aValue);
			setImDateFinDgpTheorique(calculeDateFinTheorique());
		}

		public void setImPenalite(Number aValue) {
			if (aValue == null) {
				aValue = BigDecimal.ZERO;
			}
			super.put(EOIm.IM_PENALITE_KEY, aValue);
			setImMontant(calculeMontant());
		}

		public void setImDateFinDgpTheorique(NSTimestamp aValue) {
			super.put(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY, aValue);
			setImDateFinDgpReelle(calculeDateFinReelle());
		}

		public void setImDureeSusp(Number aValue) {
			super.put(EOIm.IM_DUREE_SUSP_KEY, aValue);
			setImDateFinDgpReelle(calculeDateFinReelle());
		}

		public void setImDateFinDgpReelle(NSTimestamp aValue) {
			boolean notify = false;
			if (get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY) == null || !get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY).equals(aValue)) {
				notify = true;
			}
			super.put(EOIm.IM_DATE_FIN_DGP_REELLE_KEY, aValue);
			if (notify) {
				notifyImDateFinReelleChanged();
			}
			setImNbJoursDepassement(calculeNbJourDepassement());
		}

		public void setImNbJoursDepassement(Number aValue) {
			super.put(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY, aValue);
			setImMontant(calculeMontant());
		}

		public void setImTaux(EOImTaux aValue) {
			super.put(EOIm.IM_TAUX_KEY, aValue);
			setImTauxApplicable(calculeImTauxApplicable());
		}

		public void setImTauxApplicable(BigDecimal aValue) {
			super.put(EOIm.IM_TAUX_APPLICABLE_KEY, aValue);
			setImMontant(calculeMontant());
		}

		public void setImMontant(BigDecimal aValue) {
			super.put(EOIm.IM_MONTANT_KEY, aValue);
		}

		public NSTimestamp calculeDateFinReelle() {
			NSTimestamp dateFinReelle = null;
			NSTimestamp dateDepart = (NSTimestamp) get(EOIm.IM_DATE_FIN_DGP_THEORIQUE_KEY);
			Number dureeSusp = (Number) get(EOIm.IM_DUREE_SUSP_KEY);
			if (dureeSusp == null) {
				dureeSusp = new Integer(0);
			}
			if (dateDepart != null) {
				dateFinReelle = new NSTimestamp(ZDateUtil.addDHMS(dateDepart, dureeSusp.intValue(), 0, 0, 0));
			}
			return dateFinReelle;

		}

		public NSTimestamp calculeDateFinTheorique() {
			NSTimestamp dateFinTheorique = null;
			NSTimestamp dateDepart = (NSTimestamp) get(EOIm.IM_DATE_DEPART_DGP_KEY);
			Number dgp = (Number) get(EOIm.IM_DGP_KEY);
			if (dgp != null && dateDepart != null) {
				dateFinTheorique = new NSTimestamp(ZDateUtil.addDHMS(dateDepart, dgp.intValue(), 0, 0, 0));
			}
			return dateFinTheorique;
		}

		public BigDecimal calculeMontant() {
			BigDecimal montantRegle = ((BigDecimal) get(EOIm.DEPENSE_KEY + "." + EODepense.DEP_MONTANT_DISQUETTE_KEY)).setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal tauxApplicable = (BigDecimal) get(EOIm.IM_TAUX_APPLICABLE_KEY);
			Number nbJoursDepassements = (Number) get(EOIm.IM_NB_JOURS_DEPASSEMENT_KEY);
			BigDecimal penalite = (BigDecimal) get(EOIm.IM_PENALITE_KEY);
			return calculeMontantIm(montantRegle, tauxApplicable, nbJoursDepassements, penalite);
		}

		public Number calculeNbJourDepassement() {
			NSTimestamp dateFinDgpReelle = (NSTimestamp) get(EOIm.IM_DATE_FIN_DGP_REELLE_KEY);
			NSTimestamp datePaiement = (NSTimestamp) get(EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.PAIEMENT_KEY + "." + EOPaiement.PAI_DATE_CREATION_KEY);
			return calculeNbJoursDepassement(dateFinDgpReelle, datePaiement);
		}

		public BigDecimal calculeImTauxApplicable() {
			EOImTaux imtTaux = (EOImTaux) get(EOIm.IM_TAUX_KEY);
			NSTimestamp datePaiement = (NSTimestamp) get(EOIm.DEPENSE_KEY + "." + EODepense.MANDAT_KEY + "." + EOMandat.PAIEMENT_KEY + "." + EOPaiement.PAI_DATE_CREATION_KEY);
			return determineTauxApplicable(imtTaux, datePaiement);
		}

		public void notifyImDateFinReelleChanged() {
			if (this.listener != null) {
				listener.onImDateFinReelleChanged();
			}
		}

	}

	public static interface IImMapListener {
		public void onImDateFinReelleChanged();
	}

}
