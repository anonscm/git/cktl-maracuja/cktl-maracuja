/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.metier.EOPrelevementFichier;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author rprin
 */
public class FactoryPrelevementFichier extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryPrelevementFichier(boolean withLog) {
		super(withLog);
	}

	public EOPrelevementFichier creerPrelevementFichier(EOEditingContext ed, EORecouvrement recouvrement, EOTypeRecouvrement typeRecouvrement, EOUtilisateur utilisateur, String contenu, NSTimestamp creation, NSTimestamp dateRemise, NSTimestamp dateReglement, String libelle, BigDecimal ficpMontant,
			Integer ficpNbPrelevements, Integer ficpNumero, String ficpCodeOp) {
		final EOPrelevementFichier monVirementFichier = (EOPrelevementFichier) Factory.instanceForEntity(ed, EOPrelevementFichier.ENTITY_NAME);
		ed.insertObject(monVirementFichier);

		monVirementFichier.setFicpContenu(contenu);
		monVirementFichier.setFicpDateCreation(creation);
		monVirementFichier.setFicpDateModif(creation);
		monVirementFichier.setFicpLibelle(libelle);
		monVirementFichier.setFicpDateRemise(dateRemise);
		monVirementFichier.setFicpMontant(ficpMontant);
		monVirementFichier.setFicpNbPrelevements(ficpNbPrelevements);
		monVirementFichier.setFicpDateReglement(dateReglement);
		monVirementFichier.setFicpNumero(ficpNumero);
		monVirementFichier.setFicpCodeOp(ficpCodeOp);

		monVirementFichier.setRecouvrementRelationship(recouvrement);
		monVirementFichier.setUtilisateurRelationship(utilisateur);
		monVirementFichier.setTypeRecouvrementRelationship(typeRecouvrement);

		return monVirementFichier;
	}
}
