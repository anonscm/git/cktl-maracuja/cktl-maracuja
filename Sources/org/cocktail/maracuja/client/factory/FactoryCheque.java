/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOCheque;
import org.cocktail.maracuja.client.metier.EOChequeBrouillard;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author frivalla
 * @author rprin
 */
public class FactoryCheque extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryCheque(boolean withLog) {
		super(withLog);
	}

	public EOCheque creerUnCheque(EOEditingContext ed, String banqAgence, String banqCode, String banqLibelle, EOBordereau bordereau, NSTimestamp cheDateCheque, NSTimestamp cheDateCreation, BigDecimal cheMontant, String cheNomTireur, String cheNumeroCheque, String cheNumeroCompte,
			String chePrenomTireur, String rcptCode, EOUtilisateur utilisateur) throws Exception {
		final EOCheque nouveauCheque = (EOCheque) Factory.instanceForEntity(ed, EOCheque.ENTITY_NAME);

		ed.insertObject(nouveauCheque);

		nouveauCheque.setBanqAgence(banqAgence);
		nouveauCheque.setBanqCode(banqCode);
		nouveauCheque.setBanqLibelle(banqLibelle);
		nouveauCheque.setCheDateCheque(cheDateCheque);
		nouveauCheque.setCheDateSaisie(cheDateCreation);
		nouveauCheque.setCheEtat(EOCheque.etatValide);
		nouveauCheque.setCheMontant(cheMontant);
		nouveauCheque.setCheNomTireur(cheNomTireur);
		nouveauCheque.setCheNumeroCheque(cheNumeroCheque);
		nouveauCheque.setCheNumeroCompte(cheNumeroCompte);
		nouveauCheque.setChePrenomTireur(chePrenomTireur);
		nouveauCheque.setRcptCode(rcptCode);

		affecterBordereau(nouveauCheque, bordereau);
		nouveauCheque.setUtilisateurRelationship(utilisateur);

		//		nouveauCheque.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

		return nouveauCheque;
	}

	public final void affecterBordereau(final EOCheque cheque, EOBordereau bordereau) {

		if (EOBordereau.BordereauVise.equals(bordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		// ANNULE
		if (EOBordereau.BordereauAnnule.equals(bordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAnnule);
		}

		// A VISER
		if (EOBordereau.BordereauAViser.equals(bordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAViser);
		}

		// le cheque nedoit pas etre ANNULE
		if (EOCheque.etatAnnule.equals(cheque.cheEtat())) {
			throw new FactoryException(EOCheque.problemeEtatChequeAnnule);
		}

		cheque.setBordereauRelationship(bordereau);
		//		cheque.addObjectToBothSidesOfRelationshipWithKey(bordereau, "bordereau");

	}

	public EOCheque creerUnChequeVide(EOEditingContext ed, EOBordereau bordereau, EOUtilisateur utilisateur) throws Exception {
		EOCheque nouveauCheque = (EOCheque) Factory.instanceForEntity(ed, EOCheque.ENTITY_NAME);
		ed.insertObject(nouveauCheque);

		nouveauCheque.setBanqAgence("");
		nouveauCheque.setBanqCode("");
		nouveauCheque.setBanqLibelle("");
		nouveauCheque.setCheDateCheque(null);
		nouveauCheque.setCheDateSaisie(null);
		nouveauCheque.setCheEtat(EOCheque.etatValide);
		nouveauCheque.setCheMontant(BIG_DECIMAL_0);
		nouveauCheque.setCheNomTireur("");
		nouveauCheque.setCheNumeroCheque("");
		nouveauCheque.setCheNumeroCompte("");
		nouveauCheque.setChePrenomTireur("");
		nouveauCheque.setRcptCode("");

		nouveauCheque.setBordereauRelationship(bordereau);
		nouveauCheque.setUtilisateurRelationship(utilisateur);

		//		nouveauCheque.addObjectToBothSidesOfRelationshipWithKey(bordereau, "bordereau");
		//		nouveauCheque.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

		if (EOBordereau.BordereauVise.equals(bordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		if (EOBordereau.BordereauAnnule.equals(bordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAnnule);
		}

		return nouveauCheque;
	}

	public void annulerUnCheque(EOEditingContext ed, EOCheque leCheque, EOUtilisateur utilisateur) throws Exception {

		if (EOBordereau.BordereauVise.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		if (EOBordereau.BordereauAViser.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAViser);
		}

		//PB quand on annule car brouillard deja saisi sans pco_num et donc exception lors de l'enregistrement
		// donc suppression definitive du cheque + brouillard au lieu de l'annulation
		//historique pas vraiment interessant dans ce cas
		//        leCheque.setCheEtat(EOCheque.etatAnnule);
		//        leCheque.addObjectToBothSidesOfRelationshipWithKey(utilisateur,"utilisateur");

		for (int i = 0; i < leCheque.chequeBrouillards().count(); i++) {
			EOChequeBrouillard element = (EOChequeBrouillard) leCheque.chequeBrouillards().objectAtIndex(i);
			element.setChequeRelationship(null);
			ed.deleteObject(element);
		}
		leCheque.setBordereauRelationship(null);
		ed.deleteObject(leCheque);

	}

	public void modifierUnCheque(EOEditingContext ed, EOCheque leCheque, EOUtilisateur utilisateur) throws Exception {

		if (EOBordereau.BordereauAViser.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAViser);
		}

		if (EOBordereau.BordereauVise.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		if (EOBordereau.BordereauAnnule.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAnnule);
		}

		if (leCheque.cheEtat().equals(EOCheque.etatAnnule)) {
			throw new FactoryException(EOCheque.problemeEtatChequeAnnule);
		}

		leCheque.setUtilisateurRelationship(utilisateur);
		//		leCheque.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

	}

	public void modifierUnCheque(EOEditingContext ed, EOCheque leCheque, String banqAgence, String banqCode, String banqLibelle, NSTimestamp cheDateCheque, NSTimestamp cheDateCreation, BigDecimal cheMontant, String cheNomTireur, String cheNumeroCheque, String cheNumeroCompte,
			String chePrenomTireur, String rcptCode, EOUtilisateur utilisateur) throws Exception {

		if (EOBordereau.BordereauAViser.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAViser);
		}

		if (EOBordereau.BordereauVise.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		if (EOBordereau.BordereauAnnule.equals(leCheque.bordereau().borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAnnule);
		}

		if (leCheque.cheEtat().equals(EOCheque.etatAnnule)) {
			throw new FactoryException(EOCheque.problemeEtatChequeAnnule);
		}

		leCheque.setUtilisateurRelationship(utilisateur);

		//		leCheque.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

		leCheque.setBanqAgence(banqAgence);
		leCheque.setBanqCode(banqCode);
		leCheque.setBanqLibelle(banqLibelle);
		leCheque.setCheDateCheque(cheDateCheque);
		if (leCheque.cheDateSaisie() == null) {
			leCheque.setCheDateSaisie(cheDateCreation);
		}
		leCheque.setCheEtat(EOCheque.etatValide);
		leCheque.setCheMontant(cheMontant);
		leCheque.setCheNomTireur(cheNomTireur);
		leCheque.setCheNumeroCheque(cheNumeroCheque);
		leCheque.setCheNumeroCompte(cheNumeroCompte);
		leCheque.setChePrenomTireur(chePrenomTireur);
		leCheque.setRcptCode(rcptCode);

	}

}
