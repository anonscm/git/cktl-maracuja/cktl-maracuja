/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 8 juil. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Vector;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.zutil.client.ZDateUtil;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */

public abstract class Factory {
	public static final String SENS_DEBIT = "D";
	public static final String SENS_CREDIT = "C";

	public static final BigDecimal BIG_DECIMAL_0 = new BigDecimal(0);

	private boolean withLogs;

	//    public Factory() {
	//        super();
	//        this.setWithLogs(false);
	//    }

	public Factory(boolean withLog) {
		super();
		this.setWithLogs(withLog);
	}

	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) throws FactoryException {

		EODistributedClassDescription description = (EODistributedClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
		if (description == null) {
			throw new FactoryException("Impossible de recuperer la description de l'entite  \"" + entity + "\" ");
		}
		EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);

		return object;

	}

	public static NSArray fetchArray(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects) {
		NSArray res = fetchArray(ec, entityName, conditionStr, params, sortOrderings, refreshObjects);
		if ((res == null) || (res.count() == 0)) {
			return null;
		}
		return (EOEnterpriseObject) res.objectAtIndex(0);
	}

	public void trace(final String prefix, final NSArray a) {
		for (int i = 0; i < a.count(); i++) {
			trace(prefix + "  " + i + "-->", a.objectAtIndex(i));
		}
	}

	//    public void trace(String m) {
	//        if (withLogs())
	//            System.out.println(m);
	//    }
	//    
	//    public void trace(final String prefix, String m) {
	//        if (withLogs())
	//            System.out.println(m);
	//    }
	//    
	public void trace(final Object obj) {
		trace("", obj);
	}

	public void trace(final String prefix, final Object obj) {
		if (withLogs()) {
			if (obj == null) {
				System.out.println(prefix + "null");
			}
			else {
				if (obj instanceof NSArray) {
					trace(prefix, (NSArray) obj);
				}
				else if (obj instanceof EOEnterpriseObject) {
					trace(prefix, (EOEnterpriseObject) obj);
				}
				else {
					System.out.println(prefix + obj.toString());
				}
			}

		}
	}

	public void trace(final String prefix, final EOEnterpriseObject object) {
		if (object != null) {
			final Iterator iter = object.attributeKeys().vector().iterator();
			while (iter.hasNext()) {
				String obj = (String) iter.next();
				trace(prefix + "  " + obj + "-->", object.valueForKey(obj));
			}

			Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
			while (iter2.hasNext()) {
				String obj = (String) iter2.next();
				trace(prefix + "  " + obj + "-->" + object.valueForKey(obj));
			}
			Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
			while (iter3.hasNext()) {
				String obj = (String) iter3.next();
				if (prefix != null && prefix.length() > 250) {
					trace(prefix + "  " + obj + "-->" + object.valueForKey(obj));
				}
				else {
					trace(prefix + "  " + obj + "-->", object.valueForKey(obj));
				}

			}
		}
	}

	public void setWithLogs(boolean b) {
		this.withLogs = b;
	}

	public boolean withLogs() {
		return withLogs;
	}

	/*
	 * public float computeSumForKey( NSArray eo, String key) {
	 * 
	 * float total = 0; int i = 0; while (i < eo.count()) { total = total + ((Number) ((EOEnterpriseObject) eo.objectAtIndex(i))
	 * .valueForKey(key)).floatValue(); i = i + 1; } return (float) total; }
	 */

	public BigDecimal computeSumForKeyBigDecimal(NSArray eo, String keyBigDecimal) {

		BigDecimal total = new BigDecimal(0);
		int i = 0;
		while (i < eo.count()) {
			total = total.add((BigDecimal) ((EOEnterpriseObject) eo.objectAtIndex(i)).valueForKey(keyBigDecimal));
			i = i + 1;
		}
		return total;

	}

	public static final String sensDebit() {
		return SENS_DEBIT;
	}

	public static final String sensCredit() {
		return SENS_CREDIT;
	}

	public static boolean inferieur(BigDecimal a, BigDecimal b) {
		return (a.compareTo(b) < 0);
	}

	public static boolean inferieurOuEgal(final BigDecimal a, final BigDecimal b) {
		return (a.compareTo(b) <= 0);
	}

	public static boolean superieur(final BigDecimal a, final BigDecimal b) {
		return (a.compareTo(b) > 0);
	}

	public static boolean superieurOuEgal(BigDecimal a, BigDecimal b) {
		return (a.compareTo(b) >= 0);
	}

	public static boolean egal(BigDecimal a, BigDecimal b) {
		return (a.compareTo(b) == 0);
	}

	public static boolean different(BigDecimal a, BigDecimal b) {
		return (a.compareTo(b) != 0);
	}

	/**
	 * @return la date du jour (sans heures minutes etc.)
	 */
	public final NSTimestamp getDateJour() {
		return new NSTimestamp(ZDateUtil.getTodayAsCalendar().getTime());
	}

	/**
	 * @return la date actuelle
	 */
	public static final NSTimestamp getNow() {
		return ZDateUtil.now();
	}

	/**
	 * Calcule la somme de toutes les valeurs contenues dans un champ des EOEnterpriseObjects contenues dans le tableau.
	 * 
	 * @param array Tableau d'EOEnterpriseObjects
	 * @param keyName Nom de l'attribut qui contient les valeurs à sommer (le champ doit être un BigDecimal)
	 * @return
	 */
	public static final BigDecimal calcSommeOfBigDecimals(final NSArray array, final String keyName) {
		return calcSommeOfBigDecimals(array.vector(), keyName);
	}

	public static final BigDecimal calcSommeOfBigDecimals(final Vector array, final String keyName) {
		BigDecimal res = new BigDecimal(0).setScale(2);
		Iterator iter = array.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal) element.valueForKey(keyName));
		}
		return res;
	}

	/**
	 * Méthode qui <b>tente</b> de contourner le bug EOF qui se produit lors d'un saveChanges avec l'erreur "reentered responseToMessage()".<br>
	 * <b>Il faut appeler cette méthode avant de créer un descendant d'EOCustomObject, donc bien avant le saveChanges()</b><br>
	 * Le principe est d'appeler la méthode EOClassDescription.classDescriptionForEntityName("A") pour chaque relation de l'objet qu'on va créer. Il
	 * faut appeler cette méthode avant de créer un objet. Par exemple dans le cas d'un objet Facture qui a des objets Ligne, appeler
	 * EOClassDescription.classDescriptionForEntityName("Facture") avant de créer un objet Ligne. Répéter l'opération pour toutes les relations de
	 * l'objet.
	 * 
	 * @param list Liste de String identifiant une entité du modèle.
	 * @see http://www.omnigroup.com/mailman/archive/webobjects-dev/2002-May/023698.html
	 */
	public static final void fixWoBug_responseToMessage(final String[] list) {
		for (int i = 0; i < list.length; i++) {
			EOClassDescription.classDescriptionForEntityName(list[i]);
		}
	}

}
