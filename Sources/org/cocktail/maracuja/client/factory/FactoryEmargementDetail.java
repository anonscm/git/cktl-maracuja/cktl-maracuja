/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 9 oct. 2004
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.EcritureDetailException;
import org.cocktail.maracuja.client.exception.EmargementException;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOEmargementDetail;
import org.cocktail.maracuja.client.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */
public class FactoryEmargementDetail extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryEmargementDetail(boolean withLog) {
		super(withLog);
		// 
	}

	/**
	 * Permet d emarger l ecritureDetail source par l ecritureDetail destination <br>
	 * du montant emaMontant
	 * 
	 * @param ed
	 * @param emaMontant
	 * @param source
	 * @param destination
	 * @param emargement
	 * @param exercice
	 * @return
	 * @throws EcritureDetailException
	 */
	public EOEmargementDetail creerEmargementDetail(EOEditingContext ed, BigDecimal emaMontant, EOEcritureDetail source, EOEcritureDetail destination, EOEmargement emargement, EOExercice exercice) throws EmargementException {

		EOEmargementDetail newEOEmargementDetail = (EOEmargementDetail) Factory.instanceForEntity(ed, EOEmargementDetail.ENTITY_NAME);
		ed.insertObject(newEOEmargementDetail);
		newEOEmargementDetail.setEmdMontant(emaMontant.abs());
		newEOEmargementDetail.setSourceRelationship(source);
		newEOEmargementDetail.setDestinationRelationship(destination);
		newEOEmargementDetail.setEmargementRelationship(emargement);
		newEOEmargementDetail.setExerciceRelationship(exercice);
		if (Factory.inferieur(source.ecdResteEmarger(), emaMontant.abs())) {
			throw new EmargementException(EmargementException.resteInfMontantAEmarger + " (" + source.ecdResteEmarger() + " / " + emaMontant.abs() + ")");
		}
		trace("----------");
		trace("source.ecdResteEmarger() : " + source.ecdResteEmarger());
		trace("emaMontant.abs().negate() : " + emaMontant.abs().negate());
		source.setEcdResteEmarger(source.ecdResteEmarger().add(emaMontant.abs().negate()));
		trace("source.ecdResteEmarger() : " + source.ecdResteEmarger());
		if (Factory.inferieur(destination.ecdResteEmarger(), emaMontant.abs())) {
			throw new EmargementException(EmargementException.resteInfMontantAEmarger + " (" + destination.ecdResteEmarger() + " / " + emaMontant.abs() + ")");
		}
		trace("destination.ecdResteEmarger() : " + destination.ecdResteEmarger());
		trace("emaMontant.abs().negate() : " + emaMontant.abs().negate());
		destination.setEcdResteEmarger(destination.ecdResteEmarger().add(emaMontant.abs().negate()));
		trace("destination.ecdResteEmarger() : " + destination.ecdResteEmarger());
		trace("----------");
		return newEOEmargementDetail;

	}

	public EOEmargementDetail creerEmargementDetailSourceDestination(EOEditingContext ed, BigDecimal emaMontant, EOEcritureDetail source, EOEmargement emargement, EOExercice exercice) throws EmargementException {

		this.trace("");
		this.trace("creerEmargementDetail");
		this.trace("emaMontant  = " + emaMontant);
		this.trace("source  = " + source);
		this.trace("emargement  = " + emargement);
		this.trace("exercice  = " + exercice);
		this.trace("-----------------------------");
		this.trace("");

		EOEmargementDetail newEOEmargementDetail = (EOEmargementDetail) Factory.instanceForEntity(ed, EOEmargementDetail.ENTITY_NAME);
		ed.insertObject(newEOEmargementDetail);

		newEOEmargementDetail.setEmdMontant(emaMontant);

		newEOEmargementDetail.setSourceRelationship(source);
		newEOEmargementDetail.setDestinationRelationship(source);
		newEOEmargementDetail.setEmargementRelationship(emargement);
		newEOEmargementDetail.setExerciceRelationship(exercice);
		// modification du reste a emarger de la source et destination : c est
		// le meme objet
		source.setEcdResteEmarger(BIG_DECIMAL_0);

		return newEOEmargementDetail;

	}
}
