/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory;

import org.cocktail.maracuja.client.exception.BordereauRejetException;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauInfo;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * @author frivalla To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FactoryBordereauCheque extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryBordereauCheque(boolean withLog) {
		super(withLog);
	}

	public EOBordereau creerBordereauCheque(EOEditingContext ed, Integer borNum, EOExercice exercice, EOGestion gestion, EOUtilisateur utilisateur) throws Exception {

		final EOBordereau nouveauBordereau = (EOBordereau) Factory.instanceForEntity(ed, EOBordereau.ENTITY_NAME);

		ed.insertObject(nouveauBordereau);

		nouveauBordereau.setBorEtat(EOBordereau.BordereauValide);
		nouveauBordereau.setBorNum(borNum);
		nouveauBordereau.setBorDateCreation(getDateJour());

		nouveauBordereau.setExerciceRelationship(exercice);
		nouveauBordereau.setGestionRelationship(gestion);
		nouveauBordereau.setTypeBordereauRelationship(FinderVisa.leTypeBordereauBTCC(ed));
		nouveauBordereau.setUtilisateurRelationship(utilisateur);

		try {
			nouveauBordereau.validateObjectMetier();
			return nouveauBordereau;
		} catch (BordereauRejetException e1) {
			e1.printStackTrace();
			System.out.println(nouveauBordereau);
			return null;
		}

	}

	public void annulerBordereauCheque(EOEditingContext ed, EOBordereau nouveauBordereau) throws Exception {

		if (EOBordereau.BordereauVise.equals(nouveauBordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		nouveauBordereau.setBorEtat(EOBordereau.BordereauAnnule);
	}

	// ////////////////////
	// / Ajouts Rod

	// /**
	// *
	// * @param editingContext
	// * @param currentExercice
	// * @param utilisateur
	// * @return
	// */
	// public EOBordereau creerBordereauChequeVide(EOEditingContext ed,
	// EOExercice exercice, EOUtilisateur utilisateur) {
	// EOBordereau nouveauBordereau = (EOBordereau) Factory.instanceForEntity(
	// ed,
	// EOBordereau.ENTITY_NAME);
	//
	// ed.insertObject(nouveauBordereau);
	//
	// nouveauBordereau.setBorEtat(EOBordereau.BordereauValide);
	// nouveauBordereau.setBorNum(new Integer(0));
	//
	// nouveauBordereau.addObjectToBothSidesOfRelationshipWithKey(
	// exercice,
	// "exercice");
	// nouveauBordereau.addObjectToBothSidesOfRelationshipWithKey(
	// FinderVisa.leTypeBordereauBTCC(ed),
	// "typeBordereau");
	// nouveauBordereau.addObjectToBothSidesOfRelationshipWithKey(
	// utilisateur,
	// "utilisateur");
	//
	// return nouveauBordereau;
	// }
	//

	public void affecteBordereauInfo(EOBordereau bordereau, EOBordereauInfo bordereauInfo) {
		//		bordereau.addObjectToBothSidesOfRelationshipWithKey(bordereauInfo, "bordereauInfo");
		bordereau.setBordereauInfoRelationship(bordereauInfo);
	}

	public void affecteGestion(EOBordereau bordereau, EOGestion gestion) {
		bordereau.setGestionRelationship(gestion);
		//		bordereau.addObjectToBothSidesOfRelationshipWithKey(gestion, "gestion");
	}

	public void affecterPlanComptableVisa(EOBordereau bordereau, EOPlanComptable planComptable) throws FactoryException {
		if (bordereau.bordereauInfo() == null) {
			throw new FactoryException("BordereauInfo null");
		}
		//		bordereau.bordereauInfo().addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptableVisa");

		bordereau.bordereauInfo().setPlanComptableVisaRelationship(planComptable);
	}

	public EOBordereauInfo creerBordereauInfoVide(EOEditingContext ed) {
		final EOBordereauInfo nouveauBordereauInfo = (EOBordereauInfo) Factory.instanceForEntity(ed, EOBordereauInfo.ENTITY_NAME);
		ed.insertObject(nouveauBordereauInfo);
		return nouveauBordereauInfo;
	}

}
