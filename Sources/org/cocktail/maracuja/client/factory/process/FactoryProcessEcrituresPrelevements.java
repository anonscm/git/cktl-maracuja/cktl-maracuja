/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryPrelevementDetailEcriture;
import org.cocktail.maracuja.client.factory.FactoryTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EOPrelevementDetailEcr;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryProcessEcrituresPrelevements extends FactoryProcess {

	private final FactoryEcritureDetail maFactoryEcritureDetail;
	private final FactoryProcessJournalEcriture maFactoryEcriture;
	private final FactoryTitreDetailEcriture maFactoryTitreDetailEcriture;
	private final FactoryPrelevementDetailEcriture maFactoryPrelevementDetailEcriture;

	public FactoryProcessEcrituresPrelevements(boolean withLog, NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
		maFactoryEcritureDetail = new FactoryEcritureDetail(withLog);
		maFactoryEcriture = new FactoryProcessJournalEcriture(withLog, getDateJourComptable());
		maFactoryTitreDetailEcriture = new FactoryTitreDetailEcriture(withLog);
		maFactoryPrelevementDetailEcriture = new FactoryPrelevementDetailEcriture(withLog);
	}

	/**
	 * Cette methode permet de generer les ecritures lies a prelevment lors de la creation du fichier de prelements on cree une ecriture par
	 * prelements un credit du compte de classe 4 du moderecouvrement.plancovisa et un debit du compte de classe 4 du moderecouvrement.plancopaiement
	 * tous ceci pour le montant du prelevement. Le prelevement peut ou non etre relie à un titre.
	 * 
	 * @param ed editingContext de travail
	 * @param lesEOPrelevements les prelevements en cours de traitemens
	 * @param setTypeJournal le type de journal comptable
	 * @param setTypeOperation le type d operation
	 * @param utilisateur l utyilisateur qui traite les prelevements
	 * @param modeRecouvrement Le mode de recouvrement par defaut (ne sert que pour les prélèvements non reliés à des recettes)
	 * @return le tableau des ecritures generŽes et a numerotees
	 * @throws Exception
	 */
	public NSMutableArray genererEcrituresPrelevementsFichier(EOEditingContext ed, NSArray lesEOPrelevements, EOTypeJournal setTypeJournal, EOTypeOperation setTypeOperation, EOModeRecouvrement modeRecouvrement, EOUtilisateur utilisateur, EOExercice exerciceTresorerie, EOComptabilite compta)
			throws Exception {
		NSMutableArray mesEcritures = new NSMutableArray();
		final EOGestion gestionAgence = compta.gestion();

		for (int i = 0; i < lesEOPrelevements.count(); i++) {
			EOPrelevement prelevement = (EOPrelevement) lesEOPrelevements.objectAtIndex(i);
			//Si le prelevement est relie a un titre
			if (prelevement.echeancier().hasTitreAssocie()) {
				final EOTitre leTitre = prelevement.echeancier().recette().titre();
				final EORecette recette = prelevement.echeancier().recette();
				//				final boolean isSacd = FinderGestion.gestionIsSacd(leTitre.gestion(), leTitre.exercice());
				final boolean isSacd = leTitre.gestion().isSacd(leTitre.exercice());
				//	            final EOGestion compta = (isSacd ? leTitre.gestion() : leTitre.gestion().comptabilite().gestion());
				final EOPlanComptable pcoVisa = recette.modeRecouvrement().getPlanCoVisaSelonExercice(exerciceTresorerie).planComptable();
				final EOPlanComptable pcoPaiement = recette.modeRecouvrement().getPlanCoPaiementSelonExercice(exerciceTresorerie).planComptable();

				//				.getPlanCoPaiementSelonExercice(exercice)
				//				 si moderecouvrement.exercice <> exercice tresorerie, prendre le compte de BE
				//				sinon si compte pas present sur exercice tresorerie, erreur

				//si c'est un sacd, on fait les 2 ecritures sacd, sinon on en fait une
				if (!isSacd) {
					//******
					// C agence 4675
					// D agence 5xxx
					//correction Rod : on prend l'exercice de tresorerie pour les ecr de prelevement a la place de l'exercice du titre
					EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), "Tit. Num . " + leTitre.titNumero().toString() + " Bord. Num. " + leTitre.bordereau().borNum().toString() +
							" du " + leTitre.bordereau().gestion().gesCode(), new Integer(0), null, leTitre
							.bordereau().gestion().comptabilite(), exerciceTresorerie, leTitre.origine(), setTypeJournal, setTypeOperation, utilisateur);
					mesEcritures.addObject(newEOEcriture);
					this.trace("newEOEcriture " + newEOEcriture);

					// creation du detail en CREDIT du compte du visa (mode recouvrement)	
					EOEcritureDetail newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, prelevement.prelevMontant(), EOEcritureDetail.SENS_CREDIT, newEOEcriture, gestionAgence, pcoVisa, exerciceTresorerie);
					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);

					// creation du detail en DEBIT du compte de paiement (mode recouvrement)
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, prelevement.prelevMontant(), EOEcritureDetail.SENS_DEBIT, newEOEcriture, gestionAgence, pcoPaiement, exerciceTresorerie);

					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);
				}
				else {
					//ON EST SUR UN SACD !!!!
					// C SACD 4675
					// D SACD 185
					//   +
					// C Agence 185sacd
					// D agence 5xxx

					//	final EOPlanComptable planco185 = FinderJournalEcriture.planComptable185(ed);
					final EOPlanComptable planco185CtpSacd = leTitre.gestion().getPlanco185CtpSacd(exerciceTresorerie);
					//					final EOPlanComptable planco185sacd = FinderGestion.getPlanco185ForGestionAndExercice(leTitre.gestion(), exerciceTresorerie);
					final EOPlanComptable planco185sacd = leTitre.gestion().getGestionExercice(exerciceTresorerie).planComptable185();
					final EOGestion gestionSacd = leTitre.bordereau().gestion();

					//correction Rod : on prend l'exercice de tresorerie pour les ecr de prelevement a la place de l'exercice du titre
					EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), "Tit. Num . " + leTitre.titNumero().toString() + " Bord. Num. " + leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion().gesCode(), new Integer(0), " automatique",
							leTitre.bordereau().gestion().comptabilite(), exerciceTresorerie, leTitre.origine(), setTypeJournal, setTypeOperation, utilisateur);
					mesEcritures.addObject(newEOEcriture);
					this.trace("newEOEcriture " + newEOEcriture);

					// creation du detail en CREDIT du compte du visa (mode recouvrement)	
					EOEcritureDetail newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, ((EOPrelevement) lesEOPrelevements.objectAtIndex(i)).prelevMontant(), EOEcritureDetail.SENS_CREDIT, newEOEcriture, gestionSacd, pcoVisa, exerciceTresorerie);
					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);

					// creation du detail en DEBIT du compte 185 sur le sacd
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, ((EOPrelevement) lesEOPrelevements.objectAtIndex(i)).prelevMontant(), EOEcritureDetail.SENS_DEBIT, newEOEcriture, gestionSacd, planco185CtpSacd, exerciceTresorerie);

					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);

					////////////////////

					newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), "Tit. Num . " + leTitre.titNumero().toString() + " Bord. Num. " + leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion().gesCode(), new Integer(0), null, leTitre.bordereau()
							.gestion().comptabilite(), exerciceTresorerie, leTitre.origine(), setTypeJournal, setTypeOperation, utilisateur);
					mesEcritures.addObject(newEOEcriture);
					this.trace("newEOEcriture " + newEOEcriture);

					// creation du detail en CREDIT du compte du visa (mode recouvrement)	
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, ((EOPrelevement) lesEOPrelevements.objectAtIndex(i)).prelevMontant(), EOEcritureDetail.SENS_CREDIT, newEOEcriture, gestionAgence, planco185sacd, exerciceTresorerie);
					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);

					// creation du detail en DEBIT du compte de paiement (mode recouvrement)
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, ((EOPrelevement) lesEOPrelevements.objectAtIndex(i)).prelevMontant(), EOEcritureDetail.SENS_DEBIT, newEOEcriture, gestionAgence, pcoPaiement, exerciceTresorerie);

					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);
				}

			}
			else {
				//il n'y a pas de titre relié. On ne traite pas le cas des SACD pour l'instant...
				String ecrLibelle = (prelevement.echeancier().bordereau() != null ? prelevement.echeancier().libelle() : prelevement.prelevCommentaire());

				EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), ecrLibelle, new Integer(0), null, compta, exerciceTresorerie, null, setTypeJournal, setTypeOperation, utilisateur);
				mesEcritures.addObject(newEOEcriture);
				this.trace("newEOEcriture " + newEOEcriture);
				final EOPlanComptable pcoVisa = modeRecouvrement.getPlanCoVisaSelonExercice(exerciceTresorerie).planComptable();
				final EOPlanComptable pcoPaiement = modeRecouvrement.getPlanCoPaiementSelonExercice(exerciceTresorerie).planComptable();

				// creation du detail en CREDIT du compte du visa (mode recouvrement)	
				EOEcritureDetail newEOEcritureDetail = creerEcritureDetailPrelev(ed, prelevement, EOEcritureDetail.SENS_CREDIT, newEOEcriture, gestionAgence, pcoVisa, exerciceTresorerie);
				// on cree le PrelevementDetailEcriture
				maFactoryPrelevementDetailEcriture.creerPrelevementDetailEcriture(ed, newEOEcritureDetail, prelevement);

				// creation du detail en DEBIT du compte de paiement (mode recouvrement)
				newEOEcritureDetail = creerEcritureDetailPrelev(ed, prelevement, EOEcritureDetail.SENS_DEBIT, newEOEcriture, gestionAgence, pcoPaiement, exerciceTresorerie);
				maFactoryPrelevementDetailEcriture.creerPrelevementDetailEcriture(ed, newEOEcritureDetail, prelevement);

			}

		}
		return mesEcritures;

	}

	/**
	 * Cette methode permet de traiter la phase de releve bancaire: les prelevements sont en 2 groupes (1 ecriture par prelevevement ) : 1 ceux percus
	 * donc sur le releve 2 ceux non percus donc ne figurant pas sur le releve (attention qd le montant du fichier de prelevment est different du
	 * montant du releve , la banque doit fournir le detail sinon c est un traitement global N prevelements sur le fichier 1 ligne sur le releve)
	 * 
	 * @param ed editingContext de travail
	 * @param lesEOPrelevementsPercu tableau des prelevements sur le releve
	 * @param lesEOPrelevementsNonPercu tableau des prelevements non present sur le releve
	 * @param setTypeJournal type de journal comptable
	 * @param setTypeOperation type d opertation
	 * @param utilisateur utilisateur qui traite le releve
	 * @param comptePercu compte pour gere les prelevements percus P5 : 515
	 * @param compteNonPercu compte pour gere les prelevements non percus P5 : 5116
	 * @return renvoie les ecritures generes
	 * @throws Exception
	 */
	public NSMutableArray genererEcrituresPrelevementsReleveBancaire(EOEditingContext ed, NSArray lesEOPrelevementsPercu, NSArray lesEOPrelevementsNonPercu, EOTypeJournal setTypeJournal, EOTypeOperation setTypeOperation, EOUtilisateur utilisateur, EOPlanComptable comptePercu,
			EOPlanComptable compteNonPercu, EOModeRecouvrement modeRecouvrement, EOExercice exerciceTresorerie, EOComptabilite comptabilite

			) throws Exception {
		NSMutableArray mesEcritures = new NSMutableArray();
		EOEcritureDetail newEOEcritureDetail = null;
		EOEcriture newEOEcriture = null;

		//		les prelevements percus
		if (lesEOPrelevementsPercu != null) {
			for (int i = 0; i < lesEOPrelevementsPercu.count(); i++) {
				EOPrelevement prelevement = (EOPrelevement) lesEOPrelevementsPercu.objectAtIndex(i);
				if (prelevement.echeancier().hasTitreAssocie()) {
					final EOTitre leTitre = prelevement.echeancier().recette().titre();
					final EORecette recette = prelevement.echeancier().recette();

					modeRecouvrement = leTitre.modeRecouvrement();
					//				creation de l ecriture
					newEOEcriture = creerEcriturePrelev(ed, prelevement, comptabilite, exerciceTresorerie, setTypeJournal, setTypeOperation, utilisateur);
					mesEcritures.addObject(newEOEcriture);

					this.trace("newEOEcriture " + newEOEcriture);
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, prelevement.prelevMontant(), EOEcritureDetail.SENS_CREDIT, newEOEcriture, comptabilite.gestion(), modeRecouvrement.planComptablePaiement(), exerciceTresorerie);

					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);

					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, prelevement.prelevMontant(), EOEcritureDetail.SENS_DEBIT, newEOEcriture, comptabilite.gestion(), comptePercu, exerciceTresorerie);

					// on cree le EOTitreDetailEcriture
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);
				}

				else {
					//Pas de titre associé au prélèvement
					//On recupere le debit non emarge associe au prelevement (genere lors de l'emission du fichier)

					NSArray res = EOQualifier.filteredArrayWithQualifier(prelevement.prelevementDetailEcrs(), new EOAndQualifier(new NSArray(new Object[] {
							EOPrelevementDetailEcr.QUAL_DEBIT, EOPrelevementDetailEcr.QUAL_RESTE_EMARGER_NON_NUL
					})));
					if (res.count() == 1) {
						EOEcritureDetail debitPrecedent = ((EOPrelevementDetailEcr) res.objectAtIndex(0)).ecritureDetail();
						newEOEcriture = creerEcriturePrelev(ed, prelevement, comptabilite, exerciceTresorerie, setTypeJournal, setTypeOperation, utilisateur);
						mesEcritures.addObject(newEOEcriture);
						//this.trace("newEOEcriture " + newEOEcriture);
						// creation du detail en CREDIT	
						newEOEcritureDetail = creerEcritureDetailPrelev(ed, prelevement, EOEcritureDetail.SENS_CREDIT, newEOEcriture, comptabilite.gestion(), debitPrecedent.planComptable(), exerciceTresorerie);
						maFactoryPrelevementDetailEcriture.creerPrelevementDetailEcriture(ed, newEOEcritureDetail, prelevement);

						newEOEcritureDetail = creerEcritureDetailPrelev(ed, prelevement, EOEcritureDetail.SENS_DEBIT, newEOEcriture, comptabilite.gestion(), comptePercu, exerciceTresorerie);
						maFactoryPrelevementDetailEcriture.creerPrelevementDetailEcriture(ed, newEOEcritureDetail, prelevement);
					}
				}

			}
		}

		// les prelevements non percu
		if (lesEOPrelevementsNonPercu != null) {
			for (int i = 0; i < lesEOPrelevementsNonPercu.count(); i++) {
				EOPrelevement prelevement = (EOPrelevement) lesEOPrelevementsNonPercu.objectAtIndex(i);
				if (prelevement.echeancier().hasTitreAssocie()) {
					final EOTitre leTitre = prelevement.echeancier().recette().titre();
					final EORecette recette = prelevement.echeancier().recette();
					modeRecouvrement = leTitre.modeRecouvrement();
					newEOEcriture = creerEcriturePrelev(ed, prelevement, comptabilite, exerciceTresorerie, setTypeJournal, setTypeOperation, utilisateur);
					mesEcritures.addObject(newEOEcriture);

					//this.trace("newEOEcriture " + newEOEcriture);

					// creation du detail en CREDIT 	
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, prelevement.prelevMontant(), EOEcritureDetail.SENS_CREDIT, newEOEcriture, comptabilite.gestion(), modeRecouvrement.planComptablePaiement(), exerciceTresorerie);
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);

					// creation du detail en DEBIT 
					newEOEcritureDetail = creerEcritureDetailPrelevForTitre(ed, leTitre, prelevement.prelevMontant(), EOEcritureDetail.SENS_DEBIT, newEOEcriture, comptabilite.gestion(), compteNonPercu, exerciceTresorerie);
					maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOriginePRELEVEMENT, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), recette);
				}
				else {
					NSArray res = EOQualifier.filteredArrayWithQualifier(prelevement.prelevementDetailEcrs(), new EOAndQualifier(new NSArray(new Object[] {
							EOPrelevementDetailEcr.QUAL_DEBIT, EOPrelevementDetailEcr.QUAL_RESTE_EMARGER_NON_NUL
					})));
					if (res.count() == 1) {
						EOEcritureDetail debitPrecedent = ((EOPrelevementDetailEcr) res.objectAtIndex(0)).ecritureDetail();
						newEOEcriture = creerEcriturePrelev(ed, prelevement, comptabilite, exerciceTresorerie, setTypeJournal, setTypeOperation, utilisateur);
						mesEcritures.addObject(newEOEcriture);
						//this.trace("newEOEcriture " + newEOEcriture);
						// creation du detail en CREDIT	
						newEOEcritureDetail = creerEcritureDetailPrelev(ed, prelevement, EOEcritureDetail.SENS_CREDIT, newEOEcriture, comptabilite.gestion(), debitPrecedent.planComptable(), exerciceTresorerie);
						maFactoryPrelevementDetailEcriture.creerPrelevementDetailEcriture(ed, newEOEcritureDetail, prelevement);

						newEOEcritureDetail = creerEcritureDetailPrelev(ed, prelevement, EOEcritureDetail.SENS_DEBIT, newEOEcriture, comptabilite.gestion(), compteNonPercu, exerciceTresorerie);
						maFactoryPrelevementDetailEcriture.creerPrelevementDetailEcriture(ed, newEOEcritureDetail, prelevement);
					}
				}
			}
		}
		return mesEcritures;

	}

	private EOEcritureDetail creerEcritureDetailPrelevForTitre(EOEditingContext ed, EOTitre leTitre, BigDecimal montant, String sens, EOEcriture ecriture, EOGestion gestion, EOPlanComptable compte, EOExercice exerciceTresorerie) {
		return maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), (leTitre.fournisseur() != null ? leTitre.fournisseur().adrNom() : "") + " Tit. " + leTitre.titNumero().toString() + " Bord. " + leTitre.bordereau().borNum().toString() + " du "
				+ leTitre.bordereau().gestion().gesCode() + " " + leTitre.exercice().exeExercice() + " \"" + leTitre.titLibelle() + "\"", montant, null, montant, null, sens, ecriture, exerciceTresorerie, gestion, compte);
	}

	private EOEcritureDetail creerEcritureDetailPrelev(EOEditingContext ed, EOPrelevement prelev, String sens, EOEcriture ecriture, EOGestion gestion, EOPlanComptable compte, EOExercice exerciceTresorerie) {

		String ecdLibelle = prelev.echeancier().libelle() + " - Prélèvement " + prelev.prelevIndex().intValue() + "/" + prelev.echeancier().nombrePrelevements().intValue();
		if (prelev.echeancier().bordereau() != null) {
			ecdLibelle += " - Bord. " + prelev.echeancier().bordereau().typeBordereau().tboType() + " num. " + prelev.echeancier().bordereau().borNum().intValue() + " du " + prelev.echeancier().bordereau().gestion().gesCode();
		}

		return maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), ecdLibelle, prelev.prelevMontant(), null, prelev.prelevMontant(), null, sens, ecriture, exerciceTresorerie, gestion, compte);
	}

	private EOEcriture creerEcriturePrelev(EOEditingContext ed, EOPrelevement prelevement, EOComptabilite compta, EOExercice exerciceTresorerie, EOTypeJournal setTypeJournal, EOTypeOperation setTypeOperation, EOUtilisateur utilisateur) {
		String ecrLibelle = null;
		EOOrigine origine = null;

		if (prelevement.echeancier().hasTitreAssocie()) {
			EOTitre leTitre = prelevement.echeancier().recette().titre();
			ecrLibelle = "Tit. Num . " + leTitre.titNumero().toString() + " Bord. Num. " + leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion().gesCode();
			origine = leTitre.origine();
		}
		else {
			ecrLibelle = (prelevement.echeancier().bordereau() != null ? prelevement.echeancier().libelle() : prelevement.prelevCommentaire());
		}
		EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), ecrLibelle, new Integer(0), null, compta, exerciceTresorerie, origine, setTypeJournal, setTypeOperation, utilisateur);
		return newEOEcriture;
	}

}
