/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.foundation.NSTimestamp;


public abstract class FactoryProcessPrelevementFichiers extends FactoryProcess {

    /**
     * Formats des champs
     */
    protected final static NumberFormat FormatMontant = new DecimalFormat("000000000000");
    protected final static NumberFormat FormatNumeroSequentiel = new DecimalFormat("000000");
    protected final static DateFormat FormatDate = new SimpleDateFormat("ddMMyy");
  
    
    
//    protected NSArray prelevements;
//    private File file;
    
    private int lengthOfTask;
    private int current;
    private String message;
    
    private int nbCar;
    
    
    
    public FactoryProcessPrelevementFichiers(final boolean withLog, final NSTimestamp dateJourComptable) {
        super(withLog, dateJourComptable);        
    }
    
    
    
    
//    
//    
//    
//    
//    public FactoryProcessPrelevementFichiers(final boolean withLog, final NSTimestamp laDate, NSArray prelevements, final File file) {
//        super(withLog, laDate);
//        this.prelevements = prelevements;
////        this.file = file;
//        
//        this.lengthOfTask = NB_CAR_PAR_ENTREE + NB_CAR_PAR_ENTREE*prelevements.count() + NB_CAR_PAR_ENTREE;
//        this.current = 0;
//    }
    public void cancel() {
        // interdit
    }
    public int getCurrent() {
        return this.current;
    }
    public int getLengthOfTask() {
        return this.lengthOfTask;
    }
    public String getStatusMessage() {
        return this.message;
    }
    
    public boolean isDone() {
        return this.current >= this.lengthOfTask;
    }
    
    /**
     * 
     * @param string
     */
    private void write(final PrintWriter writer, final String string) {
        nbCar += string.length();
        writer.print(string);
        
        this.current = this.nbCar;
    }
    
    private String formattedMontant(final BigDecimal montant) {
        String temp = FormatMontant.format(montant);
        StringBuffer buf = new StringBuffer(temp.substring(0, temp.indexOf(",")));
        buf.append(temp.substring(temp.indexOf(",") + 1));
        return buf.toString();
    }
    
    /**
     * Ajoute des caracteres a la suite d'une String (ou tronque) pour lui faire atteindre la taille specifiee.
     * @param initialString String a etendre (ou tronquer)
     * @param length Nombre de caracteres voulu
     * @param charToPropagate Caractere a utilise pour etendre la taille de la String
     * @return La nouvelle String
     */
    private String formattedString(final String initialString, final int length, final char charToPropagate) {
        if (length < initialString.length())
            return initialString.substring(0, length);
        
        StringBuffer buf = new StringBuffer(initialString);
        int i=initialString.length();
        
        while (i < length) {
            buf.append(charToPropagate);
            i++;
        }
        //System.out.println(initialString+" --> ["+buf.toString()+"] ("+buf.toString().length()+")");
        return buf.toString();
    }
    /**
     * Ajoute des caracteres 'espaces' a la suite d'une String (ou tronque) pour lui faire atteindre la taille specifiee.
     * @param initialString String a etendre (ou tronquer)
     * @param length Nombre de caracteres voulu
     * @return La nouvelle String
     */
    private String formattedString(final String initialString, final int length) {
        return formattedString(initialString, length, ' ');
    }
    /**
     * Fabrique une String composee du nombre specifie de caracteres specifie.
     * @param length Nombre de caracteres voulu
     * @param charToPropagate Caractere qui composera la String
     * @return La String fabriquee
     */
    private String charactersString(final int length, final char charToPropagate) {
        return formattedString("", length, charToPropagate);
    }
    /**
     * Fabrique une String composee du nombre specifie de caracteres 'espace'.
     * @param length Nombre de caracteres voulu
     * @param charToPropagate Caractere qui composera la String
     * @return La String fabriquee
     */ 
    private String spacesString(final int length) {
        return formattedString("", length, ' ');
    }
    
    /**
     * Ajoute  x jours à une date.
     * @param aDate
     * @param aDay
     * @return
     */
    public final Date addD(Date aDate, int aDay) {
        final GregorianCalendar myCalendar = new GregorianCalendar();
        myCalendar.setTime(aDate);
        myCalendar.add(Calendar.DAY_OF_MONTH, aDay);
        return myCalendar.getTime();
    }    
    
    public String chaineSansCaracteresSpeciaux(String chaine) {
//        String retour = chaine.toLowerCase();
//
//        retour = retour.replaceAll("é","e");
//        retour = retour.replaceAll("è","e");
//        retour = retour.replaceAll("ê","e");
//        retour = retour.replaceAll("ë","e");
//        
//        retour = retour.replaceAll("à","a");
//        retour = retour.replaceAll("â","a");
//        
//        retour = retour.replaceAll("î","i");
//        retour = retour.replaceAll("ï","i");
//        
//        retour = retour.replaceAll("ô","o");
//        retour = retour.replaceAll("ö","o");
//        
//        retour = retour.replaceAll("ü","u");
//        retour = retour.replaceAll("û","u");
//        
//        
//        retour = retour.replaceAll("\n", " ");
//        retour = retour.replaceAll("\r", "");
//        retour = retour.replaceAll("\t", "");
//
//        return retour.toUpperCase();
    	return ZStringUtil.chaineSansCaracteresSpeciauxUpper(chaine);
    }
    
//    public String replace(String s, String what, String byWhat) {
//        String emptyString = "";
//        StringBuffer sb;
//        int i;
//
//        //     if ((s == null) || (what == null)) return s;
//        sb = new StringBuffer();
//        if (byWhat == null) byWhat = emptyString;
//        do {
//          i = s.indexOf(what);
//          if (i >= 0) {
//            sb.append(s.substring(0, i));
//            sb.append(byWhat);
//            s = s.substring(i+what.length());
//          }
//        } while(i != -1);
//        sb.append(s);
//        return sb.toString();
//      }    
    
    
    
    
}
