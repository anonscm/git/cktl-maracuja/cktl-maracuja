/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 21 sept. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory.process;

import java.io.PrintWriter;
import java.math.BigDecimal;

import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EOPrelevementParamBdf;
import org.cocktail.maracuja.client.metier.EORib;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author frivalla s
 */
public class FactoryProcessPrelevementFichiersBdf extends FactoryProcessPrelevementFichiers {

	public final static int NB_CAR_PAR_ENTREE = 240;
	public static final int LONGUEUR_LIGNE_BDF = NB_CAR_PAR_ENTREE;

	//    private static final int NB_JOURS_NORMAL=4;
	//    private static final int NB_JOURS_ACCELERE=2;
	public static final String CODE_VITESSE_NORMAL = "80";
	public static final String CODE_VITESSE_ACCELERE = "85";
	private static final String C2_CODE_ETABLISSEMENT_EMETTEUR = "30001"; //toujours ca
	private static final char SPACE = ' '; //
	private static final String C11_CODE_MONNAIE = " "; //
	private static final String D10_2_CODE_FLUX = "54"; //

	private int lengthOfTask;
	private int current;
	private String message;

	private int nbCar;

	public FactoryProcessPrelevementFichiersBdf(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public String genereContenuFichier(final NSTimestamp laDateEchange, final NSTimestamp dateReglement, final boolean accelere, NSArray prelevements, EOPrelevementParamBdf leVirementParamBdf) throws Exception {

		final String codeVitesse = (accelere ? CODE_VITESSE_ACCELERE : CODE_VITESSE_NORMAL);

		final String c3 = leVirementParamBdf.ppbC3();
		final String c41 = leVirementParamBdf.ppbC41();
		final String c42 = leVirementParamBdf.ppbC42();
		final String c5 = leVirementParamBdf.ppbC5();
		final String c6 = leVirementParamBdf.ppbC6();

		if (c3.length() != 5) {
			throw new Exception("Nombre de caractères invalide pour le parametre c3");
		}
		if (c41.length() != 4) {
			throw new Exception("Nombre de caractères invalide pour le parametre c41");
		}
		if (c42.length() != 7) {
			throw new Exception("Nombre de caractères invalide pour le parametre c42");
		}
		if (c5.length() > 24) {
			throw new Exception("Nombre de caractères invalide pour le parametre c5");
		}
		if (c6.length() != 6) {
			throw new Exception("Nombre de caractères invalide pour le parametre c6");
		}

		final String b2_dateReglement = FormatDate.format(dateReglement);
		StringBuffer buf = new StringBuffer();

		//        PrintWriter out = new PrintWriter(new StringWriter());
		BigDecimal sommeMontants = new BigDecimal(0);
		this.nbCar = 0;

		// entete 240 car
		buf.append("01");
		buf.append("000001");
		buf.append(spacesString(2));
		buf.append(FormatDate.format(laDateEchange)); //B2
		buf.append(C11_CODE_MONNAIE); //C11 Code monnaie
		buf.append(spacesString(4)); //C12 Code monnaie
		buf.append(C2_CODE_ETABLISSEMENT_EMETTEUR); //C2
		buf.append(c3); //C3 Code guichet emetteur
		buf.append(c41); //C41 identifiant client
		buf.append(c42); //C42 code application+divers
		buf.append(formattedString(c5, 24, SPACE)); //Nom du donneur d'ordre
		buf.append(spacesString(6));
		buf.append(spacesString(168));

		//Vérifier que la longueur de la ligne est correcte
		System.out.println("nb car " + buf.length());
		if (buf.length() % LONGUEUR_LIGNE_BDF != 0) {
			throw new Exception("Erreur de longueur sur la ligne d'en-tete - verifiez les parametres de prelevement (table pelevement_param_bdf)");
		}

		//details N*240 car
		int i;
		for (i = 0; i < prelevements.count(); i++) {

			final StringBuffer laLigneBuf = new StringBuffer();
			// echeance courante
			final EOPrelevement prelevement = (EOPrelevement) prelevements.objectAtIndex(i);
			final EORib ribfourUlr = prelevement.rib();
			laLigneBuf.append("04");
			laLigneBuf.append(FormatNumeroSequentiel.format(new Integer(i + 2)));
			laLigneBuf.append(codeVitesse); // (80 : prelev normal ; 85 : prelev accelere)
			laLigneBuf.append(b2_dateReglement); //B2
			laLigneBuf.append(C11_CODE_MONNAIE); //C11 Code monnaie
			laLigneBuf.append(spacesString(4)); //C12 Code monnaie
			laLigneBuf.append(C2_CODE_ETABLISSEMENT_EMETTEUR); //C2 
			laLigneBuf.append(formattedString(c3, 5, SPACE)); //C3 Code guichet emetteur
			laLigneBuf.append(formattedString(c41, 4, SPACE)); //C41 identifiant client
			laLigneBuf.append(formattedString(c42, 7, SPACE)); //C42 code application+divers
			laLigneBuf.append(formattedString(c5, 24, SPACE)); //Nom du donneur d'ordre
			laLigneBuf.append(formattedString(c6, 6, SPACE)); //numero national emetteur
			laLigneBuf.append(spacesString(5)); //
			laLigneBuf.append(formattedString(ribfourUlr.ribCodBanc(), 5, SPACE)); // code etablissement destinataire
			laLigneBuf.append(formattedString(ribfourUlr.ribGuich(), 5, SPACE)); // code guichet destinataire
			laLigneBuf.append(formattedString(ribfourUlr.ribNum(), 11, SPACE)); //numero compte destinataire
			laLigneBuf.append(formattedString(chaineSansCaracteresSpeciaux(prelevement.getClientNomAndPrenom()), 24, SPACE)); //nom destinataire
			laLigneBuf.append(spacesString(6)); //
			laLigneBuf.append(formattedString(ribfourUlr.ribLib(), 24, SPACE)); // libelle abbréviatif de domiciliation
			laLigneBuf.append(formattedString(chaineSansCaracteresSpeciaux(prelevement.echeancier().libelle()), 32, SPACE)); // libelle 1
			laLigneBuf.append(formattedString(prelevement.getPrelevementNumAndTotal(), 32, SPACE)); // libelle 2
			laLigneBuf.append(spacesString(10)); // 
			laLigneBuf.append(D10_2_CODE_FLUX); // 
			laLigneBuf.append(FormatMontant.format(prelevement.prelevMontant().multiply(new BigDecimal(100)))); // 

			//Vérifier que la longueur de la ligne est correcte
			if (laLigneBuf.length() % LONGUEUR_LIGNE_BDF != 0) {
				throw new Exception("Erreur de longueur sur la ligne " + i + 1 + " (client : " + prelevement.fournisseur().getNomAndPrenomAndCode() + ") " + prelevement.prelevIndex() + " - verifiez les informations (rib, etc.)");
			}
			buf.append(laLigneBuf);
			sommeMontants = sommeMontants.add(prelevement.prelevMontant());

		}

		// conclusion 240 car
		buf.append("09"); // 
		buf.append(FormatNumeroSequentiel.format(new Integer(i + 2))); // 
		buf.append(codeVitesse); // 
		buf.append(b2_dateReglement); //
		buf.append(C11_CODE_MONNAIE); //C11 Code monnaie
		buf.append(spacesString(4)); //C11 Code monnaie
		buf.append(C2_CODE_ETABLISSEMENT_EMETTEUR); //C2
		buf.append(c3); //C3 Code guichet emetteur
		buf.append(c41); //C41 identifiant client
		buf.append(c42); //C42 code application+divers
		buf.append(formattedString(c5, 24, SPACE)); //Nom du donneur d'ordre
		buf.append(spacesString(6));
		buf.append(spacesString(156));
		buf.append(FormatMontant.format(sommeMontants.multiply(new BigDecimal(100)))); //montant total 

		//Vérifier que la longueur de la ligne est correcte
		if (buf.length() % LONGUEUR_LIGNE_BDF != 0) {
			throw new Exception("Erreur de longueur sur la ligne de totalisation");
		}

		System.out.println("Somme des montants = " + sommeMontants);
		System.out.println("Nombre de caracteres ecrits : " + this.nbCar);

		this.current = this.lengthOfTask;//indique la fin de la tache

		return buf.toString();

	}

	public void cancel() {
		// interdit
	}

	public int getCurrent() {
		return this.current;
	}

	public int getLengthOfTask() {
		return this.lengthOfTask;
	}

	public String getStatusMessage() {
		return this.message;
	}

	public boolean isDone() {
		return this.current >= this.lengthOfTask;
	}

	/**
	 * @param string
	 */
	private void write(final PrintWriter writer, final String string) {
		nbCar += string.length();
		writer.print(string);

		this.current = this.nbCar;
	}

	private String formattedMontant(final BigDecimal montant) {
		String temp = FormatMontant.format(montant);
		StringBuffer buf = new StringBuffer(temp.substring(0, temp.indexOf(",")));
		buf.append(temp.substring(temp.indexOf(",") + 1));
		return buf.toString();
	}

	/**
	 * Ajoute des caracteres a la suite d'une String (ou tronque) pour lui faire atteindre la taille specifiee.
	 * 
	 * @param initialString String a etendre (ou tronquer)
	 * @param length Nombre de caracteres voulu
	 * @param charToPropagate Caractere a utilise pour etendre la taille de la String
	 * @return La nouvelle String
	 */
	private String formattedString(final String initialString, final int length, final char charToPropagate) {
		if (length < initialString.length())
			return initialString.substring(0, length);

		StringBuffer buf = new StringBuffer(initialString);
		int i = initialString.length();

		while (i < length) {
			buf.append(charToPropagate);
			i++;
		}
		//System.out.println(initialString+" --> ["+buf.toString()+"] ("+buf.toString().length()+")");
		return buf.toString();
	}

	/**
	 * Ajoute des caracteres 'espaces' a la suite d'une String (ou tronque) pour lui faire atteindre la taille specifiee.
	 * 
	 * @param initialString String a etendre (ou tronquer)
	 * @param length Nombre de caracteres voulu
	 * @return La nouvelle String
	 */
	private String formattedString(final String initialString, final int length) {
		return formattedString(initialString, length, ' ');
	}

	/**
	 * Fabrique une String composee du nombre specifie de caracteres specifie.
	 * 
	 * @param length Nombre de caracteres voulu
	 * @param charToPropagate Caractere qui composera la String
	 * @return La String fabriquee
	 */
	private String charactersString(final int length, final char charToPropagate) {
		return formattedString("", length, charToPropagate);
	}

	/**
	 * Fabrique une String composee du nombre specifie de caracteres 'espace'.
	 * 
	 * @param length Nombre de caracteres voulu
	 * @param charToPropagate Caractere qui composera la String
	 * @return La String fabriquee
	 */
	private String spacesString(final int length) {
		return formattedString("", length, ' ');
	}
}
