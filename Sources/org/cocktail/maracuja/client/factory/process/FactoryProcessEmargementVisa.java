/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 * 
 * @author Prin Rodolphe - CRI ULR
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.EmargementException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeEmargement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryProcessEmargementVisa extends FactoryProcessEmargement {

	public FactoryProcessEmargementVisa(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	/*
	 * CAS 1 : COMPOSANTE - COMPOSANTE >0 ECRITURE ET 1 EMARGMENT AGENCE - AGENCE >0 ECRITURE ET 1 EMARGMENT SACD 1 - SACD 1 >0 ECRITURE ET 1
	 * EMARGMENT CAS 2 : COMPOSANTE - AGENCE > 1 ECRITURE ET 2 EMARGMENTS CAS 3 : AGENCE - COMPOSANTE > 1 ECRITURE ET 2 EMARGMENTS CAS 4 : SACD -
	 * AGENCE >2 ECRITURES ET 2 EMARGEMENTS CAS 5 : AGENCE - SACD >2 ECRITURES ET 2 EMARGEMENTS CAS 6 : SACD - COMPOSANTE >3 ECRITURES ET 3
	 * EMARGEMENTS CAS 7 : COMPOSANTE - SACD >3 ECRITURES ET 3 EMARGEMENTS CAS 8 : SACD 1 - SACD 2 >4 ECRITURES ET 3 EMARGEMENTS CAS 9 : COMP1 ->
	 * COMP2 > 2ECRITURES ET 3 EMARGEMENTS
	 */

	public NSMutableArray emargementAuVisa(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {
		trace("");
		trace("=====>>>> emargementAuVisa");

		/*
		 * cas numero 1 le code gestion des details sont identiques donc on emarge ! PAS DE NOUVELLES ECRITURES
		 */
		if (detailCrediteur.gestion().equals(detailDebiteur.gestion())) {
			trace("cas numero 1");

			return emargementAuVisaMemeCodeGestion(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}
		/*
		 * cas numero 2 CREDIT COMPOSANTE ET DEBIT AGENCE
		 */
		if (this.estUneComposante(comptabilite, detailCrediteur.gestion(), exercice) && this.estUneAgenceComptable(comptabilite, detailDebiteur.gestion())) {
			trace("cas numero 2");
			return emargementAuVisaComposanteAgence(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * cas numero 3 CREDIT AGENCE ET DEBIT COMPOSANTE
		 */
		if (this.estUneAgenceComptable(comptabilite, detailCrediteur.gestion()) && this.estUneComposante(comptabilite, detailDebiteur.gestion(), exercice)) {
			trace("cas numero 3");
			return emargementAuVisaAgenceComposante(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * CAS 4 CREDIT SACD ET DEBIT AGENCE
		 */
		if (this.estUnSACD(detailCrediteur.gestion(), exercice) && this.estUneAgenceComptable(comptabilite, detailDebiteur.gestion())) {
			trace("cas numero 4");
			return emargementAuVisaSacdAgence(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * CAS 5 CREDIT AGENCE ET DEBIT SACD
		 */
		if (this.estUneComposante(comptabilite, detailCrediteur.gestion(), exercice) && this.estUnSACD(detailDebiteur.gestion(), exercice)) {
			trace("cas numero 5");
			return emargementAuVisaAgenceSacd(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * CAS 6 CREDIT SACD ET DEBIT COMPOSANTE
		 */
		if (this.estUnSACD(detailCrediteur.gestion(), exercice) && this.estUneComposante(comptabilite, detailDebiteur.gestion(), exercice)) {
			trace("cas numero 6");
			return emargementAuVisaSacdComposante(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * CAS 7 CREDIT COMPOSANTE ET DEBIT SACD
		 */
		if (this.estUneComposante(comptabilite, detailCrediteur.gestion(), exercice) && this.estUnSACD(detailDebiteur.gestion(), exercice)) {
			trace("cas numero 7");
			return emargementAuVisaComposanteSacd(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * CAS 8 CREDIT SACD 1 ET DEBIT SACD 2
		 */
		if (this.estUnSACD(detailCrediteur.gestion(), exercice) && this.estUnSACD(detailDebiteur.gestion(), exercice)) {
			trace("cas numero 8");
			return emargementAuVisaSacd1Sacd2(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		/*
		 * CAS 9 CREDIT COMP 1 ET DEBIT COMP 2
		 */
		if (this.estUneComposante(comptabilite, detailCrediteur.gestion(), exercice) && this.estUneComposante(comptabilite, detailDebiteur.gestion(), exercice)) {
			trace("cas numero 9");
			return emargementAuVisaComp1Comp2(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant, lesFutursEcritures);
		}

		trace("cas numero ????");
		throw new EmargementException("Emargement auto non pris en charge dans ce cas.");

	}

	public NSMutableArray emargementAuVisaMemeCodeGestion(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaMemeCodeGestion");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();

		if (!detailCrediteur.gestion().equals(detailDebiteur.gestion())) {
			throw new EmargementException("CODE GESTION DIFFERENTS !");
		}

		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, detailDebiteur, numero, comptabilite, montant));
		return lesNouveauxEmargements;

	}

	public NSMutableArray emargementAuVisaComposanteAgence(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaComposanteAgence");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 1 ECRITURE ET 1 EMARGMENT
		EOGestion agence = detailDebiteur.gestion();
		EOGestion composante = detailCrediteur.gestion();
		//EOGestion sacdC = null;
		//EOGestion sacdD = null;

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();
		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);

		// nous avons CREDIT COMPOSANTE ET DEBIT AGENCE
		// PREMIERE ECRITURE
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT COMPOSANTE
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, composante);

		// CREDIT AGENCE
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);

		// EMARGEMENT COMPOSANTE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// EMARGEMENT AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		return lesNouveauxEmargements;

	}

	public NSMutableArray emargementAuVisaAgenceComposante(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaAgenceComposante ");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 1 ECRITURE ET 1 EMARGMENT
		EOGestion agence = detailCrediteur.gestion();
		EOGestion composante = detailDebiteur.gestion();
		//EOGestion sacdC = null;
		//EOGestion sacdD = null;

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();
		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);

		// nous avons CREDIT AGENCE ET DEBIT COMPOSANTE
		// PREMIERE ECRITURE
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT AGENCE
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);

		// CREDIT COMPOSANTE
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, composante);

		// EMARGEMENT AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// EMARGEMENT COMPOSANTE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		return lesNouveauxEmargements;

	}

	public NSMutableArray emargementAuVisaSacdAgence(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaSacdAgence");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 2 ECRITURES ET 2 EMARGEMENTS
		EOGestion agence = detailDebiteur.gestion();
		//EOGestion composante = null;
		EOGestion sacdC = detailCrediteur.gestion();
		//EOGestion sacdD = null;

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();

		// //Modif rod : code gestion exer
		//		final EOPlanComptable planco185SacdD = FinderGestion.getPlanco185ForGestionAndExercice(detailDebiteur.gestion(), exercice);
		final EOPlanComptable planco185SacdD = detailDebiteur.gestion().getPlanco185(exercice);

		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);

		// nous avons CREDIT SACD ET DEBIT AGENCE
		// creation d'une ecriture pour revenir a la agence compable PREMIERE
		// ECRITURE !!!!
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT SACD
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdC);

		// on emarge le debit SACD avec le credit SACD
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT 185 DANS LE SACD
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdC);
		//setPlanComptable(nouveauDetailCrediteur, FinderJournalEcriture.planComptable185(ed));
		setPlanComptable(nouveauDetailCrediteur, sacdC.getPlanco185CtpSacd(exercice));

		// SECONDE ECRITURE ....ON PART DU DEBIT AGENCE
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT AGENCE
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);

		// on emarge le debit SACD avec le credit SACD
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// DEBIT 185xxx DANS L AGENCE
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		// setPlanComptable(nouveauDetailDebiteur,detailDebiteur.gestion().planComptable185());
		setPlanComptable(nouveauDetailDebiteur, planco185SacdD);

		return lesNouveauxEmargements;

	}

	public NSMutableArray emargementAuVisaAgenceSacd(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaAgenceSacd");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 2 ECRITURES ET 2 EMARGEMENTS
		EOGestion agence = detailCrediteur.gestion();
		//EOGestion composante = null;
		//EOGestion sacdC = null;
		EOGestion sacdD = detailDebiteur.gestion();

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();

		// Modif rod : code gestion exer
		//		final EOPlanComptable planco185SacdD = FinderGestion.getPlanco185ForGestionAndExercice(sacdD, exercice);
		final EOPlanComptable planco185SacdD = sacdD.getPlanco185(exercice);

		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);
		// if (detailCrediteur.gestion().equals(detailDebiteur.gestion()))
		// {}

		// nous avons CREDIT AGENCE ET DEBIT SACD
		// PREMIERE ECRITURE !!!!
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT AGENCE
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);

		// EMARGEMENT AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT 185xxx AGENCE
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		// setPlanComptable(nouveauDetailCrediteur,detailDebiteur.gestion().planComptable185());
		setPlanComptable(nouveauDetailCrediteur, planco185SacdD);

		// 2EM ECRITURE ON PART DU DEBIT SACD
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT SACD
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, sacdD);

		// on emarge le debit SACD avec le credit SACD
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// DEBIT 185 DANS SACD
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdD);
		//		setPlanComptable(nouveauDetailDebiteur, FinderJournalEcriture.planComptable185(ed));
		setPlanComptable(nouveauDetailDebiteur, sacdD.getPlanco185CtpSacd(exercice));

		return lesNouveauxEmargements;

	}

	public NSMutableArray emargementAuVisaSacdComposante(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaSacdComposante");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 3 ECRITURES ET 3 EMARGEMENTS
		EOGestion agence = comptabilite.gestion();
		EOGestion composante = detailDebiteur.gestion();
		EOGestion sacdC = detailCrediteur.gestion();
		//EOGestion sacdD = null;

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();

		// Modif rod : code gestion exer
		//	final EOPlanComptable planco185sacdC = FinderGestion.getPlanco185ForGestionAndExercice(sacdC, exercice);
		final EOPlanComptable planco185sacdC = sacdC.getPlanco185(exercice);

		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);
		// if (detailCrediteur.gestion().equals(detailDebiteur.gestion()))
		// {}

		// nous avons CREDIT SACD ET DEBIT COMPOSANTE
		// PREMIERE ECRITURE !!!!
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT SACD
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdC);

		// on emarge le debit SACD avec le credit SACD
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT 185 DANS LE SACD
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdC);
		//		setPlanComptable(nouveauDetailCrediteur, FinderJournalEcriture.planComptable185(ed));
		setPlanComptable(nouveauDetailCrediteur, sacdC.getPlanco185CtpSacd(exercice));

		// SECONDE ECRITURE ....ON PART DU DEBIT COMPOSANTE
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT COMPOSANTE
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, composante);

		// on emarge le debit COMPOSANTE avec le credit COMPOSANTE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// DEBIT DANS L AGENCE
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		// setPlanComptable(nouveauDetailDebiteur,detailDebiteur.gestion().planComptable185());

		// TROISIEME ECRITURE ....AGENCE
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT AGENCE
		// on conserve le nouveauDebiteur au AGENCE
		EOEcritureDetail nouveauDetailDebiteurAgence = nouveauDetailDebiteur;
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);

		// on emarge le debit AGENCE avec le credit AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, nouveauDetailDebiteurAgence, numero, comptabilite, montant));

		// creation des details :
		// DEBIT DANS L AGENCE 185xxx
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		// setPlanComptable(nouveauDetailDebiteur,detailCrediteur.gestion().planComptable185());
		setPlanComptable(nouveauDetailDebiteur, planco185sacdC);

		return lesNouveauxEmargements;

	}

	public NSMutableArray emargementAuVisaComposanteSacd(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaComposanteSacd");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 3 ECRITURES ET 3 EMARGEMENTS
		EOGestion agence = comptabilite.gestion();
		EOGestion composante = detailCrediteur.gestion();
		//EOGestion sacdC = null;
		EOGestion sacdD = detailDebiteur.gestion();

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();

		//		final EOPlanComptable planco185SacdD = FinderGestion.getPlanco185ForGestionAndExercice(sacdD, exercice);
		final EOPlanComptable planco185SacdD = sacdD.getPlanco185(exercice);
		;

		// normalement c bon : < le montant n'est pas bon : il faut prendre le plus petit entre
		// le montant de la facture et le montant du débit (ce montant est
		// normalement celui de l'émargement, passé en paramètre)>
		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);

		// nous avons CREDIT COMPOSANTE ET DEBIT SACD
		// PREMIERE ECRITURE !!!!
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT COMPOSANTE
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, composante);

		// on emarge le debit SACD avec le credit SACD
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT AGENCE
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		EOEcritureDetail nouveauDetailCrediteurAgence = nouveauDetailCrediteur;

		// SECONDE ECRITURE ....ON PART DU DEBIT SACD
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT SACD
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, sacdD);

		// on emarge le debit COMPOSANTE avec le credit COMPOSANTE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// DEBIT DANS LE SACD 185
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdD);
		//		setPlanComptable(nouveauDetailDebiteur, FinderJournalEcriture.planComptable185(ed));
		setPlanComptable(nouveauDetailDebiteur, sacdD.getPlanco185CtpSacd(exercice));
		// detailDebiteur.gestion().planComptable185());

		// TROISIEME ECRITURE ....AGENCE
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT AGENCE
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);

		// on emarge le debit AGENCE avec le credit AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteurAgence, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT DANS L AGENCE 185xxx
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailCrediteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);
		// setPlanComptable(nouveauDetailCrediteur,detailDebiteur.gestion().planComptable185());
		setPlanComptable(nouveauDetailCrediteur, planco185SacdD);

		return lesNouveauxEmargements;

	}

	/**
	 * CAS 8
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param detailCrediteur
	 * @param detailDebiteur
	 * @param numero
	 * @param comptabilite
	 * @param montant
	 * @param lesFutursEcritures
	 * @return
	 * @throws EmargementException
	 */
	public NSMutableArray emargementAuVisaSacd1Sacd2(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaSacd1Sacd2");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 4 ECRITURES ET 3 EMARGEMENTS
		EOGestion agence = comptabilite.gestion();
		//EOGestion composante = null;
		EOGestion sacdC = detailCrediteur.gestion();
		EOGestion sacdD = detailDebiteur.gestion();

		// Modif rod : code gestion exer
		final EOPlanComptable planco185SacdC = sacdC.getPlanco185(exercice);
		//		final EOPlanComptable planco185SacdD = FinderGestion.getPlanco185ForGestionAndExercice(sacdD, exercice);
		final EOPlanComptable planco185SacdD = sacdD.getPlanco185(exercice);
		;
		;

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();

		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);

		// nous avons CREDIT SACD1 ET DEBIT SACD2
		// PREMIERE ECRITURE ON PART DU SACD 1 !!!!
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT SACD1
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdC);

		// on emarge le debit SACD 1 avec le credit SACD 1
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT SACD 1 185
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, sacdC);
		//	setPlanComptable(nouveauDetailCrediteur, FinderJournalEcriture.planComptable185(ed));
		setPlanComptable(nouveauDetailCrediteur, sacdC.getPlanco185CtpSacd(exercice));

		// SECONDE ECRITURE ....ON PART DU DEBIT SACD 2
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT SACD
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, sacdD);

		// on emarge le debit SACD 2 avec le credit SACD 2
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// DEBIT DANS LE SACD 185
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, sacdD);
		//setPlanComptable(nouveauDetailDebiteur, FinderJournalEcriture.planComptable185(ed));
		setPlanComptable(nouveauDetailDebiteur, sacdD.getPlanco185CtpSacd(exercice));
		// detailDebiteur.gestion().planComptable185());

		// TROISIEME ECRITURE ....AGENCE
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT AGENCE 185sacd1
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		// setPlanComptable(nouveauDetailDebiteur,sacdC.planComptable185());
		setPlanComptable(nouveauDetailDebiteur, planco185SacdC);

		// creation des details :
		// CREDIT DANS L AGENCE compte comptable du sacd1
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);
		setPlanComptable(nouveauDetailCrediteur, detailCrediteur.planComptable());
		EOEcritureDetail nouveauDetailCrediteurAgence = nouveauDetailCrediteur;

		// QUATRIEME ECRITURE ....AGENCE
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :

		// creation des details :
		// CREDIT DANS L AGENCE compte 185sacd2
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);
		// setPlanComptable(nouveauDetailCrediteur,sacdD.planComptable185());
		setPlanComptable(nouveauDetailCrediteur, planco185SacdD);

		// DEBIT AGENCE 185sacd1
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		setPlanComptable(nouveauDetailDebiteur, detailDebiteur.planComptable());
		EOEcritureDetail nouveauDetailDebiteurAgence = nouveauDetailDebiteur;

		// on emarge le debit AGENCE avec le credit AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteurAgence, nouveauDetailDebiteurAgence, numero, comptabilite,
				montant));

		return lesNouveauxEmargements;

	}

	/**
	 * CAS 9
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param detailCrediteur
	 * @param detailDebiteur
	 * @param numero
	 * @param comptabilite
	 * @param montant
	 * @param lesFutursEcritures
	 * @return
	 * @throws EmargementException
	 */
	public NSMutableArray emargementAuVisaComp1Comp2(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur,
			EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant, NSMutableArray lesFutursEcritures) throws EmargementException {

		trace("emargementAuVisaSacd1Sacd2");
		trace("D: " + detailDebiteur.gestion().gesCode() + " / C:" + detailCrediteur.gestion().gesCode());

		// 4 ECRITURES ET 3 EMARGEMENTS
		EOGestion agence = comptabilite.gestion();
		// EOGestion composante =null;
		EOGestion compC = detailCrediteur.gestion();
		EOGestion compD = detailDebiteur.gestion();
		EOEcritureDetail nouveauDetailCrediteurAgence = null;
		// Modif rod : code gestion exer
		// final EOPlanComptable planco185SacdC =
		// FinderGestion.getPlanco185ForGestionAndExercice(sacdC, exercice);
		// final EOPlanComptable planco185SacdD =
		// FinderGestion.getPlanco185ForGestionAndExercice(sacdD, exercice);

		NSMutableArray lesNouveauxEmargements = new NSMutableArray();
		BigDecimal leMontant = definirMontantEmargementEcriture(detailCrediteur, detailDebiteur);

		// nous avons CREDIT COMP1 ET DEBIT COMP2
		// PREMIERE ECRITURE ON PART DU COMP 1 !!!!
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// DEBIT COMP1
		EOEcritureDetail nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailCrediteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, compC);

		// on emarge le debit comp 1 avec le credit comp 1
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, detailCrediteur, nouveauDetailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// CREDIT AGENCE
		EOEcritureDetail nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, nouveauDetailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, agence);
		nouveauDetailCrediteurAgence = nouveauDetailCrediteur;

		// SECONDE ECRITURE ....ON PART DU DEBIT COMP 2
		nouvelleEcriture = this.creerEcriture(ed, utilisateur, detailCrediteur);
		lesFutursEcritures.addObject(nouvelleEcriture);

		// creation des details :
		// CREDIT COMP2
		nouveauDetailCrediteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnCredit(nouveauDetailCrediteur);
		setMontantEtResteEmarger(nouveauDetailCrediteur, leMontant);
		setGestion(nouveauDetailCrediteur, compD);

		// on emarge le debit COMP 2 avec le credit COMP 2
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailCrediteur, detailDebiteur, numero, comptabilite, montant));

		// creation des details :
		// DEBIT DANS LE AGENCE
		nouveauDetailDebiteur = this.creerCopieDetailPourEcriture(ed, detailDebiteur, nouvelleEcriture);
		passerEnDebit(nouveauDetailDebiteur);
		setMontantEtResteEmarger(nouveauDetailDebiteur, leMontant);
		setGestion(nouveauDetailDebiteur, agence);
		// setPlanComptable(nouveauDetailDebiteur,FinderJournalEcriture.planComptable185(ed));
		// detailDebiteur.gestion().planComptable185());

		// on emarge le debit AGENCE avec le credit AGENCE
		lesNouveauxEmargements.addObject(this.emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, nouveauDetailDebiteur, nouveauDetailCrediteurAgence, numero, comptabilite, montant));

		return lesNouveauxEmargements;

	}

	//  
	// private boolean estUnSACD(EOGestion gestion)
	// {
	// if(gestion.planComptable181() != null)
	// return true;
	// else
	// return false;
	// }

	// Modif Rod : prise en compte exercice
	private boolean estUnSACD(final EOGestion gestion, final EOExercice exercice) {
		//return (FinderGestion.getPlanco185ForGestionAndExercice(gestion, exercice) != null);
		return gestion.isSacd(exercice);

	}

	private boolean estUneAgenceComptable(EOComptabilite comptabilite, EOGestion gestion) {
		if (gestion.gesCode().equals(comptabilite.gestion().gesCode())) {
			return true;
		}
		return false;
	}

	// Modif Rod : prise en compte exercice
	private boolean estUneComposante(final EOComptabilite comptabilite, final EOGestion gestion, final EOExercice exercice) {
		// est ce un SACD ?
		if (estUnSACD(gestion, exercice)) {
			return false;
		}

		// est l agence comptable
		if (estUneAgenceComptable(comptabilite, gestion)) {
			return false;
		}
		return true;
	}

	private EOEcriture creerEcriture(EOEditingContext ed, EOUtilisateur utilisateur,
				// EOExercice exercice,
			EOEcritureDetail leDetail) {

		FactoryProcessJournalEcriture maFactoryEcriture = new FactoryProcessJournalEcriture(this.withLogs(), getDateJourComptable());

		// sinon creation d'une ecriture et creation de 2 details
		EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), "Ecriture automatique pour emargement au visa", new Integer(0), null, leDetail
				.gestion().comptabilite(), leDetail.exercice(), leDetail.ecriture().origine(), leDetail.ecriture().typeJournal(), leDetail.ecriture().typeOperation(),
				utilisateur); // FIXED chgt exercice date journee comptable
		// EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(
		// ed,
		// getDateJour(),
		// "Ecriture automatique pour emargement au visa",
		// new Integer(0),
		// null,
		// (EOComptabilite) leDetail.gestion().comptabilite(),
		// (EOExercice) leDetail.exercice(),
		// (EOOrigine) leDetail.ecriture().origine(),
		// leDetail.ecriture().typeJournal(),
		// leDetail.ecriture().typeOperation(),
		// utilisateur);

		return newEOEcriture;
	}

	private EOEcritureDetail creerCopieDetailPourEcriture(EOEditingContext ed, EOEcritureDetail leDetail, EOEcriture laNouvelleEcriture) {
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

		EOEcritureDetail nouveauDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, "auto emargment visa", new Integer(1), leDetail.ecdLibelle(), leDetail.ecdMontant(), null, leDetail
				.ecdMontant(), null, leDetail.ecdSens(), laNouvelleEcriture, leDetail.exercice(), leDetail.gestion(), leDetail.planComptable());
		return nouveauDetail;
	}

	private void passerEnCredit(EOEcritureDetail leDetail) {
		leDetail.setEcdSens(Factory.sensCredit());
	}

	private void passerEnDebit(EOEcritureDetail leDetail) {
		leDetail.setEcdSens(Factory.sensDebit());
	}

	private void setGestion(EOEcritureDetail leDetail, EOGestion gestion) {
		leDetail.setGestionRelationship(gestion);
	}

	private void setPlanComptable(EOEcritureDetail leDetail, EOPlanComptable planComptable) {
		leDetail.setPlanComptableRelationship(planComptable);
	}

	private void setMontantEtResteEmarger(EOEcritureDetail leDetail, BigDecimal montant) {
		if (leDetail.ecdSens().equalsIgnoreCase(sensCredit())) {
			leDetail.setEcdCredit(montant);
		}
		else {
			leDetail.setEcdCredit(new BigDecimal(0));
		}

		if (leDetail.ecdSens().equalsIgnoreCase(sensDebit())) {
			leDetail.setEcdDebit(montant);
		}
		else {
			leDetail.setEcdDebit(new BigDecimal(0));
		}
		leDetail.setEcdMontant(montant);
		leDetail.setEcdResteEmarger(montant.abs());
	}

	// private BigDecimal definirMontantEmargementEcriture (EOEcritureDetail
	// detailCrediteur,EOEcritureDetail detailDebiteur) {
	// if (
	// Factory.inferieurOuEgal(detailCrediteur.ecdResteEmarger(),detailDebiteur.ecdResteEmarger()))
	// {
	// return detailDebiteur.ecdResteEmarger();
	// }
	// return detailCrediteur.ecdResteEmarger();
	// }

	//
	/**
	 * Renvoie le montant de l'emargement a effectuer. Il s'agit du plus petit montant entre les reste a emarger du debit et du credit. Correction rod
	 * (la comparaison etait inversée, la comparaison ne s'efefctuait pas sur les valeurs absolues).
	 */
	private BigDecimal definirMontantEmargementEcriture(EOEcritureDetail detailCrediteur, EOEcritureDetail detailDebiteur) {
		if (Factory.inferieurOuEgal(detailCrediteur.ecdResteEmarger().abs(), detailDebiteur.ecdResteEmarger().abs())) {
			return detailCrediteur.ecdResteEmarger();
		}
		return detailDebiteur.ecdResteEmarger();
	}

	private void trace(String s) {
		super.trace("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		super.trace(s);
		super.trace("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
	}
}
