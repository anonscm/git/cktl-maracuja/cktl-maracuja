/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;
import java.util.Map;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryMandatDetailEcriture;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author frivalla
 * @author rprin
 */
public class FactoryProcessPaiementMandat extends FactoryProcess {

	private Map _comptesBE;

	/**
	 * @param withLog
	 */
	public FactoryProcessPaiementMandat(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public NSArray viserVirementMandats(EOEditingContext ed, NSArray lesMandats, EOUtilisateur utilisateur, String traitement, EOExercice exercice, Map compteBe, String paiNumero) throws Exception {
		_comptesBE = compteBe;
		EOMandat tmpMandat = null;
		EOTypeJournal typeJournal = FinderJournalEcriture.leTypeJournalPaiement(ed);

		NSMutableArray lesEcritures = new NSMutableArray();
		//        boolean sacd;
		int i = 0;

		// VERIFIER PAS DE DEPENSE OU PAS VISE !
		i = 0;
		while (i < lesMandats.count()) {
			if (EOMandat.mandatAnnule.equals(((EOMandat) (lesMandats.objectAtIndex(i))).manEtat())) {
				throw new FactoryException(EOMandat.problemeAnnule);
			}

			if (EOMandat.mandatAttente.equals(((EOMandat) (lesMandats.objectAtIndex(i))).manEtat())) {
				throw new FactoryException(EOMandat.problemeNonVise);
			}
			i++;
		}

		i = 0;
		while (i < lesMandats.count()) {
			tmpMandat = ((EOMandat) lesMandats.objectAtIndex(i));
			if (EOMandat.mandatVise.equals(tmpMandat.manEtat())) {
				NSArray lesEcrituresDuMandat = new NSArray();
				final EOPlanComptable planco185 = tmpMandat.gestion().getPlanco185(exercice);
				if (planco185 != null) {
					lesEcrituresDuMandat = this.ecritureMandatGestionMandat(ed, tmpMandat, typeJournal, utilisateur, true, exercice, paiNumero);
				}
				else {
					lesEcrituresDuMandat = this.ecritureMandatGestionMandat(ed, tmpMandat, typeJournal, utilisateur, false, exercice, paiNumero);
				}
				lesEcritures.addObjectsFromArray(lesEcrituresDuMandat);
			}
			i++;
		}
		return lesEcritures;
	}

	public NSArray ecritureMandatModifierCredit(EOEditingContext ed, EOMandat mandat, EOTypeJournal typeJournal, EOUtilisateur utilisateur, String libelle, EOPlanComptable credit, EOGestion gestionCredit, boolean sacd, EOExercice exercice) throws Exception {
		//On ne doit pas faire l'écriture sur un compte BE (si visé sur 2005 et paiement en 2006, seule l'écriture de paiement en 2006 doit basculer sur le compte BE) 
		_comptesBE = null;
		return this.ecritureMandat(ed, mandat, typeJournal, utilisateur, libelle, credit, gestionCredit, sacd, exercice, EOMandatDetailEcriture.MDE_ORIGINE_MODIF_MODE_PAIEMENT);
	}

	private NSArray ecritureMandatGestionMandat(EOEditingContext ed, EOMandat mandat, EOTypeJournal typeJournal, EOUtilisateur utilisateur, boolean sacd, EOExercice exercice, String paiNumero) throws Exception {
		String libelle = "PAIEMENT " + paiNumero + " MANDAT " + mandat.manNumero() + " Bord. " + mandat.bordereau().typeBordereau().tboSousType() + " " + mandat.bordereau().borNum() + " du " + mandat.gestion().gesCode();
		EOPlanComptable credit = mandat.modePaiement().getPlanCoPaiementSelonExercice(exercice).planComptable();
		return this.ecritureMandat(ed, mandat, typeJournal, utilisateur, libelle, credit, null, sacd, exercice, EOMandatDetailEcriture.MDE_ORIGINE_VIREMENT);
	}

	private NSArray ecritureMandat(EOEditingContext ed, EOMandat mandat, EOTypeJournal typeJournal, EOUtilisateur utilisateur, String libelle, EOPlanComptable creditModePaiement, EOGestion gestionCreditModifModPaiement, boolean sacd, EOExercice exercice, String mdeOrigine) throws Exception {
		EOGestion gestionCredit = null;
		BigDecimal montantDebits = new BigDecimal(0.00);

		EOEcriture monEcritureVirement = null;
		EOEcritureDetail monEcritureDetail = null;

		BigDecimal montantRetenue = new BigDecimal(0);
		BigDecimal montantCredit = new BigDecimal(0);
		BigDecimal montantRetenues = new BigDecimal(0);

		int i = 0;
		int j = 0;

		if (mandat.manEtat().equals((EOMandat.mandatVirement))) {
			throw new FactoryException(EOMandat.problemeDejaPaye);
		}

		if (mandat.manEtat().equals((EOMandat.mandatAnnule))) {
			throw new FactoryException(EOMandat.problemeAnnule);
		}

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());
		FactoryMandatDetailEcriture maFactoryMandatDetailEcriture = new FactoryMandatDetailEcriture(withLogs());
		NSMutableArray lesEcritures = new NSMutableArray();

		// recup du code gestion du credit
		if (gestionCreditModifModPaiement == null) {
			gestionCredit = mandat.gestion().comptabilite().gestion();
		}
		else {
			gestionCredit = gestionCreditModifModPaiement;
		}
		// CREATION DE LECRITURE :
		monEcritureVirement = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), libelle, new Integer(0), null, mandat.bordereau().gestion().comptabilite(), exercice, mandat.origine(), typeJournal, FinderJournalEcriture.leTypeOperationPaiement(ed), utilisateur);
		// recup des ecritures de visa credit du visa !
		i = 0;

		NSArray mdes = mandat.mandatDetailEcritures().immutableClone();
		while (i < mdes.count()) {
			// si c est un credit on le met dans le tableau pour generer un
			// debit equivalent si reste a emarger diff de zero ( a zero si
			// modif de mode de paiement)
			EOMandatDetailEcriture mde = (EOMandatDetailEcriture) mdes.objectAtIndex(i);
			final EOEcritureDetail ecd = mde.ecritureDetail();
			ed.invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(ed, new NSArray(new Object[] {
					ecd
			})));

			// prendre seulement les ecritures de VISA (et non TVA en crédit)
			if (!EOMandatDetailEcriture.MDE_ORIGINE_VISA_TVA.equals(mde.mdeOrigine())) {
				if (SENS_CREDIT.equals(ecd.ecdSens()) && ecd.ecriture() != monEcritureVirement && ecd.ecdResteEmarger().abs().compareTo(ecd.ecdMontant().abs()) == 0) {
					// CREATION DU DETAIL ECRITURE DEBIT GESTION
					monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i), mandat.fournisseur().adrNom() + " " + libelle, ecd.ecdMontant(), "", ecd.ecdMontant(), null, sensDebit(), monEcritureVirement, monEcritureVirement.exercice(), ecd.gestion(), getPlancoBE(
							ecd.planComptable(), mandat.exercice()));
					if (ecd.toAccordsContrat() != null) {
						monEcritureDetail.setToAccordsContratRelationship(ecd.toAccordsContrat());
					}
					montantDebits = montantDebits.add(ecd.ecdMontant());
					System.out.println("--");
					System.out.println("D=" + ecd.ecdMontant());
					maFactoryMandatDetailEcriture.creerMandatDetailEcriture(ed, Factory.getNow(), mdeOrigine, monEcritureDetail, monEcritureDetail.exercice(), mandat, mandat.origine());
				}
			}

			i++;
		}

		// Traitement des retenues (les retenues generent des credits (a diminuer du credit sur la classe 5) sur le SACD s'il existe
		i = 0;
		// on scanne les depense
		while (i < mandat.depenses().count()) {
			j = 0;
			// on recupere les retenues d une depense
			while (j < ((EODepense) (mandat.depenses().objectAtIndex(i))).retenues().count()) {
				final EORetenue tmpRetenue = ((EORetenue) (((EODepense) (mandat.depenses().objectAtIndex(i))).retenues().objectAtIndex(j)));
				final EOGestion gestionRetenue = (sacd ? mandat.gestion() : gestionCredit);
				montantRetenue = tmpRetenue.retMontant();
				montantRetenues = montantRetenues.add(montantRetenue);
				final EOPlanComptable plancoRetenueCredit = tmpRetenue.typeRetenue().planComptable();

				// on diminue la part à basculer sur le compte 5 montantCredit = montantCredit - montantRetenue;
				//montantCredit = montantCredit.add(montantRetenue.negate());
				// CREATION DU DETAIL ECRITURE CREDIT RETENUE
				final String lib = mandat.fournisseur().adrNom() + " RETENUE SUR PAIEMENT Man. " + mandat.manNumero() + " Bord. " + mandat.bordereau().typeBordereau().tboSousType() + " " + mandat.bordereau().borNum() + " du " + mandat.gestion().gesCode() + " Fac. "
						+ tmpRetenue.depense().depNumero() + " : " + tmpRetenue.retLibelle();

				monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i + 1000), lib, montantRetenue, "", montantRetenue, null, sensCredit(), monEcritureVirement, monEcritureVirement.exercice(), gestionRetenue, plancoRetenueCredit);
				maFactoryMandatDetailEcriture.creerMandatDetailEcriture(ed, Factory.getNow(), EOMandatDetailEcriture.MDE_ORIGINE_VIREMENT_RETENUE, monEcritureDetail, monEcritureDetail.exercice(), mandat, mandat.origine());
				j++;
			}
			System.out.println("C=" + ((EODepense) (mandat.depenses().objectAtIndex(i))).depMontantDisquette());
			montantCredit = montantCredit.add(((EODepense) (mandat.depenses().objectAtIndex(i))).depMontantDisquette());
			i++;

		}
		System.out.println("Total credits =" + montantCredit.add(montantRetenues));
		System.out.println("---");
		if (montantCredit.add(montantRetenues).compareTo(montantDebits) != 0) {
			throw new Exception("Les débits sont différents des crédits pour le paiement du mandat " + mandat.gescodeAndNum() + " (D: " + montantDebits + " / C:" + montantCredit.add(montantRetenues)
					+ "). Si les débits sont inférieurs, vérifiez que la contrepartie du visa de ce mandat n'a pas été émargée.");
		}

		if (sacd) {
			//Dans le cas d'un SACD, le visa sur la classe client se fait sur le SACD, donc les ecritures de paiement sur la classe client doivent aussi etre sur le SACD
			//Par contre le credit (classe 5) doit etre fait sur l'agence, donc on a deux ecritures
			lesEcritures.addObject(monEcritureVirement);
			// Creation d un detail somme des debit en Credit 185 SACD a ajouter a l'ecriture initiale
			// CREATION DU DETAIL ECRITURE CREDIT SACD 185
			monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i), "", montantCredit, "", montantCredit, null, sensCredit(), monEcritureVirement, monEcritureVirement.exercice(), mandat.gestion(), mandat.gestion().getPlanco185CtpSacd(exercice));
			// creation d une nouvelle ecriture
			monEcritureVirement = maFactoryProcessJournalEcriture.creerEcriture(ed, Factory.getNow(), libelle, new Integer(0), " automatique", mandat.bordereau().gestion().comptabilite(), exercice, mandat.origine(), typeJournal, FinderJournalEcriture.leTypeOperationPaiement(ed), utilisateur);
			// creation d un nouveau detail somme des debit Debit comptabilite
			//Modif code gestion exer
			//on se base sur l'exercice de paiement pour passer le debit sinon on recupere l'ancien compte
			//final EOPlanComptable planco185 = mandat.gestion().getPlanco185(mandat.exercice());

			final EOPlanComptable planco185 = mandat.gestion().getPlanco185(exercice);

			monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i), "", montantCredit, "", montantCredit, null, sensDebit(), monEcritureVirement, monEcritureVirement.exercice(), mandat.bordereau().gestion().comptabilite().gestion(), planco185);
		}
		//		this.trace("-----------------------------");
		//		this.trace(" montant credit " + montantCredit);

		// CREATION DU DETAIL ECRITURE CREDIT paiement !
		//CHECK si retenues = montant mandat => on ne crée pas de ligne
		if (montantCredit.compareTo(ZConst.BIGDECIMAL_ZERO.setScale(2)) > 0) {
			monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i + 2000), "", montantCredit, "", montantCredit, null, sensCredit(), monEcritureVirement, monEcritureVirement.exercice(), gestionCredit, creditModePaiement);
			// CREATION DU MandatDetailEcriture
			maFactoryMandatDetailEcriture.creerMandatDetailEcriture(ed, Factory.getNow(), mdeOrigine, monEcritureDetail, monEcritureDetail.exercice(), mandat, mandat.origine());
			this.trace(" MANDAT DETAIL ECRITURE AGENCE CREDIT PAIEMENT ");
		}

		// maj du mandat si gestionCreditModifModPaiement est null => on est
		// dans un paiement sinon chgt mode paiement
		if (gestionCreditModifModPaiement == null) {
			mandat.setManEtat(EOMandat.mandatVirement);
		}
		lesEcritures.addObject(monEcritureVirement);
		return lesEcritures;
	}

	/**
	 * Recupere le compte de BE associe au compte planco et le renvoie si exercice n'est pas de type tresorerie. Sinon renvoie planco. Passer
	 * l'exercice du mandat (ou de l'OP) à payer
	 * 
	 * @param planco
	 * @param exercice
	 * @return
	 */
	private EOPlanComptable getPlancoBE(final EOPlanComptable planco, final EOExercice exercice) {
		if (EOExercice.EXE_TYPE_TRESORERIE.equals(exercice.exeType())) {
			return planco;
		}
		//		if (planco.pcoCompteBe() == null || planco.pcoCompteBe().trim().length() == 0 || _comptesBE == null) {
		//			return planco;
		//		}
		if (_comptesBE != null && _comptesBE.get(planco) != null) {
			return (EOPlanComptable) _comptesBE.get(planco);
		}
		return planco;

		//return (EOPlanComptable) _comptesBE.get(planco);
	}

}
