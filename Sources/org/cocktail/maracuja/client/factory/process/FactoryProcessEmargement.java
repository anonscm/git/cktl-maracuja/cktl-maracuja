/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 14 juil. 2004
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.EmargementException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEmargementDetail;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOEmargementDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeEmargement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */

public class FactoryProcessEmargement extends FactoryProcess {

	private final FactoryEmargementDetail maFactoryEmargementDetail;

	/**
	 * @param withLog
	 */
	public FactoryProcessEmargement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
		maFactoryEmargementDetail = new FactoryEmargementDetail(withLogs());
	}

	/**
	 * Permet d emarger deux details
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param detailCrediteur
	 * @param detailDebiteur
	 * @param montant
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerEcritureDetailD1C1(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur, EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant)
			throws EmargementException {
		this.trace("");
		this.trace("emargerEcritureDetail");
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, numero, comptabilite, montant);

		// creation du detail emargement
		maFactoryEmargementDetail.creerEmargementDetail(ed, montant, detailCrediteur, detailDebiteur, newEOEmargement, exercice);

		return newEOEmargement;
	}


	/**
	 * Permet d emarger un credit par n debits <br>
	 * attention credit >= somme(debits) <br>
	 * ATTENTION LA SOMME DES ecdResteEmarger DES DEBITS <br>
	 * DOIT ETRE inferieur ou egale au ecdResteEmarger DU CREDIT
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param detailCrediteur
	 * @param lesDetailDebiteur
	 * @param numero
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerEcritureDetailDnC1(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail detailCrediteur, NSArray lesDetailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant)
			throws EmargementException {

		int i = 0;
		this.trace("");
		this.trace("emargerEcritureDetail");
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, numero, comptabilite, montant);

		/*
		 * ATTENTION LA SOMME DES ecdResteEmarger DES DEBITS <= au ecdResteEmarger DU CREDIT
		 */

		// on cree les details
		while (lesDetailDebiteur.count() > i) {
			// creation du detail emargement
			maFactoryEmargementDetail.creerEmargementDetail(ed, ((EOEcritureDetail) lesDetailDebiteur.objectAtIndex(i)).ecdResteEmarger(), detailCrediteur, (EOEcritureDetail) lesDetailDebiteur.objectAtIndex(i), newEOEmargement, exercice);
			i++;
		}
		return newEOEmargement;
	}

	/**
	 * Permet d emarger un debit par n credits <br>
	 * attention debit >= somme(credits) <br>
	 * ATTENTION LA SOMME DES ecdResteEmarger DES CREDITS <br>
	 * DOIT ETRE inferieur ou egale au ecdResteEmarger DU DEBIT
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesDetailCrediteur
	 * @param detailDebiteur
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerEcritureDetailD1Cn(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesDetailCrediteur, EOEcritureDetail detailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant)
			throws EmargementException {

		return this.emargerEcritureDetailDnC1(ed, utilisateur, typeEmargement, exercice, detailDebiteur, lesDetailCrediteur, numero, comptabilite, montant);
	}

	/**
	 * Permet d emarger N credits par N debits <br>
	 * attention somme(credits) = somme(debits)
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesDetailCrediteur
	 * @param lesDetailDebiteur
	 * @param numero
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerEcritureDetailDnCn(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesDetailCrediteur, NSArray lesDetailDebiteur, Integer numero, EOComptabilite comptabilite, BigDecimal montant) throws EmargementException {

		this.trace("");
		this.trace("emargerEcritureDetail");
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, numero, comptabilite, montant);

		/*
		 * ATTENTION LA SOMME DES ecdResteEmarger DES DEBITS = au ecdResteEmarger DES CREDITS
		 */

		// on cree les details lesDetailCrediteur
		int i = 0;
		while (lesDetailCrediteur.count() > i) {
			// creation du detail emargement
			maFactoryEmargementDetail.creerEmargementDetailSourceDestination(ed, ((EOEcritureDetail) lesDetailCrediteur.objectAtIndex(i)).ecdResteEmarger(), (EOEcritureDetail) lesDetailCrediteur.objectAtIndex(i), newEOEmargement, exercice);
			i++;
		}

		// on cree les details lesDetailDebiteur
		i = 0;
		while (lesDetailDebiteur.count() > i) {
			// creation du detail emargement
			maFactoryEmargementDetail.creerEmargementDetailSourceDestination(ed, ((EOEcritureDetail) lesDetailDebiteur.objectAtIndex(i)).ecdResteEmarger(), (EOEcritureDetail) lesDetailDebiteur.objectAtIndex(i), newEOEmargement, exercice);
			i++;
		}
		return newEOEmargement;

	}

	/**
	 * Permet d emarger les details CREDITEURS
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesDetailCrediteurs
	 * @param numero
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerEcritureDetailD0C2(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail credit1, EOEcritureDetail credit2, Integer numero, EOComptabilite comptabilite, BigDecimal montant) throws EmargementException {

		return emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, credit1, credit2, numero, comptabilite, montant);
	}

	public EOEmargement emargerEcritureDetailD0CN(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesCredits, Integer numero, EOComptabilite comptabilite, BigDecimal montant) throws EmargementException {

		this.trace("");
		this.trace("emargerEcritureDetail");
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, numero, comptabilite, montant);
		// on boucle les sur details
		for (int i = 0; i < lesCredits.count(); i++) {
			EOEcritureDetail element = (EOEcritureDetail) lesCredits.objectAtIndex(i);

			maFactoryEmargementDetail.creerEmargementDetailSourceDestination(ed, element.ecdResteEmarger(), element, newEOEmargement, exercice);
		}

		return newEOEmargement;
	}

	public EOEmargement emargerEcritureDetailDNC0(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesDebits, Integer numero, EOComptabilite comptabilite, BigDecimal montant) throws EmargementException {

		return emargerEcritureDetailD0CN(ed, utilisateur, typeEmargement, exercice, lesDebits, numero, comptabilite, montant);

	}

	/**
	 * Permet d emarger les details DEBITEURS
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesDetailDebiteurs
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerEcritureDetailD2C0(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, EOEcritureDetail debit1, EOEcritureDetail debit2, Integer numero, EOComptabilite comptabilite, BigDecimal montant) throws EmargementException {
		return emargerEcritureDetailD1C1(ed, utilisateur, typeEmargement, exercice, debit1, debit2, numero, comptabilite, montant);
	}

	/**
	 * Permet d emarger des ecritures AUTOMATIQUE
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesEcritures
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerLesEcritures(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesEcritures) throws EmargementException {
		return null;
	}

	/**
	 * Permet d emarger les ecritures d un mandat AUTOMATIQUE
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesMandats
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerLesEcrituresDesMandats(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesMandats, EOComptabilite comptabilite) throws EmargementException {

		// creer l emargement
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, new Integer(-1), comptabilite, new BigDecimal(0));

		// boucler sur les mandats
		int i = 0;
		while (i < lesMandats.count()) {
			this.emargerLesEcrituresDuMandat(ed, utilisateur, newEOEmargement, exercice, ((EOMandat) (lesMandats.objectAtIndex(i))));
			i++;
		}
		return newEOEmargement;
	}

	/**
	 * Permet d emarger les ecritures d un titre AUTOMATIQUE
	 * 
	 * @param ed
	 * @param utilisateur
	 * @param typeEmargement
	 * @param exercice
	 * @param lesTitres
	 * @param comptabilite
	 * @return
	 * @throws EmargementException
	 */
	public EOEmargement emargerLesEcrituresDesTitres(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, NSArray lesTitres, EOComptabilite comptabilite) throws EmargementException {

		// creer l emargement
		EOEmargement newEOEmargement = this.creerEmargement(ed, utilisateur, typeEmargement, exercice, new Integer(-1), comptabilite, new BigDecimal(0));

		// boucler sur les mandats
		int i = 0;
		while (i < lesTitres.count()) {
			this.emargerLesEcrituresDuTitre(ed, utilisateur, newEOEmargement, exercice, ((EOTitre) (lesTitres.objectAtIndex(i))));
			i++;
		}
		return newEOEmargement;

	}

	public void numeroterEmargement(EOEditingContext ed, EOEmargement emargement, FactoryNumerotation leFactoryNumerotation) {
		leFactoryNumerotation.getNumeroEOEmargement(ed, emargement);
	}

	/**
	 * Permet d annuler un Emargement <br>
	 * annulation 11 1N N1 NN <br>
	 * ne permet pas d annuler un emargement avec des ecritureDetails de signe differents
	 * 
	 * @param ed
	 * @param emargement
	 */
	public void supprimerEmargement(EOEditingContext ed, EOEmargement emargement) {
		// determiner le type d emargement NN ou autres
		// si source = destination -> NN
		if ((((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(0))).source()).equals(((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(0))).destination())) {
			int i = 0;
			while (i < emargement.emargementDetails().count()) {

				// source puis destination NN => meme champs
				((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).source().setEcdResteEmarger(
						((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).source().ecdResteEmarger().add(((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).emdMontant()));
				i++;
			}
		}
		else {
			int i = 0;
			while (i < emargement.emargementDetails().count()) {

				// source
				((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).source().setEcdResteEmarger(
						((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).source().ecdResteEmarger().add(((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).emdMontant()));

				// destination
				((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).destination().setEcdResteEmarger(
						((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).destination().ecdResteEmarger().add(((EOEmargementDetail) (emargement.emargementDetails().objectAtIndex(i))).emdMontant()));
				i++;
			}
		}
		emargement.setEmaEtat(EOEmargement.emaEtatAnnuler);
	}

	private void emargerLesEcrituresDuMandat(EOEditingContext ed, EOUtilisateur utilisateur, EOEmargement emargement, EOExercice exercice, EOMandat leMandat) throws EmargementException {

		NSMutableArray lesDebits = new NSMutableArray();
		NSMutableArray lesCredits = new NSMutableArray();
		//	BigDecimal montant = new BigDecimal(0);
		int i = 0;
		//int j = 0;

		// on trie les debits et les credits
		while (i < leMandat.mandatDetailEcritures().count()) {
			if (((((EOMandatDetailEcriture) (leMandat.mandatDetailEcritures().objectAtIndex(i)))).ecritureDetail()).ecdSens().equals(sensCredit())) {
				lesCredits.addObject(((((EOMandatDetailEcriture) (leMandat.mandatDetailEcritures().objectAtIndex(i)))).ecritureDetail()));
			}
			else {
				lesDebits.addObject(((((EOMandatDetailEcriture) (leMandat.mandatDetailEcritures().objectAtIndex(i)))).ecritureDetail()));
			}
			i++;
		}

		// emargement automatique
		this.creerEmargementAutomatique(ed, utilisateur, lesDebits, lesCredits, emargement, exercice);
	}

	private void emargerLesEcrituresDuTitre(EOEditingContext ed, EOUtilisateur utilisateur, EOEmargement emargement, EOExercice exercice, EOTitre leTitre) throws EmargementException {

		NSMutableArray lesDebits = new NSMutableArray();
		NSMutableArray lesCredits = new NSMutableArray();
		//BigDecimal montant = new BigDecimal(0);
		int i = 0;
		//int j = 0;

		// on trie les debits et les credits
		while (i < leTitre.titreDetailEcritures().count()) {
			if (((((EOTitreDetailEcriture) (leTitre.titreDetailEcritures().objectAtIndex(i)))).ecritureDetail()).ecdSens().equals(sensCredit())) {
				lesCredits.addObject(((((EOTitreDetailEcriture) (leTitre.titreDetailEcritures().objectAtIndex(i)))).ecritureDetail()));
			}
			else {
				lesDebits.addObject(((((EOTitreDetailEcriture) (leTitre.titreDetailEcritures().objectAtIndex(i)))).ecritureDetail()));
			}
			i++;
		}
		//      emargement automatique
		this.creerEmargementAutomatique(ed, utilisateur, lesDebits, lesCredits, emargement, exercice);
	}

	protected EOEmargement creerEmargement(EOEditingContext ed, EOUtilisateur utilisateur, EOTypeEmargement typeEmargement, EOExercice exercice, Integer numero, EOComptabilite comptabilite, BigDecimal montant) {
		EOEmargement newEOEmargement = (EOEmargement) Factory.instanceForEntity(ed, EOEmargement.ENTITY_NAME);
		ed.insertObject(newEOEmargement);
		newEOEmargement.setEmaDate(getDateJourComptable());
		if (numero == null) {
			numero = new Integer(0);
		}

		newEOEmargement.setEmaNumero(numero);
		newEOEmargement.setEmaMontant(montant);
		newEOEmargement.setEmaEtat(EOEmargement.emaEtatValide);

		newEOEmargement.setUtilisateurRelationship(utilisateur);
		newEOEmargement.setTypeEmargementRelationship(typeEmargement);
		newEOEmargement.setExerciceRelationship(exercice);
		newEOEmargement.setComptabiliteRelationship(comptabilite);
		return newEOEmargement;
	}

	private void creerEmargementAutomatique(EOEditingContext ed, EOUtilisateur utilisateur, NSArray lesDebits, NSArray lesCredits, EOEmargement emargement, EOExercice exercice) {
		BigDecimal montant = new BigDecimal(0);
		trace("les debits = " + lesDebits);
		trace("lesCredits = " + lesCredits);
		// on cherche les credits de meme imputation que le debit courant
		int i = 0;
		int j = 0;
		while (i < lesDebits.count()) {
			j = 0;
			while (j < lesCredits.count()) {
				if (((EOEcritureDetail) lesDebits.objectAtIndex(i)).planComptable().equals(((EOEcritureDetail) lesCredits.objectAtIndex(j)).planComptable()) && Factory.different(((EOEcritureDetail) lesCredits.objectAtIndex(j)).ecdResteEmarger(), new BigDecimal(0))) {
					if (Factory.inferieurOuEgal(((EOEcritureDetail) lesCredits.objectAtIndex(j)).ecdResteEmarger(), ((EOEcritureDetail) lesDebits.objectAtIndex(i)).ecdResteEmarger())) {
						montant = ((EOEcritureDetail) lesCredits.objectAtIndex(j)).ecdResteEmarger();
					}
					else {
						montant = ((EOEcritureDetail) lesDebits.objectAtIndex(i)).ecdResteEmarger();
					}

					// on a un debit et un credit de meme imputation dans le credit un reste a emarger != de 0
					// ATTENTION DANS LE MEME EXERCICE SINON ON EMARGE PAS !!!!!!!!
					if (((EOEcritureDetail) lesCredits.objectAtIndex(j)).ecriture().exercice().equals(((EOEcritureDetail) lesCredits.objectAtIndex(j)).ecriture().exercice())) {
						maFactoryEmargementDetail.creerEmargementDetail(ed, montant, ((EOEcritureDetail) lesCredits.objectAtIndex(j)), ((EOEcritureDetail) lesDebits.objectAtIndex(i)), emargement, exercice);
					}
				}
				j++;
			}
			i++;
		}

	}

}
