/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 17 juil. 2004
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.MandatExceptions;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;


/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */
public class FactoryProcessVisaModifMandat extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessVisaModifMandat(boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	/**
	 * Permet de modifier le mode de paiement du mandat
	 * 
	 * @param ed
	 *            EditingContext de travail
	 * @param utilisateur
	 *            utilisateur qui modifie le mandat
	 * @param leMandat
	 *            Mandat a modifier
	 * @param leNouveauModeDePaiement
	 *            nouveau mode paiement
	 * @throws MandatExceptions
	 */
	public void modifierModeDePaiement(EOEditingContext ed, EOUtilisateur utilisateur, EOMandat leMandat, EOModePaiement leNouveauModeDePaiement) throws MandatExceptions {

		// mandat non vise ?
		if (leMandat.manEtat().equals(EOMandat.mandatVise)) {
			throw MandatExceptions.impossible_modifier_mandat_vise();
		}

		if (leMandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw MandatExceptions.impossible_modifier_mandat_annule();
		}

		// y a t il modification de la classe 4 ??
		// leMandat.addObjectToBothSidesOfRelationshipWithKey(leNouveauModeDePaiement,"modePaiement");
		leMandat.setModePaiementRelationship(leNouveauModeDePaiement);

		// modifier le brouillard

		// modifier le mandat
	}

	/**
	 * Permet de modifier une contre partie de classe 4 <br>
	 * du brouillard de visa du mandat.
	 * 
	 * @param ed
	 *            EditingContext de travail
	 * @param utilisateur
	 *            utilisateur qui modifie le mandat
	 * @param leMandat
	 *            Mandat a modifier
	 * @param leMandatBrouillard
	 *            le mandat brouillard a modifier
	 * @param leNouveauPlanComptable
	 *            le nouveau compte d imputation du mandat brouillard
	 * @throws MandatExceptions
	 */
	public void modifierMandatBrouillard(EOEditingContext ed, EOUtilisateur utilisateur, EOMandat leMandat, EOMandatBrouillard leMandatBrouillard, EOPlanComptable leNouveauPlanComptable, EOGestion gestion, BigDecimal leMontant) throws MandatExceptions {

		this.verifierModification(ed, leMandatBrouillard, utilisateur, leMandat, leMandat.modePaiement());

		leMandatBrouillard.setPlanComptableRelationship(leNouveauPlanComptable);
		leMandatBrouillard.setGestionRelationship(gestion);

		leMandatBrouillard.setMabMontant(leMontant);
	}

	/**
	 * Permet de creer un nouveau mandatBrouillard
	 * 
	 * @param ed
	 *            EditingContext de travail
	 * @param utilisateur
	 *            utilisateur qui modifie le mandat
	 * @param execice
	 *            exercice de travail
	 * @param gestion
	 *            code gestion
	 * @param lePlanComptable
	 *            le compte d imputation du mandat brouillard
	 * @param leMandat
	 *            Mandat a modifier
	 * @param leSens
	 *            D Debit ou C Credit
	 * @param leMontant
	 *            le montant du mandat brouillard
	 * @return
	 * @throws MandatExceptions
	 */
	public EOMandatBrouillard creerMandatBrouillard(final EOEditingContext ed, final EOUtilisateur utilisateur, final EOExercice execice, final EOGestion gestion, final EOPlanComptable lePlanComptable, final EOMandat leMandat, final String uneMabOperation, final String leSens,
			final BigDecimal leMontant) throws MandatExceptions {

		final EOMandatBrouillard nouveauMandatBrouillard = (EOMandatBrouillard) FactoryProcess.instanceForEntity(ed, EOMandatBrouillard.ENTITY_NAME);

		nouveauMandatBrouillard.setExerciceRelationship(execice);
		nouveauMandatBrouillard.setPlanComptableRelationship(lePlanComptable);
		nouveauMandatBrouillard.setGestionRelationship(gestion);

		nouveauMandatBrouillard.setMabMontant(leMontant);
		nouveauMandatBrouillard.setMabSens(leSens);
		nouveauMandatBrouillard.setMabOperation(uneMabOperation);

		try {
			this.verifierCreation(ed, nouveauMandatBrouillard, utilisateur, execice, gestion, lePlanComptable, leMandat);

			ed.insertObject(nouveauMandatBrouillard);

			nouveauMandatBrouillard.validateObjectMetier();

			//			leMandat.addObjectToBothSidesOfRelationshipWithKey(nouveauMandatBrouillard, "mandatBrouillards");
			leMandat.addToMandatBrouillardsRelationship(nouveauMandatBrouillard);

			return nouveauMandatBrouillard;
		} catch (ValidationException e) {
			e.printStackTrace();
			System.out.println(nouveauMandatBrouillard);
			return null;
		}

	}

	/**
	 * Permet de modifier une contre partie de classe 4 <br>
	 * du brouillard de visa du mandat et le nouveau mode paiement.
	 * 
	 * @param ed
	 *            EditingContext de travail
	 * @param utilisateur
	 *            utilisateur qui modifie le mandat
	 * @param leMandat
	 *            Mandat a modifier
	 * @param leMandatBrouillard
	 *            le mandat brouillard a modifier
	 * @param leNouveauPlanComptable
	 *            nouveau compte d imputation
	 * @param leNouveauModeDePaiement
	 *            nouveau mode de paiement
	 * @throws MandatExceptions
	 */
	public void modifierMandatBrouillardEtModeDePaiement(EOEditingContext ed, EOUtilisateur utilisateur, EOMandat leMandat, EOMandatBrouillard leMandatBrouillard, EOPlanComptable leNouveauPlanComptable, EOModePaiement leNouveauModeDePaiement, EOGestion gestion, BigDecimal leMontant)
			throws MandatExceptions {

		// possible si le mode de paiement ne modifie pas la contrepartie !

		this.modifierModeDePaiement(ed, utilisateur, leMandat, leNouveauModeDePaiement);

		this.modifierMandatBrouillard(ed, utilisateur, leMandat, leMandatBrouillard, leNouveauPlanComptable, gestion, leMontant);

	}

	/**
	 * Permet de supprimer un mandat brouillard (passe mabOperation a
	 * EOMandatBrouillard.visaAnnuler)
	 * 
	 * @param ed
	 *            EditingContext de travail
	 * @param leMandatBrouillard
	 *            a supprimer
	 */
	public void supprimerMandatBrouillard(EOEditingContext ed, EOMandatBrouillard leMandatBrouillard) {

		if (leMandatBrouillard.planComptable().equals(leMandatBrouillard.mandat().planComptable())) {
			throw MandatExceptions.impossible_modifier_imputation();
		}
		leMandatBrouillard.setMabOperation(EOMandatBrouillard.VISA_ANNULER);
	}

	private void verifierModification(EOEditingContext ed, EOMandatBrouillard leMandatBrouillard, EOUtilisateur utilisateur, EOMandat leMandat, EOModePaiement leNouveauModeDePaiement) throws MandatExceptions {

		// mandat non vise ?
		if (leMandat.manEtat().equals(EOMandat.mandatVise)) {
			throw MandatExceptions.impossible_modifier_mandat_vise();
		}

		if (leMandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw MandatExceptions.impossible_modifier_mandat_annule();
		}

		// leNouveauModeDePaiement existe ?

		// utilisateur autorise ?

		// if (leMandatBrouillard.mabSens().equals(
		// sensDebit()))
		// throw MandatExceptions.impossible_modifier_debit();

		if (leMandatBrouillard.planComptable().equals(leMandat.planComptable())) {
			throw MandatExceptions.impossible_modifier_imputation();
		}

	}

	private void verifierCreation(EOEditingContext ed, EOMandatBrouillard leMandatBrouillard, EOUtilisateur utilisateur, EOExercice execice, EOGestion gestion, EOPlanComptable lePlanComptable, EOMandat leMandat) throws MandatExceptions {
		// mandat non vise ?
		if (leMandat.manEtat().equals(EOMandat.mandatVise)) {
			throw MandatExceptions.impossible_modifier_mandat_vise();
		}

		if (leMandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw MandatExceptions.impossible_modifier_mandat_annule();
		}

		if (leMandatBrouillard.planComptable().equals(leMandat.planComptable())) {
			throw MandatExceptions.impossible_modifier_imputation();
		}

		// if ( leMandatBrouillard.mabSens().equals(sensDebit()))
		// throw MandatExceptions.impossible_modifier_debit();

		// leNouveauModeDePaiement existe ?

		// utilisateur autorise ?

	}

}
