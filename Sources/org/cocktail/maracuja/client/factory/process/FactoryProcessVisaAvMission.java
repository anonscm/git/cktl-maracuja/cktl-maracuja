/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.metier.EOVisaAvMission;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org Visa des avances de mission. Inclue la génération de l'OP, le rejet.
 */
public class FactoryProcessVisaAvMission extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessVisaAvMission(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	/**
	 * Génère et associe un OP à partir des infos de l'avancede la mission.
	 * 
	 * @param ed
	 * @param avMission
	 * @param comptabilite
	 * @param exercice
	 * @param utilisateur
	 * @param isVisaContrePartieAgence Indique si la contrepartie de la depsne va à l'agence ou a la composante : Si agence, le débit est passé sur le
	 *            code de l'agence sinon sur celui de la composante.
	 * @return
	 * @throws Exception
	 */
	public EOOrdreDePaiement genererOP(EOEditingContext ed, EOVisaAvMission avMission, EOComptabilite comptabilite, EOExercice exercice, EOUtilisateur utilisateur) throws Exception {

		//Vérifier si un OP est déjà affecté
		if (avMission.ordreDePaiement() != null) {
			throw new Exception("Un ordre de paiement est déjà affecté à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + ".");
		}

		//Verifier si mp virement on a un rib
		if (EOModePaiement.MODDOM_VIREMENT.equals(avMission.modePaiementAvance().modDom())) {
			if (avMission.rib() == null || !EORib.RIB_VALIDE.equals(avMission.rib().ribValide())) {
				throw new Exception("Le rib est non défini ou invalide.");
			}
		}

		if (avMission.modePaiementRegul().modContrepartieGestion() == null) {
			throw new Exception("Vous devez spécifiez comment la contrepartie visa du mode de paiement " + avMission.modePaiementRegul().modCode() + " doit être gérée (AGENCE ou  COMPOSANTE). Allez dans l'administration des modes de paiement pour spécifier cette information.");
		}

		String lib = "AVANCE MISSION n°" + avMission.mission_misNumero().intValue() + " du " + ZConst.FORMAT_DATESHORT.format(avMission.mission().misDebut()) + " au " + ZConst.FORMAT_DATESHORT.format(avMission.mission().misFin()) + " - " + avMission.mission_misMotif();
		if (lib.length() > 1999) {
			lib = lib.substring(0, 1999);
		}

		final FactoryProcessOrdrePaiement factoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(withLogs(), getDateJourComptable());
		final EOOrdreDePaiement odp = factoryProcessOrdrePaiement.creerOrdreDePaiement(ed, comptabilite, exercice, avMission.fournisseur(), avMission.modePaiementAvance(), avMission.vamMontant(), lib, avMission.fournisseur().getNomAndPrenomAndCode(), new BigDecimal(0), avMission.vamMontant(),
				new BigDecimal(0), null, avMission.rib(), utilisateur, EOOrdreDePaiement.etatValide, getDateJourComptable(), avMission.organ(), null);

		if (odp == null) {
			throw new Exception("Erreur lors de la création de l'ordre de paiement pour l'avance de la mission n°" + avMission.mission().misNumero().intValue() + ".");
		}

		//créer les brouillards
		final EOPlanComptable pcoDebit = avMission.modePaiementRegul().planComptableVisa();
		final EOPlanComptable pcoCredit = avMission.modePaiementAvance().planComptablePaiement();

		boolean isVisaContrePartieAgence = !EOModePaiement.CONTREPARTIE_COMPOSANTE.equals(avMission.modePaiementRegul().modContrepartieGestion());
		//		boolean isSacd = FinderGestion.gestionIsSacd(avMission.gestion(), avMission.exercice());
		boolean isSacd = avMission.gestion().isSacd(avMission.exercice());

		final EOGestion gestionCredit = comptabilite.gestion();
		final EOGestion gestionDebit = (isSacd || !isVisaContrePartieAgence ? avMission.gestion() : comptabilite.gestion());

		if (lib.length() > 199) {
			lib = lib.substring(0, 199);
		}

		final FactoryProcessOrdrePaiementBrouillard factoryProcessOrdrePaiementBrouillard = new FactoryProcessOrdrePaiementBrouillard(withLogs(), getDateJourComptable());
		final EOOrdreDePaiementBrouillard debit = factoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardDebit(ed, odp.odpHt(), odp, gestionDebit, pcoDebit, utilisateur);
		//        debit.setOdbLibelle(lib); // pas de brouillard specifique
		final EOOrdreDePaiementBrouillard credit = factoryProcessOrdrePaiementBrouillard.creerOrdreDePaiementBrouillardCredit(ed, odp.odpHt(), odp, gestionCredit, pcoCredit, utilisateur);
		//        credit.setOdbLibelle(lib);

		//associer l'op a l'avance
		avMission.setOrdreDePaiementRelationship(odp);

		return odp;
	}

	/**
	 * Valide l'OP associé à l'avance de la mission. Cet OP doit être en attente. Des controles de cohérences sont effectués.
	 * 
	 * @param ed
	 * @param avMission
	 * @param utilisateur
	 * @throws Exception
	 */
	public void validerOP(EOEditingContext ed, EOVisaAvMission avMission, EOUtilisateur utilisateur) throws Exception {

		if (avMission.ordreDePaiement() == null) {
			throw new Exception("Aucun ordre de paiement n'est affecté à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + ".");
		}
		if (EOOrdreDePaiement.etatValide.equals(avMission.ordreDePaiement().odpEtat())) {
			return;
		}
		final FactoryProcessOrdrePaiement factoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(withLogs(), getDateJourComptable());
		factoryProcessOrdrePaiement.validerOrdreDePaiement(ed, avMission.ordreDePaiement(), utilisateur);
	}

	/**
	 * Annule l'OP et le detache de l'avance de mission. l'OP doit etre EN ATTENTE ou valide. Impossible si OP est deja vise ou paye.
	 * 
	 * @param ed
	 * @param avMission
	 * @param utilisateur
	 * @throws Exception
	 */
	public void annulerOP(EOEditingContext ed, EOVisaAvMission avMission, EOUtilisateur utilisateur) throws Exception {

		if (avMission.ordreDePaiement() == null) {
			throw new Exception("Aucun ordre de paiement n'est affecté à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + ".");
		}

		if (EOOrdreDePaiement.etatVise.equals(avMission.ordreDePaiement().odpEtat()) || EOOrdreDePaiement.etatVirement.equals(avMission.ordreDePaiement().odpEtat())) {
			throw new Exception("L'ordre de paiement n° " + avMission.ordreDePaiement().odpNumero().intValue() + " associé à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + " doit être validé, mais vous n''avez pas les droits pour le faire.");
		}

		final FactoryProcessOrdrePaiement factoryProcessOrdrePaiement = new FactoryProcessOrdrePaiement(withLogs(), getDateJourComptable());
		factoryProcessOrdrePaiement.annulerOrdreDePaiement(ed, avMission.ordreDePaiement(), utilisateur);
		avMission.setOrdreDePaiementRelationship(null);

	}

	/**
	 * Passe l'état de la demande à "ACCEPTE". L'OP associé doit etre VALIDE a la fin du traitement (donc soit il l'est déjà, soit il le devient. Pour
	 * cela l'utilisateur doit avoir les droits).
	 * 
	 * @param ed
	 * @param avMission
	 * @param utilisateur
	 * @throws Exception
	 */
	public void accepterAvMission(EOEditingContext ed, EOVisaAvMission avMission, EOComptabilite comptabilite, EOExercice exercice, EOUtilisateur utilisateur, boolean canCreerOp, boolean canValiderOP) throws Exception {

		if (avMission.ordreDePaiement() == null) {
			if (!canCreerOp) {
				throw new Exception("Aucun ordre de paiement n'est affecté à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + ", vous n''avez pas les droits pour le créer.");
			}
			genererOP(ed, avMission, comptabilite, exercice, utilisateur);
		}

		if (EOOrdreDePaiement.etatOrdonnateur.equals(avMission.ordreDePaiement().odpEtat())) {
			if (!canValiderOP) {
				throw new Exception("L'ordre de paiement associé à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + " doit être validé, mais vous n''avez pas les droits pour le faire.");
			}
		}
		validerOP(ed, avMission, utilisateur);

		//Si OP non valide a ce niveau, ca marche pas
		if (!EOOrdreDePaiement.etatValide.equals(avMission.ordreDePaiement().odpEtat())) {
			throw new Exception("L'ordre de paiement associé à l'avance de la mission n°" + avMission.mission().misNumero().intValue() + " devrait être à l'état VALIDE.");
		}

		avMission.setTypeEtatRelationship(ZFinderEtats.etat_ACCEPTE());
		avMission.setVamDateVisa(Factory.getNow());
		avMission.setUtilisateurValideurRelationship(utilisateur);

	}

	/**
	 * Passe l'état de la demande à "REJETE". S'il y a déjà un OP associé, celui-ci doit etre annule en meme temps et detache de l'avance. Donc si OP
	 * est deja VISE ou PAYE le rejet est impossible.
	 * 
	 * @param ed
	 * @param avMission
	 * @param utilisateur
	 * @throws Exception
	 */
	public void rejeterAvMission(EOEditingContext ed, EOVisaAvMission avMission, EOUtilisateur utilisateur) throws Exception {

		if (avMission.ordreDePaiement() != null) {
			annulerOP(ed, avMission, utilisateur);
		}
		avMission.setTypeEtatRelationship(ZFinderEtats.etat_REJETE());
		avMission.setVamDateVisa(Factory.getNow());
		avMission.setUtilisateurValideurRelationship(utilisateur);
	}

}
