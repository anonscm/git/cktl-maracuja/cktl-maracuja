/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *  
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.exception.VisaException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryTitreDetailEcriture;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author frivalla To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 * @deprecated
 */
public class FactoryProcessPaiementTitre extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessPaiementTitre(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	//    public NSArray viserVirementTitres(
	//            EOEditingContext ed,
	//            NSArray lesTitres,
	//            EOUtilisateur utilisateur,
	//            String traitement
	//            ) throws Exception
	//    {
	//
	//        EOTitre  tmpTitre = null;
	//        EOTypeJournal typeJournal = FinderJournalEcriture.leTypeJournalPaiement(ed);        
	////        EOVirement leVirement=null;
	//        EOOperationTresorerie leOPT= null;
	//        
	//        NSMutableArray lesEcritures=new  NSMutableArray();
	//        boolean sacd;
	//        int i = 0;
	//        
	//        // VERIFIER PAS DE DEPENSE OU PAS VISE !
	//        i=0;
	//        while (i < lesTitres.count()) {       
	//            if ( EOTitre.titreAnnule.equals( ((EOTitre)(lesTitres.objectAtIndex(i))).titEtat()))
	//                throw new FactoryException(EOTitre.problemeAnnule);
	//        
	//            if ( EOTitre.titreValide.equals( ((EOTitre)(lesTitres.objectAtIndex(i))).titEtat()))
	//                throw new FactoryException(EOTitre.problemeNonVise);
	//            i++;
	//        }
	//        
	//    
	//        // recup du type de journal
	//         i = 0;
	//        while (i < lesTitres.count()) {
	//    //recup des informations.        
	//            tmpTitre = ((EOTitre) lesTitres.objectAtIndex(i));
	//        
	//            if (EOTitre.titreVise.equals(tmpTitre.titEtat())) {
	//                
	//                //Modif code gestion exer
	//                final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(tmpTitre.gestion(), tmpTitre.exercice());
	//
	//                
	//                
	////                if (tmpTitre.gestion().planComptable185() != null)
	//                if (planco185 != null)
	//                    lesEcritures.addObjectsFromArray(this.ecritureTitreGestionTitre(
	//                            ed,
	//                            tmpTitre,
	//                            typeJournal,
	//                            utilisateur,
	//                            true));
	//                else
	//                    lesEcritures.addObjectsFromArray( this.ecritureTitreGestionTitre(
	//                            ed,
	//                            tmpTitre,
	//                            typeJournal,
	//                            utilisateur,
	//                            false));
	//            }
	//         i++;
	//        }
	//        return lesEcritures;
	//    }

	//    public  NSArray ecritureTitreModifierCredit(
	//            EOEditingContext ed,
	//            EOTitre titre,
	//            EOTypeJournal typeJournal,
	//            EOUtilisateur utilisateur,
	//            String libelle,
	//            EOPlanComptable credit,
	//            boolean sacd)
	//    
	//    {
	//        
	//        if (!titre.bordereau().typeBordereau().equals(EOTypeBordereau.TypeBordereauBTODP))
	//            throw new FactoryException ("TRAITEMENT RESERVER AUX ORDRES DE PAIEMENT (TITRE)");
	//        
	//        // a revoir.....
	//        if (true)
	//        throw new FactoryException ("PAS DE MODIFICATION DES ORDRES DE PAIEMENT (TITRE)");
	//        
	//        return null;
	//            //return this.ecritureTitre(ed,titre,typeJournal,utilisateur,libelle,credit,sacd);  
	//    }

	private NSArray ecritureTitreGestionTitre(
			EOEditingContext ed,
			EOTitre titre,
			EOTypeJournal typeJournal,
			EOUtilisateur utilisateur,
			boolean sacd) throws Exception

	{
		//        if (!titre.bordereau().typeBordereau().equals(EOTypeBordereau.TypeBordereauBTODP))
		//            throw new FactoryException ("TRAITEMENT RESERVER AUX ORDRES DE PAIEMENT (TITRE)");
		//    
		//String libelle =     "PAIEMENT titre"+titre.titNumero()+" Bord. "+titre.bordereau().borNum();
		//  EOPlanComptable credit = titre.modePaiement().planComptablePaiement();

		return creerLesEcrituresDuTitreAuPaiementODP(ed, titre, utilisateur);
		//return this.ecritureTitre(ed,titre,typeJournal,utilisateur,libelle,credit,sacd);  
	}

	private NSMutableArray creerLesEcrituresDuTitreAuPaiementODP(
			EOEditingContext ed,
			EOTitre leTitre,
			EOUtilisateur utilisateur) throws Exception {

		//sNSMutableArray lesEcritures = new NSMutableArray();
		FactoryProcessJournalEcriture maFactoryJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());

		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());
		FactoryTitreDetailEcriture maFactoryTitreDetailEcriture = new FactoryTitreDetailEcriture(withLogs());

		this.trace("Avant newEOEcriture : ");
		this.trace(" - titre = " + leTitre);
		this.trace("- compta = " + leTitre.bordereau().gestion().comptabilite());
		this.trace(" - origine = " + leTitre.origine());
		//       this.trace(" - type journal. = " + getTypeJournal() );        

		// verifications
		if (leTitre.titEtat().equals(
				EOTitre.titreAnnule))
			throw new VisaException(VisaException.titreDejaAnnule);

		// creation de l ecriture
		EOEcriture newEOEcriture = maFactoryJournalEcriture.creerEcriture(
				ed,
				getDateJourComptable(),
				"Paiement Tit. Num . " + leTitre.titNumero().toString() + " Bord. Num. "
						+ leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion(),
				new Integer(0),
				null,
				(EOComptabilite) leTitre.bordereau().gestion().comptabilite(),
				(EOExercice) leTitre.exercice(),
				(EOOrigine) leTitre.origine(),
				FinderJournalEcriture.leTypeJournalPaiement(ed),
				FinderJournalEcriture.leTypeOperationPaiement(ed),
				utilisateur);

		ed.insertObject(newEOEcriture);

		this.trace("newEOEcriture " + newEOEcriture);

		// recup des titreBrouillard du titre
		NSArray lesBrouillons = FactoryProcessVisaTitre.fetchArray(
				ed,
				EOTitreBrouillard.ENTITY_NAME,
				"titre = %@",
				new NSArray(new Object[] {
						leTitre
				}),
				null,
				true);

		this.trace("lesBrouillons " + lesBrouillons);

		if (lesBrouillons.count() == 0)
			throw new VisaException(VisaException.pasDeTitreBrouillard);

		// pour chaque ligne du brouillon on cree un detail ecriture
		int i = 0;
		while (i < lesBrouillons.count()) {
			((EOTitreBrouillard) lesBrouillons.objectAtIndex(i)).tibSens();
			((EOTitreBrouillard) lesBrouillons.objectAtIndex(i)).tibMontant();
			EOEcritureDetail newEOEcritureDetail = maFactoryEcritureDetail
					.creerEcritureDetail(
							ed,
							null,
							new Integer(0),
							leTitre.fournisseur().adrNom() + " " + "Vir. Tit. Num . " + leTitre.titNumero().toString() + " Bord. Num. "
									+ leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion(),
							((EOTitreBrouillard) lesBrouillons.objectAtIndex(i))
									.tibMontant(),
							null,
							((EOTitreBrouillard) lesBrouillons.objectAtIndex(i))
									.tibMontant(),
							null,
							((EOTitreBrouillard) lesBrouillons.objectAtIndex(i))
									.tibSens(),
							newEOEcriture,
							(EOExercice) leTitre.exercice(),
							(EOGestion) ((EOTitreBrouillard) lesBrouillons
									.objectAtIndex(i)).gestion(),
							(EOPlanComptable) ((EOTitreBrouillard) lesBrouillons
									.objectAtIndex(i)).planComptable());

			this.trace("newEOEcritureDetail " + newEOEcritureDetail);

			if (newEOEcritureDetail == null)
				throw new VisaException(VisaException.problemeCreationEcriture);

			// on cree le EOTitreDetailEcriture
			EOTitreDetailEcriture newEOTitreDetailEcriture = maFactoryTitreDetailEcriture
					.creerTitreDetailEcriture(
							ed,
							Factory.getNow(),
							EOTitreDetailEcriture.TDE_ORIGINE_VISA,
							newEOEcritureDetail,
							(EOExercice) newEOEcriture.exercice(),
							leTitre,
							(EOOrigine) newEOEcriture.origine(),
							null);

			this.trace("newEOTitreDetailEcriture " + newEOTitreDetailEcriture);
			if (newEOTitreDetailEcriture == null)
				throw new VisaException(
						VisaException.problemeCreationDetailEcriture);

			ed.insertObject(newEOTitreDetailEcriture);
			i++;
		}
		return (NSMutableArray) maFactoryJournalEcriture.determinerLesEcrituresSACD(ed, newEOEcriture);
	}

	/*
	 * private NSArray ecritureTitre( EOEditingContext ed, EOTitre titre, EOTypeJournal typeJournal, EOUtilisateur utilisateur, String libelle,
	 * EOPlanComptable creditModePaiement, boolean sacd){
	 * 
	 * //EOPlanComptable creditModePaiement = null; EOPlanComptable creditRetenue = null;
	 * 
	 * EOGestion gestionCredit = null;
	 * 
	 * EORetenueDetail tmpRetenue = null;
	 * 
	 * EOEcriture monEcritureVirement=null; EOEcritureDetail monEcritureDetail =null;
	 * 
	 * BigDecimal montantRetenue = new BigDecimal(0); BigDecimal montantCredit = new BigDecimal(0);
	 * 
	 * int i = 0; int j = 0;
	 * 
	 * if (titre.titEtat().equals((EOTitre.titrePaye))) throw new FactoryException(EOTitre.problemeDejaPaye);
	 * 
	 * if (titre.titEtat().equals((EOTitre.titreAnnule))) throw new FactoryException(EOTitre.problemeAnnule);
	 * 
	 * 
	 * FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs());
	 * 
	 * FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());
	 * 
	 * FactoryTitreDetailEcriture maFactoryTitreDetailEcriture = new FactoryTitreDetailEcriture(withLogs());
	 * 
	 * NSMutableArray lesEcritures = new NSMutableArray();
	 * 
	 * // recup du compte de credit //creditModePaiement = titre.modePaiement().planComptablePaiement(); this.trace("-----------------------------");
	 * this.trace("recup du compte de credit au visa "+creditModePaiement.pcoNum());
	 * 
	 * // recup du code gestion du credit gestionCredit = titre.gestion().comptabilite().gestion(); this.trace("-----------------------------");
	 * this.trace("  recup du code gestion du credit au visa"+gestionCredit.gesCode());
	 * 
	 * 
	 * // CREATION DE LECRITURE : monEcritureVirement = maFactoryProcessJournalEcriture.creerEcriture( ed, Factory.getNow(), libelle, new Integer(0),
	 * null, titre.bordereau().gestion().comptabilite(), titre.exercice(), titre.origine(), typeJournal,
	 * FinderJournalEcriture.leTypeOperationPaiement(ed), utilisateur );
	 * 
	 * this.trace("-----------------------------"); this.trace(" CREATION DE L ECRITURE "+monEcritureVirement);
	 * 
	 * //recup des ecritures de visa credit du visa ! i = 0;
	 * 
	 * while (i < titre.titreDetailEcritures().count()) { // si c est un credit on le met dans le tableau pour generer un debit equivalent si reste a
	 * emarger diff de zero ( a zero si modif de mode de paiement) if (this.sensCredit().equals(((EOTitreDetailEcriture)
	 * titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().ecdSens()) && ((EOTitreDetailEcriture)
	 * titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().ecriture()!= monEcritureVirement && Factory.different(((EOTitreDetailEcriture)
	 * titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().ecdResteEmarger(),new BigDecimal(0))) { // CREATION DU DETAIL ECRITURE DEBIT
	 * GESTION monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail( ed, null, new Integer(i), "", ((EOTitreDetailEcriture)
	 * titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().ecdMontant(), "", ((EOTitreDetailEcriture)
	 * titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().ecdMontant(), null, sensDebit(), monEcritureVirement,
	 * monEcritureVirement.exercice(), ((EOTitreDetailEcriture) titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().gestion(),
	 * ((EOTitreDetailEcriture) titre.titreDetailEcritures().objectAtIndex(i)).ecritureDetail().planComptable() );
	 * 
	 * this.trace("-----------------------------"); this.trace(" CREATION DU DETAILL ECRITURE DEBIT GESTION "+monEcritureDetail);
	 * 
	 * // CREATION DU titreDetailEcriture maFactoryTitreDetailEcriture.creerTitreDetailEcriture( ed, Factory.getNow(),
	 * EOTitreDetailEcriture.tdeOrigineVirement, monEcritureDetail, monEcritureDetail.exercice(), titre, titre.origine() );
	 * 
	 * this.trace("-----------------------------"); this.trace(" CREATION DU titre DETAIL ECRITURE DEBIT GESTION ");
	 * 
	 * } i++; }
	 * 
	 * if (sacd) { this.trace("-----------------------------"); this.trace("SACD");
	 * 
	 * lesEcritures.addObject(monEcritureVirement); //Creation d un detail somme des debit en Credit 185 SACD // CREATION DU DETAIL ECRITURE CREIT
	 * SACD 185 monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail( ed, null, new Integer(i), "", titre.titTtc(), "", titre.titTtc(),
	 * null, sensCredit(), monEcritureVirement, monEcritureVirement.exercice(), titre.gestion(), FinderJournalEcriture.planComptable185(ed) );
	 * 
	 * this.trace("-----------------------------"); this.trace(" DETAIL SACD C 185"+monEcritureDetail);
	 * this.trace(" DETAIL ECRITURE SACD FINALE"+monEcritureVirement.detailEcriture());
	 * 
	 * //creation d une nouvelle ecriture // CREATION DE LECRITURE : monEcritureVirement = maFactoryProcessJournalEcriture.creerEcriture( ed,
	 * Factory.getNow(), libelle, new Integer(0), " automatique", titre.bordereau().gestion().comptabilite(), titre.exercice(), titre.origine(),
	 * typeJournal, titre.origine().typeOperation(), utilisateur );
	 * 
	 * this.trace("-----------------------------"); this.trace(" CREATION DE LA NOUVELLE ECRITURE"+monEcritureVirement);
	 * 
	 * 
	 * // creation d un nouveau detail somme des debit Debit comptabilite monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail( ed, null,
	 * new Integer(i), "", titre.titTtc(), "", titre.titTtc(), null, sensDebit(), monEcritureVirement, monEcritureVirement.exercice(),
	 * titre.bordereau().gestion().comptabilite().gestion(), titre.gestion().planComptable185() );
	 * 
	 * this.trace("-----------------------------"); this.trace(" CREATION DE LA NOUVELLE ECRITURE "+monEcritureVirement);
	 * this.trace(" DETAIL ECRITURE AGENCE DEBIT 185"+monEcritureVirement.detailEcriture());
	 * 
	 * }
	 * 
	 * // la retenue ?? NSMutableArray lesDepensesAvecRetenue = new NSMutableArray();
	 * 
	 * i = 0; j = 0; // on scanne les depense while (i < titre.recettes().count()) { // on recupere les retenues d une depense while (j < ((EORecette)
	 * (titre.recettes().objectAtIndex(i))) .retenueDetails().count()) {
	 * 
	 * this.trace("-----------------------------"); this.trace(" retenue "+tmpRetenue+" depense : "+tmpRetenue.recette());
	 * 
	 * tmpRetenue = ((EORetenueDetail) (((EORecette) (titre .recettes().objectAtIndex(i))).retenueDetails() .objectAtIndex(j)));
	 * 
	 * montantRetenue = tmpRetenue.redMontant(); creditRetenue = (EOPlanComptable) tmpRetenue.retenue() .typeRetenue().planComptable();
	 * 
	 * //montantCredit = montantCredit - montantRetenue; montantCredit.add(montantRetenue.negate()); // CREATION DU DETAIL ECRITURE CREDIT RETENUE
	 * monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail( ed, null, new Integer(i+1000), "", montantRetenue, "", montantRetenue, null,
	 * sensCredit(), monEcritureVirement, monEcritureVirement.exercice(), gestionCredit, creditRetenue );
	 * 
	 * this.trace("-----------------------------"); this.trace(" retenue"+tmpRetenue);
	 * this.trace(" DETAIL ECRITURE AGENCE CREDIT RETENUE "+monEcritureVirement.detailEcriture());
	 * 
	 * // CREATION DU titreDetailEcriture maFactoryTitreDetailEcriture.creerTitreDetailEcriture( ed, Factory.getNow(),
	 * EOTitreDetailEcriture.tdeOrigineVirement, monEcritureDetail, monEcritureDetail.exercice(), titre, titre.origine() );
	 * 
	 * this.trace("-----------------------------"); this.trace(" titre DETAIL ECRITURE CREDIT RETENUE");
	 * 
	 * // on change l etat de retenue. tmpRetenue.setRedEtat(EORetenueDetail.etatTermine);
	 * 
	 * this.trace("-----------------------------"); this.trace("nouvel etat pour  retenue"+tmpRetenue);
	 * 
	 * j++; } i++; }
	 * 
	 * // nouveau montant du credit //montantCredit = montantCredit + titre.manTtc().floatValue(); montantCredit = montantCredit.add(titre.titTtc());
	 * 
	 * 
	 * this.trace("-----------------------------"); this.trace(" montant credit "+montantCredit);
	 * 
	 * // CREATION DU DETAIL ECRITURE CREDIT paiement ! monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail( ed, null, new
	 * Integer(i+2000), "", montantCredit, "", montantCredit, null, sensCredit(), monEcritureVirement, monEcritureVirement.exercice(), gestionCredit,
	 * creditModePaiement );
	 * 
	 * this.trace("-----------------------------"); this.trace(" NOUVEAU DETAIL ECRITURE AGENCE CREDIT PAIEMENT" + monEcritureDetail);
	 * this.trace("DETAIL  ECRITURE PAIEMENT " + monEcritureVirement.detailEcriture());
	 * 
	 * // CREATION DU titreDetailEcriture maFactoryTitreDetailEcriture.creerTitreDetailEcriture( ed, Factory.getNow(),
	 * EOTitreDetailEcriture.tdeOrigineVirement, monEcritureDetail, monEcritureDetail.exercice(), titre, titre.origine());
	 * 
	 * this.trace(" TITRE DETAIL ECRITURE AGENCE CREDIT PAIEMENT ");
	 * 
	 * // maj du mandat titre.setTitEtat(EOTitre.titrePaye); //mandat.addObjectToBothSidesOfRelationshipWithKey(virement,"virement");
	 * 
	 * this.trace("MAJ DU TITRE"+titre);
	 * 
	 * lesEcritures.addObject(monEcritureVirement);
	 * 
	 * this.trace("LE(S) ECRITURE(S) "+lesEcritures);
	 * 
	 * return lesEcritures; }
	 */

}
