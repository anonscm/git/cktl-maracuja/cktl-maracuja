/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryMandatDetailEcriture;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryTitreDetailEcriture;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOReimputation;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryProcessReimputation extends FactoryProcess {

	/**
     *
     */
	public FactoryProcessReimputation(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public EOReimputation reimputerMandat(EOEditingContext ed, EOExercice exercice, EOMandat mandat, EOPlanComptable planComptableAncien, EOPlanComptable planComptableNouveau, EOPlanComptable planComptableAncienEcr, EOPlanComptable planComptableNouveauEcr, NSTimestamp reiDate, String reiLibelle,
			EOUtilisateur utilisateur) {

		this.trace("");
		this.trace("reimputerMandat");
		this.trace("exercice = " + exercice);
		this.trace("mandat = " + mandat);
		this.trace("pcoAncien = " + planComptableAncien);
		this.trace("pcoNouveau = " + planComptableNouveau);
		this.trace("utilisateur = " + utilisateur);
		this.trace("reiLibelle = " + reiLibelle);
		this.trace("reiDate = " + reiDate);
		this.trace("-----------------------------");
		this.trace("");

		// verifier 6=> 6
		// verifier 2 => 2
		return this.reimputer(ed, exercice, mandat, null, planComptableAncien, planComptableNouveau, planComptableAncienEcr, planComptableNouveauEcr, reiDate, reiLibelle, utilisateur);
	}

	public EOReimputation reimputerTitre(EOEditingContext ed, EOExercice exercice, EOPlanComptable planComptableAncien, EOPlanComptable planComptableNouveau, EOPlanComptable planComptableAncienEcr, EOPlanComptable planComptableNouveauEcr, NSTimestamp reiDate, String reiLibelle, EOTitre titre,
			EOUtilisateur utilisateur) {

		this.trace("");
		this.trace("reimputerTitre");
		this.trace("exercice = " + exercice);
		this.trace("titre = " + titre);
		this.trace("pcoAncien = " + planComptableAncien);
		this.trace("pcoNouveau = " + planComptableNouveau);
		this.trace("utilisateur = " + utilisateur);
		this.trace("reiLibelle = " + reiLibelle);
		this.trace("reiDate = " + reiDate);
		this.trace("-----------------------------");
		this.trace("");
		// verifier 7 => 7
		// verifier 1 => 1
		return this.reimputer(ed, exercice, null, titre, planComptableAncien, planComptableNouveau, planComptableAncienEcr, planComptableNouveauEcr, reiDate, reiLibelle, utilisateur);
	}

	public void numeroterReimputation(EOEditingContext ed, EOReimputation reimputation, FactoryNumerotation leFactoryNumerotation) {

		this.trace("");
		this.trace("numeroterReimputation");
		this.trace("reimputation = " + reimputation);
		this.trace("numeroteur= " + leFactoryNumerotation);
		this.trace("-----------------------------");
		this.trace("");

		leFactoryNumerotation.getNumeroEOReimputation(ed, reimputation);
	}

	private EOReimputation reimputer(EOEditingContext ed, EOExercice exercice, EOMandat mandat, EOTitre titre, EOPlanComptable planComptableAncien, EOPlanComptable planComptableNouveau, EOPlanComptable planComptableAncienEcr, EOPlanComptable planComptableNouveauEcr, NSTimestamp reiDate,
			String reiLibelle, EOUtilisateur utilisateur) {

		EOReimputation nouvelleReimputation = (EOReimputation) Factory.instanceForEntity(ed, EOReimputation.ENTITY_NAME);
		ed.insertObject(nouvelleReimputation);
		nouvelleReimputation.setReiNumero(new Integer(0));
		nouvelleReimputation.setExerciceRelationship(exercice);
		nouvelleReimputation.setMandatRelationship(mandat);
		nouvelleReimputation.setTitreRelationship(titre);
		nouvelleReimputation.setPlanComptableAncienRelationship(planComptableAncien);
		nouvelleReimputation.setPlanComptableNouveauRelationship(planComptableNouveau);
		nouvelleReimputation.setUtilisateurRelationship(utilisateur);
		nouvelleReimputation.setReiDate(reiDate);
		nouvelleReimputation.setReiLibelle(reiLibelle);

		// creation de l ecriture et details
		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

		EOEcriture nouvelleEcriture = null;
		if (mandat != null) {

			String sens1 = sensDebit();
			String ecdLibelle = mandat.fournisseur().adrNom() + " Réimputation Md. " + mandat.manNumero() + " Bord. " + mandat.bordereau().borNum() + " du " + mandat.gestion().gesCode();
			String ecrLibelle = "Réimputation Mandat " + mandat.manNumero() + " Bord. " + mandat.bordereau().borNum() + " du " + mandat.gestion().gesCode();
			//modif rod : si orv c'est inversé.. 
			//Normalement il ne devrait pas y avoir de reductions de recettes reimputables... 
			if (EOTypeBordereau.SOUS_TYPE_REVERSEMENTS.equals(mandat.bordereau().typeBordereau().tboSousType())) {
				sens1 = sensCredit();
				ecdLibelle = mandat.fournisseur().adrNom() + " Réimputation ORV " + mandat.manNumero() + " Bord. " + mandat.bordereau().borNum() + " du " + mandat.gestion().gesCode();
				ecrLibelle = "Réimputation ORV " + mandat.manNumero() + " Bord. " + mandat.bordereau().borNum() + " du " + mandat.gestion().gesCode();
			}

			nouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), ecrLibelle, new Integer(0), null, mandat.gestion().comptabilite(), mandat.exercice(), mandat.origine(), FinderJournalEcriture.leTypeJournalReimputation(ed), FinderJournalEcriture
					.leTypeOperationGenerique(ed), utilisateur);

			// affectation de l ecriture
			nouvelleReimputation.setEcritureRelationship(nouvelleEcriture);

			// DEBIT NEGATIF ancien compte
			maFactoryEcritureDetail.creerEcritureDetail(ed, "reimputation", new Integer(1), ecdLibelle, mandat.manHt().abs().negate(), null, mandat.manHt().abs(), null, sens1, nouvelleEcriture, nouvelleEcriture.exercice(), mandat.gestion(), planComptableAncienEcr);
			// DEBIT POSITIF nouverau compte
			maFactoryEcritureDetail.creerEcritureDetail(ed, "reimputation", new Integer(2), ecdLibelle, mandat.manHt().abs(), null, mandat.manHt().abs(), null, sens1, nouvelleEcriture, nouvelleEcriture.exercice(), mandat.gestion(), planComptableNouveauEcr);
		}
		else {
			//on est sur un titre
			String ecdLib = " (Recettes multiples)";
			if (titre.recettes().count() == 1) {
				ecdLib = " \"" + ((EORecette) titre.recettes().objectAtIndex(0)).recLibelle() + "\"";
			}

			nouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), "Réimputation Titre " + titre.titNumero() + " Bord. " + titre.bordereau().borNum() + " du " + titre.gestion().gesCode() + " " + titre.exercice().exeExercice() + ecdLib, new Integer(0), null,
					titre.gestion().comptabilite(),
					titre.exercice(),
					titre.origine(), FinderJournalEcriture.leTypeJournalReimputation(ed), FinderJournalEcriture.leTypeOperationGenerique(ed), utilisateur);

			// affectation de l ecriture
			nouvelleReimputation.setEcritureRelationship(nouvelleEcriture);
			String sens1 = sensCredit();
			//modif rod : si reduction de recettes c'est inversé.. 
			//Normalement il ne devrait pas y avoir de reductions de recettes reimputables... 
			if (EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(titre.bordereau().typeBordereau().tboSousType())) {
				sens1 = sensDebit();
			}

			// CREDIT NEGATIF ancien compte
			maFactoryEcritureDetail.creerEcritureDetail(ed, "reimputation", new Integer(1), (titre.fournisseur() == null ? "" : titre.fournisseur().adrNom()) + " Réimputation Titre " + titre.titNumero() + " Bord. " + titre.bordereau().borNum() + " du " + titre.gestion().gesCode() + ecdLib,
					titre.titHt().abs().negate(), null, titre.titHt().abs(), null, sens1, nouvelleEcriture, nouvelleEcriture.exercice(), titre.gestion(), planComptableAncienEcr);
			// CREDIT POSITIF nouveau compte
			maFactoryEcritureDetail.creerEcritureDetail(ed, (titre.fournisseur() == null ? "" : titre.fournisseur().adrNom()) + " Réimputation Titre " + titre.titNumero() + " Bord. " + titre.bordereau().borNum() + " du " + titre.gestion().gesCode(), new Integer(2), (titre.fournisseur() == null ? ""
					: titre.fournisseur().adrNom()) + " Réimputation Titre " + titre.titNumero() + " Bord. " + titre.bordereau().borNum() + " du " + titre.gestion().gesCode() + ecdLib, titre
					.titHt().abs(), null, titre.titHt().abs(), null, sens1, nouvelleEcriture, nouvelleEcriture.exercice(), titre.gestion(), planComptableNouveauEcr);
		}

		int i = 0;
		if (mandat != null) {
			//     historique mandat et modifcation
			FactoryMandatDetailEcriture maFactoryMandatDetailEcriture = new FactoryMandatDetailEcriture(withLogs());
			mandat.setPlanComptableRelationship(planComptableNouveau);
			for (int j = 0; j < mandat.depenses().count(); j++) {
				EODepense depense = (EODepense) mandat.depenses().objectAtIndex(j);
				depense.setPlanComptableRelationship(planComptableNouveau);
			}
			while (i < nouvelleEcriture.detailEcriture().count()) {
				maFactoryMandatDetailEcriture.creerMandatDetailEcriture(ed, Factory.getNow(), EOMandatDetailEcriture.MDE_ORIGINE_REIMPUTATION, (EOEcritureDetail) nouvelleEcriture.detailEcriture().objectAtIndex(i), nouvelleEcriture.exercice(), mandat, nouvelleEcriture.origine());
				i++;
			}
		}
		else {
			//     historique titre et modification
			FactoryTitreDetailEcriture maFactoryTitreDetailEcriture = new FactoryTitreDetailEcriture(withLogs());
			titre.setPlanComptableRelationship(planComptableNouveau);
			for (int j = 0; j < titre.recettes().count(); j++) {
				EORecette recette = (EORecette) titre.recettes().objectAtIndex(j);
				recette.setPlanComptableRelationship(planComptableNouveau);
			}

			while (i < nouvelleEcriture.detailEcriture().count()) {
				maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.TDE_ORIGINE_REIMPUTATION, (EOEcritureDetail) nouvelleEcriture.detailEcriture().objectAtIndex(i), nouvelleEcriture.exercice(), titre, nouvelleEcriture.origine(), null);
				i++;
			}
		}

		//attibution de l ecriture
		nouvelleReimputation.setEcritureRelationship(nouvelleEcriture);
		return nouvelleReimputation;
	}
}
