/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryProcessModeRecouvrement extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessModeRecouvrement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public EOModeRecouvrement creerModeRecouvrementVide(EOEditingContext ed, String modCode, String modLibelle, EOExercice exercice) {

		EOModeRecouvrement newModeRecouvrement = (EOModeRecouvrement) Factory.instanceForEntity(ed, EOModeRecouvrement.ENTITY_NAME);
		ed.insertObject(newModeRecouvrement);
		newModeRecouvrement.setExerciceRelationship(exercice);
		//		newModeRecouvrement.addObjectToBothSidesOfRelationshipWithKey(exercice, "exercice");
		newModeRecouvrement.setModCode(modCode);
		newModeRecouvrement.setModLibelle(modLibelle);

		return newModeRecouvrement;
	}

	public void modifierCompteRecouvrement(EOEditingContext ed, EOModeRecouvrement modeDeRecouvrement, EOPlanComptable planComptable) {
		//        modeDeRecouvrement.addObjectToBothSidesOfRelationshipWithKey(
		//                planComptable,
		//                "planComptableRecouvrement");

		//Modif rod 27/11/2004
		if (planComptable != null) {
			modeDeRecouvrement.setPlanComptablePaiementRelationship(planComptable);
			//			modeDeRecouvrement.addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptablePaiement");
		}
		else {
			if (modeDeRecouvrement.planComptablePaiement() != null) {
				modeDeRecouvrement.removeObjectFromBothSidesOfRelationshipWithKey(modeDeRecouvrement.planComptablePaiement(), "planComptablePaiement");
			}
		}

	}

	public void modifierDomaine(EOEditingContext ed, EOModeRecouvrement modeDePaiement, String domaine) {
		modeDePaiement.setModDom(domaine);
	}

	public void modifierCompteVisa(EOEditingContext ed, EOModeRecouvrement modeDeRecouvrement, EOPlanComptable planComptable) {
		//        modeDeRecouvrement.addObjectToBothSidesOfRelationshipWithKey(
		//                planComptable,
		//                "planComptableVisa");

		//Modif rod 27/11/2004
		if (planComptable != null) {
			modeDeRecouvrement.setPlanComptableVisaRelationship(planComptable);
			//			modeDeRecouvrement.addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptableVisa");
		}
		else {
			if (modeDeRecouvrement.planComptableVisa() != null) {
				modeDeRecouvrement.removeObjectFromBothSidesOfRelationshipWithKey(modeDeRecouvrement.planComptableVisa(), "planComptableVisa");
			}
		}
	}

	//    public void modifierDomaine(
	//            EOEditingContext ed,
	//            EOModeRecouvrement modeDeRecouvrement,
	//            String domaine) {
	//        modeDeRecouvrement.setModDom(domaine);
	//    }
	//    
	//    public void modifierEmaAuto(
	//            final EOEditingContext ed,
	//            final EOModeRecouvrement modeDeRecouvrement,
	//            final String modEmaAuto) {
	//        modeDeRecouvrement.setModEmaAuto(modEmaAuto);
	//    }
	//
	//    

	public void validerModeRecouvrement(EOEditingContext ed, EOModeRecouvrement modeDeRecouvrement) {
		modeDeRecouvrement.setModValidite(EOModeRecouvrement.etatValide);
	}

	public void invaliderModeRecouvrement(EOEditingContext ed, EOModeRecouvrement modeDeRecouvrement) {
		modeDeRecouvrement.setModValidite(EOModeRecouvrement.etatInvalide);
	}

	public void verificationAvantSaveChanges(EOEditingContext ed, EOModeRecouvrement modeDeRecouvrement) throws FactoryException {

	}

}
