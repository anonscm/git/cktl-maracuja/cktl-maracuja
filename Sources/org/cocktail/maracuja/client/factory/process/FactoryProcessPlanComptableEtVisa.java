/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableAmo;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoAmortissement;
import org.cocktail.maracuja.client.metier.EOPlancoCredit;
import org.cocktail.maracuja.client.metier.EOPlancoVisa;
import org.cocktail.maracuja.client.metier.EOTypeCredit;
import org.cocktail.zutil.client.exceptions.DefaultClientException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class FactoryProcessPlanComptableEtVisa extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessPlanComptableEtVisa(final boolean withLog) {
		super(withLog, null);
	}

	public EOPlanComptable creerPlanComptable(EOEditingContext ed, String pcoBudgetaire, String pcoEmargement, String pcoLibelle, String pcoNature, String pcoNum) {

		this.trace("");
		this.trace("creerPlancomptable");
		this.trace("pcobudgetaire = " + pcoBudgetaire);
		this.trace("pcoemargement = " + pcoEmargement);
		this.trace("pcolibelle = " + pcoLibelle);
		this.trace("pconature = " + pcoNature);
		this.trace("pconum = " + pcoNum);
		this.trace("-----------------------------");
		this.trace("");

		// verifier les doublons !

		// EOPlanComptable newPlanComptable = new EOPlanComptable();

		EOPlanComptable newPlanComptable = (EOPlanComptable) Factory.instanceForEntity(ed, EOPlanComptable.ENTITY_NAME);
		ed.insertObject(newPlanComptable);

		newPlanComptable.setPcoBudgetaire(pcoBudgetaire);
		newPlanComptable.setPcoEmargement(pcoEmargement);
		newPlanComptable.setPcoLibelle(pcoLibelle);
		newPlanComptable.setPcoNature(pcoNature);
		newPlanComptable.setPcoNiveau(new Integer(pcoNum.length()));
		newPlanComptable.setPcoNum(pcoNum);
		newPlanComptable.setPcoValidite(EOPlanComptableExer.etatValide);

		return newPlanComptable;
	}

	public EOPlanComptableExer creerPlanComptableExer(EOEditingContext ed, EOExercice exercice, String pcoBudgetaire, String pcoEmargement, String pcoLibelle, String pcoNature, String pcoNum) {

		this.trace("");
		this.trace("creerPlancomptable");
		this.trace("pcobudgetaire = " + pcoBudgetaire);
		this.trace("pcoemargement = " + pcoEmargement);
		this.trace("pcolibelle = " + pcoLibelle);
		this.trace("pconature = " + pcoNature);
		this.trace("pconum = " + pcoNum);
		this.trace("-----------------------------");
		this.trace("");

		// EOPlanComptable newPlanComptable = new EOPlanComptable();
		//Verifier si on a un planComptable existant avec le pconum, et le prendre
		EOPlanComptable planComptable = pcoForPcoNum(ed, pcoNum);
		//si plan comptable n'xiste pas, on le crée
		if (planComptable == null) {
			planComptable = creerPlanComptable(ed, pcoBudgetaire, pcoEmargement, pcoLibelle, pcoNature, pcoNum);
			planComptable.setPcoNum(pcoNum);
		}

		EOPlanComptableExer newPlanComptableExer = (EOPlanComptableExer) Factory.instanceForEntity(ed, EOPlanComptableExer.ENTITY_NAME);
		ed.insertObject(newPlanComptableExer);

		newPlanComptableExer.setExerciceRelationship(exercice);

		newPlanComptableExer.setPlanComptableRelationship(planComptable);
		newPlanComptableExer.setPcoNum(pcoNum);

		newPlanComptableExer.setPcoBudgetaire(pcoBudgetaire);
		newPlanComptableExer.setPcoEmargement(pcoEmargement);
		newPlanComptableExer.setPcoLibelle(pcoLibelle);
		newPlanComptableExer.setPcoNature(pcoNature);
		newPlanComptableExer.setPcoNiveau(new Integer(pcoNum.length()));

		newPlanComptableExer.setPcoValidite(EOPlanComptableExer.etatValide);

		return newPlanComptableExer;
	}

	//	private static NSArray getAllPlancos(EOEditingContext ec) {
	//		return ZFinder.fetchArray(ec, EOPlanComptable.ENTITY_NAME, null, new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending)), false);
	//	}    

	private EOPlanComptable pcoForPcoNum(EOEditingContext ec, String pconum) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum='" + pconum + "'", null);
		NSArray res = ZFinder.fetchArray(ec, EOPlanComptable.ENTITY_NAME, qual, new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending)), false);
		return (EOPlanComptable) (res.count() > 0 ? res.objectAtIndex(0) : null);
	}

	public void annulerPlanComptable(EOEditingContext ed, EOPlanComptableExer plancomptable) {
		plancomptable.setPcoValidite(EOPlanComptableExer.etatAnnule);
	}

	public void ValiderPlanComptable(EOEditingContext ed, EOPlanComptableExer plancomptable) {
		plancomptable.setPcoValidite(EOPlanComptableExer.etatValide);
	}

	public EOPlancoVisa creerPlanComptableVisa(EOEditingContext ed, EOPlanComptableExer ordo, EOPlanComptableExer contrePartie, EOPlanComptableExer tva, String pviLibelle, String contrePartieGestion, EOExercice exercice) {
		// verifier les doublons
		EOPlancoVisa newPlancoVisa = (EOPlancoVisa) Factory.instanceForEntity(ed, EOPlancoVisa.ENTITY_NAME);
		ed.insertObject(newPlancoVisa);
		newPlancoVisa.setPviLibelle(pviLibelle);
		this.traiterDepenseRecette(newPlancoVisa, ordo, contrePartie, tva, contrePartieGestion);
		newPlancoVisa.setExerciceRelationship(exercice);
		return newPlancoVisa;
	}

	public void annulerPlanComptableVisa(EOEditingContext ed, EOPlancoVisa planComptableVisa) {
	}

	public void validerPlanComptableVisa(EOEditingContext ed, EOPlancoVisa planComptableVisa) {
	}

	public void modifierPlanComptableVisa(EOEditingContext ed, EOPlancoVisa planComptableVisa, EOPlanComptableExer ordo, EOPlanComptableExer tva, EOPlanComptableExer contrePartie, String contrePartieGestion) {
		this.traiterDepenseRecette(planComptableVisa, ordo, contrePartie, tva, contrePartieGestion);
	}

	private void traiterDepenseRecette(EOPlancoVisa planComptableVisa, EOPlanComptableExer ordo, EOPlanComptableExer contrePartie, EOPlanComptableExer tva, String contrePartieGestion) {

		if (planComptableVisa.pviLibelle().equals(EOPlancoVisa.pviLibelleMandat2) || planComptableVisa.pviLibelle().equals(EOPlancoVisa.pviLibelleMandat6) || planComptableVisa.pviLibelle().equals(EOPlancoVisa.pviLibelleTitre1) || planComptableVisa.pviLibelle().equals(EOPlancoVisa.pviLibelleTitre7)) {
			planComptableVisa.setCtrepartieRelationship(contrePartie.planComptable());
			planComptableVisa.setOrdonnateurRelationship(ordo.planComptable());
			planComptableVisa.setTvaRelationship(tva.planComptable());
		}
		planComptableVisa.setPviContrepartieGestion(contrePartieGestion);
	}

	public EOPlanComptableAmo creerPlanComptableAmo(final EOEditingContext ec, final String pcoNumAmo, final String pcoLibelleAmo) {
		final EOPlanComptableAmo newObject = (EOPlanComptableAmo) Factory.instanceForEntity(ec, EOPlanComptableAmo.ENTITY_NAME);
		ec.insertObject(newObject);
		newObject.setPcoaNum(pcoNumAmo);
		newObject.setPcoaLibelle(pcoLibelleAmo);
		newObject.setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
		return newObject;
	}

	public EOPlancoAmortissement creerPlancoAmortissement(final EOEditingContext ec, final EOPlanComptableAmo planComptableAmo, final EOPlanComptable planComptable, final EOExercice exercice, final Integer pcaDuree) {
		final EOPlancoAmortissement newObject = (EOPlancoAmortissement) Factory.instanceForEntity(ec, EOPlancoAmortissement.ENTITY_NAME);
		ec.insertObject(newObject);
		newObject.setPlanComptableRelationship(planComptable);
		newObject.setPlanComptableAmoRelationship(planComptableAmo);
		newObject.setExerciceRelationship(exercice);
		newObject.setPcaDuree(pcaDuree);
		return newObject;
	}

	public void supprimerPlancoAmortissements(final EOEditingContext editingContext, final EOPlanComptable planco, final EOExercice ex) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlancoAmortissement.EXERCICE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				ex
		}));
		final NSArray res = EOQualifier.filteredArrayWithQualifier(planco.plancoAmortissements(), qual);
		int max = res.count() - 1;
		for (int i = max; i >= 0; i--) {
			final EOPlancoAmortissement element = (EOPlancoAmortissement) res.objectAtIndex(i);
			element.setPlanComptableAmoRelationship(null);
			element.setExerciceRelationship(null);
			element.setPlanComptableRelationship(null);
			editingContext.deleteObject(element);
		}
	}

	public void supprimerPlanComptableExer(final EOEditingContext editingContext, final EOPlanComptableExer planco) {
		supprimerPlancoCredits(editingContext, planco);
		supprimerPlancoVisas(editingContext, planco);
		supprimerPlancoAmortissements(editingContext, planco);
		editingContext.deleteObject(planco);
	}

	public void supprimerPlancoAmortissements(final EOEditingContext editingContext, final EOPlanComptableExer planco) {
		NSArray pcvs = getPlancoAmortissementsForPlanco(planco);
		for (int i = pcvs.count() - 1; i >= 0; i--) {
			final EOPlancoAmortissement element = (EOPlancoAmortissement) pcvs.objectAtIndex(i);
			element.setPlanComptableAmoRelationship(null);
			element.setExerciceRelationship(null);
			element.setPlanComptableRelationship(null);
			editingContext.deleteObject(element);
		}
	}

	public void supprimerPlancoVisas(final EOEditingContext editingContext, final EOPlanComptableExer planco) {
		//Supprimer les plancoVisa sur l'exercice en cours
		NSArray pcvs = getPlancoVisasForPlanco(planco);
		for (int i = pcvs.count() - 1; i >= 0; i--) {
			EOPlancoVisa pcc = (EOPlancoVisa) pcvs.objectAtIndex(i);
			pcc.setOrdonnateurRelationship(null);
			editingContext.deleteObject(pcc);
		}
	}

	public void supprimerPlancoCredits(final EOEditingContext editingContext, final EOPlanComptableExer planco) {
		NSArray pccs = getPlancoCreditsForPlanco(planco);
		for (int i = pccs.count() - 1; i >= 0; i--) {
			EOPlancoCredit pcc = (EOPlancoCredit) pccs.objectAtIndex(i);
			pcc.setPlanComptableRelationship(null);
			pcc.setTypeCreditRelationship(null);
			editingContext.deleteObject(pcc);
		}
	}

	private final NSArray getPlancoCreditsForPlanco(EOPlanComptableExer pco) {
		return EOsFinder.fetchArray(pco.editingContext(), EOPlancoCredit.ENTITY_NAME, EOPlancoCredit.PLAN_COMPTABLE_KEY + ZFinder.QUAL_EQUALS + " and " + EOPlancoCredit.TYPE_CREDIT_KEY + "." + EOTypeCredit.EXERCICE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				pco, pco.exercice()
		}), null,
				false);
	}

	private final NSArray getPlancoVisasForPlanco(EOPlanComptableExer pco) {
		return EOsFinder.fetchArray(pco.editingContext(), EOPlancoVisa.ENTITY_NAME, EOPlancoVisa.ORDONNATEUR_KEY + ZFinder.QUAL_EQUALS + " and " + EOPlancoVisa.EXERCICE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				pco, pco.exercice()
		}), null,
				false);
	}

	private final NSArray getPlancoAmortissementsForPlanco(EOPlanComptableExer pco) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlancoAmortissement.EXERCICE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				pco.exercice()
		}));
		final NSArray res = EOQualifier.filteredArrayWithQualifier(pco.planComptable().plancoAmortissements(), qual);
		return res;
	}

	/**
	 * @param edc
	 * @param planco
	 * @param exercice
	 * @return Le nombre de lignes d'ecritures passes sur le compte pour l'exercice (via requete sql).
	 */
	public static Number nbEcritureDetailsForPlanco(EOEditingContext edc, EOPlanComptableExer planco, EOExercice exercice) {
		Object exer = ServerProxy.serverPrimaryKeyForObject(edc, exercice).valueForKey(EOExercice.EXE_ORDRE_KEY);
		String sql = "select count(*) as NB from maracuja.ecriture_detail where exe_ordre=" + exer.toString() + " and pco_num='" + planco.pcoNum() + "'";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(edc, sql);
		if (res.count() > 0) {
			return (Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB");
		}
		return null;
	}

	/**
	 * @param edc
	 * @param planco
	 * @param exercice
	 * @return Le nombre de lignes d'ecritures passes sur le compte ou des subdivisions pour l'exercice (via requete sql).
	 */
	public static Number nbEcritureDetailsForPlancoLike(EOEditingContext edc, EOPlanComptableExer planco, EOExercice exercice) {
		Object exer = ServerProxy.serverPrimaryKeyForObject(edc, exercice).valueForKey(EOExercice.EXE_ORDRE_KEY);
		String sql = "select count(*) as NB from maracuja.ecriture_detail where exe_ordre=" + exer.toString() + " and pco_num like '" + planco.pcoNum() + "%'";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(edc, sql);
		if (res.count() > 0) {
			return (Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB");
		}
		return null;
	}

	public static void checkPlancoUtiliseDansBudget(EOEditingContext edc, EOPlanComptableExer planco, EOExercice exercice) throws DefaultClientException {
		Object exer = ServerProxy.serverPrimaryKeyForObject(edc, exercice).valueForKey(EOExercice.EXE_ORDRE_KEY);
		String sql = "select count(*) as NB from jefy_budget.budget_masque_nature where exe_ordre=" + exer.toString() + " and pco_num = '" + planco.pcoNum() + "'";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(edc, sql);
		if (res.count() > 0) {
			if (((Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB")).intValue() > 0) {
				//FIXME DT5886 "Incohérence détectée : Le compte " + planco.pcoNum() + " est utilisé dans le masque de saisie du budget " + exercice.exeExercice() + ", vous ne pouvez pas le supprimer ni le passer à non 'Budgétaire'. Vérifiez que le compte est bien marqué à 'Budgétaire', ou supprimez-le du masque de saisie du budget."
				throw new DefaultClientException("Le compte " + planco.pcoNum() + " est utilisé dans le masque de saisie du budget " + exercice.exeExercice() + ", vous ne pouvez pas le supprimer ni le passer à non 'Budgétaire'");
			}
		}
		sql = "select count(*) as NB from jefy_budget.budget_saisie_nature where exe_ordre=" + exer.toString() + " and pco_num like '" + planco.pcoNum() + "%'";
		res = ServerProxy.clientSideRequestSqlQuery(edc, sql);
		if (res.count() > 0) {
			if (((Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB")).intValue() > 0) {
				//FIXME DT5886 "Incohérence détectée : Le compte " + planco.pcoNum() + " est utilisé dans le masque de saisie du budget " + exercice.exeExercice() + ", vous ne pouvez pas le supprimer ni le passer à non 'Budgétaire'. Vérifiez que le compte est bien marqué à 'Budgétaire'."
				throw new DefaultClientException("Le compte " + planco.pcoNum() + " ou une de ses subdivisions est utilisée dans la saisie du budget " + exercice.exeExercice() + ", vous ne pouvez pas le supprimer ni le passer à non 'Budgétaire'. ");
			}
		}
	}

	public static void checkPlancoUtilisePlancoVisa(EOEditingContext edc, EOPlanComptableExer planco, EOExercice exercice) throws DefaultClientException {
		Object exer = ServerProxy.serverPrimaryKeyForObject(edc, exercice).valueForKey(EOExercice.EXE_ORDRE_KEY);
		String sql = "select count(*) as NB from maracuja.planco_visa where exe_ordre=" + exer.toString() + " and pco_num_ctrepartie = '" + planco.pcoNum() + "'";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(edc, sql);
		if (res.count() > 0) {
			if (((Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB")).intValue() > 0) {
				throw new DefaultClientException("Le compte " + planco.pcoNum() + " est utilisé comme compte de contrepartie paramétré dans le plan comptable, vous ne pouvez pas le supprimer.");
			}
		}
		sql = "select count(*) as NB from maracuja.planco_visa where exe_ordre=" + exer.toString() + " and pco_num_tva = '" + planco.pcoNum() + "'";
		res = ServerProxy.clientSideRequestSqlQuery(edc, sql);
		if (res.count() > 0) {
			if (((Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("NB")).intValue() > 0) {
				throw new DefaultClientException("Le compte " + planco.pcoNum() + " est utilisé comme compte de TVA paramétré dans le plan comptable, vous ne pouvez pas le supprimer.");
			}
		}
	}

}
