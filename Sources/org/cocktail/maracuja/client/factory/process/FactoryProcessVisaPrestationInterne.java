/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 14 juil. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.exception.VisaException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */
public class FactoryProcessVisaPrestationInterne extends FactoryProcess {

	private FactoryProcessVisaMandat maFactoryProcessVisaMandat;

	private FactoryProcessVisaTitre maFactoryProcessVisaTitre;

	/**
	 * @param withLog
	 */
	public FactoryProcessVisaPrestationInterne(EOEditingContext ed,
			EOTypeJournal typeJournalDepense, EOTypeJournal typeJournalRecette,
			EOTypeOperation typeOperationDepense, EOTypeOperation typeOperationRecette, boolean withLog, final NSTimestamp dateJourComptable, boolean wantRecupConvention) {
		super(withLog, dateJourComptable);

		maFactoryProcessVisaMandat = new FactoryProcessVisaMandat(
				ed,
				typeJournalDepense,
				typeOperationDepense,
				withLog, dateJourComptable, wantRecupConvention);

		maFactoryProcessVisaTitre = new FactoryProcessVisaTitre(
				ed,
				typeJournalRecette,
				typeOperationRecette,
				withLog, dateJourComptable);
	}

	/**
	 * @param ed
	 * @param utilisateur
	 * @param dateRemiseVisa
	 * @param delegateNumerotation
	 * @param leBordereauMandat
	 * @param lesMandatsRejetes
	 * @param lesMandatsAcceptes
	 * @param leBordereauTitre
	 * @param lesTitresRejetes
	 * @param lesTitresAcceptes
	 * @return null ou bien un tableau avec deux entree, la premiere etant le bordereau de rejet mandat et la deuxieme le bordereau de rejet titre.
	 * @throws VisaException
	 */
	public NSArray viserBordereauxEtNumeroter(
			EOEditingContext ed,
			EOUtilisateur utilisateur,
			NSTimestamp dateRemiseVisa,
			FactoryNumerotation delegateNumerotation,
			EOBordereau leBordereauMandat,
			NSArray lesMandatsRejetes,
			NSArray lesMandatsAcceptes,
			EOBordereau leBordereauTitre,
			NSArray lesTitresRejetes,
			NSArray lesTitresAcceptes) throws VisaException {
		//  NSMutableArray lesBordereauxRejet = new NSMutableArray();

		this.verifierBorderaux(
				leBordereauMandat,
				lesMandatsRejetes,
				lesMandatsAcceptes,
				leBordereauTitre,
				lesTitresRejetes,
				lesTitresAcceptes);

		EOBordereauRejet bordereauRejetMandat = maFactoryProcessVisaMandat.viserUnBordereauDeMandatEtNumeroter(
				ed,
				utilisateur,
				leBordereauMandat,
				lesMandatsRejetes,
				lesMandatsAcceptes,
				dateRemiseVisa,
				delegateNumerotation);

		EOBordereauRejet bordereauRejetTitre = maFactoryProcessVisaTitre
				.viserUnBordereauDeTitreEtNumeroter(
						ed,
						utilisateur,
						leBordereauTitre,
						lesTitresRejetes,
						lesTitresAcceptes,
						dateRemiseVisa,
						delegateNumerotation,
						null);

		if (bordereauRejetTitre != null) {
			return new NSArray(new Object[] {
					bordereauRejetMandat, bordereauRejetTitre
			});
		}
		return null;
	}

	public NSArray viserBordereaux(
			EOEditingContext ed,
			EOUtilisateur utilisateur,
			NSTimestamp dateRemiseVisa,
			EOBordereau leBordereauMandat,
			NSArray lesMandatsRejetes,
			NSArray lesMandatsAcceptes,
			EOBordereau leBordereauTitre,
			NSArray lesTitresRejetes,
			NSArray lesTitresAcceptes) throws VisaException {
		NSMutableArray lesBordereauxRejet = new NSMutableArray();

		this.verifierBorderaux(
				leBordereauMandat,
				lesMandatsRejetes,
				lesMandatsAcceptes,
				leBordereauTitre,
				lesTitresRejetes,
				lesTitresAcceptes);

		lesBordereauxRejet.addObject(maFactoryProcessVisaMandat
				.viserUnBordereauDeMandat(
						ed,
						utilisateur,
						leBordereauMandat,
						lesMandatsRejetes,
						lesMandatsAcceptes,
						dateRemiseVisa));

		lesBordereauxRejet.addObject(maFactoryProcessVisaTitre
				.viserUnBordereauDeTitre(
						ed,
						utilisateur,
						leBordereauTitre,
						lesTitresRejetes,
						lesTitresAcceptes,
						dateRemiseVisa,
						null));

		return (NSArray) lesBordereauxRejet;

	}

	private void verifierBorderaux(
			EOBordereau leBordereauMandat,
			NSArray lesMandatsRejetes,
			NSArray lesMandatsAcceptes,
			EOBordereau leBordereauTitre,
			NSArray lesTitresRejetes,
			NSArray lesTitresAcceptes) throws VisaException {

		//	verifier que les 2 bordereaux sont des Prestations Internes
		if (!(leBordereauMandat.typeBordereau()
				.equals(EOTypeBordereau.TypeBordereauPrestationInterne)))
			new VisaException(VisaException.prestationProblemeTypeBordereaux);

		if (!(leBordereauTitre.typeBordereau()
				.equals(EOTypeBordereau.TypeBordereauPrestationInterne)))
			new VisaException(VisaException.prestationProblemeTypeBordereaux);

		// verifier que le montant des lesMandatsRejetes = lesTitresRejetes
		/*
		 * if ((this.computeSumForKey( lesMandatsRejetes, "manHt")) == (this.computeSumForKey( lesTitresRejetes, "titHt")))
		 */
		if (Factory.different(this.computeSumForKeyBigDecimal(
				lesMandatsRejetes,
				"manHt"), this.computeSumForKeyBigDecimal(
				lesTitresRejetes,
				"titHt")))
			throw new VisaException(
					VisaException.prestationProblemeMontantRejets);

		// verifier que le montant des lesMandatsAcceptes = lesTitresAcceptes
		/*
		 * if ((this.computeSumForKey( lesMandatsAcceptes, "manHt")) == (this.computeSumForKey( lesTitresAcceptes, "titHt")))
		 */
		if (Factory.different(this.computeSumForKeyBigDecimal(
				lesMandatsAcceptes,
				"manHt"), this.computeSumForKeyBigDecimal(
				lesTitresAcceptes,
				"titHt")))
			throw new VisaException(VisaException.prestationProblemeMontant);

	}
}
