/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 *   @author RIVALLAND FREDERIC <br>
 *                UAG <br>
 *                CRI Guadeloupe
 *
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EORetenue;
import org.cocktail.maracuja.client.metier.EOTypeRetenue;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryProcessRetenue extends FactoryProcess {

    /**
     * @param withLog
     */
    public FactoryProcessRetenue(final boolean withLog, final NSTimestamp dateJourComptable) {
        super(withLog, dateJourComptable);        
    }


    public EORetenue creerRetenue(
            EOEditingContext ed,
            String retLibelle,
            BigDecimal retMontant,
            EOTypeRetenue typeRetenue,
            EODepense depense,
            EOComptabilite comptabilite,
            EOExercice exercice,
            EOUtilisateur utilisateur) throws Exception {

        final EORetenue nouvelleEORetenue = (EORetenue) Factory.instanceForEntity(ed,
                EORetenue.ENTITY_NAME);

        ed.insertObject(nouvelleEORetenue);

        nouvelleEORetenue.setRetDate(Factory.getNow());
        nouvelleEORetenue.setRetLibelle(retLibelle);
        nouvelleEORetenue.setRetMontant(retMontant);
        nouvelleEORetenue.setRetNumero(ZConst.INTEGER_ZERO);
        
        nouvelleEORetenue.setTypeRetenueRelationship(typeRetenue);
        nouvelleEORetenue.setUtilisateurRelationship(utilisateur);
        nouvelleEORetenue.setDepenseRelationship(depense);
        nouvelleEORetenue.setComptabiliteRelationship(comptabilite);
        nouvelleEORetenue.setExerciceRelationship(exercice);

        depense.majMontantDisquette();
        
        return nouvelleEORetenue;
    }

    public void supprimerRetenue(
            EOEditingContext ed,
            EORetenue retenue,
            EOUtilisateur utilisateur) throws Exception {

        this.trace("");
        this.trace("supprimerRetenue");
        this.trace("retenue= " + retenue);
        this.trace("utilisateur = " + utilisateur);
        this.trace("-----------------------------");
        this.trace("");

        // faire des verifications sur les details , impossible qu elles soient VISE !!
        if (EOMandat.mandatVirement.equals(retenue.depense().mandat().manEtat())) {
            throw new FactoryException("Le mandat est deja paye. Impossible de supprimer la retenue.");
        }
        EODepense dep = retenue.depense();
        retenue.setDepenseRelationship(null);
        ed.deleteObject(retenue);
        
        dep.majMontantDisquette();
    }

 

//    
//    public EORetenue creerRetenue(
//            EOEditingContext ed,
//            EOExercice exercice,
//            EOFournisseur fournisseur,
//            EOFournisseur fournisseurCreancier,
//            EOLot lot,
//            NSTimestamp retDebut,
//            NSTimestamp retFin,
//            String retLibelle,
//            BigDecimal retMontant,
//            EORib rib,
//            EOTypeRetenue typeRetenue,
//            EOUtilisateur utilisateur) {
//        
//        this.trace("");
//        this.trace("creerRetenue");
//        this.trace("exercice = " + exercice);
//        this.trace("fournisseur = " + fournisseur);
//        this.trace("fournisseurCreancier = " + fournisseurCreancier);
//        this.trace("retDebut = " + retDebut);
//        this.trace("retFin = " + retFin);
//        this.trace("retLibelle = " + retLibelle);
//        this.trace("retMontant= " + retMontant);
//        this.trace("typeRetenue = " + typeRetenue);
//        this.trace("rib = " + rib);
//        this.trace("utilisateur = " + utilisateur);
//        this.trace("-----------------------------");
//        this.trace("");
//        
//        // creation de la nouvelle Ecriture
//        EORetenue nouvelleEORetenue = (EORetenue) Factory.instanceForEntity(ed,
//                EORetenue.ENTITY_NAME);
//        
//        ed.insertObject(nouvelleEORetenue);
//        
//        nouvelleEORetenue.setRetDebut(retDebut);
//        nouvelleEORetenue.setRetFin(retFin);
//        nouvelleEORetenue.setRetLibelle(retLibelle);
//        nouvelleEORetenue.setRetEtat(EORetenue.etatValide);
//        nouvelleEORetenue.setRetMontant(retMontant);
//        nouvelleEORetenue.setRetMontantReste(retMontant);
//        
//        nouvelleEORetenue.setRibRelationship(rib);
//        nouvelleEORetenue.setTypeRetenueRelationship(typeRetenue);
//        nouvelleEORetenue.setUtilisateurRelationship(utilisateur);
//        nouvelleEORetenue.setExerciceRelationship(exercice);
//        nouvelleEORetenue.setFournisseurRelationship(fournisseur);
//        nouvelleEORetenue.setFournisseurCreancierRelationship(fournisseurCreancier);
//        nouvelleEORetenue.setLotRelationship(lot);
//        
//        return nouvelleEORetenue;
//    }
//    
//    public void supprimerRetenue(
//            EOEditingContext ed,
//            EORetenue retenue,
//            EOUtilisateur utilisateur) throws FactoryException {
//        
//        this.trace("");
//        this.trace("supprimerRetenue");
//        this.trace("retenue= " + retenue);
//        this.trace("utilisateur = " + utilisateur);
//        this.trace("-----------------------------");
//        this.trace("");
//        
//        // faire des verifications sur les details , impossible qu elles soient VISE !!
//        retenue.setRetEtat(EORetenue.etatAnnule);
//        
//    }
//    
//    public void terminerRetenue(
//            EOEditingContext ed,
//            EORetenue retenue,
//            EOUtilisateur utilisateur) throws FactoryException {
//        this.trace("");
//        this.trace("terminerRetenue");
//        this.trace("retenue= " + retenue);
//        this.trace("utilisateur = " + utilisateur);
//        this.trace("-----------------------------");
//        this.trace("");
//        
//        retenue.setRetEtat(EORetenue.etatTermine);
//        
//    }
//    
//    public void modifierRetenue(
//            EOEditingContext ed,
//            EORetenue retenue,
//            EOLot lot,
//            NSTimestamp retFin,
//            String retLibelle,
//            BigDecimal nouveauRetMontant,
//            EORib rib,
//            EOUtilisateur utilisateur) throws FactoryException {
//        
//        NSArray lesRetenueDetails = null;
//        NSMutableArray lesDetails = null;
//        
//        float newMontantReste = 0;
//        int i = 0;
//        
//        this.trace("");
//        this.trace("modifierRetenue");
//        this.trace("exercice = " + retenue);
//        this.trace("retFin = " + retFin);
//        this.trace("retLibelle = " + retLibelle);
//        this.trace("nouveauRetMontant= " + nouveauRetMontant);
//        this.trace("rib = " + rib);
//        this.trace("utilisateur = " + utilisateur);
//        this.trace("-----------------------------");
//        this.trace("");
//        
//        if (retenue.retEtat().equals(
//                EORetenue.etatTermine))
//            throw new FactoryException(EORetenue.modificationImpossible);
//        
//        retenue.verifierNouveauMontant(nouveauRetMontant);
//        retenue.evaluerMontantReste(nouveauRetMontant);
//        
//        retenue.setRetFin(retFin);
//        retenue.setRetLibelle(retLibelle);
//        retenue.setRetEtat(EORetenue.etatValide);
//        retenue.setRetMontantReste(new BigDecimal(newMontantReste));
//        
//        retenue.setRibRelationship(rib);
//        retenue.setUtilisateurRelationship(utilisateur);
//        retenue.setLotRelationship(lot);
//    }
//    
//    
//    public void numeroterRetenue(
//            EOEditingContext ed,
//            EORetenue retenue,
//            FactoryNumerotation numeroteur) {
//        
//        this.trace("");
//        this.trace("numeroterRetenue");
//        this.trace("retenue = " + retenue);
//        this.trace("numeroteur = " + numeroteur);
//        this.trace("-----------------------------");
//        this.trace("");
//        
//        
//        numeroteur.getNumeroEORetenue(
//                ed,
//                retenue);
//    }
}
