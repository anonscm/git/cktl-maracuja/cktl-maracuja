/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

/*
 * Created on 9 juil. 2004 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteRecetteCtrlPlanco;
import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancier;
import org.cocktail.fwkcktlcompta.client.sepasdd.helpers.SepaSddOrigineClientHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigine;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddOrigineHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddMandatRule;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.exception.VisaException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryBordereauRejet;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryTitreDetailEcriture;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOSepaSddEcheancierEcd;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryProcessVisaTitre extends FactoryProcess {

	private EOTypeJournal typeJournal;
	private EOTypeOperation typeOperation;
	private final FactoryEcritureDetail maFactoryEcritureDetail;
	private final FactoryProcessJournalEcriture maFactoryEcriture;
	private final FactoryBordereauRejet maFactoryBordereauRejet;
	private final FactoryTitreDetailEcriture maFactoryTitreDetailEcriture;

	/**
	 * Constructeur permettant d instancier l objet qui va gerer les objets metiers de la phase de visa :<br>
	 * Ecriture , EcritureDetails ,TitreDetailEcriture ,Titre ,TitreBrouillon ,BordereauRejet. <br>
	 * typeJounal peut etre null (valeur pas defaut journal GENERAL) <br>
	 * typeOperation peut etre null (valeur pas defaut operation VISA )
	 * 
	 * @param typeJournal type de journal pour les ecritures de visa
	 * @param typeOperation type d'operation pour les ecritures de visa
	 * @param withLog activer les trace de cette factory
	 */
	public FactoryProcessVisaTitre(EOEditingContext ed, EOTypeJournal ptypeJournal, EOTypeOperation ptypeOperation, boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);

		// ajout Rod
		if (ptypeJournal == null) {
			ptypeJournal = FinderJournalEcriture.leTypeJournalVisaMandat(ed);
		}
		if (ptypeOperation == null) {
			ptypeOperation = FinderVisa.leTypeOperationVisaMandat(ed);
		}

		if (ptypeJournal == null) {
			throw new FactoryException(FactoryException.TYPE_JOURNAL_NON_SPECIFIE);
		}
		if (ptypeOperation == null) {
			throw new FactoryException(FactoryException.TYPE_OPERATION_NON_SPECIFIE);
		}

		// ajout Rod
		setTypeJournal(ptypeJournal);
		setTypeOperation(ptypeOperation);

		maFactoryEcritureDetail = new FactoryEcritureDetail(withLog);
		maFactoryEcriture = new FactoryProcessJournalEcriture(withLog, getDateJourComptable());
		maFactoryBordereauRejet = new FactoryBordereauRejet(withLog);
		maFactoryTitreDetailEcriture = new FactoryTitreDetailEcriture(withLog);
	}

	/**
	 * Methode permettant de viser un bordereau de titre. <br>
	 * Cette Methode genere les ecritures comptable.
	 * 
	 * @param ed EditingContext de travail (Nested ??)
	 * @param operateur Agent qui fait le visa
	 * @param leBordereau Bordereau a Viser
	 * @param lesTitresRejetes Tableaux des Titres a rejeter
	 * @param lesTitresAcceptes Tableaux des Titres a valider
	 * @param dateRemiseVisa Date de remise du bordereau
	 * @return Le Bordereau de rejet s il existe
	 */
	public EOBordereauRejet viserUnBordereauDeTitre(EOEditingContext ed, EOUtilisateur utilisateur, EOBordereau leBordereau, NSArray lesTitresRejetes, NSArray lesTitresAcceptes, NSTimestamp dateRemiseVisa, NSTimestamp dateLimitePaiement) throws VisaException {
		try {
			this.verifierFormatDuBordereau(ed, utilisateur, leBordereau);
		} catch (VisaException e) {
			throw new VisaException(VisaException.problemeFormatBordereau + e);
		}

		// on gere l etat du bordereau : VISER
		if (!EOBordereau.BordereauValide.equals(leBordereau.borEtat())) {
			throw new VisaException("Le bordereau n'est pas a l'etat VALIDE");
		}
		leBordereau.setBorEtat(EOBordereau.BordereauVise);
		leBordereau.setBorDateVisa(Factory.getNow());
		leBordereau.setUtilisateurVisaRelationship(utilisateur);
		//		leBordereau.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateurVisa");

		this.trace("leBordereau " + leBordereau);

		// on met a jour les titres acceptï¿½s
		int i = 0;
		while (i < lesTitresAcceptes.count()) {
			// passage a l etat VISE
			final EOTitre leTitre = ((EOTitre) lesTitresAcceptes.objectAtIndex(i));
			leTitre.setTitEtat(EOTitre.titreVise);
			//
			leTitre.setTitDateRemise(dateRemiseVisa);

			this.trace("lesTitresAcceptes " + leTitre);

			// creation des ecitures si on a pas de bordereau d ODP (titre)
			if (!leBordereau.typeBordereau().equals(EOTypeBordereau.TypeBordereauBTODP)) {
				this.creerLesEcrituresDuTitre(ed, leTitre, utilisateur);
			}

			// Ajout Rod 23/06/2005
			// balayer les recettes et ajouter la date limite de paiement
			for (int j = 0; j < leTitre.recettes().count(); j++) {
				final EORecette element = (EORecette) leTitre.recettes().objectAtIndex(j);
				element.setRecDateLimitePaiement(dateLimitePaiement);
			}
			i++;

		}

		this.trace("lesTitresRejetes " + lesTitresRejetes);
		// on met a jour les titres rejetï¿½s
		if (lesTitresRejetes.count() == 0) {
			return null;
		}
		return this.creerLeBordereauDeRejet(ed, lesTitresRejetes, leBordereau.exercice(), leBordereau.gestion(), utilisateur, FinderVisa.leTypeBordereauBTTNA(ed));
	}

	/**
	 * Methode permettant de viser un bordereau de titre. <br>
	 * Cette Methode genere les ecritures comptable.
	 * 
	 * @param ed EditingContext de travail (Nested ??)
	 * @param operateur Agent qui fait le visa
	 * @param leBordereau Bordereau a Viser
	 * @param lesTitresRejetes Tableaux des Titres a rejeter
	 * @param lesTitresAcceptes Tableaux des Titres a valider
	 * @param dateRemiseVisa Date de remise du bordereau
	 * @param delegateNumerotation Objet qui gere l attribution des numeros (ecritures,bordereauRejet)
	 * @return Le Bordereau de rejet s il existe
	 */
	public EOBordereauRejet viserUnBordereauDeTitreEtNumeroter(EOEditingContext ed, EOUtilisateur utilisateur, EOBordereau leBordereau, NSArray lesTitresRejetes, NSArray lesTitresAcceptes, NSTimestamp dateRemiseVisa, FactoryNumerotation delegateNumerotation, NSTimestamp dateLimitePaiement)
			throws VisaException {
		EOBordereauRejet nouveauEOBordereauRejet = this.viserUnBordereauDeTitre(ed, utilisateur, leBordereau, lesTitresRejetes, lesTitresAcceptes, dateRemiseVisa, dateLimitePaiement);
		return nouveauEOBordereauRejet;
	}

	private void creerLesEcrituresDuTitre(EOEditingContext ed, EOTitre leTitre, EOUtilisateur utilisateur) throws VisaException {

		final NSMutableArray lesDebitsDuVisa = new NSMutableArray();
		final Map recettesPourDebits = new HashMap();

		if (leTitre.bordereau().typeBordereau().equals(EOTypeBordereau.TypeBordereauBTODP)) {
			throw new FactoryException("TRAITEMENT RESERVE AUX ORDRES DE PAIEMENT (TITRE)");
		}

		this.trace("Avant newEOEcriture : ");
		// verifications
		if (leTitre.titEtat().equals(EOTitre.titreAnnule)) {
			throw new VisaException(VisaException.titreDejaAnnule);
		}

		if (leTitre.titEtat().equals(EOTitre.titreAnnule)) {
			throw new VisaException(VisaException.titreDejaVise);
		}

		// creation de l ecriture
		final String typeTitre = (EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(leTitre.bordereau().typeBordereau().tboSousType()) ? "Red." : "Tit.");

		final EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), typeTitre + " Num. " + leTitre.titNumero().toString() + " Bord. Num. " + leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion().gesCode(), new Integer(0), null, leTitre
				.bordereau().gestion().comptabilite(), leTitre.exercice(), leTitre.origine(), this.getTypeJournal(), this.getTypeOperation(), utilisateur);

		ed.insertObject(newEOEcriture);

		//        this.trace("newEOEcriture " + newEOEcriture);
		final NSArray lesBrouillons = EOQualifier.filteredArrayWithQualifier(leTitre.titreBrouillards(), EOQualifier.qualifierWithQualifierFormat("tibOperation <> %@ and tibOperation <> %@", new NSArray(new Object[] {
				EOTitreBrouillard.VISA_SACD, EOTitreBrouillard.VISA_ANNULER
		})));
		//        this.trace("lesBrouillons " + lesBrouillons);

		if (lesBrouillons.count() == 0) {
			throw new VisaException(VisaException.pasDeTitreBrouillard);
		}

		// pour chaque ligne du brouillon on cree un detail ecriture
		int i = 0;
		while (i < lesBrouillons.count()) {
			final EOTitreBrouillard leBrouillard = (EOTitreBrouillard) lesBrouillons.objectAtIndex(i);
			final EOEcritureDetail newEOEcritureDetail;
			newEOEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i + 1), leBrouillard.recette().recDebiteur() + " " + typeTitre + " Num. " + leTitre.titNumero().toString() + " Bord. " + leTitre.bordereau().borNum().toString() + " du "
					+ leTitre.bordereau().gestion().gesCode() + " " + leTitre.exercice().exeExercice() + " \"" + leBrouillard.recette().recLibelle() + "\"", leBrouillard.tibMontant(), null, leBrouillard.tibMontant(), null, leBrouillard.tibSens(), newEOEcriture, leTitre.exercice(), leBrouillard
					.gestion(), leBrouillard.planComptable());

			EOAccordsContrat contrat = leBrouillard.recette().getAccordContratUnique();
			if (contrat != null) {
				newEOEcritureDetail.setToAccordsContratRelationship(contrat);
			}

			// on conserve les ecritures de DEBIT pour le traitement de l
			// echeancier
			if (EOEcritureDetail.SENS_DEBIT.equals(leBrouillard.tibSens())) {
				lesDebitsDuVisa.addObject(newEOEcritureDetail);
				recettesPourDebits.put(newEOEcritureDetail, leBrouillard.recette());
			}

			this.trace("newEOEcritureDetail " + newEOEcritureDetail);

			if (newEOEcritureDetail == null) {
				throw new VisaException(VisaException.problemeCreationEcriture);
			}

			// on cree le EOTitreDetailEcriture
			String tdeOrigine = (EOTitreBrouillard.VISA_TVA.equals(leBrouillard.tibOperation()) ? EOTitreDetailEcriture.TDE_ORIGINE_VISA_TVA : (EOTitreBrouillard.VISA_CTP.equals(leBrouillard.tibOperation()) ? EOTitreDetailEcriture.TDE_ORIGINE_VISA_CTP : EOTitreDetailEcriture.TDE_ORIGINE_VISA));
			final EOTitreDetailEcriture newEOTitreDetailEcriture = maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), tdeOrigine, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), leBrouillard.recette());

			this.trace("newEOTitreDetailEcriture " + newEOTitreDetailEcriture);
			if (newEOTitreDetailEcriture == null) {
				throw new VisaException(VisaException.problemeCreationDetailEcriture);
			}

			i++;
		}

		// si le titre concerne un echeancier (mode de recouvrement a echeancier)
		// ne pas générer d'ecritures si le titre est une reduction
		if (!EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(leTitre.bordereau().typeBordereau().tboSousType()) && leTitre.modeRecouvrement() != null && EOModeRecouvrement.MODDOM_ECHEANCIER.equals(leTitre.modeRecouvrement().modDom())) {
			this.trace("Titre avec echeancier ");
			// Verifier le plancovisa
			if (leTitre.modeRecouvrement().planComptableVisa() == null || NSKeyValueCoding.NullValue.equals(leTitre.modeRecouvrement())) {
				throw new VisaException("Le plan comptable VISA n'est pas défini pour le mode de recouvrement associé au titre (" + leTitre.modeRecouvrement().modLibelle() + ")");
			}

			// Verifier si on a bien un echeancier
			boolean isSepa = false;
			Map<EORecette, EOSepaSddEcheancier> echeanciersPourRecette = new HashMap<EORecette, EOSepaSddEcheancier>();
			for (int j = 0; j < leTitre.recettes().count(); j++) {
				EORecette element = (EORecette) leTitre.recettes().objectAtIndex(j);

				EOSepaSddEcheancier echeancierSepa = getEcheancierSepaSdd(new EOEditingContext(), element.recOrdre());
				if (echeancierSepa != null) {
					echeanciersPourRecette.put(element, echeancierSepa.localInstanceIn(ed));
					SepaSddMandatRule.getSharedInstance().checkMandatValide(echeancierSepa.mandat());
					isSepa = true;
				}
				if (element.echeancier() == null && echeancierSepa == null) {
					throw new VisaException("Erreur : Il manque un échéancier associé pour le titre n° " + leTitre.titNumero().intValue());
				}
			}

			if (isSepa) {
		        creerLesEcrituresDuTitreEcheancierSepaSDD(ed, leTitre, echeanciersPourRecette, utilisateur);
			} else {
				creerLesEcrituresDuTitreEcheancier(ed, leTitre, newEOEcriture, lesDebitsDuVisa, recettesPourDebits, utilisateur);
			}
		}

	}

	private EOSepaSddEcheancier getEcheancierSepaSdd(EOEditingContext editingContext, Integer jefyRecetteRecetteCtrlPlancoId) {
		try {
			EOJefyRecetteRecetteCtrlPlanco rpco = EOJefyRecetteRecetteCtrlPlanco.fetchByKeyValue(editingContext, EOJefyRecetteRecetteCtrlPlanco.RPCO_ID_KEY, jefyRecetteRecetteCtrlPlancoId);
			NSArray faps = rpco.toJefyRecetteRecette().toFacture().toFacturePapier();
			if (faps.count() > 0) {
				EOJefyRecetteFacturePapier fap = (EOJefyRecetteFacturePapier) faps.objectAtIndex(0);
				ISepaSddOrigineType origineType = SepaSddOrigineClientHelper.getSharedInstance()
						.fetchOrigineTypeValide(editingContext, ISepaSddOrigineType.FACTURE);
				ISepaSddOrigine origine = SepaSddOrigineHelper.getSharedInstance().creerOuRecupererOrigine(editingContext, origineType, fap);
				return (EOSepaSddEcheancier) SepaSddOrigineHelper.getSharedInstance().premierEcheancierPourOrigine(origine);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}


	/**
	 * Methode qui permet de creer un ecriture de Credit du compte de classe 4 au moment du visa du tire de de generer un DEBIT au compte de classe 4
	 * parameter dans la tabel mode de recouvrement plancoVisa si ce mode de recouvrement est dans le domaine ECHEANCIER
	 * 
	 * @param ed editing context de travail
	 * @param leTitre tire en cours de traitement
	 * @param monEcritureVisa ecriture de visa
	 * @param lesDebitsDuVisa les debits creer lors du visa
	 * @param recettesPourDebits la recette associee a chaque debit
	 * @param utilisateur l utilisateur qui genere les ecritures
	 * @throws VisaException Exception en cas de probleme !
	 */
	private void creerLesEcrituresDuTitreEcheancier(EOEditingContext ed, EOTitre leTitre, EOEcriture monEcritureVisa, NSMutableArray lesDebitsDuVisa, Map recettesPourDebits, EOUtilisateur utilisateur) throws VisaException {

		//ajout Rod: Verifier que la contrepartie du titre est differente de l'imputation visa affectee au mode de recouvrement
		final EOPlanComptable plancoVisaRecouvrement = leTitre.modeRecouvrement().planComptableVisa();
		for (int i = 0; i < lesDebitsDuVisa.count(); i++) {
			final EOEcritureDetail unDebitDuVisa = ((EOEcritureDetail) lesDebitsDuVisa.objectAtIndex(i));
			if (plancoVisaRecouvrement.equals(unDebitDuVisa.planComptable())) {
				throw new VisaException("Titre n° " + leTitre.titNumero().intValue() + " : la contrepartie en débit " + unDebitDuVisa.planComptable().pcoNum() + " ne doit pas etre la même que l'imputation visa associée au mode de recouvrement.");
			}
		}

		// creation d une nouvelle ecriture
		// D C4xx_titre C4xxmode_recourvement + Emargement
		// creation de l ecriture
		final EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), "Tit. Num. " + leTitre.titNumero().toString() + " Bord. Num. " + leTitre.bordereau().borNum().toString() + " du " + leTitre.bordereau().gestion().gesCode() + " (Echeancier)", new Integer(0), null,
				leTitre.bordereau().gestion().comptabilite(), leTitre.exercice(), leTitre.origine(), this.getTypeJournal(), this.getTypeOperation(), utilisateur);

		ed.insertObject(newEOEcriture);

		final boolean isSacd = leTitre.gestion().isSacd(leTitre.exercice());
		final EOGestion compta = (isSacd ? leTitre.gestion() : leTitre.gestion().comptabilite().gestion());

		// creation du detail ecriture de DEBIT (le plus simple car celui du mode de recouvrement)
		EOEcritureDetail newEOEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), (leTitre.fournisseur() != null ? leTitre.fournisseur().adrNom() : "") + " Echeancier Tit. " + leTitre.titNumero().toString() + " Bord. " + leTitre.bordereau().borNum().toString()
				+ " du " + leTitre.bordereau().gestion().gesCode() + " " + leTitre.exercice().exeExercice() + " \"" + leTitre.titLibelle() + "\"", leTitre.titTtc(), null, leTitre.titTtc(), null, EOEcritureDetail.SENS_DEBIT, newEOEcriture, leTitre.exercice(), compta, leTitre.modeRecouvrement()
				.planComptableVisa());

		// on cree le EOTitreDetailEcriture
		maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOrigineVisaEcheancier, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), null);

		// creation des details ecriture de CREDIT en fonction de l ecriture de
		// VISA
		// recup des ecritures details debiteur

		int i = 0;
		for (i = 0; i < lesDebitsDuVisa.count(); i++) {
			final EOEcritureDetail unDebitDuVisa = ((EOEcritureDetail) lesDebitsDuVisa.objectAtIndex(i));
			newEOEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), (leTitre.fournisseur() != null ? leTitre.fournisseur().adrNom() : "") + " Echeancier Tit. " + leTitre.titNumero().toString() + " Bord. " + leTitre.bordereau().borNum().toString() + " du "
					+ leTitre.bordereau().gestion().gesCode() + " " + leTitre.exercice().exeExercice() + " \"" + leTitre.titLibelle() + "\"", unDebitDuVisa.ecdMontant(), null, unDebitDuVisa.ecdMontant(), null, EOEcritureDetail.SENS_CREDIT, newEOEcriture, leTitre.exercice(), unDebitDuVisa.gestion(),
					unDebitDuVisa.planComptable());

			maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOrigineVisaEcheancier, newEOEcritureDetail, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), (EORecette) recettesPourDebits.get(unDebitDuVisa));

		}
		// emargement des compte de D (visa) et C (ecriture echeancier) fait en
		// procedure stockee
	}

	private NSArray getDebitsPriseEnChargePourRecette(EOExercice exercice, EORecette recette) {
		EOQualifier qualDebit = new EOKeyValueQualifier(EOTitreDetailEcriture.ECRITURE_DETAIL_KEY + "." + EOEcritureDetail.ECD_SENS_KEY, EOQualifier.QualifierOperatorEqual, EOEcritureDetail.SENS_DEBIT);
		EOQualifier qualExercice = new EOKeyValueQualifier(EOTitreDetailEcriture.ECRITURE_DETAIL_KEY + "." + EOEcritureDetail.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exercice);
		NSArray tdes = recette.titreDetailEcritures(new EOAndQualifier(new NSArray(new Object[] {
				qualDebit, qualExercice
		})));
		return (NSArray) tdes.valueForKey(EOTitreDetailEcriture.ECRITURE_DETAIL_KEY);
	}

	/**
	 * Methode qui permet de creer un ecriture de Credit du compte de classe 4 au moment du visa du tire de de generer un DEBIT au compte de classe 4
	 * parameter dans la tabel mode de recouvrement plancoVisa si ce mode de recouvrement est dans le domaine ECHEANCIER
	 * 
	 * @param ed editing context de travail
	 * @param leTitre titre en cours de traitement
	 * @param monEcritureVisa ecriture de visa
	 * @param lesDebitsDuVisa les debits creer lors du visa
	 * @param recettesPourDebits la recette associee a chaque debit
	 * @param utilisateur l utilisateur qui genere les ecritures
	 * @throws VisaException Exception en cas de probleme !
	 */
	private void creerLesEcrituresDuTitreEcheancierSepaSDD(EOEditingContext ed, EOTitre leTitre, Map<EORecette, EOSepaSddEcheancier> echeanciersPourRecette, EOUtilisateur utilisateur) throws VisaException {

		Set<EORecette> recettes = echeanciersPourRecette.keySet();
		
		//balayer les recettes et creer un debit pour le pco du mode de recouvrement du montant de l'échéancier rattaché à la recette
		for (EORecette eoRecette : recettes) {
			final boolean isSacd = leTitre.gestion().isSacd(leTitre.exercice());
			final EOGestion compta = (isSacd ? eoRecette.gestion() : eoRecette.gestion().comptabilite().gestion());
			EOSepaSddEcheancier echeancier = echeanciersPourRecette.get(eoRecette);

			if (echeancier.toSepaSddOrigine().toSepaSddOrigineType().isGenerationEcrituresAutomatique()) {
    			BigDecimal montant = echeancier.montantAPayer();
    			String ecdLibelle = SepaSddEcheancierHelper.getSharedInstance().getLibelleEcheancier(echeancier);
    			org.cocktail.maracuja.client.metier.EOComptabilite comptabilite = eoRecette.gestion().comptabilite();
    			EOExercice exercice = eoRecette.exercice();
    
    			final EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), ecdLibelle, new Integer(0), null,
    					comptabilite, exercice, leTitre.origine(), this.getTypeJournal(), this.getTypeOperation(), utilisateur);
    			ed.insertObject(newEOEcriture);
    
    			final EOEcritureDetail newDebitEcheancier = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), ecdLibelle, montant, null, montant, null, EOEcritureDetail.SENS_DEBIT, newEOEcriture, eoRecette.exercice(), compta, eoRecette.modeRecouvrement().planComptableVisa());
    			maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOrigineVisaEcheancier, newDebitEcheancier, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), null);
    
    			//Memoriser le lien entre l'echeancier et l'ecriture
    			EOSepaSddEcheancierEcd sepaSddEcheancierEcd = EOSepaSddEcheancierEcd.createSepaSddEcheancierEcd(ed, newDebitEcheancier, echeancier);
    			
    			NSArray debitsDuVisa = getDebitsPriseEnChargePourRecette(eoRecette.exercice(), eoRecette);
    			//si total debit=montant echeancier c ok, sinon exception
    			if (debitsDuVisa.count() > 1 && !montantDebits(debitsDuVisa).equals(echeancier.montantAPayer())) {
    				throw new VisaException("Erreur : plusieurs débits existent pour la recette et le total des débits de la recette ne correspond pas au montant de l'échéancier lié. Impossible de créer une écriture de credit en automatique pour la prise en charge de l'échéancier.");
    			}
    			
    			for (int i = 0; i < debitsDuVisa.count(); i++) {
    				EOEcritureDetail unDebitDuVisa = (EOEcritureDetail) debitsDuVisa.objectAtIndex(i);
    				final EOEcritureDetail newCreditEcheancier = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), ecdLibelle, unDebitDuVisa.ecdMontant(), null, unDebitDuVisa.ecdMontant(), null, EOEcritureDetail.SENS_CREDIT, newEOEcriture, leTitre.exercice(), unDebitDuVisa.gestion(),
    						unDebitDuVisa.planComptable());
    
    				maFactoryTitreDetailEcriture.creerTitreDetailEcriture(ed, Factory.getNow(), EOTitreDetailEcriture.tdeOrigineVisaEcheancier, newCreditEcheancier, newEOEcriture.exercice(), leTitre, newEOEcriture.origine(), eoRecette);
    			}
			}
		}

		// emargement des compte de D (visa) et C (ecriture echeancier) fait en
		// procedure stockee
	}

	private BigDecimal montantDebits(NSArray ecritureDetails) {
		BigDecimal montant = BigDecimal.ZERO;
		for (int i = 0; i < ecritureDetails.count(); i++) {
			EOEcritureDetail ecd = (EOEcritureDetail) ecritureDetails.objectAtIndex(i);
			montant = montant.add(ecd.ecdDebit());
		}
		return montant;
	}

	private EOBordereauRejet creerLeBordereauDeRejet(EOEditingContext ed, NSArray lesTitresRejetes, EOExercice exercice, EOGestion gestion, EOUtilisateur utilisateur, EOTypeBordereau typBordereau) throws VisaException {
		// verifications
		if (lesTitresRejetes.count() == 0) {
			throw new VisaException(VisaException.pasDeTitresARejeter);
		}

		// creation du bordereau de rejet
		EOBordereauRejet newEOBordereauRejet = maFactoryBordereauRejet.creerBordereauRejet(ed, EOBordereauRejet.BordereauRejetValide, new Integer(0), exercice, gestion, typBordereau, utilisateur);

		this.trace("newEOBordereauRejet " + newEOBordereauRejet);

		if (newEOBordereauRejet == null) {
			throw new VisaException(VisaException.problemeCreationBordereauRejet);
		}

		ed.insertObject(newEOBordereauRejet);

		// passer les titres a Annule
		int i = 0;
		while (i < lesTitresRejetes.count()) {
			((EOTitre) lesTitresRejetes.objectAtIndex(i)).setTitEtat(EOTitre.titreAnnule);

			// rattache les titres a un boredereau de rejet
			((EOTitre) lesTitresRejetes.objectAtIndex(i)).setBordereauRejetRelationship(newEOBordereauRejet);
			//			((EOTitre) lesTitresRejetes.objectAtIndex(i)).addObjectToBothSidesOfRelationshipWithKey(newEOBordereauRejet, "bordereauRejet");

			//			this.trace("leTitreRejete " + (lesTitresRejetes.objectAtIndex(i)));

			i++;
		}

		// numerotation de BTTNA
		return newEOBordereauRejet;
	}

	private void creerLesOperationsTresorerie() {

	}

	private void verifierFormatDuBordereau(EOEditingContext ed, EOUtilisateur utilisateur, EOBordereau leBordereau) throws VisaException {
		// verifier le format du bordereau :
		// gestion
		// pco_num titre = pco_num recette
		// montant titre = montant des recettes
		// HT + TVA = TTC pour les titre et les recette !

		// le bordereau est il de ja vise ?

	}

	/**
	 * Effectue la numerotation d'un bordereau de rejet.
	 * 
	 * @param ed
	 * @param leBordereauRejet
	 * @param leFactoryNumerotation
	 * @param withLogs
	 */
	public void numeroterUnBordereauDeTitreRejet(EOEditingContext ed, EOBordereauRejet leBordereauRejet, FactoryNumerotation leFactoryNumerotation, boolean withLogs) {
		// numero le bordereau de rejet
		leFactoryNumerotation.getNumeroEOBordereauRejet(ed, leBordereauRejet);
		// numeroter les mandats du bordereau rejet fait par lka procedure
		// stockÃ©e
	}

	/**
	 * Effectue la numerotation des ecritures liees a un bordereau de titre.
	 * 
	 * @param ed
	 * @param leBordereau
	 * @param leFactoryNumerotation
	 * @param withLogs
	 */
	public NSArray numeroterUnBordereauDeTitre(EOEditingContext ed, EOBordereau leBordereau, FactoryNumerotation leFactoryNumerotation, boolean withLogs) {
		// leFactoryNumerotation. getNumeroEOEcriture( ed, monEcriture);
		// leFactoryNumerotation.getNumeroEOEmargement(ed, monEmargement);
		// numero les ecritures du bordereau
		// recup des ecritures du bordereau
		NSArray lesEcritures = FinderVisa.lesEcrituresDuBordereauTitre(ed, leBordereau, withLogs);

		// Laisser la trace, apparemment ca permet de s'assurer que les
		// ecritures sont bien triees .. ????
		System.out.println(lesEcritures);

		int i = 0;
		while (i < lesEcritures.count()) {
			leFactoryNumerotation.getNumeroEOEcriture(ed, (EOEcriture) lesEcritures.objectAtIndex(i));
			i++;
		}
		return lesEcritures;
	}

	/**
	 * @return
	 */
	private EOTypeJournal getTypeJournal() {
		return typeJournal;
	}

	/**
	 * @return
	 */
	private EOTypeOperation getTypeOperation() {
		return typeOperation;
	}

	/**
	 * @param journal
	 */
	private void setTypeJournal(EOTypeJournal journal) {
		typeJournal = journal;
	}

	/**
	 * @param operation
	 */
	private void setTypeOperation(EOTypeOperation operation) {
		typeOperation = operation;
	}

}
