/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryOrdrePaiementDetailEcriture;
import org.cocktail.maracuja.client.finder.FinderOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard;
import org.cocktail.maracuja.client.metier.EOOrdrePaiementDetailEcriture;
import org.cocktail.maracuja.client.metier.EOOrgan;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryProcessOrdrePaiement extends FactoryProcess {

	public FactoryProcessOrdrePaiement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public EOOrdreDePaiement creerOrdreDePaiement(EOEditingContext ed, EOComptabilite comptabilite, EOExercice exercice, EOFournisseur fournisseur, EOModePaiement modePaiement, BigDecimal odpHt, String odpLibelle, String odpLibelleFournisseur, Number odpNumero, BigDecimal odpTtc, BigDecimal odpTva,
			EOOrigine origine, EORib rib, EOUtilisateur utilisateur, String odpEtat,
			//String odpDate,
			NSTimestamp odpDateSaisie, EOOrgan organ, String odpReferencePaiement) {

		this.trace("");
		this.trace("creerOrdreDePaiement");
		//        this.trace("comptabilite = " + comptabilite);
		//        this.trace("fournisseur = " + fournisseur);
		//        this.trace("organ = " + organ);
		//        this.trace("exercice = " + exercice);
		//        this.trace("origine = " + origine);
		//        this.trace("modePaiement = " + modePaiement);
		//        this.trace("rib = " + rib);
		//        this.trace("odpLibelle = " + odpLibelle);
		//        this.trace("odpLibelleFournisseur = " + odpLibelleFournisseur);
		//        this.trace("odpNumero = " + odpNumero);
		//        this.trace("odpTtc = " + odpTtc);
		//        this.trace("odpHt = " + odpHt);
		//        this.trace("odpTva = " + odpTva);

		this.trace("-----------------------------");
		this.trace("");

		// creation du nouvel OP
		EOOrdreDePaiement nouvelOpd = (EOOrdreDePaiement) Factory.instanceForEntity(ed, EOOrdreDePaiement.ENTITY_NAME);

		ed.insertObject(nouvelOpd);

		nouvelOpd.setOdpHt(odpHt);
		nouvelOpd.setOdpLibelle(odpLibelle);
		nouvelOpd.setOdpLibelleFournisseur(odpLibelleFournisseur);
		nouvelOpd.setOdpNumero(new Integer(0));
		nouvelOpd.setOdpTtc(odpTtc);
		nouvelOpd.setOdpTva(odpTva);
		nouvelOpd.setOdpEtat(odpEtat);
		nouvelOpd.setOdpDate(Factory.getNow());
		nouvelOpd.setOdpDateSaisie(odpDateSaisie);
		nouvelOpd.setOdpReferencePaiement(odpReferencePaiement);

		nouvelOpd.setOrigineRelationship(origine);
		nouvelOpd.setRibRelationship(rib);
		nouvelOpd.setComptabiliteRelationship(comptabilite);
		nouvelOpd.setExerciceRelationship(exercice);
		nouvelOpd.setFournisseurRelationship(fournisseur);
		nouvelOpd.setModePaiementRelationship(modePaiement);
		nouvelOpd.setOrganRelationship(organ);
		nouvelOpd.setUtilisateurRelationship(utilisateur);

		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(origine,"origine");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(rib,"rib");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(comptabilite,"comptabilite");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(exercice,"exercice");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(fournisseur,"fournisseur");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(modePaiement,"modePaiement");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(organ,"organ");
		//        nouvelOpd.addObjectToBothSidesOfRelationshipWithKey(utilisateur,"utilisateur");

		return nouvelOpd;
	}

	/**
	 * Permet de mettre à jour un OP avant que celui-ci soit visé ou payé ou annulé.
	 * 
	 * @param ed
	 * @param ordreDePaiement
	 * @param comptabilite
	 * @param exercice
	 * @param fournisseur
	 * @param modePaiement
	 * @param odpHt
	 * @param odpLibelle
	 * @param odpLibelleFournisseur
	 * @param odpTtc
	 * @param odpTva
	 * @param origine
	 * @param rib
	 * @param utilisateur
	 * @param odpEtat
	 * @param odpDateSaisie
	 * @param organ
	 */
	public void updateOrdreDePaiement(final EOEditingContext ed, final EOOrdreDePaiement ordreDePaiement, final EOFournisseur fournisseur, final EOModePaiement modePaiement, final BigDecimal odpHt, final String odpLibelle, final String odpLibelleFournisseur, final BigDecimal odpTtc,
			final BigDecimal odpTva, final EOOrigine origine, final EORib rib, final EOUtilisateur utilisateur, final NSTimestamp odpDateSaisie, final EOOrgan organ, final String odpReferencePaiement) {

		if (ordreDePaiement == null) {
			throw new FactoryException("L'ordre de paiement est nul.");
		}

		if (EOOrdreDePaiement.etatVise.equals(ordreDePaiement.odpEtat())) {
			throw new FactoryException("L'ordre de paiement est visé.");
		}

		if (EOOrdreDePaiement.etatVirement.equals(ordreDePaiement.odpEtat())) {
			throw new FactoryException("L'ordre de paiement est payé.");
		}

		ordreDePaiement.setOdpHt(odpHt);
		ordreDePaiement.setOdpLibelle(odpLibelle);
		ordreDePaiement.setOdpLibelleFournisseur(odpLibelleFournisseur);
		ordreDePaiement.setOdpTtc(odpTtc);
		ordreDePaiement.setOdpTva(odpTva);
		ordreDePaiement.setOdpDateSaisie(odpDateSaisie);
		ordreDePaiement.setOdpReferencePaiement(odpReferencePaiement);

		ordreDePaiement.setOrigineRelationship(origine);
		ordreDePaiement.setRibRelationship(rib);
		ordreDePaiement.setFournisseurRelationship(fournisseur);
		ordreDePaiement.setModePaiementRelationship(modePaiement);
		ordreDePaiement.setOrganRelationship(organ);
		ordreDePaiement.setUtilisateurRelationship(utilisateur);

		//        if (ordreDePaiement.origine() != origine) {
		//            ordreDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiement.origine(), "origine");
		//            ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(origine,"origine");
		//        }
		//        
		//        if (ordreDePaiement.rib() != rib) {
		//            ordreDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiement.rib(), "rib");
		//            ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(rib,"rib");
		//        }
		//        
		//        if (ordreDePaiement.fournisseur() != fournisseur) {
		//            ordreDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiement.fournisseur(), "fournisseur");
		//            ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(fournisseur,"fournisseur");
		//        }
		//        
		//        if (ordreDePaiement.modePaiement() != modePaiement) {
		//            ordreDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiement.modePaiement(), "modePaiement");
		//            ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(modePaiement,"modePaiement");
		//        }
		//        
		//        if (ordreDePaiement.organ() != organ) {
		//            ordreDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiement.organ(), "organ");
		//            ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(organ,"organ");
		//        }
		//        
		//        if (ordreDePaiement.utilisateur() != utilisateur) {
		//            ordreDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiement.utilisateur(), "utilisateur");
		//            ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(utilisateur,"utilisateur");
		//        }
		//        

	}

	public void numeroterOrdreDePaiement(EOEditingContext ed, EOOrdreDePaiement ordreDePaiement, FactoryNumerotation numeroteur) {
		this.trace("");
		this.trace("numeroterOrdreDePaiement");
		this.trace("numeroteur = " + numeroteur);
		this.trace("ordreDePaiement= " + ordreDePaiement);

		this.trace("-----------------------------");
		this.trace("");
		numeroteur.getNumeroEOOrdreDePaiement(ed, ordreDePaiement);
	}

	public void annulerOrdreDePaiement(EOEditingContext ed, EOOrdreDePaiement ordreDePaiement, EOUtilisateur utilisateur) throws FactoryException {
		if (ordreDePaiement.ecriture() == null) {
			ordreDePaiement.setOdpEtat(EOOrdreDePaiement.etatAnnule);
		}
		else {
			throw new FactoryException(EOOrdreDePaiement.problemeAnnulationEcriture);
		}
	}

	public NSArray viserOrdreDePaiementSansRib(EOEditingContext ed, EOOrdreDePaiement ordreDePaiement, EOUtilisateur utilisateur) throws Exception {

		this.trace("");
		this.trace("viserOrdreDePaiement");
		this.trace("ordreDePaiement = " + ordreDePaiement);
		this.trace("urilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");

		NSMutableArray lesEcritures = new NSMutableArray();

		// on vise tout de suite un op s il ne demande pas de virement RIB
		//le brouillard existe t il ??
		if (ordreDePaiement.ecriture() == null && (ordreDePaiement.ordreDePaiementBrouillards() != null && ordreDePaiement.ordreDePaiementBrouillards().count() > 0)) {

			//creation des factory.
			FactoryProcessJournalEcriture maFactoryJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
			FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

			EOEcriture ecriture = maFactoryJournalEcriture.creerEcriture(ed, getDateJourComptable(), "VISA O.P. " + ordreDePaiement.odpNumero() + "  " + ordreDePaiement.fournisseur().adrNom() + " " + ordreDePaiement.odpLibelle(), new Integer(0), null, ((EOOrdreDePaiementBrouillard) (ordreDePaiement
					.ordreDePaiementBrouillards().objectAtIndex(0))).gestion().comptabilite(), ordreDePaiement.exercice(), null, FinderOrdreDePaiement.typeJournal(ed), FinderOrdreDePaiement.typeOperationVisa(ed), utilisateur);//FIXED chgt exercice date journee comptable
			//            

			// ajouter les details ecritures
			int i = 0;
			while (i < ordreDePaiement.ordreDePaiementBrouillards().count()) {

				String lib = ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards().objectAtIndex(i))).odbLibelle();
				if (lib == null) {
					lib = ordreDePaiement.odpLibelle();
				}
				lib = "VISA O.P. " + ordreDePaiement.odpNumero() + "  " + ordreDePaiement.fournisseur().adrNom() + " - " + lib;

				maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i), lib, ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards().objectAtIndex(i))).odbMontant(), null, ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards()
						.objectAtIndex(i))).odbMontant(), null, ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards().objectAtIndex(i))).odbSens(), ecriture, ordreDePaiement.exercice(), ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards()
						.objectAtIndex(i))).gestion(), ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards().objectAtIndex(i))).planComptable());

				i++;
			}

			// le machin SACD quand ca marchera
			lesEcritures.addObjectsFromArray(maFactoryJournalEcriture.determinerLesEcrituresSACD(ed, ecriture));
			if (!lesEcritures.containsObject(ecriture)) {
				lesEcritures.insertObjectAtIndex(ecriture, 0);
			}
			ordreDePaiement.setEcritureRelationship(ecriture);
			//			ordreDePaiement.addObjectToBothSidesOfRelationshipWithKey(ecriture, "ecriture");
			ordreDePaiement.setOdpEtat(EOOrdreDePaiement.etatVise);

		}
		else {
			throw new FactoryException(EOOrdreDePaiement.problemeViserPasPossible);
		}

		// creation des EOOrdrePaiementDetailEcriture
		FactoryOrdrePaiementDetailEcriture maFacto = new FactoryOrdrePaiementDetailEcriture(withLogs());
		int i = 0;
		int j = 0;

		while (i < lesEcritures.count()) {
			j = 0;
			while (j < ((EOEcriture) lesEcritures.objectAtIndex(i)).detailEcriture().count()) {
				maFacto.creerOrdrePaiementDetailEcriture(ed, ordreDePaiement, (EOEcritureDetail) (((EOEcriture) lesEcritures.objectAtIndex(i)).detailEcriture().objectAtIndex(j)), Factory.getNow(), EOOrdrePaiementDetailEcriture.ORIGINE_VISA);
				j++;
			}
			i++;
		}
		return lesEcritures;
	}

	public void verifierModificationOrdreDePaiement(EOEditingContext ed, EOOrdreDePaiement ordreDePaiement, EOUtilisateur utilisateur) throws FactoryException {
		this.trace("");
		this.trace("verifierOrdreDePaiement");
		this.trace("ordreDePaiement = " + ordreDePaiement);
		this.trace("utilisateur = " + utilisateur);

		this.trace("-----------------------------");
		this.trace("");
		// si la somme des brouillard/2 != montant de L OP !!!!!
		/*
		 * if (ordreDePaiement.odpTtc().floatValue() != (this.computeSumForKeyBigDecimal( ordreDePaiement.ordreDePaiementBrouillards(), "odpMontant"))
		 * / 2)
		 */
		BigDecimal tmpBigDecimal = new BigDecimal(0);
		tmpBigDecimal.divide(this.computeSumForKeyBigDecimal(ordreDePaiement.ordreDePaiementBrouillards(), "odpMontant"), 2);

		if (Factory.different(ordreDePaiement.odpTtc(), tmpBigDecimal)) {
			throw new FactoryException(EOOrdreDePaiement.problemeMontantEcriture);
		}

	}

	/**
	 * Rendre l'op payable et visable
	 * 
	 * @param editingContext
	 * @param obj
	 * @param utilisateur
	 */
	public void validerOrdreDePaiement(EOEditingContext editingContext, EOOrdreDePaiement obj, EOUtilisateur utilisateur) {
		if (EOOrdreDePaiement.etatValide.equals(obj.odpEtat())) {
			return;
		}
		if (!EOOrdreDePaiement.etatOrdonnateur.equals(obj.odpEtat())) {
			throw new FactoryException("L'état du bordereau ne lui permet pas d'être validé.");
		}
		obj.setOdpEtat(EOOrdreDePaiement.etatValide);

	}

}
