/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;
import java.util.Map;

import org.cocktail.maracuja.client.exception.EcritureDetailException;
import org.cocktail.maracuja.client.exception.EcritureException;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.metier.EOBrouillard;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 * @author rodolphe prin at cocktail
 */
public class FactoryProcessJournalEcriture extends FactoryProcess {

	private final FactoryEcritureDetail maFactoryEcritureDetail;

	public FactoryProcessJournalEcriture(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
		maFactoryEcritureDetail = new FactoryEcritureDetail(withLog);
	}

	/**
	 * Permet de creer une ecriture Comptable
	 * 
	 * @param ed editingContext de travail
	 * @param ecrDateSaisie date de l ecriture (doit être la date de la journee comptable)
	 * @param ecrEtat etat de l ecriture (VALIDE)
	 * @param ecrLibelle libelle de l ecriture
	 * @param ecrNumero numero de l ecriture (par defaut 0)
	 * @param ecrPostit post it ..remarque...
	 * @param comptabilite Agence comptable
	 * @param exercice Exercice
	 * @param origine origine de l ecriture Ress. Affec. ,Ope. Lucrat.
	 * @param typeJournal type de journal
	 * @param typeOperation type d orperation
	 * @param utilisateur utilisateur a l origine de cette creation
	 * @param lesEcrituresDetails le detail de l ecriture
	 * @return l ecriture
	 * @throws EcritureException
	 */
	public EOEcriture creerEcriture(EOEditingContext ed, NSTimestamp ecrDateSaisie, String ecrLibelle, Integer ecrNumero, String ecrPostit, EOComptabilite comptabilite, EOExercice exercice, EOOrigine origine, EOTypeJournal typeJournal, EOTypeOperation typeOperation, EOUtilisateur utilisateur)
			throws EcritureException {

		this.trace("");
		this.trace("Creation d'une ecriture");
		this.trace("");

		// creation de la nouvelle Ecriture
		EOEcriture nouvelleEcriture = (EOEcriture) Factory.instanceForEntity(ed, EOEcriture.ENTITY_NAME);

		ed.insertObject(nouvelleEcriture);

		nouvelleEcriture.setEcrDate(Factory.getNow());
		nouvelleEcriture.setEcrNumero(ecrNumero);
		nouvelleEcriture.setEcrDateSaisie(ecrDateSaisie);
		nouvelleEcriture.setEcrEtat(EOEcriture.ecritureValide);
		nouvelleEcriture.setEcrLibelle(ecrLibelle);

		nouvelleEcriture.setEcrPostit(ecrPostit);

		nouvelleEcriture.setComptabiliteRelationship(comptabilite);
		nouvelleEcriture.setExerciceRelationship(exercice);
		nouvelleEcriture.setOrigineRelationship(origine);
		nouvelleEcriture.setTypeJournalRelationship(typeJournal);
		nouvelleEcriture.setTypeOperationRelationship(typeOperation);
		nouvelleEcriture.setUtilisateurRelationship(utilisateur);
		return nouvelleEcriture;
	}

	/**
	 * Permet d annuler une ecriture : saisie d une ecriture negative !
	 * 
	 * @param ed editingContext de travail
	 * @param ecriture ecriture a annuler
	 * @param utilisateur utilisateur a l origine de cette suppression
	 * @return un tableau decriture de suppression
	 * @throws EcritureException
	 */
	public NSArray annulerEcriture(EOEditingContext ed, EOEcriture ecriture, EOUtilisateur utilisateur, Map correspondancesLignes) throws EcritureException {
		// on annule uniquement une ecriture VALIDE
		if (ecriture.ecrEtat().equals(EOEcriture.ecritureAnnule)) {
			throw new EcritureException(EcritureException.impossibleEcritureAnnule);
		}

		// on creer un nouvelle ecriture negative de l originale
		EOEcriture nouvelleEcriture = this.creerEcriture(ed, getDateJourComptable(), "Annulation Ecriture " + ecriture.exercice().exeExercice().intValue() + "/" + ecriture.ecrNumero(), new Integer(0), "", ecriture.comptabilite(), ecriture.exercice(), ecriture.origine(), ecriture.typeJournal(),
				ecriture.typeOperation(), utilisateur);// FIXED

		ed.insertObject(nouvelleEcriture);

		// traiter mandatDetailEcriture ou titreDetailEcriture ???
		// depende de l origine de l ecriture
		// recpup des ecritureDetails
		NSArray lesEcrituresDetails = FinderJournalEcriture.lesEcrituresDetailsDeLecriture(ed, ecriture);
		int i = 0;
		while (lesEcrituresDetails.count() > i) {
			EOEcritureDetail nouveauEOEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdCommentaire(), ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdIndex(), ((EOEcritureDetail) lesEcrituresDetails
					.objectAtIndex(i)).ecdLibelle(), ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdMontant().negate(), ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdPostit(), ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdMontant().abs(),
					((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdSecondaire(), ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).ecdSens(), nouvelleEcriture, ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).exercice(), ((EOEcritureDetail) lesEcrituresDetails
							.objectAtIndex(i)).gestion(), ((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i)).planComptable());
			// inutile car deja dans l ed
			// ed.insertObject(nouveauEOEcritureDetail);
			correspondancesLignes.put((EOEcritureDetail) lesEcrituresDetails.objectAtIndex(i), nouveauEOEcritureDetail);
			i++;
		}
		// emarger les 2 ecritures

		// generer les annulations de ecritures SACD !

		return new NSArray(nouvelleEcriture);
	}

	public EOEcriture rectifierEcriture(EOEditingContext ed, EOEcriture ecritureARectifier, NSArray lesNouveauxDetails, EOUtilisateur utilisateur) throws EcritureException {

		EOEcriture nouvelleEcriture = null;
		this.trace("");
		this.trace("rectifierEcriture");
		this.trace("ecritureARectifier = " + ecritureARectifier);
		this.trace("NSArray lesNouveauxDetails = " + lesNouveauxDetails);
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");
		//int i;

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());

		if (lesNouveauxDetails == null || lesNouveauxDetails.count() < 2) {
			throw new EcritureDetailException("Il manque des details d'ecriture");
		}

		// Creation de la nouvelle ecriture
		nouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), "RECTIFICATION ECRITURE : " + ecritureARectifier.ecrNumero().toString(), new Integer(0), null, ecritureARectifier.comptabilite(), ecritureARectifier.exercice(), ecritureARectifier.origine(),
				ecritureARectifier.typeJournal(), ecritureARectifier.typeOperation(), utilisateur);// FIXED

		// On affecte les nouveaux details ï¿½ la nouvelle ecriture
		for (int j = 0; j < lesNouveauxDetails.count(); j++) {
			EOEcritureDetail element = (EOEcritureDetail) lesNouveauxDetails.objectAtIndex(j);
			element.setEcritureRelationship(nouvelleEcriture);
			//			element.addObjectToBothSidesOfRelationshipWithKey(nouvelleEcriture, "ecriture");
		}

		return nouvelleEcriture;
	}

	/**
	 * Permet de rectifier une ecriture :<br>
	 * modification des DEBITS ou CREDITS de lecriture
	 * 
	 * @param ed editingContext de travail
	 * @param ecriture ecriture a modifier
	 * @param lesNouveauxDetailsCredit les Credits a ajouter
	 * @param lesNouveauxDetailsDebit Les debits a ajouter
	 * @param utilisateur utilisateur a l origine de cette rectification
	 * @throws EcritureException
	 */
	public EOEcriture rectifierEcriture(EOEditingContext ed, EOEcriture ecriture, NSArray lesNouveauxDetailsCredit, NSArray lesNouveauxDetailsDebit, EOUtilisateur utilisateur) throws EcritureException {

		EOEcriture nouvelleEcriture = null;
		int i;
		// tous les details doivent appartenir a ecriture

		// tous les details sont DEBIT ou CREDIT

		// LES MASSES DE L ECRITURES NE DOIVENT PAS CHANGER

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());

		nouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), "RECTIFICATION ECRITURE : " + ecriture.ecrNumero().toString(), new Integer(0), null, ecriture.comptabilite(), ecriture.exercice(), ecriture.origine(), ecriture.typeJournal(), ecriture
				.typeOperation(), utilisateur);// FIXED
		if (lesNouveauxDetailsDebit != null) {
			i = 0;
			while (i < lesNouveauxDetailsDebit.count()) {
				// tous les EcrituresDetails doivent appartenir a ecriture
				if (!(((EOEcritureDetail) lesNouveauxDetailsDebit.objectAtIndex(i)).ecriture().equals(ecriture))) {
					throw new EcritureDetailException(EcritureDetailException.problemeRelationEcriture);
				}

				ed.insertObject((EOEcritureDetail) lesNouveauxDetailsDebit.objectAtIndex(i));
				//				((EOEcritureDetail) lesNouveauxDetailsDebit.objectAtIndex(i)).addObjectToBothSidesOfRelationshipWithKey(nouvelleEcriture, "ecriture");
				((EOEcritureDetail) lesNouveauxDetailsDebit.objectAtIndex(i)).setEcritureRelationship(nouvelleEcriture);
				i++;
			}
		}

		if (lesNouveauxDetailsCredit != null) {
			i = 0;
			while (i < lesNouveauxDetailsCredit.count()) {
				// tous les EcrituresDetails doivent appartenir a ecriture
				trace("---------------------------------");
				trace("ecriture = " + ecriture);
				trace("ecritureDetail.ecriture = " + ((EOEcritureDetail) lesNouveauxDetailsCredit.objectAtIndex(i)).ecriture());
				trace("---------------------------------");

				if (!(((EOEcritureDetail) lesNouveauxDetailsCredit.objectAtIndex(i)).ecriture().equals(ecriture))) {
					throw new EcritureDetailException(EcritureDetailException.problemeRelationEcriture);
				}

				ed.insertObject((EOEcritureDetail) lesNouveauxDetailsCredit.objectAtIndex(i));

				//				((EOEcritureDetail) lesNouveauxDetailsCredit.objectAtIndex(i)).addObjectToBothSidesOfRelationshipWithKey(nouvelleEcriture, "ecriture");
				((EOEcritureDetail) lesNouveauxDetailsCredit.objectAtIndex(i)).setEcritureRelationship(nouvelleEcriture);
				i++;
			}
		}
		return nouvelleEcriture;
	}

	/**
	 * Permet d ajouter une ecriture dans un brouillard ouvert
	 * 
	 * @param ed editingContext de travail
	 * @param ecrDateSaisie date de saisie de l ecriture
	 * @param ecrEtat etat de l ecriture
	 * @param ecrLibelle libelle de l ecriture
	 * @param ecrNumeroBrouillard numero de l ecriture dans la brouillard
	 * @param ecrPostit posit it ...remarques
	 * @param brouillard brouillard de l ecriture
	 * @param comptabilite Agence comptable
	 * @param detailEcriture les details de l ecriture
	 * @param exercice Exercice de l ecriture
	 * @param origine oringine : Res. Affec. , Ope. Lucrat.
	 * @param typeJournal type de journal
	 * @param typeOperation type d orperation a l origine de cette ecriture
	 * @param utilisateur utilisateur a l origine de cette ecriture
	 * @param lesEcrituresDetails
	 * @return
	 * @throws EcritureException
	 */
	public EOEcriture creerEcritureBrouillard(EOEditingContext ed, NSTimestamp ecrDateSaisie, String ecrLibelle, Integer ecrNumeroBrouillard, String ecrPostit, EOBrouillard brouillard, EOComptabilite comptabilite, EOExercice exercice, EOOrigine origine, EOTypeJournal typeJournal,
			EOTypeOperation typeOperation, EOUtilisateur utilisateur) throws EcritureException {

		this.trace("");
		this.trace("creerEcritureBrouillard");
		this.trace("comptabilite = " + comptabilite);
		this.trace("ecrDateSaisie = " + ecrDateSaisie);
		this.trace("exercice = " + exercice);
		this.trace("origine = " + origine);
		this.trace("typeJournal = " + typeJournal);
		this.trace("typeOperation = " + typeOperation);
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");
		// le brouillard est il ouvert

		// creation de la nouvelle Ecriture
		EOEcriture nouvelleEcriture = (EOEcriture) Factory.instanceForEntity(ed, EOEcriture.ENTITY_NAME);

		ed.insertObject(nouvelleEcriture);

		nouvelleEcriture.setEcrDate(Factory.getNow());
		nouvelleEcriture.setEcrDateSaisie(ecrDateSaisie);
		nouvelleEcriture.setEcrEtat(EOEcriture.ecritureBrouillard);
		nouvelleEcriture.setEcrLibelle(ecrLibelle);
		nouvelleEcriture.setEcrNumeroBrouillard(ecrNumeroBrouillard);

		nouvelleEcriture.setEcrPostit(ecrPostit);

		nouvelleEcriture.addObjectToBothSidesOfRelationshipWithKey(brouillard, "brouillard");
		nouvelleEcriture.addObjectToBothSidesOfRelationshipWithKey(comptabilite, "comptabilite");
		nouvelleEcriture.addObjectToBothSidesOfRelationshipWithKey(exercice, "exercice");
		nouvelleEcriture.addObjectToBothSidesOfRelationshipWithKey(origine, "origine");
		nouvelleEcriture.addObjectToBothSidesOfRelationshipWithKey(typeJournal, "typeJournal");
		nouvelleEcriture.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

		try {
			nouvelleEcriture.validateObjectMetier();
			return nouvelleEcriture;
		} catch (ValidationException e) {
			e.printStackTrace();
			System.out.println(nouvelleEcriture);
			return null;
		}

	}

	/**
	 * Permet d annuler une ecriture du brouillard
	 * 
	 * @param ed editingContext de travail
	 * @param ecriture ecriture a supprimer
	 * @param utilisateur utilisateur a l origine de la suppression
	 * @throws EcritureException
	 */
	public void annulerEcritureBrouillard(EOEditingContext ed, EOEcriture ecriture, EOUtilisateur utilisateur) throws EcritureException {

		this.trace("");
		this.trace("annulerEcritureBrouillard");
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");
		// on passe ecriture a ANNULE si pas ecriture VALIDE
		if (ecriture.ecrEtat().equals(EOEcriture.ecritureValide)) {
			throw new EcritureException(EcritureException.impossibleEcritureValide);
		}

		if (ecriture.ecrEtat().equals(EOEcriture.ecritureAnnule)) {
			throw new EcritureException(EcritureException.impossibleEcritureAnnule);
		}

		ecriture.setEcrEtat(EOEcriture.ecritureAnnule);
		ecriture.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

	}

	/**
	 * Permet de rectifier une ecriture du brouillard
	 * 
	 * @param ed editingContext de travail
	 * @param ecriture ecriture a rectifier
	 * @param annulerEcrituresDetails les details a supprimer
	 * @param nouveauxEcrituresDetails les details a ajouter
	 * @param utilisateur utilisateur utilisateur a l origine de la rectification
	 * @throws EcritureDetailException
	 */
	public void rectifierEcritureBrouillard(EOEditingContext ed, EOEcriture ecriture, NSArray annulerEcrituresDetails, NSArray nouveauxEcrituresDetails, EOUtilisateur utilisateur) throws EcritureDetailException {

		this.trace("");
		this.trace("rectifierEcritureBrouillard");
		this.trace("NSArray annulerEcrituresDetails = " + annulerEcrituresDetails);
		this.trace("NSArray nouveauxEcrituresDetails = " + nouveauxEcrituresDetails);
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");
		// tous les annulerEcrituresDetails doivent appartenir a ecriture

		// apres les nouveauxEcrituresDetails DEBIT = CREDIT

		// LES MASSES DE L ECRITURES NE DOIVENT PAS CHANGER
		int i = 0;
		while (annulerEcrituresDetails.count() > i) {

			// tous les EcrituresDetails doivent appartenir a ecriture
			if (!(((EOEcritureDetail) annulerEcrituresDetails.objectAtIndex(i)).ecriture().equals(ecriture))) {
				throw new EcritureDetailException(EcritureDetailException.problemeRelationEcriture);
			}

			ed.deleteObject((EOEcritureDetail) annulerEcrituresDetails.objectAtIndex(i));
			i++;
		}

		i = 0;
		while (nouveauxEcrituresDetails.count() > i) {
			// tous les EcrituresDetails doivent appartenir a ecriture
			if (!(((EOEcritureDetail) nouveauxEcrituresDetails.objectAtIndex(i)).ecriture().equals(ecriture))) {
				throw new EcritureDetailException(EcritureDetailException.problemeRelationEcriture);
			}

			ed.insertObject((EOEcritureDetail) nouveauxEcrituresDetails.objectAtIndex(i));
			i++;
		}

		ecriture.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

	}

	/**
	 * Renvoie un tableau avec les ecritures SACD. Le tableau ne contient pas l'écriture initiale passée en paremetre.
	 * 
	 * @param ed
	 * @param monEcriture
	 * @return
	 * @throws Exception
	 */
	public NSArray determinerLesEcrituresSACD(EOEditingContext ed, EOEcriture monEcriture) throws Exception {

		NSMutableArray lesEcritures = new NSMutableArray();

		NSMutableArray lesDebits = new NSMutableArray();
		NSMutableArray lesCredits = new NSMutableArray();
		NSMutableArray listeDesSacd = new NSMutableArray();

		//boolean casSimple = true;
		int i = 0;
		//recuperer les sacd presents dans l'ecriture
		listeDesSacd.addObjectsFromArray(lesSacds(ed, monEcriture.detailEcriture()));
		//trace(" listeDesSacd detectes" + listeDesSacd);
		if (listeDesSacd.count() > 0) {
			while (i < monEcriture.detailEcriture().count()) {
				// les credits
				if (Factory.sensCredit().equals(((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(i)).ecdSens())) {
					lesCredits.addObject(monEcriture.detailEcriture().objectAtIndex(i));
				}

				// les debits
				if (Factory.sensDebit().equals(((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(i)).ecdSens())) {
					lesDebits.addObject(monEcriture.detailEcriture().objectAtIndex(i));
				}
				i++;
			}

			if ((lesCredits.count() == 0) || (lesDebits.count() == 0)) {
				return determinerLesEcrituresSACDMemeSens(ed, monEcriture);
			}

			else {
				// TOUS les DEBIT = un gestion et TOUS les CREDIT un code gestion !
				if (this.memeCodeGestion(lesDebits) && this.memeCodeGestion(lesCredits)) {
					//trace("*****************TOUS les DEBIT = un gestion et TOUS les DEBIT un code gestion !******************");
					// si gestion D = gestion C on retourne l ecriture !
					if (((EOEcritureDetail) lesDebits.objectAtIndex(0)).gestion().equals(((EOEcritureDetail) lesCredits.objectAtIndex(0)).gestion())) {
						trace("***************RIEN A FAIRE GESTION DEBIT + GESTION CREDIT******************");
					}
					else {
						trace("*****************determinerLesEcrituresSACDSimple  l ancienne !******************");
						lesEcritures.addObjectsFromArray(this.determinerLesEcrituresSACDSimple(ed, monEcriture));
					}
				}
				else {

					trace("*****************determinerLesEcrituresSACD memeCodeGestion******************");
					trace("memeCodeGestion lesCredits" + this.memeCodeGestion(lesCredits));
					trace(" memeCodeGestion lesDebits" + this.memeCodeGestion(lesDebits));

					// si 1 gestion en DEBIT et N en CREDIT
					if (this.memeCodeGestion(lesDebits)) {
						trace("*****************si 1 gestion en DEBIT et N en CREDIT!******************");
						if (!(this.memeCodeGestion(lesCredits))) {
							lesEcritures.addObjectsFromArray(determinerLesEcrituresSACDComplex(ed, monEcriture, true, false));
						}
					}

					// si 1 gestion en CREDIT et N en DEBIT
					if (this.memeCodeGestion(lesCredits)) {
						trace("*****************si 1 gestion en CREDIT et N en DEBIT*****************");
						if (!(this.memeCodeGestion(lesDebits))) {
							lesEcritures.addObjectsFromArray(determinerLesEcrituresSACDComplex(ed, monEcriture, false, true));
						}

					}

					if ((!(this.memeCodeGestion(lesDebits))) && (!(this.memeCodeGestion(lesCredits)))) {
						throw new FactoryException("IMPOSSIBLE DE DETERMINER L ECRITURE SACD");
					}
				}
			}

		}

		return lesEcritures.immutableClone();

	}

	/**
	 * Determine les ecritures SACD dans le cas d'une annulation ou rectification (D- / D+ ou C- / C+). Dans cas on traite les choses comme si les -
	 * étaient des debits et les + des credits.
	 * 
	 * @param ed
	 * @param monEcriture
	 * @return
	 * @throws Exception
	 */
	private NSArray determinerLesEcrituresSACDMemeSens(EOEditingContext ed, EOEcriture monEcriture) throws Exception {
		NSMutableArray lesEcritures = new NSMutableArray();
		NSArray debits = monEcriture.detailEcriture(EOEcritureDetail.QUAL_DEBITS);
		NSArray credits = monEcriture.detailEcriture(EOEcritureDetail.QUAL_CREDITS);

		//verifier qu'on a  bien que des credit ou que des debits
		if (debits.count() > 0 && credits.count() > 0) {
			throw new Exception("determinerLesEcrituresSACDMemeSens : Erreur : l'ecriture doit comporter que des debit ou que des credits");
		}

		NSArray lesMoins = monEcriture.detailEcriture(EOEcritureDetail.QUAL_NEGATIF);
		NSArray lesPlus = monEcriture.detailEcriture(EOEcritureDetail.QUAL_POSITIF);

		//Verifier les codes gestion

		//si on est sur le meme code gestion on ne fait rien

		// TOUS les DEBIT = un gestion et TOUS les CREDIT un code gestion !
		if (this.memeCodeGestion(lesMoins) && this.memeCodeGestion(lesPlus)) {
			//trace("*****************TOUS les DEBIT = un gestion et TOUS les DEBIT un code gestion !******************");
			if (!((EOEcritureDetail) lesMoins.objectAtIndex(0)).gestion().equals(((EOEcritureDetail) lesPlus.objectAtIndex(0)).gestion())) {
				lesEcritures.addObjectsFromArray(this.determinerLesEcrituresSACDSimpleMemeSens(ed, monEcriture));
			}
		}
		else {
			// si 1 gestion en - et N en +
			if (this.memeCodeGestion(lesMoins)) {
				if (!(this.memeCodeGestion(lesPlus))) {
					lesEcritures.addObjectsFromArray(determinerLesEcrituresSACDComplexMemeSens(ed, monEcriture, true, false));
				}
			}

			// si 1 gestion en CREDIT et N en DEBIT
			if (this.memeCodeGestion(lesPlus)) {
				trace("*****************si 1 gestion en CREDIT et N en DEBIT*****************");
				if (!(this.memeCodeGestion(lesMoins))) {
					lesEcritures.addObjectsFromArray(determinerLesEcrituresSACDComplexMemeSens(ed, monEcriture, false, true));
				}

			}

			if ((!(this.memeCodeGestion(lesMoins))) && (!(this.memeCodeGestion(lesPlus)))) {
				throw new FactoryException("IMPOSSIBLE DE DETERMINER L ECRITURE SACD");
			}
		}
		return lesEcritures.immutableClone();
	}

	/**
	 * determine les ecriture sacd lorsque
	 * 
	 * @param ed
	 * @param monEcriture
	 * @param credits
	 * @param debits
	 * @return
	 * @throws Exception
	 */
	private NSArray determinerLesEcrituresSACDComplex(EOEditingContext ed, EOEcriture monEcriture, boolean credits, boolean debits) throws Exception {

		NSMutableArray lesEcritures = new NSMutableArray();
		NSMutableArray lesCredits = new NSMutableArray();
		NSMutableArray lesDebits = new NSMutableArray();
		NSMutableArray lesSacds = new NSMutableArray();
		NSMutableArray detailATraiter = new NSMutableArray();
		NSMutableArray lesNouveauxDetails = new NSMutableArray();
		BigDecimal montant = new BigDecimal(0);
		int indexEcd = 100;
		String sens = sensCredit();
		String senstemp = null;
		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());
		EOEcriture maNouvelleEcriture = null;

		trace("***********************************");
		trace("determinerLesEcrituresSACDComplex");

		// init de l agence
		EOGestion agence = ((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(0)).gestion().comptabilite().gestion();

		trace("***********************************");
		trace(" init de l agence" + agence);

		int i = 0;

		// recup des tableaux
		while (i < monEcriture.detailEcriture().count()) {
			// les credits
			if (Factory.sensCredit().equals(((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(i)).ecdSens())) {
				lesCredits.addObject(monEcriture.detailEcriture().objectAtIndex(i));
			}

			// les debits
			if (Factory.sensDebit().equals(((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(i)).ecdSens())) {
				lesDebits.addObject(monEcriture.detailEcriture().objectAtIndex(i));
			}
			i++;
		}

		trace("***********************************");
		trace(" init lesCredits" + lesCredits);

		trace("***********************************");
		trace(" init lesDebits" + lesDebits);

		// init du du sens
		if (credits) {
			sens = SENS_CREDIT;
			lesSacds.addObjectsFromArray(FactoryProcessJournalEcriture.lesSacds(ed, lesCredits));
			detailATraiter.addObjectsFromArray(lesCredits);
		}
		else {
			sens = SENS_DEBIT;
			lesSacds.addObjectsFromArray(FactoryProcessJournalEcriture.lesSacds(ed, lesDebits));
			detailATraiter.addObjectsFromArray(lesDebits);
		}
		trace("***********************************");
		trace(" init detailATraiter" + detailATraiter);
		trace("***********************************");
		trace(" init lesSacds" + lesSacds);
		trace("***********************************");
		trace(" init sens" + sens);

		// CREATION DES ECRITURES DETAILS SACD !
		i = 0;
		while (i < lesSacds.count()) {
			// somme du montant des details ecritures pour le SACD en cours
			montant = this.sommeDesDetailsPourLeCodeGestion(detailATraiter, (EOGestion) lesSacds.objectAtIndex(i));

			trace("***********************************");
			trace(" init montant" + montant);

			trace("***********************************");
			trace(" lesNouveauxDetails 1");

			// creation de l ecritureDetail du SACD
			senstemp = this.inverserSens(sens);
			indexEcd++;
			lesNouveauxDetails.addObject(maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", senstemp, monEcriture, monEcriture.exercice(), (EOGestion) lesSacds.objectAtIndex(i),
					//					FinderJournalEcriture.planComptable185(ed)));
					((EOGestion) lesSacds.objectAtIndex(i)).getPlanco185CtpSacd(monEcriture.exercice())
					));

			trace("***********************************");
			trace(" lesNouveauxDetails 2");

			// creation de l ecritureDetail de l agence Comptable
			senstemp = this.inverserSens(senstemp);
			indexEcd++;
			// Modif code gestion exer
			//			final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(((EOGestion) lesSacds.objectAtIndex(i)), monEcriture.exercice());
			final EOPlanComptable planco185 = ((EOGestion) lesSacds.objectAtIndex(i)).getPlanco185(monEcriture.exercice());
			lesNouveauxDetails.addObject(maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", senstemp, monEcriture, monEcriture.exercice(), agence, planco185));

			trace("***********************************");
			trace(" lesNouveauxDetails " + lesNouveauxDetails);
			i++;
		}

		// REAFFECTATION DES ECRITURES !
		// ajout des nouveaudetail
		detailATraiter.addObjectsFromArray(lesNouveauxDetails);

		trace("***********************************");
		trace(" detailATraiter " + detailATraiter);

		i = 0;
		while (i < lesSacds.count()) {
			// on cree une ecriture pour ce SACD
			maNouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique sacd", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(), monEcriture.typeOperation(),
					monEcriture.utilisateur());

			trace("***********************************");
			trace(" maNouvelleEcriture " + maNouvelleEcriture);

			lesEcritures.addObject(maNouvelleEcriture);

			trace("***********************************");
			trace(" lesEcritures " + lesEcritures);

			// on affecte les detailsEcriture pour ce SACD
			int j = 0;
			while (detailATraiter.count() > j) {
				if (((EOEcritureDetail) detailATraiter.objectAtIndex(j)).gestion().equals(lesSacds.objectAtIndex(i))) {
					maFactoryEcritureDetail.attribuerUnEcritureDetail(ed, maNouvelleEcriture, (EOEcritureDetail) detailATraiter.objectAtIndex(j));
				}
				j++;
			}
			i++;
		}

		trace("***********************************");
		trace(" lesEcritures " + lesEcritures);

		return lesEcritures;
	}

	private NSArray determinerLesEcrituresSACDComplexMemeSens(EOEditingContext ed, EOEcriture monEcriture, boolean plus, boolean moins) throws Exception {

		NSMutableArray lesEcritures = new NSMutableArray();
		NSMutableArray lesSacds = new NSMutableArray();
		NSMutableArray detailATraiter = new NSMutableArray();
		NSMutableArray lesNouveauxDetails = new NSMutableArray();
		BigDecimal montant = new BigDecimal(0);
		int indexEcd = 100;
		int sens = 1;
		int senstemp = 0;
		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());
		EOEcriture maNouvelleEcriture = null;

		NSArray lesMoins = monEcriture.detailEcriture(EOEcritureDetail.QUAL_NEGATIF);
		NSArray lesPlus = monEcriture.detailEcriture(EOEcritureDetail.QUAL_POSITIF);

		String seulSens = ((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(0)).ecdSens();

		// init de l agence
		EOGestion agence = ((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(0)).gestion().comptabilite().gestion();
		int i = 0;

		// init du du sens
		if (plus) {
			sens = 1;
			lesSacds.addObjectsFromArray(FactoryProcessJournalEcriture.lesSacds(ed, lesPlus));
			detailATraiter.addObjectsFromArray(lesPlus);
		}
		else {
			sens = -1;
			lesSacds.addObjectsFromArray(FactoryProcessJournalEcriture.lesSacds(ed, lesMoins));
			detailATraiter.addObjectsFromArray(lesMoins);
		}

		// CREATION DES ECRITURES DETAILS SACD !
		i = 0;
		BigDecimal montantTheorique = null;
		BigDecimal montantReel = null;
		while (i < lesSacds.count()) {
			// somme du montant des details ecritures pour le SACD en cours
			montant = this.sommeDesDetailsPourLeCodeGestion(detailATraiter, (EOGestion) lesSacds.objectAtIndex(i));

			// creation de l ecritureDetail du SACD
			//			senstemp = sens * (-1);
			senstemp = sens;
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(senstemp));
			String sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			lesNouveauxDetails.addObject(maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(),
					(EOGestion) lesSacds.objectAtIndex(i),
					((EOGestion) lesSacds.objectAtIndex(i)).getPlanco185CtpSacd(monEcriture.exercice())));
			//	FinderJournalEcriture.planComptable185(ed)));
			//			lesNouveauxDetails.addObject(maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(senstemp)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(),
			//					(EOGestion) lesSacds.objectAtIndex(i),
			//					FinderJournalEcriture.planComptable185(ed)));

			// creation de l ecritureDetail de l agence Comptable
			senstemp = senstemp * (-1);
			indexEcd++;
			// Modif code gestion exer
			//final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(((EOGestion) lesSacds.objectAtIndex(i)), monEcriture.exercice());
			final EOPlanComptable planco185 = ((EOGestion) lesSacds.objectAtIndex(i)).getPlanco185(monEcriture.exercice());

			montantTheorique = montant.multiply(new BigDecimal(senstemp));
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}

			lesNouveauxDetails.addObject(maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), agence,
					planco185));
			//			lesNouveauxDetails.addObject(maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(senstemp)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(), agence,
			//					planco185));

			i++;
		}

		// REAFFECTATION DES ECRITURES !
		// ajout des nouveaudetail
		detailATraiter.addObjectsFromArray(lesNouveauxDetails);

		i = 0;
		while (i < lesSacds.count()) {
			// on cree une ecriture pour ce SACD
			maNouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique sacd", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(), monEcriture.typeOperation(),
					monEcriture.utilisateur());

			lesEcritures.addObject(maNouvelleEcriture);

			// on affecte les detailsEcriture pour ce SACD
			int j = 0;
			while (detailATraiter.count() > j) {
				if (((EOEcritureDetail) detailATraiter.objectAtIndex(j)).gestion().equals(lesSacds.objectAtIndex(i))) {
					maFactoryEcritureDetail.attribuerUnEcritureDetail(ed, maNouvelleEcriture, (EOEcritureDetail) detailATraiter.objectAtIndex(j));
				}
				j++;
			}
			i++;
		}

		trace("***********************************");
		trace(" lesEcritures " + lesEcritures);

		return lesEcritures;
	}

	/**
	 * determine les ecritures SACD // pour tous les DEBIT OU CREDIT DU MEME CODE GESTION
	 */
	private NSArray determinerLesEcrituresSACDSimple(EOEditingContext ed, EOEcriture monEcriture) throws Exception {

		EOGestion agence = null;
		EOGestion sacd1 = null;
		EOGestion sacd2 = null;

		EOEcriture monEcritureAgence = null;
		EOEcriture monEcritureSacd1 = null;
		EOEcriture monEcritureSacd2 = null;

		EOPlanComptable pcoSacd1 = null;
		EOPlanComptable pcoSacd2 = null;
		//EOPlanComptable pco185 = null;

		BigDecimal montant = new BigDecimal(0);

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

		String sensAgence = "INIT";
		String sensSacd1 = "INIT";
		String sens = "INIT";

		int indexEcd = 100;
		NSArray tmpDetails = null;
		NSMutableArray lesMontants = new NSMutableArray();
		NSMutableArray lesNouvellesEcritures = new NSMutableArray();

		// Si pas de details, on renvoie null
		if (monEcriture.detailEcriture() == null) {
			trace("Pas de detail ecriture, pas d'ecriture SACD");
			return null;
		}
		// vérifier qu'on n'est pas dans le même SACD (si tous les codes
		// gestions sont identiques on renvoie null)
		String gesCodeTmp = ((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(0)).gestion().gesCode();
		boolean identiques = true;
		for (int i = 1; i < monEcriture.detailEcriture().count(); i++) {
			EOEcritureDetail element = (EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(i);
			if (!gesCodeTmp.equals(element.gestion().gesCode())) {
				identiques = false;
			}
		}
		if (identiques) {
			trace("les codes gestions sont identiques, pas d'ecriture SACD");
			return null;
		}

		// impossible sens D et C pour meme AGENCE COMPTABLE OU SACD
		// impossible sens D et C pour meme SACD1 et SACD2

		// 1 SACD - AGENCE COMPTABLE
		NSArray lesDetails = monEcriture.detailEcriture();
		int i = 0;
		while (i < lesDetails.count()) {
			// determine l agence !!!
			if (((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().gesCode().equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().comptabilite().gestion().gesCode()) && agence == null) {
				agence = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();
				sensAgence = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdSens();

				this.trace("-----------------------------");
				this.trace(" AGENCE ? ");
				this.trace(" AGENCE : " + agence);
				this.trace(" SENS AGENCE : " + sensAgence);
			}

			// determine SACD1
			if (!(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().gesCode().equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().comptabilite().gestion().gesCode())) && (sacd1 == null)) {
				{
					sacd1 = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();
					sensSacd1 = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdSens();

					this.trace("-----------------------------");
					this.trace(" SACD1 ? ");
					this.trace(" SACD1: " + sacd1);
					this.trace(" SENS SACD1: " + sensSacd1);
				}
			}

			// determine SACD2
			if (!(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().gesCode().equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().comptabilite().gestion().gesCode())) && (sacd1 != null) && (sacd2 == null)
					&& !(sacd1.equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion()))) {
				sacd2 = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();

				this.trace("-----------------------------");
				this.trace(" SACD2 ? ");
				this.trace(" SACD2: " + sacd2);
			}

			if ((((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdSens()).equals(SENS_CREDIT)) {
				lesMontants.addObject(lesDetails.objectAtIndex(i));
			}
			i++;
		}

		/*
		 * montant = new BigDecimal(this.computeSumForKey( lesMontants, "ecdMontant"));
		 */
		montant = this.computeSumForKeyBigDecimal(lesMontants, "ecdMontant");

		this.trace(" MONTANT DES CREDITS " + montant);

		this.trace("-----------------------------");
		this.trace("agence " + agence);
		this.trace("sacd1 " + sacd1);
		this.trace("sacd2 " + sacd2);

		this.trace("-----------------------------");
		this.trace("sens agence " + sensAgence);
		this.trace("sens sacd 1" + sensSacd1);

		// init du sens
		if (sensAgence.equals("INIT")) {
			sens = sensSacd1;
		}
		else {
			sens = sensAgence;
		}

		// if (sacd1!=null) {
		// pcoSacd1 = sacd1.planComptable185();
		// }
		//
		// if (sacd2!=null) {
		// pcoSacd2 = sacd2.planComptable185();
		// }
		//        
		if (sacd1 != null) {
			// Modif code gestion exer
			//			final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(sacd1, monEcriture.exercice());
			final EOPlanComptable planco185 = sacd1.getPlanco185(monEcriture.exercice());
			pcoSacd1 = planco185;
		}

		if (sacd2 != null) {
			// Modif code gestion exer
			//final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(sacd2, monEcriture.exercice());
			final EOPlanComptable planco185 = sacd2.getPlanco185(monEcriture.exercice());
			;
			;
			pcoSacd2 = planco185;
		}
		//pco185 = FinderJournalEcriture.planComptable185(ed);
		//pco185 = ((EOGestion) lesSacds.objectAtIndex(i)).getPlanco185CtpSacd(monEcriture.exercice());

		// verifier que les 2 sacd ne sont pas le meme SACD !
		if (sacd1 != null && sacd2 != null) {
			if (sacd1.gesCode().equals(sacd2.gesCode())) {
				return null;
			}
		}

		this.trace("-----------------------------");
		this.trace(" compte 185 sacd1 " + pcoSacd1);
		this.trace(" compte 185 sacd2  " + pcoSacd2);
		//	this.trace(" compte 185 " + pco185);
		this.trace(" sensde DEPART " + sens);

		if (pcoSacd1 == null && pcoSacd2 == null) {
			return null;
		}

		if (agence != null) {
			this.trace("-----------------------------");
			this.trace("1 SACD ET AGENCE");
			// origine ecriture AGENCE

			// AGENCE inverser le sens et faire la somme des montants de l agence X compte 185sacd
			sens = this.inverserSens(sens);
			indexEcd++;
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), agence, pcoSacd1);

			this.trace("-----------------------------");
			this.trace(" 11 les  details de monEcriture " + monEcriture.detailEcriture());

			// SACD inverser de nouveau de sens montant X compte 185
			sens = this.inverserSens(sens);
			indexEcd++;
			//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), sacd1, pco185);
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), sacd1, sacd1.getPlanco185CtpSacd(monEcriture.exercice()));

			this.trace("-----------------------------");
			this.trace(" 12 les  details de monEcriture " + monEcriture.detailEcriture());

			// ecriture SACD 1
			monEcritureSacd1 = monEcriture;
			// creer nouvelle ECRITURE AGENCE
			monEcritureAgence = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique Agence", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(),
					monEcriture.typeOperation(), monEcriture.utilisateur());

			lesNouvellesEcritures.addObject(monEcritureAgence);

			this.trace("-----------------------------");
			this.trace(" les  details de monEcritureAGENCE " + monEcritureAgence.detailEcriture());
			this.trace(" les  details de monEcritureSACD1 " + monEcritureSacd1.detailEcriture());

		}
		else {
			// 2 SACDs
			agence = sacd1.comptabilite().gestion();

			this.trace("-----------------------------");
			this.trace(" 2SACD agence  GESTION" + agence);

			// origine SACD 1

			// SACD1 inversion du sens montant somme X des montants SACD1 185
			sens = this.inverserSens(sensSacd1);
			indexEcd++;
			//maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), sacd1, pco185);
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), sacd1, sacd1.getPlanco185CtpSacd(monEcriture.exercice()));

			this.trace("-----------------------------");
			this.trace(" 21 les  details de monEcriture " + monEcriture.detailEcriture());

			// AGENCE inversion sens montant X 185sacd1
			sens = this.inverserSens(sens);
			indexEcd++;
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), agence, pcoSacd1);

			this.trace("-----------------------------");
			this.trace(" 22 les  details de monEcriture " + monEcriture.detailEcriture());

			// AGENCE inversion sens montant X 185 sacd2
			sens = this.inverserSens(sens);
			indexEcd++;
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), agence, pcoSacd2);

			this.trace("-----------------------------");
			this.trace(" 23 les  details de monEcriture " + monEcriture.detailEcriture());

			// SACD2 inversion sens montant X 185
			sens = this.inverserSens(sens);
			indexEcd++;
			//maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), sacd2, pco185);
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant, "", sens, monEcriture, monEcriture.exercice(), sacd2, sacd2.getPlanco185CtpSacd(monEcriture.exercice()));

			this.trace("-----------------------------");
			this.trace(" 24 les  details de monEcriture " + monEcriture.detailEcriture());

			// ecriture SACD 1
			monEcritureSacd1 = monEcriture;
			// creer nouvelle ECRITURE AGENCE
			monEcritureAgence = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique agence", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(),
					monEcriture.typeOperation(), monEcriture.utilisateur());// FIXED
			// chgt
			// exercice
			// date
			// journee
			// comptable
			// monEcritureAgence =
			// maFactoryProcessJournalEcriture.creerEcriture(
			// ed,
			// new NSTimestamp(),
			// monEcriture.ecrLibelle(),
			// new Integer(0),
			// "automatique agence",
			// monEcriture.comptabilite(),
			// monEcriture.exercice(),
			// monEcriture.origine(),
			// monEcriture.typeJournal(),
			// monEcriture.typeOperation(),
			// monEcriture.utilisateur()
			// );

			this.trace("-----------------------------");
			this.trace(" CREATION ECRITURE AGENCE " + monEcritureAgence);
			this.trace("  ECRITURE SACD1" + monEcritureSacd1);

			lesNouvellesEcritures.addObject(monEcritureAgence);

			// creer nouvelle ECRITURE SACD2
			monEcritureSacd2 = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique sacd2", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(), monEcriture.typeOperation(),
					monEcriture.utilisateur());// FIXED
			// chgt
			// exercice
			// date
			// journee
			// comptable
			// monEcritureSacd2 = maFactoryProcessJournalEcriture.creerEcriture(
			// ed,
			// new NSTimestamp(),
			// monEcriture.ecrLibelle(),
			// new Integer(0),
			// "automatique sacd2",
			// monEcriture.comptabilite(),
			// monEcriture.exercice(),
			// monEcriture.origine(),
			// monEcriture.typeJournal(),
			// monEcriture.typeOperation(),
			// monEcriture.utilisateur()
			// );

			this.trace("-----------------------------");
			this.trace(" NOUVELLE ECRITURE SACD2" + monEcritureSacd2);

			lesNouvellesEcritures.addObject(monEcritureSacd2);
		}

		this.trace("-----------------------------");
		this.trace(" LES NOUVELLES  ECRITURES" + lesNouvellesEcritures);

		this.trace("***********REAFFECTATION DES LIGNES D ECRITURE***********");

		// mise ajour des details et des ecritures!
		tmpDetails = new NSArray(monEcriture.detailEcriture());
		i = 0;
		while (i < tmpDetails.count()) {
			if (sacd1 != null) {
				if (((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).gestion().equals(sacd1)) {
					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).setEcritureRelationship(monEcritureSacd1);
					//					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(monEcritureSacd1, "ecriture");
				}
			}

			if (sacd2 != null) {
				if (((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).gestion().equals(sacd2)) {
					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).setEcritureRelationship(monEcritureSacd2);
					//					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(monEcritureSacd2, "ecriture");
				}
			}

			if (((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).gestion().equals(agence)) {
				((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).setEcritureRelationship(monEcritureAgence);
				//				((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(monEcritureAgence, "ecriture");
			}

			i++;
		}

		this.trace("-----------------------------");

		if (monEcritureAgence != null) {
			this.trace(" DETAIL ECRITURE AGENCE" + monEcritureAgence.detailEcriture());
		}
		if (monEcritureSacd1 != null) {
			this.trace(" DETAIL ECRITURE SACD 1" + monEcritureSacd1.detailEcriture());
		}
		if (monEcritureSacd2 != null) {
			this.trace(" DETAIL ECRITURE SACD 2" + monEcritureSacd2.detailEcriture());
		}

		this.trace("----VERIFICATION AGENCE---------");
		// verifier les ecritures !
		monEcritureAgence.validateObjectMetier();

		this.trace("----VERIFICATION SACD1 ---------");
		if (monEcritureSacd1 != null) {
			monEcritureSacd1.validateObjectMetier();
		}

		this.trace("----VERIFICATION SACD2---------");
		if (monEcritureSacd2 != null) {
			monEcritureSacd2.validateObjectMetier();
		}

		return lesNouvellesEcritures;
	}

	private NSArray determinerLesEcrituresSACDSimpleMemeSens(EOEditingContext ed, EOEcriture monEcriture) throws Exception {
		EOGestion agence = null;
		EOGestion sacd1 = null;
		EOGestion sacd2 = null;

		EOEcriture monEcritureAgence = null;
		EOEcriture monEcritureSacd1 = null;
		EOEcriture monEcritureSacd2 = null;

		EOPlanComptable pcoSacd1 = null;
		EOPlanComptable pcoSacd2 = null;
		//EOPlanComptable pco185 = null;

		BigDecimal montant = new BigDecimal(0);

		String seulSens = ((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(0)).ecdSens();

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

		int sensAgence = 0;
		int sensSacd1 = 0;
		int sens = 0;

		int indexEcd = 100;
		NSArray tmpDetails = null;
		NSMutableArray lesMontants = new NSMutableArray();
		NSMutableArray lesNouvellesEcritures = new NSMutableArray();

		// Si pas de details, on renvoie null
		if (monEcriture.detailEcriture() == null) {
			trace("Pas de detail ecriture, pas d'ecriture SACD");
			return null;
		}
		// vï¿½rifier qu'on n'est pas dans le meï¿½m SACD (si tous les codes
		// gestions sont identiques on renvoie null)
		String gesCodeTmp = ((EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(0)).gestion().gesCode();
		boolean identiques = true;
		for (int i = 1; i < monEcriture.detailEcriture().count(); i++) {
			EOEcritureDetail element = (EOEcritureDetail) monEcriture.detailEcriture().objectAtIndex(i);
			if (!gesCodeTmp.equals(element.gestion().gesCode())) {
				identiques = false;
			}
		}
		if (identiques) {
			trace("les codes gestions sont identiques, pas d'ecriture SACD");
			return null;
		}

		// impossible sens D et C pour meme AGENCE COMPTABLE OU SACD
		// impossible sens D et C pour meme SACD1 et SACD2

		// 1 SACD - AGENCE COMPTABLE
		NSArray lesDetails = monEcriture.detailEcriture();
		int i = 0;
		while (i < lesDetails.count()) {
			// determine l agence !!!
			if (((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().gesCode().equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().comptabilite().gestion().gesCode()) && agence == null) {
				agence = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();
				//				sensAgence = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdSens();
				sensAgence = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdMontant().signum();

				this.trace("-----------------------------");
				this.trace(" AGENCE ? ");
				this.trace(" AGENCE : " + agence);
				this.trace(" SENS AGENCE : " + sensAgence);
			}

			// determine SACD1
			if (!(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().gesCode().equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().comptabilite().gestion().gesCode())) && (sacd1 == null)) {
				{
					sacd1 = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();
					sensSacd1 = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdMontant().signum();

					this.trace("-----------------------------");
					this.trace(" SACD1 ? ");
					this.trace(" SACD1: " + sacd1);
					this.trace(" SENS SACD1: " + sensSacd1);
				}
			}

			// determine SACD2
			if (!(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().gesCode().equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().comptabilite().gestion().gesCode())) && (sacd1 != null) && (sacd2 == null)
					&& !(sacd1.equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion()))) {
				sacd2 = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();

				this.trace("-----------------------------");
				this.trace(" SACD2 ? ");
				this.trace(" SACD2: " + sacd2);
			}

			if ((((EOEcritureDetail) lesDetails.objectAtIndex(i)).ecdMontant().signum() == 1)) {
				lesMontants.addObject(lesDetails.objectAtIndex(i));
			}
			i++;
		}

		/*
		 * montant = new BigDecimal(this.computeSumForKey( lesMontants, "ecdMontant"));
		 */
		montant = this.computeSumForKeyBigDecimal(lesMontants, EOEcritureDetail.ECD_MONTANT_KEY);

		this.trace(" MONTANT DES + " + montant);

		this.trace("-----------------------------");
		this.trace("agence " + agence);
		this.trace("sacd1 " + sacd1);
		this.trace("sacd2 " + sacd2);

		this.trace("-----------------------------");
		this.trace("sens agence " + sensAgence);
		this.trace("sens sacd 1" + sensSacd1);

		// init du sens
		if (sensAgence == 0) {
			sens = sensSacd1;
		}
		else {
			sens = sensAgence;
		}

		if (sacd1 != null) {
			// Modif code gestion exer
			//final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(sacd1, monEcriture.exercice());
			final EOPlanComptable planco185 = sacd1.getPlanco185(monEcriture.exercice());
			;
			pcoSacd1 = planco185;
		}

		if (sacd2 != null) {
			// Modif code gestion exer
			//final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(sacd2, monEcriture.exercice());
			final EOPlanComptable planco185 = sacd2.getPlanco185(monEcriture.exercice());
			;
			pcoSacd2 = planco185;
		}
		//pco185 = FinderJournalEcriture.planComptable185(ed);

		// verifier que les 2 sacd ne sont pas le meme SACD !
		if (sacd1 != null && sacd2 != null) {
			if (sacd1.gesCode().equals(sacd2.gesCode())) {
				return null;
			}
		}

		this.trace("-----------------------------");
		this.trace(" compte 185 sacd1 " + pcoSacd1);
		this.trace(" compte 185 sacd2  " + pcoSacd2);
		//	this.trace(" compte 185 " + pco185);
		this.trace(" sensde DEPART " + sens);

		if (pcoSacd1 == null && pcoSacd2 == null) {
			return null;
		}
		BigDecimal montantTheorique = null;
		BigDecimal montantReel = null;
		String sensReel = null;
		if (agence != null) {
			this.trace("-----------------------------");
			this.trace("1 SACD ET AGENCE");
			// origine ecriture AGENCE

			// AGENCE inverser le sens et faire la somme des montants de l
			// agence X compte 185sacd
			sens = sens * (-1);
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(sens));
			sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			else {
				montantReel = montantTheorique;
				sensReel = seulSens;
			}
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), agence, pcoSacd1);

			this.trace("-----------------------------");
			this.trace(" 11 les  details de monEcriture " + monEcriture.detailEcriture());

			// SACD inverser de nouveau de sens montant X compte 185
			sens = sens * (-1);
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(sens));
			sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			else {
				montantReel = montantTheorique;
				sensReel = seulSens;
			}
			//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), sacd1, pco185);
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), sacd1, sacd1.getPlanco185CtpSacd(monEcriture.exercice()));

			this.trace("-----------------------------");
			this.trace(" 12 les  details de monEcriture " + monEcriture.detailEcriture());

			// ecriture SACD 1
			monEcritureSacd1 = monEcriture;
			// creer nouvelle ECRITURE AGENCE
			monEcritureAgence = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique Agence", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(),
					monEcriture.typeOperation(), monEcriture.utilisateur());// FIXED

			lesNouvellesEcritures.addObject(monEcritureAgence);

			this.trace("-----------------------------");
			this.trace(" les  details de monEcritureAGENCE " + monEcritureAgence.detailEcriture());
			this.trace(" les  details de monEcritureSACD1 " + monEcritureSacd1.detailEcriture());

		}
		else {
			// 2 SACDs
			agence = sacd1.comptabilite().gestion();

			this.trace("-----------------------------");
			this.trace(" 2SACD agence  GESTION" + agence);

			// origine SACD 1

			// SACD1 inversion du sens montant somme X des montants SACD1 185
			sens = sens * (-1);
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(sens));
			sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			else {
				montantReel = montantTheorique;
				sensReel = seulSens;
			}
			//maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), sacd1, pco185);
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), sacd1, sacd1.getPlanco185CtpSacd(monEcriture.exercice()));

			this.trace("-----------------------------");
			this.trace(" 21 les  details de monEcriture " + monEcriture.detailEcriture());

			// AGENCE inversion sens montant X 185sacd1
			sens = sens * (-1);
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(sens));
			sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			else {
				montantReel = montantTheorique;
				sensReel = seulSens;
			}
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), agence, pcoSacd1);

			this.trace("-----------------------------");
			this.trace(" 22 les  details de monEcriture " + monEcriture.detailEcriture());

			// AGENCE inversion sens montant X 185 sacd2
			sens = sens * (-1);
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(sens));
			sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			else {
				montantReel = montantTheorique;
				sensReel = seulSens;
			}
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), agence, pcoSacd2);

			this.trace("-----------------------------");
			this.trace(" 23 les  details de monEcriture " + monEcriture.detailEcriture());

			// SACD2 inversion sens montant X 185
			sens = sens * (-1);
			indexEcd++;
			montantTheorique = montant.multiply(new BigDecimal(sens));
			sensReel = seulSens;
			if (montantTheorique.doubleValue() < 0) {
				montantReel = montantTheorique.negate();
				sensReel = inverserSens(seulSens);
			}
			else {
				montantReel = montantTheorique;
				sensReel = seulSens;
			}
			//	maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), sacd2, pco185);
			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montantReel, "sacd automatique", montantReel.abs(), "", sensReel, monEcriture, monEcriture.exercice(), sacd2, sacd2.getPlanco185CtpSacd(monEcriture.exercice()));

			this.trace("-----------------------------");
			this.trace(" 24 les  details de monEcriture " + monEcriture.detailEcriture());

			// ecriture SACD 1
			monEcritureSacd1 = monEcriture;
			// creer nouvelle ECRITURE AGENCE
			monEcritureAgence = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique agence", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(),
					monEcriture.typeOperation(), monEcriture.utilisateur());

			this.trace("-----------------------------");
			this.trace(" CREATION ECRITURE AGENCE " + monEcritureAgence);
			this.trace("  ECRITURE SACD1" + monEcritureSacd1);

			lesNouvellesEcritures.addObject(monEcritureAgence);

			// creer nouvelle ECRITURE SACD2
			monEcritureSacd2 = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique sacd2", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(), monEcriture.typeOperation(),
					monEcriture.utilisateur());

			this.trace("-----------------------------");
			this.trace(" NOUVELLE ECRITURE SACD2" + monEcritureSacd2);

			lesNouvellesEcritures.addObject(monEcritureSacd2);
		}
		//		if (agence != null) {
		//			this.trace("-----------------------------");
		//			this.trace("1 SACD ET AGENCE");
		//			// origine ecriture AGENCE
		//			
		//			// AGENCE inverser le sens et faire la somme des montants de l
		//			// agence X compte 185sacd
		//			sens = sens * (-1);
		//			indexEcd++;
		//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(sens)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(), agence, pcoSacd1);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 11 les  details de monEcriture " + monEcriture.detailEcriture());
		//			
		//			// SACD inverser de nouveau de sens montant X compte 185
		//			sens = sens * (-1);
		//			indexEcd++;
		//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(sens)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(), sacd1, pco185);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 12 les  details de monEcriture " + monEcriture.detailEcriture());
		//			
		//			// ecriture SACD 1
		//			monEcritureSacd1 = monEcriture;
		//			// creer nouvelle ECRITURE AGENCE
		//			monEcritureAgence = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique Agence", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(),
		//					monEcriture.typeOperation(), monEcriture.utilisateur());// FIXED
		//			
		//			lesNouvellesEcritures.addObject(monEcritureAgence);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" les  details de monEcritureAGENCE " + monEcritureAgence.detailEcriture());
		//			this.trace(" les  details de monEcritureSACD1 " + monEcritureSacd1.detailEcriture());
		//			
		//		}
		//		else {
		//			// 2 SACDs
		//			agence = sacd1.comptabilite().gestion();
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 2SACD agence  GESTION" + agence);
		//			
		//			// origine SACD 1
		//			
		//			// SACD1 inversion du sens montant somme X des montants SACD1 185
		//			sens = sens * (-1);
		//			indexEcd++;
		//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(sens)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(), sacd1, pco185);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 21 les  details de monEcriture " + monEcriture.detailEcriture());
		//			
		//			// AGENCE inversion sens montant X 185sacd1
		//			sens = sens * (-1);
		//			indexEcd++;
		//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(sens)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(), agence, pcoSacd1);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 22 les  details de monEcriture " + monEcriture.detailEcriture());
		//			
		//			// AGENCE inversion sens montant X 185 sacd2
		//			sens = sens * (-1);
		//			indexEcd++;
		//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant.multiply(new BigDecimal(sens)), "sacd automatique", montant, "", seulSens, monEcriture, monEcriture.exercice(), agence, pcoSacd2);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 23 les  details de monEcriture " + monEcriture.detailEcriture());
		//			
		//			// SACD2 inversion sens montant X 185
		//			sens = sens * (-1);
		//			indexEcd++;
		//			maFactoryEcritureDetail.creerEcritureDetail(ed, "", new Integer(indexEcd), "SACD automatique : " + monEcriture.ecrLibelle(), montant, "sacd automatique", montant.multiply(new BigDecimal(sens)), "", seulSens, monEcriture, monEcriture.exercice(), sacd2, pco185);
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" 24 les  details de monEcriture " + monEcriture.detailEcriture());
		//			
		//			// ecriture SACD 1
		//			monEcritureSacd1 = monEcriture;
		//			// creer nouvelle ECRITURE AGENCE
		//			monEcritureAgence = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique agence", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(),
		//					monEcriture.typeOperation(), monEcriture.utilisateur());
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" CREATION ECRITURE AGENCE " + monEcritureAgence);
		//			this.trace("  ECRITURE SACD1" + monEcritureSacd1);
		//			
		//			lesNouvellesEcritures.addObject(monEcritureAgence);
		//			
		//			// creer nouvelle ECRITURE SACD2
		//			monEcritureSacd2 = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), monEcriture.ecrLibelle(), new Integer(0), "automatique sacd2", monEcriture.comptabilite(), monEcriture.exercice(), monEcriture.origine(), monEcriture.typeJournal(), monEcriture.typeOperation(),
		//					monEcriture.utilisateur());
		//			
		//			this.trace("-----------------------------");
		//			this.trace(" NOUVELLE ECRITURE SACD2" + monEcritureSacd2);
		//			
		//			lesNouvellesEcritures.addObject(monEcritureSacd2);
		//		}

		this.trace("-----------------------------");
		this.trace(" LES NOUVELLES  ECRITURES" + lesNouvellesEcritures);

		this.trace("***********REAFFECTATION DES LIGNES D ECRITURE***********");

		// mise ajour des details et des ecritures!
		tmpDetails = new NSArray(monEcriture.detailEcriture());
		i = 0;
		while (i < tmpDetails.count()) {
			if (sacd1 != null) {
				if (((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).gestion().equals(sacd1)) {
					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).setEcritureRelationship(monEcritureSacd1);
					//					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(monEcritureSacd1, "ecriture");
				}
			}

			if (sacd2 != null) {
				if (((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).gestion().equals(sacd2)) {
					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).setEcritureRelationship(monEcritureSacd2);
					//					((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(monEcritureSacd2, "ecriture");
				}
			}

			if (((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).gestion().equals(agence)) {
				((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).setEcritureRelationship(monEcritureAgence);
				//				((EOEcritureDetail) (tmpDetails.objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(monEcritureAgence, "ecriture");
			}

			i++;
		}

		this.trace("-----------------------------");

		if (monEcritureAgence != null) {
			this.trace(" DETAIL ECRITURE AGENCE" + monEcritureAgence.detailEcriture());
		}
		if (monEcritureSacd1 != null) {
			this.trace(" DETAIL ECRITURE SACD 1" + monEcritureSacd1.detailEcriture());
		}
		if (monEcritureSacd2 != null) {
			this.trace(" DETAIL ECRITURE SACD 2" + monEcritureSacd2.detailEcriture());
		}

		this.trace("----VERIFICATION AGENCE---------");
		// verifier les ecritures !
		monEcritureAgence.validateObjectMetier();

		this.trace("----VERIFICATION SACD1 ---------");
		if (monEcritureSacd1 != null) {
			monEcritureSacd1.validateObjectMetier();
		}

		this.trace("----VERIFICATION SACD2---------");
		if (monEcritureSacd2 != null) {
			monEcritureSacd2.validateObjectMetier();
		}

		return lesNouvellesEcritures;
	}

	/**
	 * Permet de numeroter une ecriture comptable <br>
	 * APRES UN SAVECHANGES <br>
	 * faire un fault ou un invalidate sur l objet
	 * 
	 * @param ed editing context de travail
	 * @param ecriture ecriture a numeroter
	 * @param leFactoryNumerotation objet qui gere la numerotation
	 */
	public void numeroterEcriture(EOEditingContext ed, EOEcriture ecriture, FactoryNumerotation leFactoryNumerotation) {
		leFactoryNumerotation.getNumeroEOEcriture(ed, ecriture);
	}

	/**
	 * Permet de numeroter une ecriture de brouillard <br>
	 * APRES UN SAVECHANGES <br>
	 * faire un fault ou un invalidate sur l objet
	 * 
	 * @param ed editing context de travail
	 * @param brouillard brouillard (ecriture) a numeroter
	 * @param leFactoryNumerotation objetqui gere la numerotation
	 */
	public void numeroterEcritureBrouillard(EOEditingContext ed, EOEcriture brouillard, FactoryNumerotation leFactoryNumerotation) {
		leFactoryNumerotation.getNumeroEOEcritureBrouillard(ed, brouillard);
	}

	private String inverserSens(String sens) {
		if (sens.equals(Factory.sensCredit())) {
			return Factory.sensDebit();
		}

		if (sens.equals(Factory.sensDebit())) {
			return Factory.sensCredit();
		}

		return null;
	}

	private boolean memeCodeGestion(NSArray lesDetails) {
		boolean reponse = true;
		EOGestion monEOGestion = ((EOEcritureDetail) lesDetails.objectAtIndex(0)).gestion();
		//		trace("*****************memeCodeGestion******************");
		//		trace(" lesDetails" + lesDetails);
		//		trace(" monEOGestion" + monEOGestion);
		int i = 1;
		while (i < lesDetails.count() && reponse) {
			if (!(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion().equals(monEOGestion))) {
				reponse = false;
			}
			i++;
		}
		//		trace("*****************memeCodeGestion******************");
		//		trace(" reponse" + reponse);
		return reponse;
	}

	public static NSArray lesSacds(final EOEditingContext ed, NSArray lesDetails) throws Exception {
		NSMutableArray listeSACD = new NSMutableArray();
		EOGestion tmpGestion = null;
		int i = 0;
		while (i < lesDetails.count()) {
			tmpGestion = ((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion();
			//final EOPlanComptable planco185 = FinderGestion.getPlanco185ForGestionAndExercice(tmpGestion, ((EOEcritureDetail) lesDetails.objectAtIndex(i)).exercice());
			final EOPlanComptable planco185 = tmpGestion.getPlanco185(((EOEcritureDetail) lesDetails.objectAtIndex(i)).exercice());
			;
			if (planco185 != null && listeSACD.indexOfObject(tmpGestion) == NSArray.NotFound) {
				listeSACD.addObject(tmpGestion);
			}
			i++;
		}
		return listeSACD.immutableClone();
	}

	private BigDecimal sommeDesDetailsPourLeCodeGestion(NSArray lesDetails, EOGestion leGestion) {
		//	BigDecimal somme = new BigDecimal(0);
		NSMutableArray tmpDetails = new NSMutableArray(0);
		int i = 0;

		trace("*****************sommeDesDetailsPourLeCodeGestion******************");
		trace(" lesDetails" + lesDetails);
		trace(" leGestion" + leGestion);

		while (i < lesDetails.count()) {
			if (leGestion.equals(((EOEcritureDetail) lesDetails.objectAtIndex(i)).gestion())) {
				tmpDetails.addObject(lesDetails.objectAtIndex(i));
			}
			i++;
		}

		trace("*****************sommeDesDetailsPourLeCodeGestion******************");
		trace(" tmpDetails" + tmpDetails);

		return this.computeSumForKeyBigDecimal(tmpDetails, EOEcritureDetail.ECD_MONTANT_KEY);

	}

}
