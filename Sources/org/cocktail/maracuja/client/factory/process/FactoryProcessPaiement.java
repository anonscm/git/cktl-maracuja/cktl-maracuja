/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;
import java.util.Map;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryPaiement;
import org.cocktail.maracuja.client.finder.FinderTypeVirement;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOTypeVirement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.metier.EOVirementParamBdf;
import org.cocktail.maracuja.client.metier.EOVirementParamEtebac;
import org.cocktail.maracuja.client.metier.EOVirementParamVint;
import org.cocktail.maracuja.client.remotecall.ServerCallPaiement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author frivalla
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class FactoryProcessPaiement extends FactoryProcess {
	private final String virement = EOModePaiement.MODDOM_VIREMENT;
	private final String caisse = EOModePaiement.MODDOM_CAISSE;
	private final String cheque = EOModePaiement.MODDOM_CHEQUE;

	/**
	 * @param withLog
	 */
	public FactoryProcessPaiement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public void genererVirements(EOEditingContext ed, NSArray lesEOMandats, NSArray lesEOOrdreDePaiement, NSTimestamp dateDeValeur,
			EOUtilisateur utilisateur, NSMutableArray lesEcrituresQuiSerontGenerees, NSMutableArray lesVirementsQuiSerontGenerees, EOEmargement emargementQuiSeraGenere, NSMutableArray lesOPTresorerieQuiSerontGenerees, EOComptabilite comptabilite, EOTypeVirement typePaiement, EOExercice exercice,
			final Map compteBe) throws Exception {
		trace("lesEcrituresQuiSerontGenerees AVT GENERERPAIEMENT " + lesEcrituresQuiSerontGenerees);
		this.genererPaiement(ed, lesEOMandats, lesEOOrdreDePaiement, dateDeValeur, utilisateur, this.virement, lesEcrituresQuiSerontGenerees, lesVirementsQuiSerontGenerees, comptabilite, typePaiement, exercice,
				compteBe);
		return;
	}

	public void genererCaisses(EOEditingContext ed, NSArray lesEOMandats, NSArray lesEOOrdreDePaiement, NSArray lesEOTitres, EOVirementParamBdf leVirementParamBdf, EOVirementParamEtebac leVirementParamEtebac, EOVirementParamVint leVirementParamVint, NSTimestamp dateDeValeur,
			EOUtilisateur utilisateur, NSMutableArray lesEcrituresQuiSerontGenerees, NSMutableArray lesVirementsQuiSerontGenerees, EOEmargement emargementQuiSeraGenere, NSMutableArray lesOPTresorerieQuiSerontGenerees, EOComptabilite comptabilite, EOExercice exercice, Map compteBe) throws Exception {

		this.genererPaiement(ed, lesEOMandats, lesEOOrdreDePaiement, dateDeValeur, utilisateur, this.caisse, lesEcrituresQuiSerontGenerees, lesVirementsQuiSerontGenerees, comptabilite, FinderTypeVirement
				.leTypeVirementCaisse(ed), exercice, compteBe);

		return;
	}

	public void genererCheques(EOEditingContext ed, NSArray lesEOMandats, NSArray lesEOOrdreDePaiement, NSArray lesEOTitres, EOVirementParamBdf leVirementParamBdf, EOVirementParamEtebac leVirementParamEtebac, EOVirementParamVint leVirementParamVint, NSTimestamp dateDeValeur,
			EOUtilisateur utilisateur, NSMutableArray lesEcrituresQuiSerontGenerees, NSMutableArray lesVirementsQuiSerontGenerees, EOEmargement emargementQuiSeraGenere, NSMutableArray lesOPTresorerieQuiSerontGenerees, EOComptabilite comptabilite, EOExercice exercice, Map compteBe) throws Exception {

		// NSMutableArray lesOPTresorerie = new NSMutableArray(0);

		this.genererPaiement(ed, lesEOMandats, lesEOOrdreDePaiement, dateDeValeur, utilisateur, this.cheque, lesEcrituresQuiSerontGenerees, lesVirementsQuiSerontGenerees, comptabilite, FinderTypeVirement
				.leTypeVirementCheque(ed), exercice, compteBe);

		// creation des OPT.

		return;
	}

	private void genererPaiement(EOEditingContext ed,
			NSArray lesEOMandats,
			NSArray lesEOOrdreDePaiement,
			NSTimestamp dateDeValeur,
			EOUtilisateur utilisateur,
			String traitement,
			NSMutableArray lesEcrituresGenerees,
			NSMutableArray lesPaiementsGenerees,
			EOComptabilite comptabilite,
			EOTypeVirement typePaiement,
			EOExercice exercice,
			Map compteBe) throws Exception {

		final NSMutableArray depenses = new NSMutableArray();

		BigDecimal montantPaiement = new BigDecimal(0).setScale(2);
		BigDecimal montantRetenues = new BigDecimal(0).setScale(2);
		BigDecimal montantPaiementEcritures = new BigDecimal(0).setScale(2);

		int i = 0;

		if (lesPaiementsGenerees == null) {
			throw new FactoryException("lesVirementsGenerees doit etre initialise");
		}
		if (lesEcrituresGenerees == null) {
			throw new FactoryException("lesEcrituresGenerees doit etre initialise");
		}

		// Remplir les tableaux
		for (i = 0; i < lesEOMandats.count(); i++) {
			EOMandat element = (EOMandat) lesEOMandats.objectAtIndex(i);
			depenses.addObjectsFromArray(element.depenses());
		}

		// CREATION DU VIREMENT
		// calcul du montant
		montantPaiement = montantPaiement.add(this.computeSumForKeyBigDecimal(this.lesDepensesDesMandats(lesEOMandats), EODepense.DEP_MONTANT_DISQUETTE_KEY));
		montantPaiement = montantPaiement.add(this.computeSumForKeyBigDecimal(lesEOOrdreDePaiement, EOOrdreDePaiement.ODP_MONTANT_PAIEMENT_KEY));
		montantRetenues = this.computeSumForKeyBigDecimal(this.lesDepensesDesMandats(lesEOMandats), EODepense.MONTANT_RETENUES_KEY);

		// calcul du nombre de virements dans le fichier
		int nb = 0;
		nb = nb + this.lesDepensesDesMandats(lesEOMandats).count();
		nb = nb + lesEOOrdreDePaiement.count();

		final FactoryPaiement maFactoryPaiement = new FactoryPaiement(withLogs());
		//Le paiement est généré en memoire, on réserve son numero
		Integer numeroPaiement = ServerCallPaiement.clientSideRequestNextNumeroPaiement(ed, comptabilite, exercice);

		final EOPaiement monPaiement = maFactoryPaiement.creerPaiement(ed, comptabilite, exercice, utilisateur, typePaiement, getNow(), montantPaiement, new Integer(nb), new Integer(0));
		monPaiement.setPaiNumero(numeroPaiement);

		final FactoryProcessPaiementMandat myFactoryVirementDepense = new FactoryProcessPaiementMandat(withLogs(), getDateJourComptable());
		final FactoryProcessPaiementOrdreDePaiement myFactoryVirementOrdreDePaiement = new FactoryProcessPaiementOrdreDePaiement(withLogs(), getDateJourComptable());

		if (lesEOMandats.count() != 0) {
			lesEcrituresGenerees.addObjectsFromArray(myFactoryVirementDepense.viserVirementMandats(ed, lesEOMandats, utilisateur, traitement, exercice, compteBe, monPaiement.paiNumero().toString()));
		}
		if (lesEOOrdreDePaiement.count() != 0) {
			lesEcrituresGenerees.addObjectsFromArray(myFactoryVirementOrdreDePaiement.viserVirementOrdreDePaiement(ed, lesEOOrdreDePaiement, utilisateur, traitement, exercice, monPaiement.paiNumero().toString()));
		}

		// creation des relations avec le paiement
		i = 0;
		if (lesEOMandats.count() > 0) {
			EOMandat man = null;
			while (i < lesEOMandats.count()) {
				man = ((EOMandat) lesEOMandats.objectAtIndex(i));
				man.setPaiementRelationship(monPaiement);
				i++;
			}
		}

		i = 0;
		while (i < lesEOOrdreDePaiement.count()) {
			((EOOrdreDePaiement) lesEOOrdreDePaiement.objectAtIndex(i)).setPaiementRelationship(monPaiement);
			i++;
		}

		lesPaiementsGenerees.addObject(monPaiement);

		// verifier montant du paiement et montant des ecritures de Credit classe 5
		// Modif rod : on ajoute le montant des retenues
		montantPaiementEcritures = montantPaiementEcritures.add(EOMandat.montantEcrituresCreditsPaiement(lesEOMandats));
		montantPaiementEcritures = montantPaiementEcritures.add(EOMandat.montantEcrituresCreditsRetenues(lesEOMandats));
		montantPaiementEcritures = montantPaiementEcritures.add(EOOrdreDePaiement.montantEcrituresCreditsPaiement(lesEOOrdreDePaiement));

		if (Factory.different(montantPaiement.add(montantRetenues), montantPaiementEcritures)) {
			throw new FactoryException("PROBLEMES montant des virements (" + montantPaiement + ") + montant des retenues (" + montantRetenues + ") different du montant des credits générés : " + montantPaiementEcritures);
		}
		return;
	}

	public void numeroterVirement(EOEditingContext ed, EOPaiement virement, FactoryNumerotation leFactoryNumerotation) {
		leFactoryNumerotation.getNumeroEOPaiement(ed, virement);
	}

	private NSArray lesDepensesDesMandats(NSArray lesMandats) {
		NSMutableArray depenses = new NSMutableArray();
		// les depenses du paiement
		int i = 0;
		while (i < lesMandats.count()) {
			depenses.addObjectsFromArray(((EOMandat) (lesMandats.objectAtIndex(i))).depenses());
			i++;
		}
		return depenses;
	}

}
