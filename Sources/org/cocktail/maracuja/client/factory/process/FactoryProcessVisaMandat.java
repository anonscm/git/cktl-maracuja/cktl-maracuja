/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.exception.VisaException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryBordereauRejet;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryMandatDetailEcriture;
import org.cocktail.maracuja.client.factory.FactoryNumerotation;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

// import org.cocktail.maracuja.client.zutil.wo.ZEOUtilities;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */
public class FactoryProcessVisaMandat extends FactoryProcess {

	private EOTypeJournal typeJournal;
	private EOTypeOperation typeOperation;
	private final FactoryEcritureDetail maFactoryEcritureDetail;
	private final FactoryProcessJournalEcriture maFactoryEcriture;
	private final FactoryBordereauRejet maFactoryBordereauRejet;
	private final FactoryMandatDetailEcriture maFactoryMandatDetailEcriture;
	private boolean wantRecupConvention;

	/**
	 * Constructeur permettant d instancier l objet qui va gerer les objets metiers de la phase de visa :<br>
	 * Ecriture , EcritureDetails ,MandatDetailEcriture ,Mandat ,MandatBrouillon ,BordereauRejet. <br>
	 * Le contenu du MandatBrouillon doit prendre en compte tous les aspects du visa : SACD,Convention Ress. Aff.,etc ... <br>
	 * typeJounal peut etre null (valeur pas defaut journal GENERAL) <br>
	 * typeOperation peut etre null (valeur pas defaut operation VISA MANDAT)
	 * 
	 * @param ed EOEditingContext
	 * @param typeJournal type de journal pour les ecritures de visa
	 * @param typeOperation type d'operation pour les ecritures de visa
	 * @param withLog activer les trace de cette factory
	 */
	public FactoryProcessVisaMandat(EOEditingContext ed, EOTypeJournal ptypeJournal, EOTypeOperation ptypeOperation, boolean withLog, final NSTimestamp dateJourComptable, boolean wantRecupConvention) throws VisaException {
		super(withLog, dateJourComptable);
		this.wantRecupConvention = wantRecupConvention;
		if (ptypeJournal == null) {
			ptypeJournal = FinderJournalEcriture.leTypeJournalVisaMandat(ed);
		}
		if (ptypeOperation == null) {
			ptypeOperation = FinderVisa.leTypeOperationVisaMandat(ed);
		}

		if (ptypeJournal == null) {
			throw new FactoryException(FactoryException.TYPE_JOURNAL_NON_SPECIFIE);
		}
		if (ptypeOperation == null) {
			throw new FactoryException(FactoryException.TYPE_OPERATION_NON_SPECIFIE);
		}

		setTypeJournal(ptypeJournal);
		setTypeOperation(ptypeOperation);

		maFactoryEcritureDetail = new FactoryEcritureDetail(withLog);
		maFactoryEcriture = new FactoryProcessJournalEcriture(withLog, getDateJourComptable());
		maFactoryBordereauRejet = new FactoryBordereauRejet(withLog);
		maFactoryMandatDetailEcriture = new FactoryMandatDetailEcriture(withLog);
	}

	/**
	 * Methode permettant de viser un bordereau de Mandat. <br>
	 * Pour la Numerotation instancier un objet d'une sous classe de <br>
	 * FactoryNumerotation qui implemente l'interface InterfaceFactoryNumerotation. <br>
	 * Ne pas oublier de numeroter les ecritures du bordereau (voir FinderVisa + FactoryProcessEcriture) <br>
	 * Ne pas oublier de numeroter le bordereau de rejet (voir FinderVisa +FactoryProcessBordereauRejet)
	 * 
	 * @param ed EditingContext de travail (Nested ??)
	 * @param utilisateur utilisateur qui fait le visa
	 * @param leBordereau Bordereau a Viser
	 * @param lesMandatsRejetes Tableaux des Mandats a rejeter
	 * @param lesMandatsAcceptes Tableaux des Mandats a valider
	 * @param dateRemiseVisa Date de remise des mandat a la comptabilitï¿½
	 * @return Le Bordereau de rejet s il existe
	 * @throws VisaException probleme lors de la phase de visa voir message.
	 */
	public EOBordereauRejet viserUnBordereauDeMandat(EOEditingContext ed, EOUtilisateur utilisateur, EOBordereau leBordereau, NSArray lesMandatsRejetes, NSArray lesMandatsAcceptes, NSTimestamp dateRemiseVisa) throws VisaException {

		try {
			this.verifierFormatDuBordereau(ed, utilisateur, leBordereau);
		} catch (VisaException e) {
			throw new VisaException(VisaException.problemeFormatBordereau + e);
		}

		// on gere l etat du bordereau : VISER
		leBordereau.setBorEtat(EOBordereau.BordereauVise);
		leBordereau.setBorDateVisa(Factory.getNow());
		leBordereau.setUtilisateurVisaRelationship(utilisateur);

		this.trace("");
		this.trace("leBordereau " + leBordereau);
		this.trace("-------------------------------");

		// on met a jour les mandats acceptes
		int i = 0;
		while (i < lesMandatsAcceptes.count()) {
			this.trace("");
			this.trace("lesMandatsAcceptes " + (lesMandatsAcceptes.objectAtIndex(i)));
			this.trace("-------------------------------");

			// creation des ecitures.
			this.creerLesEcrituresDuMandat(ed, ((EOMandat) lesMandatsAcceptes.objectAtIndex(i)), utilisateur);
			// passage a l etat VISE
			((EOMandat) lesMandatsAcceptes.objectAtIndex(i)).setManEtat(EOMandat.mandatVise);
			((EOMandat) lesMandatsAcceptes.objectAtIndex(i)).setManDateRemise(dateRemiseVisa);

			i++;

		}
		this.trace("");
		this.trace("lesMandatsRejetes " + lesMandatsRejetes);
		this.trace("-------------------------------");
		// on met a jour les mandats rejetï¿½s
		if (lesMandatsRejetes.count() == 0) {
			return null;
		}
		return this.creerLeBordereauDeRejet(ed, lesMandatsRejetes, leBordereau.exercice(), leBordereau.gestion(), utilisateur, FinderVisa.leTypeBordereauBTMNA(ed));
	}

	/**
	 * Methode permettant de viser un bordereau de Mandat. <br>
	 * Pour la Numerotation instancier un objet d'une sous classe de <br>
	 * FactoryNumerotation qui implemente l'interface InterfaceFactoryNumerotation. <br>
	 * Ne pas oublier de numeroter les ecritures du bordereau (voir FinderVisa + FactoryProcessEcriture) <br>
	 * Ne pas oublier de numeroter le bordereau de rejet (voir FinderVisa +FactoryProcessBordereauRejet)
	 * 
	 * @param ed EditingContext de travail (Nested ??)
	 * @param utilisateur utilisateur qui fait le visa
	 * @param leBordereau Bordereau a Viser
	 * @param lesMandatsRejetes Tableaux des Mandats a rejeter
	 * @param lesMandatsAcceptes Tableaux des Mandats a valider
	 * @param dateRemiseVisa Date de remise des mandat a la comptabilitï¿½
	 * @param delegateNumerotation Objet qui gere l attribution des numeros (ecritures,bordereauRejet)
	 * @return Le Bordereau de rejet s il existe
	 * @throws VisaException probleme lors de la phase de visa voir message.
	 */
	public EOBordereauRejet viserUnBordereauDeMandatEtNumeroter(EOEditingContext ed, EOUtilisateur utilisateur, EOBordereau leBordereau, NSArray lesMandatsRejetes, NSArray lesMandatsAcceptes, NSTimestamp dateRemiseVisa, FactoryNumerotation delegateNumerotation) throws VisaException {
		EOBordereauRejet nouveauEOBordereauRejet = this.viserUnBordereauDeMandat(ed, utilisateur, leBordereau, lesMandatsRejetes, lesMandatsAcceptes, dateRemiseVisa);
		return nouveauEOBordereauRejet;
	}

	/**
	 * Permet de numeroter un bordereaude mandat non admis <br>
	 * attribution du numero de bordereau rejet <br>
	 * attribution du numero de mandat rejet
	 * 
	 * @param ed editingContext de travail
	 * @param leBordereauRejet bordereau rejet a numeroter
	 * @param leFactoryNumerotation object qui gere la numerotation
	 * @param withLogs avec trace
	 */
	public void numeroterUnBordereauDeMandatRejet(EOEditingContext ed, EOBordereauRejet leBordereauRejet, FactoryNumerotation leFactoryNumerotation, boolean withLogs) {
		// numero le bordereau de rejet
		leFactoryNumerotation.getNumeroEOBordereauRejet(ed, leBordereauRejet);
		// numeroter les mandats du bordereau rejet fait par lka procedure
		// stockÃ©e
	}

	/**
	 * permet de numeroter les ecritures du visa de bordereau <br>
	 * numerote les ecrituresdu bordereau
	 * 
	 * @param ed editing context de travail
	 * @param leBordereau bordereau al origine des ecritures
	 * @param leFactoryNumerotation object qui gere la numerotation
	 * @param withLogs avec trace
	 * @return les ecritures numerotees
	 */
	public NSArray numeroterUnBordereauDeMandat(EOEditingContext ed, EOBordereau leBordereau, FactoryNumerotation leFactoryNumerotation, boolean withLogs) {
		// leFactoryNumerotation. getNumeroEOEcriture( ed, monEcriture);
		// leFactoryNumerotation.getNumeroEOEmargement(ed, monEmargement);
		// numero les ecritures du bordereau
		// recup des ecritures du bordereau
		NSArray lesEcritures = FinderVisa.lesEcrituresDuBordereauMandat(ed, leBordereau, withLogs);

		// Laisser la trace, apparemment ca permet de s'assurer que les
		// ecritures sont bien triees .. ????
		System.out.println(lesEcritures);

		int i = 0;
		while (i < lesEcritures.count()) {
			trace("Numerotation ", ((EOEcriture) lesEcritures.objectAtIndex(i)).ecrLibelle());
			leFactoryNumerotation.getNumeroEOEcriture(ed, (EOEcriture) lesEcritures.objectAtIndex(i));
			i++;
		}
		return lesEcritures;

	}

	private void creerLesEcrituresDuMandat(EOEditingContext ed, EOMandat leMandat, EOUtilisateur utilisateur) throws VisaException {
		// verifications
		if (leMandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw new VisaException(VisaException.mandatDejaAnnule);
		}
		if (leMandat.manEtat().equals(EOMandat.mandatVise)) {
			throw new VisaException(VisaException.mandatDejaVise);
		}
		this.trace("");
		this.trace("Avant newEOEcriture : ");
		this.trace("ec = " + ed);
		this.trace(" - ec mandat = " + leMandat.editingContext());
		this.trace("- compta = " + leMandat.bordereau().gestion().comptabilite());
		this.trace("- ec compta = " + leMandat.bordereau().gestion().comptabilite().editingContext());
		this.trace(" - origine = " + leMandat.origine());

		// creation de l ecriture
		final String typeMandat = (EOTypeBordereau.SOUS_TYPE_REVERSEMENTS.equals(leMandat.bordereau().typeBordereau().tboSousType()) ? "Orv" : "Md.");
		EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), typeMandat + " Num . " + leMandat.manNumero().toString() + " Bord. Num. " + leMandat.bordereau().borNum().toString(), new Integer(0), null, leMandat.bordereau().gestion().comptabilite(), leMandat
				.exercice(), leMandat.origine(), this.getTypeJournal(), this.getTypeOperation(), utilisateur);
		this.trace("- ec newEOEcriture = " + newEOEcriture.editingContext());

		// recup des MandatBrouillard du mandat
		final NSArray lesBrouillons = EOQualifier.filteredArrayWithQualifier(leMandat.mandatBrouillards(), EOMandatBrouillard.QUAL_NON_SACD);
		final NSArray lesBrouillonsSacd = EOQualifier.filteredArrayWithQualifier(leMandat.mandatBrouillards(), EOMandatBrouillard.QUAL_SACD);
		this.trace("lesBrouillons " + lesBrouillons);
		this.trace("lesBrouillonsSACD " + lesBrouillonsSacd);

		if (lesBrouillons.count() == 0) {
			throw new VisaException(VisaException.pasDeMandatBrouillard);
		}
		// pour chaque ligne du brouillon on cree un detail ecriture
		int i = 0;
		while (i < lesBrouillons.count()) {
			EOMandatBrouillard mb = (EOMandatBrouillard) lesBrouillons.objectAtIndex(i);
			final EOEcritureDetail newEOEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i + 1), leMandat.bordereau().typeBordereau().tboSousType() + " " + leMandat.fournisseur().adrNom() + "  " + typeMandat + "  " + leMandat.manNumero().toString() + " Bord.  "
					+ leMandat.bordereau().borNum().toString() + " du  " + leMandat.bordereau().gestion().gesCode() + " " + leMandat.exercice().exeExercice(), mb.mabMontant(), null, mb.mabMontant(), null, mb.mabSens(), newEOEcriture, leMandat.exercice(), mb.gestion(), mb.planComptable());

			if (newEOEcritureDetail == null) {
				throw new VisaException(VisaException.problemeCreationEcriture);
			}
			EOAccordsContrat contrat = null;
			if (wantConventionParMandat()) {
				//Recuperation du contrat eventuellement associé a la depense. Si plusieurs contrats sont affectés a la recette on n'en affecte aucun.
				contrat = leMandat.getAccordContratUniqueAvecRepartitionTotale();
			}
			else {
				//ne fonctionne que pour les conventions RA
				contrat = EOOrigine.getAccordContratFromOrigine(ed, leMandat.origine());
			}
			if (contrat != null) {
				newEOEcritureDetail.setToAccordsContratRelationship(contrat);
			}
			// on cree le MandatDetailEcriture
			String mdeOrigine = (EOMandatBrouillard.VISA_TVA.equals(mb.mabOperation()) ? EOMandatDetailEcriture.MDE_ORIGINE_VISA_TVA : (EOMandatBrouillard.VISA_CTP.equals(mb.mabOperation()) ? EOMandatDetailEcriture.MDE_ORIGINE_VISA_CTP : EOMandatDetailEcriture.MDE_ORIGINE_VISA));
			final EOMandatDetailEcriture newEOMandatDetailEcriture = maFactoryMandatDetailEcriture.creerMandatDetailEcriture(ed, Factory.getNow(), mdeOrigine, newEOEcritureDetail, newEOEcriture.exercice(), leMandat, newEOEcriture.origine());

			if (newEOMandatDetailEcriture == null) {
				throw new VisaException(VisaException.problemeCreationDetailEcriture);
			}
			i++;
		}

		// Y ATIL DES ECRITURES SACD ?
		// obligatoirement 1 D 1C
		if (lesBrouillonsSacd.count() >= 2) {
			this.creerLesEcrituresSACDDuMandat(ed, leMandat, utilisateur);
		}

	}

	/**
	 * Genere les ecritures sacd a partir des brouillons SACD.
	 * 
	 * @param ed
	 * @param leMandat
	 * @param utilisateur
	 * @throws VisaException
	 */
	private void creerLesEcrituresSACDDuMandat(EOEditingContext ed, EOMandat leMandat, EOUtilisateur utilisateur) throws VisaException {

		// verifications
		if (leMandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw new VisaException(VisaException.mandatDejaAnnule);
		}

		if (leMandat.manEtat().equals(EOMandat.mandatVise)) {
			throw new VisaException(VisaException.mandatDejaVise);
		}
		final String typeMandat = (EOTypeBordereau.SOUS_TYPE_REVERSEMENTS.equals(leMandat.bordereau().typeBordereau().tboSousType()) ? "Orv" : "Md.");

		// creation de l ecriture
		final EOEcriture newEOEcriture = maFactoryEcriture.creerEcriture(ed, getDateJourComptable(), typeMandat + " Num . " + leMandat.manNumero().toString() + " Bord. Num. " + leMandat.bordereau().borNum().toString(), new Integer(0), null, leMandat.bordereau().gestion().comptabilite(), leMandat
				.exercice(), leMandat.origine(), this.getTypeJournal(), this.getTypeOperation(), utilisateur);// FIXED
		// chgt
		// exercice
		// date
		// journee
		// comptable
		this.trace("- ec newEOEcriture = " + newEOEcriture.editingContext());

		final NSArray lesBrouillons = EOQualifier.filteredArrayWithQualifier(leMandat.mandatBrouillards(), EOMandatBrouillard.QUAL_SACD);

		// this.trace("lesBrouillons " + lesBrouillons);

		if (lesBrouillons.count() == 0) {
			throw new VisaException(VisaException.pasDeMandatBrouillard);
		}

		// pour chaque ligne du brouillon on cree un detail ecriture
		int i = 0;
		while (i < lesBrouillons.count()) {
			//((EOMandatBrouillard) lesBrouillons.objectAtIndex(i)).mabSens();
			//((EOMandatBrouillard) lesBrouillons.objectAtIndex(i)).mabMontant();
			EOMandatBrouillard mb = (EOMandatBrouillard) lesBrouillons.objectAtIndex(i);
			EOEcritureDetail newEOEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i + 1), leMandat.bordereau().typeBordereau().tboSousType() + " " + leMandat.fournisseur().adrNom() + "  " + typeMandat + "  " + leMandat.manNumero().toString() + " Bord.  "
					+ leMandat.bordereau().borNum().toString() + " du  " + leMandat.bordereau().gestion().gesCode(), mb.mabMontant(), null, mb.mabMontant(), null,
					mb.mabSens(), newEOEcriture, leMandat.exercice(), mb.gestion(), mb.planComptable());

			this.trace("newEOEcritureDetail " + newEOEcritureDetail);

			if (newEOEcritureDetail == null) {
				throw new VisaException(VisaException.problemeCreationEcriture);
			}
			// on cree le MandatDetailEcriture
			EOMandatDetailEcriture newEOMandatDetailEcriture = maFactoryMandatDetailEcriture.creerMandatDetailEcriture(ed, Factory.getNow(), EOMandatDetailEcriture.MDE_ORIGINE_VISA, newEOEcritureDetail, newEOEcriture.exercice(), leMandat, newEOEcriture.origine());

			if (newEOMandatDetailEcriture == null) {
				throw new VisaException(VisaException.problemeCreationDetailEcriture);
			}
			i++;
		}
	}

	private EOBordereauRejet creerLeBordereauDeRejet(EOEditingContext ed, NSArray lesMandatsRejetes, EOExercice exercice, EOGestion gestion, EOUtilisateur utilisateur, EOTypeBordereau typBordereau) throws VisaException {
		// verifications
		if (lesMandatsRejetes.count() == 0) {
			throw new VisaException(VisaException.pasDeMandatsARejeter);
		}

		// creation du bordereau de rejet
		final EOBordereauRejet newEOBordereauRejet = maFactoryBordereauRejet.creerBordereauRejet(ed, EOBordereauRejet.BordereauRejetValide, new Integer(0), exercice, gestion, typBordereau, utilisateur);

		if (newEOBordereauRejet == null) {
			throw new VisaException(VisaException.problemeCreationBordereauRejet);
		}
		this.trace("newEOBordereauRejet " + newEOBordereauRejet);

		// passer les mandats a Annule
		int i = 0;
		while (i < lesMandatsRejetes.count()) {
			((EOMandat) lesMandatsRejetes.objectAtIndex(i)).setManEtat(EOMandat.mandatAnnule);
			// rattache les mandats a un boredereau de rejet
			// ((EOMandat)
			((EOMandat) lesMandatsRejetes.objectAtIndex(i)).setBordereauRejetRelationship(newEOBordereauRejet);
			i++;
		}

		return newEOBordereauRejet;
	}

	private void creerLesOperationsTresorerie() {

	}

	private void verifierFormatDuBordereau(EOEditingContext ed, EOUtilisateur utilisateur, EOBordereau leBordereau) throws VisaException {
		// verifier le format du bordereau :
		// gestion
		// pco_num mandat = pco_num depense
		// montant mandat = montant des depenses
		// HT + TVA = TTC pour les mandats et les depenses !

		// le bordereau est il de ja vise ?

	}

	/**
	 * @return
	 */
	private EOTypeJournal getTypeJournal() {
		return typeJournal;
	}

	/**
	 * @return
	 */
	private EOTypeOperation getTypeOperation() {
		return typeOperation;
	}

	/**
	 * @param journal
	 */
	private void setTypeJournal(EOTypeJournal journal) {
		typeJournal = journal;
	}

	/**
	 * @param operation
	 */
	private void setTypeOperation(EOTypeOperation operation) {
		typeOperation = operation;
	}

	public boolean wantConventionParMandat() {
		return wantRecupConvention;
	}

}
