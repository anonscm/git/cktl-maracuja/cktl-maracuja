/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.MandatExceptions;
import org.cocktail.maracuja.client.exception.TitreExceptions;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class FactoryProcessVisaModifTitre extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessVisaModifTitre(boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);

	}

	public void modifierModeDeRecouvrement(EOEditingContext ed, EOUtilisateur utilisateur, EOTitre leTitre, EOModeRecouvrement leNouveauModeRecouvrement) throws TitreExceptions {

		// y a t il modification de la classe 4 ??

		// modifier le brouillard

		// modifier le mandat
	}

	/**
	 * Permet de modifier la contre partie de classe 4 <br>
	 * du brouillard de visa du titre.
	 * 
	 * @param ed EditingContext de travail
	 * @param utilisateur utilisateur qui modifie le mandat
	 * @param leTitre Titre a modifier
	 * @param leTitreBrouillard le titre brouillard a modifier
	 * @param leNouveauPlanComptable le nouveau compte d imputation du titre brouillard
	 * @throws MandatExceptions
	 */
	public void modifierContrePartie(EOEditingContext ed, EOUtilisateur utilisateur, EOTitre leTitre, EOTitreBrouillard leTitreBrouillard, EOPlanComptable leNouveauPlanComptable) throws TitreExceptions {
	}

	/**
	 * Permet de modifier la contre partie de classe 4 <br>
	 * du brouillard de visa du titre et le nouveau mode paiement.
	 * 
	 * @param ed EditingContext de travail
	 * @param utilisateur utilisateur qui modifie le titre
	 * @param leTitre Titre a modifier
	 * @param leTitreBrouillard le titre brouillard a modifier
	 * @param leNouveauPlanComptable nouveau compte d imputation
	 * @param leNouveauModeRecouvrement nouveau mode recouvrement
	 * @throws MandatExceptions
	 */
	public void modifierContrePartieEtModeDeRecouvrement(EOEditingContext ed, EOUtilisateur utilisateur, EOTitre leTitre, EOTitreBrouillard leTitreBrouillard, EOPlanComptable leNouveauPlanComptable, EOModeRecouvrement leNouveauModeRecouvrement) throws TitreExceptions {

		// possible si le mode de paiement ne modifie pas la contrepartie !

	}

	private void verifierModification(EOEditingContext ed, EOUtilisateur utilisateur, EOTitre leTitre, EOModeRecouvrement leNouveauModeRecouvrement) throws TitreExceptions {

		// mandat non vise ?

		// leNouveauModeDePaiement existe ?

		// utilisateur autorise ?

	}

	public EOTitreBrouillard creerTitreBrouillard(final EOEditingContext ed, final EOUtilisateur utilisateur, final EOExercice execice, final EOGestion gestion, final EOPlanComptable lePlanComptable, final EOTitre leTitre, final String uneTibOperation, final String leSens,
			final BigDecimal leMontant, final EORecette recette) throws TitreExceptions {

		final EOTitreBrouillard nouveauTitreBrouillard = (EOTitreBrouillard) FactoryProcess.instanceForEntity(ed, EOTitreBrouillard.ENTITY_NAME);

		nouveauTitreBrouillard.setExerciceRelationship(execice);
		nouveauTitreBrouillard.setPlanComptableRelationship(lePlanComptable);
		nouveauTitreBrouillard.setGestionRelationship(gestion);
		nouveauTitreBrouillard.setRecetteRelationship(recette);
		nouveauTitreBrouillard.setTibMontant(leMontant);
		nouveauTitreBrouillard.setTibSens(leSens);
		nouveauTitreBrouillard.setTibOperation(uneTibOperation);

		try {
			this.verifierCreation(ed, nouveauTitreBrouillard, utilisateur, execice, gestion, lePlanComptable, leTitre);

			ed.insertObject(nouveauTitreBrouillard);
			nouveauTitreBrouillard.validateObjectMetier();
			//            leTitre. addObjectToBothSidesOfRelationshipWithKey(nouveauTitreBrouillard, "titreBrouillards");
			leTitre.addToTitreBrouillardsRelationship(nouveauTitreBrouillard);

			return nouveauTitreBrouillard;
		} catch (ValidationException e) {
			e.printStackTrace();
			System.out.println(nouveauTitreBrouillard);
			return null;
		}

	}

	public void supprimerTitreBrouillard(final EOEditingContext ed, final EOTitreBrouillard leTitreBrouillard) {

		leTitreBrouillard.setTibOperation(EOTitreBrouillard.VISA_ANNULER);
	}

	private void verifierCreation(EOEditingContext ed, EOTitreBrouillard leTitreBrouillard, EOUtilisateur utilisateur, EOExercice execice, EOGestion gestion, EOPlanComptable lePlanComptable, EOTitre leTitre) throws TitreExceptions {
		//      titre non vise ?
		if (leTitre.titEtat().equals(EOTitre.titreVise)) {
			throw TitreExceptions.impossible_modifier_titre_vise();
		}

		if (leTitre.titEtat().equals(EOTitre.titreAnnule)) {
			throw TitreExceptions.impossible_modifier_titre_annule();
		}

		//rod : on peut modifier un credit seulement si reduction de recette ou si compte TVA
		if (leTitreBrouillard.tibSens().equals(sensCredit())) {
			if (!EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(leTitre.bordereau().typeBordereau().tboSousType()) && leTitreBrouillard.planComptable().pcoNum().equals(leTitre.planComptable())) {
				throw TitreExceptions.impossible_modifier_credit();
			}
		}

		if (EOTypeBordereau.SOUS_TYPE_REDUCTION.equals(leTitre.bordereau().typeBordereau().tboSousType()) && leTitreBrouillard.tibSens().equals(sensDebit())) {
			throw TitreExceptions.impossible_modifier_debit();
		}

		// utilisateur autorise ?

	}

}
