/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.FactoryOrdrePaiementDetailEcriture;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author frivalla, rprin
 */
public class FactoryProcessPaiementOrdreDePaiement extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessPaiementOrdreDePaiement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public NSArray viserVirementOrdreDePaiement(EOEditingContext ed, NSArray lesOrdreDepaiement, EOUtilisateur utilisateur, String traitement, EOExercice exercice, String paiNumero) throws Exception {
		EOTypeJournal typeJournal = FinderJournalEcriture.leTypeJournalPaiement(ed);
		NSMutableArray lesEcritures = new NSMutableArray(0);
		int i = 0;
		// VERIFIER PAS D OP ANNULE OU VIRE!
		i = 0;
		while (i < lesOrdreDepaiement.count()) {
			if (EOOrdreDePaiement.etatAnnule.equals(((EOOrdreDePaiement) lesOrdreDepaiement.objectAtIndex(i)).odpEtat())) {
				throw new FactoryException(EOOrdreDePaiement.problemeAnnule);
			}

			if (EOOrdreDePaiement.etatVirement.equals(((EOOrdreDePaiement) lesOrdreDepaiement.objectAtIndex(i)).odpEtat())) {
				throw new FactoryException(EOOrdreDePaiement.problemeDejaPaye);
			}
			i++;
		}

		i = 0;
		while (i < lesOrdreDepaiement.count()) {
			lesEcritures.addObjectsFromArray(this.ecritureOPGestion(ed, (EOOrdreDePaiement) lesOrdreDepaiement.objectAtIndex(i), typeJournal, utilisateur, exercice, paiNumero));
			i++;
		}

		return lesEcritures;
	}

	private NSArray ecritureOPGestion(EOEditingContext ed, EOOrdreDePaiement ordreDePaiement, EOTypeJournal typeJournal, EOUtilisateur utilisateur, EOExercice exercice, String paiNumero) throws Exception {

		EOEcriture monEcritureVirement = null;
		EOEcritureDetail monEcritureDetail = null;

		int i = 0;

		if (ordreDePaiement.odpEtat().equals((EOOrdreDePaiement.etatVirement))) {
			throw new FactoryException(EOOrdreDePaiement.problemeDejaPaye);
		}

		if (ordreDePaiement.odpEtat().equals((EOOrdreDePaiement.etatAnnule))) {
			throw new FactoryException(EOOrdreDePaiement.problemeAnnule);
		}

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());
		FactoryOrdrePaiementDetailEcriture maFactoryOrdrePaiementDetailEcriture = new FactoryOrdrePaiementDetailEcriture(withLogs());

		NSMutableArray lesEcritures = new NSMutableArray();

		// CREATION DE LECRITURE :
		monEcritureVirement = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), "PAIEMENT " + paiNumero + " O.P. " + ordreDePaiement.odpNumero() + "  " + ordreDePaiement.fournisseur().adrNom() + " - " + ordreDePaiement.odpLibelle(), new Integer(0), " automatique",
				ordreDePaiement
						.comptabilite(), exercice, ordreDePaiement.origine(), typeJournal, FinderJournalEcriture.leTypeOperationPaiement(ed), utilisateur);
		i = 0;
		while (i < ordreDePaiement.ordreDePaiementBrouillards().count()) {
			// creation d un nouveau detail somme des debit Debit comptabilite

			if (!EOExercice.EXE_TYPE_TRESORERIE.equals(ordreDePaiement.exercice().exeType())) {
				throw new Exception("L'ordre de paiement " + ordreDePaiement.odpNumero() + " aurait du être payé sur son exercice de " + "création (" + ordreDePaiement.exercice().exeExercice().intValue() + "). Vous devez créer un nouvel ordre de paiement sur l'exercice en cours.");
			}

			// creation d un nouveau detail somme des debit Debit comptabilite
			String lib = ((EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards().objectAtIndex(i))).odbLibelle();
			if (lib == null) {
				lib = ordreDePaiement.odpLibelle();
			}
			lib = "PAIEMENT " + paiNumero + " O.P. " + ordreDePaiement.odpNumero() + "  " + ordreDePaiement.fournisseur().adrNom() + " - " + lib;

			EOOrdreDePaiementBrouillard odpb = (EOOrdreDePaiementBrouillard) (ordreDePaiement.ordreDePaiementBrouillards().objectAtIndex(i));

			monEcritureDetail = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(i), lib,
					odpb.odbMontant(), "", odpb.odbMontant(), null, odpb.odbSens(), monEcritureVirement, monEcritureVirement.exercice(), odpb.gestion(), odpb.planComptable());
			maFactoryOrdrePaiementDetailEcriture.creerOrdrePaiementDetailEcriture(ed, ordreDePaiement, monEcritureDetail, Factory.getNow(), EOOrdreDePaiementBrouillard.virement);
			i++;
		}

		// mise a jour de l ODP
		ordreDePaiement.setOdpEtat(EOOrdreDePaiement.etatVirement);
		lesEcritures.addObjectsFromArray(maFactoryProcessJournalEcriture.determinerLesEcrituresSACD(ed, monEcritureVirement));
		lesEcritures.addObject(monEcritureVirement);
		return lesEcritures;
	}

}
