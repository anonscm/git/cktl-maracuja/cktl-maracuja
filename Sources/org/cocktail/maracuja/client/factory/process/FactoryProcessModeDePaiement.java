/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryProcessModeDePaiement extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessModeDePaiement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public EOModePaiement creerModePaiementVide(EOEditingContext ed, String modCode, String modLibelle, EOExercice exercice) {

		EOModePaiement newModePaiement = (EOModePaiement) Factory.instanceForEntity(ed, EOModePaiement.ENTITY_NAME);
		ed.insertObject(newModePaiement);
		newModePaiement.setExerciceRelationship(exercice);
		//		newModePaiement.addObjectToBothSidesOfRelationshipWithKey(exercice, "exercice");
		newModePaiement.setModCode(modCode);
		newModePaiement.setModLibelle(modLibelle);

		return newModePaiement;
	}

	public void modifierComptePaiement(EOEditingContext ed, EOModePaiement modeDePaiement, EOPlanComptable planComptable) {
		//        modeDePaiement.addObjectToBothSidesOfRelationshipWithKey(
		//                planComptable,
		//                "planComptablePaiement");

		//Modif rod 27/11/2004
		if (planComptable != null) {
			modeDePaiement.setPlanComptablePaiementRelationship(planComptable);
			//			modeDePaiement.addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptablePaiement");
		}
		else {
			modeDePaiement.setPlanComptablePaiementRelationship(null);
			//			if (modeDePaiement.planComptablePaiement() != null) {
			//				modeDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(modeDePaiement.planComptablePaiement(), "planComptablePaiement");
			//			}
		}

	}

	public void modifierCompteVisa(EOEditingContext ed, EOModePaiement modeDePaiement, EOPlanComptable planComptable) {
		//        modeDePaiement.addObjectToBothSidesOfRelationshipWithKey(
		//                planComptable,
		//                "planComptableVisa");

		//Modif rod 27/11/2004
		if (planComptable != null) {

			modeDePaiement.setPlanComptableVisaRelationship(planComptable);
			//			modeDePaiement.addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptableVisa");
		}
		else {
			modeDePaiement.setPlanComptableVisaRelationship(null);
			//			if (modeDePaiement.planComptableVisa() != null) {
			//				modeDePaiement.removeObjectFromBothSidesOfRelationshipWithKey(modeDePaiement.planComptableVisa(), "planComptableVisa");
			//			}
		}
	}

	public void modifierDomaine(EOEditingContext ed, EOModePaiement modeDePaiement, String domaine) {
		modeDePaiement.setModDom(domaine);
	}

	public void modifierEmaAuto(final EOEditingContext ed, final EOModePaiement modeDePaiement, final String modEmaAuto) {
		modeDePaiement.setModEmaAuto(modEmaAuto);
	}

	public void validerModePaiement(EOEditingContext ed, EOModePaiement modeDePaiement) {
		modeDePaiement.setModValidite(EOModePaiement.etatValide);
	}

	public void invaliderModePaiement(EOEditingContext ed, EOModePaiement modeDePaiement) {
		modeDePaiement.setModValidite(EOModePaiement.etatInvalide);
	}

	public void verificationAvantSaveChanges(EOEditingContext ed, EOModePaiement modeDePaiement) throws FactoryException {

	}

}
