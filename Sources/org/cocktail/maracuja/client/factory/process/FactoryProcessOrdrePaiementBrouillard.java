/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiementBrouillard;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


public class FactoryProcessOrdrePaiementBrouillard extends FactoryProcess {

	public FactoryProcessOrdrePaiementBrouillard(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public EOOrdreDePaiementBrouillard creerOrdreDePaiementBrouillardDebit(EOEditingContext ed, BigDecimal odbMontant, EOOrdreDePaiement ordreDePaiement, EOGestion gestion, EOPlanComptable planComptable, EOUtilisateur utilisateur) {

		this.trace("");
		this.trace("creerOrdreDePaiementBrouillard");
		this.trace("odbMontant = " + odbMontant);
		this.trace("ordreDePaiement = " + ordreDePaiement);
		this.trace("gestion = " + gestion);
		this.trace("planComptable = " + planComptable);
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");

		// creation de la nouvelle Ecriture
		EOOrdreDePaiementBrouillard nouvelleOpdBrouillard = (EOOrdreDePaiementBrouillard) Factory.instanceForEntity(ed, EOOrdreDePaiementBrouillard.ENTITY_NAME);

		ed.insertObject(nouvelleOpdBrouillard);

		nouvelleOpdBrouillard.setOdbMontant(odbMontant);
		nouvelleOpdBrouillard.setOdbSens(EOOrdreDePaiementBrouillard.sensDebit);

		nouvelleOpdBrouillard.setPlanComptableRelationship(planComptable);
		nouvelleOpdBrouillard.setGestionRelationship(gestion);
		nouvelleOpdBrouillard.setOrdreDePaiementRelationship(ordreDePaiement);

		return nouvelleOpdBrouillard;
	}

	public EOOrdreDePaiementBrouillard creerOrdreDePaiementBrouillardVide(EOEditingContext ed,
	// BigDecimal odbMontant,
			EOOrdreDePaiement ordreDePaiement,
			// EOGestion gestion,
			// EOPlanComptable planComptable,
			EOUtilisateur utilisateur) {

		this.trace("");
		this.trace("creerOrdreDePaiementBrouillard");
		this.trace("ordreDePaiement = " + ordreDePaiement);
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");

		// creation de la nouvelle Ecriture
		EOOrdreDePaiementBrouillard nouvelleOpdBrouillard = (EOOrdreDePaiementBrouillard) Factory.instanceForEntity(ed, EOOrdreDePaiementBrouillard.ENTITY_NAME);

		ed.insertObject(nouvelleOpdBrouillard);

		// nouvelleOpdBrouillard.addObjectToBothSidesOfRelationshipWithKey(
		// utilisateur,
		// "utilisateur");
		nouvelleOpdBrouillard.setOrdreDePaiementRelationship(ordreDePaiement);
		return nouvelleOpdBrouillard;
	}

	public void affecterGestion(EOEditingContext ed, EOGestion gestion, EOOrdreDePaiementBrouillard nouvelleOpdBrouillard) {
		nouvelleOpdBrouillard.setGestionRelationship(gestion);
	}

	public void affecterPlanComptable(EOEditingContext ed, EOPlanComptable planComptable, EOOrdreDePaiementBrouillard nouvelleOpdBrouillard) {
		nouvelleOpdBrouillard.setPlanComptableRelationship(planComptable);
	}

	public void affecterMontantCredit(EOEditingContext ed, BigDecimal odbMontant, EOOrdreDePaiementBrouillard nouvelleOpdBrouillard) {
		nouvelleOpdBrouillard.setOdbMontant(odbMontant);
		nouvelleOpdBrouillard.setOdbSens(EOOrdreDePaiementBrouillard.sensCredit);
	}

	public void affecterMontantDebit(EOEditingContext ed, BigDecimal odbMontant, EOOrdreDePaiementBrouillard nouvelleOpdBrouillard) {
		nouvelleOpdBrouillard.setOdbMontant(odbMontant);
		nouvelleOpdBrouillard.setOdbSens(EOOrdreDePaiementBrouillard.sensDebit);
	}

	public EOOrdreDePaiementBrouillard creerOrdreDePaiementBrouillardCredit(EOEditingContext ed, BigDecimal odbMontant, EOOrdreDePaiement ordreDePaiement, EOGestion gestion, EOPlanComptable planComptable, EOUtilisateur utilisateur) {

		this.trace("");
		this.trace("creerOrdreDePaiementBrouillard");
		this.trace("odbMontant = " + odbMontant);
		this.trace("ordreDePaiement = " + ordreDePaiement);
		this.trace("gestion = " + gestion);
		this.trace("planComptable = " + planComptable);
		this.trace("utilisateur = " + utilisateur);
		this.trace("-----------------------------");
		this.trace("");

		// creation de la nouvelle Ecriture
		EOOrdreDePaiementBrouillard nouvelleOpdBrouillard = (EOOrdreDePaiementBrouillard) Factory.instanceForEntity(ed, EOOrdreDePaiementBrouillard.ENTITY_NAME);

		ed.insertObject(nouvelleOpdBrouillard);

		nouvelleOpdBrouillard.setOdbMontant(odbMontant);
		nouvelleOpdBrouillard.setOdbSens(EOOrdreDePaiementBrouillard.sensCredit);

		nouvelleOpdBrouillard.setPlanComptableRelationship(planComptable);
		nouvelleOpdBrouillard.setGestionRelationship(gestion);
		nouvelleOpdBrouillard.setOrdreDePaiementRelationship(ordreDePaiement);

		return nouvelleOpdBrouillard;
	}

	public void annulerOrdreDePaiementBrouillard(EOEditingContext ed, EOOrdreDePaiementBrouillard ordreDePaiementBrouillard, EOUtilisateur utilisateur) throws FactoryException {

		this.trace("");
		this.trace("annulerOrdreDePaiementBrouillard");
		this.trace("comptabilite = " + ordreDePaiementBrouillard);
		this.trace("utilisateur = " + utilisateur);

		this.trace("-----------------------------");
		this.trace("");

		if (ordreDePaiementBrouillard.ordreDePaiement().ecriture() == null) {

			// Rod : le deleteObject ne s'applique qu'au moment du saveChanges,
			// il
			// faut donc faire des removeObject
			//			ordreDePaiementBrouillard.removeObjectFromBothSidesOfRelationshipWithKey(ordreDePaiementBrouillard.ordreDePaiement(), "ordreDePaiement");

			ordreDePaiementBrouillard.setOrdreDePaiementRelationship(null);
			ed.deleteObject(ordreDePaiementBrouillard);
		}
		else {
			throw new FactoryException(EOOrdreDePaiementBrouillard.problemeAnnulationEcritureBrouillard);
		}
	}

}
