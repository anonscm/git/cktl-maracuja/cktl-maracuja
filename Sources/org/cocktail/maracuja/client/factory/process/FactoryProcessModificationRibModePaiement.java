/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.maracuja.client.finder.FinderEmargement;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlancoVisa;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FactoryProcessModificationRibModePaiement extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessModificationRibModePaiement(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public EOEcriture modifierMandatModePaiement(EOEditingContext ed, EOMandat mandat, EOModePaiement nouveauMode, EOUtilisateur utilisateur, EOExercice exercice, boolean forceEcritureIfPcoNumNewModePaiementNull, boolean generateEcriture) throws Exception {
		//EOPlanComptable ancienPco = mandat.modePaiement().planComptableVisa();
		if (mandat.manEtat().equals(EOMandat.mandatVirement)) {
			throw new Exception("IMPOSSIBLE DE MODIFIER UN MANDAT PAYE");
		}

		if (mandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw new Exception("IMPOSSIBLE DE MODIFIER UN MANDAT ANNULE");
		}

		if (mandat.modePaiement().isPaiementHT() != nouveauMode.isPaiementHT()) {
			throw new Exception("Il est impossible de passer d'un mode de paiement de type 'Paiement HT' vers un type 'Paiement TTC'");
		}

		EOPlanComptable nouveauPco = nouveauMode.planComptableVisa();

		//si le planco affecté au mode de paiement est nul, on récupère celui affecté par defaut compte de charge, si l'utilisateur souhaite créer l'écriture
		if (nouveauPco == null) {
			if (mandat.ecritureDetailCtpNonEmargee() != null && forceEcritureIfPcoNumNewModePaiementNull) {
				EOPlancoVisa pvi = EOPlancoVisa.getPlancoVisaForPcoOrdo(ed, mandat.planComptable().pcoNum(), mandat.exercice());
				if (pvi != null && !mandat.ecritureDetailCtpNonEmargee().planComptable().pcoNum().equals(pvi.plancoExerCtrePartie().pcoNum())) {
					EOPlanComptable pcoe = EOPlanComptableFinder.getPlancoValideForPcoNum(ed, pvi.ctrepartie().pcoNum(), false);
					nouveauPco = pcoe;
				}
			}
		}

		EOGestion gestionCredit = null;
		EOPlanComptable ancienPco = null;
		//recuperer le compte de contrepartie pour le mandat :
		EOEcritureDetail ecdCtp = mandat.ecritureDetailCtpNonEmargee();
		if (ecdCtp == null) {
			trace("ecriture de contrepartie émargée, pas d'ecriture auto de changement de mode de paiement");
		}
		if (ecdCtp != null) {
			ancienPco = ecdCtp.planComptable();
			gestionCredit = ecdCtp.gestion();
		}

		NSArray lesEcritures = null;

		trace("gestion Credit = " + gestionCredit);
		trace("ancienPco =" + ancienPco);
		trace("nouveauPco =" + nouveauPco);

		// ecriture ??
		if (generateEcriture) {
			if (ancienPco != null && nouveauPco != null) {
				// les 2 planComptables sont differents on passe une ecriture
				if (!ancienPco.equals(nouveauPco)) {
					trace("Les 2 planComptables sont differents on passe une ecriture");
					FactoryProcessPaiementMandat maFactoryPaiementMandat = new FactoryProcessPaiementMandat(withLogs(), getDateJourComptable());
					lesEcritures = maFactoryPaiementMandat.ecritureMandatModifierCredit(ed, mandat, FinderJournalEcriture.leTypeJournalPaiement(ed), utilisateur, "MODIF PAIEMENT MANDAT " + mandat.manNumero() + " BORD. " + mandat.bordereau().borNum(), nouveauPco, gestionCredit, false, exercice);
					// emargement automatique
					FactoryProcessEmargement maFactoryProcessEmargement = new FactoryProcessEmargement(withLogs(), getDateJourComptable());
					maFactoryProcessEmargement.emargerLesEcrituresDesMandats(ed, utilisateur, FinderEmargement.leTypeRapprochement(ed), mandat.exercice(), new NSArray(mandat), mandat.gestion().comptabilite());
				}
			}
		}

		//		// si lancien est null et le nouveau est renseigne
		//		if (ancienPco == null && nouveauPco != null) {
		//			
		//			trace("ancien est null et le nouveau est renseigne");
		//			if (ecd != null && ecd.ecdResteEmarger().doubleValue() == 0) {
		//				throw new FactoryException("L'écriture n°" + ecd.ecrNumeroAndEcdIndex() + " a été émargée, impossible de créer une écriture pour passer le crédit du compte <b>" + ecd.planComptable().pcoNum() + "</b> vers le compte <b>" + nouveauPco.pcoNum() + "</b>");
		//			}
		//			FactoryProcessPaiementMandat maFactoryPaiementMandat = new FactoryProcessPaiementMandat(withLogs(), getDateJourComptable());
		//			lesEcritures = maFactoryPaiementMandat.ecritureMandatModifierCredit(ed, mandat, FinderJournalEcriture.leTypeJournalPaiement(ed), utilisateur, "MODIF PAIEMENT MANDAT " + mandat.manNumero() + " BORD. " + mandat.bordereau().borNum(), nouveauPco, gestionCredit, false, exercice);
		//			// emargement automatique
		//			FactoryProcessEmargement maFactoryProcessEmargement = new FactoryProcessEmargement(withLogs(), getDateJourComptable());
		//			maFactoryProcessEmargement.emargerLesEcrituresDesMandats(ed, utilisateur, FinderEmargement.leTypeRapprochement(ed), mandat.exercice(), new NSArray(mandat), mandat.gestion().comptabilite());
		//		}

		// on modifie le mandat
		mandat.setModePaiementRelationship(nouveauMode);
		int i = 0;
		while (i < mandat.depenses().count()) {
			((EODepense) (mandat.depenses().objectAtIndex(i))).setModePaiementRelationship(nouveauMode);
			i++;
		}
		// une seul ecriture dans la tableau pas de SACD !!! ou vide !!!
		if (lesEcritures != null && lesEcritures.count() > 0) {
			return (EOEcriture) lesEcritures.objectAtIndex(0);
		}
		return null;

	}

	//	
	//	public EOEcriture modifierMandatModePaiement(EOEditingContext ed, EOMandat mandat, EOModePaiement nouveauMode, EOUtilisateur utilisateur, EOExercice exercice) throws Exception {
	//		EOPlanComptable ancienPco = mandat.modePaiement().planComptableVisa();
	//		EOPlanComptable nouveauPco = nouveauMode.planComptableVisa();
	//		
	//		//si le planco affecté au mode de paiement est nul, on récupère celui affecté compte de charge
	//		if (nouveauPco == null) {
	//			EOQualifier qual1 = new EOKeyValueQualifier(EOPlancoVisa.PCO_NUM_ORDONNATEUR_KEY, EOQualifier.QualifierOperatorEqual, mandat.planComptable().pcoNum());
	//			EOQualifier qual2 = new EOKeyValueQualifier(EOPlancoVisa.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, mandat.exercice());
	//			EOPlancoVisa pcoc = EOPlancoVisa.fetchByQualifier(ed, new EOAndQualifier(new NSArray(new Object[] {
	//					qual1, qual2
	//			})));
	//			if (pcoc != null) {
	//				EOPlanComptable pcoe = EOPlanComptableFinder.getPlancoValideForPcoNum(ed, pcoc.ctrepartie().pcoNum(), false);
	//				nouveauPco = pcoe;
	//			}
	//		}
	//		
	//		EOGestion gestionCredit = null;
	//		
	//		NSArray lesEcritures = null;
	//		
	//		if (mandat.manEtat().equals(EOMandat.mandatVirement)) {
	//			throw new Exception("IMPOSSIBLE DE MODIFIER UN MANDAT PAYE");
	//		}
	//		
	//		if (mandat.manEtat().equals(EOMandat.mandatAnnule)) {
	//			throw new Exception("IMPOSSIBLE DE MODIFIER UN MANDAT ANNULE");
	//		}
	//		
	//		// deteminer le gestion Credit composante ou agence ou sacd
	//		
	//		int i = 0;
	//		EOEcritureDetail ecd = null;
	//		while (i < mandat.mandatDetailEcritures().count() && gestionCredit == null) {
	//			ecd = ((EOMandatDetailEcriture) mandat.mandatDetailEcritures().objectAtIndex(i)).ecritureDetail();
	//			
	//			if (ecd.isCredit()) {
	//				gestionCredit = ecd.gestion();
	//			}
	//			i++;
	//		}
	//		trace("gestion Credit = " + gestionCredit);
	//		trace("ancienPco =" + ancienPco);
	//		trace("nouveauPco =" + nouveauPco);
	//		
	//		// ecriture ??
	//		if (ancienPco != null && nouveauPco != null) {
	//			// les 2 planComptables sont differents on passe une ecriture
	//			if (!ancienPco.equals(nouveauPco)) {
	//				trace("Les 2 planComptables sont differents on passe une ecriture");
	//				if (ecd != null && ecd.ecdResteEmarger().doubleValue() == 0) {
	//					throw new FactoryException("L'écriture n°" + ecd.ecrNumeroAndEcdIndex() + " a été émargée, impossible de créer une écriture pour passer le crédit du compte <b>" + ecd.planComptable().pcoNum() + "</b> vers le compte <b>" + nouveauPco.pcoNum() + "</b>");
	//				}
	//				FactoryProcessPaiementMandat maFactoryPaiementMandat = new FactoryProcessPaiementMandat(withLogs(), getDateJourComptable());
	//				lesEcritures = maFactoryPaiementMandat.ecritureMandatModifierCredit(ed, mandat, FinderJournalEcriture.leTypeJournalPaiement(ed), utilisateur, "MODIF PAIEMENT MANDAT " + mandat.manNumero() + " BORD. " + mandat.bordereau().borNum(), nouveauPco, gestionCredit, false, exercice);
	//				// emargement automatique
	//				FactoryProcessEmargement maFactoryProcessEmargement = new FactoryProcessEmargement(withLogs(), getDateJourComptable());
	//				maFactoryProcessEmargement.emargerLesEcrituresDesMandats(ed, utilisateur, FinderEmargement.leTypeRapprochement(ed), mandat.exercice(), new NSArray(mandat), mandat.gestion().comptabilite());
	//			}
	//		}
	//		
	//		// si lancien est null et le nouveau est renseigne
	//		if (ancienPco == null && nouveauPco != null) {
	//			
	//			trace("ancien est null et le nouveau est renseigne");
	//			if (ecd != null && ecd.ecdResteEmarger().doubleValue() == 0) {
	//				throw new FactoryException("L'écriture n°" + ecd.ecrNumeroAndEcdIndex() + " a été émargée, impossible de créer une écriture pour passer le crédit du compte <b>" + ecd.planComptable().pcoNum() + "</b> vers le compte <b>" + nouveauPco.pcoNum() + "</b>");
	//			}
	//			FactoryProcessPaiementMandat maFactoryPaiementMandat = new FactoryProcessPaiementMandat(withLogs(), getDateJourComptable());
	//			lesEcritures = maFactoryPaiementMandat.ecritureMandatModifierCredit(ed, mandat, FinderJournalEcriture.leTypeJournalPaiement(ed), utilisateur, "MODIF PAIEMENT MANDAT " + mandat.manNumero() + " BORD. " + mandat.bordereau().borNum(), nouveauPco, gestionCredit, false, exercice);
	//			// emargement automatique
	//			FactoryProcessEmargement maFactoryProcessEmargement = new FactoryProcessEmargement(withLogs(), getDateJourComptable());
	//			maFactoryProcessEmargement.emargerLesEcrituresDesMandats(ed, utilisateur, FinderEmargement.leTypeRapprochement(ed), mandat.exercice(), new NSArray(mandat), mandat.gestion().comptabilite());
	//		}
	//		
	//		// on modifie le mandat
	//		mandat.setModePaiementRelationship(nouveauMode);
	//		i = 0;
	//		while (i < mandat.depenses().count()) {
	//			((EODepense) (mandat.depenses().objectAtIndex(i))).setModePaiementRelationship(nouveauMode);
	//			i++;
	//		}
	//		// une seul ecriture dans la tableau pas de SACD !!! ou vide !!!
	//		if (lesEcritures != null && lesEcritures.count() > 0) {
	//			return (EOEcriture) lesEcritures.objectAtIndex(0);
	//		}
	//		return null;
	//		
	//	}
	//
	//	public EOEcriture modifierTitreModePaiement(EOEditingContext ed, EOTitre titre, EOModePaiement nouveauMode, EOUtilisateur utilisateur) throws Exception {
	//
	//		return null;
	//	}

	public void modifierRibMandat(EOEditingContext ed, EOMandat mandat, EORib rib) throws Exception {

		if (mandat.manEtat().equals(EOMandat.mandatVirement)) {
			throw new Exception("IMPOSSIBLE DE MODIFIER UN MANDAT PAYE");
		}

		if (mandat.manEtat().equals(EOMandat.mandatAnnule)) {
			throw new Exception("IMPOSSIBLE DE MODIFIER UN MANDAT ANNULE");
		}

		if (mandat.modePaiement().modDom().equals(EOModePaiement.MODDOM_VIREMENT)) {
			mandat.setRibRelationship(rib);
			int i = 0;
			while (i < mandat.depenses().count()) {
				((EODepense) (mandat.depenses().objectAtIndex(i))).setRibRelationship(rib);
				i++;
			}
		}
		else {
			throw new Exception("PAS DE RIB POUR CE MODE DE PAIEMENT");
		}

	}
	//
	//	public void modifierRibTitre(EOEditingContext ed, EOTitre titre, EORib rib) throws Exception {
	//
	//		if (titre.titEtat().equals(EOTitre.titrePaye)) {
	//			throw new Exception("IMPOSSIBLE DE MODIFIER UN TITRE PAYE");
	//		}
	//
	//		if (titre.titEtat().equals(EOTitre.titreAnnule)) {
	//			throw new Exception("IMPOSSIBLE DE MODIFIER UN TITRE ANNULE");
	//		}
	//
	//		if (titre.modePaiement().modDom().equals(EOModePaiement.MODDOM_VIREMENT)) {
	//			titre.setRibRelationship(rib);
	//			//			titre.addObjectToBothSidesOfRelationshipWithKey(rib, "rib");
	//			int i = 0;
	//			while (i < titre.recettes().count()) {
	//				((EORecette) (titre.recettes().objectAtIndex(i))).setRibRelationship(rib);
	//				//				((EORecette) (titre.recettes().objectAtIndex(i))).addObjectToBothSidesOfRelationshipWithKey(rib, "rib");
	//				i++;
	//			}
	//		}
	//		else {
	//			throw new Exception("PAS DE RIB POUR CE MODE DE PAIEMENT");
	//		}
	//
	//	}

}
