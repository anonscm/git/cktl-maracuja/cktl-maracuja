/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.factory.FactoryChequeDetailEcriture;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOCheque;
import org.cocktail.maracuja.client.metier.EOChequeBrouillard;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author frivalla To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FactoryProcessBordereauDeCheques extends FactoryProcess {

	/**
	 * @param withLog
	 */
	public FactoryProcessBordereauDeCheques(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public void fermerUnBordereauDeCheques(EOEditingContext ed, EOBordereau leBordereau) throws Exception {

		if (EOBordereau.BordereauVise.equals(leBordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		if (EOBordereau.BordereauAnnule.equals(leBordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAnnule);
		}

		if (EOBordereau.BordereauAViser.equals(leBordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAViser);
		}

		leBordereau.setBorEtat(EOBordereau.BordereauAViser);

	}

	public NSMutableArray viserUnBordereauDeCheques(EOEditingContext ed, EOBordereau leBordereau, EOUtilisateur utilisateur) throws Exception {
		NSMutableArray lesEcritures = new NSMutableArray();
		BigDecimal montantDebit = new BigDecimal(0);

		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

		EOEcriture monEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, getDateJourComptable(), "VISA " + leBordereau.bordereauInfo().borLibelle(), new Integer(0), null, leBordereau.gestion().comptabilite(), leBordereau.exercice(), null, //origine ????
				FinderJournalEcriture.leTypeJournalBordCheque(ed), FinderJournalEcriture.leTypeOperationGenerique(ed), utilisateur);

		// creation du ecritureDetail de Dedit de montatDebit = 0 init
		EOEcritureDetail monEcritureDetailDebit = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(0), leBordereau.borLibelle(), montantDebit, "", montantDebit, null, sensDebit(), monEcriture, monEcriture.exercice(), leBordereau.gestion(), leBordereau.bordereauInfo()
				.planComptableVisa());
		// DEBUT  pour chaque cheque VALIDE (cheques)
		int i = 0;
		// creation du chequeEcritureDetail Credit
		FactoryChequeDetailEcriture maFactoryChequeDetailEcriture = new FactoryChequeDetailEcriture(withLogs());
		NSArray cheques = leBordereau.getChequesValides();
		while (i < cheques.count()) {
			EOCheque tmpCheque = (EOCheque) cheques.objectAtIndex(i);

			// chequebrouillard (VALIDE)
			if (EOCheque.etatValide.equals(tmpCheque.cheEtat())) {
				int j = 0;
				while (j < tmpCheque.chequeBrouillards().count()) {
					EOChequeBrouillard tmpChequeBrouillard = (EOChequeBrouillard) (tmpCheque.chequeBrouillards().objectAtIndex(j));

					//ajout rod:
					if (EOChequeBrouillard.etatValide.equals(tmpChequeBrouillard.chbEtat())) {
						if (sensDebit().equals(tmpChequeBrouillard.chbSens())) {
							throw new FactoryException(EOChequeBrouillard.problemeEcritureDedit);
						}

						// creation du ecritureDetail pour le
						EOEcritureDetail monEcritureDetailCredit = maFactoryEcritureDetail.creerEcritureDetail(ed, null, new Integer(j + 1),
								tmpChequeBrouillard.chbLibelle(), tmpChequeBrouillard.chbMontant(), "", tmpChequeBrouillard.chbMontant(), null, tmpChequeBrouillard.chbSens(), monEcriture, monEcriture.exercice(), tmpChequeBrouillard.gestion(), tmpChequeBrouillard.planComptable());
						maFactoryChequeDetailEcriture.creerChequeDetailEcriture(ed, this.getDateJour(), "VISA", monEcritureDetailCredit, monEcriture.exercice(), tmpCheque);
						montantDebit = montantDebit.add(tmpChequeBrouillard.chbMontant());
					}
					j++;
				}

				// creation du chequeEcritureDetail Debit
				maFactoryChequeDetailEcriture.creerChequeDetailEcriture(ed, this.getDateJour(), "VISA", monEcritureDetailDebit, monEcriture.exercice(), tmpCheque);

				// maj de  l etat du cheque
				tmpCheque.setCheEtat(EOCheque.etatVise);

			}
			i++;
		}
		// FIN pour chaque cheque VALIDE

		// MAJ du ecritureDetail de Dedit de montantDebit
		monEcritureDetailDebit.setEcdDebit(montantDebit);
		monEcritureDetailDebit.setEcdMontant(montantDebit);
		monEcritureDetailDebit.setEcdResteEmarger(montantDebit);

		// maj du bordereau
		leBordereau.setBorEtat(EOBordereau.BordereauVise);
		//ajout rod:
		leBordereau.setBorDateVisa(getDateJour());
		leBordereau.setUtilisateurVisaRelationship(utilisateur);

		lesEcritures.addObject(monEcriture);

		// eciture SACD
		lesEcritures.addObjectsFromArray(maFactoryProcessJournalEcriture.determinerLesEcrituresSACD(ed, monEcriture));

		// operation de tresorerie ?????

		return lesEcritures;
	}

	private void verfierTypeDeBordereau(EOBordereau leBordereau) throws FactoryException {

		if (EOBordereau.BordereauVise.equals(leBordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauDejaVise);
		}

		if (EOBordereau.BordereauAnnule.equals(leBordereau.borEtat())) {
			throw new FactoryException(EOBordereau.problemeBordereauAnnule);
		}

	}

}
