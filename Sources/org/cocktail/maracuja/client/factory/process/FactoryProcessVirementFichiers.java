/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 21 sept. 2004
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOVirementParamBdf;
import org.cocktail.maracuja.client.metier.EOVirementParamEtebac;
import org.cocktail.maracuja.client.metier.EOVirementParamVint;
import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * @author frivalla
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class FactoryProcessVirementFichiers extends FactoryProcess {
	public static final int LONGUEUR_LIGNE_BDF = 240;

	/**
	 * @param withLog
	 */
	public FactoryProcessVirementFichiers(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	public String genererFichierBanqueDeFrance(EOEditingContext ed, NSArray lesEODepense, NSArray lesEOOrdreDePaiement, EOVirementParamBdf leVirementParamBdf, NSTimestamp dateDisquette, boolean groupByRib) throws FactoryException {

		// creation du virement

		// creation di verimentbdf

		// maj des depenses

		// maj des ordres de paiement

		if (lesEODepense.count() == 0 && lesEOOrdreDePaiement.count() == 0) {
			throw new FactoryException("Aucune depense ou ordre de paiement à exporter");
		}

		return this.genererBanqueDeFrance(ed, lesEODepense, lesEOOrdreDePaiement, leVirementParamBdf, dateDisquette, groupByRib);
	}

	public String genererFichierETEBAC(EOEditingContext ed, NSArray lesEODepense, NSArray lesEOOrdreDePaiement, NSArray lesEORecettes, EOVirementParamEtebac leVirementParamEtebac, NSTimestamp dateValeur, boolean groupByRib) {

		return this.genererETEBAC(ed, lesEODepense, lesEOOrdreDePaiement, lesEORecettes, leVirementParamEtebac, dateValeur, groupByRib);
	}

	public String genererFichierVirementInternational(EOEditingContext ed, NSArray lesEODepense, NSArray lesEOOrdreDePaiement, EOVirementParamVint leVirementParamVint) {

		return this.genererFichierVirementInternational(ed, lesEODepense, lesEOOrdreDePaiement, leVirementParamVint);
	}

	/*
	 * Format de la disquette :
	 * 
	 * Entete : A : A1 (2) : 01 A2 (6) : Numérotage B : B1 (2) : INUTILISE B2 (6) : date JJMMAA C : C1 (5) : C2 (5) : C3 (5) : Code guichet C4/1 (4) :
	 * Code du remettant C4/2 (7) : No compte remettant C5 (24) : Nom organisme remettant C6 (6) : INUTILISE D : (168) : INUTILISE
	 * 
	 * DETAIL : A : A1 (2) : 01 A2 (6) : Numérotage B : B1 (2) : Code Operation B2 (6) : date JJMMAA C : C1 (5) : C2 (5) : C3 (5) : Code guichet C4/1
	 * (4) : Code du remettant C4/2 (7) : D000000 C5 (24) : Nom organisme remettant C6 (6) : INUTILISE D : D1 (5) : INUTILISE D2 (5) : Code banque D3
	 * (5) : Code guichet D4 (11) : No Compte destinataire D5 (24) : Nom du destinataire D6 (6) : Nom organisme remettant D7 (24) : Libelle
	 * domiciliation D8 (32) : libelle 1 D9 (32) : libelle 2 D10 (12) : INUTILISE D11 (12) : Montant
	 */

	/**
     *
     */
	private String genererBanqueDeFrance(EOEditingContext ed, NSArray lesEODepense, NSArray lesEOOrdreDePaiement, EOVirementParamBdf leVirementParamBdf, NSTimestamp dateDisquette, boolean groupByRib) {

		//int numero = 0;

		//
		//        trace("lesEODepense = " + lesEODepense);
		//        trace("lesEOOrdreDePaiement = " + lesEOOrdreDePaiement);
		//        trace("lesEORecettes = " + lesEORecettes);

		FichierVirement fichierVirement = new FichierVirement(lesEODepense.arrayByAddingObjectsFromArray(lesEOOrdreDePaiement), leVirementParamBdf, dateDisquette, groupByRib);

		String contenu = fichierVirement.contenu();

		// verification que la DK a un bon format (n lignes de 240 car.)
		if (!fichierVirement.isValide()) {
			throw new FactoryException("Erreur, le fichier n'est pas valide.");
		}

		return contenu;
	}

	/*
	 * Format de la disquette :
	 * 
	 * Entete : A (2) : B1 (2) : B2 (8) : INUTILISE B3 (6) : C1 (12) : date echeance JJMMA (calé à droite) C2 (24) : D1-1 (7) : ref remise (vide) D1-2
	 * (19) : INUTILISE D2-1 (1) : D2-2 (5) : INUTILISE D3 (5) : D4 (11) : E : F (47) : INUTILISE G1 (5) : G2 (6) : INUTILISE
	 * 
	 * DETAIL : A (2) : B1 (2) : B2 (8) : INUTILISE B3 (6) : C1 (12) : "REFERE" + Numérotage C2 (24) : nom destinataire D1 (24) : docimiciliation D2
	 * (8) : INUTILISE D3 (5) : code guichet D4 (11) : no compte E (16) : Montant (cadré a droite) F (31) : libelle G1 (5): code etablissement
	 * destinataire G2 (6) : INUTILISE
	 * 
	 * Total: A (2) : B1 (2) : B2 (8) : B3 (6) : C : D (54) : INUTILISE E (16) montant total (cadre a droite) F : G (42) : INUTILISE
	 */

	private String genererETEBAC(EOEditingContext ed, NSArray lesEODepense, NSArray lesEOOrdreDePaiement, NSArray lesEORecettes, EOVirementParamEtebac leVirementParamEtebac, NSTimestamp dateValeur, boolean groupByRib) {

		//BigDecimal montantEtebac = new BigDecimal(0);

		FichierVirement fichierVirement = new FichierVirement(lesEODepense.arrayByAddingObjectsFromArray(lesEOOrdreDePaiement.arrayByAddingObjectsFromArray(lesEORecettes)), leVirementParamEtebac, dateValeur, groupByRib);

		String contenu = fichierVirement.contenu();

		// verification que le fichier etebac a un bon format (n lignes de 160 car.)
		if (fichierVirement.isValide() == false) {
			return null;
		}

		return contenu;

	}

	private String genererVirementInternational(EOEditingContext ed, NSArray lesEODepense, NSArray lesEOOrdreDePaiement, NSArray lesEORecettes, EOVirementParamVint leVirementParamVint, boolean groupByRib) {

		return "";
	}

	/**
	 * @author ctarade
	 */
	private class FichierVirement {

		private NSArray lesRecords;
		private Object unRecord;
		private final Object lesParams;
		private int leNumero;
		private final NSTimestamp laDateFichier;
		private BigDecimal montantTotal;
		private String leContenu;
		private final boolean groupByRib;

		/**
		 * @param desRecords : les depenses, ordres de paiement et recettes
		 * @param desParams : l'enregistrement parametres associé au fichier a produire
		 * @param numeroDisquette : numero de la disquette (obligatoire pour BDF)
		 */
		public FichierVirement(NSArray desRecords, Object desParams, NSTimestamp uneDateFichier, boolean unGroupByRib) {
			super();
			lesRecords = desRecords;
			lesParams = desParams;
			laDateFichier = uneDateFichier;
			montantTotal = new BigDecimal(0);
			groupByRib = unGroupByRib;
			leNumero = 1;
		}

		// METHODES SUR TOUT LE FICHIER

		public boolean isDisquette() {
			return lesParams instanceof EOVirementParamBdf;
		}

		public boolean isEtebac() {
			return lesParams instanceof EOVirementParamEtebac;
		}

		/**
		 * verification de la validité du contenu (nombre de caracteres)
		 * 
		 * @return
		 */
		public boolean isValide() {
			boolean isValide = false;
			if (isDisquette()) {
				isValide = ((contenu().length() % LONGUEUR_LIGNE_BDF) == 0);
			}
			else if (isEtebac()) {
				isValide = ((contenu().length() % 160) == 0);
			}
			return isValide;
		}

		private EOVirementParamBdf leVirementParamBdf() {
			return (EOVirementParamBdf) lesParams;
		}

		private EOVirementParamEtebac leVirementParamEtebac() {
			return (EOVirementParamEtebac) lesParams;
		}

		public String contenu() {
			if (leContenu == null) {
				leContenu = entete() + details() + total();
			}
			trace("Longueur fichier : " + leContenu.length());

			return leContenu;
		}

		private String entete() {
			String entete = "";
			if (isDisquette()) {
				entete += "01"; // A
				entete += stringCompletion(Integer.toString(leNumero++), 6, "0", "G"); // A
				entete += stringCompletion("", 2, " ", "D"); // B
				//                entete += stringCompletion(leVirementParamBdf().vpbB1(), 			2, 		" ", 	"D");
				entete += dateToString(laDateFichier, "%d%m%y");
				entete += stringCompletion(leVirementParamBdf().vpbC1(), 5, "0", "D");
				entete += stringCompletion(leVirementParamBdf().vpbC2(), 5, " ", "D");
				entete += stringCompletion(leVirementParamBdf().vpbC3(), 5, " ", "D");
				entete += stringCompletion(leVirementParamBdf().vpbC41(), 4, " ", "D");
				entete += stringCompletion(leVirementParamBdf().vpbC42(), 7, " ", "D");
				entete += stringCompletion(leVirementParamBdf().vpbC5(), 24, " ", "D");
				entete += stringCompletion("", 6, " ", "G"); // C
				entete += stringCompletion("", 168, " ", "G");

			}
			else if (isEtebac()) {
				entete += stringCompletion(leVirementParamEtebac().vpeEA(), 2, " ", "D"); // A
				entete += stringCompletion(leVirementParamEtebac().vpeEB1(), 2, " ", "D"); // B1
				entete += stringCompletion("", 8, " ", "D"); // B2
				entete += stringCompletion(leVirementParamEtebac().vpeEB3(), 6, " ", "D"); // B3
				entete += stringCompletion(dateToString(laDateFichier, "%d%m%y"), 12, " ", "D"); // C1
				entete += stringCompletion(leVirementParamEtebac().vpeEC2(), 24, " ", "D"); // C2
				entete += stringCompletion("", 7, " ", "D"); // D1-1
				entete += stringCompletion("", 19, " ", "D"); // D1-2
				entete += stringCompletion("", 1, " ", "D"); // D2-1
				entete += stringCompletion("", 5, " ", "D"); // D2-2
				entete += stringCompletion(leVirementParamEtebac().vpeED3(), 5, " ", "D"); // D3
				entete += stringCompletion(leVirementParamEtebac().vpeED4(), 11, " ", "D"); // D4
				entete += stringCompletion("", 47, " ", "D"); // E-F
				entete += stringCompletion(leVirementParamEtebac().vpeEG1(), 5, " ", "D"); // G1
				entete += stringCompletion("", 6, " ", "D"); // G2
			}
			return entete;
		}

		/**
		 * genere une ligne de detail
		 * 
		 * @param unRib : rib du destinataire
		 * @param unMontant : montant de la ligne
		 * @return
		 */
		private String detailPourRibEtMontant(EORib unRib, BigDecimal unMontant) {

			// on vire les accents eventuels
			//Modif rod : ca sert à rien de prendre la substring car c'est reformaté apres et en plus pb de index ou of bounds :/
			//            String ribTitco	= chaineSansAccents(rib().ribTitco()).substring(0,24);
			//            String ribLib	= chaineSansAccents(rib().ribLib()).substring(0,24);
			String ribTitco = ZStringUtil.chaineSansCaracteresSpeciauxUpper(unRib.ribTitco());
			String ribLib = ZStringUtil.chaineSansCaracteresSpeciauxUpper(unRib.ribLib());

			String detail = "";
			if (isDisquette()) {
				//                trace("----> generation format disquette");

				// Construction de la ligne de disquette

				detail += "04"; // A
				detail += stringCompletion(Integer.toString(leNumero++), 6, "0", "G"); // A
				detail += stringCompletion(leVirementParamBdf().vpbB1(), 2, " ", "D"); // B
				detail += dateToString(laDateFichier, "%d%m%y");
				detail += stringCompletion(leVirementParamBdf().vpbC1(), 5, "0", "D"); // C
				detail += stringCompletion(leVirementParamBdf().vpbC2(), 5, " ", "D");
				detail += stringCompletion(leVirementParamBdf().vpbC3(), 5, " ", "D");
				detail += stringCompletion(leVirementParamBdf().vpbC41(), 4, " ", "D");
				detail += stringCompletion(leVirementParamBdf().vpbC42(), 7, " ", "D");
				detail += stringCompletion(leVirementParamBdf().vpbC5(), 24, " ", "D");
				detail += stringCompletion("", 6, " ", "G");
				detail += stringCompletion("", 5, " ", "G"); // D1
				detail += stringCompletion(unRib.ribCodBanc(), 5, " ", "D"); // D2
				detail += stringCompletion(unRib.ribGuich(), 5, " ", "D"); // D3
				detail += stringCompletion(unRib.ribNum(), 11, " ", "D"); // D4
				detail += stringCompletion(ribTitco, 24, " ", "D"); // D5
				detail += stringCompletion("", 6, " ", "G"); // D6
				detail += stringCompletion(ribLib, 24, " ", "D"); // D7
				detail += stringCompletion(libelle(), 64, " ", "D"); // D8 - D9
				detail += stringCompletion(leVirementParamBdf().vpbD10(), 12, " ", "G"); // D10
				detail += stringCompletion(montantString(unMontant), 12, "0", "G"); // D11

			}
			else if (isEtebac()) {
				trace("----> generation format etebac");
				detail += stringCompletion(leVirementParamEtebac().vpeDA(), 2, " ", "D"); // A
				detail += stringCompletion(leVirementParamEtebac().vpeDB1(), 2, " ", "D"); // B1
				detail += stringCompletion("", 8, " ", "D"); // B2
				detail += stringCompletion(leVirementParamEtebac().vpeDB3(), 6, " ", "D"); // B3
				detail += stringCompletion("REFERE" + (leNumero++), 12, " ", "D"); // C1
				detail += stringCompletion(ribTitco, 24, " ", "G"); // C2
				detail += stringCompletion(ribLib, 24, " ", "G"); // D1
				detail += stringCompletion("", 8, " ", "D"); // D2
				detail += stringCompletion(unRib.ribGuich(), 5, " ", "D"); // D3
				detail += stringCompletion(unRib.ribNum(), 11, " ", "D"); // D4
				detail += stringCompletion(montantString(unMontant), 16, "0", "D"); // E
				detail += stringCompletion(libelle(), 31, " ", "G"); // F
				detail += stringCompletion(unRib.ribCodBanc(), 5, " ", "D"); // G1
				detail += stringCompletion("", 6, " ", "G"); // G2
			}
			else {
				trace("----> pas de generation, format inconnu");
			}

			return detail;
		}

		private String details() {
			String details = "";
			//int numeroLigne = 0;
			//EORib prevRib = null;

			// on regroupe par rib identique
			if (groupByRib) {
				trace("----> Groupement des factures par RIB");
				NSArray arraySort = new NSArray(EOSortOrdering.sortOrderingWithKey("rib", EOSortOrdering.CompareAscending));
				lesRecords = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesRecords, arraySort);

				// tous les ribs
				NSArray lesRibs = (NSArray) lesRecords.valueForKey("rib");
				// on vire les doublons
				lesRibs = removeDoublons(lesRibs);

				for (int i = 0; i < lesRibs.count(); i++) {

					// liste des records associes au rib en cours
					EORib unRib = (EORib) lesRibs.objectAtIndex(i);
					EOQualifier qualRib = EOQualifier.qualifierWithQualifierFormat("rib = %@", new NSArray(unRib));
					NSArray lesRecordsDuRib = EOQualifier.filteredArrayWithQualifier(lesRecords, qualRib);

					// somme des montants de tous les records du rib
					BigDecimal montantLigne = ZConst.BIGDECIMAL_ZERO;
					for (int j = 0; j < lesRecordsDuRib.count(); j++) {
						unRecord = lesRecordsDuRib.objectAtIndex(j);
						montantLigne = montantLigne.add(montant());
					}
					//              On ne crée pas de ligne à 0
					if (montantLigne.compareTo(ZConst.BIGDECIMAL_ZERO) > 0) {
						// ligne associee
						details += detailPourRibEtMontant(unRib, montantLigne);
						montantTotal = montantTotal.add(montantLigne);
					}

				}
			}
			// on prends tout en vrac
			else {
				trace("----> PAS de groupement des factures par RIB");
				for (int i = 0; i < lesRecords.count(); i++) {
					unRecord = lesRecords.objectAtIndex(i);
					final BigDecimal montantDisk = montant();
					//On ne crée pas de ligne à 0
					if (montantDisk.compareTo(ZConst.BIGDECIMAL_ZERO) > 0) {
						details += detailPourRibEtMontant(rib(), montantDisk);
						montantTotal = montantTotal.add(montantDisk);
					}
				}
			}

			return details;
		}

		private String total() {
			String total = "";
			String montantTotalString = replace(montantTotal.toString(), ".", "");
			if (isDisquette()) {

				//                trace("");
				//                trace("");
				//                trace("");
				//                trace("");
				//                trace("");
				//                trace("leVirementParamBdf().vpbB1() = " + leVirementParamBdf().vpbB1() + "|");
				//                trace("stringCompletion(leVirementParamBdf().vpbB1() = " + stringCompletion(leVirementParamBdf().vpbB1(),					2,		" ",	"D")+ "|");
				//                trace("************************************************");
				//

				total += "09"; // A
				total += stringCompletion(Integer.toString(leNumero++), 6, "0", "G"); // A
				total += stringCompletion(leVirementParamBdf().vpbB1(), 2, " ", "D");
				total += dateToString(laDateFichier, "%d%m%y"); // B
				total += stringCompletion(leVirementParamBdf().vpbC1(), 5, "0", "D");
				total += stringCompletion(leVirementParamBdf().vpbC2(), 5, " ", "D");
				total += stringCompletion(leVirementParamBdf().vpbC3(), 5, " ", "D");
				total += stringCompletion(leVirementParamBdf().vpbC41(), 4, " ", "D");
				total += stringCompletion(leVirementParamBdf().vpbC42(), 7, " ", "D");
				total += stringCompletion(leVirementParamBdf().vpbC5(), 24, " ", "D");
				total += stringCompletion("", 6, " ", "D"); // C
				total += stringCompletion("", 156, " ", "G"); // D
				total += stringCompletion(montantTotalString, 12, "0", "G");
			}
			else if (isEtebac()) {
				total += stringCompletion(leVirementParamEtebac().vpeTA(), 2, " ", "D"); // A
				total += stringCompletion(leVirementParamEtebac().vpeTB1(), 2, " ", "D"); // B1
				total += stringCompletion(leVirementParamEtebac().vpeTB2(), 8, " ", "D"); // B2
				total += stringCompletion(leVirementParamEtebac().vpeTB3(), 6, " ", "D"); // B3
				total += stringCompletion("", 54, " ", "D"); // C : D
				total += stringCompletion(montantTotalString, 16, " ", "D"); // E
				total += stringCompletion("", 42, " ", "D"); // F : G

			}
			return total;

		}

		// METHODES SUR LE RECORD EN COURS

		private EODepense laDepense() {
			return (EODepense) unRecord;
		}

		private EOOrdreDePaiement lOrdreDePaiement() {
			return (EOOrdreDePaiement) unRecord;
		}

		private EORecette laRecette() {
			return (EORecette) unRecord;
		}

		private boolean isEODepense() {
			return unRecord instanceof EODepense;
		}

		private boolean isEOOrdreDePaiement() {
			return unRecord instanceof EOOrdreDePaiement;
		}

		private boolean isEORecette() {
			return unRecord instanceof EORecette;
		}

		/**
		 * libelle de la ligne
		 * 
		 * @return
		 */
		public String libelle() {
			String libelleBDF = null;
			if (isEODepense()) {
				libelleBDF = stringCompletion(ZStringUtil.toBasicString(ZStringUtil.chaineSansCaracteresSpeciauxUpper(laDepense().getLibelleBdf1()), "._- ", ' '), 32, " ", "D");
				libelleBDF += stringCompletion(ZStringUtil.toBasicString(ZStringUtil.chaineSansCaracteresSpeciauxUpper(laDepense().getLibelleBdf2()), "._- ", ' '), 32, " ", "D");
			}
			else if (isEOOrdreDePaiement()) {
				libelleBDF = stringCompletion(ZStringUtil.toBasicString(ZStringUtil.chaineSansCaracteresSpeciauxUpper(lOrdreDePaiement().getLibelleBdf1())), 32, " ", "D");
				libelleBDF += stringCompletion(ZStringUtil.toBasicString(ZStringUtil.chaineSansCaracteresSpeciauxUpper(lOrdreDePaiement().getLibelleBdf2())), 32, " ", "D");
			}
			//            else if (isEORecette()) {
			////                if (laRecette().recLibelle().toString().length() <= 12) {
			////    	            libelleBDF = stringCompletion(laRecette().recLibelle().toString(),	12, " ",  "D");
			////    	        }
			////    	        else {
			////    	            libelleBDF = laRecette().recLibelle().toString().substring(laRecette().recLibelle().toString().length() - 12, laRecette().recLibelle().toString().length());
			////    	        }
			//                libelleBDF = laRecette().recLibelle();
			//                if (laRecette().recDate() != null) {
			//                    libelleBDF += " DU " + dateToString(laRecette().recDate(), "%d%m%y");
			//                }
			//                libelleBDF += " " + laRecette().recNum().toString();
			//
			//            }

			//modif rod pb substring inutile
			//            libelleBDF = chaineSansAccents(libelleBDF).substring(0,64);
			//            libelleBDF = ZlibelleBDF);
			return libelleBDF;
		}

		/**
		 * montant de la ligne transformée en string
		 * 
		 * @return
		 */
		public String montantString(BigDecimal unMontant) {
			String montantString = replace(unMontant.toString(), ".", "");
			return montantString;
		}

		/**
		 * montant de la ligne
		 * 
		 * @return
		 */
		public BigDecimal montant() {
			BigDecimal montant = null;
			if (isEODepense()) {
				montant = laDepense().depMontantDisquette();
			}
			else if (isEOOrdreDePaiement()) {
				montant = lOrdreDePaiement().odpMontantPaiement();
			}
			else if (isEORecette()) {
				montant = laRecette().recMontantDisquette();
			}
			return montant;
		}

		/**
		 * rib attache
		 * 
		 * @return
		 */
		public EORib rib() {
			EORib rib = null;
			if (isEODepense()) {
				rib = laDepense().rib();
			}
			else if (isEOOrdreDePaiement()) {
				rib = lOrdreDePaiement().rib();
			}
			else if (isEORecette()) {
				rib = laRecette().rib();
			}
			else {
				throw new FactoryException("Le rib n'est pas défini");
			}
			return rib;
		}

		// METHODES UTILITAIRES

		/**
		 * Formatte une chaine selon une longueur donnee, avec un caractere et un sens donne Exemple : "TOTO",10,"Z","D" ==> return "TOTOZZZZZZ"
		 * 
		 * @param chaine Chaine a completer
		 * @param nbCars Nombre de caracteres en retour
		 * @param carac Caractere de completion
		 * @param sens Completion vers la droite (D) ou la gauche (G)
		 * @return
		 */
		public String stringCompletion(String chaine, int nbCars, String carac, String sens) {
			if (chaine == null) {
				chaine = "";
			}

			String retour = chaine;
			//            trace("retour="+retour+"|");
			if (retour.length() > nbCars) {
				//                trace("dans premier if nbcar="+nbCars);
				retour = retour.substring(0, nbCars);
			}

			for (int i = 0; i < nbCars - chaine.length(); i++) {
				//                trace("dans for i="+i);
				if ("D".equals(sens)) {
					//                    trace("dans for D");
					retour = retour + carac;
				}
				else {
					retour = carac + retour;
				}
			}

			return retour;
		}

		/**
        *
        */
		//       public String chaineSansAccents(String chaine) {
		//           String retour = chaine.toLowerCase();
		//
		//           replace(retour, "é","e");
		//           replace(retour, "è","e");
		//           replace(retour, "ê","e");
		//
		//           replace(retour, "à","a");
		//           replace(retour, "â","a");
		//
		//           replace(retour, "î","i");
		//           replace(retour, "ï","i");
		//
		//           replace(retour, "ô","o");
		//           replace(retour, "ö","o");
		//
		//           replace(retour, "ü","u");
		//           replace(retour, "û","u");
		//
		//           return retour.toUpperCase();
		//       }
		/**
		 * Converti un objet date en un objet <code>String</code> suivant le format <code>dateFormat</code>. Le format doit correspondre au format
		 * accepte par <code>NSTimestampFormatter</code>.
		 * 
		 * @see #dateToString(NSTimestamp)
		 */
		public String dateToString(NSTimestamp date, String dateFormat) {
			// WO4.5.x != WO5.x
			String dateString;
			NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
			try {
				dateString = formatter.format(date);
			} catch (Exception ex) {
				dateString = "";
			}
			return dateString;

		}

		/**
		 * Remplace dans la chaine de caracteres <i>s</i> toutes les occurences de la chaine <i>what</i> par la chaine <i>byWhat</i>.
		 */
		public String replace(String s, String what, String byWhat) {
			String emptyString = "";
			StringBuffer sb;
			int i;

			//     if ((s == null) || (what == null)) return s;
			sb = new StringBuffer();
			if (byWhat == null) {
				byWhat = emptyString;
			}
			do {
				i = s.indexOf(what);
				if (i >= 0) {
					sb.append(s.substring(0, i));
					sb.append(byWhat);
					s = s.substring(i + what.length());
				}
			} while (i != -1);
			sb.append(s);
			return sb.toString();
		}

		/**
		 * enleve les doublons d'un array
		 * 
		 * @param array
		 * @return
		 */
		public NSArray removeDoublons(NSArray array) {
			NSMutableArray unique = new NSMutableArray();
			for (int i = 0; i < array.count(); i++) {
				if (!unique.containsObject(array.objectAtIndex(i))) {
					unique.addObject(array.objectAtIndex(i));
				}
			}
			return unique.immutableClone();
		}

	}

}
