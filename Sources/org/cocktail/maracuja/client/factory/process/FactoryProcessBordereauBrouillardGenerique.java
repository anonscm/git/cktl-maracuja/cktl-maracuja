/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory.process;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheancierBob;
import org.cocktail.fwkcktlcompta.common.echeancier.helpers.EcheancierHelper;
import org.cocktail.fwkcktlcompta.common.helpers.GrhumFournisHelper;
import org.cocktail.fwkcktlcompta.common.helpers.GrhumRibHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddMandatHelper;
import org.cocktail.fwkcktlcompta.common.sepasdd.rules.SepaSddEcheancierRule;
import org.cocktail.maracuja.client.exception.BordereauRejetException;
import org.cocktail.maracuja.client.factory.FactoryEcheancierDetailEcriture;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauBrouillard;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcheancierBrouillard;
import org.cocktail.maracuja.client.metier.EOEcheancierDetailEcr;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOSepaSddEcheancierEcd;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author frivalla To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public final class FactoryProcessBordereauBrouillardGenerique extends FactoryProcess {
	private final String BROUILLARD_TRAITE = "TRAITE";

	/**
	 * @param withLog
	 * @param dateJourComptable
	 */
	public FactoryProcessBordereauBrouillardGenerique(final boolean withLog, final NSTimestamp dateJourComptable) {
		super(withLog, dateJourComptable);
	}

	/**
	 * permet de viser un bordereau de a une seule bobOperation
	 */
	public EOEcriture viserUnBordereau(EOEditingContext ed, EOBordereau bordereau, EOComptabilite comptabilite, EOExercice exercice, EOOrigine origine, EOTypeJournal typeJournal, EOTypeOperation typeOperation, EOUtilisateur utilisateur) throws Exception {
		return viserUnBordereau(ed, bordereau, comptabilite, exercice, origine, typeJournal, typeOperation, utilisateur, getDateJour());
	}

	/**
	 * @param ed
	 * @param bordereau
	 * @param comptabilite
	 * @param exercice
	 * @param origine
	 * @param typeJournal
	 * @param typeOperation
	 * @param utilisateur
	 * @param dateEcriture Attention, passer la bonne date (journée comptable). Si la date passée n'est pas sur le bon exercice, on prend la date de
	 *            journée comptable
	 * @return
	 * @throws Exception
	 */
	public EOEcriture viserUnBordereau(EOEditingContext ed, EOBordereau bordereau, EOComptabilite comptabilite, EOExercice exercice, EOOrigine origine, EOTypeJournal typeJournal, EOTypeOperation typeOperation, EOUtilisateur utilisateur, NSTimestamp dateEcriture) throws Exception {
		EOEcriture nouvelleEcriture = null;
		NSTimestamp dateEcriture2 = new NSTimestamp(dateEcriture);
		FactoryProcessJournalEcriture maFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(withLogs(), getDateJourComptable());
		//Verifier que si la dateEcriture est passée, on est sur le bon exercice, sinon on prend la date de journée comptable
		if (dateEcriture2 != null) {
			if (ZDateUtil.getYear(dateEcriture2) != exercice.exeExercice().intValue()) {
				dateEcriture2 = getDateJourComptable();
			}
		}

		// creation de l ecriture
		nouvelleEcriture = maFactoryProcessJournalEcriture.creerEcriture(ed, dateEcriture, bordereau.bordereauInfo().borLibelle(), new Integer(0), null, comptabilite, exercice, origine, typeJournal, typeOperation, utilisateur);
		// traiter le brouillard !
		traiterBrouillard(ed, nouvelleEcriture, bordereau.bordereauBrouillards());
		// mise a jour du bordereau
		bordereau.setBorEtat(EOBordereau.BordereauVise);
		return nouvelleEcriture;
	}


	private final void traiterBrouillard(EOEditingContext ed, EOEcriture nouvelleEcriture, NSArray leBrouillard) throws Exception {
		FactoryEcritureDetail maFactoryEcritureDetail = new FactoryEcritureDetail(withLogs());

		System.out.println("FactoryProcessBordereauBrouillardGenerique.traiterBrouillard() ");
		System.out.println("nb Brouillards : " + leBrouillard.count());

		for (int i = 0; i < leBrouillard.count(); i++) {

			EOBordereauBrouillard element = (EOBordereauBrouillard) leBrouillard.objectAtIndex(i);
			if (BROUILLARD_TRAITE.equals(element.bobLibelle3())) {
				throw new Exception("IMPOSSIBLE DE RETRAITER UN BROUILLARD !!!!!!!");
			}

			// creation des details en fonction du brouillard.
			EOEcritureDetail ecd = maFactoryEcritureDetail.creerEcritureDetail(ed, element.bobLibelle2(), new Integer(i), element.bobLibelle1(), element.bobMontant(),//   ecdMontant,
					element.bobLibelle3(), element.bobMontant(), null,//String    ecdSecondaire,
					element.bobSens(),// ecdSens,
					nouvelleEcriture, nouvelleEcriture.exercice(), element.gestion(), element.planComptableVisa());

			System.out.println(element.bobLibelle1());
			System.out.println("nb echeancier Brouillard = " + element.echeancierBrouillards().count());

			if (element.echeancierBrouillards().count() > 0) {
				System.out.println(element.echeancierBrouillards().objectAtIndex(0));
			}

			traiterBrouillardEcheancier(element, ecd);
			traiterBrouillardEcheancierSEPA(element, ecd);

			element.setBobLibelle3(BROUILLARD_TRAITE);
		}

	}

	/**
	 * Permet de memoriser le lien entre un echeancier et son ecriture de prise en charge. Cette ecriture peut ensuite etre recuperee pour realiser
	 * des emargements.
	 * 
	 * @param element
	 * @param ecd
	 * @throws Exception
	 */
	private void traiterBrouillardEcheancier(EOBordereauBrouillard element, EOEcritureDetail ecd) throws Exception {
		System.out.println("FactoryProcessBordereauBrouillardGenerique.traiterBrouillardEcheancier()");
		System.out.println("nb echeancier brouillard = " + element.echeancierBrouillards().count());

		if (element.echeancierBrouillards().count() > 0) {

			System.out.println("nb echeancier brouillard ok = " + element.echeancierBrouillards().count());

			FactoryEcheancierDetailEcriture factoryEcheancierDetailEcriture = new FactoryEcheancierDetailEcriture(withLogs());
			for (int j = 0; j < element.echeancierBrouillards().count(); j++) {
				EOEcheancierBrouillard eb = (EOEcheancierBrouillard) element.echeancierBrouillards().objectAtIndex(j);
				EOEcheancierDetailEcr echeancierDetailEcriture = factoryEcheancierDetailEcriture.creerEcheancierDetailEcriture(ecd.editingContext(), ecd, eb.echeancier());

				System.out.println("echeancierDetailEcriture = " + echeancierDetailEcriture.ecritureDetail().ecdLibelle());

				//On valide l'echeancier
				EcheancierHelper.getSharedInstance().validerEcheancier(eb.echeancier());
				//eb.echeancier().validerEcheancier();

				System.out.println("echeancier validé");
			}
		}
	}

	private void traiterBrouillardEcheancierSEPA(EOBordereauBrouillard element, EOEcritureDetail ecd) throws Exception {
		if (element.toSepaSddEcheancierBobs().count() > 0) {
			System.out.println("nb echeancier sepa brouillard ok = " + element.toSepaSddEcheancierBobs().count());
			for (int j = 0; j < element.toSepaSddEcheancierBobs().count(); j++) {
				EOSepaSddEcheancierBob eb = (EOSepaSddEcheancierBob) element.toSepaSddEcheancierBobs().objectAtIndex(j);
				ISepaSddMandat mandat = eb.toSepaSddEcheancier().toSepaSddMandat();
				try {
					//verifier que fournisseur et rib sont valides
					if (!GrhumRibHelper.getSharedInstance().isRibValide(mandat.toDebiteurRib())) {
						throw new Exception("Rib marqué comme non valide. " + GrhumRibHelper.getSharedInstance().bicEtIban(mandat.toDebiteurRib()));
					}
					if (!GrhumRibHelper.getSharedInstance().isRibValide(mandat.toDebiteurRib())) {
						throw new Exception("Rib incomplet. " + GrhumRibHelper.getSharedInstance().bicEtIban(mandat.toDebiteurRib()));
					}
					if (!GrhumFournisHelper.getSharedInstance().isValide(mandat.toDebiteurRib().toFournis())) {
						throw new Exception("Fournisseur non valide. code =" + mandat.toDebiteurRib().toFournis().fouCode());
					}

					SepaSddMandatHelper.getSharedInstance().validerMandat(ecd.editingContext(), mandat);
					SepaSddEcheancierRule.getSharedInstance().validateSepaSddEcheancier(eb.toSepaSddEcheancier());
					EOSepaSddEcheancierEcd sepaSddEcheancierEcd = EOSepaSddEcheancierEcd.createSepaSddEcheancierEcd(ecd.editingContext(), ecd, eb.toSepaSddEcheancier());
				} catch (Exception e) {
					throw new Exception("Mandat : " + SepaSddMandatHelper.getSharedInstance().getLibelleMandat(mandat) + " " + e.getMessage(), e);
				}
			}
		}
	}



	/**
	 * permet de rejeter un bordereau
	 * 
	 * @param ed
	 * @param bordereau
	 * @throws BordereauRejetException
	 */
	public void rejeterUnBordereau(EOEditingContext ed, EOBordereau bordereau) throws BordereauRejetException {
		if (EOBordereau.BordereauAnnule.equals(bordereau.borEtat())) {
			throw new BordereauRejetException(BordereauRejetException.bordereauDejaRejete);
		}
		bordereau.setBorEtat(EOBordereau.BordereauAnnule);

	}

	/**
	 * Verifie que les brouillards ne sont pas fait sur un compte non autorise.
	 * 
	 * @throws DataCheckException
	 */
	public void verifierComptesBrouillards(EOBordereau bordereau, NSArray plancoExerAutorises) throws DataCheckException {
		NSArray pcoNumAutorises = (NSArray) plancoExerAutorises.valueForKey(EOPlanComptableExer.PCO_NUM_KEY);

		NSArray lesBrouillards = bordereau.bordereauBrouillards();
		for (int i = 0; i < lesBrouillards.count(); i++) {
			EOBordereauBrouillard bob = (EOBordereauBrouillard) lesBrouillards.objectAtIndex(i);
			if (pcoNumAutorises.indexOfObject(bob.planComptableVisa().pcoNum()) == NSArray.NotFound) {
				throw new DataCheckException("Le compte " + bob.planComptableVisa().pcoNum() + " utilise dans un brouillard n'est pas valide sur l'exerice.");
			}
		}
	}
}
