/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory;

import org.cocktail.maracuja.client.exception.TitreDetailEcritureException;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */
public final class FactoryTitreDetailEcriture extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryTitreDetailEcriture(boolean withLog) {
		super(withLog);

	}

	public final EOTitreDetailEcriture creerTitreDetailEcriture(final EOEditingContext ed, final NSTimestamp tdeDate, final String tdeOrigine, final EOEcritureDetail ecritureDetail, final EOExercice exercice, final EOTitre titre, final EOOrigine origine, final EORecette recette) {

		final EOTitreDetailEcriture newEOTitreDetailEcriture = (EOTitreDetailEcriture) Factory.instanceForEntity(ed, EOTitreDetailEcriture.ENTITY_NAME);
		ed.insertObject(newEOTitreDetailEcriture);

		newEOTitreDetailEcriture.setTdeDate(tdeDate);
		newEOTitreDetailEcriture.setTdeOrigine(tdeOrigine);

		newEOTitreDetailEcriture.setEcritureDetailRelationship(ecritureDetail);
		newEOTitreDetailEcriture.setExerciceRelationship(exercice);
		newEOTitreDetailEcriture.setTitreRelationship(titre);
		newEOTitreDetailEcriture.setOrigineRelationship(origine);
		newEOTitreDetailEcriture.setRecetteRelationship(recette);

		try {
			newEOTitreDetailEcriture.validateObjectMetier();
			return newEOTitreDetailEcriture;
		} catch (TitreDetailEcritureException e) {
			e.printStackTrace();
			System.out.println(newEOTitreDetailEcriture);
			return null;
		}

	}

}
