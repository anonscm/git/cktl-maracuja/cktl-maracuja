/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddEcheance;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPrelevement;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.metier.EOTypeRecouvrement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class FactoryRecouvrement extends Factory {

	public FactoryRecouvrement(boolean withLog) {
		super(withLog);
	}

	public EORecouvrement creerRecouvrement(EOEditingContext ed, EOComptabilite comptabilite, EOExercice exercice, EOUtilisateur utilisateur, EOTypeRecouvrement typeRecouvrement, NSTimestamp creation, BigDecimal montant, Integer nbVirements, Integer numero) {
		final EORecouvrement monRecouvrement = (EORecouvrement) Factory.instanceForEntity(ed, EORecouvrement.ENTITY_NAME);
		ed.insertObject(monRecouvrement);

		monRecouvrement.setRecoDateCreation(creation);
		monRecouvrement.setRecoMontant(montant);
		monRecouvrement.setRecoNb(nbVirements);
		monRecouvrement.setRecoNumero(numero);

		monRecouvrement.setComptabiliteRelationship(comptabilite);
		monRecouvrement.setExerciceRelationship(exercice);
		monRecouvrement.setUtilisateurRelationship(utilisateur);
		monRecouvrement.setTypeRecouvrementRelationship(typeRecouvrement);
		return monRecouvrement;
	}

	/**
	 * Associe l'objet recouvrement à tous les prelevements.
	 * 
	 * @param recouvrement
	 * @param prelevements
	 */
	public final void associerPrelevements(final EORecouvrement recouvrement, final NSArray prelevements) {
		for (int i = 0; i < prelevements.count(); i++) {
			final EOPrelevement element = (EOPrelevement) prelevements.objectAtIndex(i);
			element.setRecouvrementRelationship(recouvrement);
		}
	}

	/**
	 * Passe tous les prélèvements associés au recouvrement à l'état
	 * EOPrelevement.ETAT_PRELEVE ("PRELEVE");
	 * 
	 * @param recouvrement
	 */
	public final void fermerPrelevements(final EORecouvrement recouvrement) {
		final NSArray prelevements = recouvrement.prelevements();
		for (int i = 0; i < prelevements.count(); i++) {
			final EOPrelevement element = (EOPrelevement) prelevements.objectAtIndex(i);
			element.setPrelevEtatMaracuja(EOPrelevement.ETAT_PRELEVE);
		}
	}

	/**
	 * Passe les prelevements a l'etat CONFIRME
	 * 
	 * @param prelevements
	 */
	public final void confirmerPrelevements(NSArray prelevements) {
		for (int i = 0; i < prelevements.count(); i++) {
			final EOPrelevement element = (EOPrelevement) prelevements.objectAtIndex(i);
			element.setPrelevEtatMaracuja(EOPrelevement.ETAT_CONFIRME);
		}
	}

	/**
	 * Passe les prelevements a l'etat REJETE
	 * 
	 * @param prelevements
	 */
	public final void rejeterPrelevements(NSArray prelevements) {
		for (int i = 0; i < prelevements.count(); i++) {
			final EOPrelevement element = (EOPrelevement) prelevements.objectAtIndex(i);
			element.setPrelevEtatMaracuja(EOPrelevement.ETAT_REJETE);
		}
	}

	public final void supprimerPrelevements(NSArray prelevements) {
		for (int i = 0; i < prelevements.count(); i++) {
			final EOPrelevement element = (EOPrelevement) prelevements.objectAtIndex(i);
			element.setPrelevEtatMaracuja(EOPrelevement.ETAT_SUPPRIME);
		}
	}


}
