/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 13 juil. 2004
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory;

import org.cocktail.maracuja.client.exception.BordereauRejetException;
import org.cocktail.maracuja.client.exception.EcritureException;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 */
public class FactoryBordereauRejet extends Factory {

	public FactoryBordereauRejet(boolean withLog) {
		super(withLog);
	}

	public EOBordereauRejet creerBordereauRejet(EOEditingContext ed, String brjEtat, Integer brjNum, EOExercice exercice, EOGestion gestion, EOTypeBordereau typBordereau, EOUtilisateur utilisateur) throws BordereauRejetException {

		EOBordereauRejet newEObordereauRejet = (EOBordereauRejet) Factory.instanceForEntity(ed, EOBordereauRejet.ENTITY_NAME);

		newEObordereauRejet.setBrjEtat(brjEtat);
		newEObordereauRejet.setBrjNum(brjNum);

		ed.insertObject(newEObordereauRejet);

		newEObordereauRejet.setExerciceRelationship(exercice);
		newEObordereauRejet.setGestionRelationship(gestion);
		newEObordereauRejet.setTypeBordereauRelationship(typBordereau);
		newEObordereauRejet.setUtilisateurRelationship(utilisateur);

		//		newEObordereauRejet.addObjectToBothSidesOfRelationshipWithKey(exercice, "exercice");
		//		newEObordereauRejet.addObjectToBothSidesOfRelationshipWithKey(gestion, "gestion");
		//		newEObordereauRejet.addObjectToBothSidesOfRelationshipWithKey(typBordereau, "typeBordereau");
		//		newEObordereauRejet.addObjectToBothSidesOfRelationshipWithKey(utilisateur, "utilisateur");

		try {
			newEObordereauRejet.validateObjectMetier();
			return newEObordereauRejet;
		} catch (BordereauRejetException e1) {
			e1.printStackTrace();
			System.out.println(newEObordereauRejet);
			return null;
		}

	}

	public void numeroterBordereauRejet(EOBordereauRejet bordereauRejet, FactoryNumerotation delegateNumerotation) throws BordereauRejetException {
		if (delegateNumerotation == null) {
			throw new BordereauRejetException("PROBLEME : delegateNumerotation" + BordereauRejetException.objetNull);
		}
		if (bordereauRejet.brjNum().intValue() != 0 && bordereauRejet.brjNum() != null) {
			throw new EcritureException(EcritureException.ecritureDejaNumerote);
		}

	}
}
