/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 14 juil. 2004
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.factory;

import org.cocktail.maracuja.client.metier.EOEcheancier;
import org.cocktail.maracuja.client.metier.EOEcheancierDetailEcr;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;

import com.webobjects.eocontrol.EOEditingContext;


/**
 *  
 */
public class FactoryEcheancierDetailEcriture extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryEcheancierDetailEcriture(boolean withLog) {
		super(withLog);
	}

	/**
	 * Permet de memoriser le lien entre un echeancier et les ecritures liées.
	 * 
	 * @param ed
	 * @param ecritureDetail
	 * @param echeancier
	 * @return
	 */
	public EOEcheancierDetailEcr creerEcheancierDetailEcriture(EOEditingContext ed, EOEcritureDetail ecritureDetail, EOEcheancier echeancier) {

		System.out.println("FactoryEcheancierDetailEcriture.creerEcheancierDetailEcriture()");

		EOEcheancierDetailEcr newObj = (EOEcheancierDetailEcr) Factory.instanceForEntity(ed, EOEcheancierDetailEcr.ENTITY_NAME);
		ed.insertObject(newObj);

		newObj.setEcheancierRelationship(echeancier);
		newObj.setEcritureDetailRelationship(ecritureDetail);

		//        newObj.addObjectToBothSidesOfRelationshipWithKey(echeancier, EOEcheancierDetailEcr.ECHEANCIER_KEY);
		//        newObj.addObjectToBothSidesOfRelationshipWithKey(ecritureDetail, EOEcheancierDetailEcr.ECRITURE_DETAIL_KEY);
		return newObj;
		//		try {
		//			newObj.validateObjectMetier();
		//			return newObj;
		//		} catch (MandatDetailEcritureException e) {
		//			e.printStackTrace();
		//			System.out.println(newObj);
		//			throw e;
		//		}

	}

}
