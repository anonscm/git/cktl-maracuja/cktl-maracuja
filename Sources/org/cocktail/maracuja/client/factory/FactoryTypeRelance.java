/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * @author RIVALLAND FREDERIC <br> UAG <br> CRI Guadeloupe
 */
package org.cocktail.maracuja.client.factory;

import org.cocktail.maracuja.client.metier.EOTypeRelance;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * @author frivalla To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class FactoryTypeRelance extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryTypeRelance(boolean withLog) {
		super(withLog);
	}

	/**
	 * @param ed
	 * @param trlDelaiPaiement
	 * @param trlEtat
	 *            Non utilisé, les types créés sont toujours valides
	 * @param trlLibelle1
	 * @param trlLibelle2
	 * @param trlLibelle3
	 * @param trlNiveau
	 * @param trlNom
	 * @param trlReportId
	 * @param trlSign
	 * @param trlContactSuivi
	 * @return
	 */
	public EOTypeRelance ajouterTypeRelance(
			EOEditingContext ed,
			Integer trlDelaiPaiement,
			String trlEtat,
			String trlLibelle1,
			String trlLibelle2,
			String trlLibelle3,
			Integer trlNiveau,
			String trlNom,
			String trlReportId,
			String trlSign,
			String trlContactSuivi) {

		EOTypeRelance monEOTypeRelance = (EOTypeRelance) Factory.instanceForEntity(ed, EOTypeRelance.ENTITY_NAME);
		ed.insertObject(monEOTypeRelance);

		monEOTypeRelance.setTrlDelaiPaiement(trlDelaiPaiement);
		monEOTypeRelance.setTrlEtat(EOTypeRelance.ETAT_VALIDE);
		monEOTypeRelance.setTrlLibelle1(trlLibelle1);
		monEOTypeRelance.setTrlLibelle2(trlLibelle2);
		monEOTypeRelance.setTrlLibelle3(trlLibelle3);
		monEOTypeRelance.setTrlNiveau(trlNiveau);
		monEOTypeRelance.setTrlNom(trlNom);
		monEOTypeRelance.setTrlReportId(trlReportId);
		monEOTypeRelance.setTrlSign(trlSign);
		monEOTypeRelance.setTrlContactSuivi(trlContactSuivi);

		return monEOTypeRelance;
	}

	/**
	 * @param ed
	 * @param maRelance
	 * @param trlDelaiPaiement
	 * @param trlEtat
	 *            Non utilisé
	 * @param trlLibelle1
	 * @param trlLibelle2
	 * @param trlLibelle3
	 * @param trlNiveau
	 * @param trlNom
	 * @param trlReportId
	 * @param trlSign
	 * @param trlContactSuivi
	 */
	public void modifierTypeRelance(EOEditingContext ed,
			EOTypeRelance maRelance,
			Integer trlDelaiPaiement,
			String trlEtat,
			String trlLibelle1,
			String trlLibelle2,
			String trlLibelle3,
			Integer trlNiveau,
			String trlNom,
			String trlReportId,
			String trlSign,
			String trlContactSuivi) {
		maRelance.setTrlDelaiPaiement(trlDelaiPaiement);
		//        maRelance.setTrlEtat(EOTypeRelance.ETAT_VALIDE);
		maRelance.setTrlLibelle1(trlLibelle1);
		maRelance.setTrlLibelle2(trlLibelle2);
		maRelance.setTrlLibelle3(trlLibelle3);
		maRelance.setTrlNiveau(trlNiveau);
		maRelance.setTrlNom(trlNom);
		maRelance.setTrlReportId(trlReportId);
		maRelance.setTrlSign(trlSign);
		maRelance.setTrlContactSuivi(trlContactSuivi);
	}

	public void annulerTypeRelance(EOEditingContext ed, EOTypeRelance maRelance) {
		maRelance.setTrlEtat(EOTypeRelance.ETAT_ANNULE);
	}

}
