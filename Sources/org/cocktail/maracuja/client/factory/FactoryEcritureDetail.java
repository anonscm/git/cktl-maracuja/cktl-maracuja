/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.factory;

import java.math.BigDecimal;

import org.cocktail.maracuja.client.exception.EcritureDetailException;
import org.cocktail.maracuja.client.exception.EcritureException;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 * @author rprin - Cocktail
 */
public class FactoryEcritureDetail extends Factory {

	/**
	 * @param withLog
	 */
	public FactoryEcritureDetail(boolean withLog) {
		super(withLog);

	}

	/**
	 * Permet de creer un ecriture detail
	 * 
	 * @param ed Editing Context de travail
	 * @param ecdCommentaire un commentaire
	 * @param ecdIndex l index de saisie utilisateur
	 * @param ecdLibelle libelle du detail ecriture
	 * @param ecdMontant montant du detail ecriture
	 * @param ecdPostit commentaire interne utilisateur
	 * @param ecdResteEmarger reste a emarger (=montant a la creation 99%)
	 * @param ecdSecondaire est un ecriture detail d une comptabilite secondaire
	 * @param ecdSens sens Credit ou Debit
	 * @param ecriture Ecriture du detail
	 * @param exercice exercice
	 * @param gestion code gestion du detail
	 * @param planComptable compte d imputation du detail
	 * @return nouveauEcritureDetail
	 * @throws EcritureDetailException
	 */
	public EOEcritureDetail creerEcritureDetail(EOEditingContext ed, String ecdCommentaire, Integer ecdIndex, String ecdLibelle, BigDecimal ecdMontant, String ecdPostit, BigDecimal ecdResteEmarger, String ecdSecondaire, String ecdSens, EOEcriture ecriture, EOExercice exercice, EOGestion gestion,
			EOPlanComptable planComptable) throws EcritureDetailException {

		EOEcritureDetail newEOEcritureDetail = ecritureDetailVideCreer(ed);

		newEOEcritureDetail.setEcdCommentaire(ecdCommentaire);
		newEOEcritureDetail.setEcdSens(ecdSens.toUpperCase());

		if (ecdSens.equals(SENS_CREDIT)) {
			newEOEcritureDetail.setEcdCredit(ecdMontant);
			newEOEcritureDetail.setEcdDebit(BIG_DECIMAL_0);
		}
		else {
			newEOEcritureDetail.setEcdCredit(BIG_DECIMAL_0);
			newEOEcritureDetail.setEcdDebit(ecdMontant);
		}

		if (ecdLibelle == null || ecdLibelle.trim().length() == 0) {
			ecdLibelle = ecriture.ecrLibelle();
		}

		newEOEcritureDetail.setEcdIndex(ecdIndex);
		newEOEcritureDetail.setEcdLibelle(ecdLibelle);
		newEOEcritureDetail.setEcdMontant(ecdMontant);
		newEOEcritureDetail.setEcdPostit(ecdPostit);
		newEOEcritureDetail.setEcdResteEmarger(ecdResteEmarger.abs());
		newEOEcritureDetail.setEcdSecondaire(ecdSecondaire);

		newEOEcritureDetail.setEcritureRelationship(ecriture);
		newEOEcritureDetail.setExerciceRelationship(exercice);
		newEOEcritureDetail.setGestionRelationship(gestion);
		newEOEcritureDetail.setPlanComptableRelationship(planComptable);

		try {
			newEOEcritureDetail.validateObjectMetier();
			return newEOEcritureDetail;
		} catch (EcritureDetailException e) {
			e.printStackTrace();
			System.out.println(newEOEcritureDetail);
			return null;
		}

	}

	/**
	 * Permet de creer un ecritureDetail Vierge <br>
	 * Utiliser les methodes ecritureDetailVideSet* pour le reste des proprietes de l objet <br>
	 * puis utiliser les methodes de la Class EOEcritureDetail
	 * 
	 * @param ed Editing Context de travail
	 * @return nouveau ecritureDetail
	 * @throws EcritureDetailException
	 */
	public EOEcritureDetail ecritureDetailVideCreer(EOEditingContext ed) throws EcritureDetailException {
		EOEcritureDetail newEOEcritureDetail = (EOEcritureDetail) Factory.instanceForEntity(ed, EOEcritureDetail.ENTITY_NAME);
		ed.insertObject(newEOEcritureDetail);
		return newEOEcritureDetail;

	}

	/**
	 * Pertmet de positionner Montant d un detail ecriture (obligation de connaitre le ecdSens)
	 * 
	 * @param newEOEcritureDetail le detail ecriture de travail
	 * @param ecdMontant Montant a attribuer
	 */
	public void ecritureDetailVideSetMontant(EOEcritureDetail newEOEcritureDetail, BigDecimal ecdMontant) {
		if (newEOEcritureDetail.ecdSens() == null) {
			throw new EcritureException(EcritureException.ecritureSensNonRenseigne);
		}

		newEOEcritureDetail.setEcdMontant(ecdMontant);
		newEOEcritureDetail.setEcdResteEmarger(ecdMontant.abs());
		if (newEOEcritureDetail.ecdSens().equals(SENS_CREDIT)) {
			newEOEcritureDetail.setEcdCredit(ecdMontant);
			newEOEcritureDetail.setEcdDebit(BIG_DECIMAL_0);
		}
		else {
			newEOEcritureDetail.setEcdCredit(BIG_DECIMAL_0);
			newEOEcritureDetail.setEcdDebit(ecdMontant);
		}
		//        if (newEOEcritureDetail.ecdSens().equalsIgnoreCase(
		//                sensCredit()))
		//            newEOEcritureDetail.setEcdCredit(ecdMontant);
		//        else
		//            newEOEcritureDetail.setEcdCredit(new BigDecimal(0));
		//
		//        if (newEOEcritureDetail.ecdSens().equalsIgnoreCase(
		//                sensDebit()))
		//            newEOEcritureDetail.setEcdDebit(ecdMontant);
		//        else
		//            newEOEcritureDetail.setEcdDebit(new BigDecimal(0));
	}

	/**
	 * Pertmet de positionner l exercice d un detail ecriture
	 * 
	 * @param newEOEcritureDetail le detail ecriture de travail
	 * @param exercice exercice a attribuer
	 */
	public void ecritureDetailVideSetExercice(EOEcritureDetail newEOEcritureDetail, EOExercice exercice) {
		newEOEcritureDetail.setExerciceRelationship(exercice);
	}

	/**
	 * Pertmet de positionner le code gestion d un detail ecriture
	 * 
	 * @param newEOEcritureDetail le detail ecriture de travail
	 * @param gestion le code gestion a attribuer
	 */
	public void ecritureDetailVideSetGestion(EOEcritureDetail newEOEcritureDetail, EOGestion gestion) {
		newEOEcritureDetail.setGestionRelationship(gestion);
	}

	/**
	 * Pertmet de positionner le plan comptable d un detail ecriture
	 * 
	 * @param newEOEcritureDetail le detail ecriture de travail
	 * @param planComptable le plan comptable attribuer
	 */
	public void ecritureDetailVideSetPlanComptable(EOEcritureDetail newEOEcritureDetail, EOPlanComptable planComptable) {
		newEOEcritureDetail.setPlanComptableRelationship(planComptable);

	}

	//    /**
	//     * Permet d attribuer les details a une ecriture
	//     * 
	//     * @param ed
	//     *            editingContext de travail
	//     * @param ecriture
	//     *            ecriture des details
	//     * @param lesEcrituresDetails
	//     *            les details de l ecriture DEBIT = CREDIT
	//     * @throws EcritureException
	//     */
	//    public void attribuerDesEcritureDetail(
	//            EOEditingContext ed,
	//            EOEcriture ecriture,
	//            NSArray lesEcrituresDetailsDebit,
	//            NSArray lesEcrituresDetailsCredit) throws EcritureException {
	//        
	//
	//        //debit = credit ???
	//        //		verifier que le montant des lesEcrituresDetailsDebit =
	//        // lesEcrituresDetailsCredit
	//        /*if ((this.computeSumForKey(
	//                lesEcrituresDetailsCredit,
	//                "ecdMont")) == (this.computeSumForKey(
	//                lesEcrituresDetailsDebit,
	//                "ecdMont")))
	//            */
	//        if (Factory.different((this.computeSumForKeyBigDecimal(lesEcrituresDetailsCredit,"ecdMont")), (this.computeSumForKeyBigDecimal(lesEcrituresDetailsDebit,"ecdMont")))
	//                )
	//            throw new EcritureException(EcritureException.ecritureNonEquilibre);
	//
	//        // on fait les liens avec les details ecritures DEBIT
	//        int i = 0;
	//        while (lesEcrituresDetailsDebit.count() > i) {
	//            this.attribuerUnEcritureDetail(
	//                    ed,
	//                    ecriture,
	//                    (EOEcritureDetail) lesEcrituresDetailsDebit
	//                            .objectAtIndex(i));
	//            i++;
	//        }
	//
	//        //		on fait les liens avec les details ecritures DEBIT
	//        i = 0;
	//        while (lesEcrituresDetailsCredit.count() > i) {
	//            this.attribuerUnEcritureDetail(
	//                    ed,
	//                    ecriture,
	//                    (EOEcritureDetail) lesEcrituresDetailsCredit
	//                            .objectAtIndex(i));
	//            i++;
	//        }
	//
	//    }

	/**
	 * Permet d attribuer un detail ecriture a une ecriture
	 * 
	 * @param ed Editing context de trvail
	 * @param ecriture Ecriture du detail ecriture
	 * @param unEcrituresDetails detail ecriture a relier
	 * @throws EcritureDetailException
	 */
	public void attribuerUnEcritureDetail(EOEditingContext ed, EOEcriture ecriture, EOEcritureDetail unEcrituresDetails) throws EcritureDetailException {
		if (ecriture == null) {
			throw new EcritureDetailException("ECRITURE  " + EcritureDetailException.objetNull);
		}
		if (unEcrituresDetails == null) {
			throw new EcritureDetailException("ECRITURE DETAIL  " + EcritureDetailException.objetNull);
		}
		unEcrituresDetails.setEcritureRelationship(ecriture);

	}

	/**
	 * Permet de supprimer un detail ecriture tant que celui ci n est pas dans la base.
	 * 
	 * @param ed editing context de travail
	 * @param EcritureDetail ecritureDetail a supprimer
	 * @throws EcritureDetailException
	 */
	public void supprimerEcritureDetail(EOEditingContext ed, EOEcritureDetail EcritureDetail) throws EcritureDetailException {
		//Si l'ecriture est enregistree
		if (ed.insertedObjects().indexOfObject(EcritureDetail.ecriture()) == NSArray.NotFound) {
			// l ecriture est elle valide ?
			if (EcritureDetail.ecriture().ecrEtat().equals(EOEcriture.ecritureValide)) {
				throw new EcritureDetailException(EcritureDetailException.supprimerDetailEcritureValide);
			}
		}
		EcritureDetail.setEcritureRelationship(null);
		ed.deleteObject(EcritureDetail);
	}

	/**
	 * Permet de verifier l EcritureDetail avant saveChanges.
	 * 
	 * @param newEOEcritureDetail
	 */
	public void verifierEcritureDetail(EOEcritureDetail newEOEcritureDetail) {
		newEOEcritureDetail.validateObjectMetier();
	}

	public void ecritureDetailVideSetEcrDetailForEmargement(EOEcritureDetail newEOEcritureDetail, EOEcritureDetail ecdForEmarge) {
		newEOEcritureDetail.setEcdForEmargement(ecdForEmarge);
	}

	public void ecritureDetailVideSetContratConvention(EOEcritureDetail newEOEcritureDetail, EOAccordsContrat convention) {
		newEOEcritureDetail.setToAccordsContratRelationship(convention);

	}
}
