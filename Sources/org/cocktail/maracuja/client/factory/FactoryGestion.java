/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.factory;

import org.cocktail.maracuja.client.exception.FactoryException;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOGestionExercice;

import com.webobjects.eocontrol.EOEditingContext;


public class FactoryGestion extends Factory {

	public FactoryGestion(boolean withLog) {
		super(withLog);
	}

	public final EOGestion creerUnGestion(final EOEditingContext ec, final EOComptabilite comptabilite, final String gescode) {
		final EOGestion gestion = (EOGestion) Factory.instanceForEntity(ec, EOGestion.ENTITY_NAME);
		ec.insertObject(gestion);
		gestion.setGesCode(gescode);
		gestion.setGesOrdre(gescode);
		gestion.setComptabiliteRelationship(comptabilite);
		//        gestion.addObjectToBothSidesOfRelationshipWithKey(comptabilite, "comptabilite");
		return gestion;
	}

	/**
	 * Crée un objet EOGestionExercice si celui-ci n'est pas déjà présent dans
	 * l'editingContext.
	 * 
	 * @param ec
	 * @param gestion
	 * @param exercice
	 * @param exeOrdre
	 * @return
	 */
	public final EOGestionExercice creerUnGestionExercice(final EOEditingContext ec, final EOGestion gestion, final EOExercice exercice, final Integer exeOrdre) throws Exception {
		//Vérifier s'il existe déjà
		for (int i = 0; i < gestion.gestionExercices().count(); i++) {
			final EOGestionExercice element = (EOGestionExercice) gestion.gestionExercices().objectAtIndex(i);
			if (exercice.equals(element.exercice())) {
				throw new FactoryException("Un objet GestionExercice existe deja pour l'exercice et le code gestion sélectionné");
			}
		}
		final EOGestionExercice gestionExercice = (EOGestionExercice) Factory.instanceForEntity(ec, EOGestionExercice.ENTITY_NAME);
		ec.insertObject(gestionExercice);
		gestionExercice.setExerciceRelationship(exercice);
		gestionExercice.setGestionRelationship(gestion);
		//		gestionExercice.addObjectToBothSidesOfRelationshipWithKey(exercice, "exercice");
		//		gestionExercice.addObjectToBothSidesOfRelationshipWithKey(gestion, "gestion");
		gestionExercice.setGesCode(gestion.gesCode());
		gestionExercice.setExeOrdre(exeOrdre);
		return gestionExercice;
	}

	public final void supprimerGestionExercice(final EOEditingContext editingContext, final EOGestionExercice gestionExercice) {
		gestionExercice.gestion().removeFromGestionExercicesRelationship(gestionExercice);
		//		gestionExercice.gestion().removeObjectFromBothSidesOfRelationshipWithKey(gestionExercice, "gestionExercices");
		editingContext.deleteObject(gestionExercice);
	}

}
