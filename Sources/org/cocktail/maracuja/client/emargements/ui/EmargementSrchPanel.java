/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.EcritureDetailListPanel;
import org.cocktail.maracuja.client.common.ui.EmargementListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


/**
 * Panneau principal de l'annulation des émargements.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementSrchPanel extends ZKarukeraPanel {
	private IEmargementSrchPanelListener myListener;
	private EmargementSrchFilterPanel myEmargementFilterPanel;
	private EmargementListPanel emargementListPanel;
	private CreditListPanel creditListPanel;
	private DebitListPanel debitListPanel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public EmargementSrchPanel(IEmargementSrchPanelListener listener) {
		super();
		myListener = listener;

		emargementListPanel = new EmargementListPanel(myListener.getEmargementListListener());
		myEmargementFilterPanel = new EmargementSrchFilterPanel(myListener.getEmargementFilterPanelListener());
		creditListPanel = new CreditListPanel(myListener.getCreditsListListener());
		debitListPanel = new DebitListPanel(myListener.getDebitsListListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		this.setLayout(new BorderLayout());
		myEmargementFilterPanel.initGUI();
		emargementListPanel.initGUI();
		creditListPanel.initGUI();
		debitListPanel.initGUI();

		JPanel debits = encloseInPanelWithTitle("Débits", null, ZConst.BG_COLOR_TITLE, debitListPanel, null, null);
		JPanel credits = encloseInPanelWithTitle("Crédits", null, ZConst.BG_COLOR_TITLE, creditListPanel, null, null);

		JPanel p = new JPanel(new BorderLayout());
		p.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, myEmargementFilterPanel, null, null), BorderLayout.NORTH);
		p.add(encloseInPanelWithTitle("Emargements", null, ZConst.BG_COLOR_TITLE, buildVerticalSplitPane(emargementListPanel, buildVerticalSplitPane(debits, credits)), null, null), BorderLayout.CENTER);

		add(p, BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);
		add(buildRightPanel(), BorderLayout.EAST);

	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(80, 8, 8, 8));
		ArrayList list = new ArrayList();
		list.add(myListener.actionNew());
		list.add(myListener.actionAnnuler());
		list.add(myListener.actionImprimer());

		ArrayList comps = getButtonListFromActionList(list);
		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionFermer());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraPanel.buildHorizontalButtonsFromActions(a), BorderLayout.EAST);
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		emargementListPanel.updateData();
	}

	public interface IEmargementSrchPanelListener {
		public ZKarukeraTablePanel.IZKarukeraTablePanelListener getEmargementListListener();

		public Action actionFermer();

		public Action actionImprimer();

		public Action actionAnnuler();

		public Action actionNew();

		public ZKarukeraTablePanel.IZKarukeraTablePanelListener getCreditsListListener();

		public ZKarukeraTablePanel.IZKarukeraTablePanelListener getDebitsListListener();

		public EmargementSrchFilterPanel.IEmargementFilterPanelListener getEmargementFilterPanelListener();
	}

	public final class CreditListPanel extends EcritureDetailListPanel {
		public static final String COL_ECRNUMERO = "ecriture.ecrNumero";

		/**
		 * @param listener
		 */
		public CreditListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
			ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup, COL_ECRNUMERO, "N° Ecriture", 68);
			ecrNumero.setAlignment(SwingConstants.LEFT);

			LinkedHashMap colsNew = new LinkedHashMap();
			colsNew.put(COL_ECRNUMERO, ecrNumero);
			colsNew.put(COL_ECDINDEX, colsMap.get(COL_ECDINDEX));
			colsNew.put(COL_GESCODE, colsMap.get(COL_GESCODE));
			colsNew.put(COL_PCONUM, colsMap.get(COL_PCONUM));
			colsNew.put(COL_PCOLIBELLE, colsMap.get(COL_PCOLIBELLE));
			colsNew.put(COL_ECDLIBELLE, colsMap.get(COL_ECDLIBELLE));
			//    		colsNew.put(COL_ECDDEBIT , colsMap.get(COL_ECDDEBIT));
			colsNew.put(COL_ECDCREDIT, colsMap.get(COL_ECDCREDIT));
			colsNew.put(COL_ECDRESTEEMARGER, colsMap.get(COL_ECDRESTEEMARGER));

			colsMap = colsNew;
		}

	}

	public final class DebitListPanel extends EcritureDetailListPanel {
		public static final String COL_ECRNUMERO = "ecriture.ecrNumero";

		/**
		 * @param listener
		 */
		public DebitListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup, COL_ECRNUMERO, "N° Ecriture", 68);
			ecrNumero.setAlignment(SwingConstants.LEFT);

			LinkedHashMap colsNew = new LinkedHashMap();
			colsNew.put(COL_ECRNUMERO, ecrNumero);
			colsNew.put(COL_ECDINDEX, colsMap.get(COL_ECDINDEX));
			colsNew.put(COL_GESCODE, colsMap.get(COL_GESCODE));
			colsNew.put(COL_PCONUM, colsMap.get(COL_PCONUM));
			colsNew.put(COL_PCOLIBELLE, colsMap.get(COL_PCOLIBELLE));
			colsNew.put(COL_ECDLIBELLE, colsMap.get(COL_ECDLIBELLE));
			colsNew.put(COL_ECDDEBIT, colsMap.get(COL_ECDDEBIT));
			//    		colsNew.put(COL_ECDCREDIT , colsMap.get(COL_ECDCREDIT));
			colsNew.put(COL_ECDRESTEEMARGER, colsMap.get(COL_ECDRESTEEMARGER));

			colsMap = colsNew;

		}

	}

	public final EmargementListPanel getEmargementListPanel() {
		return emargementListPanel;
	}

	public final CreditListPanel getCreditListPanel() {
		return creditListPanel;
	}

	public final DebitListPanel getDebitListPanel() {
		return debitListPanel;
	}
}
