/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class EmargementSaisieDetailPanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener, TableModelListener {
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected EODisplayGroup myDisplayGroup;
	protected TableSorter myTableSorter;
	protected Vector myCols;
	protected IEmargementDetailPanelListener myListener;
	private FiltreDetailPanel myFiltreDetailPanel;

	/**
	 * @param editingContext
	 */
	public EmargementSaisieDetailPanel(IEmargementDetailPanelListener listener) {
		super();
		myListener = listener;

	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.onSelectionChanged();
	}

	/**
     *
     */
	public void clearData() {
		myEOTable.clearData();

	}

	private void initTableModel() {
		myCols = new Vector(8, 0);

		final ZEOTableModelColumn colEcrNumero = new ZEOTableModelColumn(myDisplayGroup, "ecriture.ecrNumero", "N° écriture", 110);
		colEcrNumero.setAlignment(SwingConstants.LEFT);
		colEcrNumero.setColumnClass(Integer.class);

		final ZEOTableModelColumn colEcrDateSaisie = new ZEOTableModelColumn(myDisplayGroup, "ecriture.ecrDateSaisie", "Date", 90);
		colEcrDateSaisie.setAlignment(SwingConstants.CENTER);
		colEcrDateSaisie.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colEcrDateSaisie.setColumnClass(Date.class);

		ZEOTableModelColumn colEcdIndex = new ZEOTableModelColumn(myDisplayGroup, "ecdIndex", "#", 29);
		colEcdIndex.setAlignment(SwingConstants.LEFT);
		colEcdIndex.setColumnClass(Integer.class);

		final ZEOTableModelColumn colEcdGestionGesCode = new ZEOTableModelColumn(myDisplayGroup, "gestion.gesCode", "Code gestion", 90);
		colEcdGestionGesCode.setAlignment(SwingConstants.CENTER);

		final ZEOTableModelColumn colEcdLibelle = new ZEOTableModelColumn(myDisplayGroup, "ecdLibelle", "Lib. détail", 213);
		colEcdLibelle.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn colEcrLibelle = new ZEOTableModelColumn(myDisplayGroup, "ecriture.ecrLibelle", "Lib. ecriture", 213);
		colEcrLibelle.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn oriLibelle = new ZEOTableModelColumn(myDisplayGroup, "ecriture.origine.oriLibelle", "Origine", 100);
		oriLibelle.setAlignment(SwingConstants.LEFT);

		final ZEOTableModelColumn colEcdMontant = new ZEOTableModelColumn(myDisplayGroup, "ecdMontant", "Montant", 125);
		colEcdMontant.setAlignment(SwingConstants.RIGHT);
		colEcdMontant.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdMontant.setColumnClass(BigDecimal.class);

		final ZEOTableModelColumn colEcdResteAEmarger = new ZEOTableModelColumn(myDisplayGroup, "ecdResteEmarger", "Reste à émarger", 125);
		colEcdResteAEmarger.setAlignment(SwingConstants.RIGHT);
		colEcdResteAEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdResteAEmarger.setColumnClass(BigDecimal.class);

		myCols.add(colEcrNumero);
		myCols.add(colEcdIndex);
		myCols.add(colEcrDateSaisie);
		myCols.add(colEcdGestionGesCode);
		myCols.add(colEcrLibelle);
		myCols.add(colEcdLibelle);
		myCols.add(oriLibelle);
		myCols.add(colEcdMontant);
		myCols.add(colEcdResteAEmarger);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter(myTableModel);
		//		myTableSorter.activeSortOnColumn(0, false, false);

		myTableModel.addTableModelListener(this);
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}

	public void initGUI() {
		myDisplayGroup = new EODisplayGroup();
		//		myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "EcritureDetail"));
		//		((EODistributedDataSource)myDisplayGroup.dataSource()).setEditingContext(getEditingContext());

		initTableModel();
		initTable();

		myFiltreDetailPanel = new FiltreDetailPanel();
		myFiltreDetailPanel.initGUI();

		setLayout(new BorderLayout());
		add(new JScrollPane(myEOTable), BorderLayout.CENTER);
		add(myFiltreDetailPanel, BorderLayout.NORTH);

	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {

	}

	/**
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {

	}

	/**
	 * @return Un tableau de qualifiers créés à partir des choix de l'utilisateur.
	 * @throws Exception
	 */
	protected NSMutableArray buildFilterQualifiers() throws Exception {
		if (myListener.getExercice() == null) {
			throw new DefaultClientException("L'exercice ne doit pas être nul");
		}
		//        if (myListener.getComptabilite()==null) {
		//            throw new DefaultClientException("La comptabilité ne doit pas être nulle");
		//        }
		if (myListener.getPlanComptable() == null) {
			throw new DefaultClientException("L'imputation ne doit pas être nulle");
		}

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOEcritureDetail.EXERCICE_KEY + "=%@", new NSArray(myListener.getExercice())));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOEcritureDetail.ECRITURE_KEY + "." + EOEcriture.ECR_ETAT_KEY + "=%@", new NSArray(EOEcriture.ecritureValide)));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOEcritureDetail.ECRITURE_KEY + "." + EOEcriture.COMPTABILITE_KEY + "=%@", new NSArray(myListener.getComptabilite())));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOEcritureDetail.PLAN_COMPTABLE_KEY + "=%@", new NSArray(myListener.getPlanComptable())));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOEcritureDetail.ECD_RESTE_EMARGER_KEY + "!=0", null));

		//Construire les qualifiers à partir des saisies utilisateur

		///Numero
		final NSMutableArray qualsNum = new NSMutableArray();
		if (myListener.getMyFilterValues().get("ecrNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrNumero>=%@", new NSArray(new Integer(((Number) myListener.getMyFilterValues().get("ecrNumeroMin")).intValue()))));
		}
		if (myListener.getMyFilterValues().get("ecrNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrNumero<=%@", new NSArray(new Integer(((Number) myListener.getMyFilterValues().get("ecrNumeroMax")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		final NSMutableArray qualsGes = new NSMutableArray();
		if (myListener.getMyFilterValues().get(EOGestion.GES_CODE_KEY) != null) {
			qualsGes.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.gesCode=%@", new NSArray(myListener.getMyFilterValues().get(EOGestion.GES_CODE_KEY))));
		}
		if (qualsGes.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsGes));
		}

		if (myListener.getMyFilterValues().get("libelle") != null) {
			final String cond = "*" + myListener.getMyFilterValues().get("libelle") + "*";
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("ecdLibelle caseInsensitiveLike %@ or ecriture.ecrLibelle caseInsensitiveLike %@", new NSArray(new Object[] {
					cond, cond
			})));
		}

		if (myListener.getMyFilterValues().get("origine") != null) {
			final String cond = "*" + myListener.getMyFilterValues().get("origine") + "*";
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.origine.oriLibelle caseInsensitiveLike %@", new NSArray(new Object[] {
					cond
			})));
		}

		///Montant
		final NSMutableArray qualsMontant = new NSMutableArray();
		if (myListener.getMyFilterValues().get("ecdMontantMin") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("ecdMontant>=%@", new NSArray((Number) myListener.getMyFilterValues().get("ecdMontantMin"))));
		}
		if (myListener.getMyFilterValues().get("ecdMontantMax") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("ecdMontant<=%@", new NSArray((Number) myListener.getMyFilterValues().get("ecdMontantMax"))));
		}
		if (qualsMontant.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMontant));
		}

		///reste emarger
		final NSMutableArray qualsReste = new NSMutableArray();
		if (myListener.getMyFilterValues().get("ecdResteEmargerMin") != null) {
			qualsReste.addObject(EOQualifier.qualifierWithQualifierFormat("ecdResteEmarger>=%@", new NSArray((Number) myListener.getMyFilterValues().get("ecdResteEmargerMin"))));
		}
		if (myListener.getMyFilterValues().get("ecdResteEmargerMax") != null) {
			qualsReste.addObject(EOQualifier.qualifierWithQualifierFormat("ecdResteEmarger<=%@", new NSArray((Number) myListener.getMyFilterValues().get("ecdResteEmargerMax"))));
		}
		if (qualsReste.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsReste));
		}

		///Date
		final NSMutableArray qualsDate = new NSMutableArray();
		if (myListener.getMyFilterValues().get("ecrDateSaisieMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrDateSaisie>=%@", new NSArray(new NSTimestamp((Date) myListener.getMyFilterValues().get("ecrDateSaisieMin")))));
		}
		if (myListener.getMyFilterValues().get("ecrDateSaisieMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrDateSaisie<=%@", new NSArray(new NSTimestamp((Date) myListener.getMyFilterValues().get("ecrDateSaisieMax")))));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		//        //Dans le cas ou on connait le numero emargement
		//        if (myListener.getMyFilterValues().get("ecrDateSaisieMax")!=null) {
		//            qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrDateSaisie<=%@", new NSArray( new NSTimestamp((Date) myListener.getMyFilterValues().get("ecrDateSaisieMax") ))));
		//        }

		ZLogger.debug(quals);

		return quals;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myFiltreDetailPanel.updateData();

		EOQualifier qual = new EOAndQualifier(buildFilterQualifiers());

		System.out.println("QUAL=" + qual);

		NSArray res = EOsFinder.fetchArray(myListener.getEditingContext(), EOEcritureDetail.ENTITY_NAME, qual, null, true);

		res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
			new EOSortOrdering(EOEcritureDetail.ECR_NUMERO_AND_ECD_INDEX_KEY, EOSortOrdering.CompareAscending)
		}));

		myDisplayGroup.setObjectArray(res);

		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();
		myDisplayGroup.setSelectedObject(null);

		//		myTableModel.updateInnerRowCount();
		//		myTableModel.fireTableDataChanged();
		//
		//		myEOTable.synchroniseSelectionFromDisplayGroupSelection();

	}

	public void setSelectedObject(EOEcritureDetail ecritureDetail) {
		myEOTable.forceNewSelectionOfObjects(new NSArray(new Object[] {
				ecritureDetail
		}));
	}

	public NSArray getSelectedDetailEcritures() {
		return myDisplayGroup.selectedObjects();
	}

	/**
	 * Panel affichant les options de filtre pour la liste.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public class FiltreDetailPanel extends ZKarukeraPanel {
		//	    private final int DEFAULT_LABEL_WIDTH=100;
		private final Color BORDURE_COLOR = getBackground().brighter();

		//	    private ZLabelTextField myEcdMontantField;
		//	    private ZTextField myEcrNumeroMinField;
		//	    private ZTextField myEcrNumeroMaxField;
		//	    private ZDatePickerField myEcrDateSaisieMinField;
		//	    private ZDatePickerField myEcrDateSaisieMaxField;
		//	    private ZTextField myEcrMontantMinField;
		//	    private ZTextField myEcrMontantMaxField;
		//
		//	    private EcrNumeroMinFieldModel myEcrNumeroMinFieldModel;
		//	    private EcrNumeroMaxFieldModel myEcrNumeroMaxFieldModel;
		//	    private EcrDateSaisieMinFieldModel myEcrDateSaisieMinFieldModel;
		//	    private EcrDateSaisieMaxFieldModel myEcrDateSaisieMaxFieldModel;
		//	    private EcrMontantMinFieldModel myEcrMontantMinFieldModel;
		//	    private EcrMontantMaxFieldModel myEcrMontantMaxFieldModel;

		private ActionFiltrer myActionFiltrer;

		private ZFormPanel num;
		private ZFormPanel date;
		private ZFormPanel montant;
		private ZFormPanel gestion;

		private ZFormPanel libelle;
		private ZFormPanel origine;

		private ZFormPanel resteEmarger;

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
		 */
		public void initGUI() {
			setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 10));
			myActionFiltrer = new ActionFiltrer();

			//            Component[] comps = new Component[4];

			gestion = ZFormPanel.buildLabelField("Code gestion", new ZTextField.DefaultTextFieldModel(myListener.getMyFilterValues(), EOGestion.GES_CODE_KEY));
			origine = ZFormPanel.buildLabelField("Origine", new ZTextField.DefaultTextFieldModel(myListener.getMyFilterValues(), "origine"));
			((ZTextField) origine.getMyFields().get(0)).getMyTexfield().setColumns(15);
			//            num = buildNumFields();
			num = ZFormPanel.buildFourchetteNumberFields("<= N° <=", new EcrNumeroMinFieldModel(), new EcrNumeroMaxFieldModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER);
			//            date = buildDateFields();
			date = ZFormPanel.buildFourchetteDateFields("<= Date <=", new EcrDateSaisieMinFieldModel(), new EcrDateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
			//            montant = buildMontantFields();
			montant = ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getMyFilterValues(), "ecdMontantMin"), new ZNumberField.BigDecimalFieldModel(myListener.getMyFilterValues(), "ecdMontantMax"), ZConst.DECIMAL_EDIT_FORMATS,
					ZConst.FORMAT_EDIT_NUMBER);
			resteEmarger = ZFormPanel.buildFourchetteNumberFields("<= Reste a emarger <=", new ZNumberField.BigDecimalFieldModel(myListener.getMyFilterValues(), "ecdResteEmargerMin"), new ZNumberField.BigDecimalFieldModel(myListener.getMyFilterValues(), "ecdResteEmargerMax"),
					ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER);
			libelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(myListener.getMyFilterValues(), "libelle"));
			((ZTextField) libelle.getMyFields().get(0)).getMyTexfield().setColumns(20);

			((ZTextField) montant.getMyFields().get(0)).getMyTexfield().setColumns(10);
			((ZTextField) montant.getMyFields().get(1)).getMyTexfield().setColumns(10);

			((ZTextField) resteEmarger.getMyFields().get(0)).getMyTexfield().setColumns(10);
			((ZTextField) resteEmarger.getMyFields().get(1)).getMyTexfield().setColumns(10);

			gestion.setDefaultAction(myActionFiltrer);
			num.setDefaultAction(myActionFiltrer);
			resteEmarger.setDefaultAction(myActionFiltrer);
			date.setDefaultAction(myActionFiltrer);
			montant.setDefaultAction(myActionFiltrer);
			libelle.setDefaultAction(myActionFiltrer);

			ArrayList list = new ArrayList();
			list.add(gestion);
			list.add(num);
			list.add(date);
			list.add(montant);

			ArrayList list2 = new ArrayList();
			list2.add(origine);
			list2.add(libelle);
			list2.add(resteEmarger);

			Box b = Box.createVerticalBox();
			b.add(buildLine(list));
			b.add(buildLine(list2));

			//            ArrayList actions = new ArrayList();
			//            actions.add(myActionFiltrer);

			add(b, BorderLayout.WEST);
			add(buildRightPanel(), BorderLayout.EAST);
			add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

			//            myEcrNumeroField.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
		}

		private final JPanel buildRightPanel() {
			JPanel tmp = new JPanel(new BorderLayout());
			tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
			ArrayList list = new ArrayList();
			list.add(getMyActionFiltrer());

			ArrayList comps = ZKarukeraPanel.getButtonListFromActionList(list, 50, 50);
			//            JComponent comp = (JComponent) comps.get(0);

			tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
			tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
			return tmp;
		}

		//
		//        private JPanel buildDateFields() {
		//            JPanel p = new JPanel();
		//            myEcrDateSaisieMinFieldModel = new EcrDateSaisieMinFieldModel();
		//            myEcrDateSaisieMinField = new ZDatePickerField(myEcrDateSaisieMinFieldModel, (DateFormat) ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16),myListener.getDialog());
		//            myEcrDateSaisieMinField.getMyTexfield().setColumns(8);
		//
		//            myEcrDateSaisieMaxFieldModel = new EcrDateSaisieMaxFieldModel();
		//            myEcrDateSaisieMaxField = new ZDatePickerField(myEcrDateSaisieMaxFieldModel, (DateFormat) ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16),myListener.getDialog());
		//            myEcrDateSaisieMaxField.getMyTexfield().setColumns(8);
		//
		//            JLabel l = new JLabel("<= Date <=");
		//            p.add(myEcrDateSaisieMinField);
		//            p.add(l);
		//            p.add(myEcrDateSaisieMaxField);
		//            p.setBorder(BorderFactory.createLineBorder(BORDURE_COLOR));
		//            return p;
		//        }
		//
		//
		//        private JPanel buildNumFields() {
		//            JPanel p = new JPanel();
		//            myEcrNumeroMinFieldModel = new EcrNumeroMinFieldModel();
		//            myEcrNumeroMinField = new ZTextField(myEcrNumeroMinFieldModel);
		//            myEcrNumeroMinField.setFormat(ZConst.FORMAT_EDIT_NUMBER);
		//            myEcrNumeroMinField.getMyTexfield().setColumns(6);
		//
		//            myEcrNumeroMaxFieldModel = new EcrNumeroMaxFieldModel();
		//            myEcrNumeroMaxField = new ZTextField(myEcrNumeroMaxFieldModel);
		//            myEcrNumeroMaxField.setFormat(ZConst.FORMAT_EDIT_NUMBER);
		//            myEcrNumeroMaxField.getMyTexfield().setColumns(6);
		//
		//            JLabel l = new JLabel("<= N° écriture <=");
		//            p.add(myEcrNumeroMinField);
		//            p.add(l);
		//            p.add(myEcrNumeroMaxField);
		//
		//
		//            p.setBorder(BorderFactory.createLineBorder(BORDURE_COLOR));
		//            return p;
		//        }
		//
		//        private JPanel buildMontantFields() {
		//            JPanel p = new JPanel();
		//            myEcrMontantMinFieldModel = new EcrMontantMinFieldModel();
		//            myEcrMontantMinField = new ZTextField(myEcrMontantMinFieldModel);
		//            myEcrMontantMinField.setFormat(ZConst.FORMAT_EDIT_NUMBER);
		//            myEcrMontantMinField.getMyTexfield().setColumns(6);
		//
		//
		//            myEcrMontantMaxFieldModel = new EcrMontantMaxFieldModel();
		//            myEcrMontantMaxField = new ZTextField(myEcrMontantMaxFieldModel);
		//            myEcrMontantMaxField.setFormat(ZConst.FORMAT_EDIT_NUMBER);
		//            myEcrMontantMaxField.getMyTexfield().setColumns(6);
		//
		////            Box b = Box.createHorizontalBox();
		//
		//            JLabel l = new JLabel("<= Montant <=");
		//            p.add(myEcrMontantMinField);
		//            p.add(l);
		//            p.add(myEcrMontantMaxField);
		//            p.setBorder(BorderFactory.createLineBorder(BORDURE_COLOR));
		////            p.add(b, BorderLayout.CENTER);
		//            return p;
		//        }
		//

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
		 */
		public void updateData() throws Exception {
			libelle.updateData();
			date.updateData();
			gestion.updateData();
			montant.updateData();
			resteEmarger.updateData();
			num.updateData();
			origine.updateData();
		}

		private final class EcrNumeroMinFieldModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getMyFilterValues().get("ecrNumeroMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getMyFilterValues().put("ecrNumeroMin", value);
			}
		}

		private final class EcrNumeroMaxFieldModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getMyFilterValues().get("ecrNumeroMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getMyFilterValues().put("ecrNumeroMax", value);
			}
		}

		//
		//        private final class EcrMontantMinFieldModel implements ZTextField.IZTextFieldModel {
		//
		//            /**
		//             * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
		//             */
		//            public Object getValue() {
		//                return myListener.getMyFilterValues().get("ecdMontantMin");
		//            }
		//
		//            /**
		//             * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
		//             */
		//            public void setValue(Object value) {
		//                myListener.getMyFilterValues().put("ecdMontantMin", value);
		//            }
		//        }
		//
		//
		//        private final class EcrMontantMaxFieldModel implements ZTextField.IZTextFieldModel {
		//
		//            /**
		//             * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
		//             */
		//            public Object getValue() {
		//                return myListener.getMyFilterValues().get("ecdMontantMax");
		//            }
		//
		//            /**
		//             * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
		//             */
		//            public void setValue(Object value) {
		//                myListener.getMyFilterValues().put("ecdMontantMax", value);
		//            }
		//        }

		private final class EcrDateSaisieMaxFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getMyFilterValues().get("ecrDateSaisieMax");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getMyFilterValues().put("ecrDateSaisieMax", value);
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

		private final class EcrDateSaisieMinFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getMyFilterValues().get("ecrDateSaisieMin");
			}

			/**
			 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getMyFilterValues().put("ecrDateSaisieMin", value);
			}

			public Window getParentWindow() {
				return getMyDialog();
			}
		}

		protected final class ActionFiltrer extends AbstractAction {
			/**
                 *
                 */
			public ActionFiltrer() {
				super("Filtrer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				myListener.onFiltrer();
			}

		}

		public ActionFiltrer getMyActionFiltrer() {
			return myActionFiltrer;
		}
	}

	public interface IEmargementDetailPanelListener {
		public void onSelectionChanged();

		public EOComptabilite getComptabilite();

		public EOEditingContext getEditingContext();

		public Dialog getDialog();

		public void onFiltrer();

		public HashMap getMyFilterValues();

		public EOExercice getExercice();

		public EOPlanComptable getPlanComptable();
	}

	public FiltreDetailPanel getMyFiltreDetailPanel() {
		return myFiltreDetailPanel;
	}
}
