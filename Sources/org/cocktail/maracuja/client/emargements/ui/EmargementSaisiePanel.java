/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.text.Format;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZStatusBar;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;

import com.webobjects.foundation.NSArray;


/**
 * Panel principal pour la saisie des émargements.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementSaisiePanel extends ZKarukeraPanel {
    private EmargementSaisieEcrFilterPanel myEmargementEcrFilterPanel;
    private EmargementSaisieDetailCreditPanel myEmargementDetailCreditPanel;
    private EmargementSaisieDetailDebitPanel myEmargementDetailDebitPanel;

//    private EmargementEcrFilterPanelListener myEmargementEcrFilterPanelListener;
//    private EmargementDetailCreditPanelListener myEmargementDetailCreditPanelListener;
//    private EmargementDetailDebitPanelListener myEmargementDetailDebitPanelListener;

//    private ZPanelBalanceProvider myZPanelBalanceProvider;
    private ZPanelBalance myZPanelBalance;

//    private UserMontantFieldProvider myUserMontantFieldProvider;
    private ZNumberField myUserMontantField;

//    private HashMap ecrFilterValues;
//    private HashMap debitFilterValues;
//    private HashMap creditFilterValues;

//    private NSArray planComptables;
//    private NSArray comptabilites;

    private IEmargementSaisiePanelListener myListener;

    private ZStatusBar myStatusBar;
    private ZStatusBar.ZStatusZone myStatusZoneInfo;



//    private JLabel labelNbDetailsSelectionnes;
    private ZTextField detailSelection;

//    private BigDecimal userMontant;


    /**
     * @param editingContext
     */
    public EmargementSaisiePanel(IEmargementSaisiePanelListener listener,
            EmargementSaisieEcrFilterPanel ecrFilterPanel,
            EmargementSaisieDetailCreditPanel creditPanel,
            EmargementSaisieDetailDebitPanel debitPanel,
            ZPanelBalance panelBalance
            ) {
        super();
        myListener = listener;


//        myEmargementEcrFilterPanelListener = new EmargementEcrFilterPanelListener();
//        myEmargementDetailCreditPanelListener = new EmargementDetailCreditPanelListener();
//        myEmargementDetailDebitPanelListener = new EmargementDetailDebitPanelListener();
//        myEmargementEcrFilterPanel = new EmargementSaisieEcrFilterPanel(myEmargementEcrFilterPanelListener);
//        myEmargementDetailCreditPanel = new EmargementSaisieDetailCreditPanel(getEditingContext(), myEmargementDetailCreditPanelListener);
//        myEmargementDetailDebitPanel = new EmargementSaisieDetailDebitPanel(getEditingContext(), myEmargementDetailDebitPanelListener);

        myEmargementEcrFilterPanel = ecrFilterPanel;
        myEmargementDetailCreditPanel = creditPanel;
        myEmargementDetailDebitPanel = debitPanel;

//        myZPanelBalanceProvider = new ZPanelBalanceProvider();
        myZPanelBalance = panelBalance;


//        myZPanelBalanceProvider = new ZPanelBalanceProvider();
//        myZPanelBalance = new ZPanelBalance(myZPanelBalanceProvider, true);
//

    }


    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionEmarger());
        a.add(myListener.actionClose());
        ArrayList buttons = ZKarukeraPanel.getButtonListFromActionList(a);

//        JLabel infoTip = new JLabel(ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16));
//        infoTip.setToolTipText("<html><head><style type=\"text/css\"><!-- body {margin-left: 2px; margin-top: 2px; margin-right: 2px; margin-bottom: 2px;} --></style></head>" +
//        		"<body>Pour sélectionner plusieurs écritures, utilisez la touche <strong>Ctrl</strong> de votre " +
//        		"clavier <br>et cliquez sur la ligne que vous souhaitez ajouter/retirer.</body></html>");


        JLabel infoTip = ZTooltip.getTooltip_EMARGEMENTPLSUISEURSECRITURES();

        buttons.add(0, infoTip);


        JPanel p1 = new JPanel(new BorderLayout());
        p1.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        p1.add(ZKarukeraPanel.buildHorizontalPanelOfComponent(buttons), BorderLayout.EAST);

        JPanel p = new JPanel(new BorderLayout());




        p.add(p1, BorderLayout.CENTER);
        p.add(myStatusBar, BorderLayout.SOUTH);
        p.add(buildBoxTotaux(), BorderLayout.NORTH);
        return p;
    }

    private JPanel buildBoxTotaux() {
        ZLabeledComponent tmp = new ZLabeledComponent("Montant à émarger", myUserMontantField, ZLabeledComponent.LABELONLEFT,-1);
        detailSelection = new ZTextField(myListener.getSetailSelectionModel());
        detailSelection.getMyTexfield().setColumns(25);
        detailSelection.getMyTexfield().setEditable(false);
        detailSelection.getMyTexfield().setFocusable(false);
        detailSelection.setUIReadOnly(this.getBackground());

        JPanel p = new JPanel(new BorderLayout());
        p.add(myZPanelBalance, BorderLayout.WEST);
        p.add(ZKarukeraPanel.buildLine(new Component[]{ new ZLabeledComponent("",detailSelection,ZLabeledComponent.LABELONLEFT,-10),Box.createHorizontalStrut(10),tmp }), BorderLayout.EAST);
        p.add(new JPanel(new BorderLayout()),BorderLayout.CENTER);
        return p;
    }






    /**
     * Met à jour le contenu du panneau des totaux.
     */
    public void updateTotaux() {
        myZPanelBalance.updateData();
        detailSelection.updateData();

//        labelNbDetailsSelectionnes.setText( getSelectedDebits().count() + " débit(s) / "+getSelectedCredits().count() + " crédit(s)");
    }

    private void buildStatutBar() {
        myStatusZoneInfo = new ZStatusBar.ZStatusZone(new JLabel());
        myStatusBar = new ZStatusBar(new ZStatusBar.ZStatusZone[]{myStatusZoneInfo});
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        buildStatutBar();

//        myUserMontantFieldProvider = new UserMontantFieldProvider();
//        myUserMontantField = new ZNumberField(myUserMontantFieldProvider,new Format[]{ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL, ZConst.FORMAT_DISPLAY_NUMBER},ZConst.FORMAT_DISPLAY_NUMBER);
        myUserMontantField = new ZNumberField(myListener.getUserMontantFieldModel()  ,new Format[]{ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL, ZConst.FORMAT_DISPLAY_NUMBER},ZConst.FORMAT_DISPLAY_NUMBER);
        myUserMontantField.getMyTexfield().setColumns(10);


        myEmargementDetailCreditPanel.setMyDialog(getMyDialog());
        myEmargementDetailDebitPanel.setMyDialog(getMyDialog());
        myEmargementEcrFilterPanel.setMyDialog(getMyDialog());

        myEmargementDetailCreditPanel.initGUI();
        myEmargementDetailDebitPanel.initGUI();
        myEmargementEcrFilterPanel.initGUI();
        myZPanelBalance.initGUI();


        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        add(myEmargementEcrFilterPanel, BorderLayout.NORTH);

        JPanel p1 = ZKarukeraPanel.encloseInPanelWithTitle("Débits",null,ZConst.BG_COLOR_TITLE,myEmargementDetailDebitPanel, null, null);
        JPanel p2 = ZKarukeraPanel.encloseInPanelWithTitle("Crédits",null,ZConst.BG_COLOR_TITLE,myEmargementDetailCreditPanel, null, null);

        JSplitPane s = new JSplitPane(JSplitPane.VERTICAL_SPLIT, p1, p2);
        s.setOneTouchExpandable(false);
        s.setResizeWeight(0.5);
        s.setDividerLocation(0.5);
        s.setBorder(BorderFactory.createEmptyBorder());

        this.add(s, BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);

//        initDefaultValues();

    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myListener.selectionsChanged();

    }


    /**
     * @return
     */
    public ZTextField getUserMontantField() {
        return myUserMontantField;
    }



	public interface IEmargementSaisiePanelListener {
        public NSArray getPlanComptables();
        public void selectionsChanged();
//        public NSArray getComptabilites();
        public NSArray getGestions();
        public Action actionClose();
        public Action actionEmarger();
        public ZTextField.IZTextFieldModel getUserMontantFieldModel();
        public ZTextField.IZTextFieldModel getSetailSelectionModel();
	}


































}
