/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZKeyValueLookUpField;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextField;

import com.webobjects.foundation.NSArray;


/**
 * Panel de filtre concernant les imputations et la comptabilité pour la saisie des émargements.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementSaisieEcrFilterPanel extends ZKarukeraPanel {
    private IEmargementEcrFilterPanelListener myListener;
    private ZKeyValueLookUpField pcoLookupField;


    /**
     * @param editingContext
     */
    public EmargementSaisieEcrFilterPanel(IEmargementEcrFilterPanelListener listener) {
        super();
        myListener = listener;
//        initGUI();
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(2,2,2,2));


        pcoLookupField = new ZKeyValueLookUpField( new EcdPcoNumFieldModel(), myListener.getPcoMap());
        pcoLookupField.getMyTexfield().setColumns(10);
        pcoLookupField.addDocumentListener(new PcoNumDocumentListener());

        pcoLookupField.getMyTexfield().addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                myListener.actionFiltrer().actionPerformed(e);
            }
        });
        

        Component[] comps = new Component[1];
        comps[0] = new ZLabeledComponent("Compte :",  pcoLookupField, ZLabeledComponent.LABELONLEFT, -1);

        final ArrayList actions = new ArrayList();
        actions.add(myListener.actionFiltrer());

        final ArrayList comps2 = ZKarukeraPanel.getButtonListFromActionList(actions);
        
        
        
        
        
        this.add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
        this.add(ZKarukeraPanel.buildHorizontalPanelOfComponent(comps2) , BorderLayout.EAST);
        this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

//        planComptableSelectionDialog = createPlanComptableSelectionDialog();
    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        pcoLookupField.updateData();

    }

    private final class EcdPcoNumFieldModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            EOPlanComptable res = (EOPlanComptable) myListener.getMyValues() .get(EOEcritureDetail.PLAN_COMPTABLE_KEY);
            if (res ==null) {
                return null;
            }
            return res.pcoNum();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           //On fait rien
        }
    }




    public interface IEmargementEcrFilterPanelListener {
        /**
         * @return Un dictionnaire contenant les valeurs saisies par l'utilisateur.
         */
        public HashMap getMyValues();
        /**
         * @return
         */
        public Map getPcoMap();
        public NSArray getPlanComptables();
        public Action actionFiltrer();
//        public NSArray getComptabilites();
        /** Appelée lorsque le contenu du champ de saisie des plancos est en train de changer */
        public void pcoNumChanged();
    }




    /**
     * @return
     */
    public ZTextField getEcdPcoNumField() {
        return pcoLookupField;
    }

    private final class PcoNumDocumentListener implements DocumentListener {

        /**
         * @see javax.swing.event.DocumentListener#changedUpdate(javax.swing.event.DocumentEvent)
         */
        public void changedUpdate(DocumentEvent e) {
            myListener.pcoNumChanged();
        }

        /**
         * @see javax.swing.event.DocumentListener#insertUpdate(javax.swing.event.DocumentEvent)
         */
        public void insertUpdate(DocumentEvent e) {
            myListener.pcoNumChanged();
        }

        /**
         * @see javax.swing.event.DocumentListener#removeUpdate(javax.swing.event.DocumentEvent)
         */
        public void removeUpdate(DocumentEvent e) {
            myListener.pcoNumChanged();
        }

    }


}
