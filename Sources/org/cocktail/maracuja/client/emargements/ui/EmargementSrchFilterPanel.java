/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementSrchFilterPanel extends ZKarukeraPanel {

    private IEmargementFilterPanelListener myListener;
    private ZFormPanel numerosPanel;
    private ZFormPanel datesPanel;
    private ZFormPanel montantHtsPanel;
    private ZFormPanel pcoNumPanel;
    private ZFormPanel libelle;
    private ZFormPanel ecrNumero;
    private ZFormPanel gesCode;




    public EmargementSrchFilterPanel(IEmargementFilterPanelListener listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {

        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(buildRightPanel(), BorderLayout.EAST);
    }


    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        final ArrayList list = new ArrayList();
        list.add(myListener.getActionSrch());

        final ArrayList comps = ZKarukeraPanel.getButtonListFromActionList(list, 50,50);
//        JComponent comp = (JComponent) comps.get(0);

        tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }



    private final JPanel buildFilters() {
        numerosPanel = buildNumeroFields();
        datesPanel = buildDateFields();
        montantHtsPanel = buildHtFields();
        pcoNumPanel = ZFormPanel.buildLabelField("Imputation", new PcoNumModel());
        libelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "libelle") );
        ((ZTextField)libelle.getMyFields().get(0)).getMyTexfield().setColumns(20);
        ecrNumero = buildEcrNumeroFields();
        gesCode = buildGesCodeField();


        ((ZTextField)montantHtsPanel.getMyFields().get(0)).getMyTexfield().setColumns(10);
        ((ZTextField)montantHtsPanel.getMyFields().get(1)).getMyTexfield().setColumns(10);


        numerosPanel.setDefaultAction(myListener.getActionSrch());
        datesPanel.setDefaultAction(myListener.getActionSrch());
        montantHtsPanel.setDefaultAction(myListener.getActionSrch());
        pcoNumPanel.setDefaultAction(myListener.getActionSrch());
        libelle.setDefaultAction(myListener.getActionSrch());
        ecrNumero.setDefaultAction(myListener.getActionSrch());
        gesCode.setDefaultAction(myListener.getActionSrch());


        setSimpleLineBorder(numerosPanel);
        setSimpleLineBorder(datesPanel);        
        setSimpleLineBorder(montantHtsPanel);        
        setSimpleLineBorder(pcoNumPanel);        
        setSimpleLineBorder(ecrNumero);        
        setSimpleLineBorder(gesCode);        
        setSimpleLineBorder(libelle);        

        
        JPanel p = new JPanel(new BorderLayout());

        Box b = Box.createVerticalBox();
        b.add(ZKarukeraPanel.buildLine(new Component[]{pcoNumPanel, gesCode, numerosPanel, ecrNumero}));
        b.add(ZKarukeraPanel.buildLine(new Component[]{datesPanel, montantHtsPanel, libelle}));

        p.add(b, BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;
    }


    /**
     * @return
     */
    private final ZFormPanel buildNumeroFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= N° Emargement <=", new NumeroMinModel(), new NumeroMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER );
    }
    private final ZFormPanel buildEcrNumeroFields() {
//        return ZFormPanel.buildFourchetteNumberFields("<= N° Ecriture <=", new EcrNumeroMinModel(), new EcrNumeroMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER );
        
        return ZFormPanel.buildLabelField("N° écriture", new ZNumberField(new EcrNumeroMinModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER));
    }



    private final ZFormPanel buildDateFields() {
        return ZFormPanel.buildFourchetteDateFields("<= Date <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
    }


    private final ZFormPanel buildHtFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "emaMontantMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "emaMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
    }

    private final ZFormPanel buildGesCodeField() {
        return ZFormPanel.buildLabelField("Code gestion", new GesCodeModel());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        numerosPanel.updateData();
    }



    public interface IEmargementFilterPanelListener {

        /**
         * @return
         */
        public Action getActionSrch();

        /**
         * @return
         */
        public Dialog getDialog();

        /**
         * @return un dictionnaire dans lequel sont stockés les filtres.
         */
        public HashMap getFilters();

    }


    private final class NumeroMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("emaNumeroMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("emaNumeroMin",value);

        }

    }




    private final class NumeroMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("emaNumeroMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("emaNumeroMax",value);

        }

    }
    private final class EcrNumeroMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("ecrNumeroMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("ecrNumeroMin",value);

        }

    }



//
//    private final class EcrNumeroMaxModel implements ZTextField.IZTextFieldModel {
//
//        /**
//         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
//         */
//        public Object getValue() {
//            return myListener.getFilters().get("ecrNumeroMax") ;
//        }
//
//        /**
//         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
//         */
//        public void setValue(Object value) {
//            myListener.getFilters().put("ecrNumeroMax",value);
//
//        }
//
//    }

    private final class PcoNumModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get(EOPlanComptable.PCO_NUM_KEY) ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put(EOPlanComptable.PCO_NUM_KEY,value);

        }

    }

//
//    private final class MontantHtMinModel implements ZTextField.IZTextFieldModel {
//
//        /**
//         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
//         */
//        public Object getValue() {
//            return myListener.getFilters().get("emaMontantMin") ;
//        }
//
//        /**
//         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
//         */
//        public void setValue(Object value) {
//            myListener.getFilters().put("emaMontantMin",value);
//
//        }
//
//    }
//    private final class MontantHtMaxModel implements ZTextField.IZTextFieldModel {
//
//        /**
//         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
//         */
//        public Object getValue() {
//            return myListener.getFilters().get("emaMontantMax") ;
//        }
//
//        /**
//         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
//         */
//        public void setValue(Object value) {
//            myListener.getFilters().put("emaMontantMax",value);
//
//        }
//
//    }


    private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("emaDateMax");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("emaDateMax", value);
        }
        

        public Window getParentWindow() {
            return getMyDialog();
        }        
    }

    private final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("emaDateMin");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("emaDateMin", value);
        }

        public Window getParentWindow() {
            return getMyDialog();
        }        
    }

    private final class GesCodeModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get(EOGestion.GES_CODE_KEY) ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put(EOGestion.GES_CODE_KEY,value);

        }

    }

}
