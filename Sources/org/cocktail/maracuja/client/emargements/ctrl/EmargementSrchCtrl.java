/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZAction;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.emargements.ui.EmargementSrchFilterPanel;
import org.cocktail.maracuja.client.emargements.ui.EmargementSrchFilterPanel.IEmargementFilterPanelListener;
import org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel;
import org.cocktail.maracuja.client.factory.process.FactoryProcessEmargement;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOEmargementDetail;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZNumberUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementSrchCtrl extends CommonCtrl {

	private static final String TITLE = "Gestion des émargements";
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	private EmargementSrchPanel myPanel;

	private final ZAction ACTION_SAISIE = myApp.getActionbyId(ZActionCtrl.IDU_JOEMSA);
	private final ZAction ACTION_ANNULER = myApp.getActionbyId(ZActionCtrl.IDU_COEMAN);

	private final ActionClose actionClose = new ActionClose();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionNew actionNew = new ActionNew();
	//    private final ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionAnnulerEmargement actionAnnulerEmargement = new ActionAnnulerEmargement();

	private final HashMap myFilters = new HashMap();

	//    private NSArray emargementsForRecap;
	private NSArray gestionsPourConsultation;
	private NSArray gestionsPourAnnulation;

	private CreditListListener creditListListener;
	private DebitListListener debitListListener;
	private EmargementSrchPanelListener emargementSrchPanelListener;
	private EmargementFilterPanelListener emargementFilterPanelListener;
	private EmargementListPanelListener emargementListPanelListener;

	private boolean isAnnulationEnabled = false;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public EmargementSrchCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COEM)) {
			gestionsPourConsultation = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COEM);
		}
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COEMAN)) {
			gestionsPourAnnulation = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COEMAN);
		}
		
		isAnnulationEnabled = ACTION_ANNULER.isEnabled();

		creditListListener = new CreditListListener();
		debitListListener = new DebitListListener();
		emargementFilterPanelListener = new EmargementFilterPanelListener();
		emargementListPanelListener = new EmargementListPanelListener();

		emargementSrchPanelListener = new EmargementSrchPanelListener();

		myPanel = new EmargementSrchPanel(emargementSrchPanelListener);
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.EmargementRechercheFilterPanel.IEmargementRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			setWaitCursor(true);
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		this.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial, NSArray emargements) {
		ZKarukeraDialog win = createModalDialog(dial);

		try {
			loadEmargements(emargements);
			win.open();
		} finally {
			win.dispose();
		}
	}

	private final void emargementNew() {
		try {
			EmargementSaisieCtrl emargementSaisieCtrl = new EmargementSaisieCtrl(getEditingContext());
			NSArray emargementsTmp = emargementSaisieCtrl.openDialog(getMyDialog());
			loadEmargements(emargementsTmp);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public final void resetFilter() {
		myFilters.clear();
	}

	//    private final void annulerEmargement() {
	//        EOEmargement currentEmargement = (EOEmargement) myPanel.getEmargementListPanel().selectedObject();
	//	    try {
	//	        if (currentEmargement==null) {
	//	            throw new DataCheckException("L'émargement n'est pas spécifié");
	//	        }
	//
	//	        if (showConfirmationDialog("Confirmation", "Souhaitez-vous annuler l'émargement n°" + currentEmargement.emaNumero()+" ?",ZMsgPanel.BTLABEL_NO)) {
	//	    	    FactoryProcessEmargement myFactoryProcessEmargement = new FactoryProcessEmargement(myApp.wantShowTrace());
	//		        myFactoryProcessEmargement.supprimerEmargement(getEditingContext(), currentEmargement);
	//		        getEditingContext().saveChanges();
	//		        myPanel.updateData();
	//		        showInfoDialog("L'émargement a été annulé.");
	//	        }
	//	    }
	//	    catch (Exception e) {
	//	        getEditingContext().revert();
	//            showErrorDialog(e);
	//        }
	//
	//    }

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Saisir un nouvel émargement");
			setEnabled(ACTION_SAISIE.isEnabled());
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				ZActionCtrl.checkActionWithExeStat(ACTION_SAISIE);
				emargementNew();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	public final class ActionImprimer extends AbstractAction {

		public ActionImprimer() {
			super("Imprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer les émargements sélectionnés");
			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//TODO impression emargements
		}

	}

	private final class EmargementSrchPanelListener implements EmargementSrchPanel.IEmargementSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#getEmargementListListener()
		 */
		public IZKarukeraTablePanelListener getEmargementListListener() {
			return emargementListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#actionFermer()
		 */
		public Action actionFermer() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#actionImprimer()
		 */
		public Action actionImprimer() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#actionAnnuler()
		 */
		public Action actionAnnuler() {
			return actionAnnulerEmargement;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#actionNew()
		 */
		public Action actionNew() {
			return actionNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#getCreditsListListener()
		 */
		public IZKarukeraTablePanelListener getCreditsListListener() {
			return creditListListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#getDebitsListListener()
		 */
		public IZKarukeraTablePanelListener getDebitsListListener() {
			return debitListListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchPanel.IEmargementSrchPanelListener#getEmargementFilterPanelListener()
		 */
		public IEmargementFilterPanelListener getEmargementFilterPanelListener() {
			return emargementFilterPanelListener;
		}

	}

	/**
	 * Precharge un tableau d'emargements.
	 * 
	 * @param emargementsTmp tableau d'EOEmargement
	 */
	private final void loadEmargements(final NSArray emargementsTmp) {
		if (emargementsTmp != null && emargementsTmp.count() > 0) {
			myFilters.clear();
			NSMutableArray emaOrdres = new NSMutableArray();
			for (int i = 0; i < emargementsTmp.count(); i++) {
				EOEmargement element = (EOEmargement) emargementsTmp.objectAtIndex(i);
				emaOrdres.addObject(ServerProxy.serverPrimaryKeyForObject(getEditingContext(), element).valueForKey("emaOrdre"));
			}
			myFilters.put("emaOrdre", emaOrdres);
			try {
				myPanel.updateData();
			} catch (Exception e) {
				myFilters.clear();
				showErrorDialog(e);
			} finally {
				myFilters.clear();
			}
		}
	}

	private final void refreshActions() {
		EOEmargement currentEmargement = (EOEmargement) myPanel.getEmargementListPanel().selectedObject();
		actionAnnulerEmargement.setEnabled(isAnnulationEnabled && (currentEmargement != null));
	}

	/**
	 * Lance la procédure d'annulation de l'émargement en cours.
	 * 
	 * @throws DefaultClientException
	 */
	private void annulerCurrentEmargement() throws DefaultClientException {
		final FactoryProcessEmargement myFactoryProcessEmargement = new FactoryProcessEmargement(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		final EOEmargement currentEmargement = (EOEmargement) myPanel.getEmargementListPanel().selectedObject();

		try {
			if (currentEmargement == null) {
				throw new DataCheckException("L'émargement n'est pas spécifié");
			}

			//faire un invalidate + vérifier si pas annulé
			getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
				currentEmargement
			})));

			if (!EOEmargement.emaEtatValide.equals(currentEmargement.emaEtat())) {
				throw new DataCheckException("L'émargement n° " + currentEmargement.emaNumero() + " n'est pas valide. Il vient peut-etre d'être annulé par un autre utilisateur.");
			}

			final NSMutableArray gestions = new NSMutableArray();
			for (int i = 0; i < currentEmargement.emargementDetails().count(); i++) {
				final EOEmargementDetail element = (EOEmargementDetail) currentEmargement.emargementDetails().objectAtIndex(i);
				if (gestions.indexOfObject(element.source().gestion()) == NSArray.NotFound) {
					gestions.addObject(element.source().gestion());
				}
				if (gestions.indexOfObject(element.destination().gestion()) == NSArray.NotFound) {
					gestions.addObject(element.destination().gestion());
				}
			}

			//Vérifier que tous les codes gestions sont bien autorisés
			for (int i = 0; i < gestions.count(); i++) {
				final EOGestion element = (EOGestion) gestions.objectAtIndex(i);
				if (gestionsPourAnnulation.indexOfObject(element) == NSArray.NotFound) {
					throw new DataCheckException("Vous n'avez pas le droit d'annuler des émargements comportant des écritures sur le code gestion " + element.gesCode());
				}
			}

			if (showConfirmationDialog("Confirmation", "Souhaitez-vous annuler l'émargement n°" + currentEmargement.emaNumero() + " ?", ZMsgPanel.BTLABEL_NO)) {
				myFactoryProcessEmargement.supprimerEmargement(getEditingContext(), currentEmargement);
				getEditingContext().saveChanges();
				myPanel.getEmargementListPanel().updateData();
				showInfoDialog("L'émargement a été annulé.");
			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		}

	}

	/**
	 * @return Les emargements valides.
	 */
	private NSArray getEmargements() {
		EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("emaNumero", EOSortOrdering.CompareAscending);
		//
		//
		//Créer la condition à partir des filtres
		try {
			return EOsFinder.fetchArray(getEditingContext(), EOEmargement.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)), new NSArray(sort1), true, true, false, null);
		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}

	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		ZLogger.debug(dicoFiltre);

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[] {
			myApp.appUserInfo().getCurrentExercice()
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("emaEtat=%@ ", new NSArray(new Object[] {
			EOEmargement.emaEtatValide
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("emaNumero<>%@ ", new NSArray(new Object[] {
			new Integer(-1)
		})));

		//Construire les qualifiers à partir des saisies utilisateur

		NSMutableArray qualsOrdres = new NSMutableArray();
		if (dicoFiltre.get("emaOrdre") != null) {
			NSArray emaOrdres = (NSArray) dicoFiltre.get("emaOrdre");
			for (int i = 0; i < emaOrdres.count(); i++) {
				Number element = (Number) emaOrdres.objectAtIndex(i);
				qualsOrdres.addObject(EOQualifier.qualifierWithQualifierFormat("emaOrdre=%@", new NSArray(element)));
			}
		}
		if (qualsOrdres.count() > 0) {
			quals.addObject(new EOOrQualifier(qualsOrdres));
		}

		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("emaNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("emaNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("emaNumeroMin")).intValue()))));
		}
		if (dicoFiltre.get("emaNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("emaNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("emaNumeroMax")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		NSMutableArray qualsEcrNum = new NSMutableArray();
		//        if (dicoFiltre.get("ecrNumeroMin")!=null) {
		//            qualsEcrNum.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.ecriture.ecrNumero>=%@ or emargementDetails.destination.ecriture.ecrNumero>=%@", new NSArray(new Object[]{new Integer(((Number) dicoFiltre.get("ecrNumeroMin")).intValue()), new Integer(((Number) dicoFiltre.get("ecrNumeroMin")).intValue())} )));
		//        }
		//        if (dicoFiltre.get("ecrNumeroMax")!=null) {
		//            qualsEcrNum.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.ecriture.ecrNumero<=%@ or emargementDetails.source.ecriture.ecrNumero<=%@", new NSArray(new Object[]{new Integer(((Number) dicoFiltre.get("ecrNumeroMax")).intValue()), new Integer(((Number) dicoFiltre.get("ecrNumeroMax")).intValue())} )));
		//        }
		if (dicoFiltre.get("ecrNumeroMin") != null) {
			qualsEcrNum.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.ecriture.ecrNumero=%@ or emargementDetails.destination.ecriture.ecrNumero=%@", new NSArray(new Object[] {
					new Integer(((Number) dicoFiltre.get("ecrNumeroMin")).intValue()), new Integer(((Number) dicoFiltre.get("ecrNumeroMin")).intValue())
			})));
		}
		if (qualsEcrNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsEcrNum));
		}

		///Montant
		NSMutableArray qualsMontant = new NSMutableArray();
		if (dicoFiltre.get("emaMontantMin") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("emaMontant>=%@", new NSArray(ZNumberUtil.arrondi2((Number) dicoFiltre.get("emaMontantMin")))));
		}
		if (dicoFiltre.get("emaMontantMax") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("emaMontant<=%@", new NSArray(ZNumberUtil.arrondi2((Number) dicoFiltre.get("emaMontantMax")))));
		}
		if (qualsMontant.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMontant));
		}

		///Date
		NSMutableArray qualsDate = new NSMutableArray();
		if (dicoFiltre.get("emaDateMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("emaDate>=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("emaDateMin")))));
		}
		if (dicoFiltre.get("emaDateMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("emaDate<=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("emaDateMax")))));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		if (dicoFiltre.get(EOGestion.GES_CODE_KEY) != null && ((String) dicoFiltre.get(EOGestion.GES_CODE_KEY)).length() > 0) {
			//Vérifier si les codes gestions sont autorisés pour l'utilisateur
			if (EOQualifier.filteredArrayWithQualifier(gestionsPourConsultation, EOQualifier.qualifierWithQualifierFormat("gesCode=%@", new NSArray(dicoFiltre.get(EOGestion.GES_CODE_KEY)))).count() == 0) {
				throw new DataCheckException("Code gestion non autorisé");
			}
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.gesCode=%@ or emargementDetails.destination.gesCode=%@", new NSArray(new Object[] {
					(String) dicoFiltre.get(EOGestion.GES_CODE_KEY), (String) dicoFiltre.get(EOGestion.GES_CODE_KEY)
			})));
		}

		if (dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY) != null && ((String) dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY)).length() > 0) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.pcoNum=%@", new NSArray(dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))));
		}

		if (dicoFiltre.get("libelle") != null && ((String) dicoFiltre.get("libelle")).length() > 0) {
			NSMutableArray qualsLibelle = new NSMutableArray();
			String cond = "*" + (String) dicoFiltre.get("libelle") + "*";
			cond = cond.replace(' ', '*');
			qualsLibelle.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.ecriture.ecrLibelle caseInsensitiveLike %@", new NSArray(cond)));
			qualsLibelle.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.destination.ecriture.ecrLibelle caseInsensitiveLike %@", new NSArray(cond)));
			qualsLibelle.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.source.ecdLibelle caseInsensitiveLike %@", new NSArray(cond)));
			qualsLibelle.addObject(EOQualifier.qualifierWithQualifierFormat("emargementDetails.destination.ecdLibelle caseInsensitiveLike %@", new NSArray(cond)));
			quals.addObject(new EOOrQualifier(qualsLibelle));
		}

		ZLogger.debug(quals);

		return quals;
	}

	/**
	 * @return Les EcritureDetails en debits concernées par l'emargement
	 */
	private NSArray getDebitsForEmargement() {
		EOEmargement emargement = (EOEmargement) myPanel.getEmargementListPanel().selectedObject();
		NSMutableArray res = new NSMutableArray();
		//	    emargementDetailDebits = new NSMutableArray();
		if (emargement != null) {
			NSArray tmp = emargement.emargementDetails();
			for (int i = 0; i < tmp.count(); i++) {
				EOEmargementDetail element = (EOEmargementDetail) tmp.objectAtIndex(i);
				if (ZConst.SENS_DEBIT.equals(element.source().ecdSens())) {
					if (res.indexOfObject(element.source()) == NSArray.NotFound) {
						res.addObject(element.source());
						//	                    emargementDetailDebits.addObject(element);
					}
				}

				if (ZConst.SENS_DEBIT.equals(element.destination().ecdSens())) {
					if (res.indexOfObject(element.destination()) == NSArray.NotFound) {
						res.addObject(element.destination());
						//	                    emargementDetailDebits.addObject(element);
					}
				}
			}
		}
		//	    ecritureDetailDebits = res;
		return res;
	}

	/**
	 * @return Les EcritureDetails en crédits concernées par l'emargement
	 */
	private NSArray getCreditsForEmargement() {
		EOEmargement emargement = (EOEmargement) myPanel.getEmargementListPanel().selectedObject();
		NSMutableArray res = new NSMutableArray();
		//	    emargementDetailCredits = new NSMutableArray();
		if (emargement != null) {
			NSArray tmp = emargement.emargementDetails();
			for (int i = 0; i < tmp.count(); i++) {
				EOEmargementDetail element = (EOEmargementDetail) tmp.objectAtIndex(i);

				if (ZConst.SENS_CREDIT.equals(element.destination().ecdSens())) {
					if (res.indexOfObject(element.destination()) == NSArray.NotFound) {
						res.addObject(element.destination());
						//	                    emargementDetailCredits.addObject(element);
					}
				}
				if (ZConst.SENS_CREDIT.equals(element.source().ecdSens())) {
					if (res.indexOfObject(element.source()) == NSArray.NotFound) {
						res.addObject(element.source());
						//	                    emargementDetailCredits.addObject(element);
					}
				}
			}
		}
		//	    ecritureDetailCredits = res;
		return res;
	}

	private final class ActionAnnulerEmargement extends AbstractAction {
		/**
		 * @param name
		 */
		public ActionAnnulerEmargement() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_TRAITEMENT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'émargement sélectionné");
			setEnabled(ACTION_ANNULER.isEnabled());
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				ZActionCtrl.checkActionWithExeStat(ACTION_ANNULER);
				annulerCurrentEmargement();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	//    private final class ActionFermer extends AbstractAction {
	//        /**
	//         * @param name
	//         */
	//        public ActionFermer() {
	//            super("Fermer");
	//            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
	//        }
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            getMyDialog().onCloseClick();
	//        }
	//
	//    }

	private final class EmargementFilterPanelListener implements EmargementSrchFilterPanel.IEmargementFilterPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchFilterPanel.IEmargementFilterPanelListener#getActionSrch()
		 */
		public Action getActionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchFilterPanel.IEmargementFilterPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSrchFilterPanel.IEmargementFilterPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

	}

	private final class CreditListListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getCreditsForEmargement();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class DebitListListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getDebitsForEmargement();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class EmargementListPanelListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			try {
				refreshActions();
				myPanel.getDebitListPanel().updateData();
				myPanel.getCreditListPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getEmargements();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}
}
