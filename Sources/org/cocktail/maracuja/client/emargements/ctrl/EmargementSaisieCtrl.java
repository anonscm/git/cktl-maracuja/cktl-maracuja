/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.emargements.ui.EmargementSaisieDetailCreditPanel;
import org.cocktail.maracuja.client.emargements.ui.EmargementSaisieDetailDebitPanel;
import org.cocktail.maracuja.client.emargements.ui.EmargementSaisieEcrFilterPanel;
import org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderEmargement;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'émargement";
	private static final Dimension WINDOW_DIMENSION = new Dimension(1000, 700);

	private NSArray gestions;
	//    private NSArray comptabilites;
	//    private ZEOComboBoxModel gestionsModel;
	private static final String ACTION_ID_SAISIE = ZActionCtrl.IDU_COEM;

	private EmargementSaisiePanel emargementSaisiePanel;
	private EmargementSaisiePanelListener emargementSaisiePanelListener;

	private EmargementSaisieEcrFilterPanel myEmargementEcrFilterPanel;
	private EmargementSaisieDetailCreditPanel myEmargementDetailCreditPanel;
	private EmargementSaisieDetailDebitPanel myEmargementDetailDebitPanel;

	private EmargementEcrFilterPanelListener myEmargementEcrFilterPanelListener;
	private EmargementDetailCreditPanelListener myEmargementDetailCreditPanelListener;
	private EmargementDetailDebitPanelListener myEmargementDetailDebitPanelListener;
	private ZPanelBalanceProvider myZPanelBalanceProvider;
	private ZPanelBalance myZPanelBalance;

	private NSArray planComptables;

	private HashMap ecrFilterValues;
	private HashMap debitFilterValues;
	private HashMap creditFilterValues;

	private HashMap pcoMap;

	private PcoSelectDlg planComptableSelectionDialog;

	private final NSMutableArray emargementsCrees = new NSMutableArray();
	private final FactoryEmargementProxy tmpFactoryEmargementProxy = new FactoryEmargementProxy(getEditingContext(), myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(), FinderEmargement.leTypeLettrage(getEditingContext()), myApp.wantShowTrace(), getDateJourneeComptable());
	private NSArray sacds;

	//    private final NSArray sacds;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public EmargementSaisieCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		//        planComptables = EOPlanComptableFinder.getPlancosValidesEmargeables(getEditingContext(), false);
		planComptables = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), EOPlanComptableExer.QUAL_EMARGEABLE, false);

		pcoMap = new HashMap(planComptables.count());
		for (int i = 0; i < planComptables.count(); i++) {
			EOPlanComptableExer element = (EOPlanComptableExer) planComptables.objectAtIndex(i);
			pcoMap.put(element.pcoNum(), element.pcoLibelle());
		}

		gestions = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_SAISIE);
		if (gestions.count() == 0) {
			throw new DefaultClientException(
					"Vous n'avez pas suffisamment de droits pour saisir un émargement. Demandez à votre administrateur de vous affecter les codes de gestions pour cette fonction.");
		}
		ecrFilterValues = new HashMap();
		debitFilterValues = new HashMap();
		creditFilterValues = new HashMap();

		ecrFilterValues.put("comptabilite", getComptabilite());

		myEmargementEcrFilterPanelListener = new EmargementEcrFilterPanelListener();
		myEmargementDetailCreditPanelListener = new EmargementDetailCreditPanelListener();
		myEmargementDetailDebitPanelListener = new EmargementDetailDebitPanelListener();
		myEmargementEcrFilterPanel = new EmargementSaisieEcrFilterPanel(myEmargementEcrFilterPanelListener);
		myEmargementDetailCreditPanel = new EmargementSaisieDetailCreditPanel(myEmargementDetailCreditPanelListener);
		myEmargementDetailDebitPanel = new EmargementSaisieDetailDebitPanel(myEmargementDetailDebitPanelListener);
		myZPanelBalanceProvider = new ZPanelBalanceProvider();
		myZPanelBalance = new ZPanelBalance(myZPanelBalanceProvider, true);

		emargementSaisiePanelListener = new EmargementSaisiePanelListener();
		emargementSaisiePanel = new EmargementSaisiePanel(emargementSaisiePanelListener, myEmargementEcrFilterPanel, myEmargementDetailCreditPanel, myEmargementDetailDebitPanel, myZPanelBalance);

	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		planComptableSelectionDialog = createPlanComptableSelectionDialog();

		//        emargementSaisiePanel.setMyDialog(getMyDialog());
		emargementSaisiePanel.initGUI();
		emargementSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(emargementSaisiePanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final NSArray openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return emargementsCrees;
	}

	/**
	 * Ouvre un dialogue de création d'émargement avec préselection des données.
	 * 
	 * @param dial
	 * @param ecritureDetail
	 * @return
	 */
	public final NSArray openDialog(Window dial, EOEcritureDetail ecritureDetail) {
		if (ecritureDetail == null) {
			return openDialog(dial);
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PLAN_COMPTABLE_KEY + "=%@", new NSArray(new Object[] {
				ecritureDetail.planComptable()
		}));
		if (EOQualifier.filteredArrayWithQualifier(planComptables, qual).count() == 0) {
			showInfoDialog("Le compte " + ecritureDetail.planComptable().pcoNum() + " n'est pas défini comme émargeable dans le plan comptable.");
			return null;
		}
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			final EOPlanComptable planComptable = ecritureDetail.planComptable();
			final BigDecimal resteEmarger = ecritureDetail.ecdResteEmarger();

			ecrFilterValues.put(EOEcritureDetail.PLAN_COMPTABLE_KEY, planComptable);
			if (EOEcritureDetail.SENS_DEBIT.equals(ecritureDetail.ecdSens())) {
				debitFilterValues.put("ecrNumeroMin", ecritureDetail.ecriture().ecrNumero());
				debitFilterValues.put("ecrNumeroMax", ecritureDetail.ecriture().ecrNumero());
				debitFilterValues.put("ecdResteEmargerMin", resteEmarger);
				debitFilterValues.put("ecdResteEmargerMax", resteEmarger);
				creditFilterValues.put("ecdResteEmargerMin", resteEmarger);
				changeFiltreMain();
				myEmargementDetailDebitPanel.setSelectedObject(ecritureDetail);
			}
			else {
				creditFilterValues.put("ecrNumeroMin", ecritureDetail.ecriture().ecrNumero());
				creditFilterValues.put("ecrNumeroMax", ecritureDetail.ecriture().ecrNumero());
				creditFilterValues.put("ecdResteEmargerMin", resteEmarger);
				creditFilterValues.put("ecdResteEmargerMax", resteEmarger);
				debitFilterValues.put("ecdResteEmargerMin", resteEmarger);
				changeFiltreMain();
				myEmargementDetailCreditPanel.setSelectedObject(ecritureDetail);
			}

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return emargementsCrees;
	}

	/**
	 * Ferme la fenetre de saisie avec confirmation si des changements ne sont pas enregistres.
	 */
	private void fermer() {
		if (getEditingContext().hasChanges()) {
			if (!showConfirmationDialog("Confirmation", "Vous n'avez pas validé l'émargement en cours de saisie, souhaitez-vous néammoins quitter cette fenêtre ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}

	/**
	 * Procedure d'emargement (dialogue avec la factory emargement)
	 */
	private void emarger() {
		NSArray lesCredits = getSelectedCredits();
		NSArray lesDebits = getSelectedDebits();

		final NSArray debitsIds = ZEOUtilities.globalIDsForObjects(getEditingContext(), lesDebits);
		final NSArray creditsIds = ZEOUtilities.globalIDsForObjects(getEditingContext(), lesCredits);

		getEditingContext().invalidateObjectsWithGlobalIDs(debitsIds);
		getEditingContext().invalidateObjectsWithGlobalIDs(creditsIds);

		try {
			if (lesCredits.count() == 0 && lesDebits.count() == 0) {
				throw new DataCheckException("Rien n'est sélectionné pour l'émargement. Au moins deux écritures doivent être sélectionnées pour réaliser un émargement.");
			}

			//Vérifier si les ecritures sont inter-composantes
			final NSMutableArray gestionsCredits = new NSMutableArray();
			final NSMutableArray gestionsDebits = new NSMutableArray();
			for (int i = 0; i < lesCredits.count(); i++) {
				final EOEcritureDetail element = (EOEcritureDetail) lesCredits.objectAtIndex(i);
				if (gestionsCredits.indexOfObject(element.gestion()) == NSArray.NotFound) {
					gestionsCredits.addObject(element.gestion());
				}
			}

			for (int i = 0; i < lesDebits.count(); i++) {
				final EOEcritureDetail element = (EOEcritureDetail) lesDebits.objectAtIndex(i);
				if (gestionsCredits.indexOfObject(element.gestion()) == NSArray.NotFound && gestionsDebits.indexOfObject(element.gestion()) == NSArray.NotFound) {
					gestionsDebits.addObject(element.gestion());
				}
			}

			NSArray gestionAll = ZEOUtilities.unionOfNSArrays(new NSArray[] {
					gestionsDebits, gestionsCredits
			});
			gestionAll = ZEOUtilities.getDistinctsOfNSArray(gestionAll);

			//Si SACD, rester dans le SACD
			boolean plusieursCompta = false;
			NSArray lesSacdsDebits = FactoryProcessJournalEcriture.lesSacds(getEditingContext(), lesDebits);
			NSArray lesSacdsCredits = FactoryProcessJournalEcriture.lesSacds(getEditingContext(), lesCredits);
			NSArray sacdsAll = ZEOUtilities.unionOfNSArrays(new NSArray[] {
					lesSacdsDebits, lesSacdsCredits
			});
			sacdsAll = ZEOUtilities.getDistinctsOfNSArray(sacdsAll);

			if (sacdsAll.count() > 1) {
				plusieursCompta = true;
			}
			else if (sacdsAll.count() == 1 && (gestionAll.count() != 1 || !gestionAll.objectAtIndex(0).equals(sacdsAll.objectAtIndex(0)))) {
				plusieursCompta = true;
			}
			if (plusieursCompta) {
				throw new DataCheckException("Emargement impossible entre des écritures passées sur un SACD et des écritures passées hors du SACD.");
			}
			if ((gestionsCredits.count() == 0 && gestionsDebits.count() > 1) || (gestionsCredits.count() > 1 && gestionsDebits.count() == 0) || (gestionsCredits.count() >= 1 && gestionsDebits.count() >= 1)) {
				if (!showConfirmationDialog("Confirmation", "Les écritures sélectionnées ne sont pas sur la même composante. Si vous confirmer, vous risquez ensuite d'avoir des erreurs dans les états de développement de solde.\nSouhaitez-vous néammoins réaliser l'émargement ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}
			}

			//Detecter le mode d'emargement
			int modeEmargement = tmpFactoryEmargementProxy.getModeEmargement(lesDebits, lesCredits);

			BigDecimal userMontantLocal = null;

			if ((modeEmargement == FactoryEmargementProxy.EMARGEMENT_D1_C1) || (modeEmargement == FactoryEmargementProxy.EMARGEMENT_D1_C1_IDEM)) {
				userMontantLocal = emargementSaisiePanelListener.getUserMontant().setScale(2, BigDecimal.ROUND_HALF_UP);
			}

			final NSMutableArray ecritureGenerees = new NSMutableArray();

			NSArray newEmargements = tmpFactoryEmargementProxy.emarger(modeEmargement, lesDebits, lesCredits, userMontantLocal, getComptabilite(), ecritureGenerees);
			ZLogger.debug(newEmargements);
			//reactiver le savechanges
			getEditingContext().saveChanges();

			emargementsCrees.addObjectsFromArray(newEmargements);
			changeFiltreMain();

			String msgFin = "Emargement effectué. ";

			//On numerote l'emargement
			//Si c'est bien enregistre, on numerote et on croise les doigts
			//            KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

			try {
				if (ecritureGenerees.count() > 0) {

					for (int i = 0; i < ecritureGenerees.count(); i++) {
						final EOEcriture element = (EOEcriture) ecritureGenerees.objectAtIndex(i);
						final KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
						myKFactoryNumerotation.getNumeroEOEcriture(getEditingContext(), element);
					}
					msgFin += "\nEcriture(s) auto générée(s) " + ZEOUtilities.getCommaSeparatedListOfValues(ecritureGenerees, "ecrNumero");
				}
			} catch (Exception e) {
				System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
				e.printStackTrace();

				throw new Exception(e);
			}

			try {
				for (int i = 0; i < newEmargements.count(); i++) {
					final EOEmargement element = (EOEmargement) newEmargements.objectAtIndex(i);
					tmpFactoryEmargementProxy.numeroterEmargement(getEditingContext(), element);

				}
				msgFin += "\n\nEmargement(s) généré(s) " + ZEOUtilities.getCommaSeparatedListOfValues(newEmargements, "emaNumero");
			} catch (Exception e) {
				System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
				e.printStackTrace();

				throw new Exception(e);
			}

			showInfoDialog(msgFin);

		} catch (Exception e) {
			showErrorDialog(e);
			getEditingContext().revert();
		}

	}

	/**
	 * A Appeler pour la mise à jour des infos lors d'un changement du filtre portant sur le plancomptable et la comptabilité.
	 */
	private final void changeFiltreMain() {
		try {
			setWaitCursor(true);
			myEmargementEcrFilterPanel.updateData();
			myEmargementDetailDebitPanel.updateData();
			myEmargementDetailCreditPanel.updateData();
			selectionsChanged();

			//            sacds = EOsFinder.getSACDGestionForComptabilite(getEditingContext(), (EOComptabilite) ecrFilterValues.get("comptabilite"),getExercice());            
			sacds = EOsFinder.getSACDGestionForComptabilite(getEditingContext(), getComptabilite(), getExercice());

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void changeFiltreDebit() {
		try {
			setWaitCursor(true);
			myEmargementDetailDebitPanel.updateData();
			selectionsChanged();
			setWaitCursor(false);
			//            myEmargementDetailDebitPanel.requestFocusInWindow();
			//            myEmargementDetailDebitPanel.myEOTable.requestFocusInWindow();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private void changeFiltreCredit() {
		try {
			setWaitCursor(true);
			myEmargementDetailCreditPanel.updateData();
			selectionsChanged();
			setWaitCursor(false);
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#selectionsChanged()
	 */
	/**
	 * Modifie l'interface en fonction des selections
	 * 
	 * @throws DefaultClientException
	 */
	public void selectionsChanged() {
		emargementSaisiePanel.updateTotaux();

		try {
			final NSArray lesCredits = getSelectedCredits();
			final NSArray lesDebits = getSelectedDebits();

			emargementSaisiePanel.getUserMontantField().getMyTexfield().setEnabled(false);

			//Détecter les modes d'emargements
			FactoryEmargementProxy tmpFactoryEmargementProxy2 = new FactoryEmargementProxy(getEditingContext(), myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(),
					FinderEmargement.leTypeLettrage(getEditingContext()), myApp.wantShowTrace(), getDateJourneeComptable());
			//Detecter le mode d'emargement
			int modeEmargement = tmpFactoryEmargementProxy2.getModeEmargement(lesDebits, lesCredits);
			emargementSaisiePanelListener.userMontant = tmpFactoryEmargementProxy2.getDefaultMontantEmargement(modeEmargement, lesDebits, lesCredits);

			if ((modeEmargement == FactoryEmargementProxy.EMARGEMENT_D1_C1) || (modeEmargement == FactoryEmargementProxy.EMARGEMENT_D1_C1_IDEM)) {
				//Le montant emargement est modifiable
				emargementSaisiePanel.getUserMontantField().getMyTexfield().setEnabled(true);
			}
		} catch (DefaultClientException e) {
			showErrorDialog(e);
		}

		emargementSaisiePanel.getUserMontantField().updateData();
	}

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	}

	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		if (planComptableSelectionDialog.open(planComptables) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;
	}

	private NSArray getSelectedDebits() {
		return myEmargementDetailDebitPanel.getSelectedDetailEcritures();
	}

	private NSArray getSelectedCredits() {
		return myEmargementDetailCreditPanel.getSelectedDetailEcritures();
	}

	private class EmargementSaisiePanelListener implements EmargementSaisiePanel.IEmargementSaisiePanelListener {
		private BigDecimal userMontant;
		private final UserMontantFieldModel userMontantFieldModel = new UserMontantFieldModel();
		private final DetailSelectionModel detailSelectionModel = new DetailSelectionModel();
		private ActionClose actionClose = new ActionClose();
		private ActionEmarger actionEmarger = new ActionEmarger();

		/**
		 * @return
		 */
		public BigDecimal getUserMontant() {
			return userMontant;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.EcritureSaisiePanel.IEcritureSaisiePanelListener#onCancel()
		 */
		public void onCancel() {
			getMyDialog().onCancelClick();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.EcritureSaisiePanel.IEcritureSaisiePanelListener#onClose()
		 */
		public void onClose() {
			getMyDialog().onCloseClick();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#getPlanComptables()
		 */
		public NSArray getPlanComptables() {
			return planComptables;
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#getComptabilites()
		//         */
		//        public NSArray getComptabilites() {
		//            return comptabilites;
		//        }

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#getGestions()
		 */
		public NSArray getGestions() {
			return gestions;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#actionEmarger()
		 */
		public Action actionEmarger() {
			return actionEmarger;
		}

		/**
		 * @throws DefaultClientException
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#selectionsChanged()
		 */
		public void selectionsChanged() {
			EmargementSaisieCtrl.this.selectionsChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#getSetailSelectionModel()
		 */
		public IZTextFieldModel getSetailSelectionModel() {
			return detailSelectionModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisiePanel.IEmargementSaisiePanelListener#getUserMontantFieldModel()
		 */
		public IZTextFieldModel getUserMontantFieldModel() {
			return userMontantFieldModel;
		}

		private class DetailSelectionModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return getSelectedDebits().count() + " débit(s) / " + getSelectedCredits().count() + " crédit(s)";
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {

			}

		}

		private final class UserMontantFieldModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return userMontant;
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				if (value != null) {
					userMontant = new BigDecimal(((Number) value).doubleValue());
				}
			}

		}

		private final class ActionEmarger extends AbstractAction {
			/**
             *
             */
			public ActionEmarger() {
				super("Emarger");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_TRAITEMENT_16));
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				emarger();
			}
		}

		public final class ActionClose extends AbstractAction {

			public ActionClose() {
				super("Fermer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				fermer();
			}

		}

	}

	private final class ZPanelBalanceProvider implements ZPanelBalance.IZPanelBalanceProvider {

		/**
		 * @see org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider#getDebitValue()
		 */
		public BigDecimal getDebitValue() {
			return tmpFactoryEmargementProxy.calcResteEmargerDebits(getSelectedDebits());
		}

		/**
		 * @see org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider#getCreditValue()
		 */
		public BigDecimal getCreditValue() {
			return tmpFactoryEmargementProxy.calcResteEmargerCredits(getSelectedCredits());
		}

	}

	private final class EmargementEcrFilterPanelListener implements EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener {
		private ActionFiltrerMain actionFiltrerMain = new ActionFiltrerMain();

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener#getMyValues()
		 */
		public HashMap getMyValues() {
			return ecrFilterValues;
		}

		//
		//        /**
		//         * @see org.cocktail.maracuja.client.emargements.EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener#getComptabilites()
		//         */
		//        public NSArray getComptabilites() {
		//            return comptabilites;
		//        }

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener#getPlanComptables()
		 */
		public NSArray getPlanComptables() {
			return planComptables;
		}

		//        /**
		//         * Initialise les valeurs par defaut des listes deroulantes.
		//         */
		//        private void initDefaultValues() {
		//            ecrFilterValues.put("comptabilite", comptabilites.objectAtIndex(0));
		//
		//        }

		private final class ActionFiltrerMain extends AbstractAction {
			/**
             *
             */
			public ActionFiltrerMain() {
				super("Filtrer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				analysePcoNum();
				changeFiltreMain();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener#actionFiltrer()
		 */
		public Action actionFiltrer() {
			return actionFiltrerMain;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener#getPcoMap()
		 */
		public Map getPcoMap() {
			return pcoMap;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisieEcrFilterPanel.IEmargementEcrFilterPanelListener#pcoNumChanged()
		 */
		public void pcoNumChanged() {
			myEmargementDetailCreditPanel.clearData();
			myEmargementDetailDebitPanel.clearData();

		}

	}

	private final class EmargementDetailCreditPanelListener implements EmargementSaisieDetailCreditPanel.IEmargementDetailCreditPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			selectionsChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#onFiltrer()
		 */
		public void onFiltrer() {
			changeFiltreCredit();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getMyFilterValues()
		 */
		public HashMap getMyFilterValues() {
			return creditFilterValues;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getComptabilite()
		 */
		public EOComptabilite getComptabilite() {
			return (EOComptabilite) ecrFilterValues.get("comptabilite");
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getExercice()
		 */
		public EOExercice getExercice() {
			return myApp.appUserInfo().getCurrentExercice();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getPlanComptable()
		 */
		public EOPlanComptable getPlanComptable() {
			return (EOPlanComptable) ecrFilterValues.get(EOEcritureDetail.PLAN_COMPTABLE_KEY);
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

		public EOEditingContext getEditingContext() {
			return EmargementSaisieCtrl.this.getEditingContext();
		}

	}

	private final class EmargementDetailDebitPanelListener implements EmargementSaisieDetailDebitPanel.IEmargementDetailDebitPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			selectionsChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#onFiltrer()
		 */
		public void onFiltrer() {
			changeFiltreDebit();

		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getMyFilterValues()
		 */
		public HashMap getMyFilterValues() {
			return debitFilterValues;
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getComptabilite()
		 */
		public EOComptabilite getComptabilite() {
			return (EOComptabilite) ecrFilterValues.get("comptabilite");
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getExercice()
		 */
		public EOExercice getExercice() {
			return myApp.appUserInfo().getCurrentExercice();
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getPlanComptable()
		 */
		public EOPlanComptable getPlanComptable() {
			return (EOPlanComptable) ecrFilterValues.get(EOEcritureDetail.PLAN_COMPTABLE_KEY);
		}

		/**
		 * @see org.cocktail.maracuja.client.emargements.ui.EmargementSaisieDetailPanel.IEmargementDetailPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

		public EOEditingContext getEditingContext() {
			return EmargementSaisieCtrl.this.getEditingContext();
		}

	}

	public NSArray getPlanComptables() {
		return planComptables;
	}

	public void analysePcoNum() {

		EOPlanComptable res = null;
		String pconum = myEmargementEcrFilterPanel.getEcdPcoNumField().getMyTexfield().getText();
		EOPlanComptableExer restmp = EOPlanComptableExerFinder.findPlanComptableExerForPcoNumInList(getPlanComptables(), pconum);
		if (restmp == null) {
			res = selectPlanComptableIndDialog(pconum);
		}
		else {
			res = restmp.planComptable();
		}
		ecrFilterValues.put(EOEcritureDetail.PLAN_COMPTABLE_KEY, res);
		myEmargementEcrFilterPanel.getEcdPcoNumField().updateData();

		//        EOPlanComptable res=null;
		//        String pconum = myEmargementEcrFilterPanel.getEcdPcoNumField().getMyTexfield().getText();
		//            NSArray tmp = EOQualifier.filteredArrayWithQualifier( getPlanComptables(), EOQualifier.qualifierWithQualifierFormat("pcoNum like '"+pconum+"*'" ,null) );
		//            if (tmp.count()==1) {
		//                //pas de doute sur le choix du pcoNum
		//                res = (EOPlanComptable) tmp.objectAtIndex(0);
		//            }
		//            else {
		//               //on affiche une fenetre de recherche de PlanComptable
		//                res = selectPlanComptableIndDialog( myEmargementEcrFilterPanel.getEcdPcoNumField().getMyTexfield().getText());
		//            }
		//            ecrFilterValues.put(EOEcritureDetail.PLAN_COMPTABLE_KEY, res);
		////        }
		//          myEmargementEcrFilterPanel.getEcdPcoNumField().updateData();
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return emargementSaisiePanel;
	}

	public String title() {
		return TITLE;
	}

}
