/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.emargements.ctrl;

import java.math.BigDecimal;
import java.util.Iterator;

import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessEmargement;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOTypeEmargement;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZNumberUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


public final class FactoryEmargementProxy {
	/**
         * 
         */
	//        private final EmargementSaisieCtrl ctrl;
	public static final int EMARGEMENT_UNKNOWN = 0;
	public static final int EMARGEMENT_D1_C1 = 1;
	public static final int EMARGEMENT_D1_C1_IDEM = 2;
	public static final int EMARGEMENT_D1_Cn = 3;
	public static final int EMARGEMENT_Dn_C1 = 4;
	public static final int EMARGEMENT_Dn_Cn = 5;
	public static final int EMARGEMENT_D0_Cn = 6;
	public static final int EMARGEMENT_Dn_C0 = 7;
	public static final int EMARGEMENT_D0_C2 = 8;
	public static final int EMARGEMENT_D2_C0 = 9;

	private EOEditingContext _ec;
	private EOUtilisateur _utilisateur;
	private EOExercice _exercice;
	private EOTypeEmargement _typeEmargement;
	//        private EOComptabilite _comptabilite;

	private FactoryProcessEmargement myFactoryProcessEmargement;

	private final boolean _wantShowTrace;

	/**
         *
         */
	public FactoryEmargementProxy(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOExercice exercice, final EOTypeEmargement typeEmargement, final boolean wantShowTrace, final java.util.Date dateJourneeComptable) {
		//            this.ctrl = ctrl;
		_ec = ec;
		_utilisateur = utilisateur;
		_exercice = exercice;
		_typeEmargement = typeEmargement;
		//            _comptabilite = comptabilite;
		_wantShowTrace = wantShowTrace;
		myFactoryProcessEmargement = new FactoryProcessEmargement(_wantShowTrace, new NSTimestamp(dateJourneeComptable));

	}

	public final void numeroterEmargement(final EOEditingContext ec, EOEmargement emargement) throws Exception {
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(_wantShowTrace);
		myFactoryProcessEmargement.numeroterEmargement(ec, emargement, myKFactoryNumerotation);
	}

	/**
	 * Renvoie le montant de l'emargement "calculé" d'apres les debits et crédits et d'apres le mode d'emargement indiqué en parametre.
	 * 
	 * @param mode
	 * @param lesDebits
	 * @param lesCredits
	 * @return
	 */
	public final BigDecimal getDefaultMontantEmargement(final int mode, final NSArray lesDebits, final NSArray lesCredits) {
		final BigDecimal resteEmargerCredits = calcResteEmargerCredits(lesCredits);
		final BigDecimal resteEmargerDebits = calcResteEmargerDebits(lesDebits);
		BigDecimal montantEmargement = null;
		switch (mode) {
		case EMARGEMENT_D1_C1_IDEM:
		case EMARGEMENT_D1_C1:
		case EMARGEMENT_D1_Cn:
		case EMARGEMENT_Dn_C1:
		case EMARGEMENT_Dn_Cn:
			montantEmargement = ZNumberUtil.plusPetitValeurAbsolue(resteEmargerDebits, resteEmargerCredits);
			break;
		case EMARGEMENT_D0_C2:
			montantEmargement = ((EOEcritureDetail) lesCredits.objectAtIndex(0)).ecdResteEmarger();
			if (((EOEcritureDetail) lesCredits.objectAtIndex(1)).ecdResteEmarger().compareTo(montantEmargement) < 0) {
				montantEmargement = ((EOEcritureDetail) lesCredits.objectAtIndex(1)).ecdResteEmarger();
			}
			break;
		case EMARGEMENT_D2_C0:
			montantEmargement = ((EOEcritureDetail) lesDebits.objectAtIndex(0)).ecdResteEmarger();
			if (((EOEcritureDetail) lesDebits.objectAtIndex(1)).ecdResteEmarger().compareTo(montantEmargement) < 0) {
				montantEmargement = ((EOEcritureDetail) lesDebits.objectAtIndex(1)).ecdResteEmarger();
			}
			break;
		case EMARGEMENT_D0_Cn:
			montantEmargement = new BigDecimal(0).setScale(2);
			break;
		case EMARGEMENT_Dn_C0:
			montantEmargement = new BigDecimal(0).setScale(2);
			break;
		default:
			break;
		}
		return montantEmargement;
	}

	/**
	 * @param mode
	 * @param lesDebits
	 * @param lesCredits
	 * @param userMontantEmargement facultatif
	 * @param _comptabilite
	 * @param ecrituresGenerees
	 * @return
	 * @throws Exception
	 */
	public final NSArray emarger(final int mode, final NSArray lesDebits, final NSArray lesCredits, final BigDecimal userMontantEmargement, final EOComptabilite _comptabilite, final NSMutableArray ecrituresGenerees) throws Exception {
		BigDecimal montantEmargement = userMontantEmargement;
		final BigDecimal resteEmargerCredits = calcResteEmargerCredits(lesCredits);
		final BigDecimal resteEmargerDebits = calcResteEmargerDebits(lesDebits);

		final BigDecimal resteEmargerCreditsAbs = resteEmargerCredits.abs();
		final BigDecimal resteEmargerDebitsAbs = resteEmargerDebits.abs();

		//
		System.out.println("resteEmargerCredits: " + resteEmargerCredits);
		System.out.println("resteEmargerDebits: " + resteEmargerDebits);

		System.out.println("montantEmargement: " + montantEmargement);
		ecrituresGenerees.removeAllObjects();
		Integer numero = new Integer(0);
		final NSMutableArray res = new NSMutableArray();
		switch (mode) {
		case EMARGEMENT_D1_C1_IDEM:
		case EMARGEMENT_D1_C1:
			//Vérifier qu'on a le même signe
			checkSameSignum(lesDebits, lesCredits);

			//Vérifier que le montant est correct
			if (montantEmargement == null) {
				montantEmargement = ZNumberUtil.plusPetitValeurAbsolue(resteEmargerDebits, resteEmargerCredits).setScale(2, BigDecimal.ROUND_HALF_UP);
				System.out.println("montantEmargement: " + montantEmargement);
			}
			else if (montantEmargement.abs().compareTo(ZNumberUtil.plusPetitValeurAbsolue(resteEmargerCredits, resteEmargerDebits).abs()) > 0) {
				throw new DataCheckException("Le montant que vous avez spécifié pour l'émargement est trop grand en valeur absolue par rapport aux reste à émarger. Impossible d'émarger");
			}
			montantEmargement = montantEmargement.setScale(2, BigDecimal.ROUND_HALF_UP);
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailD1C1(_ec, _utilisateur, _typeEmargement, _exercice, (EOEcritureDetail) lesCredits.objectAtIndex(0), (EOEcritureDetail) lesDebits.objectAtIndex(0), numero, _comptabilite, montantEmargement));

			System.out.println("ecr...");
			System.out.println(ecrituresGenerees);
			break;

		case EMARGEMENT_D1_Cn:
			//Vérifier qu'on a le même signe
			checkSameSignum(lesDebits, lesCredits);
			if (!(resteEmargerDebitsAbs.compareTo(resteEmargerCreditsAbs) >= 0)) {
				throw new DataCheckException("Le reste à émarger du débit doit être supérieur au reste à émarger des crédits. Impossible d'émarger");
			}
			montantEmargement = ZNumberUtil.plusPetitValeurAbsolue(resteEmargerDebits, resteEmargerCredits);
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailD1Cn(_ec, _utilisateur, _typeEmargement, _exercice, lesCredits, (EOEcritureDetail) lesDebits.objectAtIndex(0), numero, _comptabilite, montantEmargement));
			break;

		case EMARGEMENT_Dn_C1:
			//Vérifier qu'on a le même signe
			checkSameSignum(lesDebits, lesCredits);
			if (!(resteEmargerCreditsAbs.compareTo(resteEmargerDebitsAbs) >= 0)) {
				throw new DataCheckException("Le reste à émarger du crédit doit être supérieur au reste à émarger des débits. Impossible d'émarger");
			}
			montantEmargement = ZNumberUtil.plusPetitValeurAbsolue(resteEmargerDebits, resteEmargerCredits);
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailDnC1(_ec, _utilisateur, _typeEmargement, _exercice, (EOEcritureDetail) lesCredits.objectAtIndex(0), lesDebits, numero, _comptabilite, montantEmargement));
			break;

		case EMARGEMENT_Dn_Cn:
			//Vérifier que les montants sont identiques
			if (resteEmargerCredits.compareTo(resteEmargerDebits) != 0) {
				throw new DataCheckException("Dans le cas d'un émargement n débits / n crédits, les restes à émarger doivent être identiques. Impossible d'émarger");
			}
			montantEmargement = ZNumberUtil.plusPetitValeurAbsolue(resteEmargerDebits, resteEmargerCredits);
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailDnCn(_ec, _utilisateur, _typeEmargement, _exercice, lesCredits, lesDebits, numero, _comptabilite, montantEmargement));
			break;

		case EMARGEMENT_D0_C2:
			//Vérifier que la somme est nulle
			//			if (resteEmargerCredits.doubleValue() != 0) {
			//				throw new DataCheckException("Dans le cas d'un émargement 0 débit / 2 crédits, la somme des crédits restant à émarger doit être nulle.");
			//			}

			//Verifier qu'on a un credit + et un crédit -
			if (((EOEcritureDetail) lesCredits.objectAtIndex(0)).ecdCredit().multiply(((EOEcritureDetail) lesCredits.objectAtIndex(1)).ecdCredit()).signum() > 0) {
				throw new DataCheckException("Dans le cas d'un émargement 0 débit / 2 crédits, les crédits doivent être de signes différents");
			}
			montantEmargement = ((EOEcritureDetail) lesCredits.objectAtIndex(0)).ecdResteEmarger();
			if (((EOEcritureDetail) lesCredits.objectAtIndex(1)).ecdResteEmarger().compareTo(montantEmargement) < 0) {
				montantEmargement = ((EOEcritureDetail) lesCredits.objectAtIndex(1)).ecdResteEmarger();
			}
			//montantEmargement = ((EOEcritureDetail) lesCredits.objectAtIndex(0)).ecdResteEmarger().abs();
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailD0C2(_ec, _utilisateur, _typeEmargement, _exercice, (EOEcritureDetail) lesCredits.objectAtIndex(0), (EOEcritureDetail) lesCredits.objectAtIndex(1), numero, _comptabilite, montantEmargement));
			break;

		case EMARGEMENT_D2_C0:
			//Vérifier que la somme est nulle
			//Verifier qu'on a un credit + et un crédit -
			if (((EOEcritureDetail) lesDebits.objectAtIndex(0)).ecdDebit().multiply(((EOEcritureDetail) lesDebits.objectAtIndex(1)).ecdDebit()).signum() > 0) {
				throw new DataCheckException("Dans le cas d'un émargement 2 débits / 0 crédit, les débits doivent être de signes différents");
			}
			montantEmargement = ((EOEcritureDetail) lesDebits.objectAtIndex(0)).ecdResteEmarger();
			if (((EOEcritureDetail) lesDebits.objectAtIndex(1)).ecdResteEmarger().compareTo(montantEmargement) < 0) {
				montantEmargement = ((EOEcritureDetail) lesDebits.objectAtIndex(1)).ecdResteEmarger();
			}
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailD2C0(_ec, _utilisateur, _typeEmargement, _exercice, (EOEcritureDetail) lesDebits.objectAtIndex(0), (EOEcritureDetail) lesDebits.objectAtIndex(1), numero, _comptabilite, montantEmargement));
			break;

		case EMARGEMENT_D0_Cn:
			//Vérifier que la somme est nulle
			if (resteEmargerCredits.doubleValue() != 0) {
				throw new DataCheckException("Dans le cas d'un émargement 0 débit / n crédits, la somme des restes à émarger des crédits doit être nulle.");
			}

			montantEmargement = new BigDecimal(0).setScale(2);
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailD0CN(_ec, _utilisateur, _typeEmargement, _exercice, lesCredits, numero, _comptabilite, montantEmargement));
			break;

		case EMARGEMENT_Dn_C0:
			//Vérifier que la somme est nulle
			if (resteEmargerDebits.doubleValue() != 0) {
				throw new DataCheckException("Dans le cas d'un émargement n débits / 0 crédit, la somme des restes à émarger des débits doit être nulle.");
			}

			montantEmargement = new BigDecimal(0).setScale(2);
			res.addObject(myFactoryProcessEmargement.emargerEcritureDetailDNC0(_ec, _utilisateur, _typeEmargement, _exercice, lesDebits, numero, _comptabilite, montantEmargement));
			break;

		default:
			throw new DataCheckException("Mode d'émargement non reconnu. Impossible d'émarger");
		}
		return res;
	}

	private final void checkSameSignum(NSArray debits, NSArray credits) throws DataCheckException {
		if (!isSameSignNum(debits, credits)) {
			throw new DataCheckException("Les restes à émarger des débits et crédits ne sont pas du même signe. Impossible d'émarger.");
		}
	}

	/**
	 * @param details
	 * @return True si les montant des details ecritures sont du même signe.
	 */
	private final boolean isSameSignum(NSArray details) {
		if (details.count() > 0) {
			int s = ((EOEcritureDetail) details.objectAtIndex(0)).ecdMontant().signum();
			;
			for (int i = 0; i < details.count(); i++) {
				EOEcritureDetail element = ((EOEcritureDetail) details.objectAtIndex(i));
				if (s != element.ecdMontant().signum()) {
					return false;
				}
			}
		}
		return true;
	}

	//        private final boolean isSameSignum(NSArray details) {
	//            for (int i = 0; i < details.count(); i++) {
	//                EOEcritureDetail element = ((EOEcritureDetail)details.objectAtIndex(i));
	//                if (element.ecdResteEmarger().compareTo(element.ecdResteEmarger().abs())!=0) {
	//                    return false;
	//                }
	//            }
	//            return true;
	//        }

	/**
	 * Vérifie que tous les debits et crédits sont du meme signe.
	 * 
	 * @param debits
	 * @param credits
	 * @return
	 */
	private final boolean isSameSignNum(NSArray debits, NSArray credits) {
		NSMutableArray tmp = new NSMutableArray();
		tmp.addObjectsFromArray(debits);
		tmp.addObjectsFromArray(credits);
		return isSameSignum(tmp);
	}

	/**
	 * Analyse les choix utilisateurs et indique dans quel mode d'emargement on se situe (D1/C1, D1/Cn, etc.)
	 * 
	 * @param lesDebits
	 * @param lesCredits
	 * @return
	 */
	public final int getModeEmargement(final NSArray lesDebits, final NSArray lesCredits) {
		int mode = EMARGEMENT_UNKNOWN;

		final int nbCredits = lesCredits.count();
		final int nbDebits = lesDebits.count();

		final BigDecimal resteEmargerCredits = calcResteEmargerCredits(lesCredits);
		final BigDecimal resteEmargerDebits = calcResteEmargerDebits(lesDebits);

		if (nbDebits == 0 && nbCredits == 0) {
			mode = EMARGEMENT_UNKNOWN;
		}
		//1D / 1C idem
		else if (nbDebits == 1 && nbCredits == 1 && resteEmargerCredits.compareTo(resteEmargerDebits) == 0) {
			mode = EMARGEMENT_D1_C1_IDEM;
		}
		// 1D / 1C
		else if (nbDebits == 1 && nbCredits == 1) {
			mode = EMARGEMENT_D1_C1;
		}
		else if (nbDebits == 1 && nbCredits > 1) {
			mode = EMARGEMENT_D1_Cn;
		}
		else if (nbDebits == 1 && nbCredits > 1) {
			mode = EMARGEMENT_D1_Cn;
		}
		else if (nbDebits > 1 && nbCredits == 1) {
			mode = EMARGEMENT_Dn_C1;
		}
		else if (nbDebits == 0 && nbCredits > 1) {
			if (nbCredits == 2) {
				mode = EMARGEMENT_D0_C2;
			}
			else {
				mode = EMARGEMENT_D0_Cn;
			}
		}
		else if (nbDebits > 1 && nbCredits == 0) {
			if (nbDebits == 2) {
				mode = EMARGEMENT_D2_C0;
			}
			else {
				mode = EMARGEMENT_Dn_C0;
			}
		}
		else if (nbDebits > 1 && nbCredits > 1) {
			mode = EMARGEMENT_Dn_Cn;
		}
		return mode;
	}

	public BigDecimal calcResteEmargerDebits(NSArray debits) {
		BigDecimal res = new BigDecimal(0).setScale(2);
		Iterator iter = debits.vector().iterator();
		while (iter.hasNext()) {
			EOEcritureDetail element = (EOEcritureDetail) iter.next();
			res = res.add(element.ecdResteEmarger().multiply(new BigDecimal(element.ecdDebit().signum())));
		}
		return res;
	}

	public BigDecimal calcResteEmargerCredits(NSArray credits) {
		BigDecimal res = new BigDecimal(0).setScale(2);
		final Iterator iter = credits.vector().iterator();
		while (iter.hasNext()) {
			final EOEcritureDetail element = (EOEcritureDetail) iter.next();
			res = res.add(element.ecdResteEmarger().multiply(new BigDecimal(element.ecdCredit().signum())));
		}
		return res;
	}

}
