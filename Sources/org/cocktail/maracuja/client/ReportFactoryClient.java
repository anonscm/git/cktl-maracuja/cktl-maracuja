/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.common.util.DateConversionUtil;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.cheques.BdChequeSrchCtrl;
import org.cocktail.maracuja.client.impression.ZKarukeraImprCtrl;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EORecouvrement;
import org.cocktail.maracuja.client.recouvrement.sepasdd.ctrl.RecoSepaSddCtrl;
import org.cocktail.zutil.client.NumberToLettres;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZWaitingPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanel.ZWaitingPanelListener;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.joda.time.ReadablePartial;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class ReportFactoryClient {

	public final static int FORMATPDF = 1;
	public static final String PDF_MANDAT_PRELEVEMENT_SEPA = "MandatDePrelevementSepa.pdf";

	private static final ApplicationClient myApp = (ApplicationClient) ApplicationClient.sharedApplication();

	/** sert aux threads */
	private static Integer stateLock = new Integer(0);

	/** Les constantes sont aussi definies coté serveur dans DefaulReport.java */
	private static final String JASPERFILENAME_COMPTABILITEGESTION = "comptabilite_gestionv4.jasper";
	private static final String JASPERFILENAME_BTMNA = "btmna.jasper";
	private final static String JASPERFILENAME_BTTNA = "bttna.jasper";
	private final static String JASPERFILENAME_BORDEREAU_EXTOURNE = "extourneBordereauMandats.jasper";
	private final static String JASPERFILENAME_BILAN_FORMULES = "bilan_formules.jasper";
	private static final String JASPERFILENAME_MODEPAIEMENT = "modePaiementv4.jasper";
	private static final String JASPERFILENAME_ECRITUREVALIDE = "ecriture_valide.jasper";
	private final static String JASPERFILENAME_REIMP_TITRE = "reimputation_titre.jasper";
	private final static String JASPERFILENAME_REIMP_MANDAT = "reimputation_depense.jasper";
	//private final static String JASPERFILENAME_ORDRE_PAIEMENT = "ordrepaie.jasper";
	private final static String JASPERFILENAME_ORDRE_PAIEMENT = "ordrepaie_avec_acquit.jasper";
	private final static String JASPERFILENAME_ORDRE_PAIEMENT_SCOL = "ordrepaiescol.jasper";
	private final static String JASPERFILENAME_CONTENU_PAIEMENT = "contenu_paiement.jasper";
	private final static String JASPERFILENAME_PAIEMENT_RETENUES = "paiement_retenues.jasper";
	private final static String JASPERFILENAME_BORDEREAU_DISQUETTE = "b_disquette.jasper";
	private final static String JASPERFILENAME_BORDEREAU_SEPA_SCT = "bordereau_sepa_sct.jasper";
	private final static String JASPERFILENAME_BORDEREAU_SEPA_SDD = "bordereau_sepa_sdd.jasper";
	private final static String JASPERFILENAME_PRELEVEMENT_BORDEREAU = "prelev_bordereau.jasper";
	private final static String JASPERFILENAME_PRELEVEMENT_CONTENU = "prelev_contenu.jasper";
	public static final String JASPERFILENAME_BORDEREAU_CHEQUE = "BordereauCheque.jasper";
	public static final String JASPERFILENAME_BORDEREAU_CHEQUE2 = "BordereauCheque2.jasper";
	private static final String JASPERFILENAME_RELANCE_DEFAUT = "relance_sql.jasper";
	private static final String JASPERFILENAME_RELANCE_EXECUTOIRE = "relance_executoire.jasper";
	private static final String JASPERFILENAME_BDF_CALENDRIER = "bdfCalendrier.jasper";
	private final static String JASPERFILENAME_DETAIL_VIREMENT_BDF = "detail_fichier_virement_bdf.jasper";
	private final static String JASPERFILENAME_DETAIL_VIREMENT_SEPA = "detail_fichier_virement_sepa.jasper";
	private final static String JASPERFILENAME_DETAIL_PRELEVEMENT_SEPA = "detail_fichier_prelevement_sepa.jasper";
	private final static String JASPERFILENAME_PRELEVEMENT_ATTENTE = "prelev_attente.jasper";
	private final static String JASPERFILENAME_IMLISTE = "im_liste.jasper";
	private final static String JASPERFILENAME_IMLISTE_VISES = "im_liste_vises.jasper";
	private final static String JASPERFILENAME_ZANALYSE_PROBLEME = "zanalyse_probleme.jasper";
	private static final String JASPERFILENAME_PRELEVEMENT_ATTENTE_SEPA = "sepa_prelevements_en_attente.jasper";

	private static final String EPOCH_DATE = "01/01/1970";

	private static ZWaitingPanelDialog waitingDialog;
	private static ReportFactoryPrintWaitThread printThread;

	// private static ZWaitingCtrl waitingCtrl;

	public static final String imprimerBtmna(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucune objet n'a été passé au moteur d'impression");
		}
		NSMutableDictionary metadata2 = new NSMutableDictionary();
		metadata2.addEntriesFromDictionary(metadata);

		EOEnterpriseObject element = (EOEnterpriseObject) objs.objectAtIndex(0);
		metadata2.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey(EOBordereauRejet.BRJ_ORDRE_KEY), EOBordereauRejet.BRJ_ORDRE_COLKEY);
		return imprimerReportAndWait(ec, temporaryDir, metadata2, JASPERFILENAME_BTMNA, "btmna.pdf", null);
	}

	public static final String imprimerBordereauExtourne(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucune objet n'a été passé au moteur d'impression");
		}
		NSMutableDictionary metadata2 = new NSMutableDictionary();
		metadata2.addEntriesFromDictionary(metadata);

		String ids = "";
		for (int i = 0; i < objs.count(); i++) {
			EOEnterpriseObject element = (EOEnterpriseObject) objs.objectAtIndex(i);
			if (ids.length() > 0) {
				ids = ids.concat(",");
			}
			ids = ids.concat(ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey(EOBordereau.BOR_ID_KEY).toString());
		}

		metadata2.takeValueForKey(ids, "BORIDS");
		return imprimerReportAndWait(ec, temporaryDir, metadata2, JASPERFILENAME_BORDEREAU_EXTOURNE, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BORDEREAU_EXTOURNE), FORMATPDF), null);
	}

	public static final String imprimerBdfCalendrier(EOEditingContext ec, String temporaryDir, NSDictionary metadata, Window win) throws Exception {
		final Date debEx = ZDateUtil.getFirstDayOfYear(ZDateUtil.nowAsDate());
		String req = "select to_char(to_date(BDFCAL_DATE_ISO, 'YYYYMMDD'), 'WW') as bdfcal_week, to_date(substr(BDFCAL_DATE_ISO,1,6)||'01', 'YYYYMMDD') as bdf_cal_mois, to_date(BDFCAL_DATE_ISO, 'YYYYMMDD') as bdf_cal_date from maracuja.bdf_calendrier where bdfcal_DATE_ISO >='"
				+ ZConst.FORMAT_DATE_YYYYMMDD.format(debEx) + "' order by BDFCAL_DATE_ISO";
		return imprimerReportByThreadPdf(ec, temporaryDir, metadata, JASPERFILENAME_BDF_CALENDRIER, "bdf_calendrier.pdf", req, win, null);
	}

	public static final String imprimerZanalyseProbleme(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, HashMap paramRequete, Frame win, boolean execReq) throws Exception {
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		if (execReq) {
			final NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
			final Thread t = new Thread() {

				public void run() {
					try {
						ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "zanalyse.checkAllProblemes", dicoProc);
						waitingDialog.setVisible(false);
					} catch (Exception e) {
						waitingDialog.setVisible(false);
						myApp.showErrorDialog(e);
					}
				};

			};
			t.start();
			waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Analyse en cours...");
			waitingDialog.setModal(true);
			waitingDialog.setVisible(true);

		}
		metadata.takeValueForKey(exercice.exeExercice(), "EXE_ORDRE");
		return imprimerReportByThreadPdf(ec, temporaryDir, metadata, JASPERFILENAME_ZANALYSE_PROBLEME, getFileNameWithExtension("MaracujaAnalyseProblemes", FORMATPDF), null, win, null);
	}

	public static final String imprimerBilanFormules(EOEditingContext ec, String temporaryDir, NSDictionary metadata) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_BILAN_FORMULES, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BILAN_FORMULES), FORMATPDF), null);
	}

	public static final String imprimerBttna(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucune objet n'a été passé au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "brjOrdre", "bordereau_rejet.brj_ordre", objs);

		String req = "select brj_num,bordereau_rejet.ges_code,bordereau_rejet.brj_ordre, exe_exercice,titre.tit_ordre,tit_numero," +
				"titre.tit_id,tit_ht,tit_ttc,tit_motif_rejet,rec_libelle,rec_ligne_budgetaire,rec_debiteur,rec_date,org_lib ,bordereau.bor_num "
				+ " from MARACUJA.bordereau_rejet,MARACUJA.exercice,MARACUJA.titre,MARACUJA.recette,MARACUJA.v_organ2, maracuja.bordereau " +
				" where bordereau_rejet.EXE_ORDRE=exercice.EXE_ORDRE " +
				" and bordereau_rejet.BRJ_ORDRE=titre.BRJ_ORDRE " +
				" and titre.tit_id=recette.tit_id " +
				" and bordereau.bor_id = titre.bor_id" +
				" and titre.org_ordre = v_organ2.org_id (+)"
				+ " and exercice.exe_exercice=" + myApp.appUserInfo().getCurrentExercice().exeExercice().toString() + " and " + cond +
				" order by brj_ordre,tit_ordre ";

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_BTTNA, "bttna.pdf", req);
	}

	public static final String imprimerReimputationTitre(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "reiOrdre", "r.rei_ordre", objs);

		String req = "select r.REI_ORDRE,r.rei_numero,r.REI_DATE,r.REI_LIBELLE,r.PCO_NUM_ANCIEN,r.PCO_NUM_NOUVEAU,m.TIT_NUMERO,m.GES_CODE, " + "m.tit_date_remise,m.tit_ht,m.tit_ttc,e.EXE_EXERCICE, b.BOR_NUM, f.nom ADR_NOM,f.prenom ADR_PRENOM, ec.ecr_numero,ec.ecr_ordre,ed.pco_num,"
				+ "ed.ecd_libelle,ed.ecd_montant,ed.ecd_sens" + ", maracuja.api_planco.get_pco_libelle(r.pco_num_ancien, r.exe_ordre) as pco_libelle_ancien, maracuja.api_planco.get_pco_libelle(r.pco_num_nouveau, r.exe_ordre) as pco_libelle_nouveau "
				+ " from MARACUJA.reimputation r,MARACUJA.titre m,MARACUJA.bordereau b,MARACUJA.exercice e,MARACUJA.v_fournis_light f,MARACUJA.ecriture ec,MARACUJA.ecriture_detail ed" +
				" where r.EXE_ORDRE = e.EXE_ORDRE and r.TIT_ID=m.TIT_ID " + " and m.FOU_ORDRE=f.FOU_ORDRE(+)  " + " and m.bor_id=b.bor_id " + " and r.ecr_ordre = ec.ecr_ordre "
				+ " and ec.ecr_ordre = ed.ecr_ordre " + " and " + cond + " order by rei_ordre";
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_REIMP_TITRE, "reimptitre.pdf", req);
	}

	public static final String imprimerReimputationMandat(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "reiOrdre", "r.rei_ordre", objs);
		String req = "select r.REI_ORDRE,r.rei_numero,r.REI_DATE,r.REI_LIBELLE,r.PCO_NUM_ANCIEN,r.PCO_NUM_NOUVEAU,m.MAN_NUMERO," + "m.GES_CODE, m.man_date_remise,m.man_ht,m.man_ttc ,e.EXE_EXERCICE, b.BOR_NUM, f.nom ADR_NOM,f.prenom ADR_PRENOM, "
				+ "ec.ecr_numero,ec.ecr_ordre,ed.pco_num,ed.ecd_libelle,ed.ecd_montant,ed.ecd_sens " + ", maracuja.api_planco.get_pco_libelle(r.pco_num_ancien, r.exe_ordre) as pco_libelle_ancien, maracuja.api_planco.get_pco_libelle(r.pco_num_nouveau, r.exe_ordre) as pco_libelle_nouveau "
				+ "from MARACUJA.reimputation r,MARACUJA.mandat m,MARACUJA.bordereau b,MARACUJA.exercice e,MARACUJA.v_fournis_light f,MARACUJA.ecriture ec,MARACUJA.ecriture_detail ed " +
				"where r.EXE_ORDRE = e.EXE_ORDRE " + "and r.MAN_ID=m.MAN_ID " + "and m.FOU_ORDRE=f.FOU_ORDRE " + "and m.bor_id=b.bor_id " + "and r.ecr_ordre = ec.ecr_ordre "
				+ "and ec.ecr_ordre = ed.ecr_ordre " + " and " + cond + " order by rei_ordre";
		;

		return imprimerReportByThreadPdf(ec, temporaryDir, metadata, JASPERFILENAME_REIMP_MANDAT, "reimpmandat.pdf", req, win, null);
	}

	public static final String imprimerContenuPaiement(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "paiOrdre", "p.pai_ordre", objs);

		String req = "select 'DEPENSES' types, " + " 1000000+d.dep_id macle," + " t.tvi_libelle ," + " p.PAI_DATE_CREATION, p.PAI_MONTANT, p.PAI_NB_VIREMENTS, p.PAI_NUMERO," + " d.DEP_MONTANT_DISQUETTE MONTANT," + " f.nom ADR_NOM, f.prenom ADR_PRENOM, f.civilite ADR_CIVILITE,"
				+ " r.RIB_BIC, RIB_CLE, r.RIB_CODBANC, r.RIB_GUICH, r.RIB_IBAN, r.RIB_DOMICIL, r.RIB_NUM, r.RIB_TITCO," + 
				"m.ges_code, substr(d.dep_numero,1,32) || ' ' || chr(13)||chr(10) || ' DU ' || TO_CHAR(DEP_DATE_fournis,'DD/MM/YYYY') || ' MAN. ' || man_numero as dep_numero"
				+ " from MARACUJA.paiement p , MARACUJA.mandat m , MARACUJA.depense d , MARACUJA.v_rib r,MARACUJA.v_fournis_light f,MARACUJA.type_virement t " +
				" where p.pai_ordre = m.pai_ordre" + " and m.man_id = d.man_id " + " and d.DEP_MONTANT_DISQUETTE>0 " + " and m.rib_ordre_comptable = r.rib_ordre (+)" + " and m.fou_ordre = f.fou_ordre"
				+ " and p.tvi_ordre = t.tvi_ordre"
				+ " and "
				+ cond
				+ " union all"
				+ " select 'RECETTES' types,"
				+ " 2000000+d.rec_id macle,"
				+ " t.tvi_libelle ,"
				+ " p.PAI_DATE_CREATION, p.PAI_MONTANT, p.PAI_NB_VIREMENTS, p.PAI_NUMERO,"
				+ " d.REC_MONTANT_DISQUETTE MONTANT,"
				+ " f.ADR_NOM, f.ADR_PRENOM, f.ADR_CIVILITE,"
				+ " r.RIB_BIC, RIB_CLE, r.RIB_CODBANC, r.RIB_GUICH, r.RIB_IBAN, r.RIB_DOMICIL, r.RIB_NUM, r.RIB_TITCO,"
				+ "m.ges_code, to_char(d.rec_num) dep_numero"
				+ " from MARACUJA.paiement p , MARACUJA.titre m , MARACUJA.recette d , MARACUJA.v_rib r,MARACUJA.v_fournisseur f,MARACUJA.type_virement t"
				+ " where p.pai_ordre = m.pai_ordre"
				+ " and m.tit_id = d.tit_id "
				+ " and m.rib_ordre_comptable = r.rib_ordre (+)"
				+ " and m.fou_ordre = f.fou_ordre"
				+ " and p.tvi_ordre = t.tvi_ordre"
				+ " and "
				+ cond
				+ " union all"
				+ " select 'ORDRES' types,"
				+ " 3000000+m.odp_ordre macle,"
				+ " t.tvi_libelle ,"
				+ " p.PAI_DATE_CREATION, p.PAI_MONTANT, p.PAI_NB_VIREMENTS, p.PAI_NUMERO,"
				+ " m.ODP_MONTANT_PAIEMENT MONTANT,"
				+ " f.nom ADR_NOM, f.prenom ADR_PRENOM, f.civilite ADR_CIVILITE,"
				+ " r.RIB_BIC, RIB_CLE, r.RIB_CODBANC, r.RIB_GUICH, r.RIB_IBAN, r.RIB_DOMICIL, r.RIB_NUM, r.RIB_TITCO,"
				+ "null, substr(m.odp_libelle,1,32) || ' ' || chr(13)||chr(10) || to_char(m.odp_numero) || ' DU ' || TO_CHAR(ODP_DATE_SAISIE,'DDMMYY') as dep_numero"
				+ " from MARACUJA.paiement p , MARACUJA.ordre_de_paiement m , MARACUJA.v_rib r,MARACUJA.v_fournis_light f,MARACUJA.type_virement t "
				+ " where p.pai_ordre = m.pai_ordre"
				+ " and m.rib_ordre = r.rib_ordre (+)" + " and m.fou_ordre = f.fou_ordre" + " and p.tvi_ordre = t.tvi_ordre" + " and " + cond;

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_CONTENU_PAIEMENT, "contenu_paiement.pdf", req);
	}

	public static final String imprimerDetailFichierVirementBDF(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		EOEnterpriseObject element = (EOEnterpriseObject) objs.objectAtIndex(0);
		metadata.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey("paiOrdre"), "PAI_ORDRE");

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_DETAIL_VIREMENT_BDF, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_DETAIL_VIREMENT_BDF), FORMATPDF), null);
	}

	public static String imprimerDetailFichierVirementSEPA(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		EOEnterpriseObject element = (EOEnterpriseObject) objs.objectAtIndex(0);
		metadata.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey("paiOrdre"), "PAI_ORDRE");

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_DETAIL_VIREMENT_SEPA, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_DETAIL_VIREMENT_SEPA), FORMATPDF), null);
	}
	
	
	public static String imprimerDetailFichierPrelevementSEPA(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		EORecouvrement element = (EORecouvrement) objs.objectAtIndex(0);
		metadata.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey("recoOrdre"), "RECO_ORDRE");		
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_DETAIL_PRELEVEMENT_SEPA, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_DETAIL_PRELEVEMENT_SEPA) + "_" + element.recoNumero(), FORMATPDF), null);
	}

	public static final String imprimerImListe(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win, boolean onlyVises) throws Exception {
		if (objs == null || objs.count() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		EOEnterpriseObject element = (EOEnterpriseObject) objs.objectAtIndex(0);
		metadata.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey("paiOrdre"), "PAI_ORDRE");
		if (onlyVises) {
			return imprimerReportByThreadPdf(ec, temporaryDir, metadata, JASPERFILENAME_IMLISTE_VISES, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_IMLISTE_VISES), FORMATPDF), null, win, null);
		}
		else {
			return imprimerReportByThreadPdf(ec, temporaryDir, metadata, JASPERFILENAME_IMLISTE, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_IMLISTE), FORMATPDF), null, win, null);
		}

	}

	public static final String imprimerContenuPrelevement(EOEditingContext ec, String temporaryDir, NSDictionary metadata, EORecouvrement rec, Window win) throws Exception {
		final Integer recoOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(ec, rec).valueForKey("recoOrdre");

		String q = "select FICP_ORDRE from MARACUJA.prelevement_Fichier where reco_ordre=" + recoOrdre.intValue() + " and rownum=1 order by ficp_Ordre desc";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(ec, q);

		if (res.count() == 0) {
			throw new DefaultClientException("Impossible de recupérer le fichier de prélèvement");
		}
		NSDictionary dico = (NSDictionary) res.objectAtIndex(0);
		final Number ficpOrdre = (Number) dico.valueForKey("FICP_ORDRE");
		metadata.takeValueForKey(recoOrdre, "RECO_ORDRE");
		metadata.takeValueForKey(new Integer(ficpOrdre.intValue()), "FICP_ORDRE");

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_PRELEVEMENT_CONTENU, "prelev_contenu.pdf", null);
	}

	public static final String imprimerPaiementRetenues(EOEditingContext ec, String temporaryDir, NSDictionary metadata) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_PAIEMENT_RETENUES, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_PAIEMENT_RETENUES), FORMATPDF), null);
	}

	public static final String imprimerBordereauDisquette(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_BORDEREAU_DISQUETTE, "bordereau_disquette.pdf", null);
	}

	public static final String imprimerBordereauSepaSct(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_BORDEREAU_SEPA_SCT, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BORDEREAU_SEPA_SCT), FORMATPDF), null);
	}
	
	public static final String imprimerBordereauSepaSdd(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs) throws Exception {
	    EORecouvrement recouvrement = (EORecouvrement) objs.objectAtIndex(0);
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_BORDEREAU_SEPA_SDD, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BORDEREAU_SEPA_SDD) + "_" + recouvrement.recoNumero(), FORMATPDF), null);
	}

	public static final String imprimerBordereauPrelevement(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_PRELEVEMENT_BORDEREAU, "prelev_bordereau.pdf", null);
	}

	/**
	 * Imprime la liste des prelevements en attente.
	 *
	 * @param ec
	 * @param temporaryDir
	 * @param metadata
	 * @param paramRequete Doit contenir prelevDatePrelevementMin et prelevDatePrelevementMax pour definir la periode. prelevDatePrelevementMin peut
	 *            être null.
	 * @param win
	 * @return
	 * @throws Exception
	 */
	public static final String imprimerPrelevementsAttente(EOEditingContext ec, String temporaryDir, NSDictionary metadata, Map paramRequete, Window win) throws Exception {

		final NSMutableDictionary params = new NSMutableDictionary();
		params.addEntriesFromDictionary(metadata);

		Date dateDebut = ((Date) paramRequete.get("prelevDatePrelevementMin"));
		Date dateFin = (Date) paramRequete.get("prelevDatePrelevementMax");

		if (dateFin == null) {
			throw new DefaultClientException("La date de fin ne doit pas être vide.");
		}
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		String dateDebutStr = null;

		if (dateDebut != null) {
			params.takeValueForKey(dateDebut, "DATE_MIN");
			dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		}

		params.takeValueForKey(dateFin, "DATE_MAX");

		String req = "select * from (" + "SELECT f.FOU_CODE," +
				"f.civilite ADR_CIVILITE, f.nom ADR_NOM,f.prenom ADR_PRENOM," +
				"ra.ADR_ADRESSE1,ADR_ADRESSE2,ra.code_postal ADR_CP, ra.ville ADR_VILLE , " +
				"ra.CP_ETRANGER , p.LC_PAYS  ," +
				"t0.*," + "t3.TIT_DATE_REMISE, t3.GES_CODE, t3.TIT_TTC,t3.tit_numero," + "t1.ECHE_LIBELLE,"
				+ "t1.ECHE_NUMERO_INDEX, t1.ECHE_NOMBRE_ECHEANCES,t1.ECHE_DATE_CREATION,t1.ECHE_MONTANT," +
				"t4.bor_num, t4.BOR_DATE_VISA," + "r.RIB_BIC, r.RIB_CLE, r.RIB_CODBANC, r.RIB_DOMICIL, r.RIB_GUICH, r.RIB_NUM, r.RIB_TITCO " +
				"FROM maracuja.Prelevement t0, " + "maracuja.Titre T3, "
				+ "maracuja.Echeancier T1, " +
				"maracuja.Recette T2, " +
				"maracuja.Bordereau T4 , " +
				" maracuja.v_recette_adresse ra, maracuja.v_fournis_light f, grhum.pays p," +
				//"maracuja.v_fournisseur f, " +
				"maracuja.v_rib r " +
				"WHERE   T2.TIT_ID = T3.TIT_ID  " +
				" and t2.rec_ordre=ra.rpco_id and ra.c_pays=p.c_pays " +
				"and t3.fou_ordre=f.fou_ordre  " +
				"and T0.rib_ordre=r.rib_ordre "
				+ "AND t0.ECHE_ECHEANCIER_ORDRE = T1.ECHE_ECHEANCIER_ORDRE " + "AND T1.REC_ID = T2.REC_ID " +
				"AND T3.bor_id = T4.bor_id " +
				"and t0.prel_DATE_PRELEVEMENT < to_date('"
				+ dateFinStr
				+ "', 'YYYYMMDD') "
				+ (dateDebutStr != null ? "and t0.prel_DATE_PRELEVEMENT >= to_date('" + dateDebutStr + "', 'YYYYMMDD') " : "")
				+ "AND T4.bor_etat = 'VISE' "
				+ "AND T3.tit_Ttc > 0 "
				+ "AND t0.PREL_ETAT_MARACUJA = 'ATTENTE' "
				+

				"UNION ALL "
				+

				"SELECT f.FOU_CODE,ADR_CIVILITE, ADR_NOM,ADR_PRENOM,ADR_ADRESSE1,ADR_ADRESSE2,ADR_CP, ADR_VILLE , "
				+ "CP_ETRANGER , LC_PAYS  ,"
				+ "t0.*,"
				+ "null as TIT_DATE_REMISE, null as GES_CODE, null As TIT_TTC,null as tit_numero,"
				+ "t1.ECHE_LIBELLE,"
				+ "t1.ECHE_NUMERO_INDEX, t1.ECHE_NOMBRE_ECHEANCES,t1.ECHE_DATE_CREATION,t1.ECHE_MONTANT,"
				+ "t4.bor_num, t4.BOR_DATE_VISA,"
				+ "r.RIB_BIC, r.RIB_CLE, r.RIB_CODBANC, r.RIB_DOMICIL, r.RIB_GUICH, r.RIB_NUM, r.RIB_TITCO "
				+ "FROM maracuja.Prelevement t0, "
				+ "maracuja.Echeancier T1, "
				+ "maracuja.Bordereau T4 , "
				+ "maracuja.v_fournisseur f, "
				+ "maracuja.v_rib r "
				+ "WHERE   "
				+ "t1.FOU_ORDRE_CLIENT=f.fou_ordre  "
				+ "and T0.rib_ordre=r.rib_ordre "
				+ "AND t0.ECHE_ECHEANCIER_ORDRE = T1.ECHE_ECHEANCIER_ORDRE "
				+ "AND T1.bor_id = T4.bor_id "
				+ "and t0.prel_DATE_PRELEVEMENT < to_date('"
				+ dateFinStr
				+ "', 'YYYYMMDD') "
				+ (dateDebutStr != null ? "and t0.prel_DATE_PRELEVEMENT >= to_date('" + dateDebutStr + "', 'YYYYMMDD') " : "")
				+ "AND T4.bor_etat = 'VISE' "
				+ "AND t0.PREL_ETAT_MARACUJA = 'ATTENTE' "
				+ ")"
				+ "ORDER BY prel_date_prelevement, tit_numero ";
		return imprimerReportAndWait(ec, temporaryDir, params, JASPERFILENAME_PRELEVEMENT_ATTENTE, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_PRELEVEMENT_ATTENTE), FORMATPDF), req);
	}

	/**
	 * Imprime la liste des prelevements SEPA en attente.
	 *
	 * @param ec
	 * @param temporaryDir
	 * @param metadata
	 * @param paramRequete Doit contenir prelevDatePrelevementMin et prelevDatePrelevementMax pour definir la periode. prelevDatePrelevementMin peut
	 *            être null.
	 * @param win
	 * @return
	 * @throws Exception
	 */
	public static final String imprimerSepaPrelevementsAttente(EOEditingContext ec, String temporaryDir, NSDictionary metadata, Map paramRequete, Window win) throws Exception {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.addEntriesFromDictionary(metadata);

		ReadablePartial dateDebut = (ReadablePartial) paramRequete.get(RecoSepaSddCtrl.PRELEV_DATE_PRELEVEMENT_MIN);
		ReadablePartial dateFin = (ReadablePartial) paramRequete.get(RecoSepaSddCtrl.PRELEV_DATE_PRELEVEMENT_MAX);

		String dateDebutStr = EPOCH_DATE;
		String dateFinStr = EPOCH_DATE;

		if (dateFin == null) {
			throw new DefaultClientException("La date de fin ne doit pas être vide.");
		}

		if (dateDebut != null) {
			dateDebutStr = DateConversionUtil.sharedInstance().formatDateShort(dateDebut);
		}
		if (dateFin != null) {
			dateFinStr = DateConversionUtil.sharedInstance().formatDateShort(dateFin);
		}

		params.takeValueForKey(dateDebutStr, "DATE_MIN");
		params.takeValueForKey(dateFinStr, "DATE_MAX");
		return imprimerReportAndWait(ec, temporaryDir, params, JASPERFILENAME_PRELEVEMENT_ATTENTE_SEPA,
				getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_PRELEVEMENT_ATTENTE_SEPA), FORMATPDF), null);
	}


	public static final String imprimerOrdrePaiement(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray objs, Window win) throws Exception {
		if (objs == null) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "odpOrdre", "o.odp_ordre", objs);
		String req = "select odp_date_saisie odp_date,odp_ht,odp_ttc,odp_tva,odp_montant_paiement,odp_numero,o.odp_ordre,odp_libelle, mod_libelle, adr_nom,"
				+ "adr_prenom,adr_adresse1,adr_adresse2,adr_cp,adr_ville,exe_exercice,rib_codbanc,rib_guich,rib_num,rib_cle,rib_titco,  rib_bic, trim(grhum.format_iban(rib_iban)) rib_iban,odb_sens ecd_sens,d.ges_code, "
				+ "nvl(odb_montant,0) ecd_montant, " +
				"nvl(decode(odb_sens, 'D', odb_montant),0) ecd_debit , " +
				"nvl(decode(odb_sens, 'C', odb_montant),0) ecd_credit, " +
				"pco_num,d.opb_ordre ecd_ordre, "
				+ "org.org_etab as org_unit, org.org_ub as org_comp, org.org_cr as org_lbud, org.org_souscr as org_uc, org.org_lib," +
				"maracuja.number_to_lettres.transformer(odp_montant_paiement) odp_montant_texte "
				+ "from MARACUJA.ordre_de_paiement o,MARACUJA.mode_paiement m,MARACUJA.v_fournisseur f,MARACUJA.exercice e,MARACUJA.v_rib r,MARACUJA.odpaiement_brouillard d,MARACUJA.v_organ2 org " +
				"where o.mod_ordre = m.mod_ordre " + "and o.fou_ordre = f.fou_ordre " + "and o.exe_ordre = e.exe_ordre " + "and o.rib_ordre = r.rib_ordre(+) "
				+ "and o.odp_ordre = d.odp_ordre(+) " + "and o.org_ordre = org.org_id (+)" + " and " + cond + " order by o.odp_ordre,ecd_sens desc, ecd_ordre";

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_ORDRE_PAIEMENT, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_ORDRE_PAIEMENT)+"NV", FORMATPDF), req);
	}

	//
	public static final String imprimerOrdrePaiementScol(EOEditingContext ec, String temporaryDir, NSDictionary metadata, EOBordereau objs, final Window win) throws Exception {
		if (objs == null) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final String cond = buildConditionFromPrimaryKeyAndValues(ec, "borId", "m.bor_id", new NSArray(objs));

		final BigDecimal montantOdp = objs.getMontant();
		final Date odpDate = objs.borDateVisa();
		final Number odpNumero = objs.borNum();

		final String req = "select " + odpNumero + " as odp_numero,'" + ZConst.FORMAT_DATESHORT.format(odpDate) + "' as odp_date, exercice.exe_exercice, e.ecr_date_saisie as ecr_date,e.ecr_numero,ecd.ECD_ORDRE, ecd.ecd_sens, ecd.ges_code, "
				+ "ecd.ECD_LIBELLE, ecd.ECD_DEBIT , ecd.ECD_CREDIT, ecd.PCO_NUM, " + montantOdp.toString() + " as odp_ht, " +
				montantOdp.toString() + " as odp_montant_paiement, " + montantOdp.toString() + " as odp_ttc, " + "0.00" + " as odp_tva, " + " '"
				+ NumberToLettres.transforme(montantOdp, (String) metadata.valueForKeyPath("DEVISE_LIBELLE")) +
				"' odp_montant_texte " +
				"from MARACUJA.mandat_detail_ecriture mde, MARACUJA.mandat m, MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e, MARACUJA.exercice "
				+ "where ecd.ECD_ORDRE=mde.ecd_ordre and e.ecr_ordre=ecd.ecr_ordre and mde.man_id=m.man_id " +
				" and e.exe_ordre=exercice.exe_ordre " +
				"and e.top_ordre=6 " +
				"and ecr_etat='VALIDE' " +
				"and " + cond + " " +
				"and ecd_sens='D' " +
				" union all " +
				"select " + odpNumero + " as odp_numero,'" + ZConst.FORMAT_DATESHORT.format(odpDate) + "' as odp_date, exercice.exe_exercice, e.ecr_date_saisie as ecr_date,e.ecr_numero,ecd.ECD_ORDRE, ecd.ecd_sens, ecd.ges_code, "
				+ "ecd.ECD_LIBELLE, ecd.ECD_DEBIT , " + montantOdp.toString() + " as ECD_CREDIT, ecd.PCO_NUM, " + montantOdp.toString() + " as odp_ht, " +
				montantOdp.toString() + " as odp_montant_paiement, " + montantOdp.toString() + " as odp_ttc, " + "0.00" + " as odp_tva, " + " '"
				+ NumberToLettres.transforme(montantOdp, (String) metadata.valueForKeyPath("DEVISE_LIBELLE")) +
				"' odp_montant_texte " +
				"from MARACUJA.mandat_detail_ecriture mde, MARACUJA.mandat m, MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e, MARACUJA.exercice "
				+ "where ecd.ECD_ORDRE=mde.ecd_ordre and e.ecr_ordre=ecd.ecr_ordre and mde.man_id=m.man_id " +
				" and e.exe_ordre=exercice.exe_ordre " +
				"and e.top_ordre=6 " +
				"and ecr_etat='VALIDE' " +
				"and " + cond + " " +
				"and ecd_sens='C' " +
				"and rownum=1 " +
				"order by ecd_sens desc  ";

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_ORDRE_PAIEMENT_SCOL, "odpscol.pdf", req);
	}

	public static final String imprimerComptabiliteGestion(EOEditingContext ec, String temporaryDir, NSDictionary metadata, Window win) throws Exception {
		final String req = "select exercice.exe_exercice, c.com_ordre, com_libelle, c.com_libelle, gc.ges_code as ges_code_compta, ge.ges_code, ge.pco_num_185, ge.pco_num_185_ctp_sacd, ge.pco_num_181 from MARACUJA.comptabilite c, MARACUJA.gestion gc,  MARACUJA.gestion g, MARACUJA.gestion_exercice ge, MARACUJA.exercice where c.com_ordre = gc.com_ordre and c.ges_code = gc.ges_code and c.com_ordre=g.com_ordre and ge.ges_code=g.ges_code  and ge.exe_ordre=exercice.exe_ordre  and exercice.exe_exercice="
				
				+ myApp.appUserInfo().getCurrentExercice().exeExercice().toString() + " order by ge.ges_code";

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_COMPTABILITEGESTION, "comptabilite_gestion.pdf", req);
	}

	public static final String imprimerModePaiement(EOEditingContext ec, String temporaryDir, NSDictionary metadata) throws Exception {
		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_MODEPAIEMENT, "modepaiement.pdf", null);
	}

	/**
	 * Imprime une ou plusieurs ecritures valides.
	 *
	 * @param ec
	 * @param temporaryDir
	 * @param metadata
	 * @param ecritures Tableau d'objets EOEcriture à imprimer
	 * @return
	 * @throws Exception
	 */
	public static final String imprimerEcritureValide(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray ecritures) throws Exception {
		if (ecritures == null || ecritures.count() == 0) {
			throw new DefaultClientException("Aucune ecriture n'a été passée au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "ecrOrdre", "ecriture.ecr_ordre", ecritures);

		String req = "select ecr_numero,to_char(ecr_date,'DD/MM/YYYY') ecr_date,to_char(ecr_date_saisie,'DD/MM/YYYY') ecr_date_saisie," + "exe_exercice,ecr_libelle,com_libelle,tjo_libelle,ori_libelle,nvl(ecd_credit,0) ecd_credit,nvl(ecd_debit,0) ecd_debit,"
				+ "ecd_index,ecd_libelle,ecd_sens,ecriture_detail.ges_code,ecriture_detail.pco_num,maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle " +
				"from MARACUJA.ecriture,MARACUJA.exercice,MARACUJA.comptabilite,MARACUJA.type_journal,MARACUJA.origine,MARACUJA.ecriture_detail "
				+ "where ecriture.com_ordre = comptabilite.com_ordre and " + "ecriture.tjo_ordre = type_journal.tjo_ordre and " + "ecriture.ori_ordre = origine.ori_ordre(+) and " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre and " + "ecriture.exe_ordre=exercice.exe_ordre and " + cond + " "
				+ "order by ecr_numero,ecd_index";

		return imprimerReportAndWait(ec, temporaryDir, metadata, JASPERFILENAME_ECRITUREVALIDE, "ecriture_valide.pdf", req);
	}

	public static final String imprimerRelances(EOEditingContext ec, String temporaryDir, NSDictionary metadata, NSArray relances, Window win, final String filename, final String customJasperId) throws Exception {
		if (relances == null || relances.count() == 0) {
			throw new DefaultClientException("Aucune relance n'a été passée au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "rerOrdre", "r.rer_ordre", relances);

		String req = "select r.rer_ordre, r.rer_libelle, r.rer_ps, r.rer_mont, r.rer_contact, r.rer_date_creation, r.rer_date_impression, " +
				"r.rer_delai_paiement, (r.rer_date_creation+r.rer_delai_paiement) as rer_date_limite_paiement, t.trl_libelle1,t.trl_libelle2,"
				+ "t.trl_libelle3,t.trl_sign, t.trl_niveau, rec.rec_debiteur, f.nom adr_nom, f.prenom adr_prenom, f.civilite, "
				+ "ra.ADR_ADRESSE1, ra.ADR_ADRESSE2, ra.adr_bp,ra.code_postal ADR_CP, ra.ville adr_ville, f.fou_etranger, ra.cp_etranger, p.lc_pays, "
				+ "tit.tit_numero, tit.ges_code, tit.fou_ordre, decode(tit.tit_libelle, 'TITRE COLLECTIF', x.rec_lib, tit.tit_libelle) tit_libelle, decode(tit.tit_libelle, 'TITRE COLLECTIF', x.rec_ttc_saisie, tit.tit_ttc) tit_ttc,"
				+ "CONCAT(CONCAT (CONCAT (CONCAT (CONCAT ('Titre n° ', ex.exe_exercice),'/'),tit.ges_code), '/'), TO_CHAR (tit.tit_numero) ) " +
				"" +
				"" +
				" || ' ' || decode(x.fap_numero, null, '(Rec. n° '|| rp.rpp_numero ||')', '(Fact. n° '|| x.fap_numero ||')'   ) " +
				"as tit_references, "
				+ " ex.exe_exercice, tit.tit_id, r.trl_ordre, rec.rec_date "
				+ "from MARACUJA.recette_relance r, MARACUJA.type_relance t, MARACUJA.recette rec, MARACUJA.titre tit, maracuja.v_recette_adresse ra, maracuja.v_fournis_light f, grhum.pays p, MARACUJA.exercice ex, "
				+ "  jefy_recette.recette_ctrl_planco rcp, jefy_recette.recette_budget rb, jefy_recette.recette_papier rp, " +
				"(select rec_id, fap_numero, r.rec_lib, r.rec_ttc_saisie from jefy_recette.recette r, jefy_recette.facture_papier fpp where r.FAC_ID=fpp.FAC_ID) x "
				+ "where r.trl_ordre=t.trl_ordre "
				+ "and r.rec_id=rec.rec_id "
				+ "and rec.tit_id=tit.tit_id "
				+ "and tit.exe_ordre=ex.exe_ordre "
				+ "and rec.rec_ordre = rcp.rpco_id and rcp.rec_id=rb.rec_id and rb.rpp_id=rp.rpp_id "
				+ "and rcp.rpco_id=ra.rpco_id and ra.c_pays=p.c_pays "
				+ "and rec.fou_ordre = f.fou_ordre(+) " +
				" and rcp.rec_id=x.rec_id(+) "
				+ "and "
				+ cond + " "
				+ "order by t.trl_niveau desc, tit.tit_numero asc, tit.ges_code";

		return imprimerReportByThreadPdf(ec, temporaryDir, metadata, JASPERFILENAME_RELANCE_DEFAUT, filename, req, win, customJasperId);
	}

	/**
	 * Construit une condition de type (attr=val1 or attr=val2 or ....) pour requetes sql. Ca ne marche qu'avec les cles primaires simples et
	 * numeriques.
	 *
	 * @param keyNameInEos L'attribut de l'entite qui est la cle primaire
	 * @param attributeName L'attribut clé primaire dans la table
	 * @param eos
	 * @return
	 */
	private static final String buildConditionFromPrimaryKeyAndValues(EOEditingContext ec, String keyNameInEos, String attributeName, NSArray eos) {
		String res = "";
		for (int i = 0; i < eos.count(); i++) {
			EOEnterpriseObject element = (EOEnterpriseObject) eos.objectAtIndex(i);
			if (res.length() > 0) {
				res = res + " or ";
			}
			res = res + attributeName + " = " + ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey(keyNameInEos);
		}
		res = "(" + res + ")";
		return res;
	}

	/**
	 * Fait appel au serveur pour imprimer un report, et enregistre le flux PDF dans un fichier temporaire. La méthode renvoie le chemin d'accès au
	 * fichier.
	 */
	private static final String imprimerReportAndWait(EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery) throws Exception {
		try {
			String filePath = "";
			filePath = temporaryDir + "maracuja_" + pdfFileName;

			// Si le fichier existe, tenter de le supprimer.
			// Si pas possible, message d'erreur comme quoi il est ouvert
			File test = new File(filePath);
			if (test.exists()) {
				if (!test.delete()) {
					throw new Exception("Impossible de supprimer le fichier temporaire " + filePath + ". Il est vraissemblablement déjà ouvert, si c'est le cas, fermez-le avant de relancer l'impression.");
				}
			}

			NSData datas = ServerProxy.clientSideRequestPrintAndWait(ec, jasperFileName, sqlQuery, metadata);
			if (datas == null) {
				throw new Exception("Impossible de récupérer le flux PDF.");
			}

			try {
				FileOutputStream fileOutputStream = new FileOutputStream(filePath);
				datas.writeToStream(fileOutputStream);
				fileOutputStream.close();
			} catch (Exception exception) {
				// Do something with the exception
				throw new Exception("Impossible d'ecrire le fichier PDF sur le disque. Vérifiez qu'un autre devis n'est pas déjà ouvert.\n" + exception.getMessage());
			}

			// Vérifier que le fichier a bien ete cree
			try {
				File tmpFile = new File(filePath);
				if (!tmpFile.exists()) {
					throw new Exception("Le fichier " + filePath + " n'existe pas.");
				}
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}

			return filePath;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible d'imprimer. " + e.getMessage());
		}
	}

	public static final String imprimerReportByThreadPdf(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, String customJasperId) throws Exception {
		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, FORMATPDF, customJasperId);
	}

	//	private static final String imprimerReportByThreadXls(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, String customJasperId) throws Exception {
	//		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, FORMATXLS, customJasperId);
	//	}

	private static final String imprimerReportByThread(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, final int format, String customJasperId) throws Exception {

		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, format, customJasperId, Boolean.FALSE);

	}

	/**
	 * Imprime un report en affichant une fenetre d'attente.
	 *
	 * @param ec
	 * @param temporaryDir
	 * @param metadata
	 * @param jasperFileName
	 * @param pdfFileName
	 * @param sqlQuery
	 * @param win
	 * @param customJasperId Permet de spécifier un fichier jasper à utiliser à la place de celui par défaut (utile dans le cas des relances par
	 *            exemple)
	 * @return
	 * @throws Exception
	 */
	private static final String imprimerReportByThread(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, final int format, String customJasperId, final Boolean printIfEmpty) throws Exception {
		try {
			String filePath = "";
			filePath = temporaryDir + "maracuja_" + pdfFileName;

			// Si le fichier existe, tenter de le supprimer.
			// Si pas possible, message d'erreur comme quoi il est ouvert
			File test = new File(filePath);
			if (test.exists()) {
				if (!test.delete()) {
					throw new Exception("Impossible de supprimer le fichier temporaire " + filePath + ". Il est vraissemblablement déjà ouvert, si c'est le cas, fermez-le avant de relancer l'impression.");
				}
			}

			printThread = new ReportFactoryPrintWaitThread(ec, jasperFileName, sqlQuery, metadata, filePath, format, customJasperId, printIfEmpty);

			final Action cancelAction = new ActionCancelImpression(ec);

			final ZWaitingPanel.ZWaitingPanelListener waitingListener = new ZWaitingPanelListener() {
				public Action cancelAction() {
					return cancelAction;
				}
			};

			if (win instanceof Dialog) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Dialog) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else if (win instanceof Frame) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Frame) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else {
				throw new DefaultClientException("Fenetre parente non définie.");
			}
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Construction de l'état...");
			waitingDialog.setModal(true);

			printThread.start();
			waitingDialog.setVisible(true);

			// Vérifier s'il y a eu une exception
			if (printThread.getLastException() != null) {
				throw printThread.getLastException();
			}

			return printThread.getFilePath();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible d'imprimer. " + e.getMessage());
		}
	}

	private static void killCurrentPrintTask(EOEditingContext ec) {
		ServerProxy.clientSideRequestPrintKillCurrentTask(ec);
	}

	/**
	 * @param editingContext
	 * @param temporaryDir
	 * @param parametres
	 * @param jasperfilenameBordereauCheque
	 * @param selectedBdCheque
	 * @return
	 * @throws Exception
	 */
	public static String imprimerBdCheque(EOEditingContext ec, String temporaryDir, NSDictionary parametres, EOBordereau obj, final int mode, String jasperfilenameBordereauCheque) throws Exception {
		if (obj == null) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "borId", "c.bor_id", new NSArray(obj));
		String req = "SELECT b.exe_ordre, to_char(bor_date_visa,'DD/MM/YYYY') bor_date_visa,  bor_etat,  bor_num,  bor_ordre,  tbo_ordre,  " +
				"utl_ordre_visa,  bor_libelle,  pco_num_visa,  che_nom_tireur,  "
				+ "che_prenom_tireur,  rcpt_code,  banq_code,  banq_libelle,  banq_agence,  che_numero_cheque,  che_numero_compte,  che_montant,  che_date_cheque,  " +
				"che_date_saisie,  che_etat,  c.che_ordre che_ordre " +
				"FROM MARACUJA.BORDEREAU b,MARACUJA.BORDEREAU_INFO bf,MARACUJA.CHEQUE c "
				+ "WHERE b.bor_id = bf.bor_id " +
				"AND c.bor_id = b.bor_id " +
				"AND (c.CHE_ETAT = 'VALIDE' or c.CHE_ETAT = 'VISE')" +
				" and " + cond;

		switch (mode) {
		case BdChequeSrchCtrl.TRIER_CREATION:
			req = req + " order by che_ordre";
			break;

		case BdChequeSrchCtrl.TRIER_DATE_SAISIE:
			req = req + " order by che_date_saisie";
			break;

		case BdChequeSrchCtrl.TRIER_NOM:
			req = req + " order by che_nom_tireur, che_prenom_tireur";
			break;

		case BdChequeSrchCtrl.TRIER_NUMCHEQUE:
			req = req + " order by che_numero_cheque";
			break;

		default:
			throw new DefaultClientException("Tri spécifié non pris en charge.");
		}

		return imprimerReportAndWait(ec, temporaryDir, parametres, jasperfilenameBordereauCheque, "bdcheque.pdf", req);
	}

	private static Integer getStateLock() {
		return stateLock;
	}

	// ///////////////////////////////////////////////////////////

	/**
	 * Tache qui scanne le serveur pour avoir le résultat de l'impression.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private static class ReportFactoryPrintWaitTask extends TimerTask {
		private final EOEditingContext _ec;
		private NSData datas;
		private Exception lastException;
		private NSDictionary dictionary;

		/**
         *
         */
		public ReportFactoryPrintWaitTask(EOEditingContext ec) {
			_ec = ec;
		}

		/**
		 * @see java.util.TimerTask#run()
		 */
		public void run() {
			System.out.println("Attente report...");
			Thread.yield();
			try {
				if (datas == null) {
					datas = ServerProxy.clientSideRequestPrintDifferedGetPdf(_ec);
				}
				// waitingDialog.update(waitingDialog.getGraphics());
				if (datas != null) {
					// le report est rempli
					synchronized (getStateLock()) {
						// System.out.println("ReportFactoryPrintWaitTask.run()
						// : report recu du serveur --> delock");
						getStateLock().notifyAll();
					}
				}
				else {
					try {
						dictionary = ServerProxy.clientSideRequestGetPrintProgression(_ec);

						// ZLogger.debug(dictionary);

						int _pageNum = -1;
						int _pageCount = -1;
						boolean dataSourceCreated = false;
						boolean reportBuild = false;
						boolean reportExported = false;

						if (dictionary != null && waitingDialog != null) {
							String etat = null;
							_pageNum = ((Integer) dictionary.valueForKey("PAGE_NUM")).intValue();
							_pageCount = ((Integer) dictionary.valueForKey("PAGE_TOTAL_COUNT")).intValue();
							dataSourceCreated = ((Boolean) dictionary.valueForKey("DATASOURCE_CREATED")).booleanValue();
							reportBuild = ((Boolean) dictionary.valueForKey("REPORT_BUILD")).booleanValue();
							reportExported = ((Boolean) dictionary.valueForKey("REPORT_EXPORTED")).booleanValue();

							if (reportExported) {
								System.out.println("reportExported");
								waitingDialog.getMyProgressBar().setIndeterminate(true);
								waitingDialog.getMyProgressBar().setStringPainted(false);
								etat = "Téléchargement du fichier " + (_pageCount == -1 ? "" : _pageCount + " page(s)") + "...";
							}
							else if (reportBuild) {
								System.out.println("reportBuild");
								etat = "Création du fichier " + (_pageCount == -1 ? "" : _pageCount + " page(s)") + "...";
								waitingDialog.getMyProgressBar().setIndeterminate(false);
								waitingDialog.getMyProgressBar().setMinimum(0);
								waitingDialog.getMyProgressBar().setMaximum(_pageCount);
								waitingDialog.getMyProgressBar().setValue(_pageNum);
								// waitingDialog.getMyProgressBar().setStringPainted(true);

								// waitingDialog.getMyProgressBar().setString(new
								// String());

							}
							else if (dataSourceCreated) {
								waitingDialog.getMyProgressBar().setIndeterminate(true);
								waitingDialog.getMyProgressBar().setStringPainted(false);
								//System.out.println("dataSourceCreated");
								etat = "Construction de l'état...";
							}
							else {
								waitingDialog.getMyProgressBar().setIndeterminate(true);
								waitingDialog.getMyProgressBar().setStringPainted(false);
								etat = "Construction de l'état...";
							}
							waitingDialog.setBottomText(etat);
							Thread.yield();
						}

					} catch (Exception e0) {
						e0.printStackTrace();
						System.out.println("Erreur lors de la recuperation de la progression : " + e0.getMessage());
					}
				}
			} catch (Exception e) {
				lastException = e;
				synchronized (getStateLock()) {
					getStateLock().notifyAll();
				}
			}
		}

		public NSData getDatas() {
			return datas;
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	/**
	 * Thread qui se charge du dialogue avec le serveur pour l'impression.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public static class ReportFactoryPrintWaitThread extends Thread {
		private final EOEditingContext _ec;
		private final String _jasperFileName;
		private final String _sqlQuery;
		private final NSDictionary _metadata;
		// private NSData result;
		private final String filePath;
		private Exception lastException;
		private ReportFactoryPrintWaitTask waitTask;
		private Timer scruteur;
		private final int _format;
		private final String _customJasperId;
		private final Boolean _printIfEmpty;

		/**
         *
         */
		public ReportFactoryPrintWaitThread(EOEditingContext ec, String jasperFileName, String sqlQuery, NSDictionary metadata, String pfilepath, final int format, String customJasperId, final Boolean printIfEmpty) {
			super();
			_ec = ec;
			_jasperFileName = jasperFileName;
			_sqlQuery = sqlQuery;
			_metadata = metadata;
			filePath = pfilepath;
			_format = format;
			_customJasperId = customJasperId;
			_printIfEmpty = printIfEmpty;
		}

		public void run() {
			try {

				scruteur = new Timer();
				waitTask = new ReportFactoryPrintWaitTask(_ec);
				scruteur.scheduleAtFixedRate(waitTask, 1000, 500);

				// Lancer l'impression
				switch (_format) {
				case FORMATPDF:
					ServerProxy.clientSideRequestPrintByThread(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId, _printIfEmpty);
					break;
				//				case FORMATXLS:
				//					ServerProxy.clientSideRequestPrintByThreadXls(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId);
				//					break;

				default:
					ServerProxy.clientSideRequestPrintByThread(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId, _printIfEmpty);
					break;
				}

				// Attendre la fin de la tache d'impression
				synchronized (getStateLock()) {
					try {
						try {
							// On attend...
							getStateLock().wait();
							System.out.println("ReportFactoryPrintWaitThread.run() state delocke");
							// a ce niveau on doit avoir le resultat en pdf ou
							// une exception
							NSData datas = waitTask.getDatas();

							if (datas == null) {
								if (waitTask.getLastException() != null) {
									throw waitTask.getLastException();
								}
								throw new Exception("Erreur : le resultat est null");
							}
							try {
								FileOutputStream fileOutputStream = new FileOutputStream(filePath);
								datas.writeToStream(fileOutputStream);
								fileOutputStream.close();
							} catch (Exception exception) {
								// Do something with the exception
								throw new Exception("Impossible d'ecrire le fichier PDF sur le disque. Vérifiez qu'un autre fichier n'est pas déjà ouvert.\n" + exception.getMessage());
							}

							// Vérifier que le fichier a bien ete cree
							try {
								File tmpFile = new File(filePath);
								if (!tmpFile.exists()) {
									throw new Exception("Le fichier " + filePath + " n'existe pas.");
								}
							} catch (Exception e) {
								throw new Exception(e.getMessage());
							}

							scruteur.cancel();

						} catch (InterruptedException e1) {
							e1.printStackTrace();
							throw new DefaultClientException("Impression interrompue.");
						}

					} catch (Exception ie) {
						scruteur.cancel();
						throw ie;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}

		/**
		 * @see java.lang.Thread#interrupt()
		 */
		public void interrupt() {
			if (scruteur != null) {
				scruteur.cancel();
			}
			super.interrupt();
		}

		public String getFilePath() {
			return filePath;
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	public static final String buildCriteresPlanco(final String field, String cond) throws Exception {
		NSMutableArray quals = new NSMutableArray();
		// Analyser la condition
		if (cond == null) {
			return "";
		}
		String[] plages = cond.split("[,;]");

		for (int i = 0; i < plages.length; i++) {
			String plage = plages[i];
			// analyser la plage
			String[] bornes = plage.split("-");
			if (bornes.length > 2) {
				throw new DataCheckException("Erreur dans la spécification de la plage de compte. Veuillez vérifier vos critères.");
			}
			// s'il s'agit d'un compte unique
			if (bornes.length == 1) {
				quals.addObject(buildPossibleLikeCritere(field, bornes[0]));
			}
			else if (bornes.length == 2) {
				// On a deux comptes, donc on fait uen fourchette
				quals.addObject("(" + field + ">='" + bornes[0] + "' and " + field + "<='" + bornes[1] + "')");
			}
		}

		final String res = "(" + quals.componentsJoinedByString(" OR ") + ")";
		return res;
	}

	/**
	 * Construit un critere de type like si la condition contient une étoile, sinon prend le comparateur "=".
	 *
	 * @return
	 */
	private static final String buildPossibleLikeCritere(String field, String cond) {
		String cond2 = cond.replace('*', '%');
		return field + (cond.indexOf("*") == -1 ? "='" + cond2 + "'" : " LIKE '" + cond2 + "'");
	}

	private static final class ActionCancelImpression extends AbstractAction {
		EOEditingContext _ec;

		/**
         *
         */
		public ActionCancelImpression(EOEditingContext ec) {
			super("Annuler");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interrompre l'impression");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
			_ec = ec;
		}

		public void actionPerformed(ActionEvent e) {
			if (printThread.isAlive()) {
				killCurrentPrintTask(_ec);
				printThread.interrupt();
			}
		}

	}

	private static final String getFileNameWithExtension(final String name, final int format) {
		return name + "." + getExtensionForFormat(format);
	}

	private static final String getExtensionForFormat(final int format) {
		String res = "";
		switch (format) {
		case FORMATPDF:
			res = "pdf";
			break;

		//		case FORMATXLS:
		//			res = "xls";
		//			break;

		default:
			break;
		}
		return res;
	}

}
