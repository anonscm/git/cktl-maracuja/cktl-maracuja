/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.ClassesComboBoxModel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.BalancePanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BalanceCtrl extends ZKarukeraImprCtrl {
	private final static String WINDOW_TITLE = "Impression de la balance";
	private final static Dimension WINDOW_SIZE = new Dimension(650, 270);
	private final static String ACTION_ID = ZActionCtrl.IDU_IMPR003;
	protected final BalancePanel myPanel;

	private final ClassesComboBoxModel classesModel;
	private final ClasseCompteListener classeCompteListener;

	private final TypeBalanceModel typeBalanceModel = new TypeBalanceModel();
	private final TypeOperationModel typeOperationModel = new TypeOperationModel();
	private final TypeBalanceListener typeBalanceListener = new TypeBalanceListener();

	public static final String BALANCE_OPE = "Balance OPE";
	public static final String BALANCE_GEN = "Balance Générale";
	public static final String BALANCE_GEN_CS = "Balance Générale consolidée";
	public static final String BALANCE_RECAP_GRAND_LIVRE = "Balance Récapitulative des comptes du grand livre";
	public static final String BALANCE_VALEUR_INACTIVE = "Balance des comptes de valeur inactive";
	public static final String BALANCE_AUXILIAIRE = "Balance auxiliaire";

	public static final String BALANCE_OPE_TOUTES = "Toutes";
	public static final String BALANCE_OPE_BUDGETAIRES = "Budgétaires";
	public static final String BALANCE_OPE_NON_BUDGETAIRES = "Non budgétaires";

	private static final String JASPERFILENAME_BALANCE_GEN = "balancegen.jasper";
	private static final String JASPERFILENAME_BALANCE_OPE = "balanceope.jasper";
	private static final String JASPERFILENAME_BALANCE_GEN_CS = "balanceGCSG.jasper";
	private static final String JASPERFILENAME_BALANCE_RECAP_GRANDLIVRE = "balance_recap_comptes_grandlivre.jasper";
	private static final String JASPERFILENAME_BALANCE_VALEUR_INACTIVE = "balance_valeur_inactive.jasper";
	private static final String JASPERFILENAME_BALANCE_AUXILIAIRE = "balance_auxiliaire.jasper";
	private static final String JXLSFILENAME_BALANCE_OPE = "balance_ope.xls";
	private static final String JXLSFILENAME_BALANCE_GEN = "balance_gen.xls";
	private static final String JXLSFILENAME_BALANCE_GEN_CS = "balance_gencs.xls";
	private static final String JXLSFILENAME_BALANCE_RECAP_GRANDLIVRE = "balance_recap_comptes_grandlivre.xls";
	private static final String JXLSFILENAME_BALANCE_VALEUR_INACTIVE = "balance_valeur_inactive.xls";
	private static final String JXLSFILENAME_BALANCE_AUXILIAIRE = "balance_auxiliaire.xls";

	private final ActionXls2 actionImprimerXls2 = new ActionXls2();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public BalanceCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		classesModel = new ClassesComboBoxModel();
		classeCompteListener = new ClasseCompteListener();
		classesModel.setSelectedItem(ClassesComboBoxModel.TOUTES);

		typeBalanceModel.setSelectedItem(BALANCE_OPE);
		myPanel = new BalancePanel(new BalancePanelListener());
	}

	protected final void imprimer() {
		if (BALANCE_OPE.equals(myFilters.get("typeBalance"))) {
			imprimerOPE();
		}
		else if (BALANCE_GEN.equals(myFilters.get("typeBalance"))) {
			imprimerGEN();
		}
		else if (BALANCE_GEN_CS.equals(myFilters.get("typeBalance"))) {
			imprimerGEN_CS();
		}
		else if (BALANCE_RECAP_GRAND_LIVRE.equals(myFilters.get("typeBalance"))) {
			imprimerBalanceRecapGrandLivre();
		}
		else if (BALANCE_VALEUR_INACTIVE.equals(myFilters.get("typeBalance"))) {
			imprimerBalanceValeurInactive();
		}
		else if (BALANCE_AUXILIAIRE.equals(myFilters.get("typeBalance"))) {
			imprimerBalanceAuxiliaire();
		}
		else {
			showErrorDialog(new DefaultClientException("Impression non implémentée"));
		}
	}

	protected final void imprimerXls() {
		if (BALANCE_OPE.equals(myFilters.get("typeBalance"))) {
			imprimerJXlsOPE();
		}
		else if (BALANCE_GEN.equals(myFilters.get("typeBalance"))) {
			imprimerJXlsGEN();
		}
		else if (BALANCE_GEN_CS.equals(myFilters.get("typeBalance"))) {
			imprimerJXlsGENCS();
		}
		else if (BALANCE_RECAP_GRAND_LIVRE.equals(myFilters.get("typeBalance"))) {
			imprimerJXlsRecapGrandLivre();
		}
		else if (BALANCE_AUXILIAIRE.equals(myFilters.get("typeBalance"))) {
			imprimerJXlsBalanceAuxiliaire();
		}
		else if (BALANCE_VALEUR_INACTIVE.equals(myFilters.get("typeBalance"))) {
			imprimerJXlsValeurInactive();
		}
		else {
			showErrorDialog(new DefaultClientException("Impression non implémentée"));
		}
	}

	private String buildSqlGen(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		// Date dateDebut = ((Date)paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);

		// String dateDebutStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(dateDebut) ;
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		// String dateFinStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin),
		// 1, 0, 0, 0));
		//EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		//EOGestion gestion = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_FILTER_KEY);

		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		Integer classeCompte = (Integer) paramRequete.get("classeCompte");

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		//	String condsacd = "";
		//	String condGestion = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condCompte = null;
		if (classeCompte != null) {
			condCompte = " ecriture_detail.pco_num like'" + classeCompte.intValue() + "%' ";
		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");
		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_BALANCE");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");
		final String condTypeJournal = " type_journal.tjo_ordre<>18";
		final String condComptes = "  pco_num not like '86%'  ";

		String req = "select SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1) AS classe,substr(ecriture_detail.pco_num, 1, 2) as chapitre,"
				+ "ecriture_detail.ges_code,ecriture_detail.pco_num,ecriture_detail.exe_ordre,maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle ," +
				"SUM(ECRITURE_DETAIL.ecd_debit) ecd_debit," +
				"SUM(ECRITURE_DETAIL.ecd_credit) ecd_credit,"
				+ "sum(decode(ecriture.tjo_ordre, 2, ecd_debit, 0)) ecd_debit_be," +
				"sum(decode(ecriture.tjo_ordre, 2, ecd_credit, 0)) ecd_credit_be," +
				"sum(decode(ecriture.tjo_ordre, 2, 0, ecd_debit)) ecd_debit_exe," +
				"sum(decode(ecriture.tjo_ordre, 2, 0, ecd_credit)) ecd_credit_exe" +
				", decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), 1, SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit), 0  ) solde_debit," +
				"decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), -1, SUM (ecriture_detail.ecd_credit)-SUM (ecriture_detail.ecd_debit), 0  ) solde_credit " +
				" from MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal " +
				"where " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
				"and ecriture.tjo_ordre=type_journal.tjo_ordre " +
				" and " + condTypeJournal +
				" and " + condComptes +
				"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " + "and ecriture.ecr_numero > 0    "
				+ " and substr(ecriture.ecr_etat,1,1)='" + EOEcriture.ecritureValide.substring(0, 1) + "' "
				// + "and ecriture.ecr_etat='"+ EOEcriture.ecritureValide+ "' "
				+ (condGestion.length() != 0 ? " and " + condGestion : "");

		if (paramRequete.get("avantSolde6_7") != null && Boolean.TRUE.equals(paramRequete.get("avantSolde6_7"))) {
			req += " and ecriture.TOP_ORDRE<>12 ";
		}

		if (condCompte != null && condCompte.length() > 0) {
			req += " and " + condCompte;
		}
		req += " and " + cond + " and " + condCompta;

		//		if (condGestion.length() != 0) {
		//			req += " and " + condGestion;
		//		}

		req += " GROUP BY SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1),substr(ecriture_detail.pco_num, 1, 2),ECRITURE_DETAIL.ges_code,ECRITURE_DETAIL.pco_num,ECRITURE_DETAIL.exe_ordre order by exe_ordre, classe, ges_code, chapitre, pco_num";

		System.out.println(req);

		return req;
	}

	private String buildSqlGEN_CS(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		// Date dateDebut = ((Date)paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		//EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		Integer classeCompte = (Integer) paramRequete.get("classeCompte");

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ex.exe_ordre", new NSArray(exercice));
		//String condsacd = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condCompte = null;
		if (classeCompte != null) {
			condCompte = " ecriture_detail.pco_num like'" + classeCompte.intValue() + "%'";
		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_BALANCE");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		final String condTypeJournal = " type_journal.tjo_ordre<>18";
		final String condComptes = "  pco_num not like '86%'  ";

		String req = "select SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1) AS classe," + "substr(ecriture_detail.pco_num, 1, 2) as chapitre," + "ecriture_detail.pco_num,ecriture_detail.exe_ordre,maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle ,"
				+ "SUM(ECRITURE_DETAIL.ecd_debit) ecd_debit," + "SUM(ECRITURE_DETAIL.ecd_credit) ecd_credit," + "sum(decode(ecriture.tjo_ordre, 2, ecd_debit, 0)) ecd_debit_be," + "sum(decode(ecriture.tjo_ordre, 2, ecd_credit, 0)) ecd_credit_be,"
				+ "sum(decode(ecriture.tjo_ordre, 2, 0, ecd_debit)) ecd_debit_exe," + "sum(decode(ecriture.tjo_ordre, 2, 0, ecd_credit)) ecd_credit_exe" +
				", decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), 1, SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit), 0  ) solde_debit," +
				"decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), -1, SUM (ecriture_detail.ecd_credit)-SUM (ecriture_detail.ecd_debit), 0  ) solde_credit " +
				" from MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal, MARACUJA.exercice ex " + "where " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre "
				+ "  and ecriture.exe_ordre=ex.exe_ordre " +
				"and ecriture.tjo_ordre=type_journal.tjo_ordre " +
				" and " + condTypeJournal +
				" and " + condComptes +
				"and to_char(ecriture.ecr_date_saisie, 'YYYYMMDD') < '" + dateFinStr + "' " + "and ecriture.ecr_numero > 0    " + " and substr(ecriture.ecr_etat,1,1)='"
				+ EOEcriture.ecritureValide.substring(0, 1) + "' ";

		if (paramRequete.get("avantSolde6_7") != null && Boolean.TRUE.equals(paramRequete.get("avantSolde6_7"))) {
			req += " and ecriture.TOP_ORDRE<>12 ";
		}

		if (condCompte != null && condCompte.length() > 0) {
			req += " and " + condCompte;
		}
		req += " and " + cond + " and " + condCompta;
		req += (condGestion.length() != 0 ? " and " + condGestion : "");
		//
		//		if (condsacd.length() != 0) {
		//			req += " and " + condsacd;
		//		}
		req += " GROUP BY SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1),substr(ecriture_detail.pco_num, 1, 2),ECRITURE_DETAIL.pco_num,ECRITURE_DETAIL.exe_ordre " + " order by exe_ordre, classe,chapitre, pco_num";

		return req;
		//        return imprimerReportByThreadPdf(ec, temporaryDir, params, JASPERFILENAME_BALANCE_GEN_CS, "balancegencs.pdf", req, win, null);
	}

	private String imprimerBalanceope(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		String req = buildSqlOPE(ec, parametres, paramRequete);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, JASPERFILENAME_BALANCE_OPE, "balanceope.pdf", req, win, null);
	}

	private String imprimerBalanceGEN(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		String req = buildSqlGen(ec, parametres, paramRequete);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, JASPERFILENAME_BALANCE_GEN, "balancegen.pdf", req, win, null);
	}

	private String imprimerBalanceGEN_CS(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		String req = buildSqlGEN_CS(ec, parametres, paramRequete);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, JASPERFILENAME_BALANCE_GEN_CS, "balancegencs.pdf", req, win, null);
	}

	private String imprimerBalanceRecapGrandLivre(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		String req = buildSqlRecapGrandLivre(ec, parametres, paramRequete);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, JASPERFILENAME_BALANCE_RECAP_GRANDLIVRE, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BALANCE_RECAP_GRANDLIVRE), FORMATPDF), req, win, null);
	}

	private String imprimerBalanceValeurInactive(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		String req = buildSqlBalanceValeurInactive(ec, parametres, paramRequete);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, JASPERFILENAME_BALANCE_VALEUR_INACTIVE, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BALANCE_RECAP_GRANDLIVRE), FORMATPDF), req, win, null);
	}

	private String imprimerBalanceAuxiliaire(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		String req = buildSqlBalanceAuxiliaire(ec, parametres, paramRequete);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, JASPERFILENAME_BALANCE_AUXILIAIRE, getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_BALANCE_AUXILIAIRE), FORMATPDF), req, win, null);
	}

	private String buildSqlRecapGrandLivre(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		// Date dateDebut = ((Date)paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		//	EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		Integer classeCompte = (Integer) paramRequete.get("classeCompte");

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ex.exe_ordre", new NSArray(exercice));
		//String condsacd = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condCompte = null;
		if (classeCompte != null) {
			condCompte = " ecriture_detail.pco_num like'" + classeCompte.intValue() + "%'";
		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_BALANCE");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		String req = "select SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1) AS classe," + "substr(ecriture_detail.pco_num, 1, 2) as chapitre," +
				"ecriture_detail.pco_num,ecriture_detail.exe_ordre," +
				"maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle ,"
				+ "SUM(ECRITURE_DETAIL.ecd_debit) ecd_debit," +
				"SUM(ECRITURE_DETAIL.ecd_credit) ecd_credit," +
				"sum(decode(ecriture.tjo_ordre, 2, ecd_debit, 0)) ecd_debit_be," +
				"sum(decode(ecriture.tjo_ordre, 2, ecd_credit, 0)) ecd_credit_be,"
				+ "sum(decode(ecriture.tjo_ordre, 2, 0, ecd_debit)) ecd_debit_exe," +
				"sum(decode(ecriture.tjo_ordre, 2, 0, ecd_credit)) ecd_credit_exe" +
				", decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), 1, SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit), 0  ) solde_debit," +
				"decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), -1, SUM (ecriture_detail.ecd_credit)-SUM (ecriture_detail.ecd_debit), 0  ) solde_credit " +
				" from MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal, MARACUJA.exercice ex " +
				"where " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre "
				+ "  and ecriture.exe_ordre=ex.exe_ordre " +
				"and ecriture.tjo_ordre=type_journal.tjo_ordre " + "and to_char(ecriture.ecr_date_saisie, 'YYYYMMDD') < '" + dateFinStr + "' " + "and ecriture.ecr_numero > 0    " + " and substr(ecriture.ecr_etat,1,1)='"
				+ EOEcriture.ecritureValide.substring(0, 1) + "' ";

		if (paramRequete.get("avantSolde6_7") != null && Boolean.TRUE.equals(paramRequete.get("avantSolde6_7"))) {
			req += " and ecriture.TOP_ORDRE<>12 ";
		}

		if (condCompte != null && condCompte.length() > 0) {
			req += " and " + condCompte;
		}
		req += " and " + cond + " and " + condCompta + (condGestion.length() != 0 ? " and " + condGestion : "");

		req += " GROUP BY SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1),substr(ecriture_detail.pco_num, 1, 2),ECRITURE_DETAIL.pco_num,ECRITURE_DETAIL.exe_ordre ";

		req = "select x.exe_ordre, x.classe,maracuja.api_planco.get_pco_libelle(x.classe, x.exe_ordre) as pco_libelle, sum(ecd_debit) ecd_debit, sum(ecd_credit) ecd_credit, sum(ecd_debit_be) ecd_debit_be, sum(ecd_credit_be) ecd_credit_be, sum(ecd_debit_exe) ecd_debit_exe, sum(ecd_credit_exe) ecd_credit_exe, "
				+ " sum (decode ( sign(abs(ecd_debit)-abs(ecd_credit)) , -1, 0, ecd_debit-ecd_credit ) ) solde_debit,"
				+ " sum (decode ( sign(abs(ecd_credit)-abs(ecd_debit)) , -1, 0, ecd_credit-ecd_debit ) ) solde_credit "
				+ " from ("
				+ req
				+ ")x "
				+ "group by x.exe_ordre, x.classe "
				+ "ORDER BY exe_ordre, classe";

		return req;
	}

	private String buildSqlBalanceValeurInactive(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		//      final String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e.com_ordre", new NSArray(comptabilite));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		//String condsacd = "";

		//final NSMutableDictionary params = new NSMutableDictionary(params);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		//Condition a appliquer a toutes les vues
		String condDate = " to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' ";

		final String condTypeJournal = " e.tjo_ordre=18";

		//Condition sur les comptes
		final String condComptes = "  pco_num like '86%'  ";

		String req = "select exe_ordre, ges_code, pco_num, maracuja.api_planco.get_pco_libelle(pco_num, exe_ordre) as pco_libelle, debit_be, debit_exe, debit_total, " +
				"      credit_be, credit_exe, credit_total,  "
				+ "      (case when abs(debit_total)>=abs(credit_total) then debit_total-credit_total else 0 end) as debit_solde ,  " +
				"      (case when abs(credit_total)>abs(debit_total) then credit_total-debit_total else 0 end) as credit_solde   " +
				"from " + "( "
				+ "select exe_ordre, ges_code, pco_num, sum(debit_be) debit_be, sum(debit_exe) debit_exe, sum(debit_be)+sum(debit_exe) debit_total, " +
				"      sum(credit_be) credit_be, sum(credit_exe) credit_exe, sum(credit_be)+sum(credit_exe) credit_total " + "from (     "
				+ "   select e.exe_ordre, ecd.ges_code, pco_num, 0 as debit_be, sum(ecd_debit) debit_exe, 0 as credit_be, sum(ecd_credit) credit_exe   " +
				"   from MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e  " +
				"   where ecd.ecr_ordre=e.ecr_ordre " + "   and substr(e.ecr_etat,1,1)='V' "
				+ "   and " + condTypeJournal +
				" and " + condDate + "   group by e.exe_ordre, ecd.ges_code, pco_num " +
				"union   " +
				"   select e.exe_ordre, ecd.ges_code, pco_num, sum(ecd_debit) as debit_be, 0 debit_exe, sum(ecd_credit) as credit_be, 0 credit_exe   "
				+ "   from MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e  " +
				"   where ecd.ecr_ordre=e.ecr_ordre " +
				"   and substr(e.ecr_etat,1,1)='V' " +
				"   and e.tjo_ordre=2 " +
				" and " + condDate +
				"   group by e.exe_ordre, ecd.ges_code, pco_num " + ") x " +
				" where " + condexer +
				" and " + condComptes
				+ (condGestion.length() > 0 ? " and " + condGestion : "") +
				"group by exe_ordre, ges_code, pco_num " + ")" +
				"order by exe_ordre, ges_code, pco_num";
		return req;
		//return imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CADRE6, "cadre6.pdf", req, win, FORMATPDF, null, Boolean.TRUE);

	}

	private String buildSqlBalanceAuxiliaire(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		// Date dateDebut = ((Date)paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		//EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		//String condCompte = (String) paramRequete.get(ZKarukeraImprCtrl.PCO_NUM_FILTER_KEY);

		String condExercice = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecd.exe_ordre", new NSArray(exercice));
		//String condsacd = "";

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecd.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_BALANCE");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		String condCompte = " ecd.pco_num like'" + "4%'";

		String req = "select x.exe_ordre, x.pco_num, pco.pco_libelle,f.fou_ordre, f.fou_code, f.nom adr_nom, nvl(f.prenom, ' ') adr_prenom, debit, " +
				"credit, solde_debiteur,solde_crediteur from  ( " +
				"select ecd.exe_ordre, ecd.pco_num, fou_ordre, sum(ecd.ecd_debit) debit, sum(ecd.ecd_credit) credit, " +
				"decode (sign(sum(ecd.ecd_debit)-sum(ecd.ecd_credit) ),1,sum(ecd.ecd_debit)-sum(ecd.ecd_credit),0 ) solde_debiteur, " +
				"decode (sign(sum(ecd.ecd_credit)-sum(ecd.ecd_debit) ),1,sum(ecd.ecd_credit)-sum(ecd.ecd_debit),0 ) solde_crediteur  " +
				"from  maracuja.ecriture_detail ecd, maracuja.ecriture e, maracuja.mandat_detail_ecriture mde, maracuja.mandat m where  " +
				"e.ecr_ordre=ecd.ecr_ordre and ecd.ecd_ordre=mde.ECD_ORDRE and m.man_id=mde.man_id " +
				"and to_char(e.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
				"and  " + condExercice + " " +
				"and  " + condCompte + " " +
				(condGestion.length() != 0 ? " and " + condGestion : "") +
				"group by ecd.exe_ordre, ecd.pco_num, fou_ordre " +
				"union all " +
				"select ecd.exe_ordre, ecd.pco_num, fou_ordre, sum(ecd.ecd_debit) debit, sum(ecd.ecd_credit) credit, " +
				"decode (sign(sum(ecd.ecd_debit)-sum(ecd.ecd_credit) ),1,sum(ecd.ecd_debit)-sum(ecd.ecd_credit),0 ) " +
				"solde_debiteur, decode (sign(sum(ecd.ecd_credit)-sum(ecd.ecd_debit) ),1,sum(ecd.ecd_credit)-sum(ecd.ecd_debit),0 ) solde_crediteur  " +
				"from  maracuja.ecriture_detail ecd, maracuja.ecriture e, maracuja.TITRE_detail_ecriture mde, maracuja.TITRE m " +
				"where  e.ecr_ordre=ecd.ecr_ordre and ecd.ecd_ordre=mde.ECD_ORDRE and m.tit_id=mde.tit_id " +
				"and to_char(e.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
				"and  " + condExercice + " " +
				"and  " + condCompte + " " +
				(condGestion.length() != 0 ? " and " + condGestion : "") +
				"group by ecd.exe_ordre, ecd.pco_num, fou_ordre" +
				" UNION ALL " +
				"select ecd.exe_ordre, ecd.pco_num, fou_ordre, sum(ecd.ecd_debit) debit, sum(ecd.ecd_credit) credit, " +
				"decode (sign(sum(ecd.ecd_debit)-sum(ecd.ecd_credit) ),1,sum(ecd.ecd_debit)-sum(ecd.ecd_credit),0 ) solde_debiteur, " +
				"decode (sign(sum(ecd.ecd_credit)-sum(ecd.ecd_debit) ),1,sum(ecd.ecd_credit)-sum(ecd.ecd_debit),0 ) solde_crediteur  " +
				"from  maracuja.ecriture_detail ecd, maracuja.ecriture e, maracuja.ODPAIEM_detail_ecriture mde, " +
				"maracuja.ORDRE_DE_PAIEMENT m where  e.ecr_ordre=ecd.ecr_ordre and ecd.ecd_ordre=mde.ECD_ORDRE " +
				"and m.ODP_ORDRE=mde.ODP_ORDRE " +
				"and to_char(e.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
				"and  " + condExercice + " " +
				"and  " + condCompte + " " +
				"group by ecd.exe_ordre, ecd.pco_num, fou_ordre " +
				") x, maracuja.v_fournis_light f, " +
				"maracuja.plan_comptable_exer pco where  x.fou_ordre=f.fou_ordre and x.pco_num=pco.pco_num(+) and x.exe_ordre=pco.exe_ordre(+) " +
				"order by pco_num,fou_code";
		return req;
	}

	private String buildSqlOPE(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		//EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		//EOGestion gestion = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_FILTER_KEY);
		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		Integer classeCompte = (Integer) paramRequete.get("classeCompte");

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		//String condsacd = "";
		//String condGestion = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condCompte = null;
		if (classeCompte != null) {
			condCompte = " ecriture_detail.pco_num like'" + classeCompte.intValue() + "%'";
		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		String condOperations = "";
		params.takeValueForKey("", "OPERATIONS");
		final String condOperationsBudgetaires = " and type_journal.tjo_libelle IN ('" + EOTypeJournal.typeJournalVisaMandat + "','" + EOTypeJournal.typeJournalVisaTitre + "','" + EOTypeJournal.typeJournalReimputation + "','" + EOTypeJournal.typeJournalVisaPrestationInterne + "','"
				+ EOTypeJournal.typeJournalSalaires + "') ";
		final String condOperationsNonBudgetaires = " and type_journal.tjo_libelle NOT IN ('" + EOTypeJournal.typeJournalVisaMandat + "','" + EOTypeJournal.typeJournalVisaTitre + "','" + EOTypeJournal.typeJournalReimputation + "','" + EOTypeJournal.typeJournalVisaPrestationInterne + "','"
				+ EOTypeJournal.typeJournalSalaires + "') ";
		if (!BALANCE_OPE_TOUTES.equals(typeOperationModel.getSelectedItem())) {
			if (BALANCE_OPE_BUDGETAIRES.equals(typeOperationModel.getSelectedItem())) {
				params.takeValueForKey("Opérations budgétaires", "OPERATIONS");
				condOperations = condOperationsBudgetaires;
			}
			else {
				params.takeValueForKey("Opérations non budgétaires", "OPERATIONS");
				condOperations = condOperationsNonBudgetaires;
			}
		}

		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_BALANCE");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		final String condTypeJournal = " type_journal.tjo_ordre<>18 and type_journal.tjo_ordre<>2";

		String req = "select SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1) AS classe,SUM(ECRITURE_DETAIL.ecd_credit) ecd_credit,SUM(ECRITURE_DETAIL.ecd_debit) ecd_debit,  "
				+ "ecriture_detail.ges_code, ecriture_detail.pco_num, ecriture_detail.exe_ordre, " +
				"maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle, " +
				"decode(ge.pco_num_185, null,'ETABLISSEMENT', 'SACD '||  ge.ges_code) compta" +
				", decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), 1, SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit), 0  ) solde_debit," +
				"decode(sign(SUM (ecriture_detail.ecd_debit)-SUM (ecriture_detail.ecd_credit)), -1, SUM (ecriture_detail.ecd_credit)-SUM (ecriture_detail.ecd_debit), 0  ) solde_credit " +
				"from MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal, MARACUJA.gestion_exercice ge " +
				"where "
				+ "ecriture_detail.ges_code=ge.ges_code and ecriture_detail.exe_ordre=ge.exe_ordre and " +
				"ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
				"and ecriture.tjo_ordre=type_journal.tjo_ordre "
				+ "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' "
				+ "and ecriture.ecr_numero > 0    "
				+ " and substr(ecriture.ecr_etat,1,1)='" + EOEcriture.ecritureValide.substring(0, 1) + "' " +
				" and " + condTypeJournal +
				//"and type_journal.tjo_libelle <> '" + EOTypeJournal.typeJournalBalanceEntree + "' "
				(condGestion.length() != 0 ? " and " + condGestion : "")
				+ condOperations;

		if (paramRequete.get("avantSolde6_7") != null && Boolean.TRUE.equals(paramRequete.get("avantSolde6_7"))) {
			req += " and ecriture.TOP_ORDRE<>12 ";
		}

		if (condCompte != null && condCompte.length() > 0) {
			req += "and " + condCompte;
		}
		req += "and " + cond + " and " + condCompta;

		//		if (condsacd.length() != 0) {
		//			req += " and " + condsacd;
		//		}

		req += " GROUP BY SUBSTR(ECRITURE_DETAIL.pco_num, 1, 1),ECRITURE_DETAIL.ges_code,ECRITURE_DETAIL.pco_num,ECRITURE_DETAIL.exe_ordre,  decode(ge.pco_num_185, null,'ETABLISSEMENT', 'SACD '||  ge.ges_code)" +
				" order by exe_ordre, classe, ges_code, pco_num";
		return req;
	}

	private final void imprimerOPE() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerBalanceope(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerGEN() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerBalanceGEN(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerGEN_CS() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerBalanceGEN_CS(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerBalanceRecapGrandLivre() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerBalanceRecapGrandLivre(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerBalanceValeurInactive() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerBalanceValeurInactive(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerBalanceAuxiliaire() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerBalanceAuxiliaire(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void checkFilters() {

	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		myFilters.put("dateDebut", ZDateUtil.getTodayAsCalendar().getTime());
		//        myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getToday().getTime());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));

		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		//myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, null);
		//myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, null);
		myFilters.put("classeCompte", classesModel.getSelectedClasse());
		myFilters.put("typeBalance", typeBalanceModel.getSelectedItem());
		//myFilters.put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, NIVEAU2_ETAB_SACD);

	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel().updateData();

			//			myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, (gestionsSacdModel.getSelectedEObject()));
			//			myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, (gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class BalancePanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements BalancePanel.IBalancePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.BalancePanel.IBalancePanelListener#classeCompteModel()
		 */
		public DefaultComboBoxModel classeCompteModel() {
			return classesModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.BalancePanel.IBalancePanelListener#classeCompteListener()
		 */
		public ActionListener classeCompteListener() {
			return classeCompteListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.BalancePanel.IBalancePanelListener#typeBalanceModel()
		 */
		public DefaultComboBoxModel typeBalanceModel() {
			return typeBalanceModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.BalancePanel.IBalancePanelListener#typeBalanceListener()
		 */
		public ActionListener typeBalanceListener() {
			return typeBalanceListener;
		}

		public ActionListener typeOperationListener() {
			// TODO Auto-generated method stub
			return null;
		}

		public DefaultComboBoxModel typeOperationModel() {
			return typeOperationModel;
		}

		public Action actionImprimerXls2() {
			return actionImprimerXls2;

		}

	}

	private final class TypeBalanceModel extends DefaultComboBoxModel {
		public TypeBalanceModel() {
			addElement(BALANCE_OPE);
			addElement(BALANCE_GEN);
			addElement(BALANCE_GEN_CS);
			addElement(BALANCE_RECAP_GRAND_LIVRE);
			addElement(BALANCE_AUXILIAIRE);
			addElement(BALANCE_VALEUR_INACTIVE);
		}
	}

	private final class TypeOperationModel extends DefaultComboBoxModel {
		public TypeOperationModel() {
			addElement(BALANCE_OPE_TOUTES);
			addElement(BALANCE_OPE_BUDGETAIRES);
			addElement(BALANCE_OPE_NON_BUDGETAIRES);
		}
	}

	private final class ClasseCompteListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			myFilters.put("classeCompte", classesModel.getSelectedClasse());
		}
	}

	private final class TypeBalanceListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			myFilters.put("typeBalance", typeBalanceModel.getSelectedItem());
			myFilters.put("typeOperation", BALANCE_OPE_TOUTES);
		}
	}

	private final class TypeOperationListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			myFilters.put("typeOperation", typeOperationModel.getSelectedItem());
		}
	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	private final void imprimerXlsOPE() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlOPE(getEditingContext(), dico, myFilters);

			NSArray headers = new NSArray(new Object[] {
					"Exercice",
					"Comptabilité",
					"Classe",
					"Compte",
					"Gestion",
					"Libellé",
					"Débit",
					"Crédit",
					"Solde débiteur",
					"Solde créditeur",
			});


			NSArray champs = new NSArray(new Object[] {
					"EXE_ORDRE", "COMPTA", "CLASSE", "PCO_NUM", "GES_CODE", "PCO_LIBELLE", "ECD_DEBIT", "ECD_CREDIT", "SOLDE_DEBIT", "SOLDE_CREDIT"
			});
			NSArray keysToSum = new NSArray(new Object[] {
					"ECD_DEBIT", "ECD_CREDIT", "SOLDE_DEBIT", "SOLDE_CREDIT"
			});

			exportXlsTab(headers, champs, keysToSum, dico, req);

		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerJXlsOPE() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlOPE(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_BALANCE_OPE, "balanceope.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerJXlsGEN() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlGen(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_BALANCE_GEN, "balancegen.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerJXlsGENCS() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlGEN_CS(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_BALANCE_GEN_CS, "balancegencs.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerJXlsRecapGrandLivre() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlRecapGrandLivre(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_BALANCE_RECAP_GRANDLIVRE, "balancerecapgrandlivre.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerJXlsValeurInactive() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlBalanceValeurInactive(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_BALANCE_VALEUR_INACTIVE, "balancevaleurinactive.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerJXlsBalanceAuxiliaire() {
		try {
			checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSqlBalanceAuxiliaire(getEditingContext(), dico, myFilters);
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_BALANCE_AUXILIAIRE, "balancereauxiliaire.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public final class ActionXls2 extends AbstractAction {

		public ActionXls2() {
			super("Excel");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Export Excel");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls();
		}

	}
}
