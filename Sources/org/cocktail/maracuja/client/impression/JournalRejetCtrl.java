/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.JournalRejetPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class JournalRejetCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression du journal des rejets";
	private final Dimension WINDOW_SIZE = new Dimension(550, 210);
	//On prend comme action celle du journal general (pour la verification des droits)
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR001;
	public static final String JASPERFILENAME_JOURNAL_REJETS = "journal_rejets_sql.jasper";

	private JournalRejetPanel myPanel;
	private final String MODELE_DEPENSE = "Journal des rejets de dépenses";
	private final String MODELE_RECETTE = "Journal des rejets de recettes";

	//	public final static String MDL_TRI_NUMERO_GESTION = "Trier par code gestion, date de rejet";
	//	public final static String MDL_TRI_DATE_GESTION = "Trier par date de rejet, code gestion, numero";

	//private final ActionXls actionImprimerXls = new ActionXls();
	private final ActionXls2 actionImprimerXls2 = new ActionXls2();

	private final DefaultComboBoxModel modeleModel;
	//private final DefaultComboBoxModel comboBoxModeleTri;
	private final Map modelesMap = new HashMap(2);

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public JournalRejetCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			//if (gestions.count() + gestionsSacd.count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		//		comboBoxModeleTri = new DefaultComboBoxModel();
		//		comboBoxModeleTri.addElement(MDL_TRI_NUMERO);

		modeleModel = new DefaultComboBoxModel();
		modeleModel.addElement(MODELE_DEPENSE);
		modeleModel.addElement(MODELE_RECETTE);

		modelesMap.put(MODELE_DEPENSE, JASPERFILENAME_JOURNAL_REJETS);
		modelesMap.put(MODELE_RECETTE, JASPERFILENAME_JOURNAL_REJETS);

		myPanel = new JournalRejetPanel(new JournalRejetPanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerDevSoldeJour(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), (String) modelesMap.get(myFilters.get(ZKarukeraImprCtrl.MODELE_FILTER_KEY)), FORMATPDF);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public String imprimerDevSoldeJour(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win, final String modeleJasper, final int format) throws Exception {
		if (modeleJasper == null) {
			throw new DefaultClientException("Modele d'impression non récupéré");
		}
		NSMutableDictionary params = new NSMutableDictionary(parametres);
		final String req = buildSql(ec, paramRequete, params);
		return imprimerReportByThread(ec, temporaryDir, params, modeleJasper, getFileNameWithExtension(ZFileUtil.removeExtension(modeleJasper), format), req, win, format, null);
	}

	private String buildSql(EOEditingContext ec, HashMap paramRequete, NSMutableDictionary params) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		// Date dateDebut = ((Date)paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);

		// String dateDebutStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(dateDebut) ;
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		//EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		//EOGestion gestion = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_FILTER_KEY);

		//        EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		String condExer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		//String condsacd = "";
		String condGestion = "";
		//String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));
		//
		//		final String niveau = (String) paramRequete.get(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY);
		//		if (ZKarukeraImprCtrl.NIVEAU1_AGREGE.equals(niveau)) {
		//			params.takeValueForKey(" AGREGE (Etablissement + SACDs)", "SACD");
		//			condsacd = "";
		//		}
		//		else if (gesCodeSACD != null) {
		//			params.takeValueForKey("SACD " + gesCodeSACD.gesCode(), "SACD");
		//			condsacd = " ges_code = '" + gesCodeSACD.gesCode() + "'";
		//		}
		//		else {
		//			// Récupérer tous les SACDs
		//			NSArray sacds = EOsFinder.getSACDGestionForComptabilite(ec, comptabilite, exercice);
		//			// on les exclue de la requete
		//			for (int i = 0; i < sacds.count(); i++) {
		//				EOGestion element = (EOGestion) sacds.objectAtIndex(i);
		//				if (condsacd.length() > 0) {
		//					condsacd = condsacd + "and ";
		//				}
		//				condsacd = condsacd + " ges_code <> '" + element.gesCode() + "' ";
		//			}
		//		}
		//		if (gestion != null) {
		//			condGestion = " ges_code = '" + gestion.gesCode() + "' ";
		//		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		//
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_FIN");

		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");
		//		if (condGestion.length() > 0) {
		//			params.takeValueForKey(gestion.gesCode(), "GESTION");
		//		}

		String req = "";

		if (MODELE_RECETTE.equals(modeleModel.getSelectedItem())) {
			req = "SELECT 'titre' as type_rejet, EXE_ORDRE as EXERCICE, GES_CODE ,TIT_NUMERO as NUMERO, DATE_TITRE as DATE_ORIGINE, BOR_NUM_ORIGINE, TBO_LIBELLE_ORIGINE, PCO_NUM as IMPUTATION, FOU_CODE, FOURNISSEUR ,BRJ_NUM as REJET_NUMERO, TBO_LIBELLE_REJET, DATE_REJET, TIT_MOTIF_REJET as MOTIF_REJET, TIT_HT as MONTANT"
					+
					" FROM MARACUJA.V_TITRE_REJETE " +
					" where to_char(DATE_REJET,'YYYYMMDD') < '" + dateFinStr + "' "
					+ " and " + condExer
					//+ (condsacd.length() != 0 ? " and " + condsacd : "")
					+ (condGestion.length() != 0 ? " and " + condGestion : "");
			req += " order by date_rejet";
		}
		else {
			req = "SELECT 'mandat' as type_rejet, EXE_ORDRE as EXERCICE, GES_CODE ,MAN_NUMERO as NUMERO, DATE_MANDAT as DATE_ORIGINE, BOR_NUM_ORIGINE, TBO_LIBELLE_ORIGINE,PCO_NUM as IMPUTATION, FOU_CODE, FOURNISSEUR, BRJ_NUM as REJET_NUMERO, TBO_LIBELLE_REJET, DATE_REJET, MAN_MOTIF_REJET as MOTIF_REJET, MAN_HT as MONTANT"
					+
					" FROM MARACUJA.V_MANDAT_REJETE " +
					" where to_char(DATE_REJET,'YYYYMMDD') < '" + dateFinStr + "' "
					+ " and " + condExer
					//+ (condsacd.length() != 0 ? " and " + condsacd : "")
					+ (condGestion.length() != 0 ? " and " + condGestion : "");
			req += " order by date_rejet";
		}

		//		final String tri = (String) comboBoxModeleTri.getSelectedItem();
		//
		//		if (JournalRejetCtrl.MDL_TRI_DATE.equals(tri)) {
		//			req += " order by date_rejet, ges_code, tit_numero";
		//		}
		//		else if (JournalRejetCtrl.MDL_TRI_MONTANT.equals(tri)) {
		//			req += " order by pco_num, ecd_montant, ecr_numero";
		//		}
		//		else if (JournalRejetCtrl.MDL_TRI_GESTION.equals(tri)) {
		//			req += " order by pco_num, ges_code, ecr_numero";
		//		}
		//		else {
		//			req += " order by pco_num, ecr_numero";
		//		}

		return req;
	}

	//	private final void imprimerXls() {
	//		try {
	//			ZLogger.debug(myFilters);
	//			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
	//			dico.takeValueForKey("OUI", "XLS");
	//			String filePath = imprimerDevSoldeJour(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), (String) modelesMap.get(myFilters.get(ZKarukeraImprCtrl.MODELE_FILTER_KEY)), ReportFactoryClient.FORMATXLS);
	//			myApp.saveAsAndOpenFile(getMyDialog(), filePath);
	//		} catch (Exception e1) {
	//			showErrorDialog(e1);
	//		}
	//	}

	private final void imprimerXls2() {
		try {
			NSArray headers = new NSArray(new Object[] {
					"Code gestion",
					"No bord. rejet",
					"Date rejet",
					(MODELE_RECETTE.equals(modeleModel.getSelectedItem()) ? "No bord. recette" : "No bord. depense"),
					(MODELE_RECETTE.equals(modeleModel.getSelectedItem()) ? "No titre/AOR" : "No Mandat/ORV"),
					"Imputation",
					"Code fournisseur",
					"Fournisseur",
					"Motif du rejet",
					"Montant"
			});
			NSArray champs = new NSArray(new Object[] {
					"GES_CODE", "REJET_NUMERO", "DATE_REJET", "BOR_NUM_ORIGINE", "NUMERO", "IMPUTATION", "FOU_CODE", "FOURNISSEUR", "MOTIF_REJET", "MONTANT"
			});

			NSArray keysToSum = new NSArray(new Object[] {
					"MONTANT"
			});

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			final String sqlQuery = buildSql(getEditingContext(), myFilters, dico);
			exportXlsTab(headers, champs, keysToSum, dico, sqlQuery);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		//		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getTodayAsCalendar().getTime());
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		myFilters.put(ZKarukeraImprCtrl.MODELE_FILTER_KEY, MODELE_DEPENSE);
		//myFilters.put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, NIVEAU2_ETAB_SACD);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			//myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			//myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, ((EOGestion) gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public void doBeforeOpen() {
	}

	private final class JournalRejetPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements JournalRejetPanel.IJournalRejetPanelListener {

		public ComboBoxModel getModelesModel() {
			return modeleModel;
		}

		public Action actionImprimerXls() {
			return null;
		}

		//		public ComboBoxModel comboModeleTri() {
		//			return comboBoxModeleTri;
		//		}

		public Action actionImprimerXls2() {
			return actionImprimerXls2;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	//	public final class ActionXls extends AbstractAction {
	//
	//		public ActionXls() {
	//			super("Excel");
	//			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
	//			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Export Excel similaire a l'impression");
	//			setEnabled(true);
	//		}
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			imprimerXls();
	//		}
	//
	//	}

	public final class ActionXls2 extends AbstractAction {

		public ActionXls2() {
			super("Excel (*)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Export Excel tabulaire");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls2();
		}

	}

}
