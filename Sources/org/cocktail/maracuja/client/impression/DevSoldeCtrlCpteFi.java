package org.cocktail.maracuja.client.impression;

import com.webobjects.eocontrol.EOEditingContext;

public class DevSoldeCtrlCpteFi extends DevSoldeCtrl {
	private AgregatsCtrl _agregatCtrl;

	public DevSoldeCtrlCpteFi(EOEditingContext editingContext) throws Exception {
		super(editingContext);
	}

	@Override
	public AgregatsCtrl getAgregatCtrl() {
		try {
			if (_agregatCtrl == null) {
				_agregatCtrl = new AgregatsCtrl(getActionId(), true, true, true, false, true);
			}
			return _agregatCtrl;
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}
		return null;

	}

	protected String getActionId() {
		return null;
	}
}
