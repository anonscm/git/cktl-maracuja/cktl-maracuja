/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper;
import org.cocktail.maracuja.client.ExportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.maracuja.client.impression.ui.ZKarukeraImprPanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.common.VersionCommon;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanel.ZWaitingPanelListener;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */

public abstract class ZKarukeraImprCtrl extends CommonCtrl {
	protected final Dimension PCOWINDOW_SIZE = new Dimension(300, 350);
	protected final ActionClose actionClose = new ActionClose();
	protected final ActionImprimer actionImprimer = new ActionImprimer();
	protected final HashMap myFilters = new HashMap();

	public static final String KEY_AGREGEE = "KEY_AGREGE";

	public static final String MODELE_FILTER_KEY = "modele";
	public static final String NIVEAU_FILTER_KEY = "niveau";
	public static final String EXERCICE_FILTER_KEY = "exercice";
	public static final String COMPTABILITE_FILTER_KEY = "comptabilite";
	public static final String DATE_FIN_FILTER_KEY = "dateFin";
	public static final String DATE_DEBUT_FILTER_KEY = "dateDebut";
	public static final String TYPE_OPERATION_FILTER_KEY = "typeOperation";
	public static final String ORIGINE_FILTER_KEY = "origine";

	protected final EOComptabilite comptabilite = getComptabilite();
	private final HashMap dispayGroupsOrigine = new HashMap();

	protected final PcoLookupModel pcoLookupModel;
	protected final PcoLookupListener pcoLookupListener = new PcoLookupListener();
	protected HashMap pcoMap;
	private AgregatsCtrl agregatCtrl;

	private ZEOComboBoxModel typeOperationsModel = null;
	private ZEOComboBoxModel typeJournalModel = null;
	protected ActionSrchOrigine actionSelectOrigine = new ActionSrchOrigine();

	public ZKarukeraImprCtrl(final EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		final NSArray pcos;
		pcos = getPcoValides();

		pcoMap = new LinkedHashMap(pcos.count());
		for (int i = 0; i < pcos.count(); i++) {
			final EOPlanComptableExer element = (EOPlanComptableExer) pcos.objectAtIndex(i);
			pcoMap.put(ZStringUtil.extendWithChars(element.pcoNum(), " ", 15, false) + " " + element.pcoLibelle(), element);
		}

		System.out.println(pcoMap.size());

		pcoLookupModel = new PcoLookupModel();

	}

	protected abstract void imprimer();

	protected abstract String getActionId();

	protected abstract ZKarukeraPanel myPanel();

	protected abstract Dimension getWindowSize();

	protected abstract String getWindowTitle();

	protected abstract void initFilters();

	/**
	 * @param headers Titres des colonnes dans Excel
	 * @param champs Nom des champs resultats de la requete SQL (parallele aux headers)
	 * @param dico Dictionnaire de parametre
	 * @param sqlQuery Requete sql
	 * @throws Exception
	 */
	protected void exportXlsTab(NSArray headers, NSArray champs, NSArray keysToSum, NSMutableDictionary dico, String sqlQuery) throws Exception {
		ExportFactoryClient exportFactoryClient = new ExportFactoryClient();
		File file = exportFactoryClient.showFileSelectDialog("xls", "Fichier Excel");
		if (file != null) {
			exportFactoryClient.exportToExcelFromSQL(getMyDialog(), file, champs, headers, keysToSum, sqlQuery, dico);
			//Vérifier que le fichier a bien ete cree
			try {
				if (!file.exists()) {
					throw new Exception("Le fichier " + file.getAbsolutePath() + " n'existe pas.");
				}
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}

			if (!showConfirmationDialog("Confirmation", "Le fichier " + file.getAbsolutePath() + " a été enregistré. Souhaitez-vous l'ouvrir ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
			OperatingSystemHelper.openFile(file.getAbsolutePath());
		}

	}

	protected final NSArray getPcoValides() {
		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), null, false);
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	protected final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, getWindowTitle(), true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, getWindowTitle(), true);
		}
		setMyDialog(win);
		myPanel().setMyDialog(win);
		myPanel().setPreferredSize(getWindowSize());
		myPanel().initGUI();
		win.setContentPane(myPanel());
		win.pack();
		return win;
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	public final class ActionImprimer extends AbstractAction {

		public ActionImprimer() {
			super("Imprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimer();
		}

	}

	public final class SACDFormat extends Format {

		/**
		 * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
		 */
		public Object parseObject(String source, ParsePosition pos) {

			return null;
		}

		/**
		 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
		 */
		public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
			if (obj instanceof EOGestion) {
				return new StringBuffer("SACD " + ((EOGestion) obj).gesCode());
			}
			else if (obj instanceof String) {
				return new StringBuffer("SACD " + (String) obj);
			}
			else {
				return new StringBuffer("");
			}
		}

	}

	private class PcoLookupListener implements IZLookupButtonListener {

		public void setNewValue(Object res) {
			final EOPlanComptableExer tmp = (EOPlanComptableExer) pcoMap.get(res);
			String s = (String) myFilters.get("pcoNum");
			if (tmp != null) {
				if (s != null && s.length() > 0) {
					s = s + "," + tmp.pcoNum();
				}
				else {
					s = tmp.pcoNum();
				}
			}

			System.out.println("PcoLookupListener.setNewValue() = " + s);
			myFilters.put("pcoNum", s);
			try {
				myPanel().updateData();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onTextHasChanged() {

		}

		public void onDbClick() {

		}

	}

	private class PcoLookupModel implements IZLookupButtonModel {
		private final Collection pcosTmp;

		public PcoLookupModel() {
			pcosTmp = pcoMap.keySet();
		}

		public Collection getDatas() {
			return pcosTmp;
		}

		public String getActionDescription() {
			return "Sélectionner un compte";
		}

		public ImageIcon getIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
		}

		public Window getMyWindow() {
			return getMyDialog();
		}

		public String getTitle() {
			return "Sélectionnez un compte";
		}

		public Dimension getPreferredSize() {
			return PCOWINDOW_SIZE;
		}

	}

	public final static int FORMATXLS = 0;
	public final static int FORMATPDF = 1;
	public final static int FORMATJXLS = 2;

	/** sert aux threads */
	private static Integer stateLock = new Integer(0);

	protected static ZWaitingPanelDialog waitingDialog;
	protected static ReportFactoryPrintWaitThread printThread;

	protected final String imprimerReportByThreadPdf(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, String customJasperId) throws Exception {
		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, FORMATPDF, customJasperId);
	}

	//	protected final String imprimerReportByThreadXls(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, String customJasperId) throws Exception {
	//		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, FORMATXLS, customJasperId);
	//	}

	protected final String imprimerReportByThreadJXls(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, String customJasperId) throws Exception {
		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, FORMATJXLS, customJasperId);
	}

	protected final String imprimerReportByThread(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, final int format, String customJasperId) throws Exception {
		return imprimerReportByThread(ec, temporaryDir, metadata, jasperFileName, pdfFileName, sqlQuery, win, format, customJasperId, Boolean.FALSE);
	}

	/**
	 * Imprime un report en affichant une fenetre d'attente.
	 *
	 * @param ec
	 * @param temporaryDir
	 * @param metadata
	 * @param jasperFileName
	 * @param pdfFileName
	 * @param sqlQuery
	 * @param win
	 * @param customJasperId Permet de spécifier un fichier jasper à utiliser à la place de celui par défaut (utile dans le cas des relances par
	 *            exemple)
	 * @return
	 * @throws Exception
	 */
	protected final String imprimerReportByThread(final EOEditingContext ec, String temporaryDir, NSDictionary metadata, String jasperFileName, String pdfFileName, String sqlQuery, Window win, final int format, String customJasperId, final Boolean printIfEmpty) throws Exception {
		try {
			if (printThread != null && printThread.isAlive()) {
				printThread.interrupt();
				printThread = null;
			}
			String filePath = "";

			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir.concat(File.separator);
			}

			filePath = temporaryDir + VersionCommon.APPLICATION_TYAP_STRID + "_" + pdfFileName;

			// Si le fichier existe, tenter de le supprimer.
			// Si pas possible, message d'erreur comme quoi il est ouvert
			File test = new File(filePath);
			if (test.exists()) {
				if (!test.delete()) {
					throw new Exception("Impossible de supprimer le fichier temporaire " + filePath + ". Il est vraissemblablement déjà ouvert, si c'est le cas, fermez-le avant de relancer l'impression.");
				}
			}

			printThread = new ReportFactoryPrintWaitThread(ec, jasperFileName, sqlQuery, metadata, filePath, format, customJasperId, printIfEmpty);

			final Action cancelAction = new ActionCancelImpression(ec);

			final ZWaitingPanel.ZWaitingPanelListener waitingListener = new ZWaitingPanelListener() {
				public Action cancelAction() {
					return cancelAction;
				}
			};

			if (win instanceof Dialog) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Dialog) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else if (win instanceof Frame) {
				waitingDialog = new ZWaitingPanelDialog(waitingListener, (Frame) win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			}
			else {
				throw new DefaultClientException("Fenetre parente non définie.");
			}
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Construction de l'état...");
			waitingDialog.setModal(true);

			printThread.start();
			// Vérifier s'il y a eu une exception
			if (printThread.getLastException() != null) {
				printThread.interrupt();
				throw printThread.getLastException();
			}
			waitingDialog.setVisible(true);

			// Vérifier s'il y a eu une exception
			if (printThread.getLastException() != null) {
				printThread.interrupt();
				throw printThread.getLastException();
			}

			return printThread.getFilePath();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Impossible d'imprimer. " + e.getMessage());
		}
	}

	private void killCurrentPrintTask(EOEditingContext ec) {
		ServerProxy.clientSideRequestPrintKillCurrentTask(ec);
	}

	private Integer getStateLock() {
		return stateLock;
	}

	// ///////////////////////////////////////////////////////////

	/**
	 * Tache qui scanne le serveur pour avoir le résultat de l'impression.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private class ReportFactoryPrintWaitTask extends TimerTask {
		private EOEditingContext _ec;
		private NSData datas;
		private Exception lastException;
		private NSDictionary dictionary;

		/**
             *
             */
		public ReportFactoryPrintWaitTask(EOEditingContext ec) {
			_ec = ec;
		}

		/**
		 * @see java.util.TimerTask#run()
		 */
		public void run() {
			System.out.println("Attente report...");
			Thread.yield();
			try {
				if (datas == null) {
					datas = ServerProxy.clientSideRequestPrintDifferedGetPdf(_ec);
				}
				// waitingDialog.update(waitingDialog.getGraphics());
				if (datas != null) {
					// le report est rempli
					synchronized (getStateLock()) {
						// System.out.println("ReportFactoryPrintWaitTask.run()
						// : report recu du serveur --> delock");
						getStateLock().notifyAll();
					}
				}
				else {
					try {
						dictionary = ServerProxy.clientSideRequestGetPrintProgression(_ec);

						// myApp.appLog.trace(dictionary);

						int _pageNum = -1;
						int _pageCount = -1;
						boolean dataSourceCreated = false;
						boolean reportBuild = false;
						boolean reportExported = false;

						if (dictionary != null && waitingDialog != null) {
							String etat = null;
							_pageNum = ((Integer) dictionary.valueForKey("PAGE_NUM")).intValue();
							_pageCount = ((Integer) dictionary.valueForKey("PAGE_TOTAL_COUNT")).intValue();
							dataSourceCreated = ((Boolean) dictionary.valueForKey("DATASOURCE_CREATED")).booleanValue();
							reportBuild = ((Boolean) dictionary.valueForKey("REPORT_BUILD")).booleanValue();
							reportExported = ((Boolean) dictionary.valueForKey("REPORT_EXPORTED")).booleanValue();

							if (reportExported) {
								System.out.println("reportExported");
								waitingDialog.getMyProgressBar().setIndeterminate(true);
								waitingDialog.getMyProgressBar().setStringPainted(false);
								etat = "Téléchargement du fichier " + (_pageCount == -1 ? "" : _pageCount + " page(s)") + "...";
							}
							else if (reportBuild) {
								System.out.println("reportBuild");
								etat = "Création du fichier " + (_pageCount == -1 ? "" : _pageCount + " page(s)") + "...";
								waitingDialog.getMyProgressBar().setIndeterminate(false);
								waitingDialog.getMyProgressBar().setMinimum(0);
								waitingDialog.getMyProgressBar().setMaximum(_pageCount);
								waitingDialog.getMyProgressBar().setValue(_pageNum);

							}
							else if (dataSourceCreated) {
								waitingDialog.getMyProgressBar().setIndeterminate(true);
								waitingDialog.getMyProgressBar().setStringPainted(false);
								etat = "Construction de l'état...";
							}
							else {
								waitingDialog.getMyProgressBar().setIndeterminate(true);
								waitingDialog.getMyProgressBar().setStringPainted(false);
								etat = "Construction de l'état...";
							}
							waitingDialog.setBottomText(etat);
							Thread.yield();
						}

					} catch (Exception e0) {
						e0.printStackTrace();
						System.out.println("Erreur lors de la recuperation de la progression : " + e0.getMessage());
					}
				}
			} catch (Exception e) {
				lastException = e;
				synchronized (getStateLock()) {
					getStateLock().notifyAll();
				}
			}
		}

		public NSData getDatas() {
			return datas;
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	/**
	 * Thread qui se charge du dialogue avec le serveur pour l'impression.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public class ReportFactoryPrintWaitThread extends Thread {
		private EOEditingContext _ec;
		private String _jasperFileName;
		private String _sqlQuery;
		private NSDictionary _metadata;
		// private NSData result;
		private String filePath;
		private Exception lastException;
		private ReportFactoryPrintWaitTask waitTask;
		private Timer scruteur;
		private final int _format;
		private final String _customJasperId;
		private final Boolean _printIfEmpty;

		/**
             *
             */
		public ReportFactoryPrintWaitThread(EOEditingContext ec, String jasperFileName, String sqlQuery, NSDictionary metadata, String pfilepath, final int format, String customJasperId, final Boolean printIfEmpty) {
			super();
			_ec = ec;
			_jasperFileName = jasperFileName;
			_sqlQuery = sqlQuery;
			_metadata = metadata;
			filePath = pfilepath;
			_format = format;
			_customJasperId = customJasperId;
			_printIfEmpty = printIfEmpty;
		}

		public void run() {
			try {

				scruteur = new Timer();
				waitTask = new ReportFactoryPrintWaitTask(_ec);
				scruteur.scheduleAtFixedRate(waitTask, 1000, 500);

				// Lancer l'impression
				switch (_format) {
				case FORMATPDF:
					System.out.println("ZKarukeraImprCtrl.ReportFactoryPrintWaitThread.run() PDF");
					ServerProxy.clientSideRequestPrintByThread(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId, _printIfEmpty);
					break;
				//				case FORMATXLS:
				//					System.out.println("ZKarukeraImprCtrl.ReportFactoryPrintWaitThread.run() XLS");
				//					ServerProxy.clientSideRequestPrintByThreadXls(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId);
				//					break;
				case FORMATJXLS:
					System.out.println("ZKarukeraImprCtrl.ReportFactoryPrintWaitThread.run() JXLS");
					ServerProxy.clientSideRequestPrintByThreadJXls(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId);
					break;

				default:
					ServerProxy.clientSideRequestPrintByThread(_ec, _jasperFileName, _sqlQuery, _metadata, _customJasperId, _printIfEmpty);
					break;
				}

				// Attendre la fin de la tache d'impression
				synchronized (getStateLock()) {
					try {
						try {
							// On attend...
							getStateLock().wait();
							System.out.println("ReportFactoryPrintWaitThread.run() state delocke");
							// a ce niveau on doit avoir le resultat en pdf ou
							// une exception
							NSData datas = waitTask.getDatas();

							if (datas == null) {
								if (waitTask.getLastException() != null) {
									throw waitTask.getLastException();
								}
								throw new Exception("Erreur : le resultat est null");
							}
							try {
								FileOutputStream fileOutputStream = new FileOutputStream(filePath);
								datas.writeToStream(fileOutputStream);
								fileOutputStream.close();
							} catch (Exception exception) {
								// Do something with the exception
								throw new Exception("Impossible d'ecrire le fichier PDF sur le disque. Vérifiez qu'un autre fichier n'est pas déjà ouvert.\n" + exception.getMessage());
							}

							// Vérifier que le fichier a bien ete cree
							try {
								File tmpFile = new File(filePath);
								if (!tmpFile.exists()) {
									throw new Exception("Le fichier " + filePath + " n'existe pas.");
								}
							} catch (Exception e) {
								throw new Exception(e.getMessage());
							}

							scruteur.cancel();

						} catch (InterruptedException e1) {
							e1.printStackTrace();
							throw new DefaultClientException("Impression interrompue.");
						}

					} catch (Exception ie) {
						scruteur.cancel();
						throw ie;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
				lastException = e;
			} finally {
				waitingDialog.setVisible(false);
			}

		}

		/**
		 * @see java.lang.Thread#interrupt()
		 */
		public void interrupt() {
			if (scruteur != null) {
				scruteur.cancel();
			}
			super.interrupt();
		}

		public String getFilePath() {
			return filePath;
		}

		public Exception getLastException() {
			return lastException;
		}
	}

	/**
	 * Construit un critere de type like si la condition contient une étoile, sinon prend le comparateur "=".
	 *
	 * @return
	 */
	private final String buildPossibleLikeCritere(String field, String cond) {
		String cond2 = cond.replace('*', '%');
		return field + (cond.indexOf("*") == -1 ? "='" + cond2 + "'" : " LIKE '" + cond2 + "'");
	}

	protected final String buildCriteresPlanco(final String field, String cond) throws Exception {
		NSMutableArray quals = new NSMutableArray();
		// Analyser la condition
		if (cond == null) {
			return "";
		}
		String[] plages = cond.split("[,;]");

		for (int i = 0; i < plages.length; i++) {
			String plage = plages[i];
			// analyser la plage
			String[] bornes = plage.split("-");
			if (bornes.length > 2) {
				throw new DataCheckException("Erreur dans la spécification de la plage de compte. Veuillez vérifier vos critères.");
			}
			// s'il s'agit d'un compte unique
			if (bornes.length == 1) {
				quals.addObject(buildPossibleLikeCritere(field, bornes[0].trim()));
			}
			else if (bornes.length == 2) {
				// On a deux comptes, donc on fait uen fourchette
				quals.addObject("(" + field + ">='" + bornes[0].trim() + "' and " + field + "<='" + bornes[1].trim() + "')");
			}
		}

		final String res = "(" + quals.componentsJoinedByString(" OR ") + ")";
		return res;
	}

	private final class ActionCancelImpression extends AbstractAction {
		EOEditingContext _ec;

		/**
             *
             */
		public ActionCancelImpression(EOEditingContext ec) {
			super("Annuler");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interrompre l'impression");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
			_ec = ec;
		}

		public void actionPerformed(ActionEvent e) {
			if (printThread.isAlive()) {
				killCurrentPrintTask(_ec);
				printThread.interrupt();
			}
		}

	}

	protected final String getFileNameWithExtension(final String name, final int format) {
		return name + "." + getExtensionForFormat(format);
	}

	private final String getExtensionForFormat(final int format) {
		String res = "";
		switch (format) {
		case FORMATPDF:
			res = "pdf";
			break;

		case FORMATXLS:
			res = "xls";
			break;

		case FORMATJXLS:
			res = "xls";
			break;

		default:
			break;
		}
		return res;
	}

	protected static final String buildConditionFromPrimaryKeyAndValues(EOEditingContext ec, String keyNameInEos, String attributeName, NSArray eos) {
		String res = "";
		for (int i = 0; i < eos.count(); i++) {
			EOEnterpriseObject element = (EOEnterpriseObject) eos.objectAtIndex(i);
			if (res.length() > 0) {
				res = res + " or ";
			}
			res = res + attributeName + " = " + ServerProxy.serverPrimaryKeyForObject(ec, element).valueForKey(keyNameInEos);
		}
		if (eos.count() > 0) {
			res = "(" + res + ")";
		}
		return res;
	}

	public Dimension defaultDimension() {
		return getWindowSize();
	}

	public ZAbstractPanel mainPanel() {
		return myPanel();
	}

	public String title() {
		return getWindowTitle();
	}

	public class ZKarukeraImprPanelListener implements ZKarukeraImprPanel.IZKarukeraImprlListener {

		public Action actionImprimer() {
			return actionImprimer;
		}

		public Action actionClose() {
			return actionClose;
		}

		public HashMap getFilters() {
			return myFilters;
		}

		public AgregatsCtrl getAgregatCtrl() {
			return ZKarukeraImprCtrl.this.getAgregatCtrl();
		}

		public ZEOComboBoxModel typeOperationsModel() {
			return typeOperationsModel;
		}

		public ZEOComboBoxModel typeJournalModel() {
			return typeJournalModel;
		}

		public Action actionSelectOrigine() {
			return actionSelectOrigine;
		}

	}

	public final void resetFilter() {
		myFilters.clear();
	}

	public AgregatsCtrl getAgregatCtrl() {
		try {
			if (agregatCtrl == null) {
				agregatCtrl = new AgregatsCtrl(getActionId());
			}
			return agregatCtrl;
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}
		return null;

	}

	private final ZEOSelectionDialog createOrigineDialog(EOTypeOperation top) {

		EODisplayGroup dg2 = null;
		if (top != null) {
			if (dispayGroupsOrigine.get(top) == null) {
				final EODisplayGroup dg = new EODisplayGroup();
				NSArray res = null;
				if (EOTypeOperation.TYPE_OPERATION_LUCRATIVE.equals(top.topLibelle())) {
					res = EOsFinder.getOriginesLucratives(getEditingContext(), getExercice());
				}
				else if (EOTypeOperation.TYPE_OPERATION_CONVENTION.equals(top.topLibelle())) {
					res = EOsFinder.getOriginesConvRA(getEditingContext(), getExercice());
				}
				dg.setObjectArray(res);
				dispayGroupsOrigine.put(top, dg);
			}

			dg2 = (EODisplayGroup) dispayGroupsOrigine.get(top);

		}

		final Vector filterFields = new Vector(2, 0);
		filterFields.add("oriLibelle");

		final Vector myColsTmp = new Vector(2, 0);
		final ZEOTableModelColumn col1 = new ZEOTableModelColumn(dg2, "oriLibelle", "Libellé");
		myColsTmp.add(col1);

		ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMyDialog(), "Sélection d'une origine", dg2, myColsTmp, "Veuillez sélectionner une origine dans la liste", filterFields);
		dialog.setFilterPrefix("*");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" like %@");
		return dialog;
	}

	private final EOOrigine selectOrigineIndDialog(EOTypeOperation top) {
		setWaitCursor(true);
		EOOrigine res = null;
		final ZEOSelectionDialog origineDialog = createOrigineDialog(top);

		if (origineDialog.open() == ZEOSelectionDialog.MROK) {
			final NSArray selections = origineDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOOrigine) selections.objectAtIndex(0);
			}
			myFilters.put(ORIGINE_FILTER_KEY, res);

		}
		setWaitCursor(false);
		return res;
	}

	private final class ActionSrchOrigine extends AbstractAction {

		public ActionSrchOrigine() {
			super("");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
		}

		public void actionPerformed(ActionEvent e) {
			selectOrigineIndDialog((EOTypeOperation) myFilters.get(TYPE_OPERATION_FILTER_KEY));
			((ZKarukeraImprPanel) myPanel()).updateOrigine();
		}

	}

	protected void initTypeOperationsModel() {
		try {
			NSArray typeOperations = EOsFinder.getTypeOperationsPubliques(getEditingContext());
			typeOperationsModel = new ZEOComboBoxModel(typeOperations, EOTypeOperation.TOP_LIBELLE_KEY, "Toutes", null);
		} catch (ZFinderException e) {
			e.printStackTrace();
		}
	}
}
