/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.CoherenceSoldePanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CoherenceSoldeCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression de la cohérence des soldes de compte";
	private final Dimension WINDOW_SIZE = new Dimension(609, 190);
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR013;
	private static final String JASPERFILENAME_COHERENCESOLDE = "coherence_solde_sql.jasper";

	private final CoherenceSoldePanel myPanel;
	private AgregatsCtrl _agregatCtrl;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public CoherenceSoldeCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			//if (gestions.count() + gestionsSacd.count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		//   gestionsSacd = EOsFinder.getSACDGestion(getEditingContext(), getExercice());
		//  gestionsSacdModel = new ZEOComboBoxModel(gestionsSacd,"gesCode","Hors SACD",new SACDFormat());
		//getAgregatCtrl().init(true, true, true, true, true);
		myPanel = new CoherenceSoldePanel(new CoherenceSoldePanelListener());

		initFilters();

		getFilters().put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		getFilters().put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, getComptabilite());
		getFilters().put(CoherenceSoldePanel.SHOWALL, Boolean.FALSE);
	}

	protected final void imprimer() {
		try {
			setWaitCursor(true);
			ZLogger.debug(myFilters);
			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			final String filePath = imprimerCoherenceSolde(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			setWaitCursor(false);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}

		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		}
	}

	public String imprimerCoherenceSolde(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		String modeleJasper = JASPERFILENAME_COHERENCESOLDE;
		NSMutableDictionary params = new NSMutableDictionary(parametres);
		final String req = buildSql(ec, params, paramRequete);
		return imprimerReportByThread(ec, temporaryDir, params, modeleJasper, getFileNameWithExtension(ZFileUtil.removeExtension(modeleJasper), FORMATPDF), req, win, FORMATPDF, null);
	}

	private String buildSql(final EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		//final EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		final EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "e.exe_ordre", new NSArray(exercice));
		final String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e.com_ordre", new NSArray(comptabilite));
		final String condCompte = buildCriteresPlanco("ecd.pco_num", (String) paramRequete.get(EOPlanComptable.PCO_NUM_KEY));
		//String condsacd = "";

		//final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		//		if (gesCodeSACD != null) {
		//			params.takeValueForKey(gesCodeSACD.gesCode(), "SACD");
		//			condsacd = " ecd.ges_code = '" + gesCodeSACD.gesCode() + "'";
		//		}
		//		else {
		//			// Récupérer tous les SACDs
		//			final NSArray sacds = EOsFinder.getSACDGestionForComptabilite(ec, comptabilite, exercice);
		//			// on les exclue de la requete
		//			for (int i = 0; i < sacds.count(); i++) {
		//				final EOGestion element = (EOGestion) sacds.objectAtIndex(i);
		//				if (condsacd.length() > 0) {
		//					condsacd = condsacd + "and";
		//				}
		//				condsacd = condsacd + " ecd.ges_code <> '" + element.gesCode() + "' ";
		//			}
		//		}
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecd.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		String req = "select exe_ordre, pco_num,pco_libelle, pco_sens_solde, debit, credit, " +
				"abs(debit-credit) as solde, decode(greatest(abs(debit), abs(credit)), " +
				"abs(debit),  debit-credit  ,0) as debiteur, "
				+ "decode(greatest(abs(debit), abs(credit)), abs(credit), credit-debit  ,0) as crediteur, " +
				"MARACUJA.check_erreur_solde(pco_sens_solde, debit, credit) as erreur  from  "
				+ "( select e.exe_ordre, pco.pco_num, pco.pco_libelle, pco.pco_sens_solde, sum(ecd_debit) as debit, sum(ecd_credit) as credit" +
				"        from MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e, MARACUJA.plan_comptable_exer pco" +
				"        where ecd.ecr_ordre=e.ecr_ordre"
				+ "        and ecd.pco_num=pco.pco_num " +
				"        and ecd.exe_ordre=pco.exe_ordre " +
				"        and substr(e.ecr_etat,1,1)='" + EOEcriture.ecritureValide.substring(0, 1) + "' " +
				"        and e.ecr_numero>0 ";

		if (condCompte.length() > 0) {
			req += " and " + condCompte;
		}

		req += " and " + cond + " and " + condCompta + (condGestion.length() != 0 ? " and " + condGestion : "");

		//		if (condsacd.length() != 0) {
		//			req += " and " + condsacd;
		//		}
		req += "        group by e.exe_ordre, pco.pco_num,pco.pco_libelle, pco.pco_sens_solde" + "        ) ";

		if (Boolean.FALSE.equals(paramRequete.get("showall"))) {
			req = " select * from (" + req + ") where erreur is not null ";
		}
		req += "        order by exe_ordre, pco_num";

		// return imprimerReportAndWait(ec, temporaryDir, params,
		return req;
	}

	/**
	 * Permet d'imprimer directement (sans passer par le dialogue de sélection) en spécifiant les filtres d'impression.
	 * 
	 * @param dialog Fenetre active, servira de fenetre parente pour le panneau d'attente
	 * @param filtres
	 */
	public final void imprimerSilent(final ZKarukeraDialog dialog) {
		if (dialog != null) {
			setMyDialog(dialog);
		}
		imprimer();
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		myFilters.put("masquerTauxNuls", Boolean.FALSE);
	}

	public Map getFilters() {
		return myFilters;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {

			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();

			//	myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class CoherenceSoldePanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements CoherenceSoldePanel.ICoherenceSoldePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#getPcoMap()
		 */
		public Map getPcoMap() {
			return pcoMap;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	@Override
	public AgregatsCtrl getAgregatCtrl() {
		try {
			if (_agregatCtrl == null) {
				_agregatCtrl = new AgregatsCtrl(getActionId(), true, true, true, false, false);
			}
			return _agregatCtrl;
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}
		return null;

	}
}
