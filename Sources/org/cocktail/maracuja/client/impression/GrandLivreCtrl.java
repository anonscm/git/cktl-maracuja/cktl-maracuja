/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.GrandLivrePanel;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class GrandLivreCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression du grand livre";
	private final Dimension WINDOW_SIZE = new Dimension(609, 250);
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR002;
	private static final String JASPERFILENAME_GRAND_LIVRE = "grandlivre_sql.jasper";
	private GrandLivrePanel myPanel;
	//    protected final ActionImprimerXls actionImprimerXls = new ActionImprimerXls();

	public final static String MDL_TRI_NUMERO = "Trier par numéros";
	public final static String MDL_TRI_LIBELLE = "Trier par libellés";
	public final static String MDL_TRI_MONTANT = "Trier par montant";

	private DefaultComboBoxModel comboBoxModeleModel;

	private final ActionXls2 actionImprimerXls2 = new ActionXls2();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public GrandLivreCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}
		comboBoxModeleModel = new DefaultComboBoxModel();
		comboBoxModeleModel.addElement(MDL_TRI_NUMERO);
		comboBoxModeleModel.addElement(MDL_TRI_LIBELLE);
		comboBoxModeleModel.addElement(MDL_TRI_MONTANT);

		myPanel = new GrandLivrePanel(new GrandLivrePanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerGrandLivre(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), FORMATPDF);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerXls2() {
		try {
			NSArray headers = new NSArray(new Object[] {
					"Compte",
					"Libelle compte",
					"Numero",
					"Date",
					"Gestion",
					"Convention",
					"Libelle ecriture",
					"Debit",
					"Credit"
			});
			NSArray champs = new NSArray(new Object[] {
					"PCO_NUM", "PCO_LIBELLE", "ECR_NUMERO", "ECR_DATE", "GES_CODE", "CONV_NUMERO", "ECD_LIBELLE", "ECD_DEBIT", "ECD_CREDIT"
			});
			NSArray keysToSum = new NSArray(new Object[] {
					"ECD_DEBIT", "ECD_CREDIT"
			});

			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			final String sqlQuery = buildSql(getEditingContext(), myFilters, dico);
			exportXlsTab(headers, champs, keysToSum, dico, sqlQuery);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put("dateDebut", ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			initFilters();
			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class GrandLivrePanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements GrandLivrePanel.IGrandLivrePanelListener {

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public Action actionImprimerXls() {
			return actionImprimerXls2;
		}

		public DefaultComboBoxModel comboModeleModel() {
			return comboBoxModeleModel;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	private String buildSql(EOEditingContext ec, HashMap paramRequete, NSMutableDictionary params) throws Exception {
		final String tri = (String) comboBoxModeleModel.getSelectedItem();

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);

		String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		String condsacd = "";
		String condGestion = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condCompte = buildCriteresPlanco("ecriture_detail.pco_num", (String) paramRequete.get("planco"));

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		// On fait les requetes pour recupérer les cumuls precedents
		String reqCumuls = "select ecriture_detail.pco_num, nvl(sum(nvl(ecriture_detail.ecd_debit,0)),0) as TOT_DEBIT, nvl(sum(nvl(ecriture_detail.ecd_credit,0)),0) as TOT_CREDIT "
				+ "from MARACUJA.ecriture, MARACUJA.ecriture_detail " +
				"where " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre " + "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateDebutStr + "' "
				+ "and ecriture.ecr_numero > 0    " + "and substr(ecriture.ecr_etat,1,1)='" + EOEcriture.ecritureValide.substring(0, 1) + "' "
				+ " and " + condCompte
				+ " and " + cond + " and "
				+ condCompta + (condGestion.length() != 0 ? " and " + condGestion : "");

		if (condsacd.length() != 0) {
			reqCumuls += " and " + condsacd;
		}
		reqCumuls += " group by ecriture_detail.pco_num";

		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOUR_DEBUT");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_FIN");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		if (dateDebut != null && dateFin != null) {
			if (dateDebut.equals(dateFin)) {
				params.takeValueForKey("Journée du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOURNEES");
			}
			else {
				params.takeValueForKey("Journées du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut) + " au " + new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOURNEES");
			}
		}

		String req = "select distinct ecd_ordre, com_libelle, ecr_numero, ecr_date_saisie as ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, ecriture.exe_ordre, ecriture_detail.pco_num, ecd_montant, "
				+ "maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle, ecriture_detail.ges_code, y.TOT_DEBIT as PREC_DEBIT, y.TOT_CREDIT as PREC_CREDIT, " +
				"ac.numero as conv_numero "
				+ "from MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail,  " + "(" + reqCumuls
				+ ") y," +
				" maracuja.v_accords_contrat ac " +
				"where " + "ecriture_detail.pco_num = y.pco_num (+)"
				+ "and ecriture.ecr_ordre = ecriture_detail.ecr_ordre "
				+ "and ecriture_detail.con_ordre=ac.con_ordre(+) "
				+ "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " + "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' "
				+ "and ecriture.ecr_numero > 0    "
				+ " and substr(ecriture.ecr_etat,1,1)='" + EOEcriture.ecritureValide.substring(0, 1) + "' "
				+ " and " + condCompte + " and " + cond + " and " + condCompta + (condGestion.length() != 0 ? " and " + condGestion : "");
		if (condsacd.length() != 0) {
			req += " and " + condsacd;
		}

		if (GrandLivreCtrl.MDL_TRI_LIBELLE.equals(tri)) {
			req += " order by ecriture_detail.pco_num, ecd_libelle, ecd_index";
		}
		else if (GrandLivreCtrl.MDL_TRI_MONTANT.equals(tri)) {
			req += " order by ecriture_detail.pco_num, ecd_montant, ecr_numero, ecd_index";
		}
		else {
			req += " order by ecriture_detail.pco_num, ecr_numero, ecd_index";
		}

		return req;
	}

	/**
	 * @param ec
	 * @param temporaryDir
	 * @param parametres
	 * @param paramRequete Map contenant les paramètres de la requete à effectuer. Les parametres sont dateDebut, dateFin, gesCodeSACD
	 * @param tri
	 * @return
	 * @throws Exception
	 */
	public String imprimerGrandLivre(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win, final int format) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		NSMutableDictionary params = new NSMutableDictionary(parametres);
		final String req = buildSql(ec, paramRequete, params);
		return imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_GRAND_LIVRE, getFileNameWithExtension("grandlivre", format), req, win, format, null);
	}

	public final class ActionXls2 extends AbstractAction {

		public ActionXls2() {
			super("Excel (*)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Export Excel tabulaire");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls2();
		}

	}

}
