package org.cocktail.maracuja.client.impression;

import java.awt.Window;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;

import com.webobjects.eocontrol.EOEditingContext;

public class BalanceCtrlCpteFi extends BalanceCtrl {
	private AgregatsCtrl _agregatCtrl;

	public BalanceCtrlCpteFi(EOEditingContext editingContext) throws Exception {
		super(editingContext);
	}

	/**
	 * Ouvre une fenetre spécifique pour la sortie du cadre 1 du compte financier
	 * 
	 * @param dial
	 */
	public final void openDialogForCpteFiCadre1(final Window dial, final boolean avantSolde6_7) {
		ZKarukeraDialog win = createModalDialog(dial);
		win.setTitle("Balance avant solde classes 6 & 7");
		this.setMyDialog(win);
		try {
			initFilters();

			//Préselections
			myFilters.put("typeBalance", BALANCE_GEN_CS);

			//Désactiver
			//getAgregatCtrl().init(true, true, true, false, true);
			myPanel.desactiverCpteFi();

			myPanel().updateData();

			//			myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, (gestionsSacdModel.getSelectedEObject()));
			//			myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, (gestionsModel.getSelectedEObject()));

			if (avantSolde6_7) {
				myFilters.put("avantSolde6_7", Boolean.TRUE);
			}

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	@Override
	public AgregatsCtrl getAgregatCtrl() {
		try {
			if (_agregatCtrl == null) {
				_agregatCtrl = new AgregatsCtrl(getActionId(), true, true, true, false, true);
			}
			return _agregatCtrl;
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}
		return null;

	}

	protected String getActionId() {
		return null;
	}
}
