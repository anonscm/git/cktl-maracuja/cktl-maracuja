/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.JournalTvaPanel;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class JournalTvaCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression du journal de TVA";
	private final Dimension WINDOW_SIZE = new Dimension(609, 250);

	private final String ACTION_ID = ZActionCtrl.IDU_IMPR011;

	private static final String JASPERFILENAME_TVA_DEDUCTIBLE = "tva_deductible_sql.jasper";
	private static final String JASPERFILENAME_TVA_COLLECTEE = "tva_collectee_sql.jasper";

	public final static String TVATYPE_DEDUCTIBLE = "TVA déductible";
	public final static String TVATYPE_COLLECTEE = "TVA collectée";

	private JournalTvaPanel myPanel;
	private final TVATypeModel typeModel = new TVATypeModel();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public JournalTvaCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		myPanel = new JournalTvaPanel(new JournalTvaPanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);

			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerJournalTva(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public String imprimerJournalTva(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		//	EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String regroupement = (String) paramRequete.get("regroupement");
		String modeleJasper = null;
		if (JournalTvaCtrl.TVATYPE_DEDUCTIBLE.equals(paramRequete.get("typeTva"))) {
			modeleJasper = JASPERFILENAME_TVA_DEDUCTIBLE;
		}
		else {
			modeleJasper = JASPERFILENAME_TVA_COLLECTEE;
		}

		if (modeleJasper == null) {
			throw new DefaultClientException("Modele d'impression non récupéré");
		}
		NSMutableDictionary params = new NSMutableDictionary(parametres);

		final String req = buildSql(ec, params, paramRequete);
		return imprimerReportByThread(ec, temporaryDir, params, modeleJasper, getFileNameWithExtension(ZFileUtil.removeExtension(modeleJasper), FORMATPDF), req, win, FORMATPDF, null);
	}

	private String buildSql(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get("dateFin");
		final String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		String condGestion = "";
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "m.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");
		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOUR_DEBUT");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_FIN");
		String req = null;

		if (JournalTvaCtrl.TVATYPE_DEDUCTIBLE.equals(paramRequete.get("typeTva"))) {
			String condfin = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe.exe_ordre", new NSArray(exercice.getPrevEOExercice()));
			condfin = condfin.replaceAll("=", ">=");
			condfin = condfin
					+ "and to_char(e.ecr_date,'YYYYMMDD') >= '" + dateDebutStr + "' " +
					"and to_char(e.ecr_date,'YYYYMMDD') < '" + dateFinStr + "' "
					//					+ "and to_char(e.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " + 
					//					"and to_char(e.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' "
					+ (condGestion.length() != 0 ? " and " + condGestion : "");

			if (exercice.exeExercice().intValue() <= 2006) {
				req = "select exe.exe_exercice, m.man_id,m.ges_code, m.man_numero, ecd.pco_num, maracuja.api_planco.get_pco_libelle(ecd.pco_num, e.exe_ordre) as pco_libelle, "
						+ "sum(jf.DEP_HT_SAISIE) as montant_ht_saisie, (sum(dep_ttc) - sum(jf.DEP_HT_SAISIE)) as tva_facturee, sum(dep_ttc) as total_ttc,"
						+ "ecd. ecd_montant as tva_deduite, sum(dep_mont) as montant_budgetaire  " +
						"from MARACUJA.ecriture_detail ecd, MARACUJA.mandat_detail_ecriture mde, MARACUJA.mandat m, MARACUJA.exercice exe, MARACUJA.v_jefy_multiex_facture jf, MARACUJA.ecriture e " +
						"where ecd.ecd_ordre = mde.ecd_ordre  " + "and ecd.ecr_ordre = e.ecr_ordre  "
						+ "and mde.man_id=m.man_id  " + "and m.EXE_ORDRE=exe.exe_ordre  " + "and jf.exe_exercice=exe.exe_exercice  " + "and jf.MAN_ORDRE=m.MAN_ORDRE  " + "and mde.mde_origine='VISA TVA' " + "and substr(e.ecr_etat,1,1)='"
						+ EOEcriture.ecritureValide.substring(0, 1)
						+ "' "
						+ "and ecd.pco_num like '4456%' "
						+ "and "
						+ condfin
						+ " "
						+ "group by exe.exe_exercice, m.man_id,m.ges_code, m.man_numero, ecd.pco_num, ecd.ecd_montant  "
						+ "UNION ALL  "
						+ "select exe.exe_exercice, m.tit_id as man_id,m.ges_code, m.tit_numero as man_numero, ecd.pco_num, maracuja.api_planco.get_pco_libelle(ecd.pco_num, e.exe_ordre) as pco_libelle, "
						+ "sum(jf.DEP_HT_SAISIE) as montant_ht_saisie, (sum(dep_ttc) - sum(jf.DEP_HT_SAISIE)) as tva_facturee, "
						+ "sum(dep_ttc) as total_ttc,(-ecd. ecd_montant) as tva_deduite, sum(dep_mont) as montant_budgetaire "
						+ "from MARACUJA.ecriture_detail ecd, MARACUJA.titre_detail_ecriture mde, MARACUJA.titre m, MARACUJA.v_jefy_multiex_titre jm, MARACUJA.exercice exe, MARACUJA.v_jefy_multiex_facture jf, MARACUJA.ecriture e "
						+ "where ecd.ecd_ordre = mde.ecd_ordre  "
						+ "and ecd.ecr_ordre = e.ecr_ordre  "
						+ "and mde.tit_id=m.tit_id  "
						+ "and m.EXE_ORDRE=exe.exe_ordre  "
						+ "and jm.exe_exercice=exe.exe_exercice  "
						+ "and jm.tit_ordre=m.tit_ordre  "
						+ "and jf.exe_exercice=exe.exe_exercice  "
						+ "and jf.dep_ORDRE=jm.dep_ORDRE  "
						+ "and mde.tde_origine='VISA' "
						+ "and substr(e.ecr_etat,1,1)='"
						+ EOEcriture.ecritureValide.substring(0, 1)
						+ "' "
						+ "and ecd.pco_num=p.pco_num  "
						+ "and ecd.pco_num like '4456%'  "
						+ "and "
						+ condfin
						+ " "
						+ "group by exe.exe_exercice, m.tit_id,m.ges_code, m.tit_numero , ecd.pco_num, ecd.ecd_montant  " + "order by pco_num, ges_code, man_numero";

			}
			else {
				req = "select e.exe_ordre exe_exercice, m.man_id, m.ges_code, m.man_numero, ecd.pco_num, maracuja.api_planco.get_pco_libelle(ecd.pco_num, e.exe_ordre) as pco_libelle, SUM(d.DPCO_HT_SAISIE) AS montant_ht_saisie, "
						+ "SUM(d.DPCO_TVA_SAISIE) AS tva_facturee, m.man_ttc AS total_ttc, SIGN(m.man_ht)*ecd.ecd_montant AS tva_deduite,         "
						+ "m.MAN_HT AS montant_budgetaire "
						+ "FROM MARACUJA.ECRITURE_DETAIL ecd, MARACUJA.MANDAT_DETAIL_ECRITURE mde,MARACUJA.MANDAT m, MARACUJA.exercice exe, jefy_depense.depense_ctrl_planco d,MARACUJA.ECRITURE e " +
						"WHERE ecd.ecd_ordre = mde.ecd_ordre " +
						"AND ecd.ecr_ordre = e.ecr_ordre  "
						+ "AND mde.man_id = m.man_id  "
						+ "AND d.man_id = m.man_id "
						+ "AND mde.mde_origine = 'VISA TVA' "
						+ "AND m.exe_ordre=exe.exe_ordre "
						+ "AND SUBSTR (e.ecr_etat, 1, 1) = '" + EOEcriture.ecritureValide.substring(0, 1) + "' " +
						"AND ecd.pco_num LIKE '4456%' " + "and " + condfin + " "
						+ "GROUP BY e.exe_ordre, m.man_id,m.ges_code, m.man_numero, ecd.pco_num, ecd.ecd_montant, m.man_ttc,m.man_ht " + "order by pco_num, ges_code, exe_exercice,man_numero";

			}

			return req;
		}

		// Sinon JOURNAL TVA COLLECTEE
		//on prend la date de visa le journal doit prendre les opérations prises en charge durant le mois.
		String condfin2 = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "m.exe_ordre", new NSArray(exercice.getPrevEOExercice()));
		condfin2 = condfin2.replaceAll("=", ">=");
		condfin2 = condfin2
				+ " and to_char( b.bor_date_visa,'YYYYMMDD') >= '" + dateDebutStr + "' " +
				"and to_char( b.bor_date_visa,'YYYYMMDD') < '" + dateFinStr + "' " + (condGestion.length() != 0 ? " and " + condGestion : "");

		if (exercice.exeExercice().intValue() <= 2006) {
			req = "select b.ges_code, tit_numero, tit_libelle, tit_ht, round(tit_tva*100/tit_ht, 1) as taux_tva, tit_tva, tit_ttc " + "from bordereau b,titre m, type_bordereau tb " + "where b.bor_id=m.bor_id " + "and b.tbo_ordre=tb.tbo_ordre " + "and m.TIT_ETAT='VISE' " + "and (tb.tbo_sous_type<>'"
					+ EOTypeBordereau.SOUS_TYPE_REVERSEMENTS + "' and tb.tbo_sous_type<>'" + EOTypeBordereau.SOUS_TYPE_REDUCTION + "' )  " + "and " + condfin2 + " union all " + "select b.ges_code, tit_numero, tit_libelle, -tit_ht, round(tit_tva*100/tit_ht, 1) as taux_tva, -tit_tva, -tit_ttc  "
					+ "from MARACUJA.bordereau b,titre m, MARACUJA.type_bordereau tb " +
					"where b.bor_id=m.bor_id " + "and b.tbo_ordre=tb.tbo_ordre " + "and m.TIT_ETAT='VISE' " + "and (tb.tbo_sous_type='" + EOTypeBordereau.SOUS_TYPE_REDUCTION + "' ) " + "and " + condfin2
					+ "order by taux_tva desc, ges_code, tit_numero";
		}
		else {

			String condfin3 = "";
			if (Boolean.TRUE.equals(paramRequete.get("masquerTauxNuls"))) {
				condfin3 = " taux_tva<>0 ";
			}

			req = "select * from ("
					+ "-- Recette avec facture récupération du taux de TVA \n"
					+ "select b.ges_code, m.exe_ordre, tit_numero, tit_libelle, " +
					"sum(fpl.FLIG_TOTAL_HT) TIT_HT, nvl(tva.TVA_TAUX,0) TAUX_TVA, " +
					"(sum(fpl.FLIG_TOTAL_TTC)-sum(fpl.FLIG_TOTAL_HT)) TIT_TVA, " +
					"sum(fpl.FLIG_TOTAL_TTC) TIT_TTC  "
					+ "from maracuja.bordereau b, maracuja.titre m, maracuja.type_bordereau tb, maracuja.recette r, " +
					"jefy_recette.recette_ctrl_planco rcp, jefy_recette.recette rec, jefy_recette.facture f, "
					+ "jefy_recette.facture_papier fp, jefy_recette.facture_papier_ligne fpl, jefy_admin.tva tva " +
					"where b.bor_id=m.bor_id  " +
					" and b.tbo_ordre=tb.tbo_ordre  " +
					" and m.tit_id = r.tit_id " +
					" and r.rec_ordre = rcp.RPCO_ID " +
					" and rcp.rec_id = rec.rec_id "
					+ " and rec.FAC_ID = f.FAC_ID " +
					" and f.fac_id = fp.FAC_ID " +
					" and fp.fap_id = fpl.FAP_ID " +
					" and fpl.tva_id = tva.TVA_ID " +
					" and m.TIT_ETAT='VISE'  " +
					" and (tb.tbo_sous_type<>'"
					+ EOTypeBordereau.SOUS_TYPE_REVERSEMENTS
					+ "'  "
					+ " and tb.tbo_sous_type<>'"
					+ EOTypeBordereau.SOUS_TYPE_REDUCTION
					+ "' )   "
					+ " and "
					+ condfin2
					+ " "
					+ "GROUP BY b.ges_code, m.exe_ordre, tit_numero, tit_libelle, tva.TVA_TAUX "
					+ "UNION ALL "
					+ " -- Recette sans facture estimation du taux de TVA non stocké \n"
					+ "select b.ges_code, m.exe_ordre, tit_numero, tit_libelle, sum(f.FAC_HT_SAISIE) TIT_HT,  "
					+ "tva.TVA_TAUX TAUX_TVA, sum(f.FAC_TVA_SAISIE) TIT_TVA, sum(f.FAC_TTC_SAISIE) TIT_TTC "
					+ "from maracuja.bordereau b, maracuja.titre m, maracuja.type_bordereau tb, maracuja.recette r, "
					+ "jefy_recette.recette_ctrl_planco rcp, jefy_recette.recette rec, jefy_recette.facture f, jefy_admin.tva tva "
					+ "where b.bor_id=m.bor_id  "
					+ " and b.tbo_ordre=tb.tbo_ordre  "
					+ " and m.tit_id = r.tit_id "
					+ " and r.rec_ordre = rcp.RPCO_ID "
					+ " and rcp.rec_id = rec.rec_id "
					+ " and rec.FAC_ID = f.FAC_ID "
					+ "  and tit_ht<>0 "
					+ " AND (round(round(100*tit_tva *100/tit_ht)/100,1) = tva.TVA_taux  or trunc(1000*tit_tva/tit_ht)/10 = tva.tva_taux) "
					+ " and f.fac_id in (select fac_id from jefy_recette.facture minus select fac_id from jefy_recette.facture_papier) "
					+ " and m.TIT_ETAT='VISE'  "
					+ " and (tb.tbo_sous_type<>'"
					+ EOTypeBordereau.SOUS_TYPE_REVERSEMENTS
					+ "'  "
					+ " and tb.tbo_sous_type<>'"
					+ EOTypeBordereau.SOUS_TYPE_REDUCTION
					+ "' )   "
					+ " and "
					+ condfin2
					+ " "
					+ "GROUP BY b.ges_code, m.exe_ordre, tit_numero, tit_libelle, tva_taux "
					+ "union all "
					+ "-- reduction complete sur recette avec facture recuperation du taux de TVA dans la facture\n"
					+ "select b.ges_code, m.exe_ordre, tit_numero,  tit_libelle,-sum(fpl.FLIG_TOTAL_HT) TIT_HT,    " +
					"nvl(tva.TVA_TAUX,0) TAUX_TVA, -(sum(fpl.FLIG_TOTAL_TTC)-sum(fpl.FLIG_TOTAL_HT)) TIT_TVA, " +
					"-sum(fpl.FLIG_TOTAL_TTC) TIT_TTC  " +
					"from maracuja.bordereau b,    " +
					"maracuja.titre m,    " +
					"maracuja.type_bordereau tb,   " +
					" maracuja.recette r,    " +
					"jefy_recette.recette_ctrl_planco rcp,    " +
					"jefy_recette.recette rec,    " +
					"jefy_recette.facture f,    " +
					"jefy_recette.facture_papier fp,    " +
					"jefy_recette.facture_papier_ligne fpl,    " +
					"jefy_admin.tva tva  where" +
					" b.bor_id                                =m.bor_id  " +
					"and b.tbo_ordre                               =tb.tbo_ordre  " +
					"and m.tit_id                                  = r.tit_id  " +
					"and r.rec_ordre                               = rcp.RPCO_ID  " +
					"and rcp.rec_id                                = rec.rec_id  " +
					"and rec.FAC_ID                                = f.FAC_ID  " +
					"and f.fac_id                                  = fp.FAC_ID  " +
					"and fp.fap_id                                 = fpl.FAP_ID  " +
					"and fpl.tva_id                                = tva.TVA_ID (+)  " +
					" and m.TIT_ETAT='VISE'  " + " and (tb.tbo_sous_type='" + EOTypeBordereau.SOUS_TYPE_REDUCTION
					+ "' )  "
					+ " and "
					+ condfin2
					+ " and abs(rec.rec_montant_budgetaire) = abs(f.fac_montant_budgetaire) "
					+ "GROUP BY b.ges_code, m.exe_ordre, tit_numero, tit_libelle, tva.TVA_TAUX, m.tit_ttc "
					//+ " having   -sum(fpl.FLIG_TOTAL_TTC) = m.TIT_TTC "
					+ "union all "
					+ "-- Réduction partielle ou réduction sans facture estimation du taux de TVA non stocké\n"
					+ "select b.ges_code, m.exe_ordre, tit_numero,    tit_libelle,    sum(rec.REC_HT_SAISIE) TIT_HT,    nvl(tva.TVA_TAUX,0) TAUX_TVA,    " +
					"sum(rec.REC_TVA_SAISIE) TIT_TVA,    sum(rec.REC_TTC_SAISIE) TIT_TTC  " +
					"from maracuja.bordereau b,    " +
					"maracuja.titre m,    " +
					"maracuja.type_bordereau tb,    " +
					"maracuja.recette r,    " +
					"jefy_recette.recette_ctrl_planco rcp,    " +
					"jefy_recette.recette rec,    " +
					"jefy_recette.facture f,    " +
					"jefy_admin.tva tva  " +
					"where b.bor_id                          =m.bor_id  " +
					"and b.tbo_ordre                         =tb.tbo_ordre  " +
					"and m.tit_id                            = r.tit_id  " +
					"and r.rec_ordre                         = rcp.RPCO_ID  " +
					"and rcp.rec_id                          = rec.rec_id  " +
					"and rec.FAC_ID                          = f.FAC_ID  "
					+ "  and tit_ht<>0 "
					+ "AND (round(round(100*tit_tva *100/tit_ht)/100,1) = tva.TVA_taux  or trunc(1000*tit_tva/tit_ht)/10 = tva.tva_taux)  " +
					"and f.fac_id                           in   " +
					" (select fac_id from jefy_recette.facture minus    select fac_id from jefy_recette.facture_papier    )"
					+ " and m.TIT_ETAT='VISE'  "
					+ " and (tb.tbo_sous_type='"
					+ EOTypeBordereau.SOUS_TYPE_REDUCTION
					+ "' )  "
					+ " and "
					+ condfin2
					+ " "
					+ "group by b.ges_code, m.exe_ordre, tit_numero, tit_libelle, tva_taux "
					+ "union all "
					+ "-- Réduction partielle avec facture initiale estimation du taux de TVA non stocké\n"
					+ "  select b.ges_code, m.exe_ordre, tit_numero,    tit_libelle,    sum(rec.REC_HT_SAISIE) TIT_HT,    nvl(tva.TVA_TAUX,0) TAUX_TVA,    " +
					"sum(rec.REC_TVA_SAISIE) TIT_TVA,    sum(rec.REC_TTC_SAISIE) TIT_TTC  " +
					"from maracuja.bordereau b,    " +
					"maracuja.titre m,    " +
					"maracuja.type_bordereau tb,    " +
					"maracuja.recette r,    " +
					"jefy_recette.recette_ctrl_planco rcp,    " +
					"jefy_recette.recette rec,    " +
					"jefy_recette.facture f,    " +
					"jefy_recette.facture_papier fp,    " +
					"jefy_admin.tva tva  " +
					"where b.bor_id                          =m.bor_id  " +
					"and b.tbo_ordre                         =tb.tbo_ordre  " +
					"and m.tit_id                            = r.tit_id  " +
					"and r.rec_ordre                         = rcp.RPCO_ID  " +
					"and rcp.rec_id                          = rec.rec_id  " +
					"and rec.FAC_ID                          = f.FAC_ID  " +
					"and f.fac_id=fp.fac_id " +
					"AND (round(round(100*tit_tva *100/tit_ht)/100,1) = tva.TVA_taux  or trunc(1000*tit_tva/tit_ht)/10 = tva.tva_taux)  " +
					" and abs(rec.rec_montant_budgetaire)<>abs(f.fac_montant_budgetaire) " +
					" and m.TIT_ETAT='VISE'  "
					+ " and (tb.tbo_sous_type='"
					+ EOTypeBordereau.SOUS_TYPE_REDUCTION
					+ "' )  "
					+ " and "
					+ condfin2
					+ " "
					+ "group by b.ges_code, m.exe_ordre, tit_numero, tit_libelle, tva_taux) "

					+ (condfin3.length() > 1 ? " where " + condfin3 : "")
					+ "order by TAUX_TVA desc, ges_code, exe_ordre, tit_numero";
		}
		return req;
		//return imprimerReportByThreadPdf(ec, temporaryDir, params, JASPERFILENAME_TVA_COLLECTEE, "tva_collectee.pdf", req, win, null);
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.DATE_DEBUT_FILTER_KEY, ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		myFilters.put("typeTva", TVATYPE_DEDUCTIBLE);
		myFilters.put(JournalTvaPanel.MASQUER_TAUX_NULS, Boolean.TRUE);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			initFilters();

			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();
			//		myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			//		myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, ((EOGestion) gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class JournalTvaPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements JournalTvaPanel.IJournalTvaPanelListener {

		//        /**
		//         * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#actionPlancomptableSelect()
		//         */
		//        public AbstractAction actionPlancomptableSelect() {
		//            return actionPlancoSelect;
		//        }

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#getPcoMap()
		 */
		public Map getPcoMap() {
			return pcoMap;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public DefaultComboBoxModel getTypeJournalModel() {
			return typeModel;
		}

		public void setTypeTVA(Object selectedItem) {
			myFilters.put("typeTva", selectedItem);
		}
	}

	private final class TVATypeModel extends DefaultComboBoxModel {
		private final LinkedHashMap typesTVA = new LinkedHashMap();

		public TVATypeModel() {
			typesTVA.put("TVATYPE_DEDUCTIBLE", TVATYPE_DEDUCTIBLE);
			typesTVA.put("TVATYPE_COLLECTEE", TVATYPE_COLLECTEE);

			final Iterator iterator = typesTVA.keySet().iterator();
			while (iterator.hasNext()) {
				addElement(typesTVA.get(iterator.next()));
			}
		}
	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}
}
