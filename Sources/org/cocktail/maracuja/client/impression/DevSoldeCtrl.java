/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.DevSoldePanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DevSoldeCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression de l'état de développement de soldes";
	private final Dimension WINDOW_SIZE = new Dimension(550, 450);
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR004;
	public static final String JASPERFILENAME_DEV_SOLDE_JOUR = "devSoldJOUR_sql.jasper";
	public static final String JASPERFILENAME_DEV_SOLDE_JOUR2 = "devSoldJOUR2_sql.jasper";
	public static final String JASPERFILENAME_DEV_SOLDE_JOUR3 = "devSoldJOUR3_sql.jasper";
	public static final String JXLSFILENAME_DEV_SOLDE = "devsoldjour.xls";
	public static final String SOLDE_KEY = "solde";

	private DevSoldePanel myPanel;
	private final String MODELE1 = "Modèle avec montants écritures";
	private final String MODELE2 = "Modèle simplifé";
	private final String MODELE3 = "Modèle avec émargements";

	public final static String MDL_TRI_NUMERO = "Trier par numéros";
	public final static String MDL_TRI_LIBELLE = "Trier par libellés";
	public final static String MDL_TRI_MONTANT = "Trier par montant";
	public final static String MDL_TRI_GESTION = "Trier par code gestion";

	private final ActionXls actionImprimerXls = new ActionXls();
	private final ActionXls2 actionImprimerXls2 = new ActionXls2();

	private final DefaultComboBoxModel modeleModel;
	private final DefaultComboBoxModel comboBoxModeleTri;
	private final Map modelesMap = new HashMap(2);
	private final CheckBoxSoldeListener cbBoxSoldeListener = new CheckBoxSoldeListener();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public DevSoldeCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		comboBoxModeleTri = new DefaultComboBoxModel();
		comboBoxModeleTri.addElement(MDL_TRI_NUMERO);
		comboBoxModeleTri.addElement(MDL_TRI_LIBELLE);
		comboBoxModeleTri.addElement(MDL_TRI_MONTANT);
		comboBoxModeleTri.addElement(MDL_TRI_GESTION);

		modeleModel = new DefaultComboBoxModel();
		modeleModel.addElement(MODELE3);
		modeleModel.addElement(MODELE1);
		modeleModel.addElement(MODELE2);

		modelesMap.put(MODELE1, JASPERFILENAME_DEV_SOLDE_JOUR);
		modelesMap.put(MODELE2, JASPERFILENAME_DEV_SOLDE_JOUR2);
		modelesMap.put(MODELE3, JASPERFILENAME_DEV_SOLDE_JOUR3);

		myPanel = new DevSoldePanel(new DevSoldePanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}

			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerDevSoldeJour(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), (String) modelesMap.get(myFilters.get(ZKarukeraImprCtrl.MODELE_FILTER_KEY)), FORMATPDF);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public String imprimerDevSoldeJour(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win, final String modeleJasper, final int format) throws Exception {
		if (modeleJasper == null) {
			throw new DefaultClientException("Modele d'impression non récupéré");
		}
		NSMutableDictionary params = new NSMutableDictionary(parametres);

		final String req = buildSql(ec, params, paramRequete);
		return imprimerReportByThread(ec, temporaryDir, params, modeleJasper, getFileNameWithExtension(ZFileUtil.removeExtension(modeleJasper), format), req, win, format, null);
	}

	private String buildSql(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		Date dateDebut = (Date) paramRequete.get(DATE_DEBUT_FILTER_KEY);
		String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.getDateOnly(dateDebut));
		//si 01/01, on n'ajoute pas de condition
		if (dateDebutStr.substring(4).equals("0101")) {
			dateDebutStr = null;
		}

		Date dateFin = (Date) paramRequete.get(DATE_FIN_FILTER_KEY);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		String condExer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		String condGestion = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condCompte = buildCriteresPlanco("ecriture_detail.pco_num", (String) paramRequete.get("planco"));

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		params.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "DATE_DEBUT");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_EDS");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		Boolean solde = (Boolean) paramRequete.get(SOLDE_KEY);

		String req = "select distinct ecd_ordre, ecd_index, ecr_numero, ecr_date, ecr_libelle, ecd_credit, ecd_debit, ecd_reste_emarger, ecd_libelle, ecd_montant, " +
				"ges_code, exe_ordre, pco_num, maracuja.api_planco.get_pco_libelle(pco_num, exe_ordre) as pco_libelle, tot_debit, tot_credit, conv_numero from "
				+ " (select ecd_ordre, ecd_index, ecr_numero, ecr_date_saisie AS ecr_date, ecr_libelle, ecd_credit, " +
				"ecd_debit, ecd_reste_emarger, ecd_libelle, ecd_montant, " +
				"ecriture_detail.ges_code, ecriture_detail.exe_ordre, " +
				"ecriture_detail.pco_num, x.tot_debit, " + "x.tot_credit,   " +
				"ac.numero as conv_numero " +
				"FROM MARACUJA.ecriture, "
				+ " MARACUJA.ecriture_detail, " +
				" maracuja.v_accords_contrat ac, " +
				"(SELECT   comptabilite.com_ordre, ecriture_detail.exe_ordre,ecriture_detail.pco_num, " + " SUM (ecd_debit) AS tot_debit, " + "SUM (ecd_credit) AS tot_credit " + "FROM MARACUJA.comptabilite, MARACUJA.ecriture_detail , MARACUJA.ecriture  "
				+ "WHERE ecriture.com_ordre = comptabilite.com_ordre " + "AND ecriture_detail.ecr_ordre = ecriture.ecr_ordre " + "AND ecriture.ecr_numero <> 0 "
				+ "AND SUBSTR (ecriture.ecr_etat, 1, 1) = 'V' "
				+ "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '"
				+ dateFinStr
				+ "' "
				+ (dateDebutStr != null ? "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " : "")
				+ " and "
				+ condExer
				+ " and "
				+ condCompta
				+ " and "
				+ condCompte
				//+ (condsacd.length() != 0 ? " and " + condsacd : "")
				+ (condGestion.length() != 0 ? " and " + condGestion : "")
				+ "GROUP BY comptabilite.com_ordre, ecriture_detail.exe_ordre,ecriture_detail.pco_num ) x"
				+ " WHERE  ecriture.com_ordre=x.com_ordre  "
				+ "AND (ecriture.ecr_numero <> 0) "
				+ "and ecriture_detail.exe_ordre = x.exe_ordre "
				+ "and ecriture_detail.pco_num = x.pco_num "
				+ "AND ecriture.ecr_ordre = ecriture_detail.ecr_ordre "
				+ "AND ecriture_detail.con_ordre = ac.con_ordre (+)"
				+ "AND SUBSTR (ecriture.ecr_etat, 1, 1) = 'V' "
				+ (solde.booleanValue() ? "AND ecriture_detail.ecd_reste_emarger <> 0 " : "")
				+ "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '"
				+ dateFinStr
				+ "' "
				+ (dateDebutStr != null ? "and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " : "")
				+ ") "
				+ (condGestion.length() != 0 ? " where " : "") + (condGestion.length() != 0 ? condGestion.replaceAll("ecriture_detail.", "") : "");

		final String tri = (String) comboBoxModeleTri.getSelectedItem();

		if (DevSoldeCtrl.MDL_TRI_LIBELLE.equals(tri)) {
			req += " order by pco_num, ecd_libelle";
		}
		else if (DevSoldeCtrl.MDL_TRI_MONTANT.equals(tri)) {
			req += " order by pco_num, ecd_montant, ecr_numero, ecd_index";
		}
		else if (DevSoldeCtrl.MDL_TRI_GESTION.equals(tri)) {
			req += " order by pco_num, ges_code, ecr_numero, ecd_index";
		}
		else {
			req += " order by pco_num, ecr_numero, ecd_index";
		}
		return req;
	}

	private final void imprimerXls() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSql(getEditingContext(), dico, myFilters);
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_DEV_SOLDE, "devsolde.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerXls2() {
		try {
			NSArray headers = new NSArray(new Object[] {
					"Compte", "Libelle compte", "Numero", "Date", "Gestion", "Libelle ecriture", "Debit", "Credit", "Reste a emarger", "N° convention", "Libelle detail"
			});
			NSArray champs = new NSArray(new Object[] {
					"PCO_NUM", "PCO_LIBELLE", "ECR_NUMERO", "ECR_DATE", "GES_CODE", "ECR_LIBELLE", "ECD_DEBIT", "ECD_CREDIT", "ECD_RESTE_EMARGER", "CONV_NUMERO", "ECD_LIBELLE"
			});

			NSArray keysToSum = new NSArray(new Object[] {
					"ECD_DEBIT", "ECD_CREDIT", "ECD_RESTE_EMARGER"
			});

			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			final String sqlQuery = buildSql(getEditingContext(), dico, myFilters);
			exportXlsTab(headers, champs, keysToSum, dico, sqlQuery);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final void initFilters() {
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		myFilters.put(DATE_DEBUT_FILTER_KEY, ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		myFilters.put(ZKarukeraImprCtrl.MODELE_FILTER_KEY, MODELE3);
		myFilters.put(SOLDE_KEY, Boolean.TRUE);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	//	public final void resetFilter() {
	//		myFilters.clear();
	//	}

	private final class DevSoldePanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements DevSoldePanel.IDevSoldePanelListener {

		//		/**
		//		 * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#getPcoMap()
		//		 */
		//		public Map getPcoMap() {
		//			System.out.println(pcoMap.size());
		//			return pcoMap;
		//		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public ComboBoxModel getModelesModel() {
			return modeleModel;
		}

		public Action actionImprimerXls() {
			return actionImprimerXls;
		}

		public ComboBoxModel comboModeleTri() {
			return comboBoxModeleTri;
		}

		public Action actionImprimerXls2() {
			return actionImprimerXls2;
		}

		public ActionListener getCheckSoldeListener() {
			return cbBoxSoldeListener;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	public final class ActionXls extends AbstractAction {

		public ActionXls() {
			super("Excel");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Export Excel");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls();
		}

	}

	public final class ActionXls2 extends AbstractAction {

		public ActionXls2() {
			super("Excel (*)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Export Excel tabulaire");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls2();
		}

	}

	private final class CheckBoxSoldeListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			myFilters.put(SOLDE_KEY, Boolean.valueOf(myPanel.getCheckBoxSolde().isSelected()));
		}

	}

}
