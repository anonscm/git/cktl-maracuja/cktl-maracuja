/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.ResteRecouvrerPanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ResteRecouvrerCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression de l'état des restes à recouvrer";
	private final Dimension WINDOW_SIZE = new Dimension(550, 360);

	private final String ACTION_ID = ZActionCtrl.IDU_IMPR009;
	private static final String JASPERFILENAME_RESTERECOUVRER = "resterecouvrer_sql.jasper";

	private ResteRecouvrerPanel myPanel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ResteRecouvrerCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			//if (gestions.count()== 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		myPanel = new ResteRecouvrerPanel(new ResteRecouvrerPanelListener());

	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}

			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerResteRecouvrer(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public String imprimerResteRecouvrer(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		String modeleJasper = JASPERFILENAME_RESTERECOUVRER;
		NSMutableDictionary params = new NSMutableDictionary(parametres);
		final String req = buildSql(ec, params, paramRequete);
		return imprimerReportByThread(ec, temporaryDir, params, modeleJasper, getFileNameWithExtension(ZFileUtil.removeExtension(modeleJasper), FORMATPDF), req, win, FORMATPDF, null);
	}

	private String buildSql(EOEditingContext ec, NSMutableDictionary parametres, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		Date dateFin = (Date) paramRequete.get("dateFin");

		if (dateFin == null) {
			dateFin = ZDateUtil.getDateOnly(ZDateUtil.nowAsDate());
		}

		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		final String condExercice = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "e.exe_ordre", new NSArray(exercice));
		String condGestion = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e.com_ordre", new NSArray(comptabilite));
		String condCompte = buildCriteresPlanco("ecd.pco_num", (String) paramRequete.get("planco"));

		String condReste = "";
		String condDebiteur = "";
		String condDebiteurOrv = "";
		String condSiret = "";

		if (paramRequete.get("ecdResteEmarger") != null) {
			Number r = (Number) paramRequete.get("ecdResteEmarger");
			condReste = " and ecd.ecd_Reste_Emarger >= " + ZConst.FORMAT_DECIMAL_COURT.format(r).replace(',', '.');
		}

		if (paramRequete.get("debiteur") != null) {
			String debiteur = (String) paramRequete.get("debiteur");
			debiteur = debiteur.toUpperCase();
			condDebiteur = " and (upper(f.Nom) like '%" + debiteur + "%' or upper(rec_debiteur) like '%" + debiteur + "%' or f.fou_code='" + debiteur + "') ";
			condDebiteurOrv = " and (upper(f.Nom) like '%" + debiteur + "%' or f.fou_code='" + debiteur + "') ";
		}

		if (paramRequete.get("siret") != null) {
			String siret = (String) paramRequete.get("siret");
			siret = siret.replace(" ", "");
			condSiret = " and f.siret='" + siret + "'";
		}

		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "t.", null);
		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");
		parametres.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_FIN");
		parametres.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		String req = "SELECT   'TITRE' AS type_piece, t.exe_ordre, ecd.pco_num, t.ges_code,  maracuja.api_planco.get_pco_libelle (ecd.pco_num,ecd.exe_ordre) AS pco_libelle,  "
				+ " t.tit_numero, TO_CHAR (b.bor_date_creation, 'DD/MM/YYYY') tit_date_emission, "
				+ "         r.rec_debiteur, t.tit_id, f.fou_code, f.nom adr_nom, f.prenom adr_prenom,         "
				+ " ra.code_postal adr_cp, ra.adr_adresse1, ra.adr_adresse2, ra.ville adr_ville, t.tit_etat,   "
				+ " sum(r.REC_MONT) tit_ht, sum(r.REC_MONTANT_DISQUETTE) tit_ttc,  sum(r.REC_MONTTVA) tit_tva, r.REC_LIBELLE tit_libelle,   "
				+ " t.tit_nb_piece, ecd.ecd_credit, ecd.ecd_debit, ecd.ecd_reste_emarger, f.siret           "
				+ " FROM MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e, MARACUJA.titre_detail_ecriture tde, MARACUJA.titre t, "
				+ " MARACUJA.v_fournis_light f, MARACUJA.recette r,  MARACUJA.bordereau b ,  maracuja.v_recette_adresse ra , grhum.pays p "
				+ " where "
				+ " e.ecr_ordre = ecd.ecr_ordre     AND ecd.ecd_ordre = tde.ecd_ordre     "
				+ "and tde.REC_ID = r.rec_id"
				+ "     AND r.tit_id = t.tit_id "
				+ "    and t.BOR_ID = b.bor_id "
				+ "    AND r.fou_ordre = f.fou_ordre(+)     "
				+ "and r.rec_ordre=ra.rpco_id and ra.c_pays=p.c_pays "
				+ " AND (   (ecd.pco_num LIKE '4%' AND ecd.pco_num NOT LIKE '445%')          OR ecd.pco_num LIKE '5%'         )"
				+ " and to_char(b.bor_date_creation,'YYYYMMDD') < '"
				+ dateFinStr
				+ "' "
				+ " and e.ecr_numero > 0    "
				+ " and ecd.ecd_reste_emarger>0 "
				+ " and substr(e.ecr_etat,1,1)='"
				+ EOEcriture.ecritureValide.substring(0, 1)
				+ "' "
				+ (condGestion.length() != 0 ? " and " + condGestion : "")
				+ condDebiteur
				+ condSiret
				+ " and "
				+ condExercice
				+ " "
				+ condReste
				+ (condCompte.length() > 0 ? " and " + condCompte : "")
				+ " and "
				+ condCompta
				+
				" group by t.exe_ordre, ecd.pco_num, t.ges_code, ecd.exe_ordre,         " +
				"t.tit_numero,b.bor_date_creation,         r.rec_debiteur, t.tit_id, " +
				"    f.fou_code,     f.nom,f.prenom,         ra.code_postal,         ra.adr_adresse1,         ra.adr_adresse2,         ra.ville, " +
				"t.tit_etat,   r.REC_LIBELLE,      bor_date_creation ,         t.tit_nb_piece,         ecd.ecd_credit, ecd.ecd_debit, ecd.ecd_reste_emarger, f.siret ";
		req += " UNION ALL ";
		req += "select 'ORV' as type_piece, t.exe_ordre, ecd.pco_num,t.ges_code, maracuja.api_planco.get_pco_libelle (ecd.pco_num,ecd.exe_ordre) AS pco_libelle," +
				"man_numero as tit_numero,TO_CHAR (x.tit_date_emission, 'DD/MM/YYYY') tit_date_emission,null as rec_debiteur, t.man_id, f.fou_code, "
				+ " f.nom adr_nom, f.prenom adr_prenom,         "
				+ " ra.code_postal adr_cp, ra.adr_adresse1, ra.adr_adresse2, ra.ville adr_ville, " +
				"t.man_etat,-t.man_ht as tit_ht, -t.man_ttc as tit_ttc, " +
				"-t.man_tva as tit_tva, null as tit_libelle, t.man_nb_piece as  tit_nb_piece,ecd.ecd_credit, ecd.ecd_debit, ecd.ecd_reste_emarger, f.siret"
				+ " FROM MARACUJA.ecriture_detail ecd,MARACUJA.ecriture e,MARACUJA.mandat_detail_ecriture tde,MARACUJA.mandat t,MARACUJA.bordereau b,MARACUJA.type_bordereau tb," +
				" MARACUJA.v_fournis_light f, MARACUJA.depense r,  maracuja.v_depense_adresse ra , grhum.pays p, "
				+ "(SELECT   m.man_id, MAX (bor_date_creation) tit_date_emission FROM MARACUJA.depense d, MARACUJA.mandat m, MARACUJA.bordereau b " +
				"where d.man_id=m.man_id and m.bor_id=b.bor_id GROUP BY m.man_id) x "
				+ " WHERE t.bor_id=b.bor_id " +
				"and b.tbo_ordre = tb.tbo_ordre " +
				"and tb.tbo_sous_type='REVERSEMENTS' " +
				"and e.ecr_ordre = ecd.ecr_ordre " +
				"AND tde.ecd_ordre = ecd.ecd_ordre " +
				"AND tde.man_id = t.man_id " +
				"AND x.man_id = t.man_id  " +
				"and r.fou_ordre=f.fou_ordre "
				+ "and r.dep_ordre=ra.dpco_id and ra.c_pays=p.c_pays "
				+ " and t.man_id = r.man_id "
				+ " and ((ecd.pco_num like '4%' and ecd.pco_num not like '445%') or ecd.pco_num like '5%') " + "and to_char(x.tit_date_emission,'YYYYMMDD') < '" + dateFinStr + "' " +
				"and e.ecr_numero > 0    " + "and ecd.ecd_reste_emarger>0 " + " and substr(e.ecr_etat,1,1)='"
				+ EOEcriture.ecritureValide.substring(0, 1) + "' " + (condGestion.length() != 0 ? " and " + condGestion : "")
				+ condDebiteurOrv + condSiret + " and " + condExercice + " " + condReste + (condCompte.length() > 0 ? " and " + condCompte : "") + " and " + condCompta;

		req += " order by pco_num, ges_code , exe_ordre, tit_numero";

		System.out.println("date fin " + parametres.valueForKey("JOUR_FIN"));
		return req;
	}

	protected final void initFilters() {
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class ResteRecouvrerPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements ResteRecouvrerPanel.IResteRecouvrerPanelListener {

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

}
