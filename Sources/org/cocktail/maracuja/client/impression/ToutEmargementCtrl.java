/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.ToutEmargementPanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ToutEmargementCtrl extends ZKarukeraImprCtrl {
	public static final String JXLSFILENAME_TOUT_EMARGEMENT = "toutemargement.xls";
	private static final String JASPERFILENAME_TOUTEMARGEMENT = "tout_emarg_sql.jasper";

	private final String WINDOW_TITLE = "Impression des émargements (tout émargement)";
	private final Dimension WINDOW_SIZE = new Dimension(550, 200);
	private ToutEmargementPanel myPanel;
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR005;
	private final ActionXls actionImprimerXls = new ActionXls();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ToutEmargementCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		myPanel = new ToutEmargementPanel(new ToutEmargementPanelListener());
	}

	private final String buildSql(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get("dateFin");

		// String dateDebutStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(dateDebut);
		String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		// String dateFinStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin),
		// 1, 0, 0, 0));

		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		//	EOGestion gestion = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_FILTER_KEY);
		//	EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);

		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		// EOPlanComptable planco = (EOPlanComptable)
		// paramRequete.get("planco");
		// if (planco==null) {
		// throw new DefaultClientException("Vous devez choisir un compte.");
		// }

		String condExercice = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "e2.exe_ordre", new NSArray(exercice));
		String condGestion = "";
		//String condsacd = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e2.com_ordre", new NSArray(comptabilite));
		// String condCompte = " ecd2.pco_num='"+ planco.pcoNum() +"' ";
		String condCompte = buildCriteresPlanco("ecd2.pco_num", (String) paramRequete.get("planco"));

		//		//NSMutableDictionary params = new NSMutableDictionary(parametres);
		//		if (gesCodeSACD != null) {
		//			parametres.takeValueForKey(gesCodeSACD.gesCode(), "SACD");
		//			condsacd = " ecd2.ges_code='" + gesCodeSACD.gesCode() + "'";
		//		}
		//		else {
		//			// Récupérer tous les SACDs
		//			NSArray sacds = EOsFinder.getSACDGestionForComptabilite(ec, comptabilite, exercice);
		//			// on les exclue de la requete
		//			for (int i = 0; i < sacds.count(); i++) {
		//				EOGestion element = (EOGestion) sacds.objectAtIndex(i);
		//				if (condsacd.length() > 0) {
		//					condsacd = condsacd + "and ";
		//				}
		//				condsacd = condsacd + " ecd2.ges_code<>'" + element.gesCode() + "' ";
		//			}
		//		}
		//
		//		
		//		if (condsacd.length() > 0) {
		//			condsacd = " and " + condsacd;
		//		}
		//
		//		if (gestion != null) {
		//			condGestion = condGestion + " and ecd2.ges_code = '" + gestion.gesCode() + "' ";
		//		}

		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecd2.", null);
		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		parametres.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		parametres.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		parametres.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOUR_DEBUT");
		parametres.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_FIN");
		parametres.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		final String condSqlDate = " and to_char(e2.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " + "and to_char(e2.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' ";

		String condsql = " and substr(e2.ecr_etat,1,1)='" + EOEcriture.ecritureValide.substring(0, 1) + "' " + " and e2.ecr_numero > 0 ";

		if (condCompte != null && condCompte.length() > 0) {
			condsql += "and " + condCompte;
		}
		condsql += "and " + condExercice + "and " + condCompta;
		condsql += (condGestion.length() != 0 ? " and " + condGestion : "");
		//condsql += condsacd;

		// avec visualisation des deux cotés

		String req = "SELECT   z.pco_num, z.ecr_numero_1, z.ecr_date_saisie, z.ecd_ordre_1, z.ges_code_1, " + "         y.cumul_debit, y.cumul_credit, z.ecd_libelle_1, z.ecd_montant_1, " + "         z.ecd_debit_1, z.ecd_credit_1, z.ecd_ordre_2, z.ecr_numero_2, "
				+ "         z.emd_montant, z.ema_numero, z.ecd_reste_emarger, " + "         k.cumul_reste_debit, k.cumul_reste_credit " +
				"    FROM (SELECT   ecd2.pco_num, ecd2.ges_code, " +
				"                   SUM (ecd2.ecd_debit) AS cumul_debit, "
				+ "                   SUM (ecd2.ecd_credit) AS cumul_credit " +
				"              FROM MARACUJA.ecriture_detail ecd2, MARACUJA.ecriture e2 " +
				"             WHERE ecd2.ecr_ordre = e2.ecr_ordre "
				+ condSqlDate
				+ condsql
				+ "          GROUP BY ecd2.pco_num, ecd2.ges_code) y, "
				+ "         (SELECT   ecd2.pco_num, ecd2.ges_code, "
				+ "                   SUM (ecd2.ecd_reste_emarger* SIGN (ecd2.ecd_debit) ) cumul_reste_debit, "
				+ "                   SUM (ecd2.ecd_reste_emarger* SIGN (ecd2.ecd_credit)) cumul_reste_credit "
				+ "              FROM MARACUJA.ecriture_detail ecd2, MARACUJA.ecriture e2 "
				+ "             WHERE ecd2.ecr_ordre = e2.ecr_ordre "
				+ condSqlDate
				+ condsql
				+ "          GROUP BY ecd2.pco_num, ecd2.ges_code) k, "
				+ "         (         "
				+ "         select z2.pco_num, z2.ecr_numero_source ecr_numero_1,z2.ecr_date_saisie, z2.ecd_ordre ecd_ordre_1, z2.ges_code ges_code_1, "
				+ "                              z2.ecd_libelle_1, "
				+ "                              z2.ecd_montant_1, z2.ecd_debit ecd_debit_1, "
				+ "                              z2.ecd_credit ecd_credit_1, ecd_ordre_2, "
				+ "                              z1.ecr_numero_2, z1.emd_montant, z1.ema_numero, z2.ecd_reste_emarger "
				+ "          from  "
				+ "         ( "
				+ "                         SELECT ecd2.pco_num, e2.ecr_numero ecr_numero_1, "
				+ "                              ecd2.ecd_ordre ecd_ordre_1, ecd2.ges_code ges_code_1, "
				+ "                              NVL (ecd2.ecd_libelle, e2.ecr_libelle) ecd_libelle_1, "
				+ "                              ecd2.ecd_montant ecd_montant_1, ecd2.ecd_debit ecd_debit_1, "
				+ "                              ecd2.ecd_credit ecd_credit_1, x.ecd_ordre ecd_ordre_2, "
				+ "                              x.ecr_numero ecr_numero_2, x.emd_montant, x.ema_numero, "
				+ "                              ecd2.ecd_reste_emarger "
				+ "                         FROM (SELECT emd1.emd_ordre, ecd1.ecd_ordre, e1.ecr_numero, "
				+ "                                      emd1.emd_montant, ema1.ema_numero "
				+ "                                 FROM MARACUJA.ecriture_detail ecd1, "
				+ "                                      MARACUJA.emargement_detail emd1, "
				+ "                                      MARACUJA.ecriture e1, "
				+ "                                      MARACUJA.emargement ema1 "
				+ "                                WHERE emd1.ecd_ordre_destination = ecd1.ecd_ordre "
				+ "                                  AND e1.ecr_ordre = ecd1.ecr_ordre "
				+ "                                  AND ema1.ema_ordre = emd1.ema_ordre "
				+ "                                  AND '"
				+ EOEmargement.emaEtatValide.substring(0, 1)
				+ "' = substr(ema1.ema_etat,1,1)) x, "
				+ "                              MARACUJA.ecriture e2, "
				+ "                              MARACUJA.ecriture_detail ecd2, "
				+ "                              MARACUJA.emargement_detail emd2 "
				+ "                        WHERE ecd2.ecr_ordre = e2.ecr_ordre "
				+ "                          AND ecd2.ecd_ordre = emd2.ecd_ordre_source "
				+ "                          AND emd2.emd_ordre = x.emd_ordre "
				+ condsql
				+ "                       UNION "
				+ "                       SELECT ecd2.pco_num, e2.ecr_numero ecr_numero_1, "
				+ "                              ecd2.ecd_ordre ecd_ordre_1, ecd2.ges_code ges_code_1, "
				+ "                              NVL (ecd2.ecd_libelle, e2.ecr_libelle) ecd_libelle_1, "
				+ "                              ecd2.ecd_montant ecd_montant_1, ecd2.ecd_debit ecd_debit_1, "
				+ "                              ecd2.ecd_credit ecd_credit_1, x.ecd_ordre ecd_ordre_2, "
				+ "                              x.ecr_numero ecr_numero_2, x.emd_montant, x.ema_numero, "
				+ "                              ecd2.ecd_reste_emarger "
				+ "                         FROM (SELECT emd1.emd_ordre, ecd1.ecd_ordre, e1.ecr_numero, "
				+ "                                      emd1.emd_montant, ema1.ema_numero "
				+ "                                 FROM MARACUJA.ecriture_detail ecd1, "
				+ "                                      MARACUJA.emargement_detail emd1, "
				+ "                                      MARACUJA.ecriture e1, "
				+ "                                      MARACUJA.emargement ema1 "
				+ "                                WHERE emd1.ecd_ordre_source = ecd1.ecd_ordre "
				+ "                                  AND e1.ecr_ordre = ecd1.ecr_ordre "
				+ "                                  AND ema1.ema_ordre = emd1.ema_ordre "
				+ "                                  AND '"
				+ EOEmargement.emaEtatValide.substring(0, 1)
				+ "' = substr(ema1.ema_etat,1,1)) x, "
				+ "                              MARACUJA.ecriture e2, "
				+ "                              MARACUJA.ecriture_detail ecd2, "
				+ "                              MARACUJA.emargement_detail emd2 "
				+ "                        WHERE ecd2.ecr_ordre = e2.ecr_ordre "
				+ "                          AND ecd2.ecd_ordre = emd2.ecd_ordre_destination "
				+ "                          AND emd2.emd_ordre = x.emd_ordre "
				+ condsql
				+ "          ) z1, "
				+ "       ( "
				+ "               SELECT ecd2.pco_num, e2.ecr_numero ecr_numero_source, e2.ecr_date_saisie,"
				+ "                      ecd2.ecd_ordre, ecd2.ges_code, "
				+ "                      NVL (ecd2.ecd_libelle, e2.ecr_libelle) ecd_libelle_1, "
				+ "                      ecd2.ecd_montant ecd_montant_1, ecd2.ecd_debit, "
				+ "                      ecd2.ecd_credit,ecd2.ecd_reste_emarger "
				+ "                 FROM MARACUJA.ecriture e2, MARACUJA.ecriture_detail ecd2 "
				+ "                WHERE ecd2.ecr_ordre = e2.ecr_ordre             "
				+ condSqlDate
				+ condsql
				+ "          ) z2          "
				+ "          where  "
				+ "          z2.ecd_ordre = z1.ecd_ordre_1 (+) "
				+ "          ) z "
				+ "   WHERE z.pco_num = y.pco_num "
				+ "     AND z.ges_code_1 = y.ges_code "
				+ "     AND z.pco_num = k.pco_num "
				+ "     AND z.ges_code_1 = k.ges_code " +
				"ORDER BY pco_num, ges_code_1, ecr_numero_1, ecd_ordre_1, ecr_numero_2";

		return req;
		//return imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_TOUTEMARGEMENT, getFileNameWithExtension("toutemargement", format), req, win, format, null);
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());

			String req = buildSql(getEditingContext(), myApp.temporaryDir, dico, myFilters);
			//String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_TOUT_EMARGEMENT, "toutemargement.xls", req, getMyDialog(), null);
			String filePath = imprimerReportByThread(getEditingContext(), myApp.temporaryDir, dico, JASPERFILENAME_TOUTEMARGEMENT, getFileNameWithExtension("toutemargement", FORMATPDF), req, getMyDialog(), FORMATPDF, null);

			//String filePath = ReportFactoryClient.imprimerToutEmargement(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), FORMATPDF);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerXls() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());

			String req = buildSql(getEditingContext(), myApp.temporaryDir, dico, myFilters);
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_TOUT_EMARGEMENT, "toutemargement.xls", req, getMyDialog(), null);
			//String filePath = ReportFactoryClient.imprimerToutEmargement(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), ReportFactoryClient.FORMATXLS);
			myApp.saveAsAndOpenFile(getMyDialog(), filePath);
			//	        if (filePath!=null) {
			//	            myApp.openPdfFile(filePath);
			//	        }
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.DATE_DEBUT_FILTER_KEY, ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();
			//myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			//myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, ((EOGestion) gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final class ActionXls extends AbstractAction {

		public ActionXls() {
			super("Excel");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls();
		}

	}

	private final class ToutEmargementPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements ToutEmargementPanel.IToutEmargementPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#getPcoMap()
		 */
		public Map getPcoMap() {
			return pcoMap;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.ToutEmargementPanel.IToutEmargementPanelListener#actionImprimerXls()
		 */
		public Action actionImprimerXls() {
			return actionImprimerXls;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

}
