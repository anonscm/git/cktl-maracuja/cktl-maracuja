/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.JournalDivPanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class JournalDivCtrl extends ZKarukeraImprCtrl {
	private static final String JASPERFILENAME_JOURNAL_DIV = "journaldiv_sql.jasper";
	private static final String JXLSFILENAME_JOURNAL_DIV = "journaldiv.xls";

	private final String WINDOW_TITLE = "Impression du journal divisionnaire";
	private final Dimension WINDOW_SIZE = new Dimension(609, 190);

	private final String ACTION_ID = ZActionCtrl.IDU_IMPR012;
	private JournalDivPanel myPanel;

	private ActionImprimerXls actionImprimerXls = new ActionImprimerXls();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public JournalDivCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			//if (gestions.count() + gestionsSacd.count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}
		myPanel = new JournalDivPanel(new JournalDivPanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}

			//Recupererer le compte
			final String pco = (String) myFilters.get("pcoNum");
			final NSArray pcos = getPcoValides();
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum=%@", new NSArray(pco));
			final NSArray res = EOQualifier.filteredArrayWithQualifier(pcos, qual);
			if (res.count() == 0) {
				throw new DefaultClientException(pco + " n'est pas un numéro de compte valide. ");
			}
			myFilters.put("JOURNAL_LIB", "Compte " + pco + " " + ((EOPlanComptableExer) res.objectAtIndex(0)).pcoLibelle());

			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey("Compte " + pco + " " + ((EOPlanComptableExer) res.objectAtIndex(0)).pcoLibelle(), "JOURNAL_LIB");
			//final String filePath = ReportFactoryClient.imprimerJournalDiv(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), FORMATPDF);
			String req = buildSql(getEditingContext(), dico, myFilters);

			final String filePath = imprimerReportByThread(getEditingContext(), myApp.temporaryDir, dico, JASPERFILENAME_JOURNAL_DIV, getFileNameWithExtension("journalDiv", FORMATPDF), req, getMyDialog(), FORMATPDF, null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void imprimerXls() {
		try {
			myFilters.put("planco", null);
			if (myFilters.get("pcoNum") == null) {
				throw new DefaultClientException("Vous devez spécifier un compte.");
			}
			ZLogger.debug(myFilters);
			//Recupererer le compte
			final String pco = (String) myFilters.get("pcoNum");
			final NSArray pcos = getPcoValides();
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum=%@", new NSArray(pco));
			final NSArray res = EOQualifier.filteredArrayWithQualifier(pcos, qual);
			if (res.count() == 0) {
				throw new DefaultClientException(pco + " n'est pas un numéro de compte valide. ");
			}

			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey("Compte " + pco + " " + ((EOPlanComptableExer) res.objectAtIndex(0)).pcoLibelle(), "JOURNAL_LIB");

			String req = buildSql(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_JOURNAL_DIV, "journaldiv.xls", req, getMyDialog(), null);

			//final String filePath = ReportFactoryClient.imprimerJournalDiv(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), ReportFactoryClient.FORMATXLS);
			myApp.saveAsAndOpenFile(getMyDialog(), filePath);
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	//	private final void imprimerXls() {
	//		try {
	//			myFilters.put("planco", null);
	//			if (myFilters.get("pcoNum") == null) {
	//				throw new DefaultClientException("Vous devez spécifier un compte.");
	//			}
	//			ZLogger.debug(myFilters);
	//			//Recupererer le compte
	//			final String pco = (String) myFilters.get("pcoNum");
	//			final NSArray pcos = getPcoValides();
	//			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum=%@", new NSArray(pco));
	//			final NSArray res = EOQualifier.filteredArrayWithQualifier(pcos, qual);
	//			if (res.count() == 0) {
	//				throw new DefaultClientException(pco + " n'est pas un numéro de compte valide. ");
	//			}
	//
	//			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
	//			dico.takeValueForKey("Compte " + pco + " " + ((EOPlanComptableExer) res.objectAtIndex(0)).pcoLibelle(), "JOURNAL_LIB");
	//			
	//			final String filePath = ReportFactoryClient.imprimerJournalDiv(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), ReportFactoryClient.FORMATXLS);
	//			myApp.saveAsAndOpenFile(getMyDialog(), filePath);
	//		} catch (Exception e1) {
	//			showErrorDialog(e1);
	//		}
	//	}

	private String buildSql(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get("dateFin");

		String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		// String dateDebutStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(dateDebut);
		// String dateFinStr = new
		// SimpleDateFormat("dd/MM/yyyy").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin),
		// 1, 0, 0, 0));
		// EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_FILTER_KEY);
		//EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		//	String condsacd = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		//NSMutableDictionary params = new NSMutableDictionary(parametres);
		//		if (gesCodeSACD != null) {
		//			params.takeValueForKey(gesCodeSACD.gesCode(), "SACD");
		//			params.takeValueForKey(gesCodeSACD.gesCode(), "JOURNAL_SACD");
		//			condsacd = " ecriture_detail.ges_code = '" + gesCodeSACD.gesCode() + "'";
		//		}
		//		else {
		//			// Récupérer tous les SACDs
		//			NSArray sacds = EOsFinder.getSACDGestionForComptabilite(ec, comptabilite, exercice);
		//			// on les exclue de la requete
		//			for (int i = 0; i < sacds.count(); i++) {
		//				EOGestion element = (EOGestion) sacds.objectAtIndex(i);
		//				if (condsacd.length() > 0) {
		//					condsacd = condsacd + " and ";
		//				}
		//				condsacd = condsacd + " ecriture_detail.ges_code <> '" + element.gesCode() + "' ";
		//			}
		//		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		final String condCompte = buildCriteresPlanco("ecriture_detail.pco_num", (String) paramRequete.get(EOPlanComptable.PCO_NUM_KEY));

		// On fait les requetes pour recupérer les cumuls precedents
		String reqCumuls = "select nvl(sum(nvl(ecd_debit,0)),0) as TOT_DEBIT, nvl(sum(nvl(ecd_credit,0)),0) as TOT_CREDIT " +
				"from " +
				"(select com_libelle, ecr_numero, ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, " + "x.exe_ordre, ecd.pco_num, ecd.ges_code "
				+ "from MARACUJA.ecriture_detail ecd, " + "(SELECT  distinct ecriture.ecr_ordre, com_libelle, ecr_numero, ecr_date, ecr_libelle, ecriture.exe_ordre   " +
				"FROM MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail " +
				" WHERE ecriture.ecr_ordre = ecriture_detail.ecr_ordre     "
				+ " and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" +
				dateDebutStr + "' " + " and ecriture.ecr_numero > 0    " +
				" and ecriture.ecr_etat='" + EOEcriture.ecritureValide + "'" +
				" and " + condCompte + " and " + cond + " and " + condCompta
				+ (condGestion.length() != 0 ? " and " + condGestion : "") + ") x " +
				"where ecd.ecr_ordre=x.ecr_ordre " + ") ";
		ZLogger.debug(reqCumuls);

		NSArray res = ServerProxy.clientSideRequestSqlQuery(ec, reqCumuls);
		if (res == null || res.count() == 0) {
			throw new DefaultClientException("Impossible de calculer les cumuls");
		}
		NSDictionary dicCumul = (NSDictionary) res.objectAtIndex(0);

		if (NSKeyValueCoding.NullValue.equals(dicCumul.valueForKey("TOT_DEBIT")) || NSKeyValueCoding.NullValue.equals(dicCumul.valueForKey("TOT_CREDIT"))) {
			throw new DefaultClientException("Aucune écriture pour la période sélectionnée.");
		}

		BigDecimal cumulDebit = new BigDecimal(((Number) dicCumul.valueForKey("TOT_DEBIT")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal cumulCredit = new BigDecimal(((Number) dicCumul.valueForKey("TOT_CREDIT")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);

		params.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		params.takeValueForKey(cumulDebit, "PREC_DEBIT");
		params.takeValueForKey(cumulCredit, "PREC_CREDIT");

		if (dateDebut != null && dateFin != null) {
			if (dateDebut.equals(dateFin)) {
				params.takeValueForKey("Journée du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOURNEES");
			}
			else {
				params.takeValueForKey("Journées du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut) + " au " + new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOURNEES");
			}
		}

		// ZLogger.debug(params);

		String req = "select com_libelle, ecr_numero, ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, " + "x.exe_ordre, ecd.pco_num, maracuja.api_planco.get_pco_libelle(ecd.pco_num, ecd.exe_ordre) as pco_libelle, ecd.ges_code " +
				"from MARACUJA.ecriture_detail ecd, "
				+ "(SELECT  distinct ecriture.ecr_ordre, com_libelle, ecr_numero, ecr_date, ecr_libelle, ecriture.exe_ordre   " +
				"FROM MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail " +
				"WHERE ecriture.ecr_ordre = ecriture_detail.ecr_ordre     " + " and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '"
				+ dateDebutStr + "' " + " and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " + "and ecriture.ecr_numero > 0    " + " and ecriture.ecr_etat='" + EOEcriture.ecritureValide + "'" + " and " + condCompte + " and " + cond + " and " + condCompta
				+ (condGestion.length() != 0 ? " and " + condGestion : "");
		//		if (condsacd.length() != 0) {
		//			req += " and " + condsacd;
		//		}

		req += ") x " + "where ecd.ecr_ordre=x.ecr_ordre";
		req += " order by ecr_numero, ecd_index";

		return req;
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put("dateDebut", ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));

		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			//	myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			//	myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, ((EOGestion) gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final class ActionImprimerXls extends AbstractAction {

		public ActionImprimerXls() {
			super("Excel");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Impresssion au format Excel (en test)");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls();
		}

	}

	private final class JournalDivPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements JournalDivPanel.IJournalDivPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#actionImprimerXls()
		 */
		public Action actionImprimerXls() {
			return actionImprimerXls;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

	}

	//
	//
	//    private class PcoLookupListener implements IZLookupButtonListener {
	//
	//        public void setNewValue(Object res) {
	//            final EOPlanComptable tmp = (EOPlanComptable) pcoMap.get(res);
	//            myFilters.put("pcoNum", tmp.pcoNum());
	//            try {
	//                myPanel.updateData();
	//            } catch (Exception e) {
	//                e.printStackTrace();
	//            }
	//        }
	//
	//        public void onTextHasChanged() {
	//            
	//        }
	//
	//        public void onDbClick() {
	//            
	//        }
	//        
	//    }
	//
	//
	//    private class PcoLookupModel implements IZLookupButtonModel {
	//        private final Collection pcosTmp;
	//        
	//        public PcoLookupModel() {
	//            pcosTmp = pcoMap.keySet(); 
	//        }
	//
	//        public Collection getDatas() {
	//            System.out.println("PcoLookupModel.getDatas()");
	//            ZLogger.debug(pcosTmp);
	//            return pcosTmp;
	//        }
	//
	//        public String getActionDescription() {
	//            return "Sélectionner un compte";
	//        }
	//
	//        public ImageIcon getIcon() {
	//            return ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16);
	//        }
	//
	//        public Window getMyWindow() {
	//            return getMyDialog();
	//        }
	//
	//        public String getTitle() {
	//            return "Sélectionnez un compte";
	//        }
	//
	//        public Dimension getPreferredSize() {
	//            return PCOWINDOW_SIZE;
	//        }
	//
	//    }

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

}
