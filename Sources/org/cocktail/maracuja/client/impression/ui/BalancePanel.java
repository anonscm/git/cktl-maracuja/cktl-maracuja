/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.impression.BalanceCtrl;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BalancePanel extends ZKarukeraImprPanel {

	private IBalancePanelListener myListener;
	private JComboBox myClassesCompte;
	private JComboBox myTypeBalance;
	private JComboBox myTypeOperation;

	/**
	 * @param editingContext
	 */
	public BalancePanel(IBalancePanelListener listener) {
		super(listener);
		myListener = listener;
		//		comboNiveau = new JComboBox(myListener.getNiveauxModel());
		myClassesCompte = new JComboBox(myListener.classeCompteModel());
		myClassesCompte.addActionListener(myListener.classeCompteListener());

		myTypeBalance = new JComboBox(myListener.typeBalanceModel());
		myTypeBalance.addActionListener(myListener.typeBalanceListener());
		myTypeBalance.addActionListener(new MyTypeBalanceListener());

		myTypeOperation = new JComboBox(myListener.typeOperationModel());
		myTypeOperation.addActionListener(myListener.typeOperationListener());
		//myTypeOperation.addActionListener(new MyTypeBalanceListener());
	}

	protected void buildLines() {
		myDateSaisieMaxLabeled.getMyLabel().setText("Date de la balance ");
		getLines().add(buildLine(new ZLabeledComponent("Type de balance ", myTypeBalance, ZLabeledComponent.LABELONLEFT, 125)));
		getLines().add(buildLine(new ZLabeledComponent("Type d'opérations ", myTypeOperation, ZLabeledComponent.LABELONLEFT, 125)));
		super.buildLines();
		getLines().add(buildLine(new ZLabeledComponent("Classe ", myClassesCompte, ZLabeledComponent.LABELONLEFT, 125)));

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		super.updateData();
		dateSaisieMaxField.updateData();
		if (myListener.getFilters().get("typeBalance") != null) {
			myTypeBalance.setSelectedItem(myListener.getFilters().get("typeBalance"));
		}
		if (myListener.getFilters().get("typeOperation") != null) {
			myTypeOperation.setSelectedItem(myListener.getFilters().get("typeOperation"));
		}
		//agregatCtrUi.iniGui();
	}

	public interface IBalancePanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {
		public DefaultComboBoxModel typeBalanceModel();

		public ActionListener typeOperationListener();

		public DefaultComboBoxModel typeOperationModel();

		public ActionListener typeBalanceListener();

		public DefaultComboBoxModel classeCompteModel();

		public ActionListener classeCompteListener();

		public Action actionImprimerXls2();

	}

	private final class MyTypeBalanceListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			refreshBoutons();

		}

	}

	/**
	 * descative les options inutiles pour le cadre 1 du compte fi.
	 */
	public final void desactiverCpteFi() {
		myTypeBalance.setEnabled(false);
		myTypeOperation.setEnabled(false);
		//myCodeGestionLabeled.setVisible(false);
		myClassesCompte.setEnabled(false);
	}

	private void refreshBoutons() {
		//myCodeGestion.setEnabled(false);
		myClassesCompte.setEnabled(false);
		myTypeOperation.setEnabled(false);

		if (BalanceCtrl.BALANCE_GEN_CS.equals(myTypeBalance.getSelectedItem()) || BalanceCtrl.BALANCE_RECAP_GRAND_LIVRE.equals(myTypeBalance.getSelectedItem())) {
			//myCodeGestion.setSelectedIndex(0);
			//myCodeGestion.setEnabled(false);
			myClassesCompte.setEnabled(true);
		}
		else if (BalanceCtrl.BALANCE_AUXILIAIRE.equals(myTypeBalance.getSelectedItem())) {
			//myCodeGestion.setSelectedIndex(0);
			//myCodeGestion.setEnabled(false);
			myClassesCompte.setEnabled(false);
		}
		else if (BalanceCtrl.BALANCE_OPE.equals(myTypeBalance.getSelectedItem())) {
			myTypeOperation.setEnabled(true);
			//myCodeGestion.setEnabled(true);
			myClassesCompte.setEnabled(true);
		}
		else if (BalanceCtrl.BALANCE_VALEUR_INACTIVE.equals(myTypeBalance.getSelectedItem())) {
			myTypeOperation.setEnabled(false);
			//myCodeGestion.setEnabled(false);
			myClassesCompte.setEnabled(false);
		}
		else {
			//myCodeGestion.setEnabled(true);
			myClassesCompte.setEnabled(true);
		}

	}

	public final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionImprimer());
		list.add(myListener.actionImprimerXls2());
		list.add(myListener.actionClose());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}
}
