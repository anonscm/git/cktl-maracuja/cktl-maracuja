/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.AgregatsCtrl;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;

import com.webobjects.foundation.NSArray;

public class AgregatsCtrlUi {
	private final AgregatsCtrl ctrl;
	private Object myListener;
	private JComboBox comboAgregats;
	private JComboBox comboGescode;
	private ZLabeledComponent myAgregatsLabeled;
	private ZLabeledComponent myCodeGestionLabeled;
	private HashMap filters;
	private ArrayList lines;
	private JPanel agregatLine;

	public AgregatsCtrlUi(AgregatsCtrl ctrl, HashMap filters) {
		this.ctrl = ctrl;
		this.filters = filters;
		iniGui();
	}

	public void iniGui() {
		comboAgregats = new JComboBox(ctrl.getAgregatsMap().keySet().toArray());
		comboAgregats.addActionListener(new AgregatsListener());

		comboGescode = new JComboBox();
		comboGescode.addActionListener(new GesCodesListener());

		myAgregatsLabeled = new ZLabeledComponent("Niveau ", comboAgregats, ZLabeledComponent.LABELONLEFT, 125);
		myCodeGestionLabeled = new ZLabeledComponent("Code gestion ", comboGescode, ZLabeledComponent.LABELONLEFT, 125);

		agregatLine = ZKarukeraPanel.buildLine(new Component[] {
				myAgregatsLabeled, Box.createHorizontalStrut(15), ZTooltip.getTooltip_AGREGAT()
		});

		if (comboAgregats.getItemCount() > 0) {
			comboAgregats.setSelectedIndex(0);
		}
		if (comboGescode.getItemCount() > 0) {
			comboGescode.setSelectedIndex(0);
		}
	}

	public final class AgregatsListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String selected = (String) comboAgregats.getSelectedItem();
			filters.put(AgregatsCtrl.AGREGAT_KEY, selected);
			filters.put(AgregatsCtrl.GES_CODE_KEY, null);
			comboGescode.removeAllItems();
			if (AgregatsCtrl.NIVEAU3_UB.equals(selected)) {
				comboGescode.setEnabled(true);
				NSArray gescodes = ctrl.getAgregatsMap().get(selected);
				for (int i = 0; i < gescodes.count(); i++) {
					comboGescode.addItem(gescodes.objectAtIndex(i));
				}
			}
			else {
				comboGescode.setEnabled(false);
			}
		}

	}

	public final class GesCodesListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			String selected = (String) comboGescode.getSelectedItem();
			filters.put(AgregatsCtrl.GES_CODE_KEY, selected);
		}

	}

	public JComboBox getComboAgregats() {
		return comboAgregats;
	}

	public JComboBox getComboGescode() {
		return comboGescode;
	}

	public JPanel getMyAgregatsLabeled() {
		return agregatLine;
	}

	public ZLabeledComponent getMyCodeGestionLabeled() {
		return myCodeGestionLabeled;
	}

}
