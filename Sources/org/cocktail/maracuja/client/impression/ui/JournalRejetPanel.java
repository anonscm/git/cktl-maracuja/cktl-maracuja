/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.impression.ZKarukeraImprCtrl;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class JournalRejetPanel extends ZKarukeraImprPanel {

	private IJournalRejetPanelListener myListener;
	private final JComboBox comboModele;

	//private final JComboBox comboTri;

	/**
	 * @param editingContext
	 */
	public JournalRejetPanel(IJournalRejetPanelListener listener) {
		super(listener);
		myListener = listener;

		comboModele = new JComboBox(myListener.getModelesModel());
		//comboTri = new JComboBox(listener.comboModeleTri());
		comboModele.addActionListener(new ModeleListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		super.initGUI();
	}

	public final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionImprimer());
		//list.add(myListener.actionImprimerXls());
		list.add(myListener.actionImprimerXls2());
		list.add(myListener.actionClose());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}

	protected void buildLines() {
		getLines().add(buildLine(new ZLabeledComponent("Modèle d'impression ", comboModele, ZLabeledComponent.LABELONLEFT, -1)));
		super.buildLines();
		//	getLines().add(buildLine(new ZLabeledComponent("Tri ", comboTri, ZLabeledComponent.LABELONLEFT, -1)));
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		super.updateData();
		dateSaisieMaxField.updateData();
	}

	public interface IJournalRejetPanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {
		//public Action actionImprimer();
		public Action actionImprimerXls();

		public Action actionImprimerXls2();

		public ComboBoxModel getModelesModel();

		//public ComboBoxModel comboModeleTri();

	}

	private final class ModeleListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.getFilters().put(ZKarukeraImprCtrl.MODELE_FILTER_KEY, comboModele.getSelectedItem());
		}

	}

}
