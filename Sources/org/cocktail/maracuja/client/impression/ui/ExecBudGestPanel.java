/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

import org.cocktail.maracuja.client.impression.ZKarukeraImprCtrl;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ExecBudGestPanel extends ZKarukeraImprPanel {
	private IExecBudGestPanelListener myListener;
	private JComboBox myCodeGestionSacd;
	private JComboBox myCodeGestion;
	private final JComboBox comboModele;
	private final JComboBox comboModeleConso;

	//private final JComboBox comboNiveau;

	/**
	 * @param editingContext
	 */
	public ExecBudGestPanel(IExecBudGestPanelListener listener) {
		super(listener);
		myListener = listener;
		comboModele = new JComboBox(myListener.getModelesModel());
		comboModeleConso = new JComboBox(myListener.getModelesConsoModel());
		//comboNiveau = new JComboBox(myListener.getNiveauxModel());

		comboModele.addActionListener(new ModeleListener());
		comboModeleConso.addActionListener(new ModeleConsoListener());

	}

	public void initGUI() {
		super.initGUI();
		//		setLayout(new BorderLayout());
		//		setBorder(ZKarukeraPanel.createMargin());
		//		add(buildFilters(), BorderLayout.CENTER);
		//		add(buildRightPanel(), BorderLayout.EAST);
	}

	//
	//	private final JPanel buildRightPanel() {
	//		JPanel tmp = new JPanel(new BorderLayout());
	//		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
	//		ArrayList list = new ArrayList();
	//		list.add(myListener.actionImprimer());
	//		list.add(myListener.actionClose());
	//
	//		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
	//		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	//
	//		return tmp;
	//	}

	protected void buildLines() {
		//myDateSaisieMaxLabeled.getMyLabel().setText("Date de la balance ");
		getLines().add(buildLine(new ZLabeledComponent("Modèle d'impression ", comboModele, ZLabeledComponent.LABELONLEFT, -1)));
		getLines().add(buildLine(new ZLabeledComponent("Consolidation ", comboModeleConso, ZLabeledComponent.LABELONLEFT, -1)));
		getLines().add(buildLine(agregatCtrUi.getMyAgregatsLabeled()));
		getLines().add(buildLine(agregatCtrUi.getMyCodeGestionLabeled()));

	}

	//	
	//	private final JPanel buildFilters() {
	//		//comboNiveau.addActionListener(new NiveauxListener());
	//
	//
	////
	////		myCodeGestionSacd = new JComboBox(myListener.getCodeGestionSacdModel());
	////		myCodeGestionSacd.addActionListener(new CodeGestionSacdListener());
	////
	////		myCodeGestion = new JComboBox(myListener.getCodeGestionModel());
	////		myCodeGestion.addActionListener(new CodeGestionListener());
	//
	//		if (comboModeleConso.getItemCount() == 1) {
	//			myCodeGestion.setEnabled(false);
	//		}
	//
	//		JPanel p = new JPanel(new BorderLayout());
	//		Component[] comps = new Component[5];
	//
	//		comps[0] = buildLine(new ZLabeledComponent("Modèle d'impression ", comboModele, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[1] = buildLine(new ZLabeledComponent("Consolidation ", comboModeleConso, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[2] = buildLine(new ZLabeledComponent("Niveau ", comboNiveau, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[3] = buildLine(new ZLabeledComponent("Comptabilité ", myCodeGestionSacd, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[4] = buildLine(new ZLabeledComponent("Code gestion ", myCodeGestion, ZLabeledComponent.LABELONLEFT, -1));
	//
	//		p.add(ZKarukeraPanel.buildVerticalPanelOfComponent(comps), BorderLayout.WEST);
	//		p.add(new JPanel(new BorderLayout()));
	//		return p;
	//
	//	}

	//	public void updateData() throws Exception {
	//		comboNiveau.setSelectedItem(myListener.getFilters().get(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY));
	//		myCodeGestion.setEnabled(false);
	//	}

	public interface IExecBudGestPanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {
		//	public Action actionImprimer();

		public ComboBoxModel getModelesConsoModel();

		public ComboBoxModel getModelesModel();

		public IZLookupButtonListener getLookupButtonCompteListener();

		public IZLookupButtonModel getLookupButtonCompteModel();

		public Map getPcoMap();

		//		public Action actionClose();
		//
		//		public HashMap getFilters();
		//
		//		public ZEOComboBoxModel getCodeGestionSacdModel();
		//
		//		public ZEOComboBoxModel getCodeGestionModel();
		//
		//		public void setGestion(EOGestion gestion);
		//
		//		public void setGestionSacd(EOGestion gestion);
		//
		//		public EOGestion getGestionSacd();
		//
		//		public EOGestion getGestion();
		//
		//		public ComboBoxModel getNiveauxModel();

	}

	private final class ModeleListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.getFilters().put(ZKarukeraImprCtrl.MODELE_FILTER_KEY, comboModele.getSelectedItem());
		}
	}

	private final class ModeleConsoListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.getFilters().put("modeleconso", comboModeleConso.getSelectedItem());
		}
	}

	//    private final class CodeGestionListener implements ActionListener {
	//
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            myListener.setGestion( (EOGestion) myListener.getCodeGestionModel().getSelectedEObject() )  ;
	//        }
	//
	//    }
	//
	//    private final class CodeGestionSacdListener implements ActionListener {
	//
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            myListener.setGestionSacd( (EOGestion) myListener.getCodeGestionSacdModel().getSelectedEObject() )  ;
	//            if (myListener.getGestionSacd()!=null) {
	//                myListener.setGestion(null);
	//                myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//                myCodeGestion.setEnabled(false);
	//            }
	//            else {
	//                myCodeGestion.setEnabled(true);
	//            }
	//        }
	//
	//    }
	//
	//	private final class NiveauxListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.getFilters().put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, comboNiveau.getSelectedItem());
	//
	//			if (DevDepRecCtrl.NIVEAU2_ETAB_SACD.equals(comboNiveau.getSelectedItem())) {
	//				myListener.setGestion(null);
	//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//				myCodeGestion.setEnabled(false);
	//				myCodeGestionSacd.setEnabled(true);
	//			}
	//			else if (DevDepRecCtrl.NIVEAU1_AGREGE.equals(comboNiveau.getSelectedItem())) {
	//				myListener.setGestionSacd(null);
	//				myListener.getCodeGestionSacdModel().setSelectedEObject(myListener.getGestionSacd());
	//				myListener.setGestion(null);
	//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//				myCodeGestion.setEnabled(false);
	//				myCodeGestionSacd.setEnabled(false);
	//
	//			}
	//			else {
	//				myListener.setGestionSacd(null);
	//				myListener.getCodeGestionSacdModel().setSelectedEObject(myListener.getGestionSacd());
	//				myCodeGestionSacd.setEnabled(false);
	//				myCodeGestion.setEnabled(true);
	//			}
	//
	//		}
	//
	//	}

	//	//
	//	private final class CodeGestionSacdListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestionSacd((EOGestion) myListener.getCodeGestionSacdModel().getSelectedEObject());
	//			myListener.setGestion(null);
	//			myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//
	//			//			if (myListener.getGestionSacd() != null) {
	//			//				myListener.setGestion(null);
	//			//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//			//				myCodeGestion.setEnabled(false);
	//			//			}
	//			//			else {
	//			//				myCodeGestion.setEnabled(true);
	//			//			}
	//		}
	//
	//	}
	//
	//	private final class CodeGestionListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestion((EOGestion) myListener.getCodeGestionModel().getSelectedEObject());
	//		}
	//
	//	}

}
