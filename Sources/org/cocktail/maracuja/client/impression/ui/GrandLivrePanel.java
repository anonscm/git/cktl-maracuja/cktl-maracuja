/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class GrandLivrePanel extends ZKarukeraImprPanel {
	//private final Color BORDURE_COLOR = getBackground().brighter();
	private IGrandLivrePanelListener myListener;
	//private ZFormPanel datesPanel;
	//    private JComboBox myCodeGestionSacd;
	//    private JComboBox myCodeGestion;
	private ZFormPanel pcoNum;
	private final ZLookupButton pcoSelectButton;
	private JComboBox myComboModele;

	//private AgregatsCtrlUi agregatCtrUi;

	/**
	 * @param editingContext
	 */
	public GrandLivrePanel(IGrandLivrePanelListener listener) {
		super(listener);
		myListener = listener;
		myComboModele = new JComboBox(listener.comboModeleModel());
		pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());
		//agregatCtrUi = new AgregatsCtrlUi(myListener.getAgregatCtrl(), myListener.getFilters());
	}

	protected void buildLines() {

		pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);

		getLines().add(buildLine(agregatCtrUi.getMyAgregatsLabeled()));
		getLines().add(buildLine(agregatCtrUi.getMyCodeGestionLabeled()));
		getLines().add(buildLine(new Component[] {
				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
		}));
		getLines().add(buildLine(buildDateFields()));
		getLines().add(buildLine(new Component[] {
				ZFormPanel.buildLabelField("Tri ", myComboModele)
		}));
	}

	public void initGUI() {
		super.initGUI();
		pcoSelectButton.initGUI();

	}

	//
	//	/**
	//	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	//	 */
	//	public void initGUI() {
	//		setLayout(new BorderLayout());
	//		pcoSelectButton.initGUI();
	//		setBorder(ZKarukeraPanel.createMargin());
	//		add(buildFilters(), BorderLayout.CENTER);
	//		add(buildRightPanel(), BorderLayout.EAST);
	//		//        add(buildBottomPanel(), BorderLayout.SOUTH);
	//	}

	//	private final JPanel buildRightPanel() {
	//		JPanel tmp = new JPanel(new BorderLayout());
	//		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
	//		ArrayList list = new ArrayList();
	//		list.add(myListener.actionImprimer());
	//		list.add(myListener.actionImprimerXls());
	//		list.add(myListener.actionClose());
	//
	//		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
	//		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	//
	//		return tmp;
	//	}

	public final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionImprimer());
		list.add(myListener.actionImprimerXls());
		list.add(myListener.actionClose());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}

	//
	//	private final JPanel buildFilters() {
	//		myCodeGestionSacd = new JComboBox(myListener.getCodeGestionSacdModel());
	//		myCodeGestionSacd.addActionListener(new CodeGestionSacdListener());
	//
	//		myCodeGestion = new JComboBox(myListener.getCodeGestionModel());
	//		myCodeGestion.addActionListener(new CodeGestionListener());
	//
	//		pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
	//		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);
	//
	//		final JPanel p = new JPanel(new BorderLayout());
	//		final Component[] comps = new Component[5];
	//		comps[0] = buildLine(new ZLabeledComponent("Comptabilité ", myCodeGestionSacd, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[1] = buildLine(new ZLabeledComponent("Code gestion ", myCodeGestion, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[2] = buildLine(new Component[] {
	//				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
	//		});
	//		comps[3] = buildLine(buildDateFields());
	//		comps[4] = buildLine(new Component[] {
	//			ZFormPanel.buildLabelField("Tri ", myComboModele)
	//		});
	//
	//		p.add(ZKarukeraPanel.buildVerticalPanelOfComponent(comps), BorderLayout.WEST);
	//		p.add(new JPanel(new BorderLayout()));
	//		return p;
	//	}

	//	private final JPanel buildDateFields() {
	//		datesPanel = ZFormPanel.buildFourchetteDateFields(" au ", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
	//		datesPanel.setBorder(BorderFactory.createEmptyBorder());
	//		return new ZLabeledComponent("Journées du ", datesPanel, ZLabeledComponent.LABELONLEFT, -1);
	//	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		super.updateData();
		datesPanel.updateData();
		pcoNum.updateData();
		//pcoSelectButton.updateData();
	}

	public interface IGrandLivrePanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {

		public DefaultComboBoxModel comboModeleModel();

		public Action actionImprimerXls();

		public IZLookupButtonListener getLookupButtonCompteListener();

		public IZLookupButtonModel getLookupButtonCompteModel();

	}

	//
	//	private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {
	//
	//		/**
	//		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
	//		 */
	//		public Object getValue() {
	//			return myListener.getFilters().get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
	//		}
	//
	//		/**
	//		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
	//		 */
	//		public void setValue(Object value) {
	//			myListener.getFilters().put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, value);
	//		}
	//
	//		public Window getParentWindow() {
	//			return getMyDialog();
	//		}
	//	}
	//
	//	private final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {
	//
	//		/**
	//		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
	//		 */
	//		public Object getValue() {
	//			return myListener.getFilters().get("dateDebut");
	//		}
	//
	//		/**
	//		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
	//		 */
	//		public void setValue(Object value) {
	//			myListener.getFilters().put("dateDebut", value);
	//		}
	//
	//		public Window getParentWindow() {
	//			return getMyDialog();
	//		}
	//	}

	//
	//	private final class CodeGestionListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestion((EOGestion) myListener.getCodeGestionModel().getSelectedEObject());
	//		}
	//
	//	}
	//
	//	private final class CodeGestionSacdListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestionSacd((EOGestion) myListener.getCodeGestionSacdModel().getSelectedEObject());
	//			if (myListener.getGestionSacd() != null) {
	//				myListener.setGestion(null);
	//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//				myCodeGestion.setEnabled(false);
	//			}
	//			else {
	//				myCodeGestion.setEnabled(true);
	//			}
	//		}
	//	}

	public ZLookupButton getPcoSelectButton() {
		return pcoSelectButton;
	}

}
