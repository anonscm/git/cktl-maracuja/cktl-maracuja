/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CoherenceSoldePanel extends ZKarukeraImprPanel {
	private final static String COMMENT = "<html>Cette édition vous permet de vérifier pour chaque compte si le solde respecte ce qui est indiqué <br>dans le plan comptable (débiteur, créditeur, nul)</html>";
	private final static String COMMENT_TITLE = "Vérification de la cohérence des soldes";
	public static final String SHOWALL = "showall";
	private ICoherenceSoldePanelListener myListener;
	private JComboBox myCodeGestionSacd;
	private ZFormPanel pcoNum;
	private JCheckBox checkBox;
	private final ZLookupButton pcoSelectButton;

	/**
	 * @param editingContext
	 */
	public CoherenceSoldePanel(ICoherenceSoldePanelListener listener) {
		super(listener);
		myListener = listener;
		pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());

	}

	@Override
	public String getComment() {
		return COMMENT;
	}

	@Override
	public String getCommentTitle() {
		return COMMENT_TITLE;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		pcoSelectButton.initGUI();
		super.initGUI();
	}

	//	private final JPanel buildRightPanel() {
	//		JPanel tmp = new JPanel(new BorderLayout());
	//		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
	//		ArrayList list = new ArrayList();
	//		list.add(myListener.actionImprimer());
	//		list.add(myListener.actionClose());
	//
	//		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
	//		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	//
	//		return tmp;
	//	}

	protected void buildLines() {
		checkBox = new JCheckBox("Voir tous les soldes (même sans erreur)");
		checkBox.addActionListener(new CheckBoxListener());

		pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);

		getLines().add(buildLine(agregatCtrUi.getMyAgregatsLabeled()));
		//getLines().add(buildLine(agregatCtrUi.getMyCodeGestionLabeled()));
		//		getLines().add(buildLine(niveauLabeled));
		//		getLines().add(buildLine(comptabiliteLabeled));
		//		getLines().add(buildLine(myCodeGestionLabeled));

		getLines().add(buildLine(new Component[] {
				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
		}));
		getLines().add(buildLine(new Component[] {
				checkBox
		}));

	}

	//	
	//	private final JPanel buildFilters() {
	//		myCodeGestionSacd = new JComboBox(myListener.getCodeGestionSacdModel());
	//		myCodeGestionSacd.addActionListener(new CodeGestionSacdListener());
	//
	//		checkBox = new JCheckBox("Voir tous les soldes (même sans erreur)");
	//		checkBox.addActionListener(new CheckBoxListener());
	//
	//		pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
	//		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);
	//
	//		final JPanel p = new JPanel(new BorderLayout());
	//		p.setBorder(ZKarukeraPanel.createMargin());
	//		final Component[] comps = new Component[3];
	//		comps[0] = buildLine(new ZLabeledComponent("Comptabilité ", myCodeGestionSacd, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[1] = buildLine(new Component[] {
	//				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
	//		});
	//		comps[2] = buildLine(new Component[] {
	//				checkBox
	//		});
	//
	//		p.add(ZKarukeraPanel.buildVerticalPanelOfComponent(comps), BorderLayout.WEST);
	//		p.add(new JPanel(new BorderLayout()));
	//		return p;
	//	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		super.updateData();
		pcoNum.updateData();
		checkBox.setSelected(((Boolean) myListener.getFilters().get(SHOWALL)).booleanValue());
	}

	public interface ICoherenceSoldePanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {
		//  public Action actionImprimer();
		public IZLookupButtonListener getLookupButtonCompteListener();

		public IZLookupButtonModel getLookupButtonCompteModel();

		public Map getPcoMap();
		//public Action actionClose();
		//public HashMap getFilters();
		//public ZEOComboBoxModel getCodeGestionSacdModel();
		//public void setGestionSacd(EOGestion gestion);
		//public EOGestion getGestionSacd();

	}

	//
	//	private final class CodeGestionSacdListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestionSacd((EOGestion) myListener.getCodeGestionSacdModel().getSelectedEObject());
	//		}
	//
	//	}

	private final class CheckBoxListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.getFilters().put(SHOWALL, new Boolean(checkBox.isSelected()));
		}

	}

	public ZLookupButton getPcoSelectButton() {
		return pcoSelectButton;
	}

}
