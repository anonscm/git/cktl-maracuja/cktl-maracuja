package org.cocktail.maracuja.client.impression.ui;

import java.awt.Component;
import java.util.Map;

import javax.swing.Box;

import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;

public class JournalConventionRAPanel extends JournalGeneralPanel {
	private ZFormPanel pcoNum;
	private final ZLookupButton pcoSelectButton;
	private IJournalConventionRAPanelListener myListener;

	public JournalConventionRAPanel(IJournalConventionRAPanelListener listener) {
		super(listener);
		myListener = listener;
		pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());
		pcoNum = ZFormPanel.buildLabelField("Compte(s)", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);

	}

	protected void buildLines() {
		super.buildLines();
		//getLines().add(buildTypeOperationsPanel());
		getLines().add(buildOriginePanel());
		getLines().add(buildLine(new Component[] {
				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
		}));
	}

	public void initGUI() {
		pcoSelectButton.initGUI();
		super.initGUI();
	}

	public interface IJournalConventionRAPanelListener extends IJournalGeneralPanelListener {

		public IZLookupButtonListener getLookupButtonCompteListener();

		public IZLookupButtonModel getLookupButtonCompteModel();

		public Map getPcoMap();

	}

	@Override
	public void updateData() throws Exception {
		super.updateData();
		//pcoSelectButton.updateData();
		pcoNum.updateData();
	}
}
