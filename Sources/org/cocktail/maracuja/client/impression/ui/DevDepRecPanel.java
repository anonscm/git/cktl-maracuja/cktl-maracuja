/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DevDepRecPanel extends ZKarukeraImprPanel {
	//    private final Color BORDURE_COLOR=getBackground().brighter();

	private IDevDepRecPanelListener myListener;

	//    private ZFormPanel datesPanel;

	//	private ZDatePickerField dateSaisieMaxField;
	//
	//	private JComboBox myCodeGestionSacd;
	//	private JComboBox myCodeGestion;
	//
	//	private final JComboBox comboNiveau;

	/**
	 * @param editingContext
	 */
	public DevDepRecPanel(IDevDepRecPanelListener listener) {
		super(listener);
		myListener = listener;
		//		comboNiveau = new JComboBox(myListener.getNiveauxModel());
	}

	//	/**
	//	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	//	 */
	//	public void initGUI() {
	//		setLayout(new BorderLayout());
	//		setBorder(ZKarukeraPanel.createMargin());
	//		add(buildFilters(), BorderLayout.CENTER);
	//		add(buildRightPanel(), BorderLayout.EAST);
	//		//        add(buildBottomPanel(), BorderLayout.SOUTH);
	//	}
	//
	//	private final JPanel buildRightPanel() {
	//		JPanel tmp = new JPanel(new BorderLayout());
	//		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
	//		ArrayList list = new ArrayList();
	//		list.add(myListener.actionImprimer());
	//		list.add(myListener.actionClose());
	//
	//		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
	//		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	//
	//		return tmp;
	//	}

	//    private JPanel buildBottomPanel() {
	//        ArrayList a = new ArrayList();
	//        a.add(myListener.actionClose());
	//        JPanel p = new JPanel(new BorderLayout());
	//        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
	//        return p;
	//    }

	//	private final JPanel buildFilters() {
	//
	////		comboNiveau.addActionListener(new NiveauxListener());
	////
	////		myCodeGestionSacd = new JComboBox(myListener.getCodeGestionSacdModel());
	////		myCodeGestionSacd.addActionListener(new CodeGestionSacdListener());
	////
	////		myCodeGestion = new JComboBox(myListener.getCodeGestionModel());
	////		myCodeGestion.addActionListener(new CodeGestionListener());
	////
	////		dateSaisieMaxField = new ZDatePickerField(new DateSaisieMaxFieldModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
	////		dateSaisieMaxField.getMyTexfield().setColumns(8);
	//
	//		//        pcoLookupField = new ZKeyValueLookUpField( new PcoNumNewModel(), myListener.getPcoMap());
	//		//        pcoLookupField.getMyTexfield().setColumns(10);
	//
	//		//        pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum") , this.getBackground());
	//		//        ((ZTextField)pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);
	//
	//		JPanel p = new JPanel(new BorderLayout());
	//		Component[] comps = new Component[4];
	//
	//		comps[0] = buildLine(new ZLabeledComponent("Niveau ", comboNiveau, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[1] = buildLine(new ZLabeledComponent("Comptabilité ", myCodeGestionSacd, ZLabeledComponent.LABELONLEFT, -1));
	//		comps[2] = buildLine(new ZLabeledComponent("Code gestion ", myCodeGestion, ZLabeledComponent.LABELONLEFT, -1));
	//		//comps[2] = buildLine(new ZLabeledComponent("Compte ", buildLine(new Component[]{pcoLookupField }), ZLabeledComponent.LABELONLEFT, -1));
	//		//        comps[2] = buildLine(new Component[]{pcoNum, ZTooltip.getTooltip_SELECTCOMPTE()});
	//
	//		//        comps[3] = buildLine(new Component[]{pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()});
	//		comps[3] = buildLine(new ZLabeledComponent("Date opérations", dateSaisieMaxField, ZLabeledComponent.LABELONLEFT, 125));
	//		;
	//
	//		p.add(ZKarukeraPanel.buildVerticalPanelOfComponent(comps), BorderLayout.WEST);
	//		p.add(new JPanel(new BorderLayout()));
	//		return p;
	//
	//	}

	//	/**
	//	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	//	 */
	//	public void updateData() throws Exception {
	//		//        myListener.getCodeGestionSacdModel().setSelectedEObject(myListener.getGestionSacd());
	//		//        myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//		//        pcoNum.updateData();
	//		comboNiveau.setSelectedItem(myListener.getFilters().get(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY));
	//
	//		dateSaisieMaxField.updateData();
	//		myCodeGestion.setEnabled(false);
	//	}

	public interface IDevDepRecPanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {
		//		public Action actionImprimer();
		//
		//		public ComboBoxModel getNiveauxModel();
		//
		//		public Action actionClose();
		//
		//		public HashMap getFilters();
		//
		//		public ZEOComboBoxModel getCodeGestionSacdModel();
		//
		//		public ZEOComboBoxModel getCodeGestionModel();
		//
		//		public void setGestion(EOGestion gestion);
		//
		//		public void setGestionSacd(EOGestion gestion);
		//
		//		public EOGestion getGestionSacd();
		//
		//		public EOGestion getGestion();

	}
	//
	//	private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {
	//
	//		/**
	//		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
	//		 */
	//		public Object getValue() {
	//			return myListener.getFilters().get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
	//		}
	//
	//		/**
	//		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
	//		 */
	//		public void setValue(Object value) {
	//			myListener.getFilters().put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, value);
	//		}
	//
	//		public Window getParentWindow() {
	//			return getMyDialog();
	//		}
	//	}
	//
	//	private final class NiveauxListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.getFilters().put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, comboNiveau.getSelectedItem());
	//
	//			if (DevDepRecCtrl.NIVEAU2_ETAB_SACD.equals(comboNiveau.getSelectedItem())) {
	//				myListener.setGestion(null);
	//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//				myCodeGestion.setEnabled(false);
	//				myCodeGestionSacd.setEnabled(true);
	//			}
	//			else if (DevDepRecCtrl.NIVEAU1_AGREGE.equals(comboNiveau.getSelectedItem())) {
	//				myListener.setGestionSacd(null);
	//				myListener.getCodeGestionSacdModel().setSelectedEObject(myListener.getGestionSacd());
	//				myListener.setGestion(null);
	//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//				myCodeGestion.setEnabled(false);
	//				myCodeGestionSacd.setEnabled(false);
	//
	//			}
	//			else {
	//				myListener.setGestionSacd(null);
	//				myListener.getCodeGestionSacdModel().setSelectedEObject(myListener.getGestionSacd());
	//				myCodeGestionSacd.setEnabled(false);
	//				myCodeGestion.setEnabled(true);
	//			}
	//
	//		}
	//
	//	}
	//
	//	private final class CodeGestionListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestion((EOGestion) myListener.getCodeGestionModel().getSelectedEObject());
	//		}
	//
	//	}
	//
	//	private final class CodeGestionSacdListener implements ActionListener {
	//
	//		/**
	//		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//		 */
	//		public void actionPerformed(ActionEvent e) {
	//			myListener.setGestionSacd((EOGestion) myListener.getCodeGestionSacdModel().getSelectedEObject());
	//			myListener.setGestion(null);
	//			myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//
	//			//			
	//			//			if (myListener.getGestionSacd() != null) {
	//			//				myListener.setGestion(null);
	//			//				myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//			//				myCodeGestion.setEnabled(false);
	//			//			}
	//			//			else {
	//			//				myCodeGestion.setEnabled(true);
	//			//			}
	//		}
	//
	//	}

}
