/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.impression.ZKarukeraImprCtrl;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextField;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DevSoldePanel extends ZKarukeraImprPanel {

	private IDevSoldePanelListener myListener;
	private ZFormPanel pcoNum;
	private final ZLookupButton pcoSelectButton;
	private final JComboBox comboModele;
	private final JComboBox comboTri;

	private final JCheckBox checkBoxSolde;
	private ZDatePickerField dateSaisieMinField;

	public JCheckBox getCheckBoxSolde() {
		return checkBoxSolde;
	}

	/**
	 * @param editingContext
	 */
	public DevSoldePanel(IDevSoldePanelListener listener) {
		super(listener);
		myListener = listener;
		pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());

		comboModele = new JComboBox(myListener.getModelesModel());
		comboTri = new JComboBox(listener.comboModeleTri());
		pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);

		comboModele.addActionListener(new ModeleListener());

		checkBoxSolde = new JCheckBox("Reste à émarger <> 0");
		checkBoxSolde.setSelected(true);
		checkBoxSolde.addActionListener(myListener.getCheckSoldeListener());

		dateSaisieMinField = new ZDatePickerField(new DateSaisieMinFieldModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateSaisieMinField.getMyTexfield().setColumns(8);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		pcoSelectButton.initGUI();
		super.initGUI();
	}

	public final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionImprimer());
		list.add(myListener.actionImprimerXls());
		list.add(myListener.actionImprimerXls2());
		list.add(myListener.actionClose());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}

	protected void buildLines() {
		getLines().add(buildLine(new ZLabeledComponent("Modèle d'impression ", comboModele, ZLabeledComponent.LABELONLEFT, -1)));
		getLines().add(buildLine(agregatCtrUi.getMyAgregatsLabeled()));
		getLines().add(buildLine(agregatCtrUi.getMyCodeGestionLabeled()));
		getLines().add(buildLine(new ZLabeledComponent("Date début écritures", dateSaisieMinField, ZLabeledComponent.LABELONLEFT, 125)));
		getLines().add(buildLine(new ZLabeledComponent("Date fin écritures", dateSaisieMaxField, ZLabeledComponent.LABELONLEFT, 125)));
		getLines().add(buildLine(new Component[] {
				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
		}));
		getLines().add(buildLine(checkBoxSolde));
		getLines().add(buildLine(new ZLabeledComponent("Tri ", comboTri, ZLabeledComponent.LABELONLEFT, -1)));
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		super.updateData();
		pcoNum.updateData();
		dateSaisieMinField.updateData();
	}

	public interface IDevSoldePanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {
		public Action actionImprimerXls();

		public Action actionImprimerXls2();

		public ComboBoxModel getModelesModel();

		public ComboBoxModel comboModeleTri();

		public IZLookupButtonListener getLookupButtonCompteListener();

		public IZLookupButtonModel getLookupButtonCompteModel();

		public ActionListener getCheckSoldeListener();

	}

	//
	//    private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {
	//
	//        /**
	//         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
	//         */
	//        public Object getValue() {
	//            return myListener.getFilters().get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
	//        }
	//
	//        /**
	//         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
	//         */
	//        public void setValue(Object value) {
	//            myListener.getFilters().put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, value);
	//        }
	//
	//        public Window getParentWindow() {
	//            return getMyDialog();
	//        }
	//    }

	private final class ModeleListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.getFilters().put(ZKarukeraImprCtrl.MODELE_FILTER_KEY, comboModele.getSelectedItem());
		}

	}

	//    private final class CodeGestionListener implements ActionListener {
	//
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            myListener.setGestion( (EOGestion) myListener.getCodeGestionModel().getSelectedEObject() )  ;
	//        }
	//
	//    }
	//
	//    private final class CodeGestionSacdListener implements ActionListener {
	//
	//        /**
	//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	//         */
	//        public void actionPerformed(ActionEvent e) {
	//            myListener.setGestionSacd( (EOGestion) myListener.getCodeGestionSacdModel().getSelectedEObject() )  ;
	//            if (myListener.getGestionSacd()!=null) {
	//                myListener.setGestion(null);
	//                myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//                myCodeGestion.setEnabled(false);
	//            }
	//            else {
	//                myCodeGestion.setEnabled(true);
	//            }
	//        }
	//
	//    }

	public ZLookupButton getPcoSelectButton() {
		return pcoSelectButton;
	}
	//
	//	final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {
	//		public Object getValue() {
	//			return myListener.getFilters().get(DevSoldeCtrl.DATE_DEBUT_FILTER_KEY);
	//		}
	//
	//		public void setValue(Object value) {
	//			myListener.getFilters().put(DevSoldeCtrl.DATE_DEBUT_FILTER_KEY, value);
	//		}
	//
	//		public Window getParentWindow() {
	//			return getMyDialog();
	//		}
	//	}

}
