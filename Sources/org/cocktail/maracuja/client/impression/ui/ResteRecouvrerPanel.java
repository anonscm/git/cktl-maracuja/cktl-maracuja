/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.Component;
import java.text.DateFormat;
import java.text.Format;

import javax.swing.Box;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.zutil.client.ui.ZLookupButton;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ResteRecouvrerPanel extends ZKarukeraImprPanel {
	private IResteRecouvrerPanelListener myListener;
	private ZDatePickerField dateSaisieMaxField;
	private ZFormPanel pcoNum;
	private ZFormPanel reste;
	private ZFormPanel debiteur;
	private ZFormPanel siret;
	private final ZLookupButton pcoSelectButton;

	/**
	 * @param editingContext
	 */
	public ResteRecouvrerPanel(IResteRecouvrerPanelListener listener) {
		super(listener);
		myListener = listener;
		pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());
		dateSaisieMaxField = new ZDatePickerField(new DateSaisieMaxFieldModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateSaisieMaxField.getMyTexfield().setColumns(8);

		pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "pcoNum"));
		((ZTextField) pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);

		ZNumberField ecdResteEmarger = new ZNumberField(new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "ecdResteEmarger"), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER
		}, ZConst.FORMAT_EDIT_NUMBER);
		ecdResteEmarger.getMyTexfield().setColumns(8);

		reste = ZFormPanel.buildLabelField("Reste à recouvrer >=", ecdResteEmarger);
		debiteur = ZFormPanel.buildLabelField("Débiteur", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "debiteur"));
		((ZTextField) debiteur.getMyFields().get(0)).getMyTexfield().setColumns(20);

		siret = ZFormPanel.buildLabelField("Siret", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "siret"));
		((ZTextField) siret.getMyFields().get(0)).getMyTexfield().setColumns(20);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		super.initGUI();
		pcoSelectButton.initGUI();
	}

	protected void buildLines() {
		getLines().add(buildLine(agregatCtrUi.getMyAgregatsLabeled()));
		getLines().add(buildLine(agregatCtrUi.getMyCodeGestionLabeled()));
		getLines().add(buildLine(new Component[] {
				pcoNum, pcoSelectButton, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()
		}));
		getLines().add(buildLine(new ZLabeledComponent("Titres émis avant le ", dateSaisieMaxField, ZLabeledComponent.LABELONLEFT, 125)));
		getLines().add(buildLine(reste));
		getLines().add(buildLine(debiteur));
		getLines().add(buildLine(siret));
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		dateSaisieMaxField.updateData();
		pcoNum.updateData();
		super.updateData();
	}

	public interface IResteRecouvrerPanelListener extends ZKarukeraImprPanel.IZKarukeraImprlListener {

		public IZLookupButtonListener getLookupButtonCompteListener();

		public IZLookupButtonModel getLookupButtonCompteModel();

	}

	public ZLookupButton getPcoSelectButton() {
		return pcoSelectButton;
	}

}
