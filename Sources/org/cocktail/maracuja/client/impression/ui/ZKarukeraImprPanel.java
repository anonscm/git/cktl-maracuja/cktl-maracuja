/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.AgregatsCtrl;
import org.cocktail.maracuja.client.impression.ZKarukeraImprCtrl;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZKarukeraImprPanel extends ZKarukeraPanel {

	protected IZKarukeraImprlListener myListener;
	protected ZDatePickerField dateSaisieMaxField;
	protected ZLabeledComponent myDateSaisieMaxLabeled;
	protected ZFormPanel datesPanel;
	protected JPanel origineSelectPanel;
	protected ZCommentPanel commentPanel;

	protected AgregatsCtrlUi agregatCtrUi;

	private ArrayList lines = new ArrayList();
	private JComboBox myTypeOperationField;
	private JLabel origineLabel;

	public ZKarukeraImprPanel(IZKarukeraImprlListener listener) {
		super();
		myListener = listener;
		agregatCtrUi = new AgregatsCtrlUi(myListener.getAgregatCtrl(), myListener.getFilters());
		dateSaisieMaxField = new ZDatePickerField(new DateSaisieMaxFieldModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateSaisieMaxField.getMyTexfield().setColumns(8);
		myDateSaisieMaxLabeled = new ZLabeledComponent("Date des opérations ", dateSaisieMaxField, ZLabeledComponent.LABELONLEFT, 125);
	}

	public void initGUI() {
		setLayout(new BorderLayout());
		setBorder(ZKarukeraPanel.createMargin());
		if (getComment() != null || getCommentTitle() != null) {
			commentPanel = new ZCommentPanel(getCommentTitle(), getComment(), null, Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.WEST);
			add(commentPanel, BorderLayout.NORTH);
		}
		add(buildFilters(), BorderLayout.CENTER);
		add(buildRightPanel(), BorderLayout.EAST);
	}

	public String getComment() {
		return null;
	}

	public String getCommentTitle() {
		return null;
	}

	public JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionImprimer());
		list.add(myListener.actionClose());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}

	protected final JPanel buildFilters() {
		buildLines();
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraPanel.buildVerticalPanelOfComponents(getLines(), 5, 20), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()));
		return p;

	}

	/**
	 * Initialise les lignes a afficher.Par defaut Niveau, comptabilite, code gestion, date operations.
	 */
	protected void buildLines() {
		getLines().add(buildLine(agregatCtrUi.getMyAgregatsLabeled()));
		getLines().add(buildLine(agregatCtrUi.getMyCodeGestionLabeled()));
		lines.add(buildLine(myDateSaisieMaxLabeled));
	}

	public void updateData() throws Exception {
		if (datesPanel != null) {
			datesPanel.updateData();
		}
	}

	public interface IZKarukeraImprlListener {
		public Action actionImprimer();

		public Action actionClose();

		public HashMap getFilters();

		public AgregatsCtrl getAgregatCtrl();

		public ZEOComboBoxModel typeOperationsModel();

		public ZEOComboBoxModel typeJournalModel();

		public Action actionSelectOrigine();

	}

	final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {
		public Object getValue() {
			return myListener.getFilters().get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
		}

		public void setValue(Object value) {
			myListener.getFilters().put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {
		public Object getValue() {
			return myListener.getFilters().get(ZKarukeraImprCtrl.DATE_DEBUT_FILTER_KEY);
		}

		public void setValue(Object value) {
			myListener.getFilters().put(ZKarukeraImprCtrl.DATE_DEBUT_FILTER_KEY, value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

	public ArrayList getLines() {
		return lines;
	}

	public IZKarukeraImprlListener getMyListener() {
		return myListener;
	}

	public ZDatePickerField getDateSaisieMaxField() {
		return dateSaisieMaxField;
	}

	public ZLabeledComponent getMyDateSaisieMaxLabeled() {
		return myDateSaisieMaxLabeled;
	}

	protected JPanel buildDateFields() {
		datesPanel = ZFormPanel.buildFourchetteDateFields(" au ", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		datesPanel.setBorder(BorderFactory.createEmptyBorder());
		return new ZLabeledComponent("Journées du ", datesPanel, ZLabeledComponent.LABELONLEFT, -1);
	}

	protected JPanel buildTypeOperationsPanel() {
		myTypeOperationField = new JComboBox(myListener.typeOperationsModel());
		myTypeOperationField.addActionListener((new TypeOperationListener()));
		return buildLine(new Component[] {
				ZFormPanel.buildLabelField("Type Opérations ", myTypeOperationField)
		});

	}

	protected JPanel buildOriginePanel() {
		origineLabel = new JLabel();
		//	myListener.actionSelectOrigine().setEnabled(false);
		return buildLine(new Component[] {
				ZFormPanel.buildLabelField("Convention RA ", new JButton(myListener.actionSelectOrigine())), origineLabel
		});

	}

	private final class TypeOperationListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			myListener.getFilters().put(ZKarukeraImprCtrl.TYPE_OPERATION_FILTER_KEY, myListener.typeOperationsModel().getSelectedEObject());
			myListener.getFilters().put(ZKarukeraImprCtrl.ORIGINE_FILTER_KEY, null);
			try {
				updateOrigine();
				myListener.actionSelectOrigine().setEnabled(myListener.getFilters().get(ZKarukeraImprCtrl.TYPE_OPERATION_FILTER_KEY) != null);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

	}

	public void updateOrigine() {
		if (origineLabel != null) {
			origineLabel.setText("");
			if (myListener.getFilters().get(ZKarukeraImprCtrl.ORIGINE_FILTER_KEY) != null) {
				origineLabel.setText(((EOOrigine) myListener.getFilters().get(ZKarukeraImprCtrl.ORIGINE_FILTER_KEY)).oriLibelle());
			}
		}
	}
}
