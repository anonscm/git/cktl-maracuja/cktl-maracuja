/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PlanComptableImprPanel extends ZKarukeraPanel {
//    public static final String MODELE_KEY = ZKarukeraImprCtrl.MODELE_FILTER_KEY;
    
    public static final String PLANCO_KEY = "planco";

    private IPlanComptableImprPanelListener myListener;

    private ZFormPanel pcoNum;
    private final JComboBox comboModele;
    private final JCheckBox cbValide;
//    private final ZLookupButton pcoSelectButton;
    
    /**
     * @param editingContext
     */
    public PlanComptableImprPanel(IPlanComptableImprPanelListener listener) {
        super();
        myListener = listener;
        comboModele = new JComboBox(myListener.getModelesModel());
//        comboModele.addActionListener(new ModeleListener());
        cbValide = new JCheckBox("Comptes actifs uniquement");
        cbValide.addActionListener(new ValideListener());
//        pcoSelectButton = new ZLookupButton(myListener.getLookupButtonCompteModel(), myListener.getLookupButtonCompteListener());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(buildRightPanel(), BorderLayout.EAST);
    }


    private final JPanel buildRightPanel() {
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        ArrayList list = new ArrayList();
        list.add(myListener.actionImprimer());
        list.add(myListener.actionImprimerXls());
        list.add(myListener.actionClose());

        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

        return tmp;
    }


    private final JPanel buildFilters() {

        pcoNum = ZFormPanel.buildLabelField("Compte(s)/Classe(s)", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), PLANCO_KEY) );
        ((ZTextField)pcoNum.getMyFields().get(0)).getMyTexfield().setColumns(20);        
        
        JPanel p = new JPanel(new BorderLayout());
        Component[] comps = new Component[4];
        comps[0] = buildLine(new ZLabeledComponent("Modèle d'impression ", comboModele, ZLabeledComponent.LABELONLEFT, -1));
        comps[1] = buildLine(new Component[]{cbValide});
        comps[2] = buildLine(new Component[]{pcoNum, Box.createHorizontalStrut(15), ZTooltip.getTooltip_SELECTCOMPTES()});
        comps[3] = new JPanel(new BorderLayout());

//        p.add(ZKarukeraPanel.buildVerticalPanelOfComponent(comps), BorderLayout.WEST);
        p.add(ZUiUtil.buildBoxColumn(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;

    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        pcoNum.updateData();
    }


    public interface IPlanComptableImprPanelListener {
        public Action actionImprimer();
        public Action actionImprimerXls();
        public Action actionClose();
        public HashMap getFilters();
        public ComboBoxModel getModelesModel();
//        public IZLookupButtonListener getLookupButtonCompteListener();
//        public IZLookupButtonModel getLookupButtonCompteModel();        
    }


//
//    private final class ModeleListener implements ActionListener {
//        
//        
//
//        /**
//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
//         */
//        public void actionPerformed(ActionEvent e) {
//            myListener.getFilters().put(MODELE_KEY, comboModele.getSelectedItem() )  ;
//        }
//        
//    }
    
    private final class ValideListener implements ActionListener {
        
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            myListener.getFilters().put(EOPlanComptable.PCO_VALIDITE_KEY, (cbValide.isSelected() ? EOPlanComptableExer.etatValide : null ) )  ;
        }
        
    }

}
