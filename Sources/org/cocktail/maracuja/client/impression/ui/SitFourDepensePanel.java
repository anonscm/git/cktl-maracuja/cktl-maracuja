/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZNSKeyValueCodingTextFieldModel;

import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SitFourDepensePanel extends ZKarukeraPanel {
//    private final Color BORDURE_COLOR=getBackground().brighter();

    private ISitFourDepenseListener myListener;
    private ZFormPanel fournisseur;


    /**
     * @param editingContext
     */
    public SitFourDepensePanel(ISitFourDepenseListener listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(buildRightPanel(), BorderLayout.EAST);
    }


    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        final ArrayList list = new ArrayList();
        list.add(myListener.actionImprimer());
        list.add(myListener.actionClose());

        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

        return tmp;
    }


    private final JPanel buildFilters() {
//        fournisseur = ZFormPanel.buildLabelField("Fournisseur", new ZActionField(new ZTextField.EOTextFieldModel(myListener.getFilters(), "fournisseur","nomAndPrenomAndCode"), (AbstractAction) myListener.actionSelectFournis()) );
        fournisseur = ZFormPanel.buildLabelField("Fournisseur", new ZActionField(new FournisseurProvider(), (AbstractAction) myListener.actionSelectFournis()) );
        ((ZTextField)fournisseur.getMyFields().get(0)).getMyTexfield().setColumns(20);
        ((ZTextField)fournisseur.getMyFields().get(0)).getMyTexfield().setEnabled(false);

        
        
        final JPanel p = new JPanel(new BorderLayout());
        final Component[] comps = new Component[1];
        comps[0] = buildLine(new Component[]{fournisseur});

        p.add(ZKarukeraPanel.buildVerticalPanelOfComponent(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;

    }




    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        fournisseur.updateData();
    }




    public interface ISitFourDepenseListener {
        public Action actionImprimer();
        public Action actionSelectFournis();
        public Action actionClose();
        public HashMap getFilters();
    }


    
    private final class FournisseurProvider extends ZNSKeyValueCodingTextFieldModel  {

        public FournisseurProvider() {
            super(EOFournisseur.NOM_AND_PRENOM_KEY);
        }

        public Object getValue() {
            return super.getValue();
        }

        public void setValue(Object value) {
            super.setValue(null);
//            System.out.println("UtilDebutProvider.setValue() = " + value);
            if (value!=null) {
                if (value instanceof Date) {
                    super.setValue(new NSTimestamp((Date)value));
                }
            }
        }


        protected NSKeyValueCodingAdditions getObject() {
            return (NSKeyValueCodingAdditions) myListener.getFilters().get("fournisseur");
        }
    }    



}
