/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.ExecBudGestPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ExecBudGestCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression de l'exécution du budget de gestion";
	private final Dimension WINDOW_SIZE = new Dimension(550, 200);
	//    private final String ACTION_ID  = ZActionCtrl.IDU_IMPR004;
	private ExecBudGestPanel myPanel;
	private final String MODELE1 = "Dépenses";
	private final String MODELE2 = "Recettes";

	private final String MODELECONSO1 = "Consolidé";
	private final String MODELECONSO2 = "Non consolidé (= par UB)";

	private DefaultComboBoxModel modeleModel;
	private DefaultComboBoxModel consolidationModel;
	private final Map modelesMap = new HashMap(2);
	private final Map consolidationModelMap = new HashMap(2);

	static public final String DEPENSE = "DEPENSE";
	static public final String RECETTE = "RECETTE";

	static public final String JASPERFILENAME_EXECBUDGEST_DEP = "budgest_dep.jasper";
	public static final String JASPERFILENAME_EXECBUDGEST_REC = "budgest_rec.jasper";
	public static final String JASPERFILENAME_EXECBUDGEST_DEP2007 = "comptefi_budgetgestionexec.jasper";
	public static final String JASPERFILENAME_EXECBUDGEST_COMP_DEP2007 = "comptefi_budgetgestionexec_comp.jasper";

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ExecBudGestCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		modeleModel = new DefaultComboBoxModel();
		modeleModel.addElement(MODELE1);
		modeleModel.addElement(MODELE2);

		modelesMap.put(MODELE1, DEPENSE);
		modelesMap.put(MODELE2, RECETTE);

		consolidationModel = new DefaultComboBoxModel();
		consolidationModel.addElement(MODELECONSO1);
		if (getExercice().exeExercice().intValue() >= 2007) {
			consolidationModel.addElement(MODELECONSO2);
		}
		myPanel = new ExecBudGestPanel(new ExecBudGestPanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath;

			if (getExercice().exeExercice().intValue() < 2007) {
				filePath = imprimerExecGestBudDep(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), (String) modelesMap.get(myFilters.get(ZKarukeraImprCtrl.MODELE_FILTER_KEY)));
			}
			else {
				filePath = imprimerExecGestBudDep2007(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), (String) modelesMap.get(myFilters.get(ZKarukeraImprCtrl.MODELE_FILTER_KEY)), MODELECONSO1.equals(myFilters.get("modeleconso")));
			}

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected final void initFilters() {
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		myFilters.put(ZKarukeraImprCtrl.MODELE_FILTER_KEY, MODELE1);
		myFilters.put("modeleconso", MODELECONSO1);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public void doBeforeOpen() {
		//        myPanel.getPcoSelectButton().updateData();
	}

	private final class ExecBudGestPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements ExecBudGestPanel.IExecBudGestPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#getPcoMap()
		 */
		public Map getPcoMap() {
			return pcoMap;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public ComboBoxModel getModelesModel() {
			return modeleModel;
		}

		public ComboBoxModel getModelesConsoModel() {
			return consolidationModel;
		}

	}

	protected String getActionId() {
		return null;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	public final String imprimerExecGestBudDep(final EOEditingContext ec, final String temporaryDir, final NSDictionary parametres, final HashMap paramRequete, final Window win, final String modeleJasper) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condExer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		String condGestion = "";

		final NSMutableDictionary params = new NSMutableDictionary(parametres);

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", "ub");
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");
		params.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");

		String req = "";

		if (DEPENSE.equals(modeleJasper)) {
			req = "select EXE_ORDRE, null as ges_code, DST_CODE, DST_LIB, SSDST_CODE, SSDST_LIB, " +
					"sum(SUM_DEP10) SUM_DEP10, sum(SUM_DEP20) SUM_DEP20, sum(SUM_DEP30) SUM_DEP30 " +
					"from COMPTEFI.V_SITU_EXEBUDGDEP " +
					"where " + condExer + (condGestion.length() != 0 ? " and " + condGestion : "")
					+ " group by EXE_ORDRE, DST_CODE, DST_LIB, SSDST_CODE, SSDST_LIB " + " order by EXE_ORDRE, DST_CODE, DST_LIB, SSDST_CODE, SSDST_LIB";
		}
		else { //on veut les recettes
			req = "select EXE_ORDRE, null as ges_code, DST_CODE, DST_LIB, SSDST_CODE, SSDST_LIB, " +
					"sum(SUM_REC1) SUM_REC1, sum(SUM_REC2) SUM_REC2 " +
					"from comptefi.V_SITU_EXEBUDGREC " +
					"where " + condExer +
					(condGestion.length() != 0 ? " and " + condGestion : "") + " group by EXE_ORDRE, DST_CODE, DST_LIB, SSDST_CODE, SSDST_LIB " + " order by EXE_ORDRE, DST_CODE, DST_LIB, SSDST_CODE, SSDST_LIB";

		}
		String jasper = (DEPENSE.equals(modeleJasper) ? JASPERFILENAME_EXECBUDGEST_DEP : JASPERFILENAME_EXECBUDGEST_REC);
		return imprimerReportByThreadPdf(ec, temporaryDir, params, jasper, getFileNameWithExtension(ZFileUtil.removeExtension(jasper), FORMATPDF), req, win, null);
	}

	/**
	 * Budget de gestion >=2007
	 * 
	 * @param ec
	 * @param temporaryDir
	 * @param parametres
	 * @param paramRequete
	 * @param win
	 * @param modeleJasper
	 * @param consolide
	 * @return
	 * @throws Exception
	 */
	public final String imprimerExecGestBudDep2007(final EOEditingContext ec, final String temporaryDir, final NSDictionary parametres, final HashMap paramRequete, final Window win, final String modeleJasper, boolean consolide) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condExer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, new NSArray(exercice));
		String condGestion = "";

		final NSMutableDictionary params = new NSMutableDictionary(parametres);

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", "ub");
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");
		params.takeValueForKey(modeleJasper, "NATURE");
		params.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");

		String req0 = "	select lolf_code_pere, lolf_libelle_pere, tcd_libelle,BUDGET,ub,exercice,code_lolf,libelle_lolf,type_credit,nature, " +
				" sum(case when masse='1' then montant else 0 end) fonctionnement, " +
				" sum(case when masse='2' then montant else 0 end) investissement,"
				+ " sum(case when masse='3' then montant else 0 end) personnel "
				+ " from maracuja.v_budget_lolf_exec "
				+ " group by lolf_code_pere, lolf_libelle_pere, tcd_libelle,BUDGET,ub,exercice,code_lolf,libelle_lolf,type_credit,nature ";
		String req = "";

		req = "select exercice,lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf,nature, " + (consolide ? "" : "ub,") +
				" sum(fonctionnement) fonctionnement,  sum(investissement) investissement,  sum(personnel) personnel,  sum(fonctionnement+investissement+personnel) TOTAL "
				+ " from ( "
				+ req0 + ") where "
				+ condExer +
				(condGestion.length() != 0 ? " and " + condGestion : "") +
				" and budget = 'execution' " + " and nature='" + modeleJasper + "' " + " group by nature, " + (consolide ? "" : "ub,")
				+ "lolf_code_pere, lolf_libelle_pere, exercice,code_lolf,libelle_lolf " + " order by nature, " + (consolide ? "" : "ub,") + "lolf_code_pere, lolf_libelle_pere, exercice,code_lolf";

		return imprimerReportByThreadPdf(ec, temporaryDir, params, (consolide ? JASPERFILENAME_EXECBUDGEST_DEP2007 : JASPERFILENAME_EXECBUDGEST_COMP_DEP2007), getFileNameWithExtension(ZFileUtil.removeExtension(JASPERFILENAME_EXECBUDGEST_DEP2007), FORMATPDF), req, win, null);
	}

}
