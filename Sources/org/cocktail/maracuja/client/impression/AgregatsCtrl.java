/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.io.StringWriter;
import java.util.Enumeration;
import java.util.LinkedHashMap;

import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOGestionAgregat;
import org.cocktail.maracuja.client.metier.EOGestionExercice;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Permet de récupérer les agrégats, comptabilites et codes gestions autorisés pour classification dans les opérations d'impressions.
 * 
 * @author rprin
 */
public class AgregatsCtrl {
	public static final String AGREGAT_KEY = "agregat";
	public static final String GES_CODE_KEY = "gesCode";
	protected static final ApplicationClient myApp = (ApplicationClient) EOApplication.sharedApplication();
	public static final String NIVEAU3_UB = "UB";
	public static final String NIVEAU1_AGREGE = "Agrégé (Etablissement + SACDs)";
	public static final String NIVEAU2_SACD_PREFIX = "SACD ";
	public static final String NIVEAU2_ETABLISSEMENT = "Etablissement";
	private static final String STR_GES_CODE_AGREGE = "AGREGE";
	private static final String STR_GES_CODE_ETAB = "ETAB";
	private static final String STR_GES_CODE_SACD = EOGestionAgregat.RESERVE_SACD;

	private static final String _OR = " or ";
	private static final String _QUOTE = "'";
	private static final String _EQUALS = "=";
	private static final String STR_SACD_N = "N";
	private static final String STR_SACD_O = "O";
	private static final String STR_SACD_AGREGE = "G";

	private String actionID;

	private NSArray eoagregats;
	private NSArray allgestions;
	private NSArray allgestionsNonSACD;
	private NSArray allgestionsSACD;
	private NSArray allAuthorizedGestions;
	private NSArray authorizedGestionsSacd;
	private NSArray authorizedGestionsNonSacd;
	private boolean isAllCodeGestionAuthorized = false;
	private boolean isAllCodeGestionOnEtabAuthorized = false;

	private LinkedHashMap<String, NSArray> agregatsMap;

	public AgregatsCtrl(String actionID) throws Exception {
		this(actionID, true, true, true, true, true);
	}

	public AgregatsCtrl(String actionID, boolean isAgregeActif, boolean isEtabActif, boolean isSACDActif, boolean isUBActif, boolean isPersoActif) throws Exception {
		this.actionID = actionID;
		agregatsMap = new LinkedHashMap<String, NSArray>();
		init(isAgregeActif, isEtabActif, isSACDActif, isUBActif, isPersoActif);
	}

	public void init(boolean isAgregeActif, boolean isEtabActif, boolean isSACDActif, boolean isUBActif, boolean isPersoActif) throws Exception {
		agregatsMap.clear();
		allgestions = EOsFinder.getAllGestionsPourExercice(myApp.editingContext(), myApp.appUserInfo().getCurrentExercice());
		allgestionsNonSACD = EOsFinder.getGestionNonSacd(myApp.editingContext(), myApp.appUserInfo().getCurrentExercice());
		allgestionsSACD = ZEOUtilities.complementOfNSArray(allgestionsNonSACD, allgestions);
		if (actionID == null) {
			allAuthorizedGestions = allgestions;
			authorizedGestionsSacd = allgestionsSACD;
			authorizedGestionsNonSacd = allgestionsNonSACD;
		}
		else {
			allAuthorizedGestions = myApp.appUserInfo().getAllowedCodesGestionForFonction(myApp.editingContext(), myApp.getMyActionsCtrl(), actionID);
			authorizedGestionsSacd = myApp.appUserInfo().getAllowedSacdsForFonction(myApp.editingContext(), myApp.getMyActionsCtrl(), actionID);
			authorizedGestionsNonSacd = myApp.appUserInfo().getAllowedNonSacdsForFonction(myApp.editingContext(), myApp.getMyActionsCtrl(), actionID);
		}

		eoagregats = EOGestionAgregat.getGestionAgregatsForExercice(myApp.appUserInfo().getCurrentExercice());

		final NSArray extGestion = ZEOUtilities.extOfNSArrays(allAuthorizedGestions, allgestions, false);
		isAllCodeGestionAuthorized = extGestion.count() == 0;

		final NSArray intersect = ZEOUtilities.intersectionOfNSArrayWithGlobalIds(allAuthorizedGestions, allgestionsNonSACD);
		isAllCodeGestionOnEtabAuthorized = intersect.count() == allgestionsNonSACD.count();

		//ajouter les agregats built-in
		//1 Agrégés
		if (isAgregeActif && isAllCodeGestionAuthorized) {
			NSMutableArray gescodes = new NSMutableArray();
			gescodes.addObjectsFromArray((NSArray) allAuthorizedGestions.valueForKey(EOGestion.GES_CODE_KEY));
			agregatsMap.put(NIVEAU1_AGREGE, gescodes);
		}

		//2 Etab 
		if (isEtabActif && isAllCodeGestionOnEtabAuthorized) {
			agregatsMap.put(NIVEAU2_ETABLISSEMENT, (NSArray) allgestionsNonSACD.valueForKey(EOGestion.GES_CODE_KEY));
		}

		/// 3 SACDs
		if (isSACDActif) {
			for (int i = 0; i < authorizedGestionsSacd.count(); i++) {
				EOGestion gestion = (EOGestion) authorizedGestionsSacd.objectAtIndex(i);
				agregatsMap.put(NIVEAU2_SACD_PREFIX + gestion.gesCode(), new NSArray(new Object[] {
						gestion.gesCode()
				}));
			}
		}

		//4 UBs
		if (isUBActif && authorizedGestionsNonSacd.count() > 0) {
			agregatsMap.put(NIVEAU3_UB, (NSArray) authorizedGestionsNonSacd.valueForKey(EOGestion.GES_CODE_KEY));
		}

		//ajouter les agregats personnalisés (seulement lorsque l'utilisateur a tous les droits sur les codes gestions pour la fonction).
		if (isPersoActif) {
			for (int i = 0; i < eoagregats.count(); i++) {
				EOGestionAgregat ga = (EOGestionAgregat) eoagregats.objectAtIndex(i);
				NSArray gestions = (NSArray) ga.toGestionExercices().valueForKey(EOGestionExercice.GESTION_KEY);
				NSArray res = ZEOUtilities.intersectionOfNSArrayWithGlobalIds(allAuthorizedGestions, gestions);
				//on n'autorise l'agregat que si l'util a tous les droits sur les codes gestion
				if (res.count() == gestions.count()) {
					agregatsMap.put(ga.gaLibelle(), (NSArray) gestions.valueForKey(EOGestion.GES_CODE_KEY));
				}
			}
		}

	}

	/**
	 * Les différents gesCode regroupés par agrégats.
	 * 
	 * @return En clé le libellé de l'agrégat, en valeur un NSArray contenant les gesCodes affectés à l'agrégat.
	 */
	public LinkedHashMap<String, NSArray> getAgregatsMap() {
		return agregatsMap;
	}

	/**
	 * Renvoie le gescode (pour compte fi) correspondant à l'agregat.
	 * 
	 * @param agregat
	 * @return gesCodeForUB dans le cas ou agregat=UB
	 * @return
	 */
	public String cptefiGetGescodeParamForAgregat(String agregat, String gesCodeForUB) {
		if (AgregatsCtrl.NIVEAU1_AGREGE.equals(agregat)) {
			return STR_GES_CODE_AGREGE;
		}
		else if (agregat.startsWith(AgregatsCtrl.NIVEAU2_SACD_PREFIX)) {
			return STR_GES_CODE_SACD + (String) getAgregatsMap().get(agregat).objectAtIndex(0);
		}
		else if (AgregatsCtrl.NIVEAU2_ETABLISSEMENT.equals(agregat)) {
			return STR_GES_CODE_ETAB;
		}
		else if (AgregatsCtrl.NIVEAU3_UB.equals(agregat)) {
			return gesCodeForUB;
		}
		else {
			return agregat;
		}
	}

	/**
	 * Remplit le dictionnaire de parametres des procedures stockées de compte fi en fonction de l'agregat choisi
	 * 
	 * @param agregat
	 * @param paramsDict
	 */
	public void cptefiBuildStoredProcParams(String agregat, String gesCodeForUB, NSMutableDictionary paramsDict) {
		paramsDict.takeValueForKey(cptefiGetGescodeParamForAgregat(agregat, gesCodeForUB), "15_gescode");
	}

	/**
	 * @param agregat
	 * @param restriction Valeur du ges_code à utiliser (par exemple dans le cas d'une UB)
	 * @param prefix prefix à utiliser pour placer devant attributeName (par exemple "gestion.")
	 * @param attributeName Nom de l'attribut. Si null, EOGestion.GES_CODE_COLKEY est utilisé
	 * @return
	 * @throws Exception
	 */
	public String buildSqlConditionForAgregat(String agregat, String restriction, String prefix, String attributeName) throws Exception {
		StringWriter sw = new StringWriter();
		sw.write("");
		if (NIVEAU1_AGREGE.equals(agregat)) {
			//on ne fait rien l'ensemble de l'établissement est récupéré
		}
		else if (NIVEAU3_UB.equals(agregat)) {
			if (ZStringUtil.isEmpty(restriction)) {
				throw new Exception("buildSqlConditionForAgregat : restriction par UB obligatoire");
			}
			sw.write(buildSqlCondition(prefix, attributeName, new NSArray(new Object[] {
					restriction
			})));
		}
		else {
			if (getAgregatsMap().get(agregat) == null) {
				throw new Exception("Aucun code gestion n'est autorisé ou spécifié pour le regroupement : " + agregat);
			}
			sw.write(buildSqlCondition(prefix, attributeName, getAgregatsMap().get(agregat)));
		}
		String res = sw.toString();
		if (res.length() > 0) {
			res = " (" + res + ")";
		}
		return res;
	}

	private String buildSqlCondition(String prefix, String attributeName, NSArray gescodes) {
		StringWriter sw = new StringWriter();
		Enumeration enumGesCodes = gescodes.objectEnumerator();

		boolean start = true;
		while (enumGesCodes.hasMoreElements()) {
			if (start) {
				start = false;
			}
			else {
				sw.write(_OR);
			}
			String gescode = (String) enumGesCodes.nextElement();
			sw.write(ZStringUtil.ifNull(prefix).concat((attributeName == null ? EOGestion.GES_CODE_COLKEY : attributeName)).concat(_EQUALS).concat(_QUOTE).concat(gescode).concat(_QUOTE));
		}
		return sw.toString();
	}

	public NSArray getAllAuthorizedGestions() {
		return allAuthorizedGestions;
	}

}
