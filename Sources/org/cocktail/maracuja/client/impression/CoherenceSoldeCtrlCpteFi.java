package org.cocktail.maracuja.client.impression;

import com.webobjects.eocontrol.EOEditingContext;

public class CoherenceSoldeCtrlCpteFi extends CoherenceSoldeCtrl {
	private AgregatsCtrl _agregatCtrl;

	public CoherenceSoldeCtrlCpteFi(EOEditingContext editingContext) throws Exception {
		super(editingContext);
	}

	@Override
	public AgregatsCtrl getAgregatCtrl() {
		try {
			if (_agregatCtrl == null) {
				_agregatCtrl = new AgregatsCtrl(getActionId(), true, true, true, false, false);
			}
			return _agregatCtrl;
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}
		return null;

	}

	protected String getActionId() {
		return null;
	}
}
