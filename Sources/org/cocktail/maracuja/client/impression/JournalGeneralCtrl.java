/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class JournalGeneralCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression du journal";
	private final Dimension WINDOW_SIZE = new Dimension(550, 260);
	protected JournalGeneralPanel myPanel;
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR001;
	private ActionImprimerXls actionImprimerXls = new ActionImprimerXls();
	protected DefaultComboBoxModel comboBoxModeleModel;
	protected DefaultComboBoxModel comboBoxModeleModel2;
	//public static final String MDL_GENERAL2 = "Journal général";
	public static final String MDL_GENERAL = "Journal général";
	//    public static final String JOURNAL_EXERCICE = "Journal Exercice";
	public static final String MDL_CORRECTIONS = "Journal des annulations/rectifications";
	public static final String MDL_GRAND_LIVRE_VALEURS_INACTIVES = "Journal Grand livre des valeurs inactives";
	public static final String MDL_BE = "Journal Balance d'entrée";
	public static final String JASPERFILENAME_JOURNAL_GENERAL = "journalGV_sql.jasper";
	public static final String JASPERFILENAME_JOURNAL_GENERAL2 = "journalGV2_sql.jasper";
	public static final String JXLSFILENAME_JOURNAL_GENERAL = "journal.xls";

	public static final String MDL2_ANCIEN = "Avec libellés de comptes";
	public static final String MDL2_NOUVEAU = "Avec décalage débit/crédit";

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public JournalGeneralCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			//if (gestions.count() + gestionsSacd.count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}

		comboBoxModeleModel = new DefaultComboBoxModel();
		comboBoxModeleModel.addElement(MDL_GENERAL);
		comboBoxModeleModel.addElement(MDL_CORRECTIONS);
		comboBoxModeleModel.addElement(MDL_BE);
		comboBoxModeleModel.addElement(MDL_GRAND_LIVRE_VALEURS_INACTIVES);

		comboBoxModeleModel2 = new DefaultComboBoxModel();
		comboBoxModeleModel2.addElement(MDL2_NOUVEAU);
		comboBoxModeleModel2.addElement(MDL2_ANCIEN);

		//initTypeOperationsModel();
		myPanel = new JournalGeneralPanel(new JournalGeneralPanelListener());
	}

	protected void imprimer() {
		try {
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerJournal(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), FORMATPDF);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	//
	//
	private void imprimerXls() {
		try {
			//checkFilters();
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String req = buildSql(getEditingContext(), dico, myFilters);
			//myApp.temporaryDir, dico, myFilters, getMyDialog()
			String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, JXLSFILENAME_JOURNAL_GENERAL, "journal.xls", req, getMyDialog(), null);

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.DATE_DEBUT_FILTER_KEY, ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		//myFilters.put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, NIVEAU2_ETAB_SACD);
	}

	public void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final class ActionImprimerXls extends AbstractAction {

		public ActionImprimerXls() {
			super("Excel");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Impresssion au format Excel (en test)");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls();
		}

	}

	protected class JournalGeneralPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements JournalGeneralPanel.IJournalGeneralPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#actionImprimerXls()
		 */
		public Action actionImprimerXls() {
			return actionImprimerXls;
		}

		public DefaultComboBoxModel comboModeleModel() {
			return comboBoxModeleModel;
		}

		public DefaultComboBoxModel comboModeleModel2() {
			return comboBoxModeleModel2;
		}

	}

	/**
	 * @param ec
	 * @param temporaryDir
	 * @param parametres
	 * @param paramRequete Map contenant les paramètres de la requete à effectuer. Les parametres sont dateDebut, dateFin, gesCodeSACD
	 * @return
	 * @throws Exception
	 */
	public String imprimerJournal(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, Window win, final int format) throws Exception {
		final String mdl = (String) comboBoxModeleModel2.getSelectedItem();
		String req = buildSql(ec, parametres, paramRequete);

		String file = JASPERFILENAME_JOURNAL_GENERAL;
		if (JournalGeneralCtrl.MDL2_NOUVEAU.equals(mdl)) {
			file = JASPERFILENAME_JOURNAL_GENERAL2;
		}

		return imprimerReportByThread(ec, temporaryDir, parametres, file, getFileNameWithExtension("journalG", format), req, win, format, null);
	}

	protected String buildSql(EOEditingContext ec, NSMutableDictionary parametres, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		final String typeJournal = (String) comboBoxModeleModel.getSelectedItem();
		final String mdl = (String) comboBoxModeleModel2.getSelectedItem();

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);

		String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		//String condsacd = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condTypeJournal = "";
		if (JournalGeneralCtrl.MDL_GENERAL.equals(mdl)) {
			condTypeJournal = "";
		}
		else if (JournalGeneralCtrl.MDL_CORRECTIONS.equals(typeJournal)) {
			condTypeJournal = " ecriture.ecr_ordre in (select distinct ecr_ordre from maracuja.ecriture_detail where ecd_montant<0 and exe_ordre=" + exercice.exeExercice().intValue() + ")";
		}
		else if (JournalGeneralCtrl.MDL_BE.equals(typeJournal)) {
			condTypeJournal = " ecriture.tjo_ordre in (select tjo_ordre from maracuja.type_journal where tjo_libelle = 'JOURNAL BALANCE ENTREE')";
		}
		else if (JournalGeneralCtrl.MDL_GRAND_LIVRE_VALEURS_INACTIVES.equals(typeJournal)) {
			condTypeJournal = " ecriture.tjo_ordre in (select tjo_ordre from maracuja.type_journal where tjo_ordre = 18)";
		}

		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);
		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		parametres.takeValueForKey(typeJournal, "TYPE_JOURNAL");
		parametres.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		if (dateDebut != null && dateFin != null) {
			if (dateDebut.equals(dateFin)) {
				parametres.takeValueForKey("Journée du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOURNEES");
			}
			else {
				parametres.takeValueForKey("Journées du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut) + " au " + new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOURNEES");
			}
		}

		String reqCumuls = "";
		// On fait les requetes pour recupérer les cumuls precedents
		reqCumuls = "select nvl(sum(nvl(ecriture_detail.ecd_debit,0)),0) as TOT_DEBIT, " +
				" nvl(sum(nvl(ecriture_detail.ecd_credit,0)),0) as TOT_CREDIT " +
				"from MARACUJA.ecriture, MARACUJA.ecriture_detail " +
				"where " +
				"ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
				"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateDebutStr + "' " +
				"and ecriture.ecr_numero > 0    " +
				"and " + cond +
				" and " + condCompta +
				(condGestion.length() != 0 ? " and " + condGestion : "") +
				(condTypeJournal.length() != 0 ? " and " + condTypeJournal : "");

		ZLogger.debug(reqCumuls);

		NSArray res = ServerProxy.clientSideRequestSqlQuery(ec, reqCumuls);
		if (res == null || res.count() == 0) {
			throw new DefaultClientException("Impossible de calculer les cumuls");
		}
		NSDictionary dicCumul = (NSDictionary) res.objectAtIndex(0);
		BigDecimal cumulDebit = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal cumulCredit = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);

		if (!NSKeyValueCoding.NullValue.equals(dicCumul.valueForKey("TOT_DEBIT"))) {
			cumulDebit = new BigDecimal(((Number) dicCumul.valueForKey("TOT_DEBIT")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		if (!NSKeyValueCoding.NullValue.equals(dicCumul.valueForKey("TOT_CREDIT"))) {
			cumulCredit = new BigDecimal(((Number) dicCumul.valueForKey("TOT_CREDIT")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		}

		parametres.takeValueForKey(cumulDebit, "PREC_DEBIT");
		parametres.takeValueForKey(cumulCredit, "PREC_CREDIT");

		// ZLogger.debug(params);
		String req = "";
		if (JournalGeneralCtrl.MDL2_NOUVEAU.equals(mdl)) {
			req = "select  com_libelle, ecr_numero, ecr_date_saisie as ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, ecriture.exe_ordre, ecriture_detail.pco_num, " +
					"ecriture_detail.ges_code, type_journal.tjo_libelle type_journal " +
					"from MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal " +
					"where " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
					"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " +
					" and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
					"and ecriture.ecr_numero > 0    " +
					"and ecriture.tjo_ordre=type_journal.tjo_ordre " +
					" and " + cond +
					" and " + condCompta +
					(condGestion.length() != 0 ? " and " + condGestion : "") +
					(condTypeJournal.length() != 0 ? " and " + condTypeJournal : "");

		}
		else {
			req = "select  com_libelle, ecr_numero, ecr_date_saisie as ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, ecriture.exe_ordre, ecriture_detail.pco_num, " +
					"maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle, ecriture_detail.ges_code, type_journal.tjo_libelle type_journal " +
					"from MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal " +
					"where " +
					"ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
					"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " +
					" and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
					"and ecriture.ecr_numero > 0    " +
					"and ecriture.tjo_ordre=type_journal.tjo_ordre " +
					" and " + cond +
					" and " + condCompta +
					(condGestion.length() != 0 ? " and " + condGestion : "") +
					(condTypeJournal.length() != 0 ? " and " + condTypeJournal : "");
		}

		req += " order by ecr_numero, ecd_index";
		return req;
	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

}
