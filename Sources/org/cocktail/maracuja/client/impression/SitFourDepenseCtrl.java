/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.FournisseurSrchDialog;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.SitFourDepensePanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class SitFourDepenseCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression de la situation fournisseur";
	private final Dimension WINDOW_SIZE = new Dimension(500, 140);

	private final String ACTION_ID = ZActionCtrl.IDU_IMPR014;

	private static final String JASPERFILENAME_SITDEPFOUR = "sitcptfourdepenses.jasper";

	private final ActionSelectFournis actionSelectFournis = new ActionSelectFournis();

	private SitFourDepensePanel myPanel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public SitFourDepenseCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		//        if (gestions.count()== 0) {
		//            throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		//        }                  
		myPanel = new SitFourDepensePanel(new SitFourDepensePanelListener());

	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);

			if (myFilters.get("fournisseur") == null) {
				throw new DefaultClientException("Vous devez spécifier un fournisseur.");
			}

			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			final String filePath = imprimerSitFourDepense(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected final void initFilters() {
		//  myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		//        myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY,  ZDateUtil.getMinOf2Dates(ZDateUtil.getToday().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue()))  );
	}

	private final void selectFournis() {
		final FournisseurSrchDialog myFournisseurSrchDialog = new FournisseurSrchDialog(getMyDialog(), "Sélection d'un fournisseur", getEditingContext());
		if (myFournisseurSrchDialog.open() == ZEOSelectionDialog.MROK) {
			try {
				System.out.println("ici");
				myFilters.put("fournisseur", (EOFournisseur) myFournisseurSrchDialog.getSelectedEOObject());
				myPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		myFournisseurSrchDialog.dispose();
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final class ActionSelectFournis extends AbstractAction {

		public ActionSelectFournis() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un fournisseur");
		}

		public void actionPerformed(ActionEvent e) {
			selectFournis();
		}

	}

	private final class SitFourDepensePanelListener implements SitFourDepensePanel.ISitFourDepenseListener {

		public Action actionImprimer() {
			return actionImprimer;
		}

		public Action actionSelectFournis() {
			return actionSelectFournis;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	public String imprimerSitFourDepense(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		//final EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final EOFournisseur fournisseur = (EOFournisseur) paramRequete.get("fournisseur");
		final String condExercice = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "m.exe_ordre", new NSArray(exercice));
		final String condFournisseur = buildConditionFromPrimaryKeyAndValues(ec, "fouOrdre", "d.fou_ordre", new NSArray(fournisseur));
		;
		final NSMutableDictionary params = new NSMutableDictionary(parametres);

		String req = "SELECT p.pai_numero,pai_date_creation,d.*, m.man_numero,b.BOR_NUM,exer.exe_exercice exe_paiement, r.*, mp.mod_libelle, mp.mod_dom " +
				"FROM maracuja.mandat m , maracuja.bordereau b, maracuja.depense d, maracuja.paiement p, maracuja.v_rib r, MARACUJA.exercice exer, maracuja.mode_paiement mp " +
				"WHERE b.bor_id = m.bor_id "
				+ "AND m.man_id = d.man_id " +
				"and r.rib_ordre = d.dep_rib " +
				" and m.mod_ordre=mp.mod_ordre " +
				" and (m.man_etat='VISE' or m.man_etat='PAYE') " + "and " + condExercice + " " + "and " + condFournisseur + "AND m.pai_ordre = p.pai_ordre (+) " + "and p.exe_ordre=exer.exe_ordre (+) "
				+ "order by dep_fournisseur,p.exe_ordre,pai_numero";

		return imprimerReportByThreadPdf(ec, temporaryDir, params, JASPERFILENAME_SITDEPFOUR, JASPERFILENAME_SITDEPFOUR + ".pdf", req, win, null);

	}

}
