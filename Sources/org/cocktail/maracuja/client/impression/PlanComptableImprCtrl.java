/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.impression.ui.PlanComptableImprPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PlanComptableImprCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression du plan comptable";
	private final Dimension WINDOW_SIZE = new Dimension(609, 190);

	private final String ACTION_ID = ZActionCtrl.IDU_IMPR007;
	private final PlanComptableImprPanel myPanel;

	private final static String J_MODELE_05 = "plancomptable_05.jasper";
	private final static String J_MODELE_10 = "plancomptable_10.jasper";
	private final static String J_MODELE_15 = "plancomptable_15.jasper";
	private final static String J_MODELE_20 = "planco_credits.jasper";
	private static final String JXLSFILENAME_PLAN_COMPTABLE = "plancomptable.xls";

	private final String MODELE_05 = "Présenté par classe/chapitre";
	private final String MODELE_10 = "Tableau";
	private final String MODELE_15 = "Comparatif par rapport au plan comptable de référence";
	private final String MODELE_20 = "Liste des comptes par type de crédit";

	private final DefaultComboBoxModel modeleModel;
	private final Map modelesMap = new HashMap(2);

	private final ActionImprimerXls actionImprimerXls = new ActionImprimerXls();
	protected final NSMutableDictionary dico;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public PlanComptableImprCtrl(EOEditingContext editingContext, final EOExercice exer) throws Exception {
		super(editingContext);
		modeleModel = new DefaultComboBoxModel();
		modeleModel.addElement(MODELE_05);
		modeleModel.addElement(MODELE_10);
		modeleModel.addElement(MODELE_20);
		modeleModel.addElement(MODELE_15);

		modelesMap.put(MODELE_05, J_MODELE_05);
		modelesMap.put(MODELE_10, J_MODELE_10);
		modelesMap.put(MODELE_20, J_MODELE_20);
		modelesMap.put(MODELE_15, J_MODELE_15);

		myPanel = new PlanComptableImprPanel(new PlanComptablePanelListener());

		dico = EOsFinder.getParametresDico(_editingContext, exer);
		dico.takeValueForKey(exer.exeExercice(), "EXE_ORDRE");
	}

	/**
	 * Crée la requete sql d'apres le modele choisi.
	 * 
	 * @param mdl
	 * @return
	 * @throws Exception
	 */
	private final String prepareSql(final String mdl) throws Exception {
		final String condCompte = buildCriteresPlanco("p.pco_num", (String) myFilters.get(PlanComptableImprPanel.PLANCO_KEY));
		final String condValide = (String) (myFilters.get(EOPlanComptable.PCO_VALIDITE_KEY) != null ? myFilters.get(EOPlanComptable.PCO_VALIDITE_KEY) : null);

		String cond = "";
		cond = cond + (condCompte != null ? condCompte : "");
		cond = cond + (condValide != null ? (cond.length() > 0 ? " and " : "") + " p.pco_validite='" + condValide + "'" : "");

		final String req;
		if (J_MODELE_10.equals(mdl)) {
			//			req = "select * from plan_comptable p " + (cond.length() > 0 ? " where " + cond : "") + " order by p.pco_num";
			req = "select P.PCOE_ID, P.EXE_ORDRE, P.PCO_NUM,    P.PCO_NIVEAU, P.PCO_BUDGETAIRE, P.PCO_EMARGEMENT,   P.PCO_LIBELLE, P.PCO_NATURE, P.PCO_VALIDITE, P.PCO_J_EXERCICE, P.PCO_J_FIN_EXERCICE," +
					" P.PCO_J_BE, nvl(P.PCO_COMPTE_BE, ' ') pco_compte_be, P.PCO_SENS_SOLDE " +
					"from MARACUJA.plan_comptable_exer p " +
					" where p.exe_ordre=" + getExercice().exeExercice() + " " + (cond.length() > 0 ? " and " + cond : "") + " order by p.pco_num";
		}
		else if (J_MODELE_05.equals(mdl)) {
			req = "SELECT c.pco_classe, p1.pco_libelle AS pco_classe_lib,  c.pco_chapitre, p2.pco_libelle AS pco_chapitre_lib, " +
					"p.exe_ordre, p.PCO_NUM, p.PCO_LIBELLE, p.PCO_NIVEAU, p.PCO_BUDGETAIRE, p.PCO_EMARGEMENT,   "
					+ "p.PCO_NATURE, p.PCO_VALIDITE, p.PCO_J_EXERCICE,   p.PCO_J_FIN_EXERCICE, " +
					"p.PCO_J_BE, p.PCO_COMPTE_BE,   p.PCO_SENS_SOLDE "
					+ "FROM maracuja.PLAN_COMPTABLE_EXER p, maracuja.v_planco_chapitre c, maracuja.PLAN_COMPTABLE p1, maracuja.PLAN_COMPTABLE p2 " +
					"WHERE p.pco_num=c.pco_num AND c.pco_classe=p1.pco_num AND c.pco_chapitre=p2.pco_num AND p.pco_num <> p1.pco_num "
					+ "AND p.pco_num<>p2.pco_num "
					+ " and p.exe_ordre=" + getExercice().exeExercice() + " " + (cond.length() > 0 ? " and " + cond : "")
					+ "ORDER BY c.pco_classe,  c.pco_chapitre, p.pco_num";
		}
		else if (J_MODELE_15.equals(mdl)) {
			req = "select p.*, decode(pco_num,null,ref_pco_num,pco_num) pco_num_tri  from (" +
					"     SELECT   p.exe_ordre, p.pco_num, p.pco_libelle," +
					"              p.pco_niveau, p.pco_budgetaire, p.pco_emargement, p.pco_nature," +
					"              p.pco_validite, p.pco_j_exercice," +
					"              p.pco_j_fin_exercice, p.pco_j_be, p.pco_compte_be, p.pco_sens_solde, pco_num_nouveau, " +
					"              r.ref_pco_num, r.ref_pco_libelle,r.REF_PCO_NUM_OLD,r.ref_pco_import_id," +
					"              r.REF_PCO_NOMENCLATURE_REF, r.REF_PCO_REMARQUES, r.REF_PCO_SUBDIVISABLE, r.REF_PCO_UTILISATION, r.REF_PCO_BUDGETAIRE," +
					" 		grhum.Chaine_Sans_Accents_simple(p.pco_libelle) pco_libelle_clean," +
					" 		grhum.Chaine_Sans_Accents_simple(r.ref_pco_libelle) ref_pco_libelle_clean" +
					"     from (SELECT p.*, y.ref_pco_num as pco_num_nouveau " +
					"			FROM maracuja.plan_comptable_exer p," +
					"                    (SELECT ref_pco_num, ref_pco_num_old" +
					"                    FROM maracuja.plan_comptable_ref_ext" +
					"                   WHERE TO_DATE ('01/01/' || " + getExercice().exeExercice() + ", 'dd/mm/yyyy') >= ref_pco_date_debut" +
					"                     AND (   TO_DATE ('01/01/' || (" + getExercice().exeExercice() + " + 1), 'dd/mm/yyyy') < ref_pco_date_fin" +
					"                          OR ref_pco_date_fin IS NULL" +
					"                         )" +
					"                   ) y" +
					"                   WHERE p.exe_ordre = " + getExercice().exeExercice() + "" +
					"                   and p.pco_num=y.ref_pco_num_old(+)" +
					"      ) p" +
					"      full outer join" +
					"       (select * from maracuja.plan_comptable_ref_ext where  to_date('01/01/' || " + getExercice().exeExercice() + ",'dd/mm/yyyy') >= ref_pco_date_debut" +
					"             and (to_date('01/01/'|| (" + getExercice().exeExercice() + "+1),'dd/mm/yyyy') <  ref_pco_date_fin or ref_pco_date_fin is null ) ) r" +
					"     on  r.ref_pco_num=p.pco_num ) p " +
					" order by pco_num_tri";
		}
		else {
			req = "select JEFY_ADMIN.API_JASPER_PARAM.get_param_ADMIN('UNIV', tc.exe_ordre) UNIV,p.Exe_ordre,tc.tcd_ordre,  "
					+ "tcd_type, tc.tcd_code, tc.tcd_libelle, substr(p.pco_num, 1, 1) as pco_classe, "
					+ "p.pco_num, p.pco_libelle "
					+ "from maracuja.plan_comptable_EXER p, maracuja.planco_credit pcc, jefy_admin.type_credit tc "
					+ "where p.pco_num=pcc.pco_num and pcc.tcd_ordre=tc.tcd_ordre and "
					+ " substr(pcc.pcc_etat,1,1)='V' and "
					+ " tc.exe_ordre=" + getExercice().exeExercice() + " " + " and p.exe_ordre="
					+ getExercice().exeExercice() + " " + (cond.length() > 0 ? " and " + cond : "") + " order by exe_ordre, tcd_type, tc.tcd_code, tc.tcd_libelle, p.pco_num  ";
		}
		return req;
	}

	protected final void imprimer() {
		try {
			final String selected = (String) modeleModel.getSelectedItem();
			final String mdl = (String) modelesMap.get(selected);

			final String req = prepareSql(mdl);
			ZLogger.debug("sql", req);
			final String f = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATPDF), req, getMyDialog(), null);

			if (f != null) {
				myApp.openPdfFile(f);
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void imprimerXls() {
		try {
			final String mdl = JXLSFILENAME_PLAN_COMPTABLE;
			final String req = prepareSql(J_MODELE_10);
			ZLogger.debug("sql", req);
			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());

			final String filePath = imprimerReportByThreadJXls(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATXLS), req, getMyDialog(), null);
			//	final String filePath = imprimerReportByThreadXls(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATXLS), req, getMyDialog(), null);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected final void initFilters() {
		myFilters.clear();
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	//	public final void resetFilter() {
	//		myFilters.clear();
	//	}
	//
	//	public final class ActionClose extends AbstractAction {
	//
	//		public ActionClose() {
	//			super("Fermer");
	//			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			fermer();
	//		}
	//
	//	}

	public final class ActionImprimerXls extends AbstractAction {

		public ActionImprimerXls() {
			super("Excel");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Impresssion au format Excel");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerXls();
		}

	}

	private final class PlanComptablePanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements PlanComptableImprPanel.IPlanComptableImprPanelListener {

		//		public Action actionImprimer() {
		//			return actionImprimer;
		//		}
		//
		//		/**
		//		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#actionClose()
		//		 */
		//		public Action actionClose() {
		//			return actionClose;
		//		}
		//
		//		/**
		//		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#getFilters()
		//		 */
		//		public HashMap getFilters() {
		//			return myFilters;
		//		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#actionImprimerXls()
		 */
		public Action actionImprimerXls() {
			return actionImprimerXls;
		}

		//        public IZLookupButtonListener getLookupButtonCompteListener() {
		//            return pcoLookupListener;
		//        }
		//
		//        public IZLookupButtonModel getLookupButtonCompteModel() {
		//            return pcoLookupModel;
		//        }
		public ComboBoxModel getModelesModel() {
			return modeleModel;
		}
	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

}
