/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.cptefi.ctrl.ListeActionGenCtrl;
import org.cocktail.maracuja.client.impression.ui.CompteFiCadre5Panel;
import org.cocktail.maracuja.client.impression.ui.CompteFiGenPanel;
import org.cocktail.maracuja.client.metier.EOBilanType;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Fenêtre de sélection générique pour certaines impressions du compte fi.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CompteFiGenCtrl extends ZKarukeraImprCtrl {
	private final Dimension WINDOW_SIZE = new Dimension(550, 200);
	private CompteFiGenPanel myPanel;

	public final static String CADRE4_ID = "CADRE4";
	public final static String CADRE4_TITLE = "Cadre 4 - Execution du budget";

	public final static String CADRE5_ID = "CADRE5";
	public final static String CADRE5_TITLE = "Cadre 5";

	public final static String SIG_ID = "SIG";
	public final static String SIG_TITLE = "Soldes intermédiaires de gestion";

	public static final String BILAN_ID = "Bilan";
	public final static String BILAN_TITLE = "Bilan";

	public static final String CPTERESULTAT_ID = "cpteresultat";
	public final static String CPTERESULTAT_TITLE = "Compte de résultat";

	public final static String CADRE6_ID = "CADRE6";
	public final static String CADRE6_TITLE = "Cadre 6 - Balance des comptes de valeur inactives";

	public final static String CAF_ID = "CAF";
	public final static String CAF_TITLE = "Détermination de la capacité d'autofinancement";

	public final static String TAB_FINANCEMENT_ID = "TAB_FINANCEMENT";
	public final static String TAB_FINANCEMENT_TITLE = "Tableau de financement global";

	public final static String LISTE_OP_ID = "LISTE_OP";
	public final static String LISTE_OP_TITLE = "Liste des ordres de paiement";

	private static final String JASPERFILENAME_CADRE4 = "cadre4.jasper";
	private static final String JASPERFILENAME_CADRE5 = "cadre5_sql.jasper";
	public static final String JASPERFILENAME_CADRE6 = "cadre6_sql.jasper";
	private static final String JASPERFILENAME_SIG = "comptefi_sig.jasper";
	private static final String JASPERFILENAME_CPTERESULTAT_CHARGES = "cpte_rtat_charges.jasper";
	private static final String JASPERFILENAME_CPTERESULTAT_PRODUITS = "cpte_rtat_produits.jasper";
	private static final String JASPERFILENAME_BILAN_ACTIF = "bilan_actif.jasper";
	private static final String JASPERFILENAME_BILAN_PASSIF = "bilan_passif.jasper";
	private static final String JASPERFILENAME_BILAN_ACTIF_AVT_2013 = "bilan_actif_avt_2013.jasper";
	private static final String JASPERFILENAME_BILAN_PASSIF_AVT_2013 = "bilan_passif_avt_2013.jasper";
	private static final String JASPERFILENAME_CAF_EBE = "caf_ebe.jasper";
	private static final String JASPERFILENAME_CAF_RESULTAT = "caf_resultat.jasper";
	private static final String JASPERFILENAME_FRNG = "frng.jasper";
	private static final String JASPERFILENAME_LISTE_OP = "liste_op.jasper";

	private static final HashMap impressionTitles = new HashMap();

	static {
		impressionTitles.put(CADRE4_ID, CADRE4_TITLE);
		impressionTitles.put(CADRE5_ID, CADRE5_TITLE);
		impressionTitles.put(CADRE6_ID, CADRE6_TITLE);
		impressionTitles.put(SIG_ID, SIG_TITLE);
		impressionTitles.put(CPTERESULTAT_ID, CPTERESULTAT_TITLE);
		impressionTitles.put(BILAN_ID, BILAN_TITLE);
		impressionTitles.put(CAF_ID, CAF_TITLE);
		impressionTitles.put(TAB_FINANCEMENT_ID, TAB_FINANCEMENT_TITLE);
		impressionTitles.put(LISTE_OP_ID, LISTE_OP_TITLE);
	};
	private String currentImpression = null;
	private boolean _showDateField;
	private AgregatsCtrl _agregatCtrl;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public CompteFiGenCtrl(EOEditingContext editingContext, final String _currentImpression, final boolean showDateField) throws Exception {
		//		super(editingContext, EOsFinder.getAllGestionsPourExercice(editingContext, myApp.appUserInfo().getCurrentExercice()), EOsFinder.getSACDGestion(editingContext, myApp.appUserInfo().getCurrentExercice()));
		//super(editingContext, EOsFinder.getGestionNonSacd(editingContext, myApp.appUserInfo().getCurrentExercice()), EOsFinder.getSACDGestion(editingContext, myApp.appUserInfo().getCurrentExercice()));
		super(editingContext);
		currentImpression = _currentImpression;
		myPanel = new CompteFiGenPanel(new CompteFiGenPanelListener());
		_showDateField = showDateField;

		//niveauModel.removeElement(NIVEAU3_UB);

	}

	@Override
	public AgregatsCtrl getAgregatCtrl() {
		try {
			if (_agregatCtrl == null) {
				_agregatCtrl = new AgregatsCtrl(getActionId(), true, true, true, false, true);
			}
			return _agregatCtrl;
		} catch (Exception e) {
			e.printStackTrace();
			showErrorDialog(e);
		}
		return null;

	}

	public CompteFiGenCtrl(EOEditingContext editingContext, final String _currentImpression, final boolean showDateField, CompteFiGenPanel panel) throws Exception {
		//super(editingContext, EOsFinder.getGestionNonSacd(editingContext, myApp.appUserInfo().getCurrentExercice()), EOsFinder.getSACDGestion(editingContext, myApp.appUserInfo().getCurrentExercice()));
		super(editingContext);
		currentImpression = _currentImpression;
		myPanel = panel;
		_showDateField = showDateField;
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = null;
			if (CADRE4_ID.equals(currentImpression)) {
				filePath = imprimerCadre4(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}
			else if (CADRE5_ID.equals(currentImpression)) {
				filePath = imprimerCadre5(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}
			else if (CADRE6_ID.equals(currentImpression)) {
				filePath = imprimerCadre6(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}
			else if (SIG_ID.equals(currentImpression)) {
				filePath = imprimerSIG(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}
			else if (CPTERESULTAT_ID.equals(currentImpression)) {
				imprimerCPTERESULTAT(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
				//                imprimerCpteResultat(dico);
			}
			else if (BILAN_ID.equals(currentImpression)) {
				EOBilanType bt = null;
				//NSArray res = EOBilanPoste.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOBilanPoste.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, getExercice()), null);
				if (getExercice().exeExercice().intValue() >= 2010) {
					bt = EOBilanType.fetchByKeyValue(getEditingContext(), EOBilanType.BT_STR_ID_KEY, EOBilanType.DEFAULT_STR_ID);
				}
				imprimerBILAN(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), bt);
			}
			else if (CAF_ID.equals(currentImpression)) {
				imprimerCAF(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}
			else if (TAB_FINANCEMENT_ID.equals(currentImpression)) {
				imprimerTAB_FINANCEMENT(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}
			else if (LISTE_OP_ID.equals(currentImpression)) {
				imprimerLISTE_OP(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			}

			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected final void initFilters() {
		//	myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		///	myFilters.put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, NIVEAU2_ETAB_SACD);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(final Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			myPanel.getDateSaisieMaxField().setEnabled(_showDateField);
			initFilters();
			myPanel.updateData();
			//	myFilters.put(GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			//	myFilters.put(GESTION_FILTER_KEY, ((EOGestion) gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public class CompteFiGenPanelListener extends ZKarukeraImprPanelListener implements CompteFiGenPanel.ICompteFiGenPanelListener {

	}

	protected String getActionId() {
		return null;
	}

	public ZKarukeraPanel myPanel() {
		return myPanel;
	}

	public void setMyPanel(CompteFiGenPanel panel) {
		myPanel = panel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return (String) impressionTitles.get(currentImpression);
	}

	private String imprimerCadre4(final EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, ZKarukeraDialog win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		final Date dateFin = (Date) paramRequete.get("dateFin");

		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String niveau = (String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY);
		final String vGescode = getAgregatCtrl().cptefiGetGescodeParamForAgregat(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY));

		//Demander si recalcul
		if (myApp.showConfirmationDialog("Question", "Voulez-vous calculer le cadre 4 : Execution du budget ?\n (Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
			//Lancer la procédure de calcul
			final NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
			getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);

			final Thread t = new Thread() {

				public void run() {
					try {
						ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_EXECUTION_BUDGET", dicoProc);
						waitingDialog.setVisible(false);
					} catch (Exception e) {
						waitingDialog.setVisible(false);
						myApp.showErrorDialog(e);
					}
				};

			};
			t.start();
			waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Calculs en cours...");
			waitingDialog.setModal(true);
			waitingDialog.setVisible(true);

		}
		String req = "select EXBUD_ORDRE, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, EXBUD_LIB_DEP, EXBUD_DEP, EXBUD_LIB_REC, EXBUD_REC" + " from comptefi.EXECUTION_BUDGET ";
		req += " where " + condexer + " and ges_code='" + vGescode + "'" + " order by EXBUD_ORDRE";
		return imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CADRE4, "comptefi_cadre4.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
	}

	private String imprimerCadre5(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, ZKarukeraDialog win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		//String condsacd = "";

		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);

		//Condition a appliquer a toutes les vues
		String condDate = " to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' ";
		String subCondition = condexer + " and " + condDate +
				(condGestion.length() != 0 ? " and " + condGestion : "");

		String subConditionBudg = subCondition + " and substr(pco_num,1,3)<>'186'  and substr(pco_num,1,3)<>'187' and substr(pco_num,1,3)<>'181'";
		String subConditionNonBudg = subCondition;

		//Condition sur les comptes
		String condComptes = "  " + "((substr(pco.pco_num,1,1) in ('1','2','3')       "
				+ "    or pco.pco_num like '481%'        "
				+ "    or pco.pco_num like '50%'     )";

		if (Boolean.FALSE.equals(paramRequete.get(CompteFiCadre5Panel.INCLURE_18))) {
			condComptes = condComptes + " and substr(pco.pco_num,1,3)<>'186'  and substr(pco.pco_num,1,3)<>'187' and substr(pco.pco_num,1,3)<>'181'";
		}

		condComptes = condComptes + ")";
		String req = "  select " + ServerProxy.serverPrimaryKeyForObject(ec, exercice).valueForKey("exeOrdre")
				+ " as exe_ordre, pco.pco_num, pcoe.pco_libelle,         "
				+ "         nvl(man.ecd_montant,0) as man_mont, nvl(orv.ecd_montant,0) as orv_mont,  "
				+ "         nvl(red.ecd_montant,0) as red_mont, nvl(tit.ecd_montant,0) as tit_mont,  "
				+ "         nvl(ecr.ecd_debit,0)  as debit,  nvl(ecr.ecd_credit,0) as credit,    "
				+ "         nvl(man.ecd_montant,0) + nvl(red.ecd_montant,0) + nvl(ecr.ecd_debit,0) as total_debit,   "
				+ "         nvl(orv.ecd_montant,0) + nvl(tit.ecd_montant,0) + nvl(ecr.ecd_credit,0) as total_credit, "
				+ "         nvl(bal.exe_debit,0) as bal_debit, nvl(bal.exe_credit,0) as bal_credit   "
				+ "  from maracuja.plan_comptable pco, maracuja.plan_comptable_exer pcoe,      "
				+ "       (select pco_num, nvl(sum(ecd_montant),0) as ecd_montant from COMPTEFI.V_ECR_MANDATS_DEBITS where "
				+ subConditionBudg
				+ " group by pco_num) man, "
				+ "       (select pco_num, nvl(sum(ecd_montant),0) as ecd_montant from COMPTEFI.V_ECR_OR_CREDITS  where "
				+ subConditionBudg
				+ " group by pco_num) orv,    " + "       (select pco_num, nvl(sum(ecd_montant),0) as ecd_montant from COMPTEFI.V_ECR_REDUCTIONS_DEBITS where  "
				+ subConditionBudg + " group by pco_num) red,  "
				+ "       (select pco_num, nvl(sum(ecd_montant),0) as ecd_montant from COMPTEFI.V_ECR_TITRES_CREDITS where  "
				+ subConditionBudg
				+ "  group by pco_num) tit, "
				+ "       (select pco_num, nvl(sum(ecd_debit),0) as ecd_debit, nvl(sum(ecd_credit),0) as ecd_credit from " +
				" (SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num, " +
				"      ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle" +
				"  FROM maracuja.ecriture_detail ed," +
				"       maracuja.ecriture e," +
				"       maracuja.mandat_detail_ecriture mde," +
				"       maracuja.titre_detail_ecriture tde" +
				" WHERE ed.ecd_ordre = mde.ecd_ordre(+)" +
				"   AND ed.ecd_ordre = tde.ecd_ordre(+)" +
				"   AND (((mde.ecd_ordre IS NULL)" +
				"   AND tde.ecd_ordre IS NULL" +
				"  )" +
				"   or (ed.pco_num like '186%' or ed.pco_num like '187%')" +
				"   )" +
				"   AND ed.ecr_ordre = e.ecr_ordre" +
				"   AND SUBSTR (e.ecr_etat, 1, 1) = 'V'   AND e.tjo_ordre <> 2" +
				" UNION ALL" +
				" SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num," +
				"       ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle" +
				"  FROM maracuja.ecriture_detail ed," +
				"       maracuja.ecriture e," +
				"       maracuja.mandat_detail_ecriture mde," +
				"       maracuja.mandat m" +
				" WHERE ed.ecd_ordre = mde.ecd_ordre" +
				"   AND mde.man_id = m.man_id" +
				"   AND ed.ecr_ordre = e.ecr_ordre" +
				"   AND SUBSTR (e.ecr_etat, 1, 1) = 'V'" +
				"   AND e.tjo_ordre <> 2" +
				"   AND m.pco_num <> ed.pco_num AND ed.pco_num<>'18'||m.pco_num " +
				" UNION ALL " +
				" SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num," +
				"       ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle" +
				"  FROM maracuja.ecriture_detail ed," +
				"       maracuja.ecriture e," +
				"       maracuja.titre_detail_ecriture mde," +
				"	   maracuja.titre m" +
				" WHERE ed.ecd_ordre = mde.ecd_ordre" +
				"   AND mde.tit_id = m.tit_id" +
				"   AND ed.ecr_ordre = e.ecr_ordre" +
				"   AND SUBSTR (e.ecr_etat, 1, 1) = 'V'" +
				"  AND e.tjo_ordre <> 2" +
				"   AND m.pco_num <> ed.pco_num AND ed.pco_num<>'18'||m.pco_num )" +
				" " +
				"where  "
				+ subConditionNonBudg + "  group by pco_num) ecr, "
				+ "       (select pco_num, nvl(sum(exe_debit),0) as exe_debit, nvl(sum(exe_credit),0) as exe_credit from ("
				+ "            select e.exe_ordre, ges_code, ecd.pco_num, sum(ecd_debit) as exe_debit, sum(ecd_credit) as exe_credit "
				+ "            FROM maracuja.ecriture_detail ecd, "
				+ "             maracuja.ecriture e " + "             where "
				+ condDate
				+ " and e.exe_ordre=" + ServerProxy.serverPrimaryKeyForObject(ec, exercice).valueForKey("exeOrdre")
				+ " and e.ecr_ordre = ecd.ecr_ordre "
				+ "                  and e.tjo_ordre<>2 "
				+ "              and substr(e.ecr_etat,1,1)='V' "
				+ " group by e.exe_ordre, ges_code, ecd.pco_num " + "         ) "
				+ (condGestion.length() != 0 ? " where " + condGestion : "")
				+ "  group by pco_num) bal  "
				+ "  where       "
				+ condComptes
				+ "  and pco.pco_num=pcoe.pco_num and pcoe.exe_ordre=" + ServerProxy.serverPrimaryKeyForObject(ec, exercice).valueForKey("exeOrdre")
				+ "  and pco.pco_num = ecr.pco_num(+)        "
				+ "  and pco.pco_num = man.pco_num(+)        "
				+ "  and pco.pco_num = orv.pco_num(+)        "
				+ "  and pco.pco_num = red.pco_num(+)        "
				+ "  and pco.pco_num = tit.pco_num(+)        "
				+ "  and pco.pco_num = bal.pco_num(+)        "
				+ "  and (man.ecd_montant <>0 or         "
				+ "      orv.ecd_montant <>0 or  "
				+ "      red.ecd_montant <>0 or      "
				+ "      tit.ecd_montant <>0 or      "
				+ "      ecr.ecd_debit <>0 or    "
				+ "      ecr.ecd_credit <>0 or   "
				+ "      bal.exe_debit <>0 or    "
				+ "      bal.exe_credit <>0  "
				+ "  )       "
				+ " order by pco.pco_num";

		return imprimerReportByThreadPdf(ec, temporaryDir, params, JASPERFILENAME_CADRE5, "cadre5.pdf", req, win, null);
	}

	private String imprimerCadre6(EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, ZKarukeraDialog win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);

		//Condition a appliquer a toutes les vues
		String condDate = " to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' ";


		//Condition sur les comptes
		final String condComptes = "  pco_num like '86%'  ";

		String req = "select exe_ordre, ges_code, pco_num, maracuja.api_planco.get_pco_libelle(pco_num, exe_ordre) as pco_libelle, debit_be, debit_exe, debit_total, " +
				"      credit_be, credit_exe, credit_total,  "
				+ "      (case when abs(debit_total)>=abs(credit_total) then debit_total-credit_total else 0 end) as debit_solde ,  " +
				"      (case when abs(credit_total)>abs(debit_total) then credit_total-debit_total else 0 end) as credit_solde   " + "from " + "( "
				+ "select exe_ordre, ges_code, pco_num, sum(debit_be) debit_be, sum(debit_exe) debit_exe, sum(debit_be)+sum(debit_exe) debit_total, " +
				"      sum(credit_be) credit_be, sum(credit_exe) credit_exe, sum(credit_be)+sum(credit_exe) credit_total " + "from (     "
				+ "   select e.exe_ordre, ecd.ges_code, pco_num, 0 as debit_be, sum(ecd_debit) debit_exe, 0 as credit_be, sum(ecd_credit) credit_exe   " +
				"   from MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e  " +
				"   where ecd.ecr_ordre=e.ecr_ordre " + "   and substr(e.ecr_etat,1,1)='V' "
				+ "   and e.tjo_ordre=18   " +
				" and " + condDate + "   group by e.exe_ordre, ecd.ges_code, pco_num " +
				"union   " +
				"   select e.exe_ordre, ecd.ges_code, pco_num, sum(ecd_debit) as debit_be, 0 debit_exe, sum(ecd_credit) as credit_be, 0 credit_exe   "
				+ "   from MARACUJA.ecriture_detail ecd, MARACUJA.ecriture e  " +
				"   where ecd.ecr_ordre=e.ecr_ordre " +
				"   and substr(e.ecr_etat,1,1)='V' " +
				"   and e.tjo_ordre=2 " +
				" and " + condDate +
				"   group by e.exe_ordre, ecd.ges_code, pco_num " + ") x " +
				" where " + condexer +
				" and " + condComptes
				+ (condGestion.length() != 0 ? " and " + condGestion : "") +
				"group by exe_ordre, ges_code, pco_num " + ")";

		return imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CADRE6, "cadre6.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
	}

	private String imprimerSIG(final EOEditingContext ec, String temporaryDir, NSMutableDictionary parametres, HashMap paramRequete, ZKarukeraDialog win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		//final EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		//final EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		//      final String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e.com_ordre", new NSArray(comptabilite));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		//final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		//		
		//		if (gesCodeSACD != null) {
		//			params.takeValueForKey(gesCodeSACD.gesCode(), "SACD");
		//			condsacd = " ges_code = '" + gesCodeSACD.gesCode() + "'";
		//		}
		//		else {
		//			// Récupérer tous les SACDs
		//			final NSArray sacds = EOsFinder.getSACDGestionForComptabilite(ec, comptabilite, exercice);
		//			// on les exclue de la requete
		//			for (int i = 0; i < sacds.count(); i++) {
		//				final EOGestion element = (EOGestion) sacds.objectAtIndex(i);
		//				if (condsacd.length() > 0) {
		//					condsacd = condsacd + "and";
		//				}
		//				condsacd = condsacd + " ges_code <> '" + element.gesCode() + "' ";
		//			}
		//		}

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String niveau = (String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY);
		final String vGescode = getAgregatCtrl().cptefiGetGescodeParamForAgregat(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY));
		//final String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);

		//Condition a appliquer a toutes les vues
		//String condDate = " to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' ";
		//		String subCondition = condexer + " and " + condDate;
		//		if (condsacd.length() > 0) {
		//			subCondition += " and " + condsacd;
		//		}

		//	final String niveau = (String) paramRequete.get(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY);
		//Demander si recalcul
		if (myApp.showConfirmationDialog("Question", "Voulez-vous recalculer les soldes intermédiaires de gestion ?\n (Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
			//Lancer la procédure de calcul
			final NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");

			//			if (gesCodeSACD != null) {
			//				dicoProc.takeValueForKey(gesCodeSACD.gesCode(), "15_gescode");
			//				dicoProc.takeValueForKey("O", "20_sacd");
			//			}
			//			else {
			//				dicoProc.takeValueForKey("ETAB", "15_gescode");
			//				dicoProc.takeValueForKey("N", "20_sacd");
			//			}
			getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);
			//
			//			if (ZKarukeraImprCtrl.NIVEAU1_AGREGE.equals(niveau)) {
			//				dicoProc.takeValueForKey("AGREGE", "15_gescode");
			//				dicoProc.takeValueForKey("G", "20_sacd");
			//			}
			//			else if (gesCodeSACD != null) {
			//				dicoProc.takeValueForKey(gesCodeSACD.gesCode(), "15_gescode");
			//				dicoProc.takeValueForKey("O", "20_sacd");
			//			}
			//			else {
			//				dicoProc.takeValueForKey("ETAB", "15_gescode");
			//				dicoProc.takeValueForKey("N", "20_sacd");
			//			}

			final Thread t = new Thread() {

				public void run() {
					try {
						ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_SIG", dicoProc);
						waitingDialog.setVisible(false);
					} catch (Exception e) {
						waitingDialog.setVisible(false);
						myApp.showErrorDialog(e);
					}
				};

			};
			t.start();
			waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Calculs en cours...");
			waitingDialog.setModal(true);
			waitingDialog.setVisible(true);

		}

		//		final String vGescode;
		//		if (ZKarukeraImprCtrl.NIVEAU1_AGREGE.equals(niveau)) {
		//			vGescode = "AGREGE";
		//		}
		//		else if (gesCodeSACD == null) {
		//			vGescode = "ETAB";
		//		}
		//		else {
		//			vGescode = gesCodeSACD.gesCode();
		//		}
		//
		//		if (ZKarukeraImprCtrl.NIVEAU1_AGREGE.equals(niveau)) {
		//			params.takeValueForKey(" AGREGE (Etablissement + SACDs)", "SACD");
		//		}
		//		else if (gesCodeSACD != null) {
		//			params.takeValueForKey(" SACD " + gesCodeSACD.gesCode(), "SACD");
		//		}

		String req = "SELECT SIG_ID, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, SIG_LIBELLE, SUM(PRODUIT) PRODUIT, SUM(CHARGE) CHARGE,SUM(PRODUIT_ANT) PRODUIT_ANT, SUM(CHARGE_ANT) CHARGE_ANT " + "FROM ("
				+ "SELECT SIG_ID, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, SIG_LIBELLE, SIG_MONTANT PRODUIT, 0 CHARGE,0 PRODUIT_ANT, 0 CHARGE_ANT " + "FROM (SELECT * FROM comptefi.SIG) " + "WHERE GROUPE2='produits' " + "and ges_code = '"
				+ vGescode
				+ "' "
				+ "and "
				+ condexer
				+ " "
				+ " union all "
				+ " SELECT SIG_ID, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, SIG_LIBELLE, 0 PRODUIT, 0 CHARGE, SIG_MONTANT_ANT PRODUIT_ANT, 0 CHARGE_ANT "
				+ "FROM (SELECT * FROM comptefi.SIG) "
				+ "WHERE GROUPE_ANT ='produits' "
				+ "and ges_code = '"
				+ vGescode
				+ "' "
				+ "and "
				+ condexer
				+ " "
				+ " union all "
				+ "SELECT SIG_ID, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, SIG_LIBELLE, 0 PRODUIT, SIG_MONTANT CHARGE,    0 PRODUIT_ANTERIEUR, 0 CHARGE_ANT   "
				+ "FROM (SELECT * FROM comptefi.SIG) "
				+ "WHERE GROUPE2='charges' "
				+ "and ges_code = '"
				+ vGescode
				+ "' "
				+ "and "
				+ condexer
				+ " "
				+ " union all "
				+ " SELECT SIG_ID, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, SIG_LIBELLE, 0 PRODUIT, 0 CHARGE,    0 PRODUIT_ANT, SIG_MONTANT_ANT CHARGE_ANT  "
				+ "FROM (SELECT * FROM comptefi.SIG) WHERE GROUPE_ANT='charges' " + "and ges_code = '" + vGescode + "' " + "and " + condexer + " " + ") " + "GROUP BY SIG_ID, EXE_ORDRE, GES_CODE, GROUPE1, GROUPE2, SIG_LIBELLE " + "ORDER BY sig_id";

		return imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_SIG, "comptefi_sig.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
	}

	private String imprimerCPTERESULTAT(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		//final EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		//final EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		//	final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		//      final String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e.com_ordre", new NSArray(comptabilite));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		//	final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String niveau = (String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY);
		final String vGescode = getAgregatCtrl().cptefiGetGescodeParamForAgregat(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY));
		//final String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);

		//		String condDate = " to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' ";

		//Demander si recalcul
		if (myApp.showConfirmationDialog("Question", "Voulez-vous recalculer le compte de resultat ?\n L'opération peut être longue.\n (Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
			//Lancer la procédure de calcul
			final NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
			getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);

			final Thread t = new Thread() {

				public void run() {
					try {
						ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_CPTE_RTAT", dicoProc);
						waitingDialog.setVisible(false);
					} catch (Exception e) {
						waitingDialog.setVisible(false);
						myApp.showErrorDialog(e);
					}
				};

			};
			t.start();
			waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Calculs en cours...");
			waitingDialog.setModal(true);
			waitingDialog.setVisible(true);
		}

		params.takeValueForKeyPath(vGescode, "GES_CODE");
		params.takeValueForKey(exercice.exeExercice(), "EXE_ORDRE");

		final AbstractAction actionProduits = new AbstractAction("Produits") {

			public void actionPerformed(ActionEvent e) {
				try {
					//String req = "select * from comptefi.cpte_rtat_produits " + "where ges_code = '" + vGescode + "' " + "and  " + condexer + " order by crp_ordre, groupe1, groupe2";
					String req = "requete sql dans le report";
					final String res = imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CPTERESULTAT_PRODUITS, "cpte_rtat_produits.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
					if (res != null) {
						myApp.openPdfFile(res);
					}
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}

		};

		final AbstractAction actionCharges = new AbstractAction("Charges") {

			public void actionPerformed(ActionEvent e) {
				try {
					//String req = "select * from comptefi.cpte_rtat_charges " + "where ges_code = '" + vGescode + "' " + "and " + condexer + " order by crc_ordre, groupe1, groupe2";
					String req = "requete sql dans le report";
					final String res = imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CPTERESULTAT_CHARGES, "cpte_rtat_charges.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
					if (res != null) {
						myApp.openPdfFile(res);
					}
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}

		};

		final ArrayList actions = new ArrayList();
		actions.add(actionCharges);
		actions.add(actionProduits);

		ListeActionGenCtrl listeActionGenCtrl = new ListeActionGenCtrl(ec, actions, "Impression du compte de résultat");
		listeActionGenCtrl.openDialog(win);

		return null;
	}

	private String imprimerBILAN(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win, final EOBilanType bt) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String niveau = (String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY);
		final String vGescode = getAgregatCtrl().cptefiGetGescodeParamForAgregat(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY));

		//Demander si recalcul
		if (myApp.showConfirmationDialog("Question", "Voulez-vous recalculer le bilan ?\n L'opération peut être longue.\n(Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
			//Lancer la procédure de calcul
			final NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
			getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);

			if (bt != null) {
				dicoProc.takeValueForKey(ServerProxy.serverPrimaryKeyForObject(ec, bt).valueForKey(EOBilanType.BT_ID_KEY), "05_btId");
			}
			final Thread t = new Thread() {
				public void run() {
					try {
						if (bt != null) {
							ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "api_bilan.prepare_BILAN", dicoProc);
						}
						else {
							ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_BILAN", dicoProc);
						}
						waitingDialog.setVisible(false);
					} catch (Exception e) {
						waitingDialog.setVisible(false);
						myApp.showErrorDialog(e);
					}
				};

			};
			t.start();
			waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Calculs en cours...");
			waitingDialog.setModal(true);
			waitingDialog.setVisible(true);
		}

		params.takeValueForKeyPath(vGescode, "GES_CODE");
		params.takeValueForKey(exercice.exeExercice(), "EXE_ORDRE");

		final AbstractAction actionActif = new AbstractAction("Actif") {

			public void actionPerformed(ActionEvent e) {
				try {
					String jasperFileName = JASPERFILENAME_BILAN_ACTIF;
					if (exercice.exeExercice().intValue() < 2013) {
						jasperFileName = JASPERFILENAME_BILAN_ACTIF_AVT_2013;
					}
					final String res = imprimerReportByThread(ec, temporaryDir, params, jasperFileName, "cpte_bilan_actif.pdf", null, win, FORMATPDF, null, Boolean.TRUE);
					if (res != null) {
						myApp.openPdfFile(res);
					}
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}

		};

		final AbstractAction actionPassif = new AbstractAction("Passif") {

			public void actionPerformed(ActionEvent e) {
				try {
					String jasperFileName = JASPERFILENAME_BILAN_PASSIF;
					if (exercice.exeExercice().intValue() < 2013) {
						jasperFileName = JASPERFILENAME_BILAN_PASSIF_AVT_2013;
					}

					final String res = imprimerReportByThread(ec, temporaryDir, params, jasperFileName, "cpte_bilan_passif.pdf", null, win, FORMATPDF, null, Boolean.TRUE);
					if (res != null) {
						myApp.openPdfFile(res);
					}
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}

		};

		final ArrayList actions = new ArrayList();
		actions.add(actionActif);
		actions.add(actionPassif);

		ListeActionGenCtrl listeActionGenCtrl = new ListeActionGenCtrl(ec, actions, "Impression du bilan");
		listeActionGenCtrl.openDialog(win);

		return null;
	}

	private String imprimerCAF(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String niveau = (String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY);
		final String vGescode = getAgregatCtrl().cptefiGetGescodeParamForAgregat(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY));


		final AbstractAction actionRes = new AbstractAction("A partir du résultat") {

			public void actionPerformed(ActionEvent e) {

				//Demander si recalcul
				if (myApp.showConfirmationDialog("Question", "Voulez-vous recalculer la capacité d'autofinancement à partir du le résultat ?\n L'opération peut être longue.\n(Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
					//Lancer la procédure de calcul
					final NSMutableDictionary dicoProc = new NSMutableDictionary();
					dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
					getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);

					dicoProc.takeValueForKey("N", "30_methodeEBE");

					final Thread t = new Thread() {
						public void run() {
							try {
								ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_DETERMINATION_CAF", dicoProc);
								waitingDialog.setVisible(false);
							} catch (Exception e) {
								waitingDialog.setVisible(false);
								myApp.showErrorDialog(e);
							}
						};

					};
					t.start();
					waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
					waitingDialog.setTitle("Veuillez patienter...");
					waitingDialog.setTopText("Veuillez patienter ...");
					waitingDialog.setBottomText("Calculs en cours...");
					waitingDialog.setModal(true);
					waitingDialog.setVisible(true);
				}

				try {
					String req = "select * from comptefi.caf " + "where ges_code = '" + vGescode + "' " + " and methode_ebe = 'N'" + "and " + condexer + " order by caf_ordre";

					final String res = imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CAF_RESULTAT, "caf_resultat.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
					if (res != null) {
						myApp.openPdfFile(res);
					}
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}

		};

		final AbstractAction actionEbe = new AbstractAction("A partir de l'EBE") {

			public void actionPerformed(ActionEvent e) {

				//Demander si recalcul
				if (myApp.showConfirmationDialog("Question", "Voulez-vous recalculer la capacité d'autofinancement à partir de l'EBE ?\n L'opération peut être longue.\n(Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
					//Lancer la procédure de calcul
					final NSMutableDictionary dicoProc = new NSMutableDictionary();
					dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
					getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);
					//
					//					if (ZKarukeraImprCtrl.NIVEAU1_AGREGE.equals(niveau)) {
					//						dicoProc.takeValueForKey("AGREGE", "15_gescode");
					//						dicoProc.takeValueForKey("G", "20_sacd");
					//					}
					//					else if (gesCodeSACD != null) {
					//						dicoProc.takeValueForKey(gesCodeSACD.gesCode(), "15_gescode");
					//						dicoProc.takeValueForKey("O", "20_sacd");
					//					}
					//					else {
					//						dicoProc.takeValueForKey("ETAB", "15_gescode");
					//						dicoProc.takeValueForKey("N", "20_sacd");
					//					}

					//					if (gesCodeSACD != null) {
					//						dicoProc.takeValueForKey(gesCodeSACD.gesCode(), "15_gescode");
					//						dicoProc.takeValueForKey("O", "20_sacd");
					//					}
					//					else {
					//						dicoProc.takeValueForKey("ETAB", "15_gescode");
					//						dicoProc.takeValueForKey("N", "20_sacd");
					//					}
					dicoProc.takeValueForKey("O", "30_methodeEBE");

					final Thread t = new Thread() {
						public void run() {
							try {
								ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_DETERMINATION_CAF", dicoProc);
								waitingDialog.setVisible(false);
							} catch (Exception e) {
								waitingDialog.setVisible(false);
								myApp.showErrorDialog(e);
							}
						};

					};
					t.start();
					waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
					waitingDialog.setTitle("Veuillez patienter...");
					waitingDialog.setTopText("Veuillez patienter ...");
					waitingDialog.setBottomText("Calculs en cours...");
					waitingDialog.setModal(true);
					waitingDialog.setVisible(true);
				}

				try {
					String req = "select * from comptefi.caf " + "where ges_code = '" + vGescode + "' " + " and methode_ebe = 'O'" + "and " + condexer + " order by caf_ordre";

					final String res = imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_CAF_EBE, "caf_ebe.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
					if (res != null) {
						myApp.openPdfFile(res);
					}
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}

		};

		final ArrayList actions = new ArrayList();
		actions.add(actionEbe);
		actions.add(actionRes);

		ListeActionGenCtrl listeActionGenCtrl = new ListeActionGenCtrl(ec, actions, "Impression de la capacité d'autofinancement");
		listeActionGenCtrl.openDialog(win);

		return null;
	}

	private void imprimerTAB_FINANCEMENT(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condexer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		final String niveau = (String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY);
		final String vGescode = getAgregatCtrl().cptefiGetGescodeParamForAgregat(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY));

		//Demander si recalcul
		if (myApp.showConfirmationDialog("Question", "Voulez-vous recalculer le tableau de financement global ?\n L'opération peut être longue.\n(Si vous répondez Non, l'impression s'effectuera avec les valeurs calculées précédemment) ", ZMsgPanel.BTLABEL_YES)) {
			//Lancer la procédure de calcul
			final NSMutableDictionary dicoProc = new NSMutableDictionary();
			dicoProc.takeValueForKey(exercice.exeExercice(), "10_exeordre");
			getAgregatCtrl().cptefiBuildStoredProcParams(niveau, (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), dicoProc);


			final Thread t = new Thread() {
				public void run() {
					try {
						ServerProxy.clientSideRequestExecuteStoredProcedureNamed(ec, "comptefi.prepare_VARIATION_FRNG", dicoProc);
						waitingDialog.setVisible(false);
					} catch (Exception e) {
						waitingDialog.setVisible(false);
						myApp.showErrorDialog(e);
					}
				};

			};
			t.start();
			waitingDialog = new ZWaitingPanelDialog(null, win, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.setTitle("Veuillez patienter...");
			waitingDialog.setTopText("Veuillez patienter ...");
			waitingDialog.setBottomText("Calculs en cours...");
			waitingDialog.setModal(true);
			waitingDialog.setVisible(true);
		}


		try {
			String req = "select * from comptefi.frng " + "where ges_code = '" + vGescode + "' " + "and  " + condexer + " order by FRNG_ORDRE";
			final String res = imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_FRNG, "frng.pdf", req, win, FORMATPDF, null, Boolean.TRUE);
			if (res != null) {
				myApp.openPdfFile(res);
			}
		} catch (Exception e1) {
			myApp.showErrorDialog(e1);
		}

	}

	private void imprimerLISTE_OP(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		//final EOGestion gesCodeSACD = (EOGestion) paramRequete.get(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY);
		//final EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		final EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String condExer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "e.exe_ordre", new NSArray(exercice));
		//      final String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "e.com_ordre", new NSArray(comptabilite));
		final Date dateFin = (Date) paramRequete.get("dateFin");
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		//String condsacd = "";

		final NSMutableDictionary params = new NSMutableDictionary(parametres);
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "DATE_OPERATIONS");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_FIN");

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);

		String req = "SELECT   o.exe_ordre, o.odp_numero, o.odp_date, o.odp_libelle, o.odp_ht, o.odp_tva, o.odp_ttc, ecd_debit, "
				+ "ed.pco_num, maracuja.api_planco.get_pco_libelle(ed.pco_num, ed.exe_ordre) as pco_libelle, (f.adr_nom || ' ' || f.adr_prenom) AS fournisseur_nom, e.ecr_date_saisie, ed.ges_code, nvl(org.org_lib, ed.ges_code) AS ges_lib "
				+ "FROM MARACUJA.ordre_de_paiement o, MARACUJA.odpaiem_detail_ecriture od,  " +
				"MARACUJA.ecriture_detail ed, MARACUJA.ecriture e, " +
				" (select fou_ordre, max(adr_ordre) adr_ordre from maracuja.v_fournisseur f group by fou_ordre) x, " +
				"MARACUJA.v_fournisseur f, " +
				"(select exe_ordre, org_ub,  org_lib from MARACUJA.v_organ_exer org where org_niv=2) org " +
				"WHERE ed.ecr_ordre = e.ecr_ordre AND o.odp_ordre = od.odp_ordre AND od.ecd_ordre = ed.ecd_ordre " +
				"AND o.fou_ordre = f.fou_ordre "
				+
				" AND ed.ges_code = org.org_ub(+) " +
				"  AND ed.exe_ordre = org.exe_ordre (+) " +
				"AND ed.exe_ordre = o.exe_ordre " +
				"AND " + condExer +
				"AND SUBSTR (e.ecr_etat, 1, 1) = 'V' " +
				"AND (odp_etat = 'PAYE' OR odp_etat = 'VISE') " +
				"and ed.ecd_sens='D' " +
				" and x.fou_ordre=f.fou_ordre and f.adr_ordre=x.adr_ordre " +
				"and to_char(ecr_date_saisie,'YYYYMMDD') < '"
				+ dateFinStr + "' " +
				" UNION ALL " +
				" select o.exe_ordre, o.man_numero as odp_numero, b.bor_date_creation as odp_date, 'Remboursement Etudiant' as odp_libelle," +
				" o.man_ht as odp_ht, o.man_tva as odp_tva, o.man_ttc as odp_ttc, ecd_debit, ed.pco_num," +
				" maracuja.api_planco.get_pco_libelle (ed.pco_num,ed.exe_ordre) AS pco_libelle," +
				" (f.adr_nom || ' ' || f.adr_prenom) AS fournisseur_nom, " +
				" e.ecr_date_saisie, ed.ges_code," +
				" NVL (org.org_lib, ed.ges_code) AS ges_lib " +
				" FROM MARACUJA.mandat o, " +
				"    MARACUJA.depense d,  " +
				"  MARACUJA.bordereau b, " +
				" MARACUJA.type_bordereau tb, " +
				" MARACUJA.mandat_detail_ecriture od, " +
				" MARACUJA.ecriture_detail ed, " +
				" MARACUJA.ecriture e, " +
				" (SELECT   fou_ordre, MAX (adr_ordre) adr_ordre " +
				"     FROM maracuja.v_fournisseur f " +
				" GROUP BY fou_ordre) x, " +
				" MARACUJA.v_fournisseur f, " +
				" (SELECT exe_ordre, org_ub, org_lib " +
				"  FROM MARACUJA.v_organ_exer org " +
				" WHERE org_niv = 2) org " +
				" WHERE ed.ecr_ordre = e.ecr_ordre " +
				"  and o.man_id=d.man_id " +
				" and o.bor_id=b.bor_id  " +
				" and b.tbo_ordre=tb.tbo_ordre " +
				" and tb.tbo_type='BTRU' " +
				" AND o.man_id = od.man_id " +
				" AND od.ecd_ordre = ed.ecd_ordre " +
				" AND o.fou_ordre = f.fou_ordre " +
				" AND ed.ges_code = org.org_ub(+) " +
				" AND ed.exe_ordre = org.exe_ordre(+) " +
				" AND ed.exe_ordre = o.exe_ordre " +
				" AND " + condExer +
				" AND SUBSTR (e.ecr_etat, 1, 1) = 'V' " +
				" AND (man_etat = 'PAYE' OR man_etat = 'VISE') " +
				" AND ed.ecd_sens = 'D' " +
				" AND x.fou_ordre = f.fou_ordre " +
				" AND f.adr_ordre = x.adr_ordre " +
				"and to_char(ecr_date_saisie,'YYYYMMDD') < '"
				+ dateFinStr + "' " +

				" ORDER BY ges_code, pco_num, odp_numero";

		req = "select * from (" + req + ") x ";
		req = req + (condGestion.length() > 0 ? " where " : "") + condGestion + " ORDER BY ges_code, pco_num, odp_numero";

		try {
			final String res = imprimerReportByThread(ec, temporaryDir, params, JASPERFILENAME_LISTE_OP, JASPERFILENAME_LISTE_OP + ".pdf", req, win, FORMATPDF, null, Boolean.TRUE);
			if (res != null) {
				myApp.openPdfFile(res);
			}
		} catch (Exception e1) {
			myApp.showErrorDialog(e1);
		}

	}

}
