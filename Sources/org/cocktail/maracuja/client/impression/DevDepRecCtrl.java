/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.DevDepRecPanel;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DevDepRecCtrl extends ZKarukeraImprCtrl {
	public static final int MODE_CADRE2 = 2;
	public static final int MODE_CADRE3 = 3;
	private final String WINDOW_TITLE_2 = "Cadre 2 : Développement des dépenses budgétaires";
	private final String WINDOW_TITLE_3 = "Cadre 3 : Développement des recettes budgétaires";
	private final Dimension WINDOW_SIZE = new Dimension(550, 170);
	private DevDepRecPanel myPanel;
	private int _mode;

	private static final String JASPERFILENAME_DEV_DEP_BUD = "dvlop_depbud.jasper";
	private static final String JASPERFILENAME_DEV_DEP_BUD_EXT = "dvlop_depbud.jasper";
	private static final String JASPERFILENAME_DEV_REC_BUD = "dvlop_recbud.jasper";
	private static final String JASPERFILENAME_DEV_DEP_BUD_CONS = "dvlop_depbud_cons.jasper";
	private static final String JASPERFILENAME_DEV_REC_BUD_CONS = "dvlop_recbud_cons.jasper";

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public DevDepRecCtrl(EOEditingContext editingContext, final int mode) throws Exception {
		//		super(editingContext, EOsFinder.getGestionNonSacd(editingContext, myApp.appUserInfo().getCurrentExercice()), EOsFinder.getSACDGestion(editingContext, myApp.appUserInfo().getCurrentExercice()));
		super(editingContext);
		_mode = mode;
		myPanel = new DevDepRecPanel(new DevDepRecPanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			//			String filePath = imprimerCadre2_3DevDepRec(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), _mode, (String) niveauxMap.get(myFilters.get(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY)));
			String filePath = imprimerCadre2_3DevDepRec(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog(), _mode);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public String imprimerCadre2_3DevDepRec(final EOEditingContext ec, final String temporaryDir, final NSMutableDictionary parametres, final HashMap paramRequete, final ZKarukeraDialog win, final int _mode) throws Exception {
		String fileName;
		//		if (modele == null) {
		//			throw new DefaultClientException("Modele d'impression non récupéré");
		//		}
		//        
		if (DevDepRecCtrl.MODE_CADRE2 == _mode) {
			//Cadre 2 (depense)
			//			if (DevDepRecCtrl.NIVEAU2_ETAB_SACD.equals(modele) || DevDepRecCtrl.NIVEAU1_AGREGE.equals(modele)) {

			if (AgregatsCtrl.NIVEAU3_UB.equals(paramRequete.get(AgregatsCtrl.AGREGAT_KEY))) {
				//fileName = JASPERFILENAME_DEV_DEP_BUD;
				fileName = JASPERFILENAME_DEV_DEP_BUD_EXT;
			}
			else {
				fileName = JASPERFILENAME_DEV_DEP_BUD_CONS;
			}
		}
		else {
			//Cadre 3
			if (AgregatsCtrl.NIVEAU3_UB.equals(paramRequete.get(AgregatsCtrl.AGREGAT_KEY))) {
				fileName = JASPERFILENAME_DEV_REC_BUD;
			}
			else {
				fileName = JASPERFILENAME_DEV_REC_BUD_CONS;
			}
		}
		//        
		String req = buildSql(ec, paramRequete, parametres);
		return imprimerReportByThreadPdf(ec, temporaryDir, parametres, fileName, getFileNameWithExtension(ZFileUtil.removeExtension(fileName), FORMATPDF), req, win, null);
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		//myFilters.put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, NIVEAU2_ETAB_SACD);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(final Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class DevDepRecPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements DevDepRecPanel.IDevDepRecPanelListener {


	}

	protected String getActionId() {
		return null;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return (MODE_CADRE2 == _mode ? WINDOW_TITLE_2 : WINDOW_TITLE_3);
	}

	private String buildSql(EOEditingContext ec, HashMap paramRequete, NSMutableDictionary params) throws Exception {

		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));
		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		String condExer = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "d.exe_ordre", new NSArray(exercice));

		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "d.", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");
		params.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_EDS");
		params.takeValueForKey(exercice.exeExercice().toString(), "EXER");
		params.takeValueForKey(comptabilite.comLibelle(), "COMPTA");

		String req = "";

		if (DevDepRecCtrl.MODE_CADRE2 == _mode) {
			//Cadre 2 (depense)
			if (!AgregatsCtrl.NIVEAU3_UB.equals(paramRequete.get(AgregatsCtrl.AGREGAT_KEY))) {
				req = "select d.EXE_ORDRE,   SECTION, section_lib, ordre_presentation, PCO_NUM_BDN, d.pco_num PCO_NUM," +
						"maracuja.api_planco.get_pco_libelle(d.pco_num, d.exe_ordre) as pco_libelle , sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, "
						+ "sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL, " +
						"sum(mandats_sur_extourne) MANDATS_SUR_EXTOURNE, sum(abs(cr_extourne)) CR_EXTOURNE, sum(abs(cr_extourne))-sum(mandats_sur_extourne) CR_EXTOURNE_NON_EMPL "
						+ "from comptefi.v_dvlop_dep_ext d, maracuja.v_organ_exer o ";
			}
			else {
				req = "select d.EXE_ORDRE, d.ges_code GES_CODE, org_lib GES_LIBELLE, SECTION, section_lib, ordre_presentation, PCO_NUM_BDN, d.pco_num PCO_NUM," +
						"maracuja.api_planco.get_pco_libelle(d.pco_num, d.exe_ordre) as pco_libelle, sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, "
						+ "sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL, " +
						"sum(mandats_sur_extourne) MANDATS_SUR_EXTOURNE, sum(abs(cr_extourne)) CR_EXTOURNE, sum(abs(cr_extourne))-sum(mandats_sur_extourne) CR_EXTOURNE_NON_EMPL " +
						"from comptefi.v_dvlop_dep_ext d, maracuja.v_organ_exer o ";
			}
		}
		else {
			//Cadre 3
			if (!AgregatsCtrl.NIVEAU3_UB.equals(paramRequete.get(AgregatsCtrl.AGREGAT_KEY))) {
				req = "select d.exe_ordre, section, pco_num_bdn, d.pco_num PCO_NUM, maracuja.api_planco.get_pco_libelle(d.pco_num, d.exe_ordre) as pco_libelle," + " sum(recettes) RECETTES, sum(reductions) REDUCTIONS, " + "sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS "
						+ "from comptefi.v_dvlop_rec d, maracuja.v_organ_exer o ";

			}
			else {
				req = "select d.exe_ordre, d.ges_code GES_CODE, org_lib GES_LIBELLE, SECTION, PCO_NUM_BDN, d.pco_num PCO_NUM, " + "maracuja.api_planco.get_pco_libelle(d.pco_num, d.exe_ordre) as pco_libelle, sum(recettes) RECETTES, sum(reductions) REDUCTIONS, "
						+ "sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS " + "from comptefi.v_dvlop_rec d, maracuja.v_organ_exer o ";
			}
		}
		//        

		req += "where " + " to_char(dep_date,'YYYYMMDD') < '" + dateFinStr + "' " + " and " + condExer +
				(condGestion.length() != 0 ? " and " + condGestion : "")
				+ " and (d.ges_code = o.org_ub and org_niv = 2) and o.exe_ordre=d.exe_ordre";

		//		if (condsacd.length() != 0) {
		//			req += " and " + condsacd;
		//		}

		if (DevDepRecCtrl.MODE_CADRE2 == _mode) {
			if (!AgregatsCtrl.NIVEAU3_UB.equals(paramRequete.get(AgregatsCtrl.AGREGAT_KEY))) {
				req += " group by d.exe_ordre, section, section_lib, ordre_presentation, pco_num_bdn, d.pco_num " + " order by exe_ordre, ordre_presentation,section, pco_num_bdn, pco_num";
			}
			else {
				req += " group by d.exe_ordre, d.ges_code, org_lib, section, section_lib, ordre_presentation, pco_num_bdn, d.pco_num " + " order by exe_ordre, ordre_presentation,section, ges_code, section, pco_num_bdn, pco_num";
			}
		}
		else {
			if (!AgregatsCtrl.NIVEAU3_UB.equals(paramRequete.get(AgregatsCtrl.AGREGAT_KEY))) {
				req += " group by d.exe_ordre, section,pco_num_bdn, d.pco_num " + " order by exe_ordre, section, pco_num_bdn, pco_num";
			}
			else {
				req += " group by d.exe_ordre, d.ges_code, org_lib, section, pco_num_bdn, d.pco_num " + " order by exe_ordre, section, ges_code, section, pco_num_bdn, pco_num";
			}
		}

		return req;
	}
}
