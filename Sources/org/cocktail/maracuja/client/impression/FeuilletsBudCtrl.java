/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.FeuilletsBudPanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class FeuilletsBudCtrl extends ZKarukeraImprCtrl {
	private final String WINDOW_TITLE = "Impression des feuillets budgétaires";
	private final Dimension WINDOW_SIZE = new Dimension(609, 370);
	private final String ACTION_ID = ZActionCtrl.IDU_IMPR010;

	private static final String JASPERFILENAME_FBX_MANDAT = "FBX_mandat_sql.jasper";
	private static final String JASPERFILENAME_FBX_MANDAT_CHAP = "FBX_mandat_chap_sql.jasper";
	private static final String JASPERFILENAME_FBX_MANDAT_COMP_CHAP = "FBX_mandat_comp_chap_sql.jasper";
	private static final String JASPERFILENAME_FBX_TITRE = "FBX_titre_sql.jasper";
	private static final String JASPERFILENAME_FBX_TITRE_CHAP = "FBX_titre_chap_sql.jasper";
	private static final String JASPERFILENAME_FBX_TITRE_COMP_CHAP = "FBX_titre_comp_chap_sql.jasper";

	public final static String FBTYPE_MANDATS = "Dépenses (mandats)";
	public final static String FBTYPE_OR = "Dépenses (ordres de reversements)";
	public final static String FBTYPE_TITRES = "Recettes (titres)";
	public final static String FBTYPE_RED = "Recettes (Réductions de recettes)";

	public final static String REGROUPEMENT_CHAPITRE = "par chapitre";
	public final static String REGROUPEMENT_CLASSE = "par classe";
	public final static String REGROUPEMENT_COMPOSANTE = "par composante";
	public static final String REGROUPEMENT_COMP_CHAPITRE = "par composante puis chapitre";
	public static final String REGROUPEMENT_COMP_CLASSE = "par composante puis classe";

	private FeuilletsBudPanel myPanel;
	private final FBTypeModel typeModel = new FBTypeModel();
	private final RegroupementModel regroupementModel = new RegroupementModel();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public FeuilletsBudCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		if (getAgregatCtrl().getAllAuthorizedGestions().count() == 0) {
			//if (gestions.count() + gestionsSacd.count() == 0) {
			throw new DefaultClientException("Aucun code gestion ne vous est autorisé pour cette impression. Contactez un administrateur qui peut vous affecter les droits.");
		}
		myPanel = new FeuilletsBudPanel(new FeuilletsBudPanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);

			myFilters.put("planco", myFilters.get("pcoNum"));

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			String filePath = imprimerFeuilletsBud(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	public String imprimerFeuilletsBud(EOEditingContext ec, String temporaryDir, NSDictionary parametres, HashMap paramRequete, Window win) throws Exception {
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		final String regroupement = (String) paramRequete.get("regroupement");
		String modeleJasper = null;
		if (FeuilletsBudCtrl.FBTYPE_MANDATS.equals(paramRequete.get("typeFb")) || (exercice.exeExercice().intValue() >= ZConst.EXE_EXERCICE_2007 && FeuilletsBudCtrl.FBTYPE_OR.equals(paramRequete.get("typeFb")))) {
			if (FeuilletsBudCtrl.REGROUPEMENT_COMPOSANTE.equals(regroupement)) {
				modeleJasper = JASPERFILENAME_FBX_MANDAT;
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
				modeleJasper = JASPERFILENAME_FBX_MANDAT_CHAP;
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement)) {
				modeleJasper = JASPERFILENAME_FBX_MANDAT_COMP_CHAP;
			}
		}
		else {
			if (FeuilletsBudCtrl.REGROUPEMENT_COMPOSANTE.equals(regroupement)) {
				modeleJasper = JASPERFILENAME_FBX_TITRE;
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
				modeleJasper = JASPERFILENAME_FBX_TITRE_CHAP;
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement)) {
				modeleJasper = JASPERFILENAME_FBX_TITRE_COMP_CHAP;
			}
		}

		if (modeleJasper == null) {
			throw new DefaultClientException("Modele d'impression non récupéré");
		}
		NSMutableDictionary params = new NSMutableDictionary(parametres);

		final String req = buildSql(ec, params, paramRequete);
		return imprimerReportByThread(ec, temporaryDir, params, modeleJasper, getFileNameWithExtension(ZFileUtil.removeExtension(modeleJasper), FORMATPDF), req, win, FORMATPDF, null);
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, myApp.appUserInfo().getCurrentExercice());
		myFilters.put("dateDebut", ZDateUtil.getFirstDayOfYear(getExercice().exeExercice().intValue()));
		myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY, ZDateUtil.getMinOf2Dates(ZDateUtil.getTodayAsCalendar().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue())));
		myFilters.put(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY, comptabilite);
		myFilters.put("typeFb", FBTYPE_MANDATS);
		myFilters.put("regroupement", REGROUPEMENT_CHAPITRE);
		//	myFilters.put(ZKarukeraImprCtrl.NIVEAU_FILTER_KEY, NIVEAU2_ETAB_SACD);
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		try {
			initFilters();
			myPanel.updateData();
			myPanel.getPcoSelectButton().updateData();
			//	myFilters.put(ZKarukeraImprCtrl.GESTION_SACD_FILTER_KEY, ((EOGestion) gestionsSacdModel.getSelectedEObject()));
			//	myFilters.put(ZKarukeraImprCtrl.GESTION_FILTER_KEY, ((EOGestion) gestionsModel.getSelectedEObject()));

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final class FeuilletsBudPanelListener extends ZKarukeraImprCtrl.ZKarukeraImprPanelListener implements FeuilletsBudPanel.IFeuilletsBudPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.GrandLivrePanel.IGrandLivrePanelListener#getPcoMap()
		 */
		public Map getPcoMap() {
			return pcoMap;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

		public DefaultComboBoxModel getTypeFBModel() {
			return typeModel;
		}

		public void setTypeFB(Object selectedItem) {
			myFilters.put("typeFb", selectedItem);
		}

		public DefaultComboBoxModel getRegroupementModel() {
			return regroupementModel;
		}

		public void setRegroupement(Object selectedItem) {
			myFilters.put("regroupement", selectedItem);

		}

	}

	private final class FBTypeModel extends DefaultComboBoxModel {
		private final LinkedHashMap typesFB = new LinkedHashMap();

		public FBTypeModel() {
			typesFB.put("FBTYPE_MANDATS", FBTYPE_MANDATS);
			typesFB.put("FBTYPE_OR", FBTYPE_OR);
			//            typesFB.put("FBTYPE_OP", FBTYPE_OP);
			typesFB.put("FBTYPE_TITRES", FBTYPE_TITRES);
			typesFB.put("FBTYPE_RED", FBTYPE_RED);

			final Iterator iterator = typesFB.keySet().iterator();
			while (iterator.hasNext()) {
				addElement(typesFB.get(iterator.next()));
			}
		}
	}

	private final class RegroupementModel extends DefaultComboBoxModel {
		private final LinkedHashMap elts = new LinkedHashMap();

		public RegroupementModel() {
			elts.put("REGROUPEMENT_CHAPITRE", REGROUPEMENT_CHAPITRE);
			elts.put("REGROUPEMENT_CLASSE", REGROUPEMENT_CLASSE);
			elts.put("REGROUPEMENT_COMPOSANTE", REGROUPEMENT_COMPOSANTE);
			elts.put("REGROUPEMENT_CHAPITRE_COMP", REGROUPEMENT_COMP_CHAPITRE);
			elts.put("REGROUPEMENT_CLASSE_COMP", REGROUPEMENT_COMP_CLASSE);

			final Iterator iterator = elts.keySet().iterator();
			while (iterator.hasNext()) {
				addElement(elts.get(iterator.next()));
			}
		}
	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	private String buildSql(EOEditingContext ec, NSMutableDictionary params, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		final String regroupement = (String) paramRequete.get("regroupement");

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get("dateFin");

		final String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		final String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "exe_ordre", new NSArray(exercice));
		String condGestion = "";
		String condCompte = buildCriteresPlanco("pco_num", (String) paramRequete.get("planco"));
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "", null);
		params.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		params.takeValueForKey(exercice.exeExercice().toString(), "EXERCICE");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOUR_DEBUT");
		params.takeValueForKey(new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOUR_FIN");

		if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
			params.takeValueForKey("Classe", "REGROUPEMENT");
		}
		else {
			params.takeValueForKey("Chapitre", "REGROUPEMENT");
		}

		String req = null;

		if (FeuilletsBudCtrl.FBTYPE_MANDATS.equals(paramRequete.get("typeFb")) || (exercice.exeExercice().intValue() >= ZConst.EXE_EXERCICE_2007 && FeuilletsBudCtrl.FBTYPE_OR.equals(paramRequete.get("typeFb")))) {

			req = "select exe_ordre, ges_code, pco_num, bor_id, man_id, man_num, man_lib, man_mont, man_tva, " +
					"man_ttc, ecr_sacd, ecr_date, ecr_date_saisie, bor_num, imp_lib, ges_lib,m.tbo_ordre " +
					"from MARACUJA.v_mandat_reimp m, MARACUJA.type_bordereau tb " + " where " + condCompte
					+ (condCompte.length() > 0 ? " and " : "") + cond + " and substr(pco_num,1,1)<>'4' " + " and m.tbo_ordre=tb.tbo_ordre " + "and to_char(ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " + "and to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' "
					+ (condGestion.length() != 0 ? " and " + condGestion : "");

			if (FeuilletsBudCtrl.FBTYPE_OR.equals(paramRequete.get("typeFb"))) {
				req += " and tb.tbo_sous_type='" + EOTypeBordereau.SOUS_TYPE_REVERSEMENTS + "' ";
			}
			else {
				req += " and tb.tbo_sous_type<>'" + EOTypeBordereau.SOUS_TYPE_REVERSEMENTS + "' ";
			}

			if (FeuilletsBudCtrl.REGROUPEMENT_COMPOSANTE.equals(regroupement)) {
				req += " order by exe_ordre, ges_code, pco_num, man_num,ecr_date_saisie, ecr_date ";
				return req;
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
				req = "select exe_ordre,ges_code, x.pco_num, bor_id, man_id, man_num, man_lib, man_mont, man_tva, " +
						"man_ttc, ecr_sacd, ecr_date, ecr_date_saisie, bor_num,imp_lib, ges_lib, pco_classe, pco_chapitre, tbo_ordre " +
						"from MARACUJA.v_planco_chapitre p, (" + req + ") x where x.pco_num=p.pco_num ";
				if (FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
					req += " order by exe_ordre, pco_classe, man_num, ges_code ,ecr_date_saisie, ecr_date ";
				}
				if (FeuilletsBudCtrl.REGROUPEMENT_CHAPITRE.equals(regroupement)) {
					req += " order by exe_ordre, pco_classe, pco_chapitre, man_num, ges_code ,ecr_date_saisie, ecr_date ";
				}
				return req;
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement)) {
				req = "select exe_ordre,ges_code, x.pco_num, bor_id, man_id, man_num, man_lib, man_mont, man_tva, " +
						"man_ttc, ecr_sacd, ecr_date, ecr_date_saisie, bor_num,imp_lib, ges_lib, pco_classe, pco_chapitre, tbo_ordre " +
						"from MARACUJA.v_planco_chapitre p, (" + req + ") x where x.pco_num=p.pco_num ";
				if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement)) {
					req += " order by exe_ordre , ges_code, pco_classe, man_num,ecr_date_saisie, ecr_date  ";
				}
				else if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CHAPITRE.equals(regroupement)) {
					req += " order by exe_ordre , ges_code, pco_classe, pco_chapitre, man_num,ecr_date_saisie, ecr_date  ";
				}

				return req;
			}
			else {
				return null;
			}

		}

		// Sinon on se base sur les titres
		req = "select exe_ordre, ges_code, pco_num, bor_id, tit_id, tit_num, tit_lib, tit_mont, tit_tva, debiteur,tb.tbo_ordre," +
				"tit_ttc, ecr_sacd, ecr_date, ecr_date_saisie, bor_num, imp_lib, ges_lib " +
				"from maracuja.v_titre_reimp m, maracuja.type_bordereau tb" +
				" where " + condCompte
				+ (condCompte.length() > 0 ? " and " : "") + cond +
				"and to_char(ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " + "and to_char(ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
				" and m.tbo_ordre=tb.tbo_ordre " +
				(condGestion.length() != 0 ? " and " + condGestion : "");

		if (FeuilletsBudCtrl.FBTYPE_OR.equals(paramRequete.get("typeFb"))) {
			req += " and tb.tbo_sous_type='" + EOTypeBordereau.SOUS_TYPE_REVERSEMENTS + "' ";
		}
		else if (FeuilletsBudCtrl.FBTYPE_RED.equals(paramRequete.get("typeFb"))) {
			req += " and tb.tbo_sous_type='" + EOTypeBordereau.SOUS_TYPE_REDUCTION + "' ";
		}
		else {
			req += " and tb.tbo_sous_type<>'" + EOTypeBordereau.SOUS_TYPE_REDUCTION + "' and tb.tbo_sous_type<>'" + EOTypeBordereau.SOUS_TYPE_REVERSEMENTS + "'";
		}

		if (FeuilletsBudCtrl.REGROUPEMENT_COMPOSANTE.equals(regroupement)) {
			req += " order by exe_ordre, ges_code, pco_num, tit_num, ecr_date_saisie, ecr_date";
			return req;
		}
		else if (FeuilletsBudCtrl.REGROUPEMENT_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
			req = "select exe_ordre, ges_code, x.pco_num, bor_id, tit_id, tit_num, tit_lib, tit_mont, tit_tva, debiteur,tbo_ordre," + "tit_ttc, ecr_sacd, ecr_date,ecr_date_saisie, bor_num, imp_lib, ges_lib, pco_classe, pco_chapitre " +
					"from MARACUJA.v_planco_chapitre p, (" + req + ") x where x.pco_num=p.pco_num ";

			if (FeuilletsBudCtrl.REGROUPEMENT_CLASSE.equals(regroupement)) {
				req += " order by exe_ordre, pco_classe , tit_num, ges_code , ecr_date_saisie, ecr_date";
			}
			if (FeuilletsBudCtrl.REGROUPEMENT_CHAPITRE.equals(regroupement)) {
				req += " order by exe_ordre, pco_classe, pco_chapitre , tit_num, ges_code , ecr_date_saisie, ecr_date";
			}

			return req;
		}
		else if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CHAPITRE.equals(regroupement) || FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement)) {
			req = "select exe_ordre, ges_code, x.pco_num, bor_id, tit_id, tit_num, tit_lib, tit_mont, tit_tva, debiteur,tbo_ordre," + "tit_ttc, ecr_sacd, ecr_date, ecr_date_saisie, bor_num, imp_lib, ges_lib, pco_classe, pco_chapitre " +
					"from MARACUJA.v_planco_chapitre p, (" + req + ") x where x.pco_num=p.pco_num ";
			if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CLASSE.equals(regroupement)) {
				req += " order by exe_ordre , ges_code , pco_classe, tit_num, ecr_date_saisie, ecr_date";
			}
			else if (FeuilletsBudCtrl.REGROUPEMENT_COMP_CHAPITRE.equals(regroupement)) {
				req += " order by exe_ordre , ges_code , pco_classe, pco_chapitre, tit_num, ecr_date_saisie, ecr_date";
			}

			return req;
		}
		else {
			return null;
		}
	}
}
