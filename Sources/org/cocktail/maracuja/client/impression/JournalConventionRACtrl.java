package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.impression.ui.JournalConventionRAPanel;
import org.cocktail.maracuja.client.impression.ui.JournalConventionRAPanel.IJournalConventionRAPanelListener;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonListener;
import org.cocktail.zutil.client.ui.ZLookupButton.IZLookupButtonModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

public class JournalConventionRACtrl extends JournalGeneralCtrl {

	private final String WINDOW_TITLE = "Journal par convention RA";
	private final Dimension WINDOW_SIZE = new Dimension(550, 260);

	public JournalConventionRACtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		myPanel = new JournalConventionRAPanel(new JournalConventionRAPanelListener());
	}

	@Override
	protected void initFilters() {
		super.initFilters();
		EOTypeOperation top = EOTypeOperation.fetchByKeyValue(getEditingContext(), EOTypeOperation.TOP_LIBELLE_KEY, EOTypeOperation.TYPE_OPERATION_CONVENTION);
		//EOTypeOperation top = EOTypeOperation.fetchByKeyValue(getEditingContext(), EOTypeOperation.TOP_LIBELLE_KEY, EOTypeOperation.TYPE_OPERATION_LUCRATIVE);
		myFilters.put(ZKarukeraImprCtrl.TYPE_OPERATION_FILTER_KEY, top);

		//Initialiser avec les comptes 4682 et 4684

	}

	private final class JournalConventionRAPanelListener extends JournalGeneralCtrl.JournalGeneralPanelListener implements IJournalConventionRAPanelListener {

		public Map getPcoMap() {
			return pcoMap;
		}

		public IZLookupButtonListener getLookupButtonCompteListener() {
			return pcoLookupListener;
		}

		public IZLookupButtonModel getLookupButtonCompteModel() {
			return pcoLookupModel;
		}

	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}

	protected String buildSql(EOEditingContext ec, NSMutableDictionary parametres, HashMap paramRequete) throws Exception {
		if (paramRequete == null || paramRequete.size() == 0) {
			throw new DefaultClientException("Aucun objet n'a été passé au moteur d'impression");
		}

		final String typeJournal = (String) comboBoxModeleModel.getSelectedItem();
		final String mdl = (String) comboBoxModeleModel2.getSelectedItem();

		Date dateDebut = ((Date) paramRequete.get("dateDebut"));
		Date dateFin = (Date) paramRequete.get(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY);

		String dateDebutStr = new SimpleDateFormat("yyyyMMdd").format(dateDebut);
		String dateFinStr = new SimpleDateFormat("yyyyMMdd").format(ZDateUtil.addDHMS(ZDateUtil.getDateOnly(dateFin), 1, 0, 0, 0));

		EOComptabilite comptabilite = (EOComptabilite) paramRequete.get(ZKarukeraImprCtrl.COMPTABILITE_FILTER_KEY);
		EOExercice exercice = (EOExercice) paramRequete.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);

		String cond = buildConditionFromPrimaryKeyAndValues(ec, "exeOrdre", "ecriture.exe_ordre", new NSArray(exercice));
		//String condsacd = "";
		String condCompta = buildConditionFromPrimaryKeyAndValues(ec, "comOrdre", "ecriture.com_ordre", new NSArray(comptabilite));

		String condTypeJournal = "";
		if (JournalGeneralCtrl.MDL_GENERAL.equals(mdl)) {
			condTypeJournal = "";
		}
		else if (JournalGeneralCtrl.MDL_CORRECTIONS.equals(typeJournal)) {
			condTypeJournal = " ecriture.ecr_ordre in (select distinct ecr_ordre from maracuja.ecriture_detail where ecd_montant<0 and exe_ordre=" + exercice.exeExercice().intValue() + ")";
		}
		else if (JournalGeneralCtrl.MDL_BE.equals(typeJournal)) {
			condTypeJournal = " ecriture.tjo_ordre in (select tjo_ordre from maracuja.type_journal where tjo_libelle = 'JOURNAL BALANCE ENTREE')";
		}
		else if (JournalGeneralCtrl.MDL_GRAND_LIVRE_VALEURS_INACTIVES.equals(typeJournal)) {
			condTypeJournal = " ecriture.tjo_ordre in (select tjo_ordre from maracuja.type_journal where tjo_ordre = 18)";
		}

		String condCompte = buildCriteresPlanco("ecriture_detail.pco_num", (String) paramRequete.get("planco"));
		String condOrigine = buildConditionFromPrimaryKeyAndValues(ec, EOOrigine.ORI_ORDRE_KEY, "ecriture.ori_ordre", new NSArray(paramRequete.get(ORIGINE_FILTER_KEY)));

		parametres.takeValueForKey(paramRequete.get("planco"), "PLANCO");
		parametres.takeValueForKey(((EOOrigine) paramRequete.get(ORIGINE_FILTER_KEY)).oriLibelle(), "ORIGINE");

		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.AGREGAT_KEY) + " " + ZStringUtil.ifNull((String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), ""), "SACD");
		String condGestion = getAgregatCtrl().buildSqlConditionForAgregat((String) paramRequete.get(AgregatsCtrl.AGREGAT_KEY), (String) paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "ecriture_detail.", null);
		parametres.takeValueForKey(paramRequete.get(AgregatsCtrl.GES_CODE_KEY), "GESTION");

		parametres.takeValueForKey(typeJournal, "TYPE_JOURNAL");
		parametres.takeValueForKey(exercice.exeExercice().toString(), "JOURNAL_EXERCICE");
		if (dateDebut != null && dateFin != null) {
			if (dateDebut.equals(dateFin)) {
				parametres.takeValueForKey("Journée du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut), "JOURNEES");
			}
			else {
				parametres.takeValueForKey("Journées du " + new SimpleDateFormat("dd/MM/yyyy").format(dateDebut) + " au " + new SimpleDateFormat("dd/MM/yyyy").format(dateFin), "JOURNEES");
			}
		}

		String reqCumuls = "";
		// On fait les requetes pour recupérer les cumuls precedents
		reqCumuls = "select nvl(sum(nvl(ecriture_detail.ecd_debit,0)),0) as TOT_DEBIT, " +
				" nvl(sum(nvl(ecriture_detail.ecd_credit,0)),0) as TOT_CREDIT " +
				"from MARACUJA.ecriture, MARACUJA.ecriture_detail " +
				"where " +
				"ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
				"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateDebutStr + "' " +
				"and ecriture.ecr_numero > 0    " +
				"and " + cond +
				" and " + condCompta +
				" and " + condOrigine +
				(condCompte != null && condCompte.length() != 0 ? " and " + condCompte : "") +
				(condGestion.length() != 0 ? " and " + condGestion : "") +
				(condTypeJournal.length() != 0 ? " and " + condTypeJournal : "");

		ZLogger.debug(reqCumuls);

		NSArray res = ServerProxy.clientSideRequestSqlQuery(ec, reqCumuls);
		if (res == null || res.count() == 0) {
			throw new DefaultClientException("Impossible de calculer les cumuls");
		}
		NSDictionary dicCumul = (NSDictionary) res.objectAtIndex(0);
		BigDecimal cumulDebit = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal cumulCredit = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);

		if (!NSKeyValueCoding.NullValue.equals(dicCumul.valueForKey("TOT_DEBIT"))) {
			cumulDebit = new BigDecimal(((Number) dicCumul.valueForKey("TOT_DEBIT")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		}
		if (!NSKeyValueCoding.NullValue.equals(dicCumul.valueForKey("TOT_CREDIT"))) {
			cumulCredit = new BigDecimal(((Number) dicCumul.valueForKey("TOT_CREDIT")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP);
		}

		parametres.takeValueForKey(cumulDebit, "PREC_DEBIT");
		parametres.takeValueForKey(cumulCredit, "PREC_CREDIT");

		// ZLogger.debug(params);
		String req = "";
		if (JournalGeneralCtrl.MDL2_NOUVEAU.equals(mdl)) {
			req = "select  com_libelle, ecr_numero, ecr_date_saisie as ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, ecriture.exe_ordre, ecriture_detail.pco_num, " +
					"ecriture_detail.ges_code, type_journal.tjo_libelle type_journal " +
					"from MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal " +
					"where " + "ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
					"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " +
					" and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
					"and ecriture.ecr_numero > 0    " +
					"and ecriture.tjo_ordre=type_journal.tjo_ordre " +
					" and " + cond +
					" and " + condCompta +
					" and " + condOrigine +
					(condCompte != null && condCompte.length() != 0 ? " and " + condCompte : "") +
					(condGestion.length() != 0 ? " and " + condGestion : "") +
					(condTypeJournal.length() != 0 ? " and " + condTypeJournal : "");

		}
		else {
			req = "select  com_libelle, ecr_numero, ecr_date_saisie as ecr_date, ecr_libelle, ecd_libelle, ecd_debit, ecd_credit, ecd_index, ecriture.exe_ordre, ecriture_detail.pco_num, " +
					"maracuja.api_planco.get_pco_libelle(ecriture_detail.pco_num, ecriture_detail.exe_ordre) as pco_libelle, ecriture_detail.ges_code, type_journal.tjo_libelle type_journal " +
					"from MARACUJA.comptabilite, MARACUJA.ecriture, MARACUJA.ecriture_detail, MARACUJA.type_journal " +
					"where " +
					"ecriture.ecr_ordre = ecriture_detail.ecr_ordre " +
					"and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') >= '" + dateDebutStr + "' " +
					" and to_char(ecriture.ecr_date_saisie,'YYYYMMDD') < '" + dateFinStr + "' " +
					"and ecriture.ecr_numero > 0    " +
					"and ecriture.tjo_ordre=type_journal.tjo_ordre " +
					" and " + cond +
					" and " + condCompta +
					" and " + condOrigine +
					(condCompte != null && condCompte.length() != 0 ? " and " + condCompte : "") +
					(condGestion.length() != 0 ? " and " + condGestion : "") +
					(condTypeJournal.length() != 0 ? " and " + condTypeJournal : "");
		}

		req += " order by ecr_numero, ecd_index";
		return req;
	}

	@Override
	protected void imprimer() {
		try {
			if (myFilters.get(ORIGINE_FILTER_KEY) == null) {
				throw new DefaultClientException("Veuillez sélectionner une origine");
			}
			myFilters.put("planco", myFilters.get("pcoNum"));
			super.imprimer();
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}
}
