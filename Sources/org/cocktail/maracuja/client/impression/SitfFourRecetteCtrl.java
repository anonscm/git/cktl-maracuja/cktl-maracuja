/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.impression;

import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.FournisseurSrchDialog;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.impression.ui.SitFourRecettePanel;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

public class SitfFourRecetteCtrl extends ZKarukeraImprCtrl {
	public static final String JASPERFILENAME_SITUATION_CLIENT = "situation_client.jasper";
	private final String WINDOW_TITLE = "Impression de la situation débiteur";
	private final Dimension WINDOW_SIZE = new Dimension(500, 180);

	private final String ACTION_ID = ZActionCtrl.IDU_IMPR014;

	private final ActionSelectFournis actionSelectFournis = new ActionSelectFournis();

	private SitFourRecettePanel myPanel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public SitfFourRecetteCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		myPanel = new SitFourRecettePanel(new SitFourRecettePanelListener());
	}

	protected final void imprimer() {
		try {
			ZLogger.debug(myFilters);
			final NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			EOExercice exercice = (EOExercice) myFilters.get(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY);
			//EOFournisseur fournisseur = (EOFournisseur) myFilters.get("debiteur");
			Number resteMin = (Number) myFilters.get("ecdResteEmargerMin");
			Number resteMax = (Number) myFilters.get("ecdResteEmargerMax");

			dico.takeValueForKey(exercice.exeExercice(), "CA_EXER");
			dico.takeValueForKey(myFilters.get("CA_GESTION"), "CA_GESTION");
			String debiteur = (String) myFilters.get("debiteur");
			if (debiteur != null) {
				System.out.println(debiteur + "-->");
				debiteur = ZStringUtil.toBasicString(debiteur, ZStringUtil.defaultAcceptChars(), '_');
				System.out.println(debiteur);
				dico.takeValueForKey(debiteur + "%", "CA_CODE_FOUR");
			}
			//dico.put("CA_GESTION", exercice.exeExercice());
			if (resteMin != null) {
				dico.takeValueForKey(new BigDecimal(resteMin.doubleValue()), "CA_MONTANT_MIN");
			}
			if (resteMax != null) {
				dico.takeValueForKey(new BigDecimal(resteMax.doubleValue()), "CA_MONTANT_MAX");
			}

			final String filePath = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, JASPERFILENAME_SITUATION_CLIENT, "situation_client.pdf", null, getMyDialog(), null);

			//	final String filePath = ReportFactoryClient.imprimerSitFourRecette(getEditingContext(), myApp.temporaryDir, dico, myFilters, getMyDialog());
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	protected final void initFilters() {
		//myFilters.clear();
		myFilters.put(ZKarukeraImprCtrl.EXERCICE_FILTER_KEY, getExercice());
		//        myFilters.put(ZKarukeraImprCtrl.DATE_FIN_FILTER_KEY,  ZDateUtil.getMinOf2Dates(ZDateUtil.getToday().getTime(), ZDateUtil.getLastDayOfYear(getExercice().exeExercice().intValue()))  );
	}

	private final void selectFournis() {
		final FournisseurSrchDialog myFournisseurSrchDialog = new FournisseurSrchDialog(getMyDialog(), "Sélection d'un client", getEditingContext());
		if (myFournisseurSrchDialog.open() == ZEOSelectionDialog.MROK) {
			try {
				myFilters.put("fournisseur", (EOFournisseur) myFournisseurSrchDialog.getSelectedEOObject());
				myPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
		myFournisseurSrchDialog.dispose();
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			initFilters();
			myPanel.updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	public final class ActionSelectFournis extends AbstractAction {

		public ActionSelectFournis() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un fournisseur");
		}

		public void actionPerformed(ActionEvent e) {
			selectFournis();
		}

	}

	private final class SitFourRecettePanelListener implements SitFourRecettePanel.ISitFourRecetteListener {

		public Action actionImprimer() {
			return actionImprimer;
		}

		public Action actionSelectFournis() {
			return actionSelectFournis;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.impression.ui.JournalGeneralPanel.IJournalGeneralPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

	}

	protected String getActionId() {
		return ACTION_ID;
	}

	protected ZKarukeraPanel myPanel() {
		return myPanel;
	}

	protected Dimension getWindowSize() {
		return WINDOW_SIZE;
	}

	protected String getWindowTitle() {
		return WINDOW_TITLE;
	}
}
