package org.cocktail.maracuja.client.remotecall;

import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;

public class ServerCallPaiement extends ServerCall {

	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + "remoteDelegatePaiement";
	}

	public static Integer clientSideRequestNextNumeroPaiement(EOEditingContext ec, EOComptabilite comptabilite, EOExercice exercice) {
		return (Integer) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestNextNumeroPaiement", new Class[] {
				EOEnterpriseObject.class, EOEnterpriseObject.class
		}, new Object[] {
				comptabilite, exercice
		}, false);
	}

}
