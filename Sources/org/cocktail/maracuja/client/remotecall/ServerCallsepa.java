/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.remotecall;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import org.cocktail.fwkcktlcompta.client.metier.EOSepaSddParam;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheance;
import org.cocktail.fwkcktlcompta.common.sepasdd.remises.SepaSddEcheancesCombinaison;
import org.cocktail.maracuja.client.metier.EOVirementParamSepa;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Gère les appels au serveur pour sepa.
 * 
 * @author rprin
 */
public class ServerCallsepa extends ServerCall {

	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + "remoteDelegateSepa";
	}

	/**
	 * @param ec
	 * @param virementParamSepa
	 * @param dateTimeCreation
	 * @param lesDepenses
	 * @param lesOrdresDePaiment
	 * @param montantTotal
	 * @param nbTransactions
	 * @param dateExecutionDemandee
	 * @param numeroDeFichierDansLaJournee3Car
	 * @param identifiantFichierVirement
	 * @param optionRegroupementLignes
	 * @return Un dictionaire constitué d'une String (contenu) et d'un Integer (nbTransactions)
	 * @throws IOException
	 */
	public static NSDictionary clientSideRequestGenereVirementSepa(String exeOrdre, String paiNumero, EOEditingContext ec, EOVirementParamSepa virementParamSepa, NSTimestamp dateTimeCreation, NSArray lesDepenses, NSArray lesOrdresDePaiment, BigDecimal montantTotal,
			Integer nbTransactions, NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car,
			String identifiantFichierVirement, Integer optionRegroupementLignes) throws IOException {

		NSArray depensesGids = ZEOUtilities.globalIDsForObjects(ec, lesDepenses);
		NSArray odpGids = ZEOUtilities.globalIDsForObjects(ec, lesOrdresDePaiment);

		NSDictionary res = (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestGenereVirementSepa", new Class[] {
				String.class, String.class, EOEnterpriseObject.class, NSTimestamp.class, NSArray.class, NSArray.class, BigDecimal.class, Integer.class, NSTimestamp.class, String.class, String.class, Integer.class
		}, new Object[] {
				exeOrdre, paiNumero, virementParamSepa, dateTimeCreation, depensesGids, odpGids, montantTotal, nbTransactions, dateExecutionDemandee, numeroDeFichierDansLaJournee3Car, identifiantFichierVirement, optionRegroupementLignes
		}, false);

		return res;
	}

	/**
	 * Le nombre de paiement déjà enregistrés dans la journée en fonction du paramètre.
	 */
	public static Integer clientSideRequestNbPaiementJour(EOEditingContext ec, EOVirementParamSepa virementParamSepa, NSTimestamp dateCreation) {
		return (Integer) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestNbPaiementJour", new Class[] {
				EOEnterpriseObject.class, NSTimestamp.class
		}, new Object[] {
				virementParamSepa, dateCreation
		}, false);

	}

	public static NSDictionary clientSideRequestGenerePrelevementsSepa(EOEditingContext ec, EOEnterpriseObject exercice, EOEnterpriseObject comptabilite, EOEnterpriseObject utilisateur, NSTimestamp dateCreation, EOEnterpriseObject sepaSddParam,
			Map<SepaSddEcheancesCombinaison, NSArray> echeancesRegroupeesParCombinaison)
			throws Exception {

		NSMutableDictionary dico = new NSMutableDictionary();
		for (SepaSddEcheancesCombinaison combinaison : echeancesRegroupeesParCombinaison.keySet()) {
			dico.takeValueForKey(ZEOUtilities.globalIDsForObjects(ec, echeancesRegroupeesParCombinaison.get(combinaison)), combinaison.toString());
		}

		NSDictionary res = (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestGenerePrelevementsSepa", new Class[] {
				EOEnterpriseObject.class, EOEnterpriseObject.class, EOEnterpriseObject.class, NSTimestamp.class, EOEnterpriseObject.class, NSDictionary.class
		}, new Object[] {
				exercice, comptabilite, utilisateur, dateCreation, sepaSddParam, dico
		}, false);

		return res;
	}

	public static NSDictionary clientSideRequestSepaSddGenereEcrituresReleve(EOEditingContext ec, EOEnterpriseObject exercice, EOEnterpriseObject comptabilite, EOEnterpriseObject utilisateur, NSTimestamp dateCreation, EOEnterpriseObject pcoDebitForRejet, EOEnterpriseObject pcoDebitForConfirme,
			Map<ISepaSddEcheance.Etat, NSArray> echeancesRegroupeesParEtat, Boolean genererEcritures)
			throws Exception {

		NSMutableDictionary dico = new NSMutableDictionary();
		for (ISepaSddEcheance.Etat etat : echeancesRegroupeesParEtat.keySet()) {
			dico.takeValueForKey(ZEOUtilities.globalIDsForObjects(ec, echeancesRegroupeesParEtat.get(etat)), etat.toString());
		}

		NSDictionary res = (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestSepaSddGenereEcrituresReleve", new Class[] {
				EOEnterpriseObject.class, EOEnterpriseObject.class, EOEnterpriseObject.class, NSTimestamp.class, EOEnterpriseObject.class, EOEnterpriseObject.class, NSDictionary.class, Boolean.class
		}, new Object[] {
				exercice, comptabilite, utilisateur, dateCreation, pcoDebitForRejet, pcoDebitForConfirme, dico, genererEcritures
		}, false);

		return res;
	}


	public static Integer clientSideRequestNbRecouvrementJour(EOEditingContext editingContext, EOSepaSddParam sepaSddParam, NSTimestamp recoDateCreation) {
		return (Integer) ((EODistributedObjectStore) editingContext.parentObjectStore()).invokeRemoteMethodWithKeyPath(editingContext, getRemoteKeyPath(), "clientSideRequestNbRecouvrementJour", new Class[] {
				EOEnterpriseObject.class, NSTimestamp.class
		}, new Object[] {
				sepaSddParam, recoDateCreation
		}, false);
	}
}
