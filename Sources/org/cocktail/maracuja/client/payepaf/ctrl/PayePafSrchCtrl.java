/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.payepaf.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.exception.VisaException;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryBordereauRejet;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaMandat;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.metier.IEOBrouillard;
import org.cocktail.maracuja.client.payepaf.ui.PayePafSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PayePafSrchCtrl extends CommonCtrl {
	private static final String TITLE = "Gestion des bordereaux de Paye à façon (PAF)";
	private final String ACTION_ID_VISA = ZActionCtrl.IDU_PAYEPAF;
	private final Dimension WINDOW_DIMENSION = new Dimension(970, 600);

	private final String TAG_VISA = "VISA PAF";
	private final String TAG_SACD_VISA = "SACD PAF";
	private final String TAG_RETENUES = "RETENUES PAF";
	private final String TAG_PAIEMENT = "PAIEMENT PAF";

	private PayePafSrchPanel payePafSrchPanel;

	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionViser actionViser = new ActionViser();
	private final ActionClose actionClose = new ActionClose();
	private final ActionRetenue actionRetenue = new ActionRetenue();
	private final ActionPaiement actionPaiement = new ActionPaiement();
	private final ActionShowInfo actionShowInfo = new ActionShowInfo();
	private final ActionVoirEcritures actionVoirEcritures = new ActionVoirEcritures();
	private final ActionRejeter actionRejeter = new ActionRejeter();

	//    private final ActionRejeter actionRejeter = new ActionRejeter();

	private final HashMap myFilters;

	private NSArray comptabilites;
	private final MoisListListener moisListListener = new MoisListListener();
	private BrouillardBalanceProvider brouillardBalanceProvider;

	private BigDecimal brouillardsTotalDebit = BigDecimal.valueOf(0);
	private BigDecimal brouillardsTotalCredit = BigDecimal.valueOf(0);

	//    private ZEOComboBoxModel typeBordereauxScolModel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public PayePafSrchCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		myFilters = new HashMap();
		initSubObjects();

	}

	public void initSubObjects() throws Exception {
		//          origineModel = new ZEOComboBoxModel(new NSArray(),"oriLibelle","",null);
		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_VISA)) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour accéder aux bordereaux de Papaye. Demandez à votre administrateur "
					+ "de vous affecter les codes de gestions pour cette fonction");
		}
		brouillardBalanceProvider = new BrouillardBalanceProvider();

		payePafSrchPanel = new PayePafSrchPanel(new PayePafSrchPanelListener());

		myFilters.put("date", ZDateUtil.addMonths(ZDateUtil.getFirstDayOfMonth(ZDateUtil.nowAsDate()), -1));

	}

	public final NSArray getScolBordereaus() {
		//Créer la condition à partir des filtres
		try {
			setWaitCursor(true);
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("borNum", EOSortOrdering.CompareDescending);
			EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareAscending);
			NSArray res = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)), null, true);

			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					sort2, sort1
			}));

		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		} finally {
			setWaitCursor(false);
		}
	}

	public final NSArray getPafBrouillards() {
		try {
			setWaitCursor(true);
			NSMutableArray res = new NSMutableArray();
			if (getSelectedBordereau() != null) {
				res.addObjectsFromArray(getSelectedBordereau().bordereauBrouillards(IEOBrouillard.QUAL_VISA_PAF));
				res.addObjectsFromArray(getSelectedBordereau().allMandatBrouillards(IEOBrouillard.QUAL_VISA_PAF));
			}
			brouillardsTotalDebit = BigDecimal.valueOf(0);
			brouillardsTotalCredit = BigDecimal.valueOf(0);
			if (res.count() > 0) {
				brouillardsTotalDebit = ZEOUtilities.calcSommeOfBigDecimals(res, IEOBrouillard.DEBIT_KEY);
				brouillardsTotalCredit = ZEOUtilities.calcSommeOfBigDecimals(res, IEOBrouillard.CREDIT_KEY);
			}
			return res.immutableClone();

		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		} finally {
			setWaitCursor(false);
		}
	}

	public final EOBordereau getSelectedBordereau() {
		return payePafSrchPanel.getSelectedBordereau();
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.ScolBordereauRechercheFilterPanel.IScolBordereauRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			setWaitCursor(true);
			payePafSrchPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final String buildFiltreMois() {
		Date date = (Date) myFilters.get("date");
		int month = ZDateUtil.getMonth(date);
		String monthLib = ZDateUtil.MOIS_ANNEE[month];
		int year = ZDateUtil.getYear(date);
		return monthLib + " " + year;
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice()
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeBordereau.tboType=%@", new NSArray(new Object[] {
				EOTypeBordereau.TypeBordereauBTMS
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeBordereau.tboSousType=%@", new NSArray(new Object[] {
				EOTypeBordereau.SOUS_TYPE_PAYEPAF
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("borEtat=%@ or borEtat=%@ or borEtat=%@ or borEtat=%@", new NSArray(new Object[] {
				EOBordereau.BordereauValide,
				EOBordereau.BordereauEtatPaiement, EOBordereau.BordereauEtatRetenue, EOBordereau.BordereauEtatPaye
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("bordereauInfo.borLibelle caseInsensitiveLike %@ ", new NSArray(new Object[] {
				"*" + buildFiltreMois() + "*"
		})));

		return quals;
	}

	/**
	 * Active/desactive les actions en fonction de l'ecriture selectionnee.
	 */
	private final void refreshActions() {
		if (getSelectedBordereau() == null) {
			actionViser.setEnabled(false);
			actionRetenue.setEnabled(false);
			actionPaiement.setEnabled(false);
			actionShowInfo.setEnabled(false);
			actionVoirEcritures.setEnabled(false);
			actionRejeter.setEnabled(false);
		}
		else {
			actionViser.setEnabled(true && payePafSrchPanel.getSelectedBordereaux().count() >= 1 && (EOBordereau.BordereauValide.equals(getSelectedBordereau().borEtat())));
			actionRetenue.setEnabled(true && (payePafSrchPanel.getSelectedBordereaux().count() == 1) && (EOBordereau.BordereauEtatRetenue.equals(getSelectedBordereau().borEtat())));
			actionRejeter.setEnabled(true && (payePafSrchPanel.getSelectedBordereaux().count() == 1) && (EOBordereau.BordereauValide.equals(getSelectedBordereau().borEtat())));
			actionPaiement.setEnabled(paiementReady());
			actionShowInfo.setEnabled(true);
			actionVoirEcritures.setEnabled(true);
		}
	}

	private final NSArray payePafGetEcrituresVisaForBord(EOBordereau bordereau) {
		String filtre = TAG_VISA;
		filtre += " " + buildFiltreMois();
		filtre += " " + "Bord. " + bordereau.borNum() + " du " + bordereau.gestion().gesCode();

		String filtre2 = TAG_SACD_VISA;
		filtre2 += " " + buildFiltreMois();
		filtre2 += " " + "Bord. " + bordereau.borNum() + " du " + bordereau.gestion().gesCode();

		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("ecrNumero", EOSortOrdering.CompareAscending);

		return EOsFinder.fetchArray(getEditingContext(), EOEcriture.ENTITY_NAME, "ecrEtat=%@ and (ecrPostit=%@ or ecrPostit=%@) and typeOperation=%@ ", new NSArray(new Object[] {
				EOEcriture.ecritureValide, filtre, filtre2,
				EOsFinder.getLeTypeOperation(getEditingContext(), EOTypeOperation.TYPE_OPERATION_VISA_PAYE)
		}), new NSArray(sort),
				true);
	}

	private final NSArray payePafGetEcrituresRetenueForBord(EOBordereau bordereau) {
		String filtre = TAG_RETENUES;
		filtre += " " + buildFiltreMois();
		filtre += " " + "Bord. " + bordereau.borNum() + " du " + bordereau.gestion().gesCode();
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("ecrNumero", EOSortOrdering.CompareAscending);
		return EOsFinder.fetchArray(getEditingContext(), EOEcriture.ENTITY_NAME, "ecrEtat=%@ and ecrPostit=%@ and typeOperation=%@", new NSArray(new Object[] {
				EOEcriture.ecritureValide, filtre, EOsFinder.getLeTypeOperation(getEditingContext(), EOTypeOperation.TYPE_OPERATION_VISA_PAYE)
		}),
				new NSArray(sort),
				true);
	}

	private final NSArray payePafGetEcrituresPaiement() {
		String filtre = TAG_PAIEMENT;
		filtre += " " + buildFiltreMois();
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("ecrNumero", EOSortOrdering.CompareAscending);
		return EOsFinder.fetchArray(getEditingContext(), EOEcriture.ENTITY_NAME, "ecrEtat=%@ and ecrPostit=%@ and typeOperation=%@", new NSArray(new Object[] {
				EOEcriture.ecritureValide, filtre, EOsFinder.getLeTypeOperation(getEditingContext(), EOTypeOperation.TYPE_OPERATION_PAIEMENT)
		}),
				new NSArray(sort),
				true);
	}

	private final NSArray getNumerosOfEcritures(final NSArray ecritures) {
		return (NSArray) ecritures.valueForKey("ecrNumero");
	}

	private final void payePafViser() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			setWaitCursor(true);
			NSArray lesBords = payePafSrchPanel.getSelectedBordereaux();
			if (lesBords.count() > 0) {
				NSMutableArray allEcritures = new NSMutableArray();
				for (int i = 0; i < lesBords.count(); i++) {
					EOBordereau payePaf = (EOBordereau) lesBords.objectAtIndex(i);
					if (payePaf != null) {
						ServerProxy.clientSideRequestPayePafPasserEcritureVISABord(getEditingContext(), payePaf, getUtilisateur());
						getEditingContext().revert();
						getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
								getEditingContext().globalIDForObject(payePaf)
						}));

						//Récupérer les écritures générées
						allEcritures.addObjectsFromArray(payePafGetEcrituresVisaForBord(payePaf));
					}
				}
				payePafSrchPanel.updateData();

				String msgFin;
				if (allEcritures.count() == 0) {
					throw new DefaultClientException("Problème : Visa effectué mais aucune écriture n'a été récupérée.");
				}
				else if (allEcritures.count() == 1) {
					msgFin = "L'écriture n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " a été créée lors du visa.";
				}
				else {
					msgFin = "Les écritures n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " ont été créées lors du visa.";
				}
				showInfoDialog(msgFin);

			}
			else {
				throw new DefaultClientException("Aucun bordereau sélectionné.");
			}

		} catch (Exception e) {
			getEditingContext().revert();
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void payePafRetenue() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau payePaf = getSelectedBordereau();
			ZLogger.debug(payePaf);

			if (payePaf != null) {
				if (!EOBordereau.BordereauEtatRetenue.equals(payePaf.borEtat())) {
					throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauEtatRetenue + " pour pouvoir passer les écritures de retenues/oppositions.");
				}

				int choix = showConfirmationCancelDialog("Confirmation", "Souhaitez-vous passer les écritures de retenues/oppositions pour le bordereau n°" + payePaf.borNum() + " ?", ZMsgPanel.BTLABEL_NO);

				if (choix == ZMsgPanel.MR_CANCEL) {
					return;
				}
				else if (choix == ZMsgPanel.MR_YES) {
					ServerProxy.clientSideRequestPayePafPasserEcritureOppRetBord(getEditingContext(), payePaf, "O", getUtilisateur());
				}
				else if (choix == ZMsgPanel.MR_NO) {
					ServerProxy.clientSideRequestPayePafPasserEcritureOppRetBord(getEditingContext(), payePaf, "N", getUtilisateur());
				}
				getEditingContext().revert();
				getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
						getEditingContext().globalIDForObject(payePaf)
				}));
				payePafSrchPanel.updateData();

				String msgFin;
				if (choix == ZMsgPanel.MR_YES) {
					NSArray allEcritures = payePafGetEcrituresRetenueForBord(payePaf);
					if (allEcritures.count() == 0) {
						throw new Exception("Problème : création des écritures de retenue/oppositions théoriquement effectué mais aucune écriture n'a été récupérée.");
					}
					else if (allEcritures.count() == 1) {
						msgFin = "L'écriture n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " a été créée pour les retenues/oppositions.";
					}
					else {
						msgFin = "Les écritures n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " ont été créées pour les retenues/oppositions.";
					}
				}
				else {
					msgFin = "Les écritures de retenues/oppositions n'ont pas été générées à votre demande.";
				}
				showInfoDialog(msgFin);
			}
			else {
				throw new DefaultClientException("Aucun bordereau sélectionné.");
			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void payePafRejeter() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			getEditingContext().revert();
			boolean supprimerDepenses = false;
			EOBordereau papaye = getSelectedBordereau();
			ZLogger.debug(papaye);

			EOTypeBordereau typeBdRejet = FinderVisa.leTypeBordereauBTMNA(getEditingContext());

			if (papaye != null) {
				if (!EOBordereau.BordereauValide.equals(papaye.borEtat())) {
					throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauValide + " pour pouvoir le rejeter.");
				}

				//				boolean choix = showConfirmationDialog("Confirmation", "Souhaitez-vous rejeter les mandats du bordereau n°" + papaye.borNum() + " ?", ZMsgPanel.BTLABEL_NO);
				if (!showConfirmationDialog("Confirmation", "Souhaitez-vous rejeter les mandats du bordereau n°" + papaye.borNum() + " ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}

				NSArray mandats = papaye.mandats();
				//verifier que les mandats sont bien a l'état 'ATTENTE'
				EOQualifier qual = new EONotQualifier(new EOKeyValueQualifier(EOMandat.MAN_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOMandat.mandatAttente));
				NSArray res = EOQualifier.filteredArrayWithQualifier(mandats, qual);
				if (res.count() > 0) {
					throw new DefaultClientException("Certains mandats du bordereau " + papaye.borNum() + " ne sont pas a l'état ATTENTE.");
				}

				EOBordereauRejet myEOBordereauRejet = creerLeBordereauDeRejet(getEditingContext(), mandats, papaye.gestion(), getUtilisateur(), papaye.exercice(), typeBdRejet, supprimerDepenses);
				papaye.setBorEtat(EOBordereau.BordereauAnnule);
				papaye.setUtilisateurVisaRelationship(getUtilisateur());
				papaye.setBorDateVisa(new NSTimestamp(new Date()));

				if (getEditingContext().hasChanges()) {
					getEditingContext().saveChanges();
				}

				getEditingContext().revert();
				getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
						getEditingContext().globalIDForObject(papaye)
				}));
				payePafSrchPanel.updateData();

				try {
					KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
					FactoryProcessVisaMandat myFactoryProcessVisaMandat = new FactoryProcessVisaMandat(getEditingContext(), null, null, myApp.wantShowTrace(), null, false);
					myFactoryProcessVisaMandat.numeroterUnBordereauDeMandatRejet(getEditingContext(), myEOBordereauRejet, myKFactoryNumerotation, myApp.wantShowTrace());

				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU REJET...");
					e.printStackTrace();
					throw new DefaultClientException("Erreur lors de la numérotation du bordereau de rejet. \n" + e.getMessage());
				}

				try {
					String filePath = ReportFactoryClient.imprimerBtmna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(myEOBordereauRejet));
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

				String msgFin = "";
				msgFin = msgFin + "Le bordereau de rejet généré porte le numéro " + myEOBordereauRejet.brjNum() + ".";
				msgFin = msgFin + " <br>Il va s'imprimer automatiquement";
				//					wantImprimer = showConfirmationDialog("Rejet réussi", msgFin, ZMsgPanel.BTLABEL_YES);
				showInfoDialog(msgFin);
			}
			else {
				throw new DefaultClientException("Aucun bordereau sélectionné.");
			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void payePafPaiement() {

		//Vérifier si l'utilisateur a bien les droits
		try {
			//Vérifier que tous les bordereaux affichés sont à l'état PAIEMENT
			//TODO refault sur les objets pour être surs de leur état ?

			if (!paiementReady()) {
				throw new DataCheckException("Tous les bordereaux doivent être à l'état " + EOBordereau.BordereauEtatPaiement + " pour pouvoir générer les écritures de paiement.");
			}

			String filtre = buildFiltreMois();
			System.out.println(filtre);
			//
			int choix = showConfirmationCancelDialog("Confirmation", "Souhaitez-vous passer l'écriture de paiement pour le mois de " + filtre
					+ " ?\n Si vous répondez NON, les bordereaux et mandats passeront à l'état PAYE sans qu'une écriture de paiement soit générée.", "Non");

			if (choix == ZMsgPanel.MR_CANCEL) {
				return;
			}
			else if (choix == ZMsgPanel.MR_YES) {
				ServerProxy.clientSideRequestPayePafPasserEcriturePaiement(getEditingContext(), filtre, "O", getUtilisateur());
			}
			else if (choix == ZMsgPanel.MR_NO) {
				ServerProxy.clientSideRequestPayePafPasserEcriturePaiement(getEditingContext(), filtre, "N", getUtilisateur());
			}
			else {
				throw new DefaultClientException("Non reconnu.");
			}
			//            
			//            ServerProxy.clientSideRequestPayePafPasserEcriturePaiement(getEditingContext(), filtre, "O");

			NSArray lesBords = payePafSrchPanel.getBordereauListPanel().getMyDisplayGroup().displayedObjects();
			for (int i = 0; i < lesBords.count(); i++) {
				EOBordereau payePaf = (EOBordereau) lesBords.objectAtIndex(i);
				if (payePaf != null) {
					getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
							getEditingContext().globalIDForObject(payePaf)
					}));
				}
			}

			getEditingContext().revert();
			payePafSrchPanel.updateData();

			String msgFin;
			if (choix == ZMsgPanel.MR_YES) {
				NSArray allEcritures = payePafGetEcrituresPaiement();
				if (allEcritures.count() == 0) {
					throw new Exception("Problème : création des écritures de paiement théoriquement effectué mais aucune écriture n'a été récupérée.");
				}
				else if (allEcritures.count() == 1) {
					msgFin = "L'écriture n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " a été créée pour le paiement.";
				}
				else {
					msgFin = "Les écritures n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " ont été créées pour le paiement.";
				}
			}
			else {
				msgFin = "Les écritures de paiement n'ont pas été généres à votre demande.";
			}

			//			NSArray allEcritures = payePafGetEcrituresPaiement();
			//			if (allEcritures.count() == 0) {
			//				throw new Exception("Problème : création des écritures de paiement théoriquement effectué mais aucune écriture n'a été récupérée.");
			//			}
			//			else if (allEcritures.count() == 1) {
			//				msgFin = "L'écriture n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " a été créée pour le paiement.";
			//			}
			//			else {
			//				msgFin = "Les écritures n°" + getNumerosOfEcritures(allEcritures).componentsJoinedByString(", ") + " ont été créées pour le paiement.";
			//			}

			showInfoDialog(msgFin);

		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		} finally {
			try {
				payePafSrchPanel.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
			setWaitCursor(false);
		}

	}

	private final void payePafShowInfos() {
		try {
			EOBordereau bordereau = getSelectedBordereau();
			if (bordereau == null) {
				throw new DefaultClientException("Aucun bordereau de sélectionné.");
			}
			String msgFin;
			msgFin = "Ecriture(s) de visa : ";
			NSArray visas = payePafGetEcrituresVisaForBord(bordereau);
			if (visas.count() == 0) {
				msgFin += "non passées";
			}
			else {
				msgFin += getNumerosOfEcritures(visas).componentsJoinedByString(",");
			}
			msgFin += "\n<br>";

			msgFin += "Ecriture(s) de retenues/oppositions : ";
			NSArray retrenues = payePafGetEcrituresRetenueForBord(bordereau);
			if (retrenues.count() == 0) {
				msgFin += "non passées";
			}
			else {
				msgFin += getNumerosOfEcritures(retrenues).componentsJoinedByString(",");
			}
			msgFin += "\n<br>";

			msgFin += "Ecriture(s) de paiement : ";
			NSArray paiements = payePafGetEcrituresPaiement();
			if (paiements.count() == 0) {
				msgFin += "non passées";
			}
			else {
				msgFin += getNumerosOfEcritures(paiements).componentsJoinedByString(",");
			}
			msgFin += "\n<br>";
			showInfoDialog(msgFin);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void payePafOuvreEcritures() {
		try {
			EOBordereau bordereau = getSelectedBordereau();
			if (bordereau == null) {
				throw new DefaultClientException("Aucun bordereau de sélectionné.");
			}
			NSArray visas = payePafGetEcrituresVisaForBord(bordereau);
			NSArray retrenues = payePafGetEcrituresRetenueForBord(bordereau);
			NSArray paiements = payePafGetEcrituresPaiement();

			NSMutableArray allEcritures = new NSMutableArray();
			allEcritures.addObjectsFromArray(visas);
			allEcritures.addObjectsFromArray(retrenues);
			allEcritures.addObjectsFromArray(paiements);

			if (allEcritures.count() == 0) {
				showInfoDialog("Aucune écriture n'a été passée pour le bordereau sélectionné.");
				return;
			}

			if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {
				EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
				win.openDialog(getMyDialog(), allEcritures);
			}
			else {
				throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public final void resetFilter() {
		myFilters.clear();
	}

	private final boolean paiementReady() {
		NSArray bords = payePafSrchPanel.getBordereauListPanel().getMyDisplayGroup().displayedObjects();
		boolean etatPaiement = true;

		EOQualifier qualPaiement = EOQualifier.qualifierWithQualifierFormat(EOBordereau.BOR_ETAT_KEY + "=%@", new NSArray(new Object[] {
				EOBordereau.BordereauEtatPaiement
		}));
		EOQualifier qualPaye = EOQualifier.qualifierWithQualifierFormat(EOBordereau.BOR_ETAT_KEY + "=%@", new NSArray(new Object[] {
				EOBordereau.BordereauEtatPaye
		}));

		int nbPaiements = EOQualifier.filteredArrayWithQualifier(bords, qualPaiement).count();
		int nbPayes = EOQualifier.filteredArrayWithQualifier(bords, qualPaye).count();

		if (nbPaiements == bords.count()) {
			etatPaiement = true;
		}
		else if (nbPayes == bords.count()) {
			etatPaiement = false;
		}
		else if (nbPayes + nbPaiements == bords.count()) {
			etatPaiement = true;
		}
		else {
			etatPaiement = false;
		}
		//        
		//        for (int i = 0; i < bords.count() && etatPaiement; i++) {
		//            EOBordereau element = (EOBordereau) bords.objectAtIndex(i);
		//            if (!EOBordereau.BordereauEtatPaiement.equals(element.borEtat())) {
		//                etatPaiement = false;
		//            }
		//        }
		return etatPaiement;
	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Générer les écritures de visa du/des bordereau(x) sélectionné(s)");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			payePafViser();
		}
	}

	private final class ActionVoirEcritures extends AbstractAction {
		public ActionVoirEcritures() {
			super("Ecritures");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir le détail des écritures générées à partir du bordereau sélectionné");
			setEnabled(true);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			payePafOuvreEcritures();
		}
	}

	private final class PayePafSrchPanelListener implements PayePafSrchPanel.IPayePafSrchPanelListener {

		public NSArray getBordereaux() {
			return getScolBordereaus();
		}

		public HashMap getFilters() {
			return myFilters;
		}

		public Action actionSrch() {
			return actionSrch;
		}

		public Action getActionViser() {
			return actionViser;
		}

		public Action getActionRejeter() {
			return actionRejeter;
		}

		public void onBordereauSelectionChanged() {
			try {
				refreshActions();
				payePafSrchPanel.getBrouillardListPanel().updateData();
				//                payePafSrchPanel.updateMandatList();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public ArrayList getFirstDaysOfMonth() {
			return ZDateUtil.getFirstDaysOfMonth(getExercice().exeExercice().intValue());
		}

		public Action getActionClose() {
			return actionClose;
		}

		public ActionListener getMoisListListener() {
			return moisListListener;
		}

		public Action getActionRetenue() {
			return actionRetenue;
		}

		public Action getActionPaiement() {
			return actionPaiement;
		}

		public Action getActionShowInfo() {
			return actionShowInfo;
		}

		public Action getActionVoirEcritures() {
			return actionVoirEcritures;
		}

		public NSArray getBrouillards() {
			return getPafBrouillards();
		}

		public IZPanelBalanceProvider getBrouillardBalanceProvider() {
			return brouillardBalanceProvider;
		}
	}

	public PayePafSrchPanel getPayePafSrchPanel() {
		return payePafSrchPanel;
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		payePafSrchPanel.setMyDialog(win);
		payePafSrchPanel.setPreferredSize(WINDOW_DIMENSION);
		payePafSrchPanel.initGUI();
		win.setContentPane(payePafSrchPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		win.addWindowListener(new PayePafWindowListener());
		try {
			//            payePafAdmSrchCtrl.getScolBordereauAdmSrchPanel().updateData();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final class ActionShowInfo extends AbstractAction {

		public ActionShowInfo() {
			super("Infos");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Voir les numéros des écritures associées au bordereau sélectionné.");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			payePafShowInfos();
		}

	}

	private final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionRetenue extends AbstractAction {

		public ActionRetenue() {
			super("Retenue");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Générer les écritures de retenue/oppositions");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			payePafRetenue();
		}

	}

	private final class ActionPaiement extends AbstractAction {

		public ActionPaiement() {
			super("Paiement");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Générer les écritures de paiement (pour tous les bordereaux)");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			payePafPaiement();
		}

	}

	private final class ActionRejeter extends AbstractAction {

		public ActionRejeter() {
			super("Rejeter");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rejeter le bordereau entier.");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			payePafRejeter();
		}

	}

	private final class MoisListListener implements ActionListener {

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				myFilters.put("date", payePafSrchPanel.getMoisList().getSelectedItem());
				payePafSrchPanel.getBordereauListPanel().updateData();

			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private final class PayePafWindowListener extends WindowAdapter {
		// This method is called after a window has been opened
		public void windowOpened(WindowEvent evt) {
			try {
				onSrch();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		// This method is called when the user clicks the close button
		public void windowClosing(WindowEvent evt) {
			return;
		}

		// This method is called after a window is closed
		public void windowClosed(WindowEvent evt) {
			return;
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return payePafSrchPanel;
	}

	public String title() {
		return TITLE;
	}

	/**
	 * Cree et renvoie un bordereau de rejet pour les mandats passes en parametre.
	 * 
	 * @param ed
	 * @param lesMandatsRejetes
	 * @param gestion
	 * @param utilisateur
	 * @param exercice
	 * @param typBordereau
	 * @param supprimeDepenses
	 * @return
	 * @throws VisaException
	 */
	private EOBordereauRejet creerLeBordereauDeRejet(EOEditingContext ed, NSArray lesMandatsRejetes, EOGestion gestion, EOUtilisateur utilisateur, EOExercice exercice, EOTypeBordereau typBordereau, boolean supprimeDepenses) throws VisaException {
		// verifications
		if (lesMandatsRejetes.count() == 0) {
			throw new VisaException(VisaException.pasDeMandatsARejeter);
		}

		FactoryBordereauRejet maFactoryBordereauRejet = new FactoryBordereauRejet(myApp.wantShowTrace());

		// creation du bordereau de rejet
		final EOBordereauRejet newEOBordereauRejet = maFactoryBordereauRejet.creerBordereauRejet(ed, EOBordereauRejet.BordereauRejetValide, new Integer(0), exercice, gestion, typBordereau, utilisateur);

		if (newEOBordereauRejet == null) {
			throw new VisaException(VisaException.problemeCreationBordereauRejet);
		}

		// passer les mandats a Annule
		int i = 0;
		while (i < lesMandatsRejetes.count()) {
			EOMandat mandat = (EOMandat) lesMandatsRejetes.objectAtIndex(i);
			mandat.setManEtat(EOMandat.mandatAnnule);
			mandat.setManMotifRejet("Rejet par comptable");
			mandat.setBordereauRejetRelationship(newEOBordereauRejet);
			//On marque les depenses à "Supprimer" si nécessaire
			if (supprimeDepenses) {
				NSArray depenses = mandat.depenses();
				for (int j = 0; j < depenses.count(); j++) {
					EODepense dep = (EODepense) depenses.objectAtIndex(j);
					dep.setDepSuppression(ZConst.OUI);
				}
			}
			i++;
		}

		return newEOBordereauRejet;
	}

	private class BrouillardBalanceProvider implements IZPanelBalanceProvider {

		public BigDecimal getDebitValue() {
			return brouillardsTotalDebit;
		}

		public BigDecimal getCreditValue() {
			return brouillardsTotalCredit;
		}

	}
}
