/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.payepaf.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.metier.EOBordereauBrouillard;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;


public class PayePafBrouillardListPanel extends ZKarukeraTablePanel {

	public static final String COL_ETAT = "bobEtat";
	public static final String COL_GESTION = "gestion.gesCode";
	public static final String COL_PCONUM = "planComptableVisa.pcoNum";
	public static final String COL_DEBIT = "debit";
	public static final String COL_CREDIT = "credit";
	public static final String COL_OPERATION = "operation";
	private ZPanelBalance myZPanelBalanceNew;

	/**
	     * 
	     */
	public PayePafBrouillardListPanel(IPayePafBrouillardListPanelListener listener) {
		super(listener);

		ZEOTableModelColumn colGescode = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code gestion", 84);
		colGescode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumnWithProvider colPcoNum = new ZEOTableModelColumnWithProvider("Compte", new pcoProvider(), 80);
		//ZEOTableModelColumn colPcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imp.", 60);
		colPcoNum.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colOperation = new ZEOTableModelColumn(myDisplayGroup, COL_OPERATION, "Opération", 84);
		colOperation.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumnWithProvider colDebit = new ZEOTableModelColumnWithProvider("Débit", new colDebitProvider(), 80);
		colDebit.setAlignment(SwingConstants.RIGHT);
		colDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumnWithProvider colCredit = new ZEOTableModelColumnWithProvider("Crédit", new colCreditProvider(), 80);
		colCredit.setAlignment(SwingConstants.RIGHT);
		colCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colCredit.setColumnClass(BigDecimal.class);

		colsMap.clear();
		colsMap.put(COL_GESTION, colGescode);
		colsMap.put(COL_OPERATION, colOperation);
		colsMap.put(COL_PCONUM, colPcoNum);
		colsMap.put(COL_DEBIT, colDebit);
		colsMap.put(COL_CREDIT, colCredit);

		myZPanelBalanceNew = new ZPanelBalance(listener.getBalanceProvider());
	}

	private class colDebitProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			Object element = myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (element instanceof EOBordereauBrouillard) {
				EOBordereauBrouillard el = (EOBordereauBrouillard) element;
				if (ZConst.SENS_DEBIT.equals(el.bobSens())) {
					return el.bobMontant();
				}
			}
			else {
				EOMandatBrouillard el = (EOMandatBrouillard) element;
				if (ZConst.SENS_DEBIT.equals(el.mabSens())) {
					return el.mabMontant();
				}
			}
			return null;
		}

	}

	private class colCreditProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			Object element = myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (element instanceof EOBordereauBrouillard) {
				EOBordereauBrouillard el = (EOBordereauBrouillard) element;
				if (ZConst.SENS_CREDIT.equals(el.bobSens())) {
					return el.bobMontant();
				}
			}
			else {
				EOMandatBrouillard el = (EOMandatBrouillard) element;
				if (ZConst.SENS_CREDIT.equals(el.mabSens())) {
					return el.mabMontant();
				}
			}
			return null;
		}

	}

	private class pcoProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
		 */
		public Object getValueAtRow(int row) {
			Object element = myDisplayGroup.displayedObjects().objectAtIndex(row);
			if (element instanceof EOBordereauBrouillard) {
				EOBordereauBrouillard el = (EOBordereauBrouillard) element;
				return el.planComptableVisa().pcoNum();
			}
			else {
				EOMandatBrouillard el = (EOMandatBrouillard) element;
				return el.planComptable().pcoNum();
			}
			//return null;
		}

	}

	@Override
	public void initGUI() {
		super.initGUI();
		myZPanelBalanceNew.initGUI();
		this.add(myZPanelBalanceNew, BorderLayout.SOUTH);
	}

	@Override
	public void updateData() throws Exception {
		super.updateData();
		myZPanelBalanceNew.updateData();
	}

	public interface IPayePafBrouillardListPanelListener extends IZKarukeraTablePanelListener {

		IZPanelBalanceProvider getBalanceProvider();

	}
}
