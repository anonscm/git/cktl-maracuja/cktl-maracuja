/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel;
import org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel;
import org.cocktail.maracuja.client.cheques.ui.ChequeBrouillardListPanel;
import org.cocktail.maracuja.client.cheques.ui.ChequeListPanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.FactoryBordereauCheque;
import org.cocktail.maracuja.client.factory.FactoryBrouillardCheque;
import org.cocktail.maracuja.client.factory.FactoryCheque;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOCheque;
import org.cocktail.maracuja.client.metier.EOChequeBrouillard;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class BdChequeSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'un bordereau de chèques";
	private static final Dimension WINDOW_DIMENSION = new Dimension(847, 700);
	private static final int MODE_NEW = 0;
	private static final int MODE_MODIF = 1;
	private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_CHESA;

	private int modeSaisie = MODE_NEW;

	private BdChequeSaisiePanel bdChequeSaisiePanel;

	private final BdChequeSaisiePanelListener bdChequeSaisiePanelListener = new BdChequeSaisiePanelListener();
	private final BdChequeFormPanelListener bdChequeFormPanelListener = new BdChequeFormPanelListener();
	private final ChequeListPanelListener chequeListPanelListener = new ChequeListPanelListener();
	private final ChequeBrouillardListPanelListener chequeBrouillardListPanelListener = new ChequeBrouillardListPanelListener();
	private final BalanceGlobaleProvider balanceGlobaleProvider = new BalanceGlobaleProvider();

	private EOBordereau currentBdCheque;

	//    private FactoryProcessBordereauDeCheques factoryProcessBordereauDeCheques;
	private FactoryBordereauCheque factoryBordereauCheque;
	private FactoryBrouillardCheque factoryBrouillardCheque;
	private FactoryCheque factoryCheque;

	private NSArray planComptables;
	private NSArray gestions;
	private NSArray comptabilites;

	private ZEOComboBoxModel gestionsModel;

	private final ActionValider actionValider = new ActionValider();
	private final ActionClose actionClose = new ActionClose();
	private final ActionDeleteBrouillard actionDeleteBrouillard = new ActionDeleteBrouillard();
	private final ActionAddBrouillard actionAddBrouillard = new ActionAddBrouillard();
	private final ActionDeleteCheque actionDeleteCheque = new ActionDeleteCheque();
	private final ActionAddCheque actionAddCheque = new ActionAddCheque();
	private final ActionModifyCheque actionModifyCheque = new ActionModifyCheque();
	private final ActionDupliquerCheque actionDupliquerCheque = new ActionDupliquerCheque();
	private final ActionPlancoSelect actionPlancoSelect = new ActionPlancoSelect();

	private EODisplayGroup dgPlancomptable;
	private PcoSelectDlg planComptableSelectionDialog;
	private boolean modified = false;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public BdChequeSaisieCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);

		dgPlancomptable = new EODisplayGroup();

		revertChanges();

		//        factoryProcessBordereauDeCheques = new FactoryProcessBordereauDeCheques(myApp.wantShowTrace());
		factoryBordereauCheque = new FactoryBordereauCheque(myApp.wantShowTrace());
		factoryCheque = new FactoryCheque(myApp.wantShowTrace());
		factoryBrouillardCheque = new FactoryBrouillardCheque(myApp.wantShowTrace());

		//        planComptables = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), new EOOrQualifier(new NSArray(new Object[]{EOPlanComptableExer.QUAL_CLASSE_4, EOPlanComptableExer.QUAL_CLASSE_5 })), false);
		planComptables = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), null, false);

		//        
		//      Si interdiction comptes 6 et 7, on limite les plancos possibles
		String limiteClasse = (String) myApp.getParametres().valueForKey("PCOCLASSE_SAISIE_BLOQUEE");
		if (limiteClasse != null && limiteClasse.length() != 0) {
			NSArray classesInterdites = NSArray.componentsSeparatedByString(limiteClasse, ",");
			ZLogger.debug("classesInterdites", classesInterdites);
			NSMutableArray classesFilters = new NSMutableArray();
			for (int i = 0; i < classesInterdites.count(); i++) {
				String element = (String) classesInterdites.objectAtIndex(i);
				if (element != null && element.length() > 0) {
					classesFilters.addObject(EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + element + "*'", null));
				}
			}
			if (classesFilters.count() > 0) {
				EOQualifier qual = new EONotQualifier(new EOOrQualifier(classesFilters));
				planComptables = EOQualifier.filteredArrayWithQualifier(planComptables, qual);
			}
		}
		//        

		gestions = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_SAISIE);
		comptabilites = myApp.appUserInfo().getAuthorizedComptabilitesForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_SAISIE);

		if (gestions.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour saisir un bordereau de chèque. Demandez à votre administrateur " +
					"de vous affecter les codes de gestions pour cette fonction");
		}
		gestionsModel = new ZEOComboBoxModel(gestions, EOGestion.GES_CODE_KEY, null, null);

		bdChequeSaisiePanel = new BdChequeSaisiePanel(bdChequeSaisiePanelListener,
				bdChequeFormPanelListener,
				chequeListPanelListener,
				chequeBrouillardListPanelListener,
				balanceGlobaleProvider);
	}

	/**
	 * Crée un nouvel objet EcritureDetail et active les éléments d'interface nécessaires à sa saisie.
	 */
	private EOChequeBrouillard brouillardAjouter() {
		try {
			//Vérifier qu'on est sur un cheque
			final EOCheque cheque = (EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject();
			if (cheque == null) {
				throw new DefaultClientException("Aucun chèque de sélectionné");
			}
			if (bdChequeSaisiePanel.getChequeBrouillardListPanel().isEditing()) {
				bdChequeSaisiePanel.getChequeBrouillardListPanel().stopCellEditing();
			}

			final EOChequeBrouillard brouillard = factoryBrouillardCheque.creerChequeBrouillardVide(getEditingContext(), ZConst.SENS_CREDIT, cheque, myApp.appUserInfo().getCurrentExercice(), ZConst.BIGDECIMAL_ZERO.setScale(2));
			brouillard.setChbLibelle(cheque.bordereau().borLibelle() + " Tireur " + cheque.cheNomTireur() + " Num. " + cheque.cheNumeroCheque());
			bdChequeSaisiePanel.getChequeBrouillardListPanel().ajouterInDg(brouillard);
			refreshSaveAction();
			return brouillard;
		} catch (Exception e) {
			showErrorDialog(e);
		}
		return null;

	}

	private final void brouillardSupprimer() {
		try {
			//Vérifier qu'on est sur un brouillard
			EOChequeBrouillard chequeBrouillard = (EOChequeBrouillard) bdChequeSaisiePanel.getChequeBrouillardListPanel().getSelectedObject();
			if (chequeBrouillard == null) {
				throw new DefaultClientException("Aucun brouillard de sélectionné");
			}

			chequeBrouillard.removeObjectFromBothSidesOfRelationshipWithKey(chequeBrouillard.cheque(), "cheque");
			getEditingContext().deleteObject(chequeBrouillard);
			refreshSaveAction();
			bdChequeSaisiePanel.getChequeBrouillardListPanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final EOCheque createChequeFromDico(final HashMap dico) throws Exception {
		ZLogger.debug(dico);

		NSTimestamp dcheque = null;
		if (dico.get("cheDateCheque") != null) {
			dcheque = new NSTimestamp((Date) dico.get("cheDateCheque"));
		}

		final EOCheque cheque = factoryCheque.creerUnCheque(getEditingContext(),
				(String) dico.get("banqAgence"),
				(String) dico.get("banqCode"),
				(String) dico.get("banqLibelle"),
				currentBdCheque,
				dcheque,
				ZDateUtil.now(),
				((BigDecimal) dico.get("cheMontant")).setScale(2, BigDecimal.ROUND_HALF_UP),
				(String) dico.get("cheNomTireur"),
				(String) dico.get("cheNumeroCheque"),
				(String) dico.get("cheNumeroCompte"),
				(String) dico.get("chePrenomTireur"),
				(String) dico.get("rcptCode"),
				myApp.appUserInfo().getUtilisateur()
				);

		return cheque;
	}

	private final HashMap createDicoFromEOCheque(final EOCheque cheque) throws Exception {
		HashMap tmp = ZEOUtilities.convertEOEnterpriseObjectToHashMap(cheque);
		//        ZLogger.debug(tmp);
		return tmp;
	}

	private final void updateChequeFromDico(EOCheque cheque, final HashMap dico) throws Exception {
		NSTimestamp dcheque = null;
		if (dico.get("cheDateCheque") != null) {
			dcheque = new NSTimestamp((Date) dico.get("cheDateCheque"));
		}

		factoryCheque.modifierUnCheque(getEditingContext(),
					cheque,
					(String) dico.get("banqAgence"),
					(String) dico.get("banqCode"),
					(String) dico.get("banqLibelle"),
					dcheque,
					ZDateUtil.now(),
					(BigDecimal) dico.get("cheMontant"),
					(String) dico.get("cheNomTireur"),
					(String) dico.get("cheNumeroCheque"),
					(String) dico.get("cheNumeroCompte"),
					(String) dico.get("chePrenomTireur"),
					(String) dico.get("rcptCode"),
					myApp.appUserInfo().getUtilisateur()
				);
	}

	private final void chequeAjouter() {
		try {
			bdChequeSaisiePanel.getChequeBrouillardListPanel().stopCellEditing();

			final ChequeSaisieCtrl chequeSaisieCtrl = new ChequeSaisieCtrl(getEditingContext());
			final HashMap chequeDico = chequeSaisieCtrl.openDialogNew(getMyDialog());

			if (chequeDico != null) {
				final EOCheque cheque = createChequeFromDico(chequeDico);

				//Refresh de la liste des cheques
				bdChequeSaisiePanel.getChequeListPanel().updateData();
				bdChequeSaisiePanel.getChequeListPanel().selectObject(cheque);
				bdChequeSaisiePanel.getChequeListPanel().updateData();

				//Creer les brouillards en auto
				EOChequeBrouillard brouillard = brouillardAjouter();
				if (brouillard != null) {
					factoryBrouillardCheque.chequeBrouillardVideSetGestion(brouillard, currentBdCheque.gestion());
					factoryBrouillardCheque.chequeBrouillardVideSetMontant(brouillard, cheque.cheMontant());
				}
				refreshSaveAction();
				bdChequeSaisiePanel.getChequeBrouillardListPanel().updateData();
				ZLogger.debug(getEditingContext());

			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void chequeSupprimer() {
		try {

			EOCheque cheque = (EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject();
			if (cheque == null) {
				throw new DefaultClientException("Aucun chèque de sélectionné");
			}
			bdChequeSaisiePanel.getChequeBrouillardListPanel().cancelCellEditing();
			factoryCheque.annulerUnCheque(getEditingContext(), cheque, getUtilisateur());
			refreshSaveAction();
			bdChequeSaisiePanel.getChequeListPanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void chequeModifier() {
		try {
			EOCheque cheque = (EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject();
			if (cheque == null) {
				throw new DefaultClientException("Aucun chèque de sélectionné");
			}

			if (!EOCheque.etatValide.equals(cheque.cheEtat())) {
				throw new DefaultClientException("Le chèque n'est pas valide");
			}

			ChequeSaisieCtrl chequeSaisieCtrl = new ChequeSaisieCtrl(getEditingContext());
			HashMap chequeDico = createDicoFromEOCheque(cheque);
			chequeDico = chequeSaisieCtrl.openDialogModify(getMyDialog(), chequeDico);
			if (chequeDico != null) {
				updateChequeFromDico(cheque, chequeDico);

				ZLogger.debug(getEditingContext());
				refreshSaveAction();
				//Refresh de la liste des cheques
				bdChequeSaisiePanel.getChequeListPanel().updateData();

				bdChequeSaisiePanel.getBalancePanel().updateData();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void chequeDupliquer() {
		try {
			final EOCheque cheque = (EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject();
			if (cheque == null) {
				throw new DefaultClientException("Aucun chèque de sélectionné");
			}
			bdChequeSaisiePanel.getChequeBrouillardListPanel().stopCellEditing();

			if (!EOCheque.etatValide.equals(cheque.cheEtat())) {
				throw new DefaultClientException("Le chèque n'est pas valide");
			}

			final ChequeSaisieCtrl chequeSaisieCtrl = new ChequeSaisieCtrl(getEditingContext());
			final HashMap chequeDico = chequeSaisieCtrl.openDialogDuplique(getMyDialog(), createDicoFromEOCheque(cheque));
			if (chequeDico != null) {
				final EOCheque chequeNew = createChequeFromDico(chequeDico);

				//Refresh de la liste des cheques
				bdChequeSaisiePanel.getChequeListPanel().updateData();
				bdChequeSaisiePanel.getChequeListPanel().selectObject(chequeNew);
				bdChequeSaisiePanel.getChequeListPanel().updateData();

				//Creer les brouillards en auto
				final EOChequeBrouillard brouillard = brouillardAjouter();
				if (brouillard != null) {
					factoryBrouillardCheque.chequeBrouillardVideSetGestion(brouillard, currentBdCheque.gestion());
					factoryBrouillardCheque.chequeBrouillardVideSetMontant(brouillard, chequeNew.cheMontant());
					if (cheque.chequeBrouillards().count() > 0) {
						factoryBrouillardCheque.chequeBrouillardVideSetPlanComptable(brouillard, ((EOChequeBrouillard) cheque.chequeBrouillards().objectAtIndex(0)).planComptable());
					}

				}
				refreshSaveAction();
				bdChequeSaisiePanel.getChequeBrouillardListPanel().updateData();
				ZLogger.debug(getEditingContext());

			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void refreshActionsListeCheque() {
		actionModifyCheque.setEnabled(false);
		actionDupliquerCheque.setEnabled(false);
		actionDeleteCheque.setEnabled(false);
		actionAddBrouillard.setEnabled(false);
		actionDeleteBrouillard.setEnabled(false);
		EOCheque cheque = (EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject();
		if (cheque != null) {
			if (EOCheque.etatValide.equals(cheque.cheEtat())) {
				actionModifyCheque.setEnabled(true);
				actionDupliquerCheque.setEnabled(true);
				actionDeleteCheque.setEnabled(true);
				actionAddBrouillard.setEnabled(true);
			}
		}
	}

	private final void refreshActionsListeBrouillard() {
		ZLogger.debug("");
		actionDeleteBrouillard.setEnabled(false);
		EOChequeBrouillard chequeBrouillard = (EOChequeBrouillard) bdChequeSaisiePanel.getChequeBrouillardListPanel().getSelectedObject();
		if (chequeBrouillard != null) {
			actionDeleteBrouillard.setEnabled(true);
		}
	}

	//    private EOBordereau createBordereauFromDico(HashMap leDico) throws Exception {
	//        return factoryProcessBordereauDeCheques.creerUnBordereauDeCheques(
	//                getEditingContext(),
	//                new Integer(0),
	//                (EOExercice)leDico.get("exercice"),
	//                (EOGestion)leDico.get("gestion"),
	//                myApp.appUserInfo().getUtilisateur()
	//                );
	//    }

	/**
	 * @param detail
	 * @return true si le ecritrureDeatil n'a pas été saisi par l'utilisateur (ecdMontant=null, gestion==null, etc.)
	 */
	private final boolean brouillardIsEmpty(EOChequeBrouillard brouillard) {
		//	    ZLogger.debug(brouillard);
		//	    ZLogger.debug("scale brouillard = " + brouillard.chbMontant().scale());
		//	    ZLogger.debug("scale BIGDECIMAL_ZERO = " + ZConst.BIGDECIMAL_ZERO.setScale(2));

		return (brouillard.gestion() == null && brouillard.planComptable() == null && (brouillard.chbMontant() == null || ZConst.BIGDECIMAL_ZERO.setScale(2).equals(brouillard.chbMontant())));
	}

	/**
	 * Parcourt les detailecritures et supprime les details vides.
	 */
	private void chequeBrouillardsNettoyer() {
		if (currentBdCheque != null) {
			ArrayList tmp = getAllChequeBrouillardsForBordereau(currentBdCheque);
			ArrayList res = new ArrayList();

			ZLogger.debug("Tous les brouiilards", tmp);
			Iterator iter = tmp.iterator();
			while (iter.hasNext()) {
				EOChequeBrouillard element = (EOChequeBrouillard) iter.next();
				if (brouillardIsEmpty(element)) {
					res.add(element);
				}
			}
			ZLogger.debug("brouillards a supprimer", res);

			if (res.size() > 0) {
				Iterator iter2 = res.iterator();
				while (iter2.hasNext()) {
					EOChequeBrouillard element = (EOChequeBrouillard) iter2.next();
					factoryBrouillardCheque.annulerChequeBrouillard(getEditingContext(), element);
					//L'objet est vide donc on le supprime completement
					element.removeObjectFromBothSidesOfRelationshipWithKey(element.cheque(), "cheque");
					getEditingContext().deleteObject(element);
				}

			}
		}
	}

	/**
	 * Vérifications sur la validité de la saisie du bordereau
	 * 
	 * @throws Exception
	 */
	private final void checkSaisieBdCheque() throws Exception {

		//Vérifier qu'il y a un libelle
		if (currentBdCheque.borLibelle() == null || currentBdCheque.borLibelle().length() == 0) {
			throw new DataCheckException("Le libellé du bordereau est obligatoire");
		}

		//verifier qu'il y a un code gestion
		if (currentBdCheque.gestion() == null) {
			throw new DataCheckException("Le code gestion est obligatoire");
		}

		//verifier qu'il y a un plancomptable
		if (currentBdCheque.bordereauInfo().planComptableVisa() == null) {
			throw new DataCheckException("L'imputation visa est obligatoire");
		}

	}

	private final void checkSaisieCheques() throws Exception {

	}

	private final void checkSaisieChequeBrouillards() throws Exception {
		//Balayer les cheques
		NSArray cheques = getChequesValidesForBordereau(currentBdCheque);
		String erreurs = "";
		for (int i = 0; i < cheques.count(); i++) {
			EOCheque element = (EOCheque) cheques.objectAtIndex(i);
			//Vérifier qu'il y a un brouillard
			NSArray brouillards = element.chequeBrouillards();
			if (brouillards == null || brouillards.count() == 0) {
				erreurs = erreurs + "- Le chèque n° " + element.cheNumeroCheque() + " n'a pas de brouillard associé.\n";
			}
			else {
				//Vérifier l'équilibre
				BigDecimal montant = ZEOUtilities.calcSommeOfBigDecimals(brouillards, "chbMontant");
				if (!montant.equals(element.cheMontant())) {
					erreurs = erreurs + "- La somme des credits du brouillard pour le chèque n° " + element.cheNumeroCheque() + " n'est pas égale au montant du chèque. (" + element.cheMontant() + "/" + montant + ")\n";
				}
			}

			for (int j = 0; j < brouillards.count(); j++) {
				EOChequeBrouillard brouillard = (EOChequeBrouillard) brouillards.objectAtIndex(j);
				if (brouillard.planComptable() == null) {
					erreurs = erreurs + " - Un des brouillards du chèque n° " + element.cheNumeroCheque() + " n'a pas de compte défini.\n";
				}
				if (brouillard.gestion() == null) {
					erreurs = erreurs + " - Un des brouillards du chèque n° " + element.cheNumeroCheque() + " n'a pas de code gestion défini.\n";
				}
			}
		}
		if (erreurs.length() > 0) {
			throw new DataCheckException(erreurs);
		}

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			if (!showConfirmationDialog("Confirmation", "Vous n'avez pas validé le bordereau en cours de saisie, souhaitez-vous néammoins quitter cette fenêtre ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		getEditingContext().revert();
		getMyDialog().onCancelClick();
	}

	/**
     *
     */
	private final void validerSaisie() {
		try {
			if (modeSaisie == MODE_NEW) {
				validerSaisieForNew();
			}
			else {
				//Memes validations pour la modif
				validerSaisieForNew();
			}

			//On ne sort pas, mais on indique que l'objet a été modifié
			modified = true;

			//           getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void validerSaisieForNew() throws Exception {
		//si la table est en edition, on valide les modifs
		if (bdChequeSaisiePanel.getChequeBrouillardListPanel().isEditing()) {
			if (!bdChequeSaisiePanel.getChequeBrouillardListPanel().stopCellEditing()) {
				return;
			}
		}

		currentBdCheque.setBorOrdre(currentBdCheque.borNum());

		checkSaisieBdCheque();
		checkSaisieCheques();
		chequeBrouillardsNettoyer();
		checkSaisieChequeBrouillards();

		//nettoyer editingContext

		ZLogger.debug(getEditingContext());
		ZLogger.verbose("-----------------\n inserted");
		ZLogger.verbose(getEditingContext().insertedObjects());
		ZLogger.verbose("-----------------\n updated");
		ZLogger.verbose(getEditingContext().updatedObjects());
		ZLogger.verbose("-----------------\n deleted");
		ZLogger.verbose(getEditingContext().deletedObjects());
		ZLogger.verbose("");

		getEditingContext().saveChanges();

		//Si le n° du bordreau est égal à 0, on le numerote

		if (currentBdCheque.borNum().intValue() == 0) {

			KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
			try {
				myKFactoryNumerotation.getNumeroEOBordereauCheque(getEditingContext(), currentBdCheque);
			} catch (Exception e) {
				System.out.println("ERREUR LORS DE LA NUMEROTATION DU BORDEREAU DE CHEQUE ...");
				e.printStackTrace();
				throw new Exception(e);
			}
		}

		refreshSaveAction();
		bdChequeSaisiePanel.updateData();

		//        displayedBdCheques = tmpArray.immutableClone();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		bdChequeSaisiePanel.setMyDialog(getMyDialog());
		bdChequeSaisiePanel.initGUI();
		bdChequeSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(bdChequeSaisiePanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final EOBordereau openDialogNew(Window dial) {
		revertChanges();
		modified = false;
		ZKarukeraDialog win = createModalDialog(dial);
		setMyDialog(win);
		bdChequeSaisiePanel.setMyDialog(win);
		try {
			newSaisie();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return currentBdCheque;
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final EOBordereau openDialogModify(Window dial, EOBordereau bordereau) {
		revertChanges();
		modified = false;
		ZKarukeraDialog win = createModalDialog(dial);
		setMyDialog(win);
		bdChequeSaisiePanel.setMyDialog(win);
		try {
			modifySaisie(bordereau);
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return currentBdCheque;
	}

	private final void newSaisie() {
		modeSaisie = MODE_NEW;

		try {
			if (getEditingContext().hasChanges()) {
				getEditingContext().revert();
			}

			planComptableSelectionDialog = createPlanComptableSelectionDialog();
			// pb si plusieurs comptabilites (remettre gestion des comptabilies comme exercice)
			//            EOComptabilite compta = (EOComptabilite) comptabilites.objectAtIndex(0);
			EOComptabilite compta = myApp.appUserInfo().getCurrentComptabilite();
			EOGestion gestion = compta.gestion();

			if (gestions.indexOfObject(gestion) == NSArray.NotFound) {
				ZLogger.debug("gestion agence non autorisée, utilisation par defaut du premier code gestion trouvé");
				gestion = (EOGestion) gestions.objectAtIndex(0);
			}

			//On crée un objet presque vide
			currentBdCheque = factoryBordereauCheque.creerBordereauCheque(getEditingContext(), new Integer(0), myApp.appUserInfo().getCurrentExercice(), gestion, myApp.appUserInfo().getUtilisateur());

			//avec propagate primary key, pas besoin de créer l'objet             
			//	        factoryBordereauCheque.affecteBordereauInfo(currentBdCheque, factoryBordereauCheque.creerBordereauInfoVide(getEditingContext()));
			factoryBordereauCheque.affecterPlanComptableVisa(currentBdCheque, EOPlanComptableExerFinder.getPlancoExerForPcoNum(getEditingContext(), getExercice(), "5112", false).planComptable());

			bdChequeSaisiePanel.updateData();
			refreshSaveAction();
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void modifySaisie(EOBordereau bordereau) throws Exception {
		modeSaisie = MODE_MODIF;
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		planComptableSelectionDialog = createPlanComptableSelectionDialog();

		//Vérifier que le bordereau est modifiable
		if (!EOBordereau.BordereauValide.equals(bordereau.borEtat())) {
			throw new DefaultClientException("L'état du bordereau ne lui permet pas d'être modifié.");
		}

		//On crée un objet presque vide
		currentBdCheque = bordereau;
		bdChequeSaisiePanel.updateData();
		refreshSaveAction();
	}

	private final NSArray getPcoValidesForBordereauCheque() {
		final EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat("pcoNum like %@", new NSArray(new Object[] {
				"51*"
		}));
		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), qual1, false);

		//        String chapitre = "51";
		//        return ZFinder.fetchArray(getEditingContext(), EOPlanComptable.ENTITY_NAME, "pcoValidite=%@ and (pcoNum like %@)", new NSArray(new Object[]{EOPlanComptable.etatValide, chapitre+"*"} ),new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending)), false );
	}

	private final void onSelectPlancoVisa() {
		EOPlanComptable res = null;
		//on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(getPcoValidesForBordereauCheque());
		res = selectPlanComptableIndDialog(null);
		if (res != null) {
			factoryBordereauCheque.affecterPlanComptableVisa(currentBdCheque, res);
		}
		try {
			bdChequeSaisiePanel.getBdChequeFormPanel().updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	}

	//    private final ZEOSelectionDialog createPlanComptableSelectionDialog() {
	//    	Vector filterFields = new Vector(2,0);
	//    	filterFields.add(EOPlanComptable.PCO_NUM_KEY);
	//    	filterFields.add(EOPlanComptable.PCO_LIBELLE_KEY);
	//    	
	//    	Vector myCols = new Vector(2,0);
	//    	ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptable.PCO_NUM_KEY, "N°");
	//    	ZEOTableModelColumn col2 = new ZEOTableModelColumn(dgPlancomptable, EOPlanComptable.PCO_LIBELLE_KEY, "Libellé");
	//    	
	//    	myCols.add(col1);
	//    	myCols.add(col2);
	//    	
	//    	ZEOSelectionDialog dialog = new ZEOSelectionDialog( getMyDialog(), "Sélection d'une imputation" ,dgPlancomptable,myCols,"Veuillez sélectionner un compte dans la liste",filterFields);
	//    	dialog.setFilterPrefix("");
	//    	dialog.setFilterSuffix("*");
	//    	dialog.setFilterInstruction(" caseInsensitiveLike %@");
	//    	
	//    	return dialog;
	//    }

	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		if (planComptableSelectionDialog.open(planComptables) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;

		//		
		//		
		//		
		//	    EOPlanComptable res = null;
		//		setWaitCursor(true);
		//		try {
		//			planComptableSelectionDialog.getFilterTextField().setText(filter);
		//			planComptableSelectionDialog.filter();
		//			if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
		//				NSArray selections = planComptableSelectionDialog.getSelectedEOObjects();
		//
		//				if ((selections==null) || (selections.count()==0)) {
		//				    res = null;
		//				}
		//				else {
		//				    res = (EOPlanComptable) selections.objectAtIndex(0);
		//				}
		//			}
		//		}
		//		catch (Exception e) {
		//		    showErrorDialog(e);
		//        }
		//		finally {
		//		    setWaitCursor(false);
		//		}
		//		return res;
	}

	private final class BdChequeSaisiePanelListener implements BdChequeSaisiePanel.IBdChequeSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#myActionDeleteCheque()
		 */
		public Action myActionDeleteCheque() {
			return actionDeleteCheque;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#myActionAddCheque()
		 */
		public Action myActionAddCheque() {
			return actionAddCheque;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#myActionModifyCheque()
		 */
		public Action myActionModifyCheque() {
			return actionModifyCheque;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#myActionDeleteChequeBrouillard()
		 */
		public Action myActionDeleteChequeBrouillard() {
			return actionDeleteBrouillard;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSaisiePanel.IBdChequeSaisiePanelListener#myActionAddChequeBrouillard()
		 */
		public Action myActionAddChequeBrouillard() {
			return actionAddBrouillard;
		}

		public Action myActionDupliqueCheque() {
			return actionDupliquerCheque;
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "(F10)");
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final class ActionAddBrouillard extends AbstractAction {

		/**
         *
         */
		public ActionAddBrouillard() {
			super("");

			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un brouillard");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			brouillardAjouter();
		}

	}

	private final class ActionAddCheque extends AbstractAction {

		/**
         *
         */
		public ActionAddCheque() {
			super("");

			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un chèque (Insert)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			chequeAjouter();
		}

	}

	private final class ActionDeleteBrouillard extends AbstractAction {

		/**
         *
         */
		public ActionDeleteBrouillard() {
			super("");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le brouillard sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			setEnabled(false);

		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			brouillardSupprimer();
		}

	}

	private final class ActionDeleteCheque extends AbstractAction {

		/**
         *
         */
		public ActionDeleteCheque() {
			super("");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le chèque sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			setEnabled(false);

		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			chequeSupprimer();
		}

	}

	private final class ActionModifyCheque extends AbstractAction {

		/**
         *
         */
		public ActionModifyCheque() {
			super("");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le chèque sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			setEnabled(false);

		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			chequeModifier();
		}

	}

	private final class ActionDupliquerCheque extends AbstractAction {

		/**
         *
         */
		public ActionDupliquerCheque() {
			super("");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Dupliquer le chèque sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_DUPLIQUE_16));
			setEnabled(false);

		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			chequeDupliquer();
		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	private final class BdChequeFormPanelListener implements BdChequeFormPanel.IBdChequeFormPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#getBorLibelle()
		 */
		public Object getBorLibelle() {
			return currentBdCheque.borLibelle();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#setBorLibelle(java.lang.String)
		 */
		public void setBorLibelle(String string) {
			currentBdCheque.setLibelle(string);
			refreshSaveAction();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#getBorNum()
		 */
		public Object getBorNum() {
			return currentBdCheque.borNum();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#setBorNum(java.lang.Integer)
		 */
		public void setBorNum(Integer integer) {
			currentBdCheque.setBorNum(integer);
			refreshSaveAction();

		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#setGestion(org.cocktail.maracuja.client.metier.EOGestion)
		 */
		public void setGestion(EOGestion gestion) {
			factoryBordereauCheque.affecteGestion(currentBdCheque, gestion);
			refreshSaveAction();

		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#getGestion()
		 */
		public EOGestion getGestion() {
			return currentBdCheque.gestion();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#getPlanComptableVisa()
		 */
		public EOPlanComptable getPlanComptableVisa() {
			return currentBdCheque.bordereauInfo().planComptableVisa();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#setPlanComptableVisa(org.cocktail.maracuja.client.metier.EOPlanComptable)
		 */
		public void setPlanComptableVisa(EOPlanComptable planComptable) {
			factoryBordereauCheque.affecterPlanComptableVisa(currentBdCheque, planComptable);
			refreshSaveAction();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#getCodeGestionModel()
		 */
		public ZEOComboBoxModel getCodeGestionModel() {
			return gestionsModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeFormPanel.IBdChequeFormPanelListener#actionPlancomptableVisaSelect()
		 */
		public Action actionPlancomptableVisaSelect() {
			return actionPlancoSelect;
		}

	}

	private final class ChequeListPanelListener implements ChequeListPanel.IChequeListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeListPanel.IChequeListPanelListener#getData()
		 */
		public NSArray getData() {
			return getChequesValidesForBordereau(currentBdCheque);
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeListPanel.IChequeListPanelListener#onselectionChanged()
		 */
		public void onselectionChanged() {
			refreshActionsListeCheque();
			bdChequeSaisiePanel.getChequeBrouillardListPanel().updateData();
			bdChequeSaisiePanel.getBalancePanel().updateData();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeListPanel.IChequeListPanelListener#onDbClick()
		 */
		public void onDbClick() {
			chequeModifier();
		}

	}

	private final class ChequeBrouillardListPanelListener implements ChequeBrouillardListPanel.IChequeBrouillardListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getData()
		 */
		public NSArray getData() {
			if (currentBdCheque == null) {
				return new NSArray();
			}
			else if (bdChequeSaisiePanel.getChequeListPanel().getSelectedObject() == null) {
				return new NSArray();
			}
			else {
				return getChequeBrouillardsValidesForCheque(((EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject()));
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#onTableChanged()
		 */
		public void onTableChanged() {
			bdChequeSaisiePanel.getBalancePanel().updateData();
			refreshSaveAction();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#deleteRow()
		 */
		public void deleteRow() {
			brouillardSupprimer();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			refreshActionsListeBrouillard();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getPlanComptables()
		 */
		public NSArray getPlanComptables() {
			return planComptables;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getGestions()
		 */
		public NSArray getGestions() {
			return gestions;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#addRow()
		 */
		public void addRow() {
			brouillardAjouter();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeBrouillardListPanel.IChequeBrouillardListPanelListener#getFactoryBrouillardCheque()
		 */
		public FactoryBrouillardCheque getFactoryBrouillardCheque() {
			return factoryBrouillardCheque;
		}

	}

	private final NSArray getChequesValidesForBordereau(final EOBordereau bordereau) {
		return bordereau.getChequesValides();
	}

	private final NSArray getChequeBrouillardsValidesForCheque(final EOCheque cheque) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("chbEtat=%@", new NSArray(EOChequeBrouillard.etatValide));
		return EOQualifier.filteredArrayWithQualifier(cheque.chequeBrouillards(), qual);
	}

	//	private final ArrayList getChequeBrouillardsValidesForBordereau(final EOBordereau bordereau) {
	//	    ArrayList list = new ArrayList();
	//	    NSArray cheques = getChequesValidesForBordereau(currentBdCheque);
	//	    for (int i = 0; i < cheques.count(); i++) {
	//            EOCheque element = (EOCheque) cheques.objectAtIndex(i);
	//            list.addAll( getChequeBrouillardsValidesForCheque(element).vector() );
	//        }
	//	    return list;
	//	}
	private final ArrayList getAllChequeBrouillardsForBordereau(final EOBordereau bordereau) {
		ArrayList list = new ArrayList();
		NSArray cheques = bordereau.cheques();
		for (int i = 0; i < cheques.count(); i++) {
			EOCheque element = (EOCheque) cheques.objectAtIndex(i);
			list.addAll(element.chequeBrouillards().vector());
		}
		return list;
	}

	private final class BalanceGlobaleProvider implements ZPanelBalance.IZPanelBalanceProvider {

		/**
		 * @see org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider#getDebitValue()
		 */
		public BigDecimal getDebitValue() {
			BigDecimal val = new BigDecimal(0);
			if (currentBdCheque != null) {
				if (bdChequeSaisiePanel.getChequeListPanel().getSelectedObject() != null) {
					val = ((EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject()).cheMontant();
				}
			}
			return val;

		}

		/**
		 * @see org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider#getCreditValue()
		 */
		public BigDecimal getCreditValue() {
			BigDecimal val = new BigDecimal(0);
			if (currentBdCheque != null) {
				if (bdChequeSaisiePanel.getChequeListPanel().getSelectedObject() != null) {
					val = ZEOUtilities.calcSommeOfBigDecimals(getChequeBrouillardsValidesForCheque((EOCheque) bdChequeSaisiePanel.getChequeListPanel().getSelectedObject()), "chbMontant");
				}
			}
			return val;

			//            BigDecimal val = new BigDecimal(0);
			//            if (currentBdCheque!=null) {
			//                return ZEOUtilities.calcSommeOfBigDecimals(getChequeBrouillardsValidesForBordereau(currentBdCheque), "chbMontant");
			//            }
			//            return val;
		}

	}

	private final class ActionPlancoSelect extends AbstractAction {
		public ActionPlancoSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte de visa pour le bordereau de chèque");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoVisa();
		}
	}

	public boolean isModified() {
		return modified;
	}

	private final void refreshSaveAction() {
		actionValider.setEnabled(getEditingContext().hasChanges());
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return bdChequeSaisiePanel;
	}

	public String title() {
		return TITLE;
	}
}
