/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessBordereauDeCheques;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOCheque;
import org.cocktail.maracuja.client.metier.EOChequeDetailEcriture;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BdChequeSrchCtrl extends CommonCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);
	private static final String TITLE = "Gestion des bordereaux de chèques ";
	private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_CHESA;
	private final String ACTION_ID_VISA = ZActionCtrl.IDU_CHEVI;

	public final static int TRIER_NOM = 1;
	public final static int TRIER_CREATION = 2;
	public final static int TRIER_NUMCHEQUE = 3;
	public final static int TRIER_DATE_SAISIE = 4;

	private BdChequeSrchPanel myPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionNew actionNew = new ActionNew();
	private final ActionModifier actionModifier = new ActionModifier();
	private final ActionFermer actionFermer = new ActionFermer();
	private final ActionViser actionViser = new ActionViser();
	private final ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionImprimer2 actionImprimer2 = new ActionImprimer2();
	private final ActionSortNom actionSortNom = new ActionSortNom();
	private final ActionSortCreation actionSortCreation = new ActionSortCreation();
	private final ActionSortNumCheque actionSortNumCheque = new ActionSortNumCheque();
	private final ActionSortDateSaisie actionSortDateSaisie = new ActionSortDateSaisie();
	private final ActionSortNom2 actionSortNom2 = new ActionSortNom2();
	private final ActionSortCreation2 actionSortCreation2 = new ActionSortCreation2();
	private final ActionSortNumCheque2 actionSortNumCheque2 = new ActionSortNumCheque2();
	private final ActionSortDateSaisie2 actionSortDateSaisie2 = new ActionSortDateSaisie2();
	private final ActionVoirEcritures actionVoirEcritures = new ActionVoirEcritures();

	private HashMap myFilters;

	private NSArray bdcheques;

	//    private NSArray comptabilites;

	private NSArray gestionsforSaisie;
	private NSArray gestionsforVisa;

	//    private int sortMode=TRIER_NOM;

	private final ArrayList sortActionsList = new ArrayList(3);
	private final ArrayList sortActionsList2 = new ArrayList(3);

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public BdChequeSrchCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		myFilters = new HashMap();
		initSubObjects();
		sortActionsList.add(actionSortNom);
		//sortActionsList.add(actionSortCreation);
		sortActionsList.add(actionSortDateSaisie);
		sortActionsList.add(actionSortNumCheque);

		sortActionsList2.add(actionSortNom2);
		//sortActionsList2.add(actionSortCreation2);
		sortActionsList2.add(actionSortDateSaisie2);
		sortActionsList2.add(actionSortNumCheque2);

	}

	public void initSubObjects() throws Exception {
		gestionsforSaisie = new NSArray();
		gestionsforVisa = new NSArray();
		//          origineModel = new ZEOComboBoxModel(new NSArray(),"oriLibelle","",null);
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_SAISIE)) {
			gestionsforSaisie = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_SAISIE);
			//                comptabilites = myApp.appUserInfo().getAuthorizedComptabilitesForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_SAISIE);
		}
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_VISA)) {
			gestionsforVisa = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_VISA);
		}

		if (gestionsforSaisie.count() == 0 && gestionsforVisa.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour accéder aux bordereaux de chèque. Demandez à votre administrateur " +
					"de vous affecter les codes de gestions pour cette fonction");
		}

		//          ZLogger.debug(gestionsforVisa);
		//          ZLogger.debug(gestionsforSaisie);

		myPanel = new BdChequeSrchPanel(new BdChequeSrchPanelListener());

	}

	public final NSArray getBdCheques() {
		//Créer la condition à partir des filtres
		try {
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("borNum", EOSortOrdering.CompareDescending);
			bdcheques = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)), new NSArray(sort1), true);
			return bdcheques;
		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}
	}

	public final NSArray getCheques() {
		try {
			if (getSelectedBdCheque() != null) {
				//				EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("cheNomTireur", EOSortOrdering.CompareAscending);
				//				EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("chePrenomTireur", EOSortOrdering.CompareAscending);
				//				NSArray res = EOsFinder.fetchArray(getEditingContext(), EOCheque.ENTITY_NAME, "bordereau=%@ and cheEtat<>%@", new NSArray(new Object[] {
				//						getSelectedBdCheque(), EOCheque.etatAnnule
				//				}), new NSArray(new Object[] {
				//						sort1, sort2
				//				}), false);
				NSArray res = EOsFinder.fetchArray(getEditingContext(), EOCheque.ENTITY_NAME, "bordereau=%@ and cheEtat<>%@", new NSArray(new Object[] {
						getSelectedBdCheque(), EOCheque.etatAnnule
				}), new NSArray(new Object[] {
						EOCheque.SORT_CHE_DATE_SAISIE_ASC
				}), false);
				return res;
			}
			return new NSArray();
		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}
	}

	public final EOBordereau getSelectedBdCheque() {
		return (EOBordereau) myPanel.getBdChequeListPanel().selectedObject();
	}

	public final void setSelectedBdChesque(EOBordereau obj) {
		myPanel.getBdChequeListPanel().setSelectedObject(obj);
	}

	//    private final void onBdChequeSelectionChanged() {
	//        try {
	//            myPanel.getChequeListPanel().updateData();
	//            refreshActions();
	//        } catch (Exception e) {
	//           showErrorDialog(e);
	//        }
	//    }

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.BdChequeRechercheFilterPanel.IBdChequeRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice()
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeBordereau=%@", new NSArray(new Object[] {
				FinderVisa.leTypeBordereauBTCC(getEditingContext())
		})));

		//Construire les qualifiers à partir des saisies utilisateur

		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("borNumMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("borNum>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNumMin")).intValue()))));
		}
		if (dicoFiltre.get("borNumMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("borNum<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNumMax")).intValue()))));
		}

		if (dicoFiltre.get("borNum") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("borNum=%@", new NSArray(new Integer(((Number) dicoFiltre.get("borNum")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		///Date
		NSMutableArray qualsDate = new NSMutableArray();
		if (dicoFiltre.get("borDateVisaMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("borDateVisa>=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("borDateVisaMin")))));
		}
		if (dicoFiltre.get("borDateVisaMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat("borDateVisa<=%@", new NSArray(new NSTimestamp(ZDateUtil.addDHMS((Date) dicoFiltre.get("borDateVisaMax"), 0, 23, 59, 59)))));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		return quals;
	}

	private final NSArray getChequesValidesOrVisesForBordereau(final EOBordereau bordereau) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("cheEtat=%@ or cheEtat=%@ ", new NSArray(new Object[] {
				EOCheque.etatValide, EOCheque.etatVise
		}));
		return EOQualifier.filteredArrayWithQualifier(bordereau.cheques(), qual);
	}

	private final void bdChequeImprimer(final int mode) {
		try {
			if (getSelectedBdCheque() == null) {
				throw new DefaultClientException("Aucun bordereau de sélectionné.");
			}

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey(new Integer(getChequesValidesOrVisesForBordereau(getSelectedBdCheque()).count()), "NB_CHEQUES_BD");
			dico.takeValueForKey(ZEOUtilities.calcSommeOfBigDecimals(getChequesValidesOrVisesForBordereau(getSelectedBdCheque()), "cheMontant"), "MONTANT_BORDEREAU");

			String filePath = ReportFactoryClient.imprimerBdCheque(getEditingContext(), myApp.temporaryDir, dico, getSelectedBdCheque(), mode, ReportFactoryClient.JASPERFILENAME_BORDEREAU_CHEQUE);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	private final void bdChequeImprimer2(final int mode) {
		try {
			if (getSelectedBdCheque() == null) {
				throw new DefaultClientException("Aucun bordereau de sélectionné.");
			}

			NSMutableDictionary dico = new NSMutableDictionary(myApp.getParametres());
			dico.takeValueForKey(new Integer(getChequesValidesOrVisesForBordereau(getSelectedBdCheque()).count()), "NB_CHEQUES_BD");
			dico.takeValueForKey(ZEOUtilities.calcSommeOfBigDecimals(getChequesValidesOrVisesForBordereau(getSelectedBdCheque()), "cheMontant"), "MONTANT_BORDEREAU");
			String filePath = ReportFactoryClient.imprimerBdCheque(getEditingContext(), myApp.temporaryDir, dico, getSelectedBdCheque(), mode, ReportFactoryClient.JASPERFILENAME_BORDEREAU_CHEQUE2);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e1) {
			showErrorDialog(e1);
		}
	}

	/**
	 * Active/desactive les actions en fonction de l'ecriture selectionnee.
	 */
	private final void refreshActions() {
		if (getSelectedBdCheque() == null) {
			actionImprimer.setEnabled(false);
			actionImprimer2.setEnabled(false);
			actionModifier.setEnabled(false);
			actionFermer.setEnabled(false);
			actionViser.setEnabled(false);
			actionVoirEcritures.setEnabled(false);
		}
		else {
			actionImprimer.setEnabled(true && (gestionsforSaisie.count() > 0));
			actionImprimer2.setEnabled(true && (gestionsforSaisie.count() > 0));
			actionModifier.setEnabled(true && (gestionsforSaisie.count() > 0));
			actionFermer.setEnabled(true && (gestionsforSaisie.count() > 0) && (EOBordereau.BordereauValide.equals(getSelectedBdCheque().borEtat())));
			actionViser.setEnabled(true && (gestionsforVisa.count() > 0) && (EOBordereau.BordereauValide.equals(getSelectedBdCheque().borEtat()) || EOBordereau.BordereauAViser.equals(getSelectedBdCheque().borEtat())));
			actionVoirEcritures.setEnabled(EOBordereau.BordereauVise.equals(getSelectedBdCheque().borEtat()));
		}
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, "Gestion des bordereaux de chèques", true);
		}

		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial, NSArray objs) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			loadBdCheques(objs);
			win.open();
		} finally {
			win.dispose();
		}
	}

	private final void bdChequeNew() {
		try {
			//Verifier que l'exercice n'est pas clos
			if (getExercice().estClos()) {
				throw new Exception("L'exercice " + getExercice().exeExercice() + " est clos.");
			}

			BdChequeSaisieCtrl ecritureSaisieCtrl = new BdChequeSaisieCtrl(getEditingContext());
			EOBordereau bordereau = ecritureSaisieCtrl.openDialogNew(getMyDialog());
			if (bordereau != null && ecritureSaisieCtrl.isModified()) {
				myFilters.put("borNum", bordereau.borNum());
				try {
					myPanel.updateData();
				} catch (Exception e) {
					myFilters.clear();
					showErrorDialog(e);
				} finally {
					myFilters.clear();
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void bdChequeModify() {
		try {
			BdChequeSaisieCtrl ecritureSaisieCtrl = new BdChequeSaisieCtrl(getEditingContext());
			EOBordereau bordereau = ecritureSaisieCtrl.openDialogModify(getMyDialog(), getSelectedBdCheque());
			if (bordereau != null && ecritureSaisieCtrl.isModified()) {
				//                myFilters.put("borNum", bordereau.borNum());
				try {
					myPanel.updateData();
				} catch (Exception e) {
					myFilters.clear();
					showErrorDialog(e);
				} finally {
					myFilters.clear();
				}
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void bdChequeFermer() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau bdCheque = getSelectedBdCheque();
			if (bdCheque != null) {
				EOGestion gestion = bdCheque.gestion();

				if (gestionsforSaisie.indexOfObject(gestion) == NSArray.NotFound) {
					throw new DefaultClientException("Vous n'avez pas les droits de saisir/fermer un bordereau pour le code de gestion " + gestion.gesCode());
				}

				if (EOBordereau.BordereauVise.equals(bdCheque.borEtat())) {
					throw new DefaultClientException("Le bordereau " + bdCheque.borNum() + " est déjà visé.");
				}

				if (!EOBordereau.BordereauValide.equals(bdCheque.borEtat())) {
					throw new DefaultClientException("Le bordereau  " + bdCheque.borNum() + "  doit être à l'état " + EOBordereau.BordereauValide + " pour être fermé.");
				}

				if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement fermer le bordereau n°" + bdCheque.borNum() + " ? Il ne sera plus possible de le modifier ensuite.", ZMsgPanel.BTLABEL_NO)) {
					FactoryProcessBordereauDeCheques factoryProcessBordereauDeCheques = new FactoryProcessBordereauDeCheques(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					factoryProcessBordereauDeCheques.fermerUnBordereauDeCheques(getEditingContext(), bdCheque);

					getEditingContext().saveChanges();

					//Mettre à jour la ligne
					myPanel.getBdChequeListPanel().updateData();

				}

			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void bdChequeViser() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau bdCheque = getSelectedBdCheque();
			if (bdCheque != null) {
				EOGestion gestion = bdCheque.gestion();

				if (gestionsforVisa.indexOfObject(gestion) == NSArray.NotFound) {
					throw new DefaultClientException("Vous n'avez pas les droits de viser un bordereau pour le code de gestion " + gestion.gesCode());
				}

				if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement viser le bordereau n°" + bdCheque.borNum() + " ?", ZMsgPanel.BTLABEL_NO)) {

					if (!EOBordereau.BordereauAViser.equals(bdCheque.borEtat())) {
						throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauAViser + " pour être visé. Vous devez fermer le bordereau avant de le viser.");
					}

					final FactoryProcessBordereauDeCheques factoryProcessBordereauDeCheques = new FactoryProcessBordereauDeCheques(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					final FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					NSArray lesEcritures;
					try {
						lesEcritures = factoryProcessBordereauDeCheques.viserUnBordereauDeCheques(getEditingContext(), bdCheque, getUtilisateur());
					} catch (Exception e) {
						getEditingContext().revert();
						throw e;
					}

					getEditingContext().saveChanges();
					myPanel.getBdChequeListPanel().updateData();
					String msgFin = "";
					msgFin = msgFin + "Le bordereau n° " + bdCheque.borNum() + " a été visé.\n";
					msgFin = msgFin + "Nombre d'écritures générées : " + lesEcritures.count() + "\n";

					//Numéroter
					try {
						KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
						NSMutableArray nbs = new NSMutableArray();
						if ((lesEcritures != null) && (lesEcritures.count() > 0)) {
							for (int i = 0; i < lesEcritures.count(); i++) {
								EOEcriture element = (EOEcriture) lesEcritures.objectAtIndex(i);
								factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
								nbs.addObject(element.ecrNumero());
							}
						}
						if (nbs.count() > 0) {
							msgFin = msgFin + "\nLes écritures ont été numérotées : " + nbs.componentsJoinedByString(",");
						}
					} catch (Exception e) {
						e.printStackTrace();
						msgFin = msgFin + "\nErreur lors de la numérotation des écritures : \n";
						msgFin = msgFin + e.getMessage();
					}
					myApp.showInfoDialog(msgFin);
				}

			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		}

	}

	public final void resetFilter() {
		myFilters.clear();
	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Saisir un nouveau bordereau");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeNew();
		}
	}

	private final class ActionModifier extends AbstractAction {
		public ActionModifier() {
			super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDIT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le bordereau");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			bdChequeModify();
		}
	}

	private final class ActionFermer extends AbstractAction {
		public ActionFermer() {
			super("Fermer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer le bordereau. Il ne sera plus possible de le modifier ensuite.");
			setEnabled(false);
		}

		public void actionPerformed(ActionEvent e) {
			bdChequeFermer();
		}
	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Viser le bordereau");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeViser();
		}
	}

	private final class ActionVoirEcritures extends AbstractAction {
		public ActionVoirEcritures() {
			super("Ecritures");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir le détail des écritures générées à partir du bordereau sélectionné");
			setEnabled(true);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			voirEcritures();
		}
	}

	public final class ActionImprimer extends AbstractAction {

		public ActionImprimer() {
			super("Imprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le bordereau sélectionné");
			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//            myPanel.showMenuImprimer(e);
		}

	}

	public final class ActionImprimer2 extends AbstractAction {

		public ActionImprimer2() {
			super("Imprimer (*)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le bordereau sélectionné (modèle avec limites de montants)");
			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			//            myPanel.showMenuImprimer(e);
		}

	}

	private final class BdChequeSrchPanelListener implements BdChequeSrchPanel.IBdChequeSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#getActionImprimer()
		 */
		public Action getActionImprimer() {
			return actionImprimer;
		}

		public Action getActionImprimer2() {
			return actionImprimer2;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#actionNew()
		 */
		public Action actionNew() {
			return actionNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#actionModifier()
		 */
		public Action actionModifier() {
			return actionModifier;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#actionViser()
		 */
		public Action actionViser() {
			return actionViser;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#getCheques()
		 */
		public NSArray getCheques() {
			return BdChequeSrchCtrl.this.getCheques();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#getBordereaux()
		 */
		public NSArray getBordereaux() {
			return getBdCheques();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#bdChequeSelectionChanged()
		 */
		public void bdChequeSelectionChanged() {
			try {
				refreshActions();
				myPanel.getChequeListPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#actionFermer()
		 */
		public Action actionFermer() {
			return actionFermer;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#onBdChequeDbClick()
		 */
		public void onBdChequeDbClick() {
			if (actionModifier.isEnabled()) {
				bdChequeModify();
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BdChequeSrchPanel.IBdChequeSrchPanelListener#getSortActions()
		 */
		public ArrayList getSortActions() {
			return sortActionsList;
		}

		public ArrayList getSortActions2() {
			return sortActionsList2;
		}

		public Action actionVoirEcritures() {
			return actionVoirEcritures;
		}

	}

	private final void loadBdCheques(final NSArray ecrituresTmp) {
		if (ecrituresTmp != null && ecrituresTmp.count() > 0) {
			NSMutableArray ecrNums = new NSMutableArray();
			for (int i = 0; i < ecrituresTmp.count(); i++) {
				EOBordereau element = (EOBordereau) ecrituresTmp.objectAtIndex(i);
				ecrNums.addObject(element.borNum());
			}
			myFilters.put("borNumList", ecrNums);
			try {
				myPanel.updateData();
			} catch (Exception e) {
				myFilters.clear();
				showErrorDialog(e);
			} finally {
				myFilters.clear();
			}
		}
	}

	private final class ActionSortNom extends AbstractAction {
		public ActionSortNom() {
			super("Trier par nom");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer(TRIER_NOM);
		}

	}

	private final class ActionSortCreation extends AbstractAction {
		public ActionSortCreation() {
			super("Trier par ordre de saisie");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer(TRIER_CREATION);
		}

	}

	private final class ActionSortNumCheque extends AbstractAction {
		public ActionSortNumCheque() {
			super("Trier par N° de chèque");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer(TRIER_NUMCHEQUE);
		}

	}

	private final class ActionSortDateSaisie extends AbstractAction {
		public ActionSortDateSaisie() {
			super("Trier par ordre de saisie");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer(TRIER_DATE_SAISIE);
		}

	}

	private final class ActionSortNom2 extends AbstractAction {
		public ActionSortNom2() {
			super("Trier par nom");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer2(TRIER_NOM);
		}

	}

	private final class ActionSortCreation2 extends AbstractAction {
		public ActionSortCreation2() {
			super("Trier par ordre de saisie");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer2(TRIER_CREATION);
		}

	}

	private final class ActionSortNumCheque2 extends AbstractAction {
		public ActionSortNumCheque2() {
			super("Trier par N° de chèque");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer2(TRIER_NUMCHEQUE);
		}

	}

	private final class ActionSortDateSaisie2 extends AbstractAction {
		public ActionSortDateSaisie2() {
			super("Trier par ordre de saisie");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			bdChequeImprimer2(TRIER_DATE_SAISIE);
		}

	}

	private NSArray getEcrituresVisaForBord(final EOBordereau bordereau) throws Exception {
		//on considere qu'il y a une écriture de visa par bordereau
		final EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat(EOCheque.CHE_ETAT_KEY + "=%@", new NSArray(new Object[] {
				EOCheque.etatVise
		}));
		final NSArray chequesVises = EOQualifier.filteredArrayWithQualifier(bordereau.cheques(), qual1);
		if (chequesVises.count() > 0) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOChequeDetailEcriture.CHEQUE_KEY + "=%@", new NSArray(chequesVises.objectAtIndex(0)));
			final NSArray chequeDetailEcritures = EOsFinder.fetchArray(getEditingContext(), EOChequeDetailEcriture.ENTITY_NAME, qual, null, false);

			if (chequeDetailEcritures.count() > 0) {
				return new NSArray(((EOChequeDetailEcriture) chequeDetailEcritures.objectAtIndex(0)).ecritureDetail().ecriture());
			}
		}
		throw new DefaultClientException("Il n'y a pas d'écritures associées aux chèques du bordereau.");

	}

	private final void voirEcritures() {
		try {
			final EOBordereau bordereau = getSelectedBdCheque();
			if (bordereau == null) {
				throw new DefaultClientException("Aucun bordereau de sélectionné.");
			}
			NSArray visas = getEcrituresVisaForBord(bordereau);

			NSMutableArray allEcritures = new NSMutableArray();
			allEcritures.addObjectsFromArray(visas);

			if (allEcritures.count() == 0) {
				showInfoDialog("Aucune écriture n'a été passée pour le bordereau sélectionné.");
				return;
			}

			if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {
				EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
				win.openDialog(getMyDialog(), allEcritures);
			}
			else {
				throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
