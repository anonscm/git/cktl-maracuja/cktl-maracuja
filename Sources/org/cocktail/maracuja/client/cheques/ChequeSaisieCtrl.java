/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;

import org.cocktail.maracuja.client.AppUserInfo;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.cheques.ui.ChequeFormPanel;
import org.cocktail.maracuja.client.cheques.ui.ChequeSaisiePanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog.InitialFocusSetter;
import org.cocktail.zutil.client.comms.LecteurChequeClient;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ChequeSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'un chèque";
	private static final Dimension WINDOW_DIMENSION = new Dimension(690, 530);
	//    private static final int MODE_NEW=0;
	//    private static final int MODE_MODIF=1;

	//    private int modeSaisie=MODE_NEW;

	private HashMap currentChequeDico;

	private ChequeSaisiePanel chequeSaisiePanel;
	private DefaultComboBoxModel rcptModel;

	private ActionClose actionClose = new ActionClose();
	private ActionValider actionValider = new ActionValider();
	private final ActionAnnuleLecteurCheque actionAnnuleLecteurCheque = new ActionAnnuleLecteurCheque();
	private final ActionLanceLecteurCheque actionLanceLecteurCheque = new ActionLanceLecteurCheque();
	private final ActionReinitLecteurCheque actionReinitLecteurCheque = new ActionReinitLecteurCheque();

	private LireChequeThread lireChequeThread;

	private final LecteurChequeClientListener lChequeClientListener = new LecteurChequeClientListener();

	private Icon lecteurChequeIcon;
	private String lecteurChequeInfo;

	/**
	 * @param editingContext
	 */
	public ChequeSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);

		rcptModel = new DefaultComboBoxModel();
		rcptModel.addElement("CB");
		rcptModel.addElement("CCP");

		currentChequeDico = new HashMap();
		chequeSaisiePanel = new ChequeSaisiePanel(new ChequeSaisiePanelListener(), new ChequeFormPanelListener());

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionLanceLecteurCheque extends AbstractAction {

		public ActionLanceLecteurCheque() {
			super("Lire le chèque");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			lireCheque();
		}

	}

	public final class ActionReinitLecteurCheque extends AbstractAction {

		public ActionReinitLecteurCheque() {
			super("Réinitialiser le lecteur");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "(Echap)");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			reinitLecteur();
		}

	}

	public final class ActionAnnuleLecteurCheque extends AbstractAction {

		public ActionAnnuleLecteurCheque() {
			super("Stop");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annuleLireCheque();
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "(F10)");
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final void checkSaisieChequeDico() throws Exception {
		ZLogger.debug(currentChequeDico);
		if (currentChequeDico.get("cheNomTireur") == null) {
			throw new DataCheckException("Le nom du tireur est obligatoire");
		}
		if (currentChequeDico.get("cheNumeroCheque") == null) {
			throw new DataCheckException("Le n° du chèque est obligatoire");
		}
		if (currentChequeDico.get("cheMontant") == null) {
			throw new DataCheckException("Le montant est obligatoire");
		}

	}

	private final void annulerSaisie() {
		getMyDialog().onCancelClick();
	}

	/**
     * 
     */
	private final void validerSaisie() {
		try {
			validerSaisieForNewCheque();
			getMyDialog().onOkClick();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void validerSaisieForNewCheque() throws Exception {
		checkSaisieChequeDico();
		//        displayedBdCheques = tmpArray.immutableClone();
	}

	private final class ChequeFormPanelListener implements ChequeFormPanel.IChequeFormPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeFormPanel.IChequeFormPanelListener#getCurrentChequeDico()
		 */
		public HashMap getCurrentChequeDico() {
			return currentChequeDico;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeFormPanel.IChequeFormPanelListener#getRcptCodeModel()
		 */
		public DefaultComboBoxModel getRcptCodeModel() {
			return rcptModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeFormPanel.IChequeFormPanelListener#getDialog()
		 */
		public Dialog getDialog() {
			return getMyDialog();
		}

		public Action actionLanceLecteurCheque() {
			return actionLanceLecteurCheque;
		}

		public Action actionAnnuleLecteurCheque() {
			return actionAnnuleLecteurCheque;
		}

		public Icon getLecteurChequeIcon() {
			return lecteurChequeIcon;
		}

		public String getLecteurChequeInfo() {
			return lecteurChequeInfo;
		}

		public Action actionReinitLecteurCheque() {
			return actionReinitLecteurCheque;
		}

	}

	private final class ChequeSaisiePanelListener implements ChequeSaisiePanel.IChequeSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeSaisiePanel.IChequeSaisiePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeSaisiePanel.IChequeSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

	}

	//
	//    private final ZKarukeraDialog createModalDialog(Window dial ) {
	//        ZKarukeraDialog win;
	//        if (dial instanceof Dialog) {
	//            win = new ZKarukeraDialog((Dialog)dial, TITLE,true);
	//        }
	//        else {
	//            win = new ZKarukeraDialog((Frame)dial, TITLE,true);
	//        }        
	//        setMyDialog(win);
	//        chequeSaisiePanel.setMyDialog(getMyDialog());
	//        chequeSaisiePanel.initGUI();
	//        chequeSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
	//        win.setContentPane(chequeSaisiePanel);
	//        win.pack();
	//        return win;
	//    }            

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final HashMap openDialogNew(Window dial) {
		final ZKarukeraDialog win = createDialog(dial, true);
		HashMap res = null;
		try {
			initDefaultChequeDico();
			chequeSaisiePanel.initGUI();
			chequeSaisiePanel.updateData();
			actionAnnuleLecteurCheque.setEnabled(false);
			actionLanceLecteurCheque.setEnabled(true);
			actionReinitLecteurCheque.setEnabled(true);

			if (win.open() == ZKarukeraDialog.MROK) {
				res = currentChequeDico;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			annuleLireCheque();
			win.dispose();
		}
		return res;
	}

	public final HashMap openDialogDuplique(Window dial, HashMap dico) {
		final ZKarukeraDialog win = createDialog(dial, true);
		HashMap res = null;
		try {
			initDefaultChequeDico();
			chequeSaisiePanel.initGUI();
			currentChequeDico.putAll(dico);
			currentChequeDico.put("cheNumeroCheque", null);
			chequeSaisiePanel.updateData();
			actionAnnuleLecteurCheque.setEnabled(false);
			actionLanceLecteurCheque.setEnabled(true);
			actionReinitLecteurCheque.setEnabled(true);

			InitialFocusSetter.setInitialFocus(win, chequeSaisiePanel.getChequeFormPanel().getCheNumeroCheque().getMyTexfield());

			if (win.open() == ZKarukeraDialog.MROK) {
				res = currentChequeDico;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			annuleLireCheque();
			win.dispose();
		}
		return res;
	}

	public final HashMap openDialogModify(Window dial, HashMap dico) {
		final ZKarukeraDialog win = createDialog(dial, true);
		HashMap res = null;
		try {
			//            modeSaisie=MODE_MODIF;
			currentChequeDico.clear();
			currentChequeDico.putAll(dico);
			currentChequeDico.put("lecteurChequeAdresse", myApp.appUserInfo().getAppUserPreferences().get(AppUserInfo.PREFKEY_LECTEUR_CHEQUE_ADRESSE));
			ZLogger.debug(currentChequeDico);
			actionAnnuleLecteurCheque.setEnabled(false);
			actionLanceLecteurCheque.setEnabled(true);
			actionReinitLecteurCheque.setEnabled(true);
			chequeSaisiePanel.initGUI();
			chequeSaisiePanel.updateData();
			if (win.open() == ZKarukeraDialog.MROK) {
				res = currentChequeDico;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			annuleLireCheque();
			win.dispose();
		}
		return res;
	}

	private void initDefaultChequeDico() {
		currentChequeDico.clear();
		currentChequeDico.put("utilisateur", myApp.appUserInfo().getUtilisateur());
		currentChequeDico.put("rcptCode", rcptModel.getElementAt(0));
		currentChequeDico.put("lecteurChequeAdresse", myApp.appUserInfo().getAppUserPreferences().get(AppUserInfo.PREFKEY_LECTEUR_CHEQUE_ADRESSE));
	}

	public final void lireCheque() {
		lireChequeThread = new LireChequeThread((String) currentChequeDico.get("lecteurChequeAdresse"));
		lireChequeThread.start();

		lecteurChequeIcon = ZIcon.getIconForName(ZIcon.ICON_RUN_32);
		//       lecteurChequeInfo = "<html><b><font color=\"#0000FF\">Attente du chèque...</font></b></html>";
		////       lecteurChequeInfo = "Passez le chèque dans le lecteur...";
		//       chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();

		//            
		// //typeCheque String(CCP ou CB)
		// //rcptCode.setText((String)tmp.get("typeCheque"));
		//                 
		// // numCheque String numero du cheque
		// currentCheque.setCheqNumeroCheque((String)tmp.get("numCheque"));
		// // codeBanque String code de la banque
		// //banqCode.setText((String)tmp.get("codeBanque"));
		// EOQualifier qualifier =
		// EOQualifier.qualifierWithQualifierFormat("banqCode like'" +
		// (String)tmp.get("codeBanque") + "'", null);
		// EOFetchSpecification spec = new
		// EOFetchSpecification("Banque",qualifier,null);
		//
		// NSArray aBanque =
		// editingContext().objectsWithFetchSpecification(spec);
		// if(aBanque.count()>0)
		// {
		// currentCheque.setBanque((Banque)aBanque.objectAtIndex(0));
		// }
		// else
		// {
		// String libBanque = JOptionPane.showInputDialog("CODE BANQUE " +
		// (String)tmp.get("codeBanque") + " INCONNU Entrer le libelle de la
		// banque et clicker OK","Banque inconnue");
		// Banque currentBanque =
		// (Banque)CRIInstanceUtilities.instanceForEntity(editingContext(),"Banque");
		// currentBanque.setBanqCode((String)tmp.get("codeBanque"));
		// currentBanque.setBanqLibelle(libBanque);
		// currentCheque.setBanque(currentBanque);
		//      }
		//     currentCheque.setCheqNumeroCompte((String)tmp.get("numCompte"));
	}

	public final void annuleLireCheque() {

		if (lireChequeThread != null && lireChequeThread.isAlive()) {
			System.out.println("Arret de la lecture");
			lireChequeThread.interrupt();
			lireChequeThread = null;
		}
		lecteurChequeIcon = null;
		lecteurChequeInfo = null;
		chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();

	}

	private final void onChequeIsRead(Map map) {
		try {

			lecteurChequeIcon = ZIcon.getIconForName(ZIcon.ICON_CHECKED_32);
			lecteurChequeInfo = "<html><b><font color=\"#2CA103\">Chèque lu avec succès</font></b></html>";
			chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();

			System.out.println("Cheque lu");
			System.out.println("sur " + currentChequeDico.get("lecteurChequeAdresse"));
			System.out.println(map);
			//
			currentChequeDico.put("cheNumeroCompte", map.get("numCompte"));
			currentChequeDico.put("cheNumeroCheque", map.get("numCheque"));
			currentChequeDico.put("rcptCode", map.get("typeCheque"));
			currentChequeDico.put("banqCode", map.get("codeBanque"));

			//Récupérer le libelle de la banque
			final String vsql = "SELECT BANQ_LIBELLE FROM GARNUCHE.BANQUE WHERE BANQ_code='" + map.get("codeBanque") + "'";

			try {
				final NSArray res = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), vsql);
				if (res != null && res.count() > 0) {
					final NSDictionary dic = (NSDictionary) res.objectAtIndex(0);
					currentChequeDico.put("banqLibelle", dic.valueForKey("BANQ_LIBELLE"));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			chequeSaisiePanel.updateData();

			//Si adresse OK, mémoriser l'adresse du lecteur de cheque dans les preferences.
			myApp.appUserInfo().savePreferenceUser(getEditingContext(), AppUserInfo.PREFKEY_LECTEUR_CHEQUE_ADRESSE, (String) currentChequeDico.get("lecteurChequeAdresse"), AppUserInfo.PREFDEFAULTVALUE_LECTEUR_CHEQUE_ADRESSE, AppUserInfo.PREFDESCRIPTION_LECTEUR_CHEQUE_ADRESSE);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onChequeReadingEnded() {
		System.out.println("Fin lecture cheque");
		actionAnnuleLecteurCheque.setEnabled(false);
		actionLanceLecteurCheque.setEnabled(true);
		actionReinitLecteurCheque.setEnabled(true);
	}

	private final void onChequeReadingStarted() {
		actionAnnuleLecteurCheque.setEnabled(true);
		actionLanceLecteurCheque.setEnabled(false);
		actionReinitLecteurCheque.setEnabled(false);
	}

	private final class LireChequeThread extends Thread {
		private final String serverAddress;
		private LecteurChequeClient myLirecheque;

		public LireChequeThread(final String address) {
			super();
			serverAddress = address;
		}

		public void run() {
			try {
				onChequeReadingStarted();
				lChequeClientListener.onLecteurNotReady();
				if (serverAddress == null) {
					throw new DefaultClientException("L'adresse IP du serveur de chèques n''est pas définie.");
				}
				ZLogger.debug("LES DONNEES LIRE>>>>>>>>\n");
				myLirecheque = new LecteurChequeClient(lChequeClientListener, serverAddress, 2233);
				final Hashtable tmp = myLirecheque.lire();
				ZLogger.debug("LES DONNEES LUES>>>>>>>>\n" + tmp);
				myLirecheque.close();
				ZLogger.debug("LES DONNEES END>>>>>>>>\n");
				onChequeIsRead(tmp);

			} catch (Exception e) {
				e.printStackTrace();
				//String s = e.getMessage();
				lecteurChequeIcon = ZIcon.getIconForName(ZIcon.ICON_WARNING_32);
				lecteurChequeInfo = "<html><b><font color=\"#FF0000\">Erreur : " + e.getMessage() + "</font></b></html>";
				chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();
			} finally {
				try {
					myLirecheque.close();
					myLirecheque = null;
				} catch (Exception e) {
					e.printStackTrace();
				}
				onChequeReadingEnded();
			}
		}

		public void interrupt() {
			try {
				if (myLirecheque != null) {
					myLirecheque.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				lecteurChequeIcon = ZIcon.getIconForName(ZIcon.ICON_WARNING_32);
				lecteurChequeInfo = "<html><b><font color=\"#FF0000\">Erreur : " + e.getMessage() + "</font></b></html>";
			}
			super.interrupt();
		}

	}

	/**
	 * Apparemment ca marche pas...
	 */
	private final void reinitLecteur() {
		try {
			LecteurChequeClient.annuler((String) currentChequeDico.get("lecteurChequeAdresse"), 2233, null);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class LecteurChequeClientListener implements LecteurChequeClient.ILecteurChequeClientListener {

		public void onChequeAnalyse() {
			lecteurChequeInfo = "<html><b><font color=\"#0000FF\">Chèque analysé...</font></b></html>";
			chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();

		}

		public void onChequeLu() {
			lecteurChequeInfo = "<html><b><font color=\"#0000FF\">Chèque lu...</font></b></html>";
			chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();
		}

		public void onLecteurReady() {
			lecteurChequeInfo = "<html><b><font color=\"#0000FF\">Passez le chèque dans le lecteur...</font></b></html>";
			chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();
		}

		public void onLecteurNotReady() {
			lecteurChequeInfo = "<html><b><font color=\"#FF0000\">Veuillez patienter...</font></b></html>";
			chequeSaisiePanel.getChequeFormPanel().getLecteurChequePanel().updateData();
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return chequeSaisiePanel;
	}

	public String title() {
		return TITLE;
	}
}
