/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BdChequeSrchFilterPanel extends ZKarukeraPanel {
//    private final Color BORDURE_COLOR=getBackground().brighter();
    
    private IEcritureSrchFilterPanel myListener;
    
    
//    private ZTextField  fNumeroMin;
//    private ZTextField  fNumeroMax;

    private ZFormPanel numerosPanel;
    private ZFormPanel datesPanel;
    
//    private ZDatePickerField dateSaisieMinField;
//    private ZDatePickerField dateSaisieMaxField;
    
    
    
    /**
     * @param editingContext
     */
    public BdChequeSrchFilterPanel(IEcritureSrchFilterPanel listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(new JButton(myListener.getActionSrch() ), BorderLayout.EAST);
    }

    
    


    private final JPanel buildFilters() {
        numerosPanel = buildNumeroFields();
        datesPanel = buildDateFields();

        setSimpleLineBorder(numerosPanel);
        setSimpleLineBorder(datesPanel);
                
        
        JPanel p = new JPanel(new BorderLayout());
        Component[] comps = new Component[2];
        comps[0] = numerosPanel;
        comps[1] = datesPanel;
        
        p.add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;
    }
    
    
    /**
     * @return
     */
    private final ZFormPanel buildNumeroFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= N° <=", new NumeroMinModel(), new NumeroMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER_BRUT );
    }
    
    
    
    private final ZFormPanel buildDateFields() {
        return ZFormPanel.buildFourchetteDateFields("<= Date <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16)  );
    }    
    

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        numerosPanel.updateData();
    }


    
    
   
    
    
    public interface IEcritureSrchFilterPanel {
        
        /**
         * @return
         */
        public Action getActionSrch();

        /**
         * @return un dictionnaire dans lequel sont stockés les filtres.
         */
        public HashMap getFilters();
        
    }
    
    
    private final class NumeroMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("borNumMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("borNumMin",value);
            
        }
        
    }
    private final class NumeroMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("borNumMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("borNumMax",value);
            
        }
        
    }
    
    
    
    
    
    private final class DateSaisieMaxFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("borDateVisaMax");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("borDateVisaMax", value);
        }

        public Window getParentWindow() {
            return getMyDialog();
        }
    }
    
    private final class DateSaisieMinFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("borDateVisaMin");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("borDateVisaMin", value);
        }

        public Window getParentWindow() {
            return getMyDialog();
        }
    }    
    
    
}
