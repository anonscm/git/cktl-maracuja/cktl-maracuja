/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.zutil.client.ui.ZDropDownButton;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BdChequeSrchPanel extends ZKarukeraPanel {
	private IBdChequeSrchPanelListener myListener;

	private BdChequeSrchFilterPanel filterPanel;
	private BdChequeListPanel bdChequeListPanel;
	private ChequeListPanel chequeListPanel;

	public BdChequeSrchPanel(IBdChequeSrchPanelListener listener) {
		super();
		myListener = listener;

		filterPanel = new BdChequeSrchFilterPanel(new BdChequeSrchFilterPanelListener());
		bdChequeListPanel = new BdChequeListPanel(new BdChequeListPanelListener());
		chequeListPanel = new ChequeListPanel(new ChequeListPanelListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		filterPanel.setMyDialog(getMyDialog());
		filterPanel.initGUI();
		bdChequeListPanel.initGUI();
		chequeListPanel.initGUI();

		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
				encloseInPanelWithTitle("Bordereaux", null, ZConst.BG_COLOR_TITLE, bdChequeListPanel, null, null),
				encloseInPanelWithTitle("Chèques", null, ZConst.BG_COLOR_TITLE, chequeListPanel, null, null)), BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		bdChequeListPanel.updateData();
		chequeListPanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionNew());
		list.add(myListener.actionModifier());
		list.add(myListener.actionFermer());
		list.add(myListener.actionViser());
		list.add(myListener.actionVoirEcritures());

		ArrayList buttons = ZKarukeraPanel.getButtonListFromActionList(list);
		ZDropDownButton bImprimer = new ZDropDownButton(myListener.getActionImprimer());
		bImprimer.addActions(myListener.getSortActions());
		buttons.add(bImprimer);

		ArrayList buttons2 = ZKarukeraPanel.getButtonListFromActionList(list);
		ZDropDownButton bImprimer2 = new ZDropDownButton(myListener.getActionImprimer2());
		bImprimer2.addActions(myListener.getSortActions2());
		buttons.add(bImprimer2);

		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public EOBordereau getSelectedBordereau() {
		return (EOBordereau) bdChequeListPanel.selectedObject();
	}

	public void setSelectedBordereau(EOBordereau obj) {
		bdChequeListPanel.setSelectedObject(obj);
	}

	public interface IBdChequeSrchPanelListener {
		public Action actionClose();

		public ArrayList getSortActions2();

		public Action getActionImprimer2();

		/**
		 * @return
		 */
		public ArrayList getSortActions();

		/**
		 * @return
		 */
		public Action actionFermer();

		public Action getActionImprimer();

		public Action actionNew();

		public Action actionModifier();

		public Action actionViser();

		public Action actionVoirEcritures();

		public NSArray getCheques();

		public NSArray getBordereaux();

		/**
		 * @return un dictioniare contenant les filtres
		 */
		public HashMap getFilters();

		/**
		 * @return
		 */
		public Action actionSrch();

		/**
         * 
         */
		public void bdChequeSelectionChanged();

		/**
         * 
         */
		public void onBdChequeDbClick();

	}

	private final class ChequeListPanelListener implements ChequeListPanel.IChequeListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureDetailListPanel.IEcritureDetailListPanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getCheques();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeListPanel.IChequeListPanelListener#onselectionChanged()
		 */
		public void onselectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.ChequeListPanel.IChequeListPanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

	}

	private final class BdChequeListPanelListener implements BdChequeListPanel.IBdChequeListPanelListener {

		public void selectionChanged() {
			myListener.bdChequeSelectionChanged();
		}

		public NSArray getData() {
			return myListener.getBordereaux();
		}

		public void onDbClick() {
			myListener.onBdChequeDbClick();

		}

	}

	private final class BdChequeSrchFilterPanelListener implements BdChequeSrchFilterPanel.IEcritureSrchFilterPanel {

		public Action getActionSrch() {
			return myListener.actionSrch();
		}

		public HashMap getFilters() {
			return myListener.getFilters();
		}

	}

	public BdChequeListPanel getBdChequeListPanel() {
		return bdChequeListPanel;
	}

	public ChequeListPanel getChequeListPanel() {
		return chequeListPanel;
	}

}
