/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOCheque;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;



public final class ChequeListPanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener {
		protected ZEOTable myEOTable;
		protected ZEOTableModel myTableModel;
		protected EODisplayGroup myDisplayGroup;
		protected TableSorter myTableSorter;
		protected Vector myCols;

		private IChequeListPanelListener myListener;

        /**
         * @param editingContext
         */
        public ChequeListPanel(IChequeListPanelListener listener) {
            super();
            myListener = listener;
        }

    	private void initTableModel() {
    		myCols = new Vector(6,0);



    		ZEOTableModelColumn rcptCode = new ZEOTableModelColumn(myDisplayGroup,"rcptCode","Type",50);
    		rcptCode.setAlignment(SwingConstants.LEFT);

    		ZEOTableModelColumn cheNumeroCheque = new ZEOTableModelColumn(myDisplayGroup,"cheNumeroCheque","N°",80);
    		cheNumeroCheque.setAlignment(SwingConstants.LEFT);

    		ZEOTableModelColumn cheDateCheque = new ZEOTableModelColumn(myDisplayGroup,"cheDateCheque","Date",80);
    		cheDateCheque.setAlignment(SwingConstants.LEFT);
    		cheDateCheque.setFormatDisplay(ZConst.FORMAT_DATESHORT);



    		ZEOTableModelColumn nomPrenomTireur = new ZEOTableModelColumn(myDisplayGroup,"nomPrenomTireur","Tireur",150);
    		nomPrenomTireur.setAlignment(SwingConstants.CENTER);

    		ZEOTableModelColumn cheNumeroCompte = new ZEOTableModelColumn(myDisplayGroup,"cheNumeroCompte","N° Compte",150);
    		cheNumeroCompte.setAlignment(SwingConstants.CENTER);

    		ZEOTableModelColumn banqCode = new ZEOTableModelColumn(myDisplayGroup,"banqCode","Code banque",150);
    		banqCode.setAlignment(SwingConstants.CENTER);

    		ZEOTableModelColumn banqAgence = new ZEOTableModelColumn(myDisplayGroup,"banqAgence","Agence",150);
    		banqAgence.setAlignment(SwingConstants.CENTER);

    		ZEOTableModelColumn banqLibelle = new ZEOTableModelColumn(myDisplayGroup,"banqLibelle","Banque",150);
    		banqLibelle.setAlignment(SwingConstants.CENTER);

    		ZEOTableModelColumn utilisateur = new ZEOTableModelColumn(myDisplayGroup,"utilisateur.nomAndPrenom","Utilisateur",150);
    		utilisateur.setAlignment(SwingConstants.CENTER);


    		ZEOTableModelColumn cheMontant = new ZEOTableModelColumn(myDisplayGroup,"cheMontant","Montant",90);
    		cheMontant.setAlignment(SwingConstants.RIGHT);
    		cheMontant.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
    		cheMontant.setColumnClass(BigDecimal.class);

    		myCols.add(cheNumeroCheque);
    		myCols.add(rcptCode);
    		myCols.add(cheDateCheque);
    		myCols.add(nomPrenomTireur);
    		myCols.add(cheMontant);
    		myCols.add(cheNumeroCompte);
    		myCols.add(banqCode);
    		myCols.add(banqLibelle);
    		myCols.add(banqAgence);
    		myCols.add(utilisateur);


    		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
    		myTableSorter = new TableSorter (myTableModel);
    	}

    	/**
    	 * Initialise la table à afficher (le modele doit exister)
    	 */
    	private void initTable() {
    		myEOTable = new ZEOTable(myTableSorter);
    		myEOTable.addListener(this);
    		myTableSorter.setTableHeader(myEOTable.getTableHeader());
    		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    	}



        public void initGUI() {
            myDisplayGroup = new EODisplayGroup();

            initTableModel();
            initTable();

    		setLayout(new BorderLayout());
    		add(new JScrollPane(myEOTable), BorderLayout.CENTER);

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
         */
        public void updateData() throws Exception {
            myDisplayGroup.setObjectArray(myListener.getData());
    		myEOTable.updateData();
        }


    	public interface IChequeListPanelListener {
    	    public NSArray getData();
    	    public void onselectionChanged();
            /**
             *
             */
            public void onDbClick();
    	}


        /**
         * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
         */
        public void onDbClick() {
            myListener.onDbClick();
        }

        /**
         * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
         */
        public void onSelectionChanged() {
            myListener.onselectionChanged();

        }

	    public EOEnterpriseObject getSelectedObject() {
	        return (EOEnterpriseObject) myEOTable.getSelectedObject();
	    }

        /**
         * @param cheque
         */
        public void selectObject(EOCheque cheque) {
            myDisplayGroup.setSelectedObject(cheque);
        }


	}
