/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.factory.FactoryBrouillardCheque;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.metier.EOChequeBrouillard;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ChequeBrouillardListPanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener, TableModelListener {
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;

	private Vector myCols;
	private IChequeBrouillardListPanelListener myListener;

	private PcoSelectDlg planComptableSelectionDialog;
	private ZEOSelectionDialog gestionSelectionDialog;
	private EODisplayGroup dgGestion;

	/**
     * 
     */
	public ChequeBrouillardListPanel(IChequeBrouillardListPanelListener listener) {
		super();
		myListener = listener;
	}

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	}

	private ZEOSelectionDialog createGestionSelectionDialog() {
		final Vector myCols1 = new Vector(2, 0);
		final ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgGestion, EOGestion.GES_CODE_KEY, "Code gestion");
		myCols1.add(col1);
		final ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMyDialog(), "Sélection d'un code gestion", dgGestion, myCols1, "Veuillez sélectionner un code gestion dans la liste", null);
		return dialog;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		myDisplayGroup = new EODisplayGroup();

		//        dgPlancomptable = new EODisplayGroup();
		// les données changent en fonction du type de journal selectionné pour
		// l'ecriture

		dgGestion = new EODisplayGroup();
		dgGestion.setObjectArray(myListener.getGestions());

		planComptableSelectionDialog = createPlanComptableSelectionDialog();
		planComptableSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		gestionSelectionDialog = createGestionSelectionDialog();
		gestionSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		initTableModel();
		initTable();
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() {
		NSArray res = myListener.getData();
		myDisplayGroup.setObjectArray(res);
		myEOTable.updateData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		myListener.onTableChanged();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.onSelectionChanged();
	}

	public EOChequeBrouillard selectedChequeBrouillard() {
		return (EOChequeBrouillard) myEOTable.getSelectedObject();
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableModel);
		myEOTable.addListener(this);
		// myEOTable.setPreferredSize(new Dimension(680,250));
		// myTableSorter.setTableHeader(myEOTable.getTableHeader());

		// Gestion du clavier
		myEOTable.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				if (!evt.isConsumed())
					doKeyPressed(evt);
			}
		});
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private void initTableModel() {
		myCols = new Vector(4, 0);

		ZEOTableModelColumn colEcdPlancomptablePcoNum = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoNum", "Imputation", 68);
		colEcdPlancomptablePcoNum.setAlignment(SwingConstants.LEFT);
		// colEcdPlancomptablePcoNum.setColumnClass(String.class);
		colEcdPlancomptablePcoNum.setEditable(true);
		colEcdPlancomptablePcoNum.setMyModifier(new pcoNumModifier());
		colEcdPlancomptablePcoNum.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colEcdPlancomptablePcoLibelle = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoLibelle", "Libelle Imputation", 263);
		colEcdPlancomptablePcoLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colLibelle = new ZEOTableModelColumn(myDisplayGroup, "chbLibelle", "Libellé brouillard", 263);
		colLibelle.setAlignment(SwingConstants.LEFT);
		colLibelle.setEditable(true);
		colLibelle.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colEcdGestionGesCode = new ZEOTableModelColumn(myDisplayGroup, "gestion.gesCode", "Code gestion", 83);
		colEcdGestionGesCode.setAlignment(SwingConstants.CENTER);
		colEcdGestionGesCode.setEditable(true);
		colEcdGestionGesCode.setMyModifier(new gesCodeModifier());
		colEcdGestionGesCode.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colEcdCredit = new ZEOTableModelColumn(myDisplayGroup, "chbMontant", "Crédit", 90);
		colEcdCredit.setAlignment(SwingConstants.RIGHT);
		colEcdCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		//        colEcdCredit.setFormatEdit(ZConst.FORMAT_EDIT_NUMBER);
		colEcdCredit.setEditable(true);
		colEcdCredit.setMyModifier(new ecdCreditModifier());
		colEcdCredit.setTableCellEditor(new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), ZConst.FORMAT_EDIT_NUMBER));
		colEcdCredit.setColumnClass(BigDecimal.class);

		myCols.add(colEcdGestionGesCode);
		myCols.add(colEcdPlancomptablePcoNum);
		myCols.add(colEcdPlancomptablePcoLibelle);
		myCols.add(colLibelle);
		myCols.add(colEcdCredit);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		// On desactive le tri sur cette table (pb de saisie)
		// myTableSorter = new TableSorter (myTableModel);
		// myTableSorter.activeSortOnColumn(0, false, false);

		myTableModel.addTableModelListener(this);
		// permet notammen,t d'ajouter la possibilité de controler
		// l'autorisation de modifier au niveau du row
		myTableModel.setMyDelegate(new ZEOTableModelDelegate());
	}

	public void ajouterInDg(EOChequeBrouillard detail) {
		int row = myDisplayGroup.displayedObjects().count();
		myDisplayGroup.insertObjectAtIndex(detail, row);
		myDisplayGroup.setSelectedObject(detail);

		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();

		// CHECK Ne pas utiliser cette methode dans le cas ou la table est
		// modifiable
		// myEOTable.updateData();

		myEOTable.requestFocusInWindow();
		if (myEOTable.editCellAt(row, 0)) {
			myEOTable.changeSelection(row, 0, false, false);
			myEOTable.getCellEditor().cancelCellEditing();
		}

	}

	// /**
	// * Supprime le detail ecriture selectionne.
	// * @throws Exception
	// */
	// public void ecritureDetailSupprimerSelected() throws Exception {
	// if (myDisplayGroup.displayedObjects().count()>0) {
	// if (!myDisplayGroup.deleteSelection()) {
	// throw new DefaultClientException("Impossible de supprimer la ligne.");
	// }
	// myTableModel.updateInnerRowCount();
	// myTableModel.fireTableDataChanged();
	// }
	// }

	/**
	 * Down key moves down one row, appending a row if neccessary F4 key appends a row Up key moves up one row, pre-appending a row if neccessary F5
	 * key inserts a row before the currently selected row F6 & Delete key delete the current row F7 copies and appends to the bottom the current row
	 * F8 selects the first row
	 */
	private void doKeyPressed(KeyEvent evt) {
		// int i, j;
		switch (evt.getKeyCode()) {
		case KeyEvent.VK_DOWN:
			if (myEOTable.getSelectedRow() != myEOTable.getRowCount() - 1) {
				break;
			}
			myListener.addRow();
			evt.consume();
			break;

		case KeyEvent.VK_INSERT:
			if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK) {
				myListener.addRow();
				evt.consume();
			}
			break;
		case KeyEvent.VK_UP:
			if (myEOTable.getSelectedRow() != 0) {
				break;
			}
			// myEOTable.removeEditor();
			myEOTable.setRowSelectionInterval(0, 0);
			// case KeyEvent.VK_F5:
			// try{
			// ((DefaultTableModel)getModel()).insertRow(getSelectedRow(),
			// newRowData() );
			// }catch(Exception ex){}
			// evt.consume();
			// break;
		case KeyEvent.VK_DELETE:
			if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK) {
				myListener.deleteRow();
				evt.consume();
			}

			// if(getRowCount() == 1) break;
			// i = j = getSelectedRow();
			// if(j<0) j = getRowCount() -1;
			// ((DefaultTableModel)getModel()).removeRow(j);
			// try{
			// if( i <0 || i >= getRowCount() ) i = 0;
			// setRowSelectionInterval(i,i);
			// removeEditor();
			// }catch(Exception ex){
			// ex.printStackTrace();
			// }

			break;
		// case KeyEvent.VK_F7:
		// try{
		// Vector row = new Vector();
		// j = getSelectedRow();
		// if(j<0) j = getRowCount() -1;
		// for(i=0;i< getColumnCount(); ++i) row.add( getValueAt(j, i) );
		// if( j == getRowCount() -1 ) ((DefaultTableModel)getModel()).addRow(
		// row );
		// else ((DefaultTableModel)getModel()).insertRow( j+1, row );
		// }catch(Exception ex){}
		// evt.consume();
		// break;
		// case KeyEvent.VK_F8:
		// try{
		// setRowSelectionInterval(0,0);
		// }catch(Exception ex){}
		// evt.consume();
		// break;
		default:
			break;
		}
	}

	// private final class ZPanelBalanceProvider implements
	// ZPanelBalance.IZPanelBalanceProvider {
	// /**
	// * @return la valeur des debits.
	// */
	// public BigDecimal getDebitValue() {
	// NSArray tmp = myDisplayGroup.displayedObjects();
	// BigDecimal val = new BigDecimal(0);
	//
	// for (int i = 0; i < tmp.count(); i++) {
	// EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
	// if (array_element.ecdDebit()!=null) {
	// val = val.add(array_element.ecdDebit() );
	// }
	// }
	// return val;
	// }
	//
	// public BigDecimal getCreditValue() {
	// NSArray tmp = myDisplayGroup.displayedObjects();
	// BigDecimal val = new BigDecimal(0);
	//
	// for (int i = 0; i < tmp.count(); i++) {
	// EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
	// if (array_element.ecdCredit()!=null) {
	// val = val.add(array_element.ecdCredit() );
	// }
	// }
	// return val;
	// }
	// }

	private class ZEOTableModelDelegate implements ZEOTableModel.IZEOTableModelDelegate {
		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate#isCellEditable(int, int)
		 */
		public boolean isCellEditable(int row, int col) {
			return true;
		}
	}

	public interface IChequeBrouillardListPanelListener {
		// public EOEcriture getEcriture();
		/**
		 * @return
		 */
		public NSArray getData();

		/**
         * 
         */
		public void onTableChanged();

		/**
         * 
         */
		public void deleteRow();

		public void onSelectionChanged();

		public FactoryBrouillardCheque getFactoryBrouillardCheque();

		/**
		 * @return
		 */
		public NSArray getPlanComptables();

		/**
		 * @return
		 */
		public NSArray getGestions();

		public void addRow();
	}

	/**
     * 
     */
	public void lockSaisieEcritureDetail(boolean locked) {
		myEOTable.setEnabled(!locked);
	}

	/**
	 * @param pconum
	 * @param detail
	 */
	private void analysePcoNum(String pconum, EOChequeBrouillard detail) {
		EOPlanComptable res = null;
		EOPlanComptableExer restmp = EOPlanComptableExerFinder.findPlanComptableExerForPcoNumInList(myListener.getPlanComptables(), pconum);
		if (restmp == null) {
			res = selectPlanComptableIndDialog(pconum);
		}
		else {
			res = restmp.planComptable();
		}

		//        EOPlanComptable res = null;
		//        final int leRow = myEOTable.getSelectedRow();
		//        NSArray tmp = EOQualifier.filteredArrayWithQualifier(myListener.getPlanComptables(), EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + pconum + "'", null));
		//        if (tmp.count() == 1) {
		//            // pas de doute sur le choix du pcoNum
		//            res = (EOPlanComptable) tmp.objectAtIndex(0);
		//        } else {
		//            // on affiche une fenetre de recherche de PlanComptable
		//            res = selectPlanComptableIndDialog(pconum);
		//        }

		myListener.getFactoryBrouillardCheque().chequeBrouillardVideSetPlanComptable(detail, res);
		if (res != null) {
			// ZLogger.debug( "index row : " +leRow);
			// ZLogger.debug( "index col :
			// "+myEOTable.convertColumnIndexToView( 3));
			// On arrete l'editon
			if (myEOTable.isEditing()) {
				myEOTable.getCellEditor().cancelCellEditing();
			}

			// On passe en edition sur la colonne suivante (gestion), c'est
			// l'indice correspondant dans le tableau myCols
			// myEOTable.editCellAt(leRow, myEOTable.convertColumnIndexToView( 3
			// ) );
		}

		myTableModel.fireTableDataChanged();
		// myEOTable.setColumnSelectionInterval(myEOTable.convertColumnIndexToView(
		// 3 ), myEOTable.convertColumnIndexToView( 3 ));
	}

	private void analyseGesCode(String gesCode, EOChequeBrouillard detail) {
		EOGestion res = null;
		//        final int leRow = myEOTable.getSelectedRow();
		NSArray tmp = EOQualifier.filteredArrayWithQualifier(myListener.getGestions(), EOQualifier.qualifierWithQualifierFormat("gesCode like '" + gesCode + "*'", null));
		if (tmp.count() == 1) {
			// pas de doute sur le choix de la gestion
			res = (EOGestion) tmp.objectAtIndex(0);
		}
		else {
			// on affiche une fenetre de recherche de PlanComptable
			res = selectGestionIndDialog();
		}

		myListener.getFactoryBrouillardCheque().chequeBrouillardVideSetGestion(detail, res);

		if (res != null) {
			// On passe en edition sur la colonne suivante (libelle), c'est
			// l'indice correspondant dans le tableau myCols
			// ZLogger.debug( myTableModel.getColumnName(4));
			// myEOTable.editCellAt(leRow, myEOTable.convertColumnIndexToView( 4
			// ) );
			// myEOTable.editCellAt(myEOTable.getSelectedRow(),
			// myEOTable.convertColumnIndexToView( 4 ) );
		}
		myTableModel.fireTableDataChanged();
	}

	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		if (planComptableSelectionDialog.open(myListener.getPlanComptables()) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;
		//        setWaitCursor(true);
		//        EOPlanComptable res = null;
		//        dgPlancomptable.setObjectArray(myListener.getPlanComptables());
		//        planComptableSelectionDialog.getFilterTextField().setText(filter);
		//        if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
		//        	NSArray selections = planComptableSelectionDialog.getSelectedEOObjects();
		//        	
		//        	if ((selections == null) || (selections.count() == 0)) {
		//        		res = null;
		//        	} else {
		//        		res = (EOPlanComptable) selections.objectAtIndex(0);
		//        	}
		//        }
		//        setWaitCursor(false);
		//        return res;
	}

	private EOGestion selectGestionIndDialog() {
		setWaitCursor(true);
		EOGestion res = null;
		if (gestionSelectionDialog.open() == ZEOSelectionDialog.MROK) {
			NSArray selections = gestionSelectionDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOGestion) selections.objectAtIndex(0);
			}
		}
		setWaitCursor(false);
		return res;
	}

	private class pcoNumModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			EOChequeBrouillard tmp = (EOChequeBrouillard) myDisplayGroup.selectedObject();
			ZLogger.debug("Objet en cours ", tmp);
			analysePcoNum((String) value, tmp);
		}

	}

	private class gesCodeModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			EOChequeBrouillard tmp = (EOChequeBrouillard) myDisplayGroup.selectedObject();
			analyseGesCode((String) value, tmp);
		}

	}

	private class ecdCreditModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			// EOEcritureDetail tmp = (EOEcritureDetail)
			// myDisplayGroup.selectedObject();
			EOChequeBrouillard tmp = (EOChequeBrouillard) myDisplayGroup.displayedObjects().objectAtIndex(row);
			tmp.setChbSens(ZConst.SENS_CREDIT);
			if (value instanceof Number) {
				myListener.getFactoryBrouillardCheque().chequeBrouillardVideSetMontant(tmp, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		}

	}

	/**
	 * @return
	 */
	public EOEnterpriseObject getSelectedObject() {
		return (EOEnterpriseObject) myEOTable.getSelectedObject();
	}

	public void cancelCellEditing() {
		if (isEditing()) {
			myEOTable.cancelCellEditing();
		}
	}

	public boolean isEditing() {
		return myEOTable.isEditing();
	}

	public boolean stopCellEditing() {
		if (isEditing()) {
			return myEOTable.stopCellEditing();
		}
		return true;
	}

}
