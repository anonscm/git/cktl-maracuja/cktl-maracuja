/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;




/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BdChequeFormPanel extends ZKarukeraPanel {
//    private final int DEFAULT_LABEL_WIDTH=100;
    
    private IBdChequeFormPanelListener myListener;
    
    private ZTextField myLibelleBordereau;
    private ZTextField myNumBordereau;
//    private ZTextField myCodeGestion;
    private ZActionField myPcoLibelleVisa;
    private JComboBox myCodeGestion;
    
    
    
    
    
    
    /**
     * 
     */
    public BdChequeFormPanel(IBdChequeFormPanelListener listener) {
        super();
        myListener = listener;
    }
    

    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());

        myNumBordereau = new ZNumberField(new NumeroBordereauModel(), new Format[]{ZConst.FORMAT_ENTIER}, ZConst.FORMAT_ENTIER);
        myNumBordereau.getMyTexfield().setColumns(10);
        myNumBordereau.getMyTexfield().setEditable(false);
        myNumBordereau.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
        
        myLibelleBordereau = new ZTextField ( new LibelleBordereauModel()  );
        myLibelleBordereau.getMyTexfield().setColumns(35);

        myCodeGestion = new JComboBox(myListener.getCodeGestionModel());
        myCodeGestion.addActionListener(new CodeGestionListener());
        
//        myCodeGestion = new ZTextField (new CodeGestionModel());
//        myCodeGestion.getMyTexfield().setColumns(5);

        myPcoLibelleVisa = new ZActionField(new PcoLibelleModel(),(AbstractAction)myListener.actionPlancomptableVisaSelect() );
        myPcoLibelleVisa.getMyTexfield().setColumns(30);
        myPcoLibelleVisa .getMyTexfield().setEditable(false);        
        
        Box col1 = Box.createVerticalBox();
        col1.add(buildLine(new ZLabeledComponent("Libellé ", myLibelleBordereau, ZLabeledComponent.LABELONLEFT, -1)));
        col1.add(buildLine(new ZLabeledComponent("Code gestion ", myCodeGestion, ZLabeledComponent.LABELONLEFT, -1)));
        col1.add(new JPanel(new BorderLayout()));
        
        
        Box col2 = Box.createVerticalBox();
        col2.add(buildLine(new ZLabeledComponent("N° ", myNumBordereau, ZLabeledComponent.LABELONLEFT, -1)));
        col2.add(buildLine(new ZLabeledComponent("Compte au débit", buildLine( new Component[]{myPcoLibelleVisa} ), ZLabeledComponent.LABELONLEFT, 120)));
        col2.add(new JPanel(new BorderLayout()));        
        
        
        Box boxH = Box.createHorizontalBox();
        boxH.add(col1);
        boxH.add(col2);
        boxH.add(Box.createGlue());
        this.add(boxH, BorderLayout.WEST);
        
        this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
    }
    
    

    
    
    
    
    /**
     * On charge inititlise les champs avec les valeurs du dico
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myLibelleBordereau.updateData();
        myNumBordereau.updateData();
        myPcoLibelleVisa.updateData();
        myListener.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
    }
    
    
    public interface IBdChequeFormPanelListener {
        public Object getBorLibelle();
        public ZEOComboBoxModel getCodeGestionModel();
        public Action actionPlancomptableVisaSelect();
        public void setBorLibelle(String string);
        public Object getBorNum();
        public void setBorNum(Integer integer);
        public void setGestion(EOGestion gestion);
        public EOGestion getGestion();
        public void setPlanComptableVisa(EOPlanComptable planComptable);
        public EOPlanComptable getPlanComptableVisa();
    }
    
    
    
    
    

    public void lockAll(boolean locked) {
        myNumBordereau.getMyTexfield().setEnabled(!locked);
    }
    
    
    private final class LibelleBordereauModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getBorLibelle();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.setBorLibelle((String)value);
            
        }
        
    }
    private final class NumeroBordereauModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getBorNum();
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            if (value!=null) {
                myListener.setBorNum(new Integer(((Number)value).intValue()));
            }
            else {
                myListener.setBorNum(null);
            }
        }
        
    }
//    private final class CodeGestionModel implements ZTextField.IZTextFieldModel {
//
//        /**
//         * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
//         */
//        public Object getValue() {
//            return 
//        }
//
//        /**
//         * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
//         */
//        public void setValue(Object value) {
//
//        }
//        
//    }

    
    private final class PcoLibelleModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            if (myListener.getPlanComptableVisa() == null){
                return null;
            }
            return myListener.getPlanComptableVisa().pcoNum()+" " + myListener.getPlanComptableVisa().pcoLibelle();
        }

        public void setValue(Object value) {
        }
        
    }     
    
    private final class CodeGestionListener implements ActionListener {

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            myListener.setGestion( (EOGestion) myListener.getCodeGestionModel().getSelectedEObject() )  ;
        }
        
    }
        
    

    
    
    
    

    
    
    
    
}
