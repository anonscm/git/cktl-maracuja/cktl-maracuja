/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOCheque;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BdChequeListPanel extends ZKarukeraPanel implements ZEOTableListener {
    private IBdChequeListPanelListener myListener;

	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected EODisplayGroup myDisplayGroup;
	protected TableSorter myTableSorter;
	protected Vector myCols;



    /**
     * @param editingContext
     */
    public BdChequeListPanel(IBdChequeListPanelListener listener) {
        super();
        myListener = listener;
    }



	private void initTableModel() {
		myCols = new Vector(4,0);

		ZEOTableModelColumn borNum = new ZEOTableModelColumn(myDisplayGroup,"borNum","N° Bordereau",60);
		borNum.setAlignment(SwingConstants.RIGHT);
		borNum.setColumnClass(Integer.class);


		ZEOTableModelColumn borDateVisa = new ZEOTableModelColumn(myDisplayGroup,"borDateVisa","Date visa",90);
		borDateVisa.setAlignment(SwingConstants.CENTER);
		borDateVisa.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		borDateVisa.setColumnClass(Date.class);

		ZEOTableModelColumn borLibelle = new ZEOTableModelColumn(myDisplayGroup,"bordereauInfo.borLibelle","Libellé",150);
		borLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn borEtat = new ZEOTableModelColumn(myDisplayGroup,"borEtat","Etat",100);
		borEtat.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colUtilisateur = new ZEOTableModelColumn(myDisplayGroup,"utilisateur.nomAndPrenom","Utilisateur",130);
		colUtilisateur.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup,"gestion.gesCode","Code Gestion",90);
		gesCode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn planComptableVisa = new ZEOTableModelColumn(myDisplayGroup,"bordereauInfo.planComptableVisa.pcoNum","Compte de débit",130);
		planComptableVisa.setAlignment(SwingConstants.CENTER);


		ZEOTableModelColumnWithProvider nbCheques = new ZEOTableModelColumnWithProvider("NB Chèques", new NBChequesProvider(),90);
		nbCheques.setAlignment(SwingConstants.CENTER);
		nbCheques.setColumnClass(Integer.class);

		myCols.add(gesCode);
		myCols.add(borNum);
		myCols.add(borLibelle);
		myCols.add(nbCheques);
		myCols.add(borEtat);
		myCols.add(borDateVisa);
		myCols.add(colUtilisateur);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter (myTableModel);
	}




	private final class NBChequesProvider implements ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider {

        /**
         * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider#getValueAtRow(int)
         */
        public Object getValueAtRow(int row) {
            final EOBordereau bord = (EOBordereau) myDisplayGroup.displayedObjects().objectAtIndex(row);
            final NSArray cheques = EOQualifier.filteredArrayWithQualifier(bord.cheques(), EOQualifier.qualifierWithQualifierFormat("cheEtat<>%@", new NSArray(EOCheque.etatAnnule))   );
            return new Integer(cheques.count());
        }

	}




	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}



    public void initGUI() {
        myDisplayGroup = new EODisplayGroup();
        initTableModel();
        initTable();
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(myListener.getData() );
        myEOTable.updateData();
    }



	public interface IBdChequeListPanelListener {
        public void selectionChanged();
        public NSArray getData();
        public void onDbClick();
	}







	/**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        myListener.onDbClick();
    }



    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.selectionChanged();
    }



    /**
     * @return l'objet selectionne.
     */
    public EOEnterpriseObject selectedObject() {
        return (EOEnterpriseObject) myDisplayGroup.selectedObject();
    }

    public void setSelectedObject(EOEnterpriseObject obj ) {
        myDisplayGroup.setSelectedObject(obj);
      myTableModel.updateInnerRowCount();
      myTableModel.fireTableDataChanged();
    }










}
