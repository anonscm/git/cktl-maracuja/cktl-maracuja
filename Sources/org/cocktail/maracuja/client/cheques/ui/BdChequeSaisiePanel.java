/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;


/**
 * Panel qui affiche les 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class BdChequeSaisiePanel extends ZKarukeraPanel {
    private BdChequeFormPanel bdChequeFormPanel;
    private ChequeBrouillardListPanel chequeBrouillardListPanel;
    private ChequeListPanel chequeListPanel;
    
    private IBdChequeSaisiePanelListener myListener;
    private ZPanelBalance panelBalance;
    
    
    
    /**
     * 
     */
    public BdChequeSaisiePanel(IBdChequeSaisiePanelListener listener, 
            BdChequeFormPanel.IBdChequeFormPanelListener saisieFormListener, 
            ChequeListPanel.IChequeListPanelListener chequeListPanelListener, 
            ChequeBrouillardListPanel.IChequeBrouillardListPanelListener saisieDetailListener,
            ZPanelBalance.IZPanelBalanceProvider balanceProvider) {
        super();
        myListener = listener;
        
        bdChequeFormPanel = new BdChequeFormPanel(saisieFormListener);
        chequeListPanel = new ChequeListPanel(chequeListPanelListener);
        chequeBrouillardListPanel = new ChequeBrouillardListPanel(saisieDetailListener);
        panelBalance = new ZPanelBalance(balanceProvider, true);
    }
  
    /**
     * @throws Exception
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {     
        
        bdChequeFormPanel.setMyDialog(getMyDialog());
        chequeBrouillardListPanel.setMyDialog(getMyDialog());
        bdChequeFormPanel.initGUI();
        chequeBrouillardListPanel.initGUI();
        chequeListPanel.initGUI();
        panelBalance.initGUI();
        
        updateInputMap();
        
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
        this.add(buildCenterPanelForSaisie(), BorderLayout.CENTER);
//        this.add(buildRightPanel(), BorderLayout.EAST);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);             

    }

    
	private final JToolBar buildToolBarCheques() {
	    JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);
		//Initialiser les boutons à partir des iactions	
		myToolBar.add(myListener.myActionAddCheque());
		myToolBar.add(myListener.myActionModifyCheque());
		myToolBar.add(myListener.myActionDeleteCheque());
		myToolBar.addSeparator();
        myToolBar.add(myListener.myActionDupliqueCheque());
		return myToolBar;
	}	     
	private final JToolBar buildToolBarChequeBrouillards() {
	    JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);
		//Initialiser les boutons à partir des iactions	
		myToolBar.add(myListener.myActionAddChequeBrouillard());
		myToolBar.add(myListener.myActionDeleteChequeBrouillard());
		return myToolBar;
	}	     
    
    private final JPanel buildCenterPanelForSaisie() {
        
        JPanel p = new JPanel(new BorderLayout());
        p.add(buildToolBarCheques(), BorderLayout.NORTH);
        p.add(chequeListPanel, BorderLayout.CENTER);
        
        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(buildToolBarChequeBrouillards(), BorderLayout.NORTH);
        p2.add(chequeBrouillardListPanel, BorderLayout.CENTER);
        
        
        JSplitPane s = ZKarukeraPanel.buildVerticalSplitPane(encloseInPanelWithTitle("Chèques", null,ZConst.BG_COLOR_TITLE,p,null, null), encloseInPanelWithTitle("Brouillards", null,ZConst.BG_COLOR_TITLE,p2,null, null));        
        
        JPanel p3 = new JPanel(new BorderLayout());
        p3.add(encloseInPanelWithTitle("Bordereau", null,ZConst.BG_COLOR_TITLE,bdChequeFormPanel, null, null ), BorderLayout.NORTH);
        p3.add(s , BorderLayout.CENTER);
        p3.add(panelBalance, BorderLayout.SOUTH);
        return p3;
    }

    
    
    
    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionClose());        
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }
    
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        bdChequeFormPanel.updateData();
        chequeListPanel.updateData();
        chequeBrouillardListPanel.updateData();
        panelBalance.updateData();
    }
    
    
    public interface IBdChequeSaisiePanelListener {
        public Action actionValider();
        public Action actionClose();
        public Action myActionDeleteCheque();
        public Action myActionAddCheque();
        public Action myActionModifyCheque();
        public Action myActionDupliqueCheque();        
        public Action myActionDeleteChequeBrouillard();
        public Action myActionAddChequeBrouillard();
        
        
    }
    
	
	
    public BdChequeFormPanel getBdChequeFormPanel() {
        return bdChequeFormPanel;
    }
    public ChequeBrouillardListPanel getChequeBrouillardListPanel() {
        return chequeBrouillardListPanel;
    }
    public ChequeListPanel getChequeListPanel() {
        return chequeListPanel;
    }

    /**
     * @return
     */
    public ZPanelBalance getBalancePanel() {
        return panelBalance;
    }

    public void updateInputMap() {
        getActionMap().clear();
        getInputMap().clear();
        
//        final KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        final KeyStroke f10 = KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0, false);
        final KeyStroke ins = KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0, false);
        
//        getActionMap().put("ESCAPE", myListener.actionClose());          
        getActionMap().put("F10", myListener.actionValider());
        getActionMap().put("ins", myListener.myActionAddCheque() );
        
//        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(f10, "F10");    
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ins, "ins");    
                  
        
    }    
}	

