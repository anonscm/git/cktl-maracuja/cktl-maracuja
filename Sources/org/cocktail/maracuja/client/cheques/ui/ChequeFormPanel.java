/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.cheques.ui;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;




/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ChequeFormPanel extends ZKarukeraPanel {
    
    private final int DEFAULT_LABEL_WIDTH=100;
    
    private IChequeFormPanelListener myListener;
    
    private ZTextField banqAgence;
    private ZTextField banqCode;
    private ZTextField banqLibelle;
    private ZDatePickerField cheDateCheque;
//    private ZTextField cheDateCreation;
    private ZTextField cheNomTireur;
    private ZTextField cheNumeroCheque;
    private ZTextField cheNumeroCompte;
    private ZTextField chePrenomTireur;
    private JComboBox rcptCode;
    private ZNumberField cheMontant;
    
    
    
    private LecteurChequePanel lecteurChequePanel;
    
    
    
    /**
     * 
     */
    public ChequeFormPanel(IChequeFormPanelListener listener) {
        super();
        myListener = listener;
    }
    

    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        
        banqAgence = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "banqAgence") );
        banqCode = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "banqCode") );
        banqLibelle = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "banqLibelle") );
        cheNomTireur = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "cheNomTireur") );
        cheNumeroCheque = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "cheNumeroCheque") );
        cheNumeroCompte = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "cheNumeroCompte") );
        chePrenomTireur = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "chePrenomTireur") );
        cheMontant = new ZNumberField( new MontantModel(), new Format[]{ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL}, ZConst.FORMAT_DECIMAL );
//        cheDateCheque = new ZDatePickerField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "cheDateCheque"), (DateFormat)ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16),myListener.getDialog() );
        
        MyDatePickerFieldModel dateMdl = new MyDatePickerFieldModel(myListener.getCurrentChequeDico(), "cheDateCheque");
        cheDateCheque = new ZDatePickerField( dateMdl, (DateFormat)ZConst.FORMAT_DATESHORT, null,ZIcon.getIconForName(ZIcon.ICON_7DAYS_16) );
        
        rcptCode = new JComboBox(myListener.getRcptCodeModel());
        
        rcptCode.addActionListener(new RcptCodeListener());
        
        
        banqAgence.getMyTexfield().setColumns(35);
        banqCode.getMyTexfield().setColumns(10);
        banqLibelle.getMyTexfield().setColumns(35);
        cheNomTireur.getMyTexfield().setColumns(30);
        cheNumeroCheque.getMyTexfield().setColumns(10);
        cheNumeroCompte.getMyTexfield().setColumns(10);
        chePrenomTireur.getMyTexfield().setColumns(30);
        cheMontant.getMyTexfield().setColumns(10);
        cheDateCheque.getMyTexfield().setColumns(10);
        
        
        ZLabeledComponent labelNom = new ZLabeledComponent("Nom tireur ", cheNomTireur, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelPrenom = new ZLabeledComponent("Prénom tireur ", chePrenomTireur, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelCodeBanque = new ZLabeledComponent("Code banque", banqCode, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelNomBanque = new ZLabeledComponent("Nom Banque", banqLibelle, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelAgence = new ZLabeledComponent("Agence", banqAgence, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelNumCompte = new ZLabeledComponent("N° compte", cheNumeroCompte, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelTypeCheque = new ZLabeledComponent("Type de chèque", rcptCode, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelNumCheque = new ZLabeledComponent("N° chèque", cheNumeroCheque, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelMontant = new ZLabeledComponent("Montant", cheMontant, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        ZLabeledComponent labelDateCheque = new ZLabeledComponent("Date du chèque", cheDateCheque, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
        
        
        
        labelNom.getMyLabel().setFont( labelNom.getFont().deriveFont(Font.BOLD) );
        labelNumCheque.getMyLabel().setFont( labelNom.getFont().deriveFont(Font.BOLD) );
        labelMontant.getMyLabel().setFont( labelNom.getFont().deriveFont(Font.BOLD) );
        
        
        Box boxBanque = Box.createVerticalBox();
        boxBanque.setBorder(BorderFactory.createTitledBorder("Banque"));
        boxBanque.add(buildLine(labelCodeBanque));
        boxBanque.add(buildLine(labelNomBanque));
        boxBanque.add(buildLine(labelAgence));
        
        
        
        Box boxTireur = Box.createVerticalBox();
        boxTireur.setBorder(BorderFactory.createTitledBorder("Tireur"));
        boxTireur.add(buildLine(labelNom));
        boxTireur.add(buildLine(labelPrenom));
        boxTireur.add(buildLine(labelNumCompte));

        Box boxCheque = Box.createVerticalBox();
        boxCheque.setBorder(BorderFactory.createTitledBorder("Chèque"));        
        boxCheque.add(buildLine(labelTypeCheque));
        boxCheque.add(buildLine(labelNumCheque));
        boxCheque.add(buildLine(labelMontant));
        boxCheque.add(buildLine(labelDateCheque));  
        
        Box col1 = Box.createVerticalBox();
        col1.add(boxBanque);
        col1.add(boxTireur);
        col1.add(boxCheque);
        col1.add(new JPanel(new BorderLayout()));
        
        
       
        lecteurChequePanel = new LecteurChequePanel();
        
        
        
        Box boxH = Box.createHorizontalBox();
        boxH.add(col1);
        boxH.add(lecteurChequePanel);
        
        boxH.add(Box.createGlue());
        this.add(boxH, BorderLayout.WEST);
        
        this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
    }
    
    
    
    
    private class MyDatePickerFieldModel extends DefaultTextFieldModel implements IZDatePickerFieldModel {
        public MyDatePickerFieldModel(final Map filter, final String key) {
            super(filter, key);
        }
        
        public Window getParentWindow() {
            return getMyDialog();
        }
        
    }    
    
    
    
    /**
     * On charge inititlise les champs avec les valeurs du dico
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        ZLogger.debug(myListener.getCurrentChequeDico());
        banqAgence.updateData();
        banqCode.updateData();
        banqLibelle.updateData();
        cheNomTireur.updateData();
        cheNumeroCheque.updateData();
        cheNumeroCompte.updateData();
        chePrenomTireur.updateData();
        cheMontant.updateData();
        cheDateCheque.updateData();     
        lecteurChequePanel.updateData();
        myListener.getRcptCodeModel().setSelectedItem(myListener.getCurrentChequeDico().get("rcptCode"));
    }
    
    
    public interface IChequeFormPanelListener {
        public HashMap getCurrentChequeDico();
        /**
         * @return
         */
        public Dialog getDialog();
        public DefaultComboBoxModel getRcptCodeModel();
        public Action actionLanceLecteurCheque();
        public Action actionAnnuleLecteurCheque();
        public Icon getLecteurChequeIcon();
        public String getLecteurChequeInfo();
        public Action actionReinitLecteurCheque();
    }
    
    private final class MontantModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getCurrentChequeDico().get("cheMontant");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {           
            if (value!=null ) {
                myListener.getCurrentChequeDico().put("cheMontant", new BigDecimal(((Number)value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP) );
            }            
            else {
                myListener.getCurrentChequeDico().put("cheMontant", null);
            }
            
        }
        
    }
    private final class RcptCodeListener implements ActionListener {

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            myListener.getCurrentChequeDico().put("rcptCode", rcptCode.getSelectedItem());
        }
        
    }
    
    
    
        public class LecteurChequePanel extends JPanel {
            
            private final JLabel labelIcon;
            private final JLabel labelName;
            private ZTextField lecteurChequeAdresse;
            
            
            public LecteurChequePanel() {
                super();
                
                lecteurChequeAdresse = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getCurrentChequeDico(), "lecteurChequeAdresse") );
                lecteurChequeAdresse.getMyTexfield().setColumns(10);
                ZLabeledComponent labelLecteurChequeAdresse = new ZLabeledComponent("Adresse du lecteur", lecteurChequeAdresse, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH);
                
                
                labelIcon = new JLabel();
                labelName = new JLabel();
                
                labelIcon.setVerticalAlignment(SwingConstants.BOTTOM);
                labelName.setVerticalAlignment(SwingConstants.TOP);
                
                labelIcon.setHorizontalAlignment(SwingConstants.CENTER);
                labelName.setHorizontalAlignment(SwingConstants.CENTER);
                
                
                
//                final Box boxLecteur = Box.createVerticalBox();
//                boxLecteur.setBorder(BorderFactory.createTitledBorder("Lecteur de chèque"));
//                boxLecteur.add(new JPanel(new BorderLayout()));        
//                
                
                final ArrayList list = new ArrayList();
                list.add(myListener.actionLanceLecteurCheque());
                list.add(myListener.actionAnnuleLecteurCheque());
//                list.add(myListener.actionReinitLecteurCheque());
                
                
                final ArrayList list2 = ZKarukeraPanel.getButtonListFromActionList(list, 100, 25);
                list2.add(0, buildLine(labelLecteurChequeAdresse));
                final JPanel panel = ZKarukeraPanel.buildVerticalPanelOfComponents(list2);
                panel.setBorder(BorderFactory.createEmptyBorder(50, 10, 50, 10));
                

                
                
                setLayout(new BorderLayout());
                setBorder(BorderFactory.createTitledBorder("Lecteur de chèque"));
                add(panel, BorderLayout.NORTH);
                
                
                final Box b = Box.createHorizontalBox();
                b.add(Box.createHorizontalGlue());
                b.add(labelIcon);
                b.add(Box.createHorizontalGlue());
                
                final Box b2 = Box.createHorizontalBox();
                b2.add(Box.createHorizontalGlue());
                b2.add(labelName);
                b2.add(Box.createHorizontalGlue());                
                
                final Box imgDisplay = Box.createVerticalBox();
//                imgDisplay.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
                imgDisplay.add(b);
                imgDisplay.add(b2);
                add(imgDisplay, BorderLayout.CENTER);
            }
            
            
            public void updateData() {
                lecteurChequeAdresse.updateData();
                labelIcon.setIcon(myListener.getLecteurChequeIcon());
                labelName.setText(myListener.getLecteurChequeInfo());
            }
            
        }



        public LecteurChequePanel getLecteurChequePanel() {
            return lecteurChequePanel;
        }



        public final ZTextField getCheNumeroCheque() {
            return cheNumeroCheque;
        }
    
    
    
}
