/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/**
 * @author RIVALLAND FREDERIC
 * <br>UAG 
 * <br>CRI Guadeloupe
 *
 */
package org.cocktail.maracuja.client.finder;


/**
 * @deprecated
 * @author rprin
 */
public final class FinderGestion extends Finder {
	//
	//	/**
	//	 * Renvoie l'objet EOGestionExercice correspondant à un gestion et un exercice. Pas de fetch, seulement en filtre de gestion.gestionExercices
	//	 * 
	//	 * @param ed
	//	 * @param gestion
	//	 * @param exercice
	//	 * @return
	//	 * @throws Exception
	//	 * @deprecated
	//	 */
	//	public static final EOGestionExercice getGestionExercice(final EOGestion gestion, final EOExercice exercice) {
	//		final EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("exercice.exeExercice=%@", new NSArray(exercice.exeExercice()));
	//		final NSArray gestionExercices = EOQualifier.filteredArrayWithQualifier(gestion.gestionExercices(), qualifier);
	//		if (gestionExercices.count() == 0) {
	//			return null;
	//		}
	//		return (EOGestionExercice) gestionExercices.objectAtIndex(0);
	//	}

	//	/**
	//	 * @param gestion
	//	 * @param exercice
	//	 * @return Renvoi le compte du SACD
	//	 * @deprecated
	//	 */
	//	public static final EOPlanComptable getPlanco185ForGestionAndExercice(final EOGestion gestion, final EOExercice exercice) {
	//		final EOGestionExercice gestionExercice = getGestionExercice(gestion, exercice);
	//		if (gestionExercice == null) {
	//			return null;
	//		}
	//		return gestionExercice.planComptable185();
	//	}
	//
	//	/**
	//	 * @param gestion
	//	 * @param exercice
	//	 * @return
	//	 * @deprecated
	//	 */
	//	public static final EOPlanComptable getPlanco181ForGestionAndExercice(final EOGestion gestion, final EOExercice exercice) {
	//		final EOGestionExercice gestionExercice = getGestionExercice(gestion, exercice);
	//		if (gestionExercice == null) {
	//			return null;
	//		}
	//		return gestionExercice.planComptable181();
	//	}
	//
	//	/**
	//	 * @param gestion
	//	 * @param exercice
	//	 * @return true si le code gestion est un sacd sur l'exercice indiqué en parametre (on teste la presence d'un planco_185)
	//	 * @deprecated
	//	 */
	//	public static final boolean gestionIsSacd(final EOGestion gestion, final EOExercice exercice) {
	//		return (getPlanco185ForGestionAndExercice(gestion, exercice) != null);
	//	}
	//
	//	/**
	//	 * @param gestion
	//	 * @return True si le code gestion spécifié est le code de l'agence.
	//	 * @deprecated
	//	 */
	//	public static final boolean gestionIsAgence(final EOGestion gestion) {
	//		return gestion.equals(gestion.comptabilite().gestion());
	//	}
	//
	//	/**
	//	 * @param gestion
	//	 * @param exercice
	//	 * @return
	//	 * @deprecated
	//	 */
	//	public static final boolean gestionIsComposante(final EOGestion gestion, final EOExercice exercice) {
	//		return (!(gestionIsSacd(gestion, exercice) || gestionIsAgence(gestion)));
	//	}

}
