/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/**
 * @author RIVALLAND FREDERIC
 * <br>UAG
 * <br>CRI Guadeloupe
 * @author rprin
 */
package org.cocktail.maracuja.client.finder;

import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FinderJournalEcriture extends Finder {

	/**
	 * Retourne le type journal visa mandat
	 * 
	 * @param ed EditingContext de travail
	 * @return
	 */
	public static final EOTypeJournal leTypeJournalVisaMandat(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalVisaMandat
		}), null, true);
	}

	public static final EOTypeJournal leTypeJournalRecouvrement(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalRecouvrement
		}), null, false);
	}

	public static final EOTypeJournal leTypeJournalVisaPrestationInterne(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalVisaPrestationInterne
		}), null, true);
	}

	public static final EOTypeJournal leTypeJournalExercice(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalExercice
		}), null, true);
	}

	public static final EOTypeJournal leTypeJournalGrandLivreValeursInactives(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalGrandLivreValeursInactives
		}), null, true);
	}

	public static final EOTypeJournal leTypeJournalVisaTitre(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalVisaTitre
		}), null, true);
	}

	public static final EOTypeJournal leTypeJournalPaiement(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalPaiement
		}), null, true);
	}

	public static final EOTypeJournal leTypeJournalBordCheque(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalBordCheque
		}), null, true);
	}

	public static final EOTypeOperation leTypeOperationGenerique(EOEditingContext ed) {
		return (EOTypeOperation) Finder.fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_GENERIQUE
		}), null, true);
	}

	public static final EOTypeOperation leTypeOperationModifPaiement(EOEditingContext ed) {
		return (EOTypeOperation) Finder.fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_MODIF_PAIEMENT
		}), null, true);
	}

	public static final EOTypeOperation leTypeOperationVisaPrestationInterne(EOEditingContext ed) {
		return (EOTypeOperation) Finder.fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_VISA_PRESTATION_INTERNE
		}), null, true);
	}

	public static final EOTypeOperation leTypeOperationRecouvrement(EOEditingContext ed) {
		return (EOTypeOperation) Finder.fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_RECOUVREMENT
		}), null, true);
	}

	/**
	 * Retourne la liste des 'EcritureDetail' de l'ecriture
	 * 
	 * @param ed EditingContext de travail
	 * @param ecriture ecriture (critere)
	 * @return
	 */
	public static final NSArray lesEcrituresDetailsDeLecriture(EOEditingContext ed, EOEcriture ecriture) {
		return Finder.fetchArray(ed, EOEcritureDetail.ENTITY_NAME, "ecriture = %@ ", new NSArray(new Object[] {
				ecriture
		}), null, true);
	}

	//	/**
	//	 * @deprecated
	//	 * @param ed
	//	 * @return
	//	 */
	//	public static final EOPlanComptable planComptable185(EOEditingContext ed) {
	//		return EOPlanComptableFinder.getPlancoValideForPcoNum(ed, "185", true);
	//		//        return (EOPlanComptable) Finder.fetchObject(ed, EOPlanComptable.ENTITY_NAME, "pcoNum = %@ ", new NSArray(new Object[] { "185" }), null, true);
	//	}

	public static EOTypeJournal leTypeJournalReimputation(EOEditingContext ed) {
		return (EOTypeJournal) Finder.fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				EOTypeJournal.typeJournalReimputation
		}), null, true);
	}

	/**
	 * @param ed
	 * @return
	 */
	public static final EOTypeOperation leTypeOperationPaiement(EOEditingContext ed) {
		return (EOTypeOperation) Finder.fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_PAIEMENT
		}), null, true);
	}
}
