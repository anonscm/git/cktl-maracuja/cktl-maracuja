/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 10 juil. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.finder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 *  
 */
public abstract class Finder {
    
    public static final String QUAL_AND = " and ";
    public static final String QUAL_POINT = ".";
    public static final String QUAL_SUP_EQUALS = ">=%@";
    public static final String QUAL_EQUALS = "=%@";
    public static final String QUAL_INFERIEUR = "<%@";
    public static final String QUAL_PARENTHESE_FERMANTE = ")";
    public static final String QUAL_PARENTHESE_OUVRANTE = "(";
    public static final String QUAL_OR = " or ";
    public static final String QUAL_EQUALS_NIL = "=nil ";
    public static final String QUAL_CASE_INSENSITIVE_LIKE = " caseInsensitiveLike %@ ";    
    public static final String QUAL_ETOILE = "*";    

    public static NSArray fetchArray(
            EOEditingContext ec,
            String entityName,
            String conditionStr,
            NSArray params,
            NSArray sortOrderings,
            boolean refreshObjects) {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
                conditionStr,
                params);
        EOFetchSpecification spec = new EOFetchSpecification(
                entityName,
                qual,
                sortOrderings,
                true,
                true,
                null);
        spec.setRefreshesRefetchedObjects(refreshObjects);
        return ec.objectsWithFetchSpecification(spec);
    }

    public static EOEnterpriseObject fetchObject(
            EOEditingContext ec,
            String entityName,
            String conditionStr,
            NSArray params,
            NSArray sortOrderings,
            boolean refreshObjects) {
        NSArray res = fetchArray(
                ec,
                entityName,
                conditionStr,
                params,
                sortOrderings,
                refreshObjects);
        if ((res == null) || (res.count() == 0)) {
            return null;
        }
        return (EOEnterpriseObject) res.objectAtIndex(0);
    }

    public static NSArray fetchArrayWithPrefetching(
            EOEditingContext ec,
            String entityName,
            String conditionStr,
            NSArray params,
            NSArray sortOrderings,
            boolean refreshObjects,
            NSArray relationsToPrefetch) {
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
                conditionStr,
                params);
        EOFetchSpecification spec = new EOFetchSpecification(
                entityName,
                qual,
                sortOrderings,
                true,
                true,
                null);
        spec.setPrefetchingRelationshipKeyPaths(relationsToPrefetch);
        spec.setRefreshesRefetchedObjects(refreshObjects);
        return ec.objectsWithFetchSpecification(spec);
    }

}
