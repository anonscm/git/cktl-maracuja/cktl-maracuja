/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 10 juil. 2004 To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.finder;

import java.io.StringWriter;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.exception.BordereauRejetException;
import org.cocktail.maracuja.client.exception.FinderException;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeOperation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author RIVALLAND FREDERIC
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 *  
 * */

public class FinderVisa extends Finder {

    

    
    /**
     * Retourne les ecritures du bordereau de mandat
     * @param ed EditingContext de travail
     * @param leBordereau Le bordereau (critere)
     * @param withLogs
     * @return
     * @throws FinderException
     */
    public static NSArray lesEcrituresDuBordereauMandat(EOEditingContext ed, EOBordereau leBordereau, boolean withLogs) throws FinderException {

        return FinderVisa.lesEcrituresBordereau(ed, leBordereau, (EOTypeBordereau) FinderVisa.fetchObject(ed, EOTypeBordereau.ENTITY_NAME, EOTypeBordereau.TBO_TYPE_KEY + QUAL_EQUALS , new NSArray(
                new Object[] { EOTypeBordereau.TypeBordereauBTME }), null, true), withLogs);

    }

    /**
     * Retourne les ecritures du bordereau de titre
     * @param ed EditingContext de travail
     * @param leBordereau Le bordereau (critere)
     * @param withLogs
     * @return
     * @throws FinderException
     */
    public static NSArray lesEcrituresDuBordereauTitre(EOEditingContext ed, EOBordereau leBordereau, boolean withLogs) throws FinderException {

        return FinderVisa.lesEcrituresBordereau(ed, leBordereau, (EOTypeBordereau) FinderVisa.fetchObject(ed, EOTypeBordereau.ENTITY_NAME, EOTypeBordereau.TBO_TYPE_KEY + QUAL_EQUALS, new NSArray(
                new Object[] { EOTypeBordereau.TypeBordereauBTTE }), null, true), withLogs);

    }

    /**
     * Retourne le bordereau de rejet de ce bordereau
     * @param ed EditingContext de travail
     * @param leBordereau Le bordereau (critere)
     * @param withLogs
     * @return retourne null si pas de bordereau de rejet
     */
    public static EOBordereauRejet leBordereauRejetDuBordereauMandat(EOEditingContext ed, EOBordereau leBordereau, boolean withLogs) {

        return FinderVisa.leBordereauRejetDuBordereau(ed, leBordereau, (EOTypeBordereau) FinderVisa.fetchObject(ed, EOTypeBordereau.ENTITY_NAME, "tboLibelle = %@", new NSArray(
                new Object[] { EOTypeBordereau.TypeBordereauBTMNA }), null, true), withLogs);

    }

    /**
     * Retourne le bordereau de rejet du bordereau de titre
     * @param ed EditingContext de travail
     * @param leBordereau Le bordereau (critere)
     * @param withLogs
     * @return
     */
    public static EOBordereauRejet leBordereauRejetDuBordereauTitre(EOEditingContext ed, EOBordereau leBordereau, boolean withLogs) {

        return FinderVisa.leBordereauRejetDuBordereau(ed, leBordereau, (EOTypeBordereau) FinderVisa.fetchObject(ed, EOTypeBordereau.ENTITY_NAME, "tboLibelle = %@", new NSArray(
                new Object[] { EOTypeBordereau.TypeBordereauBTTNA }), null, true), withLogs);

    }

    /**
     * Retourne la liste des bordereaux de mandat a viser
     * @param ed
     *            EditingContext de travail
     * @param gestion
     *            Code Gestion pour la recherche
     * @param exercice
     *            Exercice pour la recherche
     * @param withLogs
     * @return
     */
    public static NSArray lesBordereauxMandatAViser(EOEditingContext ed, EOGestion gestion, EOExercice exercice, boolean withLogs) {
        StringWriter sw = new StringWriter();
        NSMutableArray params = new NSMutableArray(new Object[] { gestion, exercice, EOBordereau.BordereauValide, EOTypeBordereau.TypeBordereauBTME });
        sw.write("(");
        sw.write(EOBordereau.TYPE_BORDEREAU_KEY +  "." + EOTypeBordereau.TBO_TYPE_KEY + "=%@");
        
        if (exercice.exeExercice().intValue()>=ZConst.EXE_EXERCICE_2007) {
            sw.write(" or (typeBordereau.tboSousType=%@)");
            params.addObject(EOTypeBordereau.SOUS_TYPE_REVERSEMENTS);
        }               
        sw.write(")");

        return Finder.fetchArrayWithPrefetching(ed, EOBordereau.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@ and " + sw.toString(), params, new NSArray(new Object[]{EOBordereau.TRI_BOR_NUM_ASC}), true, new NSArray(EOBordereau.MANDATS_KEY));
    }

    /**
     * Retourne la liste des bordereaux de titre a viser
     * @param ed
     *            EditingContext de travail
     * @param gestion
     *            Code Gestion pour la recherche
     * @param exercice
     *            Exercice pour la recherche
     * @return
     */
    public static NSArray lesBordereauxTitreAViser(EOEditingContext ed, EOGestion gestion, EOExercice exercice) {

        NSMutableArray params = new NSMutableArray(new Object[] { gestion, exercice, EOBordereau.BordereauValide });
        NSMutableArray tmp = lesTypesBordereau(ed, EOTypeBordereau.TypeBordereauBTTE).mutableClone();
        if (exercice.exeExercice().intValue()<ZConst.EXE_EXERCICE_2007) {
            tmp.addObjectsFromArray(Finder.fetchArray(ed, EOTypeBordereau.ENTITY_NAME, EOTypeBordereau.TBO_SOUS_TYPE_KEY + " = %@", new NSArray(new Object[] { EOTypeBordereau.SOUS_TYPE_REVERSEMENTS }), new NSArray(new Object[]{EOBordereau.TRI_BOR_NUM_ASC}), false));
        }
        
        
        if (tmp.count() > 0) {
            StringWriter sw = new StringWriter();
            sw.write("AND (");
            for (int i = 0; i < tmp.count(); i++) {
                EOTypeBordereau array_element = (EOTypeBordereau) tmp.objectAtIndex(i);
                if (i > 0) {
                    sw.write(" or ");
                }
                sw.write("typeBordereau=%@");
                params.addObject(array_element);
            }
            //Ajout 2007 les Bordereaux de reversements passent sur les mandats
            if (exercice.exeExercice().intValue()>=ZConst.EXE_EXERCICE_2007) {
                sw.write(" and (typeBordereau.tboSousType<>%@)");
                params.addObject(EOTypeBordereau.SOUS_TYPE_REVERSEMENTS);
            }            
            sw.write(")");


            
            return Finder.fetchArrayWithPrefetching(ed, EOBordereau.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@  " + sw.toString(), params, null, true, new NSArray("titres"));
        }
        return null;
    }

    public static NSArray lesBordereauxTitrePIAViser2006(EOEditingContext ed, EOGestion gestion, EOExercice exercice) {
        NSMutableArray params = new NSMutableArray(new Object[] { gestion, exercice, EOBordereau.BordereauValide });
        NSArray tmp = lesTypesBordereau(ed, EOTypeBordereau.TypeBordereauPrestationInterne);
        if (tmp.count() > 0) {
            StringWriter sw = new StringWriter();
            sw.write("AND (");
            for (int i = 0; i < tmp.count(); i++) {
                EOTypeBordereau array_element = (EOTypeBordereau) tmp.objectAtIndex(i);
                if (i > 0) {
                    sw.write(" or ");
                }
                sw.write("typeBordereau =%@");
                params.addObject(array_element);
            }
            sw.write(")");
            return Finder.fetchArrayWithPrefetching(ed, EOBordereau.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@ and titres.titId<>nil " + sw.toString(), params, new NSArray(new Object[]{EOBordereau.TRI_BOR_NUM_ASC}), true,new NSArray("titres"));
            
        }
        return null;
    }
    
    public static NSArray lesBordereauxTitrePIAViser(EOEditingContext ed, EOGestion gestion, EOExercice exercice) {
        NSMutableArray params = new NSMutableArray(new Object[] { gestion, exercice, EOBordereau.BordereauValide });
        NSArray tmp = lesTypesBordereau(ed, EOTypeBordereau.TYPE_BORDEREAU_PRESTATION_INTERNE_R);
        if (tmp.count() > 0) {
            StringWriter sw = new StringWriter();
            sw.write("AND (");
            for (int i = 0; i < tmp.count(); i++) {
                EOTypeBordereau array_element = (EOTypeBordereau) tmp.objectAtIndex(i);
                if (i > 0) {
                    sw.write(" or ");
                }
                sw.write("typeBordereau =%@");
                params.addObject(array_element);
            }
            sw.write(")");

            if (exercice.exeExercice().intValue() < ZConst.EXE_EXERCICE_2007 ) {
                return Finder.fetchArrayWithPrefetching(ed, EOBordereau.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@ and titres.titId<>nil " + sw.toString(), params, new NSArray(new Object[]{EOBordereau.TRI_BOR_NUM_ASC}), true,new NSArray("titres"));
            }
            
            // > 2007, on prend les bordereaux qui ont des mandats + des titres.  
            return Finder.fetchArrayWithPrefetching(ed, EOBordereau.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@ " + sw.toString(), params, new NSArray(new Object[]{EOBordereau.TRI_BOR_NUM_ASC}), true,new NSArray(new Object[]{EOBordereau.MANDATS_KEY, EOBordereau.TITRES_KEY  }));
            
            
            
            
        }
        return null;
    }

    /**
     * Retourne la liste des bordereaux de mandat vises
     * @param ed
     *            EditingContext de travail
     * @param gestion
     *            Code Gestion pour la recherche
     * @param exercice
     *            Exercice pour la recherche
     * @return
     */
    public static NSArray lesBordereauxMandatVise(EOEditingContext ed, EOGestion gestion, EOExercice exercice) {

        return FinderVisa.lesBordereaux(ed, gestion, exercice, FinderVisa.leTypeBordereauBTME(ed), EOBordereau.BordereauVise);
    }

    /**
     * Retourne la liste des bordereaux de titre visï¿½s
     * @param ed
     *            EditingContext de travail
     * @param gestion
     *            Code Gestion pour la recherche
     * @param exercice
     *            Exercice pour la recherche
     * @return
     */
    public static NSArray lesBordereauxTitreVise(EOEditingContext ed, EOGestion gestion, EOExercice exercice) {

        return FinderVisa.lesBordereaux(ed, gestion, exercice, FinderVisa.leTypeBordereauBTTE(ed), EOBordereau.BordereauVise);
    }

    /**
     * Retourne la liste des bordereaux de rejet de mandat
     * @param ed  EditingContext de travail
     * @param gestion Code Gestion pour la recherche
     * @param exercice Exercice pour la recherche
     * @param withLogs
     * @return
     */
    public static NSArray lesBordereauxRejetMandat(EOEditingContext ed, EOGestion gestion, EOExercice exercice, boolean withLogs) {

        return FinderVisa.lesBordereauxRejet(ed, gestion, exercice, FinderVisa.leTypeBordereauBTMNA(ed));
    }

    /**
     * Retourne la liste des bordereaux de rejet de titre
     * @param ed EditingContext de travail
     * @param gestionCode Gestion pour la recherche
     * @param exercice Exercice pour la recherche
     * @return
     */
    public static NSArray lesBordereauxRejetTitre(EOEditingContext ed, EOGestion gestion, EOExercice exercice) {

        return FinderVisa.lesBordereauxRejet(ed, gestion, exercice, FinderVisa.leTypeBordereauBTTNA(ed));
    }

    public static EOTypeBordereau leTypeBordereauBTCC(EOEditingContext ed) {
        return FinderVisa.leTypeBordereau(ed, EOTypeBordereau.TypeBordereauCheque);
    }

    /**
     * Retourne le Type de Bordereau BTME
     * @param ed EditingContext de travail
     * @return
     */
    public static EOTypeBordereau leTypeBordereauBTME(EOEditingContext ec) {
        return FinderVisa.leTypeBordereau(ec, EOTypeBordereau.TypeBordereauBTME);
    }

    /**
     * Retourne le Type de Bordereau BTTE
     * @param ed EditingContext de travail
     * @return
     */
    public static EOTypeBordereau leTypeBordereauBTTE(EOEditingContext ed) {
        return FinderVisa.leTypeBordereau(ed, EOTypeBordereau.TypeBordereauBTTE);
    }

    /**
     * Retourne le Type de Bordereau BTMNA
     * @param ed EditingContext de travail
     * @return
     */
    public static EOTypeBordereau leTypeBordereauBTMNA(EOEditingContext ed) {
        return FinderVisa.leTypeBordereau(ed, EOTypeBordereau.TypeBordereauBTMNA);
    }

    /**
     * Retourne le Type de Bordereau BTTNA
     * @param ed EditingContext de travail
     * @return
     */
    public static EOTypeBordereau leTypeBordereauBTTNA(EOEditingContext ed) {
        return FinderVisa.leTypeBordereau(ed, EOTypeBordereau.TypeBordereauBTTNA);
    }

    /**
     * Retourne le type d'operation visa de titre
     * @param ed
     *            EditingContext de travail
     * @return
     */
    public static EOTypeOperation leTypeOperationVisaTitre(EOEditingContext ed) {
        return FinderVisa.leTypeOperation(ed, EOTypeOperation.TYPE_OPERATION_GENERIQUE);
    }

    /**
     * Retourne le type d'operation visa de mandat
     * @param ed
     *            EditingContext de travail
     * @return
     */
    public static EOTypeOperation leTypeOperationVisaMandat(EOEditingContext ed) {
        return FinderVisa.leTypeOperation(ed, EOTypeOperation.TYPE_OPERATION_GENERIQUE);
    }

    /**
     * Renvoie les types de bordereau (en fonction d'un type de type bordereau)
     * @param ed
     * @param tboType
     * @return
     */
    private static NSArray lesTypesBordereau(EOEditingContext ed, String tboType) {

        return Finder.fetchArray(ed, EOTypeBordereau.ENTITY_NAME, "tboType = %@", new NSArray(new Object[] { tboType }), null, true);
    }

    private static NSArray lesBordereaux(EOEditingContext ed, EOGestion gestion, EOExercice exercice, EOTypeBordereau typeBordereau, String etat) {

        // verifier le FORMAT du Bordreau !!!
        // une composante
        // un exercice

        // 1 n factures
        // 1 -mod paiement rib verifier RIB
        // 1 -fournisseur
        // 1 -montant facture
        // 1 verifier planCompotable VALIDE + brouillard

        // 2 n recettes
        // 2 -mod recouvrement rib verifier RIB
        // 2 -client
        // 2 -montant recette
        // 2 verifier planCompotable VALIDE + brouillard

        return Finder.fetchArray(ed, EOBordereau.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@ AND typeBordereau =%@", new NSArray(new Object[] { gestion, exercice, etat,
                typeBordereau }), null, true);
    }

    private static NSArray lesBordereauxRejet(EOEditingContext ed, EOGestion gestion, EOExercice exercice, EOTypeBordereau typeBordereau) {

        return Finder.fetchArray(ed, EOBordereauRejet.ENTITY_NAME, "gestion = %@ AND exercice = %@ AND borEtat = %@ AND typeBordereau =%@", new NSArray(
                new Object[] { gestion, exercice, typeBordereau }), null, true);
    }

    private static EOBordereauRejet leBordereauRejetDuBordereau(EOEditingContext ed, EOBordereau leBordereau, EOTypeBordereau typeBordereau, boolean withLogs) throws FinderException {

        // bordereau de mandat
        if (typeBordereau.tboType().equals(EOTypeBordereau.TypeBordereauBTME)) {
            // on recupere les mandats Annuler du bordereau
            final NSArray lesMandatAnnule = Finder
                    .fetchArray(ed, EOMandat.ENTITY_NAME, "bordereau = %@ AND manEtat = %@", new NSArray(new Object[] { leBordereau, EOMandat.mandatAnnule }), null, true);
            if (lesMandatAnnule.count() == 0) {
                return null;
            }

            return (EOBordereauRejet) ((EOMandat) lesMandatAnnule.objectAtIndex(0)).bordereauRejet();
        }

        // bordereau de titre
        if (typeBordereau.tboLibelle().equals(EOTypeBordereau.TypeBordereauBTTE)) {
            // on recupere les titres Annuler du bordereau
            NSArray lesTitreAnnule = Finder.fetchArray(ed, EOMandat.ENTITY_NAME, "bordereau = %@ AND manEtat = %@", new NSArray(new Object[] { leBordereau, EOTitre.titreAnnule }), null, true);
            if (lesTitreAnnule.count() == 0)
                return null;
            if ((((EOMandat) lesTitreAnnule.objectAtIndex(0)).bordereauRejet().brjNum() != null) && (((EOMandat) lesTitreAnnule.objectAtIndex(0)).bordereauRejet().brjNum().intValue() != 0))
                throw new FinderException(BordereauRejetException.bordereauRejetDejaNumerote);
            return (EOBordereauRejet) ((EOMandat) lesTitreAnnule.objectAtIndex(0)).bordereauRejet();
        }
        return null;

    }

    private static NSArray lesEcrituresBordereau(EOEditingContext ed, EOBordereau leBordereau, EOTypeBordereau typeBordereau, boolean withLogs) {

        NSMutableArray lesEcrituresTemp = new NSMutableArray();
        NSMutableArray lesEcritures = new NSMutableArray();

        // bordereau de mandat
        if (EOTypeBordereau.TypeBordereauBTME.equals(typeBordereau.tboType())) {
            // on recupere les mandatsDetails du bordereau
            NSArray lesMandatDetailEcriture = Finder.fetchArray(ed, EOMandatDetailEcriture.ENTITY_NAME, "mandat.bordereau = %@ ", new NSArray(new Object[] { leBordereau }), null, true);
            // on recupere les edritures des mandatsDetail
            int i = 0;
            while (lesMandatDetailEcriture.count() > i) {
                lesEcrituresTemp.addObject(((EOMandatDetailEcriture) lesMandatDetailEcriture.objectAtIndex(i)).ecritureDetail().ecriture());
                i++;
            }
            // on applique un DISTINCT sur les edritures
            int j = 0;
            while (lesEcrituresTemp.count() > j) {
                if (FinderVisa.objectNotInArray(lesEcritures, lesEcrituresTemp.objectAtIndex(j)))
                    lesEcritures.addObject(lesEcrituresTemp.objectAtIndex(j));

                j++;
            }
        } else
        // bordereau de titre
        if (EOTypeBordereau.TypeBordereauBTTE.equals(typeBordereau.tboType())) {
            // on recupere les mandatsDetails du bordereau
            NSArray lesTitreDetailEcriture = Finder.fetchArray(ed, EOTitreDetailEcriture.ENTITY_NAME, "titre.bordereau = %@ ", new NSArray(new Object[] { leBordereau }), null, true);
            // on recupere les edritures des titresDetail
            int i = 0;
            while (lesTitreDetailEcriture.count() > i) {
                lesEcrituresTemp.addObject(((EOTitreDetailEcriture) lesTitreDetailEcriture.objectAtIndex(i)).ecritureDetail().ecriture());
                i++;
            }
            // on applique un DISTINCT sur les edritures
            int j = 0;
            while (lesEcrituresTemp.count() > j) {
                if (FinderVisa.objectNotInArray(lesEcritures, lesEcrituresTemp.objectAtIndex(j)))
                    lesEcritures.addObject(lesEcrituresTemp.objectAtIndex(j));

                j++;
            }

        } else {
            System.out.println("FinderVisa.lesEcrituresBordereau() : " + "Type bordereau non reconnu !!!!! ");
        }

        // ajout rod : on trie les ecritures par libelle (utile pour numerotation par ordre de creation)
        final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey("ecrLibelle", EOSortOrdering.CompareAscending);
        lesEcritures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcritures, new NSArray(sortOrdering)).mutableClone();

        // trace(withLogs, lesEcritures.toString());

        return lesEcritures;
    }

    private static EOTypeBordereau leTypeBordereau(EOEditingContext ed, String tboLibelle) {
        return (EOTypeBordereau) Finder.fetchObject(ed, EOTypeBordereau.ENTITY_NAME, "tboType = %@", new NSArray(new Object[] { tboLibelle }), null, true);
    }

    private static EOTypeOperation leTypeOperation(EOEditingContext ed, String topLibelle) {
        return (EOTypeOperation) Finder.fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] { topLibelle }), null, true);
    }

    private static void trace(boolean withLogs, String m) {
        if (withLogs)
            System.out.println(m);
    }

    private static boolean objectNotInArray(NSArray array, Object obj) {
        boolean reponse = true;
        int i = 0;
        while (array.count() > i) {
            if (array.objectAtIndex(i).equals(obj))
                reponse = false;
            i++;
        }
        return reponse;
    }
}
