/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.finders;

import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOGestionExercice;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.metier.EOUtilisateurFonction;
import org.cocktail.maracuja.client.metier.EOUtilisateurFonctionExercice;
import org.cocktail.maracuja.client.metier.EOUtilisateurFonctionGestion;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EOUtilisateurFinder extends ZFinder {
	private static final String UTILISATEURFONCTION_EST_NUL = "utilisateurfonction est nul.";
	public static final String MSG_USER_UNKNOWN_NOIND = "Impossible de récuperer l'utilisateur correspondant au n° individu= %0";
	public static final String MSG_USER_UNKNOWN_PERSID = "Impossible de récuperer l'utilisateur correspondant au pers_id= %0";
	public static final String MSG_USER_UNKNOWN_NOM_PRENOM = "Impossible de récuperer l'utilisateur correspondant aux nom et prenom= %0, %1";

	public static final String FON_ADUT = "ADUT";
	public static final String FON_ADUTA = "ADUTA";
	private static final EOQualifier QUALIFIER_FONCTIONS_ADMIN_UTILISATEUR = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY + ZFinder.QUAL_EQUALS, new NSArray(FON_ADUT));

	//    private static final EOQualifier QUALIFIER_FONCTIONS_MY_TYPEAPPLICATION = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + ZFinder.QUAL_POINT+ EOTypeApplication.TYAP_STRID_KEY+ ZFinder.QUAL_EQUALS , new NSArray(ApplicationClient.APPLICATION_TYAP_STRID));

	public static final EOQualifier qualNoIndividu(final Integer noIndividu) {
		return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.INDIVIDU_NO_INDIVIDU_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				noIndividu
		}));
	}

	public static final EOQualifier qualPersId(final Integer persId) {
		return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.PERSONNE_PERS_ID_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				persId
		}));
	}

	public static final EOQualifier qualUtilisateurValide() {
		return EOUtilisateur.QUALIFIER_UTILISATEUR_VALIDE;
		//        return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY  +"=%@", new NSArray(EOUtilisateur.ETAT_VALIDE));
	}

	public static final EOQualifier qualUtilisateurNonValide() {
		return EOUtilisateur.QUALIFIER_UTILISATEUR_NON_VALIDE;
		//        return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY  +"<>%@", new NSArray(EOUtilisateur.ETAT_VALIDE));
	}

	/**
	 * @return un qualifier pour récupérer toutes les fonctions de type administrateur utilisateur.
	 */
	public static final EOQualifier qualFonctionsAdminUtilisateur() {

		return QUALIFIER_FONCTIONS_ADMIN_UTILISATEUR;
		//        QUALIFIER_FONCTIONS_ADMIN_UTILISATEUR = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY  +ZFinder.QUAL_EQUALS , new NSArray(FON_ADUT));
	}

	/**
	 * Renvoie un utilisateur en fonction de son num individu, les utilisateurs a l'etat SUPPRIME ne sont pas renvoyes.
	 * 
	 * @param ec
	 * @param noIndividu
	 * @return
	 * @throws ZFinderException
	 */
	public static EOUtilisateur fecthUtilisateurByNoIndividu(final EOEditingContext ec, final Integer noIndividu) throws ZFinderException {
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] {
				qualNoIndividu(noIndividu), qualUtilisateurValide()
		})), null, false);
		if (tmp == null) {
			throw new ZFinderException(MSG_USER_UNKNOWN_NOIND, new String[] {
					noIndividu.toString()
			});
		}
		return (EOUtilisateur) tmp;
	}

	/**
	 * Renvoie un utilisateur en fonction de son persId.
	 * 
	 * @param ec
	 * @param noIndividu
	 * @throws ZFinderException
	 */
	public static EOUtilisateur fecthUtilisateurByPersId(final EOEditingContext ec, final Integer persId) throws ZFinderException {
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] {
				qualPersId(persId), qualUtilisateurValide()
		})), null, false);
		if (tmp == null) {
			throw new ZFinderException(MSG_USER_UNKNOWN_PERSID, new String[] {
					persId.toString()
			});
		}
		return (EOUtilisateur) tmp;
	}

	public static NSArray fetchGestionsForAutorisation(final EOEditingContext ec, final EOUtilisateurFonction autorisation) {
		if (autorisation != null) {
			return (NSArray) autorisation.valueForKeyPath("autorisationGestions");
			//	        return fetchArray(ec, "AutorisationGestion", "autorisation=%@", new NSArray(autorisation), null, true);
		}
		return new NSArray();

	}

	public static NSArray fetchAutorisationsForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
		return utilisateur.utilisateurFonctions();
	}

	/**
	 * Récupère les droits d'utilisation des fonctions.
	 * 
	 * @param ec
	 * @param utilisateur
	 */
	public static NSArray fetchUtilisateurFonctionsForUtilisateurAndExercice(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOExercice exercice) {
		if (utilisateur != null) {
			if (exercice == null) {
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + QUAL_POINT + EOFonction.FON_SPEC_EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
						EOFonction.FON_SPEC_N
				}));
				return EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurFonctions(), qual);
			}
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(QUAL_PARENTHESE_OUVRANTE +
					EOUtilisateurFonction.FONCTION_KEY + QUAL_POINT + EOFonction.FON_SPEC_EXERCICE_KEY + QUAL_EQUALS + QUAL_PARENTHESE_FERMANTE +
					QUAL_OR +
					QUAL_PARENTHESE_OUVRANTE + EOUtilisateurFonction.FONCTION_KEY + QUAL_POINT + EOFonction.FON_SPEC_EXERCICE_KEY + QUAL_EQUALS +
					QUAL_AND + EOUtilisateurFonction.UTILISATEUR_FONCTION_EXERCICES_KEY + QUAL_POINT + EOUtilisateurFonctionExercice.EXERCICE_KEY + QUAL_EQUALS + QUAL_PARENTHESE_FERMANTE,

					new NSArray(new Object[] {
							EOFonction.FON_SPEC_N, EOFonction.FON_SPEC_O, exercice
					}));
			return EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurFonctions(), qual);
		}
		return new NSArray();
	}

	public static EOUtilisateurFonction getAutorisationForUtilisateurAndExerciceAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOExercice exercice, final EOFonction fonction) throws ZFinderException {
		final NSArray tmp;
		if (fonction == null) {
			throw new ZFinderException("La fonction est nulle.");
		}

		NSArray auts = fetchUtilisateurFonctionsForUtilisateurAndExercice(ec, utilisateur, exercice);
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				fonction
		}));
		tmp = EOQualifier.filteredArrayWithQualifier(auts, qual);

		//        NSArray auts = utilisateur.utilisateurFonctions();
		//		if (auts==null) {
		//		    auts = new NSArray();
		//		}
		//		
		//		if (exercice==null) {
		//			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("fonction.fonSpecExercice=%@ and fonction=%@", new NSArray(new Object[]{fonction})) ;
		//			tmp =  EOQualifier.filteredArrayWithQualifier(auts, qual)   ;
		//		}
		//		else {
		//			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("exercice=%@ and fonction=%@", new NSArray(new Object[]{exercice, fonction})) ;
		//			tmp =  EOQualifier.filteredArrayWithQualifier(auts, qual)   ;
		//		}
		//		
		if (tmp.count() == 0) {
			return null;
		}
		return (EOUtilisateurFonction) tmp.objectAtIndex(0);
	}

	public static NSArray getGestionsForUtilisateurAndExerciceAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOExercice exercice, final EOFonction fonction) throws ZFinderException {
		//		NSArray tmp;
		//On cherche d'abord pour l'exercice

		//reciuperer l'autorisation
		EOUtilisateurFonction aut = getAutorisationForUtilisateurAndExerciceAndFonction(ec, utilisateur, exercice, fonction);
		//		if (aut==null) {
		//		    aut = getAutorisationForUtilisateurAndExerciceAndFonction(ec,utilisateur,null,fonction);
		//		}
		if (aut == null) {
			return new NSArray();
		}
		final NSMutableArray res2 = new NSMutableArray();
		final NSArray res = (NSArray) aut.valueForKey(EOUtilisateurFonction.UTILISATEUR_FONCTION_GESTIONS_KEY);
		for (int i = 0; i < res.count(); i++) {
			final EOUtilisateurFonctionGestion array_element = (EOUtilisateurFonctionGestion) res.objectAtIndex(i);
			final EOGestion gestion = array_element.gestion();
			//final EOGestionExercice gestionExercice = FinderGestion.getGestionExercice(gestion, exercice);
			final EOGestionExercice gestionExercice = gestion.getGestionExercice(exercice);
			if (gestionExercice != null) {
				res2.addObject(array_element.gestion());
			}
		}

		//Trier
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res2, new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending))).immutableClone();
	}

	public static EOUtilisateurFonction fetchUtilisateurFonctionForUtilisateurAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) {
		if (utilisateur != null) {
			return (EOUtilisateurFonction) fetchObject(ec, EOUtilisateurFonction.ENTITY_NAME, EOUtilisateurFonction.UTILISATEUR_KEY + ZFinder.QUAL_EQUALS + ZFinder.QUAL_AND + EOUtilisateurFonction.FONCTION_KEY + "=%@", new NSArray(new Object[] {
					utilisateur, fonction
			}), null, true);
		}
		return null;
	}

	/**
	 * Renvoie la liste des utilisateurs disposant d'une fonction particulière
	 * 
	 * @param ec
	 * @param fonction
	 * @return
	 */
	public static NSArray fetchUtilisateursForFonction(final EOEditingContext ec, final String fonIdInterne) {
		if (fonIdInterne != null) {
			return fetchArray(ec, EOUtilisateur.ENTITY_NAME, EOUtilisateur.UTILISATEUR_FONCTIONS_KEY + ZFinder.QUAL_POINT + EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
					fonIdInterne
			}), null, false);
		}
		return new NSArray();
	}

	public static EOUtilisateurFonction getUtilisateurFonctionForUtilisateurAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) {
		final NSArray tmp;
		if (fonction == null) {
			return null;
		}

		NSArray auts = utilisateur.utilisateurFonctions();
		if (auts == null) {
			auts = new NSArray();
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				fonction
		}));
		tmp = EOQualifier.filteredArrayWithQualifier(auts, qual);

		if (tmp.count() == 0) {
			return null;
		}
		return (EOUtilisateurFonction) tmp.objectAtIndex(0);
	}

	public static EOUtilisateurFonctionExercice getUtilisateurFonctionExercice(EOEditingContext editingContext, EOUtilisateurFonction utilisateurfonction, EOExercice exercice) throws ZFinderException {
		final NSArray tmp;
		if (utilisateurfonction == null)
			throw new ZFinderException(UTILISATEURFONCTION_EST_NUL);

		NSArray auts = utilisateurfonction.utilisateurFonctionExercices();
		if (auts == null) {
			auts = new NSArray();
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonctionExercice.EXERCICE_KEY + ZFinder.QUAL_EQUALS, new NSArray(new Object[] {
				exercice
		}));
		tmp = EOQualifier.filteredArrayWithQualifier(auts, qual);

		if (tmp.count() == 0) {
			return null;
		}
		return (EOUtilisateurFonctionExercice) tmp.objectAtIndex(0);
	}

	/**
	 * Renvoie tous les enrtegistrements de la table utilisateur.
	 * 
	 * @param ec
	 */
	public static NSArray fetchAllUtilisateursValides(final EOEditingContext ec) {
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
		larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
		NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualUtilisateurValide(), null, true);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	}

	/**
	 * Renvoie les utilisateurs qui ont au moins une fonction affectée pour l'application.
	 * 
	 * @param ec
	 * @param typeApplication
	 * @return
	 */
	//    public static NSArray fetchAllUtilisateursValidesForApplication(final EOEditingContext ec, final EOTypeApplication typeApplication) {
	//        final NSMutableArray larray = new NSMutableArray();
	//        larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
	//        larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
	//        if (typeApplication != null) {
	//            final EOQualifier qual =  EOQualifier.qualifierWithQualifierFormat( EOUtilisateur.UTILISATEUR_FONCTIONS_KEY + QUAL_POINT + EOUtilisateurFonction.FONCTION_KEY +QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + QUAL_EQUALS, new NSArray(new Object[]{typeApplication}));
	//            final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[]{qual,qualUtilisateurValide() }));
	//            final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, null, true, true, false,null);
	//            return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	//        }
	//        
	//        final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[]{qualUtilisateurValide() }));
	//        final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, null, true, true, false,null);
	//        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	//        
	//    }

	public static EOUtilisateurFonctionGestion getUtilisateurFonctionGestion(EOEditingContext editingContext, EOUtilisateurFonction utilisateurfonction, EOGestion gestion) throws ZFinderException {
		final NSArray tmp;
		if (utilisateurfonction == null)
			throw new ZFinderException(UTILISATEURFONCTION_EST_NUL);

		NSArray auts = utilisateurfonction.utilisateurFonctionGestions();
		if (auts == null) {
			auts = new NSArray();
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonctionGestion.GESTION_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				gestion
		}));
		tmp = EOQualifier.filteredArrayWithQualifier(auts, qual);

		if (tmp.count() == 0) {
			return null;
		}
		return (EOUtilisateurFonctionGestion) tmp.objectAtIndex(0);
	}

}
