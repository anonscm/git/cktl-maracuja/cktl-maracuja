/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.finders;

import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class EOPlanComptableFinder extends ZFinder {
	public static EOExercice _currentExercice = null;

	public static EOExercice getCurrentExercice() {
		return _currentExercice;
	}

	public static void setCurrentExercice(EOExercice exercice) {
		_currentExercice = exercice;
	}

	/**
	 * @param ec
	 * @param refresh
	 * @return Les comptes d'imputation valides.
	 */
	public static final NSArray getPlancosValides(EOEditingContext ec, final boolean refresh) {
		return _getPlancoValidesWithQuals(ec, getCurrentExercice(), null, refresh);
	}

	//	public static final NSArray getPlancosValidesForODP(EOEditingContext ec, final boolean refresh) {
	//		return getPlancosValides(ec, refresh);
	//	}

	public static final EOPlanComptable getPlancoValideForPcoNum(EOEditingContext ec, String pcoNum, final boolean refresh) {
		final NSArray res = getPlancosValides(ec, refresh);
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);
		if (res2.count() > 0) {
			return (EOPlanComptable) res2.objectAtIndex(0);
		}
		return null;
	}

	//	public static final EOPlanComptable getPlancoExerValideForPcoNum(EOEditingContext ec, String pcoNum, final boolean refresh) {
	//		final NSArray res = getPlancosValides(ec, refresh);
	//		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum=%@", new NSArray(pcoNum));
	//		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);
	//		if (res2.count() > 0) {
	//			return (EOPlanComptable) res2.objectAtIndex(0);
	//		}
	//		return null;
	//	}

	/**
	 * Renvoie les comptes valides
	 * 
	 * @param ec
	 * @param pcoNum (par exemple "4012*")
	 * @param refresh
	 * @return
	 */
	public static final NSArray getPlancoValidesForPcoNumLike(EOEditingContext ec, String pcoNum, final boolean refresh) {
		final NSArray res = getPlancosValides(ec, refresh);
		//		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("pcoNum like %@", new NSArray(pcoNum));
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, pcoNum);
		//		EOQualifier.qualifierWithQualifierFormat("pcoNum like %@", new NSArray(pcoNum));

		return EOQualifier.filteredArrayWithQualifier(res, qual);
	}

	/**
	 * @param ec
	 * @param refresh
	 * @return Les comptes d'imputation valides et émargeables
	 */
	public static final NSArray getPlancosValidesEmargeables(EOEditingContext ec, final boolean refresh) {
		//	    return ZFinder.fetchArray(ec, EOPlanComptable.ENTITY_NAME, EOPlanComptable.PCO_VALIDITE_KEY + "=%@ and "+EOPlanComptable.PCO_EMARGEMENT_KEY+"=%@", new NSArray(new Object[] { EOPlanComptable.etatValide, EOPlanComptable.pcoEmargementOui }), new NSArray(
		//	            EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending)), refresh);
		return getPlancoValidesWithCond(ec, getCurrentExercice(), EOPlanComptableExer.PCO_EMARGEMENT_KEY + QUAL_EQUALS, new NSArray(new Object[] {
			EOPlanComptableExer.pcoEmargementOui
		}), refresh);
	}

	/**
	 * @param ec
	 * @return Les comptes d'imputation valides qui sont autorisés pour le journal d'exercice.
	 */
	public static final NSArray getPlancosValidesJExercice(EOEditingContext ec, final boolean refresh) {
		return getPlancoValidesWithCond(ec, getCurrentExercice(), "pcoJExercice=%@ or pcoJExercice=nil", new NSArray(new Object[] {
			EOPlanComptableExer.pcoJExerciceOui
		}), refresh);
	}

	/**
	 * @param ec
	 * @return Les comptes d'imputation valides qui sont autorisés pour le journal de fin d'exercice.
	 */
	public static final NSArray getPlancosValidesJFinExercice(EOEditingContext ec, final boolean refresh) {
		return getPlancoValidesWithCond(ec, getCurrentExercice(), "pcoJFinExercice=%@ or pcoJFinExercice=nil", new NSArray(new Object[] {
			EOPlanComptableExer.pcoJFinExerciceOui
		}), refresh);
	}

	/**
	 * @param ec
	 * @return Les comptes d'imputation valides qui sont autorisés pour le journal de balance d'entrée.
	 */
	public static final NSArray getPlancosValidesJBe(EOEditingContext ec, final boolean refresh) {
		return getPlancoValidesWithCond(ec, getCurrentExercice(), "pcoJBe=%@ or pcoJBe=nil", new NSArray(new Object[] {
			EOPlanComptableExer.pcoJBeOui
		}), refresh);
	}

	//	/**
	//	 * @param ec
	//	 * @param refresh
	//	 * @return les comptes valides qui ont un compte de BE associé.
	//	 */
	//	public static final NSArray getPlancosValidesAvecCompteBE(EOEditingContext ec, final boolean refresh) {
	//		return getPlancoValidesWithCond(ec, getCurrentExercice(), "pcoCompteBe<>nil", null, refresh);
	//	}

	//	public static final NSArray getPlancosValidesAvecCompteBE(EOEditingContext ec, final boolean refresh) {
	//		return ZFinder.fetchArray(ec, EOPlanComptable.ENTITY_NAME, "pcoValidite=%@ and (pcoCompteBe<>nil)", new NSArray(new Object[] { EOPlanComptable.etatValide}),
	//				new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptable.PCO_NUM_KEY, EOSortOrdering.CompareAscending)), refresh);
	//	}

	public static NSArray getPlancoValidesWithCond(EOEditingContext ec, String cond, NSArray params, final boolean refresh) {
		return getPlancoValidesWithCond(ec, getCurrentExercice(), cond, params, refresh);
	}

	public static NSArray getPlancoValidesWithQuals(EOEditingContext ec, EOQualifier qual, final boolean refresh) {
		return getPlancoValidesWithQuals(ec, getCurrentExercice(), qual, refresh);
	}

	public static NSArray getPlancoValidesWithCond(EOEditingContext ec, EOExercice exercice, String cond, NSArray params, final boolean refresh) {
		return _getPlancoValidesWithQuals(ec, exercice, EOQualifier.qualifierWithQualifierFormat(cond, params), refresh);
	}

	public static NSArray getPlancoValidesWithQuals(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		return _getPlancoValidesWithQuals(ec, exercice, qual, refresh);
	}

	private static NSArray _getPlancoValidesWithQuals(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		NSArray res = _getPlancoExerValidesWithQuals(ec, exercice, qual, refresh);
		return (NSArray) res.valueForKey(EOPlanComptableExer.PLAN_COMPTABLE_KEY);
	}

	private static NSArray _getPlancoExerValidesWithQuals(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		final EOQualifier qualValide = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_VALIDITE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
			EOPlanComptableExer.etatValide
		}));
		final EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
			exercice
		}));

		final EOQualifier quals = (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualValide, qualExercice, qual
		})) : new EOAndQualifier(new NSArray(new Object[] {
				qualValide, qualExercice
		})));
		NSArray res = ZFinder.fetchArray(ec, EOPlanComptableExer.ENTITY_NAME, quals, null, refresh);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptableExer.PCO_NUM_KEY, EOSortOrdering.CompareAscending)));
	}

	/**
	 * @param planComptablelist
	 * @param pcoNum
	 * @return Le planComptable identifié par pcoNum s'ils est trouvé dans la liste des planComptable passée en parametre.
	 */
	public static EOPlanComptable findPlanComptableForPcoNumInList(NSArray planComptableList, String pcoNum) {
		final NSArray tmp = EOQualifier.filteredArrayWithQualifier(planComptableList, EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + pcoNum + "'", null));
		if (tmp.count() == 1) {
			return ((EOPlanComptable) tmp.objectAtIndex(0));
		}
		return null;
	}

}
