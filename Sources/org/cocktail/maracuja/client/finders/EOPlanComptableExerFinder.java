/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.finders;

import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class EOPlanComptableExerFinder extends ZFinder {
	public static final NSArray SORT_PCO_NUM_ASC = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptableExer.PCO_NUM_KEY, EOSortOrdering.CompareAscending));

	public static final EOPlanComptableExer getPlancoExerForPcoNum(EOEditingContext ec, EOExercice exercice, String pcoNum, final boolean refresh) {
		final NSArray res = getPlancoExersWithQual(ec, exercice, null, refresh);
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum);
		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);
		if (res2.count() > 0) {
			return (EOPlanComptableExer) res2.objectAtIndex(0);
		}
		return null;
	}

	public static final NSArray getPlancoExersForPcoNumLike(EOEditingContext ec, EOExercice exercice, String pcoNum, final boolean refresh) {
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, pcoNum + "*");
		final NSArray res = getPlancoExersWithQual(ec, exercice, qual, refresh);
		return res;
	}

	public static NSArray getPlancoExerValidesWithCond(EOEditingContext ec, EOExercice exercice, String cond, NSArray params, final boolean refresh) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(cond, params);
		return getPlancoExerValidesWithQual(ec, exercice, qual, refresh);
	}

	public static NSArray getPlancoExerValidesWithQual(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		return _getPlancoExerValidesWithQual(ec, exercice, qual, refresh);
	}

	public static NSArray getPlancoExersWithQual(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		return _getPlancoExerWithQual(ec, exercice, qual, refresh);
	}

	public static EOPlanComptableExer findPlanComptableExerForPcoNumInList(NSArray planComptableList, String pcoNum) {
		final NSArray tmp = EOQualifier.filteredArrayWithQualifier(planComptableList, new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorEqual, pcoNum));
		if (tmp.count() == 1) {
			return ((EOPlanComptableExer) tmp.objectAtIndex(0));
		}
		return null;
	}

	private static NSArray _getPlancoExerValidesWithQual(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		final EOQualifier qualValide = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.PCO_VALIDITE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
			EOPlanComptableExer.etatValide
		}));
		final EOQualifier quals = (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualValide, qual
		})) : new EOAndQualifier(new NSArray(new Object[] {
			qualValide
		})));
		return _getPlancoExerWithQual(ec, exercice, quals, refresh);
	}

	private static NSArray _getPlancoExerWithQual(EOEditingContext ec, EOExercice exercice, EOQualifier qual, final boolean refresh) {
		final EOQualifier qualExercice = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableExer.EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
			exercice
		}));

		final EOQualifier quals = (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualExercice, qual
		})) : new EOAndQualifier(new NSArray(new Object[] {
			qualExercice
		})));
		return ZFinder.fetchArray(ec, EOPlanComptableExer.ENTITY_NAME, quals, SORT_PCO_NUM_ASC, refresh);
	}

}
