/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.finders;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOEmargementDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.metier.EOFournisseur;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOGestionExercice;
import org.cocktail.maracuja.client.metier.EOLot;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOMarche;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOOrdreDePaiement;
import org.cocktail.maracuja.client.metier.EOOrgan;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPaiement;
import org.cocktail.maracuja.client.metier.EOParametre;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableAmo;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOPlancoAmortissement;
import org.cocktail.maracuja.client.metier.EOPlancoRelance;
import org.cocktail.maracuja.client.metier.EORib;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreBrouillard;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeCredit;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe regroupe des méthodes pour fetcher plusieurs types d'objets métiers (pour lesquels il n'est pas indispensable d'avoir une classe
 * spécifique...).
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EOsFinder extends ZFinder {
	public static ApplicationClient myApp = (ApplicationClient) EOApplication.sharedApplication();

	/**
	 * Renvoie l'exercice par défaut (à ouvrir lorsqu'on entre dans l'appli). Déclenche une exception si aucun exercice est trouvé.
	 */
	public static final EOExercice getDefaultExercice(EOEditingContext ec) throws ZFinderException {
		final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey("exeExercice", EOSortOrdering.CompareAscending);
		final NSArray lesExer = ZFinder.fetchArray(ec, EOExercice.ENTITY_NAME, "exeType=%@", new NSArray(new Object[] {
				EOExercice.EXE_TYPE_TRESORERIE
		}), new NSArray(sortOrdering), true);
		if ((lesExer != null) && (lesExer.count() > 0)) {
			return (EOExercice) lesExer.objectAtIndex(0);
		}
		throw new ZFinderException("Aucun exercice n'a été récupéré depuis la base de données.");
	}

	/**
	 * Récupérer toutes les fonctions définies dans la base
	 * 
	 * @param ec
	 * @throws ZFinderException
	 */
	public static NSArray getAllFonctions(EOEditingContext ec) throws ZFinderException {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("fonCategorie", EOSortOrdering.CompareAscending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("fonIdInterne", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOFonction.ENTITY_NAME, null, null, new NSArray(new Object[] {
				sort1, sort2
		}), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucune fonction n'a été récupérée depuis la base de données.");
	}

	/**
	 * Renvoie un tableau d'individu avec nom et prenom like parametres.
	 * 
	 * @param ec
	 * @param nom
	 * @param prenom
	 * @param noIndividu
	 * @return
	 * @throws ZFinderException
	 */
	public static NSArray getIndividuUlrs(EOEditingContext ec, String nom, String prenom, Integer noIndividu) throws ZFinderException {
		//        String cond = "";
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOSortOrdering.sortOrderingWithKey("nomUsuel", EOSortOrdering.CompareAscending));
		larray.addObject(EOSortOrdering.sortOrderingWithKey("prenom", EOSortOrdering.CompareAscending));

		final EOQualifier qualInitial = EOQualifier.qualifierWithQualifierFormat("temValide=%@", new NSArray("O"));

		//        cond = " temValide='O'";
		final NSMutableArray qualsOr = new NSMutableArray();

		if (!ZStringUtil.estVide(nom)) {
			nom = nom + "*";
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("nomUsuel caseInsensitiveLike %@", new NSArray(nom));
			qualsOr.addObject(qual);
		}

		if (!ZStringUtil.estVide(prenom)) {
			prenom = prenom + "*";
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("prenom caseInsensitiveLike %@", new NSArray(prenom));
			qualsOr.addObject(qual);
		}

		if (noIndividu != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("noIndividu=%@", new NSArray(noIndividu));
			qualsOr.addObject(qual);
		}

		final EOQualifier qualFilters = new EOOrQualifier(qualsOr);

		final EOAndQualifier qualGlobal = new EOAndQualifier(new NSArray(new Object[] {
				qualInitial, qualFilters
		}));

		final NSArray res = ZFinder.fetchArray(ec, "IndividuUlr", qualGlobal, larray, true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun indivividu n'a été récupérée depuis la base de données.");
	}

	public static final NSArray getAllComptabilites(EOEditingContext ec) throws ZFinderException {
		final NSArray res = ZFinder.fetchArray(ec, EOComptabilite.ENTITY_NAME, null, null, new NSArray(EOSortOrdering.sortOrderingWithKey("comLibelle", EOSortOrdering.CompareAscending)), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun objet de type " + EOComptabilite.ENTITY_NAME + " n'a été récupéré depuis la base de données.");
	}

	/**
	 * Renvoie tous les exercices de type comptable, classés par ordre descendant.
	 * 
	 * @param ec
	 * @throws ZFinderException
	 */
	public static NSArray getAllExerciceComptables(EOEditingContext ec) throws ZFinderException {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("exeExercice", EOSortOrdering.CompareDescending);
		final NSArray res = ZFinder.fetchArray(ec, EOExercice.ENTITY_NAME, "exeType='C'", null, new NSArray(s), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun objet de type Exercice n'a été récupéré depuis la base de données.");
	}

	/**
	 * Renvoie tous les exercices
	 * 
	 * @param ec
	 * @return
	 * @throws ZFinderException
	 */
	public static NSArray getAllExercices(EOEditingContext ec) throws ZFinderException {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("exeExercice", EOSortOrdering.CompareDescending);
		final NSArray res = ZFinder.fetchArray(ec, EOExercice.ENTITY_NAME, "exeExercice>=2005", null, new NSArray(s), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun objet de type Exercice n'a été récupéré depuis la base de données.");
	}

	//    public static NSArray getAllAgentJefy(EOEditingContext ec) throws ZFinderException {
	//        final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("agtNom", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering s2 = EOSortOrdering.sortOrderingWithKey("agtPrenom", EOSortOrdering.CompareAscending);
	//        final NSArray res = ZFinder.fetchArray(ec, "AgentJefy", null, null, new NSArray(new Object[] { s, s2 }), true);
	//        if ((res != null) && (res.count() > 0)) {
	//            return res;
	//        }
	//        throw new ZFinderException("Aucun objet de type AgentJefy n'a été récupéré depuis la base de données.");
	//    }

	/**
	 * Récupère les mandatBrouillard affectés à un mandat.
	 * 
	 * @param ec
	 * @param mandat
	 * @return
	 * @throws ZFinderException
	 */
	public static final NSArray getMandatBrouilardsForMAndat(EOEditingContext ec, EOMandat mandat) {
		if (mandat == null) {
			return new NSArray();
		}
		final NSArray res = ZFinder.fetchArray(ec, EOMandatBrouillard.ENTITY_NAME, "mandat=%@", new NSArray(mandat), null, true);
		// On tri en renvoyant d'abord les debits
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("mabSens", EOSortOrdering.CompareDescending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("planComptable.pcoNum", EOSortOrdering.CompareAscending);
		if ((res != null) && (res.count() > 0)) {
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					sort1, sort2
			}));
			// return res;
		}
		return new NSArray();
	}

	public static final NSArray getTitreBrouilardsForTitre(EOEditingContext ec, EOTitre titre) {
		if (titre == null) {
			return new NSArray();
		}
		final NSArray res = ZFinder.fetchArray(ec, EOTitreBrouillard.ENTITY_NAME, "titre=%@", new NSArray(titre), null, true);
		// On tri en renvoyant d'abord les debits
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("tibSens", EOSortOrdering.CompareDescending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("planComptable.pcoNum", EOSortOrdering.CompareAscending);
		if ((res != null) && (res.count() > 0)) {
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					sort1, sort2
			}));
			// return res;
		}
		return new NSArray();
	}

	public static final NSArray getParametres(EOEditingContext ec, EOExercice exer) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("parKey", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOParametre.ENTITY_NAME, "exercice=%@", new NSArray(new Object[] {
				exer
		}), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static NSMutableDictionary getParametresDico(final EOEditingContext ec, final EOExercice exer) {
		final NSArray array = getParametres(ec, exer);
		final NSMutableDictionary dico = new NSMutableDictionary();
		for (int i = 0; i < array.count(); i++) {
			final EOParametre element = (EOParametre) array.objectAtIndex(i);
			dico.takeValueForKey(element.parValue(), element.parKey());
		}
		return dico;
	}

	public static final NSArray getTypesCredits(EOEditingContext ec, EOExercice exer) {
		final EOQualifier qualExer = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				exer
		}));
		final NSArray res = ZFinder.fetchArray(ec, EOTypeCredit.ENTITY_NAME, qualExer, new NSArray(new Object[] {
				EOTypeCredit.SORT_TCD_CODE_ASC
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static final NSArray getTypesCreditsDepenseExecutoire(EOEditingContext ec, EOExercice exer) {
		return EOQualifier.filteredArrayWithQualifier(getTypesCredits(ec, exer), EOTypeCredit.QUAL_DEPENSE_EXECUTOIRE);
	}

	public static final NSArray getTypesCreditsRecette(EOEditingContext ec, EOExercice exer) {
		return EOQualifier.filteredArrayWithQualifier(getTypesCredits(ec, exer), EOTypeCredit.QUAL_RECETTE_EXECUTOIRE);
	}

	public static final NSArray getModePaiementsValide(EOEditingContext ec, EOExercice exer) throws ZFinderException {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("modLibelle", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOModePaiement.ENTITY_NAME, "modValidite=%@ and exercice=%@", new NSArray(new Object[] {
				EOModePaiement.etatValide, exer
		}), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static final NSArray getTypeOperationsPubliques(EOEditingContext ec) throws ZFinderException {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("topLibelle", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOTypeOperation.ENTITY_NAME, "topType=%@", new NSArray(new Object[] {
				EOTypeOperation.TOP_TYPE_PUBLIC
		}), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static final NSArray getAllTypeOperations(EOEditingContext ec) throws ZFinderException {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("topLibelle", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOTypeOperation.ENTITY_NAME, null, null, new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static NSArray fetchAllPlancomptableAmo(EOEditingContext ec) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey(EOPlanComptableAmo.PCOA_NUM_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOPlanComptableAmo.ENTITY_NAME, null, new NSArray(new Object[] {}), new NSArray(new Object[] {
				s
		}), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static final NSArray getAllTypeJournal(EOEditingContext ec) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("tjoLibelle", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOTypeJournal.ENTITY_NAME, null, null, new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static final NSArray getTypeBordereauxScol(EOEditingContext ec) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("tboLibelle", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOTypeBordereau.ENTITY_NAME, "tboSousType=%@", new NSArray(EOTypeBordereau.SOUS_TYPE_SCOLARITE), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	/**
	 * Renvoie les types de bordereaux sur un type particulier
	 * 
	 * @param ec
	 * @param tboType
	 * @return
	 */
	public static final NSArray getLesTypeBordereauxByType(EOEditingContext ec, String tboType) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOTypeBordereau.ENTITY_NAME, EOTypeBordereau.TBO_TYPE_KEY + "=%@", new NSArray(tboType), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	public static final NSArray getLesTypeBordereauxByQual(EOEditingContext ec, EOQualifier qual) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey(EOTypeBordereau.TBO_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOTypeBordereau.ENTITY_NAME, qual, new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	/**
	 * @param editingContext
	 * @param filterText
	 * @param object
	 * @param object2
	 * @return
	 * @throws ZFinderException
	 */
	public static NSArray getFournisseurs(final EOEditingContext editingContext, String nom, String ville, String cp, String foucode) throws ZFinderException {
		String cond = "";
		final NSMutableArray larray = new NSMutableArray();
		final NSMutableArray quals = new NSMutableArray();

		larray.addObject(EOSortOrdering.sortOrderingWithKey("adrNom", EOSortOrdering.CompareAscending));
		larray.addObject(EOSortOrdering.sortOrderingWithKey("adrPrenom", EOSortOrdering.CompareAscending));

		if (!ZStringUtil.estVide(nom)) {
			nom = "*" + nom + "*";
			cond = "adrNom caseInsensitiveLike %@";
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(cond, new NSArray(nom)));
		}
		if (!ZStringUtil.estVide(ville)) {
			ville = "*" + ville + "*";
			cond = " adrVille caseInsensitiveLike %@";
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(cond, new NSArray(ville)));
		}
		if (!ZStringUtil.estVide(cp)) {
			cp = "*" + cp + "*";
			cond = " adrCp = %@";
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(cond, new NSArray(cp)));
		}
		if (!ZStringUtil.estVide(foucode)) {
			cond = " fouCode = %@";
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(cond, new NSArray(foucode)));
		}

		EOQualifier qualValide = new EOKeyValueQualifier(EOFournisseur.FOU_VALIDE_KEY, EOQualifier.QualifierOperatorEqual, "O");

		NSArray res = ZFinder.fetchArray(editingContext, EOFournisseur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] {
				qualValide, new EOOrQualifier(quals)
		})), larray, true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun fournisseur n'a été récupéré depuis la base de données.");
	}

	//    /**
	//     * Renvoie une chaine representant l'organ selon son niveau
	//     */
	//    public final static String getMiniStringForOrgan(EOOrgan organ) {
	//        // Suivant le niveau
	//        switch (((Number) organ.storedValueForKey("orgNiv")).intValue()) {
	//        case 2:
	//            // Niveau 2 : Composante
	//            return (String) organ.storedValueForKey("orgComp");
	//        case 4:
	//            // Niveau 4 : CR
	//            return (String) organ.storedValueForKey("orgLbud");
	//        case 5:
	//            // Niveau 5 : UC
	//            return (String) organ.storedValueForKey("orgUc");
	//        default:
	//            // Sinon : libelle
	//            return (String) organ.storedValueForKey("orgLib");
	//        }
	//    }

	/**
	 * @param editingContext
	 * @return
	 */
	public static NSArray getAllOrgan(final EOEditingContext editingContext, final EOExercice exercice) {
		final NSTimestamp debut = new NSTimestamp(ZDateUtil.getFirstDayOfYear(exercice.exeExercice().intValue()));
		final NSTimestamp fin = new NSTimestamp(ZDateUtil.getLastDayOfYear(exercice.exeExercice().intValue()));
		final NSArray sorts = new NSArray(new Object[] {
				EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC
		});
		final EOQualifier qualEx = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + ">1 and " + EOOrgan.ORG_DATE_OUVERTURE_KEY + "<=%@ and (" + EOOrgan.ORG_DATE_CLOTURE_KEY + " = nil or " + EOOrgan.ORG_DATE_CLOTURE_KEY + ">=%@)", new NSArray(new Object[] {
				debut, fin
		}));

		final NSArray res = ZFinder.fetchArray(editingContext, EOOrgan.ENTITY_NAME, qualEx, null, true);
		//        return res;
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, sorts);
	}

	//    public static NSArray getOrganLucratives(EOEditingContext editingContext) {
	//        // EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("orgNiv",
	//        // EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort11 = EOSortOrdering.sortOrderingWithKey("orgUnit", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("orgComp", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort3 = EOSortOrdering.sortOrderingWithKey("orgLbud", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort4 = EOSortOrdering.sortOrderingWithKey("orgUc", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort5 = EOSortOrdering.sortOrderingWithKey("orgLib", EOSortOrdering.CompareAscending);
	//        final NSArray sorts = new NSArray(new Object[] { sort11, sort2, sort3, sort4, sort5 });
	//
	//        final NSArray res = ZFinder.fetchArray(editingContext, "Organ", "orgNiv>1 and orgLucrativite=1", null, null, true);
	//
	//        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, sorts);
	//    }

	public static NSArray getOriginesLucratives(final EOEditingContext editingContext, final EOExercice exercice) {
		final NSTimestamp debut = new NSTimestamp(ZDateUtil.getFirstDayOfYear(exercice.exeExercice().intValue()));
		final NSTimestamp fin = new NSTimestamp(ZDateUtil.getLastDayOfYear(exercice.exeExercice().intValue()));
		//        final NSArray sorts = new NSArray(new Object[] { EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC });
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EOOrigine.ORI_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final EOQualifier qualEx = EOQualifier.qualifierWithQualifierFormat(EOOrigine.TYPE_OPERATION_KEY + "." + EOTypeOperation.TOP_LIBELLE_KEY + "=%@ and " + EOOrigine.ORI_ENTITE_KEY + "='JEFY_ADMIN.ORGAN' and " + EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_LUCRATIVITE_KEY + "=1 and "
				+ EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_DATE_OUVERTURE_KEY + "<=%@ and (" + EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_DATE_CLOTURE_KEY + " = nil or " + EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_DATE_CLOTURE_KEY + ">=%@)", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_LUCRATIVE, debut, fin
		}));
		//        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat( EOOrigine.ORI_ENTITE_KEY + "='JEFY_ADMIN.ORGAN'and organ.orgLucrativite=1 and organ."+EOOrgan.ORG_DATE_OUVERTURE_KEY + "<=%@ and (organ."+EOOrgan.ORG_DATE_CLOTURE_KEY+" = nil or organ."+EOOrgan.ORG_DATE_CLOTURE_KEY+">=%@)", new NSArray(new Object[]{debut, fin}));
		final NSArray res = ZFinder.fetchArray(editingContext, EOOrigine.ENTITY_NAME, qualEx, new NSArray(sort), false);
		return res;
	}

	public static NSArray getOriginesConvRA(final EOEditingContext editingContext, final EOExercice exercice) {
		final NSTimestamp debut = new NSTimestamp(ZDateUtil.getFirstDayOfYear(exercice.exeExercice().intValue()));
		final NSTimestamp fin = new NSTimestamp(ZDateUtil.getLastDayOfYear(exercice.exeExercice().intValue()));
		//        final NSArray sorts = new NSArray(new Object[] { EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC });
		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EOOrigine.ORI_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final EOQualifier qualEx = EOQualifier.qualifierWithQualifierFormat(EOOrigine.TYPE_OPERATION_KEY + "." + EOTypeOperation.TOP_LIBELLE_KEY + "=%@ and " + EOOrigine.ORI_ENTITE_KEY + "='JEFY_ADMIN.ORGAN' and " + EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_DATE_OUVERTURE_KEY + "<=%@ and ("
				+ EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_DATE_CLOTURE_KEY + " = nil or " + EOOrigine.ORGAN_KEY + "." + EOOrgan.ORG_DATE_CLOTURE_KEY + ">=%@)", new NSArray(new Object[] {
				EOTypeOperation.TYPE_OPERATION_CONVENTION, debut, fin
		}));
		//        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat( EOOrigine.ORI_ENTITE_KEY + "='JEFY_ADMIN.ORGAN'and organ.orgLucrativite=1 and organ."+EOOrgan.ORG_DATE_OUVERTURE_KEY + "<=%@ and (organ."+EOOrgan.ORG_DATE_CLOTURE_KEY+" = nil or organ."+EOOrgan.ORG_DATE_CLOTURE_KEY+">=%@)", new NSArray(new Object[]{debut, fin}));
		final NSArray res = ZFinder.fetchArray(editingContext, EOOrigine.ENTITY_NAME, qualEx, new NSArray(sort), false);
		return res;
	}

	//    public static NSArray getOrganLucratives(EOEditingContext editingContext) {
	//        // EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("orgNiv",
	//        // EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort11 = EOSortOrdering.sortOrderingWithKey("orgUnit", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("orgComp", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort3 = EOSortOrdering.sortOrderingWithKey("orgLbud", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort4 = EOSortOrdering.sortOrderingWithKey("orgUc", EOSortOrdering.CompareAscending);
	//        final EOSortOrdering sort5 = EOSortOrdering.sortOrderingWithKey("orgLib", EOSortOrdering.CompareAscending);
	//        final NSArray sorts = new NSArray(new Object[] { sort11, sort2, sort3, sort4, sort5 });
	//
	//        final NSArray res = ZFinder.fetchArray(editingContext, "Organ", "orgNiv>1 and orgLucrativite=1", null, null, true);
	//
	//        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, sorts);
	//    }    

	/**
	 * @param vir
	 * @return Un tableau des depenses à payer pour le virement.
	 */
	public static final NSArray getDepensesForFichierVirement(final EOEditingContext ec, final EOPaiement vir) {
		final NSArray mandats = vir.mandats();
		final NSMutableArray depenses = new NSMutableArray();
		for (int i = 0; i < mandats.count(); i++) {
			final EOMandat element = (EOMandat) mandats.objectAtIndex(i);
			depenses.addObjectsFromArray(element.depenses());
		}
		return depenses;
	}

	public static final NSArray getRecettesForFichierVirement(final EOEditingContext ec, final EOPaiement vir) {
		final NSArray titres = vir.titres();
		final NSMutableArray recettes = new NSMutableArray();
		for (int i = 0; i < titres.count(); i++) {
			final EOTitre element = (EOTitre) titres.objectAtIndex(i);
			recettes.addObjectsFromArray(element.recettes());
		}
		return recettes;
	}

	public static final NSArray getOrdreDePaiementsForFichierVirement(final EOEditingContext ec, final EOPaiement vir) {
		return vir.ordreDePaiements();
	}

	//    /**
	//     * @param editingContext
	//     * @return Le parametrage BDF valide.
	//     * @throws ZFinderException
	//     *             Si plusieurs parametres valides sont recuperes (ou 0)
	//     */
	//    public static final EOVirementParamBdf getVirementParamBdfActif(final EOEditingContext ec) throws ZFinderException {
	//        NSArray pars = ZFinder.fetchArray(ec, EOVirementParamBdf.ENTITY_NAME, "vpbEtat=%@", new NSArray(EOVirementParamBdf.ETAT_VALIDE), null, true);
	//        if (pars.count() != 1) {
	//            throw new ZFinderException("Les paramètres BDF ne sont pas correctement définis (aucun ou plusieurs valides)");
	//        }
	//        return (EOVirementParamBdf) pars.objectAtIndex(0);
	//    }
	//
	//    /**
	//     * @param editingContext
	//     * @return Le parametrage ETEBAC valide.
	//     * @throws ZFinderException
	//     *             Si plusieurs parametres valides sont recuperes (ou 0)
	//     */
	//    public static final EOVirementParamEtebac getVirementParamEtebacActif(final EOEditingContext ec) throws ZFinderException {
	//        NSArray pars = ZFinder.fetchArray(ec, EOVirementParamEtebac.ENTITY_NAME, "vpbEtat=%@", new NSArray(EOVirementParamEtebac.ETAT_VALIDE), null, true);
	//        if (pars.count() != 1) {
	//            throw new ZFinderException("Les paramètres Etebac ne sont pas correctement définis (aucun ou plusieurs valides)");
	//        }
	//        return (EOVirementParamEtebac) pars.objectAtIndex(0);
	//    }
	//
	//    /**
	//     * @param editingContext
	//     * @return Le parametrage ETEBAC valide.
	//     * @throws ZFinderException
	//     *             Si plusieurs parametres valides sont recuperes (ou 0)
	//     */
	//    public static final EOVirementParamVint getVirementParamVintActif(final EOEditingContext ec) throws ZFinderException {
	//        NSArray pars = ZFinder.fetchArray(ec, EOVirementParamVint.ENTITY_NAME, "vpbEtat=%@", new NSArray(EOVirementParamVint.ETAT_VALIDE), null, true);
	//        if (pars.count() != 1) {
	//            throw new ZFinderException("Les paramètres Vint ne sont pas correctement définis (aucun ou plusieurs valides)");
	//        }
	//        return (EOVirementParamVint) pars.objectAtIndex(0);
	//    }

	/**
	 * @param editingContext
	 * @param marcheOrdre
	 * @return
	 */
	public static final EOMarche getMarcheForMarOrdre(final EOEditingContext editingContext, final Integer marcheOrdre) {
		return (EOMarche) fetchObject(editingContext, EOMarche.ENTITY_NAME, "marOrdre=%@", new NSArray(marcheOrdre), null, false);
	}

	/**
	 * @param editingContext
	 * @param lotOrdre
	 * @return
	 */
	public static final EOLot getLotForLotOrdre(final EOEditingContext editingContext, final Integer lotOrdre) {
		return (EOLot) fetchObject(editingContext, EOLot.ENTITY_NAME, "lotOrdre=%@", new NSArray(lotOrdre), null, false);
	}

	/**
	 * Renvoie les objets gestions
	 * 
	 * @param ec
	 * @throws ZFinderException
	 */
	public static final NSArray getAllGestionsPourExercice(EOEditingContext ec, EOExercice exercice) throws ZFinderException {
		if (exercice == null) {
			throw new ZFinderException("Exercice non spécifié.");
		}
		final NSArray res = ZFinder.fetchArray(ec, EOGestion.ENTITY_NAME, "gestionExercices.exercice=%@", new NSArray(exercice), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		//        NSArray res = ZFinder
		//        .fetchArray(ec, "GestionExercice", "exercice=%@", new NSArray(exercice), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		//        if ((res != null) && (res.count() > 0)) {
		//            return (NSArray) res.valueForKey("gestion");
		//        }
		throw new ZFinderException("Aucun objet de type gestion n'a été récupéré depuis la base de données. Verifiez que les codes gestions sont autorisés pour l'exercice " + exercice.exeExercice());
	}

	public static final NSArray getAllGestionExercicesPourExercice(EOEditingContext ec, final EOExercice exercice) throws ZFinderException {
		if (exercice == null) {
			throw new ZFinderException("Exercice non spécifié.");
		}
		final NSArray res = ZFinder.fetchArray(ec, EOGestionExercice.ENTITY_NAME, "exercice=%@", new NSArray(exercice), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		//      NSArray res = ZFinder
		//      .fetchArray(ec, "GestionExercice", "exercice=%@", new NSArray(exercice), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		//      if ((res != null) && (res.count() > 0)) {
		//      return (NSArray) res.valueForKey("gestion");
		//      }
		throw new ZFinderException("Aucun objet de type gestion n'a été récupéré depuis la base de données. Verifiez que les codes gestions sont autorisés pour l'exercice " + exercice.exeExercice());
	}

	public static final NSArray getAllGestions(EOEditingContext ec) throws ZFinderException {
		final NSArray res = ZFinder.fetchArray(ec, EOGestion.ENTITY_NAME, null, null, new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun objet de type gestion n'a été récupéré depuis la base de données.");
	}

	/**
	 * @param ec
	 * @param comptabilite
	 * @return
	 * @throws Exception
	 */
	public static final NSArray getSACDGestionForComptabilite(final EOEditingContext ec, final EOComptabilite comptabilite, final EOExercice exercice) throws Exception {
		if (exercice == null) {
			throw new ZFinderException("Exercice non spécifié.");
		}

		final NSArray res = getAllGestionExercicesPourExercice(ec, exercice);
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("gestion.comptabilite=%@ and planComptable185.pcoNum<>nil", new NSArray(comptabilite));
		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);
		if ((res2 != null) && (res2.count() > 0)) {
			return (NSArray) res2.valueForKey("gestion");
		}
		return new NSArray();
		//        NSArray res = ZFinder                .fetchArray(ec, "GestionExercice", "exercice=%@ and gestion.comptabilite=%@ and planComptable185.pcoNum<>nil", new NSArray(new Object[]{exercice, comptabilite}), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		//        if ((res != null) && (res.count() > 0)) {
		//            return (NSArray) res.valueForKey("gestion");
		//        }
		//        throw new ZFinderException("Aucun objet de type gestion n'a été récupéré depuis la base de données. Verifiez que les codes gestions sont autorisés pour l'exercice " + exercice.exeExercice());
		//changement pco_num_185 dans gestion_exercice
		//        final NSArray res = getAllGestionsPourExercice(ec, exercice);
		//        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("comptabilite = %@ and planComptable185.pcoNum<>nil", new NSArray(comptabilite));
		//        return EOQualifier.filteredArrayWithQualifier(res, qual);
	}

	/**
	 * @param editingContext
	 * @param currentExercice
	 * @return
	 * @throws Exception
	 */
	public static final NSArray getSACDGestion(final EOEditingContext ec, final EOExercice exercice) throws Exception {

		if (exercice == null) {
			throw new ZFinderException("Exercice non spécifié.");
		}

		final NSArray res = getAllGestionExercicesPourExercice(ec, exercice);
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("planComptable185.pcoNum<>nil", null);
		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);

		if ((res2 != null) && (res2.count() > 0)) {
			return (NSArray) res2.valueForKey("gestion");
		}
		return new NSArray();

	}

	/**
	 * @param editingContext
	 * @param currentExercice
	 * @return
	 * @throws Exception
	 */
	public static final NSArray getGestionNonSacd(final EOEditingContext ec, final EOExercice exercice) throws Exception {
		if (exercice == null) {
			throw new ZFinderException("Exercice non spécifié.");
		}
		final NSArray res = getAllGestionExercicesPourExercice(ec, exercice);
		//		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(   EOGestionExercice.PLAN_COMPTABLE185_KEY   "planComptable185=nil", null);
		final EOQualifier qual = new EOKeyValueQualifier(EOGestionExercice.PLAN_COMPTABLE185_KEY, EOQualifier.QualifierOperatorEqual, null);
		final NSArray res2 = EOQualifier.filteredArrayWithQualifier(res, qual);
		if ((res2 != null) && (res2.count() > 0)) {
			//			NSMutableArray res3 = new NSMutableArray();
			//			Iterator iter = res2.vector().iterator();
			//			while (iter.hasNext()) {
			//				EOGestionExercice object = (EOGestionExercice) iter.next();
			//				res3.addObject(object.gestion());
			//			}
			//			return res3.immutableClone();
			return (NSArray) res2.valueForKey(EOGestionExercice.GESTION_KEY);
		}
		return new NSArray();

		//        if (exercice == null) {
		//            throw new ZFinderException("Exercice non spécifié.");
		//        }
		//        
		//        final NSArray res = ZFinder.fetchArray(ec, "Gestion", "gestionExercices=%@ and gestionExercices.planComptable185=nil", new NSArray(new Object[]{exercice}), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		//        return res;

		//        NSArray res = ZFinder
		//                .fetchArray(ec, "GestionExercice", "exercice=%@ and planComptable185=nil", new NSArray(new Object[]{exercice}), new NSArray(EOSortOrdering.sortOrderingWithKey(EOGestion.GES_CODE_KEY, EOSortOrdering.CompareAscending)), true);
		//        if ((res != null) && (res.count() > 0)) {
		//            return (NSArray) res.valueForKey("gestion");
		//        }
		//        return new NSArray();
		//        throw new ZFinderException("Aucun objet de type gestion n'a été récupéré depuis la base de données. Verifiez que les codes gestions sont autorisés pour l'exercice " + exercice.exeExercice());

		//        
		//        final NSArray res = getAllGestionsPourExercice(ec, exercice);
		//        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("planComptable185=nil", null);
		//        return EOQualifier.filteredArrayWithQualifier(res, qual);
	}

	public static final EOTypeBordereau getLeTypeBordereau(final EOEditingContext ed, final String leType) {
		return (EOTypeBordereau) fetchObject(ed, EOTypeBordereau.ENTITY_NAME, "tboType = %@", new NSArray(new Object[] {
				leType
		}), null, false);
	}

	public static final EOTypeBordereau getLeSousTypeBordereau(final EOEditingContext ed, final String leType) {
		return (EOTypeBordereau) fetchObject(ed, EOTypeBordereau.ENTITY_NAME, "tboSousType = %@", new NSArray(new Object[] {
				leType
		}), null, false);
	}

	public static final EOTypeJournal getLeTypeJournal(final EOEditingContext ed, final String leType) {
		return (EOTypeJournal) fetchObject(ed, EOTypeJournal.ENTITY_NAME, "tjoLibelle = %@", new NSArray(new Object[] {
				leType
		}), null, false);
	}

	public static final EOTypeOperation getLeTypeOperation(final EOEditingContext ed, final String leType) {
		return (EOTypeOperation) fetchObject(ed, EOTypeOperation.ENTITY_NAME, "topLibelle = %@", new NSArray(new Object[] {
				leType
		}), null, false);
	}

	/**
	 * @param ecriture
	 * @return Les émargements qui ont été créés à partir d'une écriture
	 */
	public static final NSArray getEmargementsForEcriture(final EOEditingContext ed, final EOEcriture ecriture) {
		final NSArray ecds = ecriture.detailEcriture();
		final NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < ecds.count(); i++) {
			final EOEcritureDetail element = (EOEcritureDetail) ecds.objectAtIndex(i);
			final NSArray emds = getEmargementDetailsForEcritureDetail(ed, element);

			for (int j = 0; j < emds.count(); j++) {
				final EOEmargementDetail element2 = (EOEmargementDetail) emds.objectAtIndex(j);
				if (res.indexOfObject(element2.emargement()) == NSArray.NotFound) {
					res.addObject(element2.emargement());
				}
			}
		}
		return res;
	}

	public static final NSArray getEmargementDetailsForEcritureDetail(final EOEditingContext ed, final EOEcritureDetail ecd) {
		final EOQualifier qualDest = EOQualifier.qualifierWithQualifierFormat("source=%@ and emargement.emaEtat=%@", new NSArray(new Object[] {
				ecd, EOEmargement.emaEtatValide
		}));
		final NSArray destinations = fetchArray(ed, EOEmargementDetail.ENTITY_NAME, qualDest, null, true);
		final EOQualifier qualSource = EOQualifier.qualifierWithQualifierFormat("destination=%@ and emargement.emaEtat=%@", new NSArray(new Object[] {
				ecd, EOEmargement.emaEtatValide
		}));
		final NSArray sources = fetchArray(ed, EOEmargementDetail.ENTITY_NAME, qualSource, null, true);

		final NSMutableArray dest2 = new NSMutableArray();
		dest2.addObjectsFromArray(sources);

		for (int i = 0; i < destinations.count(); i++) {
			if (sources.indexOfObject(destinations.objectAtIndex(i)) == NSArray.NotFound) {
				dest2.addObject(destinations.objectAtIndex(i));
			}
		}
		return dest2;
	}

	/**
	 * @param ed
	 * @param ecd
	 * @return Les ecritures detail emargees avec ecd (n'inclue pas ecd).
	 */
	public static final NSArray getEcritureDetailsEmargees(final EOEditingContext ed, final EOEcritureDetail ecd) {
		NSArray res = getEmargementDetailsForEcritureDetail(ed, ecd);
		NSMutableArray res2 = new NSMutableArray();
		for (int i = 0; i < res.count(); i++) {
			EOEmargementDetail emd = (EOEmargementDetail) res.objectAtIndex(i);
			EOEcritureDetail ecd1 = emd.source();
			EOEcritureDetail ecd2 = emd.destination();
			if (!ecd1.equals(ecd) && res2.indexOfObject(ecd1) == NSArray.NotFound) {
				res2.addObject(ecd1);
			}
			if (!ecd2.equals(ecd) && res2.indexOfObject(ecd2) == NSArray.NotFound) {
				res2.addObject(ecd2);
			}
		}
		return res2.immutableClone();
	}

	public static final NSArray getEcrituresForTitNumero(final EOEditingContext ed, final Integer titNumero) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("titre.titNumero=%@", new NSArray(new Object[] {
				titNumero
		}));
		final NSArray res = fetchArray(ed, EOTitreDetailEcriture.ENTITY_NAME, qual, null, false);
		final NSMutableArray res2 = new NSMutableArray();
		for (int i = 0; i < res.count(); i++) {
			EOTitreDetailEcriture element = (EOTitreDetailEcriture) res.objectAtIndex(i);
			if (res2.indexOfObject(element.ecritureDetail().ecriture()) == NSArray.NotFound) {
				res2.addObject(element.ecritureDetail().ecriture());
			}
		}
		return res2;
	}

	public static final NSArray getEcrituresForManNumero(final EOEditingContext ed, final Integer manNumero) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("mandat.manNumero=%@", new NSArray(new Object[] {
				manNumero
		}));
		final NSArray res = fetchArray(ed, EOMandatDetailEcriture.ENTITY_NAME, qual, null, false);
		final NSMutableArray res2 = new NSMutableArray();
		for (int i = 0; i < res.count(); i++) {
			EOMandatDetailEcriture element = (EOMandatDetailEcriture) res.objectAtIndex(i);
			if (res2.indexOfObject(element.ecritureDetail().ecriture()) == NSArray.NotFound) {
				res2.addObject(element.ecritureDetail().ecriture());
			}
		}
		return res2;
	}

	public static final EOPlancoRelance getPlancoRelanceForPlanco(final EOEditingContext ec, final EOPlanComptable pco) {
		return (EOPlancoRelance) fetchObject(ec, EOPlancoRelance.ENTITY_NAME, EOPlancoRelance.PLAN_COMPTABLE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				pco
		}), null, false);
	}

	public static final EOPlancoRelance getPlancoRelanceForPlancoExer(final EOEditingContext ec, final EOPlanComptableExer pco) {
		return (EOPlancoRelance) fetchObject(ec, EOPlancoRelance.ENTITY_NAME, EOPlancoRelance.PLAN_COMPTABLE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				pco.planComptable()
		}), null, false);
	}

	public static final EOPlancoAmortissement getPlancoAmortissementForPlanco(final EOEditingContext ec, final EOPlanComptable pco, final EOExercice exercice) {
		if (pco.plancoAmortissements().count() == 0) {
			return null;
		}
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlancoAmortissement.EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				exercice
		}));
		final NSArray res = EOQualifier.filteredArrayWithQualifier(pco.plancoAmortissements(), qual);
		if (res.count() > 0) {
			return (EOPlancoAmortissement) res.objectAtIndex(0);
		}
		return null;

	}

	public static final EOPlanComptableAmo getPlancomptableAmoForPlanco(final EOEditingContext ec, final EOPlanComptable pco, final EOExercice exercice) {
		final EOPlancoAmortissement plancoAmortissement = getPlancoAmortissementForPlanco(ec, pco, exercice);
		if (plancoAmortissement == null) {
			return null;
		}
		return plancoAmortissement.planComptableAmo();
	}

	public static final EOPlanComptableAmo getPlanComptableAmoForPcoANum(final EOEditingContext ec, final String pcoANum) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPlanComptableAmo.PCOA_NUM_KEY + QUAL_EQUALS + QUAL_AND + EOPlanComptableAmo.TYPE_ETAT_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				pcoANum, ZFinderEtats.etat_VALIDE()
		}));
		final NSArray res = EOQualifier.filteredArrayWithQualifier(fetchAllPlancomptableAmo(ec), qual);
		if (res.count() > 0) {
			return (EOPlanComptableAmo) res.objectAtIndex(0);
		}
		return null;
	}

	/**
	 * Renvoie les écritures qui participent aux émargements de l'écriture passée en parametre.
	 * 
	 * @param ecriture
	 * @return
	 */
	public static final NSArray getEcrituresEmargees(final EOEditingContext ec, final EOEcriture ecriture) {
		final NSArray ecds = ecriture.detailEcriture();
		final NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < ecds.count(); i++) {
			final EOEcritureDetail element = (EOEcritureDetail) ecds.objectAtIndex(i);
			final NSArray emds = getEmargementDetailsForEcritureDetail(ec, element);

			for (int j = 0; j < emds.count(); j++) {
				final EOEmargementDetail element2 = (EOEmargementDetail) emds.objectAtIndex(j);
				final EOEcritureDetail complement;
				if (element.equals(element2.source())) {
					complement = element2.destination();
				}
				else {
					complement = element2.source();
				}
				if (res.indexOfObject(complement.ecriture()) == NSArray.NotFound) {
					res.addObject(complement.ecriture());
				}
			}
		}
		return res;
	}

	public static NSArray getModeRecouvrementsValide(EOEditingContext editingContext, EOExercice currentExercice) {
		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("modLibelle", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(editingContext, EOModeRecouvrement.ENTITY_NAME, "modValidite=%@ and exercice=%@", new NSArray(new Object[] {
				EOModeRecouvrement.etatValide, currentExercice
		}), new NSArray(new Object[] {
				s
		}), false);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		return new NSArray();
	}

	/**
	 * Renvoie les dates du calendrier BDF qui entourent date (-10 jours/ +30 jours)
	 * 
	 * @param editingContext
	 * @param date
	 * @return NSArray de Date
	 * @throws ParseException
	 */
	public static final NSArray getCalendrierBdfAutour(final EOEditingContext editingContext, final Date date) throws ParseException {
		return getCalendrierBdfAutour(editingContext, date, 10, 30);
	}

	public static final NSArray getCalendrierBdfAutour(final EOEditingContext editingContext, final Date date, int nbJoursAvant, int nbJOursApres) throws ParseException {
		final String dateMinIso = ZConst.FORMAT_DATE_YYYYMMDD.format(ZDateUtil.addDHMS(date, -nbJoursAvant, 0, 0, 0));
		final String dateMaxIso = ZConst.FORMAT_DATE_YYYYMMDD.format(ZDateUtil.addDHMS(date, nbJOursApres, 0, 0, 0));
		final String dateBdfKey = "bdfcalDateBdf";
		final Format dateBdfFormat = new SimpleDateFormat("ddMMyy");

		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("bdfcalDateIso", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(editingContext, "BdfCalendrier", "bdfcalDateIso>=%@ and bdfcalDateIso<=%@", new NSArray(new Object[] {
				dateMinIso, dateMaxIso
		}), new NSArray(new Object[] {
				s
		}), true);
		final NSMutableArray cal = new NSMutableArray();
		if ((res != null) && (res.count() > 0)) {

			// On ne garde que le champ dateBdf en le transformant en date
			for (int i = 0; i < res.count(); i++) {
				final EOEnterpriseObject element = (EOEnterpriseObject) res.objectAtIndex(i);
				cal.addObject(dateBdfFormat.parseObject((String) element.valueForKey(dateBdfKey)));
			}
		}
		return cal;
	}

	//    
	/**
	 * Renvoie les dates du calendrier BDF posterieures a date
	 * 
	 * @param editingContext
	 * @param date
	 * @return NSArray de Date
	 * @throws ParseException
	 */
	public static final NSArray getCalendrierBdfAfter(final EOEditingContext editingContext, final Date date) throws ParseException {
		final String dateMinIso = ZConst.FORMAT_DATE_YYYYMMDD.format(date);
		final String dateBdfKey = "bdfcalDateBdf";
		final Format dateBdfFormat = new SimpleDateFormat("ddMMyy");

		final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("bdfcalDateIso", EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(editingContext, "BdfCalendrier", "bdfcalDateIso>=%@", new NSArray(new Object[] {
				dateMinIso
		}), new NSArray(new Object[] {
				s
		}), true);
		final NSMutableArray cal = new NSMutableArray();
		if ((res != null) && (res.count() > 0)) {

			// On ne garde que le champ dateBdf en le transformant en date
			for (int i = 0; i < res.count(); i++) {
				final EOEnterpriseObject element = (EOEnterpriseObject) res.objectAtIndex(i);
				cal.addObject(dateBdfFormat.parseObject((String) element.valueForKey(dateBdfKey)));
			}
		}
		return cal;
	}

	//    
	//    

	public static final NSArray getRibsValides(final EOEditingContext editingContext, final EOFournisseur fournis) {
		return ZFinder.fetchArray(editingContext, EORib.ENTITY_NAME, EORib.RIB_VALIDE_KEY + "='" + EORib.RIB_VALIDE + "' and " + EORib.FOURNISSEUR_KEY + "=%@", new NSArray(new Object[] {
				fournis
		}), null, true);
	}

	/**
	 * Récupere les comptes qui ont été utilisés comme comptes clients sur l'exercice passé en parametre.
	 * 
	 * @param editingContext
	 * @param exeOrdreOld
	 * @return
	 */
	public static ArrayList getPlancomptableClients(EOEditingContext editingContext, Integer exeOrdreOld) {
		final String sql = "select distinct ecd.pco_num as PCO_NUM from maracuja.ecriture_detail ecd, maracuja.titre_detail_ecriture tde, maracuja.titre t, maracuja.bordereau b where ecd.ecd_ordre=tde.ecd_ordre and ecd.exe_ordre=" + exeOrdreOld
				+ " and tde.tit_id=t.tit_id and t.bor_id=b.bor_id and b.tbo_ordre=7 and ecd.pco_num like '4%' and ecd.pco_num not like '4457%'";
		NSArray res = ServerProxy.clientSideRequestSqlQuery(editingContext, sql);

		final ArrayList res2 = new ArrayList();
		for (int i = 0; i < res.count(); i++) {
			NSDictionary array_element = (NSDictionary) res.objectAtIndex(i);
			res2.add(array_element.valueForKey("PCO_NUM"));
		}
		return res2;
	}

	/**
	 * @param rib
	 * @param montantDisquette
	 * @return Les depenses payees pour ce rib et ce montant
	 */
	public static NSArray fetchDepensesPayeesSimilaires(EOEditingContext ec, EORib rib, Number montantDisquette) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EODepense.MANDAT_KEY + "." + EOMandat.MAN_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOMandat.mandatVirement));
		quals.addObject(new EOKeyValueQualifier(EODepense.MANDAT_KEY + "." + EOMandat.RIB_KEY + "." + EORib.RIB_NUM_KEY, EOQualifier.QualifierOperatorEqual, rib.ribNum()));
		quals.addObject(new EOKeyValueQualifier(EODepense.MANDAT_KEY + "." + EOMandat.RIB_KEY + "." + EORib.RIB_COD_BANC_KEY, EOQualifier.QualifierOperatorEqual, rib.ribCodBanc()));
		quals.addObject(new EOKeyValueQualifier(EODepense.MANDAT_KEY + "." + EOMandat.RIB_KEY + "." + EORib.RIB_GUICH_KEY, EOQualifier.QualifierOperatorEqual, rib.ribGuich()));
		quals.addObject(new EOKeyValueQualifier(EODepense.MANDAT_KEY + "." + EOMandat.RIB_KEY + "." + EORib.RIB_CLE_KEY, EOQualifier.QualifierOperatorEqual, rib.ribCle()));
		quals.addObject(new EOKeyValueQualifier(EODepense.DEP_MONTANT_DISQUETTE_KEY, EOQualifier.QualifierOperatorEqual, montantDisquette));

		NSArray res = fetchArray(ec, EODepense.ENTITY_NAME, new EOAndQualifier(quals), new NSArray(new Object[] {
				EOSortOrdering.sortOrderingWithKey(EODepense.DEP_ORDRE_KEY, EOSortOrdering.CompareAscending)
		}), false);

		return res;
	}

	/**
	 * @param rib
	 * @param montantDisquette
	 * @return Les ordres de paiement payes pour ce rib et ce montant
	 */
	public static NSArray fetchOdpPayesSimilaires(EOEditingContext ec, EORib rib, Number montantDisquette) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOOrdreDePaiement.ODP_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOOrdreDePaiement.etatVirement));
		quals.addObject(new EOKeyValueQualifier(EOOrdreDePaiement.RIB_KEY + "." + EORib.RIB_NUM_KEY, EOQualifier.QualifierOperatorEqual, rib.ribNum()));
		quals.addObject(new EOKeyValueQualifier(EOOrdreDePaiement.RIB_KEY + "." + EORib.RIB_COD_BANC_KEY, EOQualifier.QualifierOperatorEqual, rib.ribCodBanc()));
		quals.addObject(new EOKeyValueQualifier(EOOrdreDePaiement.RIB_KEY + "." + EORib.RIB_GUICH_KEY, EOQualifier.QualifierOperatorEqual, rib.ribGuich()));
		quals.addObject(new EOKeyValueQualifier(EOOrdreDePaiement.RIB_KEY + "." + EORib.RIB_CLE_KEY, EOQualifier.QualifierOperatorEqual, rib.ribCle()));
		quals.addObject(new EOKeyValueQualifier(EOOrdreDePaiement.ODP_MONTANT_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, montantDisquette));

		NSArray res = fetchArray(ec, EOOrdreDePaiement.ENTITY_NAME, new EOAndQualifier(quals), new NSArray(new Object[] {
				EOSortOrdering.sortOrderingWithKey(EOOrdreDePaiement.ODP_DATE_SAISIE_KEY, EOSortOrdering.CompareAscending)
		}), false);

		return res;
	}

	/**
	 * @param key ex. toto
	 * @param liste ex.: "4*,5*"
	 * @return Un EOQualifier du genre toto like "4*" or toto like "5*"
	 */
	public static EOQualifier qualifierForKeyLikeInlist(String key, String[] liste) {
		NSMutableArray quals = new NSMutableArray();
		for (int i = 0; i < liste.length; i++) {
			String string = liste[i];
			quals.addObject(new EOKeyValueQualifier(key, EOQualifier.QualifierOperatorLike, string));
		}
		return new EOOrQualifier(quals);
	}

	/**
	 * @param key ex. toto
	 * @param liste ex.: "4*,5*"
	 * @return Un EOQualifier du genre toto like "4*" or toto like "5*"
	 */
	public static EOQualifier qualifierForKeyLikeInlist(String key, String listeAsString) {
		String[] list = listeAsString.split(",");
		for (int i = 0; i < list.length; i++) {
			list[i] = list[i].trim();
		}
		return qualifierForKeyLikeInlist(key, list);
	}

	public static EOQualifier qualifierForPconumContrepartiesDepense() {
		return qualifierForKeyLikeInlist(EOPlanComptableExer.PCO_NUM_KEY, myApp.getParametre(ApplicationClient.CONTREPARTIE_DEPENSE_COMPTES_KEY, "28*,29*,3*,4*,5*"));
	}

	public static EOQualifier qualifierForPconumContrepartiesRecette() {
		return qualifierForKeyLikeInlist(EOPlanComptableExer.PCO_NUM_KEY, myApp.getParametre(ApplicationClient.CONTREPARTIE_RECETTE_COMPTES_KEY, "102*,103*,139*,3*,4*,5*"));
	}

}
