/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.chgtex.ctrl;

import org.cocktail.maracuja.client.factories.ZFactory;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


/**
 * Sert à effectuer le changement automatique d'exercice (de trésorerie vers comptable, etc.)
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ChgtExerciceCtrl extends ZFactory {
    public final static String EXCEPTION_MSG_EXERCICE_CHGT="Erreur lors du changement automatique d'exercice : ";
    
    public final static String EXCEPTION_MSG_EXERCICE_NON_OUVERT = EXCEPTION_MSG_EXERCICE_CHGT + "L'exercice n'est pas ouvert"; 
    public final static String EXCEPTION_MSG_EXERCICE_NON_TRESORERIE = EXCEPTION_MSG_EXERCICE_CHGT + "L'exercice n'est pas l'exercice de tresorerie"; 
    public final static String EXCEPTION_MSG_EXERCICES_SUIVENT_PAS = EXCEPTION_MSG_EXERCICE_CHGT + "Les exercices ne se suivent pas";
    private static final String EXCEPTION_MSG_EXERCICE_EXISTE_PAS = EXCEPTION_MSG_EXERCICE_CHGT + "L'exercice n'existe pas, contactez un administrateur pour qu'il prépare le nouvel exercice"; 
    private static final String EXCEPTION_MSG_EXERCICE_PREPARATION_PAS = EXCEPTION_MSG_EXERCICE_CHGT + "L'exercice n'est pas en mode PREPARATION"; 
    
    
    public ChgtExerciceCtrl() {
        super(null);
    }
    
    public EOExercice effectuerChangement(final EOEditingContext editingContext, final EOExercice ancienExercice) throws Exception {
        checkOldExercice(ancienExercice);
        final int newExer = ancienExercice.exeExercice().intValue()+1;
        final EOExercice newExercice = (EOExercice) EOsFinder.fetchObject(editingContext, EOExercice.ENTITY_NAME, "exeExercice=%@", new NSArray(new Object[]{new Integer(newExer)}), null, true);
        if (newExercice==null) {
            throw new Exception(newExer + " : " + EXCEPTION_MSG_EXERCICE_EXISTE_PAS);
        }
        checkNewExercice(newExercice);
        
        //modifier l'exercice de jefy_admin
        newExercice.setExeType(EOExercice.EXE_TYPE_TRESORERIE);        
        newExercice.setExeStat(EOExercice.EXE_ETAT_OUVERT);
        newExercice.setExeStatEng(EOExercice.EXE_ETAT_OUVERT);
        newExercice.setExeStatFac(EOExercice.EXE_ETAT_OUVERT);
        newExercice.setExeStatLiq(EOExercice.EXE_ETAT_OUVERT);
        newExercice.setExeStatRec(EOExercice.EXE_ETAT_OUVERT);
        
        ancienExercice.setExeType(EOExercice.EXE_TYPE_COMPTABLE);        
        ancienExercice.setExeStat(EOExercice.EXE_ETAT_RESTREINT);
        ancienExercice.setExeStatEng(EOExercice.EXE_ETAT_RESTREINT);
        ancienExercice.setExeStatFac(EOExercice.EXE_ETAT_RESTREINT);   
        ancienExercice.setExeStatLiq(EOExercice.EXE_ETAT_RESTREINT);   
        ancienExercice.setExeStatRec(EOExercice.EXE_ETAT_RESTREINT);   
        
        
        System.out.println("Changement d'exercice effectué. : " + ancienExercice.exeExercice().intValue() +" est a present ouvert");
        
        return newExercice;
        
    }
    
    
    private void checkOldExercice(final EOExercice exercice) throws Exception {
        //Vérifier que l'exercice est Ouvert + TResorerie
        if (!exercice.estOuvert()) {
            throw new Exception(exercice.exeExercice() + " : " + EXCEPTION_MSG_EXERCICE_NON_OUVERT);
        }
        if (!exercice.estTresorerie()) {
            throw new Exception(exercice.exeExercice() + " : " + EXCEPTION_MSG_EXERCICE_NON_TRESORERIE);
        }
    }
    
    
    private void checkNewExercice(final EOExercice exercice) throws Exception {

        if (!exercice.estPreparation()) {
            throw new Exception(exercice.exeExercice() + " : " + EXCEPTION_MSG_EXERCICE_PREPARATION_PAS);
        }

    }
    
    
    
    
    

}
