/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.chgtex.ctrl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.chgtex.ui.BalanceEntreePanel;
import org.cocktail.maracuja.client.chgtex.ui.BalanceEntreePanel.IBalanceEntreePanelModel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;


public class BalanceEntreeCtrl extends CommonCtrl {
	private static final String TITLE = "Balance d'entrée";
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	private final ActionClose actionClose = new ActionClose();
	private final ActionAdd actionAdd = new ActionAdd();
	private final ActionRemove actionRemove = new ActionRemove();
	private final ActionTraitement actionTraitement = new ActionTraitement();

	private final PlancomptableSelectListPanelListener selectListPanelListener = new PlancomptableSelectListPanelListener();
	private final PlancomptableDispoListPanelListener dispoListPanelListener = new PlancomptableDispoListPanelListener();

	private final BalanceEntreePanel balanceEntreePanel;
	private final BalanceEntreePanelModel balanceEntreePanelModel;

	private NSArray allPlanComptables;

	//    private NSMutableArray filteredPlancomptableDispos;
	private NSMutableArray plancomptableDispos;
	private NSMutableArray plancomptableSelect;
	private ArrayList pcoNumClients;

	private final Integer utlOrdre;
	private final Integer exeOrdre;
	private final Integer exeOrdreOld;
	private final Integer comOrdre;

	private final ZEOComboBoxModel comboboxModelGestion;
	private final ZEOComboBoxModel gestionsSacdModel;

	private final EOPlanComptableComparator planComptableComparator = new EOPlanComptableComparator();
	private final EOExercice exerciceNew;
	private final EOExercice exerciceOld;
	private final NSArray gestionsExerOld;
	private final NSArray gestionsExerNew;
	private final NSArray gestions;

	private final String reqPcosGestion = "select * from maracuja.V_BE_REPRISE_GESTION";
	private final String reqPcosConsolidee = "select * from maracuja.V_BE_REPRISE_CONSOLIDEE";
	private final ComboGestionListener comboGestionListener = new ComboGestionListener();
	private final CodeGestionSacdListener comboSacdListener = new CodeGestionSacdListener();

	private final DispoRender dispoRender = new DispoRender();
	private EOComptabilite comptabilite;
	private NSArray sacds;
	private NSArray sacdsOld;
	private NSArray sacdsNew;

	public BalanceEntreeCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		comptabilite = myApp.appUserInfo().getCurrentComptabilite();

		exeOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), getExercice()).valueForKey("exeOrdre");
		utlOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), getUtilisateur()).valueForKey("utlOrdre");
		comOrdre = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), getComptabilite()).valueForKey("comOrdre");

		exerciceNew = getExercice();
		exerciceOld = (EOExercice) EOsFinder.fetchObject(getEditingContext(), EOExercice.ENTITY_NAME, "exeExercice=%@", new NSArray(new Integer(exerciceNew.exeExercice().intValue() - 1)), null, false);

		exeOrdreOld = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), exerciceOld).valueForKey("exeOrdre");

		//gestionsExerOld = EOsFinder.getAllGestionsPourExercice(getEditingContext(), exerciceOld);
		//gestionsExerNew = EOsFinder.getAllGestionsPourExercice(getEditingContext(), exerciceNew);

		gestionsExerOld = EOsFinder.getGestionNonSacd(editingContext, exerciceOld);
		gestionsExerNew = EOsFinder.getGestionNonSacd(editingContext, exerciceNew);
		ArrayList lesTableaux = new ArrayList();
		lesTableaux.add(gestionsExerOld);
		lesTableaux.add(gestionsExerNew);
		gestions = ZEOUtilities.intersectionOfNSArray(lesTableaux);

		sacdsOld = EOsFinder.getSACDGestionForComptabilite(getEditingContext(), comptabilite, exerciceOld);
		sacdsNew = EOsFinder.getSACDGestionForComptabilite(getEditingContext(), comptabilite, exerciceNew);
		//ArrayList lesTableaux = new ArrayList();
		lesTableaux.clear();
		lesTableaux.add(sacdsOld);
		lesTableaux.add(sacdsNew);
		sacds = ZEOUtilities.intersectionOfNSArray(lesTableaux);

		gestionsSacdModel = new ZEOComboBoxModel(sacds, EOGestion.GES_CODE_KEY, "Hors SACD", new SACDFormat());
		//		comboboxModelGestion = new ZEOComboBoxModel(gestionsExerNew, EOGestion.GES_CODE_KEY, "Consolidée -> " + getComptabilite().gestion().gesCode(), null);
		comboboxModelGestion = new ZEOComboBoxModel(gestions, EOGestion.GES_CODE_KEY, "Consolidée -> " + getComptabilite().gestion().gesCode(), null);

		pcoNumClients = EOsFinder.getPlancomptableClients(getEditingContext(), exeOrdreOld);

		balanceEntreePanelModel = new BalanceEntreePanelModel();
		balanceEntreePanel = new BalanceEntreePanel(balanceEntreePanelModel);
	}

	private final void updatePlanComptables() throws Exception {
		//		final EOGestion currentGestion = (EOGestion) comboboxModelGestion.getSelectedEObject();
		final EOGestion currentGestion = currentGestion();

		//Si on consolide sur l'agence
		if (currentGestion == null) {
			//On exclut les SACD (qui sont SACD sur les deux exercices)
			// on les exclue de la requete
			String condsacd = "";
			for (int i = 0; i < sacds.count(); i++) {
				EOGestion element = (EOGestion) sacds.objectAtIndex(i);
				if (condsacd.length() > 0) {
					condsacd = condsacd + " and ";
				}
				condsacd = condsacd + " ges_code <> '" + element.gesCode() + "' ";
			}

			allPlanComptables = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), reqPcosConsolidee + " where exe_ordre=" + exeOrdreOld + " and com_ordre=" + comOrdre + (condsacd.length() > 0 ? " and (" + condsacd + ")" : ""));

		}
		else {
			allPlanComptables = ServerProxy.clientSideRequestSqlQuery(getEditingContext(), reqPcosGestion + " where exe_ordre=" + exeOrdreOld + " and com_ordre=" + comOrdre + " and ges_code='" + currentGestion.gesCode() + "'");
		}

		plancomptableDispos = new NSMutableArray();
		plancomptableDispos.addObjectsFromArray(allPlanComptables);
		plancomptableSelect = new NSMutableArray();

		balanceEntreePanel.updateData();

	}

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		final ZKarukeraDialog win;

		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		balanceEntreePanel.setMyDialog(getMyDialog());
		balanceEntreePanel.initGUI();
		balanceEntreePanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(balanceEntreePanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final void openDialog(final Window dial) {
		revertChanges();
		final ZKarukeraDialog win = createModalDialog(dial);
		setMyDialog(win);
		try {

			final NSArray complementOfexerNew = ZEOUtilities.complementOfNSArray(gestionsExerNew, gestionsExerOld);
			if (complementOfexerNew != null && complementOfexerNew.count() > 0) {
				showWarningDialog("Certains codes gestions étaient définis sur l'exercice " + exerciceOld.exeExercice().intValue() +
						" et ne le sont plus sur l'exercice " + exerciceNew.exeExercice().intValue() + ". Vous ne pourrez donc pas " +
						"effectuer une balance d'entrée non consolidée pour les codes gestions suivants  : " + ZEOUtilities.getCommaSeparatedListOfValues(complementOfexerNew, EOGestion.GES_CODE_KEY));
			}

			final NSArray complementOfexerNew2 = ZEOUtilities.complementOfNSArray(sacdsNew, sacdsOld);
			if (complementOfexerNew2 != null && complementOfexerNew2.count() > 0) {
				showWarningDialog("Certains SACDs étaient définis sur l'exercice " + exerciceOld.exeExercice().intValue() +
						" et ne le sont plus sur l'exercice " + exerciceNew.exeExercice().intValue() + ". Utilisez l'option consolidée pour effectuer la balance d'entrée pour les SACDs  " + ZEOUtilities.getCommaSeparatedListOfValues(complementOfexerNew, EOGestion.GES_CODE_KEY));
			}
			updatePlanComptables();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private final void checkDatas() throws Exception {

	}

	private final void faitTraitement() {
		if (plancomptableSelect.count() == 0) {
			showInfoDialog("Veuillez sélectionner les comptes à basculer.");
			return;
		}

		try {
			setWaitCursor(true);
			checkDatas();

			//Faire la liste des pconum a transferer
			final NSArray pcos = plancomptableSelect;

			String pcoChaine = null;
			if (pcos.count() == 1) {
				pcoChaine = (String) ((NSKeyValueCoding) pcos.objectAtIndex(0)).valueForKey("PCO_NUM");
			}
			else {
				pcoChaine = ZEOUtilities.getSeparatedListOfValues(pcos, "pcoNum", "$") + "$$";
			}
			System.out.println(pcoChaine);

			//			final EOGestion gestion = (EOGestion) comboboxModelGestion.getSelectedEObject();
			final EOGestion gestion = currentGestion();
			final ArrayList lesEcrOrdres = new ArrayList();

			final boolean ecritureParCompte = BalanceEntreePanelModel.OPTION_ECRITURE_PARCOMPTE.equals(balanceEntreePanelModel.currentOptionEcriture);
			final boolean ecritureGroupe = BalanceEntreePanelModel.OPTION_ECRITURE_GROUPE.equals(balanceEntreePanelModel.currentOptionEcriture);
			final boolean ecritureParDetail = BalanceEntreePanelModel.OPTION_ECRITURE_PARDETAIL.equals(balanceEntreePanelModel.currentOptionEcriture);

			//verifier que s'il s'agit de compte client, l'option detaillee est bien cochee            
			if (!BalanceEntreePanelModel.OPTION_DETAILLEE.equals(balanceEntreePanelModel.getCurrentOption())) {
				boolean isCompteClient = false;
				for (int i = 0; i < pcos.count() && !isCompteClient; i++) {
					NSKeyValueCoding pco = (NSKeyValueCoding) pcos.objectAtIndex(i);
					isCompteClient = pcoNumClients.contains(pco.valueForKey("PCO_NUM"));
				}

				if (isCompteClient) {
					if (!showConfirmationDialog("Confirmation",
							"Attention, la balance d'entrée sur les comptes clients doit être effectuée avec l'option Détaillée (sinon vous allez perdre le suivi des restes à recouvrer). Vous n'avez pas sélectionné l'option Détaillée, souhaitez-vous néanmoins continuer ?", ZMsgPanel.BTLABEL_YES)) {
						setWaitCursor(false);
						return;
					}
				}
			}

			final String listeComptes = ZEOUtilities.getSeparatedListOfValues(plancomptableSelect, "PCO_NUM", "$") + "$$";

			String erreurs = "";

			if (showConfirmationDialog("Confirmation", "Voulez-vous créer les écritures de BE pour le(s) compte(s) sélectionnés", ZMsgPanel.BTLABEL_YES)) {
				System.out.println("balanceEntreePanelModel.getCurrentOption() = " + balanceEntreePanelModel.getCurrentOption());
				System.out.println("balanceEntreePanelModel.currentOptionEcriture = " + balanceEntreePanelModel.currentOptionEcriture);
				System.out.println("gestion = " + gestion);
				//gérer l'erreur possible sur certains comptes et pas les autres

				//si on est en consolide vers l'agence
				if (gestion == null) {
					if (ecritureParCompte) {
						if (BalanceEntreePanelModel.OPTION_SOLDE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_solde_du_compte((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"));
									if (n != null)
										lesEcrOrdres.add(new Integer(n));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else if (BalanceEntreePanelModel.OPTION_DC.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_dc_du_compte((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"));
									if (n != null)
										lesEcrOrdres.add(new Integer(n));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else if (BalanceEntreePanelModel.OPTION_DETAILLEE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_detail_du_compte((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"));
									if (n != null)
										lesEcrOrdres.add(new Integer(n));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else if (BalanceEntreePanelModel.OPTION_MANUELLE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									basculer_manuel_du_compte((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else {
							throw new DataCheckException("Option non supportée");
						}
					}
					else { //ecritureParCompte
						if (BalanceEntreePanelModel.OPTION_SOLDE.equals(balanceEntreePanelModel.getCurrentOption())) {
							final String n = basculer_solde_comptes(listeComptes);
							if (n != null)
								lesEcrOrdres.add(new Integer(n));
						}
						else if (BalanceEntreePanelModel.OPTION_DC.equals(balanceEntreePanelModel.getCurrentOption())) {
							final String n = basculer_dc_comptes(listeComptes);
							if (n != null)
								lesEcrOrdres.add(new Integer(n));
						}
						else if (BalanceEntreePanelModel.OPTION_DETAILLEE.equals(balanceEntreePanelModel.getCurrentOption())) {
							final String n = basculer_detail_comptes(listeComptes);
							if (n != null)
								lesEcrOrdres.add(new Integer(n));
						}
						else {
							throw new DataCheckException("Option non supportée");
						}
					}
				}
				//              si on est sur un code gestion particulier
				else { //gestion==null
					if (ecritureParCompte) {
						if (BalanceEntreePanelModel.OPTION_SOLDE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_solde_du_compte_ges((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"), gestion.gesCode());
									if (n != null)
										lesEcrOrdres.add(new Integer(n));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else if (BalanceEntreePanelModel.OPTION_DC.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_dc_du_compte_ges((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"), gestion.gesCode());
									if (n != null)
										lesEcrOrdres.add(new Integer(n));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else if (BalanceEntreePanelModel.OPTION_DETAILLEE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_detail_du_compte_ges((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"), gestion.gesCode());
									if (n != null)
										lesEcrOrdres.add(new Integer(n));
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else if (BalanceEntreePanelModel.OPTION_MANUELLE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									basculer_manuel_du_compte_ges((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"), gestion.gesCode());
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else {
							throw new DataCheckException("Option non supportée");
						}

					}
					else if (ecritureGroupe) {
						if (BalanceEntreePanelModel.OPTION_SOLDE.equals(balanceEntreePanelModel.getCurrentOption())) {
							final String n = basculer_solde_comptes_ges(listeComptes, gestion.gesCode());
							if (n != null)
								lesEcrOrdres.add(new Integer(n));
						}
						else if (BalanceEntreePanelModel.OPTION_DC.equals(balanceEntreePanelModel.getCurrentOption())) {
							final String n = basculer_dc_comptes_ges(listeComptes, gestion.gesCode());
							if (n != null)
								lesEcrOrdres.add(new Integer(n));
						}
						else if (BalanceEntreePanelModel.OPTION_DETAILLEE.equals(balanceEntreePanelModel.getCurrentOption())) {
							final String n = basculer_detail_comptes_ges(listeComptes, gestion.gesCode());
							if (n != null)
								lesEcrOrdres.add(new Integer(n));
						}
						else {
							throw new DataCheckException("Option non supportée");
						}
					}
					else if (ecritureParDetail) {
						if (BalanceEntreePanelModel.OPTION_DETAILLEE.equals(balanceEntreePanelModel.getCurrentOption())) {
							for (int i = 0; i < plancomptableSelect.count(); i++) {
								try {
									final String n = basculer_detail_du_cpt_ges_n((String) ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM"), gestion.gesCode());
									if (n != null) {
										final String[] necrs = n.split("\\$");
										for (int j = 0; j < necrs.length; j++) {
											final String string = necrs[j];
											if (string != null && string.length() > 0) {
												lesEcrOrdres.add(new Integer(string));
											}
										}
									}
								} catch (Exception e) {
									e.printStackTrace();
									erreurs = erreurs + ((NSKeyValueCoding) plancomptableSelect.objectAtIndex(i)).valueForKey("PCO_NUM") + " : " + e.getMessage() + "\n";
								}
							}
						}
						else {
							throw new DataCheckException("Option non supportée");
						}
					}
					else {
						throw new DataCheckException("Option non supportée");
					}
				}

				System.out.println("Traitement fini");

				final Iterator iterator = lesEcrOrdres.iterator();
				final NSMutableArray quals = new NSMutableArray();
				while (iterator.hasNext()) {
					final Integer element = (Integer) iterator.next();
					quals.addObject(EOQualifier.qualifierWithQualifierFormat("ecrOrdre=%@", new NSArray(element)));
				}

				updatePlanComptables();
				balanceEntreePanel.updateData();

				if (erreurs.length() > 0) {
					showWarningDialog("Il y a eu des erreurs : " + erreurs);
				}

				String msg = "Traitement terminé. \n";
				if (quals.count() > 0) {
					final NSArray ecritures = EOsFinder.fetchArray(getEditingContext(), EOEcriture.tableName, new EOOrQualifier(quals), new NSArray(EOSortOrdering.sortOrderingWithKey("ecrNumero", EOSortOrdering.CompareAscending)), true);
					setWaitCursor(false);

					msg = msg + "Les écritures de BE ont été créées : " + ZEOUtilities.getCommaSeparatedListOfValues(ecritures, "ecrNumero") + "\n souhaitez-vous visualiser ces écritures ?";
					if (showConfirmationDialog("Succès", msg, ZMsgPanel.BTLABEL_YES)) {
						if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {
							EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
							win.openDialog(getMyDialog(), ecritures);
						}
						else {
							throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
						}
					}
				}
				else {
					setWaitCursor(false);
					msg = msg + "Aucune écriture n'a été créée.";
					showInfoDialog(msg);
				}

			}

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final String basculer_solde_du_compte(final String pconum) {
		System.out.println("BalanceEntreeCtrl.basculer_solde_compte()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");

		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_solde_du_compte", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_solde_du_compte_ges(final String pconum, final String gestion) {
		System.out.println("BalanceEntreeCtrl.basculer_solde_compte_gestion()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_solde_du_compte_ges", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;

	}

	private final String basculer_solde_comptes(final String lesPconum) {
		System.out.println("BalanceEntreeCtrl.basculer_solde_des_comptes()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(lesPconum, "05_lespconums");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		//        return (String) ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_solde_comptes", params).valueForKey("80_ecrordres");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_solde_comptes", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_solde_comptes_ges(final String lesPconum, final String gestion) {
		System.out.println("BalanceEntreeCtrl.basculer_solde_comptes_gestion()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(lesPconum, "05_lespconums");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_solde_comptes_ges", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_dc_du_compte(final String pconum) {
		System.out.println("BalanceEntreeCtrl.basculer_dc_du_compte()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_dc_du_compte", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_dc_du_compte_ges(final String pconum, final String gestion) {
		System.out.println("BalanceEntreeCtrl.basculer_dc_du_compte_gestion()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_dc_du_compte_ges", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_dc_comptes(final String lesPconum) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(lesPconum, "05_lespconums");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_dc_comptes", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_dc_comptes_ges(final String lesPconum, final String gestion) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(lesPconum, "05_lespconums");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_dc_comptes_ges", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_detail_du_compte(final String pconum) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_detail_du_compte", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_detail_du_compte_ges(final String pconum, final String gestion) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_detail_du_compte_ges", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_detail_du_cpt_ges_n(final String pconum, final String gestion) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_detail_du_cpt_ges_n", params).valueForKey("80_ecrordres");

		ZLogger.verbose("basculer_detail_du_cpt_ges_n res =" + res);

		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_detail_comptes(final String lesPconum) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(lesPconum, "05_lespconums");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_detail_comptes", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final String basculer_detail_comptes_ges(final String lesPconum, final String gestion) {
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(lesPconum, "05_lespconums");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		final Object res = ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_detail_comptes_ges", params).valueForKey("80_ecrordres");
		if (res instanceof String) {
			return (String) res;
		}
		return null;
	}

	private final void basculer_manuel_du_compte(final String pconum) {
		System.out.println("BalanceEntreeCtrl.basculer_manuel_du_compte()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_manuel_du_compte", params);
	}

	private final void basculer_manuel_du_compte_ges(final String pconum, final String gestion) {
		System.out.println("BalanceEntreeCtrl.basculer_manuel_du_compte()");
		final NSMutableDictionary params = new NSMutableDictionary();
		params.takeValueForKey(pconum, "05_pconum");
		params.takeValueForKey(exeOrdre, "10_exeordre");
		params.takeValueForKey(utlOrdre, "15_utlordre");
		params.takeValueForKey(gestion, "20_gescode");
		ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "basculer_be.basculer_manuel_du_compte_ges", params);
	}

	private final class EOPlanComptableComparator extends NSComparator {
		public int compare(Object arg0, Object arg1) throws ComparisonException {
			final NSKeyValueCoding pco0 = (NSKeyValueCoding) arg0;
			final NSKeyValueCoding pco1 = (NSKeyValueCoding) arg1;
			return (((String) pco0.valueForKey("PCO_NUM")).compareTo((String) pco1.valueForKey("PCO_NUM")));
		}

	}

	private final void compteAdd() {
		try {
			final NSArray pcosel = balanceEntreePanel.getDispoListPanel().selectedObjects();
			for (int i = 0; i < pcosel.count(); i++) {
				final NSKeyValueCoding planComptable = (NSKeyValueCoding) pcosel.objectAtIndex(i);
				plancomptableDispos.removeObject(planComptable);
				plancomptableSelect.addObject(planComptable);
			}

			sortPcoSelect();

			balanceEntreePanel.getDispoListPanel().updateData();
			balanceEntreePanel.getSelectListPanel().updateData();

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void compteRemove() {
		try {

			final NSArray pcosel = balanceEntreePanel.getSelectListPanel().selectedObjects();
			for (int i = 0; i < pcosel.count(); i++) {
				final NSKeyValueCoding planComptable = (NSKeyValueCoding) pcosel.objectAtIndex(i);
				plancomptableSelect.removeObject(planComptable);
				plancomptableDispos.addObject(planComptable);
			}

			//            final EOPlanComptable planComptable = (EOPlanComptable) balanceEntreePanel.getSelectListPanel().selectedObject(); 
			//            plancomptableSelect.removeObject(planComptable);
			//            plancomptableDispos.addObject(planComptable);

			sortPcoDispos();

			balanceEntreePanel.getDispoListPanel().updateData();
			balanceEntreePanel.getSelectListPanel().updateData();

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void sortPcoSelect() throws Exception {
		plancomptableSelect.sortUsingComparator(planComptableComparator);
	}

	private final void sortPcoDispos() throws Exception {
		plancomptableDispos.sortUsingComparator(planComptableComparator);
	}

	private final NSArray getPlanComptableDispos() {
		return plancomptableDispos;
	}

	private final NSArray getPlanComptableSelect() {
		return plancomptableSelect;
	}

	private final class BalanceEntreePanelModel implements IBalanceEntreePanelModel {
		public final static String OPTION_SOLDE = "Solde (Reprise du solde debiteur ou créditeur des écritures non émargées)";
		public final static String OPTION_DC = "Débit et crédit (Reprise du débit et du crédit des écritures non émargées)";
		public final static String OPTION_DETAILLEE = "Détaillée (Reprise de toutes les écritures non émargées)";
		public final static String OPTION_MANUELLE = "Manuelle (Ne plus afficher ces comptes ici)";

		public final static String OPTION_ECRITURE_GROUPE = "Un n° d'écriture regroupant tous les comptes";
		public final static String OPTION_ECRITURE_PARCOMPTE = "Un n° d'écriture par compte";
		public final static String OPTION_ECRITURE_PARDETAIL = "Un n° d'écriture par détail (Non consolidé / option détaillée obligatoire)";

		private final FilterDocumentListener filterDocumentListener = new FilterDocumentListener();
		private final CheckBoxSoldeListener checkBoxSoldeListener = new CheckBoxSoldeListener();

		private ArrayList options = new ArrayList(5);
		private Action defaultOption;
		private String currentOption;

		private ArrayList optionsEcriture = new ArrayList(4);
		private Action defaultOptionEcriture;
		private String currentOptionEcriture;

		private String lefiltre = null;

		public BalanceEntreePanelModel() {
			final ArrayList optionsString = new ArrayList(4);
			optionsString.add(OPTION_SOLDE);
			optionsString.add(OPTION_DC);
			optionsString.add(OPTION_DETAILLEE);
			optionsString.add(OPTION_MANUELLE);

			final ArrayList optionsEcritureString = new ArrayList(3);
			optionsEcritureString.add(OPTION_ECRITURE_GROUPE);
			optionsEcritureString.add(OPTION_ECRITURE_PARCOMPTE);
			optionsEcritureString.add(OPTION_ECRITURE_PARDETAIL);

			for (int i = 0; i < optionsString.size(); i++) {
				final Action action = new AbstractAction((String) optionsString.get(i)) {
					public void actionPerformed(ActionEvent e) {
						setCurrentOption((String) this.getValue(AbstractAction.NAME));
						optionHasChanged();
					}
				};
				options.add(action);
				if (i == 0) {
					defaultOption = action;
					currentOption = OPTION_SOLDE;
				}
			}
			for (int i = 0; i < optionsEcritureString.size(); i++) {
				final Action action = new AbstractAction((String) optionsEcritureString.get(i)) {
					public void actionPerformed(ActionEvent e) {
						//                        optionHasChanged((String) this.getValue(AbstractAction.NAME));
						currentOptionEcriture = (String) this.getValue(AbstractAction.NAME);
					}
				};
				optionsEcriture.add(action);
				if (i == 0) {
					defaultOptionEcriture = action;
					currentOptionEcriture = OPTION_ECRITURE_GROUPE;
				}
			}

			optionHasChanged();

		}

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionRemove() {
			return actionRemove;
		}

		public Action actionTraitement() {
			return actionTraitement;
		}

		public ArrayList getOptions() {
			return options;
		}

		public void optionHasChanged() {
			if (OPTION_MANUELLE.equals(currentOption)) {
				balanceEntreePanel.selectOption(OPTION_ECRITURE_PARCOMPTE);
				currentOptionEcriture = OPTION_ECRITURE_PARCOMPTE;
				for (int i = 0; i < optionsEcriture.size(); i++) {
					((Action) optionsEcriture.get(i)).setEnabled(false);
				}
			}
			else {
				for (int i = 0; i < optionsEcriture.size(); i++) {
					((Action) optionsEcriture.get(i)).setEnabled(true);
				}
			}
			if (gestionsSacdModel.getSelectedEObject() != null) {
				comboboxModelGestion.setSelectedEObject(null);

			}

			//autoriser une ecriture par detail seulement si l'option detaillee est selectionnee + non consolidé
			//			((Action) optionsEcriture.get(2)).setEnabled(OPTION_DETAILLEE.equals(currentOption) && comboboxModelGestion.getSelectedEObject() != null);
			((Action) optionsEcriture.get(2)).setEnabled(OPTION_DETAILLEE.equals(currentOption) && currentGestion() != null);

			if (OPTION_ECRITURE_PARDETAIL.equals(currentOptionEcriture) && !OPTION_DETAILLEE.equals(currentOption)) {
				balanceEntreePanel.selectOption(OPTION_ECRITURE_PARCOMPTE);
				currentOptionEcriture = OPTION_ECRITURE_PARCOMPTE;
			}
		}

		public IZKarukeraTablePanelListener getSelectListPanelListener() {
			return selectListPanelListener;
		}

		public IZKarukeraTablePanelListener getDispoListPanelListener() {
			return dispoListPanelListener;
		}

		public final void onFilter() {
			lefiltre = balanceEntreePanel.getDispoListPanel().getTxtFilter().getText();
			try {
				setWaitCursor(true);
				balanceEntreePanel.getDispoListPanel().updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				//balanceEntreePanel.getDispoListPanel().getTxtFilter().setText(null);
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		public DocumentListener filterDocumentListener() {
			return filterDocumentListener;
		}

		private final class FilterDocumentListener implements DocumentListener {

			public void changedUpdate(DocumentEvent e) {
				onFilter();
			}

			public void insertUpdate(DocumentEvent e) {
				onFilter();
			}

			public void removeUpdate(DocumentEvent e) {
				onFilter();
			}

		}

		private final class CheckBoxSoldeListener implements ActionListener {

			public void actionPerformed(ActionEvent e) {
				onFilter();
			}

		}

		public final String getLefiltre() {
			return lefiltre;
		}

		public Action getDefaultOption() {
			return defaultOption;
		}

		public final String getCurrentOption() {
			return currentOption;
		}

		public final void setCurrentOption(String currentOption) {
			this.currentOption = currentOption;
		}

		public ZEOComboBoxModel getComboCodeGestionModel() {
			return comboboxModelGestion;
		}

		public ActionListener getComboGestionListener() {
			return comboGestionListener;
		}

		public ArrayList getOptionsEcriture() {
			return optionsEcriture;
		}

		public Action getDefaultOptionEcriture() {
			return defaultOptionEcriture;
		}

		public ActionListener getCheckSoldeListener() {
			return checkBoxSoldeListener;
		}

		public ActionListener getCodeGestionSacdListener() {
			return comboSacdListener;
		}

		public ZEOComboBoxModel getCodeGestionSacdModel() {
			return gestionsSacdModel;
		}

	}

	private final class ActionTraitement extends AbstractAction {
		public ActionTraitement() {
			this.putValue(AbstractAction.NAME, "Créer les écritures");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer les écritures de balance d'entrée pour les comptes sélectionnés");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RUN_32));
		}

		public void actionPerformed(ActionEvent e) {
			faitTraitement();
		}

	}

	private final class ActionClose extends AbstractAction {
		public ActionClose() {
			this.putValue(AbstractAction.NAME, "Fermer");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			getMyDialog().onCloseClick();
		}

	}

	private final class ActionAdd extends AbstractAction {
		public ActionAdd() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner le(s) compte(s)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
		}

		public void actionPerformed(ActionEvent e) {
			compteAdd();

		}
	}

	//    private final class ActionAddAll extends AbstractAction {
	//        public ActionAddAll() {
	//            this.putValue(AbstractAction.NAME, ">>");
	//            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter tous les codes gestion pour l'exercice sélectionné");
	//        }
	//        public void actionPerformed(ActionEvent e) {
	//            codeGestionAddAll();
	//        }
	//    }

	private final class ActionRemove extends AbstractAction {
		public ActionRemove() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Déselctionner le(s) compte(s)");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
		}

		public void actionPerformed(ActionEvent e) {
			compteRemove();
		}
	}

	//    private final class ActionRemoveAll extends AbstractAction {
	//        public ActionRemoveAll() {
	//            this.putValue(AbstractAction.NAME, "<<");
	//            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retirer tous les codes gestion pour l'exercice sélectionné");
	//        }
	//        public void actionPerformed(ActionEvent e) {
	//            codeGestionRemoveAll();
	//        }
	//    }

	public static class DispoRender extends ZEOTableCellRenderer {
		public final static Color colNegatif = Color.decode("#FF6464");

		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			final Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			final int mdlRow = ((ZEOTable) table).getRowIndexInModel(row);
			final NSKeyValueCoding obj = (NSKeyValueCoding) ((ZEOTable) table).getDataModel().getMyDg().displayedObjects().objectAtIndex(mdlRow);

			boolean negatif = false;
			if ((obj.valueForKey("DEBITS") != null && ((Number) obj.valueForKey("DEBITS")).doubleValue() < 0) || (obj.valueForKey("CREDITS") != null && ((Number) obj.valueForKey("CREDITS")).doubleValue() < 0)) {
				negatif = true;
			}

			if (isSelected) {
				comp.setBackground(table.getSelectionBackground());
				comp.setForeground(table.getSelectionForeground());
			}
			else {
				if (negatif) {
					comp.setBackground(colNegatif);
					comp.setForeground(table.getForeground());
				}
				else {
					comp.setBackground(table.getBackground());
					comp.setForeground(table.getForeground());
				}
			}
			return comp;
		}

	}

	private final class PlancomptableDispoListPanelListener implements IZKarukeraTablePanelListener {

		public void selectionChanged() {

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return dispoRender;
		}

		public NSArray getData() throws Exception {
			return getFilteredPlancomptableDispos();
		}

		public void onDbClick() {
			compteAdd();
		}

	}

	private final class PlancomptableSelectListPanelListener implements IZKarukeraTablePanelListener {

		public void selectionChanged() {

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return dispoRender;
		}

		public NSArray getData() throws Exception {
			return getPlanComptableSelect();
		}

		public void onDbClick() {
			compteRemove();

		}

	}

	public final NSArray getFilteredPlancomptableDispos() {
		String filtre = balanceEntreePanelModel.getLefiltre();
		//        final NSMutableArray lesquals = new NSMutableArray();
		NSArray res = getPlanComptableDispos();
		final NSMutableArray lesquals = new NSMutableArray();
		if (filtre != null && filtre.length() > 0) {
			EOQualifier qual = null;
			filtre = filtre.trim();
			if (ZStringUtil.keepOnlyNumChars(filtre).equals(filtre)) {
				qual = EOQualifier.qualifierWithQualifierFormat("PCO_NUM like %@", new NSArray(filtre + "*"));
			}
			else {
				qual = EOQualifier.qualifierWithQualifierFormat("PCO_LIBELLE caseInsensitiveLike %@", new NSArray("*" + filtre + "*"));
			}
			lesquals.addObject(qual);
		}
		if (balanceEntreePanel.isFiltreSolde()) {
			lesquals.addObject(EOQualifier.qualifierWithQualifierFormat("CREDITS<>DEBITS", null));
		}

		if (lesquals.count() > 0) {
			res = EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(lesquals));
		}
		return res;
	}

	public final class ComboGestionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			try {
				setWaitCursor(true);
				updatePlanComptables();
				balanceEntreePanelModel.optionHasChanged();
				setWaitCursor(false);
			} catch (Exception e1) {
				setWaitCursor(false);
				showErrorDialog(e1);
			}
		}

	}

	//	public final class CodeGestionSacdListener implements ActionListener {
	//		public void actionPerformed(ActionEvent e) {
	//			_model.setGestionSacd((EOGestion) _model.getCodeGestionSacdModel().getSelectedEObject());
	//			_model.setGestion(null);
	//			_model.getCodeGestionModel().setSelectedEObject(myListener.getGestion());
	//		}
	//
	//	}
	public final class CodeGestionSacdListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			try {
				setWaitCursor(true);
				updatePlanComptables();
				balanceEntreePanelModel.optionHasChanged();
				setWaitCursor(false);
			} catch (Exception e1) {
				setWaitCursor(false);
				showErrorDialog(e1);
			}
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return balanceEntreePanel;
	}

	public String title() {
		return TITLE;
	}

	public final class SACDFormat extends Format {

		/**
		 * @see java.text.Format#parseObject(java.lang.String, java.text.ParsePosition)
		 */
		public Object parseObject(String source, ParsePosition pos) {

			return null;
		}

		/**
		 * @see java.text.Format#format(java.lang.Object, java.lang.StringBuffer, java.text.FieldPosition)
		 */
		public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
			if (obj instanceof EOGestion) {
				return new StringBuffer("SACD " + ((EOGestion) obj).gesCode());
			}
			else if (obj instanceof String) {
				return new StringBuffer("SACD " + (String) obj);
			}
			else {
				return new StringBuffer("");
			}
		}

	}

	public EOGestion currentGestion() {
		EOGestion res = null;
		if (gestionsSacdModel.getSelectedEObject() != null) {
			res = (EOGestion) gestionsSacdModel.getSelectedEObject();
		}
		else {
			res = (EOGestion) comboboxModelGestion.getSelectedEObject();
		}
		return res;
	}
}
