/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.chgtex.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


public class BalanceEntreePanel extends ZKarukeraPanel {
	private final IBalanceEntreePanelModel _model;
	private final PlancomptableDispoListPanel dispoListPanel;
	private final PlancomptableSelectListPanel selectListPanel;
	private JComboBox comboCodeGestions;
	protected JComboBox myCodeGestionSacd;

	private final ButtonGroup groupOptions = new ButtonGroup();
	private final ButtonGroup groupOptionsEcriture = new ButtonGroup();

	private final Map optionsButtonsMap = new HashMap();

	private JCheckBox checkBoxSolde;

	public BalanceEntreePanel(IBalanceEntreePanelModel model) {
		super();
		_model = model;

		dispoListPanel = new PlancomptableDispoListPanel(_model.getDispoListPanelListener());
		selectListPanel = new PlancomptableSelectListPanel(_model.getSelectListPanelListener());

	}

	public void initGUI() {
		setLayout(new BorderLayout());
		dispoListPanel.initGUI();
		selectListPanel.initGUI();

		dispoListPanel.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		selectListPanel.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		add(buildTopPanel(), BorderLayout.NORTH);
		add(buildWorkingPanel(), BorderLayout.CENTER);
		add(buildButtonPanel(), BorderLayout.SOUTH);
	}

	public void updateData() throws Exception {
		dispoListPanel.updateData();
		selectListPanel.updateData();
	}

	private final JPanel buildGestionPanel() {
		comboCodeGestions = new JComboBox(_model.getComboCodeGestionModel());
		comboCodeGestions.addActionListener(_model.getComboGestionListener());

		myCodeGestionSacd = new JComboBox(_model.getCodeGestionSacdModel());
		myCodeGestionSacd.addActionListener(_model.getCodeGestionSacdListener());
		myCodeGestionSacd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				comboCodeGestions.setEnabled(_model.getCodeGestionSacdModel().getSelectedEObject() == null);

			}
		});

		final JLabel label = new JLabel("Spécifiez éventuellement un code gestion pour la BE");
		final JLabel tooltip = ZTooltip
				.createTooltipLabel(
						"Méthode",
						"Vous avez deux méthodes pour le calcul de la BE <ul><li>Consolidée : les soldes des comptes sont calculés globalement et reportés sur l'exercice suivant sur l'agence</li><li>Par code gestion : les soldes des comptes sont calculés et reportés sur l'exercice suivant par code gestion</li></ul>Vous avez la possibilité d'utiliser une méthode pour certains comptes et une autre pour d'autres comptes.");

		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(ZKarukeraPanel.buildHorizontalPanelOfComponent(new Component[] {
				label, myCodeGestionSacd, comboCodeGestions, tooltip
		}), BorderLayout.WEST);
		panel.add(new JPanel(new BorderLayout()));
		panel.setBorder(BorderFactory.createEmptyBorder(4, 4, 8, 4));
		return ZUiUtil.encloseInPanelWithTitle("Méthode", null, ZConst.BG_COLOR_TITLE, panel, null, null);
	}

	private final JPanel buildDisposPanel() {
		final ArrayList list2 = new ArrayList();
		// list2.add(_model.actionAddAll());
		list2.add(_model.actionAdd());
		list2.add(_model.actionRemove());
		// list2.add(_model.actionRemoveAll());

		final ArrayList buttons = ZUiUtil.getButtonListFromActionList(list2);
		final Iterator iter = buttons.iterator();
		while (iter.hasNext()) {
			final JButton element = (JButton) iter.next();
			element.setHorizontalAlignment(JButton.CENTER);
		}

		final JPanel toolbar2 = new JPanel(new BorderLayout());
		toolbar2.setBorder(BorderFactory.createEmptyBorder(100, 4, 4, 4));
		toolbar2.setPreferredSize(new Dimension(80, 200));
		toolbar2.add(buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
		toolbar2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(ZUiUtil.encloseInPanelWithTitle("Comptes disponibles pour la BE", null, ZConst.BG_COLOR_TITLE, dispoListPanel, null, null), BorderLayout.CENTER);
		panel.add(toolbar2, BorderLayout.EAST);
		return panel;
	}

	private final JPanel buildWorkingPanel() {
		final JPanel panelDroit = new JPanel(new BorderLayout());
		panelDroit.add(ZUiUtil.encloseInPanelWithTitle("Comptes sélectionnés pour la BE", null, ZConst.BG_COLOR_TITLE, selectListPanel, null, null), BorderLayout.CENTER);
		panelDroit.add(buildOptionsPanel(), BorderLayout.SOUTH);

		final JSplitPane split1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, buildDisposPanel(), panelDroit);
		split1.setDividerLocation(0.5);
		split1.setResizeWeight(0.5);
		split1.setBorder(BorderFactory.createEmptyBorder());

		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildGestionPanel(), BorderLayout.NORTH);
		panel.add(split1, BorderLayout.CENTER);
		return panel;
	}

	private final JPanel buildOptionsPanel() {

		//Boutons radios pour les options

		final ArrayList buttons = new ArrayList();
		final Iterator iter = _model.getOptions().iterator();
		while (iter.hasNext()) {
			final Action element = (Action) iter.next();
			final JRadioButton radioButton = new JRadioButton(element);
			buttons.add(radioButton);
			groupOptions.add(radioButton);
			optionsButtonsMap.put(element.getValue(AbstractAction.NAME), radioButton);
			if (element.equals(_model.getDefaultOption())) {
				radioButton.setSelected(true);
			}
		}

		buttons.add(new JSeparator(SwingConstants.HORIZONTAL));

		final Iterator iterEcriture = _model.getOptionsEcriture().iterator();
		while (iterEcriture.hasNext()) {
			final Action element = (Action) iterEcriture.next();
			final JRadioButton radioButton = new JRadioButton(element);
			buttons.add(radioButton);
			groupOptionsEcriture.add(radioButton);
			optionsButtonsMap.put(element.getValue(AbstractAction.NAME), radioButton);
			if (element.equals(_model.getDefaultOptionEcriture())) {
				radioButton.setSelected(true);
			}
		}

		final JPanel panelOptions = new JPanel();
		final GridLayout thisLayout = new GridLayout(buttons.size(), 1);
		panelOptions.setLayout(thisLayout);

		final Iterator iterator = buttons.iterator();
		while (iterator.hasNext()) {
			final Component c = (Component) iterator.next();
			panelOptions.add(c);
		}

		panelOptions.setBorder(BorderFactory.createTitledBorder("Options"));

		//Boutons de traitement
		final JPanel panelButtons = ZKarukeraPanel.buildVerticalPanelOfButtonsFromActions(new Action[] {
				_model.actionTraitement()
		});
		panelButtons.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(panelOptions, BorderLayout.CENTER);
		panel.add(panelButtons, BorderLayout.SOUTH);

		return panel;
	}

	private final JPanel buildButtonPanel() {
		final JPanel bPanel = new JPanel(new BorderLayout());
		final ArrayList actionList = new ArrayList();
		actionList.add(_model.actionClose());
		final Box box = buildHorizontalButtonsFromActions(actionList);
		bPanel.add(new JSeparator(), BorderLayout.PAGE_START);
		bPanel.add(box, BorderLayout.LINE_END);
		return bPanel;
	}

	private final JPanel buildTopPanel() {
		final int annee = myApp.appUserInfo().getCurrentExercice().exeExercice().intValue();
		final int anneeold = myApp.appUserInfo().getCurrentExercice().exeExercice().intValue() - 1;
		final ZCommentPanel commentPanel = new ZCommentPanel("<html>Balance d'entrée <font color=#FF0000><b>" + annee + "</b></font> (transfert " + anneeold + " vers " + annee + ")</html>",
				"<html>Après avoir sélectionné l'éventuel code gestion sur lequel vous souhaitez travailler, basculez vers la droite les comptes pour lesquels vous voulez créer les écritures de BE.\n" +
						"Sélectionnez ensuite l'option pour la création des écritures et cliquez sur le bouton de création pour que les écritures soient réellement créées.</html>", null);

		return commentPanel;
	}

	public abstract class PlancomptableBEPanel extends ZKarukeraTablePanel {
		protected final String COL_PCONUM = "PCO_NUM";
		protected final String COL_PCOLIBELLE = "PCO_LIBELLE";
		protected final String COL_RESTE_EMARGER_DEBITS = "DEBITS";
		protected final String COL_RESTE_EMARGER_CREDITS = "CREDITS";

		final ZEOTableModelColumn pconum;
		final ZEOTableModelColumn pcolibelle;
		final ZEOTableModelColumn debits;
		final ZEOTableModelColumn credits;

		public PlancomptableBEPanel(ZKarukeraTablePanel.IZKarukeraTablePanelListener listener) {
			super(listener);

			pconum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "N° compte", 67);
			pconum.setAlignment(SwingConstants.LEFT);
			pconum.setColumnClass(String.class);

			pcolibelle = new ZEOTableModelColumn(myDisplayGroup, COL_PCOLIBELLE, "Libelle", 157);
			pcolibelle.setAlignment(SwingConstants.LEFT);
			pcolibelle.setColumnClass(String.class);

			debits = new ZEOTableModelColumn(myDisplayGroup, COL_RESTE_EMARGER_DEBITS, "Restes débits", 90);
			debits.setAlignment(SwingConstants.RIGHT);
			debits.setFormatDisplay(ZConst.FORMAT_DECIMAL);
			debits.setColumnClass(BigDecimal.class);

			credits = new ZEOTableModelColumn(myDisplayGroup, COL_RESTE_EMARGER_CREDITS, "Restes crédits", 90);
			credits.setAlignment(SwingConstants.RIGHT);
			credits.setFormatDisplay(ZConst.FORMAT_DECIMAL);
			credits.setColumnClass(BigDecimal.class);

			colsMap.clear();
			colsMap.put(COL_PCONUM, pconum);
			colsMap.put(COL_PCOLIBELLE, pcolibelle);
			colsMap.put(COL_RESTE_EMARGER_DEBITS, debits);
			colsMap.put(COL_RESTE_EMARGER_CREDITS, credits);

		}
	}

	public final class PlancomptableDispoListPanel extends PlancomptableBEPanel {
		private final JTextField txtFilter;

		public PlancomptableDispoListPanel(ZKarukeraTablePanel.IZKarukeraTablePanelListener listener) {
			super(listener);

			txtFilter = new JTextField();
			txtFilter.getDocument().addDocumentListener(_model.filterDocumentListener());

		}

		public void initGUI() {
			super.initGUI();
			txtFilter.setColumns(20);
			final JPanel panelFilter = new JPanel(new BorderLayout());
			final JLabel labelFilter = new JLabel("Filtre");

			checkBoxSolde = new JCheckBox("Solde <> 0");
			checkBoxSolde.setSelected(true);
			checkBoxSolde.addActionListener(_model.getCheckSoldeListener());

			panelFilter.add(ZKarukeraPanel.buildLine(new Component[] {
					checkBoxSolde, Box.createHorizontalStrut(20), labelFilter, Box.createHorizontalStrut(4), txtFilter
			}), BorderLayout.WEST);
			panelFilter.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
			panelFilter.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
			add(panelFilter, BorderLayout.NORTH);
		}

		public final JTextField getTxtFilter() {
			return txtFilter;
		}

	}

	public final class PlancomptableSelectListPanel extends PlancomptableBEPanel {
		protected final String COL_COMPTE_BE = "COMPTE_BE";

		public PlancomptableSelectListPanel(ZKarukeraTablePanel.IZKarukeraTablePanelListener listener) {
			super(listener);

			final ZEOTableModelColumn pconumBE = new ZEOTableModelColumn(myDisplayGroup, COL_COMPTE_BE, "N° compte be", 67);
			pconumBE.setAlignment(SwingConstants.LEFT);
			pconumBE.setColumnClass(String.class);

			colsMap.clear();
			colsMap.put(COL_PCONUM, pconum);
			colsMap.put(COL_PCOLIBELLE, pcolibelle);
			colsMap.put(COL_COMPTE_BE, pconumBE);
			colsMap.put(COL_RESTE_EMARGER_DEBITS, debits);
			colsMap.put(COL_RESTE_EMARGER_CREDITS, credits);
		}

	}

	public interface IBalanceEntreePanelModel {
		public Action actionAdd();

		public ActionListener getCodeGestionSacdListener();

		public ZEOComboBoxModel getCodeGestionSacdModel();

		public ZEOComboBoxModel getComboCodeGestionModel();

		public DocumentListener filterDocumentListener();

		public IZKarukeraTablePanelListener getSelectListPanelListener();

		public IZKarukeraTablePanelListener getDispoListPanelListener();

		public Action actionClose();

		public Action actionRemove();

		public Action actionTraitement();

		/** La liste des options possibles (un tableau d'objets Action ) */
		public ArrayList getOptions();

		public Action getDefaultOption();

		public ActionListener getComboGestionListener();

		public ArrayList getOptionsEcriture();

		public Action getDefaultOptionEcriture();

		public ActionListener getCheckSoldeListener();

	}

	public PlancomptableDispoListPanel getDispoListPanel() {
		return dispoListPanel;
	}

	public PlancomptableSelectListPanel getSelectListPanel() {
		return selectListPanel;
	}

	public final ButtonGroup getGroupOptionsEcriture() {
		return groupOptionsEcriture;
	}

	public void selectOption(String option) {
		((JRadioButton) optionsButtonsMap.get(option)).setSelected(true);
	}

	public final boolean isFiltreSolde() {
		return checkBoxSolde.isSelected();
	}

}
