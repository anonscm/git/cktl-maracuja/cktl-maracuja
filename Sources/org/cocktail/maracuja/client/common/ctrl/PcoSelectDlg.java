/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.common.ctrl;

import java.awt.Dialog;
import java.util.Vector;

import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class PcoSelectDlg  extends ZEOSelectionDialog  {

	public static PcoSelectDlg createPcoNumSelectionDialog(Dialog parent) {
		final EODisplayGroup dg=new EODisplayGroup();
		
		final Vector filterFields = new Vector(2,0);
		filterFields.add(EOPlanComptable.PCO_NUM_KEY);
//		filterFields.add(EOPlanComptable.PCO_LIBELLE_KEY);

		final Vector myColsTmp = new Vector(2,0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(dg, EOPlanComptable.PCO_NUM_KEY, "N°");
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(dg, EOPlanComptable.PCO_LIBELLE_KEY, "Libellé");

		myColsTmp.add(col1);
		myColsTmp.add(col2);

		PcoSelectDlg dialog = new PcoSelectDlg( parent, "Sélection d'un compte" ,dg,myColsTmp,"Veuillez sélectionner un compte dans la liste",filterFields);
		dialog.setFilterPrefix("");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" like %@");

        return dialog;		
	}
	
	public PcoSelectDlg(Dialog win, String title, Vector cols, String message, Vector attributesToFilter) {
		super(win, title, new EODisplayGroup(), cols, message, attributesToFilter);
	}
	
	public PcoSelectDlg(Dialog win, String title, EODisplayGroup dg, Vector cols, String message, Vector attributesToFilter) {
		super(win, title, dg, cols, message, attributesToFilter);
	}

	public EOPlanComptable getSelection() {
		EOPlanComptable res;
		if ((getSelectedEOObjects()==null) || (getSelectedEOObjects().count()==0)) {
		    res = null;
		}
		else {
		    res = ((EOPlanComptableExer)getSelectedEOObjects().objectAtIndex(0)).planComptable();
		}		
		return res;
	}

	
	public int open(NSArray values) {
		myTableModel.getMyDg().setObjectArray(values);
		return super.open();
	}
	
	
}
