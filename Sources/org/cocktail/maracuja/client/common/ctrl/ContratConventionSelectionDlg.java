/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.common.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Vector;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class ContratConventionSelectionDlg extends ZEOSelectionDialog {

	private static final long serialVersionUID = 1L;

	//	public static final String COL_EXEEXERCICE = EOAccordsContrat. "toExercice.exeExercice";
	//	public static final String COL_CONINDEX = EOAccordsContrat.CON_INDEX_KEY;
	//	public static final String COL_PCOLIBELLE = "planComptable.pcoLibelle";
	//	public static final String COL_GESCODE = "gestion.gesCode";
	//	public static final String COL_ECDLIBELLE = "ecdLibelle";
	//	public static final String COL_ECDDEBIT = "ecdDebit";
	//	public static final String COL_ECDCREDIT = "ecdCredit";
	//	public static final String COL_ECDRESTEEMARGER = "ecdResteEmarger";
	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 700);

	public static ContratConventionSelectionDlg createSelectionDialog(Dialog parent) {
		final EODisplayGroup dg = new EODisplayGroup();
		LinkedHashMap<String, ZEOTableModelColumn> colsMap = new LinkedHashMap<String, ZEOTableModelColumn>();

		ZEOTableModelColumn colExercice = new ZEOTableModelColumn(dg, EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, "Exercice", 29);
		colExercice.setAlignment(SwingConstants.LEFT);
		colExercice.setColumnClass(Integer.class);

		ZEOTableModelColumn colConIndex = new ZEOTableModelColumn(dg, EOAccordsContrat.CON_INDEX_KEY, "Index", 68);
		colConIndex.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colObjet = new ZEOTableModelColumn(dg, EOAccordsContrat.CON_OBJET_KEY, "Objet", 263);
		colObjet.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colPartenaire = new ZEOTableModelColumn(dg, EOAccordsContrat.TO_PERSONNE_PARTENAIRE_KEY + "." + EOPersonne.NOM_PRENOM_KEY, "Partenaire", 83);
		colPartenaire.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colService = new ZEOTableModelColumn(dg, EOAccordsContrat.TO_PERSONNE_SERVICE_KEY + "." + EOPersonne.NOM_PRENOM_KEY, "Service", 83);
		colService.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colDateCloture = new ZEOTableModelColumn(dg, EOAccordsContrat.CON_DATE_CLOTURE_KEY, "Clôturée", 90);
		colDateCloture.setAlignment(SwingConstants.LEFT);
		colDateCloture.setColumnClass(Date.class);
		colDateCloture.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		colsMap.clear();
		colsMap.put(colExercice.getAttributeName(), colExercice);
		colsMap.put(colConIndex.getAttributeName(), colConIndex);
		colsMap.put(colObjet.getAttributeName(), colObjet);
		colsMap.put(colPartenaire.getAttributeName(), colPartenaire);
		colsMap.put(colService.getAttributeName(), colService);
		colsMap.put(colDateCloture.getAttributeName(), colDateCloture);

		//
		//		LinkedHashMap colsNew = new LinkedHashMap();
		//		colsNew.put(COL_ECRNUMERO, ecrNumero);
		//		colsNew.put(COL_ECDINDEX, colEcdIndex);
		//		colsNew.put(COL_GESCODE, colsMap.get(COL_GESCODE));
		//		//	colsNew.put(COL_PCONUM, colsMap.get(COL_PCONUM));
		//		//colsNew.put(COL_PCOLIBELLE, colsMap.get(COL_PCOLIBELLE));
		//		colsNew.put(COL_ECDLIBELLE, colsMap.get(COL_ECDLIBELLE));
		//		colsNew.put(COL_ECDDEBIT, colsMap.get(COL_ECDDEBIT));
		//		colsNew.put(COL_ECDCREDIT, colsMap.get(COL_ECDCREDIT));
		//		colsNew.put(COL_ECDRESTEEMARGER, colsMap.get(COL_ECDRESTEEMARGER));

		Vector<ZEOTableModelColumn> cols = new Vector<ZEOTableModelColumn>(colsMap.values());

		final Vector<String> filterFields = new Vector<String>(2, 0);
		filterFields.add(EOAccordsContrat.TO_PERSONNE_PARTENAIRE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY);
		filterFields.add(EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY);

		ContratConventionSelectionDlg dialog = new ContratConventionSelectionDlg(parent, "Sélection d'une ligne d'écriture pour émargement", dg, cols, "Veuillez sélectionner une ligne d'écriture dans la liste", filterFields);
		dialog.setFilterPrefix("*");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" caseInsensitiveLike %@");

		dialog.getContentPane().setPreferredSize(WINDOW_DIMENSION);
		dialog.setPreferredSize(WINDOW_DIMENSION);
		dialog.pack();
		dialog.validate();
		return dialog;
	}

	public ContratConventionSelectionDlg(Dialog win, String title, Vector<ZEOTableModelColumn> cols, String message, Vector attributesToFilter) {
		super(win, title, new EODisplayGroup(), cols, message, attributesToFilter);

	}

	public ContratConventionSelectionDlg(Dialog win, String title, EODisplayGroup dg, Vector<ZEOTableModelColumn> cols, String message, Vector attributesToFilter) {
		super(win, title, dg, cols, message, attributesToFilter);
	}

	public EOPlanComptable getSelection() {
		EOPlanComptable res;
		if ((getSelectedEOObjects() == null) || (getSelectedEOObjects().count() == 0)) {
			res = null;
		}
		else {
			res = ((EOPlanComptableExer) getSelectedEOObjects().objectAtIndex(0)).planComptable();
		}
		return res;
	}

	public int open(NSArray values) {
		myTableModel.getMyDg().setObjectArray(values);
		return super.open();
	}

}
