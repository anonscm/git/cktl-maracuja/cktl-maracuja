/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.common.ctrl;

import java.util.HashMap;

import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.metier.EOPlanComptable;


/**
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class ClassesComboBoxModel extends DefaultComboBoxModel {
    public static final String TOUTES="Toutes les classes";
    private static final String PREFIX = "Classe ";
    
    public static final String OPTIONCLASSE0=PREFIX + EOPlanComptable.CLASSE0;
    public static final String OPTIONCLASSE1=PREFIX + EOPlanComptable.CLASSE1;
    public static final String OPTIONCLASSE2=PREFIX + EOPlanComptable.CLASSE2;
    public static final String OPTIONCLASSE3=PREFIX + EOPlanComptable.CLASSE3;
    public static final String OPTIONCLASSE4=PREFIX + EOPlanComptable.CLASSE4;
    public static final String OPTIONCLASSE5=PREFIX + EOPlanComptable.CLASSE5;
    public static final String OPTIONCLASSE6=PREFIX + EOPlanComptable.CLASSE6;
    public static final String OPTIONCLASSE7=PREFIX + EOPlanComptable.CLASSE7;
    public static final String OPTIONCLASSE8=PREFIX + EOPlanComptable.CLASSE8;
    public static final String OPTIONCLASSE9=PREFIX + EOPlanComptable.CLASSE9;

    private final HashMap classesCompte = new HashMap(10);


    public ClassesComboBoxModel() {
        addElement(TOUTES);
        addElement(OPTIONCLASSE0);
        addElement(OPTIONCLASSE1);
        addElement(OPTIONCLASSE2);
        addElement(OPTIONCLASSE3);
        addElement(OPTIONCLASSE4);
        addElement(OPTIONCLASSE5);
        addElement(OPTIONCLASSE6);
        addElement(OPTIONCLASSE7);
        addElement(OPTIONCLASSE8);
        addElement(OPTIONCLASSE9);

        classesCompte.put(OPTIONCLASSE0,new Integer(0));
        classesCompte.put(OPTIONCLASSE1,new Integer(1));
        classesCompte.put(OPTIONCLASSE2,new Integer(2));
        classesCompte.put(OPTIONCLASSE3,new Integer(3));
        classesCompte.put(OPTIONCLASSE4,new Integer(4));
        classesCompte.put(OPTIONCLASSE5,new Integer(5));
        classesCompte.put(OPTIONCLASSE6,new Integer(6));
        classesCompte.put(OPTIONCLASSE7,new Integer(7));
        classesCompte.put(OPTIONCLASSE8,new Integer(8));
        classesCompte.put(OPTIONCLASSE9,new Integer(9));
    }



    public final Integer getSelectedClasse() {
        return (Integer) classesCompte.get(getSelectedItem());
    }

}
