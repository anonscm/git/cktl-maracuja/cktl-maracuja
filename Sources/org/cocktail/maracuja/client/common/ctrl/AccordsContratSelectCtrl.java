package org.cocktail.maracuja.client.common.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.AccordsContratsSelectPanel;
import org.cocktail.maracuja.client.common.ui.AccordsContratsSelectPanel.IAccordsContratsSelectPanelListener;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author rprin
 */
public class AccordsContratSelectCtrl extends CommonSrchCtrl {
	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 700);
	private static final String TITLE = "Recherche d'une convention";

	private AccordsContratsSelectPanel mainPanel;
	private HashMap<String, Object> filters = new HashMap<String, Object>();

	private MyAccordsContratsSelectPanelListener accordsContratsSelectPanelListener;
	private MyAccordsContratListPanelListener accordsContratListPanelListener;
	private Action actionOk;
	private Action actionCancel;

	private EOAccordsContrat selectedAccordsContrat = null;

	public AccordsContratSelectCtrl(EOEditingContext editingContext) {
		super(editingContext);

		actionOk = new AbstractAction("Ok") {
			public void actionPerformed(ActionEvent e) {
				onOkClick();
			}
		};
		actionOk.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));

		actionCancel = new AbstractAction("Annuler") {
			public void actionPerformed(ActionEvent e) {
				onCancelClick();
			}
		};
		actionCancel.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));

		accordsContratListPanelListener = new MyAccordsContratListPanelListener();
		accordsContratsSelectPanelListener = new MyAccordsContratsSelectPanelListener();
		mainPanel = new AccordsContratsSelectPanel(accordsContratsSelectPanelListener);
	}

	@Override
	protected NSMutableArray buildFilterQualifiers() throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOAccordsContrat.qualForExerciceEquals((Integer) filters.get(EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY)));
		quals.addObject(EOAccordsContrat.qualForIndexEquals((Integer) filters.get(EOAccordsContrat.CON_INDEX_KEY)));
		quals.addObject(EOAccordsContrat.qualForPartenaireLike((String) filters.get(EOAccordsContrat.TO_PERSONNE_PARTENAIRE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY)));
		quals.addObject(EOAccordsContrat.qualForServiceLike((String) filters.get(EOAccordsContrat.TO_PERSONNE_SERVICE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY)));
		quals.addObject(EOAccordsContrat.qualForObjetLike((String) filters.get(EOAccordsContrat.CON_OBJET_KEY)));

		return quals;
	}

	@Override
	protected NSArray getData() throws Exception {
		return null;
	}

	@Override
	public String title() {
		return TITLE;
	}

	@Override
	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	@Override
	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	private void onOkClick() {
		selectedAccordsContrat = (EOAccordsContrat) mainPanel.getListPanel().selectedObject();
		getMyDialog().onOkClick();
	}

	private void onCancelClick() {
		selectedAccordsContrat = null;
		getMyDialog().onCancelClick();
	}

	private final class MyAccordsContratsSelectPanelListener implements IAccordsContratsSelectPanelListener {

		public IZKarukeraTablePanelListener getAccordsContratListPanelListener() {
			return accordsContratListPanelListener;
		}

		public HashMap<String, Object> getFilters() {
			return filters;
		}

		public Action actionSrch() {
			return actionSrch;
		}

		public Action actionAnnuler() {
			return actionCancel;
		}

		public Action actionOk() {
			return actionOk;
		}

	}

	private final class MyAccordsContratListPanelListener implements IZKarukeraTablePanelListener {

		public void selectionChanged() {

		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public NSArray getData() throws Exception {
			return srchAccordsContrats();
		}

		public void onDbClick() {
			onOkClick();
		}

	}

	protected NSArray srchAccordsContrats() throws Exception {
		return EOsFinder.fetchArray(getEditingContext(), EOAccordsContrat.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers()), new NSArray(new Object[] {
				EOAccordsContrat.SORT_EXERCICE_ASC, EOAccordsContrat.SORT_CON_INDEX_ASC
		}), true);
	}

	public EOAccordsContrat getSelectedAccordsContrat() {
		return selectedAccordsContrat;
	}

	public HashMap<String, Object> getFilters() {
		return filters;
	}

	public void setFilters(HashMap<String, Object> filters) {
		this.filters = filters;
	}
}
