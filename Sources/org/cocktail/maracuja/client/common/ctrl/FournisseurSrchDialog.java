/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ctrl;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.zutil.client.wo.ZEOSrchDialog;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class FournisseurSrchDialog extends ZEOSrchDialog {
	private EOEditingContext editingContext;
	private EODisplayGroup myDg;
    private static final ApplicationClient myApp = (ApplicationClient)EOApplication.sharedApplication();
    
    /**
     * @param win
     * @param title
     * @param dg
     * @param cols
     * @param message
     * @param action
     * @param ec
     */
    public FournisseurSrchDialog(Dialog win,String title, EOEditingContext ec) {
        super(win, title);
		editingContext = ec;
		initDialog();
    }

    /**
     * @param win
     * @param title
     * @param dg
     * @param cols
     * @param message
     * @param action
     */
    public FournisseurSrchDialog(Frame win, String title, EOEditingContext ec) {
        super(win, title);
		editingContext = ec;
		initDialog();
    }






	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOSrchDialog#initDialog(com.webobjects.eointerface.EODisplayGroup, java.util.Vector, java.lang.String, javax.swing.AbstractAction)
	 */
	protected void initDialog() {
		myDg = new EODisplayGroup();
//		myDg.setDataSource(myApp.getDatasourceForEntity(editingContext, "Fournisseur"));

		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg,"fouCode","Code",10);
		col1.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg,"adrNom","Nom",30);
		col2.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg,"adrPrenom","Prénom",30);
		col3.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDg,"adrCp","CP",10);
		col4.setAlignment(SwingConstants.LEFT);
		ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDg,"adrVille","Ville",30);
		col5.setAlignment(SwingConstants.LEFT);

		Vector cols = new Vector(4,0);
		cols.add(col1);
		cols.add(col2);
		cols.add(col3);
		cols.add(col4);
		cols.add(col5);
		tooltipIcon = ZTooltip.getTooltip_SELECTFOURNIS();

		super.initDialog(myDg, cols, "Cherchez et sélectionnez un fournisseur", new ActionSrch());


//		final String s = "<html><p><b>Pour rechercher un fournisseur, vous pouvez indiquez :</b><ul><li>une partie de son nom</li><li>son code</li><li>une partie de sa ville</li><li>son code postal</li></ul></p></html>";
//		tooltipIcon.setToolTipText(s);
//		tooltipIcon.setIcon(ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16));

	}


	private void updateData() {
		try {
			NSArray res = EOsFinder.getFournisseurs(editingContext, getFilterText(), getFilterText(), getFilterText(), getFilterText());
			myDg.setObjectArray(res);

//			Indiquer au modele que le nombre d'enregistrements a changé
//			myTableModel.updateInnerRowCount();
//			myTableModel.fireTableDataChanged();
			myEOTable.updateData();
		}
		catch (Exception e) {
			myApp.showErrorDialog(e, this);
		}
	}





	private class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher un fournisseur");
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			updateData();
		}

	}


    public ImageIcon getImageIconCancel() {
        return ZIcon.getIconForName(ZIcon.ICON_CANCEL_16);
    }

    public ImageIcon getImageIconOk() {
        return ZIcon.getIconForName(ZIcon.ICON_OK_16);
    }


}
