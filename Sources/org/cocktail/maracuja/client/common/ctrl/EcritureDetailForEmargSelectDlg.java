/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.common.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Vector;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at univ-lr.fr>
 */

public class EcritureDetailForEmargSelectDlg extends ZEOSelectionDialog {
	public static final String COL_ECRNUMERO = "ecriture.ecrNumero";

	public static final String COL_ECDINDEX = "ecdIndex";
	public static final String COL_PCONUM = "planComptable.pcoNum";
	public static final String COL_PCOLIBELLE = "planComptable.pcoLibelle";
	public static final String COL_GESCODE = "gestion.gesCode";
	public static final String COL_ECDLIBELLE = "ecdLibelle";
	public static final String COL_ECDDEBIT = "ecdDebit";
	public static final String COL_ECDCREDIT = "ecdCredit";
	public static final String COL_ECDRESTEEMARGER = "ecdResteEmarger";
	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 700);

	public static EcritureDetailForEmargSelectDlg createSelectionDialog(Dialog parent) {
		final EODisplayGroup dg = new EODisplayGroup();
		HashMap colsMap = new LinkedHashMap();

		final Vector filterFields = new Vector(2, 0);
		filterFields.add(EOEcritureDetail.ECRITURE_KEY + "." + EOEcriture.ECR_NUMERO_KEY);
		filterFields.add(EOEcritureDetail.ECD_LIBELLE_KEY);
		//		filterFields.add(EOPlanComptable.PCO_LIBELLE_KEY);

		ZEOTableModelColumn colEcdIndex = new ZEOTableModelColumn(dg, COL_ECDINDEX, "#", 29);
		colEcdIndex.setAlignment(SwingConstants.LEFT);
		colEcdIndex.setColumnClass(Integer.class);

		ZEOTableModelColumn colEcdPlancomptablePcoNum = new ZEOTableModelColumn(dg, COL_PCONUM, "Imputation", 68);
		colEcdPlancomptablePcoNum.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdPlancomptablePcoLibelle = new ZEOTableModelColumn(dg, COL_PCOLIBELLE, "Libelle Imputation", 263);
		colEcdPlancomptablePcoLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdGestionGesCode = new ZEOTableModelColumn(dg, COL_GESCODE, "Code gestion", 83);
		colEcdGestionGesCode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colEcdLibelle = new ZEOTableModelColumn(dg, COL_ECDLIBELLE, "Libellé", 228);
		colEcdLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdDebit = new ZEOTableModelColumn(dg, COL_ECDDEBIT, "Débit", 90);
		colEcdDebit.setAlignment(SwingConstants.RIGHT);
		colEcdDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdCredit = new ZEOTableModelColumn(dg, COL_ECDCREDIT, "Crédit", 90);
		colEcdCredit.setAlignment(SwingConstants.RIGHT);
		colEcdCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdCredit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdResteEmarger = new ZEOTableModelColumn(dg, COL_ECDRESTEEMARGER, "Reste à émarger", 90);
		colEcdResteEmarger.setAlignment(SwingConstants.RIGHT);
		colEcdResteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdResteEmarger.setColumnClass(BigDecimal.class);

		final ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(dg, COL_ECRNUMERO, "N° Ecriture", 68);
		ecrNumero.setAlignment(SwingConstants.LEFT);

		colsMap.clear();
		colsMap.put(COL_ECDINDEX, colEcdIndex);
		colsMap.put(COL_GESCODE, colEcdGestionGesCode);
		//	colsMap.put(COL_PCONUM, colEcdPlancomptablePcoNum);
		colsMap.put(COL_PCOLIBELLE, colEcdPlancomptablePcoLibelle);
		colsMap.put(COL_ECDLIBELLE, colEcdLibelle);
		colsMap.put(COL_ECDDEBIT, colEcdDebit);
		colsMap.put(COL_ECDCREDIT, colEcdCredit);
		colsMap.put(COL_ECDRESTEEMARGER, colEcdResteEmarger);

		LinkedHashMap colsNew = new LinkedHashMap();
		colsNew.put(COL_ECRNUMERO, ecrNumero);
		colsNew.put(COL_ECDINDEX, colEcdIndex);
		colsNew.put(COL_GESCODE, colsMap.get(COL_GESCODE));
		//	colsNew.put(COL_PCONUM, colsMap.get(COL_PCONUM));
		//colsNew.put(COL_PCOLIBELLE, colsMap.get(COL_PCOLIBELLE));
		colsNew.put(COL_ECDLIBELLE, colsMap.get(COL_ECDLIBELLE));
		colsNew.put(COL_ECDDEBIT, colsMap.get(COL_ECDDEBIT));
		colsNew.put(COL_ECDCREDIT, colsMap.get(COL_ECDCREDIT));
		colsNew.put(COL_ECDRESTEEMARGER, colsMap.get(COL_ECDRESTEEMARGER));

		Vector cols = new Vector(colsNew.values());

		EcritureDetailForEmargSelectDlg dialog = new EcritureDetailForEmargSelectDlg(parent, "Sélection d'une ligne d'écriture pour émargement", dg, cols, "Veuillez sélectionner une ligne d'écriture dans la liste", filterFields);
		dialog.setFilterPrefix("*");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" caseInsensitiveLike %@");

		dialog.getContentPane().setPreferredSize(WINDOW_DIMENSION);
		dialog.setPreferredSize(WINDOW_DIMENSION);
		dialog.pack();
		dialog.validate();
		return dialog;
	}

	public EcritureDetailForEmargSelectDlg(Dialog win, String title, Vector cols, String message, Vector attributesToFilter) {
		super(win, title, new EODisplayGroup(), cols, message, attributesToFilter);

	}

	public EcritureDetailForEmargSelectDlg(Dialog win, String title, EODisplayGroup dg, Vector cols, String message, Vector attributesToFilter) {
		super(win, title, dg, cols, message, attributesToFilter);
	}

	public EOPlanComptable getSelection() {
		EOPlanComptable res;
		if ((getSelectedEOObjects() == null) || (getSelectedEOObjects().count() == 0)) {
			res = null;
		}
		else {
			res = ((EOPlanComptableExer) getSelectedEOObjects().objectAtIndex(0)).planComptable();
		}
		return res;
	}

	public int open(NSArray values) {
		myTableModel.getMyDg().setObjectArray(values);
		return super.open();
	}

}
