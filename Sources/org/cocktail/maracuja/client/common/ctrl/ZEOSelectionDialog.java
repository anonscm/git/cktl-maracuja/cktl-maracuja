/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ctrl;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Boite de dialogue affichant une liste sélectionnable d'objets metiers. Cette liste peut être filtrée et triée par l'utilisateur. Par defaut le
 * filtre est un like *filter*. Vous pouvez spécifier le prefixe et le suffixe à appliquer au filtre, ainsi que l'instruction de filtre. Par défaut,
 * les valeurs sont les suivantes:
 * <ul>
 * <li>filterPrefix="*"</li>
 * <li>filterSuffix="*"</li>
 * <li>filterInstruction=" caseInsensitiveLike %@"</li>
 * </ul>
 * Les formats des colonnes peuvent etre définis (utilisez un dictionaire de java.text.format) . <br>
 * Exemple à adapter suivant vos besoins:<br>
 * <code>
	private void selectionneAgent() {<br>
	<blockquote>
		ZEOSelectionDialog dialog = createAgentSelectionDialog(); <br>
		if (dialog!=null) { <br>
		<blockquote>
			dialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); <br>
			if (dialog.open() == ZEOSelectionDialog.MROK) { <br>
			<blockquote>
				NSArray selections = dialog.getSelectedEOObjects(); <br>
				if ((selections==null) || (selections.count()==0)) { <br>
				<blockquote>
					//affecteAgentJefy(null); <br>
					//... effectuez vos traitements d'affectation <br>
					 <br>
					 </blockquote>
				} <br>
				else { <br>
				<blockquote>
					//affecteAgentJefy((EOEnterpriseObject) selections.objectAtIndex(0)); <br>
					//... effectuez vos traitements d'affectation <br>
					 </blockquote>
				} <br>
				</blockquote>
			} <br>
			dialog.dispose(); <br>
			</blockquote>
		} <br>
		</blockquote>
	} <br>
 <br>
	private ZEOSelectionDialog createAgentSelectionDialog() { <br>
	<blockquote>
		Vector myCols = new Vector(3,0); <br>
		Vector myAttr = new Vector(3,0); <br>
		myCols.add("N° agent"); <br>
		myAttr.add("agtOrdre");		 <br>
		myCols.add("Nom"); <br>
		myAttr.add("agtNom"); <br>
		myCols.add("Prénom"); <br>
		myAttr.add("agtPrenom"); <br>
		 <br>
		Vector filterFields = new Vector(3,0); <br>
		filterFields.add("agtOrdre"); <br>
		filterFields.add("agtNom"); <br>
		filterFields.add("agtPrenom"); <br>
		 <br>
		 <br>
		EODisplayGroup myDisplayGroup=new EODisplayGroup(); <br>
		myDisplayGroup.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "AgentJefy")  ); <br>
		try { <br>
		<blockquote>
			//myDisplayGroup.setObjectArray( EOsFinder.getAllAgentJefy(getEditingContext()) ); <br>
			//... alimentez le displaygroup d'une façon ou d'une autre...
			ZEOSelectionDialog dialog = new ZEOSelectionDialog( (Dialog)getUpperWindow(), "Sélection d'un agent" ,myDisplayGroup,myAttr,myCols,"Veuillez sélectionner un agent dans la liste",filterFields); <br>
			return dialog; <br>
		</blockquote>
		} catch (Exception e) { <br>
		<blockquote>
			return null; <br>
		</blockquote>
		}		 <br>
	</blockquote>
	} <br>

 * </code>
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 * @see org.cocktail.zutil.client.wo.ZEOTable
 * @see org.cocktail.zutil.client.wo.ZEOTableModel
 */
public class ZEOSelectionDialog extends JDialog implements ZEOTable.ZEOTableListener {
	public static final int MROK = 1;
	public static final int MRCANCEL = 0;
	protected ApplicationClient myApp;
	protected int modalResult;
	//	private Window upperWindow;
	protected ZEOTableModel myTableModel;
	private ZEOTable myEOTable;
	private String topMessage;
	private Vector _attributesToFilter;
	private JTextField filterTextField = new JTextField();

	/**
	 * @return Returns the filterInstruction.
	 */
	public String getFilterInstruction() {
		return filterInstruction;
	}

	/**
	 * @param filterInstruction The filterInstruction to set.
	 */
	public void setFilterInstruction(String filterInstruction) {
		this.filterInstruction = filterInstruction;
	}

	/**
	 * @return Returns the filterPrefix.
	 */
	public String getFilterPrefix() {
		return filterPrefix;
	}

	/**
	 * @param filterPrefix The filterPrefix to set.
	 */
	public void setFilterPrefix(String filterPrefix) {
		this.filterPrefix = filterPrefix;
	}

	/**
	 * @return Returns the filterSuffix.
	 */
	public String getFilterSuffix() {
		return filterSuffix;
	}

	/**
	 * @param filterSuffix The filterSuffix to set.
	 */
	public void setFilterSuffix(String filterSuffix) {
		this.filterSuffix = filterSuffix;
	}

	private String filterPrefix = "*";
	private String filterSuffix = "*";
	private String filterInstruction = " caseInsensitiveLike %@";

	/**
	 * @param win Dialog parent.
	 * @param title Titre à afficher dans la barre de titre
	 * @param dg DisplayGroup contenant les objets métiers
	 * @param cols Vector contenant des objets ZEOTableModelColumn
	 * @param message Message texte à afficher en haut de la fenêtre de sélection
	 * @param attributesToFilter Contient les noms des attributs qui seront utilisés pour filtrer lza liste. (Si null, le champ "filtre" ne sera pas
	 *            affiché.
	 */
	public ZEOSelectionDialog(Dialog win, String title, EODisplayGroup dg, Vector cols, String message, Vector attributesToFilter) {
		super(win, title);
		_attributesToFilter = attributesToFilter;
		setModal(true);
		//		upperWindow = win;
		myApp = (ApplicationClient) EOApplication.sharedApplication();
		myTableModel = new ZEOTableModel(dg, cols);
		TableSorter myTableSorter = new TableSorter(myTableModel);

		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		topMessage = message;
		initGUI();
	}

	/**
	 * Idem autre constructeur sauf le premier parametre
	 * 
	 * @param win Frame parent
	 * @param title
	 * @param dg
	 * @param cols Vector contenant des objets ZEOTableModelColumn
	 * @param message
	 * @param attributesToFilter
	 */
	public ZEOSelectionDialog(Frame win, String title, EODisplayGroup dg, Vector cols, String message, Vector attributesToFilter) {
		super(win, title);
		_attributesToFilter = attributesToFilter;
		setModal(true);
		//		upperWindow = win;
		myApp = (ApplicationClient) EOApplication.sharedApplication();
		myTableModel = new ZEOTableModel(dg, cols);
		TableSorter myTableSorter = new TableSorter(myTableModel);
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		topMessage = message;
		initGUI();
	}

	private void initGUI() {
		JPanel mainPanel = new JPanel();
		Box topPanel = Box.createVerticalBox();
		JPanel bottomPanel = new JPanel();

		JScrollPane myJscrollPane = new JScrollPane(myEOTable);

		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

		Action actionOk = new AbstractAction("Ok") {
			public void actionPerformed(ActionEvent e) {
				onOkClick();
			}
		};
		actionOk.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));

		Action actionCancel = new AbstractAction("Annuler") {
			public void actionPerformed(ActionEvent e) {
				onCancelClick();
			}
		};
		actionCancel.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		//			actionCancel.putValue(AbstractAction.SMALL_ICON, (ImageIcon)myApp.resourceBundle.getObject("annuler16"));

		JButton btOk = new JButton(actionOk);
		JButton btCancel = new JButton(actionCancel);
		Dimension btSize = new Dimension(95, 24);
		btOk.setMinimumSize(btSize);
		btCancel.setMinimumSize(btSize);
		btOk.setPreferredSize(btSize);
		btCancel.setPreferredSize(btSize);
		bottomPanel.add(btOk);
		bottomPanel.add(btCancel);

		/////////////////
		if (!ZStringUtil.isEmpty(topMessage) || (_attributesToFilter != null)) {
			Box topBox = Box.createVerticalBox();

			if (!ZStringUtil.isEmpty(topMessage)) {
				Box box1 = Box.createHorizontalBox();
				box1.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
				JLabel msgLabel = new JLabel(topMessage);
				box1.add(msgLabel);
				topBox.add(box1);
			}

			if (_attributesToFilter != null) {
				Box box1 = Box.createHorizontalBox();
				box1.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
				filterTextField.getDocument().addDocumentListener(new ADocumentListener());
				filterTextField.setPreferredSize(new Dimension(100, 25));
				box1.add(new JLabel("Filtre : "));
				box1.add(filterTextField);
				box1.add(Box.createHorizontalGlue());
				topBox.add(box1);
			}
			topPanel.add(topBox);
		}

		///////////////////

		mainPanel.add(topPanel, BorderLayout.PAGE_START);
		mainPanel.add(myJscrollPane, BorderLayout.CENTER);
		mainPanel.add(bottomPanel, BorderLayout.PAGE_END);

		this.setContentPane(mainPanel);
		this.pack();
		this.validate();

	}

	/**
	 * Ouvre la fenetre en modal et renvoie le résultat (via getModalResult).
	 */
	public int open() {
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setModalResult(MRCANCEL);
		centerWindow();
		filter();
		setVisible(true);
		//en modal la fenetre reste active jusqu'au closeWindow...
		return getModalResult();
	}

	/**
	 * Action qui peut être associée à un bouton OK.
	 */
	private void onOkClick() {
		setModalResult(MROK);
		setVisible(false);
	}

	/**
	 * Action qui peut être associée à un bouton Annuler.
	 */
	private void onCancelClick() {
		setModalResult(MRCANCEL);
		setVisible(false);
	}

	//
	//	private void onCloseClick() {
	//		setModalResult(ZKarukeraEOModalDialogCtrl.MRCANCEL);
	//		hide();
	//	}

	/**
	 *
	 */
	public int getModalResult() {
		return modalResult;
	}

	/**
	 * @param i
	 */
	private void setModalResult(int i) {
		modalResult = i;
	}

	//	/**
	//	 * @param window
	//	 */
	//	public void setUpperWindow(Window window) {
	////		upperWindow = window;
	//	}

	public NSArray getSelectedEOObjects() {
		return myTableModel.getSelectedEOObjects();
	}

	public EOEnterpriseObject getSelectedEOObject() {
		if (getSelectedEOObjects().count() > 0) {
			return (EOEnterpriseObject) getSelectedEOObjects().objectAtIndex(0);
		}
		return null;
	}

	public void setSelectionMode(int selMode) {
		myEOTable.setSelectionMode(selMode);
	}

	public void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2), ((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

	}

	/**
	 * @see org.cocktail.zutil.client.wo.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
		onOkClick();

	}

	/**
	 * Met à jour le qualifier du displaygroup. Le qualifier est un EOOrQualifier de qualifiers du type "nomAttribut caseInsensitiveLike *filter*". Il
	 * y a autant de qualifier que d'attributs définis dans le tableau indiqués dans le constructeur.
	 * 
	 * @param filter Texte du filtre
	 */
	protected void updateFilter(String filter) {
		if (!ZStringUtil.isEmpty(filter)) {
			NSMutableArray lesQuals = new NSMutableArray();
			if (_attributesToFilter != null) {
				for (int i = 0; i < _attributesToFilter.size(); i++) {
					lesQuals.addObject(EOQualifier.qualifierWithQualifierFormat(_attributesToFilter.get(i).toString() + getFilterInstruction(), new NSArray(getFilterPrefix() + filter + getFilterSuffix())));
				}
			}
			EOQualifier qual = new EOOrQualifier(lesQuals);

			myTableModel.getMyDg().setQualifier(qual);
		}
		else {
			myTableModel.getMyDg().setQualifier(null);
		}
		myTableModel.getMyDg().updateDisplayedObjects();
		myEOTable.updateData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		return;

	}

	/**
	 * Met à jour le filtre sur le displaygroup. Appelle {@link ZEOSelectionDialog#updateFilter(String)}
	 */
	public void filter() {
		updateFilter(filterTextField.getText());
	}

	/**
	 * Permet de définir un listener sur le contenu du champ texte qui sert à filtrer la table. Dès que le contenu du champ change, on met à jour le
	 * filtre. Le comportement de cette classe est identique au comportement d'un EOPickTextAssociation.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	private class ADocumentListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			filter();
		}

		public void insertUpdate(DocumentEvent e) {
			filter();
		}

		public void removeUpdate(DocumentEvent e) {
			filter();
		}
	}

	/**
	 * @return Returns the filterTextField.
	 */
	public JTextField getFilterTextField() {
		return filterTextField;
	}

	/**
	 * @param filterTextField The filterTextField to set.
	 */
	public void setFilterTextField(JTextField filterTextField) {
		this.filterTextField = filterTextField;
	}

}
