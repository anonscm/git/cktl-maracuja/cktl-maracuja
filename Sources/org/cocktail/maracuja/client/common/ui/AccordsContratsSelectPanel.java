package org.cocktail.maracuja.client.common.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;

public class AccordsContratsSelectPanel extends ZKarukeraPanel {

	private static final long serialVersionUID = 1L;
	private IAccordsContratsSelectPanelListener myListener;

	private AccordsContratListPanel listPanel;

	private ZFormPanel filterExercice;
	private ZFormPanel filterIndex;
	private ZFormPanel filterPartenaire;
	private ZFormPanel filterService;
	private ZFormPanel filterObjet;

	public AccordsContratsSelectPanel(IAccordsContratsSelectPanelListener myListener) {
		super();
		this.myListener = myListener;
		listPanel = new AccordsContratListPanel(myListener.getAccordsContratListPanelListener());

		listPanel.initGUI();

		setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		add(ZKarukeraPanel.encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, buildTopPanel(), null, null), BorderLayout.NORTH);
		add(ZKarukeraPanel.encloseInPanelWithTitle("Liste des conventions", null, ZConst.BG_COLOR_TITLE, listPanel, null, null), BorderLayout.CENTER);
		add(ZKarukeraPanel.encloseInPanelWithTitle("", null, ZConst.BG_COLOR_TITLE, buildBottomPanel(), null, null), BorderLayout.SOUTH);

	}

	private final JPanel buildTopPanel() {
		//filterExercice = ZFormPanel.buildLabelField("Exercice", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY));
		filterExercice = ZFormPanel.buildLabelField("Exercice", new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.getFilters(), EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT));

		//filterIndex = ZFormPanel.buildLabelField("Index", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOAccordsContrat.CON_INDEX_KEY));
		filterIndex = ZFormPanel.buildLabelField("Index", new ZNumberField(new ZNumberField.IntegerFieldModel(myListener.getFilters(), EOAccordsContrat.CON_INDEX_KEY), new Format[] {
				ZConst.FORMAT_ENTIER_BRUT
		}, ZConst.FORMAT_ENTIER_BRUT));
		filterPartenaire = ZFormPanel.buildLabelField("Partenaire", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOAccordsContrat.TO_PERSONNE_PARTENAIRE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY));
		filterService = ZFormPanel.buildLabelField("Service", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOAccordsContrat.TO_PERSONNE_SERVICE_KEY + "." + EOPersonne.PERS_LIBELLE_KEY));
		filterObjet = ZFormPanel.buildLabelField("Objet", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOAccordsContrat.CON_OBJET_KEY));

		filterExercice.setDefaultAction(myListener.actionSrch());
		filterIndex.setDefaultAction(myListener.actionSrch());
		filterPartenaire.setDefaultAction(myListener.actionSrch());
		filterService.setDefaultAction(myListener.actionSrch());
		filterObjet.setDefaultAction(myListener.actionSrch());

		((ZTextField) filterExercice.getMyFields().get(0)).getMyTexfield().setColumns(4);
		((ZTextField) filterIndex.getMyFields().get(0)).getMyTexfield().setColumns(4);
		((ZTextField) filterPartenaire.getMyFields().get(0)).getMyTexfield().setColumns(12);
		((ZTextField) filterService.getMyFields().get(0)).getMyTexfield().setColumns(12);
		((ZTextField) filterObjet.getMyFields().get(0)).getMyTexfield().setColumns(12);

		JPanel p = new JPanel(new BorderLayout());
		final Component separator = Box.createRigidArea(new Dimension(4, 1));
		final ArrayList comps = new ArrayList();

		comps.add(ZUiUtil.buildBoxLine(new Component[] {
				filterExercice, separator, filterIndex, separator, filterPartenaire, separator, filterService, separator, filterObjet
		}));

		p.add(ZUiUtil.buildBoxLine(new Component[] {
				ZUiUtil.buildBoxColumn(comps)
		}), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		p.add(new JButton(myListener.actionSrch()), BorderLayout.EAST);
		return p;
	}

	private final JPanel buildBottomPanel() {
		JButton btOk = new JButton(myListener.actionOk());
		JButton btCancel = new JButton(myListener.actionAnnuler());
		Dimension btSize = new Dimension(95, 24);
		btOk.setMinimumSize(btSize);
		btCancel.setMinimumSize(btSize);
		btOk.setPreferredSize(btSize);
		btCancel.setPreferredSize(btSize);

		JPanel p = new JPanel();
		p.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		p.add(btOk);
		p.add(btCancel);
		return p;
	}

	@Override
	public void updateData() throws Exception {
		//super.updateData();
		filterExercice.updateData();
		filterIndex.updateData();
		filterPartenaire.updateData();
		filterService.updateData();
		filterObjet.updateData();
		listPanel.updateData();
	}

	//	public void initGUI() {
	//		listPanel.initGUI();
	//
	//		setLayout(new BorderLayout());
	//		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
	//		add(ZKarukeraPanel.encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, buildTopPanel(), null, null), BorderLayout.NORTH);
	//		add(ZKarukeraPanel.encloseInPanelWithTitle("Liste des conventions", null, ZConst.BG_COLOR_TITLE, listPanel, null, null), BorderLayout.CENTER);
	//		add(ZKarukeraPanel.encloseInPanelWithTitle("", null, ZConst.BG_COLOR_TITLE, buildBottomPanel(), null, null), BorderLayout.SOUTH);
	//		//        super.initGUI();
	//	}

	public interface IAccordsContratsSelectPanelListener {
		//	public void onMandatSelectionChanged();

		public IZKarukeraTablePanelListener getAccordsContratListPanelListener();

		public HashMap<String, Object> getFilters();

		public Action actionSrch();

		public Action actionAnnuler();

		public Action actionOk();

	}

	public AccordsContratListPanel getListPanel() {
		return listPanel;
	}

	@Override
	public void initGUI() {
		//obsolete

	}
}
