/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import java.math.BigDecimal;
import java.util.Date;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MandatListPanel extends ZKarukeraTablePanel {

	private static final long serialVersionUID = 1L;
	public static final String COL_BORNUM = "bordereau.borNum";
	public static final String COL_MAN_NUMERO = "manNumero";
	public static final String COL_MAN_DATE_REMISE = "manDateRemise";
	public static final String COL_UTILISATEUR = "utilisateur.nomAndPrenom";
	public static final String COL_GESTION = "gestion.gesCode";
	public static final String COL_FOURNISSEUR = "fournisseur.nomAndPrenomAndCode";
	public static final String COL_PCONUM = "planComptable.pcoNum";
	public static final String COL_ETAT = "manEtat";
	public static final String COL_MANTTC = "manTtc";

	public MandatListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);

		ZEOTableModelColumn bornm = new ZEOTableModelColumn(myDisplayGroup, COL_BORNUM, "Bordereau", 80);
		bornm.setAlignment(SwingConstants.CENTER);
		bornm.setColumnClass(Integer.class);

		ZEOTableModelColumn colmandat = new ZEOTableModelColumn(myDisplayGroup, COL_MAN_NUMERO, "N° mandat", 80);
		colmandat.setAlignment(SwingConstants.CENTER);
		colmandat.setColumnClass(Integer.class);

		ZEOTableModelColumn dateRemise = new ZEOTableModelColumn(myDisplayGroup, COL_MAN_DATE_REMISE, "Date remise", 80);
		dateRemise.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		dateRemise.setAlignment(SwingConstants.CENTER);
		dateRemise.setColumnClass(Date.class);

		ZEOTableModelColumn codeGestion = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code gestion", 80);
		codeGestion.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn fournisseur = new ZEOTableModelColumn(myDisplayGroup, COL_FOURNISSEUR, "Fournisseur", 200);
		fournisseur.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn pcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imputation", 80);
		pcoNum.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colEtat = new ZEOTableModelColumn(myDisplayGroup, COL_ETAT, "Etat", 80);
		colEtat.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn manTtc = new ZEOTableModelColumn(myDisplayGroup, COL_MANTTC, "Montant TTC", 90);
		manTtc.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		manTtc.setAlignment(SwingConstants.RIGHT);
		manTtc.setColumnClass(BigDecimal.class);

		colsMap.clear();
		colsMap.put(COL_GESTION, codeGestion);
		colsMap.put(COL_BORNUM, bornm);
		colsMap.put(COL_MAN_NUMERO, colmandat);
		colsMap.put(COL_FOURNISSEUR, fournisseur);
		colsMap.put(COL_MAN_DATE_REMISE, dateRemise);
		colsMap.put(COL_MANTTC, manTtc);

	}

	public NSArray selectedObjects() {
		return myDisplayGroup.selectedObjects();
	}

}
