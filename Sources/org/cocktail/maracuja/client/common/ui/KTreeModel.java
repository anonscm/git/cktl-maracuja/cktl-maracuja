/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.common.ui;

import java.util.Vector;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;



/**
 * Modele d'un JTree. Accepte des obets de type KTreeNode.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class KTreeModel implements TreeModel {
    private Vector myListeners = new Vector();
    private KTreeNode rootNode;
    
    

    /**
     * @param root Objet racine
     */
    public KTreeModel(KTreeNode root) {
        super();
        rootNode = root;
    }

    /**
     * Returns the root of the tree. Returns null only if the tree has no nodes. 
     * @see javax.swing.tree.TreeModel#getRoot()
     */
    public Object getRoot() {
        return rootNode;
    }

    /**
     * Returns the child of parent at index index in the parent's child array. 
     * parent must be a node previously obtained from this data source. 
     * This should not return null if index is a valid index for parent 
     * (that is index >= 0 && index &lt; getChildCount(parent)). 
     */
    public Object getChild(Object parent, int index) {
//      System.out.println("getChild(Object)");
        KTreeNode obj = (KTreeNode)parent;
        return obj.getChild(index);
    }


    /**
     * Returns the number of children of parent. Returns 0 if the node is a leaf or if it has no children. 
     * parent must be a node previously obtained from this data source. 
     * @see javax.swing.tree.TreeModel#getChildCount(java.lang.Object)
     */
    public int getChildCount(Object parent) {
        KTreeNode obj = (KTreeNode)parent;
        return obj.getChildCount();
    }


    
    /**
     * Returns true if node is a leaf. It is possible for this method to return false even 
     * if node has no children. 
     * A directory in a filesystem, for example, may contain no files; 
     * the node representing the directory is not a leaf, but it also has no children. 
     * @see javax.swing.tree.TreeModel#isLeaf(java.lang.Object)
     */
    public boolean isLeaf(Object arg0) {
        KTreeNode obj = (KTreeNode)arg0;
        return obj.isLeaf();
    }

    /**
     * Messaged when the user has altered the value for the item identified by path to newValue. If newValue signifies a truly new value the model should post a treeNodesChanged event. 
     * @see javax.swing.tree.TreeModel#valueForPathChanged(javax.swing.tree.TreePath, java.lang.Object)
     */
    public void valueForPathChanged(TreePath arg0, Object arg1) {
    }

    /**
     * Returns the index of child in parent. If parent is null or child is null, returns -1.
     * @see javax.swing.tree.TreeModel#getIndexOfChild(java.lang.Object, java.lang.Object)
     */
    public int getIndexOfChild(Object parent, Object child) {
        KTreeNode obj = (KTreeNode)parent;
        return obj.getIndexOfChild(obj);
    }
    
    

    /**
     * Adds a listener for the TreeModelEvent posted after the tree changes. 
     * @see javax.swing.tree.TreeModel#addTreeModelListener(javax.swing.event.TreeModelListener)
     */
    public void addTreeModelListener(TreeModelListener l) {
        myListeners.addElement(l);
    }

    /**
     * Removes a listener previously added with addTreeModelListener. 
     * @see javax.swing.tree.TreeModel#removeTreeModelListener(javax.swing.event.TreeModelListener)
     */
    public void removeTreeModelListener(TreeModelListener l) {
        myListeners.removeElement(l);
    }

    //  ////////////// Fire events //////////////////////////////////////////////

    /**
     * The only event raised by this model is TreeStructureChanged with the
     * root as path, i.e. the whole tree has changed.
     */
    protected void fireTreeStructureChanged(KTreeNode oldRoot) {
        int len = myListeners.size();
        TreeModelEvent e = new TreeModelEvent(this, new Object[] { oldRoot });
        for (int i = 0; i < len; i++) {
            ((TreeModelListener) myListeners.elementAt(i)).treeStructureChanged(e);
        }
    }


}
