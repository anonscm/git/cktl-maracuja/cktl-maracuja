/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PlanComptableListPanel extends ZKarukeraTablePanel {
    public static final String COL_PCONUM=EOPlanComptable.PCO_NUM_KEY;
    public static final String COL_PCOLIBELLE=EOPlanComptable.PCO_LIBELLE_KEY;
    public static final String COL_PCONATURE="pcoNature";
    public static final String COL_PCONIVEAU="pcoNiveau";
    public static final String COL_PCOBUDGETAIRE="pcoBudgetaire";
    public static final String COL_PCOEMARGEMENT="pcoEmargement";
    public static final String COL_PCOVALIDITE="pcoValidite";
    public static final String COL_PCOJBE="pcoJBe";
    public static final String COL_PCOJEXERCICE="pcoJExercice";
    public static final String COL_PCOFINEXERCICE="pcoJFinExercice";

    public PlanComptableListPanel(IZKarukeraTablePanelListener listener) {
        super(listener);


        final ZEOTableModelColumn pconum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "N° compte", 50);
        pconum.setAlignment(SwingConstants.LEFT);
        pconum.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcolibelle = new ZEOTableModelColumn(myDisplayGroup, COL_PCOLIBELLE, "Libelle", 200);
        pcolibelle.setAlignment(SwingConstants.LEFT);
        pcolibelle.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcoNature = new ZEOTableModelColumn(myDisplayGroup, COL_PCONATURE, "Nature", 20);
        pcoNature.setAlignment(SwingConstants.CENTER);
        pcoNature.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcoNiveau = new ZEOTableModelColumn(myDisplayGroup, COL_PCONIVEAU, "Niveau", 20);
        pcoNiveau.setAlignment(SwingConstants.CENTER);
        pcoNiveau.setColumnClass(Integer.class);
        
        final ZEOTableModelColumn pcoBudgetaire = new ZEOTableModelColumn(myDisplayGroup, COL_PCOBUDGETAIRE, "Budget", 20);
        pcoBudgetaire.setAlignment(SwingConstants.CENTER);
        pcoBudgetaire.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcoEmargement = new ZEOTableModelColumn(myDisplayGroup, COL_PCOEMARGEMENT, "Emargement", 20);
        pcoEmargement.setAlignment(SwingConstants.CENTER);
        pcoEmargement.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcoValidite = new ZEOTableModelColumn(myDisplayGroup, COL_PCOVALIDITE, "Etat", 20);
        pcoValidite.setAlignment(SwingConstants.CENTER);
        pcoValidite.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcoJBe = new ZEOTableModelColumn(myDisplayGroup, COL_PCOJBE, "Journal BE", 20);
        pcoJBe.setAlignment(SwingConstants.CENTER);
        pcoJBe.setColumnClass(String.class);
        
        final ZEOTableModelColumn pcoJExercice = new ZEOTableModelColumn(myDisplayGroup, COL_PCOFINEXERCICE, "Journal Fin Exercice", 20);
        pcoJExercice.setAlignment(SwingConstants.CENTER);
        pcoJExercice.setColumnClass(String.class);

        final ZEOTableModelColumn pcoJFinExercice = new ZEOTableModelColumn(myDisplayGroup, COL_PCOJEXERCICE, "Journal Exercice", 20);
        pcoJFinExercice.setAlignment(SwingConstants.CENTER);
        pcoJFinExercice.setColumnClass(String.class);
        

        
		colsMap.clear();
		colsMap.put(COL_PCONUM,pconum);
		colsMap.put(COL_PCOLIBELLE,pcolibelle);
		colsMap.put(COL_PCONATURE ,pcoNature);
		colsMap.put(COL_PCONIVEAU ,pcoNiveau);
		colsMap.put(COL_PCOBUDGETAIRE ,pcoBudgetaire);
		colsMap.put(COL_PCOEMARGEMENT,pcoEmargement);
		colsMap.put(COL_PCOVALIDITE,pcoValidite);


    }


    

    public NSArray selectedObjects() {
        return myDisplayGroup.selectedObjects();
    }





}
