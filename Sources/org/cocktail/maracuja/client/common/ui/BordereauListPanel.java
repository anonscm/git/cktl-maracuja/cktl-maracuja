/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import java.util.Date;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class BordereauListPanel extends ZKarukeraTablePanel {
	public static final String COL_BORNUM = "borNum";
	public static final String COL_borDateVisa = "borDateVisa";
	public static final String COL_BORLIBELLE = "bordereauInfo.borLibelle";
	public static final String COL_BORETAT = "borEtat";
	public static final String COL_UTILISATEUR = "utilisateur.nomAndPrenom";
	public static final String COL_GESTION = "gestion.gesCode";
	public static final String COL_MONTANT = EOBordereau.MONTANT_KEY;
	public static final String COL_NB = EOBordereau.NB_KEY;

	public ZEOTableModelColumn colMontant;
	public ZEOTableModelColumn colNb;
	public ZEOTableModelColumn gesCode;
	public ZEOTableModelColumn borNum;
	public ZEOTableModelColumn borDateVisa;
	public ZEOTableModelColumn borLibelle;
	public ZEOTableModelColumn borEtat;
	public ZEOTableModelColumn colUtilisateur;

	public BordereauListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);
		borNum = new ZEOTableModelColumn(myDisplayGroup, COL_BORNUM, "N° Bordereau", 90);
		borNum.setAlignment(SwingConstants.CENTER);
		borNum.setColumnClass(Integer.class);

		borDateVisa = new ZEOTableModelColumn(myDisplayGroup, COL_borDateVisa, "Date visa", 90);
		borDateVisa.setAlignment(SwingConstants.CENTER);
		borDateVisa.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		borDateVisa.setColumnClass(Date.class);

		borLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_BORLIBELLE, "Libellé", 490);
		borLibelle.setAlignment(SwingConstants.LEFT);

		borEtat = new ZEOTableModelColumn(myDisplayGroup, COL_BORETAT, "Etat", 90);
		borEtat.setAlignment(SwingConstants.CENTER);

		colUtilisateur = new ZEOTableModelColumn(myDisplayGroup, COL_UTILISATEUR, "Utilisateur", 130);
		colUtilisateur.setAlignment(SwingConstants.LEFT);

		gesCode = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code Gestion", 84);
		gesCode.setAlignment(SwingConstants.CENTER);

		colMontant = new ZEOTableModelColumn(myDisplayGroup, COL_MONTANT, "Montant", 84);
		colMontant.setAlignment(SwingConstants.RIGHT);
		colMontant.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);

		colNb = new ZEOTableModelColumn(myDisplayGroup, COL_NB, "Nombre", 50);
		colNb.setAlignment(SwingConstants.RIGHT);
		colNb.setFormatDisplay(ZConst.FORMAT_ENTIER);

		colsMap.clear();
		colsMap.put(COL_GESTION, gesCode);
		colsMap.put(COL_BORNUM, borNum);
		colsMap.put(COL_BORLIBELLE, borLibelle);
		colsMap.put(COL_BORETAT, borEtat);
		colsMap.put(COL_borDateVisa, borDateVisa);
		colsMap.put(COL_UTILISATEUR, colUtilisateur);
	}

}
