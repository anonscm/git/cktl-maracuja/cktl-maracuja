/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureDetailListPanel extends ZKarukeraTablePanel {

	public static final String COL_ECDINDEX = "ecdIndex";
	public static final String COL_PCONUM = "planComptable.pcoNum";
	public static final String COL_PCOLIBELLE = "planComptable.pcoLibelle";
	public static final String COL_GESCODE = "gestion.gesCode";
	public static final String COL_ECDLIBELLE = "ecdLibelle";
	public static final String COL_ECDDEBIT = "ecdDebit";
	public static final String COL_ECDCREDIT = "ecdCredit";
	public static final String COL_ECDRESTEEMARGER = "ecdResteEmarger";

	/**
	 * @param context
	 */
	public EcritureDetailListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);

		ZEOTableModelColumn colEcdIndex = new ZEOTableModelColumn(myDisplayGroup, COL_ECDINDEX, "#", 29);
		colEcdIndex.setAlignment(SwingConstants.LEFT);
		colEcdIndex.setColumnClass(Integer.class);

		ZEOTableModelColumn colEcdPlancomptablePcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imputation", 68);
		colEcdPlancomptablePcoNum.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdPlancomptablePcoLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_PCOLIBELLE, "Libelle Imputation", 263);
		colEcdPlancomptablePcoLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdGestionGesCode = new ZEOTableModelColumn(myDisplayGroup, COL_GESCODE, "Code gestion", 83);
		colEcdGestionGesCode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colEcdLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_ECDLIBELLE, "Libellé", 228);
		colEcdLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdDebit = new ZEOTableModelColumn(myDisplayGroup, COL_ECDDEBIT, "Débit", 90);
		colEcdDebit.setAlignment(SwingConstants.RIGHT);
		colEcdDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdCredit = new ZEOTableModelColumn(myDisplayGroup, COL_ECDCREDIT, "Crédit", 90);
		colEcdCredit.setAlignment(SwingConstants.RIGHT);
		colEcdCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdCredit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdResteEmarger = new ZEOTableModelColumn(myDisplayGroup, COL_ECDRESTEEMARGER, "Reste à émarger", 90);
		colEcdResteEmarger.setAlignment(SwingConstants.RIGHT);
		colEcdResteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdResteEmarger.setColumnClass(BigDecimal.class);

		colsMap.clear();
		colsMap.put(COL_ECDINDEX, colEcdIndex);
		colsMap.put(COL_GESCODE, colEcdGestionGesCode);
		colsMap.put(COL_PCONUM, colEcdPlancomptablePcoNum);
		colsMap.put(COL_PCOLIBELLE, colEcdPlancomptablePcoLibelle);
		colsMap.put(COL_ECDLIBELLE, colEcdLibelle);
		colsMap.put(COL_ECDDEBIT, colEcdDebit);
		colsMap.put(COL_ECDCREDIT, colEcdCredit);
		colsMap.put(COL_ECDRESTEEMARGER, colEcdResteEmarger);
	}

}
