/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZUIContainer;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * Panel qui affiche des filtres de recherche pour les mandats.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MandatFilterPanel extends ZUIContainer {
	//  private final Color BORDURE_COLOR=getBackground().brighter();

	private ZFormPanel borNumPanel;
	private ZFormPanel manNumeroPanel;
	private ZFormPanel codeGestion;

	protected IMandatFilterPanelListener myListener;

	//private ZLookupButton pcoSelectButton;

	private ZFormPanel pcoNum;

	/**
     * 
     */
	public MandatFilterPanel(IMandatFilterPanelListener listener) {
		super();
		setLayout(new BorderLayout());
		myListener = listener;
	}

	public void updateData() throws Exception {
		pcoNum.updateData();
	}

	/**
	 * @see org.cocktail.maracuja.client.ZUIContainer#initGUI()
	 */
	public void initGUI() {
		codeGestion = buildCodeGestionFields();
		borNumPanel = buildBorNumFields();
		manNumeroPanel = buildManNumeroFields();
		pcoNum = ZFormPanel.buildLabelField("Imputation", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOPlanComptable.PCO_NUM_KEY));

		codeGestion.setDefaultAction(myListener.actionSrch());
		borNumPanel.setDefaultAction(myListener.actionSrch());
		manNumeroPanel.setDefaultAction(myListener.actionSrch());
		pcoNum.setDefaultAction(myListener.actionSrch());

		setSimpleLineBorder(codeGestion);
		setSimpleLineBorder(borNumPanel);
		setSimpleLineBorder(manNumeroPanel);
		setSimpleLineBorder(pcoNum);

		final ArrayList comps = new ArrayList();
		comps.add(codeGestion);
		comps.add(borNumPanel);
		comps.add(manNumeroPanel);
		comps.add(buildLine(new Component[] {
				pcoNum
		}));

		add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
		//        add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		super.initGUI();
	}

	//    private final JPanel buildFilters() {
	//        return p;
	//    }

	/**
	 * @return
	 */
	private final ZFormPanel buildBorNumFields() {
		return ZFormPanel.buildFourchetteNumberFields("<= N° Bord. <=", new BordereauNumMinModel(), new BordereauNumMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER);
	}

	private final ZFormPanel buildCodeGestionFields() {
		return ZFormPanel.buildLabelField("Code gestion", new CodeGestionModel());
	}

	private final ZFormPanel buildManNumeroFields() {
		return ZFormPanel.buildFourchetteNumberFields("<= N° Mandat <=", new MandatNumMinModel(), new MandatNumMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER);
	}

	private final class CodeGestionModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get(EOGestion.GES_CODE_KEY);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put(EOGestion.GES_CODE_KEY, value);

		}

	}

	private final class BordereauNumMinModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get("borNumMin");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put("borNumMin", value);

		}

	}

	private final class BordereauNumMaxModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get("borNumMax");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put("borNumMax", value);

		}

	}

	private final class MandatNumMinModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get("manNumeroMin");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put("manNumeroMin", value);

		}

	}

	private final class MandatNumMaxModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.getFilters().get("manNumeroMax");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.getFilters().put("manNumeroMax", value);

		}

	}

	public interface IMandatFilterPanelListener {
		/**
		 * @return Les valeurs de chaque élément de filtr
		 */
		public HashMap getFilters();

		public Action actionSrch();
	}

}
