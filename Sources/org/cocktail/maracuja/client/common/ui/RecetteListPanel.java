/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;


import java.math.BigDecimal;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;



/**
 *
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecetteListPanel extends ZKarukeraTablePanel {

    public static final String COL_NUMERO="recNum";
    public static final String COL_DATE="recDate";
    public static final String COL_DEBITEUR="recDebiteur";
    public static final String COL_LIBELLE="recLibelle";
    public static final String COL_TTC="recMontTtc";

	/**
	 * @param context
	 *
	 */
	public RecetteListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);

        ZEOTableModelColumn colnum = new ZEOTableModelColumn(myDisplayGroup, COL_NUMERO, "N°", 80);
        colnum.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn recDate = new ZEOTableModelColumn(myDisplayGroup, COL_DATE, "Date", 80);
        recDate.setAlignment(SwingConstants.CENTER);
        recDate.setFormatDisplay(ZConst.FORMAT_DATESHORT);

        ZEOTableModelColumn debiteur = new ZEOTableModelColumn(myDisplayGroup, COL_DEBITEUR, "Débiteur", 300);
        debiteur.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn libelle = new ZEOTableModelColumn(myDisplayGroup, COL_LIBELLE, "Libellé", 300);
        libelle.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn recTtc = new ZEOTableModelColumn(myDisplayGroup, COL_TTC, "Montant TTC", 90);
        recTtc.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
        recTtc.setAlignment(SwingConstants.RIGHT);
        recTtc.setColumnClass(BigDecimal.class);


		colsMap.clear();
		colsMap.put(COL_NUMERO ,colnum);
		colsMap.put(COL_DATE ,recDate);
		colsMap.put(COL_DEBITEUR ,debiteur);
		colsMap.put(COL_LIBELLE ,libelle);
		colsMap.put(COL_TTC ,recTtc);
	}



}
