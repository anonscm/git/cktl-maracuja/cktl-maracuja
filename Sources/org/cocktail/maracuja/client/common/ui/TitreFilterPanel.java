/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZUIContainer;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * Panel qui affiche des filtres de recherche pour les titres.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TitreFilterPanel extends ZUIContainer {
//    private final Color BORDURE_COLOR=getBackground().brighter();
    
    private ZFormPanel borNumPanel;
    private ZFormPanel titNumeroPanel;
    private ZFormPanel codeGestion;  
    
    
    protected ITitreFilterPanelListener myListener;

    private ZFormPanel pcoNum;
    
    /**
     * 
     */
    public TitreFilterPanel(ITitreFilterPanelListener listener) {
        super();
        setLayout(new BorderLayout());
        myListener = listener;
    }


    
    /**
     * @see org.cocktail.maracuja.client.ZUIContainer#initGUI()
     */
    public void initGUI() {
        codeGestion = buildCodeGestionFields();
        borNumPanel = buildBorNumFields();
        titNumeroPanel = buildTitNumeroFields();
        pcoNum = ZFormPanel.buildLabelField("Compte", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOPlanComptable.PCO_NUM_KEY) );

        setSimpleLineBorder(codeGestion);
        setSimpleLineBorder(borNumPanel);  
        setSimpleLineBorder(titNumeroPanel);  
        setSimpleLineBorder(pcoNum);  

        codeGestion.setDefaultAction(myListener.actionSrch());
        borNumPanel.setDefaultAction(myListener.actionSrch());
        titNumeroPanel.setDefaultAction(myListener.actionSrch());
        pcoNum.setDefaultAction(myListener.actionSrch());        
        
//        codeGestion.setDefaultAction(action)
        
        final ArrayList comps = new ArrayList();
        comps.add(codeGestion);
        comps.add( borNumPanel);
        comps.add( titNumeroPanel);        
        comps.add( buildLine(new Component[]{pcoNum}));        
        
        add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
//        add(new JPanel(new BorderLayout()));
     
        super.initGUI();
    }
    
    

//    private final JPanel buildFilters() {
//        return p;
//    }
        
    
    
    /**
     * @return
     */
    private final ZFormPanel buildBorNumFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= N° Bord. <=", new BordereauNumMinModel(), new BordereauNumMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER );
    }
    private final ZFormPanel buildCodeGestionFields() {
        return ZFormPanel.buildLabelField("Code gestion", new CodeGestionModel() );
    }
    
    private final ZFormPanel buildTitNumeroFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= N° Titre <=", new TitreNumMinModel(), new TitreNumMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER );
    }
        
    

    
    private final class CodeGestionModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get(EOGestion.GES_CODE_KEY) ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put(EOGestion.GES_CODE_KEY,value);
            
        }
        
    }
    private final class BordereauNumMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("borNumMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("borNumMin",value);
            
        }
        
    }
    private final class BordereauNumMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("borNumMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("borNumMax",value);
            
        }
        
    }    
    
    private final class TitreNumMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("titNumeroMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("titNumeroMin",value);
            
        }
        
    }
    private final class TitreNumMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("titNumeroMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("titNumeroMax",value);
            
        }
        
    }    
    
        
    public interface ITitreFilterPanelListener {
        /**
         * @return Les valeurs de chaque élément de filtr 
         */
        public HashMap getFilters();

		public Action actionSrch();
    }
    
}
