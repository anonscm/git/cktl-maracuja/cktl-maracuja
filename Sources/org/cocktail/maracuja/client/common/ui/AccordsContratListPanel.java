/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;

import java.util.Date;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOPersonne;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;

/**
 * Affiche une liste de convention
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class AccordsContratListPanel extends ZKarukeraTablePanel {

	private static final long serialVersionUID = 1L;

	public AccordsContratListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);

		ZEOTableModelColumn colExercice = new ZEOTableModelColumn(myDisplayGroup, EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, "Exercice", 29);
		colExercice.setAlignment(SwingConstants.LEFT);
		colExercice.setColumnClass(Integer.class);

		ZEOTableModelColumn colConIndex = new ZEOTableModelColumn(myDisplayGroup, EOAccordsContrat.CON_INDEX_KEY, "Index", 68);
		colConIndex.setAlignment(SwingConstants.LEFT);
		colConIndex.setFormatDisplay(ZConst.FORMAT_CONVENTION_INDEX);

		ZEOTableModelColumn colObjet = new ZEOTableModelColumn(myDisplayGroup, EOAccordsContrat.CON_OBJET_KEY, "Objet", 263);
		colObjet.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colPartenaire = new ZEOTableModelColumn(myDisplayGroup, EOAccordsContrat.TO_PERSONNE_PARTENAIRE_KEY + "." + EOPersonne.NOM_PRENOM_KEY, "Partenaire", 83);
		colPartenaire.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colService = new ZEOTableModelColumn(myDisplayGroup, EOAccordsContrat.TO_PERSONNE_SERVICE_KEY + "." + EOPersonne.NOM_PRENOM_KEY, "Service", 83);
		colService.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colDateCloture = new ZEOTableModelColumn(myDisplayGroup, EOAccordsContrat.CON_DATE_CLOTURE_KEY, "Clôturée", 90);
		colDateCloture.setAlignment(SwingConstants.LEFT);
		colDateCloture.setColumnClass(Date.class);
		colDateCloture.setFormatDisplay(ZConst.FORMAT_DATESHORT);

		colsMap.clear();
		colsMap.put(colExercice.getAttributeName(), colExercice);
		colsMap.put(colConIndex.getAttributeName(), colConIndex);
		colsMap.put(colObjet.getAttributeName(), colObjet);
		colsMap.put(colPartenaire.getAttributeName(), colPartenaire);
		colsMap.put(colService.getAttributeName(), colService);
		colsMap.put(colDateCloture.getAttributeName(), colDateCloture);

	}

	public NSArray selectedObjects() {
		return myDisplayGroup.selectedObjects();
	}

}
