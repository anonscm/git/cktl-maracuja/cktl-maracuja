/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.common.ui;


import java.math.BigDecimal;
import java.util.Date;

import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;



/**
 *
 * Interface pour l'afichage d'une liste d'emargements.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EmargementListPanel extends ZKarukeraTablePanel {

    public static final String COL_EMANUMERO="emaNumero";
    public static final String COL_TYPEEMARGEMENT="typeEmargement.temLibelle";
    public static final String COL_EMADATE="emaDate";
    public static final String COL_UTILISATEUR="utilisateur.nomAndPrenom";
    public static final String COL_EMAMONTANT="emaMontant";

	/**
	 * @param context
	 *
	 */
	public EmargementListPanel(IZKarukeraTablePanelListener listener) {
		super(listener);

		ZEOTableModelColumn colEmaNumero = new ZEOTableModelColumn(myDisplayGroup,COL_EMANUMERO,"N° emargement",110);
		colEmaNumero.setAlignment(SwingConstants.LEFT);
		colEmaNumero.setColumnClass(Integer.class);

		ZEOTableModelColumn colTemLibelle = new ZEOTableModelColumn(myDisplayGroup,COL_TYPEEMARGEMENT,"Type",90);
		colTemLibelle.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colEmaDate = new ZEOTableModelColumn(myDisplayGroup,COL_EMADATE,"Date",90);
		colEmaDate.setAlignment(SwingConstants.CENTER);
		colEmaDate.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colEmaDate.setColumnClass(Date.class);

		ZEOTableModelColumn colUtilisateur = new ZEOTableModelColumn(myDisplayGroup,COL_UTILISATEUR,"Utilisateur",426);
		colUtilisateur.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEmaMontant = new ZEOTableModelColumn(myDisplayGroup,COL_EMAMONTANT,"Montant",125);
		colEmaMontant.setAlignment(SwingConstants.RIGHT);
		colEmaMontant.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEmaMontant.setColumnClass(BigDecimal.class);


		colsMap.clear();
		colsMap.put(COL_EMANUMERO,colEmaNumero);
		colsMap.put(COL_TYPEEMARGEMENT ,colTemLibelle);
		colsMap.put(COL_EMADATE ,colEmaDate);
		colsMap.put(COL_EMAMONTANT ,colEmaMontant);
		colsMap.put(COL_UTILISATEUR ,colUtilisateur);
	}



}
