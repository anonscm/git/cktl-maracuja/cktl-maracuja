/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.common.ui;

import java.awt.Color;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class KTreeCellRenderer extends DefaultTreeCellRenderer {
    public static final int ZLEFT=JTextField.LEFT;
    public static final int ZRIGHT=JTextField.RIGHT;
    public static final int ZCENTER=JTextField.CENTER;

    protected Component superComponent;
    


    public KTreeCellRenderer() {
        super();
    }
    
    
    
    
    

    /**
     * Crée un texfield 
     * @param libelle
     * @param columns
     * @param horizontalAlign
     * @param sel
     * @param BgColor Si null, la couleur de fond pour la non selection sera celle par défaut.
     * @return
     */
    public JTextField buildTextField(String libelle, int columns, int horizontalAlign, boolean sel, Color bgColor) {
        if (libelle==null) {
            libelle ="";
        }
        JTextField f = new JTextField(libelle);
//      System.out.println("libelle:"+libelle);
        f.setColumns(columns);
        if (libelle.length()>columns+3) {
            f.setText(libelle.substring(0, columns)+"...");
            setToolTipText(libelle);
        }
        else {
            setToolTipText(null);
        }
        f.setHorizontalAlignment(horizontalAlign);
//      f.setBorder(BorderFactory.createEmptyBorder());
        f.setBorder( BorderFactory.createEmptyBorder(0, 2, 0, 2));
        if (sel) {
            f.setBackground( getBackgroundSelectionColor() );
            f.setForeground( getTextSelectionColor());
        }
        else {
            if (bgColor!=null) {
                f.setBackground( bgColor ); 
            }
            f.setForeground( getTextNonSelectionColor());
        }
        return f;
    }
    

    
    

    
    
    
    

}
