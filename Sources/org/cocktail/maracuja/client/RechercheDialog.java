/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.ZNumberUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org Boite de dialogue qui gère les recherches.
 */
public class RechercheDialog extends ZKarukeraDialog {
	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 600);
	//    private RecherchePanel myRecherchePanel;

	private Object selectedObject;

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws HeadlessException
	 */
	public RechercheDialog(Frame owner, String title, boolean modal) throws HeadlessException {
		super(owner, title, modal);
		//        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		initGUI();
	}

	private void initGUI() {
		final RecherchePanel myRecherchePanel = new RecherchePanel(this);
		myRecherchePanel.initGUI();
		myRecherchePanel.setMyDialog(this);
		this.setContentPane(myRecherchePanel);
		this.pack();
	}

	/**
	 * @return L'objet sélectionné de l'onglet actif, ou null.
	 */
	public Object getSelectedObject() {
		return selectedObject;
	}

	protected class RecherchePanel extends ZKarukeraPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		//	    private ArrayList recherchePanels;
		private JTabbedPane onglets;
		private RechercheDialog myListener;

		/**
         *
         */
		public RecherchePanel(RechercheDialog listener) {
			super();
			setMyDialog(listener);
			myListener = listener;
		}

		public void initGUI() {
			this.setLayout(new BorderLayout());
			onglets = new JTabbedPane();

			RechBordereauOnglet myRechBordereauOnglet = new RechBordereauOnglet();
			myRechBordereauOnglet.setMyDialog(getMyDialog());
			myRechBordereauOnglet.initGUI();

			RechBordereauRejetOnglet myRechBordereauRejetOnglet = new RechBordereauRejetOnglet();
			myRechBordereauRejetOnglet.setMyDialog(getMyDialog());
			myRechBordereauRejetOnglet.initGUI();

			RechListeOperationExtourneOnglet myRechListeOperationExtourneOnglet = new RechListeOperationExtourneOnglet();
			myRechListeOperationExtourneOnglet.setMyDialog(getMyDialog());
			myRechListeOperationExtourneOnglet.initGUI();

			RechEcritureDetailOnglet myRechEcritureDetailOnglet = new RechEcritureDetailOnglet();
			myRechEcritureDetailOnglet.setMyDialog(getMyDialog());
			myRechEcritureDetailOnglet.initGUI();

			this.setPreferredSize(WINDOW_DIMENSION);
			//Créer le sliste des onglets
			onglets.addTab(myRechEcritureDetailOnglet.getOngletName(), myRechEcritureDetailOnglet);
			onglets.addTab(myRechBordereauOnglet.getOngletName(), myRechBordereauOnglet);
			onglets.addTab(myRechBordereauRejetOnglet.getOngletName(), myRechBordereauRejetOnglet);
			onglets.addTab(myRechListeOperationExtourneOnglet.getOngletName(), myRechListeOperationExtourneOnglet);

			this.add(onglets);
		}

		public ARechercheOnglet activeOnglet() {
			return (ARechercheOnglet) onglets.getSelectedComponent();
		}

		public void updateData() throws Exception {
			return;
		}

		/**
		 * @return Returns the myListener.
		 */
		public RechercheDialog getMyListener() {
			return myListener;
		}

		/**
		 * @param myListener The myListener to set.
		 */
		public void setMyListener(RechercheDialog myListener) {
			this.myListener = myListener;
		}
	}

	private abstract class ARechercheOnglet extends ZKarukeraPanel {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public abstract Object getSelectedObject();

		public abstract String getOngletName();

		public abstract void openSelectedObject();

		/**
		 * @return un panel qui construit la partie formulaire (sans le bouton rechercher)
		 */
		protected abstract JPanel buildFormPanel();

		protected abstract JPanel buildResultPanel();

		public void initGUI() {
			this.setLayout(new BorderLayout());
			setBorder(BorderFactory.createEmptyBorder(2, 2, 4, 2));
			JPanel topPanel = new JPanel(new BorderLayout());
			//	        topPanel.setBorder(BorderFactory.createTitledBorder("Critères de recherche"));
			topPanel.add(buildButtonPanel(), BorderLayout.EAST);
			topPanel.add(buildFormPanel(), BorderLayout.CENTER);

			this.add(ZKarukeraPanel.encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, topPanel, null, null), BorderLayout.PAGE_START);
			this.add(ZKarukeraPanel.encloseInPanelWithTitle("Résultats", null, ZConst.BG_COLOR_TITLE, buildResultPanel(), null, null), BorderLayout.CENTER);
		}

		private JPanel buildButtonPanel() {
			JPanel buttonPanel = new JPanel(new BorderLayout());
			JButton myButton = new JButton(new ActionSrch());
			myButton.setPreferredSize(new Dimension(50, 50));
			buttonPanel.add(myButton, BorderLayout.PAGE_START);
			buttonPanel.add(new JPanel(), BorderLayout.CENTER);
			return buttonPanel;
		}

		private final class ActionSrch extends AbstractAction {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public ActionSrch() {
				super();
				putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
				putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
			}

			/**
			 * Appelle updateData();
			 */
			public void actionPerformed(ActionEvent e) {
				try {
					updateData();
				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}
		}

	}

	private class RechListeOperationExtourneOnglet extends RechBordereauRejetOnglet implements ZEOTableListener {
		private static final long serialVersionUID = 1L;
		private final String ONGLET_TITLE = "Liste des opérations extournées";

		protected void initPopupMenu() {
			myPopupMenu = new JPopupMenu();
			myPopupMenu.add(myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_LISTE_OP_EXTOURNEES));
			myEOTable.setPopup(myPopupMenu);
		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setSelectedObject(myDg.selectedObject());
			if (selectedObject != null) {
				((ZActionCtrl.Action_IMPR_LISTE_OP_EXTOURNEES) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_LISTE_OP_EXTOURNEES)).setEnabled(true);
				((ZActionCtrl.Action_IMPR_LISTE_OP_EXTOURNEES) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_LISTE_OP_EXTOURNEES)).setBordereau((EOBordereau) selectedObject);
			}
			else {
				((ZActionCtrl.Action_IMPR_LISTE_OP_EXTOURNEES) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_LISTE_OP_EXTOURNEES)).setEnabled(false);
				((ZActionCtrl.Action_IMPR_LISTE_OP_EXTOURNEES) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_LISTE_OP_EXTOURNEES)).setBordereau(null);

			}

		}

		protected void initTableModel() {
			Vector myCols2 = new Vector(6, 0);
			ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, EOBordereau.BOR_NUM_KEY, "N°", 69);
			col1.setAlignment(SwingConstants.CENTER);
			col1.setColumnClass(Integer.class);

			ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg, "gestion.gesCode", "Code gestion", 81);
			col2.setAlignment(SwingConstants.CENTER);
			ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg, "typeBordereau.tboType", "Type", 79);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDg, "typeBordereau.tboLibelle", "Libellé", 372);
			ZEOTableModelColumn col51 = new ZEOTableModelColumn(myDg, EOBordereau.BOR_DATE_VISA_KEY, "Créé le", 372);
			col51.setAlignment(SwingConstants.CENTER);
			col51.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			col51.setColumnClass(Date.class);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDg, "utilisateur.nomAndPrenom", "Créé par", 244);

			myCols2.add(col1);
			myCols2.add(col2);
			myCols2.add(col3);
			myCols2.add(col5);
			myCols2.add(col51);
			myCols2.add(col6);

			myTableModel = new ZEOTableModel(myDg, myCols2);
			myTableSorter = new TableSorter(myTableModel);
		}

		public void updateData() throws Exception {
			try {
				setWaitCursor(true);
				ArrayList quals = new ArrayList();
				if (fieldGestion.getMyTexfield().getText() != null && fieldGestion.getMyTexfield().getText().length() > 0) {
					quals.add(EOQualifier.qualifierWithQualifierFormat("gesCode=%@", new NSArray(fieldGestion.getMyTexfield().getText())));
				}
				if (fieldBorNum.getMyTexfield().getText() != null && fieldBorNum.getMyTexfield().getText().length() > 0) {
					quals.add(EOQualifier.qualifierWithQualifierFormat("borNum=%@", new NSArray(new Integer(fieldBorNum.getMyTexfield().getText()))));
				}

				EOOrQualifier qual1 = new EOOrQualifier(new NSArray(quals.toArray()));
				EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[] {
						myApp.appUserInfo().getCurrentExercice()
				}));
				EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
						qual1,
						qual2,
						new EOKeyValueQualifier(EOBordereau.TYPE_BORDEREAU_KEY + "." + EOTypeBordereau.TBO_SOUS_TYPE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeBordereau.SOUS_TYPE_EXTOURNE),
						new EOKeyValueQualifier(EOBordereau.TYPE_BORDEREAU_KEY + "." + EOTypeBordereau.TBO_TYPE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeBordereau.TypeBordereauBTMEX),
						new EOKeyValueQualifier(EOBordereau.BOR_ETAT_KEY, EOQualifier.QualifierOperatorEqual, EOBordereau.BordereauVise)

				}));
				ZLogger.debug(qual);
				NSArray res = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, qual, new NSArray(new EOSortOrdering[] {
						EOBordereau.SORT_DATE_CREATION_DESC, EOBordereau.SORT_BOR_NUM_ASC, EOBordereau.SORT_GES_CODE_ASC
				}), true);
				myDg.setObjectArray(res);
				myEOTable.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		public String getOngletName() {
			return ONGLET_TITLE;
		}
	}

	private class RechBordereauOnglet extends ARechercheOnglet implements ZEOTableListener {
		private final String ONGLET_TITLE = "Bordereaux Dépenses";

		private ZEOTableModel myTableModel;
		private ZEOTable myEOTable;
		private TableSorter myTableSorter;

		private EODisplayGroup myDg;

		private ZLabelTextField fieldGestion;
		private ZLabelTextField fieldBorNum;

		//	    private JPopupMenu myPopupMenu;

		public void initGUI() {
			myDg = new EODisplayGroup();
			//            myDg.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "Bordereau"));
			super.initGUI();
		}

		public class FieldActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				try {
					updateData();
				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}
		}

		public void updateData() throws Exception {
			try {
				setWaitCursor(true);
				ArrayList quals = new ArrayList();
				if (fieldGestion.getMyTexfield().getText() != null && fieldGestion.getMyTexfield().getText().length() > 0) {
					quals.add(EOQualifier.qualifierWithQualifierFormat("gesCode=%@", new NSArray(fieldGestion.getMyTexfield().getText())));
				}
				if (fieldBorNum.getMyTexfield().getText() != null && fieldBorNum.getMyTexfield().getText().length() > 0) {
					quals.add(EOQualifier.qualifierWithQualifierFormat("borNum=%@", new NSArray(new Integer(fieldBorNum.getMyTexfield().getText()))));
				}
				EOOrQualifier qual1 = new EOOrQualifier(new NSArray(quals.toArray()));
				EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[] {
						myApp.appUserInfo().getCurrentExercice()
				}));
				EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
						qual1, qual2
				}));

				ZLogger.debug(qual);

				//                EOFetchSpecification spec = new EOFetchSpecification("Bordereau",qual,null,true,true,null );
				//        		spec.setRefreshesRefetchedObjects(true);
				//        		NSArray res = getEditingContext().objectsWithFetchSpecification(spec);
				NSArray res = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, qual, null, true);
				myDg.setObjectArray(res);

				//        		if (res.count()>0) {
				//        		    myDg.setSelectedObject(myDg.displayedObjects().objectAtIndex(0));
				//        		}

				//				Indiquer au modele que le nombre d'enregistrements a changé
				//                myTableModel.updateInnerRowCount();
				//                myTableModel.fireTableDataChanged();
				myEOTable.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		public String getOngletName() {
			return ONGLET_TITLE;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#buildFormPanel()
		 */
		protected JPanel buildFormPanel() {
			int labelWidth = 120;
			FieldActionListener al = new FieldActionListener();

			fieldBorNum = new ZLabelTextField("Numéro");
			fieldBorNum.getMyTexfield().setColumns(6);
			fieldBorNum.getMyTexfield().addActionListener(al);

			fieldGestion = new ZLabelTextField("Code gestion");
			fieldGestion.getMyTexfield().setColumns(4);
			fieldGestion.getMyTexfield().addActionListener(al);

			ArrayList myList = new ArrayList(2);
			myList.add(fieldBorNum);
			myList.add(fieldGestion);

			Box colonne1 = buildFormColumnOfZLabelField(myList, labelWidth);

			JPanel myPanel = new JPanel(new BorderLayout());
			myPanel.add(colonne1, BorderLayout.LINE_START);
			myPanel.add(new JPanel(), BorderLayout.CENTER);
			return myPanel;
		}

		private Box buildFormColumnOfZLabelField(ArrayList compList, int fixedlabelWidth) {
			Box myBox = Box.createVerticalBox();

			Iterator iter = compList.iterator();
			while (iter.hasNext()) {
				ZLabelTextField element = (ZLabelTextField) iter.next();

				//Fixer la largeur de la colonne des labels
				element.getMyLabel().setPreferredSize(new Dimension(fixedlabelWidth, 15));
				element.getMyLabel().setMinimumSize(element.getMyLabel().getPreferredSize());
				element.getMyLabel().setMaximumSize(element.getMyLabel().getPreferredSize());

				JPanel tmpBox = new JPanel(new BorderLayout());
				tmpBox.add(element, BorderLayout.LINE_START);
				tmpBox.add(new JPanel(), BorderLayout.CENTER);
				myBox.add(tmpBox);
			}
			myBox.add(Box.createGlue());
			return myBox;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#buildResultPanel()
		 */
		protected JPanel buildResultPanel() {
			JPanel myPanel = new JPanel(new BorderLayout());
			initTableModel();
			initTable();
			myPanel.add(new JScrollPane(myEOTable));
			return myPanel;
		}

		/**
		 * Initialise la table à afficher (le modele doit exister)
		 */
		private void initTable() {
			myEOTable = new ZEOTable(myTableSorter);
			//            myEOTable = new ZEOTable(myTableModel);
			myEOTable.addListener(this);
			myTableSorter.setTableHeader(myEOTable.getTableHeader());
		}

		/**
		 * Initialise le modeele le la table à afficher.
		 */
		private void initTableModel() {
			Vector myCols2 = new Vector(6, 0);
			ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, "borNum", "N°", 69);
			col1.setAlignment(SwingConstants.CENTER);
			col1.setColumnClass(Integer.class);

			ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg, "gestion.gesCode", "Code gestion", 81);
			col2.setAlignment(SwingConstants.CENTER);
			ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg, "typeBordereau.tboType", "Type", 79);
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDg, "typeBordereau.tboSousType", "Sous Type", 150);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDg, "typeBordereau.tboLibelle", "Libellé", 200);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDg, "utilisateurVisa.nomAndPrenom", "Visé par", 244);
			ZEOTableModelColumn colEtat = new ZEOTableModelColumn(myDg, "borEtat", "Etat", 100);

			myCols2.add(col1);
			myCols2.add(col2);
			myCols2.add(col3);
			myCols2.add(col4);
			myCols2.add(col5);
			myCols2.add(colEtat);
			myCols2.add(col6);

			myTableModel = new ZEOTableModel(myDg, myCols2);
			myTableSorter = new TableSorter(myTableModel);
		}

		/**
		 * On ferme la fenetre en simulant un OK.
		 * 
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			openSelectedObject();

		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setSelectedObject(myDg.selectedObject());
		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#getSelectedObject()
		 */
		public Object getSelectedObject() {
			if (myTableModel.getSelectedEOObjects() != null) {
				return myTableModel.getSelectedEOObjects().objectAtIndex(0);
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#openSelectedObject()
		 */
		public void openSelectedObject() {
			showinfoDialogFonctionNonImplementee();
		}

	}

	private class RechBordereauRejetOnglet extends ARechercheOnglet implements ZEOTableListener {
		private final String ONGLET_TITLE = "Bordereaux Rejet";

		protected ZEOTableModel myTableModel;
		protected ZEOTable myEOTable;
		protected TableSorter myTableSorter;

		protected EODisplayGroup myDg;

		protected ZLabelTextField fieldGestion;
		protected ZLabelTextField fieldBorNum;

		protected JPopupMenu myPopupMenu;

		public void initGUI() {
			myDg = new EODisplayGroup();
			myDg.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), "BordereauRejet"));
			super.initGUI();
			initPopupMenu();
		}

		public class FieldActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				try {
					updateData();
				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}
		}

		public void updateData() throws Exception {
			try {
				setWaitCursor(true);
				ArrayList quals = new ArrayList();
				if (fieldGestion.getMyTexfield().getText() != null && fieldGestion.getMyTexfield().getText().length() > 0) {
					quals.add(EOQualifier.qualifierWithQualifierFormat("gesCode=%@", new NSArray(fieldGestion.getMyTexfield().getText())));
				}
				if (fieldBorNum.getMyTexfield().getText() != null && fieldBorNum.getMyTexfield().getText().length() > 0) {
					quals.add(EOQualifier.qualifierWithQualifierFormat("brjNum=%@", new NSArray(new Integer(fieldBorNum.getMyTexfield().getText()))));
				}
				EOOrQualifier qual1 = new EOOrQualifier(new NSArray(quals.toArray()));
				EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[] {
						myApp.appUserInfo().getCurrentExercice()
				}));
				EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
						qual1, qual2
				}));

				EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("brjNum", EOSortOrdering.CompareDescending);

				NSArray res = ZFinder.fetchArray(getEditingContext(), "BordereauRejet", qual, new NSArray(sort1), true);
				myDg.setObjectArray(res);

				myEOTable.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		public String getOngletName() {
			return ONGLET_TITLE;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#buildFormPanel()
		 */
		protected JPanel buildFormPanel() {
			int labelWidth = 120;
			FieldActionListener al = new FieldActionListener();

			fieldBorNum = new ZLabelTextField("Numéro");
			fieldBorNum.getMyTexfield().setColumns(6);
			fieldBorNum.getMyTexfield().addActionListener(al);

			fieldGestion = new ZLabelTextField("Code gestion");
			fieldGestion.getMyTexfield().setColumns(4);
			fieldGestion.getMyTexfield().addActionListener(al);

			ArrayList myList = new ArrayList(2);
			myList.add(fieldBorNum);
			myList.add(fieldGestion);

			Box colonne1 = buildFormColumnOfZLabelField(myList, labelWidth);

			JPanel myPanel = new JPanel(new BorderLayout());
			myPanel.add(colonne1, BorderLayout.LINE_START);
			myPanel.add(new JPanel(), BorderLayout.CENTER);
			return myPanel;
		}

		private Box buildFormColumnOfZLabelField(ArrayList compList, int fixedlabelWidth) {
			Box myBox = Box.createVerticalBox();

			Iterator iter = compList.iterator();
			while (iter.hasNext()) {
				ZLabelTextField element = (ZLabelTextField) iter.next();

				//Fixer la largeur de la colonne des labels
				element.getMyLabel().setPreferredSize(new Dimension(fixedlabelWidth, 15));
				element.getMyLabel().setMinimumSize(element.getMyLabel().getPreferredSize());
				element.getMyLabel().setMaximumSize(element.getMyLabel().getPreferredSize());

				JPanel tmpBox = new JPanel(new BorderLayout());
				tmpBox.add(element, BorderLayout.LINE_START);
				tmpBox.add(new JPanel(), BorderLayout.CENTER);
				myBox.add(tmpBox);
			}
			myBox.add(Box.createGlue());
			return myBox;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#buildResultPanel()
		 */
		protected JPanel buildResultPanel() {
			JPanel myPanel = new JPanel(new BorderLayout());
			initTableModel();
			initTable();
			myPanel.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
			return myPanel;
		}

		/**
		 * Initialise la table à afficher (le modele doit exister)
		 */
		protected void initTable() {
			myEOTable = new ZEOTable(myTableSorter);
			//myEOTable = new ZEOTable(myTableModel);
			myEOTable.addListener(this);

			myTableSorter.setTableHeader(myEOTable.getTableHeader());

		}

		protected void initPopupMenu() {
			myPopupMenu = new JPopupMenu();
			myPopupMenu.add(myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTMNA));
			myPopupMenu.add(myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTTNA));

			myEOTable.setPopup(myPopupMenu);
		}

		/**
		 * Initialise le modeele le la table à afficher.
		 */
		protected void initTableModel() {
			Vector myCols2 = new Vector(6, 0);
			ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, "brjNum", "N°", 69);
			col1.setAlignment(SwingConstants.CENTER);
			col1.setColumnClass(Integer.class);

			ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg, "gestion.gesCode", "Code gestion", 81);
			col2.setAlignment(SwingConstants.CENTER);
			ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg, "typeBordereau.tboType", "Type", 79);
			ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDg, "typeBordereau.tboLibelle", "Libellé", 372);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDg, "utilisateur.nomAndPrenom", "Créé par", 244);

			myCols2.add(col1);
			myCols2.add(col2);
			myCols2.add(col3);
			myCols2.add(col5);
			myCols2.add(col6);

			myTableModel = new ZEOTableModel(myDg, myCols2);
			myTableSorter = new TableSorter(myTableModel);
		}

		/**
		 * On ferme la fenetre en simulant un OK.
		 * 
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			openSelectedObject();

		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			setSelectedObject(myDg.selectedObject());
			if (selectedObject != null) {

				if (EOTypeBordereau.TypeBordereauBTTNA.equals(((EOBordereauRejet) selectedObject).typeBordereau().tboType())) {
					((ZActionCtrl.ActionIMPR_BTMNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTMNA)).setEnabled(false);
					((ZActionCtrl.ActionIMPR_BTTNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTTNA)).setEnabled(true);
					((ZActionCtrl.ActionIMPR_BTMNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTMNA)).setBrj(null);
					((ZActionCtrl.ActionIMPR_BTTNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTTNA)).setBrj((EOBordereauRejet) selectedObject);
				}
				else {
					((ZActionCtrl.ActionIMPR_BTMNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTMNA)).setEnabled(true);
					((ZActionCtrl.ActionIMPR_BTTNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTTNA)).setEnabled(false);
					((ZActionCtrl.ActionIMPR_BTMNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTMNA)).setBrj((EOBordereauRejet) selectedObject);
					((ZActionCtrl.ActionIMPR_BTTNA) myApp.getMyActionsCtrl().getActionbyId(ZActionCtrl.ID_IMPR_BTTNA)).setBrj(null);
				}

			}

		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#getSelectedObject()
		 */
		public Object getSelectedObject() {
			if (myTableModel.getSelectedEOObjects() != null) {
				return myTableModel.getSelectedEOObjects().objectAtIndex(0);
			}
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#openSelectedObject()
		 */
		public void openSelectedObject() {
			// TODO implementer
			showinfoDialogFonctionNonImplementee();
		}

	}

	private final class RechEcritureDetailOnglet extends ARechercheOnglet implements ZEOTableListener, IEcritureDetailFilterPanelListener {
		private final String ONGLET_TITLE = "Détail Ecritures";

		private ZEOTableModel myTableModel;
		private ZEOTable myEOTable;
		private TableSorter myTableSorter;

		private EODisplayGroup myDg;
		private EcritureDetailFilterPanel filterPanel;
		private HashMap filters = new HashMap();

		public void initGUI() {
			myDg = new EODisplayGroup();
			//            myDg.setDataSource(myApp.getDatasourceForEntity(getEditingContext(), EOEcritureDetail.ENTITY_NAME));
			filterPanel = new EcritureDetailFilterPanel(this);
			filterPanel.initGUI();
			super.initGUI();
		}

		public class FieldActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				try {
					updateData();
				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}
		}

		protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {
			NSMutableArray quals = new NSMutableArray();

			quals.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.exercice=%@", new NSArray(myApp.appUserInfo().getCurrentExercice())));

			//Construire les qualifiers à partir des saisies utilisateur
			///Numero
			NSMutableArray qualsNum = new NSMutableArray();
			if (dicoFiltre.get("ecrNumeroMin") != null) {
				qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("ecrNumeroMin")).intValue()))));
			}
			if (dicoFiltre.get("ecrNumeroMax") != null) {
				qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.ecrNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("ecrNumeroMax")).intValue()))));
			}

			if (qualsNum.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsNum));
			}

			NSMutableArray qualsMontant = new NSMutableArray();
			if (dicoFiltre.get("ecdMontantMin") != null) {
				qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("ecdMontant>=%@", new NSArray(ZNumberUtil.arrondi2((Number) dicoFiltre.get("ecdMontantMin")))));
			}
			if (dicoFiltre.get("ecdMontantMax") != null) {
				qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("ecdMontant<=%@", new NSArray(ZNumberUtil.arrondi2((Number) dicoFiltre.get("ecdMontantMax")))));
			}

			if (qualsMontant.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsMontant));
			}

			NSMutableArray qualsResteEmarger = new NSMutableArray();
			if (dicoFiltre.get("ecdResteEmargerMin") != null) {
				//                BigDecimal min =  new BigDecimal(  ((Number)dicoFiltre.get("ecdResteEmargerMin")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP) ;
				qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat("ecdResteEmarger >= %@", new NSArray(new Object[] {
						ZNumberUtil.arrondi2((Number) dicoFiltre.get("ecdResteEmargerMin"))
				})));
			}
			if (dicoFiltre.get("ecdResteEmargerMax") != null) {
				//                BigDecimal max =  new BigDecimal(  ((Number)dicoFiltre.get("ecdResteEmargerMax")).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP) ;
				qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat("ecdResteEmarger <= %@", new NSArray(new Object[] {
						ZNumberUtil.arrondi2((Number) dicoFiltre.get("ecdResteEmargerMax"))
				})));
			}

			if (qualsResteEmarger.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsResteEmarger));
			}

			if (!ZStringUtil.isEmpty((String) dicoFiltre.get(EOGestion.GES_CODE_KEY))) {
				quals.addObject(EOQualifier.qualifierWithQualifierFormat("gesCode=%@", new NSArray(dicoFiltre.get(EOGestion.GES_CODE_KEY))));
			}
			if (dicoFiltre.get("typeJournal") != null) {
				quals.addObject(EOQualifier.qualifierWithQualifierFormat("ecriture.typeJournal=%@", new NSArray(dicoFiltre.get("typeJournal"))));
			}
			if (!ZStringUtil.isEmpty((String) dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))) {
				quals.addObject(EOQualifier.qualifierWithQualifierFormat("planComptable.pcoNum=%@", new NSArray(dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))));
			}

			if (qualsNum.count() > 0) {
				quals.addObject(new EOAndQualifier(qualsNum));
			}
			return quals;
		}

		public void updateData() throws Exception {
			try {
				setWaitCursor(true);
				EOQualifier qual = new EOAndQualifier(buildFilterQualifiers(filters));

				ZLogger.debug(qual);

				//                EOFetchSpecification spec = new EOFetchSpecification(EOEcritureDetail.ENTITY_NAME,qual,null,true,true,null );
				//        		spec.setRefreshesRefetchedObjects(true);
				//        		NSArray res = getEditingContext().objectsWithFetchSpecification(spec);

				//                EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("ecrNumero", EOSortOrdering.CompareDescending);
				NSArray res = EOsFinder.fetchArray(getEditingContext(), EOEcritureDetail.ENTITY_NAME, qual, new NSArray(), true);

				myDg.setObjectArray(res);

				myEOTable.updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		public String getOngletName() {
			return ONGLET_TITLE;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#buildFormPanel()
		 */
		protected JPanel buildFormPanel() {
			return filterPanel;
		}

		//        private Box buildFormColumnOfZLabelField(ArrayList compList, int fixedlabelWidth) {
		//            Box myBox= Box.createVerticalBox();
		//
		//            Iterator iter = compList.iterator();
		//            while (iter.hasNext()) {
		//                ZLabelTextField element = (ZLabelTextField) iter.next();
		//
		//                //Fixer la largeur de la colonne des labels
		//                element.getMyLabel().setPreferredSize(new Dimension(fixedlabelWidth,15));
		//                element.getMyLabel().setMinimumSize(element.getMyLabel().getPreferredSize());
		//                element.getMyLabel().setMaximumSize(element.getMyLabel().getPreferredSize());
		//
		//                JPanel tmpBox = new JPanel(new BorderLayout());
		//                tmpBox.add(element, BorderLayout.LINE_START);
		//                tmpBox.add(new JPanel(), BorderLayout.CENTER);
		//                myBox.add(tmpBox);
		//            }
		//            myBox.add(Box.createGlue());
		//            return myBox;
		//        }

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#buildResultPanel()
		 */
		protected JPanel buildResultPanel() {
			JPanel myPanel = new JPanel(new BorderLayout());
			initTableModel();
			initTable();
			myPanel.add(new JScrollPane(myEOTable));
			return myPanel;
		}

		/**
		 * Initialise la table à afficher (le modele doit exister)
		 */
		private void initTable() {
			myEOTable = new ZEOTable(myTableSorter);
			//            myEOTable = new ZEOTable(myTableModel);
			myEOTable.addListener(this);
			myTableSorter.setTableHeader(myEOTable.getTableHeader());
		}

		/**
		 * Initialise le modeele le la table à afficher.
		 */
		private void initTableModel() {
			Vector myCols2 = new Vector(6, 0);
			ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, "ecriture.ecrNumero", "N°", 60);
			col1.setColumnClass(Integer.class);
			col1.setAlignment(SwingConstants.LEFT);
			ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg, "ecriture.typeJournal.tjoLibelle", "Journal", 200);
			ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDg, "ecriture.ecrDateSaisie", "Date saisie", 75);
			col4.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			col4.setColumnClass(Date.class);
			ZEOTableModelColumn colPlanco = new ZEOTableModelColumn(myDg, "planComptable.pcoNum", "Imputation", 75);
			ZEOTableModelColumn colGestion = new ZEOTableModelColumn(myDg, "gestion.gesCode", "Code Gestion", 80);
			colGestion.setAlignment(SwingConstants.CENTER);
			//            ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDg, "origine.oriLibelle", "Origine",100);
			ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDg, "ecriture.utilisateur.nomAndPrenom", "Saisie par", 150);
			col6.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn col7 = new ZEOTableModelColumn(myDg, "ecdDebit", "Débit", 90);
			col7.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			col7.setAlignment(SwingConstants.RIGHT);
			col7.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn col8 = new ZEOTableModelColumn(myDg, "ecdCredit", "Crédit", 90);
			col8.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			col8.setAlignment(SwingConstants.RIGHT);
			col8.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn colReste = new ZEOTableModelColumn(myDg, "ecdResteEmarger", "Reste à Emarger", 100);
			colReste.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			colReste.setAlignment(SwingConstants.RIGHT);
			colReste.setColumnClass(BigDecimal.class);

			myCols2.add(col1);
			myCols2.add(col2);
			myCols2.add(colGestion);
			myCols2.add(colPlanco);
			//            myCols2.add(col3);
			myCols2.add(col4);
			//            myCols2.add(col5);
			myCols2.add(col7);
			myCols2.add(col8);
			myCols2.add(colReste);
			myCols2.add(col6);

			myTableModel = new ZEOTableModel(myDg, myCols2);
			myTableSorter = new TableSorter(myTableModel);
		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#getSelectedObject()
		 */
		public Object getSelectedObject() {
			if (myTableModel.getSelectedEOObjects() != null) {
				return myTableModel.getSelectedEOObjects().objectAtIndex(0);
			}
			return null;
		}

		/**
		 * On ferme la fenetre en simulant un OK.
		 * 
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
		 */
		public void onDbClick() {
			openSelectedObject();

		}

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
			//            ZLogger.debug("selection changee : "+myDg.selectedObject());
			setSelectedObject(myDg.selectedObject());
		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.ARechercheOnglet#openSelectedObject()
		 */
		public void openSelectedObject() {
			ZLogger.debug("Ouverture d'une ecriture");
			//Vérifier si l'utilisateur a les droits suffisants
			try {
				if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {

					EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
					//                        win.loadEcritures(new NSArray((EOEcriture)((EOEcritureDetail)myDg.selectedObject()).ecriture()));
					win.openDialog(RechercheDialog.this, new NSArray(((EOEcritureDetail) myDg.selectedObject()).ecriture()));
				}
				else {
					throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.RechercheDialog.IEcritureDetailFilterPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return filters;
		}

	}

	private final class EcritureDetailFilterPanel extends ZUIContainer {
		//	    private final Color BORDURE_COLOR=getBackground();

		private JPanel journalPanel;
		private ZFormPanel ecdNumeroPanel;
		private ZFormPanel codeGestion;
		private ZFormPanel pcoNumPanel;
		private ZFormPanel montantPanel;
		private ZFormPanel montantRestePanel;

		private ZEOComboBoxModel modelTypeJournal;

		private IEcritureDetailFilterPanelListener myListener;

		/**
	     *
	     */
		public EcritureDetailFilterPanel(IEcritureDetailFilterPanelListener listener) {
			super();
			myListener = listener;
		}

		/**
		 * @see org.cocktail.maracuja.client.ZUIContainer#initGUI()
		 */
		public void initGUI() {
			codeGestion = ZFormPanel.buildLabelField("Code gestion", new CodeGestionModel());
			ecdNumeroPanel = ZFormPanel.buildFourchetteNumberFields("<= N° Ecriture <=", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "ecrNumeroMin"), new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "ecrNumeroMax"), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER);
			journalPanel = buildJournalField();
			pcoNumPanel = ZFormPanel.buildLabelField("Imputation", new PcoNumModel());
			montantPanel = ZFormPanel
					.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "ecdMontantMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "ecdMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER);
			montantRestePanel = ZFormPanel.buildFourchetteNumberFields("<= Reste à émarger <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "ecdResteEmargerMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "ecdResteEmargerMax"), ZConst.DECIMAL_EDIT_FORMATS,
					ZConst.FORMAT_EDIT_NUMBER);
			((ZTextField) montantPanel.getMyFields().get(0)).getMyTexfield().setColumns(10);
			((ZTextField) montantPanel.getMyFields().get(1)).getMyTexfield().setColumns(10);
			((ZTextField) montantRestePanel.getMyFields().get(0)).getMyTexfield().setColumns(10);
			((ZTextField) montantRestePanel.getMyFields().get(1)).getMyTexfield().setColumns(10);

			//	        JPanel p = new JPanel(new BorderLayout());
			Component[] comps = new Component[4];
			comps[0] = codeGestion;
			comps[1] = pcoNumPanel;
			comps[2] = ecdNumeroPanel;
			comps[3] = journalPanel;

			Box b = Box.createVerticalBox();
			b.add(ZKarukeraPanel.buildLine(comps));
			b.add(ZKarukeraPanel.buildLine(new Component[] {
					montantPanel, montantRestePanel
			}));

			add(b, BorderLayout.WEST);
			add(new JPanel(new BorderLayout()));

			super.initGUI();
		}

		private final JPanel buildJournalField() {
			JPanel p = new JPanel();
			NSArray typeJournals = EOsFinder.getAllTypeJournal(getEditingContext());
			try {
				modelTypeJournal = new ZEOComboBoxModel(typeJournals, "tjoLibelle", "", null);
				JComboBox box = new JComboBox(modelTypeJournal);

				box.addActionListener(new TypeJournalListener());

				JLabel l = new JLabel("Type de journal");
				p.add(l);
				p.add(box);
				//                p.setBorder(BorderFactory.createLineBorder(bordure));

				return p;
			} catch (Exception e) {
				showErrorDialog(e);
				return new JPanel();
			}
		}

		private final class CodeGestionModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get(EOGestion.GES_CODE_KEY);
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put(EOGestion.GES_CODE_KEY, value);

			}

		}

		private final class PcoNumModel implements ZTextField.IZTextFieldModel {

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
			 */
			public Object getValue() {
				return myListener.getFilters().get(EOPlanComptable.PCO_NUM_KEY);
			}

			/**
			 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
			 */
			public void setValue(Object value) {
				myListener.getFilters().put(EOPlanComptable.PCO_NUM_KEY, value);

			}

		}

		private final class TypeJournalListener implements ActionListener {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				myListener.getFilters().put("typeJournal", modelTypeJournal.getSelectedEObject());

			}

		}

	}

	public interface IEcritureDetailFilterPanelListener {
		/**
		 * @return Les valeurs de chaque élément de filtr
		 */
		public HashMap getFilters();
	}

	/**
	 * @param selectedObject The selectedObject to set.
	 */
	public void setSelectedObject(Object obj) {
		this.selectedObject = obj;
	}
}
