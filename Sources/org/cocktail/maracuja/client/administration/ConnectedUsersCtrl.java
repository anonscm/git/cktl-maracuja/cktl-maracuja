/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


/**
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ConnectedUsersCtrl extends CommonCtrl {
    private static final String TITLE = "Liste des utilisateurs connectés";
    private static final Dimension WINDOW_DIMENSION = new Dimension(370,380);
    private static final Dimension BUTTONS_DIMENSION = new Dimension(120,20);
    private ZKarukeraPanel myPanel;
    private JPanel myPanel2;
    private Action actionClose = new ActionClose();
    
    


    /**
     * @param editingContext
     */
    public ConnectedUsersCtrl(EOEditingContext editingContext) {
        super(editingContext);
        myPanel = new ConnectedUsersPanel(new ConnectedUsersPanelListener());
        myPanel2 = new JPanel(new BorderLayout());
    }

    
    

    
    
    private final ZKarukeraDialog createModalDialog(Window dial ) {
        ZKarukeraDialog win;
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE, true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE, true);
        }
        setMyDialog(win);
        myPanel.setMyDialog(win);
        myPanel.setPreferredSize(WINDOW_DIMENSION);
        initGUI();
        win.setContentPane(myPanel2);
        win.pack();
        return win;
    }        
    
    public final void openDialog(Window dial) {
        ZKarukeraDialog win = createModalDialog(dial);
        try {
            myPanel.updateData();
            win.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
    }    
    
    
    
    
    
    
    
	/**
     * 
     */
    private void initGUI() {
        myPanel.initGUI();
        myPanel2.add(myPanel, BorderLayout.CENTER);
        myPanel2.add(createCommentaireUI(), BorderLayout.NORTH);
        myPanel2.add(buildDefaultButtonsPanel(), BorderLayout.SOUTH);
    }    
    
	private final JPanel createCommentaireUI() {
		//Le titre
		JLabel titre = new JLabel(TITLE);
		titre.setFont(myPanel.getFont().deriveFont((float)12).deriveFont(Font.BOLD));
		
		//Le blabla
		JTextArea tmpTextArea = new JTextArea("Les utilisateurs listés ci-dessous sont connectés à l'application.");
		//tmpTextArea.setMargin(new Insets(3,3,3,3));
		Color bgcol = Color.decode("#FFFFFF");
		tmpTextArea.setFont(myPanel.getFont().deriveFont((float)11));
		tmpTextArea.setEditable(false);
		tmpTextArea.setAlignmentX(SwingConstants.LEFT);
//		tmpTextArea.setBackground(bgcol);
//		tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)));
		
		JPanel tmpJPanel = new JPanel();
		tmpJPanel.setLayout(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createMatteBorder(5,5,5,5, bgcol));
		tmpJPanel.setBackground(bgcol);
		
		tmpJPanel.setPreferredSize(new Dimension( 1   ,40));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
//		tmpJPanel.setMaximumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(Box.createRigidArea(new Dimension(1,40)), BorderLayout.LINE_START);		
		tmpJPanel.add(titre, BorderLayout.PAGE_START);
		tmpJPanel.add(tmpTextArea, BorderLayout.CENTER);
		return tmpJPanel;
	}    
    
	private JPanel buildDefaultButtonsPanel() {
//		JButton prevButton;
//		JButton nextButton;
		JButton closeButton;
//		JButton actionButton1;
		
		Box tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createRigidArea(new Dimension(2,40)));
		tmpBox.add(Box.createHorizontalGlue());
	

		//Bouton Fermer
		if (actionClose()!=null) {
			closeButton = new JButton(actionClose());
			closeButton.setPreferredSize(BUTTONS_DIMENSION);
			closeButton.setMinimumSize(BUTTONS_DIMENSION);
			closeButton.setHorizontalAlignment(SwingConstants.LEFT);
			closeButton.setHorizontalTextPosition(SwingConstants.RIGHT);
			tmpBox.add(closeButton );
			tmpBox.add( Box.createRigidArea(new Dimension(25,30)) );
		}
			

		

		JPanel tmpJPanel = new JPanel(new BorderLayout());
		tmpJPanel.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		tmpJPanel.setPreferredSize(new Dimension(1, 40));
		tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
		tmpJPanel.add(new JPanel(), BorderLayout.CENTER);	
		tmpJPanel.add(tmpBox, BorderLayout.LINE_END);
		return tmpJPanel;		
		
	}
	
	

    /**
     * @return
     */
    private Action actionClose() {
        return actionClose;
    }



    private final class ConnectedUsersPanelListener implements ConnectedUsersPanel.IConnectedUsersPanelListener {

        /**
         * @see org.cocktail.maracuja.client.administration.ConnectedUsersPanel.IConnectedUsersPanelListener#getData()
         */
        public NSArray getData() {
            return ServerProxy.clientSideRequestGetConnectedUsers(getEditingContext());
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ConnectedUsersPanel.IConnectedUsersPanelListener#onSelectionChanged()
         */
        public void onSelectionChanged() {
            return;
            
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ConnectedUsersPanel.IConnectedUsersPanelListener#onDbClick()
         */
        public void onDbClick() {
            
        }
        
    }
    
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
	    

        public void actionPerformed(ActionEvent e) {
            getMyDialog().onCancelClick();
        }
	    
	}
    
    
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return myPanel;
    }

    public String title() {
        return TITLE;
    }    
}
