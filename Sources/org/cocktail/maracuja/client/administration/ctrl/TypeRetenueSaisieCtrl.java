/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.administration.ui.TypeRetenueSaisiePanel;
import org.cocktail.maracuja.client.common.ctrl.ModifCtrl;
import org.cocktail.maracuja.client.factory.FactoryTypeRetenue;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeRetenue;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TypeRetenueSaisieCtrl extends ModifCtrl {
	private static final String TITLE = "Saisie d'un type de retenue";
	private static final Dimension WINDOW_SIZE = new Dimension(860, 400);
	private static final String PCONUM_LIKE = "4*";

	private Map values = new HashMap();

	private final TypeRetenueSaisiePanel mainPanel;
	private final DefaultComboBoxModel plancoModel;

	private final ActionCancel actionCancel = new ActionCancel();
	private final ActionSave actionValider = new ActionSave();
	private final TypeRetenueSaisiePanelListener typeRetenueSaisiePanelListener = new TypeRetenueSaisiePanelListener();

	private EOTypeRetenue _typeRetenue;

	/**
	 * @param editingContext
	 */
	public TypeRetenueSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);

		final NSArray plancos = EOPlanComptableFinder.getPlancoValidesForPcoNumLike(getEditingContext(), PCONUM_LIKE, true);
		plancoModel = new DefaultComboBoxModel(plancos.objects());
		mainPanel = new TypeRetenueSaisiePanel(typeRetenueSaisiePanelListener);
	}

	private final void checkSaisie() throws Exception {
		if (values.get(EOTypeRetenue.TRE_LIBELLE_KEY) == null || ((String) values.get(EOTypeRetenue.TRE_LIBELLE_KEY)).length() == 0) {
			throw new DataCheckException("Le libellé est obligatoire.");
		}

		if (values.get(EOTypeRetenue.PLAN_COMPTABLE_KEY) == null) {
			throw new DataCheckException("L'imputation est obligatoire.");
		}
	}

	private final class TypeRetenueSaisiePanelListener implements TypeRetenueSaisiePanel.ITypeRetenueSaisiePanelListener {

		public Action actionClose() {
			return actionCancel;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map getvalues() {
			return values;
		}

		public ComboBoxModel getPlancoModel() {
			return plancoModel;
		}

	}

	protected void onClose() {
	}

	protected void onCancel() {
		getEditingContext().revert();
		getMyDialog().onCancelClick();
	}

	protected boolean onSave() {
		try {
			checkSaisie();
			if (valideSaisie()) {
				getMyDialog().onOkClick();
				return true;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
		return false;
	}

	private boolean valideSaisie() {
		try {
			if (_typeRetenue == null) {
				final FactoryTypeRetenue factoryTypeRetenue = new FactoryTypeRetenue(myApp.wantShowTrace());
				_typeRetenue = factoryTypeRetenue.creerTypeRetenue(getEditingContext(), (String) values.get(EOTypeRetenue.TRE_LIBELLE_KEY), (EOPlanComptable) values.get(EOTypeRetenue.PLAN_COMPTABLE_KEY));
			}
			else {
				_typeRetenue.setTreLibelle((String) values.get(EOTypeRetenue.TRE_LIBELLE_KEY));
				_typeRetenue.setPlanComptableRelationship((EOPlanComptable) values.get(EOTypeRetenue.PLAN_COMPTABLE_KEY));
			}
			getEditingContext().saveChanges();
			return true;

		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
			return false;
		}

	}

	public String title() {
		return TITLE;
	}

	public Dimension defaultDimension() {
		return WINDOW_SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public int openDialog(final Window dial, boolean modal, final EOTypeRetenue typeRetenue) {
		values.clear();
		if (typeRetenue == null) {
			_typeRetenue = null;
		}
		else {
			_typeRetenue = typeRetenue;
			values.put(EOTypeRetenue.TRE_LIBELLE_KEY, typeRetenue.treLibelle());
			values.put(EOTypeRetenue.PLAN_COMPTABLE_KEY, typeRetenue.planComptable());
		}
		return super.openDialog(dial, modal);
	}

	public final EOTypeRetenue getTypeRetenue() {
		return _typeRetenue;
	}

}
