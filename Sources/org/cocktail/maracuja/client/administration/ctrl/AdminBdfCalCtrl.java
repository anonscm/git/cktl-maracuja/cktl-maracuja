/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JFileChooser;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.administration.ui.AdminBdfCalPanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


public final class AdminBdfCalCtrl extends CommonCtrl {
    private static final String TITLE = "Gestion du calendrier BDF";
    
    private final Icon ICONE_FICHIER = ZIcon.getIconForName(ZIcon.ICON_TEXTFILE_32);
    
    private static final Dimension WINDOW_DIMENSION=new Dimension(450,290);
    
    private final ActionClose actionClose = new ActionClose();
    private final ActionVoir actionVoir = new ActionVoir();
    private final ActionImport actionImport = new ActionImport();
    
    private final AdminBdfCalPanel panel;
    private final AdminBdfCalPanelModel model;
    
    private final BdfFileBoxMdl bdfFileBoxMdl;
    private File bdfFile = null;
    
    
    public AdminBdfCalCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        revertChanges();
        
        bdfFileBoxMdl = new BdfFileBoxMdl();
        
        model = new AdminBdfCalPanelModel();
        panel = new AdminBdfCalPanel(model);
        
    }

    




    private final ZKarukeraDialog createModalDialog(final Window dial ) {
        final ZKarukeraDialog win;
        
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE,true);
        }
        setMyDialog(win);
        panel.setMyDialog(getMyDialog());
        panel.initGUI();
        panel.setPreferredSize(WINDOW_DIMENSION);
        win.setContentPane(panel);
        win.pack();
        return win;
    }





    /**
     * Ouvre un dialog de saisie.
     */
    public final int openDialog(final Window dial) {
        revertChanges();
        final ZKarukeraDialog win = createModalDialog(dial);
        setMyDialog(win);
        int res=ZKarukeraDialog.MRCANCEL;
        try {
            initSaisie();
            
            //onEditingContextChanged();
            res = win.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
        return res;
    }
    
    
    private final void initSaisie() throws Exception {
        panel.updateData();
    }    
    
    

    

    public void close() {
        getEditingContext().revert();
        getMyDialog().onCancelClick();
    }    
 

    
    private final class AdminBdfCalPanelModel implements AdminBdfCalPanel.IAdminBdfCalPanelModel {

        public Action actionClose() {
            return actionClose;
        }

        public Action actionVoir() {
            return actionVoir;
        }

        public Action actionImport() {
            return actionImport;
        }

        public IZFileBoxModel getFileBoxFichierBdfMdl() {
            return bdfFileBoxMdl;
        }

    }
    
    
    
    

    private class BdfFileBoxMdl implements IZFileBoxModel {

        public boolean canDrop() {
            return true;
        }

        public File getFile() {
            return bdfFile;
        }

        public Icon getIcon() {
            return ICONE_FICHIER;
        }

        public String getText() {
            return null;
        }

        public void setFile(File f) {
            bdfFile = f;
        }
        
    }
    

    


    private final class ActionClose extends AbstractAction {
        public ActionClose() {
            this.putValue(AbstractAction.NAME, "Fermer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
        public void actionPerformed(ActionEvent e) {
            close();
        }

    }
    
    private void calendrierVoir() {
        try {
            final String filePath = ReportFactoryClient.imprimerBdfCalendrier(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), getMyDialog());
            if (filePath!=null) {
                myApp.openPdfFile(filePath);
            }
        }
        catch (Exception e1) {
            showErrorDialog(e1);
        }
        
    }
    

    private void calendrierImporter() {
        
        setWaitCursor(true);
        try {
            JFileChooser fileChooser =  new JFileChooser((bdfFile!=null ? bdfFile.getParentFile() : new File(myApp.temporaryDir) ));
            fileChooser.setMultiSelectionEnabled(false);
//            fileChooser.addChoosableFileFilter(new MyTxtFilter());

            // Open file dialog.
            if (!(fileChooser.showOpenDialog(getMyDialog())== JFileChooser.APPROVE_OPTION) ) {
                setWaitCursor(false);
                return;
            }
            
            final File f = fileChooser.getSelectedFile();
            if (! f.exists()) {
                throw new DefaultClientException("Le fichier n'existe pas.");
            }
            if (! f.isFile()) {
                throw new DefaultClientException("Le fichier n'existe pas.");
            }
            if (! f.canRead()) {
                throw new DefaultClientException("Impossible de lire le fichier.");
            }
            
//            ZLogger.debug("Import de " + f.getName());
            ZLogger.debug("Import de " + f.getName());
            
            //charger le fichier
            bdfFile = f;
            Date dateDebut;
            Date dateFin;
            final LinkedList dates = new LinkedList();
            final SimpleDateFormat formatBdf = new SimpleDateFormat("ddMMyy");
            int i = 0;
            
            try {
                final InputStream is = new FileInputStream(bdfFile);
                final BufferedReader br = new BufferedReader(new InputStreamReader(is));
                String str = br.readLine();
                while (str != null) {
                    i++;
                    final Date d;
                    try {
                        if (str.length() != 6) {
                            throw new ParseException("",0);
                        }
                        d = formatBdf.parse(str);
                        dates.add(d);
                    } catch (ParseException  e) {
                        throw new DefaultClientException("Erreur a la ligne "+ i +", "+ str +" ne respecte pas le format ddMMyy");
                    }   
                    str = br.readLine();
                }
                is.close();
            } catch (FileNotFoundException e) {
                throw new DefaultClientException("Le fichier " + bdfFile.getAbsolutePath()+" n'a pas été trouvé.");
            } catch (Exception e1) {
                throw e1;
            }            
            
            if (dates.size() == 0){
                throw new DefaultClientException("Le fichier ne contient pas de ligne");
            }
            
            //recup date debut et date fin
            dateDebut = (Date) dates.getFirst(); 
            dateFin = (Date) dates.getLast(); 
            
            Date dateFinExistante = ZDateUtil.addDHMS(dateDebut, -1, 0, 0, 0);
            
            //recupererer la derniere date exitante dans le calendrier deja present
            final NSArray cal = EOsFinder.getCalendrierBdfAfter(getEditingContext(), dateDebut);
            if (cal.count()>0) {
                dateFinExistante = (Date) cal.lastObject();
            }
             
            
            //verifier que date fin n'est pas <= a la derniere date du calendrier
//          demander confirmation avec date debut et date fin
            if (dateFinExistante.after(dateDebut) ) {
                if (!showConfirmationDialog("Confirmation", "Le calendrier que vous souhaitez importer va du " + ZConst.FORMAT_DATESHORT.format(dateDebut) + 
                        " au " + ZConst.FORMAT_DATESHORT.format(dateFin) + ". Le calendrier déjà présent dans la base de donnée s'arrête au " + 
                        ZConst.FORMAT_DATESHORT.format(dateFinExistante) + ". Si vous continuer,les dates déjà présentes seront remplacées par les nouvelles. Souhaitez-vous continuer ?" , ZMsgPanel.BTLABEL_YES)) {
                    return;
                }
            }
            else {
                if (!showConfirmationDialog("Confirmation", "Le calendrier que vous souhaitez importer va du " + ZConst.FORMAT_DATESHORT.format(dateDebut) + 
                        " au " + ZConst.FORMAT_DATESHORT.format(dateFin) + ". Souhaitez-vous continuer ?" , ZMsgPanel.BTLABEL_YES)) {
                    return;
                }
            }
            
            final StringBuffer sb = new StringBuffer();
            //Creer eventuellement script de suppression
            sb.append("BEGIN\n");
            if (dateFinExistante.after(dateDebut) ) {
                sb.append("DELETE FROM MARACUJA.BDF_CALENDRIER where BDFCAL_DATE_ISO >= '" + ZConst.FORMAT_DATE_YYYYMMDD.format(dateDebut) + "' and BDFCAL_DATE_ISO <= '"+ ZConst.FORMAT_DATE_YYYYMMDD.format(dateFin) +"';\n");
            }
            //Creer script sql d'insertion
            final Iterator iterator = dates.iterator();
            while (iterator.hasNext()) {
                final Date element = (Date) iterator.next();
                final String dateIso = ZConst.FORMAT_DATE_YYYYMMDD.format(element);
                final String dateBdf = formatBdf.format(element);
                sb.append("INSERT INTO MARACUJA.BDF_CALENDRIER (BDFCAL_DATE_ISO, BDFCAL_DATE_BDF) VALUES ( '"+ dateIso +"', '"+ dateBdf +"')" +";\n");
            }
            sb.append("COMMIT;\n");
            sb.append("END;\n");
            
//            System.out.println("");
//            System.out.println("******************");
//            System.out.println(sb.toString());
//            System.out.println("******************");
//            System.out.println("");
            
            //executer
            ServerProxy.clientSideRequestSqlQuery(getEditingContext(), sb.toString());
            showInfoDialog("Mise à jour effectuée.");
            
            setWaitCursor(false);
        }
        catch (Exception e) {
            setWaitCursor(false);
            showErrorDialog(e);
        }
        finally {
            setWaitCursor(false);
        }
        
        
       
        
        
    }    
    
    private final class ActionVoir extends AbstractAction {
        public ActionVoir() {
            this.putValue(AbstractAction.NAME, "Voir");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Visualiser le calendrier");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_VIEWTEXT_16));
        }
        public void actionPerformed(ActionEvent e) {
            calendrierVoir();
            
        }

    }
    
    private final class ActionImport extends AbstractAction {
        public ActionImport() {
            this.putValue(AbstractAction.NAME, "Importer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Importer un fichier dans le calendrier");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
        }
        public void actionPerformed(ActionEvent e) {
            calendrierImporter();
            
        }
        
    }
    
    
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return panel;
    }

    public String title() {
        return TITLE;
    }
    
    
}
