/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessModeRecouvrement;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModRecouvrementSaisieCtrl extends CommonCtrl {
    private static final String TITLE = "Saisie d'un mode de recouvrement";

    private final Dimension WINDOW_DIMENSION=new Dimension(550,280); 
    
    public final static int MODE_NEW=1;
    public final static int MODE_MODIF=2;
    private int mode;
    
    
//    private Window parentWindow;
    private HashMap modDico;    
    private ModRecouvrementSaisiePanel myPanel;
    
    private final ActionAnnuler actionAnnuler = new ActionAnnuler();
    private final ActionValider actionValider = new ActionValider();
    private final ActionPlancoRecouvrementSelect actionPlancoRecouvrementSelect = new ActionPlancoRecouvrementSelect();
    private final ActionPlancoVisaSelect actionPlancoVisaSelect = new ActionPlancoVisaSelect();
    private final ActionPlancoVisaSupprimer actionPlancoVisaSupprimer = new ActionPlancoVisaSupprimer();
    private final ActionPlancoRecouvrementSupprimer actionPlancoRecouvrementSupprimer = new ActionPlancoRecouvrementSupprimer();
    
    

    private ZKarukeraDialog win;
    private EOModeRecouvrement currentMod;
    
    private FactoryProcessModeRecouvrement myFactoryProcessModeRecouvrement; 
    
    private EODisplayGroup dgPlancomptable;  
    private PcoSelectDlg planComptableSelectionDialog;
    
    private DefaultComboBoxModel domainesModel;
    
    
    
    /**
     * @param editingContext
     */
    public ModRecouvrementSaisieCtrl(EOEditingContext editingContext, Window win) {
        super(editingContext);
        revertChanges();
//        parentWindow = win;
        initSubObjects();
    }

    public void initSubObjects() {
        dgPlancomptable = new EODisplayGroup();
        domainesModel = new DefaultComboBoxModel(new String[]{EOModeRecouvrement.MODDOM_CHEQUE, EOModeRecouvrement.MODDOM_ECHEANCIER, EOModeRecouvrement.MODDOM_INTERNE, EOModeRecouvrement.MODDOM_AUTRE});
        
        
        myFactoryProcessModeRecouvrement = new FactoryProcessModeRecouvrement(myApp.wantShowTrace(),null);
        modDico = new HashMap();
        myPanel = new ModRecouvrementSaisiePanel(new ModSaisiePanelListener());
    }
    
    
    
    private final void annulerSaisie() {
        getEditingContext().revert();
        getMyDialog().onCancelClick();
    }
    
    
    private final void validerSaisie() {
        try {
            if (mode==MODE_NEW) {
                validerSaisieForNew();
            }
            else {
                validerSaisieForModify();
            }
            getMyDialog().onOkClick();
            
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }  
    

    /**
     * @throws Exception
     * 
     */
    private void validerSaisieForNew() throws Exception {
        ZLogger.debug("");
        checkSaisieDico();
        EOModeRecouvrement mod = myFactoryProcessModeRecouvrement.creerModeRecouvrementVide(getEditingContext(), (String)modDico.get("modCode"), (String)modDico.get("modLibelle"), myApp.appUserInfo().getCurrentExercice());
        myFactoryProcessModeRecouvrement.modifierDomaine(getEditingContext(), mod, (String) modDico.get("modDom"));
        myFactoryProcessModeRecouvrement.modifierCompteRecouvrement(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptablePaiement"));
        myFactoryProcessModeRecouvrement.modifierCompteVisa(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptableVisa"));
        myFactoryProcessModeRecouvrement.validerModeRecouvrement(getEditingContext(), mod);
        
        ZLogger.debug(getEditingContext());
        
        
        //la sauvegarde
        getEditingContext().saveChanges();
        
        currentMod = mod;
        
    }
    
    private void validerSaisieForModify() throws Exception {
        ZLogger.debug("");
        checkSaisieDico();
        EOModeRecouvrement mod = currentMod;
        myFactoryProcessModeRecouvrement.modifierDomaine(getEditingContext(), mod, (String) modDico.get("modDom"));
        myFactoryProcessModeRecouvrement.modifierCompteRecouvrement(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptablePaiement"));
        myFactoryProcessModeRecouvrement.modifierCompteVisa(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptableVisa"));
        mod.setModLibelle((String)modDico.get("modLibelle"));
        
//        ZLogger.debug(getEditingContext());
        
        
        NSArray users = ServerProxy.clientSideRequestGetConnectedUsers(getEditingContext());
        if (users.count()>1) {
            throw new DefaultClientException("Pour utiliser cette fonctionalité, vous devez être la seule personne connectée à l'application. Actuellement " + users.count() +" utilisateurs sont connectés. Pour les visualiser, utiliser la fonction Outils/Utilisateurs connectés.");
        }
                
        
        //la sauvegarde
        getEditingContext().saveChanges();
        
        currentMod = mod;
    }

    
    
    private final void checkSaisieDico() throws Exception {
        ZLogger.debug(modDico);
        
        if (modDico.get("modLibelle")==null) {
            throw new DataCheckException("Le libellé est obligatoire.");
        }
        
        if (modDico.get("modCode")==null) {
            throw new DataCheckException("Le code est obligatoire.");
        }
        
        
        //Vérifier si le code est déja utilisé
        if (mode==MODE_NEW) {
            EOModeRecouvrement obs = (EOModeRecouvrement)ZFinder.fetchObject(getEditingContext(), "ModeRecouvrement","exercice=%@ and modCode=%@", new NSArray(new Object[]{myApp.appUserInfo().getCurrentExercice(),modDico.get("modCode")} ), null, true);
            if (obs!=null) {
                throw new DataCheckException("Le code "+ modDico.get("modCode") +" est déjà utilisé par le mode de recouvrement " + obs.modLibelle());
            }
        }
        
        

        if (modDico.get("planComptablePaiement")!=null) {
            if (   !EOPlanComptable.CLASSE5.equals(((EOPlanComptable)modDico.get("planComptablePaiement")).getClasseCompte())   ) {
                throw new DataCheckException("Le compte de recouvrement doit être de la classe " + EOPlanComptable.CLASSE5  );
            }
        }
        
        
    }         
        
    public final void initDicoWithDefault() {
        modDico.put("modCode", null);
        modDico.put("modDom", EOModeRecouvrement.MODDOM_CHEQUE);
        modDico.put("modLibelle",null );
        modDico.put("exercice", myApp.appUserInfo().getCurrentExercice());
        modDico.put("planComptablePaiement", null);
        modDico.put("planComptableVisa", null);
//        modDico.put("modEmaAuto", EOModeRecouvrement.EMA_AUTO_NON);
    }
    
    public final void initDicoWithObject() {
        ZLogger.debug("");
        modDico.put("modCode", currentMod.modCode());
        modDico.put("modDom", currentMod.modDom());
        modDico.put("modLibelle",currentMod.modLibelle());
        modDico.put("exercice", currentMod.exercice());
        modDico.put("planComptablePaiement", currentMod.planComptablePaiement());
        modDico.put("planComptableVisa", currentMod.planComptableVisa());
//        modDico.put("modEmaAuto", currentMod.modEmaAuto());
    }
    
    
    
    
    
    private final ZKarukeraDialog createModalDialog(Dialog dial ) {
        win = new ZKarukeraDialog(dial, TITLE, true);
        setMyDialog(win);
        myPanel.setMyDialog(win);
        myPanel.setPreferredSize(WINDOW_DIMENSION);
        initGUI();
        win.setContentPane(myPanel);
        win.pack();
        return win;
    }    
    
//    private final ZKarukeraDialog createModalDialog(Frame dial ) {
//        win = new ZKarukeraDialog(dial, "Saisie d'un mode de recouvrement", true);
//        setMyDialog(win);
//        myPanel.setMyDialog(win);
//        myPanel.setPreferredSize(WINDOW_DIMENSION);
//        initGUI();
//        win.setContentPane(myPanel);
//        win.pack();
//        return win;
//    }     
//    
    
    
	/**
     * 
     */
    private void initGUI() {
        planComptableSelectionDialog = createPlanComptableSelectionDialog();
        myPanel.initGUI();
    }

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	} 
	
//    private ZEOSelectionDialog createPlanComptableSelectionDialog() {
//    	
//    	Vector filterFields = new Vector(2,0);
//    	filterFields.add("pcoNum");
//    	filterFields.add("pcoLibelle");
//    	
//    	Vector myCols = new Vector(2,0);
//    	ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgPlancomptable, "pcoNum", "N°");
//    	ZEOTableModelColumn col2 = new ZEOTableModelColumn(dgPlancomptable, "pcoLibelle", "Libellé");
//    	
//    	myCols.add(col1);
//    	myCols.add(col2);
//    	
//    	ZEOSelectionDialog dialog = new ZEOSelectionDialog( getMyDialog(), "Sélection d'une imputation" ,dgPlancomptable,myCols,"Veuillez sélectionner un compte dans la liste",filterFields);
//    	dialog.setFilterPrefix("");
//    	dialog.setFilterSuffix("*");
//    	dialog.setFilterInstruction(" caseInsensitiveLike %@");
//    	
//    	return dialog;		
//    }        
    
    
	
	
    private final NSArray getPcoValidesForVisa() {
//        final EOQualifier qual1 =  EOQualifier.qualifierWithQualifierFormat("pcoNum like %@", new NSArray(new Object[]{"4*"} )); 
////        final EOQualifier qual1 =  EOQualifier.qualifierWithQualifierFormat("pcoNum like %@ or pcoNum like %@", new NSArray(new Object[]{"6*", "7*"} )); 
//        final EOQualifier qual2 =  EOQualifier.qualifierWithQualifierFormat("pcoValidite=%@", new NSArray(new Object[]{EOPlanComptable.etatValide} ) );
//        final NSMutableArray quals = new NSMutableArray();
//        quals.addObject(qual1);
////        quals.addObject(new EONotQualifier(qual1));
//        quals.addObject(qual2);
//        final EOQualifier qual = new EOAndQualifier(quals);
        
    	final EOQualifier qual1 =  EOQualifier.qualifierWithQualifierFormat("pcoNum like %@ ", new NSArray(new Object[]{"4*"} )); 
//      	 return EOPlanComptableFinder.getPlancoValidesWithQuals(getEditingContext(), qual1, false);        
      	return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(),qual1, false);
//        return ZFinder.fetchArray(getEditingContext(), EOPlanComptable.ENTITY_NAME, qual,new NSArray(EOSortOrdering.sortOrderingWithKey("pcoNum", EOSortOrdering.CompareAscending)), false );
    }	
    private final NSArray getPcoValidesForRecouvrement() {
    	final EOQualifier qual1 =  EOQualifier.qualifierWithQualifierFormat("pcoNum like %@ ", new NSArray(new Object[]{"5*"} )); 
//   	 return EOPlanComptableFinder.getPlancoValidesWithQuals(getEditingContext(), qual1, false);
    	return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(),qual1, false);
    }	
	
	
	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		if (planComptableSelectionDialog.open(dgPlancomptable.allObjects()) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;  		
//		setWaitCursor(true);
//		EOPlanComptable res = null;
//		
//		
//		planComptableSelectionDialog.getFilterTextField().setText(filter);
//		planComptableSelectionDialog.filter();
//		if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
//			NSArray selections = planComptableSelectionDialog.getSelectedEOObjects();
//			
//			if ((selections==null) || (selections.count()==0)) {
//			    res = null;
//			}
//			else {
//			    res = (EOPlanComptable) selections.objectAtIndex(0);
//			}
//		}
//		setWaitCursor(false);
//		return res;
	}           
         
    
    
    /**
     * Gere la selection d'une plancomptable de visa
     */
    private final void onSelectPlancoVisa() {
        EOPlanComptable res=null;
        //on affiche une fenetre de recherche de PlanComptable
        dgPlancomptable.setObjectArray(getPcoValidesForVisa());
        res = selectPlanComptableIndDialog((String) modDico.get("pcoNumVisa"));
        
        if (res != null) {
        	modDico.put("planComptableVisa", res);
            modDico.put("pcoNumVisa", res.pcoNum() );
        }
        
        
        try {
            myPanel.updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    /**
     * Gere la selection d'une plancomptable de recouvrement
     */
    private final void onSelectPlancoRecouvrement() {
        EOPlanComptable res=null;
        //on affiche une fenetre de recherche de PlanComptable
        dgPlancomptable.setObjectArray(getPcoValidesForRecouvrement());
        res = selectPlanComptableIndDialog((String) modDico.get("pcoNumPaiement"));
            
        
        if (res != null) {
        	modDico.put("planComptablePaiement", res);
            modDico.put("pcoNumPaiement", res.pcoNum() );
        }
        try {
            myPanel.updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    private final void onSupprimerPlancoRecouvrement() {
        modDico.put("planComptablePaiement", null);
        modDico.put("pcoNumPaiement", null );
        try {
            myPanel.updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    private final void onSupprimerPlancoVisa() {
        modDico.put("planComptableVisa", null);
        modDico.put("pcoNumVisa", null );
        try {
            myPanel.updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    /**
     * Ouvre un dialog de saisie.
     */
    public final EOModeRecouvrement openDialogNew(Dialog dial) {
        ZKarukeraDialog win1 = createModalDialog(dial);
//        this.setMyDialog(win);
        try {
            mode = MODE_NEW;
            newSaisie();
            win1.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win1.dispose();
        }
        return currentMod;
    }
    
    public final EOModeRecouvrement openDialogModify(Dialog dial, final EOModeRecouvrement leMod) {
        if (leMod==null) {
            return null;
        }
        
        ZKarukeraDialog win1 = createModalDialog(dial);
//        this.setMyDialog(win);
        try {
            mode = MODE_MODIF;
            modifySaisie(leMod);
            win1.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win1.dispose();
        }
        return currentMod;
    }
    
 
    
    
    
    
    public final void newSaisie() {
        try {
            currentMod=null;
            initDicoWithDefault();
            myPanel.updateData();
            actionValider.setEnabled(true);
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        
    }    
    public final void modifySaisie(EOModeRecouvrement leMod) {
        try {
            currentMod=leMod;
            initDicoWithObject();
            myPanel.updateData();
            myPanel.lockForModify();
            actionValider.setEnabled(true);
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        
    }    
    
    
	public final class ActionAnnuler extends AbstractAction {

	    public ActionAnnuler() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
	    

        public void actionPerformed(ActionEvent e) {
          annulerSaisie();
        }
	    
	}  
    
 	
	public final class ActionValider extends AbstractAction {
	    public ActionValider() {
            super("Enregistrer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
        }

        public void actionPerformed(ActionEvent e) {
          validerSaisie();
        }
	    
	}	
    
	
	
    private final class ActionPlancoVisaSelect extends AbstractAction {
        public ActionPlancoVisaSelect() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Sélectionner un compte pour le visa");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            onSelectPlancoVisa();
        }
    } 	
    
    private final class ActionPlancoVisaSupprimer extends AbstractAction {
        public ActionPlancoVisaSupprimer() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Supprimer le compte pour le visa");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            onSupprimerPlancoVisa();
        }
    } 	
    private final class ActionPlancoRecouvrementSupprimer extends AbstractAction {
        public ActionPlancoRecouvrementSupprimer() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Supprimer le compte pour le recouvrement");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            onSupprimerPlancoRecouvrement();
        }
    } 	
    
    
    
    private final class ActionPlancoRecouvrementSelect extends AbstractAction {
        public ActionPlancoRecouvrementSelect() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Sélectionner un compte pour le recouvrement");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            onSelectPlancoRecouvrement();
        }
    } 	
	
	
    private final class ModSaisiePanelListener implements ModRecouvrementSaisiePanel.IModSaisiePanelListener {
        public DefaultComboBoxModel getDomainesModel() {
            return domainesModel;
        }
        
        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#dicoValues()
         */
        public HashMap dicoValues() {
            return modDico;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#actionValider()
         */
        public Action actionValider() {
            return actionValider;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#actionAnnuler()
         */
        public Action actionAnnuler() {
            return actionAnnuler;
        }



        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#actionPlancomptableVisaSelect()
         */
        public AbstractAction actionPlancomptableVisaSelect() {
            return actionPlancoVisaSelect;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#actionPlancomptableRecouvrementSelect()
         */
        public AbstractAction actionPlancomptableRecouvrementSelect() {
            return actionPlancoRecouvrementSelect;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#actionPlancomptableRecouvrementSupprimer()
         */
        public AbstractAction actionPlancomptableRecouvrementSupprimer() {
            return actionPlancoRecouvrementSupprimer;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementSaisiePanel.IModSaisiePanelListener#actionPlancomptableVisaSupprimer()
         */
        public AbstractAction actionPlancomptableVisaSupprimer() {
            return actionPlancoVisaSupprimer;
        }
        
    }
    public EOModeRecouvrement getCurrentMod() {
        return currentMod;
    }
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return myPanel;
    }

    public String title() {
        return TITLE;
    }
    
}
