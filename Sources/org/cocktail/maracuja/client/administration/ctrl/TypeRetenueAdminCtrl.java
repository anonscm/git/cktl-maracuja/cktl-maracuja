/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.administration.ui.TypesRetenueAdminPanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderEtats;
import org.cocktail.maracuja.client.metier.EOTypeEtat;
import org.cocktail.maracuja.client.metier.EOTypeRetenue;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;




public final class TypeRetenueAdminCtrl extends CommonCtrl {
    private static final String TITLE = "Administration des types de retenues";
    private final Dimension SIZE=new Dimension(860,550);
    
    
    private final TypesRetenueAdminPanel panel;
    
    private final ActionAdd actionAdd = new ActionAdd();
    private final ActionModify actionModify = new ActionModify();
    private final ActionSupprimer actionSupprimer = new ActionSupprimer();
    
    private final AdminTypesCreditPanelModel model;
    
    private final Map mapFilter = new HashMap();
    private final TypeRetenueTableListener typeRetenueTableListener;
    
    
    
    
    public TypeRetenueAdminCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        revertChanges();

        typeRetenueTableListener = new TypeRetenueTableListener();
        model = new AdminTypesCreditPanelModel();
        panel = new TypesRetenueAdminPanel(model);
        
    }

    
    
    
    
    private final NSArray getTypeRetenues() throws Exception {
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeRetenue.TYPE_ETAT_KEY + "=%@", new NSArray(new Object[]{ ZFinderEtats.etat_VALIDE() }));
        NSArray res = EOsFinder.fetchArray(getEditingContext(), EOTypeRetenue.ENTITY_NAME, qual, new NSArray(new Object[]{EOSortOrdering.sortOrderingWithKey(EOTypeRetenue.TRE_LIBELLE_KEY, EOSortOrdering.CompareAscending)}), true);
        return res;
       
    }


    private final void typeRetenueAdd() {
        final TypeRetenueSaisieCtrl typeRetenueSaisieCtrl = new TypeRetenueSaisieCtrl(getEditingContext());
        if (typeRetenueSaisieCtrl.openDialog(getMyDialog(), true, null)==ZCommonDialog.MROK) {
            try {
                mainPanel().updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }
        
    }    
    
    private final void typeRetenueModify() {
        final EOTypeRetenue typeRetenue = getSelectedTypeRetenue();
        
        final TypeRetenueSaisieCtrl typeRetenueSaisieCtrl = new TypeRetenueSaisieCtrl(getEditingContext());
        if (typeRetenueSaisieCtrl.openDialog(getMyDialog(), true, typeRetenue)==ZCommonDialog.MROK) {
            try {
                mainPanel().updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }        
        
    }    
    
    private final void save() {
        try {
            getEditingContext().saveChanges();
            refreshActions();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        
        
    }
    
    private final void typeRetenueInvalider() {
        final EOTypeRetenue typeRetenue = getSelectedTypeRetenue();
        if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement supprimer le type de retenue " + typeRetenue.treLibelle() + "?", ZMsgPanel.BTLABEL_NO)) {

            try {
                getSelectedTypeRetenue().setTypeEtatRelationship(ZFinderEtats.etat_ANNULE());
                save();
                panel.updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
            
        }
    }    
    
    
//    private final void typeRetenueValider() {
//        final EOTypeRetenue typeRetenue = getSelectedTypeRetenue();
//        if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement rendre le type de crédit " + typeRetenue.tcdCode() + " valide?", ZMsgPanel.BTLABEL_NO)) {
//            getSelectedTypeRetenue().setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
//            save();
//            panel.getTypesRetenueTablePanel().fireTableRowUpdated(panel.getTypesRetenueTablePanel().selectedRowIndex());
//        }
//    }    
//    
    


    
    private final class AdminTypesCreditPanelModel implements TypesRetenueAdminPanel.IAdminTypeRetenuePanelModel {

        public Action actionAdd() {
            return actionAdd;
        }

        public Action actionClose() {
            return actionClose;
        }

        public Action actionInvalider() {
            return actionSupprimer;
        }
        
        public Action actionModify() {
            return actionModify;
        }        
        
        public IZTablePanelMdl typesCreditTableListener() {
            return typeRetenueTableListener;
        }

        public Map getFilterMap() {
            return mapFilter;
        }

    }
    
    
    
    

    
    private final class TypeRetenueTableListener implements IZTablePanelMdl {
//        private final TypeRetenueCellRenderer typeRetenueCellRenderer = new TypeRetenueCellRenderer();
        

        public void selectionChanged() {
            refreshActions();
        }
        
        public NSArray getData() throws Exception {
            return getTypeRetenues();
        }
        
        public void onDbClick() {
            typeRetenueModify();
        }
        
        
//        private final class TypeRetenueCellRenderer extends ZEOTableCellRenderer {
//            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//                final EOTypeRetenue typeRetenue = (EOTypeRetenue) ((ZEOTable)table).getObjectAtRow(row);
//                if (EOTypeEtat.ETAT_ANNULE. equals( typeRetenue.typeEtat().tyetLibelle())) {
//                    value = ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + value + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX;
//                }
//                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//            }
//            
//        }
    }
    

    
    
    
    private final EOTypeRetenue getSelectedTypeRetenue() {
        return (EOTypeRetenue) panel.getTypesRetenueTablePanel().selectedObject();
    }

    private final void refreshActions() {
        final EOTypeRetenue exer = getSelectedTypeRetenue();
        actionAdd.setEnabled(true);
        if (exer == null) {
            actionSupprimer.setEnabled(false);
            actionModify.setEnabled(false);
        }
        else {
            actionSupprimer.setEnabled( EOTypeEtat.ETAT_VALIDE.equals(exer.typeEtat().tyetLibelle()));
//            actionValider.setEnabled( !EOTypeEtat.ETAT_VALIDE.equals(exer.typeEtat().tyetLibelle()));
            actionModify.setEnabled(true);
        }
        
    }
    


    
    private final class ActionAdd extends AbstractAction {
        public ActionAdd() {
            this.putValue(AbstractAction.NAME, "Nouveau");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouveau type de retenue");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
        }
        public void actionPerformed(ActionEvent e) {
            typeRetenueAdd();
            
        }
    }
    

    
    private final class ActionModify extends AbstractAction {
        public ActionModify() {
            this.putValue(AbstractAction.NAME, "Modifier");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le type de retenue");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
        }
        public void actionPerformed(ActionEvent e) {
            typeRetenueModify();
        }
    }
    
    private final class ActionSupprimer extends AbstractAction {
        public ActionSupprimer() {
            this.putValue(AbstractAction.NAME, "Supprimer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le type de retenue");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
        }
        public void actionPerformed(ActionEvent e) {
            typeRetenueInvalider();
        }
    }
    
    public String title() {
        return TITLE;
    }

    public Dimension defaultDimension() {
        return SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return panel;
    }

    protected void onClose() {
        getMyDialog().onCloseClick();
    }


    
}
