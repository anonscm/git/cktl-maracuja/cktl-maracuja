/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessModeDePaiement;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModPaiementSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'un mode de paiement";

	private final Dimension WINDOW_DIMENSION = new Dimension(ZConst.MAX_WINDOW_WIDTH, 350);

	public final static int MODE_NEW = 1;
	public final static int MODE_MODIF = 2;
	private int mode;

	//    private Window parentWindow;
	private HashMap modDico;
	private ModPaiementSaisiePanel myPanel;

	private final ActionAnnuler actionAnnuler = new ActionAnnuler();
	private final ActionValider actionValider = new ActionValider();
	private final ActionPlancoPaiementSelect actionPlancoPaiementSelect = new ActionPlancoPaiementSelect();
	private final ActionPlancoVisaSelect actionPlancoVisaSelect = new ActionPlancoVisaSelect();
	private final ActionPlancoTvaSelect actionPlancoTvaSelect = new ActionPlancoTvaSelect();
	private final ActionPlancoTvaCtpSelect actionPlancoTvaCtpSelect = new ActionPlancoTvaCtpSelect();
	private final ActionPlancoVisaSupprimer actionPlancoVisaSupprimer = new ActionPlancoVisaSupprimer();
	private final ActionPlancoPaiementSupprimer actionPlancoPaiementSupprimer = new ActionPlancoPaiementSupprimer();
	private final ActionPlancoTvaSupprimer actionPlancoTvaSupprimer = new ActionPlancoTvaSupprimer();
	private final ActionPlancoTvaCtpSupprimer actionPlancoTvaCtpSupprimer = new ActionPlancoTvaCtpSupprimer();

	private ZKarukeraDialog win;
	private EOModePaiement currentMod;
	private FactoryProcessModeDePaiement myFactoryProcessModeDePaiement;
	private EODisplayGroup dgPlancomptable;
	private PcoSelectDlg planComptableSelectionDialog;
	private DefaultComboBoxModel domainesModel;

	private DefaultComboBoxModel contrePartieGestionMdl = new DefaultComboBoxModel(new String[] {
			null, "AGENCE", "COMPOSANTE"
	});

	/**
	 * @param editingContext
	 * @throws DefaultClientException
	 */
	public ModPaiementSaisieCtrl(EOEditingContext editingContext, Window win) {
		super(editingContext);
		revertChanges();
		//        parentWindow = win;
		initSubObjects();
	}

	public void initSubObjects() {
		dgPlancomptable = new EODisplayGroup();
		domainesModel = new DefaultComboBoxModel(new String[] {
				EOModePaiement.MODDOM_VIREMENT, EOModePaiement.MODDOM_CHEQUE, EOModePaiement.MODDOM_CAISSE, EOModePaiement.MODDOM_INTERNE, EOModePaiement.MODDOM_AUTRE, EOModePaiement.MODDOM_A_EXTOURNER
		});
		myFactoryProcessModeDePaiement = new FactoryProcessModeDePaiement(myApp.wantShowTrace(), null);
		modDico = new HashMap();
		myPanel = new ModPaiementSaisiePanel(new ModSaisiePanelListener());
	}

	private final void annulerSaisie() {
		getEditingContext().revert();
		getMyDialog().onCancelClick();
	}

	private final void validerSaisie() {
		try {
			if (mode == MODE_NEW) {
				validerSaisieForNew();
			}
			else {
				validerSaisieForModify();
			}
			getMyDialog().onOkClick();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * @throws Exception
	 */
	private void validerSaisieForNew() throws Exception {
		ZLogger.debug("");
		checkSaisieDico();
		EOModePaiement mod = myFactoryProcessModeDePaiement.creerModePaiementVide(getEditingContext(), (String) modDico.get("modCode"), (String) modDico.get("modLibelle"), myApp.appUserInfo().getCurrentExercice());
		myFactoryProcessModeDePaiement.modifierDomaine(getEditingContext(), mod, (String) modDico.get("modDom"));
		myFactoryProcessModeDePaiement.modifierComptePaiement(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptablePaiement"));
		myFactoryProcessModeDePaiement.modifierCompteVisa(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptableVisa"));
		myFactoryProcessModeDePaiement.modifierEmaAuto(getEditingContext(), mod, (String) modDico.get("modEmaAuto"));
		myFactoryProcessModeDePaiement.validerModePaiement(getEditingContext(), mod);
		mod.setPlanComptableTvaRelationship((EOPlanComptable) modDico.get(EOModePaiement.PLAN_COMPTABLE_TVA_KEY));
		mod.setPlanComptableTvaCtpRelationship((EOPlanComptable) modDico.get(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY));
		mod.setModPaiementHt((String) modDico.get(EOModePaiement.MOD_PAIEMENT_HT_KEY));
		mod.setModContrepartieGestion((String) modDico.get(EOModePaiement.MOD_CONTREPARTIE_GESTION_KEY));
		ZLogger.debug(getEditingContext());

		//la sauvegarde
		getEditingContext().saveChanges();

		currentMod = mod;

	}

	private void validerSaisieForModify() throws Exception {
		ZLogger.debug("");
		checkSaisieDico();
		EOModePaiement mod = currentMod;
		myFactoryProcessModeDePaiement.modifierDomaine(getEditingContext(), mod, (String) modDico.get("modDom"));
		myFactoryProcessModeDePaiement.modifierComptePaiement(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptablePaiement"));
		myFactoryProcessModeDePaiement.modifierCompteVisa(getEditingContext(), mod, (EOPlanComptable) modDico.get("planComptableVisa"));
		myFactoryProcessModeDePaiement.modifierEmaAuto(getEditingContext(), mod, (String) modDico.get("modEmaAuto"));
		mod.setModLibelle((String) modDico.get("modLibelle"));
		mod.setPlanComptableTvaRelationship((EOPlanComptable) modDico.get(EOModePaiement.PLAN_COMPTABLE_TVA_KEY));
		mod.setPlanComptableTvaCtpRelationship((EOPlanComptable) modDico.get(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY));
		mod.setModPaiementHt((String) modDico.get(EOModePaiement.MOD_PAIEMENT_HT_KEY));
		mod.setModContrepartieGestion((String) modDico.get(EOModePaiement.MOD_CONTREPARTIE_GESTION_KEY));

		NSArray users = ServerProxy.clientSideRequestGetConnectedUsers(getEditingContext());
		if (users.count() > 1) {
			throw new DefaultClientException("Pour utiliser cette fonctionalité, vous devez être la seule personne connectée à l'application. Actuellement " + users.count() + " utilisateurs sont connectés. Pour les visualiser, utiliser la fonction Outils/Utilisateurs connectés.");
		}

		//la sauvegarde
		getEditingContext().saveChanges();

		currentMod = mod;
	}

	private final void checkSaisieDico() throws Exception {
		ZLogger.debug(modDico);
		modDico.put(EOModePaiement.MOD_CONTREPARTIE_GESTION_KEY, contrePartieGestionMdl.getSelectedItem());

		if (modDico.get("modLibelle") == null) {
			throw new DataCheckException("Le libellé est obligatoire.");
		}

		if (modDico.get("modCode") == null) {
			throw new DataCheckException("Le code est obligatoire.");
		}

		//Vérifier si le code est déja utilisé
		if (mode == MODE_NEW) {
			EOModePaiement obs = (EOModePaiement) ZFinder.fetchObject(getEditingContext(), "ModePaiement", "exercice=%@ and modCode=%@", new NSArray(new Object[] {
					myApp.appUserInfo().getCurrentExercice(), modDico.get("modCode")
			}), null, true);
			if (obs != null) {
				throw new DataCheckException("Le code " + modDico.get("modCode") + " est déjà utilisé par le mode de paiement " + obs.modLibelle());
			}
		}

		//autres verifications
		//suivant le domaine, autoriser ou non la saisie de visa ou paiement
		//si Interne =>paiement = null

		if (EOModePaiement.MODDOM_VIREMENT.equals(modDico.get("modDom")) || EOModePaiement.MODDOM_CHEQUE.equals(modDico.get("modDom")) || EOModePaiement.MODDOM_CAISSE.equals(modDico.get("modDom"))) {
			if (modDico.get("planComptableVisa") != null) {
				throw new DataCheckException("Le compte visa ne doit pas être défini pour le domaine " + modDico.get("modDom"));
			}
		}
		else if (EOModePaiement.MODDOM_INTERNE.equals(modDico.get("modDom")) || EOModePaiement.MODDOM_A_EXTOURNER.equals(modDico.get("modDom"))) {
			if (modDico.get("planComptablePaiement") != null) {
				throw new DataCheckException("Le compte paiement ne doit pas être défini pour le domaine " + modDico.get("modDom"));
			}
		}

		//        if (EOModePaiement.MODDOM_INTERNE.equals(modDico.get("modDom")) ||  EOModePaiement.MODDOM_AUTRE.equals(modDico.get("modDom"))  ) {
		//            if (modDico.get("planComptableVisa")!=null) {
		//                if (   !EOPlanComptable.CLASSE4.equals(((EOPlanComptable)modDico.get("planComptableVisa")).getClasseCompte())   ) {
		//                    throw new DataCheckException("Le compte visa doit être de la classe " + EOPlanComptable.CLASSE4 +" pour le domaine " + modDico.get("modDom") );
		//                }
		//            }
		//        }        

		if (modDico.get("planComptablePaiement") != null) {
			if (!EOPlanComptable.CLASSE5.equals(((EOPlanComptable) modDico.get("planComptablePaiement")).getClasseCompte())) {
				throw new DataCheckException("Le compte de paiement doit être de la classe " + EOPlanComptable.CLASSE5);
			}
		}

		if (EOModePaiement.EMA_AUTO_OUI.equals(modDico.get("modEmaAuto"))) {
			if (modDico.get("planComptableVisa") == null) {
				throw new DataCheckException("Si vous voulez activer les émargements semi-automatiques pour ce mode de paiement, vous devez définir une imputation visa.");
			}
		}

		if (EOModePaiement.PAIEMENT_HT_OUI.equals(modDico.get(EOModePaiement.MOD_PAIEMENT_HT_KEY))) {
			if (modDico.get(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY) == null) {
				throw new NSValidation.ValidationException("Les mandats passés avec ce mode de paiement seront payés au fournisseur en HT, vous devez donc indiquer le compte de TVA en crédit à utiliser pour l'écriture de TVA.");
			}
		}

	}

	public final void initDicoWithDefault() {
		modDico.put("modCode", null);
		modDico.put("modDom", EOModePaiement.MODDOM_VIREMENT);
		modDico.put("modLibelle", null);
		modDico.put("exercice", myApp.appUserInfo().getCurrentExercice());
		modDico.put("planComptablePaiement", null);
		modDico.put("planComptableVisa", null);
		modDico.put("modEmaAuto", EOModePaiement.EMA_AUTO_NON);
		modDico.put(EOModePaiement.MOD_PAIEMENT_HT_KEY, EOModePaiement.PAIEMENT_HT_NON);
		modDico.put(EOModePaiement.MOD_CONTREPARTIE_GESTION_KEY, null);
	}

	public final void initDicoWithObject() {
		ZLogger.debug("");
		modDico.put("modCode", currentMod.modCode());
		modDico.put("modDom", currentMod.modDom());
		modDico.put("modLibelle", currentMod.modLibelle());
		modDico.put("exercice", currentMod.exercice());
		modDico.put("planComptablePaiement", currentMod.planComptablePaiement());
		modDico.put("planComptableVisa", currentMod.planComptableVisa());
		modDico.put("modEmaAuto", currentMod.modEmaAuto());
		modDico.put(EOModePaiement.PLAN_COMPTABLE_TVA_KEY, currentMod.planComptableTva());
		modDico.put(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY, currentMod.planComptableTvaCtp());
		modDico.put(EOModePaiement.MOD_PAIEMENT_HT_KEY, currentMod.modPaiementHt());
		modDico.put(EOModePaiement.MOD_CONTREPARTIE_GESTION_KEY, currentMod.modContrepartieGestion());
	}

	private final ZKarukeraDialog createModalDialog(Dialog dial) {
		win = new ZKarukeraDialog(dial, TITLE, true);
		setMyDialog(win);
		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	/**
     * 
     */
	private void initGUI() {
		planComptableSelectionDialog = createPlanComptableSelectionDialog();
		myPanel.initGUI();
	}

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	}

	private final NSArray getPcoValidesForVisa() {
		final EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat("pcoNum like %@ or pcoNum like %@", new NSArray(new Object[] {
				"6*", "7*"
		}));
		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), new EONotQualifier(qual1), false);
	}

	private final NSArray getPcoValidesForPaiement() {
		final EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat("pcoNum like %@ or pcoNum like %@", new NSArray(new Object[] {
				"4*", "5*"
		}));
		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), qual1, false);
	}

	private final NSArray getPcoValidesForTva() {
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, "445*");
		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), qual, false);
	}

	private final NSArray getPcoValidesForTvaCtp() {
		final EOQualifier qual = new EOKeyValueQualifier(EOPlanComptableExer.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, "445*");
		return EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), qual, false);
	}

	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		if (planComptableSelectionDialog.open(dgPlancomptable.displayedObjects()) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;

	}

	/**
	 * Gere la selection d'une plancomptable de visa
	 */
	private final void onSelectPlancoVisa() {
		EOPlanComptable res = null;
		//on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(getPcoValidesForVisa());
		res = selectPlanComptableIndDialog((String) modDico.get("pcoNumVisa"));

		if (res != null) {
			modDico.put("planComptableVisa", res);
			modDico.put("pcoNumVisa", res.pcoNum());
		}

		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Gere la selection d'une plancomptable de paiement
	 */
	private final void onSelectPlancoPaiement() {
		EOPlanComptable res = null;
		//on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(getPcoValidesForPaiement());
		res = selectPlanComptableIndDialog((String) modDico.get("pcoNumPaiement"));

		if (res != null) {
			modDico.put("planComptablePaiement", res);
			modDico.put("pcoNumPaiement", res.pcoNum());
		}
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSupprimerPlancoPaiement() {
		modDico.put("planComptablePaiement", null);
		modDico.put("pcoNumPaiement", null);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSelectPlancoTva() {
		EOPlanComptable res = null;
		//on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(getPcoValidesForTva());
		res = selectPlanComptableIndDialog((String) modDico.get(EOModePaiement.PCO_NUM_TVA_KEY));

		if (res != null) {
			modDico.put(EOModePaiement.PLAN_COMPTABLE_TVA_KEY, res);
			modDico.put(EOModePaiement.PCO_NUM_TVA_KEY, res.pcoNum());
		}
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSupprimerPlancoTva() {
		modDico.put(EOModePaiement.PLAN_COMPTABLE_TVA_KEY, null);
		modDico.put(EOModePaiement.PCO_NUM_TVA_KEY, null);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSelectPlancoTvaCtp() {
		EOPlanComptable res = null;
		//on affiche une fenetre de recherche de PlanComptable
		dgPlancomptable.setObjectArray(getPcoValidesForTvaCtp());
		res = selectPlanComptableIndDialog((String) modDico.get(EOModePaiement.PCO_NUM_TVA_CTP_KEY));

		if (res != null) {
			modDico.put(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY, res);
			modDico.put(EOModePaiement.PCO_NUM_TVA_CTP_KEY, res.pcoNum());
		}
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSupprimerPlancoTvaCtp() {
		modDico.put(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY, null);
		modDico.put(EOModePaiement.PCO_NUM_TVA_CTP_KEY, null);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onSupprimerPlancoVisa() {
		modDico.put("planComptableVisa", null);
		modDico.put("pcoNumVisa", null);
		try {
			myPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final EOModePaiement openDialogNew(Dialog dial) {
		ZKarukeraDialog win1 = createModalDialog(dial);
		//        this.setMyDialog(win);
		try {
			mode = MODE_NEW;
			newSaisie();
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
		return currentMod;
	}

	public final EOModePaiement openDialogModify(Dialog dial, final EOModePaiement leMod) {
		if (leMod == null) {
			return null;
		}

		ZKarukeraDialog win1 = createModalDialog(dial);
		//        this.setMyDialog(win);
		try {
			mode = MODE_MODIF;
			modifySaisie(leMod);
			win1.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win1.dispose();
		}
		return currentMod;
	}

	public final void newSaisie() {
		try {
			currentMod = null;
			initDicoWithDefault();
			myPanel.updateData();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public final void modifySaisie(EOModePaiement leMod) {
		try {
			currentMod = leMod;
			initDicoWithObject();
			myPanel.updateData();
			myPanel.lockForModify();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public final class ActionAnnuler extends AbstractAction {

		public ActionAnnuler() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	public final class ActionValider extends AbstractAction {
		public ActionValider() {
			super("Enregistrer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	private final class ActionPlancoVisaSelect extends AbstractAction {
		public ActionPlancoVisaSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour le visa");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoVisa();
		}
	}

	private final class ActionPlancoVisaSupprimer extends AbstractAction {
		public ActionPlancoVisaSupprimer() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le compte pour le visa");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSupprimerPlancoVisa();
		}
	}

	private final class ActionPlancoTvaSelect extends AbstractAction {
		public ActionPlancoTvaSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour le visa");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoTva();
		}
	}

	private final class ActionPlancoTvaSupprimer extends AbstractAction {
		public ActionPlancoTvaSupprimer() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le compte pour la TVA");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSupprimerPlancoTva();
		}
	}

	private final class ActionPlancoTvaCtpSelect extends AbstractAction {
		public ActionPlancoTvaCtpSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour la TVA en crédit");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoTvaCtp();
		}
	}

	private final class ActionPlancoTvaCtpSupprimer extends AbstractAction {
		public ActionPlancoTvaCtpSupprimer() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSupprimerPlancoTvaCtp();
		}
	}

	private final class ActionPlancoPaiementSupprimer extends AbstractAction {
		public ActionPlancoPaiementSupprimer() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le compte pour le paiement");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSupprimerPlancoPaiement();
		}
	}

	private final class ActionPlancoPaiementSelect extends AbstractAction {
		public ActionPlancoPaiementSelect() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner un compte pour le paiement");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSelectPlancoPaiement();
		}
	}

	private final class ModSaisiePanelListener implements ModPaiementSaisiePanel.IModSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#dicoValues()
		 */
		public HashMap dicoValues() {
			return modDico;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionAnnuler()
		 */
		public Action actionAnnuler() {
			return actionAnnuler;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#getDomainesModel()
		 */
		public DefaultComboBoxModel getDomainesModel() {
			return domainesModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionPlancomptableVisaSelect()
		 */
		public AbstractAction actionPlancomptableVisaSelect() {
			return actionPlancoVisaSelect;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionPlancomptablePaiementSelect()
		 */
		public AbstractAction actionPlancomptablePaiementSelect() {
			return actionPlancoPaiementSelect;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionPlancomptablePaiementSupprimer()
		 */
		public AbstractAction actionPlancomptablePaiementSupprimer() {
			return actionPlancoPaiementSupprimer;
		}

		/**
		 * @see org.cocktail.maracuja.client.administration.ui.ModPaiementSaisiePanel.IModSaisiePanelListener#actionPlancomptableVisaSupprimer()
		 */
		public AbstractAction actionPlancomptableVisaSupprimer() {
			return actionPlancoVisaSupprimer;
		}

		public DefaultComboBoxModel getContrePartieGestionMdl() {
			return contrePartieGestionMdl;
		}

		public AbstractAction actionPlancomptableTvaCtpSelect() {
			return actionPlancoTvaCtpSelect;
		}

		public AbstractAction actionPlancomptableTvaSelect() {
			return actionPlancoTvaSelect;
		}

		public Action actionPlancomptableTvaCtpSupprimer() {
			return actionPlancoTvaCtpSupprimer;
		}

		public Action actionPlancomptableTvaSupprimer() {
			return actionPlancoTvaSupprimer;
		}

	}

	public EOModePaiement getCurrentMod() {
		return currentMod;
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
