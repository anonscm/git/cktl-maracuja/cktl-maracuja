/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.factory.process.FactoryProcessModeRecouvrement;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModRecouvrementRechercheCtrl extends CommonCtrl {
    private static final String TITLE = "Gestion des modes de recouvrement";

    private final Dimension WINDOW_DIMENSION=new Dimension(843,440); 

    private final ActionClose actionClose = new ActionClose();
    private final ActionModDevalider actionModAnnuler = new ActionModDevalider();
    private final ActionModValider actionModValider = new ActionModValider();
    private final ActionNew actionNew = new ActionNew();
    private final ActionImprimer actionImprimer = new ActionImprimer();
    private final ActionModify actionModify = new ActionModify();
    
//    private final ActionSrch actionSrch = new ActionSrch();
    
    private ZKarukeraDialog win;  
    private HashMap modFilters;    
    private ModRecouvrementRecherchePanel myPanel;
    private FactoryProcessModeRecouvrement myFactoryProcessModeDeRecouvrement;
    

    /**
     * @param editingContext
     */
    public ModRecouvrementRechercheCtrl(EOEditingContext editingContext) {
        super(editingContext);
        revertChanges();
        modFilters = new HashMap();
        initSubObjects();

    }

    public void initSubObjects() {
        myPanel = new ModRecouvrementRecherchePanel(new ModRecherchePanelListener());
        myFactoryProcessModeDeRecouvrement = new FactoryProcessModeRecouvrement(myApp.wantShowTrace(), null);
    }
    
    
    private void initGUI() {
        myPanel.initGUI();
    }
    
    
    
    
    
    

//    /**
//     * @see org.cocktail.maracuja.client.mod.ui.ModRechercheFilterPanel.IModRechercheFilterPanel#onSrch()
//     */
//    private final void onSrch() {
//        try {
//            myPanel.updateData();
//        } catch (Exception e) {
//            showErrorDialog(e);
//        }
//    }    
//    
    

    
	
	
    
    protected NSMutableArray buildFilterQualifiers(final HashMap dicoFiltre) throws Exception {
        NSMutableArray quals = new NSMutableArray();

        //Construire les qualifiers à partir des saisies utilisateur
        ///Numero
        NSMutableArray qualsNum = new NSMutableArray();
        if (qualsNum.count()>0) {
            quals.addObject( new EOAndQualifier(qualsNum));
        }
        return quals;
    }    
   
        
    
    private final void modNew() {
        ModRecouvrementSaisieCtrl opdSaisieCtrl = new ModRecouvrementSaisieCtrl( getEditingContext(),myPanel.getMyDialog());
        EOModeRecouvrement mod = opdSaisieCtrl.openDialogNew(getMyDialog());
        if (mod !=null) {
            try {
                myPanel.updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }
    }
    
    
    
    private final void modModifier() {
//        showInfoDialog("Cette fonction est désactivée pour l'instant");
        try {
	        //Vérifier que l'utilisateur est seul connecté
	        NSArray users = ServerProxy.clientSideRequestGetConnectedUsers(getEditingContext());
	        if (users.count()>1) {
	            throw new DefaultClientException("Pour utiliser cette fonctionalité, vous devez être la seule personne connectée à l'application. Actuellement " + users.count() +" utilisateurs sont connectés. Pour les visualiser, utiliser la fonction Outils/Utilisateurs connectés.");
	        }
	        
	        ModRecouvrementSaisieCtrl opdSaisieCtrl = new ModRecouvrementSaisieCtrl( getEditingContext(),myPanel.getMyDialog());
	        EOModeRecouvrement mod = opdSaisieCtrl.openDialogModify(getMyDialog(), (EOModeRecouvrement)myPanel.getSelectedMod() );
	        if (mod !=null) {
	                myPanel.updateData();
	        }
	    } catch (Exception e) {
	        showErrorDialog(e);
	    }
    }
    
    private final void modAnnuler() {
        try {
            EOModeRecouvrement mod = (EOModeRecouvrement) myPanel.getSelectedMod();
            if (mod == null) {
                throw new DataCheckException("Aucun mode de recouvrement sélectionné");
            }
            
            //TODO vérifier si le mode de recouvrement peut être annulé
            
            boolean goOn = showConfirmationDialog("Confirmation", "Souhaitez-vous passer le mode de recouvrement " + mod.modLibelle()+" à l'état non valide ?\n Si vous répondez oui, il ne sera plus utilisable.", ZMsgPanel.BTLABEL_NO);
//            System.out.println("ModRechercheCtrl.modAnnuler() " + mod);
            if (goOn) {
//                System.out.println("ModRechercheCtrl.modAnnuler() " + mod);
                myFactoryProcessModeDeRecouvrement.invaliderModeRecouvrement(getEditingContext(), mod);
                getEditingContext().saveChanges();
                refreshActions();
                myPanel.getModListPanel().getMyTableModel().fireTableCellUpdated( ((Integer)myPanel.getModListPanel().getMyTableModel().getMyDg().selectionIndexes().objectAtIndex(0)).intValue(), myPanel.getModListPanel().getMyTableModel().findColumn("modValidite") );
            }
        }
        catch (Exception e) {
            if (getEditingContext().hasChanges()) {
                getEditingContext().revert();
            }
            showErrorDialog(e);
        }
    }
    private final void modValider() {
        try {
            EOModeRecouvrement mod = (EOModeRecouvrement) myPanel.getSelectedMod();
            if (mod == null) {
                throw new DataCheckException("Aucun mode de recouvrement sélectionné");
            }
            
            //TODO vérifier si le mode de recouvrement peut être validé
            boolean goOn = showConfirmationDialog("Confirmation", "Souhaitez-vous passer le mode de recouvrement " + mod.modLibelle()+" à l'état valide ?", ZMsgPanel.BTLABEL_NO);
//            System.out.println("ModRechercheCtrl.modValider() " + mod);
            if (goOn) {
//                System.out.println("ModRechercheCtrl.modValider() " + mod);
                myFactoryProcessModeDeRecouvrement.validerModeRecouvrement(getEditingContext(), mod);
                
                getEditingContext().saveChanges();
                refreshActions();
                myPanel.getModListPanel().getMyTableModel().fireTableCellUpdated( ((Integer)myPanel.getModListPanel().getMyTableModel().getMyDg().selectionIndexes().objectAtIndex(0)).intValue(), myPanel.getModListPanel().getMyTableModel().findColumn("modValidite") );
            }
        }
        catch (Exception e) {
            if (getEditingContext().hasChanges()) {
                getEditingContext().revert();
            }
            showErrorDialog(e);
        }
    }
    
    private void modImprimer() {
//	    try {
//	        String filePath = ReportFactoryClient.imprimerModeRecouvrement(myApp.editingContext(), myApp.temporaryDir , null);
//	        if (filePath!=null) {
//	            myApp.openPdfFile(filePath);
//	        }
//	    }
//	    catch (Exception e1) {
//	        showErrorDialog(e1);
//	    }          
    }
       
    private void fermer() {
        getEditingContext().revert();
        win.onCloseClick();
    }
    
    
    private final ZKarukeraDialog createModalDialog(Dialog dial ) {
        win = new ZKarukeraDialog(dial, TITLE, true);
        setMyDialog(win);
        myPanel.setMyDialog(win);
        myPanel.setPreferredSize(WINDOW_DIMENSION);
        initGUI();
        
        win.setContentPane(myPanel);
        win.pack();
        return win;
    }    
    
    private final ZKarukeraDialog createModalDialog(Frame dial ) {
        win = new ZKarukeraDialog(dial, TITLE, true);
        setMyDialog(win);
        myPanel.setMyDialog(win);
        myPanel.setPreferredSize(WINDOW_DIMENSION);
        initGUI();
        
        win.setContentPane(myPanel);
        
        win.pack();
        return win;
    }     
    
    
    /**
     * Ouvre un dialog de recherche.
     */
    public final void openDialog(Dialog dial) {
        ZKarukeraDialog win1 = createModalDialog(dial);
        try {
            myPanel.updateData();
            win1.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win1.dispose();
        }
    }
    
    public final void openDialog(Frame dial) {
        ZKarukeraDialog win1 = createModalDialog(dial);
        try {
            myPanel.updateData();
            win1.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win1.dispose();
        }
    }
        
    
    
    private final void refreshActions() {
        EOModeRecouvrement mod = (EOModeRecouvrement) myPanel.getSelectedMod();
        if (mod == null) {
            actionModValider.setEnabled(false);
            actionModAnnuler.setEnabled(false);
            actionModify.setEnabled(false);
        }
        else {
            actionModify.setEnabled(true);
            actionModValider.setEnabled(  !EOModeRecouvrement.etatValide.equals(mod.modValidite())  );
            actionModAnnuler.setEnabled(  !actionModValider.isEnabled() );
        }
        
    }
        
    
    
    private final class ModRecherchePanelListener implements ModRecouvrementRecherchePanel.IModRecherchePanelListener {

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#actionClose()
         */
        public Action actionClose() {
            return actionClose;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#actionDelete()
         */
        public Action actionDelete() {
            return actionModAnnuler;
        }


        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#actionNew()
         */
        public Action actionNew() {
            return actionNew;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#getMods()
         */
        public NSArray getMods() {
            try {
                NSMutableArray quals = new NSMutableArray();
                quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(myApp.appUserInfo().getCurrentExercice())));
                quals.addObject(new EOAndQualifier(buildFilterQualifiers(modFilters)));
                
                
                NSMutableArray sort = new NSMutableArray();
                sort.addObject(EOSortOrdering.sortOrderingWithKey("modLibelle",EOSortOrdering.CompareAscending));
                System.out.println("sort = " + sort);

                return ZFinder.fetchArray(getEditingContext(), "ModeRecouvrement", new EOAndQualifier(quals), sort, true);
     	    } catch (Exception e) {
     	        showErrorDialog(e);
     	        return new NSArray();
     	    }            
            
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#getActionImprimer()
         */
        public Action getActionImprimer() {
            return actionImprimer;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#getFilters()
         */
        public HashMap getFilters() {
            return modFilters;
        }

//        /**
//         * @see org.cocktail.maracuja.client.mod.ui.ModRecherchePanel.IModRecherchePanelListener#actionSrch()
//         */
//        public Action actionSrch() {
//            return actionSrch;
//        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#actionModValider()
         */
        public Action actionModValider() {
            return actionModValider;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#onSelectionChanged()
         */
        public void onSelectionChanged() {
            refreshActions();
            
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#actionModify()
         */
        public Action actionModify() {
            return actionModify;
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModRecouvrementRecherchePanel.IModRecherchePanelListener#onDbClick()
         */
        public void onDbClick() {
            modModifier();
        }

    }
    
    

    private final class ActionNew extends AbstractAction {
        public ActionNew() {
            super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Créer un nouveau mode de recouvrement");
        }

        public void actionPerformed(ActionEvent e) {
            modNew();
        }
    }        

    private final class ActionModify extends AbstractAction {
        public ActionModify() {
            super("Modifier");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EDIT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Modifier le mode de recouvrement sélectionné");
        }

        public void actionPerformed(ActionEvent e) {
                modModifier();
        }
    }     
    
    private final class ActionModDevalider extends AbstractAction {
        public ActionModDevalider() {
            super("Dévalider");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REDLED_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Dévalider le mode de recouvrement sélectionné, il ne sera plus utilisable");
        }

        public void actionPerformed(ActionEvent e) {
                modAnnuler();
        }
    }     
    private final class ActionModValider extends AbstractAction {
        public ActionModValider() {
            super("Valider");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_GREENLED_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Valider le mode de recouvrement sélectionné");
        }

        public void actionPerformed(ActionEvent e) {
                modValider();
        }
    }     

    private final class ActionImprimer extends AbstractAction {
        public ActionImprimer() {
            super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Imprimer la liste des modes de recouvrement");
        }
        public void actionPerformed(ActionEvent e) {
                modImprimer();
        }
    }  
    

    
    
	public final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
	    

        public void actionPerformed(ActionEvent e) {
          fermer();
        }
	    
	}

//    private final class ActionSrch extends AbstractAction {
//        public ActionSrch() {
//            super();
//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
//			putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
//        }
//        /**
//         * Appelle updateData();
//         */
//        public void actionPerformed(ActionEvent e) {
//            onSrch();
//        }
//    } 
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return myPanel;
    }

    public String title() {
        return TITLE;
    }
    
    
}
