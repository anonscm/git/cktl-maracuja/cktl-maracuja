/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;

import org.cocktail.maracuja.client.ApplicationClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.administration.ui.GestionSaisiePanel;
import org.cocktail.maracuja.client.administration.ui.GestionSaisiePanel.IAgregatGestionTablePanelMdl;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.factory.FactoryGestion;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOGestionAgregat;
import org.cocktail.maracuja.client.metier.EOGestionAgregatRepart;
import org.cocktail.maracuja.client.metier.EOGestionExercice;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;
import org.cocktail.zutil.client.wo.table.ZTablePanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

public final class GestionSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Administration des codes gestion";
	// private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_ADGE;
	private static final Dimension WINDOW_DIMENSION = new Dimension(890, 550);

	private final ActionCancel actionCancel = new ActionCancel();
	private final ActionClose actionClose = new ActionClose();
	private final ActionEnregistrer actionEnregistrer = new ActionEnregistrer();
	private final ActionAdd actionAdd = new ActionAdd();
	// private final ActionAddAll actionAddAll = new ActionAddAll();
	private final ActionRemove actionRemove = new ActionRemove();
	// private final ActionRemoveAll actionRemoveAll = new ActionRemoveAll();
	private final ActionNew actionNew = new ActionNew();
	private final ActionNewAgregat actionNewAgregat = new ActionNewAgregat();
	private final ActionDelete actionDelete = new ActionDelete();
	private final ActionDeleteAgregat actionDeleteAgregat = new ActionDeleteAgregat();

	private final ZEOComboBoxModel exercicesModel;

	private final GestionSaisiePanel panel;
	private final GestionSaisiePanelModel model;

	private final GestionExerciceTableListener gestionExerciceTableListener;
	private final GestionTableListener gestionTableListener;

	private final AgregatGestionTableListener agregatGestionTableListener;
	private final AgregatTableListener agregatTableListener;

	private final CheckGestionModifier checkGestionModifier = new CheckGestionModifier();
	// private boolean isEditing;

	/**
	 * Map contenant en clés des EOExercice et en valeurs des NSMutableArray de EOGestionExercice utilisé pour afficher les gestionExercice
	 */
	private final HashMap gestionExercicesMap = new HashMap();
	private final HashMap gestionsMap = new HashMap();
	//	private final HashMap agregatGestionsMap = new HashMap();
	//	private final HashMap agregatsMap = new HashMap();
	private final HashMap<EOExercice, HashMap<EOGestionAgregat, HashMap<EOGestionExercice, Boolean>>> allCheckedGestionForAgregatByExercice = new HashMap<EOExercice, HashMap<EOGestionAgregat, HashMap<EOGestionExercice, Boolean>>>();

	private NSMutableArray gestions;
	private final NSArray exercices;

	private Integer exeOrdreCurrent;
	private final NSArray planComptables;

	private final PcoNum185Modifier pcoNum185Modifier = new PcoNum185Modifier();
	private final PcoNum185CtpModifier pcoNum185CtpModifier = new PcoNum185CtpModifier();
	private final PcoNum181Modifier pcoNum181Modifier = new PcoNum181Modifier();
	private final GesLibelleModifier gesLibelleModifier = new GesLibelleModifier();

	public GestionSaisieCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		planComptables = EOPlanComptableFinder.getPlancosValides(getEditingContext(), true);
		exercices = EOsFinder.getAllExercices(getEditingContext());

		refreshGestions();
		refreshFromEditingContext();

		final NumberFormat f = NumberFormat.getIntegerInstance();
		f.setGroupingUsed(false);
		exercicesModel = new ZEOComboBoxModel(exercices, "exeExercice", null, f);

		gestionExerciceTableListener = new GestionExerciceTableListener();
		gestionTableListener = new GestionTableListener();

		agregatGestionTableListener = new AgregatGestionTableListener();
		agregatTableListener = new AgregatTableListener();
		majExerciceCurrent();
		model = new GestionSaisiePanelModel();

		panel = new GestionSaisiePanel(model);

		majExerciceCurrent();
	}

	private final ZKarukeraDialog createModalDialog(final Window dial) {
		final ZKarukeraDialog win;

		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		panel.setMyDialog(getMyDialog());
		panel.initGUI();
		panel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(panel);
		win.pack();
		return win;
	}

	//
	//	public NSArray getAllAgregats() {
	//		NSArray resAgregats = EOGestionAgregat.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOGestionAgregat.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, selectedExercice()), new NSArray(new Object[] {
	//				EOGestionAgregat.SORT_GA_LIBELLE_ASC
	//		}));
	//
	//		return resAgregats;
	//	}

	//	/**
	//	 * Met à jour le détail des agregats
	//	 */
	//	public void refreshCheckedGestionForAgregat() {
	//		allCheckedGestionForAgregatByExercice.clear();
	//		for (int i = 0; i < exercices.count(); i++) {
	//			final EOExercice exercice = (EOExercice) exercices.objectAtIndex(i);
	//
	//			NSArray resAgregats = EOGestionAgregat.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOGestionAgregat.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, element), new NSArray(new Object[] {
	//					EOGestionAgregat.SORT_GA_LIBELLE_ASC
	//			}));
	//
	//		}
	//
	//	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final int openDialog(final Window dial) {
		revertChanges();
		final ZKarukeraDialog win = createModalDialog(dial);
		setMyDialog(win);
		int res = ZKarukeraDialog.MRCANCEL;
		try {
			initSaisie();
			actionEnregistrer.setEnabled(false);
			actionCancel.setEnabled(false);

			// onEditingContextChanged();
			res = win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return res;
	}

	private final void initSaisie() throws Exception {
		panel.updateData();
	}

	//    
	//    
	// private void cleanBeforeSave() {
	//        
	// }

	private final boolean saveInfos() {
		boolean ret = false;
		try {
			performBeforeAction();
			synchroniseEOAgregatGestionsFromMap();

			//			ZLogger.debug(getEditingContext());
			//			System.out.println(getEditingContext().insertedObjects());
			//			System.out.println(getEditingContext().updatedObjects());

			getEditingContext().lock();
			getEditingContext().saveChanges();
			onEditingContextChanged();
			refreshFromEditingContext();
			panel.updateData();
			// getEditingContext().revert();
			ret = true;
		} catch (Exception e) {
			showErrorDialog(e);
			ret = false;
			// getEditingContext().revert();
		} finally {
			getEditingContext().unlock();
		}
		return ret;

	}

	public void cancelInfos() {
		try {
			performBeforeAction();
			getEditingContext().revert();
			refreshFromEditingContext();
			onEditingContextChanged();
			panel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	public void close() {
		performBeforeAction();
		if (getEditingContext().hasChanges()) {
			if (showConfirmationDialog("Confirmation", "Vous n'avez pas enregistré vos modifications. Souhaitez-vous les enregistrer avant de sortir ?\n" + "Si vous répondez Non, les modifications seront perdues.", ZMsgPanel.BTLABEL_YES)) {
				if (!saveInfos()) {
					return;
				}
			}
		}
		getEditingContext().revert();
		getMyDialog().onCancelClick();
	}

	// private final NSArray getGestions() throws Exception {
	// return EOsFinder.getAllGestions(getEditingContext());
	// }
	//    

	private final void refreshGestions() throws Exception {
		gestions = EOsFinder.getAllGestions(getEditingContext()).mutableClone();
	}

	private final void refreshFromEditingContext() throws Exception {
		gestionExercicesMap.clear();
		gestionsMap.clear();
		allCheckedGestionForAgregatByExercice.clear();
		//	agregatGestionsMap.clear();
		//	agregatsMap.clear();
		for (int i = 0; i < exercices.count(); i++) {
			final EOExercice element = (EOExercice) exercices.objectAtIndex(i);

			NSArray res = new NSArray();
			NSArray resAgregats = new NSArray();
			try {
				res = EOsFinder.getAllGestionExercicesPourExercice(getEditingContext(), element);
				resAgregats = EOGestionAgregat.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOGestionAgregat.TO_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, element), new NSArray(new Object[] {
						EOGestionAgregat.SORT_GA_LIBELLE_ASC
				}));
			} catch (ZFinderException e) {
				// e.printStackTrace();
			}

			final NSMutableArray currentGestionExercices = res.mutableClone();
			final NSArray gestionsDansExercice = (NSArray) currentGestionExercices.valueForKey(EOGestionExercice.GESTION_KEY);
			final NSMutableArray gestionsHorsExercice = new NSMutableArray();

			for (int j = 0; j < gestions.count(); j++) {
				EOGestion gestion = (EOGestion) gestions.objectAtIndex(j);
				if (gestionsDansExercice.indexOfObject(gestion) == NSArray.NotFound) {
					gestionsHorsExercice.addObject(gestion);
				}
			}
			gestionExercicesMap.put(element, currentGestionExercices);
			gestionsMap.put(element, gestionsHorsExercice);

			//	final NSMutableArray currentAgregats = resAgregats.mutableClone();
			//final NSArray gestionExercicesDansAgregats = (NSArray) currentAgregats.valueForKey(EOGestionAgregat.TO_GESTION_EXERCICES_KEY);
			HashMap<EOGestionAgregat, HashMap<EOGestionExercice, Boolean>> tmpAgregatMap = new HashMap<EOGestionAgregat, HashMap<EOGestionExercice, Boolean>>();
			for (int j = 0; j < resAgregats.count(); j++) {
				EOGestionAgregat agregat = (EOGestionAgregat) resAgregats.objectAtIndex(j);
				HashMap<EOGestionExercice, Boolean> tmpMap = new HashMap<EOGestionExercice, Boolean>();
				NSArray allGestionExercices = EOGestionExercice.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, element), null);
				for (int k = 0; k < allGestionExercices.count(); k++) {
					EOGestionExercice ge = (EOGestionExercice) allGestionExercices.objectAtIndex(k);
					tmpMap.put(ge, Boolean.valueOf(agregat.toGestionExercices().indexOfObject(ge) != NSArray.NotFound));
				}
				tmpAgregatMap.put(agregat, tmpMap);
			}
			allCheckedGestionForAgregatByExercice.put(element, tmpAgregatMap);
		}

	}

	private final NSArray getAgregatGestions() {
		if (selectedExercice() == null || selectedAgregat() == null) {
			return null;
		}

		//		return (NSArray) agregatGestionsMap.get(exercice);
		NSArray res = new NSArray(allCheckedGestionForAgregatByExercice.get(selectedExercice()).get(selectedAgregat()).keySet().toArray());
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				EOGestion.SORT_GES_CODE_ASC
		}));
		//		return new NSArray(allCheckedGestionForAgregatByExercice.get(selectedExercice()).get(selectedAgregat()).keySet().toArray());
	}

	private final NSArray getAgregats() {
		if (selectedExercice() == null) {
			return null;
		}
		//return (NSArray) agregatsMap.get(exercice);
		return new NSArray(allCheckedGestionForAgregatByExercice.get(selectedExercice()).keySet().toArray());
		//return allCheckedGestionForAgregatByExercice.get(selectedExercice()).keySet().
	}

	private final NSArray getGestionExercices() {
		EOExercice exercice = (EOExercice) exercicesModel.getSelectedEObject();
		if (exercice == null) {
			return null;
		}
		return (NSArray) gestionExercicesMap.get(exercice);
	}

	public EOExercice selectedExercice() {
		return (EOExercice) exercicesModel.getSelectedEObject();
	}

	private final NSArray getGestions() {
		EOExercice exercice = (EOExercice) exercicesModel.getSelectedEObject();
		if (exercice == null) {
			return null;
		}
		return (NSArray) gestionsMap.get(exercice);
	}

	private final void codeGestionNew() {
		try {

			String res = ZKarukeraPanel.askValueInDialog(getMyDialog(), "Nouveau code gestion", "Code gestion", 10);
			if (res != null) {
				res = res.trim();
				if (res.length() > 0) {
					// Vérifier si le code gestion existe déjà
					final ArrayList gesCodes = new ArrayList();
					for (int i = 0; i < gestions.count(); i++) {
						final EOGestion element = (EOGestion) gestions.objectAtIndex(i);
						gesCodes.add(element.gesCode());
					}
					if (gesCodes.indexOf(res) >= 0) {
						throw new DataCheckException("Le code gestion " + res + " existe déjà.");
					}

					final FactoryGestion factoryGestion = new FactoryGestion(ApplicationClient.wantShowTrace());

					final EOGestion gestion = factoryGestion.creerUnGestion(getEditingContext(), myApp.appUserInfo().getCurrentComptabilite(), res);
					gestions.addObject(gestion);

					// Mettre à jour gestionsMap
					for (int i = 0; i < exercices.count(); i++) {
						final NSMutableArray tmp = (NSMutableArray) gestionsMap.get(exercices.objectAtIndex(i));
						tmp.addObject(gestion);
					}
					panel.getGestionTablePanel().updateData();

					// refreshFromEditingContext();
					onEditingContextChanged();
				}
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void newAgregat() {
		try {

			String res = ZKarukeraPanel.askValueInDialog(getMyDialog(), "Nouvel agrégat", "Agrégat", 200);
			if (res != null) {
				res = res.trim();
				if (res.length() > 0) {
					res = ZStringUtil.chaineSansCaracteresSpeciauxUpper(res);
					EOGestionAgregat.valideLibelle(res);
					// Vérifier si l'agregat existe déjà
					NSArray libs = (NSArray) new NSArray(allCheckedGestionForAgregatByExercice.get(selectedExercice()).keySet().toArray()).valueForKey(EOGestionAgregat.GA_LIBELLE_KEY);
					if (libs.indexOfObject(res) != NSArray.NotFound) {
						throw new DataCheckException("L'agregat " + res + " existe déjà.");
					}

					EOGestionAgregat ga = EOGestionAgregat.creerInstance(getEditingContext());
					ga.setToExerciceRelationship(selectedExercice());
					ga.setGaLibelle(res);

					onEditingContextChanged();
					//mettre à jour la map

					HashMap<EOGestionExercice, Boolean> tmpMap = new HashMap<EOGestionExercice, Boolean>();
					NSArray allGestionExercices = EOGestionExercice.fetchAll(getEditingContext(), new EOKeyValueQualifier(EOGestionExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, selectedExercice()), null);
					for (int k = 0; k < allGestionExercices.count(); k++) {
						EOGestionExercice ge = (EOGestionExercice) allGestionExercices.objectAtIndex(k);
						tmpMap.put(ge, Boolean.FALSE);
					}

					allCheckedGestionForAgregatByExercice.get(selectedExercice()).put(ga, tmpMap);
					panel.getAgregatTablePanel().updateData();
					panel.getAgregatTablePanel().getMyEOTable().forceNewSelectionOfObjects(new NSArray(new Object[] {
							ga
					}));

				}
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void deleteAgregat() {
		try {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement supprimer l'agrégat " + selectedAgregat().gaLibelle() + " ?", ZMsgPanel.BTLABEL_NO)) {
				selectedAgregat().deleteAllToGestionAgregatRepartsRelationships();
				getEditingContext().deleteObject(selectedAgregat());

				saveInfos();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private final void codeGestionDelete() {
		// TODO VERIFIER SI GLOBALID PERMANent AUQUEL CAS INTERDIRE

	}

	private final void codeGestionRemove() {
		try {
			performBeforeAction();
			setWaitCursor(true);
			final EOExercice exercice = (EOExercice) exercicesModel.getSelectedEObject();
			final EOGestionExercice gestionExercice = (EOGestionExercice) panel.getGestionExerciceTablePanel().selectedObject();
			if (gestionExercice != null) {
				// supprimer l'objet gestion exercice selectionné
				((NSMutableArray) gestionExercicesMap.get(exercice)).removeObject(gestionExercice);

				// mettre a jour le tableau des codes gestions dispo pour
				// l'exercice
				final NSMutableArray array = (NSMutableArray) gestionsMap.get(exercice);
				if (array.indexOfObject(gestionExercice.gestion()) == NSArray.NotFound) {
					array.addObject(gestionExercice.gestion());
				}
				final FactoryGestion factoryGestion = new FactoryGestion(myApp.wantShowTrace());
				factoryGestion.supprimerGestionExercice(getEditingContext(), gestionExercice);
			}

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			try {
				onEditingContextChanged();
				panel.getGestionTablePanel().updateData();
				panel.getGestionExerciceTablePanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
			setWaitCursor(false);
		}

	}

	// private final void codeGestionAddAll() {
	// // Auto-generated method stub
	//        
	// }

	private final void codeGestionAdd() {
		try {
			performBeforeAction();
			setWaitCursor(true);
			final EOExercice exercice = (EOExercice) exercicesModel.getSelectedEObject();
			final EOGestion gestion = (EOGestion) panel.getGestionTablePanel().selectedObject();

			if (gestion != null && exercice != null) {
				// Créer un objet gestionExercice
				final FactoryGestion factoryGestion = new FactoryGestion(myApp.wantShowTrace());
				final EOGestionExercice gestionExercice = factoryGestion.creerUnGestionExercice(getEditingContext(), gestion, exercice, exeOrdreCurrent);
				gestionExercice.setGesLibelle(gestion.gesCode());

				// ajouter l'objet gestion exercice
				((NSMutableArray) gestionExercicesMap.get(exercice)).addObject(gestionExercice);

				// mettre a jour le tableau des codes gestions dispo pour
				// l'exercice
				final NSMutableArray array = (NSMutableArray) gestionsMap.get(exercice);
				if (array.indexOfObject(gestionExercice.gestion()) != NSArray.NotFound) {
					array.removeObject(gestion);
				}

				// getEditingContext().deleteObject(gestionExercice);
			}

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			try {

				panel.getGestionTablePanel().updateData();
				panel.getGestionExerciceTablePanel().updateData();
				onEditingContextChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
			setWaitCursor(false);
		}

	}

	private final class GestionSaisiePanelModel implements GestionSaisiePanel.IGestionSaisiePanelModel {

		public ZTablePanel.IZTablePanelMdl gestionTableListener() {
			return gestionTableListener;
		}

		public ComboBoxModel exercicesModel() {
			return exercicesModel;
		}

		// public Action actionRemoveAll() {
		// return actionRemoveAll;
		// }

		public Action actionRemove() {
			return actionRemove;
		}

		// public Action actionAddAll() {
		// return actionAddAll;
		// }

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionDelete() {
			return actionDelete;
		}

		public Action actionNew() {
			return actionNew;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionEnregistrer() {
			return actionEnregistrer;
		}

		public ZTablePanel.IZTablePanelMdl gestionExerciceTableListener() {
			return gestionExerciceTableListener;
		}

		public void onExerciceSelected() {
			majExerciceCurrent();

			cancelInfos();
		}

		public Modifier getPcoNum185Modifier() {
			return pcoNum185Modifier;
		}

		public Modifier getPcoNum185CtpModifier() {
			return pcoNum185CtpModifier;
		}

		public Modifier getPcoNum181Modifier() {
			return pcoNum181Modifier;
		}

		public Modifier getGesLibelleModifier() {
			return gesLibelleModifier;
		}

		public Modifier checkGestionModifier() {
			return checkGestionModifier;
		}

		//
		//		public HashMap getAllCheckedGestions() {
		//			if (selectedExercice() == null || selectedAgregat() == null) {
		//				return null;
		//			}
		//			if (getAgregats().count() == 0) {
		//				return new HashMap();
		//			}
		//			return allCheckedGestionForAgregatByExercice.get(selectedExercice()).get(selectedAgregat());
		//		}

		public IZTablePanelMdl agregatTableListener() {
			return agregatTableListener;
		}

		public IAgregatGestionTablePanelMdl agregatGestionTableListener() {
			return agregatGestionTableListener;
		}

		public Action actionNewAgregat() {
			return actionNewAgregat;
		}

		public Action actionDeleteAgregat() {
			return actionDeleteAgregat;
		}
	}

	private void majExerciceCurrent() {
		final EOExercice exercice = (EOExercice) exercicesModel.getSelectedEObject();
		if (exercice == null) {
			exeOrdreCurrent = null;
		}
		else {
			exeOrdreCurrent = (Integer) ServerProxy.serverPrimaryKeyForObject(getEditingContext(), exercice).valueForKey("exeOrdre");
		}
	}

	private final EOPlanComptable getPlanComptableForPcoNum(final String value) {
		EOPlanComptable planComptable = null;
		if (value != null) {
			// Vérifier si la chaine transmise correspond à un pcoNum valide
			final String pcoNum = value;
			for (int i = 0; i < planComptables.count() && planComptable == null; i++) {
				final EOPlanComptable element = (EOPlanComptable) planComptables.objectAtIndex(i);
				if (pcoNum.equals(element.pcoNum())) {
					planComptable = element;
				}
			}
		}
		return planComptable;
	}

	private final class PcoNum181Modifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			try {
				String val = (String) value;
				if (val != null) {
					val = val.trim();
				}

				final EOGestionExercice tmp = (EOGestionExercice) panel.getGestionExerciceTablePanel().selectedObject();
				final EOPlanComptable planComptable = getPlanComptableForPcoNum(val);

				// Affecter le planComptable trouvé
				if (planComptable == null) {
					if (value != null && val.length() > 0) {
						throw new DataCheckException("Le compte " + val + " ne correspond pas à un compte valide");
					}

					if (tmp.planComptable181() != null && !NSKeyValueCoding.NullValue.equals(tmp.planComptable181())) {
						tmp.setPlanComptable181Relationship(null);
						//                        tmp.removeObjectFromBothSidesOfRelationshipWithKey(tmp.planComptable181(), "planComptable181");
					}
				}
				else {
					tmp.setPlanComptable181Relationship(planComptable);
					//                    tmp.addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptable181");
				}

				onEditingContextChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	private final class PcoNum185Modifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			try {
				String val = (String) value;
				if (val != null) {
					val = val.trim();
				}
				final EOGestionExercice tmp = (EOGestionExercice) panel.getGestionExerciceTablePanel().selectedObject();
				final EOPlanComptable planComptable = getPlanComptableForPcoNum(val);

				// Affecter le planComptable trouvé
				if (planComptable == null) {
					if (val != null && val.length() > 0) {
						throw new DataCheckException("Le compte " + val + " ne correspond pas à un compte valide");
					}
					if (tmp.planComptable185() != null && !NSKeyValueCoding.NullValue.equals(tmp.planComptable185())) {
						tmp.setPlanComptable185Relationship(null);
						//						tmp.removeObjectFromBothSidesOfRelationshipWithKey(tmp.planComptable185(), "planComptable185");
					}
				}
				else {
					tmp.setPlanComptable185Relationship(planComptable);
					//					tmp.addObjectToBothSidesOfRelationshipWithKey(planComptable, "planComptable185");
				}
				onEditingContextChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	private final class PcoNum185CtpModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			try {
				String val = (String) value;
				if (val != null) {
					val = val.trim();
				}
				final EOGestionExercice tmp = (EOGestionExercice) panel.getGestionExerciceTablePanel().selectedObject();
				final EOPlanComptable planComptable = getPlanComptableForPcoNum(val);

				// Affecter le planComptable trouvé
				if (planComptable == null) {
					if (val != null && val.length() > 0) {
						throw new DataCheckException("Le compte " + val + " ne correspond pas à un compte valide");
					}
					if (tmp.planComptable185CtpSacd() != null && !NSKeyValueCoding.NullValue.equals(tmp.planComptable185CtpSacd())) {
						tmp.setPlanComptable185CtpSacdRelationship(null);
					}
				}
				else {
					tmp.setPlanComptable185CtpSacdRelationship(planComptable);
				}
				onEditingContextChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	private final class GesLibelleModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			try {
				final EOGestionExercice tmp = (EOGestionExercice) panel.getGestionExerciceTablePanel().selectedObject();
				String val = (String) value;
				if (val != null) {
					val = val.trim();
				}

				if (val == null || val.length() == 0) {
					val = tmp.gestion().gesCode();
				}

				tmp.setGesLibelle(val);
				onEditingContextChanged();

			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	private final class GestionExerciceTableListener implements ZTablePanel.IZTablePanelMdl {

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {
			return getGestionExercices();
		}

		public void onDbClick() {

		}

	}

	private final class GestionTableListener implements ZTablePanel.IZTablePanelMdl {

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {
			return getGestions();
		}

		public void onDbClick() {

		}

	}

	private final class AgregatGestionTableListener implements IAgregatGestionTablePanelMdl {

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {
			return getAgregatGestions();
		}

		public void onDbClick() {

		}

		public Map checkDico() {
			if (selectedExercice() == null || selectedAgregat() == null) {
				return null;
			}
			if (getAgregats().count() == 0) {
				return new HashMap();
			}
			return allCheckedGestionForAgregatByExercice.get(selectedExercice()).get(selectedAgregat());
		}
	}

	private final class AgregatTableListener implements ZTablePanel.IZTablePanelMdl {

		public void selectionChanged() {
			try {
				panel.getAgregatGestionTablePanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		public NSArray getData() throws Exception {
			return getAgregats();
		}

		public void onDbClick() {

		}

	}

	private void performBeforeAction() {
		panel.getGestionExerciceTablePanel().stopCellEditing();
	}

	private final class ActionEnregistrer extends AbstractAction {
		public ActionEnregistrer() {
			this.putValue(AbstractAction.NAME, "Enregistrer");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les modifications");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
		}

		public void actionPerformed(ActionEvent e) {
			saveInfos();
		}

	}

	private final class ActionCancel extends AbstractAction {
		public ActionCancel() {
			this.putValue(AbstractAction.NAME, "Annuler");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler les modifications");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
		}

		public void actionPerformed(ActionEvent e) {
			cancelInfos();
		}

	}

	private final class ActionClose extends AbstractAction {
		public ActionClose() {
			this.putValue(AbstractAction.NAME, "Fermer");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			close();
		}

	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			this.putValue(AbstractAction.NAME, "Nouveau");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouveau code gestion");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
		}

		public void actionPerformed(ActionEvent e) {
			codeGestionNew();
		}
	}

	private final class ActionNewAgregat extends AbstractAction {
		public ActionNewAgregat() {
			this.putValue(AbstractAction.NAME, "Nouveau");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouvel agrégat");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
		}

		public void actionPerformed(ActionEvent e) {
			newAgregat();
		}
	}

	private final class ActionDeleteAgregat extends AbstractAction {
		public ActionDeleteAgregat() {
			this.putValue(AbstractAction.NAME, "Supprimer");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le code gestion sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
		}

		public void actionPerformed(ActionEvent e) {
			deleteAgregat();
		}
	}

	private final class ActionDelete extends AbstractAction {
		public ActionDelete() {
			this.putValue(AbstractAction.NAME, "Supprimer");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le code gestion sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
		}

		public void actionPerformed(ActionEvent e) {
			codeGestionDelete();
		}
	}

	private final class ActionAdd extends AbstractAction {
		public ActionAdd() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter le code gestion pour l'exercice sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
		}

		public void actionPerformed(ActionEvent e) {
			codeGestionAdd();

		}
	}

	// private final class ActionAddAll extends AbstractAction {
	// public ActionAddAll() {
	// this.putValue(AbstractAction.NAME, ">>");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter tous les codes
	// gestion pour l'exercice sélectionné");
	// }
	// public void actionPerformed(ActionEvent e) {
	// codeGestionAddAll();
	// }
	// }

	private final class ActionRemove extends AbstractAction {
		public ActionRemove() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retirer le code gestion pour l'exercice sélectionné");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
		}

		public void actionPerformed(ActionEvent e) {
			codeGestionRemove();
		}
	}

	// private final class ActionRemoveAll extends AbstractAction {
	// public ActionRemoveAll() {
	// this.putValue(AbstractAction.NAME, "<<");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retirer tous les codes
	// gestion pour l'exercice sélectionné");
	// }
	// public void actionPerformed(ActionEvent e) {
	// codeGestionRemoveAll();
	// }
	// }

	/**
	 * A appeler quand une modif est faite sur les données
	 */
	private final void onEditingContextChanged() {
		if (getEditingContext().hasChanges()) {
			actionEnregistrer.setEnabled(true);
			actionCancel.setEnabled(true);
			actionDeleteAgregat.setEnabled(false);
			panel.getComboBoxExercice().setEnabled(false);
			panel.getTabs().setEnabled(false);
		}
		else {
			actionEnregistrer.setEnabled(false);
			actionCancel.setEnabled(false);
			actionDeleteAgregat.setEnabled(true);
			panel.getComboBoxExercice().setEnabled(true);
			panel.getTabs().setEnabled(true);
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return panel;
	}

	public String title() {
		return TITLE;
	}

	public EOGestionAgregat selectedAgregat() {
		if (panel == null) {
			return null;
		}
		return (EOGestionAgregat) panel.getAgregatTablePanel().selectedObject();
	}

	private class CheckGestionModifier implements ZEOTableModelColumn.Modifier {

		public void setValueAtRow(Object value, int row) {
			final Boolean val = (Boolean) value;

			final EOGestionExercice odp = (EOGestionExercice) panel.getAgregatGestionTablePanel().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
			try {
				if (val.booleanValue()) {
					EOGestionAgregatRepart newRepart = EOGestionAgregatRepart.creerInstance(getEditingContext());
					newRepart.setToGestionAgregatRelationship(selectedAgregat());
					newRepart.setToGestionExerciceRelationship(odp);
				}
				else {
					NSArray reparts = selectedAgregat().toGestionAgregatReparts(new EOKeyValueQualifier(EOGestionAgregatRepart.TO_GESTION_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, odp), false);
					if (reparts.count() > 0) {
						EOGestionAgregatRepart repart = (EOGestionAgregatRepart) reparts.objectAtIndex(0);
						repart.setToGestionAgregatRelationship(null);
						repart.setToGestionExerciceRelationship(null);
						getEditingContext().deleteObject(repart);
					}
				}
				onEditingContextChanged();
				allCheckedGestionForAgregatByExercice.get(selectedExercice()).get(selectedAgregat()).put(odp, val);
				panel.getAgregatGestionTablePanel().getMyTableModel().fireTableRowUpdated(row);

				//step2Panel.updateTotaux();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	/**
	 * met à jour l'editingContext avec les infos contenues dans la map de selection.
	 */
	private void synchroniseEOAgregatGestionsFromMap() {
		//		//supprimer et recréer les affectations
		//		Iterator<EOGestionAgregat> iterAgregat = allCheckedGestionForAgregatByExercice.get(selectedExercice()).keySet().iterator();
		//		while

	}
}
