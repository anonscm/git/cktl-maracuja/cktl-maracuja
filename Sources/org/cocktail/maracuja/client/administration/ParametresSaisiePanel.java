/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration;


import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ParametresSaisiePanel extends ZKarukeraTablePanel {
    private IParametresSaisiePanelListener myListener;
    
    public static final String PAR_KEY="parKey";
    public static final String PAR_DESCRIPTION="parDescription";
    public static final String PAR_VALUE="parValue";
  
    
    

    /**
     * @param editingContext
     */
    public ParametresSaisiePanel(IParametresSaisiePanelListener listener) {
        super(listener);
        myListener = listener;
        final ZEOTableModelColumn parKey = new ZEOTableModelColumn(myDisplayGroup, PAR_KEY, "Nom", 250);
        parKey.setAlignment(SwingConstants.LEFT);
        
        final ZEOTableModelColumn parValue = new ZEOTableModelColumn(myDisplayGroup, PAR_VALUE, "Valeur", 200);
        parValue.setAlignment(SwingConstants.LEFT);
        parValue.setEditable(true);
        
        final ZEOTableModelColumn parDescription = new ZEOTableModelColumn(myDisplayGroup, PAR_DESCRIPTION, "Description", 400);
        parDescription.setAlignment(SwingConstants.LEFT);
        
        colsMap.clear();
        colsMap.put(PAR_KEY ,parKey);        
        colsMap.put(PAR_VALUE ,parValue);        
        colsMap.put(PAR_DESCRIPTION ,parDescription);        
        
    }


    public interface IParametresSaisiePanelListener extends IZKarukeraTablePanelListener {
        public Action actionValider();
        public Action actionClose();
    }


    public void initGUI() {
        super.initGUI();
        
        JPanel comment = createCommentaireUI(getFont(), "", "Modification des paramètres de Maracuja");
        myEOTable.setBorder(BorderFactory.createEmptyBorder(0,4,4,4));
        
        add(comment, BorderLayout.NORTH);
        add(buildBottomPanel(), BorderLayout.SOUTH);
        
    }
    
    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionClose());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }    

}
