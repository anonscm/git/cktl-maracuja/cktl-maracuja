/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


/**
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ParametresSaisieCtrl extends CommonCtrl {
    private static final String TITLE = "Consultation / Modification des paramètres";
    private final Dimension WINDOW_DIMENSION=new Dimension(860,550);

//    private static final int MODE_NEW=0;
//    private static final int MODE_MODIF=1;
//    private final String ACTION_ID_SAISIE = ZActionCtrl.IDU_CHESA;
//    private int modeSaisie=MODE_NEW;


    private final ActionAnnuler actionAnnuler = new ActionAnnuler();
    private final ActionValider actionValider = new ActionValider();


    private ParametresSaisiePanel parametresSaisiePanel;
//    private EORecetteRelance currentEORecetteRelance;
//    private boolean modified;



    /**
     * @param editingContext
     */
    public ParametresSaisieCtrl(final EOEditingContext editingContext) {
        super(editingContext);
        parametresSaisiePanel = new ParametresSaisiePanel(new ParametresSaisiePanelListener());
    }


    private final NSArray getParametres() {
        return EOsFinder.getParametres(getEditingContext(), myApp.appUserInfo().getCurrentExercice());
    }



    private final ZKarukeraDialog createModalDialog(final Window dial ) {
        final ZKarukeraDialog win;
        
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE,true);
        }
        setMyDialog(win);
        parametresSaisiePanel.setMyDialog(getMyDialog());
        parametresSaisiePanel.initGUI();
        parametresSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
        win.setContentPane(parametresSaisiePanel);
        win.pack();
        return win;
    }





    /**
     * Ouvre un dialog de saisie.
     */
    public final int openDialog(final Window dial) {
        revertChanges();
        final ZKarukeraDialog win = createModalDialog(dial);
        setMyDialog(win);
        int res=ZKarukeraDialog.MRCANCEL;
        try {
            initSaisie();
            res = win.open();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
        return res;
    }



    private final boolean checkSaisie() throws Exception {
        boolean res = true;
        
        return res;
    }


    private final void validerSaisie() {
        try {
            if (checkSaisie()) {
                getEditingContext().saveChanges();
                getMyDialog().onOkClick();
            }
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
    }


    private final void initSaisie() throws Exception {
//        values = dico;

//        System.out.println();
//        System.out.println("RelanceSaisieCtrl.initSaisie()");
//        ZLogger.debug(values);
        parametresSaisiePanel.updateData();
//        refreshSaveAction();
    }

    public final class ActionValider extends AbstractAction {

        public ActionValider() {
            super("Ok");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
        }

        public void actionPerformed(final ActionEvent e) {
            validerSaisie();

        }

    }

    public final class ActionAnnuler extends AbstractAction {

        public ActionAnnuler() {
            super("Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
        }

        public void actionPerformed(final ActionEvent e) {
          getMyDialog().onCancelClick();
        }

    }


    private final class ParametresSaisiePanelListener implements ParametresSaisiePanel.IParametresSaisiePanelListener {

        /**
         * @see org.cocktail.maracuja.client.relances.ui.parametresSaisiePanel.IparametresSaisiePanelListener#actionClose()
         */
        public Action actionClose() {
            return actionAnnuler;
        }

        /**
         * @see org.cocktail.maracuja.client.relances.ui.parametresSaisiePanel.IparametresSaisiePanelListener#actionValider()
         */
        public Action actionValider() {
            return actionValider;
        }

        public void selectionChanged() {
            
        }

        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }

        public NSArray getData() throws Exception {
            return getParametres();
        }

        public void onDbClick() {
            
        }

    }
    
    
    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return parametresSaisiePanel;
    }

    public String title() {
        return TITLE;
    }    
}
