/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSeparator;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZFileBox;
import org.cocktail.zutil.client.ui.ZFileBox.IZFileBoxModel;

public class AdminBdfCalPanel extends ZKarukeraPanel {
	private final IAdminBdfCalPanelModel _model;
	private final ZFileBox fileBoxFichierBdf;

	public AdminBdfCalPanel(final IAdminBdfCalPanelModel model) {
		super();
		_model = model;

		fileBoxFichierBdf = new ZFileBox(_model.getFileBoxFichierBdfMdl());
		fileBoxFichierBdf.setPreferredSize(new Dimension(50, 50));
	}

	public void initGUI() {
		setLayout(new BorderLayout());
		add(buildTopPanel(), BorderLayout.NORTH);
		add(buildMainPanel(), BorderLayout.CENTER);
	}

	private final JPanel buildTopPanel() {
		final ZCommentPanel commentPanel = new ZCommentPanel("Calendrier de la Banque de France",
				"<html>Le calendrier de la Banque de France vous permet de connaitre les dates valides pour la génération des fichiers de prélèvements.</html>", null);
		return commentPanel;
	}

	private final JPanel buildButtonPanel() {
		final JPanel bPanel = new JPanel(new BorderLayout());
		final ArrayList actionList = new ArrayList();
		actionList.add(_model.actionClose());
		final Box box = buildHorizontalButtonsFromActions(actionList);
		bPanel.add(new JSeparator(), BorderLayout.PAGE_START);
		bPanel.add(box, BorderLayout.LINE_END);
		return bPanel;
	}

	private final JPanel buildMainPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		//        panel.add(buildCenterPanel(), BorderLayout.CENTER);
		panel.add(buildRightPanel(), BorderLayout.CENTER);
		return panel;
	}

	private Component buildCenterPanel() {
		final JPanel p = new JPanel(new BorderLayout());

		return p;
	}

	private final JPanel buildRightPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(_model.actionVoir());
		list.add(_model.actionImport());
		list.add(_model.actionClose());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}

	public void updateData() throws Exception {

	}

	public interface IAdminBdfCalPanelModel {
		public Action actionVoir();

		public IZFileBoxModel getFileBoxFichierBdfMdl();

		public Action actionClose();

		public Action actionImport();
	}

}
