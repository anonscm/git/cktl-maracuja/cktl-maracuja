/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModPaiementRecherchePanel extends ZKarukeraPanel {
    private IModRecherchePanelListener myListener;
    private ModPaiementListPanel modListPanel;
    

    

    public ModPaiementRecherchePanel(IModRecherchePanelListener listener) {
        super();
        myListener = listener;
        modListPanel = new ModPaiementListPanel( new ModListPanelListener());
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        modListPanel.initGUI();
        
        this.setLayout(new BorderLayout());
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.add(modListPanel , BorderLayout.CENTER);
        this.add(buildRightPanel(), BorderLayout.EAST);
        this.add(buildTopPanel(), BorderLayout.NORTH);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);
        this.add(tmp, BorderLayout.CENTER);
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {   
        modListPanel.updateData();
    }

    private final JPanel buildRightPanel() {
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        ArrayList list = new ArrayList();
        list.add(myListener.actionNew());
        list.add(myListener.actionModify());
        list.add(myListener.actionModValider());
        list.add(myListener.actionDelete());
        list.add(myListener.getActionImprimer());
        
        tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }
    
    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionClose());        
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }    
    
    private JPanel buildTopPanel() {
        JPanel p = new JPanel(new BorderLayout());
        p.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
        p.setPreferredSize(new Dimension(1,50));
        JLabel l = new JLabel("Vous ne voyez que les modes de paiements définis pour l'exercice "+myApp.appUserInfo().getCurrentExercice().exeExercice());
        l.setHorizontalAlignment(SwingConstants.LEFT);
        p.add(l, BorderLayout.CENTER);
        return p;
    }    
    
    
    
    /**
     * @return L'objet Ordre de paiement actuelment sélectionné.
     */
    public Object getSelectedMod() {
        return modListPanel.selectedObject(); 
    }
    
    
    

    

    

    
 
    
    public interface IModRecherchePanelListener {

        /**
         * @return
         */
        public Action actionClose();

        /**
         * @return
         */
        public Action actionDelete();

        /**
         * @return
         */
        public Action actionModValider();

        /**
         * @return
         */
        public Action actionNew();

        /**
         * @return Les mods à afficher
         */
        public NSArray getMods();
        
        public Action getActionImprimer();

        /**
         * @return un dictionaire contenant les filtres
         */
        public HashMap getFilters();

        /**
         * @return
         */
//        public Action actionSrch();

        public Action actionModify();
        
        /**
         * 
         */
        public void onSelectionChanged();

        /**
         * 
         */
        public void onDbClick();
        
    }
    
    
    
    
    private final class ModListPanelListener implements ModPaiementListPanel.IModListPanelListener {

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModPaiementListPanel.IModListPanelListener#getData()
         */
        public NSArray getData() {
            return myListener.getMods();
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModPaiementListPanel.IModListPanelListener#onSelectionChanged()
         */
        public void onSelectionChanged() {
            myListener.onSelectionChanged();
        }

        /**
         * @see org.cocktail.maracuja.client.administration.ui.ModPaiementListPanel.IModListPanelListener#onDbClick()
         */
        public void onDbClick() {
            myListener.onDbClick();
            
        }
        
    }
    

    public ModPaiementListPanel getModListPanel() {
        return modListPanel;
    }
}
