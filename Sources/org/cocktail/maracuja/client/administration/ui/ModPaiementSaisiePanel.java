/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.ZTooltip;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOModePaiement;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModPaiementSaisiePanel extends ZKarukeraPanel {
	private ZTextField myLibelle;
	private JComboBox myDomaine;

	private ZActionField pcoNumVisaField;
	private ZTextField pcoLibelleVisaField;

	private ZActionField pcoNumPaiementField;
	private ZTextField pcoLibellePaiementField;
	private ZActionField pcoNumTvaField;
	private ZTextField pcoLibelleTvaField;
	private ZActionField pcoNumTvaCtpField;
	private ZTextField pcoLibelleTvaCtpField;

	private ZTextField modCode;

	private IModSaisiePanelListener myListener;
	private ZRadioButtonGroupPanel myEmargementAuto;
	private ZRadioButtonGroupPanel myPaiementHT;
	private JComboBox myContrePartieGestionField;

	/**
     * 
     */
	public ModPaiementSaisiePanel(IModSaisiePanelListener listener) {
		super();
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		myContrePartieGestionField = new JComboBox(myListener.getContrePartieGestionMdl());

		myLibelle = new ZTextField(new LibelleModel());
		myLibelle.getMyTexfield().setColumns(40);

		pcoNumVisaField = new ZActionField(new PcoNumVisaModel(), myListener.actionPlancomptableVisaSelect());
		pcoNumVisaField.getMyTexfield().setColumns(10);

		pcoLibelleVisaField = new ZTextField(new PcoLibelleVisaModel());
		pcoLibelleVisaField.getMyTexfield().setColumns(30);
		pcoLibelleVisaField.getMyTexfield().setEditable(false);

		pcoNumPaiementField = new ZActionField(new PcoNumPaiementModel(), myListener.actionPlancomptablePaiementSelect());
		pcoNumPaiementField.getMyTexfield().setColumns(10);

		pcoLibellePaiementField = new ZTextField(new PcoLibellePaiementModel());
		pcoLibellePaiementField.getMyTexfield().setColumns(30);
		pcoLibellePaiementField.getMyTexfield().setEditable(false);

		pcoNumTvaField = new ZActionField(new PcoNumTvaModel(), myListener.actionPlancomptableTvaSelect());
		pcoNumTvaField.getMyTexfield().setColumns(10);

		pcoLibelleTvaField = new ZTextField(new PcoLibelleTvaModel());
		pcoLibelleTvaField.getMyTexfield().setColumns(30);
		pcoLibelleTvaField.getMyTexfield().setEditable(false);

		pcoNumTvaCtpField = new ZActionField(new PcoNumTvaCtpModel(), myListener.actionPlancomptableTvaCtpSelect());
		pcoNumTvaCtpField.getMyTexfield().setColumns(10);

		pcoLibelleTvaCtpField = new ZTextField(new PcoLibelleTvaCtpModel());
		pcoLibelleTvaCtpField.getMyTexfield().setColumns(30);
		pcoLibelleTvaCtpField.getMyTexfield().setEditable(false);

		myDomaine = new JComboBox(myListener.getDomainesModel());
		myDomaine.addActionListener(new DomaineListener());
		modCode = new ZTextField(new ModCodeModel());
		modCode.getMyTexfield().setColumns(10);

		myEmargementAuto = new ZRadioButtonGroupPanel(new EmargementAutoRadioModel());
		myPaiementHT = new ZRadioButtonGroupPanel(new PaiementHTRadioModel());

		final JButton bt1 = new JButton(myListener.actionPlancomptableVisaSupprimer());
		bt1.setHorizontalAlignment(SwingConstants.CENTER);
		bt1.setText(null);
		bt1.setPreferredSize(new Dimension(((ImageIcon) myListener.actionPlancomptableVisaSupprimer().getValue(AbstractAction.SMALL_ICON)).getIconWidth() + 2, 18));
		bt1.setMinimumSize(bt1.getPreferredSize());
		bt1.setMaximumSize(bt1.getPreferredSize());
		bt1.setFocusPainted(false);

		final JButton bt2 = new JButton(myListener.actionPlancomptablePaiementSupprimer());
		bt2.setHorizontalAlignment(SwingConstants.CENTER);
		bt2.setText(null);
		bt2.setPreferredSize(new Dimension(((ImageIcon) myListener.actionPlancomptablePaiementSupprimer().getValue(AbstractAction.SMALL_ICON)).getIconWidth() + 2, 18));
		bt2.setMinimumSize(bt2.getPreferredSize());
		bt2.setMaximumSize(bt2.getPreferredSize());
		bt2.setFocusPainted(false);

		final JButton bt3 = new JButton(myListener.actionPlancomptableTvaSupprimer());
		bt3.setHorizontalAlignment(SwingConstants.CENTER);
		bt3.setText(null);
		bt3.setPreferredSize(new Dimension(((ImageIcon) myListener.actionPlancomptableTvaSupprimer().getValue(AbstractAction.SMALL_ICON)).getIconWidth() + 2, 18));
		bt3.setMinimumSize(bt3.getPreferredSize());
		bt3.setMaximumSize(bt3.getPreferredSize());
		bt3.setFocusPainted(false);

		final JButton bt4 = new JButton(myListener.actionPlancomptableTvaCtpSupprimer());
		bt4.setHorizontalAlignment(SwingConstants.CENTER);
		bt4.setText(null);
		bt4.setPreferredSize(new Dimension(((ImageIcon) myListener.actionPlancomptableTvaCtpSupprimer().getValue(AbstractAction.SMALL_ICON)).getIconWidth() + 2, 18));
		bt4.setMinimumSize(bt4.getPreferredSize());
		bt4.setMaximumSize(bt4.getPreferredSize());
		bt4.setFocusPainted(false);

		JLabel tooltip = ZTooltip.createTooltipLabel("Contrepartie sur mode de paiement",
				"Si vous spécifiez un compte de contrepartie, pour tous <br>" +
						"les mandats payés avec ce mode de paiement, le compte de contrepartie<br>" +
						"sera celui-ci et non celui paramétré dans le plan comptable.<br><br>" +
						"Si vous spécifiez AGENCE ou COMPOSANTE, le code gestion de l'écriture<br> " +
						"de contrepartie en tiendra compte. Si vous ne spécifiez rien, le code<br> " +
						"gestion tiendra compte du paramétrage indiqué dans le plan comptable<br>" +
						"<i>NB: dans le cas d'un SACD la contrepartie reste toujours sur le SACD</i>");

		final ArrayList list = new ArrayList();
		list.add(new Component[] {
				new JLabel("Code :"), ZUiUtil.buildBoxLine(new Component[] {
						modCode
				})
		});
		list.add(new Component[] {
				new JLabel("Libellé :"), ZUiUtil.buildBoxLine(new Component[] {
						myLibelle
				})
		});
		list.add(new Component[] {
				new JLabel("Domaine :"), ZUiUtil.buildBoxLine(new Component[] {
						myDomaine, new JPanel(new BorderLayout())
				})
		});
		list.add(new Component[] {
				new JLabel("Contrepartie visa au crédit :"), ZUiUtil.buildBoxLine(new Component[] {
						pcoNumVisaField, pcoLibelleVisaField, bt1, new JLabel(" sur "), myContrePartieGestionField, tooltip, new JPanel(new BorderLayout())
				})
		});
		list.add(new Component[] {
				new JLabel("Paiement :"), ZUiUtil.buildBoxLine(new Component[] {
						pcoNumPaiementField, pcoLibellePaiementField, bt2, new JPanel(new BorderLayout())
				})
		});
		list.add(new Component[] {
				new JLabel("Compte de TVA au débit :"), ZUiUtil.buildBoxLine(new Component[] {
						pcoNumTvaField, pcoLibelleTvaField, bt3, new JPanel(new BorderLayout())
				})
		});

		JLabel tooltip2 = ZTooltip.createTooltipLabel("Compte de TVA au crédit",
				"Ce compte sera utilisé lors des paiement HT au fournisseur, <br>" +
						"pour y passer la part de TVA collectée (notamment lors des paiement intra-communautaires).");

		list.add(new Component[] {
				new JLabel("Compte de TVA au crédit :"), ZUiUtil.buildBoxLine(new Component[] {
						pcoNumTvaCtpField, pcoLibelleTvaCtpField, bt4, tooltip2, new JPanel(new BorderLayout())
				})
		});

		list.add(new Component[] {
				new JLabel("Paiement HT au fournisseur :"), ZUiUtil.buildBoxLine(new Component[] {
						myPaiementHT, new JPanel(new BorderLayout())
				})
		});
		list.add(new Component[] {
				new JLabel("Emargement semi-auto :"), ZUiUtil.buildBoxLine(new Component[] {
						myEmargementAuto, new JPanel(new BorderLayout())
				})
		});
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildForm(list), BorderLayout.NORTH);

		//        final Box col1 = Box.createVerticalBox();
		//        col1.add(buildLine((new ZLabeledComponent("Code", modCode, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
		//        col1.add(buildLine((new ZLabeledComponent("Libellé", myLibelle, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
		//        col1.add(buildLine((new ZLabeledComponent("Domaine", myDomaine, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
		//        col1.add(buildLine(new ZLabeledComponent("Contrepartie visa", buildLine( new Component[]{pcoNumVisaField, pcoLibelleVisaField, bt1, new JLabel(" sur "), myContrePartieGestionField} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		//        col1.add(buildLine(new ZLabeledComponent("Paiement", buildLine( new Component[]{pcoNumPaiementField, pcoLibellePaiementField, bt2} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
		//        col1.add(buildLine((new ZLabeledComponent("Emargement semi-auto", myEmargementAuto, ZLabeledComponent.LABELONLEFT, 120))));
		//        col1.add(Box.createVerticalGlue());          
		//        
		setLayout(new BorderLayout());
		add(p, BorderLayout.CENTER);
		add(buildBottomPanel(), BorderLayout.SOUTH);

	}

	private JPanel buildBottomPanel() {
		final ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionAnnuler());
		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myLibelle.updateData();
		pcoNumVisaField.updateData();
		pcoLibelleVisaField.updateData();
		pcoNumPaiementField.updateData();
		pcoLibellePaiementField.updateData();
		pcoNumTvaField.updateData();
		pcoLibelleTvaField.updateData();
		pcoNumTvaCtpField.updateData();
		pcoLibelleTvaCtpField.updateData();
		myEmargementAuto.updateData();
		myPaiementHT.updateData();
		modCode.updateData();
		myDomaine.getModel().setSelectedItem(myListener.dicoValues().get("modDom"));
		myContrePartieGestionField.getModel().setSelectedItem(myListener.dicoValues().get(EOModePaiement.MOD_CONTREPARTIE_GESTION_KEY));
	}

	public final void lockForModify() {
		modCode.setEnabled(false);
	}

	private final class LibelleModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get("modLibelle");
		}

		public void setValue(Object value) {
			myListener.dicoValues().put("modLibelle", value);
		}

	}

	private final class ModCodeModel implements ZTextField.IZTextFieldModel {

		public Object getValue() {
			return myListener.dicoValues().get("modCode");
		}

		public void setValue(Object value) {
			myListener.dicoValues().put("modCode", value);
		}

	}

	/**
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public interface IModSaisiePanelListener {
		public HashMap dicoValues();

		public AbstractAction actionPlancomptableTvaCtpSelect();

		public AbstractAction actionPlancomptableTvaSelect();

		public Action actionPlancomptableTvaCtpSupprimer();

		public Action actionPlancomptableTvaSupprimer();

		public DefaultComboBoxModel getContrePartieGestionMdl();

		public DefaultComboBoxModel getDomainesModel();

		public Action actionValider();

		public Action actionAnnuler();

		public AbstractAction actionPlancomptableVisaSelect();

		public AbstractAction actionPlancomptablePaiementSelect();

		public AbstractAction actionPlancomptablePaiementSupprimer();

		public AbstractAction actionPlancomptableVisaSupprimer();

	}

	private final class PcoLibelleVisaModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			if (myListener.dicoValues().get("planComptableVisa") == null) {
				return null;
			}
			return ((EOPlanComptable) myListener.dicoValues().get("planComptableVisa")).pcoNum() + " " + ((EOPlanComptable) myListener.dicoValues().get("planComptableVisa")).pcoLibelle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {

		}

	}

	private final class PcoLibellePaiementModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			if (myListener.dicoValues().get("planComptablePaiement") == null) {
				return null;
			}
			return ((EOPlanComptable) myListener.dicoValues().get("planComptablePaiement")).pcoNum() + " " + ((EOPlanComptable) myListener.dicoValues().get("planComptablePaiement")).pcoLibelle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {

		}

	}

	private final class PcoLibelleTvaModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			if (myListener.dicoValues().get(EOModePaiement.PLAN_COMPTABLE_TVA_KEY) == null) {
				return null;
			}
			return ((EOPlanComptable) myListener.dicoValues().get(EOModePaiement.PLAN_COMPTABLE_TVA_KEY)).pcoNum() + " " + ((EOPlanComptable) myListener.dicoValues().get(EOModePaiement.PLAN_COMPTABLE_TVA_KEY)).pcoLibelle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {

		}

	}

	private final class PcoLibelleTvaCtpModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			if (myListener.dicoValues().get(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY) == null) {
				return null;
			}
			return ((EOPlanComptable) myListener.dicoValues().get(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY)).pcoNum() + " " + ((EOPlanComptable) myListener.dicoValues().get(EOModePaiement.PLAN_COMPTABLE_TVA_CTP_KEY)).pcoLibelle();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {

		}

	}

	private class PcoNumVisaModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get("pcoNumVisa");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoNumVisa", value);
		}

	}

	private class PcoNumPaiementModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get("pcoNumPaiement");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("pcoNumPaiement", value);
		}

	}

	private class PcoNumTvaModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get(EOModePaiement.PCO_NUM_TVA_KEY);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put(EOModePaiement.PCO_NUM_TVA_KEY, value);
		}

	}

	private class PcoNumTvaCtpModel implements ZTextField.IZTextFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
		 */
		public Object getValue() {
			return myListener.dicoValues().get(EOModePaiement.PCO_NUM_TVA_CTP_KEY);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put(EOModePaiement.PCO_NUM_TVA_CTP_KEY, value);
		}

	}

	private final class EmargementAutoRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private final HashMap values;

		public EmargementAutoRadioModel() {
			values = new HashMap();
			values.put("Oui", EOModePaiement.EMA_AUTO_OUI);
			values.put("Non", EOModePaiement.EMA_AUTO_NON);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get("modEmaAuto");
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put("modEmaAuto", value);
		}

	}

	private final class PaiementHTRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private final HashMap values;

		public PaiementHTRadioModel() {
			values = new HashMap();
			values.put("Oui", EOModePaiement.PAIEMENT_HT_OUI);
			values.put("Non", EOModePaiement.PAIEMENT_HT_NON);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValues()
		 */
		public HashMap getValues() {
			return values;
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#getValue()
		 */
		public Object getValue() {

			return myListener.dicoValues().get(EOModePaiement.MOD_PAIEMENT_HT_KEY);
		}

		/**
		 * @see org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel.IZRadioButtonGroupModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			myListener.dicoValues().put(EOModePaiement.MOD_PAIEMENT_HT_KEY, value);
		}

	}

	private final class DomaineListener implements ActionListener {
		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			myListener.dicoValues().put("modDom", myDomaine.getSelectedItem());

		}

	}
}
