/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;

import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTypeRetenue;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;


public class TypesRetenueAdminPanel extends ZAbstractPanel {
    
    private final TypesRetenueTablePanel typeRetenueTablePanel;
    private final IAdminTypeRetenuePanelModel _model;
    private final JToolBar myToolBar = new JToolBar();
    

    public TypesRetenueAdminPanel(final IAdminTypeRetenuePanelModel model) {
        super();
        _model = model;
        typeRetenueTablePanel = new TypesRetenueTablePanel(_model.typesCreditTableListener());
        typeRetenueTablePanel.initGUI();

        
        buildToolBar();
        setLayout(new BorderLayout());
        
        add(buildTopPanel(), BorderLayout.NORTH);
        add(buildButtonPanel(), BorderLayout.SOUTH);
        add(buildMainPanel(), BorderLayout.CENTER );        
    }
  

    public void initGUI() {

        
    }
    
    
    
    private final JPanel buildTopPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("Types de retenue",
                "<html></html>",null);
        return commentPanel;        
    }
    
    private final JPanel buildButtonPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        final ArrayList actionList = new ArrayList();
        actionList.add(_model.actionClose());
        final Box box = ZUiUtil.buildBoxLine(ZUiUtil.getButtonListFromActionList(actionList)) ;
        box.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }
    
    private final JPanel buildMainPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(myToolBar, BorderLayout.NORTH);
        panel.add(buildTypesCreditPanel(), BorderLayout.CENTER);
        return panel;
    }
    

    private void buildToolBar() {
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);
        
        myToolBar.addSeparator();
        myToolBar.addSeparator();
//        myToolBar.add(_model.actionPrint());
        myToolBar.add(new JPanel(new BorderLayout()));
        
    }    

    private final JPanel buildTypesCreditPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(buildRightPanel(), BorderLayout.EAST);
        panel.add(typeRetenueTablePanel , BorderLayout.CENTER);

        panel.setPreferredSize(new Dimension(320,300));
        return panel;
    }
    

    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        final ArrayList list = new ArrayList();
        list.add(_model.actionAdd());
        list.add(_model.actionModify());
//        list.add(_model.actionValider());
        list.add(_model.actionInvalider());

        
        tmp.add(ZUiUtil.buildGridColumn(ZUiUtil.getButtonListFromActionList(list)), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        return tmp;
    }    
 
    
    
    
    public void updateData() throws Exception {
        typeRetenueTablePanel.updateData();
     
    }

    
    public final class TypesRetenueTablePanel extends ZTablePanel {
        public static final String TRE_LIBELLE_KEY = EOTypeRetenue.TRE_LIBELLE_KEY;
        public static final String PLAN_COMPTABLE_KEY = EOTypeRetenue.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCONUMETPCOLIBELLE_KEY;         
        
        public TypesRetenueTablePanel(ZTablePanel.IZTablePanelMdl listener) {
            super(listener);
            
            final ZEOTableModelColumn treLibelle = new ZEOTableModelColumn(myDisplayGroup,TRE_LIBELLE_KEY,"Libellé",240);
            final ZEOTableModelColumn pco = new ZEOTableModelColumn(myDisplayGroup,PLAN_COMPTABLE_KEY,"Imputation retenue",150);
            
      
            colsMap.clear();
            colsMap.put(TRE_LIBELLE_KEY ,treLibelle);
            colsMap.put(PLAN_COMPTABLE_KEY ,pco);
        }
    }
    
    
    public interface IAdminTypeRetenuePanelModel {
        public Action actionAdd();
        public Map getFilterMap();
        public Action actionInvalider();
//        public Action actionValider();
        public Action actionModify();
        public ZTablePanel.IZTablePanelMdl typesCreditTableListener();
        public Action actionClose();
    }


    public TypesRetenueTablePanel getTypesRetenueTablePanel() {
        return typeRetenueTablePanel;
    }


    
}
