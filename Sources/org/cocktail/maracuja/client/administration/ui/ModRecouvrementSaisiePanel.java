/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.ui.forms.ZTextField;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModRecouvrementSaisiePanel extends ZKarukeraPanel {
    private ZTextField 		myLibelle;
    private JComboBox       myDomaine;
    
    private ZActionField pcoNumVisaField;
    private ZTextField pcoLibelleVisaField;
    
    private ZActionField pcoNumRecouvrementField;
    private ZTextField pcoLibelleRecouvrementField;
    
    private ZTextField modCode;
    
    private IModSaisiePanelListener myListener;
//    private ZRadioButtonGroupPanel myEmargementAuto;
    private static final int DEFAULT_LABEL_WIDTH = 65;
    
    
    /**
     * 
     */
    public ModRecouvrementSaisiePanel(IModSaisiePanelListener listener) {
        super();
        myListener = listener;
    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        myLibelle = new ZTextField(new LibelleModel());
        myLibelle.getMyTexfield().setColumns(40);
        
        pcoNumVisaField = new ZActionField(new PcoNumVisaModel(),myListener.actionPlancomptableVisaSelect());
        pcoNumVisaField.getMyTexfield().setColumns(10);
        
        pcoLibelleVisaField = new ZTextField(new PcoLibelleVisaModel());
        pcoLibelleVisaField.getMyTexfield().setColumns(35);
        pcoLibelleVisaField.getMyTexfield().setEditable(false);

        pcoNumRecouvrementField = new ZActionField(new PcoNumRecouvrementModel(),myListener.actionPlancomptableRecouvrementSelect());
        pcoNumRecouvrementField.getMyTexfield().setColumns(10);
        
        pcoLibelleRecouvrementField = new ZTextField(new PcoLibelleRecouvrementModel());
        pcoLibelleRecouvrementField.getMyTexfield().setColumns(35);
        pcoLibelleRecouvrementField.getMyTexfield().setEditable(false);

        myDomaine = new JComboBox(myListener.getDomainesModel());
        myDomaine.addActionListener(new DomaineListener());
        
        
        modCode = new ZTextField(new ModCodeModel());
        modCode.getMyTexfield().setColumns(10);
        
        
//        myEmargementAuto= new ZRadioButtonGroupPanel(new EmargementAutoRadioModel());
        
        final JButton bt1 = new JButton(myListener.actionPlancomptableVisaSupprimer());
        bt1.setHorizontalAlignment(SwingConstants.CENTER);
        bt1.setText(null);
        bt1.setPreferredSize(new Dimension(((ImageIcon) myListener.actionPlancomptableVisaSupprimer().getValue(AbstractAction.SMALL_ICON)).getIconWidth() + 2, 18));
        bt1.setMinimumSize(bt1.getPreferredSize());
        bt1.setMaximumSize(bt1.getPreferredSize());
        bt1.setFocusPainted(false);
        
        final JButton bt2 = new JButton(myListener.actionPlancomptableRecouvrementSupprimer());
        bt2.setHorizontalAlignment(SwingConstants.CENTER);
        bt2.setText(null);
        bt2.setPreferredSize(new Dimension(((ImageIcon) myListener.actionPlancomptableRecouvrementSupprimer().getValue(AbstractAction.SMALL_ICON)).getIconWidth() + 2, 18));
        bt2.setMinimumSize(bt2.getPreferredSize());
        bt2.setMaximumSize(bt2.getPreferredSize());
        bt2.setFocusPainted(false);
        
        
        final Box col1 = Box.createVerticalBox();
        col1.add(buildLine((new ZLabeledComponent("Code", modCode, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
        col1.add(buildLine((new ZLabeledComponent("Libellé", myLibelle, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
        col1.add(buildLine((new ZLabeledComponent("Domaine", myDomaine, ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH))));
        col1.add(buildLine(new ZLabeledComponent("Visa", buildLine( new Component[]{pcoNumVisaField, pcoLibelleVisaField, bt1} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
        col1.add(buildLine(new ZLabeledComponent("Recouvrement", buildLine( new Component[]{pcoNumRecouvrementField, pcoLibelleRecouvrementField, bt2} ), ZLabeledComponent.LABELONLEFT, DEFAULT_LABEL_WIDTH)));
//        col1.add(buildLine((new ZLabeledComponent("Emargement semi-auto", myEmargementAuto, ZLabeledComponent.LABELONLEFT, 120))));
        col1.add(Box.createVerticalGlue());          
        
        setLayout(new BorderLayout());
        add(col1, BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);
        
    }

    private JPanel buildBottomPanel() {
        final ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionAnnuler());
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myLibelle.updateData();
        pcoNumVisaField.updateData();
        pcoLibelleVisaField.updateData();
        pcoNumRecouvrementField.updateData();
        pcoLibelleRecouvrementField.updateData();
//        myEmargementAuto.updateData();
        modCode.updateData();
        myDomaine.getModel().setSelectedItem( myListener.dicoValues().get("modDom") );
        
    }
    
    
    public final void lockForModify() {
        modCode.setEnabled(false);
    }
    
    
    
    
    
    
    private final class LibelleModel implements ZTextField.IZTextFieldModel {

        public Object getValue() {
            return myListener.dicoValues().get("modLibelle");
        }

        public void setValue(Object value) {
            myListener.dicoValues().put("modLibelle", value);
        }
        
    }    
    private final class ModCodeModel implements ZTextField.IZTextFieldModel {

        public Object getValue() {
            return myListener.dicoValues().get("modCode");
        }

        public void setValue(Object value) {
            myListener.dicoValues().put("modCode", value);
        }
        
    }    
    
    
    
    /**
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public interface IModSaisiePanelListener {
        public HashMap dicoValues();
        public DefaultComboBoxModel getDomainesModel();
        /**
         * @return
         */
        public Action actionValider();
        public Action actionAnnuler();
        public AbstractAction actionPlancomptableVisaSelect();
        public AbstractAction actionPlancomptableRecouvrementSelect();
        public AbstractAction actionPlancomptableRecouvrementSupprimer();
        public AbstractAction actionPlancomptableVisaSupprimer();
        
        
    }

    private final class PcoLibelleVisaModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            if (myListener.dicoValues().get("planComptableVisa")==null) {
                return null;
            }
            return ((EOPlanComptable)myListener.dicoValues().get("planComptableVisa")).pcoNum()+" "+ ((EOPlanComptable)myListener.dicoValues().get("planComptableVisa")).pcoLibelle();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     
    private final class PcoLibelleRecouvrementModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            if (myListener.dicoValues().get("planComptablePaiement")==null) {
                return null;
            }
            return ((EOPlanComptable)myListener.dicoValues().get("planComptablePaiement")).pcoNum()+" "+ ((EOPlanComptable)myListener.dicoValues().get("planComptablePaiement")).pcoLibelle();
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
           
        }
        
    }     
    private class PcoNumVisaModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.dicoValues().get("pcoNumVisa");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.dicoValues().put("pcoNumVisa",value);
        }
        
    }    
    private class PcoNumRecouvrementModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.dicoValues().get("pcoNumPaiement");
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.dicoValues().put("pcoNumPaiement",value);
        }
        
    }    
    
    
    private final class DomaineListener implements ActionListener {
        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            myListener.dicoValues().put("modDom", myDomaine.getSelectedItem());
            
        }
        
    }
    

}
