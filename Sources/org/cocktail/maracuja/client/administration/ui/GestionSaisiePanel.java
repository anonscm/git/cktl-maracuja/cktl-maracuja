/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestionAgregat;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZToolBar;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDicoDyn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

public class GestionSaisiePanel extends ZKarukeraPanel {
	private final GestionTablePanel gestionTablePanel;
	private final GestionExerciceTablePanel gestionExerciceTablePanel;
	private final IGestionSaisiePanelModel _model;

	private final JComboBox comboBoxExercice;
	private final AgregatTablePanel agregatTablePanel;
	private final AgregatGestionTablePanel agregatGestionTablePanel;
	private JTabbedPane tabs;

	public GestionSaisiePanel(final IGestionSaisiePanelModel model) {
		super();
		_model = model;

		gestionTablePanel = new GestionTablePanel(_model.gestionTableListener());
		gestionExerciceTablePanel = new GestionExerciceTablePanel(_model.gestionExerciceTableListener());

		agregatTablePanel = new AgregatTablePanel(_model.agregatTableListener());
		agregatGestionTablePanel = new AgregatGestionTablePanel(_model.agregatGestionTableListener());

		comboBoxExercice = new JComboBox(_model.exercicesModel());
		comboBoxExercice.addActionListener(new ComboFilterListener());
	}

	public void initGUI() {
		gestionTablePanel.initGUI();
		gestionExerciceTablePanel.initGUI();
		agregatTablePanel.initGUI();
		agregatGestionTablePanel.initGUI();

		setLayout(new BorderLayout());
		add(buildExercicePanel(), BorderLayout.NORTH);
		add(buildTabs(), BorderLayout.CENTER);
		add(buildButtonPanel(), BorderLayout.SOUTH);
	}

	private JPanel buildGestionAdminPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildGestionTopPanel(), BorderLayout.NORTH);
		panel.add(buildGestionMainPanel(), BorderLayout.CENTER);
		return panel;
	}

	private final JPanel buildGestionTopPanel() {
		final ZCommentPanel commentPanel = new ZCommentPanel(
				"Codes gestion",
				"<html>Les codes gestion doivent être en cohérence avec les UB.<br><b>Attention : <font color=\"#FF0000\">Il n'est pas possible de supprimer un code gestion existant</font>.</b> " +
						"Si vous souhaitez supprimer un code gestion pour un exercice donné, assurez-vous auparavant que ce code gestion n'est pas déjà utilisé (mandats, titres, écritures, etc.).<br>" +
						"<b>Compte de liason SACD (niveau agence)</b> : Il s'agit d'une subdivision du compte 185 qui va identifier les écritures du SACD passées au niveau de l'agence (par exemple 18521)<br>" +
						"<b>Compte de liason SACD (niveau SACD)</b> : Il s'agit d'une subdivision du compte 185 qui va identifier les écritures du SACD passées au niveau du SACD (logiquement le même que celui niveau agence)<br>" +
						"</html>",
				null);
		return commentPanel;
	}

	private final JPanel buildButtonPanel() {
		final JPanel bPanel = new JPanel(new BorderLayout());
		final ArrayList actionList = new ArrayList();
		actionList.add(_model.actionEnregistrer());
		actionList.add(_model.actionCancel());
		actionList.add(_model.actionClose());
		final Box box = buildHorizontalButtonsFromActions(actionList);
		bPanel.add(new JSeparator(), BorderLayout.PAGE_START);
		bPanel.add(box, BorderLayout.LINE_END);
		return bPanel;
	}

	private final JPanel buildGestionMainPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildGestionPanel(), BorderLayout.WEST);
		panel.add(buildGestionExercicePanel(), BorderLayout.CENTER);
		return panel;
	}

	private final JPanel buildGestionPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		final ArrayList list = new ArrayList();
		list.add(_model.actionNew());
		final ZToolBar toolBar = new ZToolBar(null, JToolBar.HORIZONTAL, list);

		final ArrayList list2 = new ArrayList();
		list2.add(_model.actionAdd());
		list2.add(_model.actionRemove());
		//        list2.add(_model.actionRemoveAll());

		final ArrayList buttons = getButtonListFromActionList(list2);
		final Iterator iter = buttons.iterator();
		while (iter.hasNext()) {
			JButton element = (JButton) iter.next();
			element.setHorizontalAlignment(JButton.CENTER);
		}

		final JPanel toolbar2 = new JPanel(new BorderLayout());
		toolbar2.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		toolbar2.setPreferredSize(new Dimension(120, 200));
		toolbar2.add(buildVerticalPanelOfComponents(buttons), BorderLayout.NORTH);
		toolbar2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		panel.add(toolBar, BorderLayout.NORTH);
		panel.add(toolbar2, BorderLayout.EAST);
		panel.add(gestionTablePanel, BorderLayout.CENTER);

		panel.setPreferredSize(new Dimension(320, 300));
		return panel;
	}

	private final JPanel buildGestionExercicePanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		//		final JPanel exercicePanel = new JPanel(new BorderLayout());
		//
		//		final JLabel labelExercice = new JLabel("Exercice");
		//		labelExercice.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 10));
		//
		//		exercicePanel.add(buildLine(new Component[] {
		//				labelExercice, comboBoxExercice
		//		}));
		//		exercicePanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		panel.add(gestionExerciceTablePanel, BorderLayout.CENTER);
		//	panel.add(exercicePanel, BorderLayout.NORTH);
		return panel;
	}

	private final JPanel buildExercicePanel() {
		final JPanel exercicePanel = new JPanel(new BorderLayout());

		final JLabel labelExercice = new JLabel("Exercice");
		labelExercice.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 10));

		exercicePanel.add(buildLine(new Component[] {
				labelExercice, comboBoxExercice
		}));
		exercicePanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		return exercicePanel;
	}

	public void updateData() throws Exception {
		gestionTablePanel.updateData();
		gestionExerciceTablePanel.updateData();
		agregatTablePanel.updateData();
		agregatGestionTablePanel.updateData();
	}

	public final class GestionTablePanel extends ZTablePanel {
		public static final String COL_GESCODE = "gesCode";

		public GestionTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup, COL_GESCODE, "Code Gestion", 84);
			gesCode.setAlignment(SwingConstants.CENTER);

			colsMap.clear();
			colsMap.put(COL_GESCODE, gesCode);

		}

	}

	public final class GestionExerciceTablePanel extends ZTablePanel {
		public static final String COL_GESCODE = "gestion.gesCode";
		public static final String COL_GESLIBELLE = "gesLibelle";
		public static final String COL_PCONUM185 = "planComptable185.pcoNum";
		public static final String COL_PCONUM185CTP = "planComptable185CtpSacd.pcoNum";
		public static final String COL_PCONUM181 = "planComptable181.pcoNum";

		public GestionExerciceTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			//            final ZEOTableModelColumn.ZEONumFieldTableCellEditor pcoNumEditor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), null);
			//            final ZEOTableModelColumn.ZEONumFieldTableCellEditor pcoNumEditor = new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField());

			final ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup, COL_GESCODE, "Code Gestion", 70);
			gesCode.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn gesLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_GESLIBELLE, "Libelle", 70);
			gesLibelle.setAlignment(SwingConstants.CENTER);
			gesLibelle.setEditable(true);
			//            gesLibelle.setTableCellEditor( new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), ZConst.FORMAT_ENTIER_BRUT));
			gesLibelle.setMyModifier(_model.getGesLibelleModifier());
			//
			//			final ZEOTableModelColumn pcoNum181 = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM181, "Imputation 181", 84);
			//			pcoNum181.setAlignment(SwingConstants.CENTER);
			//			pcoNum181.setEditable(true);
			//			//            pcoNum181.setTableCellEditor( pcoNumEditor);
			//			pcoNum181.setMyModifier(_model.getPcoNum181Modifier());

			final ZEOTableModelColumn pcoNum185 = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM185, "Compte de liaison SACD (niveau agence)", 190);
			pcoNum185.setAlignment(SwingConstants.CENTER);
			pcoNum185.setEditable(true);
			//            pcoNum185.setTableCellEditor( pcoNumEditor);
			pcoNum185.setMyModifier(_model.getPcoNum185Modifier());

			final ZEOTableModelColumn pcoNum185Ctp = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM185CTP, "Compte de liaison SACD (niveau SACD)", 190);
			pcoNum185Ctp.setAlignment(SwingConstants.CENTER);
			pcoNum185Ctp.setEditable(true);
			pcoNum185Ctp.setMyModifier(_model.getPcoNum185CtpModifier());

			colsMap.clear();
			colsMap.put(COL_GESCODE, gesCode);
			colsMap.put(COL_GESLIBELLE, gesLibelle);
			//colsMap.put(COL_PCONUM181, pcoNum181);
			colsMap.put(COL_PCONUM185, pcoNum185);
			colsMap.put(COL_PCONUM185CTP, pcoNum185Ctp);

		}
	}

	public final class AgregatTablePanel extends ZTablePanel {
		//public static final String COL_GESCODE = EOGestionAgregat.GA_LIBELLE_KEY;

		public AgregatTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn gaLibelleCol = new ZEOTableModelColumn(myDisplayGroup, EOGestionAgregat.GA_LIBELLE_KEY, "Agrégat", 84);
			gaLibelleCol.setAlignment(SwingConstants.LEFT);

			colsMap.clear();
			colsMap.put(EOGestionAgregat.GA_LIBELLE_KEY, gaLibelleCol);
		}
	}

	public final class AgregatGestionTablePanel extends ZTablePanel {
		public static final String COL_GESCODE = "gestion.gesCode";
		public static final String COL_GESLIBELLE = "gesLibelle";

		public AgregatGestionTablePanel(IAgregatGestionTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumnDicoDyn col0 = new ZEOTableModelColumnDicoDyn(myDisplayGroup, "Sélectionner", 60, listener);
			col0.setEditable(true);
			col0.setResizable(false);
			col0.setMyModifier(_model.checkGestionModifier());
			col0.setColumnClass(Boolean.class); //pour forcer l'affichage des cases à cocher

			final ZEOTableModelColumn gesCode = new ZEOTableModelColumn(myDisplayGroup, COL_GESCODE, "Code Gestion", 70);
			gesCode.setAlignment(SwingConstants.CENTER);

			final ZEOTableModelColumn gesLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_GESLIBELLE, "Libelle", 70);
			gesLibelle.setAlignment(SwingConstants.CENTER);
			gesLibelle.setEditable(true);
			gesLibelle.setMyModifier(_model.getGesLibelleModifier());

			colsMap.clear();
			colsMap.put("selection", col0);
			colsMap.put(COL_GESCODE, gesCode);
			colsMap.put(COL_GESLIBELLE, gesLibelle);

		}

		public ZEOTableModel getMyTableModel() {
			return myTableModel;
		}

	}

	public interface IAgregatGestionTablePanelMdl extends ZTablePanel.IZTablePanelMdl, ZEOTableModelColumnDicoDyn.IZEOTableModelColumnDicoListener {
		public Map checkDico();
	}

	private class ComboFilterListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			_model.onExerciceSelected();
		}
	}

	public interface IGestionSaisiePanelModel {
		public ZTablePanel.IZTablePanelMdl gestionTableListener();

		public Modifier checkGestionModifier();

		//	public HashMap getAllCheckedGestions();

		public ZTablePanel.IZTablePanelMdl agregatTableListener();

		public Modifier getGesLibelleModifier();

		public Modifier getPcoNum181Modifier();

		public Modifier getPcoNum185Modifier();

		public Modifier getPcoNum185CtpModifier();

		public ComboBoxModel exercicesModel();

		//        public Action actionRemoveAll();
		public Action actionRemove();

		//        public Action actionAddAll();
		public Action actionAdd();

		public Action actionDelete();

		public Action actionNew();

		public Action actionNewAgregat();

		public Action actionDeleteAgregat();

		public Action actionClose();

		public Action actionCancel();

		public Action actionEnregistrer();

		public ZTablePanel.IZTablePanelMdl gestionExerciceTableListener();

		public IAgregatGestionTablePanelMdl agregatGestionTableListener();

		public void onExerciceSelected();
	}

	public GestionExerciceTablePanel getGestionExerciceTablePanel() {
		return gestionExerciceTablePanel;
	}

	public GestionTablePanel getGestionTablePanel() {
		return gestionTablePanel;
	}

	public JComboBox getComboBoxExercice() {
		return comboBoxExercice;
	}

	private JTabbedPane buildTabs() {
		final JTabbedPane p = new JTabbedPane();
		p.addTab("Codes gestions", buildGestionAdminPanel());
		p.addTab("Regroupements", buildAgregatAdminPanel());
		tabs = p;
		return p;
	}

	private JPanel buildAgregatPanel() {
		final ArrayList list = new ArrayList();
		list.add(_model.actionNewAgregat());
		list.add(_model.actionDeleteAgregat());
		final ZToolBar toolBar = new ZToolBar(null, JToolBar.HORIZONTAL, list);

		JPanel panel = new JPanel(new BorderLayout());
		panel.add(toolBar, BorderLayout.NORTH);
		panel.add(agregatTablePanel, BorderLayout.CENTER);
		panel.setPreferredSize(new Dimension(320, 300));
		return panel;
	}

	private JPanel buildAgregatMainPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildAgregatPanel(), BorderLayout.WEST);
		panel.add(buildAgregatGestionPanel(), BorderLayout.CENTER);
		return panel;
	}

	private JPanel buildAgregatAdminPanel() {
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildAgregatTopPanel(), BorderLayout.NORTH);
		panel.add(buildAgregatMainPanel(), BorderLayout.CENTER);
		return panel;
	}

	private final JPanel buildAgregatTopPanel() {
		final ZCommentPanel commentPanel = new ZCommentPanel(
				"Agrégats de codes gestions",
				"<html>Cet écran vous permet de créer des regroupements de codes gestion. Ces agrégats seront ensuite disponibles dans les critères d'éditions, " +
						"en plus des niveaux intégrés à Maracuja (<b>Agrégé (Etablissement + SACDs)</b>, <b>Etablissement</b>, les <b>SACDs</b>, les <b>UBs</b>)" +
						"</html>",
				null);
		return commentPanel;
	}

	private final JPanel buildAgregatGestionPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		//		final JPanel exercicePanel = new JPanel(new BorderLayout());
		//
		//		final JLabel labelExercice = new JLabel("Exercice");
		//		labelExercice.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 10));
		//
		//		exercicePanel.add(buildLine(new Component[] {
		//				labelExercice, comboBoxExercice
		//		}));
		//		exercicePanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		panel.add(agregatGestionTablePanel, BorderLayout.CENTER);
		//panel.add(exercicePanel, BorderLayout.NORTH);
		return panel;
	}

	public AgregatTablePanel getAgregatTablePanel() {
		return agregatTablePanel;
	}

	public AgregatGestionTablePanel getAgregatGestionTablePanel() {
		return agregatGestionTablePanel;
	}

	public JTabbedPane getTabs() {
		return tabs;
	}

}
