/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.administration.ui;

import java.awt.BorderLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * Panel affichant une liste de modes de recouvrement.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ModRecouvrementListPanel extends ZKarukeraPanel implements ZEOTableListener {
    protected ZEOTable myEOTable;
    protected ZEOTableModel myTableModel;
    protected EODisplayGroup myDisplayGroup;
    protected TableSorter myTableSorter;
    protected Vector myCols;
    private IModListPanelListener myListener;
    

    /**
     * @param editingContext
     */
    public ModRecouvrementListPanel(IModListPanelListener listener) {
        super();
        myListener = listener;
        myDisplayGroup = new EODisplayGroup();
    }

    

    
    
    
    private void initTableModel() {
        myCols = new Vector(6, 0);

        ZEOTableModelColumn col0 = new ZEOTableModelColumn(myDisplayGroup, "modCode", "Code", 60);
        col0.setAlignment(SwingConstants.CENTER);

        ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "modDom", "Domaine", 90);
        col1.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, "modLibelle", "Libellé", 260);
        col2.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "planComptablePaiement.pcoNum", "Imputation Recouvrement", 115);
        col3.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "planComptableVisa.pcoNum", "Imputation Visa", 115);
        col4.setAlignment(SwingConstants.LEFT);

        ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, "modValidite", "Valide", 90);
        col6.setAlignment(SwingConstants.CENTER);
		        

        myCols.add(col0);
        myCols.add(col1);
        myCols.add(col2);
        myCols.add(col4);
        myCols.add(col3);        
        myCols.add(col6);

        myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
        myTableSorter = new TableSorter(myTableModel);
    }

    /**
     * Initialise la table à afficher (le modele doit exister)
     */
    private void initTable() {
        myEOTable = new ZEOTable(myTableSorter);
        myEOTable.addListener(this);
        myTableSorter.setTableHeader(myEOTable.getTableHeader());
        myEOTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void initGUI() {
        setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
        initTableModel();
        initTable();
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }

    
    
    public final Object selectedObject() {
        return myDisplayGroup.selectedObject();
    }
    
    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(myListener.getData());
        myEOTable.updateData();
//        myTableModel.updateInnerRowCount();
//        myTableModel.fireTableDataChanged();
    }

    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {
        myListener.onDbClick();
    }

    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.onSelectionChanged();
    }

    public interface IModListPanelListener {

        /**
         * @return
         */
        public NSArray getData();

        /**
         *  
         */
        public void onSelectionChanged();

        /**
         *  
         */
        public void onDbClick();

    }
    public ZEOTableModel getMyTableModel() {
        return myTableModel;
    }
}
