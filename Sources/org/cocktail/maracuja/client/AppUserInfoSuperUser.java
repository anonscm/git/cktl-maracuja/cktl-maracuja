/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client;

import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFonction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;


/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class AppUserInfoSuperUser extends AppUserInfo {

	/**
	 * 
	 */
	public AppUserInfoSuperUser() {
		super();
		login = "superUser";
	}


	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.AppUserInfo#initInfo(com.webobjects.eocontrol.EOEditingContext, java.lang.String, java.lang.Integer, java.lang.Integer, com.webobjects.foundation.NSDictionary)
	 */
	public void initInfo(
		EOEditingContext ec,
		String plogin,
		Integer noIndividu,
		Integer persId,
		NSDictionary infos)	
		throws Exception {
	
			
		
			NSArray tmp = EOsFinder.getAllFonctions(ec);
			NSMutableArray tmp1 = new NSMutableArray();
			for (int i = 0; i < tmp.count(); i++) {
				tmp1.addObject( ((EOFonction)tmp.objectAtIndex(i)).fonIdInterne() );
			}
			allowedFonctions = tmp1.immutableClone();	

	}
	
	/* (non-Javadoc)
	 * @see org.cocktail.maracuja.client.AppUserInfo#updateAllowedFonctions(com.webobjects.eocontrol.EOEditingContext, org.cocktail.maracuja.client.metier.EOExercice)
	 */
	public void updateAllowedFonctions(
		EOEditingContext ec,
		EOExercice exercice) {
			
		//on ne fait rien, les allowed fonctions ne doivent pas changer
		return;


	}

	


}
