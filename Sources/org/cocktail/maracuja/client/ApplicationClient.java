/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client;

import static org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper.OS.LINUX;
import static org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper.OS.MAC_OS_X;
import static org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper.OS.WINDOWS;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.ListSelectionModel;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppFromJar;

import org.cocktail.fwkcktlcompta.client.metier.EOScolarixInscription;
import org.cocktail.fwkcktlcompta.common.FwkCktlComptaMoteurCtrl;
import org.cocktail.fwkcktlcompta.common.IFwkCktlComptaParam;
import org.cocktail.fwkcktlcompta.common.helpers.ParamHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IComptaGuiSwingApplication;
import org.cocktail.fwkcktlcomptaguiswing.client.all.OperatingSystemHelper;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.chgtex.ctrl.ChgtExerciceCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread.DefaultZHeartBeatListener;
import org.cocktail.maracuja.client.common.ui.CommonDialogs;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.finders.EOPlanComptableFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.metier.EOParametre;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPreference;
import org.cocktail.maracuja.client.metier.IConst;
import org.cocktail.maracuja.client.remotecall.CktlServerInvoke;
import org.cocktail.zutil.client.ZBrowserControl;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.ZVersion;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.InitException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.wo.ZEOApplication;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eoapplication.EOWindowObserver;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

import er.extensions.eof.ERXEC;

/**
 * Classe principale pour l'application cliente. pour y faire reference, utiliser EOApplication.sharedApplication();
 */
public class ApplicationClient extends ZEOApplication implements IComptaGuiSwingApplication {

	public static final String CONST_NON = "NON";

	/** Version minimale (la jre active doit etre >= ) */
	private static final String MINJREVERSION = "1.5.0";

	/** Nombre maximal de tentative de login avant que l'application se ferme */
	private static final int MAXTENTATIVEFORLOGIN = 4;

	private final String LOG_PREFIX = "log_maracuja_client_";

	public static final String CONTREPARTIE_DEPENSE_COMPTES_KEY = "org.cocktail.gfc.comptabilite.contrepartie.depense.comptes";
	public static final String CONTREPARTIE_RECETTE_COMPTES_KEY = "org.cocktail.gfc.comptabilite.contrepartie.recette.comptes";

	private static final String APPLICATION_TYAP_STRID = "MARACUJA";

	private String applicationVersion = null;
	private String applicationBdConnexionName = null;

	private Superviseur toSuperviseur;

	private final String initialFwkCktlWebApp;
	private String platform;
	public String temporaryDir;
	public String homeDir;

	private AppUserInfo appUserInfo;
	private final ZWaitingPanelDialog waitingDialog;
	private ZActionCtrl myActionsCtrl;

	private ZLoginDialog tmpLoginDialogCtrl2;

	/** Stocke les parametres de la table MARACUJA.PARAMETRES */
	private NSMutableDictionary parametres;

	/**
	 * Message à afficher sur l'ecran d'accueil, specifié dans le fichier de config
	 */
	private String displayMessageClient;
	private String adminMail;

	private MyFileOutputStream fileOutputStreamOut;
	private MyFileOutputStream fileOutputStreamErr;
	private File clientLogFile;
	private String applicationName;
	private NSArray comptabilites;

	private final HashMap exerciceColorMap = new HashMap(4);
	private NSDictionary _appParametres;

	/** Thread pour maintenir une connexion avec le serveur */
	private final ZWaitingThread helloThread;

	public ApplicationClient() {
		super();
		System.out.println("Lancement ApplicationClient...");
		try {
			initLogs();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(clientLogFile.getAbsolutePath());
		System.out.println(ZDateUtil.nowAsDate());
		System.out.println("-- Lancement d'une instance de l'application cliente " + getApplicationName() + " --");
		System.out.println(ServerProxy.serverVersion(editingContext()));
		System.out.println("Connection depuis " + getIpAdress());
		//        appLog = new ZLog();
		//        appLog.setShowOutput(showLogs());
		//        appLog.setShowTrace(showTrace());

		final String logLvl = ServerProxy.clientSideRequestGetGrhumParam(getEditingContext(), "LOG_LEVEL_CLIENT");
		int lvl = ZLogger.LVL_INFO;
		if (ZLogger.VERBOSE.equals(logLvl)) {
			lvl = ZLogger.LVL_VERBOSE;
		}
		else if (ZLogger.DEBUG.equals(logLvl)) {
			lvl = ZLogger.LVL_DEBUG;
		}
		else if (ZLogger.INFO.equals(logLvl)) {
			lvl = ZLogger.LVL_INFO;
		}
		else if (ZLogger.WARNING.equals(logLvl)) {
			lvl = ZLogger.LVL_WARNING;
		}
		else if (ZLogger.ERROR.equals(logLvl)) {
			lvl = ZLogger.LVL_ERROR;
		}
		else if (ZLogger.NONE.equals(logLvl)) {
			lvl = ZLogger.LVL_NONE;
		}
		System.out.println("Niveau de log : " + logLvl);
		ZLogger.setCurrentLevel(lvl);

		IConst.WANT_EO_LOGS = ZLogger.currentLevel >= ZLogger.LVL_DEBUG;

		//        appLog.append("-- System.getProperties");
		//        appLog.append(System.getProperties());
		//        appLog.append("");

		//        appLog.append("-- arguments");
		//        appLog.append(arguments().hashtable());

		waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		waitingDialog.setTitle(ServerProxy.clientSideRequestGetApplicationName(editingContext()));
		waitingDialog.setTopText("Veuillez patienter...");
		waitingDialog.setBottomText("Initialisation de l'application");
		waitingDialog.setVisible(true);

		initialFwkCktlWebApp = System.getProperty("user.name");
		SuppressionFichiers suppressionFichiers = new SuppressionFichiers();
		suppressionFichiers.start();

		exerciceColorMap.put(EOExercice.EXE_ETAT_PREPARATION, ZConst.COULEUR_EXERCICE_PREPARATION);
		exerciceColorMap.put(EOExercice.EXE_ETAT_OUVERT, ZConst.COULEUR_EXERCICE_OUVERT);
		exerciceColorMap.put(EOExercice.EXE_ETAT_RESTREINT, ZConst.COULEUR_EXERCICE_RESTREINT);
		exerciceColorMap.put(EOExercice.EXE_ETAT_CLOS, ZConst.COULEUR_EXERCICE_CLOS);

		//maintenir la connection avec le serveur
		ServerProxy.clientSideRequestMsgToServer(getEditingContext(), "Hello depuis ..." + getIpAdress(), getIpAdress());
		final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
			public void onBeat() {
				ServerProxy.clientSideRequestMsgToServer(getEditingContext(), "Hello depuis ..." + getIpAdress(), getIpAdress());
			}

			public void mainTaskStart() {
			}

			public boolean isMainTaskFinished() {
				return false;
			};
		};
		helloThread = new ZWaitingThread(60000, 60000, defaultZHeartBeatListener);
		helloThread.start();
	}

	/**
	 * Met à jour le message dans la fenêtre d'attente, si elle est affichée.
	 *
	 * @param msg
	 */
	public void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	/**
	 * Met à jour le dictionaire des parametres pour l'exercice en cours.
	 */
	private final void initParametresForExercice(EOExercice exercice) {
		parametres = new NSMutableDictionary();
		NSArray res = ZFinder.fetchArray(getEditingContext(), EOParametre.ENTITY_NAME, "exercice=%@", new NSArray(exercice), null, true);
		for (int i = 0; i < res.count(); i++) {
			EOParametre element = (EOParametre) res.objectAtIndex(i);
			parametres.takeValueForKey(element.parValue(), element.parKey());
		}
		EOPlanComptable.refreshPcoLibelleMap(getEditingContext(), exercice);

	}

	public String getJREVersion() {
		return System.getProperty("java.version");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.webobjects.eoapplication.EOApplication#finishInitialization()
	 */
	public void finishInitialization() {
		super.finishInitialization();

		setQuitsOnLastWindowClose(false);
		initTimeZones();

		final ZVersion min = ZVersion.getVersion(MINJREVERSION);
		final ZVersion cur = ZVersion.getVersion(getJREVersion());

		ZLogger.logTitle("Verification des versions", ZLogger.LVL_INFO);
		ZLogger.logKeyValue("Version minimale JRE", MINJREVERSION, ZLogger.LVL_INFO);
		ZLogger.logKeyValue("Version active JRE", getJREVersion() + " (" + cur.toString() + ")", ZLogger.LVL_INFO);
		if (cur.compareTo(min) < 0) {
			ZLogger.warning("La version JRE presente n'est pas compatible avec la version minimale (" + MINJREVERSION + ")");
			CommonDialogs.showErrorDialog(activeWindow(), new Exception("La version JRE presente n'est pas compatible avec la version minimale (" + MINJREVERSION + ")"));
			quitter();
		} else {
			ZLogger.info("Test version min Ok");
		}

		try {
			String serverVersion = ServerProxy.serverVersionRaw(getEditingContext());
			ZLogger.logKeyValue("Application", serverVersion, ZLogger.LVL_INFO);
			compareJarVersionsClientAndServer();
		} catch (Exception e) {
			CommonDialogs.showErrorDialog(activeWindow(), e);
			quitter();
		}
		displayMessageClient = ServerProxy.clientSideRequestGetGrhumParam(getEditingContext(), "DISPLAY_MESSAGE_CLIENTS");
		adminMail = ServerProxy.clientSideRequestGetGrhumParam(getEditingContext(), "ADMIN_MAIL");

		myActionsCtrl = new ZActionCtrl(this);

		initApplication();
		initFormats();
		initEntites();
		waitingDialog.setVisible(false);
		superviseur().setWaitCursor(false);
		setQuitsOnLastWindowClose(true);
	}


	private final void initLogs() throws Exception {
		clientLogFile = createLogFile();

		// redirection des logs
		System.out.println("redirection des logs");
		fileOutputStreamOut = new MyFileOutputStream(System.out, clientLogFile);
		fileOutputStreamErr = new MyFileOutputStream(System.out, clientLogFile);
		System.setOut(new PrintStream(fileOutputStreamOut));
		System.setErr(new PrintStream(fileOutputStreamErr));

		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);
	}

	private final File createLogFile() {
		String tag = new SimpleDateFormat("yyyyMMdd_HHmmss").format(ZDateUtil.nowAsDate());
		String fileName = LOG_PREFIX + tag + ".txt";
		File file = new File(System.getProperty("java.io.tmpdir"), fileName);
		return file;
	}

	public final String getApplicationFullName() {
		return getApplicationName() + " - " + getApplicationVersion() + " - " + getApplicationBdConnexionName();
	}

	public final String getApplicationVersion() {
		if (applicationVersion == null) {
			applicationVersion = (String) appParametres().valueForKey(ServerProxy.TXTVERSION_KEY);
		}
		return applicationVersion;
	}

	public final String getApplicationBdConnexionName() {
		if (applicationBdConnexionName == null) {
			applicationBdConnexionName = (String) appParametres().valueForKey(ServerProxy.BDCONNEXIONINFO_KEY);
		}
		return applicationBdConnexionName;
	}

	/**
	 * Filtre qui sélectionne des fichiers dont la date de modifiaction est antérieurs à 15 jours.
	 */
	private class LogFileFilter implements FileFilter {
		/**
		 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
		 */
		public boolean accept(File pathname) {
			GregorianCalendar date = new GregorianCalendar();
			date.add(GregorianCalendar.DAY_OF_YEAR, -15);
			return (pathname.getName().startsWith(LOG_PREFIX) && pathname.lastModified() <= date.getTimeInMillis());
		}

	}

	private final class SuppressionFichiers extends Thread {

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			System.out.println("Thread - Nettoyage des fichiers temporaires - start");
			final File rep = new File(System.getProperty("java.io.tmpdir"));
			final FileFilter logFileFilter = new LogFileFilter();
			if (rep.exists()) {
				File[] fichiers = rep.listFiles(logFileFilter);
				for (int i = 0; i < fichiers.length; i++) {
					File file = fichiers[i];
					System.out.println("Suppression du fichier de log : " + file.getAbsolutePath());
					file.delete();
				}
			}
			System.out.println("Thread - Nettoyage des fichiers temporaires - end");
		}

	}

	private final void closeLogFile() throws Exception {
		if (fileOutputStreamOut != null) {
			fileOutputStreamOut.close();
		}
		if (fileOutputStreamErr != null) {
			fileOutputStreamErr.close();
		}
	}

	public Superviseur superviseur() {
		return toSuperviseur;
	}

	private final void initEntites() {
		ZEOUtilities.fixWoBug_responseToMessage(ZConst.ENTITIES_TO_LOAD);
	}

	/**
	 * Initialisation de l'application cliente, avec gestion identification et création de la fenetre principale.
	 */
	private void initApplication() {
		initMoteurCompta();
		initForPlatform();

		setWindowObserver(new MyEOWindowObserver());
		if (!gereIdentification()) {
			quitter();
		} else {
			ServerProxy.clientSideRequestHelloImAlive(getEditingContext(), appUserInfo().getLogin(), getIpAdress());
			tmpLoginDialogCtrl2 = null;
			initDatas();
			initUserInterface();
		}
	}

	private void initMoteurCompta() {
		FwkCktlComptaMoteurCtrl.getSharedInstance().init(FwkCktlComptaMoteurCtrl.Contexte.CLIENT);
		FwkCktlComptaMoteurCtrl.getSharedInstance().setRefApplicationCreation(APPLICATION_TYAP_STRID);

		try {
			EOScolarixInscription.SCOLARIX_ECHEANCIER_MODE_RECOUVREMENT_CODE = (String) ParamHelper.getSharedInstance().getConfig(new EOEditingContext(), IFwkCktlComptaParam.SEPASDDMANDAT_SCOLARIX_MODERECOUVREMENTCODEDEFAUT);
		} catch (Exception e) {
			System.err.println("Impossible d'initialiser le code du mode de recouvrement à utiliser pour les prélèveùents SEPA issus de SCOLARIX");
			e.printStackTrace();
		}
	}

	/**
	 * Initialisation des variables dependantes de la plateforme d'exécution.
	 */
	private void initForPlatform() {
		ZLogger.logTitle("Detection de la plateforme d'execution", ZLogger.LVL_INFO);
		temporaryDir = null;
		homeDir = null;

		platform = System.getProperties().getProperty("os.name");
		ZLogger.logKeyValue("Plateforme detectee", platform, ZLogger.LVL_INFO);
		try {
			temporaryDir = System.getProperty("java.io.tmpdir");
			homeDir = System.getProperty("user.home");
			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
			if (!homeDir.endsWith(File.separator)) {
				homeDir = homeDir + File.separator;
			}
			ZLogger.logKeyValue("java.io.tmpdir", temporaryDir, ZLogger.LVL_INFO);
			ZLogger.logKeyValue("user.home", homeDir, ZLogger.LVL_INFO);
			ZLogger.logKeyValue("user.name", System.getProperty("user.name"), ZLogger.LVL_INFO);

		} catch (Exception e) {
			ZLogger.logFailed("Impossible de recuperer le repertoire temporaire ou le repertoire home de l'utilisateur", ZLogger.LVL_WARNING);
		}

		if (platform.startsWith(WINDOWS.getName())) {
			ZLogger.logKeyValue("Famille de plateforme", WINDOWS.getName(), ZLogger.LVL_INFO);
			if (temporaryDir == null) {
				temporaryDir = "c:\\temp\\";
			}
		}
		else if (MAC_OS_X.getName().equals(platform)) {
			ZLogger.logKeyValue("Famille de plateforme", MAC_OS_X.getName(), ZLogger.LVL_INFO);
			if (temporaryDir == null) {
				temporaryDir = "/tmp/";
			}
		}
		else if (LINUX.getName().equals(platform)) {
			ZLogger.logKeyValue("Famille de plateforme", LINUX.getName(), ZLogger.LVL_INFO);
			if (temporaryDir == null) {
				temporaryDir = "/tmp/";
			}
		}
		else {
			ZLogger.logFailed("Cette plateforme n'est pas prise en charge pour les impressions", ZLogger.LVL_WARNING);
		}

		final File tmpDir = new File(temporaryDir);
		if (!tmpDir.exists()) {
			ZLogger.info("Tentative de creation du repertoire temporaire " + tmpDir);
			try {
				tmpDir.mkdirs();
				ZLogger.info("Repertoire " + tmpDir + " cree.");
			} catch (Exception e) {
				ZLogger.logFailed("Impossible de creer le repertoire " + tmpDir, ZLogger.LVL_WARNING);
			}
		}
		else {
			ZLogger.logKeyValue("Repertoire temporaire", tmpDir, ZLogger.LVL_INFO);
		}

		ZLogger.info("");
		ZLogger.info("");

	}

	/**
	 * S'occupe de la gestion de l'identification utilisateur. (Affichage boite de dialogue, initialisation des infos, etc.).
	 */
	private final boolean gereIdentification() {
		// - S'il faut utiliser l'autentification explicite
		//Boolean result = (Boolean)invokeRemoteMethod(editingContext,
		// "session", "clientSideRequestRequestsAuthentication", null);
		ServerProxy.clientSideRequestSetClientPlateforme(editingContext(), System.getProperty("os.name") + " " + System.getProperty("os.version"));
		String result = ServerProxy.clientSideRequestRequestsAuthenticationMode(editingContext());
		//Si authentification forte (SAUT ou SQL)
		if (result.equals("SAUT") || result.equals("SQL")) {
			String loginName = initialFwkCktlWebApp;
			String password = null;
			String cryptedLogin, cryptedPassword;
			boolean isUserOk = false;
			int cpt = MAXTENTATIVEFORLOGIN;
			String lastErreur = null;
			//            LoginDialogCtrl tmpLoginDialogCtrl = new LoginDialogCtrl();

			String casUserName = getCASUserName();
			System.out.println("usernameCAS = " + casUserName);

			//Si utilisateur deja identifie avec CAS on n'affiche pas de fenetre de login
			if (casUserName != null) {
				NSDictionary dico = ServerProxy.clientSideRequestGetUserIdentity(editingContext(), casUserName, null, Boolean.FALSE, Boolean.TRUE);

				//Si le dico n'est pas null, l'identification est correcte
				if (dico.valueForKey("erreur") != null) {
					showErrorDialog(new Exception(dico.valueForKey("erreur").toString()));
				}
				else {
					try {
						initUserInfo(dico);
						return true;
					} catch (Exception e) {
						showErrorDialog(e);
						//                        return false;
						//on continue en affichant une fenetre de login
					}
				}
			}

			ZLogger.verbose("Creation fenetre login");

			tmpLoginDialogCtrl2 = new ZLoginDialog((JFrame) null, getApplicationName() + " - Identifiez-vous...");
			waitingDialog.setVisible(false);
			//Tant qu'il reste des essais et que lu'tilisateur n'est pas encore identifié
			while ((cpt > 0) && (!isUserOk)) {
				cpt--;
				tmpLoginDialogCtrl2.setErrMsg(lastErreur);
				tmpLoginDialogCtrl2.setLogin(loginName);
				if (tmpLoginDialogCtrl2.open() == ZKarukeraDialog.MROK) {
					//Utilisateur a cliqué sur ok
					loginName = (tmpLoginDialogCtrl2.getLogin());
					password = tmpLoginDialogCtrl2.getPassword();

					String passAdmin = ServerProxy.clientSideRequestGetGrhumParam(editingContext(), "PASS_ADMIN");
					if (ZStringUtil.isEmpty(passAdmin) || !passAdmin.equals(password)) {
						//TODO On crypte le login et le password pour les
						// transmettre au serveur
						cryptedLogin = loginName;
						cryptedPassword = password;

						//tenter l'identification en passant par le serveur
						//                        NSArray params = new NSArray(new Object[] { cryptedLogin, cryptedPassword });
						NSDictionary dico = ServerProxy.clientSideRequestGetUserIdentity(editingContext(), cryptedLogin, cryptedPassword, Boolean.FALSE, Boolean.FALSE);

						//Si le dico n'est pas null, l'identification est correcte
						if (dico.valueForKey("erreur") != null) {
							showErrorDialog(new Exception(dico.valueForKey("erreur").toString()));
						}
						else {
							try {
								initUserInfo(dico);
								return true;
							} catch (Exception e) {
								showErrorDialog(e);
								return false;
							}
						}
					}
					else {
						try {
							appUserInfo = new AppUserInfoSuperUser();
							appUserInfo().initInfo(editingContext(), null, null, null, null);
							//                            changeCurrentExercice(EOsFinder.getDefaultExercice(editingContext()));
							changeCurrentExerciceDefault();
							return true;
						} catch (Exception e) {
							showErrorDialog(e);
							return false;
						}
					}

				}
				else {
					//Utilisateur a cliqué sur Annuler
					return false;
				}
			}
			if (cpt == 0) {
				showInfoDialog("Le nombre maximal de tentative d'identification est atteint. L'application va maintenant se fermer.");
				return false;
			}
		}
		// - Sinon utiliser le login systeme
		else {

			NSDictionary dico = ServerProxy.clientSideRequestGetUserIdentity(editingContext(), initialFwkCktlWebApp, null, Boolean.FALSE, Boolean.FALSE);
			//Si le dico n'est pas null, l'identification est correcte
			if (dico.valueForKey("erreur") != null) {
				showErrorDialog(new Exception(dico.valueForKey("erreur").toString()));
			}
			else {

				try {
					//                    appLog.trace("dico", dico);
					initUserInfo(dico);
					return true;
				} catch (Exception e) {
					showErrorDialog(e);
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Tente d'initialiser les informations de l'utilisateur (une fois que le serveur ait accepté le login). Des erreurs peuvent encore intervenir à
	 * ce niveau, et l'identification peut encore êtree refusée.
	 *
	 * @param dico
	 * @throws Exception
	 */
	private void initUserInfo(NSDictionary dico) throws Exception {
		appUserInfo = new AppUserInfo();
		Integer noInd = null;
		Integer persId = null;
		if (dico.valueForKey("persId") != null) {
			persId = new Integer(dico.valueForKey("persId").toString());
		}

		try {
			appUserInfo().initInfo(editingContext(), dico.valueForKey("login").toString(), noInd, persId, dico);
			appUserInfo().initPreferences(editingContext());
		} catch (Exception e) {
			e.printStackTrace();
			throw new InitException(
					"Une erreur s'est produite lors de la récupération de vos informations utilisateur. Veuillez vérifier auprès de l'administrateur de l'application que vous avez bien  les droits nécessaires pour accéder à cette application. \n" + e.getMessage());
		}

		//Récupère et initialise l'exercice par défaut
		//        changeCurrentExercice(EOsFinder.getDefaultExercice(editingContext()));
		changeCurrentExerciceDefault();
		ServerProxy.serverSetFwkCktlWebApp(editingContext(), dico.valueForKey("login").toString());

	}

	private void changeCurrentExerciceDefault() throws Exception {
		EOExercice ex = null;
		try {
			ex = EOsFinder.getDefaultExercice(editingContext());
		} catch (ZFinderException e) {
			e.printStackTrace();
		}

		//
		if (ex == null) {
			throw new Exception("Il n'y a aucun exercice de trésorerie défini. Prévenez un technicien pour qu'il règle le probleme.");
		}

		if (ex.estClos()) {
			throw new Exception("L'exercice de trésorerie (" + ex.exeExercice() + ") est clos. Prévenez un technicien pour qu'il règle le probleme.");
		}

		if (ex.estPreparation()) {
			throw new Exception("L'exercice de trésorerie (" + ex.exeExercice() + ") est en mode préparation. Prévenez un technicien pour qu'il règle le probleme.");
		}

		if (ex.estRestreint()) {
			throw new Exception("L'exercice de trésorerie (" + ex.exeExercice() + ") est en mode restreint. Prévenez un technicien pour qu'il règle le probleme.");
		}

		//Vérifier que l'exercice par defaut est bien celui en cours
		int exerciceReel = ZDateUtil.getYear(ZDateUtil.nowAsDate());

		//si exercice ok
		if (exerciceReel == ex.exeExercice().intValue() || isTestMode()) {
			changeCurrentExercice(ex);
		}
		//si exercice suivant
		else if (exerciceReel == ex.exeExercice().intValue() + 1) {
			System.out.println(">>>>> CHANGEMENT D'EXERCICE EN COURS");
			//Exercice de tresorerie n'est pas celui actuel, on tente de faire un changement
			final ChgtExerciceCtrl chgtExerciceCtrl = new ChgtExerciceCtrl();
			ex = chgtExerciceCtrl.effectuerChangement(editingContext(), ex);
			if (editingContext().hasChanges()) {
				editingContext().saveChanges();
			}
			changeCurrentExercice(ex);
			System.out.println(">>>>> CHANGEMENT D'EXERCICE OK");
		}
		//Exercice indetermine
		else {
			throw new Exception("L'exercice de trésorerie " + ex.exeExercice().intValue() + " est incohérent par rapport à la date système de votre ordinateur (" + ZConst.FORMAT_DATESHORT.format(ZDateUtil.nowAsDate())
					+ "). Ceci n'est autorisé que lorsque l'application est en mode TEST (à spécifier dans le fichier de configuration de l'application).");
		}
	}

	/**
	 * Initialise les informations liées à l'UI pour l'utilisateur.
	 */
	private void initUserInterface() {
		try {
			updateAllowedActionsForUser();
			if (toSuperviseur == null) {
				waitingDialog.setVisible(true);
				updateLoadingMsg("Initalisation des composants...");
				toSuperviseur = new Superviseur(getApplicationName());
			}
			toSuperviseur.initGUI();
			getMainWindow().addWindowListener(new MainWindowListener());
			getMainWindow().setLocation(new Point(10, 10));
			toSuperviseur.pack();
			updateDisplayExercice();

			final String strPosition = (String) appUserInfo().getAppUserPreferences().get(AppUserInfo.PREFKEY_MAIN_WIN_POSITION);
			if (strPosition != null) {
				String[] ss = strPosition.split("[,]");
				if (ss.length == 2) {
					int x = new Integer(ss[0]).intValue();
					int y = new Integer(ss[1]).intValue();

					Dimension screendim = Toolkit.getDefaultToolkit().getScreenSize();
					if (x - 50 >= screendim.getWidth() || y - 50 >= screendim.getHeight()) {
						EOPreference pref = EOPreference.fetchByKeyValue(getEditingContext(), EOPreference.PREF_KEY_KEY, AppUserInfo.PREFKEY_MAIN_WIN_POSITION);
						String loc = (pref != null ? (String) pref.valueForKey(EOPreference.PREF_DEFAULT_VALUE_KEY) : null);
						if (loc != null) {
							ss = strPosition.split("[,]");
							if (ss.length == 2) {
								x = new Integer(ss[0]).intValue();
								y = new Integer(ss[1]).intValue();
							}
						}
					}

					if (x + 200 >= screendim.getWidth() || y + 200 >= screendim.getHeight()) {
						x = 15;
						y = 15;
					}

					toSuperviseur.setLocation(x, y);

				}
			}
			toSuperviseur.open();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Initialise certaines données.
	 */
	private final void initDatas() {
		try {
			comptabilites = EOsFinder.getAllComptabilites(getEditingContext());
			if (comptabilites.count() > 0) {
				changeCurrentComptabilite((EOComptabilite) comptabilites.objectAtIndex(0));
			}

			//Refresh des lignes bud et des conventions (pour les origines)
			//            ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "gestionorigine.maj_origine",new NSDictionary());
			//            ZLogger.debug("gestionorigine.maj_origine effectue");
			//Dorenavant la mise a jour des origines est lancee depuis le serveur (classe Session) dans un thread.

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	/**
	 * @return l'objet représentant les infos utilisateurs.
	 * @see AppUserInfo
	 */
	public AppUserInfo appUserInfo() {
		return appUserInfo;
	}

	/**
	 * Affiche une boite de dialogue contenant diverses infos.
	 */
	public void showAboutDialog() {
		String msg = "";
		msg = msg + getApplicationName();
		msg = msg + "\n";
		msg = msg + "\nVersion :" + ServerProxy.serverVersion(editingContext());
		msg = msg + "\nUtilisateur : " + appUserInfo().getLogin();
		msg = msg + "\nBD :" + ServerProxy.serverBdConnexionName(editingContext());
		msg = msg + "\nJRE : " + getJREVersion();
		msg = msg + "\n" + ServerProxy.serverCopyright(editingContext());
		EODialogs.runInformationDialog("A propos", msg);
	}

	public final void showErrorDialog(Exception e, Window parentWindow) {
		CommonDialogs.showErrorDialog((parentWindow == null ? activeWindow() : parentWindow), e);
	}

	/**
	 * Affiche un message d'erreur à partir d'une exception.
	 *
	 * @param e
	 */
	public final void showErrorDialog(Exception e) {
		showErrorDialog(e, getMainWindow());
	}

	public final Window activeWindow() {
		return windowObserver().activeWindow();
	}

	public final void showInfoDialog(final String text) {
		showInfoDialog(text, null);
	}

	public final void showInfoDialog(final String text, Window parentWindow) {
		CommonDialogs.showInfoDialog((parentWindow == null ? activeWindow() : parentWindow), text);
	}

	public final void showWarningDialog(Dialog parentWindow, String text) {
		CommonDialogs.showWarningDialog((parentWindow == null ? activeWindow() : parentWindow), text);
	}

	public final void showinfoDialogFonctionNonImplementee() {
		showInfoDialog("Cette fonctionalité n'est pas encore implémentée");
	}

	public final void showinfoDialogFonctionNonImplementee(Window parentWindow) {
		showInfoDialog("Cette fonctionalité n'est pas encore implémentée", parentWindow);
	}

	/**
	 * Affiche un message de demande de confirmation. defaultRep = Oui / Non
	 *
	 * @param titre
	 * @param message
	 * @param defaultRep
	 * @return true si l'utilisateur a clique sur "Oui"
	 */
	public final boolean showConfirmationDialog(String titre, String message, String defaultRep) {
		return showConfirmationDialog(titre, message, defaultRep, null);
	}

	/**
	 * Affiche un message de demande de confirmation. defaultRep = Oui / Non
	 *
	 * @param titre
	 * @param message
	 * @param defaultRep
	 * @param parentWindow la fenetre parente
	 * @return true si l'utilisateur a clique sur "Oui"
	 * @see CommonDialogs#showConfirmationDialog(Window, String, String, String)
	 */
	public final boolean showConfirmationDialog(String titre, String message, String defaultRep, Window parentWindow) {
		return CommonDialogs.showConfirmationDialog((parentWindow == null ? activeWindow() : parentWindow), titre, message, defaultRep);

	}

	/**
	 * @param titre
	 * @param message
	 * @param defaultRep
	 * @param parentWindow
	 * @return
	 * @see CommonDialogs#showConfirmationCancelDialog(Window, String, String, String)
	 */
	public final int showConfirmationCancelDialog(String titre, String message, String defaultRep, Window parentWindow) {
		return CommonDialogs.showConfirmationCancelDialog((parentWindow == null ? activeWindow() : parentWindow), titre, message, defaultRep);
	}

	/**
	 * Initialise le TimeZone à utiliser dans l'application à partir de ceux du serveur d'application.
	 */
	private void initTimeZones() {
		ZLogger.logTitle("Initialisation du NSTimeZone", ZLogger.LVL_INFO);

		TimeZone.setDefault((TimeZone) appParametres().valueForKey(ServerProxy.DEFAULTTIMEZONE_KEY));
		//        NSTimeZone.setDefaultTimeZone(ServerProxy.clientSideRequestDefaultNSTimeZone(editingContext()));

		NSTimeZone.setDefaultTimeZone((NSTimeZone) appParametres().valueForKey(ServerProxy.DEFAULTNSTIMEZONE_KEY));
		ZLogger.logKeyValue("TimeZone par defaut ", appParametres().valueForKey(ServerProxy.DEFAULTTIMEZONE_KEY), ZLogger.LVL_INFO);
		ZLogger.logKeyValue("NSTimeZone par defaut ", NSTimeZone.defaultTimeZone(), ZLogger.LVL_INFO);
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		ZLogger.logKeyValue("NSTimeZone - in", ntf.defaultParseTimeZone(), ZLogger.LVL_INFO);
		ZLogger.logKeyValue("NSTimezone - out", ntf.defaultFormatTimeZone(), ZLogger.LVL_INFO);
		ZLogger.info("");
		ZLogger.debug("ZDateUtil.nowAsDate() = " + ZDateUtil.nowAsDate());
		ZLogger.debug("new NSTimestamp() = " + new NSTimestamp());
		ZLogger.debug("ZDateUtil.now() = " + ZDateUtil.now());
		ZLogger.debug("ZDateUtil.getNowAsCalendar() = " + ZDateUtil.getNowAsCalendar().getTime());

	}

	//    public boolean showLogs() {
	//        return ZLogger.currentLevel > ZLogger.LVL_NONE;
	//    }

	//    public boolean showFactoryLogs() {
	//        return true;
	//    }

	//    public boolean showTrace() {
	//        return ZLogger.currentLevel >= ZLogger.LVL_DEBUG;
	//    }

	/**
	 * Renvoie la fenêtre principale (ou null si celle-ci n'est pas encore créée).
	 */
	public Window getMainWindow() {
		if (toSuperviseur != null) {
			return toSuperviseur;
		}
		return null;
	}

	/**
	 * Renvoie la fenêtre principale (sous forme de JFrame) si celle-ci est créée.
	 */
	public JFrame getMainFrame() {
		if (toSuperviseur != null) {
			return (JFrame) getMainWindow();
		}
		return null;
	}

	public void closeAllWindowsExceptMain() {
		for (int i = windowObserver().visibleWindows().count() - 1; i >= 0; i--) {
			if ((Window) windowObserver().visibleWindows().objectAtIndex(i) != getMainWindow()) {
				((Window) windowObserver().visibleWindows().objectAtIndex(i)).dispose();
			}
		}
	}

	/** Ouvre un fichier (ou lance un programme externe) */
	public void openFile(String filePath) throws Exception {
		OperatingSystemHelper.openFile(filePath);
	}

	public void openPdfFile(String filePath) throws Exception {
		OperatingSystemHelper.openPdfFile(filePath);
	}


	/**
	 * enregistre-sous... un fichier (avec boite de dialogue) et propose son ouverture ensuite.
	 *
	 * @param parent
	 * @param filePath
	 * @throws Exception
	 */
	public void saveAsAndOpenFile(Window parent, String filePath) throws Exception {
		if (filePath != null) {
			String filePathFinal = fileCopyAsWithDlg(parent, filePath);
			if (filePathFinal != null) {
				if (showConfirmationDialog("Ouvrir le fichier", "Fichier enregistré, voulez-vous l'ouvrir?", ZMsgPanel.BTLABEL_YES)) {
					OperatingSystemHelper.openFile(filePathFinal);
				}
			}
		}

	}

	/**
	 * Enregistre un fichier texte
	 *
	 * @param filePath Chemin du fichier
	 * @param fileContent Contenu du fichier à écrire
	 * @throws Exception
	 */
	public final void saveTextFile(final String filePath, final String fileContent) throws Exception {
		try {
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write(fileContent);
			fileWriter.close();
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new Exception("Impossible d'enregistrer le fichier" + filePath + ". Vérifiez qu'il n'est pas déjà ouvert.\n" + exception.getMessage());
		}
	}

	public final String fileCopyAsWithDlg(Window parent, String filePath1) {
		File f2;
		try {
			final File f1 = new File(filePath1);

			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setCurrentDirectory(null);
			String tmpdir = fileChooser.getCurrentDirectory().getAbsolutePath();
			if (!tmpdir.endsWith(File.separator)) {
				tmpdir = tmpdir + File.separator;
			}

			f2 = new File(new File(tmpdir + f1.getName()).getCanonicalPath());
			fileChooser.setSelectedFile(f2);
			fileChooser.setAcceptAllFileFilterUsed(true);
			fileChooser.setDialogTitle("Enregistrer-sous..");

			int ret = fileChooser.showSaveDialog(parent);
			File file = fileChooser.getSelectedFile();

			if ((ret != JFileChooser.APPROVE_OPTION) || (file == null)) {
				return null;
			}

			if (file.exists()) {
				if (!showConfirmationDialog("Confirmation", "Le fichier " + file + " existe déjà, voulez-vous l'écraser ?", ZMsgPanel.BTLABEL_NO)) {
					return null;
				}
			}

			ZFileUtil.fileCopy(f1, file);
			return file.getAbsolutePath();
		} catch (Exception e) {
			showErrorDialog(e);
			return null;
		}
	}

	/**
	 * Quitte l'application client et informe le serveur.
	 */
	public void quitter() {
		try {
			ZLogger.info("Quitter...");

			//On memorise la position de la fenetre
			appUserInfo().savePreferenceUser(getEditingContext(), AppUserInfo.PREFKEY_MAIN_WIN_POSITION,
					superviseur().getX() + "," + superviseur().getY(),
					AppUserInfo.PREFDEFAULTVALUE_MAIN_WIN_POSITION,
					AppUserInfo.PREFDESCRIPTION_MAIN_WIN_POSITION);

			//On informe le serveur si possible
			if ((appUserInfo() != null) && (appUserInfo().getLogin() != null)) {
				ServerProxy.clientSideRequestSessDeconnect(editingContext(), appUserInfo().getLogin());
			}
			closeLogFile();
		} catch (Exception e) {
			quit();
		}
		quit();
	}

	/**
	 * @see ServerProxy#clientSideRequestGetAppAlias
	 */
	public String getAppAlias() {
		return ServerProxy.clientSideRequestGetAppAlias(editingContext());
	}

	public final String getApplicationName() {
		if (applicationName == null) {
			applicationName = ServerProxy.clientSideRequestGetApplicationName(editingContext());
		}
		return applicationName;
	}

	public class MyEOWindowObserver extends EOWindowObserver {
		public void windowActivated(WindowEvent event) {
			super.windowActivated(event);
			//Si l'évènement se produit sur la fenetre principale
			if (event.getWindow() == getMainWindow()) {
				//Récupérer toutes les fen^etres ouvertes et les mettre en
				// premier-plan
				for (int i = visibleWindows().count() - 1; i >= 0; i--) {
					if ((Window) visibleWindows().objectAtIndex(i) != getMainWindow()) {
						((Window) visibleWindows().objectAtIndex(i)).toFront();
					}
				}
			}
		}
	}

	/**
	 * Classe représentant le listener de la fenetre principale.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public class MainWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
			quitter();
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent arg0) {

		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			//            System.out.println("windowClosing");
			quitter();
		}
	}

	public static void setWaitCursorForWindow(Window window, boolean isWaiting) {
		if (isWaiting) {
			window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		}
		else {
			window.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	//	public void traiteException(Exception e) {
	//		if ( !(e instanceof UserActionException)) {
	//			e.printStackTrace();
	//		}
	//
	//		String text=e.getMessage();
	//		if ((text==null) || (text.trim().length()==0)) {
	//			e.printStackTrace();
	//			text = e.getStackTrace().toString() ;
	//		}
	//		else {
	//			text = e.getMessage();
	//		}
	//// MessagePanelCtrl toMessagePanelCtrl = new MessagePanelCtrl();
	//// toMessagePanelCtrl.runErrorDialog("Erreur", text);
	//	}

	/**
	 * Met à jour les actions autorisées par l'utilisateur (actions par défaut plus actions définies dans la BD).
	 */
	public void updateAllowedActionsForUser() {
		myActionsCtrl.setAllDefaultActionsEnabled(true);
		myActionsCtrl.setAllUserActionsEnabled(false);
		myActionsCtrl.setUserActionsWithIdListEnabled(appUserInfo().getAllowedFonctions(), true);
	}

	/**
	 * Change d'exercice en cours.
	 *
	 * @param newExercice
	 */
	public void changeCurrentExercice(EOExercice newExercice) {
		try {
			appUserInfo().changeCurrentExercice(editingContext(), newExercice);
			EOPlanComptableFinder.setCurrentExercice(newExercice);
			initParametresForExercice(newExercice);
			if (toSuperviseur != null) {
				updateDisplayExercice();

				//                updateAllowedActionsForUser();

				toSuperviseur.updateActionMap();
				toSuperviseur.updateInputMap();
				updateAllowedActionsForUser();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public void changeCurrentComptabilite(final EOComptabilite comptabilite) {
		appUserInfo().setCurrentComptabilite(comptabilite);
	}

	/**
	 * @return
	 */
	public ZActionCtrl getMyActionsCtrl() {
		return myActionsCtrl;
	}

	/**
	 * Crée et renvoie une datasource (utile pour initialiser des displayGroup).
	 *
	 * @param ec
	 * @param entityName
	 * @return
	 */
	public EODistributedDataSource getDatasourceForEntity(EOEditingContext ec, String entityName) {
		return new EODistributedDataSource(ec, entityName);
	}

	/**
	 * Affiche une selection des exercices accessibles et change l'exercice en cours.
	 */
	public void changerExercice() {
		final ZEOSelectionDialog dialog = createExerciceSelectionDialog();
		if (dialog != null) {
			dialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			if (dialog.open() == ZEOSelectionDialog.MROK) {
				NSArray selections = dialog.getSelectedEOObjects();

				if (!((selections == null) || (selections.count() == 0))) {
					changeCurrentExercice((EOExercice) selections.objectAtIndex(0));
					this.showInfoDialog("L'exercice en cours est désormais l'exercice " + appUserInfo().getCurrentExercice().exeExercice());
				}
			}
			dialog.dispose();
		}
	}

	/**
	 * Crée uen fenetre de selection des exercices pour selection par l'utilisateur.
	 *
	 * @return
	 */
	private final ZEOSelectionDialog createExerciceSelectionDialog() {
		EODisplayGroup myDisplayGroup = new EODisplayGroup();
		myDisplayGroup.setDataSource(this.getDatasourceForEntity(getEditingContext(), "Exercice"));
		Vector myCols = new Vector(4, 0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, "exeExercice", "Exercice");
		ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, "exeOuverture", "Date d'ouverture");
		col2.setFormatDisplay(DateFormat.getDateInstance(DateFormat.SHORT));
		col2.setColumnClass(Date.class);
		//        ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "exeStat", "Statut");
		ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, "etatLibelle", "Statut");
		ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, "typeLibelle", "Type");

		myCols.add(col1);
		myCols.add(col2);
		myCols.add(col3);
		myCols.add(col4);

		try {
			myDisplayGroup.setObjectArray(EOsFinder.getAllExercices(getEditingContext()));
			ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMainFrame(), "Sélection d'un exercice", myDisplayGroup, myCols, "Veuillez sélectionner un exercice dans la liste", null);
			return dialog;
		} catch (ZFinderException e) {
			this.showErrorDialog(e);
			return null;
		}

	}

	public void updateDisplayExercice() {
		toSuperviseur.myToolBar.updateExerciceLabel(appUserInfo().getCurrentExercice().exeExercice().toString());
		toSuperviseur.myToolBar.updateExerciceLabelBackground((Color) exerciceColorMap.get(appUserInfo().getCurrentExercice().exeStat()));
	}

	/**
	 * Indique si on veut que les tracess soient affichées
	 */
	public static boolean wantShowTrace() {
		//        return ServerProxy.clientSideRequestGetConfigParam(getEditingContext(), "SHOWTRACE").equals("1");
		return ZLogger.currentLevel >= ZLogger.LVL_DEBUG;
	}

	//    public void rechercher() {
	//        try {
	//            RechercheDialog myDial = new RechercheDialog(getMainFrame(), "Recherche", true);
	//            myDial.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	//            myDial.open();
	//
	//        } catch (Exception e) {
	//            this.showErrorDialog(e);
	//        }
	//    }

	/**
	 * @param id
	 * @return
	 */
	public ZAction getActionbyId(String id) {
		return myActionsCtrl.getActionbyId(id);
	}

	public EOFonction getFonctionByActionId(final String id) {
		return getActionbyId(id).getFonctionAssociee();
	}

	public final String getUrlForHelpId(final String id) {
		return ServerProxy.clientSideRequestGetUrlForHelpId(editingContext(), "CHANGELOG");
	}

	public final void showHelp(final String id) {
		if (id != null) {
			String url = getUrlForHelpId(id);
			if (url != null) {
				try {
					ZBrowserControl.displayURL(url);
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	public final void showUrl(final String url) {
		if (url != null) {
			try {
				ZBrowserControl.displayURL(url);
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	/**
	 * @return les parametres de la table MARACUJA.PARAMETRES
	 */
	public NSDictionary getParametres() {
		return parametres;
	}

	public String getParametre(String key, String defaultValue) {
		if (getParametres().valueForKey(key) == null) {
			return defaultValue;
		}
		return (String) getParametres().valueForKey(key);
	}

	/**
	 * Supprime ou non l'affichage/saisie des decimales dans les nombres, suivant le parametre FORMAT_USE_DECIMAL defini dans la table PARAMETRES.
	 */
	private final void initFormats() {
		boolean formatDontUseDecimal = CONST_NON.equals(getParametres().valueForKey("FORMAT_USE_DECIMAL"));

		if (formatDontUseDecimal) {
			((DecimalFormat) ZConst.FORMAT_DISPLAY_NUMBER).setParseIntegerOnly(true);
			((DecimalFormat) ZConst.FORMAT_EDIT_NUMBER).setParseIntegerOnly(true);
			((DecimalFormat) ZConst.FORMAT_DECIMAL).setParseIntegerOnly(true);

			((DecimalFormat) ZConst.FORMAT_DISPLAY_NUMBER).setMaximumFractionDigits(0);
			((DecimalFormat) ZConst.FORMAT_EDIT_NUMBER).setMaximumFractionDigits(0);
			((DecimalFormat) ZConst.FORMAT_DECIMAL).setMaximumFractionDigits(0);
		}

	}

	public String getDisplayMessageClient() {
		return displayMessageClient;
	}

	/**
     *
     */
	public void showLogViewer() {
		ZLogFileViewerCtrl win = new ZLogFileViewerCtrl(getEditingContext());
		try {
			win.openDialog(getMainWindow(), clientLogFile);
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

	}

	public final String getMailAdmin() {
		return adminMail;
	}

	public final void refreshEoEnterprisesObjects(final Window win) {
		final ZWaitingPanelDialog waitingPanelDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		;
		waitingPanelDialog.setTopText("Veuillez patienter...");
		try {
			final Thread refreshThread = new Thread() {
				/**
				 * @see java.lang.Thread#run()
				 */
				public void run() {
					ZLogger.debug("Recuperation des modifications depuis le serveur");
					waitingPanelDialog.setBottomText("Récupération des données modifiées depuis le serveur...");
					refreshData();
					ZLogger.debug("Modifications recuperees");
					waitingPanelDialog.setVisible(false);
				}
			};

			refreshThread.start();
			waitingPanelDialog.setVisible(true);

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			waitingPanelDialog.setVisible(false);
		}

	}

	//    public void updateAllowedFonctions() {
	//        appUserInfo.updateAllowedFonctions(getEditingContext(), appUserInfo.getCurrentExercice());
	//
	//    }

	public void refreshInterface() {
		changeCurrentExercice(appUserInfo.getCurrentExercice());
	}

	/**
	 * Renvoie la date du jour ou bien la date du 31/12 selon qu'on se trouve sur un exercice de tresorerie ou un exercice comptable. Si exercice est
	 * Comptable + clos/restreint, renvoie le 31/12 Si exercice est Comptable + o, renvoie le 31/12
	 *
	 * @return
	 * @throws DefaultClientException
	 */
	public final Date getDateJourneeComptable() throws DefaultClientException {
		final EOExercice currentExer = appUserInfo.getCurrentExercice();
		final Date dateReelle = ZDateUtil.getDateOnly(ZDateUtil.nowAsDate());
		final Date dateFinExercice = ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear(currentExer.exeExercice().intValue()));
		Date laDate = null;
		//Si exercice est clos, on renvoie null
		if (EOExercice.EXE_ETAT_CLOS.equals(currentExer.exeStat())) {
			laDate = null;
		}
		else {
			//Si on est sur l'exercice de tresorerie, on renvoie la date du jour
			if (EOExercice.EXE_TYPE_TRESORERIE.equals(currentExer.exeType())) {
				laDate = ZDateUtil.getDateOnly(ZDateUtil.nowAsDate());
			}
			//Si on est sur exercice comptable et qu'il est terminé, on renvoie la date du 31/12
			else if (dateReelle.getTime() > dateFinExercice.getTime()) {
				laDate = dateFinExercice;
			}
			else {
				laDate = dateReelle;
			}
		}
		if (laDate == null) {
			throw new DefaultClientException("Impossible de déterminer la date de la journée comptable. Vérifiez si l'exercice n''est pas clos.");
		}

		return laDate;
	}

	public final HashMap getExerciceColorMap() {
		return exerciceColorMap;
	}

	public NSDictionary appParametres() {
		if (_appParametres == null) {
			_appParametres = ServerProxy.clientSideRequestGetAppParametres(editingContext());
		}
		return _appParametres;
	}

	public Boolean isTestMode() {
		return ServerProxy.clientSideRequestIsTestMode(getEditingContext());
	}

	public Date getDateFinCurrentExercice() {
		return ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear(appUserInfo.getCurrentExercice().exeExercice().intValue()));
	}

	private void compareJarVersionsClientAndServer() throws Exception {
		Boolean isDevMode = CktlServerInvoke.clientSideRequestIsDevelopmentMode(getEditingContext());

		if (isDevMode) {
			System.out.println("Mode développement, pas de vérification de cohérence entre les versions des jars client et serveur.");
			return;
		}
		String serverJarVersion = CktlServerInvoke.clientSideRequestGetVersionFromJar(getEditingContext());
		String clientJarVersion = clientJarVersion();
		System.out.println("Serveur build : " + serverJarVersion);
		System.out.println("Client build : " + clientJarVersion);
		if (serverJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté serveur.");
		}
		if (clientJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté client.");
		}
		if (!serverJarVersion.equals(clientJarVersion)) {
			throw new Exception("Incoherence entre la version cliente et la version serveur. Les ressources web ne sont probablement pas à jour." + serverJarVersion + " / " + clientJarVersion);
		}
	}
	private String clientJarVersion() {
		String className = this.getClass().getName();
		VersionAppFromJar varAppFromJar = new VersionAppFromJar(className);
		return varAppFromJar.fullVersion();
	}

	public String getTemporaryDir() {
		return temporaryDir;
	}
}
