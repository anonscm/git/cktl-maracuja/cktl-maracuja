/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.maracuja.client;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZFrame extends JFrame {
	private BusyGlassPanel myBusyGlassPanel;

	/**
	 * @param owner
	 * @param title
	 * @param modal
	 * @throws java.awt.HeadlessException
	 */
	public ZFrame(String title)
			throws HeadlessException {
		super(title);
		myBusyGlassPanel = new BusyGlassPanel();
		setGlassPane(myBusyGlassPanel);
	}

	public void setWaitCursor(boolean bool) {
		if (bool) {
			this.getGlassPane().setVisible(true);
		}
		else {
			this.getGlassPane().setVisible(false);
		}
	}

	public void open() {
		//		centerWindow();
		this.setVisible(true);
	}

	public void centerWindow() {
		int screenWidth = (int) this.getGraphicsConfiguration().getBounds().getWidth();
		int screenHeight = (int) this.getGraphicsConfiguration().getBounds().getHeight();
		this.setLocation((screenWidth / 2) - ((int) this.getSize().getWidth() / 2), ((screenHeight / 2) - ((int) this.getSize().getHeight() / 2)));

	}

	// This method iconifies a frame; the maximized bits are not affected.
	public void iconify() {
		int state = getExtendedState();

		// Set the iconified bit
		state |= JFrame.ICONIFIED;

		// Iconify the frame
		setExtendedState(state);
	}

	// This method deiconifies a frame; the maximized bits are not affected.
	public void deiconify() {
		int state = getExtendedState();

		// Clear the iconified bit
		state &= ~JFrame.ICONIFIED;

		// Deiconify the frame
		setExtendedState(state);
	}

	// This method minimizes a frame; the iconified bit is not affected
	public void minimize() {
		int state = getExtendedState();

		// Clear the maximized bits
		state &= ~JFrame.MAXIMIZED_BOTH;

		// Maximize the frame
		setExtendedState(state);
	}

	// This method minimizes a frame; the iconified bit is not affected
	public void maximize() {
		int state = getExtendedState();

		// Set the maximized bits
		state |= JFrame.MAXIMIZED_BOTH;

		// Maximize the frame
		setExtendedState(state);
	}

	/**
	 * Permet de dï¿½finir un panel qui sert ï¿½ afficher un waitcursor et ï¿½ interdire toute modif sur le panel.
	 * 
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
	 */
	public final class BusyGlassPanel extends JPanel {
		public final Color COLOR_WASH = new Color(64, 64, 64, 32);

		public BusyGlassPanel() {
			super.setOpaque(false);
			super.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

			//Suck up them events!!!
			super.addKeyListener((new KeyAdapter() {
			}));
			super.addMouseListener((new MouseAdapter() {
			}));
			super.addMouseMotionListener((new MouseMotionAdapter() {
			}));
		}

		public final void paintComponent(Graphics p_graphics) {
			Dimension l_size = super.getSize();

			// Wash the pane with translucent gray.
			p_graphics.setColor(COLOR_WASH);
			p_graphics.fillRect(0, 0, l_size.width, l_size.height);

			// Paint a grid of white/black dots. 
			p_graphics.setColor(Color.white);
			for (int j = 3; j < l_size.height; j += 8) {
				for (int i = 3; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
			p_graphics.setColor(Color.black);
			for (int j = 4; j < l_size.height; j += 8) {
				for (int i = 4; i < l_size.width; i += 8) {
					p_graphics.fillRect(i, j, 1, 1);
				}
			}
		}
	}

}
