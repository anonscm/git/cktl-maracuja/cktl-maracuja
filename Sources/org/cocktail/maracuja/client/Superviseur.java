/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/* Superviseur.java created by tsaivre on Fri 20-Jun-2003 */
package org.cocktail.maracuja.client;

import java.awt.BorderLayout;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import org.cocktail.maracuja.client.MainMenu.IMainMenuModel;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinderException;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


public class Superviseur extends ZFrame {
	//    private final Dimension WINDOW_DIMENSION = new Dimension(510,500);
	protected JSplitPane splitPane;
	public MainMenu leMenu;
	public MainToolBar myToolBar;
	public JTabbedPane jtp;
	private JPanel contentPanel;
	private MainMenuPanel myMainMenuPanel;

	private final MainMenuModel mainMenuModel = new MainMenuModel();
	private ApplicationClient myApp = (ApplicationClient) EOApplication.sharedApplication();

	public Superviseur(String title) {
		super(title);
		leMenu = new MainMenu(mainMenuModel);
	}

	public void initGUI() {
		setWaitCursor(true);

		contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		//        contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		//Affecter le menu de l'application
		//this.getRootPane().setJMenuBar(leMenu);

		myMainMenuPanel = new MainMenuPanel();

		myMainMenuPanel.initGUI();

		//Construire la barre d'outils
		myToolBar = new MainToolBar("TBmain");
		myToolBar.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		//getContentPane().add(myToolBar, BorderLayout.PAGE_START);

		JPanel top = new JPanel(new BorderLayout());
		top.add(leMenu, BorderLayout.NORTH);
		top.add(myToolBar, BorderLayout.CENTER);

		getContentPane().add(top, BorderLayout.NORTH);
		getContentPane().add(myMainMenuPanel, BorderLayout.CENTER);
		getContentPane().add(buildLeftBottomPanel(), BorderLayout.SOUTH);

		//Inititlise les raccourcis clavier
		updateActionMap();
		updateInputMap();

		this.validate();
		this.pack();
	}

	protected void connectionWasEstablished() {
		ZLogger.debug("ici");
	}

	protected void initArchive() {
		//ne rien faire

	}

	private final JPanel buildLeftBottomPanel() {
		final JPanel p = new JPanel(new BorderLayout());
		final JLabel l = new JLabel(myApp.getApplicationName() + " - " + ServerProxy.serverVersion(getEditingContext()) + " - " + ServerProxy.serverBdConnexionName(getEditingContext()));
		l.setHorizontalAlignment(SwingConstants.LEFT);
		p.add(l);
		return p;
	}

	/**
	 * Initialise l'actionmap du superviseur à partir des actions autorisées pour l'utilisateur.
	 */
	public void updateActionMap() {
		final Iterator iter = myApp.appUserInfo().getAllowedActions(myApp.getMyActionsCtrl()).iterator();
		contentPanel.getActionMap().clear();
		while (iter.hasNext()) {
			final ZAction element = (ZAction) iter.next();
			System.out.println("element.getActionId()=" + element.getLibelle() + " / " + element.getActionId());
			contentPanel.getActionMap().put(element.getActionId(), element);
		}
	}

	/**
	 * Affecte les raccourcis claviers aux actions. Les raccourcis sonr ceux définis dans l'action (ACCELERATOR_KEY).
	 */
	public void updateInputMap() {
		contentPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).clear();
		final Iterator iter = myApp.appUserInfo().getAllowedActions(myApp.getMyActionsCtrl()).iterator();
		while (iter.hasNext()) {
			final ZAction element = (ZAction) iter.next();
			contentPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put((KeyStroke) element.getValue(Action.ACCELERATOR_KEY), element.getActionId());
		}
	}

	private final class MainMenuModel implements IMainMenuModel {
		private NSArray comptabilites;

		public NSArray getComptabilites() {
			if (comptabilites == null) {
				try {
					comptabilites = EOsFinder.getAllComptabilites(getEditingContext());
				} catch (ZFinderException e) {
					comptabilites = new NSArray();
					e.printStackTrace();
				}
			}
			return comptabilites;
		}

		public void onComptabiliteSelected(EOComptabilite comptabilite) {
			myApp.changeCurrentComptabilite(comptabilite);

		}

	}

	public EOEditingContext getEditingContext() {
		return myApp.editingContext();
	}

}
