/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcomptaguiswing.client.all.IConnectedUserInfos;
import org.cocktail.maracuja.client.finders.EOUtilisateurFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPreference;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.maracuja.client.metier.EOUtilisateurFonction;
import org.cocktail.maracuja.client.metier.EOUtilisateurPreference;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Informations décrivant l'utilisateur en cours.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class AppUserInfo implements IConnectedUserInfos {

	public final static String PREFKEY_LECTEUR_CHEQUE_ADRESSE = "LECTEUR_CHEQUE_ADRESSE";
	public final static String PREFDEFAULTVALUE_LECTEUR_CHEQUE_ADRESSE = "127.0.0.1";
	public final static String PREFDESCRIPTION_LECTEUR_CHEQUE_ADRESSE = "Adresse IP ou est connecté le lecteur de chèque utilisé par l'utilisateur";
	public static final String PREFKEY_MAIN_WIN_POSITION = "MAIN_WIN_POSITION";
	public static final String PREFDEFAULTVALUE_MAIN_WIN_POSITION = "15,15";
	public static final String PREFDESCRIPTION_MAIN_WIN_POSITION = "Position de la fenetre principale de Maracuja";

	protected EOExercice currentExercice;
	protected EOComptabilite currentComptabilite;

	/**
	 * Tableau de chaines.
	 */
	protected NSArray allowedFonctions;

	protected String login;
	protected EOUtilisateur utilisateur;
	protected NSDictionary userDico;

	protected String email;

	private final Map appUserPreferences;
	private Integer persId;
	private EOGrhumPersonne grhumPersonne;

	public Integer getPersId() {
		return persId;
	}

	public AppUserInfo() {
		super();
		appUserPreferences = new HashMap();
	}

	public String getLogin() {
		return login;
	}

	public String getName() {
		return getUtilisateur().getNomAndPrenom();
	}

	/**
	 * Récupère les infos utilisateur à partir du noIndividu ou à défaut du persId.
	 * 
	 * @param ec
	 * @param plogin
	 * @param noIndividu
	 * @param persId
	 * @param infos
	 * @throws Exception
	 */
	public void initInfo(EOEditingContext ec, String plogin, Integer noIndividu, Integer persId, NSDictionary infos) throws Exception {
		login = plogin;
		if (persId != null) {
			utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(ec, persId);
		}
		else {
			throw new Exception("Le persId est nuls. Impossible de récupérer les infos utilisateur.");
		}
		this.persId = persId;
		grhumPersonne = EOGrhumPersonne.fetchByKeyValue(ec, EOGrhumPersonne.PERS_ID_KEY, getPersId());
		userDico = infos;
		setEmail((String) userDico.valueForKey("email"));
		ZLogger.verbose(userDico);
		allowedFonctions = new NSArray();

	}

	/**
	 * Met a jour la liste des identifiants des fonctions autorisees.
	 * 
	 * @param ec
	 */
	public void updateAllowedFonctions(EOEditingContext ec, EOExercice exercice) {
		//		allowedFonctions = EOUtilisateurFinder.fetchAutorisationsForUtilisateur(ec, utilisateur);
		NSMutableArray tmp1 = new NSMutableArray();
		//On récupère les autorisations indépendantes des exercices
		final NSArray tmp = EOUtilisateurFinder.fetchUtilisateurFonctionsForUtilisateurAndExercice(ec, utilisateur, exercice);
		for (int i = 0; i < tmp.count(); i++) {
			if (((EOUtilisateurFonction) tmp.objectAtIndex(i)).fonction() != null && ((EOUtilisateurFonction) tmp.objectAtIndex(i)).fonction().fonIdInterne() != null) {
				if (tmp1.indexOfObject(((EOUtilisateurFonction) tmp.objectAtIndex(i)).fonction().fonIdInterne()) == NSArray.NotFound) {
					tmp1.addObject(((EOUtilisateurFonction) tmp.objectAtIndex(i)).fonction().fonIdInterne());
				}
			}
			else {
				System.out.println("Autorisation orpheline : " + tmp.objectAtIndex(i).toString());
			}
		}
		allowedFonctions = tmp1.immutableClone();
		System.out.println("fonctions autorisees : " + allowedFonctions);
	}


	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param string
	 */
	public void setEmail(String string) {
		email = string;
	}

	/**
	 * @return les fonctions autorisées pour l'utilisateur pour l'exercice en cours. Il s'agit d'un tableau de String (les ID des fonctions).
	 */
	public NSArray getAllowedFonctions() {
		return allowedFonctions;
	}

	/**
	 * @param actionCtrl
	 * @return les actions autorisées pour l'utilisateur.
	 */
	public ArrayList getAllowedActions(ZActionCtrl actionCtrl) {
		NSArray tmp = getAllowedFonctions();
		ArrayList actionList = new ArrayList(tmp.count());
		for (int i = 0; i < tmp.count(); i++) {
			String fonId = (String) tmp.objectAtIndex(i);
			if (actionCtrl.getActionbyId(fonId) != null) {
				actionList.add(actionCtrl.getActionbyId(fonId));
			}
		}
		return actionList;
	}

	/**
	 * Change l'exercice en cours, en mettant à jour également les droits de l'utilisateur en fonction de l'exercice.
	 * 
	 * @param newExercice
	 */
	public void changeCurrentExercice(EOEditingContext ec, EOExercice newExercice) throws UserActionException {
		if (newExercice == null) {
			throw new UserActionException("L'exercice sélectionné ne peut être nul.");
		}
		currentExercice = newExercice;
		updateAllowedFonctions(ec, newExercice);

	}

	/**
	 * Renvoi l'exercice en cours pour l'utilisateur.
	 */
	public EOExercice getCurrentExercice() {
		return currentExercice;
	}

	public Integer getCurrentExerciceAsInt() {
		EOExercice exerciceEnCours = getCurrentExercice();
		if (exerciceEnCours == null) {
			return null;
		}

		return exerciceEnCours.exeExercice();
	}

	/**
	 * @return
	 */
	public EOUtilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param fonction
	 * @return True si la fonction est autorisee pour l'utilisateur pour l'exercice en cours.
	 */
	public boolean isFonctionAutorisee(EOFonction fonction) {
		return (getAllowedFonctions().indexOfObject(fonction.fonIdInterne()) != NSArray.NotFound);
	}

	public boolean isFonctionAutoriseeByActionID(ZActionCtrl actionCtrl, String id) throws DefaultClientException {
		EOFonction fon = actionCtrl.getActionbyId(id).getFonctionAssociee();
		if (fon == null) {
			throw new DefaultClientException("La fonction " + id + " n'a pas été récupérée");
		}
		return isFonctionAutorisee(fon);
	}

	/**
	 * Renvoie les objets gestions autorisés pour l'utilisateur pour une action donnée (pour l'exercice en cours).
	 * 
	 * @param ec
	 * @param actionCtrl
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public NSArray getAuthorizedGestionsForActionID(EOEditingContext ec, ZActionCtrl actionCtrl, String id) throws Exception {
		final EOFonction fon = actionCtrl.getActionbyId(id).getFonctionAssociee();
		if (fon == null) {
			throw new DefaultClientException("La fonction " + id + " n'a pas été récupérée");
		}
		if (!isFonctionAutorisee(fon)) {
			throw new DefaultClientException("La fonction " + id + " n'est pas autorisée pour cet exercice.");
		}

		return EOUtilisateurFinder.getGestionsForUtilisateurAndExerciceAndFonction(ec, getUtilisateur(), getCurrentExercice(), fon);
	}

	/**
	 * @param ec
	 * @param actionCtrl
	 * @param actionId ID de la fonction
	 * @param keyForGestion le nom de l'attribut qui represente la relation gestion à utiliser dans le qualifier
	 * @return Un qualifier qui porte sur les codes gestions autorisés pour un utilisateur pour une fonction.
	 */
	public EOQualifier getQualifierForAutorizedGestionForActionID(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String actionId, final String keyForGestion) throws Exception {
		final NSArray gestions = getAuthorizedGestionsForActionID(ec, actionCtrl, actionId);
		final NSMutableArray quals = new NSMutableArray();
		for (int i = 0; i < gestions.count(); i++) {
			final EOGestion element = (EOGestion) gestions.objectAtIndex(i);
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(keyForGestion + "=%@", new NSArray(element)));
		}
		return new EOOrQualifier(quals);
	}

	/**
	 * Renvoie les codes gestions SACDs autorisés pour l'utilisateur pour la fonction identifiée par le parametre id.
	 * 
	 * @param ec
	 * @param actionCtrl
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public final NSArray getAllowedSacdsForFonction(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String id) throws Exception {
		final NSArray gestionsSacdTmp = EOsFinder.getSACDGestion(ec, getCurrentExercice());
		final NSArray autGestionsTmp = getAuthorizedGestionsForActionID(ec, actionCtrl, id);

		System.out.println("*$$$$$$$$$");
		System.out.println(gestionsSacdTmp);
		System.out.println("*$$$$$$$$$");
		System.out.println(autGestionsTmp);
		System.out.println("*");

		final ArrayList list = new ArrayList(2);
		list.add(gestionsSacdTmp);
		list.add(autGestionsTmp);
		return ZEOUtilities.intersectionOfNSArrayWithGlobalIds(list);
	}

	public final NSArray getAllowedCodesGestionForFonction(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String id) throws Exception {
		final NSArray gestionsSacdTmp = EOsFinder.getAllGestionsPourExercice(ec, getCurrentExercice());
		final NSArray autGestionsTmp = getAuthorizedGestionsForActionID(ec, actionCtrl, id);

		System.out.println("*$$$$$$$$$");
		System.out.println(gestionsSacdTmp);
		System.out.println("*$$$$$$$$$");
		System.out.println(autGestionsTmp);
		System.out.println("*");

		final ArrayList list = new ArrayList(2);
		list.add(gestionsSacdTmp);
		list.add(autGestionsTmp);
		return ZEOUtilities.intersectionOfNSArrayWithGlobalIds(list);
	}

	/**
	 * Renvoie les codes gestions Non SACDs autorisés pour l'utilisateur pour la fonction identifiée par le parametre id.
	 * 
	 * @param ec
	 * @param actionCtrl
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public final NSArray getAllowedNonSacdsForFonction(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String id) throws Exception {
		final NSArray gestionsNonSacdTmp = EOsFinder.getGestionNonSacd(ec, getCurrentExercice());
		final NSArray autGestionsTmp = getAuthorizedGestionsForActionID(ec, actionCtrl, id);
		final ArrayList list = new ArrayList(2);
		list.add(gestionsNonSacdTmp);
		list.add(autGestionsTmp);

		System.out.println("AppUserInfo.getAllowedNonSacdsForFonction() nonsacd=" + gestionsNonSacdTmp.count());
		System.out.println("AppUserInfo.getAllowedNonSacdsForFonction() nonsacd=" + gestionsNonSacdTmp);
		System.out.println("AppUserInfo.getAllowedNonSacdsForFonction() autGestionsTmp=" + autGestionsTmp);

		NSArray res = ZEOUtilities.intersectionOfNSArrayWithGlobalIds(list);
		System.out.println("intersection = " + res);
		return res;
	}

	/**
	 * Renvoie les comptabilites autorisés pour l'utilisateur pour une action donnée.
	 * 
	 * @param ec
	 * @param actionCtrl
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public NSArray getAuthorizedComptabilitesForActionID(EOEditingContext ec, ZActionCtrl actionCtrl, String id) throws Exception {
		NSArray tmp = getAuthorizedGestionsForActionID(ec, actionCtrl, id);
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < tmp.count(); i++) {
			EOGestion array_element = (EOGestion) tmp.objectAtIndex(i);
			if (res.indexOfObject(array_element.comptabilite()) == NSArray.NotFound) {
				res.addObject(array_element.comptabilite());
			}
		}
		return res;
	}

	public EOComptabilite getCurrentComptabilite() {
		return currentComptabilite;
	}

	public void setCurrentComptabilite(EOComptabilite currentComptabilite) {
		this.currentComptabilite = currentComptabilite;
	}

	public Map getAppUserPreferences() {
		return appUserPreferences;
	}

	/**
	 * Recupere les préferences de l'application et les affecte à l'utilisateur, puis récupère les préféences associees a l'utilisateur.
	 */
	public void initPreferences(final EOEditingContext editingContext) {
		final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOPreference.PREF_KEY_KEY, EOSortOrdering.CompareAscending);
		final NSArray prefAppli = ZFinder.fetchArray(editingContext, EOPreference.ENTITY_NAME, null, new NSArray(sortOrdering), true);

		final EOQualifier qualUtil = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurPreference.UTILISATEUR_KEY + "=%@", new NSArray(new Object[] {
				utilisateur
		}));

		final NSArray prefUtil = ZFinder.fetchArray(editingContext, EOUtilisateurPreference.ENTITY_NAME, qualUtil, null, true);
		appUserPreferences.clear();
		for (int i = 0; i < prefAppli.count(); i++) {
			final EOPreference element = (EOPreference) prefAppli.objectAtIndex(i);
			appUserPreferences.put(element.prefKey(), element.prefDefaultValue());
		}

		for (int i = 0; i < prefUtil.count(); i++) {
			final EOUtilisateurPreference element = (EOUtilisateurPreference) prefUtil.objectAtIndex(i);
			appUserPreferences.put(element.preference().prefKey(), element.upValue());
		}
	}

	/**
	 * Mémorise une préférence en mémoire et dans la base.
	 * 
	 * @param key
	 * @param value
	 * @throws Exception
	 */
	public final void savePreferenceUser(final EOEditingContext editingContext, final String key, final String value, final String defaultValue, final String description) throws Exception {
		ZLogger.verbose("savePreferenceUser");
		ServerProxy.clientSideRequestSavePreference(editingContext, utilisateur, key, value, defaultValue, description);
		initPreferences(editingContext);
	}

	public boolean isFonctionAutoriseeByActionID(String arg0) {
		return (getAllowedFonctions().indexOfObject(arg0) != NSArray.NotFound);
	}

	public EOGrhumPersonne getGrhumPersonne() {
		return grhumPersonne;
	}
}
