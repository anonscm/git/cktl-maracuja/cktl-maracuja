/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.EcritureDetailListPanel;
import org.cocktail.maracuja.client.common.ui.RecetteListPanel;
import org.cocktail.maracuja.client.common.ui.TitreListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOModeRecouvrement;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecetteSuiviSrchPanel extends ZKarukeraPanel {
	private IRecetteSuiviSrchPanelModel myModel;

	private RecetteSuiviSrchFilterPanel filterPanel;
	private MyTitreListPanel titreListPanel;
	private MyRecetteListPanel recetteListPanel;
	private MyTitreDetailEcritureListPanel titreDetailEcritureListPanel;
	private MyEcritureDetailEmargementListPanel ecritureDetailEmargementListPanel;

	/**
	 * @param editingContext
	 */
	public RecetteSuiviSrchPanel(IRecetteSuiviSrchPanelModel listener) {
		super();
		myModel = listener;

		filterPanel = new RecetteSuiviSrchFilterPanel(myModel.recetteSuiviSrchFilterPanelListener());
		titreListPanel = new MyTitreListPanel(myModel.titresListener());
		recetteListPanel = new MyRecetteListPanel(myModel.recettesListener());
		titreDetailEcritureListPanel = new MyTitreDetailEcritureListPanel(myModel.titresDetailEcritureListener());
		ecritureDetailEmargementListPanel = new MyEcritureDetailEmargementListPanel(myModel.ecritureDetailEmargementListPanelListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		filterPanel.setMyDialog(getMyDialog());
		filterPanel.initGUI();
		titreListPanel.initGUI();
		recetteListPanel.initGUI();
		MyEcriturePanel ecriturePanel = new MyEcriturePanel();
		ecriturePanel.initGUI();

		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Factures", recetteListPanel);
		tabbedPane.addTab("Ecritures", ecriturePanel);

		tabbedPane.addChangeListener(new TabListener());
		//        tabbedPane.getModel().setSelectedIndex(-1);

		this.setLayout(new BorderLayout());
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
				encloseInPanelWithTitle("Titres", null, ZConst.BG_COLOR_TITLE, titreListPanel, null, null),
				encloseInPanelWithTitle("Détails", null, ZConst.BG_COLOR_TITLE, tabbedPane, null, null)), BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(tmp, BorderLayout.CENTER);
	}

	private final class MyEcriturePanel extends ZKarukeraPanel {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
		 */
		public void initGUI() {
			titreDetailEcritureListPanel.initGUI();
			ecritureDetailEmargementListPanel.initGUI();

			JComponent c = ZKarukeraPanel.buildVerticalSplitPane(titreDetailEcritureListPanel, encloseInPanelWithTitle("Ecritures émargées", null, ZConst.BG_COLOR_TITLE, ecritureDetailEmargementListPanel, null, null));
			setLayout(new BorderLayout());
			add(c, BorderLayout.CENTER);
			add(buildRightPanel(myModel.actionsForEcritureDetailEmargements()), BorderLayout.EAST);

		}

		private final JPanel buildRightPanel(ArrayList actions) {
			JPanel tmp = new JPanel(new BorderLayout());
			tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
			tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(actions), BorderLayout.NORTH);
			tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
			return tmp;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
		 */
		public void updateData() throws Exception {
			titreDetailEcritureListPanel.updateData();
			ecritureDetailEmargementListPanel.updateData();
		}

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		titreListPanel.updateData();
		recetteListPanel.updateData();
		titreDetailEcritureListPanel.updateData();
		ecritureDetailEmargementListPanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();

		//        list.add(myModel.getActionImprimerAll());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	public interface IRecetteSuiviSrchPanelModel {
		public RecetteSuiviSrchFilterPanel.IRecetteSuiviSrchFilterPanelListener recetteSuiviSrchFilterPanelListener();

		public ArrayList actionsForTitreDetailEcritures();

		public ArrayList actionsForEcritureDetailEmargements();

		public IZKarukeraTablePanelListener ecritureDetailEmargementListPanelListener();

		public IZKarukeraTablePanelListener recettesListener();

		public IZKarukeraTablePanelListener titresListener();

		public IZKarukeraTablePanelListener titresDetailEcritureListener();

		public void onTabSelected(ZKarukeraPanel panel);
	}

	public MyTitreListPanel getTitreListPanel() {
		return titreListPanel;
	}

	public MyRecetteListPanel getRecetteListPanel() {
		return recetteListPanel;
	}

	public class MyRecetteListPanel extends RecetteListPanel {

		/**
		 * @param listener
		 */
		public MyRecetteListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
		}

	}

	public class MyTitreListPanel extends TitreListPanel {
		public static final String COL_ORGAN = "organ.longString";
		public static final String COL_MODE_COUVREMENT = EOTitre.MODE_RECOUVREMENT_KEY + "." + EOModeRecouvrement.MOD_LIBELLE_LONG_KEY;

		/**
		 * @param listener
		 */
		public MyTitreListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn organ = new ZEOTableModelColumn(myDisplayGroup, COL_ORGAN, "Ligne budgétaire", 190);
			organ.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn colEtat = new ZEOTableModelColumn(myDisplayGroup, COL_ETAT, "Etat", 80);
			colEtat.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn colModeRecouvrement = new ZEOTableModelColumn(myDisplayGroup, COL_MODE_COUVREMENT, "Mode recouvrement", 80);
			colModeRecouvrement.setAlignment(SwingConstants.CENTER);

			LinkedHashMap colsNew = new LinkedHashMap();
			colsNew.put(COL_GESTION, colsMap.get(COL_GESTION));
			colsNew.put(COL_BORNUM, colsMap.get(COL_BORNUM));
			colsNew.put(COL_TIT_NUMERO, colsMap.get(COL_TIT_NUMERO));
			colsNew.put(COL_ORGAN, organ);
			colsNew.put(COL_TIT_LIBELLE, colsMap.get(COL_TIT_LIBELLE));
			colsNew.put(COL_TITTTC, colsMap.get(COL_TITTTC));
			colsNew.put(COL_MODE_COUVREMENT, colModeRecouvrement);
			colsNew.put(COL_ETAT, colEtat);

			colsMap = colsNew;
		}

	}

	public class MyEcritureDetailEmargementListPanel extends EcritureDetailListPanel {
		public static final String COL_ECRNUM = "ecriture.ecrNumero";

		public MyEcritureDetailEmargementListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup, COL_ECRNUM, "N° écriture", 68);
			ecrNumero.setAlignment(SwingConstants.LEFT);

			LinkedHashMap colsNew = new LinkedHashMap();
			colsNew.put(COL_ECRNUM, ecrNumero);
			colsNew.put(COL_GESCODE, colsMap.get(COL_GESCODE));
			colsNew.put(COL_PCONUM, colsMap.get(COL_PCONUM));
			colsNew.put(COL_ECDLIBELLE, colsMap.get(COL_ECDLIBELLE));
			colsNew.put(COL_ECDDEBIT, colsMap.get(COL_ECDDEBIT));
			colsNew.put(COL_ECDCREDIT, colsMap.get(COL_ECDCREDIT));
			colsNew.put(COL_ECDRESTEEMARGER, colsMap.get(COL_ECDRESTEEMARGER));

			colsMap = colsNew;
		}
	}

	public class MyTitreDetailEcritureListPanel extends ZKarukeraTablePanel {
		public static final String COL_EXE_ORDRE = "ecritureDetail.exercice.exeExercice";
		public static final String COL_ORIGINE = "tdeOrigine";
		public static final String COL_GESTION = "ecritureDetail.gestion.gesCode";
		public static final String COL_PCONUM = "ecritureDetail.planComptable.pcoNum";
		public static final String COL_PCOLIBELLE = "ecritureDetail.planComptable.pcoLibelle";
		public static final String COL_ECDLIBELLE = "ecritureDetail.ecdLibelle";
		public static final String COL_DEBIT = "ecritureDetail.ecdDebit";
		public static final String COL_CREDIT = "ecritureDetail.ecdCredit";
		public static final String COL_RESTEEMARGER = "ecritureDetail.ecdResteEmarger";
		public static final String COL_ECRNUMERO = "ecritureDetail.ecriture.ecrNumero";

		/**
         *
         */
		public MyTitreDetailEcritureListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn colExeOrdre = new ZEOTableModelColumn(myDisplayGroup, COL_EXE_ORDRE, "Exercice", 80);
			colExeOrdre.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn colGescode = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code gestion", 80);
			colGescode.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn origine = new ZEOTableModelColumn(myDisplayGroup, COL_ORIGINE, "Type", 80);
			origine.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn pcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imp.", 80);
			pcoNum.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup, COL_ECRNUMERO, "N° écriture", 80);
			ecrNumero.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn ecdLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_ECDLIBELLE, "Libellé", 409);
			ecdLibelle.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn colDebit = new ZEOTableModelColumn(myDisplayGroup, COL_DEBIT, "Débit", 80);
			colDebit.setAlignment(SwingConstants.RIGHT);
			colDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			colDebit.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn colCredit = new ZEOTableModelColumn(myDisplayGroup, COL_CREDIT, "Crédit", 80);
			colCredit.setAlignment(SwingConstants.RIGHT);
			colCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			colCredit.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn resteEmarger = new ZEOTableModelColumn(myDisplayGroup, COL_RESTEEMARGER, "Reste à émarger", 85);
			resteEmarger.setAlignment(SwingConstants.RIGHT);
			resteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			resteEmarger.setColumnClass(BigDecimal.class);

			colsMap.clear();

			colsMap.put(COL_ORIGINE, origine);
			colsMap.put(COL_EXE_ORDRE, colExeOrdre);
			colsMap.put(COL_ECRNUMERO, ecrNumero);
			colsMap.put(COL_GESTION, colGescode);
			colsMap.put(COL_PCONUM, pcoNum);
			colsMap.put(COL_PCOLIBELLE, ecdLibelle);
			colsMap.put(COL_DEBIT, colDebit);
			colsMap.put(COL_CREDIT, colCredit);
			colsMap.put(COL_RESTEEMARGER, resteEmarger);
		}

	}

	private final class TabListener implements ChangeListener {

		/**
		 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
		 */
		public void stateChanged(ChangeEvent e) {
			JTabbedPane pane = (JTabbedPane) e.getSource();
			myModel.onTabSelected((ZKarukeraPanel) pane.getSelectedComponent());
		}

	}

	public MyTitreDetailEcritureListPanel getTitreDetailEcritureListPanel() {
		return titreDetailEcritureListPanel;
	}

	public MyEcritureDetailEmargementListPanel getEcritureDetailEmargementListPanel() {
		return ecritureDetailEmargementListPanel;
	}

	public void setTitreDetailEcritureListPanel(MyTitreDetailEcritureListPanel titreDetailEcritureListPanel) {
		this.titreDetailEcritureListPanel = titreDetailEcritureListPanel;
	}
}
