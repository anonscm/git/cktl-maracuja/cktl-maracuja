/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DepRecPanel extends ZKarukeraPanel {
    private JTabbedPane tabbedPane;
    private IDepRecPanelListener myListener;


    /**
     *
     */
    public DepRecPanel(IDepRecPanelListener listener) {
        super();
        myListener = listener;
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#initGUI()
     */
    public void initGUI() {
        myListener.getDepenseSuiviSrchPanel().initGUI();
        myListener.getRecetteSuiviSrchPanel().initGUI();

        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Dépenses", myListener.getDepenseSuiviSrchPanel());
        tabbedPane.addTab("Recettes", myListener.getRecetteSuiviSrchPanel());

        tabbedPane.addChangeListener(new TabListener());
        tabbedPane.getModel().setSelectedIndex(-1);

        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(5,1,1,1));
        add(tabbedPane, BorderLayout.CENTER);
        add(buildBottomPanel(), BorderLayout.SOUTH);

    }


    private JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionClose());
        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
        return p;
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
     */
    public void updateData() throws Exception {
        ((ZKarukeraPanel)tabbedPane.getSelectedComponent()).updateData();
    }

    public interface IDepRecPanelListener {
        public DepenseSuiviSrchPanel getDepenseSuiviSrchPanel();
        public RecetteSuiviSrchPanel getRecetteSuiviSrchPanel();
        public Action actionClose();
        public void onTabSelected(ZKarukeraPanel panel);
    }

	private final class TabListener implements ChangeListener {

        /**
         * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
         */
        public void stateChanged(ChangeEvent e) {
            JTabbedPane pane = (JTabbedPane)e.getSource();
            myListener.onTabSelected((ZKarukeraPanel)pane.getSelectedComponent());
        }

	}


    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }
}
