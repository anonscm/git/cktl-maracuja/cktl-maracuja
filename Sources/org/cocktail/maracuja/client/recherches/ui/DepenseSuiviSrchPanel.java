/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.DepenseListPanel;
import org.cocktail.maracuja.client.common.ui.EcritureDetailListPanel;
import org.cocktail.maracuja.client.common.ui.MandatListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.im.ui.ImSuspensionListPanel;
import org.cocktail.maracuja.client.metier.EOImSuspension;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DepenseSuiviSrchPanel extends ZKarukeraPanel {
	private final IDepenseSuiviSrchPanelModel myModel;

	private final DepenseSuiviSrchFilterPanel filterPanel;
	private final MyMandatListPanel mandatListPanel;
	private final MyDepenseListPanel depenseListPanel;
	private final MyMandatDetailEcritureListPanel mandatDetailEcritureListPanel;
	private final MyEcritureDetailEmargementListPanel ecritureDetailEmargementListPanel;
	private final MyImSuspensionListPanel imSuspensionListPanel;

	public DepenseSuiviSrchPanel(IDepenseSuiviSrchPanelModel listener) {
		super();
		myModel = listener;

		filterPanel = new DepenseSuiviSrchFilterPanel(myModel.depenseSuiviSrchFilterPanelListener());
		mandatListPanel = new MyMandatListPanel(myModel.mandatsListener());
		depenseListPanel = new MyDepenseListPanel(myModel.depensesListener());
		mandatDetailEcritureListPanel = new MyMandatDetailEcritureListPanel(myModel.mandatsDetailEcritureListener());
		ecritureDetailEmargementListPanel = new MyEcritureDetailEmargementListPanel(myModel.ecritureDetailEmargementListPanelListener());
		imSuspensionListPanel = new MyImSuspensionListPanel(myModel.imSuspensionListPanelListener());

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		filterPanel.setMyDialog(getMyDialog());
		filterPanel.initGUI();
		mandatListPanel.initGUI();
		depenseListPanel.initGUI();
		mandatDetailEcritureListPanel.initGUI();
		imSuspensionListPanel.initGUI();
		MyEcriturePanel ecriturePanel = new MyEcriturePanel();
		ecriturePanel.initGUI();

		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Factures", depenseListPanel);
		tabbedPane.addTab("Ecritures", ecriturePanel);
		tabbedPane.addTab("Suspensions DGP", imSuspensionListPanel);

		tabbedPane.addChangeListener(new TabListener());
		//        tabbedPane.getModel().setSelectedIndex(-1);

		this.setLayout(new BorderLayout());
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		tmp.add(ZKarukeraPanel.buildVerticalSplitPane(encloseInPanelWithTitle("Mandats", null, ZConst.BG_COLOR_TITLE, mandatListPanel, null, null), encloseInPanelWithTitle("Détails", null, ZConst.BG_COLOR_TITLE, tabbedPane, null, null)), BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		mandatListPanel.updateData();
		depenseListPanel.updateData();
		mandatDetailEcritureListPanel.updateData();
		ecritureDetailEmargementListPanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();

		//        list.add(myModel.getActionImprimerAll());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	public interface IDepenseSuiviSrchPanelModel {
		public DepenseSuiviSrchFilterPanel.IDepenseSuiviSrchFilterPanelListener depenseSuiviSrchFilterPanelListener();

		public IMyImSuspensionListPanelListener imSuspensionListPanelListener();

		public IZKarukeraTablePanelListener ecritureDetailEmargementListPanelListener();

		public IZKarukeraTablePanelListener depensesListener();

		public IZKarukeraTablePanelListener mandatsListener();

		public IZKarukeraTablePanelListener mandatsDetailEcritureListener();

		public void onTabSelected(ZKarukeraPanel panel);

		public ArrayList actionsForEcritureDetailEmargements();

	}

	public MyMandatListPanel getMandatListPanel() {
		return mandatListPanel;
	}

	public MyDepenseListPanel getDepenseListPanel() {
		return depenseListPanel;
	}

	public class MyDepenseListPanel extends DepenseListPanel {

		/**
		 * @param listener
		 */
		public MyDepenseListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
		}

	}

	public class MyMandatListPanel extends MandatListPanel {
		public static final String COL_MANETAT = "manEtat";
		public static final String COL_MODEPAIEMENT = "modePaiement.modLibelleLong";
		public static final String COL_PAIEMENTNUM = "paiement.paiNumero";
		public static final String COL_PAIEMENTDATE = "paiement.paiDateCreation";
		public static final String COL_PAIEMENTTYPE = "paiement.typeVirement.tviLibelle";

		/**
		 * @param listener
		 */
		public MyMandatListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn paiNumero = new ZEOTableModelColumn(myDisplayGroup, COL_PAIEMENTNUM, "N° Paiement", 80);
			paiNumero.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn paiDate = new ZEOTableModelColumn(myDisplayGroup, COL_PAIEMENTDATE, "Date Paiement", 80);
			paiDate.setAlignment(SwingConstants.CENTER);
			paiDate.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			paiDate.setColumnClass(Date.class);

			ZEOTableModelColumn paiType = new ZEOTableModelColumn(myDisplayGroup, COL_PAIEMENTTYPE, "Type paiement", 90);
			paiType.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn modePaiement = new ZEOTableModelColumn(myDisplayGroup, COL_MODEPAIEMENT, "Mode paiement", 90);
			modePaiement.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn manEtat = new ZEOTableModelColumn(myDisplayGroup, COL_MANETAT, "Etat", 80);
			manEtat.setAlignment(SwingConstants.CENTER);

			colsMap.put(COL_MANETAT, manEtat);
			colsMap.put(COL_MODEPAIEMENT, modePaiement);
			colsMap.put(COL_PAIEMENTNUM, paiNumero);
			colsMap.put(COL_PAIEMENTDATE, paiDate);
			colsMap.put(COL_PAIEMENTTYPE, paiType);
		}

	}

	public class MyEcritureDetailEmargementListPanel extends EcritureDetailListPanel {
		public static final String COL_ECRNUM = "ecriture.ecrNumero";

		public MyEcritureDetailEmargementListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup, COL_ECRNUM, "N° écriture", 68);
			ecrNumero.setAlignment(SwingConstants.LEFT);

			LinkedHashMap colsNew = new LinkedHashMap();
			colsNew.put(COL_ECRNUM, ecrNumero);
			colsNew.put(COL_GESCODE, colsMap.get(COL_GESCODE));
			colsNew.put(COL_PCONUM, colsMap.get(COL_PCONUM));
			colsNew.put(COL_ECDLIBELLE, colsMap.get(COL_ECDLIBELLE));
			colsNew.put(COL_ECDDEBIT, colsMap.get(COL_ECDDEBIT));
			colsNew.put(COL_ECDCREDIT, colsMap.get(COL_ECDCREDIT));
			colsNew.put(COL_ECDRESTEEMARGER, colsMap.get(COL_ECDRESTEEMARGER));

			colsMap = colsNew;
		}
	}

	private final class MyEcriturePanel extends ZKarukeraPanel {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
		 */
		public void initGUI() {
			mandatDetailEcritureListPanel.initGUI();
			ecritureDetailEmargementListPanel.initGUI();

			JComponent c = ZKarukeraPanel.buildVerticalSplitPane(mandatDetailEcritureListPanel, encloseInPanelWithTitle("Ecritures émargées", null, ZConst.BG_COLOR_TITLE, ecritureDetailEmargementListPanel, null, null));
			setLayout(new BorderLayout());
			add(c, BorderLayout.CENTER);
			add(buildRightPanel(myModel.actionsForEcritureDetailEmargements()), BorderLayout.EAST);

		}

		private final JPanel buildRightPanel(ArrayList actions) {
			JPanel tmp = new JPanel(new BorderLayout());
			tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
			tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(actions), BorderLayout.NORTH);
			tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
			return tmp;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
		 */
		public void updateData() throws Exception {
			mandatDetailEcritureListPanel.updateData();
			ecritureDetailEmargementListPanel.updateData();
		}

	}

	public class MyMandatDetailEcritureListPanel extends ZKarukeraTablePanel {
		public static final String COL_EXE_ORDRE = "ecritureDetail.exercice.exeExercice";
		public static final String COL_ORIGINE = "mdeOrigine";
		public static final String COL_GESTION = "ecritureDetail.gestion.gesCode";
		public static final String COL_PCONUM = "ecritureDetail.planComptable.pcoNum";
		public static final String COL_PCOLIBELLE = "ecritureDetail.planComptable.pcoLibelle";
		public static final String COL_ECDLIBELLE = "ecritureDetail.ecdLibelle";
		public static final String COL_DEBIT = "ecritureDetail.ecdDebit";
		public static final String COL_CREDIT = "ecritureDetail.ecdCredit";
		public static final String COL_RESTEEMARGER = "ecritureDetail.ecdResteEmarger";
		public static final String COL_ECRNUMERO = "ecritureDetail.ecriture.ecrNumero";

		/**
         *
         */
		public MyMandatDetailEcritureListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn colExeOrdre = new ZEOTableModelColumn(myDisplayGroup, COL_EXE_ORDRE, "Exercice", 80);
			colExeOrdre.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn colGescode = new ZEOTableModelColumn(myDisplayGroup, COL_GESTION, "Code gestion", 80);
			colGescode.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn origine = new ZEOTableModelColumn(myDisplayGroup, COL_ORIGINE, "Type", 80);
			origine.setAlignment(SwingConstants.CENTER);

			ZEOTableModelColumn pcoNum = new ZEOTableModelColumn(myDisplayGroup, COL_PCONUM, "Imp.", 80);
			pcoNum.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn ecrNumero = new ZEOTableModelColumn(myDisplayGroup, COL_ECRNUMERO, "N° écriture", 80);
			ecrNumero.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn ecdLibelle = new ZEOTableModelColumn(myDisplayGroup, COL_ECDLIBELLE, "Libellé", 409);
			ecdLibelle.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumn colDebit = new ZEOTableModelColumn(myDisplayGroup, COL_DEBIT, "Débit", 80);
			colDebit.setAlignment(SwingConstants.RIGHT);
			colDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			colDebit.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn colCredit = new ZEOTableModelColumn(myDisplayGroup, COL_CREDIT, "Crédit", 80);
			colCredit.setAlignment(SwingConstants.RIGHT);
			colCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			colCredit.setColumnClass(BigDecimal.class);

			ZEOTableModelColumn resteEmarger = new ZEOTableModelColumn(myDisplayGroup, COL_RESTEEMARGER, "Reste à émarger", 85);
			resteEmarger.setAlignment(SwingConstants.RIGHT);
			resteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
			resteEmarger.setColumnClass(BigDecimal.class);

			colsMap.clear();

			colsMap.put(COL_ORIGINE, origine);
			colsMap.put(COL_EXE_ORDRE, colExeOrdre);
			colsMap.put(COL_ECRNUMERO, ecrNumero);
			colsMap.put(COL_GESTION, colGescode);
			colsMap.put(COL_PCONUM, pcoNum);
			colsMap.put(COL_PCOLIBELLE, ecdLibelle);
			colsMap.put(COL_DEBIT, colDebit);
			colsMap.put(COL_CREDIT, colCredit);
			colsMap.put(COL_RESTEEMARGER, resteEmarger);
		}

	}

	private final class TabListener implements ChangeListener {

		/**
		 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
		 */
		public void stateChanged(ChangeEvent e) {
			JTabbedPane pane = (JTabbedPane) e.getSource();
			myModel.onTabSelected((ZKarukeraPanel) pane.getSelectedComponent());
		}

	}

	public class MyImSuspensionListPanel extends ImSuspensionListPanel {

		private final IMyImSuspensionListPanelListener _listener;

		public MyImSuspensionListPanel(IMyImSuspensionListPanelListener listener) {
			super(listener);
			_listener = listener;
		}

		public void initGUI() {
			super.initGUI();
			add(buildImSuspensionsToolBar(), BorderLayout.EAST);
		}

		private JToolBar buildImSuspensionsToolBar() {
			JToolBar tmpToolBar = new JToolBar("", JToolBar.VERTICAL);
			tmpToolBar.setFloatable(false);
			tmpToolBar.setRollover(false);
			tmpToolBar.add(Box.createRigidArea(new Dimension(2, 1)));
			tmpToolBar.add(_listener.getActionAddImSuspension());
			tmpToolBar.add(_listener.getActionModifyImSuspension());
			tmpToolBar.add(_listener.getActionDeleteImSuspension());
			return tmpToolBar;
		}

		public void updateData() throws Exception {
			super.updateData();
			_listener.refreshActions();
		}

		public EOImSuspension selectedImSuspension() {
			return (EOImSuspension) selectedObject();
		}

	}

	public interface IMyImSuspensionListPanelListener extends IZKarukeraTablePanelListener {
		public Action getActionAddImSuspension();

		public void refreshActions();

		public Action getActionModifyImSuspension();

		public Action getActionDeleteImSuspension();
	}

	public MyMandatDetailEcritureListPanel getMandatDetailEcritureListPanel() {
		return mandatDetailEcritureListPanel;
	}

	public MyEcritureDetailEmargementListPanel getEcritureDetailEmargementListPanel() {
		return ecritureDetailEmargementListPanel;
	}

	public MyImSuspensionListPanel getImSuspensionListPanel() {
		return imSuspensionListPanel;
	}
}
