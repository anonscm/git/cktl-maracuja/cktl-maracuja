/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DepenseSuiviSrchFilterPanel extends ZKarukeraPanel {
	private final Color BORDURE_COLOR = getBackground().brighter();

	private IDepenseSuiviSrchFilterPanelListener myListener;

	private ZFormPanel gesCode;
	private ZFormPanel manNumero;
	private ZFormPanel manTtc;
	private ZFormPanel adrNom;
	private ZFormPanel noFacture;
	private ZFormPanel pcoNum;
	private ZFormPanel noPaiement;

	private ZFormPanel noBordereau;

	/**
	 * @param editingContext
	 */
	public DepenseSuiviSrchFilterPanel(IDepenseSuiviSrchFilterPanelListener listener) {
		super();
		myListener = listener;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		setLayout(new BorderLayout());
		setBorder(ZKarukeraPanel.createMargin());
		add(buildFilters(), BorderLayout.CENTER);
		add(buildRightPanel(), BorderLayout.EAST);
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.getActionSrch());

		ArrayList comps = ZKarukeraPanel.getButtonListFromActionList(list, 50, 50);
		//  JComponent comp = (JComponent) comps.get(0);

		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private final JPanel buildFilters() {
		gesCode = ZFormPanel.buildLabelField("Code gestion", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOGestion.GES_CODE_KEY));
		noFacture = ZFormPanel.buildLabelField("N° facture", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "depNumero"));
		noBordereau = ZFormPanel.buildLabelField("N° bordereau", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "borNum"));
		noPaiement = ZFormPanel.buildLabelField("N° paiement", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "paiNumero"));
		manNumero = ZFormPanel.buildLabelField("N° mandat", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "manNumero"));
		manTtc = ZFormPanel.buildFourchetteNumberFields("<= Montant <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "manTtcMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(), "manTtcMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER);
		((ZTextField) manTtc.getMyFields().get(0)).getMyTexfield().setColumns(10);
		((ZTextField) manTtc.getMyFields().get(1)).getMyTexfield().setColumns(10);
		pcoNum = ZFormPanel.buildLabelField("Imputation", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOPlanComptable.PCO_NUM_KEY));
		adrNom = ZFormPanel.buildLabelField("Fournisseur", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "adrNom"));
		((ZTextField) adrNom.getMyFields().get(0)).getMyTexfield().setColumns(20);

		gesCode.setDefaultAction(myListener.getActionSrch());
		manNumero.setDefaultAction(myListener.getActionSrch());
		manTtc.setDefaultAction(myListener.getActionSrch());
		adrNom.setDefaultAction(myListener.getActionSrch());
		noFacture.setDefaultAction(myListener.getActionSrch());
		pcoNum.setDefaultAction(myListener.getActionSrch());
		noPaiement.setDefaultAction(myListener.getActionSrch());

		setSimpleLineBorder(gesCode);
		setSimpleLineBorder(manNumero);
		setSimpleLineBorder(manTtc);
		setSimpleLineBorder(adrNom);
		setSimpleLineBorder(noFacture);
		setSimpleLineBorder(pcoNum);
		setSimpleLineBorder(noPaiement);

		ArrayList list = new ArrayList();
		list.add(gesCode);
		list.add(noBordereau);
		list.add(manNumero);
		list.add(noFacture);
		list.add(pcoNum);

		ArrayList list2 = new ArrayList();
		list2.add(adrNom);
		list2.add(noPaiement);
		list2.add(manTtc);

		Box b = Box.createVerticalBox();
		b.add(buildLine(list));
		b.add(buildLine(list2));

		JPanel p = new JPanel(new BorderLayout());
		p.add(b, BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		gesCode.updateData();
		manNumero.updateData();
		manTtc.updateData();
	}

	public interface IDepenseSuiviSrchFilterPanelListener {
		public Action getActionSrch();

		/**
		 * @return un dictionnaire dans lequel sont stockés les filtres.
		 */
		public HashMap getFilters();
	}

}
