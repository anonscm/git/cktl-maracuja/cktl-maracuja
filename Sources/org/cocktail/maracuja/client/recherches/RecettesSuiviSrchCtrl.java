/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.emargements.ctrl.EmargementSrchCtrl;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOEmargementDetail;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTitreDetailEcriture;
import org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchFilterPanel;
import org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchFilterPanel.IRecetteSuiviSrchFilterPanelListener;
import org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class RecettesSuiviSrchCtrl extends CommonCtrl {

    private final ActionSrch actionSrch = new ActionSrch();

    private RecetteSuiviSrchPanel myPanel;
    private RecetteSuiviSrchPanelModel recetteSuiviSrchPanelModel;
    private RecetteSrchFilterPanelListener recetteSrchFilterPanelListener;
    private RecetteListPanelListener recetteListPanelListener;
    private TitreListPanelListener titreListPanelListener;
    private TitreDetailecritureListPanelListener titreDetailecritureListPanelListener;
    private EcritureDetailEmargementListPanelListener ecritureDetailEmargementListPanelListener;

    private final HashMap filters=new HashMap();





    /**
     * @param editingContext
     */
    public RecettesSuiviSrchCtrl(EOEditingContext editingContext) {
        super(editingContext);
        recetteSrchFilterPanelListener = new RecetteSrchFilterPanelListener();
        recetteListPanelListener = new RecetteListPanelListener();
        titreListPanelListener = new TitreListPanelListener();
        titreDetailecritureListPanelListener = new TitreDetailecritureListPanelListener();
        ecritureDetailEmargementListPanelListener = new EcritureDetailEmargementListPanelListener();
        recetteSuiviSrchPanelModel = new RecetteSuiviSrchPanelModel();
        myPanel = new RecetteSuiviSrchPanel(recetteSuiviSrchPanelModel);
    }


    private final void onSrch() {
        try {
            setWaitCursor(true);
            myPanel.updateData();
        } catch (Exception e) {
            setWaitCursor(false);
            showErrorDialog(e);
        }
        finally {
            setWaitCursor(false);
        }
    }


    private final NSArray getTitres() throws Exception {
    	EOQualifier qual = new EOAndQualifier(buildFilterQualifiers(filters));
    	System.out.println(qual);
        NSArray res = ZFinder.fetchArray(getEditingContext(), EOTitre.ENTITY_NAME, qual, null, true);
//        
//        
//        if (filters.get("recNum")!=null) {
//            EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat("recettes.recNum=%@", new NSArray( filters.get("recNum")));
//            res = EOQualifier.filteredArrayWithQualifier(res, qual2);
//        }
        return res;
        
        
    }

    private final NSArray getRecettes() throws Exception {
        if (myPanel.getTitreListPanel().selectedObject() == null ) {
            return null;
        }
        return ((EOTitre)myPanel.getTitreListPanel().selectedObject()).recettes();
    }

    private final NSArray getTitreDetailecritures() throws Exception {
        EOTitre man = (EOTitre)myPanel.getTitreListPanel().selectedObject();
        if (man == null ) {
            return null;
        }
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("ecritureDetail.ecriture.ecrEtat=%@",new NSArray(EOEcriture.ecritureValide));
        NSArray res1 = EOQualifier.filteredArrayWithQualifier(man.titreDetailEcritures(),qual);

        EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("ecritureDetail.ecriture.ecrNumero", EOSortOrdering.CompareAscending);
        EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("ecritureDetail.ecdSens", EOSortOrdering.CompareDescending);
        NSArray res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res1, new NSArray(new Object[]{sort,sort2}));
        return res;
    }

    private final NSArray getDetailEcritureEmargeesForEcriture() throws Exception {
        ecritureDetailEmargementListPanelListener.getEmargements().removeAllObjects() ;
        EOTitreDetailEcriture tde = (EOTitreDetailEcriture) myPanel.getTitreDetailEcritureListPanel().selectedObject();
        if (tde == null) {
            return null;
        }
        EOEcritureDetail ecd = tde.ecritureDetail();

        EOQualifier qualDest = EOQualifier.qualifierWithQualifierFormat("source=%@ and exercice=%@ and emargement.emaEtat=%@", new NSArray(new Object[]{ecd, getExercice(), EOEmargement.emaEtatValide}));
        NSArray destinations = EOsFinder.fetchArray(getEditingContext(), EOEmargementDetail.ENTITY_NAME, qualDest, null, false);

        EOQualifier qualSource = EOQualifier.qualifierWithQualifierFormat("destination=%@ and exercice=%@ and emargement.emaEtat=%@", new NSArray(new Object[]{ecd,  getExercice(), EOEmargement.emaEtatValide}));
        NSArray sources = EOsFinder.fetchArray(getEditingContext(), EOEmargementDetail.ENTITY_NAME, qualSource, null, false);

        NSMutableArray dest2 = new NSMutableArray();

        for (int i = 0; i < destinations.count(); i++) {
            if (sources.indexOfObject(destinations.objectAtIndex(i)) == NSArray.NotFound ) {
                dest2.addObject(destinations.objectAtIndex(i));
            }
        }

        for (int i = 0; i < sources.count(); i++) {
            EOEmargementDetail element = (EOEmargementDetail) sources.objectAtIndex(i);
            if (!ecritureDetailEmargementListPanelListener.getEmargements().containsObject(element.emargement())) {
                ecritureDetailEmargementListPanelListener.getEmargements().addObject(element.emargement());
            }
        }
        for (int i = 0; i < dest2.count(); i++) {
            EOEmargementDetail element = (EOEmargementDetail) dest2.objectAtIndex(i);
            if (!ecritureDetailEmargementListPanelListener.getEmargements().containsObject(element.emargement())) {
                ecritureDetailEmargementListPanelListener.getEmargements().addObject(element.emargement());
            }
        }



        NSMutableArray res = new NSMutableArray();

        res.addObjectsFromArray((NSArray) sources.valueForKey("source"));

        NSArray dest3 = (NSArray) dest2.valueForKey("destination");
        for (int i = 0; i < dest3.count(); i++) {
            if (res.indexOfObject(dest3.objectAtIndex(i)) == NSArray.NotFound ) {
                res.addObject(dest3.objectAtIndex(i));
            }
        }
        return res;
    }


    private final void openSelectedEcriture() {
        try {
            NSMutableArray ecrs = new NSMutableArray();

            NSArray tdes = myPanel.getTitreDetailEcritureListPanel().getMyDisplayGroup().displayedObjects();
            for (int i = 0; i < tdes.count(); i++) {
                EOTitreDetailEcriture element = (EOTitreDetailEcriture) tdes.objectAtIndex(i);
                ecrs.addObject(element.ecritureDetail().ecriture());
            }

            NSArray ems = myPanel.getEcritureDetailEmargementListPanel().getMyDisplayGroup().displayedObjects();
            for (int i = 0; i < ems.count(); i++) {
                EOEcritureDetail element = (EOEcritureDetail) ems.objectAtIndex(i);
                ecrs.addObject(element.ecriture());
            }

            if (ecrs.count() == 0) {
                throw new DefaultClientException("Aucun détail écriture sélectionné.");
            }
            try{
                if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {
                    EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
                    win.openDialog(getMyDialog(), ecrs);
                }
                else {
                    throw new  UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
                }
            }
            catch (Exception e) {
                showErrorDialog(e);
            }
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }

    private final void openEmargements() {
        try {
            if (ecritureDetailEmargementListPanelListener.getEmargements().count()>0) {
				EmargementSrchCtrl win = new EmargementSrchCtrl(myApp.editingContext());
				try{
				    win.openDialog(myApp.getMainWindow(), ecritureDetailEmargementListPanelListener.getEmargements() );
				    myPanel.getTitreDetailEcritureListPanel().updateData();
				    myPanel.getEcritureDetailEmargementListPanel().updateData();
				}
				catch (Exception e1) {
				    myApp.showErrorDialog(e1);
				}
            }
            else {
                throw new DefaultClientException("Aucun émargement récupéré.");
            }
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }


    protected final NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

        NSMutableArray quals = new NSMutableArray();
        quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[]{getExercice()})));


        //Construire les qualifiers à partir des saisies utilisateur
        if (dicoFiltre.get("titNumero")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("titNumero=%@", new NSArray( new Integer((String)dicoFiltre.get("titNumero")))));
        }

        if (dicoFiltre.get(EOGestion.GES_CODE_KEY)!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.gesCode=%@", new NSArray( dicoFiltre.get(EOGestion.GES_CODE_KEY))));
        }

        if (dicoFiltre.get("borNum")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borNum=%@", new NSArray( new Integer((String)dicoFiltre.get("borNum")))));
        }

        if (dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY)!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("titreDetailEcritures.ecritureDetail.pcoNum=%@", new NSArray( dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))));
        }

        if (dicoFiltre.get("titLibelle")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("titLibelle caseInsensitiveLike %@", new NSArray( "*"+dicoFiltre.get("titLibelle") +"*")));
        }

        if (dicoFiltre.get("recNum")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("recettes.recNum=%@", new NSArray( new Integer((String)dicoFiltre.get("recNum")))));
        }

        ///Montant
        NSMutableArray qualsMontant = new NSMutableArray();
        if (dicoFiltre.get("titTtcMin")!=null) {
            qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("titTtc>=%@", new NSArray( (Number) dicoFiltre.get("titTtcMin") )));
        }
        if (dicoFiltre.get("titTtcMax")!=null) {
            qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("titTtc<=%@", new NSArray( (Number) dicoFiltre.get("titTtcMax") )));
        }
        if (qualsMontant.count()>0) {
            quals.addObject( new EOAndQualifier(qualsMontant));
        }

        ///reste Emarger
        NSMutableArray qualsResteEmarger = new NSMutableArray();
        if (dicoFiltre.get("ecdResteEmargerMin")!=null) {
            qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat("titreDetailEcritures.ecritureDetail.ecdResteEmarger>=%@", new NSArray( (Number) dicoFiltre.get("ecdResteEmargerMin") )));
        }
        if (dicoFiltre.get("ecdResteEmargerMax")!=null) {
            qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat("titreDetailEcritures.ecritureDetail.ecdResteEmarger<=%@", new NSArray( (Number) dicoFiltre.get("ecdResteEmargerMax") )));
        }
        if (qualsResteEmarger.count()>0) {
            qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat("titreDetailEcritures.ecritureDetail.pcoNum like '4*' or titreDetailEcritures.ecritureDetail.pcoNum like '5*'", null));
            qualsResteEmarger.addObject(new EONotQualifier(EOQualifier.qualifierWithQualifierFormat("titreDetailEcritures.ecritureDetail.pcoNum like '445*')", null)));
            quals.addObject( new EOAndQualifier(qualsResteEmarger));
        }

        if (dicoFiltre.get("adrNom")!=null) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat("recettes.recDebiteur caseInsensitiveLike %@", new NSArray( new Object[]{"*"+dicoFiltre.get("adrNom") +"*"})));
        }
        
        
        return quals;
    }




    private final class RecetteListPanelListener implements ZKarukeraTablePanel. IZKarukeraTablePanelListener {

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
         */
        public void selectionChanged() {

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
         */
        public NSArray getData() throws Exception {
            return getRecettes();
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
         */
        public void onDbClick() {

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
         */
        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }



    }

    private final class TitreListPanelListener implements ZKarukeraTablePanel. IZKarukeraTablePanelListener  {

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
         */
        public void selectionChanged() {
            try {
                myPanel.getRecetteListPanel().updateData();
                myPanel.getTitreDetailEcritureListPanel().updateData();
                myPanel.getEcritureDetailEmargementListPanel().updateData();

            } catch (Exception e) {
                showErrorDialog(e);
            }
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
         */
        public NSArray getData() throws Exception {
            return getTitres();
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
         */
        public void onDbClick() {

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
         */
        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }





    }
    private final class RecetteSrchFilterPanelListener implements RecetteSuiviSrchFilterPanel.IRecetteSuiviSrchFilterPanelListener {

        /**
         * @see org.cocktail.maracuja.client.recettes.ui.RecetteSuiviSrchFilterPanel.IRecetteSrchFilterPanel#getActionSrch()
         */
        public Action getActionSrch() {
            return actionSrch;
        }

        /**
         * @see org.cocktail.maracuja.client.recettes.ui.RecetteSuiviSrchFilterPanel.IRecetteSrchFilterPanel#getFilters()
         */
        public HashMap getFilters() {
            return filters;
        }

    }



    private final class TitreDetailecritureListPanelListener implements   IZKarukeraTablePanelListener {

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
         */
        public void selectionChanged() {
            try {
                myPanel.getEcritureDetailEmargementListPanel().updateData();

            } catch (Exception e) {
                showErrorDialog(e);
            }

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
         */
        public NSArray getData() throws Exception {
            return getTitreDetailecritures();
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
         */
        public void onDbClick() {

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
         */
        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }

    }



    private final class EcritureDetailEmargementListPanelListener implements   IZKarukeraTablePanelListener {

        private final NSMutableArray emargements = new NSMutableArray();

        /**
         *
         */
        public EcritureDetailEmargementListPanelListener() {


        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
         */
        public void selectionChanged() {

        }

        /**
         *
         */
        public NSMutableArray getEmargements() {
            return emargements;

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
         */
        public NSArray getData() throws Exception {
            return getDetailEcritureEmargeesForEcriture();
        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
         */
        public void onDbClick() {

        }

        /**
         * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
         */
        public IZEOTableCellRenderer getTableRenderer() {
            return null;
        }

    }




    private class RecetteSuiviSrchPanelModel implements RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel {
        private final ArrayList actionsForTitreDetailEcritures = new ArrayList();
        private final ArrayList actionsForEcritureDetailEmargements = new ArrayList();

        private final ActionOpenEcriture actionOpenEcriture = new ActionOpenEcriture();
        private final ActionOpenEmargements actionPrintEmargement = new ActionOpenEmargements();

        /**
         *
         */
        public RecetteSuiviSrchPanelModel() {
            actionsForEcritureDetailEmargements.add(actionOpenEcriture);
            actionsForEcritureDetailEmargements.add(actionPrintEmargement);
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#recettesListener()
         */
        public IZKarukeraTablePanelListener recettesListener() {
            return recetteListPanelListener;
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#titresListener()
         */
        public IZKarukeraTablePanelListener titresListener() {
            return titreListPanelListener;
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#recetteSuiviSrchFilterPanelListener()
         */
        public IRecetteSuiviSrchFilterPanelListener recetteSuiviSrchFilterPanelListener() {
            return recetteSrchFilterPanelListener;
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#titresDetailEcritureListener()
         */
        public IZKarukeraTablePanelListener titresDetailEcritureListener() {
            return titreDetailecritureListPanelListener;
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#onTabSelected(org.cocktail.maracuja.client.common.ui.ZKarukeraPanel)
         */
        public void onTabSelected(ZKarukeraPanel panel) {

        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#ecritureDetailEmargementListPanelListener()
         */
        public IZKarukeraTablePanelListener ecritureDetailEmargementListPanelListener() {
            return ecritureDetailEmargementListPanelListener;
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#actionsForTitreDetailEcritures()
         */
        public ArrayList actionsForTitreDetailEcritures() {
            return actionsForTitreDetailEcritures;
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel.IRecetteSuiviSrchPanelModel#actionsForEcritureDetailEmargements()
         */
        public ArrayList actionsForEcritureDetailEmargements() {
            return actionsForEcritureDetailEmargements;
        }




    }

    private final class ActionSrch extends AbstractAction {
        public ActionSrch() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            onSrch();
        }
    }

    private final class ActionOpenEcriture extends AbstractAction {
        public ActionOpenEcriture() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Voir les écritures complètes");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            openSelectedEcriture();
        }
    }

    private final class ActionOpenEmargements extends AbstractAction {
        public ActionOpenEmargements() {
            super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EMARGMENT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Voir les émargements complets");
        }
        /**
         * Appelle updateData();
         */
        public void actionPerformed(ActionEvent e) {
            openEmargements();



        }
    }





    public RecetteSuiviSrchPanel getMyPanel() {
        return myPanel;
    }
    
    public Dimension defaultDimension() {
        return null;
    }


    public ZAbstractPanel mainPanel() {
        return myPanel;
    }

    public String title() {
        return null;
    }      
}
