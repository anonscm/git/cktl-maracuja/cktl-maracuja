/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.ecritures.EcritureSrchCtrl;
import org.cocktail.maracuja.client.emargements.ctrl.EmargementSrchCtrl;
import org.cocktail.maracuja.client.factory.FactoryImSuspension;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.finders.ZFinder;
import org.cocktail.maracuja.client.im.ctrl.ImSuspensionSaisieCtrl;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOEmargementDetail;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOImSuspension;
import org.cocktail.maracuja.client.metier.EOJdDepensePapier;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatDetailEcriture;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchFilterPanel;
import org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchFilterPanel.IDepenseSuiviSrchFilterPanelListener;
import org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel;
import org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IMyImSuspensionListPanelListener;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DepensesSuiviSrchCtrl extends CommonCtrl {

	private final ActionSrch actionSrch = new ActionSrch();

	private final DepenseSuiviSrchPanel myPanel;
	private final DepenseSuiviSrchPanelModel depenseSuiviSrchPanelModel;
	private final DepenseSrchFilterPanelListener depenseSrchFilterPanelListener;
	private final DepenseListPanelListener depenseListPanelListener;
	private final MandatListPanelListener mandatListPanelListener;
	private final MandatDetailecritureListPanelListener mandatDetailecritureListPanelListener;
	private final EcritureDetailEmargementListPanelListener ecritureDetailEmargementListPanelListener;
	private final ImSuspensionListPanelListener imSuspensionListPanelListener;

	private final HashMap filters = new HashMap();

	/**
	 * @param editingContext
	 */
	public DepensesSuiviSrchCtrl(EOEditingContext editingContext) {
		super(editingContext);
		depenseSrchFilterPanelListener = new DepenseSrchFilterPanelListener();
		depenseListPanelListener = new DepenseListPanelListener();
		mandatListPanelListener = new MandatListPanelListener();
		mandatDetailecritureListPanelListener = new MandatDetailecritureListPanelListener();
		ecritureDetailEmargementListPanelListener = new EcritureDetailEmargementListPanelListener();
		depenseSuiviSrchPanelModel = new DepenseSuiviSrchPanelModel();
		imSuspensionListPanelListener = new ImSuspensionListPanelListener();

		myPanel = new DepenseSuiviSrchPanel(depenseSuiviSrchPanelModel);
	}

	private final void onSrch() {
		try {
			setWaitCursor(true);
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void openSelectedEcriture() {
		try {
			NSMutableArray ecrs = new NSMutableArray();
			EOExercice mauvaisExercice = null;
			NSArray tdes = myPanel.getMandatDetailEcritureListPanel().getMyDisplayGroup().displayedObjects();
			for (int i = 0; i < tdes.count(); i++) {
				EOMandatDetailEcriture element = (EOMandatDetailEcriture) tdes.objectAtIndex(i);
				if (element.ecritureDetail().ecriture().exercice().equals(getExercice())) {
					ecrs.addObject(element.ecritureDetail().ecriture());
				}
				else {
					mauvaisExercice = element.ecritureDetail().ecriture().exercice();
				}
			}

			NSArray ems = myPanel.getEcritureDetailEmargementListPanel().getMyDisplayGroup().displayedObjects();
			for (int i = 0; i < ems.count(); i++) {
				EOEcritureDetail element = (EOEcritureDetail) ems.objectAtIndex(i);
				if (element.ecriture().exercice().equals(getExercice())) {
					ecrs.addObject(element.ecriture());
				}
				else {
					mauvaisExercice = element.ecriture().exercice();
				}
			}

			if (mauvaisExercice != null) {
				showWarningDialog("Les écritures de l'exercice " + mauvaisExercice.exeExercice() + " peuvent être affichées sous l'exercice " + mauvaisExercice.exeExercice());
			}

			if (ecrs.count() == 0) {
				throw new DefaultClientException("Aucun détail écriture sélectionné.");
			}
			try {
				if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COCE)) {
					EcritureSrchCtrl win = new EcritureSrchCtrl(myApp.editingContext());
					win.openDialog(getMyDialog(), ecrs);
				}
				else {
					throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void openEmargements() {
		try {
			if (ecritureDetailEmargementListPanelListener.getEmargements().count() > 0) {
				EmargementSrchCtrl win = new EmargementSrchCtrl(myApp.editingContext());
				try {
					win.openDialog(myApp.getMainWindow(), ecritureDetailEmargementListPanelListener.getEmargements());
					myPanel.getMandatDetailEcritureListPanel().updateData();
					myPanel.getEcritureDetailEmargementListPanel().updateData();
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}
			else {
				throw new DefaultClientException("Aucun émargement récupéré.");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void checkDroitsImSuspensions() throws Exception {
		if (!myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ZActionCtrl.IDU_IMSUSP)) {
			throw new UserActionException("Vous n'avez pas les droits suffisants pour accéder à cette fonctionnalité.");
		}
	}

	private void imSuspensionAdd() {
		try {
			checkDroitsImSuspensions();
			EOMandat mandat = selectedMandat();
			if (mandat == null) {
				throw new DefaultClientException("Aucun mandat n'est sélectionné.");
			}

			FactoryImSuspension factoryImSuspension = new FactoryImSuspension(myApp.wantShowTrace());
			EOImSuspension imSuspension = factoryImSuspension.newObject(getEditingContext(), null, getUtilisateur());

			final ImSuspensionSaisieCtrl ctrl = new ImSuspensionSaisieCtrl(getEditingContext());
			NSArray dpps = EOJdDepensePapier.fetchAllForMandat(getEditingContext(), mandat, null, false);

			if (ctrl.openDialog(getMyDialog(), imSuspension, dpps) == MROK) {
				myPanel.getImSuspensionListPanel().updateData();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void imSuspensionModify() {
		try {
			checkDroitsImSuspensions();
			EOMandat mandat = selectedMandat();
			if (mandat == null) {
				throw new DefaultClientException("Aucun mandat n'est sélectionné.");
			}
			EOImSuspension imSuspension = myPanel.getImSuspensionListPanel().selectedImSuspension();
			if (imSuspension == null) {
				throw new DefaultClientException("Aucune suspension n'est sélectionnée.");
			}

			imSuspension.checkIfModifiable();

			final ImSuspensionSaisieCtrl ctrl = new ImSuspensionSaisieCtrl(getEditingContext());
			NSArray dpps = EOJdDepensePapier.fetchAllForMandat(getEditingContext(), mandat, null, false);

			if (ctrl.openDialog(getMyDialog(), imSuspension, dpps) == MROK) {
				myPanel.getImSuspensionListPanel().updateData();
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void imSuspensionDelete() {
		try {
			checkDroitsImSuspensions();
			EOImSuspension imSuspension = myPanel.getImSuspensionListPanel().selectedImSuspension();
			if (imSuspension != null) {
				imSuspension.checkIfSupprimable();

				FactoryImSuspension factoryImSuspension = new FactoryImSuspension(myApp.wantShowTrace());
				if (imSuspension.isSupprimable()) {
					factoryImSuspension.supprimerObject(imSuspension);
				}
				if (getEditingContext().hasChanges()) {
					getEditingContext().saveChanges();
				}
				myPanel.getImSuspensionListPanel().updateData();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			getEditingContext().revert();
		}

	}

	private final NSArray getMandats() throws Exception {
		NSArray res = ZFinder.fetchArray(getEditingContext(), EOMandat.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(filters)), null, true, true, false, null);
		getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), res));
		return res;
	}

	private final NSArray getDepenses() throws Exception {
		if (myPanel.getMandatListPanel().selectedObject() == null) {
			return null;
		}
		return ((EOMandat) myPanel.getMandatListPanel().selectedObject()).depenses();
	}

	public EOMandat selectedMandat() {
		return (EOMandat) myPanel.getMandatListPanel().selectedObject();
	}

	private final NSArray getMandatDetailecritures() throws Exception {
		EOMandat man = selectedMandat();
		if (man == null) {
			return null;
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("ecritureDetail.ecriture.ecrEtat=%@", new NSArray(EOEcriture.ecritureValide));
		NSArray res1 = EOQualifier.filteredArrayWithQualifier(man.mandatDetailEcritures(), qual);

		EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("ecritureDetail.ecriture.ecrNumero", EOSortOrdering.CompareAscending);
		EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("ecritureDetail.ecdSens", EOSortOrdering.CompareDescending);
		NSArray res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res1, new NSArray(new Object[] {
				sort, sort2
		}));
		return res;
	}

	private final NSArray getDetailEcritureEmargeesForEcriture() throws Exception {
		ecritureDetailEmargementListPanelListener.getEmargements().removeAllObjects();
		EOMandatDetailEcriture tde = (EOMandatDetailEcriture) myPanel.getMandatDetailEcritureListPanel().selectedObject();
		if (tde == null) {
			return null;
		}
		EOEcritureDetail ecd = tde.ecritureDetail();

		EOQualifier qualDest = EOQualifier.qualifierWithQualifierFormat("source=%@ and exercice=%@ and emargement.emaEtat=%@", new NSArray(new Object[] {
				ecd, getExercice(), EOEmargement.emaEtatValide
		}));
		NSArray destinations = EOsFinder.fetchArray(getEditingContext(), EOEmargementDetail.ENTITY_NAME, qualDest, null, false);

		EOQualifier qualSource = EOQualifier.qualifierWithQualifierFormat("destination=%@ and exercice=%@ and emargement.emaEtat=%@", new NSArray(new Object[] {
				ecd, getExercice(), EOEmargement.emaEtatValide
		}));
		NSArray sources = EOsFinder.fetchArray(getEditingContext(), EOEmargementDetail.ENTITY_NAME, qualSource, null, false);

		NSMutableArray dest2 = new NSMutableArray();

		for (int i = 0; i < destinations.count(); i++) {
			if (sources.indexOfObject(destinations.objectAtIndex(i)) == NSArray.NotFound) {
				dest2.addObject(destinations.objectAtIndex(i));
			}
		}

		for (int i = 0; i < sources.count(); i++) {
			EOEmargementDetail element = (EOEmargementDetail) sources.objectAtIndex(i);
			if (!ecritureDetailEmargementListPanelListener.getEmargements().containsObject(element.emargement())) {
				ecritureDetailEmargementListPanelListener.getEmargements().addObject(element.emargement());
			}
		}
		for (int i = 0; i < dest2.count(); i++) {
			EOEmargementDetail element = (EOEmargementDetail) dest2.objectAtIndex(i);
			if (!ecritureDetailEmargementListPanelListener.getEmargements().containsObject(element.emargement())) {
				ecritureDetailEmargementListPanelListener.getEmargements().addObject(element.emargement());
			}
		}

		NSMutableArray res = new NSMutableArray();

		res.addObjectsFromArray((NSArray) sources.valueForKey("source"));

		NSArray dest3 = (NSArray) dest2.valueForKey("destination");
		for (int i = 0; i < dest3.count(); i++) {
			if (res.indexOfObject(dest3.objectAtIndex(i)) == NSArray.NotFound) {
				res.addObject(dest3.objectAtIndex(i));
			}
		}
		return res;
	}

	/**
	 * @return les suspensions associées a des depensePapiers rattachées au mandat selectionne.
	 */
	private NSArray getImSuspensionsForMandat() {
		EOMandat man = selectedMandat();
		if (man != null) {
			return EOImSuspension.fetchAllForMandat(getEditingContext(), man, new NSArray(new Object[] {
					EOImSuspension.SORT_DEBUT_ASC
			}), false);
		}
		return NSArray.EmptyArray;

	}

	protected final NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {
		ZLogger.verbose(dicoFiltre);

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@ ", new NSArray(new Object[] {
				getExercice()
		})));

		//Construire les qualifiers à partir des saisies utilisateur
		if (dicoFiltre.get("manNumero") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("manNumero=%@", new NSArray(new Integer((String) dicoFiltre.get("manNumero")))));
		}

		if (dicoFiltre.get("borNum") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("bordereau.borNum=%@", new NSArray(new Integer((String) dicoFiltre.get("borNum")))));
		}

		if (dicoFiltre.get("paiNumero") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("paiement.paiNumero=%@", new NSArray(new Integer((String) dicoFiltre.get("paiNumero")))));
		}

		if (dicoFiltre.get(EOGestion.GES_CODE_KEY) != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("gestion.gesCode=%@", new NSArray(dicoFiltre.get(EOGestion.GES_CODE_KEY))));
		}

		if (dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY) != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("mandatDetailEcritures.ecritureDetail.pcoNum=%@", new NSArray(dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))));
		}

		if (dicoFiltre.get("depNumero") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("depenses.depNumero=%@", new NSArray(dicoFiltre.get("depNumero"))));
		}

		if (dicoFiltre.get("adrNom") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat("fournisseur.adrNom caseInsensitiveLike %@ or fournisseur.fouCode=%@", new NSArray(new Object[] {
					"*" + dicoFiltre.get("adrNom") + "*", dicoFiltre.get("adrNom")
			})));
		}

		///Montant
		NSMutableArray qualsMontant = new NSMutableArray();
		if (dicoFiltre.get("manTtcMin") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("manTtc>=%@", new NSArray(dicoFiltre.get("manTtcMin"))));
		}
		if (dicoFiltre.get("manTtcMax") != null) {
			qualsMontant.addObject(EOQualifier.qualifierWithQualifierFormat("manTtc<=%@", new NSArray(dicoFiltre.get("manTtcMax"))));
		}
		if (qualsMontant.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMontant));
		}

		return quals;
	}

	private class ImSuspensionListPanelListener implements IMyImSuspensionListPanelListener {
		private final ActionAddImSuspension actionAddImSuspension = new ActionAddImSuspension();
		private final ActionDeleteImSuspension actionDeleteImSuspension = new ActionDeleteImSuspension();
		private final ActionModifyImSuspension actionModifyImSuspension = new ActionModifyImSuspension();

		public Action getActionAddImSuspension() {
			return actionAddImSuspension;
		}

		public Action getActionDeleteImSuspension() {
			return actionDeleteImSuspension;
		}

		public Action getActionModifyImSuspension() {
			return actionModifyImSuspension;
		}

		public NSArray getData() throws Exception {
			return getImSuspensionsForMandat();
		}

		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

		public void onDbClick() {
			if (actionModifyImSuspension.isEnabled()) {
				imSuspensionModify();
			}

		}

		public void selectionChanged() {
			refreshActions();
		}

		public void refreshActions() {
			//FIXME voir les droits de modif /suppression en fonction du type de suspension notamment
			EOImSuspension selectedImSuspension = myPanel.getImSuspensionListPanel().selectedImSuspension();
			actionDeleteImSuspension.setEnabled(selectedImSuspension != null);
			actionModifyImSuspension.setEnabled(selectedImSuspension != null);
		}

	}

	private final class DepenseListPanelListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getDepenses();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class MandatListPanelListener implements ZKarukeraTablePanel.IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			try {
				setWaitCursor(true);
				myPanel.getDepenseListPanel().updateData();
				myPanel.getMandatDetailEcritureListPanel().updateData();
				myPanel.getImSuspensionListPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			} finally {
				setWaitCursor(false);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getMandats();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class DepenseSrchFilterPanelListener implements DepenseSuiviSrchFilterPanel.IDepenseSuiviSrchFilterPanelListener {

		public Action getActionSrch() {
			return actionSrch;
		}

		public HashMap getFilters() {
			return filters;
		}

	}

	private final class MandatDetailecritureListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getMandatDetailecritures();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private class DepenseSuiviSrchPanelModel implements DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel {
		//        private final ArrayList actionsForMandatDetailEcritures = new ArrayList();
		private final ArrayList actionsForEcritureDetailEmargements = new ArrayList();

		private final ActionOpenEcriture actionOpenEcriture = new ActionOpenEcriture();
		private final ActionOpenEmargements actionPrintEmargement = new ActionOpenEmargements();

		/**
         *
         */
		public DepenseSuiviSrchPanelModel() {
			actionsForEcritureDetailEmargements.add(actionOpenEcriture);
			actionsForEcritureDetailEmargements.add(actionPrintEmargement);
		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#depensesListener()
		 */
		public IZKarukeraTablePanelListener depensesListener() {
			return depenseListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#mandatsListener()
		 */
		public IZKarukeraTablePanelListener mandatsListener() {
			return mandatListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#depenseSuiviSrchFilterPanelListener()
		 */
		public IDepenseSuiviSrchFilterPanelListener depenseSuiviSrchFilterPanelListener() {
			return depenseSrchFilterPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#mandatsDetailEcritureListener()
		 */
		public IZKarukeraTablePanelListener mandatsDetailEcritureListener() {
			return mandatDetailecritureListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#onTabSelected(org.cocktail.maracuja.client.common.ui.ZKarukeraPanel)
		 */
		public void onTabSelected(ZKarukeraPanel panel) {

		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#ecritureDetailEmargementListPanelListener()
		 */
		public IZKarukeraTablePanelListener ecritureDetailEmargementListPanelListener() {
			return ecritureDetailEmargementListPanelListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel.IDepenseSuiviSrchPanelModel#actionsForEcritureDetailEmargements()
		 */
		public ArrayList actionsForEcritureDetailEmargements() {
			return actionsForEcritureDetailEmargements;
		}

		public IMyImSuspensionListPanelListener imSuspensionListPanelListener() {
			return imSuspensionListPanelListener;
		}

	}

	private final class EcritureDetailEmargementListPanelListener implements IZKarukeraTablePanelListener {

		private final NSMutableArray emargements = new NSMutableArray();

		/**
         *
         */
		public EcritureDetailEmargementListPanelListener() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {

		}

		/**
         *
         */
		public NSMutableArray getEmargements() {
			return emargements;

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return getDetailEcritureEmargeesForEcriture();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionOpenEcriture extends AbstractAction {
		public ActionOpenEcriture() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ECRITURE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir les écritures complètes");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			openSelectedEcriture();
		}
	}

	private final class ActionOpenEmargements extends AbstractAction {
		public ActionOpenEmargements() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EMARGMENT_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Voir les émargements complets");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			openEmargements();

		}
	}

	private class ActionAddImSuspension extends AbstractAction {
		public ActionAddImSuspension() {
			super("+");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter une suspension de DGP");

		}

		public void actionPerformed(ActionEvent e) {
			imSuspensionAdd();
		}
	}

	private class ActionDeleteImSuspension extends AbstractAction {
		public ActionDeleteImSuspension() {
			super("-");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer la suspension de DGP sélectionnée");

		}

		public void actionPerformed(ActionEvent e) {
			imSuspensionDelete();
		}
	}

	private class ActionModifyImSuspension extends AbstractAction {
		public ActionModifyImSuspension() {
			super("");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier la suspension de DGP sélectionnée");

		}

		public void actionPerformed(ActionEvent e) {
			imSuspensionModify();
		}
	}

	public DepenseSuiviSrchPanel getMyPanel() {
		return myPanel;
	}

	public Dimension defaultDimension() {
		return null;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return null;
	}

}
