/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.recherches;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.recherches.ui.DepRecPanel;
import org.cocktail.maracuja.client.recherches.ui.DepenseSuiviSrchPanel;
import org.cocktail.maracuja.client.recherches.ui.RecetteSuiviSrchPanel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DepRecCtrl extends CommonCtrl {
    private static final String TITLE="Recherches Mandats/Titres";
    private static final Dimension WINDOW_DIMENSION=new Dimension(970,700);


    private DepRecPanel depRecPanel;

    private DepensesSuiviSrchCtrl depensesSuiviSrchCtrl;
    private RecettesSuiviSrchCtrl recettesSuiviSrchCtrl;
    private final ActionClose actionClose = new ActionClose();


    /**
     * @param editingContext
     */
    public DepRecCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        depensesSuiviSrchCtrl = new DepensesSuiviSrchCtrl(getEditingContext());
        recettesSuiviSrchCtrl = new RecettesSuiviSrchCtrl(getEditingContext());

        depRecPanel = new DepRecPanel(new ScolBordereauScolPanelListener());
    }




    private final ZKarukeraDialog createModalDialog(Window dial ) {
        ZKarukeraDialog win;
        if (dial instanceof Dialog) {
            win = new ZKarukeraDialog((Dialog)dial, TITLE,true);
        }
        else {
            win = new ZKarukeraDialog((Frame)dial, TITLE,true);
        }
        win.addWindowListener( new ScolWindowListener());
        depRecPanel.setMyDialog(win);
        depRecPanel.setPreferredSize(WINDOW_DIMENSION);
        depRecPanel.initGUI();
        win.setContentPane(depRecPanel);
        win.pack();
        return win;
    }




    /**
     * Ouvre un dialog de recherche.
     */
    public final void openDialog(Window dial) {
        ZKarukeraDialog win = createModalDialog(dial);
        this.setMyDialog(win);
        depensesSuiviSrchCtrl.setMyDialog(getMyDialog());
        recettesSuiviSrchCtrl.setMyDialog(getMyDialog());
        try {
            win.open();
        } catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
    }


    private final void fermer() {
        getMyDialog().onCloseClick();
    }




    private final class ScolBordereauScolPanelListener implements DepRecPanel.IDepRecPanelListener {

        /**
         * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauPanel.IScolBordereauSrchPanelListener#actionClose()
         */
        public Action actionClose() {
            return actionClose;
        }

        /**
         * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauPanel.IScolBordereauSrchPanelListener#onTabSelected(org.cocktail.maracuja.client.common.ui.ZKarukeraPanel)
         */
        public void onTabSelected(ZKarukeraPanel panel) {

        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.DepRecPanel.IDepRecPanelListener#getDepenseSuiviSrchPanel()
         */
        public DepenseSuiviSrchPanel getDepenseSuiviSrchPanel() {
            return depensesSuiviSrchCtrl.getMyPanel();
        }

        /**
         * @see org.cocktail.maracuja.client.recherches.ui.DepRecPanel.IDepRecPanelListener#getRecetteSuiviSrchPanel()
         */
        public RecetteSuiviSrchPanel getRecetteSuiviSrchPanel() {
            return recettesSuiviSrchCtrl.getMyPanel();
        }


    }


	private final class ActionClose extends AbstractAction {

	    public ActionClose() {
            super("Fermer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }


        public void actionPerformed(ActionEvent e) {
          fermer();
        }

	}


	private final class ScolWindowListener extends WindowAdapter {
	    // This method is called after a window has been opened
        public void windowOpened(WindowEvent evt) {
            depRecPanel.getTabbedPane().getModel().setSelectedIndex(0);
        }

        // This method is called when the user clicks the close button
        public void windowClosing(WindowEvent evt) {
            return;
        }

        // This method is called after a window is closed
        public void windowClosed(WindowEvent evt) {
            return;
        }

	}

    public Dimension defaultDimension() {
        return WINDOW_DIMENSION;
    }


    public ZAbstractPanel mainPanel() {
        return depRecPanel;
    }

    public String title() {
        return TITLE;
    }  





}
