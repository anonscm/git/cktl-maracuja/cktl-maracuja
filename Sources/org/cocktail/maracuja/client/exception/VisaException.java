/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Created on 8 juil. 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.cocktail.maracuja.client.exception;

/**
 * @author RIVALLAND FREDERIC <br>
 *         UAG <br>
 *         CRI Guadeloupe
 *
 */
public class VisaException extends FactoryException {

	public static String mandatDejaAnnule = "MANDAT DEJA ANNULE";

	public static String mandatDejaVise = "MANDAT DEJA VISE";

	public static String titreDejaAnnule = "TITRE DEJA ANNULE";

	public static String titreDejaVise = "TITRE DEJA VISE";

    public static String problemeCreationEcriture         = "PROBLEME DE CREATION D ECRITURE DETAIL;";

    public static String problemeCreationDetailEcriture   = "PROBLEME DE CREATION DE MANDAT DETAIL ECRITURE";

    public static String problemeCreationBordereauRejet   = "PROBLEME DE CREATION DE BORDEREAU REJET";

    public static String problemeFormatBordereau          = "PROBLEME DE FORMAT DE BORDEREAU : ";

	public static String pasDeMandatsARejeter = "PAS DE MANDATS A REJETER";

	public static String pasDeMandatBrouillard = "PAS DE MANDAT BROUILLARD";

	public static String pasDeTitresARejeter = "PAS DE TITRES A REJETER";

	public static String pasDeTitreBrouillard = "PAS DE TITRE BROUILLARD";

    public static String prestationProblemeMontantRejets  = "LES MANDATS REJETES ET LES TITRES REJETES NE SONT PAS DU MEME MONTANT";

    public static String prestationProblemeMontant        = "LES MANDATS  ET LES TITRES  NE SONT PAS DU MEME MONTANT";

    public static String prestationProblemeTypeBordereaux = "CE N EST PAS UN BORDEREAU DE PRESTATION INTERNE";

	/**
	 * 
	 */
	public VisaException(String mess) {
		super();
		this.setMessage(mess);
	}

}
