/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.papaye.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.BordereauListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class PapayeSrchPanel extends ZKarukeraPanel {
	private final IPapayeSrchPanelListener myListener;

	//    private ScolBordereauSrchFilterPanel filterPanel;
	private final PapayeBordereauListPanel bordereauListPanel;
	//    private ScolMandatListPanel mandatListPanel;
	//    private ScolMandatBrouillardListPanel mandatBrouillardListPanel;

	private JComboBox moisList;

	/**
	 * @param editingContext
	 */
	public PapayeSrchPanel(IPapayeSrchPanelListener listener) {
		super();
		myListener = listener;

		//        filterPanel = new ScolBordereauSrchFilterPanel(new ScolBordereauSrchFilterPanelListener());
		bordereauListPanel = new PapayeBordereauListPanel(new PapayeBordereauListPanelListener());
		//        mandatListPanel = new ScolMandatListPanel(new ScolMandatListPanelListener());
		//        mandatBrouillardListPanel = new ScolMandatBrouillardListPanel(new ScolMandatBrouillardListPanelListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		//        filterPanel.setMyDialog(getMyDialog());
		//        filterPanel.initGUI();
		bordereauListPanel.initGUI();
		//        mandatListPanel.initGUI();
		//        mandatBrouillardListPanel.initGUI();

		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));

		//        JPanel tmp2 = new JPanel(new BorderLayout());
		//        tmp2.add(ZKarukeraPanel.buildVerticalSplitPane(
		//                encloseInPanelWithTitle("Mandats",null,null,null,mandatListPanel,null,null),
		//                encloseInPanelWithTitle("Brouillards",null,null,null,mandatBrouillardListPanel,null,null)) , BorderLayout.CENTER);
		//
		//
		//
		//        JPanel tmp = new JPanel(new BorderLayout());
		//
		////        tmp.add(encloseInPanelWithTitle("Filtres de recherche",null,null,null,filterPanel,null,null), BorderLayout.NORTH);
		//        tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
		//                encloseInPanelWithTitle("Bordereaux",null,null,null,bordereauListPanel,null,null),
		//                tmp2), BorderLayout.CENTER);

		this.add(encloseInPanelWithTitle("Filtres", null, ZConst.BG_COLOR_TITLE, buildFilterPanel(), null, null), BorderLayout.NORTH);
		this.add(encloseInPanelWithTitle("Bordereaux", null, ZConst.BG_COLOR_TITLE, bordereauListPanel, null, null), BorderLayout.CENTER);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(buildRightPanel(), BorderLayout.EAST);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		//          filterPanel.updateData();
		if (myListener.getFilters().get("date") != null) {
			moisList.setSelectedItem(myListener.getFilters().get("date"));
			myListener.getFilters().put("date", moisList.getSelectedItem());
		}
		bordereauListPanel.updateData();
		//            mandatListPanel.updateData();
		//            mandatBrouillardListPanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.getActionRejeter());
		list.add(myListener.getActionViser());
		list.add(myListener.getActionRetenue());
		list.add(myListener.getActionPaiement());
		list.add(null);
		list.add(myListener.getActionShowInfo());
		list.add(myListener.getActionVoirEcritures());
		//        list.add(myListener.getActionRejeter());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.getActionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private final JPanel buildFilterPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		moisList = new JComboBox(myListener.getFirstDaysOfMonth().toArray());
		moisList.setRenderer(new MoisListRenderer());
		moisList.addActionListener(myListener.getMoisListListener());
		tmp.add(new ZLabeledComponent("Mois", moisList, ZLabeledComponent.LABELONLEFT, -1), BorderLayout.WEST);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	/**
	 * @return L'objetactuelment sélectionné.
	 */
	public EOBordereau getSelectedBordereau() {
		return (EOBordereau) bordereauListPanel.selectedObject();
	}

	public NSArray getSelectedBordereaux() {
		return bordereauListPanel.getMyDisplayGroup().selectedObjects();
	}

	//    public EOMandat getSelectedMandat() {
	//        return (EOMandat) mandatListPanel.selectedObject();
	//    }
	//

	public interface IPapayeSrchPanelListener {
		public Action getActionViser();

		public Action getActionClose();

		public Action getActionRetenue();

		public Action getActionPaiement();

		public Action getActionShowInfo();

		public Action getActionVoirEcritures();

		public Action getActionRejeter();

		/**
		 * @return un dictioniare contenant les filtres
		 */
		public HashMap getFilters();

		public Action actionSrch();

		public void onBordereauSelectionChanged();

		public NSArray getBordereaux();

		/**
		 * Doit renvoyer une liste d'objets Date contenant le 1er jour de chaque
		 * mois à afficher dans la combobox
		 */
		public ArrayList getFirstDaysOfMonth();

		public ActionListener getMoisListListener();

	}

	public final class PapayeBordereauListPanel extends BordereauListPanel {
		public static final String COL_NBMANDATS = "mandats.@count";

		/**
		 * @param listener
		 */
		public PapayeBordereauListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn nbMandats = new ZEOTableModelColumn(myDisplayGroup, COL_NBMANDATS, "Nb. mandats", 100);
			nbMandats.setAlignment(SwingConstants.CENTER);

			colsMap.remove(COL_UTILISATEUR);
			colsMap.put(COL_NBMANDATS, nbMandats);

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel#initGUI()
		 */
		public void initGUI() {
			super.initGUI();
			myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		}

	}

	private final class PapayeBordereauListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			myListener.onBordereauSelectionChanged();

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getBordereaux();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			return;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class MoisListRenderer extends DefaultListCellRenderer {
		private final SimpleDateFormat format = new SimpleDateFormat("MMMMM yyyy");

		public MoisListRenderer() {
			setOpaque(true);
		}

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			setText(ZStringUtil.capitalizedString(format.format((Date) value)));
			return this;
		}
	}

	public PapayeBordereauListPanel getBordereauListPanel() {
		return bordereauListPanel;
	}

	public JComboBox getMoisList() {
		return moisList;
	}
}
