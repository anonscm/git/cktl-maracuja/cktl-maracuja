/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread;
import org.cocktail.maracuja.client.common.ctrl.ZWaitingThread.DefaultZHeartBeatListener;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.ecritures.ui.EcritureDetailListPanel;
import org.cocktail.maracuja.client.ecritures.ui.EcritureRectifPanel;
import org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel;
import org.cocktail.maracuja.client.ecritures.ui.IEcritureDetailSaisiePanel;
import org.cocktail.maracuja.client.emargements.ctrl.FactoryEmargementProxy;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.Factory;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderEmargement;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOExercice;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.ZNSMutableArray;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureSaisieCtrl extends CommonCtrl {
	private static final String TITLE = "Saisie d'une écriture";
	public static final int MODE_NEW = 0;
	public static final int MODE_RECTIF = 1;
	private static final Dimension WINDOW_DIMENSION = new Dimension(900, 700);
	private static final Dimension PCOWINDOW_SIZE = new Dimension(200, 150);

	private int modeSaisie = MODE_NEW;

	private org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel ecritureSaisiePanel;
	private org.cocktail.maracuja.client.ecritures.ui.EcritureRectifPanel ecritureRectifPanel;

	private EcritureFormPanelListener myEcritureFormPanelListener;
	private EcritureSaisieDetailListPanelListener myEcritureSaisieDetailListPanelListener;

	private EOEcriture currentEcriture;
	private EOEcriture currentEcritureARectifier;

	private HashMap currentEcritureDico = new HashMap();

	private FactoryProcessJournalEcriture myFactoryProcessJournalEcriture;
	private FactoryEcritureDetail myFactoryEcritureDetail;

	private NSArray planComptables;
	private ZEOComboBoxModel origineModel;
	private NSArray planComptablesJBe;
	private NSArray planComptablesJExercice;
	private NSArray planComptablesJFinExercice;
	private NSArray planComptablesJGrandLivreValeursInactives;
	private NSArray gestions;
	private NSArray typeOperations;
	private NSArray typeOperationsReimputation;
	private NSArray typeJournals;
	private NSArray typeJournalsReimputation;

	private final ActionDetail actionDetail = new ActionDetail();
	private final ActionValider actionValider = new ActionValider();
	private final ActionValiderRectif actionValiderRectif = new ActionValiderRectif();
	private final ActionClose actionClose = new ActionClose();
	private final ActionDeleteEcritureDetail actionDeleteEcritureDetail = new ActionDeleteEcritureDetail();
	private final ActionAddEcritureDetail actionAddEcritureDetail = new ActionAddEcritureDetail();
	private final ActionSrchOrigine actionSrchOrigine = new ActionSrchOrigine();

	private NSArray ecrituresForRecap;
	private EOEcritureDetail lastdetail = null;

	private final HashMap dispayGroupsOrigine = new HashMap();
	private EOAccordsContrat currentConventionFromOrigine;

	/**
	 * @param editingContext
	 * @throws DefaultClientException
	 */
	public EcritureSaisieCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		_init(MODE_NEW);
	}

	public EcritureSaisieCtrl(EOEditingContext editingContext, final int pmode) throws Exception {
		super(editingContext);
		_init(pmode);
	}

	private void _init(final int mode) throws Exception {
		modeSaisie = mode;
		revertChanges();
		myFactoryEcritureDetail = new FactoryEcritureDetail(myApp.wantShowTrace());

		//Si interdiction comptes 6 et 7, on limite les plancos possibles
		EOQualifier qualPcoNum = null;
		String limiteClasse = (String) myApp.getParametres().valueForKey("PCOCLASSE_SAISIE_BLOQUEE");
		if (limiteClasse != null && limiteClasse.length() != 0) {
			NSArray classesInterdites = NSArray.componentsSeparatedByString(limiteClasse, ",");
			ZLogger.debug("classesInterdites", classesInterdites);
			NSMutableArray classesFilters = new NSMutableArray();
			for (int i = 0; i < classesInterdites.count(); i++) {
				String element = (String) classesInterdites.objectAtIndex(i);
				if (element != null && element.length() > 0) {
					classesFilters.addObject(EOQualifier.qualifierWithQualifierFormat("pcoNum like '" + element + "*'", null));
				}
			}
			if (classesFilters.count() > 0) {
				qualPcoNum = new EONotQualifier(new EOOrQualifier(classesFilters));
				//                planComptables = EOQualifier.filteredArrayWithQualifier(planComptables,qual);
				//                planComptablesJBe = EOQualifier.filteredArrayWithQualifier(planComptablesJBe,qual);
				//                planComptablesJExercice = EOQualifier.filteredArrayWithQualifier(planComptablesJExercice,qual);
				//                planComptablesJFinExercice = EOQualifier.filteredArrayWithQualifier(planComptablesJFinExercice,qual);
			}
		}

		//        planComptables = EOPlanComptableFinder.getPlancoValidesWithQuals(getEditingContext(), getExercice(), null , false);
		planComptables = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), qualPcoNum, true);

		//        planComptables = EOQualifier.filteredArrayWithQualifier(planComptables, qualPcoNum);

		final EOQualifier qualJBe = EOQualifier.qualifierWithQualifierFormat("pcoJBe=%@ or pcoJBe=nil", new NSArray(new Object[] {
				EOPlanComptableExer.pcoJBeOui
		}));
		final EOQualifier qualJExercice = EOQualifier.qualifierWithQualifierFormat("pcoJExercice=%@ or pcoJExercice=nil", new NSArray(new Object[] {
				EOPlanComptableExer.pcoJExerciceOui
		}));
		final EOQualifier qualJFinExercice = EOQualifier.qualifierWithQualifierFormat("pcoJFinExercice=%@ or pcoJFinExercice=nil", new NSArray(new Object[] {
				EOPlanComptableExer.pcoJFinExerciceOui
		}));
		final EOQualifier qualJGrandLivreValeursInactives = new EOKeyValueQualifier(EOPlanComptable.PCO_NUM_KEY, EOQualifier.QualifierOperatorLike, "86*");

		planComptablesJBe = EOQualifier.filteredArrayWithQualifier(planComptables, qualJBe);

		planComptablesJExercice = EOQualifier.filteredArrayWithQualifier(planComptables, qualJExercice);
		planComptablesJFinExercice = EOQualifier.filteredArrayWithQualifier(planComptables, qualJFinExercice);
		planComptablesJGrandLivreValeursInactives = EOQualifier.filteredArrayWithQualifier(planComptables, qualJGrandLivreValeursInactives);

		origineModel = new ZEOComboBoxModel(new NSArray(), "oriLibelle", "", null);
		gestions = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_JOECSA);
		if (gestions.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour saisir une écriture comptable. Demandez à votre administrateur de vous affecter les codes de gestions pour la saisie d'écritures.");
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalExercice));
		final EOQualifier qual1 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalBalanceEntree));
		final EOQualifier qual2 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalFinExercice));
		final EOQualifier qual8 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalGrandLivreValeursInactives));

		typeJournals = EOsFinder.fetchArray(getEditingContext(), "TypeJournal", new EOOrQualifier(new NSArray(new Object[] {
				qual, qual1, qual2, qual8
		})), null, false);

		final EOQualifier qual3 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalPaiement));
		final EOQualifier qual4 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalOrdreDePaiement));
		final EOQualifier qual5 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalBordCheque));
		final EOQualifier qual6 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalVisaDU));
		final EOQualifier qual7 = EOQualifier.qualifierWithQualifierFormat("tjoLibelle = %@", new NSArray(EOTypeJournal.typeJournalVisaRembDU));

		typeJournalsReimputation = EOsFinder.fetchArray(getEditingContext(), "TypeJournal", new EOOrQualifier(new NSArray(new Object[] {
				qual, qual1, qual2, qual3, qual4, qual5, qual6, qual7
		})), null, false);

		typeOperations = EOsFinder.getTypeOperationsPubliques(getEditingContext());
		typeOperationsReimputation = EOsFinder.getAllTypeOperations(getEditingContext());

		myFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
		myEcritureFormPanelListener = new EcritureFormPanelListener();
		myEcritureSaisieDetailListPanelListener = new EcritureSaisieDetailListPanelListener();

		if (mode == MODE_NEW) {
			ecritureSaisiePanel = new EcritureSaisiePanel(new EcritureSaisiePanelListener(), myEcritureFormPanelListener, myEcritureSaisieDetailListPanelListener, new ZPanelBalanceEcritureProvider());
		}
		else {
			ecritureRectifPanel = new EcritureRectifPanel(new EcritureRectifPanelListener(), myEcritureFormPanelListener, myEcritureSaisieDetailListPanelListener, new EcritureRectifDetailListListener(), new ZPanelBalanceEcritureRectifProvider());
		}

	}

	/**
	 * Crée un nouvel objet EcritureDetail et active les éléments d'interface nécessaires à sa saisie.
	 */
	private void ecritureDetailAjouter() {
		EOEcritureDetail detail = null;
		EOAccordsContrat conv = null;

		if (getEcritureSaisiePanel().getMyEcritureSaisieDetailListPanel().isEditing()) {
			getEcritureSaisiePanel().getMyEcritureSaisieDetailListPanel().stopCellEditing();
		}

		if (MODE_NEW == modeSaisie) {
			detail = myFactoryEcritureDetail.ecritureDetailVideCreer(getEditingContext());
			myFactoryEcritureDetail.ecritureDetailVideSetExercice(detail, currentEcriture.exercice());
			detail.setEcdIndex(new Integer(ecritureDetailGetMaxIndex(currentEcriture.detailEcriture()) + 1));
			detail.setToAccordsContratRelationship(currentConventionFromOrigine);
			myFactoryEcritureDetail.attribuerUnEcritureDetail(getEditingContext(), currentEcriture, detail);

			ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().ecritureDetailAjouterInDg(detail);
		}
		else if (MODE_RECTIF == modeSaisie) {
			detail = myFactoryEcritureDetail.ecritureDetailVideCreer(getEditingContext());
			myFactoryEcritureDetail.ecritureDetailVideSetExercice(detail, currentEcriture.exercice());
			detail.setEcdIndex(new Integer(ecritureDetailGetMaxIndex(currentEcriture.detailEcriture()) + 1));
			detail.setToAccordsContratRelationship(currentConventionFromOrigine);
			myFactoryEcritureDetail.attribuerUnEcritureDetail(getEditingContext(), currentEcriture, detail);
			ecritureRectifPanel.getMyEcritureSaisieDetailListPanel().ecritureDetailAjouterInDg(detail);
		}
		if (lastdetail != null) {
			detail.setEcdLibelle(lastdetail.ecdLibelle());
		}
		lastdetail = detail;
	}

	/**
	 * Crée un objet Ecriture à partir de la saisie et autorise la saisie du detail.
	 */
	private void ecritureDetailSaisieLance() {
		try {
			checkSaisieEcritureDico();
			revertChanges();

			EOEcriture newEcriture = createEcritureFromEcritureDico(currentEcritureDico);
			if (newEcriture == null) {
				throw new DefaultClientException("Impossible de créer un objet Ecriture.");
			}
			currentEcriture = newEcriture;

			//Mettre à jour le dico
			updateEcritureDicoFromEcriture(currentEcriture);

			ecritureSaisiePanel.getMyEcritureFormPanel().updateData();
			ecritureSaisiePanel.lockSaisieEcriture(true);
			ecritureSaisiePanel.lockSaisieEcritureDetail(false);
			actionDetail.setCancelMode();

			currentConventionFromOrigine = EOOrigine.getAccordContratFromOrigine(getEditingContext(), currentEcriture.origine());

			//On crée un nvel objet EcritureDetail en Auto
			ecritureDetailAjouter();

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Supprime tous les details saisis et reactive la saisie de l'ecriture.
	 */
	private void ecritureDetailSaisieAnnule() {
		//Nettoyer les details
		ecritureDetailsNettoyer();

		ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().updateData();

		//si details existants, demander confirmation
		NSArray details = currentEcriture.detailEcriture();
		if (details.count() > 0) {
			if (!showConfirmationDialog("Confirmation", "Tous les détails de cette écriture vont être supprimés, est-ce vraiment ce que vous voulez faire ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		//supprimer tous les details
		while (currentEcriture.detailEcriture().count() > 0) {
			myFactoryEcritureDetail.supprimerEcritureDetail(getEditingContext(), (EOEcritureDetail) currentEcriture.detailEcriture().objectAtIndex(0));
		}

		//On annule les modifications sur l'ec car lors de la prochaine activation de saisie du detail, un nveau ecrituredetail est créé
		getEditingContext().revert();

		try {
			ecritureSaisiePanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
		ecritureSaisiePanel.lockSaisieEcriture(false);
		ecritureSaisiePanel.lockSaisieEcritureDetail(true);
		actionDetail.setOkMode();
	}

	private int ecritureDetailGetMaxIndex(NSArray ecds) {
		int res = 0;
		for (int i = 0; i < ecds.count(); i++) {
			EOEcritureDetail array_element = (EOEcritureDetail) ecds.objectAtIndex(i);
			if (array_element.ecdIndex().intValue() > res) {
				res = array_element.ecdIndex().intValue();
			}
		}
		return res;
	}

	private void ecritureDetailSupprimer() {
		System.out.println("nb row = " + ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().getMyEOTable().getRowCount());
		if (!ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().stopCellEditing()) {
			return;
		}

		if (MODE_NEW == modeSaisie) {
			EOEcritureDetail detail = ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().selectedEcritureDetail();
			myFactoryEcritureDetail.supprimerEcritureDetail(getEditingContext(), detail);
			ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().updateData();
		}
		else if (MODE_RECTIF == modeSaisie) {
			EOEcritureDetail detail = ecritureRectifPanel.getMyEcritureSaisieDetailListPanel().selectedEcritureDetail();
			myFactoryEcritureDetail.supprimerEcritureDetail(getEditingContext(), detail);
			ecritureRectifPanel.getMyEcritureSaisieDetailListPanel().updateData();
		}
		System.out.println("nb row = " + ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().getMyEOTable().getRowCount());

	}

	/**
	 * Met à jour currentEcriture avec les valeurs du dico
	 * 
	 * @param dico
	 */
	private final void updateCurrentEcritureFromEcritureDico(HashMap dico) {
		//On ne touche pas au reste des attributs, on ne modifie que les infos simples,
		//sinon passer par la factory
		currentEcriture.setEcrLibelle((String) dico.get("ecrLibelle"));
		currentEcriture.setEcrPostit((String) dico.get("ecrPostit"));
	}

	private final void updateEcritureDicoFromEcriture(EOEcriture ecriture) {
		currentEcritureDico = ZEOUtilities.convertEOEnterpriseObjectToHashMap(ecriture);

		System.out.println("EcritureSaisieCtrl.updateEcritureDicoFromEcriture()");
		ZLogger.debug(currentEcritureDico);
		System.out.println();
	}

	private EOEcriture createEcritureFromEcritureDico(HashMap ecritureDico) throws Exception {
		return myFactoryProcessJournalEcriture.creerEcriture(getEditingContext(), (NSTimestamp) ecritureDico.get("ecrDateSaisie"), (String) ecritureDico.get("ecrLibelle"), (Integer) ecritureDico.get("ecrNumero"), (String) ecritureDico.get("ecrPostit"), (EOComptabilite) ecritureDico
				.get("comptabilite"), (EOExercice) ecritureDico.get("exercice"), (EOOrigine) ecritureDico.get("origine"), (EOTypeJournal) ecritureDico.get("typeJournal"), (EOTypeOperation) ecritureDico.get("typeOperation"), (EOUtilisateur) ecritureDico.get("utilisateur"));
	}

	/**
	 * A appeler pour la création d'une nouvelle ecriture.
	 * 
	 * @return un dictionnaire représentant une ecriture avec des valeurs par défaut.
	 * @throws DefaultClientException
	 */
	private HashMap defaultEcritureDico() throws DefaultClientException {
		HashMap res = new HashMap();
		res.put("ecrDateSaisie", new NSTimestamp(getDateJourneeComptable())); //FIXED chgt exercice date journee comptable
		//        res.put("ecrDateSaisie",  new NSTimestamp( ZDateUtil.getToday().getTime() ));

		res.put("ecrDate", Factory.getNow());
		//        res.put("ecrLibelle", null);
		res.put("ecrNumero", new Integer(0));
		//        res.put("ecrPostit", null);
		//        res.put("comptabilite", comptabilites.objectAtIndex(0)  );
		res.put("comptabilite", getComptabilite());
		res.put("exercice", myApp.appUserInfo().getCurrentExercice());
		//        res.put("origine", null);
		res.put("typeJournal", FinderJournalEcriture.leTypeJournalExercice(getEditingContext()));
		res.put("typeOperation", FinderJournalEcriture.leTypeOperationGenerique(getEditingContext()));
		res.put("utilisateur", myApp.appUserInfo().getUtilisateur());
		return res;
	}

	/**
	 * @param detail
	 * @return true si le ecritrureDeatil n'a pas été saisi par l'utilisateur (ecdMontant=null, gestion==null, etc.)
	 */
	private boolean ecritureDetailIsEmpty(EOEcritureDetail detail) {
		return (detail.ecdMontant() == null && detail.gestion() == null && detail.planComptable() == null);
	}

	/**
	 * Parcourt les detailecritures et supprime les details vides.
	 */
	private void ecritureDetailsNettoyer() {
		if (currentEcriture != null) {
			NSArray tmp = currentEcriture.detailEcriture();
			ArrayList res = new ArrayList();
			if (tmp != null) {
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (ecritureDetailIsEmpty(element)) {
						res.add(element);
					}
				}
				if (res.size() > 0) {
					Iterator iter = res.iterator();
					while (iter.hasNext()) {
						EOEcritureDetail element = (EOEcritureDetail) iter.next();
						myFactoryEcritureDetail.supprimerEcritureDetail(getEditingContext(), element);
					}

				}
			}
		}
	}

	/**
	 * Vérifie la validité de la saise des infos de l'ecriture, indépendamment des details.
	 * 
	 * @throws Exception
	 */
	private final void checkSaisieEcritureDico() throws Exception {
		ZLogger.debug(currentEcritureDico);
		if (ZStringUtil.isEmpty((String) currentEcritureDico.get("ecrLibelle"))) {
			throw new DataCheckException("Le libellé de l'écriture est obligatoire");
		}
		if (currentEcritureDico.get("comptabilite") == null) {
			throw new DataCheckException("La comptabilité est obligatoire");
		}
		if (currentEcritureDico.get("typeJournal") == null) {
			throw new DataCheckException("Le type de journal est obligatoire");
		}
		if (currentEcritureDico.get("typeOperation") == null) {
			throw new DataCheckException("Le type d'opération est obligatoire");
		}
	}

	/**
	 * @return les codes gestions "Agence" à partir des EcritureDetails passé en parametre
	 */
	private final ZNSMutableArray agenceFromEcritureDetails(final NSArray details) {
		final ZNSMutableArray array = new ZNSMutableArray();
		for (int i = 0; i < details.count(); i++) {
			final EOEcritureDetail element = (EOEcritureDetail) details.objectAtIndex(i);
			if (element.gestion().isAgence()) {
				array.addObject(element.gestion());
			}
		}
		return array;
	}

	/**
	 * @param details
	 * @return les codes gestions "composantes" à partir des EcritureDetails passé en parametre. (avec doublons)
	 */
	private final ZNSMutableArray composantesFromEcritureDetails(final NSArray details) {
		final ZNSMutableArray array = new ZNSMutableArray();
		for (int i = 0; i < details.count(); i++) {
			final EOEcritureDetail element = (EOEcritureDetail) details.objectAtIndex(i);
			//			if (FinderGestion.gestionIsComposante(element.gestion(), element.exercice())) {
			if (element.gestion().isComposante(element.exercice())) {
				array.addObject(element.gestion());
			}
		}
		return array;

	}

	private final ZNSMutableArray sacdsFromEcritureDetails(final NSArray details) {
		final ZNSMutableArray array = new ZNSMutableArray();
		for (int i = 0; i < details.count(); i++) {
			final EOEcritureDetail element = (EOEcritureDetail) details.objectAtIndex(i);
			//			if (FinderGestion.gestionIsSacd(element.gestion(), element.exercice())) {
			if (element.gestion().isSacd(element.exercice())) {
				array.addObject(element.gestion());
			}
		}
		return array;
	}

	/**
	 * Vérifie si la saisie des details est correcte.
	 * 
	 * @throws Exception
	 */
	private final void checkSaisieDetails() throws Exception {

		//Vérifie qu'on a des details
		if (currentEcriture.detailEcriture() == null || currentEcriture.detailEcriture().count() == 0) {
			throw new DataCheckException("Aucun détail écriture de saisi.");
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Vérifier que les codes gestions sont valides, comptes et montants
		for (int i = 0; i < currentEcriture.detailEcriture().count(); i++) {
			EOEcritureDetail element = (EOEcritureDetail) currentEcriture.detailEcriture().objectAtIndex(i);
			if (element.gestion() == null || gestions.indexOfObject(element.gestion()) == NSArray.NotFound) {
				throw new DataCheckException("Vous n'avez pas défini de code gestion valide pour un ou plusieurs détails d'écritures.");
			}
			if (element.planComptable() == null) {
				throw new DataCheckException("Vous n'avez pas défini d'imputation pour un ou plusieurs détails d'écritures.");
			}
			if (element.ecdMontant() == null || element.ecdMontant().doubleValue() == 0) {
				throw new DataCheckException("Vous n'avez pas défini de montant pour un ou plusieurs détails d'écritures.");
			}

			if (element.ecdLibelle() == null || element.ecdLibelle().length() == 0) {
				element.setEcdLibelle(currentEcriture.ecrLibelle());
			}

		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//vérifier que l'écriture est équilibrée
		final BigDecimal sommeDebits = currentEcriture.getDebitsMontant();
		final BigDecimal sommeCredits = currentEcriture.getCreditsMontant();

		final NSArray debits = currentEcriture.getDebits();
		final NSArray credits = currentEcriture.getCredits();

		ZLogger.debug("sommeDebits", sommeDebits);
		ZLogger.debug("sommeCredits", sommeCredits);

		if (((sommeDebits == null) || (sommeDebits.compareTo(sommeCredits) != 0))) {
			throw new DataCheckException("L'écriture n'est pas équilibrée. (D : " + sommeDebits + " / C : " + sommeCredits + ")");
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//Vérifications portant sur les codes gestions et sacds.
		final EOGestion agence = currentEcriture.comptabilite().gestion();

		final ZNSMutableArray agencesForCredits = agenceFromEcritureDetails(credits);
		final ZNSMutableArray agencesForDebits = agenceFromEcritureDetails(debits);
		final ZNSMutableArray sacdsForCredits = sacdsFromEcritureDetails(credits);
		final ZNSMutableArray sacdsForDebits = sacdsFromEcritureDetails(debits);
		final ZNSMutableArray composantesForCredits = composantesFromEcritureDetails(credits);
		final ZNSMutableArray composantesForDebits = composantesFromEcritureDetails(debits);

		final NSArray agencesForCreditsDistinct = agencesForCredits.distincts();
		final NSArray agencesForDebitsDistinct = agencesForDebits.distincts();
		final NSArray sacdsForCreditsDistinct = sacdsForCredits.distincts();
		final NSArray sacdsForDebitsDistinct = sacdsForDebits.distincts();
		final NSArray composantesForCreditsDistinct = composantesForCredits.distincts();
		final NSArray composantesForDebitsDistinct = composantesForDebits.distincts();

		System.out.println("**********************");
		System.out.println("Nb debits sur agence : " + agencesForDebits.count());
		System.out.println("Nb debits sur composantes : " + composantesForDebits.count());
		System.out.println("Nb debits sur sacds : " + sacdsForDebits.count());
		System.out.println("Nb credits sur agence : " + agencesForCredits.count());
		System.out.println("Nb credits sur composantes : " + composantesForCredits.count());
		System.out.println("Nb credits sur sacds : " + sacdsForCredits.count());
		System.out.println("");
		System.out.println("Nb agences en crédit : " + agencesForCreditsDistinct.count());
		System.out.println("Nb composantes en crédit : " + composantesForCreditsDistinct.count());
		System.out.println("Nb sacds en crédit : " + sacdsForCreditsDistinct.count());
		System.out.println("Nb agences en débit : " + agencesForDebitsDistinct.count());
		System.out.println("Nb composantes en débit : " + composantesForDebitsDistinct.count());
		System.out.println("Nb sacds en débit : " + sacdsForDebitsDistinct.count());

		//Vérifier si la saisie est correcte concernant les SACDs
		final NSArray sacdsDebits = FactoryProcessJournalEcriture.lesSacds(getEditingContext(), debits);
		final NSArray sacdsCredits = FactoryProcessJournalEcriture.lesSacds(getEditingContext(), credits);

		if ((sacdsForCreditsDistinct.count() > 1 && sacdsForDebitsDistinct.count() > 1) || (sacdsForCreditsDistinct.count() == 1 && sacdsForDebitsDistinct.count() > 1) || (sacdsForCreditsDistinct.count() > 1 && sacdsForDebitsDistinct.count() == 1)) {
			throw new DataCheckException("Impossible de créer une écriture avec plusieurs SACD en débits ou plusieurs SACD en crédits");
		}

		//
		if (sacdsForDebitsDistinct.count() >= 1 && (sacdsForCreditsDistinct.count() + composantesForCreditsDistinct.count() + agencesForCreditsDistinct.count() > 1)) {
			throw new DataCheckException("Impossible de créer une écriture entre un SACD et plusieurs autre codes gestions.");
		}
		if (sacdsForCreditsDistinct.count() >= 1 && (sacdsForDebitsDistinct.count() + composantesForDebitsDistinct.count() + agencesForDebitsDistinct.count() > 1)) {
			throw new DataCheckException("Impossible de créer une écriture entre un SACD et plusieurs autre codes gestions.");
		}

		//Si on a des SACDS
		if (sacdsForCreditsDistinct.count() > 0 && sacdsForDebitsDistinct.count() > 0) {
			//Si on a des SACD en debit, on ne doit pas avoir de composantes en crédit et inversement 
			if (sacdsForCreditsDistinct.count() > 0 && composantesForDebitsDistinct.count() > 0) {
				throw new DataCheckException("Les écritures sur un SACD doivent se faire à partir de l'agence (code gestion " + agence.gesCode() + ").");
			}

			if (sacdsForDebitsDistinct.count() > 0 && composantesForCreditsDistinct.count() > 0) {
				throw new DataCheckException("Les écritures sur un SACD doivent se faire à partir de l'agence (code gestion " + agence.gesCode() + ").");
			}
		}

		//Vérifier (si on a pas agence, on bloque)
		if (sacdsCredits.count() == 0 && sacdsDebits.count() > 0) {
			NSArray creditsHorsAgence = ZEOUtilities.getFilteredArrayByKeyValue(credits, "gestion", agence, "<>");
			//verifier qu'on a que l'agence en credit
			if (creditsHorsAgence != null && creditsHorsAgence.count() > 0) {
				throw new DataCheckException("Les écritures sur un SACD doivent se faire à partir de l'agence (code gestion " + agence.gesCode() + ").");
			}
		}
		if (sacdsDebits.count() == 0 && sacdsCredits.count() > 0) {
			NSArray debitsHorsAgence = ZEOUtilities.getFilteredArrayByKeyValue(debits, "gestion", agence, "<>");
			//verifier qu'on a que l'agence en credit
			if (debitsHorsAgence != null && debitsHorsAgence.count() > 0) {
				throw new DataCheckException("Les écritures sur un SACD doivent se faire à partir de l'agence (code gestion " + agence.gesCode() + ").");
			}
		}

		//Récupérer tous les comptes utilisés
		NSArray details185 = ZEOUtilities.getFilteredArrayByKeyValue(currentEcriture.detailEcriture(), "planComptable.pcoNum", "185*", "like");
		if (details185 != null && details185.count() > 0) {
			if (!showConfirmationDialog("Confirmation", "Attention\nSi vous saisissez une écriture sur des comptes 185*, la détermination des écritures SACD ne sera pas automatique. Souhaitez-vous continuer ?", ZMsgPanel.BTLABEL_NO)) {
				throw new DataCheckException("L'écriture n'a pas été enregistrée, vous pouvez la modifier.");
			}
		}

	}

	private final void annulerSaisie() {
		if (getEditingContext().hasChanges()) {
			if (!showConfirmationDialog("Confirmation", "Vous n'avez pas validé l'écriture en cours de saisie, souhaitez-vous néammoins quitter cette fenêtre ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		revertChanges();
		getMyDialog().onCancelClick();
	}

	/**
     *
     */
	private final void validerSaisie() {
		try {
			//si la table est en edition, on valide les modifs
			if (!ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().stopCellEditing()) {
				return;
			}

			validerSaisieForNewEcriture();
			//On annule les eventuelles modifications - normalement inutile... 
			getEditingContext().revert();
			getMyDialog().onOkClick();
		} catch (DataCheckException e) {
			showErrorDialog(e);
		} catch (Exception e) {
			showErrorDialog(e);
			getEditingContext().revert();
			getMyDialog().onOkClick();
		}
	}

	private final void validerSaisieRectif() {
		try {

			//si la table est en edition, on valide les modifs
			if (ecritureRectifPanel.getMyEcritureSaisieDetailListPanel().isEditing()) {
				if (!ecritureRectifPanel.getMyEcritureSaisieDetailListPanel().stopCellEditing()) {
					return;
				}
			}

			//Supprimer les details ecritures vides
			ecritureDetailsNettoyer();
			ecritureRectifPanel.getMyEcritureSaisieDetailListPanel().updateData();
			checkSaisieRectif();

			validerSaisieForRectif();
			getMyDialog().onOkClick();
		} catch (DataCheckException e) {
			showErrorDialog(e);
		} catch (Exception e) {
			showErrorDialog(e);
			getEditingContext().revert();
			getMyDialog().onOkClick();

		}
	}

	private final void validerSaisieForNewEcriture() throws Exception {
		checkSaisieEcritureDico();

		//Supprimer les details ecritures vides
		ecritureDetailsNettoyer();

		ecritureSaisiePanel.getMyEcritureSaisieDetailListPanel().updateData();

		if (currentEcriture == null) {
			throw new DataCheckException("L'écriture n'est pas valide.");
		}

		updateCurrentEcritureFromEcritureDico(currentEcritureDico);

		//Vérifier la saisie
		checkSaisieDetails();

		//Vérifier les SACD

		//Dans le cas d'un SACD, possibilité de récupérer
		//des nouvelles écritures, dans ce cas, elles soivent être numérotées

		//On affiche également le résultat de l'écriture
		//(en haut les ecritures généres, en bas le detail de chaque ecriture)

		NSArray ecrituresSACD = null;
		NSArray details185 = ZEOUtilities.getFilteredArrayByKeyValue(currentEcriture.detailEcriture(), "planComptable.pcoNum", "185*", "like");
		//Ajout 10/03/2005
		if (details185 == null || details185.count() == 0) {
			try {
				ecrituresSACD = myFactoryProcessJournalEcriture.determinerLesEcrituresSACD(getEditingContext(), currentEcriture);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Erreur lors de la détermination des éventuelles écritures SACD." + e.getMessage());
			}
		}
		System.out.println();
		//la sauvegarde
		try {
			System.out.println("");
			System.out.println(new Date() + " **Avant saveChanges-->");
			getEditingContext().saveChanges();
			System.out.println(new Date() + "-->Apres saveChanges : modifications enregistrees**");
			System.out.println("");
		}
		//Permet de traiter l'exception qui se produit de tant en temps...
		//pas terrible, mais pour l'instant je ne sais pas d'où vient cette exception
		catch (java.lang.IllegalStateException e) {
			//vérifier si l'objet est bien enregistré
			if ((getEditingContext().globalIDForObject(currentEcriture) instanceof EOTemporaryGlobalID) || currentEcriture.detailEcriture() == null || currentEcriture.detailEcriture().count() == 0) {
				System.out.println("Erreur : objet non enregistre = " + currentEcriture);
				throw e;
			}
			System.out.println("Erreur IllegalStateException mais objet enregistre = " + currentEcriture);
			//sinon on invalide
			getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
					getEditingContext().globalIDForObject(currentEcriture)
			}));
		}
		//        System.out.println("objet enregistre = " + currentEcriture);

		String msgFin = "Ecriture enregistrée. ";

		//Si c'est bien enregistre, on numerote et on croise les doigts
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
		try {
			myFactoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), currentEcriture, myKFactoryNumerotation);

			System.out.println("CurrentEcriture=" + currentEcriture);
			msgFin = msgFin + "\n\nL'écriture saisie porte le numéro " + currentEcriture.ecrNumero();

			//Numéroter les SACD
			NSMutableArray nbs = new NSMutableArray();
			if ((ecrituresSACD != null) && (ecrituresSACD.count() > 0)) {
				for (int i = 0; i < ecrituresSACD.count(); i++) {
					EOEcriture element = (EOEcriture) ecrituresSACD.objectAtIndex(i);
					if (!element.equals(currentEcriture)) {
						myFactoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
						nbs.addObject(element.ecrNumero());
					}
				}
			}
			System.out.println("Ecriture enregistree n° " + currentEcriture.ecrNumero());
			if (nbs.count() > 0) {
				msgFin = msgFin + "\nDes écritures SACD ont été généres automatiquement : " + nbs.componentsJoinedByString(",");
				System.out.println("Ecriture SACD enregistree n° " + nbs.componentsJoinedByString(","));
			}
			System.out.println();
			myApp.showInfoDialog(msgFin);

		} catch (Exception e) {
			System.out.println("ERREUR LORS DE LA NUMEROTATION ECRITURE...");
			e.printStackTrace();
			throw new Exception(e);
		}

		//mettre à jour le dico
		updateEcritureDicoFromEcriture(currentEcriture);

		NSMutableArray tmpArray = new NSMutableArray();
		if (ecrituresSACD != null) {
			tmpArray.addObjects(ecrituresSACD.objects());
		}
		tmpArray.addObject(currentEcriture);
		ecrituresForRecap = tmpArray.immutableClone();

		//Emargements autos
		try {

			NSArray newEmargements = creerEmargementsAutoForEcriture(currentEcriture);
			getEditingContext().saveChanges();
			msgFin = "";
			if (newEmargements.count() > 0) {
				try {
					for (int i = 0; i < newEmargements.count(); i++) {
						final EOEmargement element = (EOEmargement) newEmargements.objectAtIndex(i);
						FactoryEmargementProxy fp2 = new FactoryEmargementProxy(getEditingContext(), getUtilisateur(), getExercice(), FinderEmargement.leTypeLettrageAutomatique(getEditingContext()), false, new NSTimestamp(getDateJourneeComptable()));
						fp2.numeroterEmargement(getEditingContext(), element);

					}
					msgFin += "\n\nEmargement(s) généré(s) " + ZEOUtilities.getCommaSeparatedListOfValues(newEmargements, "emaNumero");
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
					e.printStackTrace();
					throw new Exception(e);
				}

				showInfoDialog(msgFin);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e);
		}

	}

	public NSArray creerEmargementsAutoForEcriture(EOEcriture currentEcriture) throws Exception {
		NSMutableArray res = new NSMutableArray();
		NSArray details = currentEcriture.detailEcriture();
		for (int i = 0; i < details.count(); i++) {
			EOEcritureDetail ecd = (EOEcritureDetail) details.objectAtIndex(i);
			if (ecd.getEcdForEmargement() != null) {
				System.out.println("ecd.ecdResteEmarger(): " + ecd.ecdResteEmarger());
				NSArray lesDebits = NSARRAYVIDE;
				NSArray lesCredits = NSARRAYVIDE;
				if (EOEcritureDetail.SENS_DEBIT.equals(ecd.ecdSens())) {
					lesDebits = new NSArray(new Object[] {
							ecd
					});
					lesCredits = new NSArray(new Object[] {
							ecd.getEcdForEmargement()
					});
				}
				else {
					lesCredits = new NSArray(new Object[] {
							ecd
					});
					lesDebits = new NSArray(new Object[] {
							ecd.getEcdForEmargement()
					});
				}

				NSMutableArray ecrituresGenerees = new NSMutableArray();
				FactoryEmargementProxy fp = new FactoryEmargementProxy(getEditingContext(), getUtilisateur(), getExercice(), FinderEmargement.leTypeLettrage(getEditingContext()), false, new NSTimestamp(getDateJourneeComptable()));
				NSArray newEmargements = fp.emarger(FactoryEmargementProxy.EMARGEMENT_D1_C1, lesDebits, lesCredits, null, getComptabilite(), ecrituresGenerees);
				res.addObjectsFromArray(newEmargements);
			}
		}
		return res.immutableClone();
	}

	private final void checkSaisieRectif() throws Exception {

		//Vï¿½rifier qu'au moins un des nouveaux details porte sur un compte dï¿½ja prï¿½sent dans l'ecriture ï¿½ rectifier
		NSMutableArray nvoxComptes = new NSMutableArray();
		for (int j = 0; j < currentEcriture.detailEcriture().count(); j++) {
			final EOEcritureDetail element = (EOEcritureDetail) currentEcriture.detailEcriture().objectAtIndex(j);
			if (nvoxComptes.indexOfObject(element.planComptable().pcoNum()) == NSArray.NotFound) {
				nvoxComptes.addObject(element.planComptable().pcoNum());
			}
		}
		NSMutableArray oldComptes = new NSMutableArray();
		for (int j = 0; j < currentEcritureARectifier.detailEcriture().count(); j++) {
			final EOEcritureDetail element = (EOEcritureDetail) currentEcritureARectifier.detailEcriture().objectAtIndex(j);
			if (oldComptes.indexOfObject(element.planComptable().pcoNum()) == NSArray.NotFound) {
				oldComptes.addObject(element.planComptable().pcoNum());
			}
		}

		boolean comptePresent = false;
		for (int j = 0; (j < nvoxComptes.count() && !comptePresent); j++) {
			final String element = (String) nvoxComptes.objectAtIndex(j);
			if (oldComptes.indexOfObject(element) != NSArray.NotFound) {
				comptePresent = true;
			}
		}
		if (!comptePresent) {
			throw new DataCheckException("La rectification doit porter sur au moins un des comptes de l'ecriture a rectifier");
		}
	}

	private final void validerSaisieForRectif() throws Exception {
		System.out.println("EcritureSaisieCtrl.validerSaisieForRectif() " + currentEcritureDico);
		ZLogger.debug(currentEcritureDico);

		checkSaisieEcritureDico();

		//Supprimer les details ecritures vides
		ecritureDetailsNettoyer();

		if (currentEcriture == null) {
			throw new DataCheckException("L'écriture n'est pas valide.");
		}

		//Vérifier la saisie
		checkSaisieDetails();

		if (currentEcriture == null) {
			throw new DataCheckException("L'écriture n'est pas valide.");
		}

		NSMutableArray nvoxDetails = new NSMutableArray();
		for (int i = 0; i < currentEcriture.detailEcriture().count(); i++) {
			EOEcritureDetail element = (EOEcritureDetail) currentEcriture.detailEcriture().objectAtIndex(i);
			nvoxDetails.addObject(element);
		}

		ZLogger.debug("nvoxDetails", nvoxDetails);

		ZLogger.debug("Avant suppression currentecriture");
		ZLogger.debug(getEditingContext());

		//On supprime l'objet currentEcriture pour qu'il ne soit pas enregistré
		getEditingContext().deleteObject(currentEcriture);

		ZLogger.debug("Apres suppression currentecriture = ", currentEcriture);
		ZLogger.debug(getEditingContext());

		EOEcriture nvEcr = myFactoryProcessJournalEcriture.rectifierEcriture(getEditingContext(), currentEcritureARectifier, nvoxDetails, myApp.appUserInfo().getUtilisateur());
		if (nvEcr == null) {
			throw new DefaultClientException("L'écriture générée est nulle.");
		}

		NSArray ecrituresSACD = null;
		NSArray details185 = ZEOUtilities.getFilteredArrayByKeyValue(nvEcr.detailEcriture(), "planComptable.pcoNum", "185*", "like");
		if (details185 == null || details185.count() == 0) {
			try {
				ecrituresSACD = myFactoryProcessJournalEcriture.determinerLesEcrituresSACD(getEditingContext(), nvEcr);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception("Erreur lors de la détermination des éventuelles écritures SACD." + e.getMessage());
			}
		}

		ZLogger.debug("Avant enregistrement nvEcr  = ", nvEcr);
		ZLogger.debug(getEditingContext());

		//la sauvegarde
		getEditingContext().saveChanges();
		String msgFin = "Ecriture enregistrée. ";

		//On numerote
		KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
		try {
			myFactoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), nvEcr, myKFactoryNumerotation);
			System.out.println("nvEcr=" + nvEcr);
			msgFin = msgFin + "\n\nL'écriture de rectification porte le n° " + nvEcr.ecrNumero();
			//Numéroter les SACD
			NSMutableArray nbs = new NSMutableArray();
			if ((ecrituresSACD != null) && (ecrituresSACD.count() > 0)) {
				for (int i = 0; i < ecrituresSACD.count(); i++) {
					EOEcriture element = (EOEcriture) ecrituresSACD.objectAtIndex(i);
					if (!element.equals(currentEcriture)) {
						myFactoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
						nbs.addObject(element.ecrNumero());
					}
				}
			}
			System.out.println("Ecriture enregistree n° " + currentEcriture.ecrNumero());
			if (nbs.count() > 0) {
				msgFin = msgFin + "\nDes écritures SACD ont été généres automatiquement : " + nbs.componentsJoinedByString(",");
				System.out.println("Ecriture SACD enregistree n° " + nbs.componentsJoinedByString(","));
			}
			System.out.println();
			myApp.showInfoDialog(msgFin);

		} catch (Exception e) {
			System.out.println("ERREUR LORS DE LA NUMEROTATION ECRITURE...");
			e.printStackTrace();
			throw new Exception(e);
		}

		ecrituresForRecap = new NSArray(new Object[] {
				currentEcriture, nvEcr
		});

		////        showRecapitulatif();
		//
		////        //Locker les formulaire
		////        lockAll(true);
		//
		//        //Desactiver le bouton valider
		//        actionValider.setEnabled(false);

	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		ecritureSaisiePanel.setMyDialog(getMyDialog());
		ecritureSaisiePanel.initGUI();
		ecritureSaisiePanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(ecritureSaisiePanel);
		win.pack();
		return win;
	}

	private final ZKarukeraDialog createModalDialogRectif(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, "Rectification d'une écriture", true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, "Rectification  d'une écriture", true);
		}
		setMyDialog(win);
		ecritureRectifPanel.setMyDialog(getMyDialog());
		ecritureRectifPanel.initGUI();
		ecritureRectifPanel.setPreferredSize(new Dimension(812, 720));
		win.setContentPane(ecritureRectifPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de saisie.
	 */
	public final NSArray openDialogNew(Window dial) {
		final ZKarukeraDialog win = createModalDialog(dial);

		//maintenir la connection avec le serveur
		final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
			public void onBeat() {
				ServerProxy.clientSideRequestMsgToServer(getDefaultEditingContext(), "Saisie d'une écriture en cours...", myApp.getIpAdress());
			}

			public void mainTaskStart() {
			}

			public boolean isMainTaskFinished() {
				return false;
			};
		};
		final ZWaitingThread waitingThread = new ZWaitingThread(20000, 20000, defaultZHeartBeatListener);
		waitingThread.start();

		try {
			newSaisie();
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
			waitingThread.interrupt();
		}
		return ecrituresForRecap;
	}

	public final NSArray openDialogRectif(Window dial, EOEcriture ecrARectifier) {
		ZKarukeraDialog win = createModalDialogRectif(dial);
		try {
			//Vérifier si on est sur un type de journal pris en charge
			final EOTypeJournal tjo = ecrARectifier.typeJournal();
			if (typeJournalsReimputation.indexOfObject(tjo) == NSArray.NotFound) {
				throw new DataCheckException("Ce type de journal (" + tjo.tjoLibelle() + ") n'est pas pris en charge pour la rectification d'écriture. S'il s'agit d'une opéraration de visa de mandat/recette, veuillez utiliser la réimputation.");
			}

			rectifierSaisie(ecrARectifier);

			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
		return ecrituresForRecap;
	}

	/**
	 * Initialise l'interface pour la saisie d'une nouvelle ecriture. L'objet currentEcriture est initialisé à null. Un Message de confirmation est
	 * affiché s'il y a des changements non enregistrés.
	 * 
	 * @throws DefaultClientException
	 */
	public final void newSaisie() throws DefaultClientException {
		modeSaisie = MODE_NEW;
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}

		//        actionDetail.setEnabled(true);
		currentEcriture = null;
		currentEcritureDico = defaultEcritureDico();
		ecrituresForRecap = new NSArray();

		//        showSaisie();
		try {
			ecritureSaisiePanel.updateData();
			ecritureSaisiePanel.lockAll(false);
			ecritureSaisiePanel.lockSaisieEcriture(false);
			ecritureSaisiePanel.lockSaisieEcritureDetail(true);
			actionDetail.setOkMode();
			actionValider.setEnabled(true);
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	/**
	 * Initialise l'interface pour la saisie d'une rectification d'ecriture.
	 */
	public final void rectifierSaisie(EOEcriture ecrARectifier) {
		modeSaisie = MODE_RECTIF;
		if (getEditingContext().hasChanges()) {
			getEditingContext().revert();
		}
		currentEcritureARectifier = ecrARectifier;

		//        currentEcriture = currentEcritureARectifier;
		//On initialise le dico de saisie avec les valeurs de l'ecriture a rectifier
		updateEcritureDicoFromEcriture(currentEcritureARectifier);

		try {

			//On cree un nouvel objet ecriture à partir du dico
			currentEcriture = createEcritureFromEcritureDico(currentEcritureDico);

			ecrituresForRecap = new NSArray();

			ecritureRectifPanel.updateData();
			//            ecritureRectifPanel.lockAll(false);
			ecritureRectifPanel.lockSaisieEcriture(true);
			ecritureRectifPanel.lockSaisieEcritureDetail(false);
			//            actionDetail.setCancelMode();
			actionValider.setEnabled(true);
			ecritureRectifPanel.updateData();

		} catch (Exception e1) {
			showErrorDialog(e1);
			getEditingContext().revert();
		}

	}

	private class EcritureSaisiePanelListener implements org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel.IEcritureSaisiePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel.IEcritureSaisiePanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValider;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel.IEcritureSaisiePanelListener#myActionDeleteEcritureDetail()
		 */
		public Action myActionDeleteEcritureDetail() {
			return actionDeleteEcritureDetail;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel.IEcritureSaisiePanelListener#myActionAddEcritureDetail()
		 */
		public Action myActionAddEcritureDetail() {
			return actionAddEcritureDetail;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel.IEcritureSaisiePanelListener#actionDetail()
		 */
		public Action actionDetail() {
			return actionDetail;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisiePanel.IEcritureSaisiePanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

	}

	public final class ActionValider extends AbstractAction {

		public ActionValider() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisie();
		}

	}

	public final class ActionValiderRectif extends AbstractAction {

		public ActionValiderRectif() {
			super("Valider");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			validerSaisieRectif();
		}

	}

	private final class ActionAddEcritureDetail extends AbstractAction {

		/**
         *
         */
		public ActionAddEcritureDetail() {
			super("");

			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un détail d'écriture");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			ecritureDetailAjouter();
		}

	}

	private final class ActionDeleteEcritureDetail extends AbstractAction {

		/**
         *
         */
		public ActionDeleteEcritureDetail() {
			super("");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer ce détail d'écriture");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			setEnabled(false);

		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			ecritureDetailSupprimer();
		}

	}

	public final class ActionDetail extends AbstractAction {
		public final int MODE_OK = 0;
		public final int MODE_CANCEL = 1;
		private final ImageIcon iconOk;
		private final ImageIcon iconCancel;
		private int mode;

		/**
		 * Crée une nouvelle action detail avec le mode OK.
		 */
		public ActionDetail() {
			super();
			iconOk = ZIcon.getIconForName(ZIcon.ICON_OK_16);
			iconCancel = ZIcon.getIconForName(ZIcon.ICON_CANCEL_16);
			setOkMode();
			setEnabled(false);
		}

		public void setOkMode() {
			mode = MODE_OK;
			this.putValue(AbstractAction.NAME, "Saisir le détail");
			this.putValue(AbstractAction.SMALL_ICON, iconOk);
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Activer la saisie du détail");
		}

		public void setCancelMode() {
			mode = MODE_CANCEL;
			this.putValue(AbstractAction.NAME, "Supprimer le détail");
			this.putValue(AbstractAction.SMALL_ICON, iconCancel);
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer tous les détails");
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			if (mode == MODE_OK) {
				ecritureDetailSaisieLance();
			}
			else {
				ecritureDetailSaisieAnnule();
			}

		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerSaisie();
		}

	}

	private final class EcritureFormPanelListener implements org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener#getTypesOperations()
		 */
		public NSArray getTypesOperations() {
			if (modeSaisie == MODE_NEW) {
				return typeOperations;
			}
			return typeOperationsReimputation;

		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener#getOrigineModel()
		 */
		public ZEOComboBoxModel getOrigineModel() {
			return origineModel;
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener#getcomptabilites()
		//         */
		//        public NSArray getcomptabilites() {
		//            return comptabilites;
		//        }

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener#getTypeJournals()
		 */
		public NSArray getTypeJournals() {
			if (modeSaisie == MODE_NEW) {
				return typeJournals;
			}
			return typeJournalsReimputation;

		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener#getCurrentEcritureDico()
		 */
		public HashMap getCurrentEcritureDico() {
			return currentEcritureDico;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureFormPanel.IEcritureFormPanelListener#typeOperationChanged()
		 */
		public void typeOperationChanged() {
			currentEcritureDico.put("typeOperation", ecritureSaisiePanel.getMyEcritureFormPanel().getMyTypeOperationModel().getSelectedEObject());
			NSArray res = new NSArray();
			if (ecritureSaisiePanel.getMyEcritureFormPanel().getMyTypeOperationModel().getSelectedEObject() != null) {
				EOTypeOperation top = (EOTypeOperation) ecritureSaisiePanel.getMyEcritureFormPanel().getMyTypeOperationModel().getSelectedEObject();
				res = top.origines();
				if (EOTypeOperation.TYPE_OPERATION_LUCRATIVE.equals(top.topType())) {
					EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("organ.orgLucrativite=1", null);
					res = EOQualifier.filteredArrayWithQualifier(res, qual);
				}
				EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("oriLibelle", EOSortOrdering.CompareAscending);
				res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(sort));
			}

			try {
				//origineModel.updateListWithData(res);

				//test
				//                origineLookupModel.setOrigines( res.vector());
				//                ecritureSaisiePanel.getMyEcritureFormPanel().getOrigineSelectButton().updateData();

				currentEcritureDico.put("origine", null);
				ecritureSaisiePanel.getMyEcritureFormPanel().updateDataOrigine();
				//                origineChanged();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IChequeFormPanelListener#origineChanged()
		 */
		public void origineChanged() {
			//            currentEcritureDico.put("origine", origineModel.getSelectedEObject());

		}

		public Color getColorExercice() {
			return (Color) myApp.getExerciceColorMap().get(getExercice().exeStat());
		}

		public EOEditingContext getEditingContext() {
			return EcritureSaisieCtrl.this.getEditingContext();
		}

		//        public IZLookupButtonListener getLookupButtonOrigineListener() {
		//            return origineLookupListener;
		//        }
		//
		//        public IZLookupButtonModel getLookupButtonOrigineModel() {
		//            return origineLookupModel;
		//        }

		public Action getActionSrch() {
			return actionSrchOrigine;
		}

	}

	private final ZEOSelectionDialog createOrigineDialog() {
		final EOTypeOperation top = (EOTypeOperation) ecritureSaisiePanel.getMyEcritureFormPanel().getMyTypeOperationModel().getSelectedEObject();

		EODisplayGroup dg2 = null;
		if (top != null) {
			if (dispayGroupsOrigine.get(top) == null) {
				final EODisplayGroup dg = new EODisplayGroup();
				NSArray res = null;
				if (EOTypeOperation.TYPE_OPERATION_LUCRATIVE.equals(top.topLibelle())) {
					res = EOsFinder.getOriginesLucratives(getEditingContext(), getExercice());
				}
				else if (EOTypeOperation.TYPE_OPERATION_CONVENTION.equals(top.topLibelle())) {
					res = EOsFinder.getOriginesConvRA(getEditingContext(), getExercice());
				}
				dg.setObjectArray(res);
				dispayGroupsOrigine.put(top, dg);
			}

			dg2 = (EODisplayGroup) dispayGroupsOrigine.get(top);

		}

		final Vector filterFields = new Vector(2, 0);
		filterFields.add("oriLibelle");

		final Vector myColsTmp = new Vector(2, 0);
		final ZEOTableModelColumn col1 = new ZEOTableModelColumn(dg2, "oriLibelle", "Libellé");
		myColsTmp.add(col1);

		ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMyDialog(), "Sélection d'une origine", dg2, myColsTmp, "Veuillez sélectionner une origine dans la liste", filterFields);
		dialog.setFilterPrefix("*");
		dialog.setFilterSuffix("*");
		dialog.setFilterInstruction(" like %@");
		return dialog;
	}

	private final class EcritureSaisieDetailListPanelListener implements org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getData()
		 */
		public NSArray getData() {
			if (currentEcriture == null) {
				return new NSArray();
			}
			return currentEcriture.detailEcriture();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#onTableChanged()
		 */
		public void onTableChanged() {
			if (MODE_NEW == modeSaisie) {
				ecritureSaisiePanel.getMyZPanelBalanceNew().updateData();
			}
			else if (MODE_RECTIF == modeSaisie) {
				ecritureRectifPanel.getMyZPanelBalance().updateData();
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#deleteRow()
		 */
		public void deleteRow() {
			ecritureDetailSupprimer();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#onSelectionChanged()
		 */
		public void onSelectionChanged() {
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getFactoryEcritureDetail()
		 */
		public FactoryEcritureDetail getFactoryEcritureDetail() {
			return myFactoryEcritureDetail;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getPlanComptables()
		 */
		public NSArray getPlanComptables() {
			return getPlanComptablesSelonJournal();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getGestions()
		 */
		public NSArray getGestions() {
			return gestions;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#addRow()
		 */
		public void addRow() {
			ecritureDetailAjouter();
		}

		/**
		 * @return Les ecritures emargeables avec l'ecriture passée en parametre.
		 */
		public NSArray getEcritureDetailPourEmargement(EOEcritureDetail detail) {
			NSArray res = detail.getEcritureDetailsEmargeables();
			return res;
		}

		public void analyseContratConv(String saisieUtil, EOEcritureDetail detail) {
			EOAccordsContrat res = null;
			try {
				setWaitCursor(true);
				if (!ZStringUtil.isEmpty(saisieUtil)) {
					EOQualifier qualUB = null;
					String conNumero = saisieUtil;
					String conExer = null;
					String conIndex = null;
					if (!ZStringUtil.isEmpty(conNumero)) {
						if (conNumero.contains("-")) {
							conExer = conNumero.substring(0, conNumero.indexOf("-"));
							conIndex = conNumero.substring(conNumero.indexOf("-") + 1);
						}
						else {
							conExer = conNumero;
						}
					}
					NSMutableArray quals = new NSMutableArray();
					if (!ZStringUtil.isEmpty(conExer)) {
						conExer = conExer.trim();
						if (conExer.length() != 4) {
							conExer = null;
						}
						else {
							try {
								Integer exer = Integer.valueOf(conExer);
								quals.addObject(new EOKeyValueQualifier(EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, exer));
							} catch (NumberFormatException e) {
								conExer = null;
							}

						}
					}
					if (!ZStringUtil.isEmpty(conIndex)) {
						conIndex = conIndex.trim();
						try {
							Integer index = Integer.valueOf(conIndex);
							quals.addObject(new EOKeyValueQualifier(EOAccordsContrat.CON_INDEX_KEY, EOQualifier.QualifierOperatorEqual, index));
						} catch (NumberFormatException e) {
							conIndex = null;
						}

					}
					NSArray res2 = EOAccordsContrat.fetchAll(getEditingContext(), new EOAndQualifier(quals), new NSArray(new Object[] {
							EOAccordsContrat.SORT_EXERCICE_ASC, EOAccordsContrat.SORT_CON_INDEX_ASC
					}));
					if (res2.count() == 1 && !ZStringUtil.isEmpty(conIndex) && ((EOAccordsContrat) res2.objectAtIndex(0)).conIndex().equals(Integer.valueOf(conIndex))) {
						res = (EOAccordsContrat) res2.objectAtIndex(0);
					}
					else {
						//on affiche une fenetre de recherche avec les conventions
						HashMap<String, Object> filtersForConventions = new HashMap<String, Object>();
						if (conExer != null) {
							try {
								Integer exer = Integer.valueOf(conExer);
								filtersForConventions.put(EOAccordsContrat.TO_EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, Integer.valueOf(conExer));
							} catch (NumberFormatException e) {
								conExer = null;
							}
						}
						res = getEcritureSaisiePanel().getMyEcritureSaisieDetailListPanel().selectContratConventionIndDialog(detail, filtersForConventions);
					}

					getFactoryEcritureDetail().ecritureDetailVideSetContratConvention(detail, res);
					getEcritureSaisiePanel().getMyEcritureSaisieDetailListPanel().getMyTableModel().fireTableDataChanged();

				}
				else {
					getFactoryEcritureDetail().ecritureDetailVideSetContratConvention(detail, res);
					getEcritureSaisiePanel().getMyEcritureSaisieDetailListPanel().getMyTableModel().fireTableDataChanged();
				}

			} finally {
				setWaitCursor(false);
			}

		}

	}

	private final class EcritureRectifDetailListListener implements EcritureDetailListPanel.IEcritureDetailListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureDetailListPanel.IEcritureDetailListPanelListener#getData()
		 */
		public NSArray getData() {
			return currentEcritureARectifier.detailEcriture();
		}

	}

	private final NSArray getPlanComptablesSelonJournal() {
		if (currentEcriture.typeJournal() == null) {
			return planComptables;
		}
		else if (EOTypeJournal.typeJournalExercice.equals(currentEcriture.typeJournal().tjoLibelle())) {
			return planComptablesJExercice;
		}
		else if (EOTypeJournal.typeJournalFinExercice.equals(currentEcriture.typeJournal().tjoLibelle())) {
			return planComptablesJFinExercice;
		}
		else if (EOTypeJournal.typeJournalBalanceEntree.equals(currentEcriture.typeJournal().tjoLibelle())) {
			return planComptablesJBe;
		}
		else if (EOTypeJournal.typeJournalGrandLivreValeursInactives.equals(currentEcriture.typeJournal().tjoLibelle())) {
			return planComptablesJGrandLivreValeursInactives;
		}
		else {
			return planComptables;
		}
	}

	public final NSArray getEcrituresForRecap() {
		return ecrituresForRecap;
	}

	private final class ZPanelBalanceEcritureProvider implements ZPanelBalance.IZPanelBalanceProvider {
		/**
		 * @return la valeur des debits.
		 */
		public BigDecimal getDebitValue() {
			BigDecimal val = new BigDecimal(0);
			if (currentEcriture != null) {
				NSArray tmp = currentEcriture.detailEcriture();
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (array_element.ecdDebit() != null) {
						val = val.add(array_element.ecdDebit());
					}
				}
			}
			return val;
		}

		public BigDecimal getCreditValue() {
			BigDecimal val = new BigDecimal(0);
			if (currentEcriture != null) {
				NSArray tmp = currentEcriture.detailEcriture();
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (array_element.ecdCredit() != null) {
						val = val.add(array_element.ecdCredit());
					}
				}
			}
			return val;
		}
	}

	private final class ZPanelBalanceEcritureRectifProvider implements ZPanelBalance.IZPanelBalanceProvider {
		/**
		 * @return la valeur des debits.
		 */
		public BigDecimal getDebitValue() {
			BigDecimal val = new BigDecimal(0);
			if (currentEcriture != null) {
				NSArray tmp = currentEcriture.detailEcriture();
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (array_element.ecdDebit() != null) {
						val = val.add(array_element.ecdDebit());
					}
				}
			}
			return val;
		}

		public BigDecimal getCreditValue() {
			BigDecimal val = new BigDecimal(0);
			if (currentEcriture != null) {
				NSArray tmp = currentEcriture.detailEcriture();
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (array_element.ecdCredit() != null) {
						val = val.add(array_element.ecdCredit());
					}
				}
			}
			return val;
		}
	}

	private final class EcritureRectifPanelListener implements EcritureRectifPanel.IEcritureRectifPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureRectifPanel.IEcritureRectifPanelListener#myActionAddEcritureDetail()
		 */
		public Action myActionAddEcritureDetail() {
			return actionAddEcritureDetail;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureRectifPanel.IEcritureRectifPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureRectifPanel.IEcritureRectifPanelListener#actionValider()
		 */
		public Action actionValider() {
			return actionValiderRectif;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureRectifPanel.IEcritureRectifPanelListener#myActionDeleteEcritureDetail()
		 */
		public Action myActionDeleteEcritureDetail() {
			return actionDeleteEcritureDetail;
		}

	}

	private final EOOrigine selectOrigineIndDialog() {
		setWaitCursor(true);
		EOOrigine res = null;
		final ZEOSelectionDialog origineDialog = createOrigineDialog();

		if (origineDialog.open() == ZEOSelectionDialog.MROK) {
			final NSArray selections = origineDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOOrigine) selections.objectAtIndex(0);
			}
			currentEcritureDico.put("origine", res);
			ecritureSaisiePanel.getMyEcritureFormPanel().updateDataOrigine();

		}
		setWaitCursor(false);
		return res;
	}

	private final class ActionSrchOrigine extends AbstractAction {

		public ActionSrchOrigine() {
			super("");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
		}

		public void actionPerformed(ActionEvent e) {
			selectOrigineIndDialog();

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		// non gere en auto
		return null;
	}

	public String title() {
		//      non gere en auto
		return null;
	}

	public IEcritureDetailSaisiePanel getEcritureSaisiePanel() {
		if (MODE_NEW == modeSaisie) {
			return ecritureSaisiePanel;
		}
		return ecritureRectifPanel;
	}

}
