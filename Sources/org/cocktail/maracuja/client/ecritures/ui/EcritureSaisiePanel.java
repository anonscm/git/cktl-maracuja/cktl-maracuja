/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;

/**
 * Panneau de saisie d'une écriture
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class EcritureSaisiePanel extends ZKarukeraPanel implements IEcritureDetailSaisiePanel {

	private static final long serialVersionUID = 1L;
	private EcritureFormPanel myEcritureFormPanel;
	private EcritureSaisieDetailListPanel myEcritureSaisieDetailListPanel;

	private IEcritureSaisiePanelListener myListener;
	private ZPanelBalance myZPanelBalanceNew;

	/**
     *
     */
	public EcritureSaisiePanel(IEcritureSaisiePanelListener listener, EcritureFormPanel.IEcritureFormPanelListener saisieFormListener, EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener saisieDetailListener, ZPanelBalance.IZPanelBalanceProvider balanceProvider) {
		super();
		myListener = listener;
		myEcritureFormPanel = new EcritureFormPanel(saisieFormListener);
		myEcritureSaisieDetailListPanel = new EcritureSaisieDetailListPanel(saisieDetailListener);
		myZPanelBalanceNew = new ZPanelBalance(balanceProvider, true);
	}

	/**
	 * @throws Exception
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		myEcritureFormPanel.setMyDialog(getMyDialog());
		myEcritureSaisieDetailListPanel.setMyDialog(getMyDialog());

		myEcritureFormPanel.initGUI();
		myEcritureSaisieDetailListPanel.initGUI();
		myZPanelBalanceNew.initGUI();

		this.setLayout(new BorderLayout());
		this.add(buildCenterPanelForSaisie(), BorderLayout.CENTER);
		//this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);

		//On bloque la saisie par défaut (deblocage lors de newEcriture).
		lockSaisieEcriture(true);
		lockSaisieEcritureDetail(true);
	}

	private final JToolBar buildToolBarEcritureDetail() {
		JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);
		//Initialiser les boutons à partir des actions
		myToolBar.add(myListener.myActionAddEcritureDetail());
		myToolBar.add(myListener.myActionDeleteEcritureDetail());
		return myToolBar;
	}

	private final JPanel buildCenterPanelForSaisie() {
		JPanel p = new JPanel(new BorderLayout());
		p.add(encloseInPanelWithTitle("Ecriture", null, ZConst.BG_COLOR_TITLE, buildTopPanel(), null, null), BorderLayout.NORTH);
		p.add(encloseInPanelWithTitle("Détails de l'écriture", null, ZConst.BG_COLOR_TITLE, buildEcritureDetailsPanel(), null, null), BorderLayout.CENTER);
		p.add(myZPanelBalanceNew, BorderLayout.SOUTH);
		return p;
	}

	private final JPanel buildTopPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.add(myEcritureFormPanel, BorderLayout.CENTER);
		p.add(buildRightPanel(), BorderLayout.EAST);
		return p;
	}

	private final JPanel buildBottomPanel() {
		ArrayList<Action> a = new ArrayList<Action>();
		a.add(myListener.actionValider());
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	private final JPanel buildEcritureDetailsPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.add(buildToolBarEcritureDetail(), BorderLayout.NORTH);
		p.add(myEcritureSaisieDetailListPanel, BorderLayout.CENTER);
		return p;
	}

	private final JPanel buildRightPanel() {
		ArrayList<Action> list = new ArrayList<Action>();
		list.add(myListener.actionDetail());
		JPanel p = new JPanel(new BorderLayout());
		p.setPreferredSize(new Dimension(150, 100));
		p.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		p.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		p.add(new JPanel(), BorderLayout.CENTER);
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myEcritureFormPanel.updateData();
		myEcritureSaisieDetailListPanel.updateData();
		myZPanelBalanceNew.updateData();
	}

	/**
	 * Bloque/debloque toute saisie.
	 * 
	 * @param locked
	 */
	public void lockAll(boolean locked) {
		myEcritureFormPanel.lockAll(locked);
		lockSaisieEcritureDetail(locked);
		lockSaisieEcriture(locked);
		myListener.actionDetail().setEnabled(!locked);
	}

	/**
	 * Interdit les changements des infos de l'ecriture en bloquant les elements de l'intyerface.
	 */
	public void lockSaisieEcriture(boolean locked) {
		myEcritureFormPanel.lockSaisieEcriture(locked);
	}

	/**
	 * Rend enebled/disabled les éléments de l'interface qui permettent de sasisir des details ecriture.
	 */
	public void lockSaisieEcritureDetail(boolean locked) {
		myListener.myActionDeleteEcritureDetail().setEnabled(!locked);
		myListener.myActionAddEcritureDetail().setEnabled(!locked);
		myEcritureSaisieDetailListPanel.lockSaisieEcritureDetail(locked);
	}

	public interface IEcritureSaisiePanelListener {

		/**
		 * @return
		 */
		public Action actionValider();

		/**
		 * @return
		 */
		public Action myActionDeleteEcritureDetail();

		/**
		 * @return
		 */
		public Action myActionAddEcritureDetail();

		/**
		 * @return
		 */
		public Action actionDetail();

		/**
		 * @return
		 */
		public Action actionClose();

	}

	public EcritureSaisieDetailListPanel getMyEcritureSaisieDetailListPanel() {
		return myEcritureSaisieDetailListPanel;
	}

	public EcritureFormPanel getMyEcritureFormPanel() {
		return myEcritureFormPanel;
	}

	public ZPanelBalance getMyZPanelBalanceNew() {
		return myZPanelBalanceNew;
	}
}
