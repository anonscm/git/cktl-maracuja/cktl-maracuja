/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ctrl.AccordsContratSelectCtrl;
import org.cocktail.maracuja.client.common.ctrl.EcritureDetailForEmargSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.PcoSelectDlg;
import org.cocktail.maracuja.client.common.ctrl.ZEOSelectionDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.factory.FactoryEcritureDetail;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.maracuja.client.metier.EOPlanComptableExer;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Saisie des lignes d'ecritures.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureSaisieDetailListPanel extends ZKarukeraPanel implements ZEOTable.ZEOTableListener, TableModelListener {
	private static final long serialVersionUID = 1L;
	private ZEOTable myEOTable;
	private ZEOTableModel myTableModel;
	private EODisplayGroup myDisplayGroup;

	private ArrayList<ZEOTableModelColumn> myCols;
	private IEcritureDetailListPanelListener myListener;

	private PcoSelectDlg planComptableSelectionDialog;
	private ZEOSelectionDialog gestionSelectionDialog;
	private EcritureDetailForEmargSelectDlg ecritureDetailForEmargementSelectionDialog;
	private EODisplayGroup dgGestion;

	private AccordsContratSelectCtrl accordsContratSelectCtrl;

	public EcritureSaisieDetailListPanel(IEcritureDetailListPanelListener listener) {
		super();
		myListener = listener;
		accordsContratSelectCtrl = new AccordsContratSelectCtrl(getEditingContext());
	}

	private PcoSelectDlg createPlanComptableSelectionDialog() {
		return PcoSelectDlg.createPcoNumSelectionDialog(getMyDialog());
	}

	private EcritureDetailForEmargSelectDlg createEcritureDetailForEmargementSelectionDialog() {
		return EcritureDetailForEmargSelectDlg.createSelectionDialog(getMyDialog());
	}

	private ZEOSelectionDialog createGestionSelectionDialog() {
		Vector<ZEOTableModelColumn> myColsTmp = new Vector<ZEOTableModelColumn>(2, 0);
		ZEOTableModelColumn col1 = new ZEOTableModelColumn(dgGestion, EOGestion.GES_CODE_KEY, "Code gestion");
		myColsTmp.add(col1);
		ZEOSelectionDialog dialog = new ZEOSelectionDialog(getMyDialog(), "Sélection d'un code gestion", dgGestion, myColsTmp, "Veuillez sélectionner un code gestion dans la liste", null);
		return dialog;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		myDisplayGroup = new EODisplayGroup();
		dgGestion = new EODisplayGroup();
		dgGestion.setObjectArray(myListener.getGestions());

		planComptableSelectionDialog = createPlanComptableSelectionDialog();
		planComptableSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		ecritureDetailForEmargementSelectionDialog = createEcritureDetailForEmargementSelectionDialog();
		ecritureDetailForEmargementSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		gestionSelectionDialog = createGestionSelectionDialog();
		gestionSelectionDialog.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		initTableModel();
		initTable();
		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(myEOTable), BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() {
		NSArray res = myListener.getData();
		myDisplayGroup.setObjectArray(res);
		myEOTable.updateData();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		myListener.onTableChanged();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.cocktail.maracuja.client.zutil.wo.table.ZEOTable.ZEOTableListener#onDbClick()
	 */
	public void onDbClick() {
	}

	/**
	 * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
	 */
	public void onSelectionChanged() {
		myListener.onSelectionChanged();
	}

	public EOEcritureDetail selectedEcritureDetail() {
		return (EOEcritureDetail) myEOTable.getSelectedObject();
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableModel);
		myEOTable.addListener(this);
		//Gestion du clavier
		myEOTable.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent evt) {
				if (!evt.isConsumed())
					doKeyPressed(evt);
			}
		});
	}

	/**
	 * Initialise le modeele le la table à afficher.
	 */
	private void initTableModel() {
		myCols = new ArrayList<ZEOTableModelColumn>(0);

		ZEOTableModelColumn colEcdIndex = new ZEOTableModelColumn(myDisplayGroup, "ecdIndex", "#", 29);
		colEcdIndex.setAlignment(SwingConstants.LEFT);
		colEcdIndex.setEditable(true);
		colEcdIndex.setFormatEdit(ZConst.FORMAT_EDIT_NUMBER);
		colEcdIndex.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));
		colEcdIndex.setColumnClass(Integer.class);

		ZEOTableModelColumn colEcdPlancomptablePcoNum = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoNum", "Imputation", 68);
		colEcdPlancomptablePcoNum.setAlignment(SwingConstants.LEFT);
		colEcdPlancomptablePcoNum.setEditable(true);
		colEcdPlancomptablePcoNum.setMyModifier(new pcoNumModifier());
		colEcdPlancomptablePcoNum.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colEcdPlancomptablePcoLibelle = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoLibelle", "Libelle Imputation", 263);
		colEcdPlancomptablePcoLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdGestionGesCode = new ZEOTableModelColumn(myDisplayGroup, "gestion.gesCode", "Code gestion", 83);
		colEcdGestionGesCode.setAlignment(SwingConstants.CENTER);
		colEcdGestionGesCode.setEditable(true);
		colEcdGestionGesCode.setMyModifier(new gesCodeModifier());
		colEcdGestionGesCode.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colEcdLibelle = new ZEOTableModelColumn(myDisplayGroup, "ecdLibelle", "Libellé", 200);
		colEcdLibelle.setAlignment(SwingConstants.LEFT);
		colEcdLibelle.setEditable(true);
		colEcdLibelle.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colEcdDebit = new ZEOTableModelColumn(myDisplayGroup, "ecdDebit", "Débit", 90);
		colEcdDebit.setAlignment(SwingConstants.RIGHT);
		colEcdDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdDebit.setEditable(true);
		colEcdDebit.setMyModifier(new ecdDebitModifier());
		colEcdDebit.setTableCellEditor(new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), ZConst.FORMAT_EDIT_NUMBER));
		colEcdDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdCredit = new ZEOTableModelColumn(myDisplayGroup, "ecdCredit", "Crédit", 90);
		colEcdCredit.setAlignment(SwingConstants.RIGHT);
		colEcdCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdCredit.setEditable(true);
		colEcdCredit.setMyModifier(new ecdCreditModifier());
		colEcdCredit.setTableCellEditor(new ZEOTableModelColumn.ZEONumFieldTableCellEditor(new JTextField(), ZConst.FORMAT_EDIT_NUMBER));
		colEcdCredit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdEmargeEcr = new ZEOTableModelColumn(myDisplayGroup, EOEcritureDetail.ECD_FOR_EMARGEMENT_KEY + "." + EOEcritureDetail.ECR_NUMERO_AND_ECD_INDEX_KEY, "Emarger avec", 100);
		colEcdEmargeEcr.setAlignment(SwingConstants.CENTER);
		colEcdEmargeEcr.setEditable(true);
		colEcdEmargeEcr.setMyModifier(new EcrEmargementModifier());
		colEcdEmargeEcr.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		ZEOTableModelColumn colConvention = new ZEOTableModelColumn(myDisplayGroup, EOEcritureDetail.TO_ACCORDS_CONTRAT_KEY + "." + EOAccordsContrat.NUMERO_KEY, "Convention", 100);
		colConvention.setAlignment(SwingConstants.CENTER);
		colConvention.setEditable(true);
		colConvention.setMyModifier(new ContratConvModifier());
		colConvention.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(new JTextField()));

		myCols.add(colEcdIndex);
		myCols.add(colEcdPlancomptablePcoNum);
		myCols.add(colEcdPlancomptablePcoLibelle);
		myCols.add(colEcdGestionGesCode);
		myCols.add(colEcdLibelle);
		myCols.add(colEcdDebit);
		myCols.add(colEcdCredit);
		myCols.add(colEcdEmargeEcr);
		myCols.add(colConvention);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableModel.addTableModelListener(this);
		//permet notammen,t d'ajouter la possibilité de controler l'autorisation de modifier au niveau du row
		myTableModel.setMyDelegate(new ZEOTableModelDelegate());
	}

	/**
	 * Insère un nouvelle objet EcritureDetail dans le displayGroup (et donc dans la table).
	 * 
	 * @param detail
	 */
	public void ecritureDetailAjouterInDg(EOEcritureDetail detail) {
		ZLogger.debug(myDisplayGroup);
		ZLogger.debug(myDisplayGroup.displayedObjects());

		int row = myDisplayGroup.displayedObjects().count();
		myDisplayGroup.insertObjectAtIndex(detail, row);
		myDisplayGroup.setSelectedObject(detail);

		myTableModel.updateInnerRowCount();
		myTableModel.fireTableDataChanged();

		//CHECK Ne pas utiliser cette methode dans le cas ou la table est modifiable
		//	    myEOTable.updateData();

		myEOTable.requestFocusInWindow();
		if (myEOTable.editCellAt(row, 0)) {
			myEOTable.changeSelection(row, 0, false, false);
		}

	}

	/**
	 * Down key moves down one row, appending a row if neccessary F4 key appends a row Up key moves up one row, pre-appending a row if neccessary F5
	 * key inserts a row before the currently selected row F6 & Delete key delete the current row F7 copies and appends to the bottom the current row
	 * F8 selects the first row
	 **/
	private void doKeyPressed(KeyEvent evt) {
		//	        int i, j;
		switch (evt.getKeyCode()) {
		case KeyEvent.VK_DOWN:
			if (myEOTable.getSelectedRow() != myEOTable.getRowCount() - 1) {
				break;
			}
			myListener.addRow();
			evt.consume();
			break;

		case KeyEvent.VK_INSERT:
			if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK) {
				myListener.addRow();
				evt.consume();
			}
			break;
		case KeyEvent.VK_UP:
			if (myEOTable.getSelectedRow() != 0) {
				break;
			}
			myEOTable.setRowSelectionInterval(0, 0);
		case KeyEvent.VK_DELETE:
			if ((evt.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK) {
				myListener.deleteRow();
				evt.consume();
			}
			break;
		default:
			break;
		}
	}

	private class ZEOTableModelDelegate implements ZEOTableModel.IZEOTableModelDelegate {
		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModel.IZEOTableModelDelegate#isCellEditable(int, int)
		 */
		public boolean isCellEditable(int row, int col) {
			return true;
		}
	}

	public interface IEcritureDetailListPanelListener {
		public NSArray getData();

		public void onTableChanged();

		public void deleteRow();

		public void onSelectionChanged();

		public FactoryEcritureDetail getFactoryEcritureDetail();

		public NSArray getPlanComptables();

		public NSArray getGestions();

		public void addRow();

		public NSArray getEcritureDetailPourEmargement(EOEcritureDetail detail);

		public void analyseContratConv(String value, EOEcritureDetail tmp);
	}

	public void lockSaisieEcritureDetail(boolean locked) {
		myEOTable.setEnabled(!locked);
	}

	private void analysePcoNum(String pconum, EOEcritureDetail detail) {
		EOPlanComptable res = null;
		//int leRow = myEOTable.getSelectedRow();
		EOPlanComptableExer restmp = EOPlanComptableExerFinder.findPlanComptableExerForPcoNumInList(myListener.getPlanComptables(), pconum);
		if (restmp == null) {
			res = selectPlanComptableIndDialog(pconum);
		}
		else {
			res = restmp.planComptable();
		}
		myListener.getFactoryEcritureDetail().ecritureDetailVideSetPlanComptable(detail, res);
		if (res != null) {
			//    		  On arrete l'editon
			if (myEOTable.isEditing()) {
				myEOTable.getCellEditor().cancelCellEditing();
			}
		}

		myTableModel.fireTableDataChanged();
	}

	private void analyseGesCode(String gesCode, EOEcritureDetail detail) {
		EOGestion res = null;
		final NSArray tmp = EOQualifier.filteredArrayWithQualifier(myListener.getGestions(), EOQualifier.qualifierWithQualifierFormat("gesCode like '" + gesCode + "*'", null));
		if (tmp.count() == 1) {
			//pas de doute sur le choix de la gestion
			res = (EOGestion) tmp.objectAtIndex(0);
		}
		else {
			//on affiche une fenetre de recherche de PlanComptable
			res = selectGestionIndDialog();
		}
		myListener.getFactoryEcritureDetail().ecritureDetailVideSetGestion(detail, res);
		myTableModel.fireTableDataChanged();
	}

	private void analyseEcrEmarge(String ecrNumeroComplex, EOEcritureDetail detail) {
		EOEcritureDetail res = null;
		try {
			setWaitCursor(true);
			if (!ZStringUtil.isEmpty(ecrNumeroComplex)) {
				final NSArray tmp = myListener.getEcritureDetailPourEmargement(detail);
				String ecrNumero = ecrNumeroComplex;
				String ecdIndex = null;
				if (ecrNumeroComplex.contains("/")) {
					ecrNumero = ecrNumeroComplex.substring(0, ecrNumeroComplex.indexOf("/"));
					ecdIndex = ecrNumeroComplex.substring(ecrNumeroComplex.indexOf("/") + 1);
					System.out.println(ecrNumero);
					System.out.println(ecdIndex);
				}
				NSMutableArray quals = new NSMutableArray();
				if (!ZStringUtil.isEmpty(ecrNumero)) {
					quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.ECRITURE_KEY + "." + EOEcriture.ECR_NUMERO_KEY, EOQualifier.QualifierOperatorEqual, ecrNumero));
				}
				if (!ZStringUtil.isEmpty(ecdIndex)) {
					quals.addObject(new EOKeyValueQualifier(EOEcritureDetail.ECD_INDEX_KEY, EOQualifier.QualifierOperatorEqual, ecdIndex));
				}
				NSArray res2 = EOQualifier.filteredArrayWithQualifier(tmp, new EOAndQualifier(quals));
				if (res2.count() == 1 && ((EOEcritureDetail) res2.objectAtIndex(0)).ecrNumeroAndEcdIndex().startsWith(ecrNumeroComplex)) {
					res = (EOEcritureDetail) res2.objectAtIndex(0);
				}
				else {
					//on affiche une fenetre de recherche
					res = selectEcritureDetailForEmargementIndDialog(detail, tmp);
				}

				myListener.getFactoryEcritureDetail().ecritureDetailVideSetEcrDetailForEmargement(detail, res);
				myTableModel.fireTableDataChanged();

			}
			else {
				myListener.getFactoryEcritureDetail().ecritureDetailVideSetEcrDetailForEmargement(detail, res);
				myTableModel.fireTableDataChanged();
			}

		} finally {
			setWaitCursor(false);
		}

	}

	private EOEcritureDetail selectEcritureDetailForEmargementIndDialog(EOEcritureDetail detail, NSArray tmp) {
		setWaitCursor(true);
		EOEcritureDetail res = null;
		if (ecritureDetailForEmargementSelectionDialog.open(tmp) == ZEOSelectionDialog.MROK) {
			NSArray selections = ecritureDetailForEmargementSelectionDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOEcritureDetail) selections.objectAtIndex(0);
			}
		}
		setWaitCursor(false);
		return res;
	}

	private EOPlanComptable selectPlanComptableIndDialog(String filter) {
		setWaitCursor(true);
		EOPlanComptable res = null;
		planComptableSelectionDialog.getFilterTextField().setText(filter);
		//		if (planComptableSelectionDialog.open() == ZEOSelectionDialog.MROK) {
		if (planComptableSelectionDialog.open(myListener.getPlanComptables()) == ZEOSelectionDialog.MROK) {
			res = planComptableSelectionDialog.getSelection();
		}
		setWaitCursor(false);
		return res;
	}

	private EOGestion selectGestionIndDialog() {
		setWaitCursor(true);
		EOGestion res = null;
		if (gestionSelectionDialog.open() == ZEOSelectionDialog.MROK) {
			NSArray selections = gestionSelectionDialog.getSelectedEOObjects();

			if ((selections == null) || (selections.count() == 0)) {
				res = null;
			}
			else {
				res = (EOGestion) selections.objectAtIndex(0);
			}
		}
		setWaitCursor(false);
		return res;
	}

	private class pcoNumModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.selectedObject();
			ZLogger.debug("Objet en cours ", tmp);
			analysePcoNum((String) value, tmp);
		}

	}

	private class EcrEmargementModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.selectedObject();
			ZLogger.debug("Objet en cours ", tmp);
			analyseEcrEmarge((String) value, tmp);
		}

	}

	private class ContratConvModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.selectedObject();
			ZLogger.debug("Objet en cours ", tmp);
			myListener.analyseContratConv((String) value, tmp);
		}

	}

	private class gesCodeModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.selectedObject();
			analyseGesCode((String) value, tmp);
		}

	}

	private class ecdDebitModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			//            EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.selectedObject();
			EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.displayedObjects().objectAtIndex(row);

			tmp.setEcdSens(ZConst.SENS_DEBIT);
			if (value instanceof Number) {
				myListener.getFactoryEcritureDetail().ecritureDetailVideSetMontant(tmp, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		}

	}

	private class ecdCreditModifier implements ZEOTableModelColumn.Modifier {

		/**
		 * @see org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		 */
		public void setValueAtRow(Object value, int row) {
			//            EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.selectedObject();
			EOEcritureDetail tmp = (EOEcritureDetail) myDisplayGroup.displayedObjects().objectAtIndex(row);
			tmp.setEcdSens(ZConst.SENS_CREDIT);
			if (value instanceof Number) {
				myListener.getFactoryEcritureDetail().ecritureDetailVideSetMontant(tmp, new BigDecimal(((Number) value).doubleValue()).setScale(2, BigDecimal.ROUND_HALF_UP));
			}
		}

	}

	/**
	 * Annule la saisie en cours dans la celule si celle-ci est en mode édition.
	 */
	public void cancelCellEditing() {
		myEOTable.cancelCellEditing();
	}

	public boolean isEditing() {
		return (myEOTable != null && myEOTable.isEditing());
	}

	/**
	 * Termine l'édition en cours dans la cellule éventuellement en mode edition.
	 * 
	 * @return True si l'édition a bien été validée.
	 */
	public boolean stopCellEditing() {
		return myEOTable.stopCellEditing();
	}

	public final ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public final ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

	public EOAccordsContrat selectContratConventionIndDialog(EOEcritureDetail detail, HashMap<String, Object> filters) {
		setWaitCursor(true);
		EOAccordsContrat res = null;
		accordsContratSelectCtrl.getFilters().putAll(filters);
		int mr = accordsContratSelectCtrl.openDialog(getMyDialog(), true);
		if (mr == ZEOSelectionDialog.MROK) {
			res = accordsContratSelectCtrl.getSelectedAccordsContrat();
		}
		setWaitCursor(false);
		return res;
	}

}
