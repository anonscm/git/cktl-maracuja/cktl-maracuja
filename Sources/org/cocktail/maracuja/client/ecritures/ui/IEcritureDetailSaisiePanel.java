package org.cocktail.maracuja.client.ecritures.ui;

public interface IEcritureDetailSaisiePanel {
	public EcritureSaisieDetailListPanel getMyEcritureSaisieDetailListPanel();
}
