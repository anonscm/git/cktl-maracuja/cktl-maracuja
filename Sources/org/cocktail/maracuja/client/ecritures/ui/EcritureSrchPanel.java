/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureSrchPanel extends ZKarukeraPanel {
	private IEcritureSrchPanelListener myListener;

	private EcritureSrchFilterPanel filterPanel;
	private EcritureListPanel ecritureListPanel;
	private EcritureDetailListPanel ecritureDetailListPanel;
	private ZPanelBalance myZPanelBalanceNew;

	/**
	 * @param editingContext
	 */
	public EcritureSrchPanel(IEcritureSrchPanelListener listener) {
		super();
		myListener = listener;

		filterPanel = new EcritureSrchFilterPanel(new EcritureSrchFilterPanelListener());
		ecritureListPanel = new EcritureListPanel(new EcritureListPanelListener());
		ecritureDetailListPanel = new EcritureDetailListPanel(new EcritureDetailListPanelListener());

		myZPanelBalanceNew = new ZPanelBalance(listener.getBalanceProvider());

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		filterPanel.setMyDialog(getMyDialog());
		filterPanel.initGUI();
		ecritureListPanel.initGUI();
		ecritureDetailListPanel.initGUI();
		myZPanelBalanceNew.initGUI();

		JPanel bpanel = new JPanel(new BorderLayout());
		bpanel.add(ecritureDetailListPanel, BorderLayout.CENTER);
		bpanel.add(myZPanelBalanceNew, BorderLayout.SOUTH);

		this.setLayout(new BorderLayout());
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.add(encloseInPanelWithTitle("Filtres de recherche", null, ZConst.BG_COLOR_TITLE, filterPanel, null, null), BorderLayout.NORTH);
		tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
				encloseInPanelWithTitle("Ecritures", null, ZConst.BG_COLOR_TITLE, ecritureListPanel, null, null),
				encloseInPanelWithTitle("Détails", null, ZConst.BG_COLOR_TITLE, bpanel, null, null)), BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);
		this.add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		filterPanel.updateData();
		ecritureListPanel.updateData();
		ecritureDetailListPanel.updateData();
		myZPanelBalanceNew.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.actionNew());
		list.add(myListener.actionAnnulerEcriture());
		list.add(myListener.actionRectifier());
		list.add(myListener.getActionImprimer());
		//        list.add(myListener.getActionImprimerAll());

		ArrayList comps = ZKarukeraPanel.getButtonListFromActionList(list);
		comps.add(buildVerticalSeparator(40));
		comps.add(getButtonFromAction(myListener.actionVoirEmargements()));
		comps.add(getButtonFromAction(myListener.actionNewEmargement()));
		comps.add(getButtonFromAction(myListener.actionExportExcel2()));
		comps.add(getButtonFromAction(myListener.actionCheckResteEmarger()));

		tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	private JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	public EOEcriture getSelectedEcriture() {
		return ecritureListPanel.selectedObject();
	}

	public NSArray getSelectedEcritures() {
		return ecritureListPanel.getSelectedEcritures();
	}

	public interface IEcritureSrchPanelListener {
		public NSArray getEcritureDetails();

		/**
		 * @return
		 */
		public Action actionExportExcel2();

		public ZPanelBalance.IZPanelBalanceProvider getBalanceProvider();

		public Action actionClose();

		public Action actionRectifier();

		public Action actionAnnulerEcriture();

		public Action getActionImprimer();

		//        public Action getActionImprimerAll();
		public Action actionNew();

		public Action actionNewEmargement();

		/**
		 * @return Les ecritures à afficher
		 */
		public NSArray getEcritures();

		/**
		 * @return un dictioniare contenant les filtres
		 */
		public HashMap getFilters();

		/**
		 * @return
		 */
		public Action actionSrch();

		public Action actionVoirEmargements();

		public Action actionCheckResteEmarger();

		/**
         *
         */
		public void selectionChanged();

		/**
		 * @return
		 */
		public ZEOComboBoxModel getTypeJournalModel();

	}

	private final class EcritureDetailListPanelListener implements EcritureDetailListPanel.IEcritureDetailListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureDetailListPanel.IEcritureDetailListPanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getEcritureDetails();
		}

	}

	private final class EcritureListPanelListener implements EcritureListPanel.IEcritureViewListPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureListPanel.IEcritureViewListPanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			myListener.selectionChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureListPanel.IEcritureViewListPanelListener#getEcritures()
		 */
		public NSArray getEcritures() {
			return myListener.getEcritures();
		}

	}

	private final class EcritureSrchFilterPanelListener implements EcritureSrchFilterPanel.IEcritureSrchFilterPanel {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchFilterPanel.IEcritureSrchFilterPanel#getActionSrch()
		 */
		public Action getActionSrch() {
			return myListener.actionSrch();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchFilterPanel.IEcritureSrchFilterPanel#getFilters()
		 */
		public HashMap getFilters() {
			return myListener.getFilters();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchFilterPanel.IEcritureSrchFilterPanel#getTypeJournalModel()
		 */
		public ZEOComboBoxModel getTypeJournalModel() {
			return myListener.getTypeJournalModel();
		}

	}

	public EcritureListPanel getEcritureListPanel() {
		return ecritureListPanel;
	}

	public EcritureDetailListPanel getEcritureDetailListPanel() {
		return ecritureDetailListPanel;
	}

	/**
	 * @return
	 */
	public ZPanelBalance getMyZPanelBalanceNew() {
		return myZPanelBalanceNew;
	}
}
