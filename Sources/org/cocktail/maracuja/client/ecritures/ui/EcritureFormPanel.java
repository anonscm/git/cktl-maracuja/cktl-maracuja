/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZLabelComboBoxField;
import org.cocktail.maracuja.client.common.ui.ZLabelField;
import org.cocktail.maracuja.client.common.ui.ZLabelTextField;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.forms.ZLabeledComponent;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;




/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureFormPanel extends ZKarukeraPanel {
    private ZEOComboBoxModel myTypeOperationModel;
    private JComboBox myTypeOperationField;
//    private JComboBox myOrigineField;
    
    private ZEOComboBoxModel myTypeJournalModel;
    private ZLabelComboBoxField myTypeJournalField;    
    
//    private ZEOComboBoxModel myComptabiliteModel;
//    private ZLabelComboBoxField myComptabiliteField;
    
    private ZLabelTextField myDateEcriture;
    private ZLabelTextField myNumEcriture;
//    private ZLabelTextField myNumSacd;
    
    private ZLabelTextField myLibelleEcriture;
    private ZLabelTextField myCommentaireEcriture;
    private final int DEFAULT_LABEL_WIDTH=100;
    
    private JLabel origineLib=new JLabel();
    
    
    private Box boxOrigine;
    private IEcritureFormPanelListener myListener;
//    private ZLookupButton origineSelectButton;
    private JButton origineSrchButton;
    
    /**
     * 
     */
    public EcritureFormPanel(IEcritureFormPanelListener listener) {
        super();
        myListener = listener;
    }
    

    
    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        
        ZLogger.debug("typeJournals");
        ZLogger.debug(myListener.getTypeJournals());
        
        
        try {
            myTypeOperationModel = new ZEOComboBoxModel(myListener.getTypesOperations(), "topLibelle", null,null);
            myTypeOperationModel.setSelectedEObject( EOsFinder.fetchObject(myListener.getEditingContext(), EOTypeOperation.ENTITY_NAME, "topLibelle=%@", new NSArray(EOTypeOperation.TYPE_OPERATION_GENERIQUE), null,false)  );
            myTypeJournalModel =  new ZEOComboBoxModel(myListener.getTypeJournals(), "tjoLibelle", null,null);
//            myComptabiliteModel =  new ZEOComboBoxModel(myListener.getcomptabilites(), "comLibelle", null,null);
        } catch (Exception e) {
            showErrorDialog(e);
        }        
        setLayout(new BorderLayout());

//        myComptabiliteField = new ZLabelComboBoxField("Comptabilité", ZLabelField.LABELONLEFT, DEFAULT_LABEL_WIDTH, myComptabiliteModel);
//        myComptabiliteField.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
//        ((JComboBox)myComptabiliteField.getContentComponent()).addActionListener((new ComptabiliteListener()));
        
        
        myTypeJournalField = new ZLabelComboBoxField("Journal", ZLabelField.LABELONLEFT, DEFAULT_LABEL_WIDTH, myTypeJournalModel);
        myTypeJournalField.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
        ((JComboBox)myTypeJournalField.getContentComponent()).addActionListener((new TypeJournalListener()));
        
        myTypeOperationField = new JComboBox(myTypeOperationModel);
//        myTypeOperationField.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
        myTypeOperationField.addActionListener((new TypeOperationListener()));
        
        myLibelleEcriture = new ZLabelTextField("Libellé", new LibelleEcritureModel()  );
        myLibelleEcriture.getMyTexfield().setColumns(35);
        myLibelleEcriture.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
        
        myCommentaireEcriture = new ZLabelTextField("Commentaire", new PostitEcritureModel()  );
        myCommentaireEcriture.getMyTexfield().setColumns(35);
        myCommentaireEcriture.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
        
        
        
//        myOrigineField = new JComboBox(myListener.getOrigineModel() ); 
//        myOrigineField.addActionListener( new OrigineListener() );
        
        boxOrigine = Box.createHorizontalBox();
        boxOrigine.add(new ZLabeledComponent("Type Opération", myTypeOperationField, ZLabeledComponent.LABELONLEFT, -1 ) );
//        boxOrigine.add(new ZLabeledComponent("Origine", myOrigineField, ZLabeledComponent.LABELONTOP, -1 ) );
        boxOrigine.add(Box.createHorizontalGlue());
        
        
//        origineSelectButton = new ZLookupButton(myListener.getLookupButtonOrigineModel(), myListener.getLookupButtonOrigineListener());
//        origineSelectButton.initGUI();
        
        
        origineSrchButton = new JButton(myListener.getActionSrch());
        
        
        
        Box col1 = Box.createVerticalBox();
//        col1.add(buildLine(myComptabiliteField));
        col1.add(buildLine(myTypeJournalField));
//        col1.add(buildLine(myTypeOperationField));
        col1.add(buildLine(myLibelleEcriture));
        col1.add(buildLine(myCommentaireEcriture));
        col1.add(new JPanel(new BorderLayout()));       
        
        
        Box boxH = Box.createHorizontalBox();
        boxH.add(col1);
        boxH.add(buildColonne2());
        boxH.add(Box.createGlue());
        
        final JPanel tmpP = new JPanel(new BorderLayout());
        tmpP.add(boxH, BorderLayout.WEST);
        tmpP.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        Box box2 = Box.createVerticalBox();
        box2.add(boxH);
        box2.add( buildLine(new Component[]{new JLabel("Origine"), Box.createHorizontalStrut(20),origineSrchButton, Box.createHorizontalStrut(20),origineLib})  );
        
        this.add(box2, BorderLayout.WEST);
        this.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
    }
    
    
    
    private Box buildColonne2() {
        myDateEcriture = new ZLabelTextField("Date", new DateEcritureModel());
        myDateEcriture.getMyTexfield().setColumns(10);
        myDateEcriture.getMyTexfield().setEditable(false);
        myDateEcriture.setUIReadOnly();
        myDateEcriture.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
        myDateEcriture.setMyFormat(ZConst.FORMAT_DATESHORT);
        myDateEcriture.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
        
        myNumEcriture = new ZLabelTextField("Numéro", new NumEcritureModel());
        myNumEcriture.getMyTexfield().setColumns(10);
        myNumEcriture.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
        myNumEcriture.getMyTexfield().setEditable(false);
        myNumEcriture.setUIReadOnly();
        myNumEcriture.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));
        myNumEcriture.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
        
        
       
//        myNumSacd = new ZLabelTextField("Numéro SACD", new NumSacdModel());
//        myNumSacd.getMyTexfield().setColumns(10);
//        myNumSacd.getMyTexfield().setHorizontalAlignment(JTextField.RIGHT);
//        myNumSacd.getMyTexfield().setEditable(false);
//        myNumSacd.setUIReadOnly();
//        myNumSacd.getMyLabel().setPreferredSize(new Dimension(DEFAULT_LABEL_WIDTH,1));       
//        myNumSacd.getMyTexfield().setHorizontalAlignment(JTextField.CENTER);
                
        
        Box col1 = Box.createVerticalBox();
        
        
        
        
        col1.add(new JPanel(new BorderLayout()));
        col1.add(buildLine(myDateEcriture));
        col1.add(buildLine(myNumEcriture));
        col1.add(boxOrigine);
        
          
        
        
        
//        col1.add(buildLine(myNumSacd));
        
//        JPanel pl = new JPanel(new BorderLayout());
//        pl.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
////        pl.add(new JButton(myListener.actionSaisirDetail()), BorderLayout.EAST);
//        col1.add(pl);
        return col1;
    }

    
//    private JPanel buildRightPanel() {
//        JPanel p = new JPanel(new BorderLayout());
//        return p;
//    }
    
    
    
    
    
    /**
     * On charge inititlise les champs avec les valeurs du dico
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myCommentaireEcriture.updateData();
        myLibelleEcriture.updateData();
        myNumEcriture.updateData();
        myDateEcriture.updateData();
//        myNumSacd.updateData();
        
        
        System.out.println("EcritureFormPanel.updateData()");
        System.out.println(myListener.getCurrentEcritureDico());
        System.out.println();
        
        
        ActionListener[] tmp = myTypeOperationField.getActionListeners();
        for (int i = 0; i < tmp.length; i++) {
            ActionListener listener = tmp[i];
            myTypeOperationField.removeActionListener(listener);
        }        
        
        
        if (myListener.getCurrentEcritureDico().get("typeOperation")!=null) {
            myTypeOperationModel.setSelectedEObject((EOEnterpriseObject)myListener.getCurrentEcritureDico().get("typeOperation"));
        }
//        if (myListener.getCurrentEcritureDico().get("origine")!=null) {
////            System.out.println("Origine : "+myListener.getCurrentEcritureDico().get("origine"));
//            myListener.getOrigineModel().setSelectedEObject((EOEnterpriseObject)myListener.getCurrentEcritureDico().get("origine"));
////            System.out.println("Origine : "+myListener.getCurrentEcritureDico().get("origine"));
//        }

        
        if (myListener.getCurrentEcritureDico().get("typeJournal")!=null) {
            myTypeJournalModel.setSelectedEObject((EOEnterpriseObject)myListener.getCurrentEcritureDico().get("typeJournal"));
        }        
//        if (myListener.getCurrentEcritureDico().get("comptabilite")!=null) {
//            myComptabiliteModel.setSelectedEObject((EOEnterpriseObject)myListener.getCurrentEcritureDico().get("comptabilite"));
//        }         
        
        for (int i = 0; i < tmp.length; i++) {
            ActionListener listener = tmp[i];
            myTypeOperationField.addActionListener(listener);
        }
        
        updateDataOrigine();        
        
        myListener.getCurrentEcritureDico().put("typeOperation", myTypeOperationModel.getSelectedEObject());
        
        //test
//        myListener.getCurrentEcritureDico().put("origine", myListener.getOrigineModel().getSelectedEObject());
        
        myListener.getCurrentEcritureDico().put("typeJournal", myTypeJournalModel.getSelectedEObject());
        
        
        myDateEcriture.getMyTexfield().setBackground(myListener.getColorExercice());
        
        
    }
    
    
    public interface IEcritureFormPanelListener {
        public NSArray getTypesOperations();
        public Action getActionSrch();
//        public IZLookupButtonListener getLookupButtonOrigineListener();
//        public IZLookupButtonModel getLookupButtonOrigineModel();
        public EOEditingContext getEditingContext();
        /**
         * @return
         */
//        public ZEOComboBoxModel getOrigineModel();
        /**
         * @return
         */
//        public NSArray getcomptabilites();
        /**
         * @return
         */
        public NSArray getTypeJournals();
        /**
         * @return Un dictionnaire contenant les informations de l'écriture.
         */
        public HashMap getCurrentEcritureDico();
        
//        public AbstractAction actionSaisirDetail();
        
        
        
        public void typeOperationChanged();

        /**
         * 
         */
        public void origineChanged();        
        
        public Color getColorExercice() ;
        
    }
    
    
    
    
    /**
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    private class LibelleEcritureModel implements ZLabelTextField.IZLabelTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getCurrentEcritureDico().get("ecrLibelle") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getCurrentEcritureDico().put("ecrLibelle", value);
        }
        
    }
    
    private class PostitEcritureModel implements ZLabelTextField.IZLabelTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getCurrentEcritureDico().get("ecrPostit") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getCurrentEcritureDico().put("ecrPostit", value);
        }
        
    }
        
    private class DateEcritureModel implements ZLabelTextField.IZLabelTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getCurrentEcritureDico().get("ecrDateSaisie") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
        }
        
    }    
    
    private class NumEcritureModel implements ZLabelTextField.IZLabelTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getCurrentEcritureDico().get("ecrNumero") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
        }
        
    }     

//    private class NumSacdModel implements ZLabelTextField.IZLabelTextFieldModel {
//
//        /**
//         * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
//         */
//        public Object getValue() {
//            return "";
//        }
//
//        /**
//         * @see org.cocktail.maracuja.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
//         */
//        public void setValue(Object value) {
//        }
//        
//    }      

    /**
     * 
     */
    public void lockSaisieEcriture(boolean locked) {
//        myComptabiliteField.getContentComponent().setEnabled(!locked);
        myTypeJournalField.getContentComponent().setEnabled(!locked);
        myTypeOperationField.setEnabled(!locked);
        myListener.getActionSrch().setEnabled(!locked);
    }        

    /**
     * @return Returns the myTypeJournalModel.
     */
    public ZEOComboBoxModel getMyTypeJournalModel() {
        return myTypeJournalModel;
    }
    /**
     * @return Returns the myTypeOperationModel.
     */
    public ZEOComboBoxModel getMyTypeOperationModel() {
        return myTypeOperationModel;
    }
//    /**
//     * @return Returns the myComptabiliteModel.
//     */
//    public ZEOComboBoxModel getMyComptabiliteModel() {
//        return myComptabiliteModel;
//    }
    
    
    
//    private final class ComptabiliteListener implements ActionListener {
//
//        /**
//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
//         */
//        public void actionPerformed(ActionEvent e) {
//            myListener.getCurrentEcritureDico().put("comptabilite", getMyComptabiliteModel().getSelectedEObject());            
//        }
//    }

    
    private final class TypeJournalListener implements ActionListener {

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            myListener.getCurrentEcritureDico().put("typeJournal", getMyTypeJournalModel().getSelectedEObject());            
        }
        
    }
    
    private final class TypeOperationListener implements ActionListener {

        /**
         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
         */
        public void actionPerformed(ActionEvent e) {
            myListener.typeOperationChanged();
        }
        
    }

    /**
     * @param locked
     */
    public void lockAll(boolean locked) {
        lockSaisieEcriture(locked);
        myLibelleEcriture.getMyTexfield().setEnabled(!locked);
        myCommentaireEcriture.getMyTexfield().setEnabled(!locked);
    }   
    
    
    
//    private final class OrigineListener implements ActionListener {
//
//        /**
//         * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
//         */
//        public void actionPerformed(ActionEvent e) {
//            myListener.origineChanged();
////            myListener.setOrigine(myOrigineModel.getSelectedEObject());            
//        }
//    }
//
//    public final ZLookupButton getOrigineSelectButton() {
//        return origineSelectButton;
//    }



    public void updateDataOrigine() {
        final EOOrigine origine = (EOOrigine) myListener.getCurrentEcritureDico().get("origine");
        if (origine == null) {
            origineLib.setText(null);
        }
        else {
            final int l = origine.oriLibelle().length();
            origineLib.setText( l>103 ? origine.oriLibelle().substring(0, 100) + "..." : origine.oriLibelle());
        }
        
    }    
}
