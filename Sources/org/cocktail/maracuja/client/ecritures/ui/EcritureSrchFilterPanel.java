/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureSrchFilterPanel extends ZKarukeraPanel {
    private final Color BORDURE_COLOR=getBackground().brighter();

    private IEcritureSrchFilterPanel myListener;

//    private ZTextField  fNumeroMin;
//    private ZTextField  fNumeroMax;

    private ZFormPanel numerosPanel;
    private ZFormPanel datesPanel;
    private ZFormPanel montantPanel;

//    private ZDatePickerField dateSaisieMinField;
//    private ZDatePickerField dateSaisieMaxField;

    private ZFormPanel pcoNum;
    private ZFormPanel gesCode;
    private ZFormPanel libelle;

    private ZFormPanel resteEmarger;
    private ZFormPanel manNumero;
    private ZFormPanel titNumero;
    private JComboBox typeJournal;
    private ZFormPanel typeJournalField;
    private ZFormPanel ecdMontant;



    /**
     * @param editingContext
     */
    public EcritureSrchFilterPanel(IEcritureSrchFilterPanel listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(buildRightPanel(), BorderLayout.EAST);
    }


    private final JPanel buildRightPanel() {
        JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        ArrayList list = new ArrayList();
        list.add(myListener.getActionSrch());

        ArrayList comps = ZKarukeraPanel.getButtonListFromActionList(list, 50,50);
//        JComponent comp = (JComponent) comps.get(0);

        tmp.add(ZKarukeraPanel.buildVerticalPanelOfComponents(comps), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        return tmp;
    }

    private final JPanel buildFilters() {
        numerosPanel = buildNumeroFields();
        datesPanel = buildDateFields();
        pcoNum = ZFormPanel.buildLabelField("Imputation", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOPlanComptable.PCO_NUM_KEY));
        montantPanel = ZFormPanel.buildFourchetteNumberFields("<= Montant écriture <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"ecrMontantMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"ecrMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
        ecdMontant = ZFormPanel.buildFourchetteNumberFields("<= Montant ligne <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"ecdMontantMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"ecdMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
//        montantPanel = ZFormPanel.buildFourchetteNumberFields("<= Montant écriture <=", new ZTextField.DefaultTextFieldModel(myListener.getFilters(),"ecrMontantMin"), new ZTextField.DefaultTextFieldModel(myListener.getFilters(),"ecrMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
//        ecdMontant = ZFormPanel.buildFourchetteNumberFields("<= Montant ligne <=", new ZTextField.DefaultTextFieldModel(myListener.getFilters(),"ecdMontantMin"), new ZTextField.DefaultTextFieldModel(myListener.getFilters(),"ecdMontantMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
        gesCode = ZFormPanel.buildLabelField("Code gestion", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), EOGestion.GES_CODE_KEY) );
        libelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "libelle") );
        ((ZTextField)libelle.getMyFields().get(0)).getMyTexfield().setColumns(15);
        resteEmarger = ZFormPanel.buildFourchetteNumberFields("<= Reste à émarger <=", new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"ecdResteEmargerMin"), new ZNumberField.BigDecimalFieldModel(myListener.getFilters(),"ecdResteEmargerMax"), ZConst.DECIMAL_EDIT_FORMATS, ZConst.FORMAT_EDIT_NUMBER );
        ((ZTextField)resteEmarger.getMyFields().get(0)).getMyTexfield().setColumns(8);
        ((ZTextField)resteEmarger.getMyFields().get(1)).getMyTexfield().setColumns(8);

        manNumero = ZFormPanel.buildLabelField("N° mandat", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "manNumero") );
        titNumero = ZFormPanel.buildLabelField("N° titre", new ZTextField.DefaultTextFieldModel(myListener.getFilters(), "titNumero") );


        typeJournal = new JComboBox(myListener.getTypeJournalModel());
        typeJournal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myListener.getFilters().put("typeJournal", myListener.getTypeJournalModel().getSelectedEObject() )  ;
            }
        });

        typeJournalField = ZFormPanel.buildLabelField("Type de journal", typeJournal );
//        typeJournalField = new ZLabeledComponent("Type de journal", typeJournal, ZLabeledComponent.LABELONLEFT, -1);


        pcoNum.setDefaultAction(myListener.getActionSrch());
        numerosPanel.setDefaultAction(myListener.getActionSrch());
        datesPanel.setDefaultAction(myListener.getActionSrch());
        montantPanel.setDefaultAction(myListener.getActionSrch());
        ecdMontant.setDefaultAction(myListener.getActionSrch());
        gesCode.setDefaultAction(myListener.getActionSrch());
        libelle.setDefaultAction(myListener.getActionSrch());
        resteEmarger.setDefaultAction(myListener.getActionSrch());
        manNumero.setDefaultAction(myListener.getActionSrch());
        titNumero.setDefaultAction(myListener.getActionSrch());


        setSimpleLineBorder(numerosPanel);
        setSimpleLineBorder(datesPanel);        
        setSimpleLineBorder(pcoNum);        
        setSimpleLineBorder(montantPanel);        
        setSimpleLineBorder(ecdMontant);        
        setSimpleLineBorder(gesCode);        
        setSimpleLineBorder(libelle);        
        setSimpleLineBorder(resteEmarger);        
        setSimpleLineBorder(manNumero);        
        setSimpleLineBorder(titNumero);        
        setSimpleLineBorder(typeJournalField);        
        
        Box b = Box.createVerticalBox();
        b.add(ZKarukeraPanel.buildLine(new Component[]{pcoNum, gesCode, numerosPanel,datesPanel}));
        b.add(ZKarukeraPanel.buildLine(new Component[]{ montantPanel , ecdMontant, resteEmarger}));
        b.add(ZKarukeraPanel.buildLine(new Component[]{libelle, typeJournalField, manNumero, titNumero}));


        JPanel p = new JPanel(new BorderLayout());
        p.add(ZKarukeraPanel.buildLine(b), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;
    }


    /**
     * @return
     */
    private final ZFormPanel buildNumeroFields() {
        return ZFormPanel.buildFourchetteNumberFields("<= N° <=", new NumeroMinModel(), new NumeroMaxModel(), ZConst.ENTIER_EDIT_FORMATS, ZConst.FORMAT_ENTIER_BRUT );
    }



    private final ZFormPanel buildDateFields() {
        return ZFormPanel.buildFourchetteDateFields("<= Date <=", new DateSaisieMinFieldModel(), new DateSaisieMaxFieldModel(), ZConst.FORMAT_DATESHORT, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16) );
    }


    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        numerosPanel.updateData();
    }







    public interface IEcritureSrchFilterPanel {

        /**
         * @return
         */
        public Action getActionSrch();

        /**
         * @return
         */
        public ZEOComboBoxModel getTypeJournalModel();

        /**
         * @return un dictionnaire dans lequel sont stockés les filtres.
         */
        public HashMap getFilters();

    }

    private final class NumeroMinModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("ecrNumeroMin") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("ecrNumeroMin",value);

        }

    }
    private final class NumeroMaxModel implements ZTextField.IZTextFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("ecrNumeroMax") ;
        }

        /**
         * @see org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("ecrNumeroMax",value);

        }

    }

    private final class DateSaisieMaxFieldModel implements IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("ecrDateSaisieMax");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("ecrDateSaisieMax", value);
        }
        
        public Window getParentWindow() {
            return getMyDialog();
        }
    }
    private final class DateSaisieMinFieldModel implements IZDatePickerFieldModel {

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
         */
        public Object getValue() {
            return myListener.getFilters().get("ecrDateSaisieMin");
        }

        /**
         * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
         */
        public void setValue(Object value) {
            myListener.getFilters().put("ecrDateSaisieMin", value);
        }

        public Window getParentWindow() {
            return getMyDialog();
        }
    }


}
