/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureRectifPanel extends ZKarukeraPanel implements IEcritureDetailSaisiePanel {

	private static final long serialVersionUID = 1L;
	private EcritureSaisieDetailListPanel myEcritureSaisieDetailListPanel;
	private EcritureDetailListPanel myEcritureViewDetailListPanel;
	private IEcritureRectifPanelListener myListener;
	private EcritureFormPanel myEcritureFormPanel;
	private ZPanelBalance myZPanelBalance;

	public EcritureRectifPanel(IEcritureRectifPanelListener listener, EcritureFormPanel.IEcritureFormPanelListener formListener, EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener saisieDetailListener, EcritureDetailListPanel.IEcritureDetailListPanelListener detailViewListener,
			ZPanelBalance.IZPanelBalanceProvider balanceProvider) {
		super();
		myListener = listener;

		myEcritureFormPanel = new EcritureFormPanel(formListener);
		myEcritureSaisieDetailListPanel = new EcritureSaisieDetailListPanel(saisieDetailListener);
		myEcritureViewDetailListPanel = new EcritureDetailListPanel(detailViewListener);
		myZPanelBalance = new ZPanelBalance(balanceProvider, true);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		myEcritureFormPanel.setMyDialog(getMyDialog());
		myEcritureSaisieDetailListPanel.setMyDialog(getMyDialog());

		myEcritureFormPanel.initGUI();
		myEcritureSaisieDetailListPanel.initGUI();
		myEcritureViewDetailListPanel.initGUI();
		myZPanelBalance.initGUI();

		//        JPanel p = new JPanel(new BorderLayout());
		this.setLayout(new BorderLayout());
		this.add(buildCenterPanelForRectif(), BorderLayout.CENTER);
		//        this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(buildBottomPanel(), BorderLayout.SOUTH);

		//        add(p);
	}

	private final JPanel buildCenterPanelForRectif() {
		JPanel p = new JPanel(new BorderLayout());

		JPanel d = new JPanel(new BorderLayout());
		d.add(myEcritureSaisieDetailListPanel, BorderLayout.CENTER);
		d.add(buildToolBarEcritureDetail(), BorderLayout.NORTH);

		JPanel ecrPanelRectif = encloseInPanelWithTitle("Ecriture", null, ZConst.BG_COLOR_TITLE, myEcritureFormPanel, null, null);
		JPanel detailsViewPanelRectif = encloseInPanelWithTitle("Détails de l'écriture", null, ZConst.BG_COLOR_TITLE, myEcritureViewDetailListPanel, null, null);
		JPanel detailsSaisieRectif = encloseInPanelWithTitle("Rectifications de l'écriture", null, ZConst.BG_COLOR_TITLE, d, null, null);

		JSplitPane s = new JSplitPane(JSplitPane.VERTICAL_SPLIT, detailsViewPanelRectif, detailsSaisieRectif);
		s.setDividerLocation(0.5);
		s.setResizeWeight(0.5);
		s.setBorder(BorderFactory.createEmptyBorder());

		p.add(ecrPanelRectif, BorderLayout.PAGE_START);
		p.add(s, BorderLayout.CENTER);

		JPanel v = new JPanel(new BorderLayout());
		v.add(p, BorderLayout.CENTER);
		v.add(myZPanelBalance, BorderLayout.SOUTH);
		return v;
	}

	private JToolBar buildToolBarEcritureDetail() {
		JToolBar myToolBar = new JToolBar();
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);
		//Initialiser les boutons à partir des iactions	
		myToolBar.add(myListener.myActionAddEcritureDetail());
		myToolBar.add(myListener.myActionDeleteEcritureDetail());
		return myToolBar;
	}

	private final JPanel buildBottomPanel() {
		ArrayList a = new ArrayList();
		a.add(myListener.actionValider());
		a.add(myListener.actionClose());
		JPanel p = new JPanel(new BorderLayout());
		p.add(ZKarukeraDialog.buildHorizontalButtonsFromActions(a));
		return p;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myEcritureFormPanel.updateData();
		myEcritureViewDetailListPanel.updateData();
		myEcritureSaisieDetailListPanel.updateData();
		myZPanelBalance.updateData();
	}

	public interface IEcritureRectifPanelListener {
		public Action myActionAddEcritureDetail();

		public Action actionClose();

		public Action actionValider();

		public Action myActionDeleteEcritureDetail();
		//        public Action actionDetail();

		//        public FactoryEcritureDetail getFactoryEcritureDetail();
		//
		//        /**
		//         * @return
		//         */
		//        public HashMap currentEcritureDico();
		//
		//        /**
		//         * @return
		//         */
		//        public NSArray getCcomptabilites();
		//
		//        /**
		//         * 
		//         */
		//        public NSArray getTypeJournals();
		//
		//        /**
		//         * @return
		//         */
		//        public NSArray getTypeOperations();
		//
		//        /**
		//         * @return
		//         */
		//        public EOEcriture getCurrentEcriture();
		//
		//        /**
		//         * @return
		//         */
		//        public NSArray getDataForDetailSaisie();
		//
		//        /**
		//         * @return
		//         */
		//        public NSArray getPlanComptables();
		//
		//        /**
		//         * @return
		//         */
		//        public NSArray getGestions();
		//
		//        /**
		//         * 
		//         */
		//        public void deleteRow();
		//
		//        /**
		//         * 
		//         */
		//        public void addRow();
		//
		//        /**
		//         * @return
		//         */
		//        public NSArray getDataForDetailView();
		//
		//        /**
		//         * Evénement lancé lorsque les données détails de rectification ont changé.
		//         */
		//        public void dataSaisieChanged();
		//
		//        /**
		//         * 
		//         */
		//        public void typeOperationChanged();
		//
		//        /**
		//         * @return
		//         */
		//        public ZEOComboBoxModel getOrigineModel();
		//
		//        /**
		//         * 
		//         */
		//        public void origineChanged();
	}

	//    
	//    
	//    
	//    private final class EcritureFormPanelListener implements EcritureFormPanel.IEcritureFormPanelListener {
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#getTypesOperations()
	//         */
	//        public NSArray getTypesOperations() {
	//            return myListener.getTypeOperations();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#getCurrentEcritureDico()
	//         */
	//        public HashMap getCurrentEcritureDico() {
	//            return myListener.currentEcritureDico();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#getTypeJournals()
	//         */
	//        public NSArray getTypeJournals() {
	//            return myListener.getTypeJournals();
	//        }
	//
	//
	//        /**
	//         * Renvoi la liste des comptabilites autorisées pour l'utilisateur pour la création d'ecritures.
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#getcomptabilites()
	//         */
	//        public NSArray getcomptabilites() {
	//            return myListener.getCcomptabilites();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#getOrigineModel()
	//         */
	//        public ZEOComboBoxModel getOrigineModel() {
	//            return myListener.getOrigineModel() ;
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#typeOperationChanged()
	//         */
	//        public void typeOperationChanged() {
	//            myListener.typeOperationChanged();
	//            
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureFormPanel.IEcritureFormPanelListener#origineChanged()
	//         */
	//        public void origineChanged() {
	//            myListener.origineChanged();
	//            
	//        }
	//    }    
	//    
	//    
	//    private final class EcritureSaisieDetailListPanelListener implements EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener {
	//
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#deleteRow()
	//         */
	//        public void deleteRow() {
	//            myListener.deleteRow();
	//            
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#onSelectionChanged()
	//         */
	//        public void onSelectionChanged() {
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getFactoryEcritureDetail()
	//         */
	//        public FactoryEcritureDetail getFactoryEcritureDetail() {
	//            return myListener.getFactoryEcritureDetail();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getPlanComptables()
	//         */
	//        public NSArray getPlanComptables() {
	//            return myListener.getPlanComptables();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getGestions()
	//         */
	//        public NSArray getGestions() {
	//            return myListener.getGestions();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#addRow()
	//         */
	//        public void addRow() {
	//            myListener.addRow();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#getData()
	//         */
	//        public NSArray getData() {
	//            return myListener.getDataForDetailSaisie();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureSaisieDetailListPanel.IEcritureDetailListPanelListener#onTableChanged()
	//         */
	//        public void onTableChanged() {
	//            myListener.dataSaisieChanged();
	//        }
	//        
	//    }
	//    
	////    
	//    private final class EcritureDetailViewListPanelListener implements EcritureDetailListPanel.IEcritureDetailListPanelListener {
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.ecritures.EcritureDetailViewListPanel.IEcritureDetailViewListPanelListener#getData()
	//         */
	//        public NSArray getData() {
	//            return myListener.getDataForDetailView();
	//        }
	//        
	//    }

	/**
	 * @param detail
	 */
	public void ecritureDetailAjouterInDg(EOEcritureDetail detail) {
		myEcritureSaisieDetailListPanel.ecritureDetailAjouterInDg(detail);
	}

	/**
	 * @return
	 */
	public EOEcritureDetail selectedDetailSaisieRow() {
		return myEcritureSaisieDetailListPanel.selectedEcritureDetail();
	}

	/**
	 * @param b
	 */
	public void lockSaisieEcriture(boolean b) {
		myEcritureFormPanel.lockAll(b);
	}

	/**
	 * @param b
	 */
	public void lockSaisieEcritureDetail(boolean b) {
		myEcritureSaisieDetailListPanel.lockSaisieEcritureDetail(b);
		myListener.myActionAddEcritureDetail().setEnabled(!b);
		myListener.myActionDeleteEcritureDetail().setEnabled(!b);
	}

	public EcritureFormPanel getMyEcritureFormPanel() {
		return myEcritureFormPanel;
	}

	public ZPanelBalance getMyZPanelBalance() {
		return myZPanelBalance;
	}

	public EcritureSaisieDetailListPanel getMyEcritureSaisieDetailListPanel() {
		return myEcritureSaisieDetailListPanel;
	}
}
