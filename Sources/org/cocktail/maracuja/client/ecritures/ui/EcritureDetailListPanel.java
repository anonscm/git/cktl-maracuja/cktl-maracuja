/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

public final class EcritureDetailListPanel extends ZKarukeraPanel {
	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected EODisplayGroup myDisplayGroup;
	protected TableSorter myTableSorter;
	protected ArrayList myCols;

	private IEcritureDetailListPanelListener myListener;

	/**
	 * @param editingContext
	 */
	public EcritureDetailListPanel(IEcritureDetailListPanelListener listener) {
		super();
		myListener = listener;
	}

	private void initTableModel() {
		myCols = new ArrayList(7);

		ZEOTableModelColumn colEcdIndex = new ZEOTableModelColumn(myDisplayGroup, "ecdIndex", "#", 29);
		colEcdIndex.setAlignment(SwingConstants.LEFT);
		colEcdIndex.setColumnClass(Integer.class);

		ZEOTableModelColumn colEcdPlancomptablePcoNum = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoNum", "Imputation", 68);
		colEcdPlancomptablePcoNum.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdPlancomptablePcoLibelle = new ZEOTableModelColumn(myDisplayGroup, "planComptable.pcoLibelle", "Libelle Imputation", 263);
		colEcdPlancomptablePcoLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdGestionGesCode = new ZEOTableModelColumn(myDisplayGroup, "gestion.gesCode", "Code gestion", 83);
		colEcdGestionGesCode.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colEcdLibelle = new ZEOTableModelColumn(myDisplayGroup, "ecdLibelle", "Libellé", 228);
		colEcdLibelle.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colEcdDebit = new ZEOTableModelColumn(myDisplayGroup, "ecdDebit", "Débit", 90);
		colEcdDebit.setAlignment(SwingConstants.RIGHT);
		colEcdDebit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdDebit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdCredit = new ZEOTableModelColumn(myDisplayGroup, "ecdCredit", "Crédit", 90);
		colEcdCredit.setAlignment(SwingConstants.RIGHT);
		colEcdCredit.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdCredit.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdResteEmarger = new ZEOTableModelColumn(myDisplayGroup, "ecdResteEmarger", "Reste à émarger", 90);
		colEcdResteEmarger.setAlignment(SwingConstants.RIGHT);
		colEcdResteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdResteEmarger.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colEcdCalcResteEmarger = new ZEOTableModelColumn(myDisplayGroup, EOEcritureDetail.CALCULATED_RESTE_EMARGER_KEY, "Reste à émarger calculé", 90);
		colEcdCalcResteEmarger.setAlignment(SwingConstants.RIGHT);
		colEcdCalcResteEmarger.setFormatDisplay(ZConst.FORMAT_DISPLAY_NUMBER);
		colEcdCalcResteEmarger.setColumnClass(BigDecimal.class);

		ZEOTableModelColumn colConvention = new ZEOTableModelColumn(myDisplayGroup, EOEcritureDetail.TO_ACCORDS_CONTRAT_KEY + "." + EOAccordsContrat.NUMERO_KEY, "Convention", 100);
		colConvention.setAlignment(SwingConstants.CENTER);

		myCols.add(colEcdIndex);
		myCols.add(colEcdPlancomptablePcoNum);
		myCols.add(colEcdPlancomptablePcoLibelle);
		myCols.add(colEcdGestionGesCode);
		myCols.add(colEcdLibelle);
		myCols.add(colEcdDebit);
		myCols.add(colEcdCredit);
		myCols.add(colEcdResteEmarger);
		myCols.add(colConvention);
		//myCols.add(colEcdCalcResteEmarger);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter(myTableModel);
		//    		myTableSorter.activeSortOnColumn(0, false, false);

		//    		myTableModel.addTableModelListener(this);
	}

	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());
		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}

	public void initGUI() {
		myDisplayGroup = new EODisplayGroup();

		initTableModel();
		initTable();

		setLayout(new BorderLayout());
		add(new JScrollPane(myEOTable), BorderLayout.CENTER);

	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		myDisplayGroup.setObjectArray(myListener.getData());
		myEOTable.updateData();
	}

	public interface IEcritureDetailListPanelListener {
		public NSArray getData();
	}

	public EOEcritureDetail getselectedObject() {
		return (EOEcritureDetail) myDisplayGroup.selectedObject();
	}

	public ZEOTable getMyEOTable() {
		return myEOTable;
	}

	public ZEOTableModel getMyTableModel() {
		return myTableModel;
	}

}
