/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures.ui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.zutil.client.TableSorter;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureListPanel extends ZKarukeraPanel implements ZEOTableListener {
    private IEcritureViewListPanelListener myListener;

	protected ZEOTable myEOTable;
	protected ZEOTableModel myTableModel;
	protected EODisplayGroup myDisplayGroup;
	protected TableSorter myTableSorter;
	protected ArrayList myCols;



    /**
     * @param editingContext
     */
    public EcritureListPanel(IEcritureViewListPanelListener listener) {
        super();
        myListener = listener;
    }



	private void initTableModel() {
		myCols = new ArrayList(7);

		ZEOTableModelColumn colEcrNumero = new ZEOTableModelColumn(myDisplayGroup,"ecrNumero","N° Ecriture",60);
		colEcrNumero.setAlignment(SwingConstants.RIGHT);
		colEcrNumero.setColumnClass(Integer.class);


		ZEOTableModelColumn colEcrDateSaisie = new ZEOTableModelColumn(myDisplayGroup,"ecrDateSaisie","Date",90);
		colEcrDateSaisie.setAlignment(SwingConstants.CENTER);
		colEcrDateSaisie.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		colEcrDateSaisie.setColumnClass(Date.class);

		ZEOTableModelColumn colEcrLibelle = new ZEOTableModelColumn(myDisplayGroup,"ecrLibelle","Libellé",96);
		colEcrLibelle.setAlignment(SwingConstants.CENTER);

		ZEOTableModelColumn colTypeJournal = new ZEOTableModelColumn(myDisplayGroup,"typeJournal.tjoLibelle","Journal",160);
		colTypeJournal.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colTypeOperation = new ZEOTableModelColumn(myDisplayGroup,"typeOperation.topLibelle","Opération",100);
		colTypeOperation.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colOrigine = new ZEOTableModelColumn(myDisplayGroup,"origine.oriLibelle","Origine",90);
		colOrigine.setAlignment(SwingConstants.LEFT);

		ZEOTableModelColumn colUtilisateur = new ZEOTableModelColumn(myDisplayGroup,"utilisateur.nomAndPrenom","Utilisateur",130);
		colUtilisateur.setAlignment(SwingConstants.LEFT);

		myCols.add(colEcrNumero);
		myCols.add(colEcrDateSaisie);
		myCols.add(colEcrLibelle);
		myCols.add(colTypeJournal);
		myCols.add(colTypeOperation);
		myCols.add(colOrigine);
		myCols.add(colUtilisateur);

		myTableModel = new ZEOTableModel(myDisplayGroup, myCols);
		myTableSorter = new TableSorter (myTableModel);
	}









	/**
	 * Initialise la table à afficher (le modele doit exister)
	 */
	private void initTable() {
		myEOTable = new ZEOTable(myTableSorter);
		myEOTable.addListener(this);
		myTableSorter.setTableHeader(myEOTable.getTableHeader());

		myEOTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	}



    public void initGUI() {
        myDisplayGroup = new EODisplayGroup();
        initTableModel();
        initTable();
        setLayout(new BorderLayout());
        add(new JScrollPane(myEOTable), BorderLayout.CENTER);
    }



    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        myDisplayGroup.setObjectArray(myListener.getEcritures() );
//        myTableModel.updateInnerRowCount();
//        myTableModel.fireTableDataChanged();
        myEOTable.updateData();
    }




	public interface IEcritureViewListPanelListener {
        public void selectionChanged();
        public NSArray getEcritures();
	}







	/**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onDbClick()
     */
    public void onDbClick() {

    }



    /**
     * @see org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener#onSelectionChanged()
     */
    public void onSelectionChanged() {
        myListener.selectionChanged();
    }



    /**
     * @return
     */
    public EOEcriture selectedObject() {
        return (EOEcriture) myDisplayGroup.selectedObject();
    }



    /**
     * @return
     */
    public NSArray getSelectedEcritures() {
        return myDisplayGroup.selectedObjects() ;
    }



}
