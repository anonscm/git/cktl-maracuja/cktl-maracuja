/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.ecritures;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcompta.common.util.CktlEOControlUtilities;
import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ExportFactoryClient;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZAction;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel;
import org.cocktail.maracuja.client.emargements.ctrl.EmargementSaisieCtrl;
import org.cocktail.maracuja.client.emargements.ctrl.EmargementSrchCtrl;
import org.cocktail.maracuja.client.emargements.ctrl.FactoryEmargementProxy;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderEmargement;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOAccordsContrat;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOEcritureDetail;
import org.cocktail.maracuja.client.metier.EOEmargement;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOPlanComptable;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EcritureSrchCtrl extends CommonCtrl {
	private static final String TITLE = "Gestion des écritures";
	private static final Dimension WINDOW_DIMENSION = new Dimension(970, 700);

	private final ZAction ACTION_SAISIE = myApp.getActionbyId(ZActionCtrl.IDU_JOECSA);
	private final ZAction ACTION_RECTIFIER = myApp.getActionbyId(ZActionCtrl.IDU_JOECSA);
	private final ZAction ACTION_ANNULER = myApp.getActionbyId(ZActionCtrl.IDU_COAN);

	private EcritureSrchPanel myPanel;

	private final ActionClose actionClose = new ActionClose();
	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionNew actionNew = new ActionNew();
	private ActionImprimer actionImprimer = new ActionImprimer();
	private final ActionVoirEmargements actionVoirEmargements = new ActionVoirEmargements();
	private final ActionNewEmargement actionNewEmargement = new ActionNewEmargement();
	private final ActionCheckResteEmarger actionCheckResteEmarger = new ActionCheckResteEmarger();
	private final ActionExportExcel2 actionExportExcel2 = new ActionExportExcel2();
	//    private ActionImprimerAll actionImprimerAll = new ActionImprimerAll();
	private ActionAnnulerEcriture actionAnnulerEcriture = new ActionAnnulerEcriture();
	private ActionRectifierEcriture actionRectifierEcriture = new ActionRectifierEcriture();
	private HashMap myFilters;

	private NSArray ecrituresForRecap;

	//    private FactoryProcessJournalEcriture myFactoryProcessJournalEcriture;
	//    private FactoryEcritureDetail myFactoryEcritureDetail;

	private ZPanelBalanceEcritureProvider panelBalanceEcritureProvider;
	private ZEOComboBoxModel modelTypeJournal;

	//    private final NSArray planComptablesEmargeables;

	/**
	 * @param editingContext
	 * @throws DefaultClientException
	 */
	public EcritureSrchCtrl(EOEditingContext editingContext) {
		super(editingContext);
		revertChanges();

		//        planComptablesEmargeables = EOsFinder.getPlancosValidesEmargeables(getEditingContext(), false);
		myFilters = new HashMap();

		panelBalanceEcritureProvider = new ZPanelBalanceEcritureProvider();
		myPanel = new EcritureSrchPanel(new EcritureSrchPanelListener());

		NSArray typeJournals = EOsFinder.getAllTypeJournal(getEditingContext());
		try {
			modelTypeJournal = new ZEOComboBoxModel(typeJournals, "tjoLibelle", "", null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.EcritureRecherchePanel.IEcritureRecherchePanelListener#getBordereaux()
	 * @return Renvoie une liste d'ordres de paiement en fonction des filtres.
	 */
	public final NSArray getEcritures() {
		//Créer la condition à partir des filtres
		try {
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("ecrNumero", EOSortOrdering.CompareDescending);
			//TODO verifier....
			ecrituresForRecap = EOsFinder.fetchArray(getEditingContext(), EOEcriture.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)), new NSArray(sort1), true, true, true, null, 1000);

			// S'il y a une recherche par titre ou mandat, on filtre ici
			NSMutableArray res3 = new NSMutableArray();
			if (myFilters.get("titNumero") != null) {
				NSArray ecrs = EOsFinder.getEcrituresForTitNumero(getEditingContext(), new Integer((String) myFilters.get("titNumero")));
				for (int i = 0; i < ecrituresForRecap.count(); i++) {
					if (ecrs.indexOfObject(ecrituresForRecap.objectAtIndex(i)) != NSArray.NotFound) {
						res3.addObject(ecrituresForRecap.objectAtIndex(i));
					}
				}
				ecrituresForRecap = res3;
			}
			NSMutableArray res4 = new NSMutableArray();
			if (myFilters.get("manNumero") != null) {
				NSArray ecrs = EOsFinder.getEcrituresForManNumero(getEditingContext(), new Integer((String) myFilters.get("manNumero")));
				for (int i = 0; i < ecrituresForRecap.count(); i++) {
					if (ecrs.indexOfObject(ecrituresForRecap.objectAtIndex(i)) != NSArray.NotFound) {
						res4.addObject(ecrituresForRecap.objectAtIndex(i));
					}
				}
				ecrituresForRecap = res4;
			}

			ZLogger.verbose("" + ecrituresForRecap.count());

			if (ecrituresForRecap.count() == 1000) {
				showInfoDialog("Plus de 1000 écritures ont été trouvées. seules les 1000 dernières apparaissent.");
			}

			return ecrituresForRecap;

		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}
	}

	public final NSArray getEcritureDetails() {
		try {
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("ecdIndex", EOSortOrdering.CompareAscending);
			if (getSelectedEcriture() != null && getSelectedEcritures().count() == 1) {
				NSArray res = getSelectedEcriture().detailEcriture();
				CktlEOControlUtilities.invalidateObjects(res);
				return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(sort1));
			}
			return new NSArray();
		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}
	}

	public final EOEcriture getSelectedEcriture() {
		return myPanel.getSelectedEcriture();
	}

	public final NSArray getSelectedEcritures() {
		return myPanel.getSelectedEcritures();
	}

	private final void onEcritureSelectionChanged() {
		try {
			myPanel.getEcritureDetailListPanel().updateData();
			myPanel.getMyZPanelBalanceNew().updateData();
			refreshActions();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.EcritureRechercheFilterPanel.IEcritureRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			setWaitCursor(true);
			myPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {
		return buildFilterQualifiers(dicoFiltre, "");
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre, String prefix) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "exercice=%@", new NSArray(new Object[] {
				myApp.appUserInfo().getCurrentExercice()
		})));

		//Construire les qualifiers à partir des saisies utilisateur

		///Numero
		NSMutableArray qualsNum = new NSMutableArray();
		if (dicoFiltre.get("ecrNumeroMin") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecrNumero>=%@", new NSArray(new Integer(((Number) dicoFiltre.get("ecrNumeroMin")).intValue()))));
		}
		if (dicoFiltre.get("ecrNumeroMax") != null) {
			qualsNum.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecrNumero<=%@", new NSArray(new Integer(((Number) dicoFiltre.get("ecrNumeroMax")).intValue()))));
		}
		if (qualsNum.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsNum));
		}

		NSMutableArray qualsMont = new NSMutableArray();
		if (dicoFiltre.get("ecrMontantMin") != null) {
			qualsMont.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecritureMontant.ecrMontant>=%@", new NSArray(dicoFiltre.get("ecrMontantMin"))));
		}
		if (dicoFiltre.get("ecrMontantMax") != null) {
			qualsMont.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecritureMontant.ecrMontant<=%@", new NSArray(dicoFiltre.get("ecrMontantMax"))));
		}
		if (qualsMont.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsMont));
		}

		NSMutableArray qualsecdMont = new NSMutableArray();
		if (dicoFiltre.get("ecdMontantMin") != null) {
			qualsecdMont.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.ecdMontant>=%@", new NSArray(dicoFiltre.get("ecdMontantMin"))));
		}
		if (dicoFiltre.get("ecdMontantMax") != null) {
			qualsecdMont.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.ecdMontant<=%@", new NSArray(dicoFiltre.get("ecdMontantMax"))));
		}
		if (qualsecdMont.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsecdMont));
		}

		NSMutableArray qualsResteEmarger = new NSMutableArray();
		if (dicoFiltre.get("ecdResteEmargerMin") != null) {
			qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.ecdResteEmarger>=%@", new NSArray(dicoFiltre.get("ecdResteEmargerMin"))));
		}
		if (dicoFiltre.get("ecdResteEmargerMax") != null) {
			qualsResteEmarger.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.ecdResteEmarger<=%@", new NSArray(dicoFiltre.get("ecdResteEmargerMax"))));
		}
		if (qualsResteEmarger.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsResteEmarger));
		}

		if (dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY) != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.pcoNum=%@", new NSArray(dicoFiltre.get(EOPlanComptable.PCO_NUM_KEY))));
		}

		if (dicoFiltre.get("typeJournal") != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "typeJournal=%@", new NSArray(dicoFiltre.get("typeJournal"))));
		}

		if (dicoFiltre.get(EOGestion.GES_CODE_KEY) != null && ((String) dicoFiltre.get(EOGestion.GES_CODE_KEY)).length() > 0) {
			//            //Vérifier si les codes gestions sont autorisés pour l'utilisateur
			//            if ( EOQualifier.filteredArrayWithQualifier( gestions, EOQualifier.qualifierWithQualifierFormat( "gesCode=%@" , new NSArray((String)dicoFiltre.get(EOGestion.GES_CODE_KEY)))).count()==0 ) {
			//                throw new DataCheckException("Code gestion non autorisé");
			//            }
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.gesCode=%@", new NSArray(new Object[] {
					(String) dicoFiltre.get(EOGestion.GES_CODE_KEY), (String) dicoFiltre.get(EOGestion.GES_CODE_KEY)
			})));
		}

		if (dicoFiltre.get("libelle") != null && ((String) dicoFiltre.get("libelle")).length() > 0) {
			NSMutableArray qualsLibelle = new NSMutableArray();
			String cond = "*" + (String) dicoFiltre.get("libelle") + "*";
			cond = cond.replace(' ', '*');
			qualsLibelle.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecrLibelle caseInsensitiveLike %@", new NSArray(cond)));
			qualsLibelle.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "detailEcriture.ecdLibelle caseInsensitiveLike %@", new NSArray(cond)));
			quals.addObject(new EOOrQualifier(qualsLibelle));
		}

		///Numero 2
		NSMutableArray qualsNum2 = new NSMutableArray();
		if (dicoFiltre.get("ecrNumeroList") != null) {
			NSArray list = (NSArray) dicoFiltre.get("ecrNumeroList");
			for (int i = 0; i < list.count(); i++) {
				Number element = (Number) list.objectAtIndex(i);
				qualsNum2.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecrNumero=%@", new NSArray(element)));
			}
		}
		if (qualsNum2.count() > 0) {
			quals.addObject(new EOOrQualifier(qualsNum2));
		}

		///Date
		NSMutableArray qualsDate = new NSMutableArray();
		if (dicoFiltre.get("ecrDateSaisieMin") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecrDateSaisie>=%@", new NSArray(new NSTimestamp((Date) dicoFiltre.get("ecrDateSaisieMin")))));
		}
		if (dicoFiltre.get("ecrDateSaisieMax") != null) {
			qualsDate.addObject(EOQualifier.qualifierWithQualifierFormat(prefix + "ecrDateSaisie<=%@", new NSArray(new NSTimestamp(ZDateUtil.addDHMS((Date) dicoFiltre.get("ecrDateSaisieMax"), 0, 23, 59, 59)))));

			ZLogger.debug("date 1", dicoFiltre.get("ecrDateSaisieMax"));
			ZLogger.debug("date 2", ZDateUtil.addDHMS((Date) dicoFiltre.get("ecrDateSaisieMax"), 0, 23, 59, 59));
		}
		if (qualsDate.count() > 0) {
			quals.addObject(new EOAndQualifier(qualsDate));
		}

		ZLogger.verbose(dicoFiltre);
		ZLogger.verbose(quals);

		return quals;
	}

	/**
	 * Imprime l'écriture en cours.
	 */
	private final void imprimerEcriture() {
		imprimerSelectedEcritures();
	}

	private final void imprimerSelectedEcritures() {
		try {
			setWaitCursor(true);
			//On fait la liste des ecritures valides
			NSMutableArray valides = new NSMutableArray();

			NSArray selectedEcritures = getSelectedEcritures();

			for (int i = 0; i < selectedEcritures.count(); i++) {
				EOEcriture element = (EOEcriture) selectedEcritures.objectAtIndex(i);
				if (EOEcriture.ecritureValide.equals(element.ecrEtat())) {
					valides.addObject(element);
				}
			}

			ZLogger.debug("Impression de " + valides.count() + " ecritures.");

			String filePath = ReportFactoryClient.imprimerEcritureValide(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), valides);
			if (filePath != null) {
				myApp.openPdfFile(filePath);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}

	}

	//    private final void imprimerEcritureAll() {
	//        //On fait la liste des ecritures valides
	//        NSMutableArray valides = new NSMutableArray();
	//        for (int i = 0; i < ecrituresForRecap .count(); i++) {
	//            EOEcriture element = (EOEcriture) ecrituresForRecap.objectAtIndex(i);
	//            if (EOEcriture.ecritureValide.equals(element.ecrEtat())) {
	//                valides.addObject(element);
	//            }
	//        }
	//
	//        try {
	//
	//            ZLogger.debug("Impression de " +  valides.count() + " ecritures.");
	//
	//
	//	        String filePath = ReportFactoryClient.imprimerEcritureValide(getEditingContext(), myApp.temporaryDir, myApp.getParametres() ,   valides  );
	//	        if (filePath!=null) {
	//	            myApp.openPdfFile(filePath);
	//	        }
	//	    } catch (Exception e) {
	//	       showErrorDialog(e);
	//	    }
	//
	//
	//
	//    }

	/**
	 * Active/desactive les actions en fonction de l'ecriture selectionnee.
	 */
	private final void refreshActions() {

		actionExportExcel2.setEnabled(ecrituresForRecap != null && ecrituresForRecap.count() > 0);

		if (getSelectedEcriture() == null) {
			actionAnnulerEcriture.setEnabled(false);
			actionRectifierEcriture.setEnabled(false);
			actionImprimer.setEnabled(false);
			//            actionImprimerAll.setEnabled(false);
		}
		else {
			actionImprimer.setEnabled(true);
			if (getSelectedEcritures().count() == 1) {
				actionAnnulerEcriture.setEnabled(getSelectedEcriture().isEcritureAnnulable());
				actionRectifierEcriture.setEnabled(getSelectedEcriture().isEcritureRectifiable());
			}
			else {
				actionAnnulerEcriture.setEnabled(false);
				actionRectifierEcriture.setEnabled(false);
			}
		}
	}

	private final void fermer() {
		getMyDialog().onCloseClick();
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}

		myPanel.setMyDialog(win);
		myPanel.setPreferredSize(WINDOW_DIMENSION);
		myPanel.initGUI();
		win.setContentPane(myPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial, NSArray ecritures) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			loadEcritures(ecritures);
			win.open();
		} finally {
			win.dispose();
		}
	}

	private final void ecritureNew() throws Exception {
		EcritureSaisieCtrl ecritureSaisieCtrl = new EcritureSaisieCtrl(getEditingContext());
		NSArray ecrituresTmp = ecritureSaisieCtrl.openDialogNew(getMyDialog());
		if (ecrituresTmp != null && ecrituresTmp.count() > 0) {
			myFilters.clear();
			NSMutableArray ecrNums = new NSMutableArray();
			for (int i = 0; i < ecrituresTmp.count(); i++) {
				EOEcriture element = (EOEcriture) ecrituresTmp.objectAtIndex(i);
				ecrNums.addObject(element.ecrNumero());
			}
			myFilters.put("ecrNumeroList", ecrNums);
			try {
				myPanel.updateData();
			} catch (Exception e) {
				myFilters.clear();
				showErrorDialog(e);
			} finally {
				myFilters.clear();
			}
		}
	}

	/**
	 * On affiche une fenêtre de rectification d'écriture.
	 *
	 * @throws DefaultClientException
	 */
	private final void lanceRectifierEcriture(EOEcriture ecriture) throws Exception {
		if (!ACTION_RECTIFIER.isEnabled()) {
			throw new DefaultClientException("Vous n'avez pas les droits de création d'écritures.");
		}

		EcritureSaisieCtrl ecritureSaisieCtrl = new EcritureSaisieCtrl(getEditingContext(), EcritureSaisieCtrl.MODE_RECTIF);
		NSArray ecrituresTmp = ecritureSaisieCtrl.openDialogRectif(getMyDialog(), ecriture);
		loadEcritures(ecrituresTmp);
	}

	public final void resetFilter() {
		myFilters.clear();
	}

	/**
	 * Envoi une exception s'il y a des émargements sur l'écriture.
	 *
	 * @param ecr
	 * @throws Exception
	 */
	private void checkIsEmargement(EOEcriture ecr) throws Exception {
		NSArray ems = EOsFinder.getEmargementsForEcriture(getEditingContext(), ecr);
		if (ems.count() > 0) {
			NSMutableArray nums = new NSMutableArray();
			for (int i = 0; i < ems.count(); i++) {
				EOEmargement element = (EOEmargement) ems.objectAtIndex(i);
				nums.addObject(element.emaNumero());
			}
			String mes = "Vous ne pouvez pas annuler l'écriture car elle a été émargée par ";
			mes += (nums.count() > 1 ? "les émargements " + nums.componentsJoinedByString(",") : "l'émargement " + nums.objectAtIndex(0));
			mes += (nums.count() > 1 ? ". Vous devez annuler ces émargements avant d'annuler l'écriture." : ". Vous devez annuler cet émargement avant d'annuler l'écriture.");
			throw new DefaultClientException(mes);
		}
	}

	private final void annulerEcriture() {
		try {
			EOEcriture currentEcriture = myPanel.getSelectedEcriture();
			if (currentEcriture == null) {
				throw new Exception("Aucune écriture sélectionnée.");
			}

			if (!ACTION_ANNULER.isEnabled()) {
				throw new DefaultClientException("Vous n'avez pas les droits pour annuler une écriture.");
			}

			final NSArray gestionsPourAnnulation = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_COAN);

			final NSMutableArray gestions = new NSMutableArray();
			for (int i = 0; i < currentEcriture.detailEcriture().count(); i++) {
				final EOEcritureDetail element = (EOEcritureDetail) currentEcriture.detailEcriture().objectAtIndex(i);
				if (gestions.indexOfObject(element.gestion()) == NSArray.NotFound) {
					gestions.addObject(element.gestion());
				}
			}
			//Vérifier que tous les codes gestions sont bien autorisés
			for (int i = 0; i < gestions.count(); i++) {
				final EOGestion element = (EOGestion) gestions.objectAtIndex(i);
				if (gestionsPourAnnulation.indexOfObject(element) == NSArray.NotFound) {
					throw new DataCheckException("Vous n'avez pas le droit d'annuler une écritures sur le code gestion " + element.gesCode());
				}
			}

			checkIsEmargement(currentEcriture);

			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous réellement créer une écriture d'annulation pour l'écriture n° " + getSelectedEcriture().ecrNumero() + " ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}

			NSArray res1 = EOsFinder.fetchArray(getEditingContext(), EOEcriture.ENTITY_NAME, "ecrLibelle=%@", new NSArray("Annulation Ecriture " + currentEcriture.exercice().exeExercice().intValue() + "/" + getSelectedEcriture().ecrNumero()), null, true);
			if (res1 != null && res1.count() > 0) {
				throw new DefaultClientException("Cette écriture a déjà été annulée par l'écriture n°" + ((EOEcriture) res1.objectAtIndex(0)).ecrNumero());
			}

			final FactoryProcessJournalEcriture myFactoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));

			HashMap correspondancesLignes = new HashMap();
			NSArray res = myFactoryProcessJournalEcriture.annulerEcriture(getEditingContext(), currentEcriture, myApp.appUserInfo().getUtilisateur(), correspondancesLignes);
			if (res.count() > 0) {
				//l'ecriture a été annulée

				//la sauvegarde
				getEditingContext().saveChanges();

				String msgFin = "L'écriture n°" + currentEcriture.ecrNumero() + " a été annulée";

				//on numerote les écritures d'annulation
				KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
				try {
					//Numéroter les ecritures
					NSMutableArray nbs = new NSMutableArray();
					for (int i = 0; i < res.count(); i++) {
						EOEcriture element = (EOEcriture) res.objectAtIndex(i);
						myFactoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), element, myKFactoryNumerotation);
						nbs.addObject(element.ecrNumero());
					}

					if (nbs.count() > 0) {
						if (nbs.count() > 1) {
							msgFin = msgFin + " par les écritures n° " + nbs.componentsJoinedByString(",");
						}
						else {
							msgFin = msgFin + " par l'écriture n° " + nbs.componentsJoinedByString(",");
						}
					}

					//nbs.addObject(currentEcriture.ecrNumero());
					myFilters.put("ecrNumeroList", nbs);
					if (showConfirmationDialog("Confirmation", msgFin + ". Souhaitez-vous les émarger entre elles ?", ZMsgPanel.BTLABEL_YES)) {
						FactoryEmargementProxy fp = new FactoryEmargementProxy(getEditingContext(), getUtilisateur(), getExercice(), FinderEmargement.leTypeLettrageAutomatique(getEditingContext()), false, new NSTimestamp(getDateJourneeComptable()));
						NSArray lesDebits = ZEOUtilities.unionOfNSArrays(new NSArray[] {
								currentEcriture.getDebits(), ((EOEcriture) res.objectAtIndex(0)).getDebits()
						});
						NSArray lesCredits = ZEOUtilities.unionOfNSArrays(new NSArray[] {
								currentEcriture.getCredits(), ((EOEcriture) res.objectAtIndex(0)).getCredits()
						});

						final NSArray debitsIds = ZEOUtilities.globalIDsForObjects(getEditingContext(), lesDebits);
						final NSArray creditsIds = ZEOUtilities.globalIDsForObjects(getEditingContext(), lesCredits);

						getEditingContext().invalidateObjectsWithGlobalIDs(debitsIds);
						getEditingContext().invalidateObjectsWithGlobalIDs(creditsIds);

						NSMutableArray ecrituresGenerees = new NSMutableArray();
						NSArray newEmargements = fp.emarger(FactoryEmargementProxy.EMARGEMENT_Dn_Cn, lesDebits, lesCredits, null, getComptabilite(), ecrituresGenerees);
						getEditingContext().saveChanges();

						if (newEmargements.count() > 0) {
							try {
								for (int i = 0; i < newEmargements.count(); i++) {
									final EOEmargement element = (EOEmargement) newEmargements.objectAtIndex(i);
									fp.numeroterEmargement(getEditingContext(), element);
								}
								msgFin += "\n\nEmargement(s) généré(s) " + ZEOUtilities.getCommaSeparatedListOfValues(newEmargements, "emaNumero");
							} catch (Exception e) {
								System.out.println("ERREUR LORS DE LA NUMEROTATION EMARGEMENT...");
								e.printStackTrace();
								throw new Exception(e);
							}
							showInfoDialog(msgFin);
						}

					}
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION ECRITURE...");
					e.printStackTrace();
					throw new Exception(e);
				}
			}
			myPanel.updateData();
			//verifier que ca passe
			myFilters.put("ecrNumeroList", null);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void openEmargements() {
		try {
			EOEcriture currentEcriture = myPanel.getSelectedEcriture();
			if (currentEcriture == null) {
				throw new Exception("Aucune écriture sélectionnée.");
			}
			NSArray emargements = EOsFinder.getEmargementsForEcriture(getEditingContext(), currentEcriture);
			if (emargements.count() > 0) {
				boolean b = false;
				for (int i = 0; i < emargements.count(); i++) {

					EOEmargement element = (EOEmargement) emargements.objectAtIndex(i);
					if (element.emaNumero().doubleValue() != -1) {
						b = true;
					}
				}
				if (!b) {
					throw new DefaultClientException("L'écriture comporte des émargements mais ils sont automatiques.");
				}

				EmargementSrchCtrl win = new EmargementSrchCtrl(myApp.editingContext());
				try {
					win.openDialog(myApp.getMainWindow(), emargements);
					myPanel.getEcritureDetailListPanel().updateData();
				} catch (Exception e1) {
					myApp.showErrorDialog(e1);
				}
			}
			else {
				throw new DefaultClientException("L'écriture " + currentEcriture.ecrNumero() + " n'a pas été émargée.");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void checkResteEmarger() {
		setWaitCursor(true);
		try {
			EOEcritureDetail ecd = myPanel.getEcritureDetailListPanel().getselectedObject();
			if (ecd == null) {
				throw new Exception("Aucune ligne d'écriture sélectionnée.");
			}
			BigDecimal calculatedReste = ecd.calculatedEcdResteEmarger();
			if (ecd.ecdResteEmarger().compareTo(calculatedReste) == 0) {
				showInfoDialog("Le reste à émarger de <b>" + ZConst.FORMAT_DECIMAL.format(ecd.ecdResteEmarger()) + "</b> semble correct");
			}
			else {
				if (ecd.calculatedEcdResteEmarger().signum() < 0) {
					throw new DefaultClientException("Le reste à émarger calculé de <b>" + ZConst.FORMAT_DECIMAL.format(calculatedReste) + "</b> est négatif. Certains émargements sur cette écriture posent problème, il est souhaitable de tous les supprimer et de les recréer.");
				}

				if (showConfirmationDialog("Confirmation", "Le reste à émarger affecté à la ligne : <b>" + ZConst.FORMAT_DECIMAL.format(ecd.ecdResteEmarger()) + "</b> semble erroné . Souhaitez-vous le corriger avec la valeur calculée de <b>" + ZConst.FORMAT_DECIMAL.format(calculatedReste)
						+ "</b> ?",
						ZMsgPanel.BTLABEL_NO)) {
					try {
						ecd.setEcdResteEmarger(calculatedReste);
						getEditingContext().saveChanges();
						getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
								getEditingContext().globalIDForObject(ecd)
						}));
						myPanel.getEcritureDetailListPanel().getMyTableModel().fireTableDataChanged();

					} catch (Exception e) {
						getEditingContext().revert();
						setWaitCursor(false);
						showErrorDialog(e);
					}
				}
			}
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		}
		setWaitCursor(false);
	}

	private final void creerEmargement() {
		try {
			final EOEcritureDetail ecritureDetail = myPanel.getEcritureDetailListPanel().getselectedObject();
			if (ecritureDetail == null) {
				throw new Exception("Aucune ligne sélectionnée.");
			}
			if (ecritureDetail.ecdResteEmarger().doubleValue() <= 0) {
				throw new Exception("Le reste à émarger est égal à 0.");
			}

			EmargementSaisieCtrl emargementSaisieCtrl = new EmargementSaisieCtrl(getEditingContext());
			emargementSaisieCtrl.openDialog(getMyDialog(), ecritureDetail);

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void exporterExcel2() {
		try {
			ExportFactoryClient exportFactoryClient = new ExportFactoryClient();
			File file = exportFactoryClient.showFileSelectDialog("xls", "Fichier Excel");
			if (file != null) {

				NSArray sorts = new NSArray(
						new Object[] {
								new EOSortOrdering("ecriture.ecrNumero", EOSortOrdering.CompareAscending),
								new EOSortOrdering("ecdIndex", EOSortOrdering.CompareAscending)
						}
						);

				NSArray headers = new NSArray(new Object[] {
						"N° Ecriture", "Type journal", "Date", "Code gestion", "Compte", "Libelle compte", "Libelle ecriture", "Debit", "Credit", "Reste a emarger", "Convention"
				});
				NSArray attributs = new NSArray(new Object[] {
						"ecriture.ecrNumero",
						"ecriture.typeJournal.tjoLibelle",
						"ecriture.ecrDateSaisie",
						"gestion.gesCode",
						"planComptable.pcoNum",
						"planComptable.pcoLibelle",
						"ecdLibelle",
						"ecdDebit",
						"ecdCredit",
						"ecdResteEmarger",
						EOEcritureDetail.TO_ACCORDS_CONTRAT_KEY + "." + EOAccordsContrat.NUMERO_KEY
				});

				EOFetchSpecification fetchSpecification = new EOFetchSpecification(EOEcritureDetail.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters, "ecriture.")), null, true, true, null);
				fetchSpecification.setPrefetchingRelationshipKeyPaths(new NSArray("ecriture"));

				exportFactoryClient.exportToExcelFromFetchSpec(getMyDialog(), file, attributs, headers, fetchSpecification, sorts, null);

				//Vérifier que le fichier a bien ete cree
				try {
					if (!file.exists()) {
						throw new Exception("Le fichier " + file.getAbsolutePath() + " n'existe pas.");
					}
				} catch (Exception e) {
					throw new Exception(e.getMessage());
				}

				if (!showConfirmationDialog("Confirmation", "Le fichier " + file.getAbsolutePath() + " a été enregistré. Souhaitez-vous l'ouvrir ?", ZMsgPanel.BTLABEL_NO)) {
					return;
				}
				myApp.openFile(file.getAbsolutePath());

			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public final class ActionClose extends AbstractAction {

		public ActionClose() {
			super("Fermer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			fermer();
		}

	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionNew extends AbstractAction {
		public ActionNew() {
			super("Nouveau");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_NEW_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Saisir une nouvelle écriture");
			setEnabled(ACTION_SAISIE.isEnabled());
		}

		/**
		 * Appelle updateData();
		 *
		 * @throws DefaultClientException
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				ZActionCtrl.checkActionWithExeStat(ACTION_SAISIE);
				ecritureNew();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	public final class ActionImprimer extends AbstractAction {

		public ActionImprimer() {
			super("Imprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer les écritures sélectionnées");
			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			imprimerEcriture();
		}

	}

	public final class ActionRectifierEcriture extends AbstractAction {

		public ActionRectifierEcriture() {
			super("Rectifier");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_TRAITEMENT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rectifier l'écriture sélectionnée");
			setEnabled(false);
		}

		/**
		 * @throws DefaultClientException
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			try {
				lanceRectifierEcriture(myPanel.getSelectedEcriture());
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}
	}

	public final class ActionAnnulerEcriture extends AbstractAction {

		public ActionAnnulerEcriture() {
			super("Annuler");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_TRAITEMENT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler l'écriture sélectionnée");
			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			annulerEcriture();
		}
	}

	public final class ActionCheckResteEmarger extends AbstractAction {

		public ActionCheckResteEmarger() {
			super("Reste à émarger");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CHECKED_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Vérifier et éventuellement corriger le reste à émarger de la ligne sélectionnée");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			checkResteEmarger();
		}
	}

	public final class ActionVoirEmargements extends AbstractAction {

		public ActionVoirEmargements() {
			super("Emargements");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EMARGMENT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Visualiser les émargements créés à partir de l'écriture sélectionnée");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			openEmargements();
		}
	}

	public final class ActionNewEmargement extends AbstractAction {

		public ActionNewEmargement() {
			super("Nv Emarg.");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EMARGMENT_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouvel émargement à partir de la ligne sélectionnée");
			setEnabled(true);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			creerEmargement();
		}
	}

	public final class ActionExportExcel2 extends AbstractAction {

		public ActionExportExcel2() {
			super("Exporter");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXCEL_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Exporter vers Excel");
			setEnabled(false);
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			exporterExcel2();
		}
	}

	private final class EcritureSrchPanelListener implements EcritureSrchPanel.IEcritureSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getEcritureDetails()
		 */
		public NSArray getEcritureDetails() {
			return EcritureSrchCtrl.this.getEcritureDetails();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionClose()
		 */
		public Action actionClose() {
			return actionClose;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionNew()
		 */
		public Action actionNew() {
			return actionNew;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getEcritures()
		 */
		public NSArray getEcritures() {
			return EcritureSrchCtrl.this.getEcritures();
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getActionImprimer()
		 */
		public Action getActionImprimer() {
			return actionImprimer;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			onEcritureSelectionChanged();

		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionRectifier()
		 */
		public Action actionRectifier() {
			return actionRectifierEcriture;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionAnnulerEcriture()
		 */
		public Action actionAnnulerEcriture() {
			return actionAnnulerEcriture;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getBalanceProvider()
		 */
		public IZPanelBalanceProvider getBalanceProvider() {
			return panelBalanceEcritureProvider;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionVoirEmargements()
		 */
		public Action actionVoirEmargements() {
			return actionVoirEmargements;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getTypeJournalModel()
		 */
		public ZEOComboBoxModel getTypeJournalModel() {
			return modelTypeJournal;
		}

		/**
		 * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#actionExportExcel2()
		 */
		public Action actionExportExcel2() {
			return actionExportExcel2;
		}

		public Action actionNewEmargement() {
			return actionNewEmargement;
		}

		public Action actionCheckResteEmarger() {
			return actionCheckResteEmarger;
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.ecritures.ui.EcritureSrchPanel.IEcritureSrchPanelListener#getActionImprimerAll()
		//         */
		//        public Action getActionImprimerAll() {
		//            return actionImprimerAll;
		//        }
		//
	}

	/**
	 * Precharge un tableau d'ecritures.
	 *
	 * @param ecrituresTmp tableau d'EOEcriture
	 */
	private final void loadEcritures(final NSArray ecrituresTmp) {
		if (ecrituresTmp != null && ecrituresTmp.count() > 0) {
			NSMutableArray ecrNums = new NSMutableArray();
			for (int i = 0; i < ecrituresTmp.count(); i++) {
				EOEcriture element = (EOEcriture) ecrituresTmp.objectAtIndex(i);
				ecrNums.addObject(element.ecrNumero());
			}
			myFilters.put("ecrNumeroList", ecrNums);
			try {
				myPanel.updateData();
			} catch (Exception e) {
				myFilters.put("ecrNumeroList", null);
				//                myFilters.clear();
				showErrorDialog(e);
			} finally {
				myFilters.clear();
			}
		}
	}

	private final class ZPanelBalanceEcritureProvider implements ZPanelBalance.IZPanelBalanceProvider {
		/**
		 * @return la valeur des debits.
		 */
		public BigDecimal getDebitValue() {
			BigDecimal val = new BigDecimal(0);
			if (myPanel.getSelectedEcritures().count() == 1) {
				NSArray tmp = myPanel.getSelectedEcriture().detailEcriture();
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (array_element.ecdDebit() != null) {
						val = val.add(array_element.ecdDebit());
					}
				}
			}
			return val;
		}

		public BigDecimal getCreditValue() {
			BigDecimal val = new BigDecimal(0);
			if (myPanel.getSelectedEcritures().count() == 1) {
				NSArray tmp = myPanel.getSelectedEcriture().detailEcriture();
				for (int i = 0; i < tmp.count(); i++) {
					EOEcritureDetail array_element = (EOEcritureDetail) tmp.objectAtIndex(i);
					if (array_element.ecdCredit() != null) {
						val = val.add(array_element.ecdCredit());
					}
				}
			}
			return val;
		}
	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return myPanel;
	}

	public String title() {
		return TITLE;
	}

}
