/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.scol.ui;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZPanelBalance;
import org.cocktail.maracuja.client.ZPanelBalance.IZPanelBalanceProvider;
import org.cocktail.maracuja.client.common.ui.BordereauListPanel;
import org.cocktail.maracuja.client.common.ui.MandatBrouillardListPanel;
import org.cocktail.maracuja.client.common.ui.MandatListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOMandatBrouillard;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ScolBordereauRembSrchPanel extends ZKarukeraPanel {
	private IScolBordereauRembSrchPanelListener myListener;

	//    private ScolBordereauSrchFilterPanel filterPanel;
	private ScolBordereauListPanel bordereauListPanel;
	private ScolMandatListPanel mandatListPanel;
	private ScolMandatBrouillardListPanel mandatBrouillardListPanel;

	/**
	 * @param editingContext
	 */
	public ScolBordereauRembSrchPanel(IScolBordereauRembSrchPanelListener listener) {
		super();
		myListener = listener;

		//        filterPanel = new ScolBordereauSrchFilterPanel(new ScolBordereauSrchFilterPanelListener());
		bordereauListPanel = new ScolBordereauListPanel(new ScolBordereauListPanelListener());
		mandatListPanel = new ScolMandatListPanel(new ScolMandatListPanelListener());
		mandatBrouillardListPanel = new ScolMandatBrouillardListPanel(new ScolMandatBrouillardListPanelListener());
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
	 */
	public void initGUI() {
		//        filterPanel.setMyDialog(getMyDialog());
		//        filterPanel.initGUI();
		bordereauListPanel.initGUI();
		mandatListPanel.initGUI();
		mandatBrouillardListPanel.initGUI();

		this.setLayout(new BorderLayout());
		setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 0));

		JPanel tmp2 = new JPanel(new BorderLayout());
		tmp2.add(ZKarukeraPanel.buildVerticalSplitPane(
				encloseInPanelWithTitle("Mandats", null, ZConst.BG_COLOR_TITLE, mandatListPanel, null, null),
				encloseInPanelWithTitle("Brouillards", null, ZConst.BG_COLOR_TITLE, mandatBrouillardListPanel, null, null)), BorderLayout.CENTER);

		JPanel tmp = new JPanel(new BorderLayout());

		//        tmp.add(encloseInPanelWithTitle("Filtres de recherche",null,null,null,filterPanel,null,null), BorderLayout.NORTH);
		tmp.add(ZKarukeraPanel.buildVerticalSplitPane(
				encloseInPanelWithTitle("Bordereaux", null, ZConst.BG_COLOR_TITLE, bordereauListPanel, null, null),
				tmp2), BorderLayout.CENTER);

		this.add(buildRightPanel(), BorderLayout.EAST);
		this.add(tmp, BorderLayout.CENTER);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
	 */
	public void updateData() throws Exception {
		bordereauListPanel.updateData();
	}

	private final JPanel buildRightPanel() {
		JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		ArrayList list = new ArrayList();
		list.add(myListener.getActionViser());
		list.add(myListener.getActionRejeter());
		list.add(null);
		list.add(myListener.getActionImprimerBordereau());

		tmp.add(ZKarukeraDialog.buildVerticalPanelOfButtonsFromActions(list), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		return tmp;
	}

	/**
	 * @return L'objetactuelment sélectionné.
	 */
	public EOBordereau getSelectedBordereau() {
		return (EOBordereau) bordereauListPanel.selectedObject();
	}

	public EOMandat getSelectedMandat() {
		return (EOMandat) mandatListPanel.selectedObject();
	}

	public interface IScolBordereauRembSrchPanelListener {

		public Action getActionRejeter();

		public Action getActionViser();

		public Action getActionImprimerBordereau();

		/**
		 * @return un dictioniare contenant les filtres
		 */
		public HashMap getFilters();

		public Action actionSrch();

		public void onBordereauSelectionChanged();

		public NSArray getBordereaux();

		public NSArray getMandatsBrouillards();

		//        public ZEOComboBoxModel getTypesBordereauxComboboxModel();
		public void onMandatSelectionChanged();

		public NSArray getMandats();
	}

	private final class ScolBordereauListPanel extends BordereauListPanel {

		/**
		 * @param listener
		 */
		public ScolBordereauListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
			colsMap.remove(COL_UTILISATEUR);
			colsMap.put(COL_NB, colNb);
			colsMap.put(COL_MONTANT, colMontant);
		}

	}

	private final class ScolMandatBrouillardListPanel extends MandatBrouillardListPanel {
		private final ZPanelBalance myZPanelBalanceNew;

		/**
		 * @param listener
		 */
		public ScolMandatBrouillardListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
			myZPanelBalanceNew = new ZPanelBalance(new BalanceMdl());
		}

		@Override
		public void initGUI() {
			super.initGUI();
			myZPanelBalanceNew.initGUI();
			add(myZPanelBalanceNew, BorderLayout.SOUTH);
		}

		@Override
		public void updateData() throws Exception {
			super.updateData();
			myZPanelBalanceNew.updateData();
		}

		private final class BalanceMdl implements IZPanelBalanceProvider {

			public BigDecimal getDebitValue() {
				return ZEOUtilities.calcSommeOfBigDecimals(getMyDisplayGroup().allObjects(), EOMandatBrouillard.DEBIT_KEY);
			}

			public BigDecimal getCreditValue() {
				return ZEOUtilities.calcSommeOfBigDecimals(getMyDisplayGroup().allObjects(), EOMandatBrouillard.CREDIT_KEY);
			}
		}
	}

	private final class ScolMandatListPanel extends MandatListPanel {
		public final String COL_FOURNISSEUR = "firstDepense.depFournisseur";

		/**
		 * @param listener
		 */
		public ScolMandatListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			ZEOTableModelColumn colFournisseur = new ZEOTableModelColumn(myDisplayGroup, COL_FOURNISSEUR, "Fournisseur", 150);
			colFournisseur.setAlignment(SwingConstants.LEFT);

			HashMap tmp = new LinkedHashMap(colsMap.size());

			tmp.put(COL_GESTION, colsMap.get(COL_GESTION));
			tmp.put(COL_MAN_NUMERO, colsMap.get(COL_MAN_NUMERO));
			tmp.put(COL_MAN_DATE_REMISE, colsMap.get(COL_MAN_DATE_REMISE));
			tmp.put(COL_FOURNISSEUR, colFournisseur);
			tmp.put(COL_MANTTC, colsMap.get(COL_MANTTC));

			colsMap = tmp;
		}

	}

	private final class ScolBordereauListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			myListener.onBordereauSelectionChanged();

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getBordereaux();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			return;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class ScolMandatListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			myListener.onMandatSelectionChanged();

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getMandats();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			return;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class ScolMandatBrouillardListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			return;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() {
			return myListener.getMandatsBrouillards();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			return;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	//    private final class ScolBordereauSrchFilterPanelListener implements ScolBordereauSrchFilterPanel.IScolBordereauSrchFilterPanel {
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauSrchFilterPanel.IScolBordereauSrchFilterPanel#getActionSrch()
	//         */
	//        public Action getActionSrch() {
	//            return myListener.actionSrch();
	//        }
	//
	//        /**
	//         * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauSrchFilterPanel.IScolBordereauSrchFilterPanel#getFilters()
	//         */
	//        public HashMap getFilters() {
	//            return myListener.getFilters();
	//        }
	//
	////        /**
	////         * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauSrchFilterPanel.IScolBordereauSrchFilterPanel#getTypesBordereauxModel()
	////         */
	////        public ZEOComboBoxModel getTypesBordereauxModel() {
	////            return myListener.getTypesBordereauxComboboxModel();
	////        }
	////
	//    }

	/**
	 * @throws Exception
	 */
	public void updateBrouillardList() throws Exception {
		mandatBrouillardListPanel.updateData();

	}

	public void updateMandatList() throws Exception {
		mandatListPanel.updateData();

	}

}
