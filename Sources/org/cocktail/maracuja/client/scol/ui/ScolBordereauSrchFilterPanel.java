/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.scol.ui;

import java.awt.BorderLayout;
import java.util.HashMap;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;

import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ScolBordereauSrchFilterPanel extends ZKarukeraPanel {
//    private final Color BORDURE_COLOR=getBackground().brighter();
    
    private IScolBordereauSrchFilterPanel myListener;
    

//    private JComboBox typesBordereaux;
    
    /**
     * @param editingContext
     */
    public ScolBordereauSrchFilterPanel(IScolBordereauSrchFilterPanel listener) {
        super();
        myListener = listener;
    }

    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#initGUI()
     */
    public void initGUI() {
        setLayout(new BorderLayout());
        setBorder(ZKarukeraPanel.createMargin());
        add(buildFilters(), BorderLayout.CENTER);
        add(new JButton(myListener.getActionSrch() ), BorderLayout.EAST);
    }

    
    


    private final JPanel buildFilters() {
//        typesBordereaux = new JComboBox(myListener.getTypesBordereauxModel());

        JPanel p = new JPanel(new BorderLayout());
//        Component[] comps = new Component[1];
//        comps[0] = buildLine(new ZLabeledComponent("Type de bordereau ", typesBordereaux, ZLabeledComponent.LABELONLEFT, 125));
//        
//        p.add(ZKarukeraPanel.buildLine(comps), BorderLayout.WEST);
//        p.add(new JPanel(new BorderLayout()));
        return p;
    }
    
   
    
    
    public interface IScolBordereauSrchFilterPanel {
        
        /**
         * @return
         */
        public Action getActionSrch();

//        /**
//         * @return
//         */
//        public ZEOComboBoxModel getTypesBordereauxModel();

        /**
         * @return un dictionnaire dans lequel sont stockés les filtres.
         */
        public HashMap getFilters();
        
        
    }




    /**
     * @see org.cocktail.maracuja.client.common.ui.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        return;
        
    }
    

    

    
}
