/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.scol.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessBordereauBrouillardGenerique;
import org.cocktail.maracuja.client.factory.process.FactoryProcessJournalEcriture;
import org.cocktail.maracuja.client.finders.EOPlanComptableExerFinder;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOComptabilite;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOOrigine;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.scol.ui.ScolBordereauAdmSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ScolBordereauAdmSrchCtrl extends CommonCtrl {
	private final String ACTION_ID_VISA = ZActionCtrl.IDU_SCOL;

	private ScolBordereauAdmSrchPanel scolBordereauAdmSrchPanel;

	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionViser actionViser = new ActionViser();
	private final ActionRejeter actionRejeter = new ActionRejeter();

	private final HashMap myFilters;

	private NSArray comptabilites;
	private NSArray gestionsforVisa;

	private EOTypeBordereau typeBord;

	private final NSArray allPlancoExersValides = EOPlanComptableExerFinder.getPlancoExerValidesWithQual(getEditingContext(), getExercice(), null, false);

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ScolBordereauAdmSrchCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		myFilters = new HashMap();
		initSubObjects();

	}

	public void initSubObjects() throws Exception {
		gestionsforVisa = new NSArray();
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_VISA)) {
			gestionsforVisa = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_VISA);
		}

		if (gestionsforVisa.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour accéder aux bordereaux de scolarité. Demandez à votre administrateur " + "de vous affecter les codes de gestions pour cette fonction");
		}

		scolBordereauAdmSrchPanel = new ScolBordereauAdmSrchPanel(new ScolBordereauAdmSrchPanelListener());
		typeBord = EOsFinder.getLeTypeBordereau(getEditingContext(), EOTypeBordereau.TypeBordereauDroitsUniversitaires);

	}

	public final NSArray getScolBordereaus() {
		//Créer la condition à partir des filtres
		try {
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("borNum", EOSortOrdering.CompareDescending);
			EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareDescending);
			NSArray res = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)), null, true);

			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					sort2, sort1
			}));

		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		}
	}

	public final EOBordereau getSelectedScolBordereau() {
		return scolBordereauAdmSrchPanel.getSelectedBordereau();
	}


	/**
	 * @see org.cocktail.maracuja.client.odp.ui.ScolBordereauRechercheFilterPanel.IScolBordereauRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			scolBordereauAdmSrchPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final NSArray bordereauxBrouillards() {
		if (scolBordereauAdmSrchPanel.getSelectedBordereau() == null) {
			return new NSArray();
		}
		return scolBordereauAdmSrchPanel.getSelectedBordereau().bordereauBrouillards();
	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[] {
			myApp.appUserInfo().getCurrentExercice()
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeBordereau=%@", new NSArray(new Object[] {
			typeBord
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("borEtat=%@", new NSArray(new Object[] {
			EOBordereau.BordereauValide
		})));

		NSMutableArray lesgestions = new NSMutableArray();
		for (int i = 0; i < gestionsforVisa.count(); i++) {
			EOGestion element = (EOGestion) gestionsforVisa.objectAtIndex(i);
			lesgestions.addObject(EOQualifier.qualifierWithQualifierFormat("gestion=%@", new NSArray(new Object[] {
				element
			})));
		}
		if (lesgestions.count() > 0) {
			quals.addObject(new EOOrQualifier(lesgestions));
		}

		return quals;
	}


	/**
	 * Active/desactive les actions en fonction de l'ecriture selectionnee.
	 */
	private final void refreshActions() {
		if (getSelectedScolBordereau() == null) {
			actionViser.setEnabled(false);
			actionRejeter.setEnabled(false);
		}
		else {
			actionRejeter.setEnabled(true && (gestionsforVisa.count() > 0) && (EOBordereau.BordereauValide.equals(getSelectedScolBordereau().borEtat())));
			actionViser.setEnabled(true && (gestionsforVisa.count() > 0) && (EOBordereau.BordereauValide.equals(getSelectedScolBordereau().borEtat())));
		}
	}

	private final void scolBordereauViser() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau scolBordereau = getSelectedScolBordereau();
			if (scolBordereau != null) {
				EOGestion gestion = scolBordereau.gestion();

				if (gestionsforVisa.indexOfObject(gestion) == NSArray.NotFound) {
					throw new DefaultClientException("Vous n'avez pas les droits de viser un bordereau pour le code de gestion " + gestion.gesCode());
				}

				if (!EOTypeBordereau.TypeBordereauDroitsUniversitaires.equals(scolBordereau.typeBordereau().tboType())) {
					throw new DefaultClientException("Le bordereau n'est pas du type " + EOTypeBordereau.TypeBordereauDroitsUniversitaires);
				}

				if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement viser le bordereau n°" + scolBordereau.borNum() + " ?", ZMsgPanel.BTLABEL_NO)) {

					if (!EOBordereau.BordereauValide.equals(scolBordereau.borEtat())) {
						throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauValide + " pour être visé.");
					}

					//
					EOComptabilite compta = scolBordereau.gestion().comptabilite();
					EOTypeOperation typeOperation = EOsFinder.getLeTypeOperation(getEditingContext(), EOTypeOperation.TYPE_OPERATION_GENERIQUE);
					EOOrigine origine = null;
					EOTypeJournal typeJournal = EOsFinder.getLeTypeJournal(getEditingContext(), EOTypeJournal.typeJournalVisaDU);

					FactoryProcessBordereauBrouillardGenerique factoryProcessBordereau = new FactoryProcessBordereauBrouillardGenerique(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					FactoryProcessJournalEcriture factoryProcessJournalEcriture = new FactoryProcessJournalEcriture(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					EOEcriture ecriture;
					try {
						Date dateVisa = null;
						//On force la date des écritures des bordereaux d'inscription (on prend la date du bordereau).
						//Le n° de bordereau dans le cas des bordereaux d'inscription est la date a l'envers
						String bornum = ZConst.FORMAT_ENTIER_BRUT.format(scolBordereau.borNum());
						int y = new Integer(bornum.substring(0, 4)).intValue();
						int m = new Integer(bornum.substring(4, 6)).intValue() - 1;
						int d = new Integer(bornum.substring(6, 8)).intValue();

						GregorianCalendar gc = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
						gc.set(GregorianCalendar.YEAR, y);
						gc.set(GregorianCalendar.MONTH, m);
						gc.set(GregorianCalendar.DAY_OF_MONTH, d);
						dateVisa = ZDateUtil.getDateOnly(gc.getTime());

						System.out.println("bornum = " + bornum + " , datevisa=" + dateVisa);

						//Verifier les comptes
						factoryProcessBordereau.verifierComptesBrouillards(scolBordereau, allPlancoExersValides);

						//Si parametre actif, on force la date des ecritures à la date du bordereau
						if (ZConst.OUI.equals(myApp.getParametres().valueForKey("SCOLADM_FORCER_DATE_VISA_DATE_BORD"))) {
							ecriture = factoryProcessBordereau.viserUnBordereau(getEditingContext(), scolBordereau, compta, myApp.appUserInfo().getCurrentExercice(), origine, typeJournal, typeOperation, myApp.appUserInfo().getUtilisateur(), new NSTimestamp(dateVisa));
						}
						else {
							ecriture = factoryProcessBordereau.viserUnBordereau(getEditingContext(), scolBordereau, compta, myApp.appUserInfo().getCurrentExercice(), origine, typeJournal, typeOperation, myApp.appUserInfo().getUtilisateur());
						}

					} catch (Exception e) {
						e.printStackTrace();
						getEditingContext().revert();
						throw e;
					}
					getEditingContext().saveChanges();

					scolBordereauAdmSrchPanel.updateData();
					String msgFin = "";
					msgFin = msgFin + "Le bordereau n° " + scolBordereau.borNum() + " a été visé.\n";
					//
					try {
						KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
						NSMutableArray nbs = new NSMutableArray();
						if ((ecriture != null)) {
							factoryProcessJournalEcriture.numeroterEcriture(getEditingContext(), ecriture, myKFactoryNumerotation);
							nbs.addObject(ecriture.ecrNumero());
						}
						if (nbs.count() > 0) {
							msgFin = msgFin + "\nLes écritures ont été numérotées : " + nbs.componentsJoinedByString(",");
						}
					} catch (Exception e) {
						e.printStackTrace();
						msgFin = msgFin + "\nErreur lors de la numérotation des écritures : \n";
						msgFin = msgFin + e.getMessage();
					}
					myApp.showInfoDialog(msgFin);
				}

			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		}

	}

	private final void scolBordereauRejeter() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau scolBordereau = getSelectedScolBordereau();
			if (scolBordereau != null) {
				EOGestion gestion = scolBordereau.gestion();

				if (gestionsforVisa.indexOfObject(gestion) == NSArray.NotFound) {
					throw new DefaultClientException("Vous n'avez pas les droits de viser/rejeter un bordereau pour le code de gestion " + gestion.gesCode());
				}

				if (!EOTypeBordereau.TypeBordereauDroitsUniversitaires.equals(scolBordereau.typeBordereau().tboType())) {
					throw new DefaultClientException("Le bordereau n'est pas du type " + EOTypeBordereau.TypeBordereauDroitsUniversitaires);
				}

				if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement rejeter le bordereau n°" + scolBordereau.borNum() + " ?", ZMsgPanel.BTLABEL_NO)) {

					if (!EOBordereau.BordereauValide.equals(scolBordereau.borEtat())) {
						throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauValide + " pour être rejeté.");
					}

					FactoryProcessBordereauBrouillardGenerique factoryProcessBordereau = new FactoryProcessBordereauBrouillardGenerique(myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
					factoryProcessBordereau.rejeterUnBordereau(getEditingContext(), scolBordereau);

					//ne pas toucher aux echeanciers associes, il sont supprimés si besoin par la scol
					//                    FactoryEcheancier factoryEcheancier = new FactoryEcheancier(myApp.wantShowTrace());
					//                    NSArray echeanciers = EOsFinder.fetchArray(getEditingContext(), EOEcheancier.ENTITY_NAME, new EOKeyValueQualifier(EOEcheancier.BORDEREAU_KEY, EOQualifier.QualifierOperatorEqual, scolBordereau), null, false);
					//                    for (int i = 0; i < echeanciers.count(); i++) {
					//						EOEcheancier echeancier = (EOEcheancier) echeanciers.objectAtIndex(i);
					//						factoryEcheancier.supprimerEcheancier(echeancier);
					//					}

				}
				getEditingContext().saveChanges();
				scolBordereauAdmSrchPanel.updateData();
			}

		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		}

	}

	public final void resetFilter() {
		myFilters.clear();
	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Viser le bordereau");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			scolBordereauViser();
		}
	}

	private final class ActionRejeter extends AbstractAction {
		public ActionRejeter() {
			super("Rejeter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rejeter le bordereau");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			scolBordereauRejeter();
		}
	}

	private final class ScolBordereauAdmSrchPanelListener implements ScolBordereauAdmSrchPanel.IScolBordereauAdmSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BordereauTaxeSrchPanel.IScolBordereauAdmSrchPanelListener#getBordereaux()
		 */
		public NSArray getBordereaux() {
			return getScolBordereaus();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BordereauTaxeSrchPanel.IScolBordereauAdmSrchPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.BordereauTaxeSrchPanel.IScolBordereauAdmSrchPanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauAdmSrchPanel.IScolBordereauAdmSrchPanelListener#getActionRejeter()
		 */
		public Action getActionRejeter() {
			return actionRejeter;
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauAdmSrchPanel.IScolBordereauAdmSrchPanelListener#getActionViser()
		 */
		public Action getActionViser() {
			return actionViser;
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauAdmSrchPanel.IScolBordereauAdmSrchPanelListener#onBordereauSelectionChanged()
		 */
		public void onBordereauSelectionChanged() {
			try {
				refreshActions();
				scolBordereauAdmSrchPanel.updateBrouillardList();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauAdmSrchPanel.IScolBordereauAdmSrchPanelListener#getBordereauxBrouillards()
		 */
		public NSArray getBordereauxBrouillards() {
			return bordereauxBrouillards();
		}

		//        /**
		//         * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauAdmSrchPanel.IScolBordereauAdmSrchPanelListener#getTypesBordereauxComboboxModel()
		//         */
		//        public ZEOComboBoxModel getTypesBordereauxComboboxModel() {
		//            return typeBordereauxScolModel;
		//        }

	}

	public ScolBordereauAdmSrchPanel getScolBordereauAdmSrchPanel() {
		return scolBordereauAdmSrchPanel;
	}

	public Dimension defaultDimension() {
		return null;
	}

	public ZAbstractPanel mainPanel() {
		return null;
	}

	public String title() {
		return null;
	}

}
