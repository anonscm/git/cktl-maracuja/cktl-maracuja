/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.scol.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaMandat;
import org.cocktail.maracuja.client.finders.EOsFinder;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ScolBordereauRembSrchCtrl extends CommonCtrl {
	private final String ACTION_ID_VISA = ZActionCtrl.IDU_SCOL;

	private ScolBordereauRembSrchPanel scolBordereauRembSrchPanel;

	private final ActionSrch actionSrch = new ActionSrch();
	private final ActionViser actionViser = new ActionViser();
	private final ActionRejeter actionRejeter = new ActionRejeter();
	private final ActionImprimerBordereau actionImprimerBordereau = new ActionImprimerBordereau();

	private HashMap myFilters;

	private NSArray comptabilites;
	private NSArray gestionsforVisa;

	//    private ZEOComboBoxModel typeBordereauxScolModel;

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public ScolBordereauRembSrchCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		myFilters = new HashMap();
		initSubObjects();

	}

	public void initSubObjects() throws Exception {
		gestionsforVisa = new NSArray();
		//          origineModel = new ZEOComboBoxModel(new NSArray(),"oriLibelle","",null);
		if (myApp.appUserInfo().isFonctionAutoriseeByActionID(myApp.getMyActionsCtrl(), ACTION_ID_VISA)) {
			gestionsforVisa = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ACTION_ID_VISA);
		}

		if (gestionsforVisa.count() == 0) {
			throw new DefaultClientException("Vous n'avez pas suffisamment de droits pour accéder aux bordereaux de scolarité. Demandez à votre administrateur " +
					"de vous affecter les codes de gestions pour cette fonction");
		}

		//          ZLogger.debug(gestionsforVisa);

		scolBordereauRembSrchPanel = new ScolBordereauRembSrchPanel(new ScolBordereauRembSrchPanelListener());

		//          NSArray lesTypesBordereauxScol = EOsFinder.getTypeBordereauxScol(getEditingContext());
		//          typeBordereauxScolModel = new ZEOComboBoxModel( lesTypesBordereauxScol, "tboLibelle", null,null  );

	}

	public final NSArray getScolBordereaus() {
		//Créer la condition à partir des filtres
		try {
			setWaitCursor(true);
			EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey("borNum", EOSortOrdering.CompareDescending);
			EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey("gestion.gesCode", EOSortOrdering.CompareAscending);
			NSArray res = EOsFinder.fetchArray(getEditingContext(), EOBordereau.ENTITY_NAME, new EOAndQualifier(buildFilterQualifiers(myFilters)), null, true);

			return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					sort2, sort1
			}));

		} catch (Exception e) {
			showErrorDialog(e);
			return new NSArray();
		} finally {
			setWaitCursor(false);
		}
	}

	public final EOBordereau getSelectedScolBordereau() {
		return (EOBordereau) scolBordereauRembSrchPanel.getSelectedBordereau();
	}

	//    private final void onScolBordereauSelectionChanged() {
	//        try {
	//            scolBordereauRembSrchPanel.updateBrouillardList();
	//            refreshActions();
	//        } catch (Exception e) {
	//           showErrorDialog(e);
	//        }
	//    }

	/**
	 * @see org.cocktail.maracuja.client.odp.ui.ScolBordereauRechercheFilterPanel.IScolBordereauRechercheFilterPanel#onSrch()
	 */
	private final void onSrch() {
		try {
			scolBordereauRembSrchPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final NSArray mandatBrouillards() {
		if (scolBordereauRembSrchPanel.getSelectedMandat() == null) {
			return new NSArray();
		}
		return EOsFinder.getMandatBrouilardsForMAndat(getEditingContext(), scolBordereauRembSrchPanel.getSelectedMandat());

	}

	private final NSArray mandats() {
		if (scolBordereauRembSrchPanel.getSelectedBordereau() == null) {
			return new NSArray();
		}
		return scolBordereauRembSrchPanel.getSelectedBordereau().mandats();

	}

	protected NSMutableArray buildFilterQualifiers(HashMap dicoFiltre) throws Exception {

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray(new Object[] {
			myApp.appUserInfo().getCurrentExercice()
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("typeBordereau=%@", new NSArray(new Object[] {
			EOsFinder.getLeTypeBordereau(getEditingContext(), EOTypeBordereau.TypeBordereauRembousrementsDroitsUniversitaires)
		})));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat("borEtat=%@ or borEtat=%@", new NSArray(new Object[] {
				EOBordereau.BordereauValide, EOBordereau.BordereauVise
		})));

		NSMutableArray lesgestions = new NSMutableArray();
		for (int i = 0; i < gestionsforVisa.count(); i++) {
			EOGestion element = (EOGestion) gestionsforVisa.objectAtIndex(i);
			lesgestions.addObject(EOQualifier.qualifierWithQualifierFormat("gestion=%@", new NSArray(new Object[] {
				element
			})));
		}
		if (lesgestions.count() > 0) {
			quals.addObject(new EOOrQualifier(lesgestions));
		}

		return quals;
	}

	//
	//	private final NSArray getChequesValidesOrVisesForBordereau(final EOBordereau bordereau) {
	//        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("bobEtat=%@ or bobEtat=%@ ", new NSArray(new Object[]{EOBordereauBrouillard.bordereauBrouillardValide, EOBordereauBrouillard.bordereauBrouillardVise}));
	//        return  EOQualifier.filteredArrayWithQualifier(bordereau.cheques(),qual);
	//	}

	/**
	 * Active/desactive les actions en fonction de l'ecriture selectionnee.
	 */
	private final void refreshActions() {
		if (getSelectedScolBordereau() == null) {
			actionViser.setEnabled(false);
			actionRejeter.setEnabled(false);
			actionImprimerBordereau.setEnabled(false);
		}
		else {
			actionRejeter.setEnabled(true && (gestionsforVisa.count() > 0) && (EOBordereau.BordereauValide.equals(getSelectedScolBordereau().borEtat())));
			actionViser.setEnabled(true && (gestionsforVisa.count() > 0) && (EOBordereau.BordereauValide.equals(getSelectedScolBordereau().borEtat())));
			actionImprimerBordereau.setEnabled(getSelectedScolBordereau().isBordereauDepensePaye());
		}
	}

	private final void scolBordereauViser() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau scolBordereau = getSelectedScolBordereau();
			if (scolBordereau != null) {
				EOGestion gestion = scolBordereau.gestion();

				if (gestionsforVisa.indexOfObject(gestion) == NSArray.NotFound) {
					throw new DefaultClientException("Vous n'avez pas les droits de viser un bordereau pour le code de gestion " + gestion.gesCode());
				}

				if (!EOTypeBordereau.TypeBordereauRembousrementsDroitsUniversitaires.equals(scolBordereau.typeBordereau().tboType())) {
					throw new DefaultClientException("Le bordereau n'est pas du type " + EOTypeBordereau.TypeBordereauRembousrementsDroitsUniversitaires);
				}

				if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement viser le bordereau n°" + scolBordereau.borNum() + " ?", ZMsgPanel.BTLABEL_NO)) {
					if (!EOBordereau.BordereauValide.equals(scolBordereau.borEtat())) {
						throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauValide + " pour être visé.");
					}
				}

				setWaitCursor(true);

				EOTypeJournal myTypeJournal = EOsFinder.getLeTypeJournal(getEditingContext(), EOTypeJournal.typeJournalVisaRembDU);
				EOTypeOperation myTypeOperation = EOsFinder.getLeTypeOperation(getEditingContext(), EOTypeOperation.TYPE_OPERATION_GENERIQUE);

				KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());

				//Pas de bordereau de rejet dans le cas d'un visa de rembst scol, le bordereau est rejeté en entuer ou pas.
				FactoryProcessVisaMandat myFactoryProcessVisaMandat = new FactoryProcessVisaMandat(getEditingContext(), myTypeJournal, myTypeOperation, myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()), false);

				//test
				System.out.println(scolBordereau.mandats());

				myFactoryProcessVisaMandat.viserUnBordereauDeMandatEtNumeroter(getEditingContext(), myApp.appUserInfo().getUtilisateur(), scolBordereau, new NSArray(), scolBordereau.mandats(), new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.nowAsDate())), myKFactoryNumerotation);

				getEditingContext().saveChanges();

				//Si c'est bien enregistre, on numerote et on croise les doigts
				NSArray lesEcritures = new NSArray();

				try {
					lesEcritures = myFactoryProcessVisaMandat.numeroterUnBordereauDeMandat(getEditingContext(), scolBordereau, myKFactoryNumerotation, myApp.wantShowTrace());
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU...");
					e.printStackTrace();
					throw new DefaultClientException("Erreur lors de la numérotation des écritures du bordereau. Les écritures ont été générées mais non numérotées. Elles seront automatiquement numérotées à la prochaine saisie d'écriture. : \n" + e.getMessage());
				}
				String msgEcritures = "";
				if (lesEcritures != null && lesEcritures.count() > 0) {
					lesEcritures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcritures, new NSArray(new EOSortOrdering("ecrNumero", EOSortOrdering.CompareAscending)));
					NSMutableArray lesEcrituresNumeros = new NSMutableArray();
					for (int i = 0; i < lesEcritures.count(); i++) {
						EOEcriture element = (EOEcriture) lesEcritures.objectAtIndex(i);
						lesEcrituresNumeros.addObject(element.ecrNumero());
					}
					msgEcritures = "Les écritures suivantes ont été générées : " + lesEcrituresNumeros.componentsJoinedByString(",") + "\n";
				}

				String msgFin = "Opération de visa du bordereau réussie.\n\n";
				msgFin = msgFin + msgEcritures;
				showInfoDialog(msgFin);

				getEditingContext().revert();
			}
			else {
				throw new DefaultClientException("Aucun bordereau sélectionné.");
			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		} finally {
			setWaitCursor(false);
		}
	}

	private final void scolBordereauRejeter() {
		//Vérifier si l'utilisateur a bien les droits
		try {
			EOBordereau scolBordereau = getSelectedScolBordereau();
			if (scolBordereau != null) {
				EOGestion gestion = scolBordereau.gestion();

				if (gestionsforVisa.indexOfObject(gestion) == NSArray.NotFound) {
					throw new DefaultClientException("Vous n'avez pas les droits de viser/rejeter un bordereau pour le code de gestion " + gestion.gesCode());
				}

				if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement rejeter le bordereau n°" + scolBordereau.borNum() + " ?", ZMsgPanel.BTLABEL_NO)) {

					if (!EOBordereau.BordereauValide.equals(scolBordereau.borEtat())) {
						throw new DefaultClientException("Le bordereau doit être à l'état " + EOBordereau.BordereauValide + " pour être rejeté.");
					}

					scolBordereau.setBorEtat(EOBordereau.BordereauAnnule);
					getEditingContext().saveChanges();

					scolBordereauRembSrchPanel.updateData();
				}

			}
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
		}

	}

	public final void resetFilter() {
		myFilters.clear();
	}

	private final void scolBordereauImprimerOdp() {
		try {
			EOBordereau scolBordereau = getSelectedScolBordereau();
			if (scolBordereau != null) {
				//Vérifier si le bordereau est payé
				boolean paye = true;
				NSArray mandats = scolBordereau.mandats();
				for (int i = 0; i < mandats.count() && paye; i++) {
					EOMandat element = (EOMandat) mandats.objectAtIndex(i);
					paye = paye && EOMandat.mandatVirement.equals(element.manEtat());
				}
				if (!paye) {
					throw new DataCheckException("Le bordereau doit être entièrement payé avant de pouvoir imprimer l'ordre de paiement correspondant.");
				}

				String filePath = ReportFactoryClient.imprimerOrdrePaiementScol(getEditingContext(), myApp.temporaryDir, myApp.getParametres(), scolBordereau, getMyDialog());
				if (filePath != null) {
					myApp.openPdfFile(filePath);
				}

			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			onSrch();
		}
	}

	private final class ActionViser extends AbstractAction {
		public ActionViser() {
			super("Viser");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Viser le bordereau");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			scolBordereauViser();
		}
	}

	private final class ActionRejeter extends AbstractAction {
		public ActionRejeter() {
			super("Rejeter");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Rejeter le bordereau");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			scolBordereauRejeter();

		}
	}

	private final class ActionImprimerBordereau extends AbstractAction {
		public ActionImprimerBordereau() {
			super("Imprimer");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer l'ordre de paiement correspondant (seulement lorsque le paiement est effectué)");
			setEnabled(false);
		}

		/**
		 * Appelle updateData();
		 */
		public void actionPerformed(ActionEvent e) {
			scolBordereauImprimerOdp();

		}
	}

	private final class ScolBordereauRembSrchPanelListener implements ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener {

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.PapayeSrchPanel.IScolBordereauRembSrchPanelListener#getBordereaux()
		 */
		public NSArray getBordereaux() {
			return getScolBordereaus();
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.PapayeSrchPanel.IScolBordereauRembSrchPanelListener#getFilters()
		 */
		public HashMap getFilters() {
			return myFilters;
		}

		/**
		 * @see org.cocktail.maracuja.client.cheques.ui.PapayeSrchPanel.IScolBordereauRembSrchPanelListener#actionSrch()
		 */
		public Action actionSrch() {
			return actionSrch;
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#getActionRejeter()
		 */
		public Action getActionRejeter() {
			return actionRejeter;
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#getActionViser()
		 */
		public Action getActionViser() {
			return actionViser;
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#onBordereauSelectionChanged()
		 */
		public void onBordereauSelectionChanged() {
			try {
				refreshActions();
				scolBordereauRembSrchPanel.updateMandatList();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#getMandatsBrouillards()
		 */
		public NSArray getMandatsBrouillards() {
			return mandatBrouillards();
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#onMandatSelectionChanged()
		 */
		public void onMandatSelectionChanged() {
			try {
				scolBordereauRembSrchPanel.updateBrouillardList();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#getMandats()
		 */
		public NSArray getMandats() {
			return mandats();
		}

		/**
		 * @see org.cocktail.maracuja.client.scol.ui.ScolBordereauRembSrchPanel.IScolBordereauRembSrchPanelListener#getActionImprimerBordereau()
		 */
		public Action getActionImprimerBordereau() {
			return actionImprimerBordereau;
		}

	}

	public ScolBordereauRembSrchPanel getScolBordereauRembSrchPanel() {
		return scolBordereauRembSrchPanel;
	}

	public Dimension defaultDimension() {
		return null;
	}

	public ZAbstractPanel mainPanel() {
		return null;
	}

	public String title() {
		return null;
	}
}
