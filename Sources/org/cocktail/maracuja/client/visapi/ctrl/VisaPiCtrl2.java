/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visapi.ctrl;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ReportFactoryClient;
import org.cocktail.maracuja.client.ServerProxy;
import org.cocktail.maracuja.client.ZActionCtrl;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ctrl.CommonCtrl;
import org.cocktail.maracuja.client.common.ui.ZKarukeraDialog;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction;
import org.cocktail.maracuja.client.factories.KFactoryNumerotation;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaMandat;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaPrestationInterne;
import org.cocktail.maracuja.client.factory.process.FactoryProcessVisaTitre;
import org.cocktail.maracuja.client.finder.FinderJournalEcriture;
import org.cocktail.maracuja.client.finder.FinderVisa;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.maracuja.client.metier.EOBordereauRejet;
import org.cocktail.maracuja.client.metier.EODepense;
import org.cocktail.maracuja.client.metier.EOEcriture;
import org.cocktail.maracuja.client.metier.EOFonction;
import org.cocktail.maracuja.client.metier.EOGestion;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOPiManTit;
import org.cocktail.maracuja.client.metier.EORecette;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.maracuja.client.metier.EOTypeBordereau;
import org.cocktail.maracuja.client.metier.EOTypeJournal;
import org.cocktail.maracuja.client.metier.EOTypeOperation;
import org.cocktail.maracuja.client.visapi.ui.VisaPiStep1;
import org.cocktail.maracuja.client.visapi.ui.VisaPiStep2;
import org.cocktail.zutil.client.ZListUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Controleur pour le visa des prestations internes > 2006
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaPiCtrl2 extends CommonCtrl {
	private final EOFonction FONCTION = myApp.getFonctionByActionId(ZActionCtrl.IDU_VIPR);
	private static final String TITLE = "Visa des prestations internes";
	private static final Dimension WINDOW_DIMENSION = new Dimension(935, 650);
	/** Selection du bordereau */
	private static final String STEP1 = "step1";
	/** la selection */
	private static final String STEP2 = "step2";
	/** le recap */
	private static final String STEP3 = "step3";
	/** les actions */
	private static final String STEP4 = "step4";

	private final NSArray gestions;
	private final ZEOComboBoxModel gestionModel;

	private JPanel cardPanel;
	private CardLayout cardLayout;
	private ZKarukeraStepPanel2 currentPanel;

	//    private ZEOComboBoxModel myComptabiliteModel;

	private final HashMap stepPanels = new HashMap();

	private VisaPiStep1Ctrl visaPiStep1Ctrl;
	private VisaPiStep2Ctrl visaPiStep2Ctrl;

	private VisaPiStep1 visaPiStep1;
	private VisaPiStep2 visaPiStep2;

	private final DefaultActionClose actionCloseLoc = new DefaultActionClose();
	private final DefaultActionViser actionViser = new DefaultActionViser();

	private final HashMap mandatForTitre = new HashMap();

	private EOBordereau selectedBordereauPi;
	private EOBordereau selectedBordereauMandat;

	protected final HashMap bordCountPerGestion = new HashMap();
	//  protected final FormatGestionDisplay formatGestionDisplay = new FormatGestionDisplay();
	protected final GestionRenderer myGestionRenderer = new GestionRenderer();

	/**
	 * @param editingContext
	 * @throws Exception
	 */
	public VisaPiCtrl2(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();
		if (FONCTION == null) {
			throw new Exception("La fonction IDU_VIPR devrait exister dans la table FONCTION de la base de données.");
		}
		//            gestions = EOUtilisateurFinder.getGestionsForUtilisateurAndExerciceAndFonction(getEditingContext(),myApp.appUserInfo().getUtilisateur(), myApp.appUserInfo().getCurrentExercice(), FONCTION);
		gestions = myApp.appUserInfo().getAuthorizedGestionsForActionID(getEditingContext(), myApp.getMyActionsCtrl(), ZActionCtrl.IDU_VIPR);

		if (gestions.count() == 0) {
			throw new DefaultClientException("Vous avez un droit sur cette fonctionnalité, mais aucun code gestion associé. Demandez à un administrateur de vous associer des codes gestions pour la fonctionnalité " + FONCTION.fonLibelle());
		}

		gestionModel = new ZEOComboBoxModel(gestions, EOGestion.GES_CODE_KEY, null, null);

		int i = 0;
		bordCountPerGestion.clear();
		while (i < gestions.count()) {
			final EOGestion element = (EOGestion) gestions.objectAtIndex(i);
			final NSArray res1 = FinderVisa.lesBordereauxTitrePIAViser(getEditingContext(), (EOGestion) gestions.objectAtIndex(i), myApp.appUserInfo().getCurrentExercice());
			bordCountPerGestion.put(element.gesCode(), new Integer(res1.count()));
			i++;
		}

		visaPiStep1Ctrl = new VisaPiStep1Ctrl(getEditingContext());
		visaPiStep2Ctrl = new VisaPiStep2Ctrl(getEditingContext());
		visaPiStep1 = new VisaPiStep1(visaPiStep1Ctrl, visaPiStep1Ctrl);
		visaPiStep2 = new VisaPiStep2(visaPiStep2Ctrl, visaPiStep2Ctrl);

		stepPanels.put(STEP1, visaPiStep1);
		stepPanels.put(STEP2, visaPiStep2);
	}

	private final void initGUI() {
		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		cardPanel.setLayout(cardLayout);
		cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

		Iterator iter = stepPanels.keySet().iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			ZKarukeraStepPanel2 panel = (ZKarukeraStepPanel2) stepPanels.get(element);
			panel.setMyDialog(getMyDialog());
			panel.initGUI();
			cardPanel.add(panel, element);
		}

		changeStep(STEP1);
	}

	private final void changeStep(final String newStep) {
		cardLayout.show(cardPanel, newStep);
		currentPanel = (ZKarukeraStepPanel2) stepPanels.get(newStep);
		try {
			currentPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final EOBordereau getBordereauMandatForBordereauTitrePI(final EOBordereau b) throws Exception {

		//Récupérer la prestation affectée au titre
		final NSArray titres = b.titres();
		final ArrayList bords = new ArrayList();
		mandatForTitre.clear();

		for (int i = 0; i < titres.count(); i++) {
			final EOTitre element = (EOTitre) titres.objectAtIndex(i);

			//22/09/05 - On teste : virer ce controle car ce n'est plus utilise            
			//            Number prestId = element.prestId();
			//            if (prestId==null) {
			//                throw new DefaultClientException("Erreur la référence à la prestation est nulle pour le titre ti_numero="+element.titNumero());
			//            }

			//Récupérer le bor_ordre depense associé
			//            EOTitrePrestInterne pi = (EOTitrePrestInterne) EOsFinder.fetchObject(getEditingContext(), EOTitrePrestInterne.ENTITY_NAME, "titOrdre=%@ and exercice=%@", new NSArray(new Object[]{element.titOrdre(), getExercice()}), null,false);
			//            if (pi==null) {
			//                throw new DefaultClientException("Aucun objet TitrePrestInterne récupéré pour le tit_ordre=" + element.titOrdre());
			//            }

			//			final Number prestId = element.prestId();
			NSMutableArray quals = new NSMutableArray();
			quals.addObject(new EOKeyValueQualifier(EOPiManTit.BORDEREAU_REC_KEY, EOQualifier.QualifierOperatorEqual, element.bordereau()));
			quals.addObject(new EOKeyValueQualifier(EOPiManTit.TITRE_KEY, EOQualifier.QualifierOperatorEqual, element));
			NSArray piManTits = EOPiManTit.fetchAll(getEditingContext(), new EOAndQualifier(quals), null);
			if (piManTits.count() == 0) {
				throw new DefaultClientException("Aucun mandat récupéré sur l'exercice courant pour le titre " + element.gestion().gesCode() + "/" + element.titNumero());
			}
			if (piManTits.count() > 1) {
				throw new DefaultClientException("Plusieurs mandats récupérés sur l'exercice courant pour le titre " + element.gestion().gesCode() + "/" + element.titNumero());
			}
			final EOMandat man = ((EOPiManTit) piManTits.objectAtIndex(0)).mandat();

			//            EOMandat man = (EOMandat) EOsFinder.fetchObject(getEditingContext(), EOMandat.ENTITY_NAME, "manOrdre=%@ and exercice=%@ and bordereau.borEtat=%@", new NSArray(new Object[]{pi.manOrdre(), getExercice(), EOBordereau.BordereauValide}), null,false);
			//			final EOMandat man = (EOMandat) EOsFinder.fetchObject(getEditingContext(), EOMandat.ENTITY_NAME, "prestId=%@ and exercice=%@ and bordereau.borEtat=%@", new NSArray(new Object[] { prestId, getExercice(), EOBordereau.BordereauValide }), null, false);
			if (man == null) {
				throw new DefaultClientException("Aucun mandat récupéré sur l'exercice courant pour le titre " + element.gestion().gesCode() + "/" + element.titNumero());
			}
			//			ZLogger.debug(man);
			EOBordereau bord = man.bordereau();
			ZLogger.debug(bord);
			if ((!EOTypeBordereau.TYPE_BORDEREAU_PRESTATION_INTERNE_D.equals(bord.typeBordereau().tboType()))) {
				throw new DefaultClientException("Le bordereau " + bord.gescodeAndNum() + " du mandat " + man.gescodeAndNum() + " n'est pas de type 'prestation interne dépense' pour le titre " + element.gescodeAndNum());
			}
			mandatForTitre.put(element, man);

			ZLogger.debug(bord);
			if (!bords.contains(bord)) {
				bords.add(bord);
			}
		}
		if (bords.size() == 0) {
			throw new DefaultClientException("Aucun bordereau de prestation interne depense récupéré sur l'exercice courant pour le bordereau de prestation interne recette " + b.gestion().gesCode() + "/" + b.borNum());
		}
		if (bords.size() > 1) {
			throw new DefaultClientException("Plusieurs bordereaux de prestation interne depenses ont été retrouvés pour le bordereau de prestation interne recette " + b.gestion().gesCode() + "/" + b.borNum());
		}

		return (EOBordereau) bords.get(0);

	}

	//    
	//    private final EOBordereau getBordereauMandatForBordereauTitrePI(final EOBordereau b) throws Exception {
	//    	
	//    	//Récupérer la prestation affectée au titre
	//    	final NSArray titres = b.titres();
	//    	final ArrayList bords = new ArrayList();
	//    	mandatForTitre.clear();
	//    	
	//    	for (int i = 0; i < titres.count(); i++) {
	//    		final EOTitre element = (EOTitre) titres.objectAtIndex(i);
	//    		
	//    		//22/09/05 - On teste : virer ce controle car ce n'est plus utilise            
	////            Number prestId = element.prestId();
	////            if (prestId==null) {
	////                throw new DefaultClientException("Erreur la référence à la prestation est nulle pour le titre ti_numero="+element.titNumero());
	////            }
	//    		
	//    		//Récupérer le bor_ordre depense associé
	////            EOTitrePrestInterne pi = (EOTitrePrestInterne) EOsFinder.fetchObject(getEditingContext(), EOTitrePrestInterne.ENTITY_NAME, "titOrdre=%@ and exercice=%@", new NSArray(new Object[]{element.titOrdre(), getExercice()}), null,false);
	////            if (pi==null) {
	////                throw new DefaultClientException("Aucun objet TitrePrestInterne récupéré pour le tit_ordre=" + element.titOrdre());
	////            }
	//    		
	//    		
	//    		final Number prestId = element.prestId();
	//    		
	////            EOMandat man = (EOMandat) EOsFinder.fetchObject(getEditingContext(), EOMandat.ENTITY_NAME, "manOrdre=%@ and exercice=%@ and bordereau.borEtat=%@", new NSArray(new Object[]{pi.manOrdre(), getExercice(), EOBordereau.BordereauValide}), null,false);
	//    		final EOMandat man = (EOMandat) EOsFinder.fetchObject(getEditingContext(), EOMandat.ENTITY_NAME, "prestId=%@ and exercice=%@ and bordereau.borEtat=%@", new NSArray(new Object[]{prestId, getExercice(), EOBordereau.BordereauValide}), null,false);
	//    		if (man==null) {
	//    			throw new DefaultClientException("Aucun mandat récupéré sur l'exercice courant pour le titre " +element.gestion().gesCode()+ "/"+ element.titNumero());
	//    		}
	//    		ZLogger.debug(man);
	//    		EOBordereau bord = man.bordereau();
	//    		ZLogger.debug(bord);
	//    		if (bord==null || (!EOTypeBordereau.TYPE_BORDEREAU_PRESTATION_INTERNE_D.equals(bord.typeBordereau().tboType()))) {
	//    			throw new DefaultClientException("Aucun bordereau de prestation interne dépense récupéré pour le titre " +element.gestion().gesCode()+ "/"+ element.titNumero() );
	//    		}
	//    		mandatForTitre.put(element, man);
	//    		
	//    		ZLogger.debug(bord);
	//    		if (!bords.contains(bord)) {
	//    			bords.add(bord);
	//    		}
	//    	}
	//    	if (bords.size()==0) {
	//    		throw new DefaultClientException("Aucun bordereau de prestation interne depense récupéré sur l'exercice courant pour le bordereau de prestation interne recette " + b.gestion().gesCode() + "/" + b.borNum() );
	//    	}
	//    	if (bords.size()>1) {
	//    		throw new DefaultClientException("Plusieurs bordereaux de prestation interne depenses ont été retrouvés pour le bordereau de prestation interne recette " + b.gestion().gesCode() + "/" + b.borNum() );
	//    	}
	//    	
	//    	
	//    	return (EOBordereau) bords.get(0);
	//    	
	//    }
	//    private final EOBordereau getBordereauMandatForBordereauTitrePI(final EOBordereau b) throws Exception {
	//        //Récupérer la prestation affectée au titre
	//        final NSArray titres = b.titres();
	//        final ArrayList bords = new ArrayList();
	//        mandatForTitre.clear();
	//        
	//        for (int i = 0; i < titres.count(); i++) {
	//            EOTitre element = (EOTitre) titres.objectAtIndex(i);
	//            
	//            //22/09/05 - On teste : virer ce controle car ce n'est plus utilise            
	////            Number prestId = element.prestId();
	////            if (prestId==null) {
	////                throw new DefaultClientException("Erreur la référence à la prestation est nulle pour le titre ti_numero="+element.titNumero());
	////            }
	//            
	//            //Récupérer le bor_ordre depense associé
	//            EOTitrePrestInterne pi = (EOTitrePrestInterne) EOsFinder.fetchObject(getEditingContext(), EOTitrePrestInterne.ENTITY_NAME, "titOrdre=%@ and exercice=%@", new NSArray(new Object[]{element.titOrdre(), getExercice()}), null,false);
	//            if (pi==null) {
	//                throw new DefaultClientException("Aucun objet TitrePrestInterne récupéré pour le tit_ordre=" + element.titOrdre());
	//            }
	//            EOMandat man = (EOMandat) EOsFinder.fetchObject(getEditingContext(), EOMandat.ENTITY_NAME, "manOrdre=%@ and exercice=%@ and bordereau.borEtat=%@", new NSArray(new Object[]{pi.manOrdre(), getExercice(), EOBordereau.BordereauValide}), null,false);
	//            if (man==null) {
	//                throw new DefaultClientException("Aucun mandat récupéré sur l'exercice courant pour le tit_ordre=" + element.titOrdre());
	//            }
	//            ZLogger.debug(man);
	//            EOBordereau bord = man.bordereau();
	//            ZLogger.debug(bord);
	//            if (bord==null || (!EOTypeBordereau.TypeBordereauPrestationInterne.equals(bord.typeBordereau().tboType()))) {
	//                throw new DefaultClientException("Aucun bordereau de prestation interne mandats récupéré pour le tit_ordre=" + element.titOrdre());
	//            }
	//            mandatForTitre.put(element, man);
	//            
	//            ZLogger.debug(bord);
	//            if (!bords.contains(bord)) {
	//                bords.add(bord);
	//            }
	//        }
	//        if (bords.size()==0) {
	//            throw new DefaultClientException("Aucun bordereau de prestation interne mandats récupéré sur l'exercice courant pour le bordereau bor_ordre=" + b.borOrdre());
	//        }
	//        if (bords.size()>1) {
	//            throw new DefaultClientException("Plusieurs bordereaux de prestation interne depenses ont été retrouvés pour le bordereau de titre bor_ordre=" + b.borOrdre());
	//        }
	//        
	//        
	//        return (EOBordereau) bords.get(0);
	//    }

	private final void next_STEP1() {
		try {
			selectedBordereauPi = visaPiStep1.getSelectedBordereau();
			ZLogger.debug(STEP2);
			if (selectedBordereauPi == null) {
				throw new DefaultClientException("Aucun bordereau n'est sélectionné.");
			}
			selectedBordereauMandat = getBordereauMandatForBordereauTitrePI(selectedBordereauPi);

			checkBordererauPiValide(selectedBordereauPi, selectedBordereauMandat);

			visaPiStep2Ctrl.setDateReception(null);

			//Verifier la cohérence du bordereau

			visaPiStep2Ctrl.initMaps();
			changeStep(STEP2);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void next_STEP2() {
		try {
			ZLogger.debug(STEP3);
			changeStep(STEP3);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void prev_STEP2() {
		try {
			ZLogger.debug(STEP1);
			changeStep(STEP1);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final ZKarukeraDialog createModalDialog(Window dial) {
		ZKarukeraDialog win;
		if (dial instanceof Dialog) {
			win = new ZKarukeraDialog((Dialog) dial, TITLE, true);
		}
		else {
			win = new ZKarukeraDialog((Frame) dial, TITLE, true);
		}
		setMyDialog(win);
		initGUI();
		cardPanel.setPreferredSize(WINDOW_DIMENSION);
		win.setContentPane(cardPanel);
		win.pack();
		return win;
	}

	/**
	 * Ouvre un dialog de recherche.
	 */
	public final void openDialog(Window dial) {
		ZKarukeraDialog win = createModalDialog(dial);
		this.setMyDialog(win);
		try {
			win.open();
		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			win.dispose();
		}
	}

	private void checkInfos() throws Exception {
		if (visaPiStep2Ctrl.getDateReception() == null) {
			throw new DataCheckException("Veuillez indiquer une date de réception.");
		}

		//Balayer les titres décochés et vérifier qu'ils ont bien un motif de rejet indiqué
		Enumeration iter = visaPiStep2Ctrl.getTitres().objectEnumerator();
		while (iter.hasMoreElements()) {
			EOTitre element = (EOTitre) iter.nextElement();
			if (!Boolean.TRUE.equals(visaPiStep2Ctrl.allcheckTitresMap.get(element))) {
				if (visaPiStep2Ctrl.allRejetTitresMap.get(element) == null || ((String) visaPiStep2Ctrl.allRejetTitresMap.get(element)).length() == 0) {
					throw new DataCheckException("Veuillez indiquer un motif de rejet pour le titre " + element.titNumero() + " ou bien cocher le titre de manière à l'accepter.");
				}
			}
		}

		ZLogger.debug(visaPiStep2Ctrl.allcheckTitresMap);
		ZLogger.debug(visaPiStep2Ctrl.allcheckMandatsMap);

		Enumeration iter2 = visaPiStep2Ctrl.getTitres().objectEnumerator();
		while (iter2.hasMoreElements()) {
			EOTitre element = (EOTitre) iter2.nextElement();
			if (!visaPiStep2Ctrl.allcheckTitresMap.get(element).equals(visaPiStep2Ctrl.allcheckMandatsMap.get(mandatForTitre.get(element)))) {
				throw new DefaultClientException("Erreur : un des mandats n'est pas synchronisé avec le titre correpondant.");
			}
		}

	}

	private final void viser() {
		try {

			visaPiStep2.stopCellEditing();

			checkInfos();
			final EOTypeJournal typeJournalDepense = FinderJournalEcriture.leTypeJournalVisaPrestationInterne(getEditingContext());
			final EOTypeJournal typeJournalRecette = typeJournalDepense;

			final EOTypeOperation typeOperationDepense = FinderJournalEcriture.leTypeOperationVisaPrestationInterne(getEditingContext());
			final EOTypeOperation typeOperationRecette = typeOperationDepense;

			final EOBordereau bordereauRecette = selectedBordereauPi;
			final EOBordereau bordereauDepense = selectedBordereauMandat;
			//            final EOBordereau bordereauDepense = selectedBordereauMandat;

			final NSArray mandatsAcceptes = visaPiStep2Ctrl.getMandatsAcceptes();
			final NSArray mandatsRejetes = visaPiStep2Ctrl.getMandatsRejetes();

			final NSArray titresAcceptes = visaPiStep2Ctrl.getTitresAcceptes();
			final NSArray titresRejetes = visaPiStep2Ctrl.getTitresRejetes();

			ZLogger.debug("titresAcceptes");
			ZLogger.debug(titresAcceptes);
			ZLogger.debug("mandatsAcceptes");
			ZLogger.debug(mandatsAcceptes);

			ZLogger.debug("titresRejetes");
			ZLogger.debug(titresRejetes);
			ZLogger.debug("mandatsRejetes");
			ZLogger.debug(mandatsRejetes);

			if (mandatsAcceptes.count() != titresAcceptes.count()) {
				throw new DefaultClientException("Erreur : le nombre de titres acceptés n'est pas égal au nombre de mandats acceptés.");
			}

			if (mandatsRejetes.count() != titresRejetes.count()) {
				throw new DefaultClientException("Erreur : le nombre de titres rejetés n'est pas égal au nombre de mandats rejetés.");
			}

			FactoryProcessVisaPrestationInterne factoryProcessVisaPrestationInterne = new FactoryProcessVisaPrestationInterne(getEditingContext(), typeJournalDepense, typeJournalRecette, typeOperationDepense, typeOperationRecette, myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()),
					false);

			NSArray bordereauxRejets = factoryProcessVisaPrestationInterne.viserBordereauxEtNumeroter(getEditingContext(),
					getUtilisateur(),
					new NSTimestamp(visaPiStep2Ctrl.getDateReception()),
					new KFactoryNumerotation(myApp.wantShowTrace()),
					bordereauDepense, mandatsRejetes, mandatsAcceptes,
					bordereauRecette, titresRejetes, titresAcceptes
					);

			EOBordereauRejet bordereauRejetDepense = null;
			EOBordereauRejet bordereauRejetRecette = null;

			if (bordereauxRejets != null) {
				bordereauRejetDepense = (EOBordereauRejet) bordereauxRejets.objectAtIndex(0);
				bordereauRejetRecette = (EOBordereauRejet) bordereauxRejets.objectAtIndex(1);
			}

			//Affecter les motifs de rejet et supprimer les factures associées
			for (int i = 0; i < mandatsRejetes.count(); i++) {
				EOMandat element = (EOMandat) mandatsRejetes.objectAtIndex(i);
				element.setManMotifRejet((String) visaPiStep2Ctrl.getAllRejetMandatsMap().get(element));
				NSArray factures = element.depenses();
				for (int j = 0; j < factures.count(); j++) {
					EODepense depense = (EODepense) factures.objectAtIndex(j);
					depense.setDepSuppression(EODepense.DEP_SUPPRESSION_OUI);
				}
			}
			for (int i = 0; i < titresRejetes.count(); i++) {
				EOTitre element = (EOTitre) titresRejetes.objectAtIndex(i);
				element.setTitMotifRejet((String) visaPiStep2Ctrl.getAllRejetTitresMap().get(element));
				NSArray recettes = element.recettes();
				for (int j = 0; j < recettes.count(); j++) {
					EORecette recette = (EORecette) recettes.objectAtIndex(j);
					recette.setRecSuppression(EORecette.REC_SUPPRESSION_OUI);
				}
			}

			try {
				getEditingContext().saveChanges();
			} catch (Exception e) {
				getEditingContext().revert();
				e.printStackTrace();
				throw e;
			}

			KFactoryNumerotation myKFactoryNumerotation = new KFactoryNumerotation(myApp.wantShowTrace());
			NSArray lesEcritures = new NSMutableArray();

			////////////////////////////////////

			try {
				FactoryProcessVisaTitre factoryProcessVisaTitre = new FactoryProcessVisaTitre(getEditingContext(), typeJournalRecette, typeOperationRecette, myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()));
				;
				//Si c'est bien enregistre, on numerote et on croise les doigts
				NSArray lesEcrituresTitre = new NSArray();
				try {
					lesEcrituresTitre = factoryProcessVisaTitre.numeroterUnBordereauDeTitre(getEditingContext(), bordereauRecette, myKFactoryNumerotation, myApp.wantShowTrace());
					((NSMutableArray) lesEcritures).addObjectsFromArray(lesEcrituresTitre);
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU...");
					e.printStackTrace();
					throw new DefaultClientException("Erreur lors de la numérotation des écritures du bordereau. Les écritures ont été générées mais non numérotées, et le bordereau n'a pas été passé coté ordonnateur. (Bord num " + bordereauRecette.borNum() + " du "
							+ bordereauRecette.gestion().gesCode() + " - bor_ordre=" + bordereauRecette.borOrdre() + "): \n" + e.getMessage());
				}

				try {
					if (bordereauRejetDepense != null) {
						factoryProcessVisaTitre.numeroterUnBordereauDeTitreRejet(getEditingContext(), bordereauRejetRecette, myKFactoryNumerotation, myApp.wantShowTrace());
					}
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU REJET...");
					e.printStackTrace();
					throw new DefaultClientException("Erreur lors de la numérotation du bordereau de rejet. Les écritures ont été générées et numérotées, mais le bordereau n'a pas été passé coté ordonnateur. (Bord num " + bordereauRecette.borNum() + " du " + bordereauRecette.gestion().gesCode()
							+ " - bor_ordre=" + bordereauRecette.borOrdre() + "): \n" + e.getMessage());
				}
			} catch (Exception e) {
				throw e;
			}

			try {
				FactoryProcessVisaMandat factoryProcessVisaMandat = new FactoryProcessVisaMandat(getEditingContext(), typeJournalDepense, typeOperationDepense, myApp.wantShowTrace(), new NSTimestamp(getDateJourneeComptable()), false);
				;
				//Si c'est bien enregistre, on numerote et on croise les doigts
				NSArray lesEcrituresMandat = new NSArray();
				try {
					lesEcrituresMandat = factoryProcessVisaMandat.numeroterUnBordereauDeMandat(getEditingContext(), bordereauDepense, myKFactoryNumerotation, myApp.wantShowTrace());
					((NSMutableArray) lesEcritures).addObjectsFromArray(lesEcrituresMandat);
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU...");
					e.printStackTrace();
					throw new DefaultClientException("Erreur lors de la numérotation des écritures du bordereau. Les écritures ont été générées mais non numérotées, et le bordereau n'a pas été passé coté ordonnateur. (Bord num " + bordereauDepense.borNum() + " du "
							+ bordereauDepense.gestion().gesCode() + " - bor_ordre=" + bordereauDepense.borOrdre() + "): \n" + e.getMessage());
				}

				try {
					if (bordereauRejetDepense != null) {
						factoryProcessVisaMandat.numeroterUnBordereauDeMandatRejet(getEditingContext(), bordereauRejetDepense, myKFactoryNumerotation, myApp.wantShowTrace());
					}
				} catch (Exception e) {
					System.out.println("ERREUR LORS DE LA NUMEROTATION BORDEREAU REJET...");
					e.printStackTrace();
					throw new DefaultClientException("Erreur lors de la numérotation du bordereau de rejet. Les écritures ont été générées et numérotées, mais le bordereau n'a pas été passé coté ordonnateur. (Bord num " + bordereauDepense.borNum() + " du " + bordereauDepense.gestion().gesCode()
							+ " - bor_ordre=" + bordereauDepense.borOrdre() + "): \n" + e.getMessage());
				}
			} catch (Exception e1) {
				throw e1;
			}

			boolean wantImprimer = false;

			String msgEcritures = "";
			if (lesEcritures != null && lesEcritures.count() > 0) {
				lesEcritures = EOSortOrdering.sortedArrayUsingKeyOrderArray(lesEcritures, new NSArray(new EOSortOrdering("ecrNumero", EOSortOrdering.CompareAscending)));
				NSMutableArray lesEcrituresNumeros = new NSMutableArray();
				for (int i = 0; i < lesEcritures.count(); i++) {
					EOEcriture element = (EOEcriture) lesEcritures.objectAtIndex(i);
					lesEcrituresNumeros.addObject(element.ecrNumero());
				}
				msgEcritures = "Les écritures suivantes ont été générées : " + lesEcrituresNumeros.componentsJoinedByString(",") + "\n";
			}

			String msgFin = "Opération de visa des bordereaux réussie.\n\n";
			msgFin = msgFin + msgEcritures;
			if (bordereauRejetDepense != null) {
				msgFin = msgFin + "Les bordereaux de rejet générés portent les numéros " + bordereauRejetDepense.brjNum() + " et " + bordereauRejetRecette.brjNum();
				msgFin = msgFin + " \nSouhaitez-vous lesimprimer ?";
				wantImprimer = showConfirmationDialog("Visa réussi", msgFin, ZMsgPanel.BTLABEL_YES);
			}
			else {
				msgFin = msgFin + "Aucun bordereau de rejet n'a été généré";
				showInfoDialog(msgFin);
			}

			if (wantImprimer) {
				try {
					String filePath = ReportFactoryClient.imprimerBtmna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(bordereauRejetDepense));
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}
				try {
					String filePath = ReportFactoryClient.imprimerBttna(myApp.editingContext(), myApp.temporaryDir, myApp.getParametres(), new NSArray(bordereauRejetRecette));
					if (filePath != null) {
						myApp.openPdfFile(filePath);
					}
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}

			ServerProxy.clientSideRequestAfaireApresTraitementVisaBordereau(getEditingContext(), bordereauRecette);
			//            ServerProxy.clientSideRequestAfaireApresTraitementVisaBordereau(getEditingContext(), bordereauDepense );

			//on sort de l'assistant
			getEditingContext().revert();

			//revnir à l'étape 1

			prev_STEP2();

		} catch (Exception e) {
			revertChanges();
			showErrorDialog(e);
		}

	}

	private final void close() {
		revertChanges();
		getMyDialog().onCloseClick();
	}

	private class VisaPiStep1Ctrl extends CommonCtrl implements VisaPiStep1.IVisaPiStep1Model, VisaPiStep1.IVisaPiStep1Listener {
		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final ComboboxGestionListener comboboxGestionListener = new ComboboxGestionListener();

		/**
		 * @param editingContext
		 */
		public VisaPiStep1Ctrl(EOEditingContext editingContext) {
			super(editingContext);
			actionPrev = new DefaultActionPrev();
			actionNext = new ActionNext();
			actionSpecial1 = new DefaultActionViser();
			actionNext.setEnabled(true);

		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep1.IVisaPiStep1Model#getcomboBoxGestionModel()
		 */
		public ZEOComboBoxModel getcomboBoxGestionModel() {
			return gestionModel;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep1.IVisaPiStep1Model#getBordereaux()
		 */
		public NSArray getBordereaux() {
			NSArray res = FinderVisa.lesBordereauxTitrePIAViser(getEditingContext(), (EOGestion) gestionModel.getSelectedEObject(), getExercice());
			return res;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep1.IVisaPiStep1Model#getcomboBoxGestionListener()
		 */
		public ActionListener getcomboBoxGestionListener() {
			return comboboxGestionListener;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionCloseLoc;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionViser;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep1.IVisaPiStep1Listener#onBordereauSelectionChanged()
		 */
		public void onBordereauSelectionChanged() {

		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep1.IVisaPiStep1Listener#onBordereauDbClick()
		 */
		public void onBordereauDbClick() {

		}

		private final class ComboboxGestionListener implements ActionListener {

			/**
			 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
			 */
			public void actionPerformed(ActionEvent e) {
				try {
					setWaitCursor(true);
					visaPiStep1.updateData();
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				} finally {
					setWaitCursor(false);
				}
			}

		}

		private final class ActionNext extends DefaultActionNext {
			public void actionPerformed(ActionEvent e) {
				next_STEP1();
			}
		}

		public ListCellRenderer getGestionRenderer() {
			return myGestionRenderer;
		}

		public Dimension defaultDimension() {
			return WINDOW_DIMENSION;
		}

		public ZAbstractPanel mainPanel() {
			return visaPiStep1;
		}

		public String title() {
			return TITLE;
		}

	}

	private class VisaPiStep2Ctrl extends CommonCtrl implements VisaPiStep2.IVisaPiStep2Model, VisaPiStep2.IVisaPiStep2Listener {
		private final ZStepAction actionPrev;
		private final ZStepAction actionNext;
		private final ZStepAction actionSpecial1;
		private final HashMap allcheckMandatsMap = new HashMap();
		private final HashMap allcheckTitresMap = new HashMap();
		private final HashMap allRejetTitresMap = new HashMap();
		private final HashMap allRejetMandatsMap = new HashMap();
		private Date dateReception;

		/**
		 * @param editingContext
		 */
		public VisaPiStep2Ctrl(EOEditingContext editingContext) {
			super(editingContext);
			actionPrev = new ActionPrev();
			actionNext = new DefaultActionNext();
			actionSpecial1 = new DefaultActionViser();
			actionPrev.setEnabled(true);
			actionNext.setEnabled(false);
			actionSpecial1.setEnabled(true);

		}

		//        private final class ActionViser extends DefaultActionViser {
		//            /**
		//             * @see org.cocktail.maracuja.client.visapi.ctrl.VisaPiCtrl.DefaultActionViser#actionPerformed(java.awt.event.ActionEvent)
		//             */
		//            public void actionPerformed(ActionEvent e) {
		//                if (visaPiStep2.stopCellEditing()) {
		//                    super.actionPerformed(e);
		//                }
		//
		//            }
		//        }
		private final class ActionPrev extends DefaultActionPrev {
			public void actionPerformed(ActionEvent e) {
				visaPiStep2.cancelCellEditing();
				prev_STEP2();
			}
		}

		public final void initMaps() {
			NSArray res = selectedBordereauMandat.mandats();
			//            final NSArray res = selectedBordereauPi.mandats();
			allcheckMandatsMap.clear();
			for (int i = 0; i < res.count(); i++) {
				EOMandat element = (EOMandat) res.objectAtIndex(i);
				allcheckMandatsMap.put(element, Boolean.TRUE);
			}
			final NSArray res2 = selectedBordereauPi.titres();
			allcheckTitresMap.clear();
			allRejetTitresMap.clear();
			allRejetMandatsMap.clear();
			for (int i = 0; i < res2.count(); i++) {
				EOTitre element = (EOTitre) res2.objectAtIndex(i);
				allcheckTitresMap.put(element, Boolean.TRUE);
				setRejet(element, null);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getMandats()
		 */
		public NSArray getMandats() throws Exception {
			//            return selectedBordereauPi.mandats();
			return selectedBordereauMandat.mandats();
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getTitres()
		 */
		public NSArray getTitres() {
			return selectedBordereauPi.titres();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionPrev()
		 */
		public ZStepAction actionPrev() {
			return actionPrev;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionNext()
		 */
		public ZStepAction actionNext() {
			return actionNext;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionClose()
		 */
		public ZStepAction actionClose() {
			return actionCloseLoc;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial1()
		 */
		public ZStepAction actionSpecial1() {
			return actionSpecial1;
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepListener#actionSpecial2()
		 */
		public ZStepAction actionSpecial2() {
			return null;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getAllCheckMandatsMap()
		 */
		public HashMap getAllCheckMandatsMap() {
			return allcheckMandatsMap;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getAllCheckTitresMap()
		 */
		public HashMap getAllCheckTitresMap() {
			return allcheckTitresMap;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Listener#onTitreCheckChanged(org.cocktail.maracuja.client.metier.EOTitre)
		 */
		public void onTitreCheckChanged(EOTitre titre) {
			allcheckMandatsMap.put(mandatForTitre.get(titre), allcheckTitresMap.get(titre));
			if (Boolean.TRUE.equals(allcheckTitresMap.get(titre))) {
				//                allRejetTitresMap.put(titre, null);
				setRejet(titre, null);

			}
			visaPiStep2.setSaisieRejetEditable(!Boolean.TRUE.equals(allcheckTitresMap.get(titre)));
		}

		private final void setRejet(EOTitre titre, String rejet) {
			allRejetTitresMap.put(titre, rejet);
			allRejetMandatsMap.put(mandatForTitre.get(titre), rejet);
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getMandatBrouillards()
		 */
		public NSArray getMandatBrouillards() {
			if (visaPiStep2.getSelectedMandat() == null) {
				return null;
			}
			EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("mabSens", EOSortOrdering.CompareDescending);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(visaPiStep2.getSelectedMandat().mandatBrouillards(), new NSArray(sort));
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getTitreBrouillards()
		 */
		public NSArray getTitreBrouillards() {
			if (visaPiStep2.getSelectedTitre() == null) {
				return null;
			}
			EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey("tibSens", EOSortOrdering.CompareDescending);
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(visaPiStep2.getSelectedTitre().titreBrouillards(), new NSArray(sort));
			//                return visaPiStep2.getSelectedTitre().titreBrouillards();
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Listener#onMandatSelectionChanged()
		 */
		public void onMandatSelectionChanged() {
			try {
				visaPiStep2.updateDataMandatBrouillard();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Listener#onTitreSelectionChanged()
		 */
		public void onTitreSelectionChanged() {
			try {
				visaPiStep2.updateDataTitreBrouillard();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getAllRejetTitresMap()
		 */
		public HashMap getAllRejetTitresMap() {
			return allRejetTitresMap;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Listener#synchroniseRejet(org.cocktail.maracuja.client.metier.EOTitre)
		 */
		public void synchroniseRejet(EOTitre titre) {
			allRejetMandatsMap.put(mandatForTitre.get(titre), allRejetTitresMap.get(titre));
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Model#getAllRejetMandatsMap()
		 */
		public HashMap getAllRejetMandatsMap() {
			return allRejetMandatsMap;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Listener#getDateReception()
		 */
		public Date getDateReception() {
			return dateReception;
		}

		/**
		 * @see org.cocktail.maracuja.client.visapi.ui.VisaPiStep2.IVisaPiStep2Listener#setDateReception(java.util.Date)
		 */
		public void setDateReception(Date date) {
			dateReception = date;

		}

		public final NSArray getMandatsAcceptes() {
			ArrayList list = ZListUtil.getKeyListForValue(getAllCheckMandatsMap(), Boolean.TRUE);
			return new NSArray(list.toArray());
		}

		public final NSArray getTitresAcceptes() {
			ArrayList list = ZListUtil.getKeyListForValue(getAllCheckTitresMap(), Boolean.TRUE);
			return new NSArray(list.toArray());
		}

		public final NSArray getMandatsRejetes() {
			ArrayList list = ZListUtil.getKeyListForValue(getAllCheckMandatsMap(), Boolean.FALSE);
			return new NSArray(list.toArray());
		}

		public final NSArray getTitresRejetes() {
			ArrayList list = ZListUtil.getKeyListForValue(getAllCheckTitresMap(), Boolean.FALSE);
			return new NSArray(list.toArray());
		}

		public Dimension defaultDimension() {
			return WINDOW_DIMENSION;
		}

		public ZAbstractPanel mainPanel() {
			return visaPiStep2;
		}

		public String title() {
			return TITLE;
		}

	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionViser extends ZStepAction {
		public DefaultActionViser() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			putValue(Action.NAME, "Viser");
			putValue(Action.SHORT_DESCRIPTION, "Viser les bordereaux");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			viser();
		}

	}

	private class DefaultActionNext extends ZStepAction {
		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(true);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	private class GestionRenderer extends DefaultListCellRenderer {

		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			String gestion = (String) value;
			final Integer nb = (Integer) bordCountPerGestion.get(gestion);
			return super.getListCellRendererComponent(list, formatGestion(gestion, nb), index, isSelected, cellHasFocus);
		}

		private String formatGestion(final String code, final Integer nb) {
			final StringBuffer sb = new StringBuffer();
			sb.append(code);
			sb.append("        ");
			sb.append((nb != null && nb.intValue() != 0 ? nb.toString() : ""));

			//le code html pose des pbs de refresh dans la liste déroulante... dommage
			//            sb.append("<html><table cellpadding=0 cellspacing=0><tr><td width=70>");
			//            sb.append(code);
			//            sb.append("</td><td>");
			//            sb.append((nb!=null && nb.intValue()!=0 ? nb : null));
			//            sb.append("</td></tr></table></html>");
			//            
			return sb.toString();
		}

	}

	/**
	 * Verifie si un bordereau de prestation interne est valide (a partir de 2007) Compare les mandats aux titres (nombre, montant)
	 * 
	 * @param bordereau
	 * @throws DefaultClientException
	 */
	private void checkBordererauPiValide(final EOBordereau bordereauRec, final EOBordereau bordereauDep) throws DefaultClientException {
		if (bordereauRec.exercice().exeExercice().intValue() >= ZConst.EXE_EXERCICE_2007) {
			//comparer le nombre de mandats avec le nombre de titres
			if (bordereauDep.mandats() == null || bordereauDep.mandats().count() == 0) {
				throw new DefaultClientException("Aucun mandat affecté au bordereau selectionné.");
			}
			if (bordereauRec.titres() == null || bordereauRec.titres().count() == 0) {
				throw new DefaultClientException("Aucun titre affecté au bordereau selectionné.");
			}
			if (bordereauRec.titres().count() != bordereauDep.mandats().count()) {
				throw new DefaultClientException("Nombre de titres (" + bordereauRec.titres().count() + ") différent du nombre de mandats (" + bordereauDep.mandats().count() + ").");
			}
			final NSArray titres = bordereauRec.titres();
			final NSArray mandats = bordereauDep.mandats();

			//Vérifier si les titres sont cohérents avec les mandats
			final ArrayList prestIds = new ArrayList();
			for (int i = 0; i < titres.count(); i++) {
				final EOTitre element = (EOTitre) titres.objectAtIndex(i);
				if (element.prestId() == null) {
					throw new DefaultClientException("Titre n° " + element.titNumero().intValue() + " : Référence a la prestation interne non presente (PREST_ID)");
				}
				if (prestIds.indexOf(element.prestId()) > -1) {
					throw new DefaultClientException("Plusieurs Titres contiennent une référence à la même prestaton (PREST_ID)");
				}
				prestIds.add(element.prestId());
			}

			//Vérifier que les mandats ont les mêmes ref que les titres pour les prestid
			for (int i = 0; i < titres.count(); i++) {
				final EOMandat element = (EOMandat) mandats.objectAtIndex(i);
				if (element.prestId() == null) {
					throw new DefaultClientException("Mandat n° " + element.manNumero().intValue() + " : Référence a la prestation interne non presente (PREST_ID)");
				}
				//Vérifier que la prestation existe dans les titres
				if (prestIds.indexOf(element.prestId()) == -1) {
					throw new DefaultClientException("Mandat n°" + element.manNumero().intValue() + " : Référence a une prestation interne non presente dans celle des titres");
				}
			}

		}

	}

	public Dimension defaultDimension() {
		return WINDOW_DIMENSION;
	}

	public ZAbstractPanel mainPanel() {
		return currentPanel;
	}

	public String title() {
		return TITLE;
	}

}
