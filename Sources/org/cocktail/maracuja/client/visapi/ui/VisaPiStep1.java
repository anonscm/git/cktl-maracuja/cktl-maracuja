/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visapi.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import org.cocktail.maracuja.client.common.ui.BordereauListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOBordereau;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;

import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaPiStep1 extends ZKarukeraStepPanel2 {
	private final VBordereauListPanelListener bordereauListPanelListener = new VBordereauListPanelListener();
	private final VBordereauListPanel bordereauListPanel = new VBordereauListPanel(bordereauListPanelListener);
	private JComboBox comboBoxGestion;
	private IVisaPiStep1Model model;
	private IVisaPiStep1Listener listener;

	/**
	 * @param listener
	 */
	public VisaPiStep1(IVisaPiStep1Model m, IVisaPiStep1Listener l) {
		super(m);
		model = m;
		listener = l;
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		bordereauListPanel.initGUI();
		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {
		JPanel p = new JPanel(new BorderLayout());
		p.add(buildFiltersPanel(), BorderLayout.NORTH);
		p.add(bordereauListPanel, BorderLayout.CENTER);
		return p;
	}

	public EOBordereau getSelectedBordereau() {
		return (EOBordereau) bordereauListPanel.selectedObject();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return "Choix du bordereau";
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return "Veuillez sélectionner un bordereau à viser";
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		bordereauListPanel.updateData();
	}

	private final JPanel buildFiltersPanel() {
		comboBoxGestion = new JComboBox(model.getcomboBoxGestionModel());
		comboBoxGestion.addActionListener(model.getcomboBoxGestionListener());
		comboBoxGestion.setRenderer(model.getGestionRenderer());
		comboBoxGestion.setMaximumRowCount(18);
		comboBoxGestion.setPreferredSize(new Dimension(150, 20));
		final JLabel label = new JLabel("Code gestion ");

		final Box box = Box.createHorizontalBox();
		box.add(label);
		box.add(comboBoxGestion);

		final JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(2, 4, 2, 4));
		panel.add(box, BorderLayout.WEST);
		panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		//        panel.add(ZKarukeraPanel.buildHorizontalPanelOfComponent(new Component[]{label, comboBoxGestion, new JPanel(new BorderLayout())}), BorderLayout.CENTER);
		return panel;
	}

	private final class VBordereauListPanel extends BordereauListPanel {

		/**
		 * @param listener
		 */
		public VBordereauListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);

			colsMap.clear();
			colsMap.put(COL_GESTION, gesCode);
			colsMap.put(COL_BORNUM, borNum);
			//colsMap.put(COL_BORLIBELLE, borLibelle);
			colsMap.put(COL_BORETAT, borEtat);
			colsMap.put(COL_borDateVisa, borDateVisa);
			colsMap.put(COL_UTILISATEUR, colUtilisateur);

			colsMap.put(COL_NB, colNb);
			colsMap.put(COL_MONTANT, colMontant);
		}

	}

	private final class VBordereauListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			listener.onBordereauSelectionChanged();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() {
			return model.getBordereaux();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
			listener.onBordereauDbClick();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	public interface IVisaPiStep1Model extends ZStepListener {
		public ZEOComboBoxModel getcomboBoxGestionModel();

		public ListCellRenderer getGestionRenderer();

		public NSArray getBordereaux();

		public ActionListener getcomboBoxGestionListener();
	}

	public interface IVisaPiStep1Listener {
		public void onBordereauSelectionChanged();

		public void onBordereauDbClick();
	}

}
