/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.maracuja.client.visapi.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.cocktail.fwkcktlcomptaguiswing.client.all.ZConst;
import org.cocktail.maracuja.client.ZIcon;
import org.cocktail.maracuja.client.common.ui.MandatBrouillardListPanel;
import org.cocktail.maracuja.client.common.ui.MandatListPanel;
import org.cocktail.maracuja.client.common.ui.TitreBrouillardListPanel;
import org.cocktail.maracuja.client.common.ui.TitreListPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraPanel;
import org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2;
import org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener;
import org.cocktail.maracuja.client.metier.EOMandat;
import org.cocktail.maracuja.client.metier.EOTitre;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField.IZDatePickerFieldModel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;

import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class VisaPiStep2 extends ZKarukeraStepPanel2 {
	private JComboBox comboBoxGestion;
	private IVisaPiStep2Model model;
	private IVisaPiStep2Listener listener;

	private VisaTitreListPanel visaTitreListPanel;
	private VisaTitreBrouillardListPanel visaTitreBrouillardListPanel;

	private VisaMandatListPanel visaMandatListPanel;
	private VisaMandatBrouillardListPanel visaMandatBrouillardListPanel;

	private VisaTitreListPanelListener visaTitreListPanelListener;
	private VisaMandatListPanelListener visaMandatListPanelListener;
	private VisaMandatBrouillardListPanelListener visaMandatBrouillardListPanelListener;
	private VisaTitreBrouillardListPanelListener visaTitreBrouillardListPanelListener;

	private ZDatePickerField dateReceptionField;

	/**
	 * @param listener
	 */
	public VisaPiStep2(IVisaPiStep2Model m, IVisaPiStep2Listener l) {
		super(m);
		model = m;
		listener = l;

		visaTitreListPanelListener = new VisaTitreListPanelListener();
		visaMandatListPanelListener = new VisaMandatListPanelListener();
		visaMandatBrouillardListPanelListener = new VisaMandatBrouillardListPanelListener();
		visaTitreBrouillardListPanelListener = new VisaTitreBrouillardListPanelListener();

		visaTitreListPanel = new VisaTitreListPanel(visaTitreListPanelListener);
		visaMandatListPanel = new VisaMandatListPanel(visaMandatListPanelListener);
		visaMandatBrouillardListPanel = new VisaMandatBrouillardListPanel(visaMandatBrouillardListPanelListener);
		visaTitreBrouillardListPanel = new VisaTitreBrouillardListPanel(visaTitreBrouillardListPanelListener);
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#initGUI()
	 */
	public void initGUI() {
		visaTitreListPanel.initGUI();
		visaMandatListPanel.initGUI();
		visaMandatBrouillardListPanel.initGUI();
		visaTitreBrouillardListPanel.initGUI();

		dateReceptionField = new ZDatePickerField(new DateReceptionFieldModel(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		dateReceptionField.getMyTexfield().setColumns(8);

		super.initGUI();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCenterPanel()
	 */
	public JPanel getCenterPanel() {

		JSplitPane panelTitresTemp = ZKarukeraPanel.buildVerticalSplitPane(visaTitreListPanel, ZKarukeraPanel.encloseInPanelWithTitle("Brouillards", null, ZConst.BG_COLOR_TITLE, visaTitreBrouillardListPanel, null, null));
		JSplitPane panelMandatsTemp = ZKarukeraPanel.buildVerticalSplitPane(visaMandatListPanel, ZKarukeraPanel.encloseInPanelWithTitle("Brouillards", null, ZConst.BG_COLOR_TITLE, visaMandatBrouillardListPanel, null, null));

		JPanel panelTitres = ZKarukeraPanel.encloseInPanelWithTitle("Titres", null, ZConst.BG_COLOR_TITLE, panelTitresTemp, null, null);
		JPanel panelMandats = ZKarukeraPanel.encloseInPanelWithTitle("Mandats", null, ZConst.BG_COLOR_TITLE, panelMandatsTemp, null, null);
		JPanel panel = new JPanel(new BorderLayout());

		panel.add(buildPanelFields(), BorderLayout.NORTH);
		panel.add(ZKarukeraPanel.buildVerticalSplitPane(panelTitres, panelMandats), BorderLayout.CENTER);
		return panel;
	}

	private final JPanel buildPanelFields() {
		return buildLine(new Component[] {
				new JLabel("Date réception :"), dateReceptionField
		});

	}

	public EOMandat getSelectedMandat() {
		return (EOMandat) visaMandatListPanel.selectedObject();
	}

	public EOTitre getSelectedTitre() {
		return (EOTitre) visaTitreListPanel.selectedObject();
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getTitle()
	 */
	public String getTitle() {
		return "Acceptation ou rejet des titres/mandats";
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraStepPanel2#getCommentaire()
	 */
	public String getCommentaire() {
		return "Veuillez cocher les titres qui seront visés. Les mandats correspondant seront automatiquement acceptés/rejetés";
	}

	/**
	 * @see org.cocktail.maracuja.client.common.ui.ZIUIComponent#updateData()
	 */
	public void updateData() throws Exception {
		dateReceptionField.updateData();
		visaTitreListPanel.updateData();
		visaTitreBrouillardListPanel.updateData();
		visaMandatListPanel.updateData();
		visaMandatBrouillardListPanel.updateData();
	}

	public interface IVisaPiStep2Model extends ZStepListener {
		public NSArray getMandats() throws Exception;

		public NSArray getTitres() throws Exception;

		public HashMap getAllCheckMandatsMap();

		public HashMap getAllCheckTitresMap();

		public NSArray getMandatBrouillards();

		public NSArray getTitreBrouillards();

		/**
		 * @return
		 */
		public HashMap getAllRejetTitresMap();

		/**
		 * @return
		 */
		public HashMap getAllRejetMandatsMap();
	}

	public interface IVisaPiStep2Listener {

		/**
		 * @param titre
		 */
		public void onTitreCheckChanged(EOTitre titre);

		/**
         *
         */
		public void onMandatSelectionChanged();

		/**
         *
         */
		public void onTitreSelectionChanged();

		/**
		 * @param titre
		 */
		public void synchroniseRejet(EOTitre titre);

		/**
		 * @return
		 */
		public Date getDateReception();

		/**
		 * @param date
		 */
		public void setDateReception(Date date);

	}

	private final class VisaTitreListPanel extends TitreListPanel {
		public static final String COL_ORGAN = "organ.longString";
		public static final String COL_COCHE = "coche";
		public static final String COL_REJET = "rejet";

		/**
		 * @param listener
		 */
		public VisaTitreListPanel(VisaTitreListPanelListener listener) {
			super(listener);
			ZEOTableModelColumn organ = new ZEOTableModelColumn(myDisplayGroup, COL_ORGAN, "Ligne budgétaire", 190);
			organ.setAlignment(SwingConstants.LEFT);

			ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDisplayGroup, "Viser", 60, listener.getAllCheckTitresMap());
			col0.setEditable(true);
			col0.setResizable(false);
			col0.setColumnClass(Boolean.class);

			ZEOTableModelColumnDico colRejet = new ZEOTableModelColumnDico(myDisplayGroup, "Motif de rejet", 120, listener.getAllRejetTitresMap());
			colRejet.setEditable(true);

			LinkedHashMap colsNew = new LinkedHashMap(colsMap.size());
			colsNew.put(COL_COCHE, col0);
			colsNew.put(COL_REJET, colRejet);

			colsNew.put(COL_GESTION, colsMap.get(COL_GESTION));
			colsNew.put(COL_BORNUM, colsMap.get(COL_BORNUM));
			colsNew.put(COL_TIT_NUMERO, colsMap.get(COL_TIT_NUMERO));
			colsNew.put(COL_ORGAN, organ);
			colsNew.put(COL_TIT_LIBELLE, colsMap.get(COL_TIT_LIBELLE));
			colsNew.put(COL_TITTTC, colsMap.get(COL_TITTTC));
			colsMap = colsNew;

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel#initTableModel()
		 */
		protected void initTableModel() {
			super.initTableModel();
			myTableModel.addTableModelListener((VisaTitreListPanelListener) myListener);
		}

		public ZEOTableModelColumn getColRejet() {
			return (ZEOTableModelColumn) colsMap.get(COL_REJET);
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel#updateData()
		 */
		public void updateData() throws Exception {
			super.updateData();
			getColRejet().setEditable(false);
		}

		public void fireColRejetOnCurrentRowChanged() {
			int i = ((Integer) myDisplayGroup.selectionIndexes().objectAtIndex(0)).intValue();
			myTableModel.fireTableRowsUpdated(i, i);
		}

	}

	private final class VisaMandatListPanel extends MandatListPanel {
		public static final String COL_COCHE = "coche";
		public static final String COL_REJET = "rejet";

		/**
		 * @param listener
		 */
		public VisaMandatListPanel(VisaMandatListPanelListener listener) {
			super(listener);

			ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDisplayGroup, "Viser", 60, listener.getAllCheckMandatsMap());
			col0.setEditable(false);
			col0.setResizable(false);
			col0.setColumnClass(Boolean.class);

			ZEOTableModelColumnDico colRejet = new ZEOTableModelColumnDico(myDisplayGroup, "Motif de rejet", 120, listener.getAllRejetMandatsMap());
			colRejet.setEditable(true);

			LinkedHashMap colsNew = new LinkedHashMap(colsMap.size());
			colsNew.put(COL_COCHE, col0);
			colsNew.put(COL_REJET, colRejet);
			colsNew.put(COL_GESTION, colsMap.get(COL_GESTION));
			colsNew.put(COL_BORNUM, colsMap.get(COL_BORNUM));
			colsNew.put(COL_MAN_NUMERO, colsMap.get(COL_MAN_NUMERO));
			colsNew.put(COL_FOURNISSEUR, colsMap.get(COL_FOURNISSEUR));
			colsNew.put(COL_MANTTC, colsMap.get(COL_MANTTC));
			colsMap = colsNew;
		}
	}

	private final class VisaMandatBrouillardListPanel extends MandatBrouillardListPanel {

		/**
		 * @param listener
		 */
		public VisaMandatBrouillardListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
		}

	}

	private final class VisaTitreBrouillardListPanel extends TitreBrouillardListPanel {

		/**
		 * @param listener
		 */
		public VisaTitreBrouillardListPanel(IZKarukeraTablePanelListener listener) {
			super(listener);
		}

	}

	private final class VisaTitreListPanelListener implements IZKarukeraTablePanelListener, TableModelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			listener.onTitreSelectionChanged();
		}

		/**
		 * @return
		 */
		public HashMap getAllRejetTitresMap() {
			return model.getAllRejetTitresMap();
		}

		/**
		 * @return un dictionnaire avec en clés les différentes lignes de la table et en valeur TRUE ou FALSE pour indiquer si la ligne est cochée ou
		 *         pas.
		 */
		public HashMap getAllCheckTitresMap() {
			return model.getAllCheckTitresMap();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return model.getTitres();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {

		}

		public void onTitreCheckChanged(EOTitre titre) {
			listener.onTitreCheckChanged(titre);
		}

		public void synchroniseRejet(EOTitre titre) {
			listener.synchroniseRejet(titre);
		}

		public void tableChanged(TableModelEvent e) {

			if (e.getType() == TableModelEvent.UPDATE) {
				int column = e.getColumn();
				int row = e.getFirstRow();
				if (column >= 0) {
					//Vérifier si on est sur la colonne des checks
					EOTitre titre = (EOTitre) visaTitreListPanel.getMyDisplayGroup().displayedObjects().objectAtIndex(row);
					if (column == 0) {
						onTitreCheckChanged(titre);
						//Rafraichir la colonne de rejet
						visaTitreListPanel.fireColRejetOnCurrentRowChanged();
					}

					synchroniseRejet(titre);
					visaMandatListPanel.fireTableDataChanged();

					//    				//Si le motif de rejet a été modifié, on décoche la ligne
					//    				else if ( ((ZEOTableModelColumn)myCols.get(column)).getAttributeName().equals("manMotifRejet")   ) {
					//    					if (myTableModel.getValueAt(row, 0).equals(Boolean.TRUE) ) {
					//    						myTableModel.setValueAt(Boolean.FALSE,row,0);
					//    						fireTableCellUpdated(row,  0  );
					//    					}
					//    				}
				}
			}

		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class VisaMandatListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
			listener.onMandatSelectionChanged();
		}

		/**
		 * @return
		 */
		public HashMap getAllRejetMandatsMap() {
			return model.getAllRejetMandatsMap();
		}

		/**
		 * @return
		 */
		public HashMap getAllCheckMandatsMap() {
			return model.getAllCheckMandatsMap();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return model.getMandats();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class VisaTitreBrouillardListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return model.getTitreBrouillards();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	private final class VisaMandatBrouillardListPanelListener implements IZKarukeraTablePanelListener {

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#selectionChanged()
		 */
		public void selectionChanged() {
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getData()
		 */
		public NSArray getData() throws Exception {
			return model.getMandatBrouillards();
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#onDbClick()
		 */
		public void onDbClick() {
		}

		/**
		 * @see org.cocktail.maracuja.client.common.ui.ZKarukeraTablePanel.IZKarukeraTablePanelListener#getTableRenderer()
		 */
		public IZEOTableCellRenderer getTableRenderer() {
			return null;
		}

	}

	public void updateDataMandatBrouillard() throws Exception {
		visaMandatBrouillardListPanel.updateData();
	}

	/**
     *
     */
	public void updateDataTitreBrouillard() throws Exception {
		visaTitreBrouillardListPanel.updateData();
	}

	public void setSaisieRejetEditable(boolean b) {
		visaTitreListPanel.getColRejet().setEditable(b);
	}

	/**
     *
     */
	public final boolean stopCellEditing() {

		return visaTitreListPanel.stopCellEditing() && visaMandatListPanel.stopCellEditing();

	}

	public final void cancelCellEditing() {
		visaTitreListPanel.cancelCellEditing();
		visaMandatListPanel.cancelCellEditing();

	}

	private final class DateReceptionFieldModel implements IZDatePickerFieldModel {

		/**
		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
		 */
		public Object getValue() {
			return listener.getDateReception();
		}

		/**
		 * @see org.cocktail.zutil.client.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
		 */
		public void setValue(Object value) {
			listener.setDateReception((Date) value);
		}

		public Window getParentWindow() {
			return getMyDialog();
		}
	}

}
