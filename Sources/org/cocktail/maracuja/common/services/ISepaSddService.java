package org.cocktail.maracuja.common.services;

/**
 * Interface permettant de mettre en commun des constantes coté serveur et client.
 * 
 * @author rprin
 */
public interface ISepaSddService {
	public static final String ECRITURES_GENEREES_GIDS_KEY = "ecrituresGenereesGids";
	public static final String EMARGEMENTS_GENEREES_GIDS_KEY = "emargementsGeneresGids";
	public static final String ERREURS_KEY = "erreurs";
	public static final String MSGS_KEY = "msgs";
	public static final String RECOUVREMENTS_GENEREES_GIDS_KEY = "recouvrementsGeneresGids";
}
