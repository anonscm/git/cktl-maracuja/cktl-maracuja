package org.cocktail.maracuja.common.sepa;

/**
 * Classe pour stocker des constantes d'options de regroupement dans la génération des fichiers de virement, utilisable coté serveur et client.
 * 
 * @author rprin
 */
public abstract class OptionsRegroupementLigneFichierVirement {
	public static final int OPTION_REGROUPEMENT_LIGNES_SANS = 0;
	public static final int OPTION_REGROUPEMENT_LIGNES_RIB = 1;
	public static final int OPTION_REGROUPEMENT_LIGNES_NO_FACTURE = 2;
}
