/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;

import java.util.List;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CreditTransferTransactionInformation extends Element {

	private static final long serialVersionUID = 1L;
	static final String TAG = "CdtTrfTxInf";

	public CreditTransferTransactionInformation(String index, String namespace) {
		super(TAG, index, namespace);
	}

	@Override
	public String getTag() {
		return TAG;
	}

	public PaymentIdentification getPaymentIdentification() {
		return (PaymentIdentification) getChild(PaymentIdentification.TAG);
	}

	public void setPaymentIdentification(PaymentIdentification paymentIdentification) {
		addContent(paymentIdentification);
	}

	public Amount getAmount() {
		return (Amount) getChild(Amount.TAG);
	}

	public void setAmount(Amount amount) {
		addContent(amount);
	}

	public CreditorAgent getCreditorAgent() {
		return (CreditorAgent) getChild(CreditorAgent.TAG);
	}

	public void setCreditorAgent(CreditorAgent creditorAgent) {
		addContent(creditorAgent);
	}

	public Creditor getCreditor() {
		return (Creditor) getChild(Creditor.TAG);
	}

	public void setCreditor(Creditor creditor) {
		addContent(creditor);
	}

	public CreditorAccount getCreditorAccount() {
		return (CreditorAccount) getChild(CreditorAccount.TAG);
	}

	public void setCreditorAccount(CreditorAccount creditorAccount) {
		addContent(creditorAccount);

	}

	public Purpose getPurpose() {
		return (Purpose) getChild(Purpose.TAG);
	}

	public void setPurpose(Purpose purpose) {
		addContent(purpose);

	}

	public UltimateDebtor getUltimateDebtor() {
		return (UltimateDebtor) getChild(UltimateDebtor.TAG);
	}

	/**
	 * Déconseillé ici (plutot dans PaymentInformation)
	 * 
	 * @param ultimateDebtor
	 */
	public void setUltimateDebtor(UltimateDebtor ultimateDebtor) {
		addContent(ultimateDebtor);

	}

	public UltimateCreditor getUltimateCreditor() {
		return (UltimateCreditor) getChild(UltimateCreditor.TAG);
	}

	public void setUltimateCreditor(UltimateCreditor ultimateDebtor) {
		addContent(ultimateDebtor);
	}

	public RemittanceInformation getRemittanceInformation() {
		return (RemittanceInformation) getChild(RemittanceInformation.TAG);
	}

	public void setRemittanceInformation(RemittanceInformation remittanceInformation) {
		addContent(remittanceInformation);
	}

	public void addToRegulatoryReportings(RegulatoryReporting regulatoryReporting) {
		addContent(regulatoryReporting);
	}

	public List<RegulatoryReporting> getRegulatoryReportings() {
		return getChildren(RegulatoryReporting.TAG);
	}

	@Override
	public void validate() throws ValidateException {
		checkRequired(PaymentIdentification.TAG);
		checkRequired(Amount.TAG);
		checkRequired(CreditorAgent.TAG);
		checkRequired(Creditor.TAG);
		checkRequired(CreditorAccount.TAG);

		if (getRegulatoryReportings().size() > 1) {
			throw new ValidateException(getRegulatoryReportings().get(0), RegulatoryReporting.TAG + "limité à une seule occurence.");
		}

		super.validate();
	}

}
