
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;


/**
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public class DirectDebitTransactionInformation extends Element {
	
	private static final long serialVersionUID = 1L;
	static final String TAG = "DrctDbtTxInf";

	public PaymentIdentification getpPaymentIdentification() {
		return (PaymentIdentification) getChild(PaymentIdentification.TAG);
	}
	
	public void setPaymentIdentification(PaymentIdentification paymentIdentification) {
		addContent(paymentIdentification);
	}

	public InstructedAmount getInstructedAmount() {
		return (InstructedAmount) getChild(InstructedAmount.TAG);
	}

	public void setInstructedAmount(InstructedAmount instructedAmount) {
		addContent(instructedAmount);
	}

	public DirectDebitTransaction getDirectDebitTransaction() {
		return (DirectDebitTransaction) getChild(DirectDebitTransaction.TAG);
	}
	
	public void setDirectDebitTransaction(DirectDebitTransaction directDebitTransaction) {
		addContent(directDebitTransaction);
	}

	public DebtorAgent getDebtorAgent() {
		return (DebtorAgent) getChild(DebtorAgent.TAG);
	}

	public void setDebtorAgent(DebtorAgent debtorAgent) {
		addContent(debtorAgent);
	}

	public Debtor getDebtor() {
		return (Debtor) getChild(Debtor.TAG);
	}

	public void setDebtor(Debtor debtor) {
		addContent(debtor);
	}

	public DebtorAccount getDebtorAccount() {
		return (DebtorAccount) getChild(DebtorAccount.TAG);
	}

	public void setDebtorAccount(DebtorAccount debtorAccount) {
		addContent(debtorAccount);
	}

	public UltimateDebtor getUltimateDebtor() {
		return (UltimateDebtor) getChild(UltimateDebtor.TAG);
	}

	public void setUltimateDebtor(UltimateDebtor ultimateDebtor) {
		addContent(ultimateDebtor);
	}

	public Purpose getPurpose() {
		return (Purpose) getChild(Purpose.TAG);
	}

	public void setPurpose(Purpose purpose) {
		addContent(purpose);
	}
	
	public RemittanceInformation getRemittanceInformation() {
		return (RemittanceInformation) getChild(RemittanceInformation.TAG);
	}

	public void setRemittanceInformation(RemittanceInformation remittanceInformation) {
		addContent(remittanceInformation);
	}

	public DirectDebitTransactionInformation(String index, String namespace) {
		super(TAG, index, namespace);
	}

	@Override
	public String getTag() {
		return TAG;
	}

	/**.
	 * Vérifie la cohérence
	 * de la valeur de l'identification et le nombre d'occurence
	 *
	 * @throws ValidateException
	 */
	protected final void checkIdentification(Identification debId) throws ValidateException {
		if (debId != null) {
			
			if (debId != null) {
				if (debId.getOrganisationIdentification() != null && debId.getPrivateIdentification() != null) {
					throw new ValidateException(debId, "Seule une occurence de " + OrganisationIdentification.TAG + " ou " + PrivateIdentification.TAG + " est permise.");
				}
				if (debId.getOrganisationIdentification() == null && debId.getPrivateIdentification() == null) {
					throw new ValidateException(debId, "Une occurence de " + OrganisationIdentification.TAG + " ou " + PrivateIdentification.TAG + " est requise.");
				}
			}
			
			if ((debId.getOrganisationIdentification().getTextTrim() != ""
					&& debId.getOrganisationIdentification().getTextTrim() != "BICorBEI")
					|| (debId.getOrganisationIdentification().getTextTrim() == "" 
							&& debId.getOrganisationIdentification().getOther() == null)) {
				throw new ValidateException(getDebtor().getIdentification(), "Sous " + OrganisationIdentification.TAG + ", soit la donnée BICorBEI soit une seule occurrence de Other est autorisée.");
			}
			if ((debId.getPrivateIdentification().getTextTrim() != ""
					&& debId.getPrivateIdentification().getTextTrim() != "DateandPlaceofBirth")
					|| (debId.getPrivateIdentification().getTextTrim() == "" 
							&& debId.getPrivateIdentification().getOther() == null)) {
				throw new ValidateException(getDebtor().getIdentification(), "Sous " + PrivateIdentification.TAG + ", soit la donnée DateandPlaceofBirth soit une seule occurrence de Other est autorisée.");
			}
			
		}
	}
	
	@Override
	public void validate() throws ValidateException {
		checkRequired(PaymentIdentification.TAG);
		checkRequired(InstructedAmount.TAG);
		checkRequired(DirectDebitTransaction.TAG);
		checkRequired(DebtorAgent.TAG);
		checkRequired(Debtor.TAG);
		checkIdentification(getDebtor().getIdentification());
		if (getUltimateDebtor() != null) {
			checkIdentification(getUltimateDebtor().getIdentification());
		}
		super.validate();
	}
}
