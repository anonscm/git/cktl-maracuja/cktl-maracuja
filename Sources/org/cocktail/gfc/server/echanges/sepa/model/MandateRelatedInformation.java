
/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;


/**
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public class MandateRelatedInformation extends Element {

	private static final long serialVersionUID = 1L;
	static final String TAG = "MndtRltdInf";
	private static final int MNDTID_MAX_LENGTH = 35;
	
	public MandateRelatedInformation(String index, String namespace) {
		super(TAG, index, namespace);
	}
	
	public MandateRelatedInformation(MandateIdentification mandateIdentification, DateOfSignature dateOfSignature,
			AmendmentIndicator amendmentIndicator, String index, String namespace) {
		super(TAG, index, namespace);
		setMandateIdentification(mandateIdentification);
		setDateOfSignature(dateOfSignature);
		setAmendmentIndicator(amendmentIndicator);
	}

	public MandateRelatedInformation(MandateIdentification mandateIdentification, DateOfSignature dateOfSignature,
			AmendmentIndicator amendmentIndicator, AmendmentInformationDetails amendmentInformationDetails, String index, String namespace) {
		super(TAG, index, namespace);
		setMandateIdentification(mandateIdentification);
		setDateOfSignature(dateOfSignature);
		setAmendmentIndicator(amendmentIndicator);
		setAmendmentInformationDetails(amendmentInformationDetails);
	}

	public MandateIdentification getMandateIdentification() {
		return (MandateIdentification) getChild(MandateIdentification.TAG);
	}

	public void setMandateIdentification(MandateIdentification mandateIdentification) {
		addContent(mandateIdentification);
	}

	public DateOfSignature getDateOfSignature() {
		return (DateOfSignature) getChild(DateOfSignature.TAG);
	}

	public void setDateOfSignature(DateOfSignature dateOfSignature) {
		addContent(dateOfSignature);
	}
	
	public AmendmentIndicator getAmendmentIndicator() {
		return (AmendmentIndicator) getChild(AmendmentIndicator.TAG);
	}

	public void setAmendmentIndicator(AmendmentIndicator amendmentIndicator) {
		addContent(amendmentIndicator);
	}

	public AmendmentInformationDetails getAmendmentInformationDetails() {
		return (AmendmentInformationDetails) getChild(AmendmentInformationDetails.TAG);
	}

	public void setAmendmentInformationDetails(AmendmentInformationDetails amendmentInformationDetails) {
		addContent(amendmentInformationDetails);
	}
	
	@Override
	public String getTag() {
		return TAG;
	}

	@Override
	public void validate() throws ValidateException {
		checkRequired(MandateIdentification.TAG);
		getMandateIdentification().checkLength(MNDTID_MAX_LENGTH);
		checkRequired(DateOfSignature.TAG);
		checkRequired(AmendmentIndicator.TAG);
		if (getAmendmentIndicator().TRUE.equals(getAmendmentIndicator().getTextTrim())) {
			checkRequired(AmendmentInformationDetails.TAG);
		}
		else {
			checkEmpty(AmendmentInformationDetails.TAG);
		}
			
		super.validate();
	}
}

