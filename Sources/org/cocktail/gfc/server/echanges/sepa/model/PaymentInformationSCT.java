/*
* Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software
* is governed by the CeCILL license under French law and abiding by the
* rules of distribution of free software. You can use, modify and/or
* redistribute the software under the terms of the CeCILL license as
* circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
* As a counterpart to the access to the source code and rights to copy, modify
* and redistribute granted by the license, users are provided only with a
* limited warranty and the software's author, the holder of the economic
* rights, and the successive licensors have only limited liability. In this
* respect, the user's attention is drawn to the risks associated with loading,
* using, modifying and/or developing or reproducing the software by the user
* in light of its specific status of free software, that may mean that it
* is complicated to manipulate, and that also therefore means that it is
* reserved for developers and experienced professionals having in-depth
* computer knowledge. Users are therefore encouraged to load and test the
* software's suitability as regards their requirements in conditions enabling
* the security of their systems and/or data to be ensured and, more generally,
* to use and operate it in the same conditions as regards security. The
* fact that you are presently reading this means that you have had knowledge
* of the CeCILL license and that you accept its terms.
*/
package org.cocktail.gfc.server.echanges.sepa.model;

import java.util.List;

/**
* @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
*/
public class PaymentInformationSCT extends PaymentInformation {

	private static final long serialVersionUID = 1L;
	
	public PaymentInformationSCT(String index, String namespace) {
		super(index, namespace);
	}
	
	public PaymentTypeInformationSCT getPaymentTypeInformationSCT() {
		return (PaymentTypeInformationSCT) getChild(PaymentTypeInformation.TAG);
	}

	public void setPaymentTypeInformationSCT(PaymentTypeInformationSCT paymentTypeInformationSCT) {
		addContent(paymentTypeInformationSCT);
	}


	public RequestedExecutionDate getRequestedExecutionDate() {
	return (RequestedExecutionDate) getChild(RequestedExecutionDate.TAG);
	}
	
	public void setRequestedExecutionDate(RequestedExecutionDate requestedExecutionDate) {
	addContent(requestedExecutionDate);
	}
	
	public Debtor getDebtor() {
	return (Debtor) getChild(Debtor.TAG);
	}
	
	public void setDebtor(Debtor debtor) {
	addContent(debtor);
	}
	
	public DebtorAccount getDebtorAccount() {
	return (DebtorAccount) getChild(DebtorAccount.TAG);
	}
	
	public void setDebtorAccount(DebtorAccount debtorAccount) {
	addContent(debtorAccount);
	}
	
	public DebtorAgent getDebtorAgent() {
	return (DebtorAgent) getChild(DebtorAgent.TAG);
	}
	
	public void setDebtorAgent(DebtorAgent debtorAgent) {
	addContent(debtorAgent);
	}
	
	public void addToCreditTransferTransactionInformations(CreditTransferTransactionInformation creditTransferTransactionInformation) {
	addContent(creditTransferTransactionInformation);
	}
	
	public List<CreditTransferTransactionInformation> getCreditTransferTransactionInformations() {
	return getChildren(CreditTransferTransactionInformation.TAG);
	}
	
	@Override
	public String getTag() {
	return TAG;
	}
	
	@Override
	public void validate() throws ValidateException {
		checkRequired(RequestedExecutionDate.TAG);
		checkRequired(Debtor.TAG);
		checkRequired(DebtorAccount.TAG);
		checkRequired(DebtorAgent.TAG);
		checkRequired(CreditTransferTransactionInformation.TAG);
		super.validate();
	}

}