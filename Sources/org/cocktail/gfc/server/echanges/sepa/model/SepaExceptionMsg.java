/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;

public class SepaExceptionMsg {
	public static final String SEPA_REQUIRED = "Seul SEPA ou vide sont autorisées";
	public static final String TRF_REQUIRED = "Seul TRF est autorisé";
	public static final String SLEV_REQUIRED = "Seul SLEV est autorisé";
	public static final String SMNDA_REQUIRED = "Seul SMNDA est autorisé";
	public static final String INVALID_PAYMENT_METHOD = "Seules les valeurs TRF ou DD sont autorisées";
	public static final String SEQTYPES_REQUIRED = "Seules les valeurs FRST, RCUR, OOFF, FNAL sont autorisées";
	public static final String NM_LIMIT_70 = "Nom limité à 70 caractères";
	public static final String LENGTH_LIMIT_EXCEEDED = "Taille limite dépassée : ";
	public static final String ELEMENT_REQUIRED = "Element requis : ";
	public static final String ELEMENT_EMPTY = "Element non vide : ";
	public static final String ELEMENT_NON_EMPTY = "Element vide : ";
	public static final String VALUE_DIFFER = "La valeur devrait etre égale à : ";
	public static final String INVALID_NUMBER = "Montant non valide : ";
	public static final String MONTANT_EGAL_0 = "Le montant ne peut être égal à 0";
	public static final String SMNDA_EXCEPTION1 = "<Id> Obligatoire et égal à SMNDA si <OrgnlDbtrAgt> est présent";
	public static final String SMNDA_EXCEPTION2 = "Si <AmdmntInd> est égal à true et <OrgnlDbtrAgt> est SMNDA, alors la valeur “FRST“ est obligatoire pour <SeqTp>";
	public static final String SMNDA_EXCEPTION3 = "<OriginalDebtorAgent> est interdit si <OriginalDebtorAccount> est présent.";
	public static final String INVALID_IBAN = "Format de l'IBAN invalide";
	public static final String INVALID_BIC = "Format du BIC invalide";

}
