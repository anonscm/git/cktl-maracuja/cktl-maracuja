/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class Identification extends Element {

	private static final long serialVersionUID = 1L;
	static final String TAG = "Id";

	public Identification(IBAN iban, String index, String namespace) {
		super(TAG, index, namespace);
		setIban(iban);
	}

	public Identification(String text, String index, String namespace) {
		super(TAG, index, namespace);
		setText(text);
	}

	public Identification(OrganisationIdentification organisationIdentification, String index, String namespace) {
		super(TAG, index, namespace);
		setOrganisationIdentification(organisationIdentification);
	}

	public Identification(PrivateIdentification privateIdentification, String index, String namespace) {
		super(TAG, index, namespace);
		setPrivateIdentification(privateIdentification);
	}

	@Override
	public String getTag() {
		return TAG;
	}

	public IBAN getIban() {
		return (IBAN) getChild(IBAN.TAG);
	}

	public void setIban(IBAN iban) {
		addContent(iban);
	}

	public OrganisationIdentification getOrganisationIdentification() {
		return (OrganisationIdentification) getChild(OrganisationIdentification.TAG);
	}

	public void setOrganisationIdentification(OrganisationIdentification organisationIdentification) {
		addContent(organisationIdentification);
	}

	public PrivateIdentification getPrivateIdentification() {
		return (PrivateIdentification) getChild(PrivateIdentification.TAG);
	}

	public void setPrivateIdentification(PrivateIdentification privateIdentification) {
		addContent(privateIdentification);
	}

	@Override
	public void validate() throws ValidateException {

		if (getParentElement() instanceof UltimateDebtor) {
			checkRequired(OrganisationIdentification.TAG);
			//getOrganisationIdentification().checkOrEquals(new String[]{"BIC", "BEI"});
		}
		
		if (getParentElement() instanceof DebtorAccount) {
			checkRequired(IBAN.TAG);
			checkIBANFormat(getIban().getTextTrim());
		}


		super.validate();
	}
}
