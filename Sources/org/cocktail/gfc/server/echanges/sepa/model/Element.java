/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.cocktail.zutil.server.ZStringUtil;

/**
 * @author Rodolphe Prin
 */
public abstract class Element {

	/**
	 * Caractères autorisés dans les items.
	 */
	private static final String AUTHORIZED_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/-?:().,'+ ";

	private static final long serialVersionUID = 1L;
	public static final Format FORMAT_DATE_TIME_ISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	public static final Format FORMAT_DATE_ISO = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * Format à utiliser pour les montants. 28/03/2013 : d'apres le schema xsd, s'il n'y a pas de decimales dans le montant, il ne faut pas les
	 * renseigner avec des 0.
	 */
	public static final Format FORMAT_MONTANT = new DecimalFormat("####0.##");

	private String index;

	private String text = "";
	private String name;
	protected String namespace;
	private String additionalNamespace;
	private Element parentElement;
	private LinkedHashMap<String, String> attributes = new LinkedHashMap<String, String>();

	private LinkedList<Element> children = new LinkedList<Element>();

	protected Element(String name, String index, String namespace) {
		super();

		this.name = name;
		this.namespace = namespace;
		setIndex(index);

	}

	public String formatNumber(Number num) {
		String t = null;
		if (num != null) {
			t = FORMAT_MONTANT.format(num).replace(",", ".");
		}
		return t;
	}

	public Element setText(Number num) {
		return setText(formatNumber(num));
	}

	public Element setText(String text) {
		try {
			this.text = (text != null ? ZStringUtil.chaineSansAccents(text.trim()) : null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public abstract String getTag();

	public void cleanTextRecursive() throws ValidateException {
		cleanText();
		Iterator<Element> childs = getChildren().iterator();
		while (childs.hasNext()) {
			Element aSepaElement = (Element) childs.next();
			aSepaElement.cleanTextRecursive();
		}
	}

	public void validate() throws ValidateException {
		if (ZStringUtil.isEmpty(name)) {
			throw new ValidateException(this, "Nom de la balise non specifie.");
		}
		cleanText();

		Iterator<Element> childs = getChildren().iterator();
		while (childs.hasNext()) {
			Element aSepaElement = (Element) childs.next();
			aSepaElement.validate();
		}
	}

	protected void checkLength(int maxLength) throws ValidateException {
		if (getTextTrim() != null && getTextTrim().length() > maxLength) {
			throw new ValidateException(this, SepaExceptionMsg.LENGTH_LIMIT_EXCEEDED + maxLength + " : " + getTextTrim());
		}
	}

	protected void checkRequired(String childName, String namespace) throws ValidateException {
		Element child = (Element) getChild(childName);
		if (child == null) {
			throw new ValidateException(this, SepaExceptionMsg.ELEMENT_REQUIRED + childName);
		}
		if (child.getChildren().size() == 0 && (child.getTextTrim() == null || child.getTextTrim().length() == 0)) {
			throw new ValidateException(this, SepaExceptionMsg.ELEMENT_REQUIRED + childName);
		}
	}

	protected void checkRequired(String childName) throws ValidateException {
		Element child = (Element) getChild(childName);

		if (child == null) {
			throw new ValidateException(this, SepaExceptionMsg.ELEMENT_REQUIRED + childName);
		}
		if (child.getChildren().size() == 0 && (child.getTextTrim() == null || child.getTextTrim().length() == 0)) {
			throw new ValidateException(this, SepaExceptionMsg.ELEMENT_REQUIRED + childName);
		}
	}
	
	protected void checkEmpty(String childName) throws ValidateException {
		Element child = (Element) getChild(childName);
		if (child != null) {
			if (child.getChildren().size() > 0 || child.getTextTrim() != null || child.getTextTrim().length() > 0) {
				throw new ValidateException(this, SepaExceptionMsg.ELEMENT_EMPTY + childName);
			}
		}
	}
	
	protected void checkIBANFormat(String iban) throws ValidateException {
		String pattern = "[A-Z]{2,2}[0-9]{2,2}[a-zA-Z0-9]{1,30}";
		if (!iban.matches(pattern)) {
			throw new ValidateException(this, SepaExceptionMsg.INVALID_IBAN + " (" + pattern + ") : " + iban);
		}
	}

	protected void checkBICFormat(String bic) throws ValidateException {
		String pattern = "[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}";
		if (!bic.matches(pattern)) {
			throw new ValidateException(this, SepaExceptionMsg.INVALID_BIC + " (" + pattern + ") : " + bic);
		}
	}
	
	/**
	 * Verifie si le texte est égal au parametre
	 * 
	 * @param value
	 * @throws ValidateException
	 */
	protected void checkEquals(String value) throws ValidateException {
		if (!value.equals(getTextTrim())) {
			throw new ValidateException(this, SepaExceptionMsg.VALUE_DIFFER + value);
		}
	}

	/**
	 * @return Le chemin d'accès à l'élément
	 */
	public String getPath() {
		String res = getTagName();
		Element elt = this;
		while (elt.getParentElement() != null) {
			elt = elt.getParentElement();
			res = elt.getTagName() + "/" + res;
		}
		return res;
	}

	/**
	 * Vérifie si le texte passé en parametre respecte bien le format d'un montant.
	 * 
	 * @param montant
	 * @throws ValidateException
	 */
	protected void checkFormatMontant(String montant) throws ValidateException {
		String num = getTextTrim();
		String[] parts = num.split(".");
		for (int i = 0; i < parts.length; i++) {
			//verifier que les elements de la string sont tous numeriques
			if (!hasOnlyDigits(parts[i])) {
				throw new ValidateException(this, SepaExceptionMsg.INVALID_NUMBER + num);
			}
		}
		try {
			Number nb = (Number) FORMAT_MONTANT.parseObject(num);

		} catch (ParseException e) {
			throw new ValidateException(this, SepaExceptionMsg.INVALID_NUMBER + num + " (" + e.getMessage() + ")");
		}

	}

	public void checkMontantDifferentDeZero(String montant) throws ValidateException {
		if (formatNumber(BigDecimal.ZERO).equals(montant)) {
			throw new ValidateException(this, SepaExceptionMsg.MONTANT_EGAL_0);
		}
	}

	/**
	 * Teste si le caractere <i>c</i> est un chiffre entre 0 et 9.
	 */
	protected boolean isBasicDigit(char c) {
		int numVal = Character.getNumericValue(c);
		return ((Character.getNumericValue('0') <= numVal) && (numVal <= Character.getNumericValue('9')));
	}

	protected boolean isAcceptChar(char c, String acceptChars) {
		for (int i = 0; i < acceptChars.length(); i++) {
			if (c == acceptChars.charAt(i))
				return true;
		}
		return false;
	}

	public String toAcceptedString(String s, String acceptChars, char charToReplace) {
		if (s == null || s.length() == 0)
			return s;
		StringBuffer newStr = new StringBuffer();
		for (int i = 0; i < s.length(); i++)
			if (isAcceptChar(s.charAt(i), acceptChars)) {
				newStr.append(s.charAt(i));
			}
			else {
				newStr.append(charToReplace);
			}
		return newStr.toString();
	}

	/**
	 * @param s
	 * @return true si la chaine est nulle ou composée de chiffres uniquement.
	 */
	protected boolean hasOnlyDigits(String s) {
		if (s == null) {
			return true;
		}
		for (int i = 0; i < s.length(); i++) {
			if (!isBasicDigit(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public Element setText(Date date) {
		if (date != null) {
			return setText(FORMAT_DATE_ISO.format(date));
		}
		return setText((String) null);
	}

	/**
	 * Nettoie les contenus textuels en supprimant les caractères non autorisés. Automatiquement appelé à partir de validate.
	 */
	protected void cleanText() {
		if (getText() != null) {
			setText(toAcceptedString(getText(), AUTHORIZED_CHARS, ' '));
		}
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	//	public void setNamespaceRecursive(Namespace namespace) {
	//		setNamespace(namespace);
	//		Iterator<ASepaElement> childs = getChildren().iterator();
	//		while (childs.hasNext()) {
	//			ASepaElement aSepaElement = (ASepaElement) childs.next();
	//			aSepaElement.setNamespaceRecursive(namespace);
	//		}
	//	}

	public Element addContent(Element child) {
		//		if (child instanceof ASepaElement) {
		//			((ASepaElement) child).setNamespace(getNamespace());
		//		}

		return addChild(child);
	}

	public List getChildren(final String name) {
		Predicate<Element> nameEquals = new Predicate<Element>() {
			public boolean apply(Element element) {
				return name.equals(element.name);
			}
		};

		return filter(children, nameEquals);
	}

	public Element getChild(String name) {
		List<? extends Element> res = getChildren(name);
		if (res.size() > 0) {
			return (Element) res.toArray()[0];
		}
		return null;
		//return super.getChild(name, namespace);
	}

	public String getTextTrim() {
		if (getText() == null) {
			return null;
		}
		return getText().trim();
	}

	public String getText() {
		return text;
	}

	public List<Element> getChildren() {
		return children;
	}

	public static <T> List<T> filter(List<T> target, Predicate<T> predicate) {
		List<T> result = new ArrayList<T>();
		for (T element : target) {
			if (predicate.apply(element)) {
				result.add(element);
			}
		}
		return result;
	}

	public Element addChild(Element element) {
		if (element != null) {
			children.add(element);
			element.setParentElement(this);
		}
		return element;
	}

	public String getTagName() {
		return getTag();
	}

	public Element getParentElement() {
		return parentElement;
	}

	public void setParentElement(Element parentElement) {
		this.parentElement = parentElement;
	}

	public void setAttribute(String attribute, String value) {
		attributes.put(attribute, value);
	}

	public String getAttributeValue(String attribute) {
		return attributes.get(attribute);
	}

	public LinkedHashMap<String, String> getAttributes() {
		return attributes;
	}
}
