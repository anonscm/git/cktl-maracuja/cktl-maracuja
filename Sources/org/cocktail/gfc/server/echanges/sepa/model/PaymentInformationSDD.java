/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;

import java.util.List;

/**
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public class PaymentInformationSDD extends PaymentInformation {

	private static final long serialVersionUID = 1L;

	public PaymentInformationSDD(String index, String namespace) {
		super(index, namespace);
	}

	public PaymentTypeInformationSDD getPaymentTypeInformationSDD() {
		return (PaymentTypeInformationSDD) getChild(PaymentTypeInformation.TAG);
	}

	public void setPaymentTypeInformationSDD(PaymentTypeInformationSDD paymentTypeInformationSDD) {
		addContent(paymentTypeInformationSDD);
	}

	public RequestedCollectionDate getRequestedCollectionDate() {
		return (RequestedCollectionDate) getChild(RequestedCollectionDate.TAG);
	}

	public void setRequestedCollectionDate(RequestedCollectionDate requestedCollectionDate) {
		addContent(requestedCollectionDate);
	}

	public Creditor getCreditor() {
		return (Creditor) getChild(Creditor.TAG);
	}

	public void setCreditor(Creditor creditor) {
		addContent(creditor);
	}

	public CreditorAccount getCreditorAccount() {
		return (CreditorAccount) getChild(CreditorAccount.TAG);
	}

	public void setCreditorAccount(CreditorAccount creditorAccount) {
		addContent(creditorAccount);
	}

	public CreditorAgent getCreditorAgent() {
		return (CreditorAgent) getChild(CreditorAgent.TAG);
	}

	public void setCreditorAgent(CreditorAgent creditorAgent) {
		addContent(creditorAgent);
	}
	
	public UltimateCreditor getUltimateCreditor() {
		return (UltimateCreditor) getChild(UltimateCreditor.TAG);
	}

	public void setUltimateCreditor(UltimateCreditor ultimateCreditor) {
		addContent(ultimateCreditor);
	}	
	
	public CreditorSchemeIdentification getCreditorSchemeIdentification() {
		return (CreditorSchemeIdentification) getChild(CreditorSchemeIdentification.TAG);
	}

	public void setCreditorSchemeIdentification(CreditorSchemeIdentification creditorSchemeIdentification) {
		addContent(creditorSchemeIdentification);
	}	
	
	public List<DirectDebitTransactionInformation> getDirectDebitTransactionInformations() {
		return getChildren(DirectDebitTransactionInformation.TAG);
	}

	public void addToDirectDebitTransactionInformation(DirectDebitTransactionInformation directDebitTransactionInformation) {
		addContent(directDebitTransactionInformation);
	}

	/**.
	 * Vérifie que si “Amendement Indicator“ est “true“
	 * et “Original DebtorAgent“ est “SMNDA“,
	 * alors la valeur de Sequence Type doit être “FRST“
	 *
	 * @throws ValidateException
	 */
	protected final void checkSMNDA() throws ValidateException {
		int j = 0;
		SequenceType sequenceType = (SequenceType) getPaymentTypeInformationSDD().getSequenceType();
		while (j < getDirectDebitTransactionInformations().size()) {
			AmendmentIndicator amendmentIndicator = (AmendmentIndicator) getDirectDebitTransactionInformations().get(j)
					.getDirectDebitTransaction().getMandateRelatedInformation().getAmendmentIndicator();
	
			if (amendmentIndicator.TRUE.equals(amendmentIndicator.getTextTrim())) {
				if (getDirectDebitTransactionInformations().get(j).getDirectDebitTransaction()
						.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalDebtorAgent()!=null) {
					
					if ((getDirectDebitTransactionInformations().get(j).getDirectDebitTransaction().getMandateRelatedInformation()
							.getAmendmentInformationDetails().getOriginalDebtorAgent().getFinancialInstitutionIdentification().getOther().getIdentification()!=null)) {
					
						Identification odaIdentification = (Identification) getDirectDebitTransactionInformations().get(j).getDirectDebitTransaction()
								.getMandateRelatedInformation().getAmendmentInformationDetails().getOriginalDebtorAgent().getFinancialInstitutionIdentification().getOther().getIdentification();
							
							if (!odaIdentification.getTextTrim().equals("SMNDA")) {
								throw new ValidateException(this, SepaExceptionMsg.SMNDA_EXCEPTION1 + odaIdentification.getTextTrim());
							} else if (!sequenceType.getTextTrim().equals("FRST")) {
									throw new ValidateException(this, SepaExceptionMsg.SMNDA_EXCEPTION2);
							}
					} else {
						throw new ValidateException(this, SepaExceptionMsg.SMNDA_EXCEPTION1);
					}
				}
			}
			j++;
		}
	}

	@Override
	public void validate() throws ValidateException {
		checkRequired(RequestedCollectionDate.TAG);
		checkRequired(Creditor.TAG);
		checkRequired(CreditorAgent.TAG);
		checkRequired(CreditorAccount.TAG);
		checkRequired(UltimateCreditor.TAG);
		checkRequired(CreditorSchemeIdentification.TAG);
		checkRequired(DirectDebitTransactionInformation.TAG);
		checkSMNDA();
		super.validate();
	}

}
