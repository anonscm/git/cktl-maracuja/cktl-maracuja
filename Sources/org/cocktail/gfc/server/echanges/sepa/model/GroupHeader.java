/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.model;


/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class GroupHeader extends Element {

	private static final long serialVersionUID = 1L;
	static final String TAG = "GrpHdr";

	public MessageIdentification getMessageIdentification() {
		return (MessageIdentification) getChild(MessageIdentification.TAG);
	}

	public void setMessageIdentification(MessageIdentification messageIdentification) {
		addContent(messageIdentification);
	}

	public CreationDateTime getCreationDateTime() {
		return (CreationDateTime) getChild(CreationDateTime.TAG);
	}

	public void setCreationDateTime(CreationDateTime creationDateTime) {
		addContent(creationDateTime);
	}

	public NumberOfTransactions getNumberOfTransactions() {
		return (NumberOfTransactions) getChild(NumberOfTransactions.TAG);
	}

	public void setNumberOfTransactions(NumberOfTransactions numberOfTransactions) {
		addContent(numberOfTransactions);
	}

	public ControlSum getControlSum() {
		return (ControlSum) getChild(ControlSum.TAG);
	}

	public void setControlSum(ControlSum controlSum) {
		addContent(controlSum);
	}

	public InitiatingParty getInitiatingParty() {
		return (InitiatingParty) getChild(InitiatingParty.TAG);
	}

	public void setInitiatingParty(InitiatingParty initiatingParty) {
		addContent(initiatingParty);
	}

	public GroupHeader(String index, String namespace) {
		super(TAG, index, namespace);
	}

	@Override
	public String getTag() {
		return TAG;
	}

	@Override
	public void validate() throws ValidateException {
		checkRequired(MessageIdentification.TAG);
		checkRequired(CreationDateTime.TAG);
		checkRequired(NumberOfTransactions.TAG);
		checkRequired(ControlSum.TAG);
		checkRequired(InitiatingParty.TAG);

		getMessageIdentification().checkLength(35);

		super.validate();
	}

}
