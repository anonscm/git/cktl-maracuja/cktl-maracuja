/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.cocktail.gfc.server.echanges.sepa.model.Amount;
import org.cocktail.gfc.server.echanges.sepa.model.BIC;
import org.cocktail.gfc.server.echanges.sepa.model.ChargeBearer;
import org.cocktail.gfc.server.echanges.sepa.model.Code;
import org.cocktail.gfc.server.echanges.sepa.model.CreditTransferTransactionInformation;
import org.cocktail.gfc.server.echanges.sepa.model.Creditor;
import org.cocktail.gfc.server.echanges.sepa.model.CreditorAccount;
import org.cocktail.gfc.server.echanges.sepa.model.CreditorAgent;
import org.cocktail.gfc.server.echanges.sepa.model.CstmrCdtTrfInitn;
import org.cocktail.gfc.server.echanges.sepa.model.Debtor;
import org.cocktail.gfc.server.echanges.sepa.model.DebtorAccount;
import org.cocktail.gfc.server.echanges.sepa.model.DebtorAgent;
import org.cocktail.gfc.server.echanges.sepa.model.EndToEndIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.FinancialInstitutionIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.GroupHeader;
import org.cocktail.gfc.server.echanges.sepa.model.IBAN;
import org.cocktail.gfc.server.echanges.sepa.model.Identification;
import org.cocktail.gfc.server.echanges.sepa.model.InstructedAmount;
import org.cocktail.gfc.server.echanges.sepa.model.MessageSCT;
import org.cocktail.gfc.server.echanges.sepa.model.Name;
import org.cocktail.gfc.server.echanges.sepa.model.OrganisationIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.Other;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentInformationIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentInformationSCT;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentMethod;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentTypeInformationSCT;
import org.cocktail.gfc.server.echanges.sepa.model.Purpose;
import org.cocktail.gfc.server.echanges.sepa.model.RemittanceInformation;
import org.cocktail.gfc.server.echanges.sepa.model.RequestedExecutionDate;
import org.cocktail.gfc.server.echanges.sepa.model.ServiceLevel;
import org.cocktail.gfc.server.echanges.sepa.model.UltimateCreditor;
import org.cocktail.gfc.server.echanges.sepa.model.UltimateDebtor;
import org.cocktail.gfc.server.echanges.sepa.model.Unstructured;
import org.w3c.dom.Document;

import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;

/**
 * Construction de messages de Virement SCT au format SEPA (se base sur des VirementRecord et non sur des objets métiers spécifiques).
 * 
 * @author rprin
 */
public class VirementSepaFactory extends ASepaFactory{
	public static String NS = "xmlns=urn:iso:std:iso:20022:tech:xsd:pain.001.001.03";
	private static final String NATURE_REMISE_SCT = "SCT";

	/**
	 * @param lesVirementRecords Tableau d'objets VirementRecord
	 * @param dateExecutionDemandee
	 * @param dateTimeCreation
	 * @param paramSepa
	 * @param numeroDeFichierDansLaJournee3Car
	 * @param identifiantFichierVirement Permet d'identifier en local le virement (pour recherche ultérieur éventuelle)
	 * @return
	 * @throws Exception
	 */
	public NSData genereMessage(NSTimestamp dateTimeCreation, ParametresSepa paramSepa, NSArray lesVirementRecords, BigDecimal montantTotal, Integer nbTransactions, 
			NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car, String identifiantFichierVirement, String xsdLocation)	throws Exception {
		checkParamSepa(paramSepa);
		try {
			String annee2Chars = (new SimpleDateFormat("yy")).format(dateTimeCreation);
			String numeroDuJourDansLAnnee3Car = (new SimpleDateFormat("DDD")).format(dateTimeCreation);
			String msgId = genMessageIdentification(paramSepa.getEmetteurTransfertId(), NATURE_REMISE_SCT, annee2Chars, numeroDuJourDansLAnnee3Car, numeroDeFichierDansLaJournee3Car);
			//verifier les données a minima
			if (lesVirementRecords.count() == 0) {
				throw new Exception("Aucun virement à construire");
			}
			//Construire le msg
			MessageSCT sctMessage = new MessageSCT(NS);
			sctMessage.setAttribute("xmlns", "urn:iso:std:iso:20022:tech:xsd:pain.001.001.03");
			sctMessage.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			//	sctMessage.setNamespace(NS);
			//sctMessage.addNamespaceDeclaration(NS_XSI);

			CstmrCdtTrfInitn cstmrCdtTrfInitn = new CstmrCdtTrfInitn(NS);
			//cstmrCdtTrfInitn.setNamespace(NS);
			sctMessage.setCstmrCdtTrfInitn(cstmrCdtTrfInitn);

			GroupHeader groupHeader = construitGroupHeaderParParametres(paramSepa.getCompteDftTitulaire(), msgId, dateTimeCreation, montantTotal, nbTransactions, NS);
			cstmrCdtTrfInitn.setGroupHeader(groupHeader);

			PaymentInformationSCT paymentInformation = construitPaymentInformationSCTParParametres(paramSepa, lesVirementRecords, dateExecutionDemandee, identifiantFichierVirement);
			cstmrCdtTrfInitn.addToPaymentInformations(paymentInformation);
			
			//	sctMessage.cleanTextRecursive();
			sctMessage.validate();

			DocumentBuilderFactory dbf = DocumentBuilderFactoryImpl.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.newDocument();

			//Element rootEle = dom.createElement("Document");
			//dom.appendChild(rootEle);
			dom.appendChild(sepaElementToDom(dom, dom, sctMessage));

			//print

			//console

			//FIXME Outputformat et XMLSerializer deprecated, voir http://xerces.apache.org/xerces2-j/faq-general.html#faq-6
			//FIXME : cf. LSSerializer

			//to generate output to console use this serializer
			OutputFormat strFormat = new OutputFormat(dom);
			strFormat.setIndenting(true);
			XMLSerializer strSerializer = new XMLSerializer(System.out, strFormat);
			strSerializer.serialize(dom);

			//binary
			OutputFormat binFormat = new OutputFormat(dom);
			binFormat.setIndenting(false);
			ByteArrayOutputStream binOutput = new ByteArrayOutputStream();
			XMLSerializer binarySerializer = new XMLSerializer(binOutput, binFormat);
			binarySerializer.serialize(dom);

			NSData res = new NSData(binOutput.toByteArray());
			validateWithSchema(res, xsdLocation);

			//			StringWriter sw = new StringWriter();
			//			XMLOutputter sortieText = new XMLOutputter(Format.getPrettyFormat());
			//			sortieText.output(document, sw);
			//			System.out.println(sw.toString());
			//
			//			sctMessage.validate();
			//
			//			XMLOutputter sortie = new XMLOutputter(Format.getRawFormat());
			//			ByteArrayOutputStream output = new ByteArrayOutputStream();
			//
			//			sortie.output(document, output);
			//			NSData res = new NSData(output.toByteArray());
			//			validateWithSchema(res, xsdLocation);
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static PaymentInformationSCT construitPaymentInformationSCTParParametres(ParametresSepa paramSepa, NSArray lesVirementRecords, NSTimestamp dateExecutionDemandee, String identifiantFichierVirement) {
		PaymentInformationSCT paymentInformation = new PaymentInformationSCT("2.0", NS);
		paymentInformation.setPaymentInformationIdentification(new PaymentInformationIdentification(identifiantFichierVirement, "2.1", NS));
		paymentInformation.setPaymentMethod(new PaymentMethod(PaymentMethod.PAYMENT_METHOD.get("SCT"), "2.2", NS));
		//paymentInformation.setBatchBooking(new BatchBooking(BatchBooking.TRUE, "2.3"));
		paymentInformation.setPaymentTypeInformationSCT(new PaymentTypeInformationSCT(new ServiceLevel(new Code("SEPA", "2.9", NS), "2.8", NS), "2.6", NS));
		paymentInformation.setRequestedExecutionDate(new RequestedExecutionDate(dateExecutionDemandee, "2.17", NS));
		paymentInformation.setDebtor(new Debtor(new Name(paramSepa.getTgNom(), "2.20", NS), "2.19", NS));
		//TODO : integrer l'adresse de la TG si necessaire
		paymentInformation.setDebtorAccount(new DebtorAccount(new Identification(new IBAN(paramSepa.getTgIban(), "2.20", NS), "2.20", NS), "2.20", NS));
		paymentInformation.setDebtorAgent(new DebtorAgent(new FinancialInstitutionIdentification(new BIC(paramSepa.getTgBic(), "2,21", NS), "2.21", NS), "2.21", NS));
		paymentInformation.setChargeBearer(new ChargeBearer(ChargeBearer.SLEV, "2.22", NS));
	
		//Construire chaque transaction
		for (Object record0 : lesVirementRecords) {
			VirementSepaRecord record = (VirementSepaRecord) record0;
			CreditTransferTransactionInformation creditTransferTransactionInformation = construitCreditTransferTransactionInformationParVirementRecord(paramSepa, record);
			paymentInformation.addToCreditTransferTransactionInformations(creditTransferTransactionInformation);
		}
	return paymentInformation;
	}

	public static CreditTransferTransactionInformation construitCreditTransferTransactionInformationParVirementRecord(ParametresSepa paramSepa, VirementSepaRecord record) {
		CreditTransferTransactionInformation creditTransferTransactionInformation = new CreditTransferTransactionInformation("2.27", NS);
		creditTransferTransactionInformation.setPaymentIdentification(new PaymentIdentification(new EndToEndIdentification("1D" + paramSepa.getTgCodique() + record.getPaymentID(), "2.30", NS), "2.28", NS));
		creditTransferTransactionInformation.setAmount(new Amount(new InstructedAmount(record.getCurrency(), record.getAmount(), "2.43", NS), "2.42", NS));
		UltimateDebtor ud = new UltimateDebtor("2.70", NS);
		ud.set_Name(new Name(paramSepa.getCompteDftTitulaire(), "2.70", NS));
		ud.setIdentification(new Identification(new OrganisationIdentification(new Other(new Identification(paramSepa.getCompteDftIban(), "2.70", NS), "2.70", NS), "2.70", NS), "2.70", NS));
		creditTransferTransactionInformation.setUltimateDebtor(ud);

		creditTransferTransactionInformation.setCreditorAgent(new CreditorAgent(new FinancialInstitutionIdentification(new BIC(record.getCreditorBIC(), "2.77", NS), "2.77", NS), "2.77", NS));
		creditTransferTransactionInformation.setCreditor(new Creditor(new Name(record.getCreditorName(), "2.79", NS), "2.79", NS));
		creditTransferTransactionInformation.setCreditorAccount(new CreditorAccount(new Identification(new IBAN(record.getCreditorIBAN(), "2.80", NS), "2.80", NS), "2.80", NS));

		if (record.getUltimateCreditorName() != null) {
			creditTransferTransactionInformation.setUltimateCreditor(new UltimateCreditor(new Name(record.getUltimateCreditorName(), "2.81", NS), "2.81", NS));
		}
		if (record.getPurposeCode() != null) {
			creditTransferTransactionInformation.setPurpose(new Purpose(new Code(record.getPurposeCode(), "2.87", NS), "2.86", NS));
		}
		creditTransferTransactionInformation.setRemittanceInformation(new RemittanceInformation(new Unstructured(record.getRemittanceInformation(), "2.99", NS), "2.98", NS));
		return creditTransferTransactionInformation;
	}

	/**
	 * Vérifie la présence des paramètres nécessaires à la constitution du fichier de virement.
	 * 
	 * @param paramSepa
	 * @throws Exception
	 */
	protected final void checkParamSepa(ParametresSepa paramSepa) throws Exception {
		try {
			if (paramSepa.getEmetteurTransfertId() == null) {
				throw new Exception("Id emetteur obligatoire");
			}
			if (paramSepa.getCompteDftIban() == null) {
				throw new Exception("IBAN du compte DFT obligatoire");
			}
			if (paramSepa.getCompteDftTitulaire() == null) {
				throw new Exception("Titulaire du compte DFT obligatoire");
			}
			if (paramSepa.getTgIban() == null) {
				throw new Exception("IBAN de la TG obligatoire");
			}
			if (paramSepa.getTgBic() == null) {
				throw new Exception("BIC de la TG obligatoire");
			}
			if (paramSepa.getTgCodique() == null) {
				throw new Exception("Codique de la TG obligatoire");
			}
			if (paramSepa.getTgNom() == null) {
				throw new Exception("Nom de la TG obligatoire");
			}
			if (paramSepa.getTgCodique().length() != 7) {
				throw new Exception("Le codique de la TG doit comporter exactement 7 caractères.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur de paramétrage (cf. maracuja.virement_param_sepa) : " + e.getMessage());
		}
	}

}
