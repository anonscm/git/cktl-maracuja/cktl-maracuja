/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

import java.math.BigDecimal;

import org.cocktail.zutil.server.ZStringUtil;

/**
 * Permet de mapper des éléments d'une transaction pour simplifier le passage des objets métiers vers le modèle SEPA. Exploité par
 * VirementSepaFactory.
 * 
 * @author rprin
 */
public class VirementSepaRecord extends ASepaRecord{
	private BigDecimal amount;
	private String creditorBIC;
	private String creditorIBAN;
	private String creditorName;
	private String ultimateCreditorName;


	public VirementSepaRecord() {
		super();
	}


	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount montant de la transaction
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCreditorBIC() {
		return creditorBIC;
	}

	public void setCreditorBIC(String creditorBIC) {
		if (creditorBIC != null) {
			creditorBIC = creditorBIC.toUpperCase().replaceAll(" ", "");
		}
		this.creditorBIC = creditorBIC;
	}

	public String getCreditorIBAN() {
		return creditorIBAN;
	}

	public void setCreditorIBAN(String creditorIBAN) {
		if (creditorIBAN != null) {
			creditorIBAN = creditorIBAN.toUpperCase().replaceAll(" ", "");
		}
		this.creditorIBAN = creditorIBAN;
	}

	public String getCreditorName() {
		return creditorName;
	}

	/**
	 * @param creditorName Nom + prenom du destinataire du virement
	 */
	public void setCreditorName(String creditorName) {
		this.creditorName = ZStringUtil.chaineSansCaracteresSpeciauxUpper(creditorName);
		this.creditorName = ZStringUtil.cut(this.creditorName, 70);
	}

	public String getUltimateCreditorName() {
		return ultimateCreditorName;
	}

	/**
	 * @param ultimateCreditorName Destinataire final du virement (si le crediteur n'est pas réellement le destinataire)
	 */
	public void setUltimateCreditorName(String ultimateCreditorName) {
		this.ultimateCreditorName = ZStringUtil.chaineSansCaracteresSpeciauxUpper(ultimateCreditorName);
		this.ultimateCreditorName = ZStringUtil.cut(this.ultimateCreditorName, 70);

	}

	@Override
	public String toString() {
		return getPaymentID() + " / " + getCreditorName() + " / " + getCreditorBIC() + " / " + getCreditorIBAN() + " / " + getAmount() + " / " + getRemittanceInformation();
	}

	@Override
	public void checkComplet() throws Exception {
		try {
			if (getCreditorBIC() == null) {
				throw new Exception("BIC créditeur obligatoire");
			}
			if (getCreditorIBAN() == null) {
				throw new Exception("IBAN créditeur obligatoire");
			}
			if (getCreditorName() == null) {
				throw new Exception("Nom créditeur obligatoire");
			}
			if (getCurrency() == null) {
				throw new Exception("Devise de la transaction obligatoire");
			}
			if (getAmount() == null) {
				throw new Exception("Montant de la transaction obligatoire");
			}
			if (getPaymentID() == null) {
				throw new Exception("ID du paiement obligatoire");
			}
			if (getRemittanceInformation() == null) {
				throw new Exception("Libelle de la transaction obligatoire");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage() + " : " + toString());
		}
	}

}
