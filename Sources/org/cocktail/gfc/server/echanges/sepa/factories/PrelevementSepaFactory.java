/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.cocktail.gfc.server.echanges.sepa.model.AmendmentIndicator;
import org.cocktail.gfc.server.echanges.sepa.model.AmendmentInformationDetails;
import org.cocktail.gfc.server.echanges.sepa.model.BIC;
import org.cocktail.gfc.server.echanges.sepa.model.ChargeBearer;
import org.cocktail.gfc.server.echanges.sepa.model.Code;
import org.cocktail.gfc.server.echanges.sepa.model.Creditor;
import org.cocktail.gfc.server.echanges.sepa.model.CreditorAccount;
import org.cocktail.gfc.server.echanges.sepa.model.CreditorAgent;
import org.cocktail.gfc.server.echanges.sepa.model.CreditorSchemeIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.CstmrDrctDbtInitn;
import org.cocktail.gfc.server.echanges.sepa.model.DateOfSignature;
import org.cocktail.gfc.server.echanges.sepa.model.Debtor;
import org.cocktail.gfc.server.echanges.sepa.model.DebtorAccount;
import org.cocktail.gfc.server.echanges.sepa.model.DebtorAgent;
import org.cocktail.gfc.server.echanges.sepa.model.DirectDebitTransaction;
import org.cocktail.gfc.server.echanges.sepa.model.DirectDebitTransactionInformation;
import org.cocktail.gfc.server.echanges.sepa.model.EndToEndIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.FinancialInstitutionIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.GroupHeader;
import org.cocktail.gfc.server.echanges.sepa.model.IBAN;
import org.cocktail.gfc.server.echanges.sepa.model.Identification;
import org.cocktail.gfc.server.echanges.sepa.model.InstructedAmount;
import org.cocktail.gfc.server.echanges.sepa.model.LocalInstrument;
import org.cocktail.gfc.server.echanges.sepa.model.MandateIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.MandateRelatedInformation;
import org.cocktail.gfc.server.echanges.sepa.model.MessageSDD;
import org.cocktail.gfc.server.echanges.sepa.model.Name;
import org.cocktail.gfc.server.echanges.sepa.model.OrganisationIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.OriginalCreditorSchemeIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.OriginalDebtorAccount;
import org.cocktail.gfc.server.echanges.sepa.model.OriginalDebtorAgent;
import org.cocktail.gfc.server.echanges.sepa.model.OriginalMandateIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.Other;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentInformationIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentInformationSDD;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentMethod;
import org.cocktail.gfc.server.echanges.sepa.model.PaymentTypeInformationSDD;
import org.cocktail.gfc.server.echanges.sepa.model.PrivateIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.Proprietary;
import org.cocktail.gfc.server.echanges.sepa.model.Purpose;
import org.cocktail.gfc.server.echanges.sepa.model.RemittanceInformation;
import org.cocktail.gfc.server.echanges.sepa.model.RequestedCollectionDate;
import org.cocktail.gfc.server.echanges.sepa.model.SchemeName;
import org.cocktail.gfc.server.echanges.sepa.model.SequenceType;
import org.cocktail.gfc.server.echanges.sepa.model.ServiceLevel;
import org.cocktail.gfc.server.echanges.sepa.model.UltimateCreditor;
import org.cocktail.gfc.server.echanges.sepa.model.UltimateDebtor;
import org.cocktail.gfc.server.echanges.sepa.model.Unstructured;
import org.w3c.dom.Document;

import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;

/**
 * Construction de messages de prélèvement SDD au format SEPA (se base sur des PrelevementSepaRecord et non sur des objets métiers spécifiques).
 * 
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public class PrelevementSepaFactory extends ASepaFactory {
	public static String NS = "xmlns=urn:iso:std:iso:20022:tech:xsd:pain.008.001.02";
	private static final String NATURE_REMISE_SDD = "SDD";

	/**
	 * @param lesPrelevementsRecords Tableau d'objets PrelevementsRecord
	 * @param dateExecutionDemandee
	 * @param dateTimeCreation
	 * @param paramSepa
	 * @param numeroDeFichierDansLaJournee3Car
	 * @param identifiantFichierPrelevement Permet d'identifier en local le prélèvement (pour recherche ultérieur éventuelle)
	 * @return
	 * @throws Exception
	 */
	public NSData genereMessage(NSTimestamp dateTimeCreation, ParametresSepa paramSepa, NSArray lesPrelevementsRecords, BigDecimal montantTotal, Integer nbTransactions,
			NSTimestamp dateExecutionDemandee, String numeroDeFichierDansLaJournee3Car, String identifiantFichierPrelevement, String xsdLocation, String sequenceType) throws Exception {
		checkParamSepa(paramSepa);
		try {
			String annee2Chars = (new SimpleDateFormat("yy")).format(dateTimeCreation);
			String numeroDuJourDansLAnnee3Car = (new SimpleDateFormat("DDD")).format(dateTimeCreation);
			String msgId = genMessageIdentification(paramSepa.getEmetteurTransfertId(), NATURE_REMISE_SDD, annee2Chars, numeroDuJourDansLAnnee3Car, numeroDeFichierDansLaJournee3Car);
			//verifier les données a minima
			if (lesPrelevementsRecords.count() == 0) {
				throw new Exception("Aucun prélèvement à construire");
			}
			//Construire le msg
			MessageSDD sddMessage = new MessageSDD(NS);
			sddMessage.setAttribute("xmlns", "urn:iso:std:iso:20022:tech:xsd:pain.008.001.02");
			sddMessage.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");

			CstmrDrctDbtInitn cstmrDrctDbtInitn = new CstmrDrctDbtInitn(NS);
			sddMessage.setCstmrDrctDbtInitn(cstmrDrctDbtInitn);

			GroupHeader groupHeader = construitGroupHeaderParParametres(paramSepa.getCompteDftTitulaire(), msgId, dateTimeCreation, montantTotal, nbTransactions, NS);
			cstmrDrctDbtInitn.setGroupHeader(groupHeader);

			PaymentInformationSDD paymentInformation = construitPaymentInformationSDDParParametres(paramSepa, lesPrelevementsRecords, dateExecutionDemandee, identifiantFichierPrelevement, sequenceType);
			cstmrDrctDbtInitn.addToPaymentInformations(paymentInformation);

			sddMessage.validate();

			DocumentBuilderFactory dbf = DocumentBuilderFactoryImpl.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document dom = db.newDocument();

			dom.appendChild(sepaElementToDom(dom, dom, sddMessage));

			//print
			//console
			//FIXME Outputformat et XMLSerializer deprecated, voir http://xerces.apache.org/xerces2-j/faq-general.html#faq-6

			//to generate output to console use this serializer
			OutputFormat strFormat = new OutputFormat(dom);
			strFormat.setIndenting(true);
			XMLSerializer strSerializer = new XMLSerializer(System.out, strFormat);
			strSerializer.serialize(dom);

			//binary
			OutputFormat binFormat = new OutputFormat(dom);
			binFormat.setIndenting(false);
			ByteArrayOutputStream binOutput = new ByteArrayOutputStream();
			XMLSerializer binarySerializer = new XMLSerializer(binOutput, binFormat);
			binarySerializer.serialize(dom);

			NSData res = new NSData(binOutput.toByteArray());
			validateWithSchema(res, xsdLocation);

			return res;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @param paramSepa Paramètres SEPA globaux
	 * @param lesPrelevementsRecords
	 * @param dateExecutionDemandee
	 * @param identifiantFichierPrelevement
	 * @param sequenceType le type d'operation (FRST, OOFF, RCUR, FNAL)
	 * @return une instance de PaymentInformationSDD pour le niveau lot du fichier
	 */
	public PaymentInformationSDD construitPaymentInformationSDDParParametres(ParametresSepa paramSepa, NSArray<PrelevementSepaRecord> lesPrelevementsRecords, NSTimestamp dateExecutionDemandee, String identifiantFichierPrelevement, String sequenceType) {

		PaymentInformationSDD paymentInformation = new PaymentInformationSDD("2.0", NS);
		paymentInformation.setPaymentInformationIdentification(new PaymentInformationIdentification(identifiantFichierPrelevement, "2.1", NS));
		paymentInformation.setPaymentMethod(new PaymentMethod(PaymentMethod.PAYMENT_METHOD.get("SDD"), "2.2", NS));

		paymentInformation.setPaymentTypeInformationSDD(new PaymentTypeInformationSDD(new ServiceLevel(new Code("SEPA", "2.9", NS), "2.8", NS), new LocalInstrument(new Code("CORE", "2.12", NS), "2.11", NS), "2.6", NS));
		paymentInformation.getPaymentTypeInformationSDD().setSequenceType(new SequenceType(sequenceType, "2.14", NS));
		paymentInformation.setRequestedCollectionDate(new RequestedCollectionDate(dateExecutionDemandee, "2.18", NS));
		//à vérifier après adaptation paramsepa
		paymentInformation.setCreditor(new Creditor(new Name(paramSepa.getTgNom(), "2.20", NS), "2.19", NS));
		//TODO : integrer l'adresse de la TG si necessaire
		paymentInformation.setCreditorAccount(new CreditorAccount(new Identification(new IBAN(paramSepa.getTgIban(), "2.20", NS), "2.20", NS), "2.20", NS));
		paymentInformation.setCreditorAgent(new CreditorAgent(new FinancialInstitutionIdentification(new BIC(paramSepa.getTgBic(), "2,21", NS), "2.21", NS), "2.21", NS));
		UltimateCreditor uc = new UltimateCreditor("2.23", NS);
		uc.set_Name(new Name(paramSepa.getCompteDftTitulaire(), "2.23", NS));
		uc.setIdentification(new Identification(new OrganisationIdentification(new Other(new Identification(paramSepa.getCompteDftIban(), "2.23", NS), "2.23", NS), "2.23", NS), "2.23", NS));
		paymentInformation.setUltimateCreditor(uc);
		paymentInformation.setChargeBearer(new ChargeBearer(ChargeBearer.SLEV, "2.24", NS));
		paymentInformation.setCreditorSchemeIdentification(new CreditorSchemeIdentification(new Identification(new PrivateIdentification(new Other(new Identification(paramSepa.getIcs(), "2.27", NS),
				new SchemeName(new Proprietary("SEPA", "2.27", NS), "2.27", NS), "2.27", NS), "2.27", NS), "2.27", NS), "2.27", NS));

		//Construire chaque transaction
		for (PrelevementSepaRecord record0 : lesPrelevementsRecords) {
			PrelevementSepaRecord record = record0;
			DirectDebitTransactionInformation directDebitTransactionInformation = construitDirectDebitTransactionInformationParPrelevementRecord(paramSepa, record);
			paymentInformation.addToDirectDebitTransactionInformation(directDebitTransactionInformation);

		}
		return paymentInformation;
	}

	/**
	 * @param paramSepa
	 * @param record
	 * @return une instance de DirectDebitTransactionInformation pour le niveau lot du fichier
	 */
	public DirectDebitTransactionInformation construitDirectDebitTransactionInformationParPrelevementRecord(ParametresSepa paramSepa, PrelevementSepaRecord record) {

		DirectDebitTransactionInformation element = new DirectDebitTransactionInformation("2.28", NS);
		element.setPaymentIdentification(new PaymentIdentification(new EndToEndIdentification("1D" + paramSepa.getTgCodique() + record.getPaymentID(), "2.31", NS), "2.29", NS));
		element.setInstructedAmount(new InstructedAmount(record.getCurrency(), record.getInstructedAmount(), "2.44", NS));

		if (record.getMandatID() != null) {
			element.setDirectDebitTransaction(getNewDirectDebitTransaction(paramSepa, record));
		}

		element.setDebtorAgent(getNewDebtorAgent(record.getDebtorBIC(), "2.70"));
		element.setDebtor(new Debtor(new Name(record.getDebtorName(), "2.72", NS), "2.72", NS));
		element.setDebtorAccount(new DebtorAccount(new Identification(new IBAN(record.getDebtorIBAN(), "2.73", NS), "2.73", NS), "2.73", NS));

		if (record.getUltimateDebtorName() != null) {
			element.setUltimateDebtor(new UltimateDebtor(new Name(record.getUltimateDebtorName(), "2.74", NS), "2.74", NS));
		}
		if (record.getPurposeCode() != null) {
			element.setPurpose(new Purpose(new Code(record.getPurposeCode(), "2.77", NS), "2.76", NS));
		}
		element.setRemittanceInformation(new RemittanceInformation(new Unstructured(record.getRemittanceInformation(), "2.89", NS), "2.88", NS));
		return element;
	}

	protected DirectDebitTransaction getNewDirectDebitTransaction(ParametresSepa paramSepa, PrelevementSepaRecord record) {
		DirectDebitTransaction element = new DirectDebitTransaction(new MandateRelatedInformation(new MandateIdentification(record.getMandatID(), "2.48", NS),
				new DateOfSignature(record.getDateOfSignature(), "2.49", NS), new AmendmentIndicator(isAmendment(paramSepa, record), "2.50", NS), "2.47", NS), "2.46", NS);
		
		if (isAmendment(paramSepa, record)) {
			AmendmentInformationDetails amendmentInformationDetails = construitAmendmentInformationDetailsParPrelevementRecord(paramSepa, record);
			element.getMandateRelatedInformation().setAmendmentInformationDetails(amendmentInformationDetails);
		}
		return element;
	}

	protected boolean isAmendment(ParametresSepa paramSepa,
			PrelevementSepaRecord record) {
		return record.getAmendmentIndicator() || !record.getPreviousICS().equals(paramSepa.getIcs()) || !record.getPreviousCreditorName().equals(paramSepa.getTgNom());
	}
	
	protected static DebtorAgent getNewDebtorAgent(String bic, String chapitre){
		return new DebtorAgent(new FinancialInstitutionIdentification(new BIC(bic, chapitre, NS), chapitre, NS), chapitre, NS);
	}

	/**
	 * @param record
	 * @return une instance de AmendmentInformationDetails pour le niveau lot du fichier
	 */
	public static AmendmentInformationDetails construitAmendmentInformationDetailsParPrelevementRecord(ParametresSepa paramSepa, PrelevementSepaRecord record) {
		AmendmentInformationDetails amendmentInformationDetails = new AmendmentInformationDetails("2.51", NS);
		if (!record.getPreviousMandatID().equals(record.getMandatID())) {
			amendmentInformationDetails.setOriginalMandateIdentification(new OriginalMandateIdentification(record.getPreviousMandatID(), "2.52", NS));
		}
		
		if (!record.getPreviousCreditorName().equals(paramSepa.getTgNom())) {
				amendmentInformationDetails.setOriginalCreditorSchemeIdentification(new OriginalCreditorSchemeIdentification(new Name(record.getPreviousCreditorName(), "2.53", NS), "2.53", NS));
		}
		
		if (!record.getPreviousICS().equals(paramSepa.getIcs())) {
			if (amendmentInformationDetails.getOriginalCreditorSchemeIdentification()!=null) {
				amendmentInformationDetails.getOriginalCreditorSchemeIdentification().setIdentification(getNewIdentification(record));
			} else {
				amendmentInformationDetails.setOriginalCreditorSchemeIdentification(new OriginalCreditorSchemeIdentification(getNewIdentification(record), "2.53", NS));
			}
		}
		
		if (!(record.getPreviousDebtorBIC().equals(record.getDebtorBIC()))) {
			amendmentInformationDetails.setOriginalDebtorAgent((new OriginalDebtorAgent(new FinancialInstitutionIdentification(new Other(new Identification("SMNDA", "2.58", NS), "2.58", NS), "2.58", NS), "2.58", NS)));
		} else if (!record.getPreviousDebtorIBAN().equals(record.getDebtorIBAN())) {
			amendmentInformationDetails.setOriginalDebtorAccount(new OriginalDebtorAccount(new Identification(new IBAN(record.getPreviousDebtorIBAN(), "2.57", NS), "2.57", NS), "2.57", NS));
		}
		
		return amendmentInformationDetails;
	}

	protected static Identification getNewIdentification(PrelevementSepaRecord record) {
		Identification element;
		if (record.getMigrationNNE()) {
			element = new Identification(new PrivateIdentification(new Other(
					new Identification(record.getPreviousICS(), "2.53", NS), "2.53", NS), "2.53", NS), "2.53", NS);
		}
		else {
			element = new Identification(new PrivateIdentification(new Other(
					new Identification(record.getPreviousICS(), "2.53", NS), new SchemeName(new Proprietary("SEPA", "2.53", NS), "2.27", NS), "2.53", NS), "2.53", NS), "2.53", NS);
		}
		return element;
	}

	/**
	 * Vérifie la présence des paramètres nécessaires à la constitution du fichier de virement.
	 * 
	 * @param paramSepa
	 * @throws Exception
	 */
	protected void checkParamSepa(ParametresSepa paramSepa) throws Exception {
		try {
			if (paramSepa.getEmetteurTransfertId() == null) {
				throw new Exception("Id emetteur obligatoire");
			}
			if (paramSepa.getCompteDftIban() == null) {
				throw new Exception("IBAN du compte DFT obligatoire");
			}
			if (paramSepa.getCompteDftTitulaire() == null) {
				throw new Exception("Titulaire du compte DFT obligatoire");
			}
			if (paramSepa.getTgIban() == null) {
				throw new Exception("IBAN de la TG obligatoire");
			}
			if (paramSepa.getTgBic() == null) {
				throw new Exception("BIC de la TG obligatoire");
			}
			if (paramSepa.getTgCodique() == null) {
				throw new Exception("Codique de la TG obligatoire");
			}
			if (paramSepa.getTgNom() == null) {
				throw new Exception("Nom de la TG obligatoire");
			}
			if (paramSepa.getIcs() == null) {
				throw new Exception("ICS obligatoire");
			}
			if (paramSepa.getTgCodique().length() != 7) {
				throw new Exception("Le codique de la TG doit comporter exactement 7 caractères.");
			}
			if (paramSepa.getIcs().length() != 13) {
				throw new Exception("L'ICS doit comporter exactement 13 caractères.");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur de paramétrage (cf. maracuja.sepa_sdd_param) : " + e.getMessage());
		}
	}
}
