/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

/**
 * Permet de stocker les paramètres pour la constitution d'un message SEPA.
 * 
 * @author rprin
 */
public class ParametresSepa {
	private String emetteurTransfertId;
	private String tgIban;
	private String tgBic;
	private String tgCodique;
	private String tgNom;
	private String compteDftIban;
	private String compteDftTitulaire;
	private String emetteurNom;
	private String devise;
	private String ics;
	

	/**
	 * @return Identification de l'établissement coté TG. Permet à la TG d'identifier l'émetteur du fichier de virement. Utilisé dans le
	 *         MessageIdentification. 8 caractères.
	 */
	public String getEmetteurTransfertId() {
		return emetteurTransfertId;
	}

	/**
	 * @param emetteurTransfertId Identification de l'établissement coté TG. Permet à la TG d'identifier l'émetteur du fichier de virement. Utilisé
	 *            dans le MessageIdentification. 8 caractères.
	 */
	public void setEmetteurTransfertId(String emetteurTransfertId) {
		this.emetteurTransfertId = emetteurTransfertId;
	}

	/**
	 * @return Compte d'operation Banque de France de la TG de rattachement de l'établissement
	 */
	public String getTgIban() {
		return tgIban;
	}

	/**
	 * @param tgIban Compte d'operation Banque de France de la TG de rattachement de l'établissement
	 */
	public void setTgIban(String tgIban) {
		this.tgIban = tgIban;
	}

	/**
	 * @return BIC de la Banque de France. Attention pour les DOM, prendre celui de l'IEDOM
	 */
	public String getTgBic() {
		return tgBic;
	}

	/**
	 * @param tgBic BIC de la Banque de France. Attention pour les DOM, prendre celui de l'IEDOM
	 */
	public void setTgBic(String tgBic) {
		this.tgBic = tgBic;
	}

	/**
	 * @return PaymentIdentification_codique (donné par DGFIP, dépend de la TG de rattachement de l'établissement)
	 */
	public String getTgCodique() {
		return tgCodique;
	}

	/**
	 * @param tgCodique PaymentIdentification_codique (donné par DGFIP, dépend de la TG de rattachement de l'établissement)
	 */
	public void setTgCodique(String tgCodique) {
		this.tgCodique = tgCodique;
	}

	/**
	 * @return Nom de la TG de rattachement
	 */
	public String getTgNom() {
		return tgNom;
	}

	/**
	 * @param tgNom Nom de la TG de rattachement
	 */
	public void setTgNom(String tgNom) {
		this.tgNom = tgNom;
	}

	/**
	 * @return Code IBAN du compte DFT (de l'établisssmeent)
	 */
	public String getCompteDftIban() {
		return compteDftIban;
	}

	/**
	 * @param compteDftIban Code IBAN du compte DFT (de l'établisssmeent)
	 */
	public void setCompteDftIban(String compteDftIban) {
		this.compteDftIban = compteDftIban;
	}

	/**
	 * @return Titulaire du compte DFT (de l'établissement)
	 */
	public String getCompteDftTitulaire() {
		return compteDftTitulaire;
	}

	/**
	 * @param compteDftTitulaire Titulaire du compte DFT (de l'établissement)
	 */
	public void setCompteDftTitulaire(String compteDftTitulaire) {
		this.compteDftTitulaire = compteDftTitulaire;
	}

	/**
	 * @return Le code devise à utiliser
	 */
	public String getDevise() {
		return devise;
	}

	/**
	 * @param devise Le code devise à utiliser
	 */
	public void setDevise(String devise) {
		this.devise = devise;
	}

	public String getEmetteurNom() {
		return emetteurNom;
	}

	public void setEmetteurNom(String emetteurNom) {
		this.emetteurNom = emetteurNom;
	}
	
	
	/**
	 * @return ICS de l'organisme émetteur de SDD, ICS qui lui a été attribué par la Banque de France. Utilisé dans le
	 *         CreditorSchemeIdentification. 13 caractères.
	 */
	public String getIcs() {
		return ics;
	}

	/**
	 * @param ICS de l'organisme émetteur de SDD, ICS qui lui a été attribué par la Banque de France. Utilisé dans le
	 *         CreditorSchemeIdentification. 13 caractères.
	 */
	public void setIcs(String ics) {
		this.ics = ics;
	}

}
