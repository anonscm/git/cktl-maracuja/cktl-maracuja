/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.math.BigDecimal;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.cocktail.gfc.server.echanges.sepa.model.ControlSum;
import org.cocktail.gfc.server.echanges.sepa.model.CreationDateTime;
import org.cocktail.gfc.server.echanges.sepa.model.GroupHeader;
import org.cocktail.gfc.server.echanges.sepa.model.InitiatingParty;
import org.cocktail.gfc.server.echanges.sepa.model.MessageIdentification;
import org.cocktail.gfc.server.echanges.sepa.model.Name;
import org.cocktail.gfc.server.echanges.sepa.model.NumberOfTransactions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;

/**
 * Construction de messages au format SEPA (se base sur des PrelevementRecord et non sur des objets métiers spécifiques).
 * 
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public abstract class ASepaFactory {
	public static String NS_XSI = "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";

	public static void validateWithSchema(NSData xmlData, String xsdLocation) throws Exception {
		SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
		File schemaLocation = new File(xsdLocation);
		Schema schema = factory.newSchema(schemaLocation);
		Validator validator = schema.newValidator();
		Source source = new StreamSource(new ByteArrayInputStream(xmlData.bytes()));

		try {
			validator.validate(source);

		} catch (SAXException ex) {
			throw new Exception("Erreur de validation XML : " + ex.getMessage());
		}
	}


	/**
	 * Extrait de "DGFIP - MISE EN PLACE DES PAIEMENTS SEPA PAR LES ÉTABLISSEMENTS TITULAIRES D’UN COMPTE DE DÉPÔT DE FONDS AU TRÉSOR - TOME 1"<br/>
	 * Il s’agit ici d’une référence technique. La structure de ce message se présente sous la forme XXXXXXXX-DFT-SCT-AAQQQ-XXX, et se décompose
	 * ainsi : <br/>
	 * un « identifiant de transfert » en 8 caractères alphanumériques. Il existe ici deux solutions en fonction de l’outil utilisé pour transmettre
	 * les fichiers de virements aux TG : <br/>
	 * il s’agit de l’identifiant qui a été fourni par le teneur de compte du titulaire d’un compte de dépôt de fonds au Trésor. Dans ce cadre,
	 * l’identifiant sera soit un identifiant CFT soit un identifiant VPN. <br/>
	 * pour les remettants qui n’utilisent pas la télétransmission, il conviendra de composer un autre identifiant : <br/>
	 * Les deux premiers caractères seront soit TG si le teneur de compte est une DRFiP/DDFiP/TG soit RN si le teneur de compte est une Recette des
	 * Finances (N correspondra alors au 4ème caractère du codique de la RF (cf. annexe n°3 et n°3 bis qui récapitule la liste des codiques).<br/>
	 * Les trois caractères suivants sont une valeur fixe : DFT. <br/>
	 * Les trois derniers caractères sont numériques et correspondent au numéro du département où se trouve le teneur de compte (exemple : 049 pour le
	 * Maine-et-Loire).<br/>
	 * Exemple : TGDFT049. <br/>
	 * <br/>
	 * Un « code type remettant » en 3 caractères alphanumériques. C’est une valeur fixe et obligatoire : DFT.<br/>
	 * la nature de la remise (3 caractères) qui est une valeur fixe et obligatoire : SCT. <br/>
	 * l’année (2 caractères numériques) et les quantièmes (3 caractères numériques : numéro du jour de l’année ; par exemple, le 5 janvier = 005).<br/>
	 * les chiffres d’incrémentation quotidienne (3 caractères numériques), par exemple : 001 pour la première remise de la journée et 002 pour la
	 * seconde ; le lendemain, il conviendra de repartir à 001.<br/>
	 * 
	 * @param idTransfertTransmisParTeneurCompte sur 8 caracteres
	 * @return L'identifiant du message construit à partir des parametres.
	 */
	public static String genMessageIdentification(String idTransfertTransmisParTeneurCompte, String natureRemise, String annee2Caracteres, String numeroDuJourDansLAnnee3Car, String numeroDeFichierDansLaJournee3Car) {
		return idTransfertTransmisParTeneurCompte.concat("-").concat("DFT-").concat(natureRemise).concat("-").concat(annee2Caracteres).concat(numeroDuJourDansLAnnee3Car).concat("-").concat(numeroDeFichierDansLaJournee3Car);
	}

	/**
	 * Vérifie la présence des paramètres nécessaires à la constitution du fichier.
	 * 
	 * @param paramSepa
	 * @throws Exception
	 */
	protected abstract void checkParamSepa(ParametresSepa paramSepa) throws Exception;

	public static Element sepaElementToDom(Document document, Node parent, org.cocktail.gfc.server.echanges.sepa.model.Element sepaElt) {
		Element res = document.createElement(sepaElt.getTagName());
		if (sepaElt.getText() != null) {
			res.appendChild(document.createTextNode(sepaElt.getText()));
		}
		//res.setNodeValue(sepaElt.getText());
		for (String key : sepaElt.getAttributes().keySet()) {
			res.setAttribute(key, sepaElt.getAttributeValue(key));
		}
		for (org.cocktail.gfc.server.echanges.sepa.model.Element eltChild : sepaElt.getChildren()) {
			res.appendChild(sepaElementToDom(document, res, eltChild));
		}
		return res;
	}
	
	
	/**
	 * crée une instance de groupheader pour l'entête du fichier
	 * @param compteDftTitulaire
	 * @param msgId
	 * @param dateTimeCreation
	 * @param montantTotal
	 * @param nbTransactions
	 * @param NS
	 * @return GroupHeader
	 */
	public static GroupHeader construitGroupHeaderParParametres(String compteDftTitulaire, String msgId, NSTimestamp dateTimeCreation,
			BigDecimal montantTotal, Integer nbTransactions, String NS) {
		GroupHeader groupHeader = new GroupHeader("1.0", NS);
		groupHeader.setMessageIdentification(new MessageIdentification(msgId, "1.1", NS));
		groupHeader.setCreationDateTime(new CreationDateTime(dateTimeCreation, "1.2", NS));
		groupHeader.setNumberOfTransactions(new NumberOfTransactions(nbTransactions, "1.6", NS));
		groupHeader.setControlSum(new ControlSum(montantTotal, "1.7", NS));
		groupHeader.setInitiatingParty(new InitiatingParty(new Name(compteDftTitulaire, "1.8", NS), "1.8", NS));
		return groupHeader;
	}

}
