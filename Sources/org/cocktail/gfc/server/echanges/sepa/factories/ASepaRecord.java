/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

import org.cocktail.zutil.server.ZStringUtil;

/**
 * Permet de mapper des éléments d'une transaction pour simplifier le passage des objets métiers vers le modèle SEPA.
 * 
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public abstract class ASepaRecord {
	private String paymentID;
	private String currency;
	private String purposeCode;
	private String remittanceInformation;

	public ASepaRecord() {
		super();
	}

	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency Code de la devise de l'opération
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPaymentID() {
		return paymentID;
	}

	/**
	 * @param paymentID Identification du l'opération sur 26 caractères. Doit permettre d'identifier l'opération unique de virement avec son destinataire
	 *            (et pas seulement le fichier). Cet élément sera ajouté au codique DGFIP et intégré dans le xml au niveau de
	 *            PaymentIdentification/EndToEndIdentification (2.30).
	 */
	public void setPaymentID(String paymentID) {
		this.paymentID = paymentID;
	}


	public String getPurposeCode() {
		return purposeCode;
	}

	/**
	 * @param purposeCode Permet de qualifier le paiement (transfert de fonds, paiment de salaire, etc.). Il s'agit d'un code iso20022. Facultatif
	 *            pour le SEPA même si l'info est bien relayée.
	 */
	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}

	public String getRemittanceInformation() {
		return remittanceInformation;
	}

	/**
	 * @param remittanceInformation Indiquer dans ce champ le libellé du paiement que le donneur d'ordre souhaite communiquer à son bénéficiaire (n°
	 *            de facture, référence de contrat ou de marché,…). Ce champ étant prioritairement restitué par les banques à leurs clients, il est
	 *            recommandé également de compléter son contenu en y ajoutant le nom de l'établissement public ou de la régie (ultimateCreditorName).
	 */
	public void setRemittanceInformation(String remittanceInformation) {
		if (remittanceInformation != null) {
			remittanceInformation = ZStringUtil.chaineSansAccents(remittanceInformation).replaceAll("\\n", " ").replaceAll("\\r", " ").replaceAll("\\t", " ");
			remittanceInformation = ZStringUtil.cut(remittanceInformation, 140);
		}
		this.remittanceInformation = remittanceInformation;

	}

	public String toString() {
		return getPaymentID() + " / " + " / " + getRemittanceInformation();
	}

	public void checkComplet() throws Exception {
		try {
			if (getCurrency() == null) {
				throw new Exception("Devise de la transaction obligatoire");
			}
			if (getPaymentID() == null) {
				throw new Exception("ID du paiement obligatoire");
			}
			if (getRemittanceInformation() == null) {
				throw new Exception("Libelle de la transaction obligatoire");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage() + " : " + toString());
		}
	}

}
