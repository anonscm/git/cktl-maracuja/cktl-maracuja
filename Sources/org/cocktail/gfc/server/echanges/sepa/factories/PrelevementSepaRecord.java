/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.gfc.server.echanges.sepa.factories;

import java.math.BigDecimal;
import java.util.Date;

import org.cocktail.zutil.server.ZStringUtil;

/**
 * Permet de mapper des éléments d'une transaction pour simplifier le passage des objets métiers vers le modèle SEPA. Exploité par
 * PrelevementSepaFactory.
 * 
 * @author Franck Bordinat <franck.bordinat at asso-cocktail.fr>
 */
public class PrelevementSepaRecord extends ASepaRecord {
	private BigDecimal instructedAmount;
	private String debtorBIC; // Identifie le debtorAgent
	private String debtorIBAN; // Identifie le debtorAccount
	private String debtorName;
	private String ultimateDebtorName;
	private String mandatID;
	private String dateOfSignature; //Date de signature du mandat (YYY-MM-DD)
	private String previousMandatID; // Identifie le OriginalMandateIdentification
	private String previousDebtorBIC; // S'il est modifié originaldebtoragent doit être égal à "SMNDA"
	private String previousDebtorIBAN; // Identifie le OriginalDebtorAccount
	private String previousICS; // Identifie le OriginalCreditorSchemeIdentification
	private String previousCreditorName; // Identifie le OriginalCreditorSchemeIdentification
	private Boolean migrationNNE = null;


	public String getMandatID() {
		return mandatID;
	}


	public PrelevementSepaRecord() {
		super();
	}

	public BigDecimal getInstructedAmount() {
		return instructedAmount;
	}

	/**
	 * @param instructedAmount montant de la transaction
	 */
	public void setInstructedAmount(BigDecimal instructedAmount) {
		this.instructedAmount = instructedAmount;
	}

	public String getDebtorBIC() {
		return debtorBIC;
	}

	public void setDebtorBIC(String debtorBIC) {
		if (debtorBIC != null) {
			debtorBIC = debtorBIC.toUpperCase().replaceAll(" ", "");
		}
		this.debtorBIC = debtorBIC;
	}

	public String getDebtorIBAN() {
		return debtorIBAN;
	}

	public void setDebtorIBAN(String debtorIBAN) {
		if (debtorIBAN != null) {
			debtorIBAN = debtorIBAN.toUpperCase().replaceAll(" ", "");
		}
		this.debtorIBAN = debtorIBAN;
	}

	public String getDebtorName() {
		return debtorName;
	}
	

	/**
	 * @param debtorName Nom + prenom ou raison sociale du débiteur du prélèvment
	 */
	public void setDebtorName(String debtorName) {
		this.debtorName = ZStringUtil.chaineSansCaracteresSpeciauxUpper(debtorName);
		this.debtorName = ZStringUtil.cut(this.debtorName, 70);
	}

	public String getUltimateDebtorName() {
		return ultimateDebtorName;
	}

	/**
	 * @param ultimateDebtorName Destinataire final du prélèvement (si le débiteur n'est pas réellement le destinataire)
	 */
	public void setUltimateDebtorName(String ultimateDebtorName) {
		this.ultimateDebtorName = ZStringUtil.chaineSansCaracteresSpeciauxUpper(ultimateDebtorName);
		this.ultimateDebtorName = ZStringUtil.cut(this.ultimateDebtorName, 70);

	}
	
	/**
	 * @param mandatID Identifiant du mandat
	 */
	public void setMandatID(String mandatID) {
		this.mandatID = ZStringUtil.chaineSansCaracteresSpeciauxUpper(mandatID);
		this.mandatID = ZStringUtil.cut(this.mandatID, 35);
	}

	public String getDateOfSignature() {
		return dateOfSignature;
	}

	public void setDateOfSignature(String dateOfSignature) {
		this.dateOfSignature = dateOfSignature;
	}


	/**
	 * @return true si le mandat a été modifié
	 */
	public Boolean getAmendmentIndicator() {
		
		if (!mandatID.equals(previousMandatID)) {
			return true;
		}
		if (!debtorBIC.equals(previousDebtorBIC)) {
			return true;
		}
		if (!debtorIBAN.equals(previousDebtorIBAN)) {
			return true;
		}
		return false;
	}


	public String getPreviousMandatID() {
		return previousMandatID;
	}


	public void setPreviousMandatID(String previousMandatID) {
		this.previousMandatID = ZStringUtil.chaineSansCaracteresSpeciauxUpper(previousMandatID);
		this.previousMandatID = ZStringUtil.cut(this.previousMandatID, 35);
	}


	public String getPreviousDebtorBIC() {
		return previousDebtorBIC;
	}


	public void setPreviousDebtorBIC(String previousDebtorBIC) {
			this.previousDebtorBIC = previousDebtorBIC;
	}


	public String getPreviousDebtorIBAN() {
		return previousDebtorIBAN;
	}


	public void setPreviousDebtorIBAN(String previousDebtorIBAN) {
		this.previousDebtorIBAN = previousDebtorIBAN;
	}


	public String getPreviousICS() {
		return previousICS;
	}


	public void setPreviousICS(String previousICS) {
		this.previousICS = previousICS;
	}
	
	public String getPreviousCreditorName() {
		return previousCreditorName;
	}


	public void setPreviousCreditorName(String previousCreditorName) {
		this.previousCreditorName = previousCreditorName;
	}


	public Boolean getMigrationNNE() {
		return migrationNNE;
	}


	public void setMigrationNNE(Boolean migrationNNE) {
		this.migrationNNE = migrationNNE;
	}

	@Override
	public String toString() {
		return getPaymentID() + " / " + getDebtorName() + " / " + getDebtorBIC() + " / " + getDebtorIBAN() + " / "
				+ getInstructedAmount() + " / " + getRemittanceInformation()+ " / " + getMandatID()+ " / " + getDateOfSignature() + " / " + getAmendmentIndicator() + " / "
				+ getPreviousMandatID() + " / " + getPreviousDebtorBIC() + " / " + getPreviousDebtorIBAN() + " / " + getPreviousICS() + " / " + getPreviousCreditorName() + " / " + getMigrationNNE().toString();
	}


	@Override
	public void checkComplet() throws Exception {
		try {
			if (getDebtorBIC() == null) {
				throw new Exception("BIC debiteur obligatoire");
			}
			if (getDebtorIBAN() == null) {
				throw new Exception("IBAN debiteur obligatoire");
			}
			if (getDebtorName() == null) {
				throw new Exception("Nom débiteur obligatoire");
			}
			if (getCurrency() == null) {
				throw new Exception("Devise de la transaction obligatoire");
			}
			if (getInstructedAmount() == null) {
				throw new Exception("Montant de la transaction obligatoire");
			}
			if (getPaymentID() == null) {
				throw new Exception("ID du paiement obligatoire");
			}
			if (getRemittanceInformation() == null) {
				throw new Exception("Libelle de la transaction obligatoire");
			}
			if (getMandatID() == null) {
				throw new Exception("Identifiant du mandat obligatoire");
			}
			if (getDateOfSignature() == null) {
				throw new Exception("Date de signature du mandat obligatoire");
			}
			if (getPreviousMandatID() == null) {
				throw new Exception("Identifiant precedent du mandat obligatoire");
			}
			if (getPreviousDebtorBIC() == null) {
				throw new Exception("BIC precedent du mandat obligatoire");
			}
			if (getPreviousDebtorIBAN() == null) {
				throw new Exception("IBAN precedent du mandat obligatoire");
			}
			if (getPreviousICS() == null) {
				throw new Exception("ICS precedent du mandat obligatoire");
			}
			if (getPreviousCreditorName() == null) {
				throw new Exception("Nom du créancier precedent du mandat obligatoire");
			}
			if (getMigrationNNE() == null) {
				throw new Exception("Indicateur de migration du mandat obligatoire");
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage() + " : " + toString());
		}
	}

}
