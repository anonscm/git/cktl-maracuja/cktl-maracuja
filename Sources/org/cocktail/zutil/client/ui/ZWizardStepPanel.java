/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.zutil.client.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JSeparator;



public abstract class ZWizardStepPanel extends ZAbstractPanel {
//    private final int topHeight = 60;
//    private final int bottomHeight = 40;
    private static final Dimension BUTTONS_DIMENSION = new Dimension(120, 25);

    private IWizardStepPanelListener myListener;
    
    private final ZCommentPanel commentPanel;

    public ZWizardStepPanel(IWizardStepPanelListener listener, final String aTitle, final String aComment, final ImageIcon aIcon) {
        super();
        myListener = listener;
        commentPanel = new ZCommentPanel(aTitle, aComment, aIcon,Color.decode("#FFFFFF"), Color.decode("#000000"), BorderLayout.WEST);
    }

    public IWizardStepPanelListener myListener() {
        return myListener;
    }

    public abstract Component getCenterPanel();


//    private JPanel buildDefaultButtonsPanel() {
//        JButton prevButton;
//        JButton nextButton;
//        JButton closeButton;
//        // JButton actionButton1;
//        
//        Box tmpBox = Box.createHorizontalBox();
//        tmpBox.add(Box.createRigidArea(new Dimension(2, bottomHeight)));
//        tmpBox.add(Box.createHorizontalGlue());
//        
//        // Bouton prev
//        if (myListener.actionPrev() != null) {
//            prevButton = new JButton(myListener.actionPrev());
//            prevButton.setPreferredSize(BUTTONS_DIMENSION);
//            prevButton.setMinimumSize(BUTTONS_DIMENSION);
//            prevButton.setHorizontalAlignment(SwingConstants.LEFT);
//            prevButton.setHorizontalTextPosition(SwingConstants.RIGHT);
//            
//            if (myListener.actionPrev().isVisible()) {
//                tmpBox.add(prevButton);
//                // separateur
//                tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
//            }
//        }
//        
//        if (myListener.actionNext() != null) {
//            nextButton = new JButton(myListener.actionNext());
//            nextButton.setMinimumSize(BUTTONS_DIMENSION);
//            nextButton.setPreferredSize(BUTTONS_DIMENSION);
//            nextButton.setHorizontalAlignment(SwingConstants.LEFT);
//            nextButton.setHorizontalTextPosition(SwingConstants.RIGHT);
//            if (myListener.actionNext().isVisible()) {
//                tmpBox.add(nextButton);
//                // separateur
//                tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
//            }
//        }
//        
//        if (myListener.actionSpecial1() != null) {
//            JButton button1 = new JButton(myListener.actionSpecial1());
//            button1.setMinimumSize(BUTTONS_DIMENSION);
//            button1.setPreferredSize(BUTTONS_DIMENSION);
//            button1.setHorizontalAlignment(SwingConstants.LEFT);
//            button1.setHorizontalTextPosition(SwingConstants.RIGHT);
//            tmpBox.add(button1);
//            // separateur
//            tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
//        }
//        
//        // Bouton Fermer
//        if (myListener.actionClose() != null) {
//            closeButton = new JButton(myListener.actionClose());
//            closeButton.setPreferredSize(BUTTONS_DIMENSION);
//            closeButton.setMinimumSize(BUTTONS_DIMENSION);
//            closeButton.setHorizontalAlignment(SwingConstants.LEFT);
//            closeButton.setHorizontalTextPosition(SwingConstants.RIGHT);
//            if (myListener.actionClose().isVisible()) {
//                tmpBox.add(closeButton);
//                tmpBox.add(Box.createRigidArea(new Dimension(25, 30)));
//            }
//        }
//        
//        JPanel tmpJPanel = new JPanel(new BorderLayout());
//        tmpJPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
//        tmpJPanel.setPreferredSize(new Dimension(1, bottomHeight));
//        tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
//        tmpJPanel.add(new JPanel(), BorderLayout.CENTER);
//        tmpJPanel.add(tmpBox, BorderLayout.LINE_END);
//        return tmpJPanel;
//        
//    }
    private JPanel buildDefaultButtonsPanel() {
        final ArrayList actions = new ArrayList();
        if (myListener.actionPrev() != null) {
            actions.add(myListener.actionPrev());
        }
        if (myListener.actionNext() != null) {
            actions.add(myListener.actionNext());
        }
        if (myListener.actionSpecial1() != null) {
            actions.add(myListener.actionSpecial1());
        }
        if (myListener.actionClose() != null) {
            actions.add(myListener.actionClose());
        }
        
        final ArrayList buttons = ZUiUtil.getButtonListFromActionList(actions, BUTTONS_DIMENSION.width, BUTTONS_DIMENSION.height);
//        final Box buttonsPanel = ;
        
        final JPanel tmpJPanel = new JPanel(new BorderLayout());
        tmpJPanel.setBorder(BorderFactory.createEmptyBorder(10, 4, 10, 20));
//        tmpJPanel.setPreferredSize(new Dimension(1, bottomHeight));
//        tmpJPanel.setMinimumSize(tmpJPanel.getPreferredSize());
        tmpJPanel.add(new JPanel(), BorderLayout.CENTER);
        tmpJPanel.add(ZUiUtil.buildGridLine(buttons), BorderLayout.EAST);
        return tmpJPanel;

    }

    /**
     * Crée les boutons d'action. Par defaut, fait appel à
     * buildDefaultButtonsPanel. Dérivez cette méthode pour personnaliser.
     * 
     * @return
     */
    protected JPanel createActionButtonsUI() {
        return buildDefaultButtonsPanel();
    }

    /**
     * Méthode à appeler pour contruire le panel.
     * 
     */
    public void initGUI() {
        this.setOpaque(true);
        this.setLayout(new BorderLayout());
        // le haut
        Box tmpBox = Box.createVerticalBox();
        tmpBox.add(commentPanel);
        tmpBox.add(new JSeparator());

        Box tmpBoxBottom = Box.createVerticalBox();
        tmpBoxBottom.add(new JSeparator());
        tmpBoxBottom.add(createActionButtonsUI());

        this.add(tmpBox, BorderLayout.NORTH);
        this.add(getCenterPanel(), BorderLayout.CENTER);
        this.add(tmpBoxBottom, BorderLayout.SOUTH);
    }

    public interface ZStepListener {
        public ZWizardStepPanel.ZStepAction actionPrev();

        public ZWizardStepPanel.ZStepAction actionNext();

        public ZWizardStepPanel.ZStepAction actionClose();

        public ZWizardStepPanel.ZStepAction actionSpecial1();

        public ZWizardStepPanel.ZStepAction actionSpecial2();

    }

    public static abstract class ZStepAction extends AbstractAction {
        public abstract boolean isVisible();
    }

    /**
     * Permet de mettre en place des controles de navigation entre les
     * differentes etapes d'un assistant.
     * 
     * Un changement d'etape est possible suite a un Next entre A et B si
     * A.canLeaveWithValidation() renvoie true puis B.canArriveWithValidation()
     * renvoit egalement true.<br>
     * 
     * Un changement d'etape est possible suite e un Previous entre B et A si B
     * canLeaveWithoutValidation() renvoie true et
     * A.canArriveWithoutValidation() renvoie egalement true.
     */
    public interface IWizardStepPanelListener extends ZStepListener {
        /**
         * Doit renvoyer true si on peut passer a une etape precedente (sans
         * validation du contenu)
         */
        public boolean canLeaveWithoutValidation() throws Exception;

        /**
         * Doit renvoyer true si on peut passer a une etape suivante (avec
         * validation)
         */
        public boolean canLeaveWithValidation() throws Exception;

        /**
         * Doit renvoyer true si on peut entrer quand on vient d'une etape
         * precedente (suite logique)
         */
        public boolean canArriveWithValidation() throws Exception;

        /**
         * Doit renvoyer true si on peut entrer quand on vient d'une etape
         * suivante (retour arriere)
         */
        public boolean canArriveWithoutValidation() throws Exception;

    }

}
