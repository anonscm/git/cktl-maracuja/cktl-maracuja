TELETRANSMISSION DES FICHIERS DE VIREMENTS BANCAIRES

6) Lancer la connexion VPN TRESOR

Cliquer sur l'ic�ne VPN Client.
Une fen�tre s'ouvre avec un menu par ic�nes. Cliquer sur l'ic�ne "Connect".
Confirmer la connexion. 
L'identifiant utilisateur s'affiche par d�faut (111799-xt). Il n'y a pas lieu de le changer. 
Saisir le mot de passe : qr9s26ym
Confirmer la connexion deux fois ("Continue" dans la premi�re fen�tre, puis "OK" dans la seconde).

7) Entrer sur le site Canop�e

Lorsque la connexion est �tablie, il faut lancer le navigateur internet (icone "Accueil Partenaires").

Saisir le nom d'utilisateur et le mot de passe,
puis cliquer sur "OK".

La page de bienvenue sur le r�seau du Tr�sor Public qui s'affiche ensuite comporte un menu dans la colonne de droite (avec un seul item actuellement pour Paris 8).
Cliquer sur le signe "+" � c�t� de "Transfert de fichiers").
Cliquer ensuite sur l'option "Cft".
On arrive alors sur la page d'envoi des fichiers.

8) Envoyer le fichier

Cliquer sur le bouton "Envoi d'un fichier".
On passe sur la page "Emission de fichier"
Saisir le site destinataire : xxxxxxxx
Saisir l'identifiant de fichier : TPGDVP
Cliquer sur "Parcourir" pour s�lectionner le fichier � envoyer.
Choisir le fichier dans la fen�tre qui affiche la liste des fichiers pr�sent sur le bureau.

ATTENTION : C'est � ce moment l� qu'il faut �tre tr�s vigilant. Si il y a plusieurs fichiers intitul�s "Fichier_BDF_0000..." sur le bureau, il y a possibilit� de se tromper  
et de transmettre � nouveau un fichier d�j� envoy� et dont les virements seront donc pay�s en double si personne ne s'aper�oit de l'erreur dans l'heure qui suit. 
Il est donc pr�f�rable, une fois les op�rations termin�es, de d�truire le ou les fichiers BDF pr�sents sur le bureau pour �viter toute m�prise au prochain transfert.

Cliquer sur "Emission"

Le serveur indique si la transmission s'est bien pass�e ("Fichier correctement �mis").
Fermer la fen�tre.

En revenant sur la page pr�c�dente, on doit constater que le fichier a bien �t� rajout� dans la liste des transferts avec le code �tat "E".

9) Terminer l'op�ration

Quitter Internet Explorer
Interrompre la connexion VPN en cliquant sur le bouton VPN Client figurant en bas � droite dans la barre des t�ches. 
Cliquer sur l'ic�ne "Disconnect" de la nouvelle fen�tre. Confirmer.
Eteindre l'ordinateur. Si l'arr�t de l'ordinateur intervient trop t�t, il est possible que la fermeture de la connexion VPN Client soit signal�e une deuxi�me fois. Dans ce cas, il faut cliquer sur le bouton "Terminer maintenant" qui est propos� pour que le syst�me puisse stopper correctement.


