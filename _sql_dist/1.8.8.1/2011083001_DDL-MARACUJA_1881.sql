SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.8.1
-- Date de publication : 30/08/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Ajout d'un parametre pour modifie les dates calculees de premiere echeance pour les echeanciers des frais d'inscriptions (scolarix)
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.8.1',  null, '');
commit;

CREATE OR REPLACE PACKAGE MARACUJA.scol_echeancier IS

-- API PUBLIQUE pour creer des echeanciers / echeances --

-- renvoie la date de premiere echeance a partir de la date d'inscription
function getDatePremiereEcheance(exeOrdre number, dateInscription date) return date;

-- renvoie un identifiant d'echeancier
function creerEcheancier3Echeances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscription date) return number;

-- affecte le bordereau pour l'echeancier
procedure associerEcheancierEtBordereau(echeId number, borId number);

-- affecte l'echeancier au bordereau_brouillard
procedure associerEcheancierEtBob(echeId number, bobOrdre number);

-- verifie l echeancier (controle que l''echeancier est coherent avec ses prelevements)
procedure verifierEcheancier(echeId number);

-- supprime l'echeancier (si c'est possible, sinon exception)
procedure supprimerEcheancier(echeId number);

END;
/


GRANT EXECUTE ON MARACUJA.SCOL_ECHEANCIER TO GARNUCHE;


CREATE OR REPLACE PACKAGE BODY MARACUJA.scol_echeancier IS
-- PUBLIC --



function getDatePremiereEcheance(exeOrdre number, dateInscription date) return date
is
    nbjoursMoisSuivantStr varchar2(10);
    nbjoursMoisSuivant integer;
    numeroJourEcheanceStr varchar2(10);
    numeroJourEcheance integer;
    dateFinMois date;
    dateInscriptionClean date;
    moisSuivant date;
    dateEcheance date;
    
begin
    nbjoursMoisSuivantStr := JEFY_ADMIN.API_PARAMETRE.GET_PARAM_MARACUJA ( 'org.cocktail.gfc.echeancier.dateecheance.nbjoursmoissuivant', exeOrdre, '0' );
    nbjoursMoisSuivant := to_number(nbjoursMoisSuivantStr);
    numeroJourEcheanceStr := JEFY_ADMIN.API_PARAMETRE.GET_PARAM_MARACUJA ( 'org.cocktail.gfc.echeancier.dateecheance.numerojour', exeOrdre, '5' );
    numeroJourEcheance := to_number(numeroJourEcheanceStr);    
     
    
    dateInscriptionClean := to_date(to_char(dateInscription, 'dd/mm/yyyy'),'dd/mm/yyyy');
    dateFinMois := LAST_DAY(dateInscriptionClean);
    
    if (dateInscriptionClean >= (dateFinMois + 1 - nbjoursMoisSuivant)) then
         moisSuivant := add_months(dateInscriptionClean, 2);
    else
         moisSuivant := add_months(dateInscriptionClean, 1);
    end if;
    
    
    dateEcheance := to_date( to_char(numeroJourEcheance, '00')  || '/' || to_char(moisSuivant, 'mm/yyyy')   ,'dd/mm/yyyy');
    return dateEcheance;
end;

function creerEcheancier3Echeances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscription date) return number
IS
    echeId number;
    flag integer;
    
    bordereau_data maracuja.bordereau%rowtype;
    personne_data  grhum.personne%rowtype;    
   
    echeLibelle maracuja.echeancier.eche_Libelle%type;
    echeReference maracuja.echeancier.ECHE_REF_FACTURE_EXTERNE%type;
    nbEcheances integer;
    date1 date;
    date2 date;
    date3 date;
    moisSuivant date;
    montant1 number(12,2);
    montant2 number(12,2);
    montant3 number(12,2);    
    echeanceId maracuja.prelevement.PREL_PRELEV_ORDRE%type;
    
    --delaiMinimalTraitement integer;
    
   
    
BEGIN
    
    -- verif

    select count(*) into flag from grhum.personne where pers_id = persId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.PERSONNE pour pers_id = '|| persId ||'.');    
    end if; 
    
    select * into personne_data from grhum.personne where pers_id= persId;  


    -- determiner les valeurs
    --exeOrdre := bordereau_data.exe_ordre;
    echeLibelle := 'INSCRIPTION ETUDIANT '|| personne_data.pers_libelle || ' ' || personne_data.pers_lc;
    echeReference := '';
    nbEcheances := 3;
   -- delaiMinimalTraitement := 6;
    
    
    
   -- moisSuivant := add_months(dateInscription, 1);
    --date1 := to_date( '05/' || to_char(moisSuivant, 'mm/yyyy')   ,'dd/mm/yyyy');
    date1 := GETDATEPREMIEREECHEANCE ( exeOrdre, dateInscription );
    date2 := add_months(date1,1);
    date3 := add_months(date2,1);
        
    montant3 := floor(montantTotal/3);
    montant2 := floor(montantTotal/3);
    montant1 := montantTotal - montant2 - montant3;
    
    
    
    -- creer l'echeancier
    echeId := MARACUJA.API_ECHEANCIER.CREERECHEANCIER ( exeOrdre, persId, fouOrdre, echeLibelle, echeReference, montantTotal, nbEcheances, date1 );        
    
    -- creer les echeances    
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 1/3', 1, montant1, date1, ribOrdre );
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 2/3', 2, montant2, date2, ribOrdre );
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 3/3', 3, montant3, date3, ribOrdre );
    return echeId;
END;




procedure associerEcheancierEtBordereau(echeId number, borId number)
is
    flag integer;
begin

    select count(*) into flag from maracuja.bordereau where bor_id=borId;
        if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table MARACUJA.BORDEREAU pour bor_id = '|| borId ||'.');    
    end if; 


    select count(*) into flag from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE=echeId;
        if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table MARACUJA.ECHEANCIER pour ECHE_ECHEANCIER_ORDRE = '|| echeId ||'.');    
    end if; 

    -- 


    update maracuja.echeancier set bor_id=borId where ECHE_ECHEANCIER_ORDRE=echeId;


end;


procedure verifierEcheancier(echeId number)
is
begin
    MARACUJA.API_ECHEANCIER.verifierEcheancier ( echeId );
end;

procedure supprimerEcheancier(echeId number)
is
begin
    MARACUJA.API_ECHEANCIER.supprimerEcheancier ( echeId );
end;

procedure associerEcheancierEtBob(echeId number, bobOrdre number) is
begin
    MARACUJA.API_ECHEANCIER.associerEcheancierEtBob ( echeId, bobOrdre );
end;

END;
/


GRANT EXECUTE ON MARACUJA.SCOL_ECHEANCIER TO GARNUCHE;



create procedure grhum.inst_patch_maracuja_1881 is
begin
	
Insert into MARACUJA.PARAMETRE
   ( PAR_ORDRE, EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_VALUE)
 select maracuja.parametre_seq.nextval, 
        exe_ordre, 
        'Nombre de jours avant le debut du mois suivant pour que la date calculée de la première échéance bascule sur le mois m+2 au lieu de m+1', 
        'org.cocktail.gfc.echeancier.dateecheance.nbjoursmoissuivant', 
        '0'
        from jefy_admin.exercice where exe_stat<>'C'; 
        
Insert into MARACUJA.PARAMETRE
   ( PAR_ORDRE, EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_VALUE)
 select maracuja.parametre_seq.nextval, 
        exe_ordre, 
        'Numero du jour dans le mois pour le clacul de l''echeance', 
        'org.cocktail.gfc.echeancier.dateecheance.numerojour', 
        '5'
        from jefy_admin.exercice where exe_stat<>'C'; 

    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.8.1';           
end;
/






