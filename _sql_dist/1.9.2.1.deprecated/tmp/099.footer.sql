

create or replace procedure grhum.inst_patch_maracuja_1921
is
begin
	
	insert into maracuja.type_numerotation
	            (tnu_entite,
	             tnu_libelle,
	             tnu_ordre
	            )
	select      'BORDEREAU', 'BORDEREAU DE MANDATS D''EXTOURNE', 25 from dual where not exists (select * from maracuja.type_numerotation where tnu_ordre=25 and tnu_libelle='BORDEREAU DE MANDATS D''EXTOURNE');
	
	insert into maracuja.type_bordereau
	            (tbo_libelle,
	             tbo_ordre,
	             tbo_sous_type,
	             tbo_type,
	             tbo_type_creation,
	             tnu_ordre
	            )
	select      'BORDEREAU DE MANDATS D''EXTOURNE', 50, 'EXTOURNE', 'BTMEX', null, 25 from dual where not exists(select * from maracuja.type_bordereau where tbo_ordre=50 and tbo_libelle='BORDEREAU DE MANDATS D''EXTOURNE');
	commit ;
	
   jefy_admin.patch_util.end_patch (4, '1.9.2.1');
end;
/

