set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.1 BETA
-- Date de publication : 20/09/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- * Ajout Modes de paiement pour fonctionalités d'extourne. Ces ajouts sont effectués via la procedure maracuja.inst_mp_extourne. 
--Le but de la procedure est de créer deux modes de paiements : A extourner (hors masse salariale) et A extourner (masse salariale). 
--Pour chaque mode de paiement, un compte de TVA et un compte de Visa doivent êre affectés. Vous obtiendrez un message d'erreur si 
--les comptes en question ne sont pas actifs dans votre plan comptable. Si c'est le cas contactez l'agence comptable pour résoudre 
--le problème et exécutez à nouveau la procédure. Une autre solution est de ne pas installer les modes de paiements 
--en question et de demander à l'agence comptable de se charger de la création des modes de paiements du domaine "Extourne". 
--La présence de ces modes de paiements dans la base de données sera indispensable pour l'utilisation de l'extourne en fin d'année. 
-- * Compte_fi/Procedure de calcul de la CAF : ajout d'un message d'erreur quand les SIG n'ont pas été calculés 
-- * ajout des types de bordereaux pour gestion de l'extourne
-- * correction bug wo54 avec les preference
-- * Modifications dans divers packages pour gestion de l'extourne
-- * DT #4928 - Visa dépenses : Régression/erreur lors du rejet total d'un bordereau de mandat
-- * DT #4931 - Lancement : Impossible de lancer Maracuja via ZAP
-- * DT #4932 - Visa dépenses : Erreur lors du rejet total d'un bordereau de mandat d'extourne
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.1.4', 'MARACUJA' );

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2101';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.2.1', null );
commit ;

grant select on jefy_depense.extourne_liq_def to maracuja with grant option;


execute grhum.drop_object('maracuja','extourne_mandat','table');
execute grhum.drop_object('maracuja','extourne_mandat_seq','sequence');

create sequence maracuja.extourne_mandat_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create table maracuja.extourne_mandat (
  em_id            number not null,
  man_id_n         number null,
  man_id_n1        number not null
)
tablespace gfc;

comment on table maracuja.extourne_mandat is 'Memorise un mandat d''extourne sur N+1 et son mandat initial correspondant eventuel sur N';
comment on column maracuja.extourne_mandat.man_id_n is 'Reference au mandat initial sur N (null dans le cas de demarrage d''un exercice)';
comment on column maracuja.extourne_mandat.man_id_n1 is 'Reference au mandat d''extourne sur N+1';



alter table maracuja.extourne_mandat add (primary key (em_id) using index tablespace gfc_indx );
alter table maracuja.extourne_mandat add (constraint fk_extourne_mandat_manidn  foreign key (man_id_n)  references maracuja.mandat (man_id));
alter table maracuja.extourne_mandat add (constraint fk_extourne_mandat_manidn1  foreign key (man_id_n1)  references maracuja.mandat (man_id));
alter table maracuja.extourne_mandat add (constraint uk_extourne_mandat  unique (man_id_n, man_id_n1) using index tablespace gfc_indx);


create or replace procedure maracuja.inst_mp_extourne
is
   modcodeextournehorssal   maracuja.mode_paiement.mod_code%type;
   modcodeextournesal       maracuja.mode_paiement.mod_code%type;
   pconumtvahorssal         maracuja.mode_paiement.pco_num_tva%type;
   pconumtvasal             maracuja.mode_paiement.pco_num_tva%type;
   pconumvisahorssal        maracuja.mode_paiement.pco_num_visa%type;
   pconumvisasal            maracuja.mode_paiement.pco_num_visa%type;
   flag                     integer;
   compteabsents            varchar2 (500);
begin
   modcodeextournehorssal := '991';
   modcodeextournesal := '992';
   pconumtvahorssal := '44586';
   pconumtvasal := '44586';
   pconumvisahorssal := '4081';
   pconumvisasal := '4286';
   

   -- verifier la presence des codes paiement
   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournehorssal and mod_dom <> 'A EXTOURNER';

   if (flag > 0) then
      raise_application_error (-20001,
                                  'Le code '
                               || modcodeextournehorssal
                               || ' est deja utilise dans les modes de paiement. Vous devez specifier un nouveau code pour le mode de paiement "Charge à payer à extourner (hors masse salariale)" dans la procedure maracuja.inst_mp_extourne'
                              );
   end if;

   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournesal and mod_dom <> 'A EXTOURNER';

   if (flag > 0) then
      raise_application_error (-20001,
                               'Le code ' || modcodeextournehorssal
                               || ' est deja utilise dans les modes de paiement. Vous devez specifier un nouveau code pour le mode de paiement "Charge à payer à extourner (masse salariale)" dans la procedure maracuja.inst_mp_extourne'
                              );
   end if;

   -- verifier presence des comptes de TVA
   compteabsents := '';

   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumtvahorssal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumtvahorssal || ' ';
   end if;

   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumtvasal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumtvasal || ' ';
   end if;

   -- verifier presence des comptes de VISA
   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumvisahorssal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumvisahorssal || ' ';
   end if;

   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumvisasal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumvisasal || ' ';
   end if;

   compteabsents := trim (compteabsents);
   compteabsents := replace (compteabsents, ' ', ', ');

   if (length (compteabsents) > 0) then
      raise_application_error (-20001,
                                  'Le(s) compte(s) '
                               || compteabsents
                               || ' devrai(en)t être actif(s) dans le plan comptable pour l''exercice en cours. Si vous ne voulez pas utiliser ces comptes, modifiez la procedure maracuja.inst_mp_extourne pour utiliser d''autres comptes.'
                              );
   end if;

   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournehorssal and mod_dom = 'A EXTOURNER';

   if (flag = 0) then
      insert into maracuja.mode_paiement
                  (exe_ordre,
                   mod_libelle,
                   mod_ordre,
                   mod_validite,
                   pco_num_paiement,
                   pco_num_visa,
                   mod_code,
                   mod_dom,
                   mod_visa_type,
                   mod_ema_auto,
                   mod_contrepartie_gestion,
                   pco_num_tva,
                   pco_num_tva_ctp,
                   mod_paiement_ht
                  )
         select exe_ordre,
                'Charge à payer à extourner (hors masse salariale)',
                maracuja.mode_paiement_seq.nextval,
                'VALIDE',
                null,
                pconumvisahorssal,
                modcodeextournehorssal,
                'A EXTOURNER',
                null,
                0,
                null,
                pconumtvahorssal,
                null,
                'N'
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');
   end if;

   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournesal and mod_dom = 'A EXTOURNER';

   if (flag = 0) then
      insert into maracuja.mode_paiement
                  (exe_ordre,
                   mod_libelle,
                   mod_ordre,
                   mod_validite,
                   pco_num_paiement,
                   pco_num_visa,
                   mod_code,
                   mod_dom,
                   mod_visa_type,
                   mod_ema_auto,
                   mod_contrepartie_gestion,
                   pco_num_tva,
                   pco_num_tva_ctp,
                   mod_paiement_ht
                  )
         select exe_ordre,
                'Charge à payer à extourner (masse salariale)',
                maracuja.mode_paiement_seq.nextval,
                'VALIDE',
                null,
                pconumvisasal,
                modcodeextournesal,
                'A EXTOURNER',
                null,
                0,
                null,
                pconumtvasal,
                null,
                'N'
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');
   end if;
end;
/


CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_DETERMINATION_CAF" (exeordre number, agregatlib varchar2, methodeebe varchar2)
is
   total                number (12, 2);
   totalant             number (12, 2);
   lib                  varchar2 (100);
   lib1                 varchar2 (50);
   libant               varchar2 (50);
   total_produits       number (12, 2);
   total_produits_ant   number (12, 2);
   total_charges        number (12, 2);
   total_charges_ant    number (12, 2);
   formule              varchar2 (50);
   ebe                  number (12, 2);
   ebe_ant              number (12, 2);
   resultat             number (12, 2);
   resultat_ant         number (12, 2);
   cpt                  number;
begin
--*************** DETERMINATION A PARTIR DE EBE  *********************************
   if methodeebe = 'O' then
      delete      caf
            where exe_ordre = exeordre and ges_code = agregatlib and methode_ebe = 'O';

      total_produits := 0;
      total_charges := 0;
      total_produits_ant := 0;
      total_charges_ant := 0;
      formule := '';

      --**** RECUPERATION EBE *******
      select count (*)
      into   cpt
      from   sig
      where  ges_code = agregatlib and exe_ordre = exeordre and (groupe1 = 'Résultat d''exploitation' or groupe1 = 'Resultat d''exploitation') and commentaire = 'EBE';

      if cpt = 0 then
         raise_application_error (-20001, 'Vous devez au préalable calculer votre Excédent/Insuffisance Brute d''Exploitation à partir des Soldes Intermédaires de Gestion');
      end if;

      select nvl (sig_montant, 0),
             groupe2,
             sig_libelle,
             nvl (sig_montant_ant, 0),
             groupe_ant
      into   ebe,
             lib1,
             lib,
             ebe_ant,
             libant
      from   sig
      where  ges_code = agregatlib and exe_ordre = exeordre and (groupe1 = 'Résultat d''exploitation' or groupe1 = 'Resultat d''exploitation') and commentaire = 'EBE';

      if lib1 = 'charges' then
         ebe := -ebe;
      end if;

      if libant = 'charges' then
         ebe_ant := -ebe_ant;
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   ebe,
                   ebe_ant,
                   formule
                  );

      -- ********  Produits ***********
      lib1 := 'produits';
      total := resultat_compte_agr (exeordre, '75%', agregatlib) + resultat_compte_agr (exeordre, '1875%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '75%', agregatlib) + resultat_compte_agr (exeordre - 1, '1875%', agregatlib);
      lib := '+ Autres produits "encaissables" d''exploitation';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '791%', agregatlib) + resultat_compte_agr (exeordre, '18791%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '791%', agregatlib) + resultat_compte_agr (exeordre - 1, '18791%', agregatlib);
      lib := '+ Transferts de charges';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '76%', agregatlib) + resultat_compte_agr (exeordre, '796%', agregatlib) + resultat_compte_agr (exeordre, '1876%', agregatlib) + resultat_compte_agr (exeordre, '18796%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '76%', agregatlib) + resultat_compte_agr (exeordre, '796%', agregatlib) + resultat_compte_agr (exeordre - 1, '1876%', agregatlib) + resultat_compte_agr (exeordre, '18796%', agregatlib);
      lib := '+ Produits financiers "encaissables" (a)';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total :=
           resultat_compte_agr (exeordre, '771%', agregatlib)
         + resultat_compte_agr (exeordre, '778%', agregatlib)
         + resultat_compte_agr (exeordre, '797%', agregatlib)
         + resultat_compte_agr (exeordre, '18771%', agregatlib)
         + resultat_compte_agr (exeordre, '18778%', agregatlib)
         + resultat_compte_agr (exeordre, '18797%', agregatlib);
      totalant :=
           resultat_compte_agr (exeordre - 1, '771%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '778%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '797%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '18771%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '18778%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '18797%', agregatlib);
      lib := '+ Produits exceptionnels "encaissables" (b)';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      -- ********  charges ***********
      lib1 := 'charges';
      total := resultat_compte_agr (exeordre, '65%', agregatlib) + resultat_compte_agr (exeordre, '1865%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '65%', agregatlib) + resultat_compte_agr (exeordre - 1, '1865%', agregatlib);
      lib := '- Autres charges "décaissables" d''exploitation';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '66%', agregatlib) + resultat_compte_agr (exeordre, '1866%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '66%', agregatlib) + resultat_compte_agr (exeordre - 1, '1866%', agregatlib);
      lib := '- Charges financières "décaissables" (c)';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '671%', agregatlib) + resultat_compte_agr (exeordre, '678%', agregatlib) + resultat_compte_agr (exeordre, '18671%', agregatlib) + resultat_compte_agr (exeordre, '18678%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '671%', agregatlib) + resultat_compte_agr (exeordre - 1, '678%', agregatlib) + resultat_compte_agr (exeordre - 1, '18671%', agregatlib) + resultat_compte_agr (exeordre - 1, '18678%', agregatlib);
      lib := '- Charges exceptionnelles "décaissables" (d)';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '695%', agregatlib) + resultat_compte_agr (exeordre, '18695%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '695%', agregatlib) + resultat_compte_agr (exeordre - 1, '18695%', agregatlib);
      lib := '- Impôts sur les bénéfices';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      --- ************ Calcul de la caf *****************
      total := ebe + total_produits - total_charges;

      select count (*)
      into   cpt
      from   caf
      where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';

      if (cpt > 0) then
         select nvl (caf_montant, 0)
         into   totalant
         from   caf
         where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';
      else
         totalant := 0;
      end if;

      if total >= 0 then
         lib1 := 'produits';
         lib := '= CAPACITE D''AUTOFINANCEMENT';
      else
         lib1 := 'charges';
         lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );
   else
      --********* Détermination à partir du résultat **********************
      delete      caf
            where exe_ordre = exeordre and ges_code = agregatlib and methode_ebe = 'N';

--    IF sacd = 'O' THEN
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N';
--    ELSIF sacd = 'G' THEN
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'N';
--    ELSE
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'N';
--    END IF;
      total_produits := 0;
      total_charges := 0;
      formule := '';
      --**** RECUPERATION RESULTAT ********
      totalant := 0;
      lib := 'Résultat de l''exercice';

      select sum (credit) - sum (debit)
      into   resultat
      from   maracuja.cfi_ecritures_2 d, maracuja.v_agregat_gestion ag
      where  (pco_num = '120' or pco_num = '129') and d.exe_ordre = exeordre and d.exe_ordre = ag.exe_ordre and d.ges_code = ag.ges_code and ag.agregat = agregatlib;

--
--
--    IF sacd = 'O' THEN
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
--    ELSIF sacd = 'G' THEN
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
--    ELSE
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
--    END IF;

      -- N-1
      select count (*)
      into   cpt
      from   caf
      where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'RTAT';

      if (cpt > 0) then
         select nvl (caf_montant, 0)
         into   totalant
         from   caf
         where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'RTAT';
      else
         totalant := 0;
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   resultat,
                   totalant,
                   'RTAT'
                  );

      -- ********  Charges ***********
      lib1 := 'charges';
      total := resultat_compte_agr (exeordre, '681%', agregatlib) + resultat_compte_agr (exeordre, '686%', agregatlib) + resultat_compte_agr (exeordre, '687%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '681%', agregatlib) + resultat_compte_agr (exeordre - 1, '686%', agregatlib) + resultat_compte_agr (exeordre - 1, '687%', agregatlib);
      lib := '+ Dotations aux amortissements et provisions';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '675%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '675%', agregatlib);
      lib := '+ Valeur comptable des éléments actifs cédés';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      -- ********  Produits ***********
      lib1 := 'produits';
      total := resultat_compte_agr (exeordre, '781%', agregatlib) + resultat_compte_agr (exeordre, '786%', agregatlib) + resultat_compte_agr (exeordre, '787%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '781%', agregatlib) + resultat_compte_agr (exeordre - 1, '786%', agregatlib) + resultat_compte_agr (exeordre - 1, '787%', agregatlib);
      lib := '- Reprises sur amortissements et provisions';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '775%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '775%', agregatlib);
      lib := '- Produits de cessions des éléments actifs cédés';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '776%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '776%', agregatlib);
      lib := '- Produits issus de la neutralisation des amortissements';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '777%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '777%', agregatlib);
      lib := '- Quote-part des subventions d''investissement virées au compte de résultat';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      --- ************ Calcul de la caf *****************
      total := resultat + total_charges - total_produits;
      totalant := 0;
      formule := 'CAF';

      if total >= 0 then
         lib1 := 'produits';
         lib := '= CAPACITE D''AUTOFINANCEMENT';
      else
         lib1 := 'charges';
         lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
      end if;

      -- N-1
      select count (*)
      into   cpt
      from   caf
      where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';

      if (cpt > 0) then
         select nvl (caf_montant, 0)
         into   totalant
         from   caf
         where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';
      else
         totalant := 0;
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );
   end if;
end;
/


create or replace force view maracuja.utilisateur_fonct (uf_ordre, utl_ordre, fon_ordre)
as
   select uf.uf_ordre,
          uf.utl_ordre,
          uf.fon_ordre
   from   jefy_admin.utilisateur_fonct uf, fonction f
   where  f.fon_ordre = uf.fon_ordre;
/



create or replace force view maracuja.preference (pref_id, pref_key, pref_default_value, pref_description, pref_personnalisable)
as
   select pref_id,
          pref_key,
          pref_default_value,
          pref_description,
          pref_personnalisable
   from   jefy_admin.preference;
/

CREATE OR REPLACE PACKAGE MARACUJA.abricot_util
is
/*
 * Copyright Cocktail, 2001-2012
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
-- rodolphe.prin at cocktail.org
   function is_dpco_tva_collectee (dpcoid integer)
      return integer;

   function is_man_sur_sacd (manid integer)
      return integer;

   function get_dpco_taux_prorata (dpcoid integer)
      return number;

   function get_dpco_montant_budgetaire (dpcoid integer)
      return number;

   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type;

   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type;

   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type;

   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type;

   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number;

   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number;

   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type;

   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type;

   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type;

   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type);
end abricot_util;
/



CREATE OR REPLACE PACKAGE MARACUJA.Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER);

PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;

PROCEDURE prv_apres_visa_bordereau (borid INTEGER);
PROCEDURE prv_apres_visa_reversement(borid INTEGER);


END;
/

CREATE OR REPLACE PACKAGE MARACUJA.api_bordereau
is
   procedure apres_visa (borid bordereau.bor_id%type);

   procedure prv_traiter_bord_rejet (lebordereau bordereau%rowtype);
    procedure prv_apres_visa_reversement (borid bordereau.bor_id%type);
   procedure emarger_visa_bord_prelevement (borid bordereau.bor_id%type);
   procedure prv_numeroter_ecritures (borid bordereau.bor_id%type);
   
   function is_bord_titres(borid bordereau.bor_id%type) return integer;
   function is_bord_mandats(borid bordereau.bor_id%type) return integer;
end;
/

CREATE OR REPLACE PACKAGE MARACUJA.api_mandat
is
   procedure viser_mandat (manid mandat.man_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type, ecdlibelleprefix varchar2);
   procedure check_mandat_avant_visa(manid mandat.man_id%type);
   function creer_ecd_libelle(mab mandat_brouillard%rowtype, lemandat mandat%rowtype, lebordereau bordereau%rowtype) return ecriture_detail.ecd_libelle%type;
   function creer_ecr_libelle(lemandat mandat%rowtype, lebordereau bordereau%rowtype) return ecriture.ecr_libelle%type;
  
end;
/
CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS

/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007
-- version du 01/10/2009 -- ajout de controles sur la generation des bd de PI

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- GES_CODE a prendre en compte en fonction du mandat.
FUNCTION  get_ges_code_for_man_id(manid NUMBER)
  RETURN comptabilite.ges_code%TYPE;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_NR1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);
function creer_depense(jefydepensebudget jefy_depense.depense_budget%rowtype, manid mandat.man_id%type) return maracuja.depense.dep_id%type;


-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;
function getFournisNom (fouOrdre integer) return varchar;
function recup_tcdsect (manid integer) return varchar;

-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);
procedure ctrl_bordereaux_PI(borIdDep integer, borIdRec integer);
END;
/
CREATE OR REPLACE PACKAGE MARACUJA.extourne
is
   -- constantes
   c_mod_dom_a_extourner   constant maracuja.mode_paiement.mod_dom%type   := 'A EXTOURNER';

   -- bordidn : bordereau sur l'exercice n
   -- bordidn1 : bordereau cree sur l'exercice n+1 par cette procedure
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type);

   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type);

   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer;

   function is_dpco_extourne_def (dpcoid integer)
      return integer;

   function is_dpco_extourne (dpcoid integer)
      return integer;

   function is_exercice_existe (exeordre integer)
      return integer;

   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type);

   function get_tboordre_mandat_ext
      return type_bordereau.tbo_ordre%type;

   function get_modordre_extourne (ladepensen maracuja.depense%rowtype)
      return mode_paiement.mod_ordre%type;

   function get_tcdordre_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.type_credit.tcd_ordre%type;

   function get_orgid_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.organ.org_id%type;

   function calcul_montant_budgetaire (dpcoid jefy_depense.depense_ctrl_planco.dpco_id%type)
      return jefy_depense.depense_ctrl_planco.dpco_montant_budgetaire%type;
end extourne;
/


CREATE OR REPLACE PACKAGE MARACUJA.util
is
   procedure annuler_visa_bor_mandat (borid integer);

   procedure annuler_visa_bor_titre (borid integer);

   procedure supprimer_visa_btme (borid integer);

   procedure supprimer_visa_btms (borid integer);

   procedure supprimer_visa_btte (borid integer);

   procedure creer_ecriture_annulation (ecrordre integer);

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer;

   procedure annuler_emargement (emaordre integer);

   procedure supprimer_bordereau_dep (borid integer);

   procedure supprimer_bordereau_rec (borid integer);

   procedure supprimer_bordereau_btms (borid integer);

   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer);

   procedure supprimer_paiement_virement (paiordre integer);

   procedure corriger_etat_mandats;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);
end;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT"
as
   procedure creer_bordereau (abrid integer)
   is
      cpt            integer;
      abrgroupby     abricot_bord_selection.abr_group_by%type;
      monborid_dep   integer;
      monborid_rec   integer;
      flag           integer;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = monborid_dep;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = monborid_rec;

      tmpmandid      integer;
      tmptitid       integer;
      tboordre       integer;
   begin
-- est ce une selection vide ???
      select count (*)
      into   cpt
      from   abricot_bord_selection
      where  abr_id = abrid;

      if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

         -- verifier l etat de l exercice
         select count (*)
         into   flag
         from   jefy_admin.exercice
         where  exe_ordre = recup_exeordre (abrid) and exe_stat in ('O', 'R');

         if (flag = 0) then
            raise_application_error (-20001, 'L''exercice ' || recup_exeordre (abrid) || ' n''est pas ouvert.');
         end if;

         -- recup du group by pour traiter les cursors
         abrgroupby := recup_groupby (abrid);

         if (abrgroupby = 'bordereau_1R1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1r1t (abrid, monborid_rec);
         end if;

         if (abrgroupby = 'bordereau_NR1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nr1t (abrid, monborid_rec);

-- controle RA
            select count (*)
            into   cpt
            from   titre
            where  ori_ordre is not null and bor_id = monborid_rec;

            if cpt != 0 then
               raise_application_error (-20001, 'Impossible de traiter une recette sur convention affectee dans un bordereau collectif !');
            end if;
         end if;

         if (abrgroupby = 'bordereau_1D1M') then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1d1m (abrid, monborid_dep);
         end if;


         if (abrgroupby not in ('bordereau_1R1T', 'bordereau_NR1T', 'bordereau_1D1M', 'bordereau_1D1M1R1T')) then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nd1m (abrid, monborid_dep);
         end if;

         if (monborid_dep is not null) then
         
         -- controle extourne
            select count(*) into cpt from mandat m, mode_paiement mp where m.mod_ordre=mp.mod_ordre and bor_id=monborid_dep and mod_dom='EXTOURNE';
            if (cpt>0) then
                select count(*) into cpt from (select distinct mod_dom from mandat m, mode_paiement mp where m.mod_ordre=mp.mod_ordre and bor_id=monborid_dep);
                if (cpt >1) then
                    raise_application_error (-20001, 'Les dépenses à extourner ne doivent pas être passées sur le même bordereau que les autres dépenses.');
                end if;
            end if; 
         
            bordereau_abricot.numeroter_bordereau (monborid_dep);

            open lesmandats;

            loop
               fetch lesmandats
               into  tmpmandid;

               exit when lesmandats%notfound;
               get_depense_jefy_depense (tmpmandid);
            end loop;

            close lesmandats;

            controle_bordereau (monborid_dep);
         end if;

         if (monborid_rec is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_rec);

            open lestitres;

            loop
               fetch lestitres
               into  tmptitid;

               exit when lestitres%notfound;
               -- recup du brouillard
               get_recette_jefy_recette (tmptitid);
               set_titre_brouillard (tmptitid);
               get_recette_prelevements (tmptitid);
            end loop;

            close lestitres;

            controle_bordereau (monborid_rec);
         end if;

-- maj de l etat dans la selection
         if (monborid_dep is not null or monborid_rec is not null) then
            if monborid_rec is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_rec
                where abr_id = abrid;
            end if;

            if monborid_dep is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_dep
                where abr_id = abrid;

               select tbo_ordre
               into   tboordre
               from   bordereau
               where  bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
               if tboordre = 3 then
                  bordereau_abricot_paye.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
               if tboordre = 18 then
                  bordereau_abricot_paye.basculer_bouillard_paye_orv (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 19 then
                  bordereau_abricot_paye.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 22 then
                  bordereau_abricot_paf.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de PAF on retravaille le brouillard
               if tboordre = 20 then
                  bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de PAF on retravaille le brouillard
               if tboordre = 21 then
                  bordereau_abricot_paf.basculer_bouillard_paye_orv (monborid_dep);
               end if;
           -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
            end if;
         end if;
      end if;
   end;

   procedure viser_bordereau_rejet (brjordre integer)
   is
      cpt              integer;
      flag             integer;
      manid            maracuja.mandat.man_id%type;
      titid            maracuja.titre.tit_id%type;
      tboordre         integer;
      reduction        integer;
      utlordre         integer;
      dpcoid           integer;
      recid            integer;
      depsuppression   varchar2 (20);
      rpcoid           integer;
      recsuppression   varchar2 (20);
      exeordre         integer;
      depid            integer;
      boridinitial     integer;

      cursor mandats
      is
         select man_id
         from   maracuja.mandat
         where  brj_ordre = brjordre;

      cursor depenses
      is
         select dep_ordre,
                dep_suppression
         from   maracuja.depense
         where  man_id = manid;

      cursor titres
      is
         select tit_id
         from   maracuja.titre
         where  brj_ordre = brjordre;

      cursor recettes
      is
         select rec_ordre,
                rec_suppression
         from   maracuja.recette
         where  tit_id = titid;

      deliq            integer;
   begin
      -- verifier si le bordereau est deja vise
      select count (*)
      into   flag
      from   bordereau_rejet
      where  brj_etat = 'VISE' and brj_ordre = brjordre;

      if (flag > 0) then
         raise_application_error (-20001, 'Ce bordereau a déjà été visé');
      end if;

      open mandats;

      loop
         fetch mandats
         into  manid;

         exit when mandats%notfound;

         select bor_id
         into   boridinitial
         from   mandat
         where  man_id = manid;

         -- memoriser le bor-id du mandat
         open depenses;

         loop
            fetch depenses
            into  dpcoid,
                  depsuppression;

            exit when depenses%notfound;
            -- casser le liens des mand_id dans depense_ctrl_planco
              -- supprimer le liens compteble <-> depense dans l inventaire
            jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, null);

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
            if depsuppression = 'OUI' and tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
 deliq:=jefy_depense.Get_Fonction('DELIQ');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
 uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

   if utlordre is null then
   deliq:=jefy_depense.Get_Fonction('DELIQINV');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre
 AND ufe.exe_ordre=exeordre
 AND uf.fon_ordre=deliq
 AND ufe.exe_ordre=e.exe_ordre
 AND exe_stat_eng='R';
 end if;
*/
               select utl_ordre
               into   utlordre
               from   jefy_depense.depense_budget
               where  dep_id in (select dep_id
                                 from   jefy_depense.depense_ctrl_planco
                                 where  dpco_id = dpcoid);

               select dep_id
               into   depid
               from   jefy_depense.depense_ctrl_planco
               where  dpco_id = dpcoid;

               -- si cest le rejet d'un bordereau de paye
               if (tboordre = 18) then
                  jefy_paye.paye_reversement.viser_rejet_reversement (depid);
               end if;

               jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
            end if;
         end loop;

         close depenses;
      end loop;

      close mandats;

      -- pour les bordereau de PAF, appeler la proc
      jefy_paf.paf_budget.viser_rejet_paf (boridinitial);
      jefy_paye.paye_budget.viser_rejet_papaye (boridinitial);

      open titres;

      loop
         fetch titres
         into  titid;

         exit when titres%notfound;

         open recettes;

         loop
            fetch recettes
            into  rpcoid,
                  recsuppression;

            exit when recettes%notfound;

-- casser le liens des tit_id dans recette_ctrl_planco
            select r.rec_id_reduction
            into   reduction
            from   jefy_recette.recette r, jefy_recette.recette_ctrl_planco rpco
            where  rpco.rpco_id = rpcoid and rpco.rec_id = r.rec_id;

            if reduction is not null then
               jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, null);
            else
               jefy_recette.api.upd_recette_ctrl_planco (rpcoid, null);
            end if;

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
            if recsuppression = 'OUI' and tboordre != 200 then
               select utl_ordre
               into   utlordre
               from   jefy_recette.recette_budget
               where  rec_id in (select rec_id
                                 from   jefy_recette.recette_ctrl_planco
                                 where  rpco_id = rpcoid);

               select rec_id
               into   recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = rpcoid;

               if reduction is not null then
                  jefy_recette.api.del_reduction (recid, utlordre);
               else
                  jefy_recette.api.del_recette (recid, utlordre);
               end if;
            end if;
         end loop;

         close recettes;
      end loop;

      close titres;

-- on passe le brjordre a VISE
      update bordereau_rejet
         set brj_etat = 'VISE'
       where brj_ordre = brjordre;
   end;

   function get_selection_id (info varchar)
      return integer
   is
      selection   integer;
   begin
      select maracuja.abricot_bord_selection_seq.nextval
      into   selection
      from   dual;

      return selection;
   end;

   function get_selection_borid (abrid integer)
      return integer
   is
      borid   integer;
   begin
      select distinct bor_id
      into            borid
      from            maracuja.abricot_bord_selection
      where           abr_id = abrid;

      return borid;
   end;

   procedure set_selection_id (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a06tboordre integer, a07abrgroupby varchar, a08gescode varchar)
   is
      chaine     varchar (32000);
      premier    integer;
      tmpdepid   integer;
      tmprecid   integer;
      cpt        integer;
   begin
/*
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

      -- traitement de la chaine des depid
      if a02lesdepid is not null or length (a02lesdepid) > 0 then
         chaine := a02lesdepid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdepid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         tmpdepid,   --DEP_ID
                         null,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      -- traitement de la chaine des recid
      if a03lesrecid is not null or length (a03lesrecid) > 0 then
         chaine := a03lesrecid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmprecid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         null,   --DEP_ID
                         tmprecid,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      select count (*)
      into   cpt
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id in (select dep_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and man_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
      end if;

      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id in (select rec_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and tit_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
      end if;

      bordereau_abricot.creer_bordereau (a01abrid);
   end;

   procedure set_selection_intern (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boriddep   bordereau.bor_id%type;
      boridrec   bordereau.bor_id%type;
      flag       integer;
   begin
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

      -- les mandats
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 201, 'bordereau_1D1M', a08gescodemandat);
-- les titres
      set_selection_id (-a01abrid, null, a03lesrecid, a04utlordre, a05exeordre, 200, 'bordereau_1R1T', a09gescodetitre);

      -- verifier que les bordereaux crees sont coherents entre eux
      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || a01abrid);
      end if;

      select max (bor_id)
      into   boriddep
      from   abricot_bord_selection
      where  abr_id = a01abrid;

      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = -a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || -a01abrid);
      end if;

      select max (bor_id)
      into   boridrec
      from   abricot_bord_selection
      where  abr_id = -a01abrid;

      -- verifier qu'on a 1 prest_id par titre/mandat
      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct tit_id) nb
              from     titre
              where    bor_id = boridrec
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs titres concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct man_id) nb
              from     mandat
              where    bor_id = boriddep
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs mandats concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      ctrl_bordereaux_pi (boriddep, boridrec);
   end;

   procedure set_selection_paye (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boridtmp    integer;
      moisordre   integer;
   begin
/*
-- a07abrgroupby = mois
select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

-- CONTROLES
-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

if (cpt = 1) then
 raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
end if;
*/
-- ATENTION
-- tboordre : 3 salaires

      -- les mandats de papaye
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 3, 'bordereau_1D1M', a08gescodemandat);
      boridtmp := get_selection_borid (a01abrid);
/*
-- maj de l etat de papaye_compta et du bor_ordre -
update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

   -- bascule du brouillard de papaye

   -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
   end;

-- creer bordereau (tbo_ordre) + numerotation
   function get_num_borid (tboordre integer, exeordre integer, gescode varchar, utlordre integer)
      return integer
   is
      cpt      integer;
      borid    integer;
      bornum   integer;
   begin
-- creation du bor_id --
      select bordereau_seq.nextval
      into   borid
      from   dual;

-- creation du bordereau --
      bornum := -1;

      insert into bordereau
                  (bor_date_visa,
                   bor_etat,
                   bor_id,
                   bor_num,
                   bor_ordre,
                   exe_ordre,
                   ges_code,
                   tbo_ordre,
                   utl_ordre,
                   utl_ordre_visa,
                   bor_date_creation
                  )
      values      (null,   --BOR_DATE_VISA,
                   'VALIDE',   --BOR_ETAT,
                   borid,   --BOR_ID,
                   bornum,   --BOR_NUM,
                   -borid,   --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   tboordre,   --TBO_ORDRE,
                   utlordre,   --UTL_ORDRE,
                   null,   --UTL_ORDRE_VISA
                   sysdate
                  );

      return borid;
   end;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
   procedure bordereau_1r1t (abrid integer, monborid integer)
   is
      cpt          integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by r.pco_num, r.rec_id asc;
   begin
      open rec_tit;

      loop
         fetch rec_tit
         into  tmprecette;

         exit when rec_tit%notfound;
         cpt := set_titre_recette (tmprecette.rpco_id, monborid);
      end loop;

      close rec_tit;
   end;

   procedure bordereau_nr1t (abrid integer, monborid integer)
   is
      ht           number (12, 2);
      tva          number (12, 2);
      ttc          number (12, 2);
      pconumero    varchar (20);
      nbpieces     integer;
      cpt          integer;
      titidtemp    integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

-- curseur de regroupement
      cursor rec_tit_group_by
      is
         select   r.pco_num,
                  sum (r.rpco_ht_saisie),
                  sum (r.rpco_tva_saisie),
                  sum (r.rpco_ttc_saisie)
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         group by r.pco_num
         order by r.pco_num asc;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and r.pco_num = pconumero
         order by r.pco_num asc, r.rec_id;
   begin
      open rec_tit_group_by;

      loop
         fetch rec_tit_group_by
         into  pconumero,
               ht,
               tva,
               ttc;

         exit when rec_tit_group_by%notfound;
         titidtemp := 0;

         open rec_tit;

         loop
            fetch rec_tit
            into  tmprecette;

            exit when rec_tit%notfound;

            if titidtemp = 0 then
               titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
            else
               update jefy_recette.recette_ctrl_planco
                  set tit_id = titidtemp
                where rpco_id = tmprecette.rpco_id;
            end if;
         end loop;

         close rec_tit;

-- recup du nombre de pieces
-- TODO
         nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

         -- maj des montants du titre
         update titre
            set tit_ht = ht,
                tit_nb_piece = nbpieces,
                tit_ttc = ttc,
                tit_tva = tva,
                tit_libelle = 'TITRE COLLECTIF'
          where tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
      end loop;

      close rec_tit_group_by;
   end;

   procedure bordereau_nd1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;
      abrgroupby   abricot_bord_selection.abr_group_by%type;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
      cursor mand_dep_convra
      is
         select distinct d.*
         from            abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.engage_budget e, maracuja.v_convention_limitative c
         where           d.dpco_id = ab.dep_id and abr_id = abrid and db.dep_id = d.dep_id and e.eng_id = db.eng_id and e.org_id = c.org_id(+) and e.exe_ordre = c.exe_ordre(+) and c.org_id is not null and d.man_id is null and ab.abr_etat = 'ATTENTE'
         order by        d.pco_num asc, d.dep_id;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
   begin
      open mand_dep_convra;

      loop
         fetch mand_dep_convra
         into  tmpdepense;

         exit when mand_dep_convra%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod') then
         cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
      end if;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_fou_rib_pco_mod') then
         cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
      end if;
   end;

   procedure bordereau_1d1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dep_mand
      is
         select   d.*
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d
         where    d.dpco_id = ab.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by d.pco_num asc, d.dep_id;
   begin
      open dep_mand;

      loop
         fetch dep_mand
         into  tmpdepense;

         exit when dep_mand%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close dep_mand;
   end;

   procedure bordereau_1d1m1r1t (abrid integer, boridep integer, boridrec integer)
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      bordereau_1d1m (abrid, boridep);
      bordereau_1r1t (abrid, boridrec);
      ctrl_bordereaux_pi (boridep, boridrec);
   end;

-- les mandats et titres
   function set_mandat_depense (dpcoid integer, borid integer)
      return integer
   is
      cpt               integer;
      flag              integer;
      ladepense         jefy_depense.depense_ctrl_planco%rowtype;
      ladepensepapier   jefy_depense.depense_papier%rowtype;
      leengagebudget    jefy_depense.engage_budget%rowtype;
      gescode           gestion.ges_code%type;
      manid             mandat.man_id%type;
      manorgine_key     mandat.man_orgine_key%type;
      manorigine_lib    mandat.man_origine_lib%type;
      oriordre          mandat.ori_ordre%type;
      prestid           mandat.prest_id%type;
      torordre          mandat.tor_ordre%type;
      virordre          mandat.pai_ordre%type;
      mannumero         mandat.man_numero%type;
      -- montantapayer     mandat.man_ttc%type;
      montantbud        mandat.man_ht%type;
      montanttva        mandat.man_tva%type;
      ttc               mandat.man_ttc%type;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

      select *
      into   ladepense
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select distinct dpp.*
      into            ladepensepapier
      from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = dpcoid;

      select eb.*
      into   leengagebudget
      from   jefy_depense.engage_budget eb, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.eng_id = eb.eng_id and db.dep_id = dpco.dep_id and dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = leengagebudget.org_id and exe_ordre = leengagebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a l''engagement num. ' || leengagebudget.eng_numero || ' n''est pas ouverte sur ' || leengagebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      manorgine_key := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      manorigine_lib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (leengagebudget.org_id, leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

-- creation du man_id --
      select mandat_seq.nextval
      into   manid
      from   dual;

-- recup du numero de mandat
      mannumero := -1;
      montantbud := abricot_util.get_dpco_montant_budgetaire (ladepense.dpco_id);
      --montantapayer := abricot_util.get_dpco_montant_apayer (ladepense.dpco_id);
      ttc := abricot_util.get_dpco_montant_ttc (ladepense.dpco_id);
      montanttva := ttc - montantbud;

      insert into mandat
                  (bor_id,
                   brj_ordre,
                   exe_ordre,
                   fou_ordre,
                   ges_code,
                   man_date_remise,
                   man_date_visa_princ,
                   man_etat,
                   man_etat_remise,
                   man_ht,
                   man_id,
                   man_motif_rejet,
                   man_nb_piece,
                   man_numero,
                   man_numero_rejet,
                   man_ordre,
                   man_orgine_key,
                   man_origine_lib,
                   man_ttc,
                   man_tva,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tor_ordre,
                   pai_ordre,
                   org_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable
                  )
      values      (borid,   --BOR_ID,
                   null,   --BRJ_ORDRE,
                   ladepensepapier.exe_ordre,   --EXE_ORDRE,
                   ladepensepapier.fou_ordre,   --FOU_ORDRE,
                   gescode,   --GES_CODE,
                   null,   --MAN_DATE_REMISE,
                   null,
                   
                   --MAN_DATE_VISA_PRINC,
                   'ATTENTE',   --MAN_ETAT,
                   'ATTENTE',   --MAN_ETAT_REMISE,
                   montantbud,   --MAN_HT,
                   manid,   --MAN_ID,
                   null,   --MAN_MOTIF_REJET,
                   ladepensepapier.dpp_nb_piece,   --MAN_NB_PIECE,
                   mannumero,
                   --MAN_NUMERO,
                   null,   --MAN_NUMERO_REJET,
                   -manid,   --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                   manorgine_key,   --MAN_ORGINE_KEY,
                   manorigine_lib,   --MAN_ORIGINE_LIB,
                   --ladepense.dpco_ttc_saisie,   --MAN_TTC,
                   ttc,   --man_ttc
                   --ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,   --MAN_TVA,
                   montanttva,   --man_tva
                   ladepensepapier.mod_ordre,   --MOD_ORDRE,
                   oriordre,   --ORI_ORDRE,
                   ladepense.pco_num,
                   --PCO_NUM,
                   prestid,   --PREST_ID,
                   torordre,   --TOR_ORDRE,
                   virordre,   --VIR_ORDRE
                   leengagebudget.org_id,
                   --org_ordre
                   ladepensepapier.rib_ordre,   --rib ordo
                   ladepensepapier.rib_ordre   -- rib_comptable
                  );

-- maj du man_id  dans la depense
      update jefy_depense.depense_ctrl_planco
         set man_id = manid
       where dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

      -- recup du brouillard
      set_mandat_brouillard (manid);
      return manid;
   end;

-- lesdepid XX$FF$....$DDD$ZZZ$$
   function set_mandat_depenses (lesdpcoid varchar, borid integer)
      return integer
   is
      cpt             integer;
      premier         integer;
      tmpdpcoid       integer;
      chaine          varchar (5000);
      premierdpcoid   integer;
      manid           integer;
      ttc             mandat.man_ttc%type;
      tva             mandat.man_tva%type;
      ht              mandat.man_ht%type;
      utlordre        integer;
      nb_pieces       integer;
   begin
      select count (*)
      into   cpt
      from   dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
      premierdpcoid := null;

      -- traitement de la chaine des depid xx$xx$xx$.....$x$$
      if lesdpcoid is not null or length (lesdpcoid) > 0 then
         chaine := lesdpcoid;

         loop
            premier := 1;

            -- On recupere le depid
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdpcoid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

-- creation du mandat lie au borid
            if premierdpcoid is null then
               manid := set_mandat_depense (tmpdpcoid, borid);

               -- suppression du brouillard car il est uniquement sur la premiere depense
               delete from mandat_brouillard
                     where man_id = manid;

               premierdpcoid := tmpdpcoid;
            else
               -- maj du man_id  dans la depense
               update jefy_depense.depense_ctrl_planco
                  set man_id = manid
                where dpco_id = tmpdpcoid;

               -- recup de la depense (maracuja)
               select distinct dpp.utl_ordre
               into            utlordre
               from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
               where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
            end if;

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
--      select sum (dpco_ttc_saisie),
--             sum (dpco_ttc_saisie - dpco_montant_budgetaire),
--             sum (dpco_montant_budgetaire)
--      into   ttc,
--             tva,
--             ht
--      from   jefy_depense.depense_ctrl_planco
--      where  man_id = manid;
      ttc := abricot_util.get_man_montant_ttc (manid);
      ht := abricot_util.get_man_montant_budgetaire (manid);
      tva := ttc - ht;

-- recup du nb de pieces
      select sum (dpp.dpp_nb_piece)
      into   nb_pieces
      from   jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and man_id = manid;

-- maj du mandat
      update mandat
         set man_ht = ht,
             man_tva = tva,
             man_ttc = ttc,
             man_nb_piece = nb_pieces
       where man_id = manid;

-- recup du brouillard
      set_mandat_brouillard (manid);
      return manid;
   end;

   function set_titre_recette (rpcoid integer, borid integer)
      return integer
   is
--     jefytitre           jefy.titre%ROWTYPE;
      gescode             gestion.ges_code%type;
      titid               titre.tit_id%type;
      titorginekey        titre.tit_orgine_key%type;
      titoriginelib       titre.tit_origine_lib%type;
      oriordre            titre.ori_ordre%type;
      prestid             titre.prest_id%type;
      torordre            titre.tor_ordre%type;
      modordre            titre.mod_ordre%type;
      presid              integer;
      cpt                 integer;
      virordre            integer;
      flag                integer;
      recettepapier       jefy_recette.recette_papier%rowtype;
      recettebudget       jefy_recette.recette_budget%rowtype;
      facturebudget       jefy_recette.facture_budget%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
      select *
      into   recettectrlplanco
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id = rpcoid;

      select *
      into   recettebudget
      from   jefy_recette.recette_budget
      where  rec_id = recettectrlplanco.rec_id;

      select *
      into   facturebudget
      from   jefy_recette.facture_budget
      where  fac_id = recettebudget.fac_id;

      select *
      into   recettepapier
      from   jefy_recette.recette_papier
      where  rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = facturebudget.org_id and exe_ordre = facturebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a la recette num. ' || recettebudget.rec_numero || ' n''est pas ouverte sur ' || facturebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      titorginekey := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      titoriginelib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (facturebudget.org_id, facturebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

      select titre_seq.nextval
      into   titid
      from   dual;

      insert into titre
                  (bor_id,
                   bor_ordre,
                   brj_ordre,
                   exe_ordre,
                   ges_code,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tit_date_remise,
                   tit_date_visa_princ,
                   tit_etat,
                   tit_etat_remise,
                   tit_ht,
                   tit_id,
                   tit_motif_rejet,
                   tit_nb_piece,
                   tit_numero,
                   tit_numero_rejet,
                   tit_ordre,
                   tit_orgine_key,
                   tit_origine_lib,
                   tit_ttc,
                   tit_tva,
                   tor_ordre,
                   utl_ordre,
                   org_ordre,
                   fou_ordre,
                   mor_ordre,
                   pai_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable,
                   tit_libelle
                  )
      values      (borid,   --BOR_ID,
                   -borid,   --BOR_ORDRE,
                   null,   --BRJ_ORDRE,
                   recettepapier.exe_ordre,   --EXE_ORDRE,
                   gescode,
                   --GES_CODE,
                   null,   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                   oriordre,   --ORI_ORDRE,
                   recettectrlplanco.pco_num,   --PCO_NUM,
                   prestid,
                   --PREST_ID,
                   sysdate,   --TIT_DATE_REMISE,
                   null,   --TIT_DATE_VISA_PRINC,
                   'ATTENTE',   --TIT_ETAT,
                   'ATTENTE',   --TIT_ETAT_REMISE,
                   recettectrlplanco.rpco_ht_saisie,   --TIT_HT,
                   titid,   --TIT_ID,
                   null,
                   --TIT_MOTIF_REJET,
                   recettepapier.rpp_nb_piece,   --TIT_NB_PIECE,
                   -1,
                   --TIT_NUMERO, numerotation en fin de transaction
                   null,   --TIT_NUMERO_REJET,
                   -titid,
                   --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                   titorginekey,   --TIT_ORGINE_KEY,
                   titoriginelib,   --TIT_ORIGINE_LIB,
                   recettectrlplanco.rpco_ttc_saisie,   --TIT_TTC,
                   recettectrlplanco.rpco_tva_saisie,   --TIT_TVA,
                   torordre,   --TOR_ORDRE,
                   recettepapier.utl_ordre,   --UTL_ORDRE
                   facturebudget.org_id,   --ORG_ORDRE,
                   recettepapier.fou_ordre,
                   -- FOU_ORDRE  --TOCHECK certains sont nuls...
                   facturebudget.mor_ordre,
                   --MOR_ORDRE
                   null,
                   -- VIR_ORDRE
                   recettepapier.rib_ordre,
                   recettepapier.rib_ordre,
                   recettebudget.rec_lib
                  );

-- maj du tit_id dans la recette
      update jefy_recette.recette_ctrl_planco
         set tit_id = titid
       where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
      return titid;
   end;

   function set_titre_recettes (lesrpcoid varchar, borid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      raise_application_error (-20001, 'OPERATION NON TRAITEE');
      return cpt;
   end;

   function ndep_mand_org_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_org_fou_rib_pco_mod
      is
         select   e.org_id,
                  dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e, jefy_admin.organ vo, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and e.eng_id = db.eng_id and vo.org_id = e.org_id and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, e.org_id, dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      cursor lesdpcoidsribnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_org_fou_rib_pco_mod;

      loop
         fetch ndep_mand_org_fou_rib_pco_mod
         into  orgid,
               fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_org_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsribnull;

            loop
               fetch lesdpcoidsribnull
               into  tmpdpcoid;

               exit when lesdpcoidsribnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsribnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_org_fou_rib_pco_mod;

      return cpt;
   end;

   function ndep_mand_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_fou_rib_pco_mod
      is
         select   dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      cursor lesdpcoidsnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_fou_rib_pco_mod;

      loop
         fetch ndep_mand_fou_rib_pco_mod
         into  fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsnull;

            loop
               fetch lesdpcoidsnull
               into  tmpdpcoid;

               exit when lesdpcoidsnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_fou_rib_pco_mod;

      return cpt;
   end;

-- procedures de verifications
   function selection_valide (abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- meme exercice

      -- si PI somme recette = somme depense

      -- recette_valides

      -- depense_valides
      return cpt;
   end;

   function recette_valide (recid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function depense_valide (depid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function verif_bordereau_selection (borid integer, abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

      -- verifier sum TTC recette selection = sum TTC titre du bord

      -- verifier sum TTC depense  = sum TTC mandat du bord

      -- verifier sum TTC recette  = sum TTC titre  du bord
      return cpt;
   end;

-- procedures de locks de transaction
   procedure lock_mandats
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure lock_titres
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure get_depense_jefy_depense (manid integer)
   is
      depid               depense.dep_id%type;
      jefydepensebudget   jefy_depense.depense_budget%rowtype;

      cursor depenses
      is
         select db.*
         from   jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  dpco.man_id = manid and db.dep_id = dpco.dep_id;
   begin
      open depenses;

      loop
         fetch depenses
         into  jefydepensebudget;

         exit when depenses%notfound;
            depid := creer_depense(jefydepensebudget, manid);
      end loop;

      close depenses;
   end;

   procedure get_recette_jefy_recette (titid integer)
   is
      recettepapier          jefy_recette.recette_papier%rowtype;
      recettebudget          jefy_recette.recette_budget%rowtype;
      facturebudget          jefy_recette.facture_budget%rowtype;
      recettectrlplanco      jefy_recette.recette_ctrl_planco%rowtype;
      recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%rowtype;
      maracujatitre          maracuja.titre%rowtype;
      adrnom                 varchar2 (200);
      letyperecette          varchar2 (200);
      titinterne             varchar2 (200);
      lbud                   varchar2 (200);
      tboordre               integer;
      cpt                    integer;

      cursor c_recette
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
      open c_recette;

      loop
         fetch c_recette
         into  recettectrlplanco;

         exit when c_recette%notfound;

         select *
         into   recettebudget
         from   jefy_recette.recette_budget
         where  rec_id = recettectrlplanco.rec_id;

         select *
         into   facturebudget
         from   jefy_recette.facture_budget
         where  fac_id = recettebudget.fac_id;

         select *
         into   recettepapier
         from   jefy_recette.recette_papier
         where  rpp_id = recettebudget.rpp_id;

         select *
         into   maracujatitre
         from   maracuja.titre
         where  tit_id = titid;

         if (recettebudget.rec_id_reduction is null) then
            letyperecette := 'R';
         else
            letyperecette := 'T';
         end if;

         select count (*)
         into   cpt
         from   jefy_recette.pi_dep_rec
         where  rec_id = recettectrlplanco.rec_id;

         if cpt > 0 then
            titinterne := 'O';
         else
            titinterne := 'N';
         end if;

         adrnom := getfournisnom (recettepapier.fou_ordre);

         select org_ub || '/' || org_cr || '/' || org_souscr
         into   lbud
         from   jefy_admin.organ
         where  org_id = facturebudget.org_id;

         select distinct tbo_ordre
         into            tboordre
         from            maracuja.titre t, maracuja.bordereau b
         where           b.bor_id = t.bor_id and t.tit_id = titid;

-- 200 bordereau de presntation interne recette
         if tboordre = 200 then
            tboordre := null;
         else
            tboordre := facturebudget.org_id;
         end if;

         insert into recette
         values      (recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      maracujatitre.ges_code,
                      --GES_CODE,
                      null,   --MOD_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM,
                      recettebudget.rec_date_saisie,
                      --jefytitre.tit_date,-- REC_DATE,
                      adrnom,   -- REC_DEBITEUR,
                      recette_seq.nextval,   -- REC_ID,
                      null,   -- REC_IMPUTTVA,
                      null,
                      -- REC_INTERNE, // TODO ROD
                      facturebudget.fac_lib,
                      -- REC_LIBELLE,
                      lbud,   -- REC_LIGNE_BUDGETAIRE,
                      'E',   -- REC_MONNAIE,
                      recettectrlplanco.rpco_ht_saisie,   --HT,
                      recettectrlplanco.rpco_ttc_saisie,   --TTC,
                      recettectrlplanco.rpco_ttc_saisie,
                      --DISQUETTE,
                      recettectrlplanco.rpco_tva_saisie,   --   REC_MONTTVA,
                      facturebudget.fac_numero,   --   REC_NUM,
                      recettectrlplanco.rpco_id,
                      --   REC_ORDRE,
                      recettepapier.rpp_nb_piece,   --   REC_PIECE,
                      facturebudget.fac_numero,
                      
                      --   REC_REF,
                      'VALIDE',   --   REC_STAT,
                      'NON',   --    REC_SUPPRESSION,  Modif Rod
                      letyperecette,   --     REC_TYPE,
                      null,   --     REC_VIREMENT,
                      titid,   --      TIT_ID,
                      -titid,
                      --      TIT_ORDRE,
                      recettebudget.utl_ordre,   --       UTL_ORDRE
                      facturebudget.org_id,
                      --       ORG_ORDRE --ajout rod
                      facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
                      null,   --mod_ordre
                      recettepapier.mor_ordre,
                      --mor_ordre
                      recettepapier.rib_ordre,
                      null
                     );
      end loop;

      close c_recette;
   end;

-- procedures du brouillard
   procedure set_mandat_brouillard (manid integer)
   is
      lemandat              mandat%rowtype;
      tboordre              type_bordereau.tbo_ordre%type;
      sens                  mandat_brouillard.mab_sens%type;
      tcdsect               jefy_admin.type_credit.tcd_sect%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
      modcode               mode_paiement.mod_code%type;
      modlibelle            mode_paiement.mod_libelle%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      tboordre := abricot_util.get_man_tboordre (manid);

      if lemandat.prest_id is null then
         -- creation du brouillard imputation budgetaire --
         sens := inverser_sens_orv (tboordre, 'D');
         montantbudgetaire := lemandat.man_ht;
         pconumbudgetaire := lemandat.pco_num;
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
         -- creation du brouillard de contrepartie
         sens := inverser_sens_orv (tboordre, 'C');

         -- si on est sur un ORV, on recupere le compte de ctp à utiliser
         if sens = 'D' then
            tcdsect := recup_tcdsect (manid);
            pconumctp := util.getpconumvalidefromparam (lemandat.exe_ordre, 'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section' || tcdsect || '.compte');

            if (pconumctp is null) then
               pconumctp := '4632';
            end if;
         else
            pconumctp := abricot_util.get_man_compte_ctp (manid);
            if (pconumctp is null) then
                raise_application_error (-20001, 'Le compte de contrepartie n''est pas parametré pour le compte (' || lemandat.pco_num || ')');
            end if;
         end if;

         gescodectp := abricot_util.get_man_gestion_ctp (manid);
         montantctp := abricot_util.get_man_montant_apayer (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA CTP', sens, manid, pconumctp);
         --si presence de TVA, on gere la TVA deduite et l eventuelle TVA collectee
         montanttvadeduite := abricot_util.get_man_montant_tva_ded (manid, null);
         montanttvacollectee := abricot_util.get_man_montant_tva_coll (manid, null);

         if (montanttvadeduite <> 0) then
            sens := inverser_sens_orv (tboordre, 'D');
            pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);

            if (pconumtvadeduite is null) then
               raise_application_error (-20001, 'Le compte par defaut de TVA a deduire n''est pas parametré pour le compte (' || lemandat.pco_num || ')');
            end if;

            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
         end if;

         if (montanttvacollectee <> 0) then
            sens := inverser_sens_orv (tboordre, 'C');
            pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);

            if (pconumtvacollectee is null) then
               select mod_code,
                      mod_libelle
               into   modcode,
                      modlibelle
               from   mode_paiement
               where  mod_ordre = lemandat.mod_ordre;

               raise_application_error (-20001, 'Le compte de TVA à collecter n''est pas parametré pour le mode de paiement (' || modcode || '-' || modlibelle || ')');
            end if;

            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
         end if;
      else
         bordereau_abricot.set_mandat_brouillard_intern (manid);
      end if;
   end;

   procedure set_mandat_brouillard_intern (manid integer)
   is
      lemandat              mandat%rowtype;
      chap                  varchar2 (2);
      sens                  mandat_brouillard.mab_sens%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      -- recup des 2 premiers caracteres du compte
      select substr (lemandat.pco_num, 1, 2)
      into   chap
      from   dual;

      if chap = '18' then
         raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || lemandat.pco_num || ')');
      end if;

      --lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      sens := 'D';
      montantbudgetaire := lemandat.man_ht;
      pconumbudgetaire := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
      sens := 'C';
      gescodectp := abricot_util.get_man_gestion_ctp (manid);
      montantctp := abricot_util.get_man_montant_apayer (manid);
      --pconumctp := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA MANDAT', sens, manid, '181');

      if (montanttvadeduite <> 0) then
         sens := 'D';
         pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
      end if;

      if (montanttvacollectee <> 0) then
         sens := 'C';
         pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
      end if;
   end;

   procedure set_titre_brouillard (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      recid               integer;
      flag                integer;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
      select max (rb.rec_id_reduction)
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

-- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      if letitre.prest_id is null then
         open c_recettes;

         loop
            fetch c_recettes
            into  recettectrlplanco;

            exit when c_recettes%notfound;

            select max (rec_id)
            into   recid
            from   recette
            where  rec_ordre = recettectrlplanco.rpco_id;

            -- verifier que la contrepartie n'est pas passée sur l'imputation du titre
            select count (*)
            into   flag
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id and pco_num = letitre.pco_num;

            if (flag > 0) then
               raise_application_error (-20001, 'La contrepartie ne doit pas etre identique à l''imputation ( : ' || letitre.tit_libelle || ' / ' || letitre.pco_num || ' / ' || letitre.tit_ttc || ')');
            end if;

            -- creation du titre_brouillard visa --
            --  RECETTE_CTRL_PLANCO
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      letitre.ges_code,
                      --GES_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM
                      abs (recettectrlplanco.rpco_ht_saisie),   --TIB_MONTANT,
                      'VISA TITRE',
                      --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_tva
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,   --PCO_NUM
                      abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                      'VISA TVA',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_tva
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_ctp
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,
                      --PCO_NUM
                      abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                      'VISA CTP',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      inverser_sens (lesens),
                      --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_ctp
               where  rpco_id = recettectrlplanco.rpco_id;
         end loop;

         close c_recettes;
      else
         set_titre_brouillard_intern (titid);
      end if;

      -- suppression des lignes d ecritures a ZERO
      delete from titre_brouillard
            where tib_montant = 0;
   end;

   procedure set_titre_brouillard_intern (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      lepconum            maracuja.plan_comptable.pco_num%type;
      libelle             maracuja.plan_comptable.pco_libelle%type;
      chap                varchar2 (2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;
      ctpgescode          titre.ges_code%type;
      pconum_185          gestion_exercice.pco_num_185%type;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

      -- modif fred 04/2007
      select c.ges_code,
             ge.pco_num_185
      into   gescodecompta,
             pconum_185
      from   gestion g, comptabilite c, gestion_exercice ge
      where  g.ges_code = letitre.ges_code and g.com_ordre = c.com_ordre and g.ges_code = ge.ges_code and ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
      select rb.rec_id_reduction
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

      -- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      open c_recettes;

      loop
         fetch c_recettes
         into  recettectrlplanco;

         exit when c_recettes%notfound;

         select max (rec_id)
         into   recid
         from   recette
         where  rec_ordre = recettectrlplanco.rpco_id;

         -- recup des 2 premiers caracteres du compte
         select substr (recettectrlplanco.pco_num, 1, 2)
         into   chap
         from   dual;

         if chap != '18' then
            lepconum := api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
         else
            raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || recettectrlplanco.pco_num || ')');
         end if;

         -- creation du titre_brouillard visa --
         --  RECETTE_CTRL_PLANCO
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   letitre.ges_code,
                   --GES_CODE,
                   lepconum,   --PCO_NUM
                   abs (recettectrlplanco.rpco_ht_saisie),
                   
                   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   lesens,
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = recettectrlplanco.rpco_id;

         -- recette_ctrl_planco_tva
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   exe_ordre,   --EXE_ORDRE,
                   gescodecompta,
                   -- ges_code,               --GES_CODE,
                   pco_num,   --PCO_NUM
                   abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_tva
            where  rpco_id = recettectrlplanco.rpco_id;

         -- si on est sur un sacd, la contrepartie reste sur le sacd
         if (pconum_185 is not null) then
            ctpgescode := letitre.ges_code;
         else
            ctpgescode := gescodecompta;
         end if;

         -- recette_ctrl_planco_ctp on force le 181
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   ctpgescode,   --GES_CODE,
                   '181',
                   --PCO_NUM
                   abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id;
      end loop;

      close c_recettes;
   end;

   -- outils
   function inverser_sens_orv (tboordre integer, sens varchar)
      return varchar
   is
      cpt   integer;
   begin
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
      select count (*)
      into   cpt
      from   type_bordereau
      where  tbo_sous_type = 'REVERSEMENTS' and tbo_ordre = tboordre;

      if (cpt != 0) then
         if (sens = 'C') then
            return 'D';
         else
            return 'C';
         end if;
      end if;

      return sens;
   end;

   function recup_gescode (abrid integer)
      return varchar
   is
      gescode   bordereau.ges_code%type;
   begin
      select distinct ges_code
      into            gescode
      from            abricot_bord_selection
      where           abr_id = abrid;

      return gescode;
   end;

   function recup_utlordre (abrid integer)
      return integer
   is
      utlordre   bordereau.utl_ordre%type;
   begin
      select distinct utl_ordre
      into            utlordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return utlordre;
   end;

   function recup_exeordre (abrid integer)
      return integer
   is
      exeordre   bordereau.exe_ordre%type;
   begin
      select distinct exe_ordre
      into            exeordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return exeordre;
   end;

   function recup_tboordre (abrid integer)
      return integer
   is
      tboordre   bordereau.tbo_ordre%type;
   begin
      select distinct tbo_ordre
      into            tboordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return tboordre;
   end;

   function recup_groupby (abrid integer)
      return varchar
   is
      abrgroupby   abricot_bord_selection.abr_group_by%type;
   begin
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

      return abrgroupby;
   end;

   function inverser_sens (sens varchar)
      return varchar
   is
   begin
      if sens = 'D' then
         return 'C';
      else
         return 'D';
      end if;
   end;

   procedure numeroter_bordereau (borid integer)
   is
      cpt_mandat   integer;
      cpt_titre    integer;
   begin
      select count (*)
      into   cpt_mandat
      from   mandat
      where  bor_id = borid;

      select count (*)
      into   cpt_titre
      from   titre
      where  bor_id = borid;

      if cpt_mandat + cpt_titre = 0 then
         raise_application_error (-20001, 'Bordereau  vide');
      else
         numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
         numerotationobject.numeroter_mandat (borid);
-- boucle titre
         numerotationobject.numeroter_titre (borid);
      end if;
   end;

   function traiter_orgid (orgid integer, exeordre integer)
      return integer
   is
      topordre     integer;
      cpt          integer;
      orilibelle   origine.ori_libelle%type;
      convordre    integer;
   begin
      if orgid is null then
         return null;
      end if;

      select count (*)
      into   cpt
      from   accords.convention_limitative
      where  org_id = orgid and exe_ordre = exeordre;

      if cpt > 0 then
         -- recup du type_origine CONVENTION--
         select top_ordre
         into   topordre
         from   type_operation
         where  top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

         select distinct con_ordre
         into            convordre
         from            accords.convention_limitative
         where           org_id = orgid and exe_ordre = exeordre;

         select (exe_ordre || '-' || lpad (con_index, 5, '0') || ' ' || con_objet)
         into   orilibelle
         from   accords.contrat
         where  con_ordre = convordre;
      else
         select count (*)
         into   cpt
         from   jefy_admin.organ
         where  org_id = orgid and org_lucrativite = 1;

         if cpt = 1 then
            -- recup du type_origine OPERATION LUCRATIVE --
            select top_ordre
            into   topordre
            from   type_operation
            where  top_libelle = 'OPERATION LUCRATIVE';

            --le libelle utilisateur pour le suivie en compta --
            select org_ub || '-' || org_cr || '-' || org_souscr
            into   orilibelle
            from   jefy_admin.organ
            where  org_id = orgid;
         else
            return null;
         end if;
      end if;

-- l origine est t elle deja  suivie --
      select count (*)
      into   cpt
      from   origine
      where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid;

      if cpt >= 1 then
         select ori_ordre
         into   cpt
         from   origine
         where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid and rownum = 1;
      else
         select origine_seq.nextval
         into   cpt
         from   dual;

         insert into origine
                     (ori_entite,
                      ori_key_name,
                      ori_libelle,
                      ori_ordre,
                      ori_key_entite,
                      top_ordre
                     )
         values      ('JEFY_ADMIN',
                      'ORG_ID',
                      orilibelle,
                      cpt,
                      orgid,
                      topordre
                     );
      end if;

      return cpt;
   end;

   procedure controle_bordereau (borid integer)
   is
      ttc             maracuja.titre.tit_ttc%type;
      detailttc       maracuja.titre.tit_ttc%type;
      ordottc         maracuja.titre.tit_ttc%type;
      debit           maracuja.titre.tit_ttc%type;
      credit          maracuja.titre.tit_ttc%type;
      cpt             integer;
      message         varchar2 (50);
      messagedetail   varchar2 (50);
   begin
      select count (*)
      into   cpt
      from   maracuja.titre
      where  bor_id = borid;

      if cpt = 0 then
-- somme des maracuja.titre
         select sum (man_ttc)
         into   ttc
         from   maracuja.mandat
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (d.dep_ttc)
         into   detailttc
         from   maracuja.mandat m, maracuja.depense d
         where  m.man_id = d.man_id and m.bor_id = borid;

-- la somme des credits
         select sum (mab_montant)
         into   credit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'C';

-- la somme des debits
         select sum (mab_montant)
         into   debit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'D';

-- somme des jefy.recette
         select sum (d.dpco_ttc_saisie)
         into   ordottc
         from   maracuja.mandat m, jefy_depense.depense_ctrl_planco d
         where  m.man_id = d.man_id and m.bor_id = borid;

         message := ' mandats ';
         messagedetail := ' depenses ';
      else
-- somme des maracuja.titre
         select sum (tit_ttc)
         into   ttc
         from   maracuja.titre
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (r.rec_monttva + r.rec_mont)
         into   detailttc
         from   maracuja.titre t, maracuja.recette r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

-- la somme des credits
         select sum (tib_montant)
         into   credit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'C';

-- la somme des debits
         select sum (tib_montant)
         into   debit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'D';

-- somme des jefy.recette
         select sum (r.rpco_ttc_saisie)
         into   ordottc
         from   maracuja.titre t, jefy_recette.recette_ctrl_planco r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

         message := ' titres ';
         messagedetail := ' recettes ';
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  debit <> credit : ' || debit || ' ' || credit);
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  ecriture <> budgetaire : ' || debit || ' ' || ttc);
      end if;

-- somme des maracuja.titre = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (detailttc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant des ' || messagedetail || ' :' || ttc || ' ' || detailttc);
      end if;

-- somme des jefy.recette = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (ordottc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant ordonnateur des ' || messagedetail || ' :' || ttc || ' ' || ordottc);
      end if;

      bordereau_abricot.ctrl_date_exercice (borid);
   end;

   -- Controle la coherence des deux bordereaux de prestations internes (dep = rec)
   procedure ctrl_bordereaux_pi (boriddep integer, boridrec integer)
   is
      flag         integer;
      nbdep        integer;
      nbrec        integer;
      manid        mandat.man_id%type;
      titid        titre.tit_id%type;
      tmpmandat    mandat%rowtype;
      tmptitre     titre%rowtype;
      tmpprest     integer;
      montantdep   mandat.man_ht%type;
      montantrec   titre.tit_ht%type;

      cursor prests
      is
         select distinct prest_id
         from            (select prest_id
                          from   mandat
                          where  bor_id = boriddep
                          union
                          select prest_id
                          from   titre
                          where  bor_id = boridrec);
   begin
      if (boriddep is null) then
         raise_application_error (-20001, 'Reference au bordereau de depense interne nulle.');
      end if;

      if (boridrec is null) then
         raise_application_error (-20001, 'Reference au bordereau de recette interne nulle.');
      end if;

      -- verifier qu'il s'agit bien de bordereaux de PI
      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 201 and bor_id = boriddep;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de depense interne.');
      end if;

      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 200 and bor_id = boridrec;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de recette interne.');
      end if;

      -- comparer le nombre de titres et de mandats
      select count (*)
      into   nbdep
      from   mandat
      where  bor_id = boriddep;

      select count (*)
      into   nbrec
      from   titre
      where  bor_id = boridrec;

      if (nbdep = 0) then
         raise_application_error (-20001, 'Aucun mandat trouve sur le bordereau');
      end if;

      if (nbdep <> nbrec) then
         raise_application_error (-20001, 'Nombre de mandats different du nombre de titres. ' || 'Mandats : ' || nbdep || ' / Titres : ' || nbrec);
      end if;

      open prests;

      loop
         fetch prests
         into  tmpprest;

         exit when prests%notfound;

         select count (*)
         into   nbdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select count (*)
         into   nbrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (nbdep <> nbrec) then
            raise_application_error (-20001, 'Incoherence : Nombre de titres (' || nbrec || ') different du nombre de mandats (' || nbdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;

         select sum (man_ht)
         into   montantdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select sum (tit_ht)
         into   montantrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (montantdep <> montantrec) then
            raise_application_error (-20001, 'Incoherence : Montant des titres (' || montantrec || ') different du montant des mandats  (' || montantdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;
      end loop;

      close prests;

      select sum (man_ht)
      into   montantdep
      from   mandat
      where  bor_id = boriddep;

      select sum (tit_ht)
      into   montantrec
      from   titre
      where  bor_id = boriddep;

      if (montantdep <> montantrec) then
         raise_application_error (-20001, 'Incoherence : Montant total des titres (' || montantrec || ') different du montant total des mandats  (' || montantdep || ')');
      end if;
   end;

   procedure get_recette_prelevements (titid integer)
   is
      cpt                      integer;
      facture_titre_data       prestation.facture_titre%rowtype;
--     client_data              prelev.client%ROWTYPE;
      oriordre                 integer;
      modordre                 integer;
      recid                    integer;
      echeid                   integer;
      echeancier_data          jefy_echeancier.echeancier%rowtype;
      echeancier_prelev_data   jefy_echeancier.echeancier_prelev%rowtype;
      facture_data             jefy_recette.facture_budget%rowtype;
      personne_data            grhum.v_personne%rowtype;
      premieredate             date;
   begin
-- verifier s il existe un echancier pour ce titre
      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

      if (cpt != 1) then
         return;
      end if;

-- recup du eche_id / ech_id
      select eche_id
      into   echeid
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

-- recup du des infos du prelevements
      select *
      into   echeancier_data
      from   jefy_echeancier.echeancier
      where  ech_id = echeid;

      select *
      into   echeancier_prelev_data
      from   jefy_echeancier.echeancier_prelev
      where  ech_id = echeid;

      select *
      into   facture_data
      from   jefy_recette.facture_budget
      where  eche_id = echeid;

      select *
      into   personne_data
      from   grhum.v_personne
      where  pers_id = facture_data.pers_id;

      select echd_date_prevue
      into   premieredate
      from   jefy_echeancier.echeancier_detail
      where  echd_numero = 1 and ech_id = echeid;

      select rec_id
      into   recid
      from   recette
      where  tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
  SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
  IF (cpt=0) THEN
        RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
  END IF;
  IF (cpt>1) THEN
        RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
  END IF;

  SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

  UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

      -- recup ??
      oriordre := gestionorigine.traiter_orgid (facture_data.org_id, facture_data.exe_ordre);

      insert into maracuja.echeancier
                  (eche_autoris_signee,
                   fou_ordre_client,
                   con_ordre,
                   eche_date_1ere_echeance,
                   eche_date_creation,
                   eche_date_modif,
                   eche_echeancier_ordre,
                   eche_etat_prelevement,
                   ft_ordre,
                   eche_libelle,
                   eche_montant,
                   eche_montant_en_lettres,
                   eche_nombre_echeances,
                   eche_numero_index,
                   org_ordre,
                   prest_ordre,
                   eche_prise_en_charge,
                   eche_ref_facture_externe,
                   eche_supprime,
                   exe_ordre,
                   tit_id,
                   rec_id,
                   tit_ordre,
                   ori_ordre,
                   pers_id,
                   org_id,
                   pers_description
                  )
      values      ('O',   --ECHE_AUTORIS_SIGNEE
                   facture_data.fou_ordre,   --FOU_ORDRE_CLIENT
                   null,
                   --echancier_data.CON_ORDRE  ,--CON_ORDRE
                   premieredate,
                   --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                   sysdate,
                   --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                   sysdate,   --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                   echeancier_data.ech_id,
                   
                   --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                   'V',
                   --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                   facture_data.fac_id,
                   --echancier_data.FT_ORDRE  ,--FT_ORDRE
                   echeancier_data.ech_libelle,
                   --echancier_data.LIBELLE,--ECHE_LIBELLE
                   echeancier_data.ech_montant,   --ECHE_MONTANT
                   echeancier_data.ech_montant_lettres,
                   --ECHE_MONTANT_EN_LETTRES
                   echeancier_data.ech_nb_echeances,   --ECHE_NOMBRE_ECHEANCES
                   echeancier_data.ech_id,
                   --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                   facture_data.org_id,
                   --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                   null,
                   
                   --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                   'O',   --ECHE_PRISE_EN_CHARGE
                   facture_data.fac_lib,
                   
                   --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                   'N',   --ECHE_SUPPRIME
                   facture_data.exe_ordre,
                   --EXE_ORDRE
                   titid,
                   recid,   --REC_ID,
                   -titid,
                   oriordre,   --ORI_ORDRE,
                   personne_data.pers_id,
                   --CLIENT_data.pers_id  ,--PERS_ID
                   facture_data.org_id,   --orgid a faire plus tard....
                   personne_data.pers_libelle   --    PERS_DESCRIPTION
                  );

      insert into maracuja.prelevement
                  (eche_echeancier_ordre,
                   reco_ordre,
                   fou_ordre,
                   prel_commentaire,
                   prel_date_modif,
                   prel_date_prelevement,
                   prel_prelev_date_saisie,
                   prel_prelev_etat,
                   prel_numero_index,
                   prel_prelev_montant,
                   prel_prelev_ordre,
                   rib_ordre,
                   prel_etat_maracuja
                  )
         select ech_id,   --ECHE_ECHEANCIER_ORDRE
                null,   --PREL_FICP_ORDRE
                facture_data.fou_ordre,   --FOU_ORDRE
                echd_commentaire,
                --PREL_COMMENTAIRE
                sysdate,
                --DATE_MODIF,--PREL_DATE_MODIF
                echd_date_prevue,   --PREL_DATE_PRELEVEMENT
                sysdate,   --,--PREL_PRELEV_DATE_SAISIE
                'ATTENTE',   --PREL_PRELEV_ETAT
                echd_numero,
                --PREL_NUMERO_INDEX
                echd_montant,
                --PREL_PRELEV_MONTANT
                echd_id,   --PREL_PRELEV_ORDRE
                echeancier_prelev_data.rib_ordre_debiteur,
                
                --RIB_ORDRE
                'ATTENTE'   --PREL_ETAT_MARACUJA
         from   jefy_echeancier.echeancier_detail
         where  ech_id = echeancier_data.ech_id;
   end;

   procedure ctrl_date_exercice (borid integer)
   is
      exeordre   integer;
      annee      integer;
   begin
      select to_char (bor_date_creation, 'YYYY'),
             exe_ordre
      into   annee,
             exeordre
      from   bordereau
      where  bor_id = borid and exe_ordre >= 2007;

      if exeordre < annee then
         update bordereau
            set bor_date_creation = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update mandat
            set man_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update titre
            set tit_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;
      end if;
   end;

-- GET_GES_CODE_FOR_MAN_ID
-- Renvoie la COMPOSANTE a prendre en compte en fonction du mandat. AGENCE ou COMPOSANTE.
   function get_ges_code_for_man_id (manid number)
      return comptabilite.ges_code%type
   is
      current_mandat          mandat%rowtype;
      pconumsacd              gestion_exercice.pco_num_185%type;
      visa_mode_paiement      planco_visa.pvi_contrepartie_gestion%type;
      visa_planco             planco_visa.pvi_contrepartie_gestion%type;
      code_agence_comptable   comptabilite.ges_code%type;
   begin
      select ges_code
      into   code_agence_comptable
      from   comptabilite;

      select *
      into   current_mandat
      from   mandat
      where  man_id = manid;

      -- SACD -- S'il s'agit d'un SACD on renvoie la composante associee au mandat.
      select pco_num_185
      into   pconumsacd
      from   gestion_exercice
      where  exe_ordre = current_mandat.exe_ordre and ges_code = current_mandat.ges_code;

      if (pconumsacd is not null) then
         return current_mandat.ges_code;
      else   -- Pas de SACD, on verifie le parametrage du Mode de Paiement (Mode_Paiement) puis du Compte de classe 6 (Planco_Visa).
         -- Si le parametrage du mode de paiement est renseigne , il est prioritaire
         select mod_contrepartie_gestion
         into   visa_mode_paiement
         from   mode_paiement
         where  mod_ordre = current_mandat.mod_ordre;

         if (visa_mode_paiement is not null) then
            -- Parametres : AGENCE ou COMPOSANTE
            if (visa_mode_paiement = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         else   -- Pas de parametrage du mode de paiement, on prend celui du compte (PCO_NUM)
            select pvi_contrepartie_gestion
            into   visa_planco
            from   planco_visa
            where  pco_num_ordonnateur = current_mandat.pco_num and exe_ordre = current_mandat.exe_ordre;

            if (visa_planco = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         end if;
      end if;
   end;

   function getfournisnom (fouordre integer)
      return varchar
   is
      founom   varchar2 (200);
   begin
      founom := '';

      select substr (nom || ' ' || prenom, 1, 200)
      into   founom
      from   v_fournis_light
      where  fou_ordre = fouordre;

      return founom;
   end;

   function recup_tcdsect (manid integer)
      return varchar
   is
      tcdsect   jefy_admin.type_credit.tcd_sect%type;
   begin
      select distinct tc.tcd_sect
      into   tcdsect
      from   jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.type_credit tc
      where  dpco.dep_id = db.dep_id and db.eng_id = eb.eng_id and eb.tcd_ordre = tc.tcd_ordre and man_id = manid;

      return tcdsect;
   end;
   
   
   function creer_depense(jefydepensebudget jefy_depense.depense_budget%rowtype, manid mandat.man_id%type) return maracuja.depense.dep_id%type is
      depid               depense.dep_id%type;
      
      tmpdepensepapier    jefy_depense.depense_papier%rowtype;
      jefydepenseplanco   jefy_depense.depense_ctrl_planco%rowtype;
      lignebudgetaire     depense.dep_ligne_budgetaire%type;
      fouadresse          depense.dep_adresse%type;
      founom              depense.dep_fournisseur%type;
      lotordre            depense.dep_lot%type;
      marordre            depense.dep_marches%type;
      fouordre            depense.fou_ordre%type;
      gescode             depense.ges_code%type;
      modordre            depense.mod_ordre%type;
      cpt                 integer;
      tcdordre            type_credit.tcd_ordre%type;
      tcdcode             type_credit.tcd_code%type;
      ecd_ordre_ema       ecriture_detail.ecd_ordre%type;
      orgid               integer;
      montantbudgetaire   depense.dep_ht%type;
      montanttva          depense.dep_ht%type;
      montantttc          depense.dep_ht%type;
      montantapayer       depense.dep_ht%type;

 
   begin
      
         -- creation du depid --
         select depense_seq.nextval
         into   depid
         from   dual;

         -- creation de lignebudgetaire--
         select org_ub || ' ' || org_cr || ' ' || org_souscr
         into   lignebudgetaire
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         --recuperer le type de credit a partir de la commande
         select tcd_ordre
         into   tcdordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         --AND eng_stat !='A'
         select org_ub,
                org_id
         into   gescode,
                orgid
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         -- fouordre --
         select fou_ordre
         into   fouordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         -- founom --
         founom := getfournisnom (fouordre);

         -- fouadresse --
         select count (*)
         into   cpt
         from   v_depense_adresse
         where  dep_id = jefydepensebudget.dep_id;

         if (cpt = 0) then
            raise_application_error (-20001, 'Pas d''adresse de facturation pour le fournisseur ' || founom);
         end if;

         select substr (adresse, 1, 196)
         into   fouadresse
         from   (select *
                 from   v_depense_adresse
                 where  dep_id = jefydepensebudget.dep_id)
         where  rownum = 1;

         --AND eng_stat !='A'

         -- lotordre --
         select count (*)
         into   cpt
         from   jefy_marches.attribution
         where  att_ordre = (select att_ordre
                             from   jefy_depense.engage_ctrl_marche
                             where  eng_id = jefydepensebudget.eng_id);

         if cpt = 0 then
            lotordre := null;
         else
            select lot_ordre
            into   lotordre
            from   jefy_marches.attribution
            where  att_ordre = (select att_ordre
                                from   jefy_depense.engage_ctrl_marche
                                where  eng_id = jefydepensebudget.eng_id);
         end if;

         -- marordre --
         select count (*)
         into   cpt
         from   jefy_marches.lot
         where  lot_ordre = lotordre;

         if cpt = 0 then
            marordre := null;
         else
            select mar_ordre
            into   marordre
            from   jefy_marches.lot
            where  lot_ordre = lotordre;
         end if;

         --MOD_ORDRE --
         select mod_ordre
         into   modordre
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recuperer l'ecriture_detail pour emargements semi-auto
         select ecd_ordre
         into   ecd_ordre_ema
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         -- recup de la depense papier
         select *
         into   tmpdepensepapier
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recup des infos de depense_ctrl_planco
         select *
         into   jefydepenseplanco
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         montantbudgetaire := abricot_util.get_dpco_montant_budgetaire (jefydepenseplanco.dpco_id);
         montantapayer := abricot_util.get_dpco_montant_apayer (jefydepenseplanco.dpco_id);
         montantttc := abricot_util.get_dpco_montant_ttc (jefydepenseplanco.dpco_id);
         montanttva := montantttc - montantbudgetaire;

         -- creation de la depense --
         insert into depense
         values      (fouadresse,   --DEP_ADRESSE,
                      null,   --DEP_DATE_COMPTA,
                      tmpdepensepapier.dpp_date_reception,   --DEP_DATE_RECEPTION,
                      tmpdepensepapier.dpp_date_service_fait,   --DEP_DATE_SERVICE,
                      'VALIDE',   --DEP_ETAT,
                      founom,   --DEP_FOURNISSEUR,
                      montantbudgetaire,   --DEP_HT,
                      depid,   --DEP_ID,
                      lignebudgetaire,   --DEP_LIGNE_BUDGETAIRE,
                      lotordre,   --DEP_LOT,
                      marordre,   --DEP_MARCHES,
                      montantapayer,   --DEP_MONTANT_DISQUETTE,
                      null,   -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                      substr (tmpdepensepapier.dpp_numero_facture, 1, 199),   --DEP_NUMERO,
                      jefydepenseplanco.dpco_id,   --DEP_ORDRE,
                      null,   --DEP_REJET,
                      tmpdepensepapier.rib_ordre,   --DEP_RIB,
                      'NON',   --DEP_SUPPRESSION,
                      montantttc,   --DEP_TTC,
                      montanttva,   -- DEP_TVA,
                      tmpdepensepapier.exe_ordre,   --EXE_ORDRE,
                      fouordre,   --FOU_ORDRE,
                      gescode,   --GES_CODE,
                      manid,   --MAN_ID,
                      jefydepenseplanco.man_id,   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                      modordre,
                      jefydepenseplanco.pco_num,   --PCO_ORDRE,
                      tmpdepensepapier.utl_ordre,   --UTL_ORDRE
                      orgid,   --org_ordre
                      tcdordre,
                      ecd_ordre_ema,   -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                      tmpdepensepapier.dpp_date_facture
                     );

    return depid;
   end;
   
end;
/





CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_util
is
-- Methodes utilitaires pour la generation des mandats et titres
--
--//FIXME 06/01/2012 adapter les fonctions lorsque la respartition par taux de TVA sera active, pour l'instant le taux n'est pas exploité
--

   /* indique si la TVA doit etre collectee pour la depense (depend du mode de paiement affecte) res=0 sinon*/
   function is_dpco_tva_collectee (dpcoid integer)
      return integer
   as
      res            integer;
      modcode        mode_paiement.mod_code%type;
      pconumtvactp   mode_paiement.pco_num_tva_ctp%type;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      if (res > 0) then
         select mod_code,
                pco_num_tva_ctp
         into   modcode,
                pconumtvactp
         from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid and pco_num_tva_ctp is not null;

         if (pconumtvactp is null) then
            raise_application_error (-20001, 'Le compte de TVA collectee n''est pas defini pour le mode de paiement ' || modcode || ' alors que le mode de paiement est défini comme "Paiement HT"');
         end if;
      end if;

      return res;
   end;



-----------------------------------------------------------------------------
   /* >0 si le mandat est passe sur un sacd */
   function is_man_sur_sacd (manid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   mandat m, gestion_exercice g
      where  m.ges_code = g.ges_code and m.exe_ordre = g.exe_ordre and g.pco_num_185 is not null and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* renvoie le taux de prorata affecte a la depense */
   function get_dpco_taux_prorata (dpcoid integer)
      return number
   as
      res   number;
   begin
      select tap_taux
      into   res
      from   jefy_admin.taux_prorata tp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.tap_id = tp.tap_id and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie le montant budgetaire de la depense*/
   function get_dpco_montant_budgetaire (dpcoid integer)
      return number
   as
      res   number;
   begin
        -- s'il s'agit dune liquidation définitive sur extourne, 
        -- le montant budgétaire stocké au niveau de la liquidation est à 0, donc on le recalcule
        if (extourne.is_dpco_extourne_def(dpcoid)>0 or extourne.is_dpco_extourne(dpcoid)>0) then
            res := extourne.calcul_montant_budgetaire(dpcoid);
        else
      select dpco_montant_budgetaire
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;
end if;
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant de TVA de la facture pour le taux specifié */
   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      -- //FIXME adapter ca lorsque la respartition par taux de TVA sera active
      select dpco_tva_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la part de TVA a deduire : TTC - budgetaire */
   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      --res := get_dep_montant_tva (depid, tauxtva) * get_dep_taux_prorata (depid) / 100;
      --select (dpco_ttc_saisie - dpco_montant_budgetaire)
      select (dpco_ttc_saisie -  get_dpco_montant_budgetaire(dpcoid))
     
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie la part de TVA a collecter : Montant de la TVA si mode de paiement collecte la TVA (i.e. fournisseur soumis TVA intra)  */
   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
      cpt   plan_comptable_exer.pco_num%type;
   begin
      res := 0;

      if (is_dpco_tva_collectee (dpcoid) > 0) then
         res := get_dpco_montant_tva (dpcoid, tauxtva);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la TVA a reverser : collectee - deduite  */
   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      res := get_dpco_montant_tva_coll (dpcoid, tauxtva) - get_dpco_montant_tva_ded (dpcoid, tauxtva);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   jefy_depense.depense_ctrl_planco
         where  dpco_id = dpcoid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant TTC pour un depense_ctrl_planco  */
   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      select dpco.dpco_ttc_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant a payer pour un depense_ctrl_planco : TTC - TVA collectee */
   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      res := get_dpco_montant_ttc (dpcoid) - get_dpco_montant_tva_coll (dpcoid, null);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le tbo_ordre du bordereau du mandat */
   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type
   as
      res   type_bordereau.tbo_ordre%type;
   begin
      select b.tbo_ordre
      into   res
      from   bordereau b, mandat m
      where  m.bor_id = b.bor_id and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement du mandat s'il existe et est actif */
   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de ctp affecte au mode de paiement
      res := get_mp_compte_ctp (modordre);

      -- si null, on recupere le compte de  ctp affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_ctp (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
 /* Renvoie le code gestion a utiliser pour la contrepartie du mandat (la composante ou agence suivant les cas) */
   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type
   as
      res                  gestion.ges_code%type;
      gescode_composante   gestion.ges_code%type;
      gescode_agence       gestion.ges_code%type;
      exeordre             mandat.exe_ordre%type;
      pconumordo           mandat.pco_num%type;
      typegestion          planco_visa.pvi_contrepartie_gestion%type;
      modordre             mandat.mod_ordre%type;
      flag                 integer;
   begin
      -- si la ctp est definie dans le mandat on la prend, sinon on prend celle definie dans planco_visa
      -- si c'est un SACD la ctp va forcement sur le SACD
      select m.ges_code,
             m.exe_ordre,
             m.pco_num,
             c.ges_code,
             m.mod_ordre
      into   gescode_composante,
             exeordre,
             pconumordo,
             gescode_agence,
             modordre
      from   mandat m, gestion g, comptabilite c
      where  m.ges_code = g.ges_code and g.com_ordre = c.com_ordre and m.man_id = manid;

      if (is_man_sur_sacd (manid) > 0) then
         res := gescode_composante;
      else
         select mod_contrepartie_gestion
         into   typegestion
         from   mode_paiement mp
         where  mp.mod_ordre = modordre;

         if (typegestion is null) then
            select count (*)
            into   flag
            from   planco_visa pv
            where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

            if (flag > 0) then
               select pvi_contrepartie_gestion
               into   typegestion
               from   planco_visa pv
               where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;
            end if;
         end if;

         -- si non specifie, on prend l'agence
         if (typegestion = 'COMPOSANTE') then
            res := gescode_composante;
         else
            res := gescode_agence;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le montant de la TVA a collecter pour le mandat (depend du mode de paiement) */
   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_coll (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_ded (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_apayer (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_ttc (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_budgetaire (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA de collecte affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva_ctp,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_visa,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res    plan_comptable_exer.pco_num%type;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (flag > 0) then
         select pco_num_tva
         into   res
         from   planco_visa
         where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

         if (res is not null) then
            if (api_planco.is_planco_valide (res, exeordre) = 0) then
               raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
            end if;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res    plan_comptable_exer.pco_num%type;
      flag   integer;
   begin
      res := null;

      select count (*)
      into   flag
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (flag > 0) then
         select pco_num_ctrepartie
         into   res
         from   planco_visa
         where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

         if (res is not null) then
            if (api_planco.is_planco_valide (res, exeordre) = 0) then
               raise_application_error (-20001, 'Le compte de contrepartie ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
            end if;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type)
   is
   begin
      insert into mandat_brouillard
      values      (null,   --ECD_ORDRE,
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   montant,   --MAB_MONTANT,
                   operation,   --MAB_OPERATION,
                   mandat_brouillard_seq.nextval,   --MAB_ORDRE,
                   sens,   --MAB_SENS,
                   manid,
                   --MAN_ID,
                   pconum   --PCO_NU
                  );
   end;
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.afaireaprestraitement
is
-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 2.0 - suppression des references aux user < 2007
   procedure apres_visa_bordereau (borid integer)
   is
   begin
      prv_apres_visa_bordereau (borid);
   end;

   procedure prv_apres_visa_bordereau (borid integer)
   is
      lebordereau          bordereau%rowtype;
      lesoustype           type_bordereau.tbo_sous_type%type;
      autoviserbordrejet   maracuja.parametre.par_value%type;
      brjordre             maracuja.bordereau_rejet.brj_ordre%type;
      flag                 integer;
   begin
   
    API_BORDEREAU.APRES_VISA ( BORID );
   
   
--      select *
--      into   lebordereau
--      from   bordereau
--      where  bor_id = borid;

--      select tbo_sous_type
--      into   lesoustype
--      from   type_bordereau
--      where  tbo_ordre = lebordereau.tbo_ordre;

--      if lebordereau.bor_etat = 'VISE' then
--         -- s il y a un bordereau de rejet, si param = auto viser, viser le bordereau de rejet
--         select count (*)
--         into   flag
--         from   maracuja.mandat
--         where  bor_id = lebordereau.bor_id and brj_ordre is not null;

--         if (flag > 0) then
--            autoviserbordrejet := jefy_admin.api_parametre.get_param_maracuja ('AUTO_VISER_BORD_REJET_DEP', lebordereau.exe_ordre, 'NON');

--            if (autoviserbordrejet = 'OUI') then
--               select min (brj_ordre)
--               into   brjordre
--               from   maracuja.mandat
--               where  bor_id = lebordereau.bor_id and brj_ordre is not null;

--               bordereau_abricot.viser_bordereau_rejet (brjordre);
--            end if;
--         end if;

--         if (lesoustype = 'REVERSEMENTS') then
--            prv_apres_visa_reversement (borid);
--         end if;

--         emarger_visa_bord_prelevement (borid);
--      end if;
   end;

-- permet de transmettre a jefy_depense les mandats de reversements vises
-- les mandats rejetes sont traites au moment du visa du bordereau de rejet
   procedure prv_apres_visa_reversement (borid integer)
   is
      manid   mandat.man_id%type;

      cursor c_mandatvises
      is
         select man_id
         from   mandat
         where  bor_id = borid and man_etat = 'VISE';
   begin
      open c_mandatvises;

      loop
         fetch c_mandatvises
         into  manid;

         exit when c_mandatvises%notfound;
         jefy_depense.apres_visa.viser_reversement (manid);
      end loop;

      close c_mandatvises;
   end;

   procedure apres_reimputation (reiordre integer)
   is
      ex              integer;
      manid           mandat.man_id%type;
      titid           titre.tit_id%type;
      pconumnouveau   plan_comptable_exer.pco_num%type;
      pconumancien    plan_comptable_exer.pco_num%type;
   begin
      select man_id,
             tit_id,
             pco_num_nouveau,
             pco_num_ancien
      into   manid,
             titid,
             pconumnouveau,
             pconumancien
      from   reimputation
      where  rei_ordre = reiordre;

      if manid is not null then
         jefy_depense.reimputer.reimputation_maracuja (manid, pconumnouveau);
      --update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;
      end if;

      if titid is not null then
         update jefy_recette.recette_ctrl_planco
            set pco_num = pconumnouveau
          where tit_id = titid and pco_num = pconumancien;
      end if;
   end;

   procedure apres_paiement (paiordre integer)
   is
      cpt   integer;
   begin
      --SELECT 1 INTO  cpt FROM dual;
      emarger_paiement (paiordre);
   end;

   procedure emarger_paiement (paiordre integer)
   is
      cursor mandats (lepaiordre integer)
      is
         select *
         from   mandat
         where  pai_ordre = lepaiordre;

      cursor non_emarge_debit (lemanid integer)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'D';

      cursor non_emarge_credit_compte (lemanid integer, lepconum varchar)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'C';

      lemandat          maracuja.mandat%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lepaiement        maracuja.paiement%rowtype;
      cpt               integer;
   begin
-- recup infos
      select *
      into   lepaiement
      from   paiement
      where  pai_ordre = paiordre;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lepaiement.exe_ordre,
                   3,
                   lepaiement.utl_ordre,
                   lepaiement.com_ordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les mandats du paiement
      open mandats (paiordre);

      loop
         fetch mandats
         into  lemandat;

         exit when mandats%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_debit (lemandat.man_id);

         loop
            fetch non_emarge_debit
            into  ecriture_debit;

            exit when non_emarge_debit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_credit_compte (lemandat.man_id, ecriture_debit.pco_num);

            loop
               fetch non_emarge_credit_compte
               into  ecriture_credit;

               exit when non_emarge_credit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_debit.ecd_ordre,
                               ecriture_credit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               lemandat.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_credit_compte;
         end loop;

         close non_emarge_debit;
      end loop;

      close mandats;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      end if;
   end;

   procedure apres_recouvrement_releve (recoordre integer)
   is
   begin
      emarger_prelevement_releve (recoordre);
   end;

   procedure emarger_prelevement_releve (recoordre integer)
   is
      cpt   integer;
   begin
      -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
      select 1
      into   cpt
      from   dual;

      emarger_prelevement (recoordre);
   end;

   procedure apres_recouvrement (recoordre integer)
   is
   begin
      emarger_prelevement (recoordre);
   end;

   procedure emarger_prelevement (recoordre integer)
   is
   begin
      if (recoordre is null) then
         raise_application_error (-20001, 'Le parametre recoordre est null.');
      end if;

      emarger_prelev_ac_titre (recoordre);
      emarger_prelev_ac_ecr (recoordre);
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
   procedure emarger_prelev_ac_titre (recoordre integer)
   is
      cursor titres (lerecoordre integer)
      is
         select *
         from   titre
         where  tit_id in (select tit_id
                           from   prelevement p, echeancier e
                           where  e.eche_echeancier_ordre = p.eche_echeancier_ordre and p.reco_ordre = lerecoordre);

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lerecouvrement    maracuja.recouvrement%rowtype;
      cpt               integer;
   begin
      -- recup infos
      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lerecouvrement.exe_ordre,
                   3,
                   lerecouvrement.utl_ordre,
                   lerecouvrement.com_ordre,
                   0,
                   'VALIDE'
                  );

      -- on fetch les titres du recrouvement
      open titres (recoordre);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               ecriture_debit.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

      -- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
   procedure emarger_prelev_ac_ecr (recoordre integer)
   is
      cursor lesprelevements (lerecoordre integer)
      is
         select *
         from   maracuja.prelevement
         where  reco_ordre = lerecoordre;

      ecrdetailcredit   ecriture_detail%rowtype;
      ecrdetaildebit    ecriture_detail%rowtype;
      ecriture_credit   ecriture_detail%rowtype;
      ecriture_debit    ecriture_detail%rowtype;
      leprelevement     maracuja.prelevement%rowtype;
      flag              integer;
      flag2             integer;
      retval            integer;
      utlordre          integer;

      cursor non_emarge_credit (prelevordre integer)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (prelevordre integer, lepconum varchar)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and pco_num = lepconum and e.ecd_sens = 'D';
   begin
      select utl_ordre
      into   utlordre
      from   recouvrement
      where  reco_ordre = recoordre;

      open lesprelevements (recoordre);

      loop
         fetch lesprelevements
         into  leprelevement;

         exit when lesprelevements%notfound;
         ecrdetailcredit := null;
         ecrdetaildebit := null;

         -- recuperation de l'ecriture de l'echeancier non emargee en debit
         select count (*)
         into   flag2
         from   maracuja.ecriture_detail
         where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                            from   maracuja.echeancier_detail_ecr
                                                                            where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

         if (flag2 = 1) then
            select *
            into   ecrdetaildebit
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                               from   maracuja.echeancier_detail_ecr
                                                                               where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

            -- s'il y a une ecriture non emargee en debit sur l'echeancier,
            -- on emarge avec le credit correspondant du prelevement
            -- recuperation de l'ecriture du prelevement non emargee en credit
            select count (*)
            into   flag
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                     from   maracuja.prelevement_detail_ecr
                                                                                                                     where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

            if (flag = 1) then
               select *
               into   ecrdetailcredit
               from   maracuja.ecriture_detail
               where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                        from   maracuja.prelevement_detail_ecr
                                                                                                                        where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

               -- si les deux ecritures ont ete recuperees, on les emarge
               if (afaireaprestraitement.verifier_emar_exercice (ecrdetailcredit.ecr_ordre, ecrdetaildebit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecrdetailcredit.ecd_ordre, ecrdetaildebit.ecd_ordre, 3, utlordre);
               end if;
            end if;
         end if;

         -- emargement entre les debits et credits associes au prelevement
         -- (suite a saisie releve)

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (leprelevement.prel_prelev_ordre);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (leprelevement.prel_prelev_ordre, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, utlordre);
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close lesprelevements;
   end;

   procedure emarger_visa_bord_prelevement (borid integer)
   is
      cursor titres (leborid integer)
      is
         select *
         from   titre
         where  bor_id = leborid;

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lebordereau       maracuja.bordereau%rowtype;
      cpt               integer;
      comordre          integer;
   begin
-- recup infos
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

-- recup du com_ordre
      select com_ordre
      into   comordre
      from   gestion
      where  ges_code = lebordereau.ges_code;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lebordereau.exe_ordre,
                   3,
                   lebordereau.utl_ordre,
                   comordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les titres du recouvrement
      open titres (borid);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_debit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               letitre.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

   function verifier_emar_exercice (ecrcredit integer, ecrdebit integer)
      return integer
   is
      reponse     integer;
      execredit   exercice.exe_ordre%type;
      exedebit    exercice.exe_ordre%type;
   begin
-- init
      reponse := 0;

      select exe_ordre
      into   execredit
      from   ecriture
      where  ecr_ordre = ecrcredit;

      select exe_ordre
      into   exedebit
      from   ecriture
      where  ecr_ordre = ecrdebit;

      if exedebit = execredit then
         return 1;
      else
         return 0;
      end if;
   end;
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.api_bordereau
is
   -- a appeler apres le visa d'un bordereau dans Maracuja
   procedure apres_visa (borid bordereau.bor_id%type)
   is
      lebordereau          bordereau%rowtype;
      lesoustype           type_bordereau.tbo_sous_type%type;
      autoviserbordrejet   maracuja.parametre.par_value%type;
      brjordre             maracuja.bordereau_rejet.brj_ordre%type;
      flag                 integer;
      boridn1              integer;
   begin
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      select tbo_sous_type
      into   lesoustype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      prv_numeroter_ecritures (borid);
      prv_traiter_bord_rejet (lebordereau);

      if (is_bord_mandats (borid) > 0) then
         if (lesoustype = 'REVERSEMENTS') then
            prv_apres_visa_reversement (borid);
         end if;

         -- si on est sur un bordereau de mandats a extourner, on cree les mandats d'extourne et on les vise
         if (extourne.is_bord_aextourner (borid) > 0) then
            extourne.creer_bordereau (borid, boridn1);
            extourne.viser_bord (boridn1, lebordereau.bor_date_visa, lebordereau.utl_ordre_visa);
         end if;
      end if;

      if (is_bord_titres (borid) > 0) then
         emarger_visa_bord_prelevement (borid);
      end if;
   end;

   -- permet de numeroter un bordereau de rejet s'il existe et de le viser en auto si le parametre de visa auto est a OUI
   procedure prv_traiter_bord_rejet (lebordereau bordereau%rowtype)
   is
      lesoustype           type_bordereau.tbo_sous_type%type;
      autoviserbordrejet   maracuja.parametre.par_value%type;
      brjordre             maracuja.bordereau_rejet.brj_ordre%type;
      flag                 integer;
   begin
      if lebordereau.bor_etat = 'VISE' then
         if (is_bord_mandats (lebordereau.bor_id) > 0) then
            select count (*)
            into   flag
            from   maracuja.mandat
            where  bor_id = lebordereau.bor_id and brj_ordre is not null;

            if (flag > 0) then
               select min (brj_ordre)
               into   brjordre
               from   maracuja.mandat
               where  bor_id = lebordereau.bor_id and brj_ordre is not null;

               numerotationobject.numeroter_bordereaurejet (brjordre);
               autoviserbordrejet := jefy_admin.api_parametre.get_param_maracuja ('AUTO_VISER_BORD_REJET_DEP', lebordereau.exe_ordre, 'NON');

               if (autoviserbordrejet = 'OUI') then
                  bordereau_abricot.viser_bordereau_rejet (brjordre);
               end if;
            end if;
         end if;

         if (is_bord_titres (lebordereau.bor_id) > 0) then
            select count (*)
            into   flag
            from   maracuja.titre
            where  bor_id = lebordereau.bor_id and brj_ordre is not null;

            if (flag > 0) then
               select min (brj_ordre)
               into   brjordre
               from   maracuja.titre
               where  bor_id = lebordereau.bor_id and brj_ordre is not null;

               numerotationobject.numeroter_bordereaurejet (brjordre);
               autoviserbordrejet := jefy_admin.api_parametre.get_param_maracuja ('AUTO_VISER_BORD_REJET_REC', lebordereau.exe_ordre, 'NON');

               if (autoviserbordrejet = 'OUI') then
                  bordereau_abricot.viser_bordereau_rejet (brjordre);
               end if;
            end if;
         end if;
      end if;
   end;

   procedure prv_apres_visa_reversement (borid bordereau.bor_id%type)
   is
      manid   mandat.man_id%type;

      cursor c_mandatvises
      is
         select man_id
         from   mandat
         where  bor_id = borid and man_etat = 'VISE';
   begin
      open c_mandatvises;

      loop
         fetch c_mandatvises
         into  manid;

         exit when c_mandatvises%notfound;
         jefy_depense.apres_visa.viser_reversement (manid);
      end loop;

      close c_mandatvises;
   end;

   procedure emarger_visa_bord_prelevement (borid bordereau.bor_id%type)
   is
      cursor titres (leborid integer)
      is
         select *
         from   titre
         where  bor_id = leborid;

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lebordereau       maracuja.bordereau%rowtype;
      cpt               integer;
      comordre          integer;
   begin
-- recup infos
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

-- recup du com_ordre
      select com_ordre
      into   comordre
      from   gestion
      where  ges_code = lebordereau.ges_code;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lebordereau.exe_ordre,
                   3,
                   lebordereau.utl_ordre,
                   comordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les titres du recouvrement
      open titres (borid);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_debit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               letitre.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

-- numerote les ecritures non numerotees associees a un bordereau (de mandat ou de titre)
   procedure prv_numeroter_ecritures (borid bordereau.bor_id%type)
   is
      ecrordre     ecriture.ecr_ordre%type;
      ecrlibelle   ecriture.ecr_libelle%type;
      flag         integer;

      cursor lesecritures (leborid bordereau.bor_id%type)
      is
         select distinct e.ecr_ordre as ecr_ordre,
                         e.ecr_libelle as ecr_libelle
         from            ecriture e inner join ecriture_detail ecd on (e.ecr_ordre = ecd.ecr_ordre)
                         inner join mandat_detail_ecriture mde on (ecd.ecd_ordre = mde.ecd_ordre)
                         inner join mandat m on (m.man_id = mde.man_id)
         where           m.bor_id = leborid and ecr_numero <= 0
         union all
         select distinct e.ecr_ordre as ecr_ordre,
                         e.ecr_libelle as ecr_libelle
         from            ecriture e inner join ecriture_detail ecd on (e.ecr_ordre = ecd.ecr_ordre)
                         inner join titre_detail_ecriture tde on (ecd.ecd_ordre = tde.ecd_ordre)
                         inner join titre m on (m.tit_id = tde.tit_id)
         where           m.bor_id = leborid and ecr_numero <= 0
         order by        ecr_libelle;
   begin
      select count (*)
      into   flag
      from   (select distinct e.ecr_ordre as ecr_ordre,
                              e.ecr_libelle as ecr_libelle
              from            ecriture e inner join ecriture_detail ecd on (e.ecr_ordre = ecd.ecr_ordre)
                              inner join mandat_detail_ecriture mde on (ecd.ecd_ordre = mde.ecd_ordre)
                              inner join mandat m on (m.man_id = mde.man_id)
              where           m.bor_id = borid and ecr_numero <= 0
              union all
              select distinct e.ecr_ordre as ecr_ordre,
                              e.ecr_libelle as ecr_libelle
              from            ecriture e inner join ecriture_detail ecd on (e.ecr_ordre = ecd.ecr_ordre)
                              inner join titre_detail_ecriture tde on (ecd.ecd_ordre = tde.ecd_ordre)
                              inner join titre m on (m.tit_id = tde.tit_id)
              where           m.bor_id = borid and ecr_numero <= 0);

      if (flag > 0) then
         open lesecritures (borid);

         loop
            fetch lesecritures
            into  ecrordre,
                  ecrlibelle;

            numerotationobject.numeroter_ecriture (ecrordre);
            exit when lesecritures%notfound;
         end loop;

         close lesecritures;
      end if;
   end;

   function is_bord_titres (borid bordereau.bor_id%type)
      return integer
   as
      flag   integer;
   begin
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      return flag;
   end;

   function is_bord_mandats (borid bordereau.bor_id%type)
      return integer
   as
      flag   integer;
   begin
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      return flag;
   end;
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.api_mandat
is
   procedure viser_mandat (manid mandat.man_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type, ecdlibelleprefix varchar2)
   is
      flag          integer;
      lemandat      mandat%rowtype;
      lebordereau   bordereau%rowtype;
      lemab         mandat_brouillard%rowtype;

      cursor mandatbrouillards
      is
         select *
         from   mandat_brouillard
         where  man_id = manid and mab_operation like 'VISA%';

      ecdlibelle    ecriture_detail.ecd_libelle%type;
      ecrlibelle    ecriture.ecr_libelle%type;
      oriordre      mandat.ori_ordre%type;
      ecrordre      ecriture.ecr_ordre%type;
      ecdordre      ecriture_detail.ecd_ordre%type;
      mdeordre      mandat_detail_ecriture.mde_ordre%type;
   begin
      check_mandat_avant_visa (manid);

      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = lemandat.bor_id;

      select ori_ordre
      into   oriordre
      from   mandat
      where  man_id = lemandat.man_id;

      -- creer l'ecriture
      ecrlibelle := '[' || ecdlibelleprefix || '] ' || creer_ecr_libelle (lemandat, lebordereau);
      ecrordre := api_plsql_journal.creerecritureexercicetype (1, datevisa, ecrlibelle, lemandat.exe_ordre, oriordre, 4, utlordre, 4);

      -- creer les ecriture_details
      open mandatbrouillards;

      loop
         fetch mandatbrouillards
         into  lemab;

         exit when mandatbrouillards%notfound;
         ecdlibelle := '[' || ecdlibelleprefix || '] ' || creer_ecd_libelle (lemab, lemandat, lebordereau);

         -- verifier que le compte existe et est valide
         if (api_planco.is_planco_valide (lemab.pco_num, lemandat.exe_ordre) = 0) then
            raise_application_error (-20001,
                                     'Le compte (' || lemab.pco_num || ') associé au mandat ' || lemandat.ges_code || '/' || lemandat.man_numero || ' (man_id ' || manid || ') n''existe pas ou n''est pas valide pour l''exercice ' || lemandat.exe_ordre
                                    );
         end if;

         ecdordre := api_plsql_journal.creerecrituredetail (null, ecdlibelle, lemab.mab_montant, null, lemab.mab_sens, ecrordre, lemab.ges_code, lemab.pco_num);

         select mandat_detail_ecriture_seq.nextval
         into   mdeordre
         from   dual;

         -- Insertion des mandat_detail_ecritures
         insert into mandat_detail_ecriture
         values      (ecdordre,
                      lemab.exe_ordre,
                      lemab.man_id,
                      sysdate,
                      mdeordre,
                      'VISA',
                      oriordre
                     );
      end loop;

      close mandatbrouillards;

      -- mettre à jour l''etat du mandat
      update mandat
         set man_etat = 'VISE',
             man_date_remise = datevisa
       where man_id = manid;

      -- verifier et numeroter l'ecriture
      maracuja.api_plsql_journal.validerecriture (ecrordre);
   end;

   procedure check_mandat_avant_visa (manid mandat.man_id%type)
   is
      lemandat   mandat%rowtype;
      flag       integer;
   begin
      select count (*)
      into   flag
      from   mandat
      where  man_id = manid;

      if (flag = 0) then
         raise_application_error (-20001, 'Mandat man_id ' || manid || ' non trouve');
      end if;

      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      -- verifier mandat non vise (attente)
      if (lemandat.man_etat <> 'ATTENTE') then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') n''est pas à l''etat ATTENTE. (' || lemandat.man_etat || ').');
      end if;

      -- verifie mandat non sur bordereau rejet
      if (lemandat.brj_ordre is not null) then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') est place sur un bordereau de rejet (brj_ordre=' || lemandat.brj_ordre || ').');
      end if;

      -- verifier qu'il n'y a pas deja des ecritures associées au mandat
      select count (*)
      into   flag
      from   mandat_detail_ecriture
      where  man_id = manid;

      if (flag > 0) then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') a deja des ecritures associees (table mandat_detail_ecriture).');
      end if;

      -- verifier qu'il y a des brouillards
      select count (*)
      into   flag
      from   mandat_brouillard
      where  man_id = manid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') n'' a pas de brouillards associés (table mandat_brouillard).');
      end if;
   end;

   function creer_ecd_libelle (mab mandat_brouillard%rowtype, lemandat mandat%rowtype, lebordereau bordereau%rowtype)
      return ecriture_detail.ecd_libelle%type
   is
      res              ecriture_detail.ecd_libelle%type;
      lesoustype       type_bordereau.tbo_sous_type%type;
      nomfournisseur   varchar2 (250);
      typemandat       varchar2 (5);
   begin
      select tbo_sous_type,
             decode (tbo_sous_type, 'REVERSEMENT', 'Orv', 'Md.')
      into   lesoustype,
             typemandat
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      select trim (nom || ' ' || nvl (prenom, ''))
      into   nomfournisseur
      from   v_fournis_light
      where  fou_ordre = lemandat.fou_ordre;

      res := lesoustype || ' ' || nomfournisseur || ' ' || typemandat || ' ' || lemandat.man_numero || ' Bord. ' || lebordereau.bor_num || ' du ' || lebordereau.ges_code || ' ' || lemandat.exe_ordre;
      return res;
   end;

   function creer_ecr_libelle (lemandat mandat%rowtype, lebordereau bordereau%rowtype)
      return ecriture.ecr_libelle%type
   is
      res          ecriture_detail.ecd_libelle%type;
      lesoustype   type_bordereau.tbo_sous_type%type;
      typemandat   varchar2 (5);
   begin
      select tbo_sous_type,
             decode (tbo_sous_type, 'REVERSEMENT', 'Orv', 'Md.')
      into   lesoustype,
             typemandat
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      res := typemandat || ' Num. ' || lemandat.man_numero || ' Bord. Num. ' || lebordereau.bor_num;
      return res;
   end;
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.extourne
is
   -- creer le bordereau de mandats d'extourne (ainsi que les depenses sur jefy_depense) a partir du bordereau de mandats a extourner
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type)
   is
      bordereau_n     bordereau%rowtype;
      mandat_n        mandat%rowtype;
      depense_n       depense%rowtype;
      flag            integer;
      depidn          jefy_depense.depense_budget.dep_id%type;
      depidn1         jefy_depense.depense_budget.dep_id%type;

      cursor c_mandat_n
      is
         select   m.*
         from     mandat m, mode_paiement mp
         where    m.mod_ordre = mp.mod_ordre and m.bor_id = bordereau_n.bor_id and mod_dom = c_mod_dom_a_extourner and m.man_etat = 'VISE'
         order by ges_code, man_numero;

      cursor c_depense_n
      is
         select d.*
         from   depense d
         where  man_id = mandat_n.man_id;

      cursor c_depensectrlplanco_n1
      is
         select dpco.*
         from   jefy_depense.depense_ctrl_planco dpco
         where  dep_id = depidn1;

      chainedpcoids   varchar2 (30000);
      exeordren1      jefy_admin.exercice.exe_ordre%type;
      res             integer;
      manidn1         mandat.man_id%type;
   begin
      select *
      into   bordereau_n
      from   bordereau
      where  bor_id = boridn;

      exeordren1 := bordereau_n.exe_ordre + 1;

      -- verifier que le bordereau d'origine des mandats avec mode de paiement extourne
      if (is_bord_aextourner (boridn) = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner".');
      end if;

      -- verifier que le bordereau d'origine est visé
      if (bordereau_n.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau initial (bord_id=' || boridn || ' n''est pas vise.');
      end if;

      -- verifier qu'au moins un mandats a extourner du bordereau d'origine sont vises
      select count (*)
      into   flag
      from   mandat m, mode_paiement mp
      where  m.mod_ordre = mp.mod_ordre and bor_id = boridn and mod_dom = c_mod_dom_a_extourner and m.man_etat = 'VISE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner" qui soitr VISE.');
      end if;

      -- verifier que l'exercice n+1 existe
      if (is_exercice_existe (exeordren1) = 0) then
         raise_application_error (-20001, 'L''exercice ' || (exeordren1) || ' n''existe pas, impossible de réserver des crédts d''extourne. Créez l''exercice en question ou attendez qu''il soit créé pour viser le bordereau initial.');
      end if;

      -- creer bordereau sur N+1
      boridn1 := bordereau_abricot.get_num_borid (get_tboordre_mandat_ext, exeordren1, bordereau_n.ges_code, bordereau_n.utl_ordre);

      -- créer chaque dépense négative sur n+1 correspondant à chaque dépense de n (intégrée dans un mandat d'extourne visé)
      open c_mandat_n;

      loop
         fetch c_mandat_n
         into  mandat_n;

         exit when c_mandat_n%notfound;
         chainedpcoids := '';

         open c_depense_n;

         loop
            fetch c_depense_n
            into  depense_n;

            exit when c_depense_n%notfound;
            -- creer la depense negative jefy_depense sur N+1
            depidn1 := prv_creer_jdep_n1 (depense_n);

            -- memoriser lien dep_idn/dep_idn1
            select dep_id
            into   depidn
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = depense_n.dep_ordre;

            insert into jefy_depense.extourne_liq
                        (el_id,
                         dep_id_n,
                         dep_id_n1
                        )
               select jefy_depense.extourne_liq_seq.nextval,
                      depidn,
                      depidn1
               from   dual;

            chainedpcoids := chainedpcoids || prv_chaine_dpcoids (depidn1) || '$';
         end loop;

         close c_depense_n;

         chainedpcoids := chainedpcoids || '$';
         -- creer mandat et brouillards sur N+1
         manidn1 := bordereau_abricot.set_mandat_depenses (chainedpcoids, boridn1);

         -- memoriser le lien man_idn/man_idn1
         insert into maracuja.extourne_mandat
                     (em_id,
                      man_id_n,
                      man_id_n1
                     )
            select maracuja.extourne_mandat_seq.nextval,
                   mandat_n.man_id,
                   manidn1
            from   dual;

         -- creer les depenses Maracuja sur N+1
         bordereau_abricot.get_depense_jefy_depense (manidn1);
         -- brouillard du mandat
         prv_creer_brouillard_inverse (mandat_n.man_id, manidn1);
      end loop;

      close c_mandat_n;

      bordereau_abricot.numeroter_bordereau (boridn1);
      bordereau_abricot.controle_bordereau (boridn1);
--   exception
--      when others then
--         raise_application_error (-20001, '[Creer Mandats Extourne]' || sqlerrm);
   end;

   -- renvoi >0 si le bordereau contient des mandats non rejetes passes sur un mode de paiement à extourner.
   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   mandat m inner join mode_paiement mp on (m.mod_ordre = mp.mod_ordre)
      where  bor_id = borid and mod_dom = c_mod_dom_a_extourner and m.brj_ordre is null;

      return cpt;
   end;

   function is_exercice_existe (exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   jefy_admin.exercice
      where  exe_ordre = exeordre;

      return cpt;
   end;

   -- creation de la depense jefy_depense sur N+1 a partir de la depense maracuja sur N
   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type
   is
      a_dep_id                number;
      a_exe_ordre             number;
      a_dpp_id                number;
      a_dep_ht_saisie         number;
      a_dep_ttc_saisie        number;
      a_fou_ordre             number;
      a_org_id                number;
      a_tcd_ordre             number;
      a_tap_id                number;
      a_eng_libelle           varchar2 (500);
      a_tyap_id               number;
      a_utl_ordre             number;
      a_dep_id_reversement    number;
      a_mod_ordre             number;
      a_chaine_action         varchar2 (30000);
      a_chaine_analytique     varchar2 (30000);
      a_chaine_convention     varchar2 (30000);
      a_chaine_hors_marche    varchar2 (30000);
      a_chaine_marche         varchar2 (30000);
      a_chaine_planco         varchar2 (30000);
      jdepense_ctrl_plancon   jefy_depense.depense_ctrl_planco%rowtype;
      jdepense_budgetn        jefy_depense.depense_budget%rowtype;
      jengage_budgetn         jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   jdepense_ctrl_plancon
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = depense_n.dep_ordre and man_id = depense_n.man_id;

      select *
      into   jdepense_budgetn
      from   jefy_depense.depense_budget
      where  dep_id = jdepense_ctrl_plancon.dep_id;

      select *
      into   jengage_budgetn
      from   jefy_depense.engage_budget
      where  eng_id = jdepense_budgetn.eng_id;

      a_dep_id := null;
      a_exe_ordre := jdepense_ctrl_plancon.exe_ordre + 1;
      a_dpp_id := null;
      a_dep_ht_saisie := -jdepense_ctrl_plancon.dpco_ht_saisie;
      a_dep_ttc_saisie := -jdepense_ctrl_plancon.dpco_ttc_saisie;
      a_fou_ordre := depense_n.fou_ordre;
      a_org_id := get_orgid_extourne (depense_n);
      a_tcd_ordre := get_tcdordre_extourne (depense_n);
      a_tap_id := jdepense_budgetn.tap_id;
      a_eng_libelle := substr ('Extourne : ' || jengage_budgetn.eng_libelle, 1, 500);
      a_tyap_id := jengage_budgetn.tyap_id;
      a_utl_ordre := jdepense_budgetn.utl_ordre;
      a_dep_id_reversement := null;
      -- on ne recupere pas les actions
      a_chaine_action := '$';
      -- on ne recupere pas les codes nomenclature
      a_chaine_hors_marche := '$';
      a_chaine_analytique := prv_chaine_analytique_dep (jdepense_budgetn.dep_id);
      a_chaine_convention := prv_chaine_convention_dep (jdepense_budgetn.dep_id);
      a_chaine_marche := prv_chaine_marche_dep (jdepense_budgetn.dep_id);
      a_chaine_planco := prv_chaine_planco_dep (jdepense_budgetn.dep_id);
      a_mod_ordre := get_modordre_extourne (depense_n);
      jefy_depense.reverser.ins_reverse_papier (a_dpp_id, a_exe_ordre, a_eng_libelle, a_dep_ht_saisie, a_dep_ttc_saisie, a_fou_ordre, null, a_mod_ordre, sysdate, sysdate, sysdate, sysdate, 1, a_utl_ordre, null);
      jefy_depense.liquider.ins_depense_directe (a_dep_id,
                                                 a_exe_ordre,
                                                 a_dpp_id,
                                                 a_dep_ht_saisie,
                                                 a_dep_ttc_saisie,
                                                 a_fou_ordre,
                                                 a_org_id,
                                                 a_tcd_ordre,
                                                 a_tap_id,
                                                 a_eng_libelle,
                                                 a_tyap_id,
                                                 a_utl_ordre,
                                                 a_dep_id_reversement,
                                                 a_chaine_action,
                                                 a_chaine_analytique,
                                                 a_chaine_convention,
                                                 a_chaine_hors_marche,
                                                 a_chaine_marche,
                                                 a_chaine_planco
                                                );
      return a_dep_id;
   end;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine               varchar2 (30000);
      chaine_inventaires   varchar2 (30000);
      repart               jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.pco_num || '$-' || repart.dpco_ht_saisie || '$-' || repart.dpco_ttc_saisie || '$' || repart.ecd_ordre || '$';
         -- FIXME recuperer les inventaires ????
         chaine_inventaires := '||';
         chaine := chaine || chaine_inventaires || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_action%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_action
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.tyac_id || '$-' || repart.dact_ht_saisie || '$-' || repart.dact_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_analytique%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_analytique
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.can_id || '$-' || repart.dana_ht_saisie || '$-' || repart.dana_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_convention%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_convention
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.conv_ordre || '$-' || repart.dcon_ht_saisie || '$-' || repart.dcon_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_hors_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.typa_id || '$' || repart.ce_ordre || '$-' || repart.dhom_ht_saisie || '$-' || repart.dhom_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.att_ordre || '$-' || repart.dmar_ht_saisie || '$-' || repart.dmar_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.dpco_id || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   -- creer un brouillard inverse a celui d'origine
   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type)
   is
   begin
      -- inverser le sens et l'affecter au mandat d'extourne
      delete from mandat_brouillard
            where man_id = manidn1;

      insert into maracuja.mandat_brouillard
                  (ecr_ordre,
                   exe_ordre,
                   ges_code,
                   mab_montant,
                   mab_operation,
                   mab_ordre,
                   mab_sens,
                   man_id,
                   pco_num
                  )
         select null,
                exe_ordre + 1,
                ges_code,
                mab_montant,
                mab_operation,
                mandat_brouillard_seq.nextval,
                decode (mab_sens, 'C', 'D', 'C'),
                manidn1,
                pco_num
         from   mandat_brouillard
         where  man_id = manidn;
   end;

   -- viser automatiquement le bordereau de mandats d'extourne en entier
   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type)
   is
      lemandat   mandat%rowtype;
      flag       integer;

      cursor lesmandats
      is
         select *
         from   mandat
         where  bor_id = boridn1;
   begin
      -- verifier que le bordereau existe
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = boridn1;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' n''existe pas.');
      end if;

      -- verifier que le bordereau  est valide
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = boridn1 and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' n''est pas à l''etat VALIDE.');
      end if;

      -- verifier s'il y a des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = boridn1;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' ne contient pas de mandats.');
      end if;

      -- viser chaque mandat
      open lesmandats;

      loop
         fetch lesmandats
         into  lemandat;

         exit when lesmandats%notfound;

         -- verifier que le mandat est bien un madat d''extourne
         select count (*)
         into   flag
         from   mandat m, extourne_mandat em
         where  m.man_id = em.man_id_n1 and m.man_id = lemandat.man_id;

         if (flag = 0) then
            raise_application_error (-20001, 'Le mandat man_id ' || lemandat.man_id || ' n''est pas un mandat d''extourne.');
         end if;

         api_mandat.viser_mandat (lemandat.man_id, datevisa, utlordre, 'Extourne');
      end loop;

      close lesmandats;

      -- mettre a jour le bordereau
      update bordereau
         set bor_etat = 'VISE',
             bor_date_visa = datevisa,
             utl_ordre_visa = utlordre
       where bor_id = boridn1;
--   exception
--      when others then
--         raise_application_error (-20001, '[Viser Mandats Extourne] ' || sqlerrm);
   end;

   function get_tboordre_mandat_ext
      return type_bordereau.tbo_ordre%type
   is
   begin
      return 50;
   end;

   -- renvoie le mod_ordre pour le mandat d'extourne
   function get_modordre_extourne (ladepensen maracuja.depense%rowtype)
      return mode_paiement.mod_ordre%type
   is
      modordren1      mode_paiement.mod_ordre%type;
      modepaiementn   mode_paiement%rowtype;
      flag            integer;
   begin
      modordren1 := null;

      select *
      into   modepaiementn
      from   mode_paiement
      where  mod_ordre = ladepensen.mod_ordre;

      select count (*)
      into   flag
      from   mode_paiement
      where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_code = (modepaiementn.mod_code) and mod_validite = 'VALIDE';

      if (flag > 0) then
         select mod_ordre
         into   modordren1
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_code = (modepaiementn.mod_code) and mod_validite = 'VALIDE';
      end if;

      -- si le mode ed paiement identique n'est pâs retrouvé sur n+1, on prend un mode de paiement interne valide au hasard
      if (modordren1 is null) then
         select count (*)
         into   flag
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_validite = 'VALIDE' and mod_dom = 'INTERNE';

         if (flag = 0) then
            raise_application_error (-20001, 'Aucun mode de paiement interne trouvé sur  ' || (modepaiementn.exe_ordre + 1) || ' pour l''affectation au mandat d''extourne.');
         end if;

         select max (mod_ordre)
         into   modordren1
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_validite = 'VALIDE' and mod_dom = 'INTERNE';
      end if;

      return modordren1;
   end;

   -- renvoie le tcd_ordre pour le mandat d'extourne
   function get_tcdordre_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.type_credit.tcd_ordre%type
   is
      tcdordren1    jefy_admin.type_credit.tcd_ordre%type;
      typecreditn   jefy_admin.type_credit%rowtype;
      flag          integer;
   begin
      tcdordren1 := null;

      select *
      into   typecreditn
      from   jefy_admin.type_credit
      where  tcd_ordre = ladepensen.tcd_ordre;

      select count (*)
      into   flag
      from   jefy_admin.type_credit
      where  exe_ordre = (ladepensen.exe_ordre + 1) and tcd_code = (typecreditn.tcd_code) and tcd_type = typecreditn.tcd_type and tyet_id = 1;

      if (flag > 0) then
         select tcd_ordre
         into   tcdordren1
         from   jefy_admin.type_credit
         where  exe_ordre = (ladepensen.exe_ordre + 1) and tcd_code = (typecreditn.tcd_code) and tcd_type = typecreditn.tcd_type and tyet_id = 1;
      end if;

      -- si le mode ed paiement identique n'est pâs retrouvé sur n+1, on prend un mode de paiement interne valide au hasard
      if (tcdordren1 is null) then
         raise_application_error (-20001, 'Le type de crédit ' || (typecreditn.tcd_code) || ' n''est pas valide pour l''exercice ' || (typecreditn.exe_ordre + 1) || '. Impossible de créer les crédits d''extourne.');
      end if;

      return tcdordren1;
   end;

   -- renvoie le tcd_ordre pour le mandat d'extourne
   function get_orgid_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.organ.org_id%type
   is
      orgid1       jefy_admin.organ.org_id%type;
      organn       jefy_admin.organ%rowtype;
      organn1      jefy_admin.organ%rowtype;
      flag         integer;
      datedebut1   date;
   begin
      orgid1 := null;
      datedebut1 := to_date ('01/01/' || (ladepensen.exe_ordre + 1), 'dd/mm/yyyy');

      select *
      into   organn
      from   jefy_admin.organ
      where  org_id = ladepensen.org_ordre;

      -- si la ligne est fermée sur l'exercice n+1 on prend la ligne de reprise
      if (organn.org_date_cloture is not null and organn.org_date_cloture < datedebut1) then
         orgid1 := organn.org_id_reprise;

         if (orgid1 is null) then
            raise_application_error (-20001,
                                        'La ligne budgétaire sur laquelle a été effectuée la liquidation initiale ( '
                                     || organn.org_etab
                                     || organn.org_ub
                                     || '/'
                                     || organn.org_cr
                                     || '/'
                                     || organn.org_souscr
                                     || ') n''est pas ouverte pour l''exercice '
                                     || (ladepensen.exe_ordre + 1)
                                     || ', et aucune ligne de reprise n''a été définie. Impossible de réserver les crédits d''extourne. Solution : ouvrez la ligne pour le nouvel exercice ou indiquez une ligne de reprise.'
                                    );
         end if;

         select *
         into   organn1
         from   jefy_admin.organ
         where  org_id = orgid1;

         if ((organn1.org_date_cloture is not null and organn1.org_date_cloture < datedebut1) or organn1.org_date_ouverture > datedebut1) then
            raise_application_error (-20001,
                                        'La ligne budgétaire sur laquelle a été effectuée la liquidation initiale ( '
                                     || organn.org_etab
                                     || organn.org_ub
                                     || '/'
                                     || organn.org_cr
                                     || '/'
                                     || organn.org_souscr
                                     || ') a une ligne de reprise définie ( '
                                     || organn1.org_etab
                                     || organn1.org_ub
                                     || '/'
                                     || organn1.org_cr
                                     || '/'
                                     || organn1.org_souscr
                                     || ') mais celle-ci n''est pas ouverte pour l''exercice '
                                     || (ladepensen.exe_ordre + 1)
                                     || '. Impossible de réserver les crédits d''extourne. Solution : ouvrez la ligne pour le nouvel exercice ou indiquez une ligne de reprise ouverte.'
                                    );
         end if;
      else
         orgid1 := organn.org_id;
      end if;

      return orgid1;
   end;

   -- s'agit-il d'une liquidation définitive sur crédit d'extourne ?
   function is_dpco_extourne_def (dpcoid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.extourne_liq_def eld on (dpco.dep_id = eld.dep_id)
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   -- s'agit-il d'une liquidation d'extourne ?
   function is_dpco_extourne (dpcoid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.extourne_liq el on (dpco.dep_id = el.dep_id_n1)
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   -- renvoie le montant budgétaire calculé de la liquidation
   function calcul_montant_budgetaire (dpcoid jefy_depense.depense_ctrl_planco.dpco_id%type)
      return jefy_depense.depense_ctrl_planco.dpco_montant_budgetaire%type
   as
      db     jefy_depense.depense_budget%rowtype;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;
      eb     jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   dpco
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select *
      into   db
      from   jefy_depense.depense_budget
      where  dep_id = dpco.dep_id;

      select *
      into   eb
      from   jefy_depense.engage_budget
      where  eng_id = db.eng_id;

      return jefy_depense.budget.calculer_budgetaire (db.exe_ordre, db.tap_id, eb.org_id, dpco.dpco_ht_saisie, dpco.dpco_ttc_saisie);
   end;
end extourne;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.util
is
   -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
   -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

   -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
   procedure annuler_visa_bor_mandat (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, mandat_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME' and tbotype <> 'BTRU') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME ou BTRU');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre=ecd.ecd_ordre  and mde.mde_origine not like 'VISA%';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandats associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from mandat_detail_ecriture
               where man_id = tmpmanid;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
--
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

      --                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

      --                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure annuler_visa_bor_titre (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      titre.tit_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmpmanid;

         exit when lestitres%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from titre_detail_ecriture
               where tit_id = tmpmanid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmpmanid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
   procedure supprimer_visa_btme (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre=ecd.ecd_ordre  and mde.mde_origine not like 'VISA%';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         select count (*)
         into   flag
         from   ecriture_detail ecd, mandat_detail_ecriture mde
         where  ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

         if (flag > 0) then
            select distinct ecr_ordre
            into            tmpecrordre
            from            ecriture_detail ecd, mandat_detail_ecriture mde
            where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

            delete from ecriture_detail
                  where ecr_ordre = tmpecrordre;

            delete from ecriture
                  where ecr_ordre = tmpecrordre;

            delete from mandat_detail_ecriture
                  where man_id = tmpmanid;
         end if;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTMS
   procedure supprimer_visa_btms (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesecritures
      is
         select distinct ed.ecr_ordre
         from            ecriture_detail ed, mandat_detail_ecriture mde, mandat m
         where           m.man_id = mde.man_id and mde.ecd_ordre = ed.ecd_ordre and m.bor_id = borid;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTMS') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTMS');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat = 'VALIDE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

      --            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;
      open lesecritures;

      loop
         fetch lesecritures
         into  tmpecrordre;

         exit when lesecritures%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre in (select ecd_ordre
                                   from   ecriture_detail
                                   where  ecr_ordre = tmpecrordre);

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;
      end loop;

      close lesecritures;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;

      update jefy_paye.jefy_paye_compta
         set ecr_ordre_visa = null,
             ecr_ordre_sacd = null,
             ecr_ordre_opp = null
       where bor_id in (borid);
   end;

   -- annulation du visa d'un bordereau de depense BTTE
   procedure supprimer_visa_btte (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmptitid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmptitid;

         exit when lestitres%notfound;

         select distinct ecr_ordre
         into            tmpecrordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmptitid;

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;

         delete from titre_detail_ecriture
               where tit_id = tmptitid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmptitid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure creer_ecriture_annulation (ecrordre integer)
   is
      ecr           maracuja.ecriture%rowtype;
      ecd           maracuja.ecriture_detail%rowtype;
      flag          integer;
      str           ecriture.ecr_libelle%type;
      newecrordre   ecriture.ecr_ordre%type;
      newecdordre   ecriture_detail.ecd_ordre%type;
      x             integer;

      cursor c1
      is
         select *
         from   maracuja.ecriture_detail
         where  ecr_ordre = ecrordre;
   begin
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucune ecriture retrouvee pour ecr_ordre= ' || ecrordre || ' .');
      end if;

      select *
      into   ecr
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      str := 'Annulation Ecriture ' || ecr.exe_ordre || '/' || ecr.ecr_numero;

      -- verifier que l'ecriture n'a pas deja ete annulee
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_libelle = str;

      if (flag > 0) then
         select ecr_numero
         into   flag
         from   maracuja.ecriture
         where  ecr_libelle = str;

         raise_application_error (-20001, 'L''ecriture numero ' || ecr.ecr_numero || ' (ecr_ordre= ' || ecrordre || ') a deja ete annulee par l''ecriture numero ' || flag || '.');
      end if;

      -- verifier que les details ne sont pas emarges
      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;

         if (ecd.ecd_reste_emarger < abs (ecd.ecd_montant)) then
            raise_application_error (-20001, 'L''ecriture ' || ecr.ecr_numero || ' ecr_ordre= ' || ecrordre || ' a ete emargee pour le compte ' || ecd.pco_num || '. Impossible d''annuler');
         end if;
      end loop;

      close c1;

--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--
--

      --             END LOOP;
--             CLOSE c1;
      newecrordre := api_plsql_journal.creerecriture (ecr.com_ordre, sysdate, str, ecr.exe_ordre, ecr.ori_ordre, ecr.tjo_ordre, ecr.top_ordre, ecr.utl_ordre);

      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;
         newecdordre := api_plsql_journal.creerecrituredetail (ecd.ecd_commentaire, ecd.ecd_libelle, -ecd.ecd_montant, ecd.ecd_secondaire, ecd.ecd_sens, newecrordre, ecd.ges_code, ecd.pco_num);
         x := creeremargementmemesens (ecd.ecd_ordre, newecdordre, 1);
      end loop;

      close c1;

      numerotationobject.numeroter_ecriture (newecrordre);
   end;

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer
   is
   begin
      return api_emargement.creeremargementmemesens (ecdordresource, ecdordredest, typeemargement);
   end;

--
--       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
--            emaordre EMARGEMENT.EMA_ORDRE%type;
--            ecdSource ecriture_detail%rowtype;
--            ecdDest ecriture_detail%rowtype;
--
--            utlOrdre ecriture.utl_ordre%type;
--            comOrdre ecriture.com_ordre%type;
--            exeOrdre ecriture.exe_ordre%type;
--            emaMontant emargement.ema_montant%type;
--            flag integer;
--       BEGIN

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
--            end if;

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
--            end if;

   --            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

   --            -- verifier que les ecriture_detail sont sur le meme sens
--            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;
--
--            -- verifier que les ecriture_detail ont le meme compte
--            if (ecdSource.pco_num <> ecdDest.pco_num) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les ecriture_detail ne sont pas emargees
--            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
--            end if;
--            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
--            end if;

   --            -- verifier que les montant s'annulent
--            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
--                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les exercices sont les memes
--            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
--                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- trouver le montant a emarger
--            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;

   --            -- trouver l'utilisateur
--            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;

   --            -- creation de l emargement
--             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

   --             INSERT INTO EMARGEMENT
--              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
--             VALUES
--              (
--              SYSDATE,
--              0,
--              EMAORDRE,
--              ecdSource.exe_ordre,
--              typeEmargement,
--              utlOrdre,
--              comordre,
--              emaMontant,
--              'VALIDE'
--              );

   --              -- creation de l emargement detail
--              INSERT INTO EMARGEMENT_DETAIL
--              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
--              VALUES
--              (
--              ecdSource.ecd_ordre,
--              ecdDest.ecd_ordre,
--              EMAORDRE,
--              emaMontant,
--              emargement_detail_seq.NEXTVAL,
--              exeOrdre
--              );

   --              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
--              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;

   --              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);

   --              return emaOrdre;
--       END;
--
--
   procedure annuler_emargement (emaordre integer)
   as
   begin
      api_emargement.annuler_emargement (emaordre);
   end;

   procedure supprimer_bordereau_dep (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de depense.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les mandat_brouillards
      delete from maracuja.mandat_brouillard
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- supprimer les depenses
      delete from maracuja.depense
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- mettre a jour les depense_ctrl_planco
      update jefy_depense.depense_ctrl_planco
         set man_id = null
       where man_id in (select man_id
                        from   maracuja.mandat
                        where  bor_id = borid);

      -- supprimer les mandats
      delete from maracuja.mandat
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   procedure supprimer_bordereau_btms (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau b, type_bordereau tb
      where  bor_id = borid and b.tbo_ordre = tb.tbo_ordre and tb.tbo_type = 'BTMS';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas ou n''est pas de type BTMS');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      update jefy_paye.jefy_paye_compta
         set jpc_etat = 'LIQUIDEE',
             bor_id = null
       where bor_id in (borid);

      --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
      supprimer_bordereau_dep (borid);
   end;

   procedure supprimer_bordereau_rec (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de recette.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les titre_brouillards
      delete from maracuja.titre_brouillard
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- supprimer les recettes
      delete from maracuja.recette
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- mettre a jour les recette_ctrl_planco
      update jefy_recette.recette_ctrl_planco
         set tit_id = null
       where tit_id in (select tit_id
                        from   maracuja.titre
                        where  bor_id = borid);

      -- supprimer les titres
      delete from maracuja.titre
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer)
   is
      flag             integer;
      lerecouvrement   recouvrement%rowtype;
      tmpemaordre      emargement.ema_ordre%type;
      tmpecrordre      ecriture.ecr_ordre%type;

      cursor emargements
      is
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_source in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))))
         union
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_destination in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))));

      cursor ecritures
      is
         select distinct e.ecr_ordre
         from            ecriture_detail ecd, ecriture e
         where           ecd.ecr_ordre = e.ecr_ordre and e.ecr_numero >= ecrnumeromin and e.ecr_numero <= ecrnumeromax and ecd_ordre in (select ecd_ordre
                                                                                                                                         from   titre_detail_ecriture
                                                                                                                                         where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                                                                                                           from   echeancier
                                                                                                                                                                                           where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                                                                                                            from   prelevement
                                                                                                                                                                                                                            where  reco_ordre = recoordre)));
   begin
      -- recuperer les numeros des ecritures min et max pour les passer en parametre
      --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
      select count (*)
      into   flag
      from   prelevement
      where  reco_ordre = recoordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun prelevement correspondant a recoordre= ' || recoordre);
      end if;

      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- verifier les dates des ecritures avec la date du prelevement
      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromin;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromax;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      -- supprimer les emargements
      open emargements;

      loop
         fetch emargements
         into  tmpemaordre;

         exit when emargements%notfound;
         annuler_emargement (tmpemaordre);
      end loop;

      close emargements;

      -- modifier les etats des prelevements
      update prelevement
         set prel_etat_maracuja = 'ATTENTE'
       where reco_ordre = recoordre;

      --supprimer le contenu du fichier (prelevement_fichier)
      delete from prelevement_fichier
            where reco_ordre = recoordre;

      -- supprimer les écritures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
      open ecritures;

      loop
         fetch ecritures
         into  tmpecrordre;

         exit when ecritures%notfound;
         creer_ecriture_annulation (tmpecrordre);
      end loop;

      close ecritures;

      delete from titre_detail_ecriture
            where ecd_ordre in (
                         select distinct ecd.ecd_ordre
                         from            ecriture_detail ecd, ecriture e
                         where           ecd.ecr_ordre = e.ecr_ordre
                         and             e.ecr_numero >= ecrnumeromin
                         and             e.ecr_numero <= ecrnumeromax
                         and             ecd_ordre in (select ecd_ordre
                                                       from   titre_detail_ecriture
                                                       where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                         from   echeancier
                                                                                                         where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                          from   prelevement
                                                                                                                                          where  reco_ordre = recoordre))));

      -- supprimer association des prélèvements au recouvrement
      update prelevement
         set reco_ordre = null
       where reco_ordre = recoordre;

      -- supprimer l'objet recouvrement
      delete from recouvrement
            where reco_ordre = recoordre;
--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))

   --        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--
   end;

   -- suppression d'un paiement de type virement avec annulation des émargements et écritures de paiements
   procedure supprimer_paiement_virement (paiordre integer)
   is
      flag       integer;
      ecdp       ecriture_detail%rowtype;
      ecdordre   ecriture_detail.ecd_ordre%type;
      emaordre   emargement.ema_ordre%type;
      ecrordre   ecriture.ecr_ordre%type;

      cursor ecdpaiement5
      is
         select ecd.*
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.man_id = mde.man_id
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    mde_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.*
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.odp_ordre = mde.odp_ordre
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    ope_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%';

      cursor ecdpaiementall
      is
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor ecrordres
      is
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor emaordreforecd
      is
         select emd.ema_ordre
         from   emargement_detail emd, emargement ema
         where  ema.ema_ordre = emd.ema_ordre and ema_etat = 'VALIDE' and ecd_ordre_source = ecdordre or ecd_ordre_destination = ecdordre;
   begin
      select count (*)
      into   flag
      from   maracuja.paiement
      where  pai_ordre = paiordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun paiement trouvé correspondant a pai_ordre= ' || paiordre);
      end if;

      --verifier que classe 5 non émargée
      open ecdpaiement5;

      loop
         fetch ecdpaiement5
         into  ecdp;

         exit when ecdpaiement5%notfound;

         if (ecdp.ecd_reste_emarger <> abs (ecdp.ecd_credit)) then
            raise_application_error (-20001, 'L''écriture ' || ecdp.pco_num || ' - ' || ecdp.ecd_libelle || ' a été émargée, supprimez l''émargement.');
         end if;
      end loop;

      close ecdpaiement5;

      -- supprimer tous les emargements
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         open emaordreforecd;

         loop
            fetch emaordreforecd
            into  emaordre;

            exit when emaordreforecd%notfound;
            api_emargement.annuler_emargement (emaordre);
         end loop;

         close emaordreforecd;
      end loop;

      close ecdpaiementall;

      --  creer ecritures annulation
      open ecrordres;

      loop
         fetch ecrordres
         into  ecrordre;

         exit when ecrordres%notfound;
         creer_ecriture_annulation (ecrordre);
      end loop;

      close ecrordres;

      -- supprimer les mde et ope
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre = ecdordre;

         delete from odpaiem_detail_ecriture
               where ecd_ordre = ecdordre;
      end loop;

      close ecdpaiementall;

      -- changer etats des mandats et ODP
      update mandat
         set man_etat = 'VISE',
             pai_ordre = null
       where pai_ordre = paiordre;

      update ordre_de_paiement
         set odp_etat = 'VALIDE',
             pai_ordre = null
       where pai_ordre = paiordre;

      -- supprimer le paiement
      delete from virement_fichier
            where pai_ordre = paiordre;

      delete from paiement
            where pai_ordre = paiordre;
   end;

   procedure corriger_etat_mandats
   is
   begin
      update maracuja.mandat
         set man_etat = 'VISE'
       where man_id in (select m.man_id
                        from   maracuja.mandat m, maracuja.mandat_detail_ecriture mde
                        where  m.man_id = mde.man_id and m.man_etat = 'ATTENTE' and mde.mde_origine = 'VISA');
   end;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type
   is
      flag     integer;
      pconum   plan_comptable_exer.pco_num%type;
   begin
      select count (*)
      into   flag
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' n''a pas été trouvé dans la table maracuja.parametre pour l''exercice ' || exeordre);
      end if;

      select par_value
      into   pconum
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      select count (*)
      into   flag
      from   plan_comptable_exer
      where  pco_num = pconum and exe_ordre = exeordre and pco_validite = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' contient la valeur ' || pconum || ' qui ne correspond pas a un compte valide du plan comptable de ' || exeordre);
      end if;

      return pconum;
   end;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   parametre
      where  exe_ordre = exeordre and par_key = parkey;

      if (flag = 0) then
         insert into parametre
                     (par_ordre,
                      exe_ordre,
                      par_description,
                      par_key,
                      par_value
                     )
         values      (parametre_seq.nextval,
                      exeordre,
                      pardescription,
                      parkey,
                      parvalue
                     );
      end if;
   end;

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat <> 'C';

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;
end;
/




create or replace procedure grhum.inst_patch_maracuja_1921
is
begin
	
	insert into maracuja.type_numerotation
	            (tnu_entite,
	             tnu_libelle,
	             tnu_ordre
	            )
	select      'BORDEREAU', 'BORDEREAU DE MANDATS D''EXTOURNE', 25 from dual where not exists (select * from maracuja.type_numerotation where tnu_ordre=25 and tnu_libelle='BORDEREAU DE MANDATS D''EXTOURNE');
	
	insert into maracuja.type_bordereau
	            (tbo_libelle,
	             tbo_ordre,
	             tbo_sous_type,
	             tbo_type,
	             tbo_type_creation,
	             tnu_ordre
	            )
	select      'BORDEREAU DE MANDATS D''EXTOURNE', 50, 'EXTOURNE', 'BTMEX', null, 25 from dual where not exists(select * from maracuja.type_bordereau where tbo_ordre=50 and tbo_libelle='BORDEREAU DE MANDATS D''EXTOURNE');
	commit ;
	
   jefy_admin.patch_util.end_patch (4, '1.9.2.1');
end;
/

