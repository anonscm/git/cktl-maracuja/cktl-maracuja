SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°3/3
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.1 BETA
-- Date de publication : 20/09/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
--
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;
	execute maracuja.inst_mp_extourne;
commit;
