begin
	 INSERT INTO maracuja.PARAMETRE (SELECT 2009, 'Effectuer des controles de doublons lors du paiement (OUI/NON)', 'CTRL_DOUBLONS_PAIEMENT', parametre_seq.NEXTVAL, 'OUI'
                             FROM MARACUJA.EXERCICE  WHERE exe_ordre=2009);
                             
     INSERT INTO maracuja.PARAMETRE (SELECT 2010, 'Effectuer des controles de doublons lors du paiement (OUI/NON)', 'CTRL_DOUBLONS_PAIEMENT', parametre_seq.NEXTVAL, 'OUI'
                             FROM MARACUJA.EXERCICE  WHERE exe_ordre=2010);
      commit;                                                   

---
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.7.9.1',  SYSDATE, '');
    commit;
end;