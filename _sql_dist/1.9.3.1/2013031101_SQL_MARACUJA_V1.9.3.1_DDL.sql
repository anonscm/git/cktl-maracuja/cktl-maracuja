set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schémas modifiés :  ACCORDS
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.3.1
-- Date de publication : 11/03/2013
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Installe la vue accords.v_convention_partenaire avec un script différent selon la version du user ACCORDS
-- (si la version 2.0.0 a été installée la vue est compatible avec Cocolight, sinon la vue est compatible avec Coconuts).
-- La vue sera également incluse dans un prochain patch d'ACCORDS
-- ce patch est obligatoire même si le patch 1.9.3.0 a bien été installé.
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.0', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.3.1', null );
commit ;

create or replace procedure grhum.inst_vue_accord_tmp
is
  isCocolight integer;
  vuecoconuts varchar2(4000);
  vuecommon varchar2(4000);
   vuecocolight varchar2(4000);

begin
  vuecommon := 'create or replace force view accords.v_convention_partenaire (pers_id_partenaire,ll_partenaire,pers_id_service,con_ordre, exe_ordre, con_index,con_etablissement,con_cr,con_nature, con_reference_externe,con_objet,con_objet_court,con_observations,tr_ordre,utl_ordre_creation,con_date_creation,utl_ordre_modif,con_date_modif,utl_ordre_valid_adm,con_date_valid_adm,con_date_cloture,con_date_apurement,con_groupe_bud,con_suppr,org_id_composante,tcc_id,avis_favorable,avis_defavorable,contexte,remarques,motifs_avis,c_naf,con_groupe_partenaire,con_duree,date_migration,con_duree_mois) as ';
    vuecocolight := vuecommon || ' /* version de la vue pour cocolight */ select s.pers_id as pers_id_partenaire,s.pers_libelle as ll_partenaire,s2.pers_id as pers_id_service,c.CON_ORDRE,c.EXE_ORDRE,c.CON_INDEX,c.CON_ETABLISSEMENT,c.CON_CR,c.CON_NATURE,c.CON_REFERENCE_EXTERNE,c.CON_OBJET,c.CON_OBJET_COURT,c.CON_OBSERVATIONS,c.TR_ORDRE,c.UTL_ORDRE_CREATION,c.CON_DATE_CREATION,c.UTL_ORDRE_MODIF,c.CON_DATE_MODIF,c.UTL_ORDRE_VALID_ADM,c.CON_DATE_VALID_ADM,c.CON_DATE_CLOTURE,c.CON_DATE_APUREMENT,c.CON_GROUPE_BUD,c.CON_SUPPR,c.ORG_ID_COMPOSANTE,c.TCC_ID,c.AVIS_FAVORABLE,c.AVIS_DEFAVORABLE,c.CONTEXTE,c.REMARQUES,c.MOTIFS_AVIS,c.C_NAF,c.CON_GROUPE_PARTENAIRE,c.CON_DUREE,c.DATE_MIGRATION,c.CON_DUREE_MOIS   from   accords.contrat c inner join accords.type_classification_contrat tcc on c.tcc_id = tcc.tcc_id left outer join accords.contrat_partenaire cp on cp.con_ordre = c.con_ordre and cp.cp_principal = ''O'' inner join grhum.personne s on cp.pers_id = s.pers_id left outer join grhum.structure_ulr s2 on c.con_cr = s2.c_structure   where  tcc.tcc_code = ''CONV''';
    vuecoconuts := vuecommon || ' /* version de la vue pour coconuts */  select s.pers_id as pers_id_partenaire,s.pers_libelle as ll_partenaire,s2.pers_id as pers_id_service,  c.CON_ORDRE,c.EXE_ORDRE,c.CON_INDEX,c.CON_ETABLISSEMENT,c.CON_CR,c.CON_NATURE,c.CON_REFERENCE_EXTERNE,c.CON_OBJET,c.CON_OBJET_COURT,c.CON_OBSERVATIONS,c.TR_ORDRE,c.UTL_ORDRE_CREATION,c.CON_DATE_CREATION,c.UTL_ORDRE_MODIF,c.CON_DATE_MODIF,c.UTL_ORDRE_VALID_ADM,c.CON_DATE_VALID_ADM,c.CON_DATE_CLOTURE,c.CON_DATE_APUREMENT,c.CON_GROUPE_BUD,c.CON_SUPPR,c.ORG_ID_COMPOSANTE,to_number(null) as TCC_ID, to_char(null) as AVIS_FAVORABLE,to_char(null) as AVIS_DEFAVORABLE,to_char(null) as CONTEXTE,to_char(null) as REMARQUES,to_char(null) as MOTIFS_AVIS,to_char(null) as C_NAF,to_char(null) as CON_GROUPE_PARTENAIRE,to_number(null) as CON_DUREE,to_date(null) as DATE_MIGRATION,to_number(null) as CON_DUREE_MOIS from accords.contrat c, grhum.personne s, grhum.structure_ulr s2,(select con_ordre, pers_id from accords.avenant a, accords.avenant_partenaire ap where a.avt_ordre = ap.avt_ordre (+) and ap.ap_principal = ''O'' and avt_suppr = ''N'') part where c.con_ordre = part.con_ordre (+) and part.pers_id = s.pers_id (+) and c.con_cr = s2.c_structure and c.con_suppr = ''N''';

    select count(*) into isCocolight 
    from accords.version_histo where vh_num ='2.0.0';


    if (isCocolight>0) then
    -- cocolight installe
        execute immediate vuecocolight;
    else
    -- cocolight non installe
        execute immediate vuecoconuts;
    end if;
end;
/

execute grhum.inst_vue_accord_tmp;
drop procedure grhum.inst_vue_accord_tmp;

grant select on accords.v_convention_partenaire to maracuja;

create or replace force view maracuja.v_accords_contrat (pers_id_partenaire,
                                                         ll_partenaire,
                                                         pers_id_service,
                                                         con_ordre,
                                                         exe_ordre,
                                                         con_index,
                                                         con_etablissement,
                                                         con_cr,
                                                         con_nature,
                                                         con_reference_externe,
                                                         con_objet,
                                                         con_objet_court,
                                                         con_observations,
                                                         tr_ordre,
                                                         utl_ordre_creation,
                                                         con_date_creation,
                                                         utl_ordre_modif,
                                                         con_date_modif,
                                                         utl_ordre_valid_adm,
                                                         con_date_valid_adm,
                                                         con_date_cloture,
                                                         con_date_apurement,
                                                         con_groupe_bud,
                                                         con_suppr,
                                                         org_id_composante,
                                                         tcc_id,
                                                         avis_favorable,
                                                         avis_defavorable,
                                                         contexte,
                                                         remarques,
                                                         motifs_avis,
                                                         c_naf,
                                                         con_groupe_partenaire,
                                                         con_duree,
                                                         date_migration,
                                                         con_duree_mois
                                                        )
as
select pers_id_partenaire,
                                                         ll_partenaire,
                                                         pers_id_service,
                                                         con_ordre,
                                                         exe_ordre,
                                                         con_index,
                                                         con_etablissement,
                                                         con_cr,
                                                         con_nature,
                                                         con_reference_externe,
                                                         con_objet,
                                                         con_objet_court,
                                                         con_observations,
                                                         tr_ordre,
                                                         utl_ordre_creation,
                                                         con_date_creation,
                                                         utl_ordre_modif,
                                                         con_date_modif,
                                                         utl_ordre_valid_adm,
                                                         con_date_valid_adm,
                                                         con_date_cloture,
                                                         con_date_apurement,
                                                         con_groupe_bud,
                                                         con_suppr,
                                                         org_id_composante,
                                                         tcc_id,
                                                         avis_favorable,
                                                         avis_defavorable,
                                                         contexte,
                                                         remarques,
                                                         motifs_avis,
                                                         c_naf,
                                                         con_groupe_partenaire,
                                                         con_duree,
                                                         date_migration,
                                                         con_duree_mois 
	from accords.v_convention_partenaire;
/	



create or replace procedure grhum.inst_patch_maracuja_1931
is
begin

   jefy_admin.patch_util.end_patch (4, '1.9.3.1');
end;
/

