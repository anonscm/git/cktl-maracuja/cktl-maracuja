
CREATE OR REPLACE PROCEDURE COMPTEFI."PREPARE_DETERMINATION_CAF" (exeordre number, agregatlib varchar2, methodeebe varchar2)
is
   total                number (12, 2);
   totalant             number (12, 2);
   lib                  varchar2 (100);
   lib1                 varchar2 (50);
   libant               varchar2 (50);
   total_produits       number (12, 2);
   total_produits_ant   number (12, 2);
   total_charges        number (12, 2);
   total_charges_ant    number (12, 2);
   formule              varchar2 (50);
   ebe                  number (12, 2);
   ebe_ant              number (12, 2);
   resultat             number (12, 2);
   resultat_ant         number (12, 2);
   cpt                  number;
begin
--*************** DETERMINATION A PARTIR DE EBE  *********************************
   if methodeebe = 'O' then
      delete      caf
            where exe_ordre = exeordre and ges_code = agregatlib and methode_ebe = 'O';

      total_produits := 0;
      total_charges := 0;
      total_produits_ant := 0;
      total_charges_ant := 0;
      formule := '';

      --**** RECUPERATION EBE *******
      select count (*)
      into   cpt
      from   sig
      where  ges_code = agregatlib and exe_ordre = exeordre and (groupe1 = 'Résultat d''exploitation' or groupe1 = 'Resultat d''exploitation') and commentaire = 'EBE';

      if cpt = 0 then
         raise_application_error (-20001, 'Vous devez au préalable calculer votre Excédent/Insuffisance Brute d''Exploitation à partir des Soldes Intermédaires de Gestion');
      end if;

      select nvl (sig_montant, 0),
             groupe2,
             sig_libelle,
             nvl (sig_montant_ant, 0),
             groupe_ant
      into   ebe,
             lib1,
             lib,
             ebe_ant,
             libant
      from   sig
      where  ges_code = agregatlib and exe_ordre = exeordre and (groupe1 = 'Résultat d''exploitation' or groupe1 = 'Resultat d''exploitation') and commentaire = 'EBE';

      if lib1 = 'charges' then
         ebe := -ebe;
      end if;

      if libant = 'charges' then
         ebe_ant := -ebe_ant;
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   ebe,
                   ebe_ant,
                   formule
                  );

      -- ********  Produits ***********
      lib1 := 'produits';
      total := resultat_compte_agr (exeordre, '75%', agregatlib) + resultat_compte_agr (exeordre, '1875%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '75%', agregatlib) + resultat_compte_agr (exeordre - 1, '1875%', agregatlib);
      lib := '+ Autres produits "encaissables" d''exploitation';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '791%', agregatlib) + resultat_compte_agr (exeordre, '18791%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '791%', agregatlib) + resultat_compte_agr (exeordre - 1, '18791%', agregatlib);
      lib := '+ Transferts de charges';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '76%', agregatlib) + resultat_compte_agr (exeordre, '796%', agregatlib) + resultat_compte_agr (exeordre, '1876%', agregatlib) + resultat_compte_agr (exeordre, '18796%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '76%', agregatlib) + resultat_compte_agr (exeordre, '796%', agregatlib) + resultat_compte_agr (exeordre - 1, '1876%', agregatlib) + resultat_compte_agr (exeordre, '18796%', agregatlib);
      lib := '+ Produits financiers "encaissables" (a)';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total :=
           resultat_compte_agr (exeordre, '771%', agregatlib)
         + resultat_compte_agr (exeordre, '778%', agregatlib)
         + resultat_compte_agr (exeordre, '797%', agregatlib)
         + resultat_compte_agr (exeordre, '18771%', agregatlib)
         + resultat_compte_agr (exeordre, '18778%', agregatlib)
         + resultat_compte_agr (exeordre, '18797%', agregatlib);
      totalant :=
           resultat_compte_agr (exeordre - 1, '771%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '778%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '797%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '18771%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '18778%', agregatlib)
         + resultat_compte_agr (exeordre - 1, '18797%', agregatlib);
      lib := '+ Produits exceptionnels "encaissables" (b)';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      -- ********  charges ***********
      lib1 := 'charges';
      total := resultat_compte_agr (exeordre, '65%', agregatlib) + resultat_compte_agr (exeordre, '1865%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '65%', agregatlib) + resultat_compte_agr (exeordre - 1, '1865%', agregatlib);
      lib := '- Autres charges "décaissables" d''exploitation';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '66%', agregatlib) + resultat_compte_agr (exeordre, '1866%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '66%', agregatlib) + resultat_compte_agr (exeordre - 1, '1866%', agregatlib);
      lib := '- Charges financières "décaissables" (c)';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '671%', agregatlib) + resultat_compte_agr (exeordre, '678%', agregatlib) + resultat_compte_agr (exeordre, '18671%', agregatlib) + resultat_compte_agr (exeordre, '18678%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '671%', agregatlib) + resultat_compte_agr (exeordre - 1, '678%', agregatlib) + resultat_compte_agr (exeordre - 1, '18671%', agregatlib) + resultat_compte_agr (exeordre - 1, '18678%', agregatlib);
      lib := '- Charges exceptionnelles "décaissables" (d)';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '695%', agregatlib) + resultat_compte_agr (exeordre, '18695%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '695%', agregatlib) + resultat_compte_agr (exeordre - 1, '18695%', agregatlib);
      lib := '- Impôts sur les bénéfices';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      --- ************ Calcul de la caf *****************
      total := ebe + total_produits - total_charges;

      select count (*)
      into   cpt
      from   caf
      where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';

      if (cpt > 0) then
         select nvl (caf_montant, 0)
         into   totalant
         from   caf
         where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';
      else
         totalant := 0;
      end if;

      if total >= 0 then
         lib1 := 'produits';
         lib := '= CAPACITE D''AUTOFINANCEMENT';
      else
         lib1 := 'charges';
         lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );
   else
      --********* Détermination à partir du résultat **********************
      delete      caf
            where exe_ordre = exeordre and ges_code = agregatlib and methode_ebe = 'N';

--    IF sacd = 'O' THEN
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N';
--    ELSIF sacd = 'G' THEN
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE' AND methode_ebe = 'N';
--    ELSE
--        DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'N';
--    END IF;
      total_produits := 0;
      total_charges := 0;
      formule := '';
      --**** RECUPERATION RESULTAT ********
      totalant := 0;
      lib := 'Résultat de l''exercice';

      select sum (credit) - sum (debit)
      into   resultat
      from   maracuja.cfi_ecritures_2 d, maracuja.v_agregat_gestion ag
      where  (pco_num = '120' or pco_num = '129') and d.exe_ordre = exeordre and d.exe_ordre = ag.exe_ordre and d.ges_code = ag.ges_code and ag.agregat = agregatlib;

--
--
--    IF sacd = 'O' THEN
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
--    ELSIF sacd = 'G' THEN
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND exe_ordre = exeordre;
--    ELSE
--        SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
--        WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
--    END IF;

      -- N-1
      select count (*)
      into   cpt
      from   caf
      where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'RTAT';

      if (cpt > 0) then
         select nvl (caf_montant, 0)
         into   totalant
         from   caf
         where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'RTAT';
      else
         totalant := 0;
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   resultat,
                   totalant,
                   'RTAT'
                  );

      -- ********  Charges ***********
      lib1 := 'charges';
      total := resultat_compte_agr (exeordre, '681%', agregatlib) + resultat_compte_agr (exeordre, '686%', agregatlib) + resultat_compte_agr (exeordre, '687%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '681%', agregatlib) + resultat_compte_agr (exeordre - 1, '686%', agregatlib) + resultat_compte_agr (exeordre - 1, '687%', agregatlib);
      lib := '+ Dotations aux amortissements et provisions';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '675%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '675%', agregatlib);
      lib := '+ Valeur comptable des éléments actifs cédés';
      total_charges := total_charges + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );

      -- ********  Produits ***********
      lib1 := 'produits';
      total := resultat_compte_agr (exeordre, '781%', agregatlib) + resultat_compte_agr (exeordre, '786%', agregatlib) + resultat_compte_agr (exeordre, '787%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '781%', agregatlib) + resultat_compte_agr (exeordre - 1, '786%', agregatlib) + resultat_compte_agr (exeordre - 1, '787%', agregatlib);
      lib := '- Reprises sur amortissements et provisions';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '775%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '775%', agregatlib);
      lib := '- Produits de cessions des éléments actifs cédés';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '776%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '776%', agregatlib);
      lib := '- Produits issus de la neutralisation des amortissements';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      total := resultat_compte_agr (exeordre, '777%', agregatlib);
      totalant := resultat_compte_agr (exeordre - 1, '777%', agregatlib);
      lib := '- Quote-part des subventions d''investissement virées au compte de résultat';
      total_produits := total_produits + total;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   -total,
                   -totalant,
                   formule
                  );

      --- ************ Calcul de la caf *****************
      total := resultat + total_charges - total_produits;
      totalant := 0;
      formule := 'CAF';

      if total >= 0 then
         lib1 := 'produits';
         lib := '= CAPACITE D''AUTOFINANCEMENT';
      else
         lib1 := 'charges';
         lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
      end if;

      -- N-1
      select count (*)
      into   cpt
      from   caf
      where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';

      if (cpt > 0) then
         select nvl (caf_montant, 0)
         into   totalant
         from   caf
         where  ges_code = agregatlib and exe_ordre = exeordre - 1 and formule = 'CAF';
      else
         totalant := 0;
      end if;

      insert into caf
      values      (caf_seq.nextval,
                   exeordre,
                   agregatlib,
                   methodeebe,
                   lib1,
                   lib,
                   total,
                   totalant,
                   formule
                  );
   end if;
end;
/

