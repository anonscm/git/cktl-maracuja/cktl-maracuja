
CREATE OR REPLACE PACKAGE BODY MARACUJA.extourne
is
   -- creer le bordereau de mandats d'extourne (ainsi que les depenses sur jefy_depense) a partir du bordereau de mandats a extourner
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type)
   is
      bordereau_n     bordereau%rowtype;
      mandat_n        mandat%rowtype;
      depense_n       depense%rowtype;
      flag            integer;
      depidn          jefy_depense.depense_budget.dep_id%type;
      depidn1         jefy_depense.depense_budget.dep_id%type;

      cursor c_mandat_n
      is
         select   m.*
         from     mandat m, mode_paiement mp
         where    m.mod_ordre = mp.mod_ordre and m.bor_id = bordereau_n.bor_id and mod_dom = c_mod_dom_a_extourner and m.man_etat = 'VISE'
         order by ges_code, man_numero;

      cursor c_depense_n
      is
         select d.*
         from   depense d
         where  man_id = mandat_n.man_id;

      cursor c_depensectrlplanco_n1
      is
         select dpco.*
         from   jefy_depense.depense_ctrl_planco dpco
         where  dep_id = depidn1;

      chainedpcoids   varchar2 (30000);
      exeordren1      jefy_admin.exercice.exe_ordre%type;
      res             integer;
      manidn1         mandat.man_id%type;
   begin
      select *
      into   bordereau_n
      from   bordereau
      where  bor_id = boridn;

      exeordren1 := bordereau_n.exe_ordre + 1;

      -- verifier que le bordereau d'origine des mandats avec mode de paiement extourne
      if (is_bord_aextourner (boridn) = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner".');
      end if;

      -- verifier que le bordereau d'origine est visé
      if (bordereau_n.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau initial (bord_id=' || boridn || ' n''est pas vise.');
      end if;

      -- verifier qu'au moins un mandats a extourner du bordereau d'origine sont vises
      select count (*)
      into   flag
      from   mandat m, mode_paiement mp
      where  m.mod_ordre = mp.mod_ordre and bor_id = boridn and mod_dom = c_mod_dom_a_extourner and m.man_etat = 'VISE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau (bord_id=' || boridn || ' ne contient pas de mandats passés sur un mode de paiement "a extourner" qui soitr VISE.');
      end if;

      -- verifier que l'exercice n+1 existe
      if (is_exercice_existe (exeordren1) = 0) then
         raise_application_error (-20001, 'L''exercice ' || (exeordren1) || ' n''existe pas, impossible de réserver des crédts d''extourne. Créez l''exercice en question ou attendez qu''il soit créé pour viser le bordereau initial.');
      end if;

      -- creer bordereau sur N+1
      boridn1 := bordereau_abricot.get_num_borid (get_tboordre_mandat_ext, exeordren1, bordereau_n.ges_code, bordereau_n.utl_ordre);

      -- créer chaque dépense négative sur n+1 correspondant à chaque dépense de n (intégrée dans un mandat d'extourne visé)
      open c_mandat_n;

      loop
         fetch c_mandat_n
         into  mandat_n;

         exit when c_mandat_n%notfound;
         chainedpcoids := '';

         open c_depense_n;

         loop
            fetch c_depense_n
            into  depense_n;

            exit when c_depense_n%notfound;
            -- creer la depense negative jefy_depense sur N+1
            depidn1 := prv_creer_jdep_n1 (depense_n);

            -- memoriser lien dep_idn/dep_idn1
            select dep_id
            into   depidn
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = depense_n.dep_ordre;

            insert into jefy_depense.extourne_liq
                        (el_id,
                         dep_id_n,
                         dep_id_n1
                        )
               select jefy_depense.extourne_liq_seq.nextval,
                      depidn,
                      depidn1
               from   dual;

            chainedpcoids := chainedpcoids || prv_chaine_dpcoids (depidn1) || '$';
         end loop;

         close c_depense_n;

         chainedpcoids := chainedpcoids || '$';
         -- creer mandat et brouillards sur N+1
         manidn1 := bordereau_abricot.set_mandat_depenses (chainedpcoids, boridn1);

         -- memoriser le lien man_idn/man_idn1
         insert into maracuja.extourne_mandat
                     (em_id,
                      man_id_n,
                      man_id_n1
                     )
            select maracuja.extourne_mandat_seq.nextval,
                   mandat_n.man_id,
                   manidn1
            from   dual;

         -- creer les depenses Maracuja sur N+1
         bordereau_abricot.get_depense_jefy_depense (manidn1);
         -- brouillard du mandat
         prv_creer_brouillard_inverse (mandat_n.man_id, manidn1);
      end loop;

      close c_mandat_n;

      bordereau_abricot.numeroter_bordereau (boridn1);
      bordereau_abricot.controle_bordereau (boridn1);
--   exception
--      when others then
--         raise_application_error (-20001, '[Creer Mandats Extourne]' || sqlerrm);
   end;

   -- renvoi >0 si le bordereau contient des mandats non rejetes passes sur un mode de paiement à extourner.
   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   mandat m inner join mode_paiement mp on (m.mod_ordre = mp.mod_ordre)
      where  bor_id = borid and mod_dom = c_mod_dom_a_extourner and m.brj_ordre is null and man_ttc>0;

      return cpt;
   end;

   function is_exercice_existe (exeordre integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   jefy_admin.exercice
      where  exe_ordre = exeordre;

      return cpt;
   end;

   -- creation de la depense jefy_depense sur N+1 a partir de la depense maracuja sur N
   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type
   is
      a_dep_id                number;
      a_exe_ordre             number;
      a_dpp_id                number;
      a_dep_ht_saisie         number;
      a_dep_ttc_saisie        number;
      a_fou_ordre             number;
      a_org_id                number;
      a_tcd_ordre             number;
      a_tap_id                number;
      a_eng_libelle           varchar2 (500);
      a_tyap_id               number;
      a_utl_ordre             number;
      a_dep_id_reversement    number;
      a_mod_ordre             number;
      a_chaine_action         varchar2 (30000);
      a_chaine_analytique     varchar2 (30000);
      a_chaine_convention     varchar2 (30000);
      a_chaine_hors_marche    varchar2 (30000);
      a_chaine_marche         varchar2 (30000);
      a_chaine_planco         varchar2 (30000);
      jdepense_ctrl_plancon   jefy_depense.depense_ctrl_planco%rowtype;
      jdepense_budgetn        jefy_depense.depense_budget%rowtype;
      jengage_budgetn         jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   jdepense_ctrl_plancon
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = depense_n.dep_ordre and man_id = depense_n.man_id;

      select *
      into   jdepense_budgetn
      from   jefy_depense.depense_budget
      where  dep_id = jdepense_ctrl_plancon.dep_id;

      select *
      into   jengage_budgetn
      from   jefy_depense.engage_budget
      where  eng_id = jdepense_budgetn.eng_id;

      a_dep_id := null;
      a_exe_ordre := jdepense_ctrl_plancon.exe_ordre + 1;
      a_dpp_id := null;
      a_dep_ht_saisie := -jdepense_ctrl_plancon.dpco_ht_saisie;
      a_dep_ttc_saisie := -jdepense_ctrl_plancon.dpco_ttc_saisie;
      a_fou_ordre := depense_n.fou_ordre;
      a_org_id := get_orgid_extourne (depense_n);
      a_tcd_ordre := get_tcdordre_extourne (depense_n);
      a_tap_id := jdepense_budgetn.tap_id;
      a_eng_libelle := substr ('Extourne : ' || jengage_budgetn.eng_libelle, 1, 500);
      a_tyap_id := jengage_budgetn.tyap_id;
      a_utl_ordre := jdepense_budgetn.utl_ordre;
      a_dep_id_reversement := null;
      -- on ne recupere pas les actions
      a_chaine_action := '$';
      -- on ne recupere pas les codes nomenclature
      a_chaine_hors_marche := '$';
      a_chaine_analytique := prv_chaine_analytique_dep (jdepense_budgetn.dep_id);
      a_chaine_convention := prv_chaine_convention_dep (jdepense_budgetn.dep_id);
      a_chaine_marche := prv_chaine_marche_dep (jdepense_budgetn.dep_id);
      a_chaine_planco := prv_chaine_planco_dep (jdepense_budgetn.dep_id);
      a_mod_ordre := get_modordre_extourne (depense_n);
      jefy_depense.reverser.ins_reverse_papier (a_dpp_id, a_exe_ordre, a_eng_libelle, a_dep_ht_saisie, a_dep_ttc_saisie, a_fou_ordre, null, a_mod_ordre, sysdate, sysdate, sysdate, sysdate, 1, a_utl_ordre, null);
      jefy_depense.liquider.ins_depense_directe (a_dep_id,
                                                 a_exe_ordre,
                                                 a_dpp_id,
                                                 a_dep_ht_saisie,
                                                 a_dep_ttc_saisie,
                                                 a_fou_ordre,
                                                 a_org_id,
                                                 a_tcd_ordre,
                                                 a_tap_id,
                                                 a_eng_libelle,
                                                 a_tyap_id,
                                                 a_utl_ordre,
                                                 a_dep_id_reversement,
                                                 a_chaine_action,
                                                 a_chaine_analytique,
                                                 a_chaine_convention,
                                                 a_chaine_hors_marche,
                                                 a_chaine_marche,
                                                 a_chaine_planco
                                                );
      return a_dep_id;
   end;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine               varchar2 (30000);
      chaine_inventaires   varchar2 (30000);
      repart               jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.pco_num || '$-' || repart.dpco_ht_saisie || '$-' || repart.dpco_ttc_saisie || '$' || repart.ecd_ordre || '$';
         -- FIXME recuperer les inventaires ????
         chaine_inventaires := '||';
         chaine := chaine || chaine_inventaires || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_action%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_action
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.tyac_id || '$-' || repart.dact_ht_saisie || '$-' || repart.dact_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_analytique%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_analytique
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.can_id || '$-' || repart.dana_ht_saisie || '$-' || repart.dana_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_convention%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_convention
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.conv_ordre || '$-' || repart.dcon_ht_saisie || '$-' || repart.dcon_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_hors_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.typa_id || '$' || repart.ce_ordre || '$-' || repart.dhom_ht_saisie || '$-' || repart.dhom_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_marche%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_marche
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.att_ordre || '$-' || repart.dmar_ht_saisie || '$-' || repart.dmar_ttc_saisie || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2
   is
      chaine   varchar2 (30000);
      repart   jefy_depense.depense_ctrl_planco%rowtype;

      cursor c_repart
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = depid;
   begin
      chaine := '';

      open c_repart;

      loop
         fetch c_repart
         into  repart;

         exit when c_repart%notfound;
         chaine := chaine || repart.dpco_id || '$';
      end loop;

      close c_repart;

      return chaine || '$';
   end;

   -- creer un brouillard inverse a celui d'origine
   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type)
   is
   begin
      -- inverser le sens et l'affecter au mandat d'extourne
      delete from mandat_brouillard
            where man_id = manidn1;

      insert into maracuja.mandat_brouillard
                  (ecr_ordre,
                   exe_ordre,
                   ges_code,
                   mab_montant,
                   mab_operation,
                   mab_ordre,
                   mab_sens,
                   man_id,
                   pco_num
                  )
         select null,
                exe_ordre + 1,
                ges_code,
                mab_montant,
                mab_operation,
                mandat_brouillard_seq.nextval,
                decode (mab_sens, 'C', 'D', 'C'),
                manidn1,
                pco_num
         from   mandat_brouillard
         where  man_id = manidn;
   end;

   -- viser automatiquement le bordereau de mandats d'extourne en entier
   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type)
   is
      lemandat   mandat%rowtype;
      flag       integer;

      cursor lesmandats
      is
         select *
         from   mandat
         where  bor_id = boridn1;
   begin
      -- verifier que le bordereau existe
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = boridn1;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' n''existe pas.');
      end if;

      -- verifier que le bordereau  est valide
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = boridn1 and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' n''est pas à l''etat VALIDE.');
      end if;

      -- verifier s'il y a des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = boridn1;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau bor_id ' || boridn1 || ' ne contient pas de mandats.');
      end if;

      -- viser chaque mandat
      open lesmandats;

      loop
         fetch lesmandats
         into  lemandat;

         exit when lesmandats%notfound;

         -- verifier que le mandat est bien un madat d''extourne
         select count (*)
         into   flag
         from   mandat m, extourne_mandat em
         where  m.man_id = em.man_id_n1 and m.man_id = lemandat.man_id;

         if (flag = 0) then
            raise_application_error (-20001, 'Le mandat man_id ' || lemandat.man_id || ' n''est pas un mandat d''extourne.');
         end if;

         api_mandat.viser_mandat (lemandat.man_id, datevisa, utlordre, 'Extourne');
      end loop;

      close lesmandats;

      -- mettre a jour le bordereau
      update bordereau
         set bor_etat = 'VISE',
             bor_date_visa = datevisa,
             utl_ordre_visa = utlordre
       where bor_id = boridn1;
--   exception
--      when others then
--         raise_application_error (-20001, '[Viser Mandats Extourne] ' || sqlerrm);
   end;

   function get_tboordre_mandat_ext
      return type_bordereau.tbo_ordre%type
   is
   begin
      return 50;
   end;

   -- renvoie le mod_ordre pour le mandat d'extourne
   function get_modordre_extourne (ladepensen maracuja.depense%rowtype)
      return mode_paiement.mod_ordre%type
   is
      modordren1      mode_paiement.mod_ordre%type;
      modepaiementn   mode_paiement%rowtype;
      flag            integer;
   begin
      modordren1 := null;

      select *
      into   modepaiementn
      from   mode_paiement
      where  mod_ordre = ladepensen.mod_ordre;

      select count (*)
      into   flag
      from   mode_paiement
      where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_code = (modepaiementn.mod_code) and mod_validite = 'VALIDE';

      if (flag > 0) then
         select mod_ordre
         into   modordren1
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_code = (modepaiementn.mod_code) and mod_validite = 'VALIDE';
      end if;

      -- si le mode ed paiement identique n'est pâs retrouvé sur n+1, on prend un mode de paiement interne valide au hasard
      if (modordren1 is null) then
         select count (*)
         into   flag
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_validite = 'VALIDE' and mod_dom = 'INTERNE';

         if (flag = 0) then
            raise_application_error (-20001, 'Aucun mode de paiement interne trouvé sur  ' || (modepaiementn.exe_ordre + 1) || ' pour l''affectation au mandat d''extourne.');
         end if;

         select max (mod_ordre)
         into   modordren1
         from   mode_paiement
         where  exe_ordre = (modepaiementn.exe_ordre + 1) and mod_validite = 'VALIDE' and mod_dom = 'INTERNE';
      end if;

      return modordren1;
   end;

   -- renvoie le tcd_ordre pour le mandat d'extourne
   function get_tcdordre_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.type_credit.tcd_ordre%type
   is
      tcdordren1    jefy_admin.type_credit.tcd_ordre%type;
      typecreditn   jefy_admin.type_credit%rowtype;
      flag          integer;
   begin
      tcdordren1 := null;

      select *
      into   typecreditn
      from   jefy_admin.type_credit
      where  tcd_ordre = ladepensen.tcd_ordre;

      select count (*)
      into   flag
      from   jefy_admin.type_credit
      where  exe_ordre = (ladepensen.exe_ordre + 1) and tcd_code = (typecreditn.tcd_code) and tcd_type = typecreditn.tcd_type and tyet_id = 1;

      if (flag > 0) then
         select tcd_ordre
         into   tcdordren1
         from   jefy_admin.type_credit
         where  exe_ordre = (ladepensen.exe_ordre + 1) and tcd_code = (typecreditn.tcd_code) and tcd_type = typecreditn.tcd_type and tyet_id = 1;
      end if;

      -- si le mode ed paiement identique n'est pâs retrouvé sur n+1, on prend un mode de paiement interne valide au hasard
      if (tcdordren1 is null) then
         raise_application_error (-20001, 'Le type de crédit ' || (typecreditn.tcd_code) || ' n''est pas valide pour l''exercice ' || (typecreditn.exe_ordre + 1) || '. Impossible de créer les crédits d''extourne.');
      end if;

      return tcdordren1;
   end;

   -- renvoie le tcd_ordre pour le mandat d'extourne
   function get_orgid_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.organ.org_id%type
   is
      orgid1       jefy_admin.organ.org_id%type;
      organn       jefy_admin.organ%rowtype;
      organn1      jefy_admin.organ%rowtype;
      flag         integer;
      datedebut1   date;
   begin
      orgid1 := null;
      datedebut1 := to_date ('01/01/' || (ladepensen.exe_ordre + 1), 'dd/mm/yyyy');

      select *
      into   organn
      from   jefy_admin.organ
      where  org_id = ladepensen.org_ordre;

      -- si la ligne est fermée sur l'exercice n+1 on prend la ligne de reprise
      if (organn.org_date_cloture is not null and organn.org_date_cloture < datedebut1) then
         orgid1 := organn.org_id_reprise;

         if (orgid1 is null) then
            raise_application_error (-20001,
                                        'La ligne budgétaire sur laquelle a été effectuée la liquidation initiale ( '
                                     || organn.org_etab
                                     || organn.org_ub
                                     || '/'
                                     || organn.org_cr
                                     || '/'
                                     || organn.org_souscr
                                     || ') n''est pas ouverte pour l''exercice '
                                     || (ladepensen.exe_ordre + 1)
                                     || ', et aucune ligne de reprise n''a été définie. Impossible de réserver les crédits d''extourne. Solution : ouvrez la ligne pour le nouvel exercice ou indiquez une ligne de reprise.'
                                    );
         end if;

         select *
         into   organn1
         from   jefy_admin.organ
         where  org_id = orgid1;

         if ((organn1.org_date_cloture is not null and organn1.org_date_cloture < datedebut1) or organn1.org_date_ouverture > datedebut1) then
            raise_application_error (-20001,
                                        'La ligne budgétaire sur laquelle a été effectuée la liquidation initiale ( '
                                     || organn.org_etab
                                     || organn.org_ub
                                     || '/'
                                     || organn.org_cr
                                     || '/'
                                     || organn.org_souscr
                                     || ') a une ligne de reprise définie ( '
                                     || organn1.org_etab
                                     || organn1.org_ub
                                     || '/'
                                     || organn1.org_cr
                                     || '/'
                                     || organn1.org_souscr
                                     || ') mais celle-ci n''est pas ouverte pour l''exercice '
                                     || (ladepensen.exe_ordre + 1)
                                     || '. Impossible de réserver les crédits d''extourne. Solution : ouvrez la ligne pour le nouvel exercice ou indiquez une ligne de reprise ouverte.'
                                    );
         end if;
      else
         orgid1 := organn.org_id;
      end if;

      return orgid1;
   end;

   -- s'agit-il d'une liquidation définitive sur crédit d'extourne ?
   function is_dpco_extourne_def (dpcoid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.extourne_liq_def eld on (dpco.dep_id = eld.dep_id)
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   -- s'agit-il d'une liquidation d'extourne ?
   function is_dpco_extourne (dpcoid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_ctrl_planco dpco inner join jefy_depense.extourne_liq el on (dpco.dep_id = el.dep_id_n1)
      where  dpco.dpco_id = dpcoid;

      return res;
   end;

   -- renvoie le montant budgétaire calculé de la liquidation
   function calcul_montant_budgetaire (dpcoid jefy_depense.depense_ctrl_planco.dpco_id%type)
      return jefy_depense.depense_ctrl_planco.dpco_montant_budgetaire%type
   as
      db     jefy_depense.depense_budget%rowtype;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;
      eb     jefy_depense.engage_budget%rowtype;
   begin
      select *
      into   dpco
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select *
      into   db
      from   jefy_depense.depense_budget
      where  dep_id = dpco.dep_id;

      select *
      into   eb
      from   jefy_depense.engage_budget
      where  eng_id = db.eng_id;

      return jefy_depense.budget.calculer_budgetaire (db.exe_ordre, db.tap_id, eb.org_id, dpco.dpco_ht_saisie, dpco.dpco_ttc_saisie);
   end;
end extourne;
/


