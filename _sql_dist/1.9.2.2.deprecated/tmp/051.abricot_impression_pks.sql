CREATE OR REPLACE PACKAGE MARACUJA.abricot_impressions
is
   -- il s'agit du dep_id de DEPENSE_budget
   function get_depense_origine_credits(depid integer)
   return varchar2;
   
   function get_depense_cn (depid integer)
      return varchar2;

   function get_depense_marche (depid integer)
      return varchar2;
      
   function get_depense_conv (depid integer)
      return varchar2;

   function get_recette_conv (recid integer)
      return varchar2;

   function get_depense_actions (depid integer)
      return varchar2;

   function get_recette_actions (recid integer)
      return varchar2;

   function get_depense_inventaire (dpcoid integer)
      return varchar2;

   function get_depense_lbud (depid integer)
      return varchar2;

   function get_depense_infos (depid integer)
      return varchar2;

   function get_recette_analytiques (recid integer)
      return varchar2;

   function get_depense_analytiques (depid integer)
      return varchar2;
      
   function get_dep_id_initial_si_extourne(depid integer) return integer;   
   
   function is_depense_sur_extourne(depid integer) return integer;
      
end abricot_impressions;
/

