CREATE OR REPLACE PACKAGE MARACUJA.extourne
is
   -- constantes
   c_mod_dom_a_extourner   constant maracuja.mode_paiement.mod_dom%type   := 'A EXTOURNER';

   -- bordidn : bordereau sur l'exercice n
   -- bordidn1 : bordereau cree sur l'exercice n+1 par cette procedure
   procedure creer_bordereau (boridn maracuja.bordereau.bor_id%type, boridn1 out maracuja.bordereau.bor_id%type);

   procedure viser_bord (boridn1 maracuja.bordereau.bor_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type);

   function is_bord_aextourner (borid maracuja.bordereau.bor_id%type)
      return integer;

   function is_dpco_extourne_def (dpcoid integer)
      return integer;

   function is_dpco_extourne (dpcoid integer)
      return integer;

   function is_exercice_existe (exeordre integer)
      return integer;

   function prv_creer_jdep_n1 (depense_n maracuja.depense%rowtype)
      return jefy_depense.depense_budget.dep_id%type;

   function prv_chaine_planco_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_action_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_analytique_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_convention_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_hors_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_marche_dep (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   function prv_chaine_dpcoids (depid jefy_depense.depense_budget.dep_id%type)
      return varchar2;

   procedure prv_creer_brouillard_inverse (manidn mandat.man_id%type, manidn1 mandat.man_id%type);

   function get_tboordre_mandat_ext
      return type_bordereau.tbo_ordre%type;

   function get_modordre_extourne (ladepensen maracuja.depense%rowtype)
      return mode_paiement.mod_ordre%type;

   function get_tcdordre_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.type_credit.tcd_ordre%type;

   function get_orgid_extourne (ladepensen maracuja.depense%rowtype)
      return jefy_admin.organ.org_id%type;

   function calcul_montant_budgetaire (dpcoid jefy_depense.depense_ctrl_planco.dpco_id%type)
      return jefy_depense.depense_ctrl_planco.dpco_montant_budgetaire%type;
end extourne;
/


