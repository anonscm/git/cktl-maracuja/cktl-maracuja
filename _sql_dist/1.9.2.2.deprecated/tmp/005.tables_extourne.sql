
execute grhum.drop_object('maracuja','extourne_mandat','table');
execute grhum.drop_object('maracuja','extourne_mandat_seq','sequence');

create sequence maracuja.extourne_mandat_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create table maracuja.extourne_mandat (
  em_id            number not null,
  man_id_n         number null,
  man_id_n1        number not null
)
tablespace gfc;

comment on table maracuja.extourne_mandat is 'Memorise un mandat d''extourne sur N+1 et son mandat initial correspondant eventuel sur N';
comment on column maracuja.extourne_mandat.man_id_n is 'Reference au mandat initial sur N (null dans le cas de demarrage d''un exercice)';
comment on column maracuja.extourne_mandat.man_id_n1 is 'Reference au mandat d''extourne sur N+1';



alter table maracuja.extourne_mandat add (primary key (em_id) using index tablespace gfc_indx );
alter table maracuja.extourne_mandat add (constraint fk_extourne_mandat_manidn  foreign key (man_id_n)  references maracuja.mandat (man_id));
alter table maracuja.extourne_mandat add (constraint fk_extourne_mandat_manidn1  foreign key (man_id_n1)  references maracuja.mandat (man_id));
alter table maracuja.extourne_mandat add (constraint uk_extourne_mandat  unique (man_id_n, man_id_n1) using index tablespace gfc_indx);

