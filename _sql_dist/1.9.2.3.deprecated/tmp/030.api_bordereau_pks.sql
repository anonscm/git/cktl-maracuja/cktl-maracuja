CREATE OR REPLACE PACKAGE MARACUJA.api_bordereau
is
   procedure apres_visa (borid bordereau.bor_id%type);

   procedure prv_traiter_bord_rejet (lebordereau bordereau%rowtype);
    procedure prv_apres_visa_reversement (borid bordereau.bor_id%type);
   procedure emarger_visa_bord_prelevement (borid bordereau.bor_id%type);
   procedure prv_numeroter_ecritures (borid bordereau.bor_id%type);
   
   function is_bord_titres(borid bordereau.bor_id%type) return integer;
   function is_bord_mandats(borid bordereau.bor_id%type) return integer;
end;
/

