
create or replace force view maracuja.utilisateur_fonct (uf_ordre, utl_ordre, fon_ordre)
as
   select uf.uf_ordre,
          uf.utl_ordre,
          uf.fon_ordre
   from   jefy_admin.utilisateur_fonct uf, fonction f
   where  f.fon_ordre = uf.fon_ordre;
/



create or replace force view maracuja.preference (pref_id, pref_key, pref_default_value, pref_description, pref_personnalisable)
as
   select pref_id,
          pref_key,
          pref_default_value,
          pref_description,
          pref_personnalisable
   from   jefy_admin.preference;
/

create or replace force view maracuja.v_credits_Extourne (exe_ordre, ges_code, pco_num, quoi, section, montant)
as
   select   d.exe_ordre,
            d.ges_code,
            --substr (d.pco_ordre, 1, 2),
            pco_ordre,
            substr (tcd.tcd_type, 0, 1),
            tcd.tcd_sect,
            sum (dep_ht)
   from     maracuja.extourne_mandat em
            inner join maracuja.mandat m on em.MAN_ID_N1=m.man_id
            inner join maracuja.depense d on (m.man_id=d.man_id)
            inner join jefy_admin.type_credit tcd on d.TCD_ORDRE=tcd.tcd_ordre
   where    tcd.tcd_sect = 1
   group by d.exe_ordre,
            d.ges_code,
            --substr (d.pco_ordre, 1, 2),
            pco_ordre,
            substr (tcd.tcd_type, 0, 1),
            tcd.tcd_sect
   union all
    select   d.exe_ordre,
            d.ges_code,
            --substr (d.pco_ordre, 1, 3),
            pco_ordre,
            substr (tcd.tcd_type, 0, 1),
            tcd.tcd_sect,
            sum (dep_ht)
   from     maracuja.extourne_mandat em
            inner join maracuja.mandat m on em.MAN_ID_N1=m.man_id
            inner join maracuja.depense d on (m.man_id=d.man_id)
            inner join jefy_admin.type_credit tcd on d.TCD_ORDRE=tcd.tcd_ordre
   where    tcd.tcd_sect = 2
   group by d.exe_ordre,
            d.ges_code,
            --substr (d.pco_ordre, 1, 3),
            pco_ordre,
            substr (tcd.tcd_type, 0, 1),
            tcd.tcd_sect
   union all
   select   d.exe_ordre,
            d.ges_code,
            --substr (d.pco_ordre, 1, 2),
            pco_ordre,
            substr (tcd.tcd_type, 0, 1),
            tcd.tcd_sect,
            sum (dep_ht)
   from     maracuja.extourne_mandat em
            inner join maracuja.mandat m on em.MAN_ID_N1=m.man_id
            inner join maracuja.depense d on (m.man_id=d.man_id)
            inner join jefy_admin.type_credit tcd on d.TCD_ORDRE=tcd.tcd_ordre
   where    tcd.tcd_sect = 3
   group by d.exe_ordre,
            d.ges_code,
            --substr (d.pco_ordre, 1, 2),
            pco_ordre,
            substr (tcd.tcd_type, 0, 1),
            tcd.tcd_sect;
/
grant select on MARACUJA.V_CREDITS_EXTOURNE to comptefi;






create or replace force view comptefi.v_dvlop_dep_listeExt 
(exe_ordre, ges_code, section, pco_num_bdn, pco_num, mandats, mandats_sur_extourne,reversements, cr_ouverts, cr_extourne,dep_date, section_lib, ordre_presentation)
as
select   m.exe_ordre,
         m.ges_code,
         s.section,
         p.pco_num_bdn,
         m.pco_num,
         sum (dep_ht),
            0,
         0,
         0,
         0,
         e.date_saisie,
         s.section_lib,
         s.ordre_presentation
from     maracuja.mandat m 
         inner join maracuja.bordereau b on (m.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (m.pco_num = p.pco_num)
         inner join maracuja.depense dep on (m.man_id = dep.man_id)
         inner join maracuja.v_depense_tcd dtcd on (dep.dep_id = dtcd.dep_id)
         inner join comptefi.v_section s on (dtcd.tcd_sect = s.section)
         inner join
         (select   m.man_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.mandat_detail_ecriture mde, maracuja.ecriture e, maracuja.mandat m
          where    m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.man_id) e on (m.man_id = e.man_id)
where    s.section_type = 'D' and tbo_ordre <> 8 and tbo_ordre <> 21 and tbo_ordre <> 18 and tbo_ordre <> 16 and tbo_ordre <> 50 and (m.man_etat = 'VISE' or m.man_etat = 'PAYE')
group by m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num, e.date_saisie, s.section_lib, s.ordre_presentation
union all
-- ordre de reversement avant 2007
select   t.exe_ordre,
         t.ges_code,
         p.section,
         p.pco_num_bdn,
         t.pco_num,
         0,
         0,
         sum (tit_ht),
         0,
         0,
         e.date_saisie,
         p.section_lib,
         p.ordre_presentation
from     maracuja.titre t inner join maracuja.bordereau b on (t.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (t.pco_num = p.pco_num)
         inner join
         (select   m.tit_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.titre_detail_ecriture mde, maracuja.ecriture e, maracuja.titre m
          where    m.tit_id = mde.tit_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.tit_id) e on (t.tit_id = e.tit_id)
where    tbo_ordre = 8 and t.tit_etat = 'VISE'
group by t.exe_ordre, t.ges_code, p.section, p.pco_num_bdn, t.pco_num, e.date_saisie, p.section_lib, p.ordre_presentation
union all
-- ordre de reversement à partir de 2007
select   m.exe_ordre,
         m.ges_code,
         s.section,
         p.pco_num_bdn,
         m.pco_num,
         0,
         0,
         -sum (dep_ht),
         0,
         0,
         e.date_saisie,
         s.section_lib,
         s.ordre_presentation
from     maracuja.mandat m inner join maracuja.bordereau b on (m.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (m.pco_num = p.pco_num)
         inner join maracuja.depense dep on (m.man_id = dep.man_id)
         inner join maracuja.v_depense_tcd dtcd on (dep.dep_id = dtcd.dep_id)
         inner join comptefi.v_section s on (dtcd.tcd_sect = s.section)
         inner join
         (select   m.man_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.mandat_detail_ecriture mde, maracuja.ecriture e, maracuja.mandat m
          where    m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.man_id) e on (m.man_id = e.man_id)
where    s.section_type = 'D' and (tbo_ordre = 8 or tbo_ordre = 18 or tbo_ordre = 21) and (m.man_etat = 'VISE' or m.man_etat = 'PAYE')
group by m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num, e.date_saisie, s.section_lib, s.ordre_presentation
union all
-- Crédits ouverts
select exe_ordre,
       ges_code,
       bdn_section section,
       pco_num,
       pco_num,
       0,
       0,
       0,
       co,
       0,
       date_co,
       s.section_lib,
       s.ordre_presentation
from   comptefi.v_budnat2 inner join comptefi.v_section s on (bdn_section = s.section)
where  co <> 0 and bdn_quoi = 'D' and s.section_type = 'D'
union all
-- Crédits d'extourne
select exe_ordre,
       ges_code,
       ce.section,
       p.pco_num_bdn,
       ce.pco_num,
       0,
       0,
       0,
       0,
       montant,
       sysdate,
       s.section_lib,
       s.ordre_presentation
from   maracuja.v_credits_Extourne ce 
inner join comptefi.v_section s on (ce.SECTION = s.section)
inner join comptefi.v_planco p on (ce.pco_num = p.pco_num)
where  ce.montant <> 0 and ce.quoi = 'D' and s.section_type = 'D'
union all
-- mandats sur extourne
select   m.exe_ordre,
         m.ges_code,
         s.section,
         p.pco_num_bdn,
         m.pco_num,
         0,
         sum (dep_ht),
         0,
         0,
         0,
         e.date_saisie,
         s.section_lib,
         s.ordre_presentation
from     maracuja.mandat m 
         inner join maracuja.bordereau b on (m.bor_id = b.bor_id)
         inner join comptefi.v_planco p on (m.pco_num = p.pco_num)
         inner join maracuja.depense dep on (m.man_id = dep.man_id)
         inner join jefy_depense.depense_ctrl_planco dpco on (dpco.dpco_ID=dep.dep_ordre and dpco.man_id=m.man_id )
         inner join jefy_depense.extourne_liq_def eld on (eld.DEP_ID=dpco.dep_id and eld.tyet_id=220)
         inner join maracuja.v_depense_tcd dtcd on (dep.dep_id = dtcd.dep_id)
         inner join comptefi.v_section s on (dtcd.tcd_sect = s.section and  s.section_type = 'D')
         inner join
         (select   m.man_id,
                   min (e.ecr_date_saisie) date_saisie
          from     maracuja.ecriture_detail ecd, maracuja.mandat_detail_ecriture mde, maracuja.ecriture e, maracuja.mandat m
          where    m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and ecd.ecr_ordre = e.ecr_ordre
          group by m.man_id) e on (m.man_id = e.man_id)
where  b.tbo_ordre <> 8 and b.tbo_ordre <> 21 and b.tbo_ordre <> 18 and b.tbo_ordre <> 16 and b.tbo_ordre <> 50  and (m.man_etat = 'VISE' or m.man_etat = 'PAYE')
group by m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num, e.date_saisie, s.section_lib, s.ordre_presentation;
/


create or replace force view comptefi.v_dvlop_dep_ext (exe_ordre, ges_code, section, pco_num_bdn, pco_num, mandats, mandats_sur_extourne,reversements, montant_net, 
cr_ouverts, cr_extourne, non_empl, dep_date, section_lib, ordre_presentation)
as
   select   exe_ordre,
            ges_code,
            section,
            pco_num_bdn,
            pco_num,
            sum (mandats),
            sum (MANDATS_SUR_EXTOURNE),
            sum (reversements),
            sum (mandats) - sum (reversements) - sum (MANDATS_SUR_EXTOURNE),
            sum (cr_ouverts),
            sum (CR_EXTOURNE),
            sum (cr_ouverts) - (sum (mandats) - sum (reversements)- sum (MANDATS_SUR_EXTOURNE)),
            trunc (dep_date),
            section_lib,
            ordre_presentation
   from     comptefi.v_dvlop_dep_listeext
   group by exe_ordre, ges_code, section, pco_num_bdn, pco_num, trunc (dep_date), section_lib, ordre_presentation;
/

GRANT SELECT ON COMPTEFI.v_dvlop_dep_ext TO EPN;
GRANT SELECT ON COMPTEFI.v_dvlop_dep_ext TO MARACUJA;

create or replace force view maracuja.v_budget_lolf_exec (exercice, nature, masse, type_credit, tcd_libelle, budget, ub, lolf_code_pere, lolf_libelle_pere, code_lolf, libelle_lolf, montant)
as
   select   exercice,
            nature,
            masse,
            type_credit,
            tcd_libelle,
            operation budget,
            ub,
            lolf_code_pere,
            lolf_libelle_pere,
            code_lolf,
            libelle_lolf,
            sum (montant_budgetaire) montant
   from     (select tcd.tcd_libelle tcd_libelle,
                    tcd_sect masse,
                    'execution' operation,
                    o.org_ub ub,
                    rpca.exe_ordre exercice,
                    jlfr.lolf_code lolf_code_pere,
                    jlfr.lolf_libelle lolf_libelle_pere,
                    lfr.lolf_code code_lolf,
                    lfr.lolf_libelle libelle_lolf,
                    rpca.ract_ht_saisie montant_budgetaire,
                    ' ' libelle,
                    tcd.tcd_code type_credit,
                    tcd.tcd_type nature
             from   jefy_recette.recette_ctrl_action rpca
             inner join jefy_recette.recette_budget rb on ( rb.rec_id = rpca.rec_id)
             inner join jefy_admin.lolf_nomenclature_recette lfr on (lfr.lolf_id = jefy_admin.api_lolf.get_rec_lolf_id_pere_at_niveau (rpca.lolf_id, 2))
             inner join jefy_admin.lolf_nomenclature_recette jlfr on (lfr.lolf_pere = jlfr.lolf_id) 
             inner join jefy_recette.facture_budget fb on ( fb.fac_id = rb.fac_id) 
             inner join jefy_admin.organ o on (o.org_id = fb.org_id)
             inner join jefy_admin.type_credit tcd on (tcd.tcd_ordre = fb.tcd_ordre)
             where    rb.rec_id in (select distinct rec_id
                                  from            jefy_recette.recette_ctrl_planco
                                  where           tit_id is not null)
             union all
             select tcd.tcd_libelle tcd_libelle,
                    tcd_sect masse,
                    'execution' operation,
                    o.org_ub ub,
                    dpp.exe_ordre exercice,
                    jlfd.lolf_code lolf_code_pere,
                    jlfd.lolf_libelle lolf_libelle_pere,
                    lfd.lolf_code code_lolf,
                    lfd.lolf_libelle libelle_lolf,
                    dpca.dact_montant_budgetaire montant_budgetaire,
                    ' ' libelle,
                    tcd.tcd_code type_credit,
                    tcd.tcd_type nature
             from   jefy_depense.depense_ctrl_action dpca
                    inner join jefy_depense.depense_budget db on (db.dep_id = dpca.dep_id)
                    inner join jefy_depense.depense_papier dpp on ( db.dpp_id = dpp.dpp_id)
                    inner join jefy_admin.lolf_nomenclature_depense lfd on (lfd.lolf_id = jefy_admin.api_lolf.get_dep_lolf_id_pere_at_niveau (tyac_id, 2))
                    inner join jefy_admin.lolf_nomenclature_depense jlfd on (lfd.lolf_pere = jlfd.lolf_id)
                    inner join jefy_depense.engage_budget eb on ( eb.eng_id = db.eng_id)
                    inner join jefy_admin.organ o on ( o.org_id = eb.org_id)
                    inner join jefy_admin.type_credit tcd on (tcd.tcd_ordre = eb.tcd_ordre)
             where 
               db.dep_id in (select distinct dep_id
                                  from            jefy_depense.depense_ctrl_planco
                                  where           man_id is not null)
               and db.dep_id in (select dep_id from jefy_depense.depense_budget minus select dep_id from jefy_depense.extourne_liq_def where tyet_id=220)
             )           
   group by exercice, nature, masse, type_credit, tcd_libelle, operation, ub, lolf_code_pere, lolf_libelle_pere, code_lolf, libelle_lolf
   order by exercice, nature, masse, type_credit, tcd_libelle, budget, ub, lolf_code_pere, lolf_libelle_pere, code_lolf, libelle_lolf;
/


create or replace force view maracuja.v_budget_lolf_exec (exercice, nature, masse, type_credit, tcd_libelle, budget, ub, lolf_code_pere, lolf_libelle_pere, code_lolf, libelle_lolf, montant)
as
   select   exercice,
            nature,
            masse,
            type_credit,
            tcd_libelle,
            operation budget,
            ub,
            lolf_code_pere,
            lolf_libelle_pere,
            code_lolf,
            libelle_lolf,
            sum (montant_budgetaire) montant
   from     (select tcd.tcd_libelle tcd_libelle,
                    tcd_sect masse,
                    'execution' operation,
                    o.org_ub ub,
                    rpca.exe_ordre exercice,
                    jlfr.lolf_code lolf_code_pere,
                    jlfr.lolf_libelle lolf_libelle_pere,
                    lfr.lolf_code code_lolf,
                    lfr.lolf_libelle libelle_lolf,
                    rpca.ract_ht_saisie montant_budgetaire,
                    ' ' libelle,
                    tcd.tcd_code type_credit,
                    tcd.tcd_type nature
             from   jefy_recette.recette_ctrl_action rpca
             inner join jefy_recette.recette_budget rb on ( rb.rec_id = rpca.rec_id)
             inner join jefy_admin.lolf_nomenclature_recette lfr on (lfr.lolf_id = jefy_admin.api_lolf.get_rec_lolf_id_pere_at_niveau (rpca.lolf_id, 2))
             inner join jefy_admin.lolf_nomenclature_recette jlfr on (lfr.lolf_pere = jlfr.lolf_id) 
             inner join jefy_recette.facture_budget fb on ( fb.fac_id = rb.fac_id) 
             inner join jefy_admin.organ o on (o.org_id = fb.org_id)
             inner join jefy_admin.type_credit tcd on (tcd.tcd_ordre = fb.tcd_ordre)
             inner join  (select distinct rec_id
                                  from            jefy_recette.recette_ctrl_planco
                                  where           tit_id is not null) x on (rb.rec_id=x.rec_id) 
             union all
             select tcd.tcd_libelle tcd_libelle,
                    tcd_sect masse,
                    'execution' operation,
                    o.org_ub ub,
                    dpp.exe_ordre exercice,
                    jlfd.lolf_code lolf_code_pere,
                    jlfd.lolf_libelle lolf_libelle_pere,
                    lfd.lolf_code code_lolf,
                    lfd.lolf_libelle libelle_lolf,
                    dpca.dact_montant_budgetaire montant_budgetaire,
                    ' ' libelle,
                    tcd.tcd_code type_credit,
                    tcd.tcd_type nature
             from   jefy_depense.depense_ctrl_action dpca
                    inner join jefy_depense.depense_budget db on (db.dep_id = dpca.dep_id)
                    inner join jefy_depense.depense_papier dpp on ( db.dpp_id = dpp.dpp_id)
                    inner join jefy_admin.lolf_nomenclature_depense lfd on (lfd.lolf_id = jefy_admin.api_lolf.get_dep_lolf_id_pere_at_niveau (tyac_id, 2))
                    inner join jefy_admin.lolf_nomenclature_depense jlfd on (lfd.lolf_pere = jlfd.lolf_id)
                    inner join jefy_depense.engage_budget eb on ( eb.eng_id = db.eng_id)
                    inner join jefy_admin.organ o on ( o.org_id = eb.org_id)
                    inner join jefy_admin.type_credit tcd on (tcd.tcd_ordre = eb.tcd_ordre)
                    inner join (select distinct dep_id
                                  from            jefy_depense.depense_ctrl_planco
                                  where           man_id is not null) x on (db.dep_id=x.dep_id)
                    inner join (select dep_id from jefy_depense.depense_budget minus select dep_id from jefy_depense.extourne_liq_def where tyet_id=220) y on (db.dep_id=y.dep_id)  
            )                    
   group by exercice, nature, masse, type_credit, tcd_libelle, operation, ub, lolf_code_pere, lolf_libelle_pere, code_lolf, libelle_lolf
   order by exercice, nature, masse, type_credit, tcd_libelle, budget, ub, lolf_code_pere, lolf_libelle_pere, code_lolf, libelle_lolf;
/

CREATE OR REPLACE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
select rec_id, sum(reste_recouvrer) reste_recouvrer
from
( select r.rec_id, ecd_reste_emarger as reste_recouvrer
   from   maracuja.recette r, maracuja.titre_detail_ecriture tde, maracuja.ecriture_detail ecd
   where  r.rec_id = tde.rec_id and tde.ecd_ordre = ecd.ecd_ordre and rec_mont >= 0 and tde_origine = 'VISA CTP' and ecd.exe_ordre = (select max (exe_ordre)
                                                                                                                                      from   maracuja.titre_detail_ecriture
                                                                                                                                      where  tit_id = r.tit_id)
   union all
   select r.rec_id,
          -ecd_reste_emarger as reste_recouvrer
   from   maracuja.recette r, maracuja.titre_detail_ecriture tde, maracuja.ecriture_detail ecd
   where  r.rec_id = tde.rec_id and tde.ecd_ordre = ecd.ecd_ordre and rec_mont < 0 and tde_origine = 'VISA CTP' and ecd.exe_ordre = (select max (exe_ordre)
                                                                                                                                     from   maracuja.titre_detail_ecriture
                                                                                                                                     where  tit_id = r.tit_id)
) group by rec_id
/




