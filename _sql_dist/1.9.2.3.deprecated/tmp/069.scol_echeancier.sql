

CREATE OR REPLACE PACKAGE BODY MARACUJA.scol_echeancier IS
-- PUBLIC --



function getDatePremiereEcheance(exeOrdre number, dateInscription date) return date
is
    nbjoursMoisSuivantStr varchar2(10);
    nbjoursMoisSuivant integer;
    numeroJourEcheanceStr varchar2(10);
    numeroJourEcheance integer;
    dateFinMois date;
    dateInscriptionClean date;
    moisSuivant date;
    dateEcheance date;
    
begin
    nbjoursMoisSuivantStr := JEFY_ADMIN.API_PARAMETRE.GET_PARAM_MARACUJA ( 'org.cocktail.gfc.echeancier.dateecheance.nbjoursmoissuivant', exeOrdre, '0' );
    nbjoursMoisSuivant := to_number(nbjoursMoisSuivantStr);
    numeroJourEcheanceStr := JEFY_ADMIN.API_PARAMETRE.GET_PARAM_MARACUJA ( 'org.cocktail.gfc.echeancier.dateecheance.numerojour', exeOrdre, '5' );
    numeroJourEcheance := to_number(numeroJourEcheanceStr);    
     
    
    dateInscriptionClean := to_date(to_char(dateInscription, 'dd/mm/yyyy'),'dd/mm/yyyy');
    dateFinMois := LAST_DAY(dateInscriptionClean);
    
    if (dateInscriptionClean >= (dateFinMois + 1 - nbjoursMoisSuivant)) then
         moisSuivant := add_months(dateInscriptionClean, 2);
    else
         moisSuivant := add_months(dateInscriptionClean, 1);
    end if;
    
    
    dateEcheance := to_date( to_char(numeroJourEcheance, '00')  || '/' || to_char(moisSuivant, 'mm/yyyy')   ,'dd/mm/yyyy');
    return dateEcheance;
end;

function creerEcheancier3Echeances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscription date) return number
IS
    echeId number;
    flag integer;
    
    bordereau_data maracuja.bordereau%rowtype;
    personne_data  grhum.personne%rowtype;    
   
    echeLibelle maracuja.echeancier.eche_Libelle%type;
    echeReference maracuja.echeancier.ECHE_REF_FACTURE_EXTERNE%type;
    nbEcheances integer;
    date1 date;
    date2 date;
    date3 date;
    moisSuivant date;
    montant1 number(12,2);
    montant2 number(12,2);
    montant3 number(12,2);    
    echeanceId maracuja.prelevement.PREL_PRELEV_ORDRE%type;
    
    --delaiMinimalTraitement integer;
    
   
    
BEGIN
    
    -- verif

    select count(*) into flag from grhum.personne where pers_id = persId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.PERSONNE pour pers_id = '|| persId ||'.');    
    end if; 
    
    select * into personne_data from grhum.personne where pers_id= persId;  


    -- determiner les valeurs
    --exeOrdre := bordereau_data.exe_ordre;
    echeLibelle := 'INSCRIPTION ETUDIANT '|| personne_data.pers_libelle || ' ' || personne_data.pers_lc;
    echeReference := '';
    nbEcheances := 3;
   -- delaiMinimalTraitement := 6;
    
    
    
   -- moisSuivant := add_months(dateInscription, 1);
    --date1 := to_date( '05/' || to_char(moisSuivant, 'mm/yyyy')   ,'dd/mm/yyyy');
    date1 := GETDATEPREMIEREECHEANCE ( exeOrdre, dateInscription );
    date2 := add_months(date1,1);
    date3 := add_months(date2,1);
        
    montant3 := floor(montantTotal/3);
    montant2 := floor(montantTotal/3);
    montant1 := montantTotal - montant2 - montant3;
    
    
    
    -- creer l'echeancier
    echeId := MARACUJA.API_ECHEANCIER.CREERECHEANCIER ( exeOrdre, persId, fouOrdre, echeLibelle, echeReference, montantTotal, nbEcheances, date1 );        
    
    -- creer les echeances    
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 1/3', 1, montant1, date1, ribOrdre );
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 2/3', 2, montant2, date2, ribOrdre );
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 3/3', 3, montant3, date3, ribOrdre );
    return echeId;
END;


function creerEcheancierXEcheances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscription date, nbEcheances integer, intervalleEcheancesMois integer) return number
IS
    echeId number;
    flag integer;
    
    bordereau_data maracuja.bordereau%rowtype;
    personne_data  grhum.personne%rowtype;    
   
    echeLibelle maracuja.echeancier.eche_Libelle%type;
    echeReference maracuja.echeancier.ECHE_REF_FACTURE_EXTERNE%type;
    dateEcheance date;
    montantEcheance number(12,2);  
    montant number(12,2); 
    cumulMontant number(12,2);
    montantPremiereEcheance number(12,2);
    echeanceId maracuja.prelevement.PREL_PRELEV_ORDRE%type;
    numeroEcheance integer;
    

   
    
BEGIN
    
    -- verif
    select count(*) into flag from grhum.personne where pers_id = persId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.PERSONNE pour pers_id = '|| persId ||'.');    
    end if; 
    
    select count(*) into flag from grhum.ribfour_ulr where rib_ordre = ribOrdre;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.ribfour_ulr pour rib_ordre = '|| ribordre ||'.');    
    end if; 
    
    select count(*) into flag from grhum.ribfour_ulr where rib_ordre = ribOrdre and rib_valide='O';
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Le rib n''est pas valide. rib_ordre = '|| ribordre ||'.');    
    end if;    
    
    select * into personne_data from grhum.personne where pers_id= persId;  


    -- determiner les valeurs
    echeLibelle := 'INSCRIPTION ETUDIANT '|| personne_data.pers_libelle || ' ' || personne_data.pers_lc;
    echeReference := '';

    dateEcheance := GETDATEPREMIEREECHEANCE ( exeOrdre, dateInscription );
    numeroEcheance := 1;
    cumulMontant := 0;
    montantEcheance := floor(montantTotal/nbEcheances);
    montantPremiereEcheance := montantTotal- (montantEcheance * (nbEcheances-1));
    
    -- creer l'echeancier
    echeId := MARACUJA.API_ECHEANCIER.CREERECHEANCIER ( exeOrdre, persId, fouOrdre, echeLibelle, echeReference, montantTotal, nbEcheances, dateEcheance );        
    
    -- creer les echeances
    while numeroEcheance <= nbEcheances loop
        montant := floor(montantTotal/nbEcheances);    
        if (numeroEcheance = 1) then
            montant := montantPremiereEcheance;
        else
            montant := montantEcheance;
        end if;
    
         echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance '|| to_char(numeroEcheance) ||'/'|| to_char(nbEcheances)  ||'', numeroEcheance, montant, dateEcheance, ribOrdre );
    
        cumulMontant := cumulMontant + montant; 
        dateEcheance := add_months(dateEcheance, intervalleEcheancesMois); 
        numeroEcheance := numeroEcheance + 1;
    end loop;
    
    -- controle
    if (cumulMontant <> montantTotal) then
        RAISE_APPLICATION_ERROR (-20001,'Erreur de calcul des montants des echeances : '|| to_char(montantTotal) || '/'|| to_char(cumulMontant) ||'.');    
    end if;
    
    return echeId;
END;




procedure associerEcheancierEtBordereau(echeId number, borId number)
is
    flag integer;
begin

    select count(*) into flag from maracuja.bordereau where bor_id=borId;
        if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table MARACUJA.BORDEREAU pour bor_id = '|| borId ||'.');    
    end if; 


    select count(*) into flag from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE=echeId;
        if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table MARACUJA.ECHEANCIER pour ECHE_ECHEANCIER_ORDRE = '|| echeId ||'.');    
    end if; 

    -- 


    update maracuja.echeancier set bor_id=borId where ECHE_ECHEANCIER_ORDRE=echeId;


end;


procedure verifierEcheancier(echeId number)
is
begin
    MARACUJA.API_ECHEANCIER.verifierEcheancier ( echeId );
end;

procedure supprimerEcheancier(echeId number)
is
begin
    MARACUJA.API_ECHEANCIER.supprimerEcheancier ( echeId );
end;

procedure associerEcheancierEtBob(echeId number, bobOrdre number) is
begin
    MARACUJA.API_ECHEANCIER.associerEcheancierEtBob ( echeId, bobOrdre );
end;

END;
/
