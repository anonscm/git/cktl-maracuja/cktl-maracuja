CREATE OR REPLACE PACKAGE MARACUJA.abricot_util
is
/*
 * Copyright Cocktail, 2001-2012
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
-- rodolphe.prin at cocktail.org
   function is_dpco_tva_collectee (dpcoid integer)
      return integer;

   function is_man_sur_sacd (manid integer)
      return integer;

   function get_dpco_taux_prorata (dpcoid integer)
      return number;

   function get_dpco_montant_budgetaire (dpcoid integer)
      return number;

   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type;

   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type;

   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type;

   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type;

   function get_dpco_tcd_ordre (dpcoid integer)
      return number;

   function get_dpco_fou_ordre (dpcoid integer)
      return number;

   function get_dpco_org_id (dpcoid integer)
      return number;

   function get_dpco_adresse (dpcoid integer, taille integer)
      return varchar;

   function get_dpco_lot_ordre (dpcoid integer)
      return number;

   function get_lot_mar_ordre (lotordre integer)
      return number;

   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type;

   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number;

   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number;

   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type;

   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type;

   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type;

   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type);

   function get_fournis_nom (fouordre integer)
      return varchar;
      
   
   function get_organ_path (orgid integer, taille integer)
      return varchar;
      
   function get_organ_ub (orgid integer)
      return varchar;      
      
      
end abricot_util;
/
