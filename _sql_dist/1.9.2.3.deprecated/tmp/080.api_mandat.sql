
CREATE OR REPLACE PACKAGE BODY MARACUJA.api_mandat
is
   procedure viser_mandat (manid mandat.man_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type, ecdlibelleprefix varchar2)
   is
      flag          integer;
      lemandat      mandat%rowtype;
      lebordereau   bordereau%rowtype;
      lemab         mandat_brouillard%rowtype;

      cursor mandatbrouillards
      is
         select *
         from   mandat_brouillard
         where  man_id = manid and mab_operation like 'VISA%';

      ecdlibelle    ecriture_detail.ecd_libelle%type;
      ecrlibelle    ecriture.ecr_libelle%type;
      oriordre      mandat.ori_ordre%type;
      ecrordre      ecriture.ecr_ordre%type;
      ecdordre      ecriture_detail.ecd_ordre%type;
      mdeordre      mandat_detail_ecriture.mde_ordre%type;
   begin
      check_mandat_avant_visa (manid);

      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = lemandat.bor_id;

      select ori_ordre
      into   oriordre
      from   mandat
      where  man_id = lemandat.man_id;

      -- creer l'ecriture
      ecrlibelle := '[' || ecdlibelleprefix || '] ' || creer_ecr_libelle (lemandat, lebordereau);
      ecrordre := api_plsql_journal.creerecritureexercicetype (1, datevisa, ecrlibelle, lemandat.exe_ordre, oriordre, 4, utlordre, 4);

      -- creer les ecriture_details
      open mandatbrouillards;

      loop
         fetch mandatbrouillards
         into  lemab;

         exit when mandatbrouillards%notfound;
         ecdlibelle := '[' || ecdlibelleprefix || '] ' || creer_ecd_libelle (lemab, lemandat, lebordereau);

         -- verifier que le compte existe et est valide
         if (api_planco.is_planco_valide (lemab.pco_num, lemandat.exe_ordre) = 0) then
            raise_application_error (-20001,
                                     'Le compte (' || lemab.pco_num || ') associé au mandat ' || lemandat.ges_code || '/' || lemandat.man_numero || ' (man_id ' || manid || ') n''existe pas ou n''est pas valide pour l''exercice ' || lemandat.exe_ordre
                                    );
         end if;

         ecdordre := api_plsql_journal.creerecrituredetail (null, ecdlibelle, lemab.mab_montant, null, lemab.mab_sens, ecrordre, lemab.ges_code, lemab.pco_num);

         select mandat_detail_ecriture_seq.nextval
         into   mdeordre
         from   dual;

         -- Insertion des mandat_detail_ecritures
         insert into mandat_detail_ecriture
         values      (ecdordre,
                      lemab.exe_ordre,
                      lemab.man_id,
                      sysdate,
                      mdeordre,
                      'VISA',
                      oriordre
                     );
      end loop;

      close mandatbrouillards;

      -- mettre à jour l''etat du mandat
      update mandat
         set man_etat = 'VISE',
             man_date_remise = datevisa
       where man_id = manid;

      -- verifier et numeroter l'ecriture
      maracuja.api_plsql_journal.validerecriture (ecrordre);
   end;

   procedure check_mandat_avant_visa (manid mandat.man_id%type)
   is
      lemandat   mandat%rowtype;
      flag       integer;
   begin
      select count (*)
      into   flag
      from   mandat
      where  man_id = manid;

      if (flag = 0) then
         raise_application_error (-20001, 'Mandat man_id ' || manid || ' non trouve');
      end if;

      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      -- verifier mandat non vise (attente)
      if (lemandat.man_etat <> 'ATTENTE') then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') n''est pas à l''etat ATTENTE. (' || lemandat.man_etat || ').');
      end if;

      -- verifie mandat non sur bordereau rejet
      if (lemandat.brj_ordre is not null) then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') est place sur un bordereau de rejet (brj_ordre=' || lemandat.brj_ordre || ').');
      end if;

      -- verifier qu'il n'y a pas deja des ecritures associées au mandat
      select count (*)
      into   flag
      from   mandat_detail_ecriture
      where  man_id = manid;

      if (flag > 0) then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') a deja des ecritures associees (table mandat_detail_ecriture).');
      end if;

      -- verifier qu'il y a des brouillards
      select count (*)
      into   flag
      from   mandat_brouillard
      where  man_id = manid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le mandat (man_id ' || manid || ') n'' a pas de brouillards associés (table mandat_brouillard).');
      end if;
   end;

   function creer_ecd_libelle (mab mandat_brouillard%rowtype, lemandat mandat%rowtype, lebordereau bordereau%rowtype)
      return ecriture_detail.ecd_libelle%type
   is
      res              ecriture_detail.ecd_libelle%type;
      lesoustype       type_bordereau.tbo_sous_type%type;
      nomfournisseur   varchar2 (250);
      typemandat       varchar2 (5);
   begin
      select tbo_sous_type,
             decode (tbo_sous_type, 'REVERSEMENT', 'Orv', 'Md.')
      into   lesoustype,
             typemandat
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      select trim (nom || ' ' || nvl (prenom, ''))
      into   nomfournisseur
      from   v_fournis_light
      where  fou_ordre = lemandat.fou_ordre;

      res := lesoustype || ' ' || nomfournisseur || ' ' || typemandat || ' ' || lemandat.man_numero || ' Bord. ' || lebordereau.bor_num || ' du ' || lebordereau.ges_code || ' ' || lemandat.exe_ordre;
      return res;
   end;

   function creer_ecr_libelle (lemandat mandat%rowtype, lebordereau bordereau%rowtype)
      return ecriture.ecr_libelle%type
   is
      res          ecriture_detail.ecd_libelle%type;
      lesoustype   type_bordereau.tbo_sous_type%type;
      typemandat   varchar2 (5);
   begin
      select tbo_sous_type,
             decode (tbo_sous_type, 'REVERSEMENT', 'Orv', 'Md.')
      into   lesoustype,
             typemandat
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      res := typemandat || ' Num. ' || lemandat.man_numero || ' Bord. Num. ' || lebordereau.bor_num;
      return res;
   end;
end;
/


