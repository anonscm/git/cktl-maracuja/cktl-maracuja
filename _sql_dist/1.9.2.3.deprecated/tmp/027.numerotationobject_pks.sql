CREATE OR REPLACE PACKAGE MARACUJA."NUMEROTATIONOBJECT"
is
-- PUBLIC -
   procedure numeroter_ecriture (ecrordre integer);

   procedure numeroter_ecriture_verif (comordre integer, exeordre integer);

   procedure private_numeroter_ecriture (ecrordre integer);

   procedure numeroter_brouillard (broid integer);

   procedure numeroter_bordereaurejet (brjordre integer);

   procedure numeroter_bordereau (borid integer);

   procedure numeroter_bordereaucheques (borid integer);

   procedure numeroter_emargement (emaordre integer);

   procedure numeroter_ordre_paiement (odpordre integer);

   procedure numeroter_paiement (paiordre integer);

   procedure numeroter_retenue (retordre integer);

   procedure numeroter_reimputation (reiordre integer);

   procedure numeroter_recouvrement (recoordre integer);

   function next_numero_paiement (comordre integer, exeordre integer)
      return integer;

-- PRIVATE --
   procedure numeroter_mandat_rejete (manid integer);

   procedure numeroter_titre_rejete (titid integer);

   procedure numeroter_mandat (borid integer);

   procedure numeroter_mandat_extourne (borid integer);

   procedure numeroter_titre (borid integer);

   procedure numeroter_orv (borid integer);

   procedure numeroter_aor (borid integer);

   function numeroter (comordre comptabilite.com_ordre%type, exeordre exercice.exe_ordre%type, gescode gestion.ges_ordre%type, tnuordre type_numerotation.tnu_ordre%type)
      return integer;
end;
/

