SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.V_DEPENSE_IM_TAUX_REF
(DPP_ID, IMTT_ID, IMTT_CODE, IMTA_ID, DPP_DATE_SERVICE_FAIT, 
 DPP_DATE_RECEPTION, IMTA_TAUX, DATE_DEBUT_DGP, IMDG_DGP, DEBUT_SUSPENSION, 
 FIN_SUSPENSION, DUREE_SUSPENSION, FIN_DGP_THEORIQUE, FIN_DGP_REELLE, DATE_DETERMINATION_TAUX)
AS 
select zz.dpp_id, yy.imtt_id, imtt_code, IMTA_id , DPP_DATE_SERVICE_FAIT, DPP_DATE_RECEPTION, imta_taux,DATE_DEBUT_DGP, IMDG_DGP, DEBUT_SUSPENSION, FIN_SUSPENSION, 
 DUREE_SUSPENSION, fin_dgp_theorique, fin_dgp_reelle , decode(  sign(to_number(to_char(zz.fin_dgp_reelle,'MM')-7)) , 1 , to_date('30/06/'||to_char(zz.fin_dgp_reelle,'YYYY'),'DD/MM/YYYY' ), trunc(zz.fin_dgp_reelle,'YYYY')-1 ) as date_determination_taux from 
 (
     select 
     dpp1.dpp_id, dpp1.DPP_DATE_SERVICE_FAIT, dpp1.DPP_DATE_RECEPTION, DATE_DEBUT_DGP, IMDG_DGP, DEBUT_SUSPENSION, FIN_SUSPENSION, 
     DUREE_SUSPENSION, fin_dgp_theorique, fin_dgp_reelle
     from
     jefy_depense.depense_papier dpp1,
    (
        select dpp.DPP_ID, DPP_DATE_SERVICE_FAIT, DPP_DATE_RECEPTION, greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION)) date_debut_dgp , nvl(dpp_im_dgp, imdg_dgp) as IMDG_DGP, 
         debut_suspension, fin_suspension , nvl(duree_suspension,0) duree_suspension, 
         greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION)) + nvl(dpp_im_dgp, imdg_dgp) fin_dgp_theorique, 
         greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION)) + nvl(dpp_im_dgp, imdg_dgp)+ nvl(duree_suspension,0) fin_dgp_reelle
         from jefy_depense.depense_papier dpp,
         jefy_admin.im_dgp dgp,
         (select dpp_id, min (IMSUS_debut) debut_suspension, max(IMSUS_FIN) fin_suspension , max(IMSUS_FIN)  - min (IMSUS_debut) as duree_suspension
         from maracuja.im_suspension
         where  IMSUS_ETAT=1 and IMSUS_FIN is not null
         group by dpp_id) x
         where 
         greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION)) >= imdg_debut 
         and (greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION))<=imdg_fin or imdg_fin is null) 
         and dpp.dpp_id = x.dpp_id(+)
    ) yy
    where 
    dpp1.dpp_id=yy.dpp_id(+)
) zz,
jefy_admin.im_taux it,
(select * from jefy_admin.im_type_taux tt where tt.IMTT_PRIORITE=(select max(imtt_priorite) from jefy_admin.im_type_taux where TYET_ID=1) and rownum=1) yy
where yy.imtt_id=it.imtt_id
and it.IMTA_DEBUT<=decode(  sign(to_number(to_char(zz.fin_dgp_reelle,'MM')-7)) , 1 , to_date('30/06/'||to_char(zz.fin_dgp_reelle,'YYYY'),'DD/MM/YYYY' ), trunc(zz.fin_dgp_reelle,'YYYY')-1 )
and (it.imta_fin is null or it.imta_fin>=decode(  sign(to_number(to_char(zz.fin_dgp_reelle,'MM')-7)) , 1 , to_date('30/06/'||to_char(zz.fin_dgp_reelle,'YYYY'),'DD/MM/YYYY' ), trunc(zz.fin_dgp_reelle,'YYYY')-1 ))
;










CREATE OR REPLACE VIEW MARACUJA.V_IM_DGP
(IMDG_ID, IMDG_DEBUT, IMDG_FIN, IMDG_DGP, UTL_ORDRE, 
 DATE_CREATION, DATE_MODIFICATION)
AS 
select IMDG_ID, IMDG_DEBUT, IMDG_FIN, IMDG_DGP, UTL_ORDRE, 
 DATE_CREATION, DATE_MODIFICATION from jefy_admin.im_dgp;
 
 
CREATE OR REPLACE FORCE VIEW MARACUJA.V_IM_TYPE_TAUX
(IMTT_ID, IMTT_CODE, IMTT_LIBELLE, IMTT_PRIORITE, TYET_ID)
AS select IMTT_ID, IMTT_CODE, IMTT_LIBELLE, IMTT_PRIORITE, TYET_ID from jefy_admin.im_type_taux;
 
CREATE OR REPLACE VIEW MARACUJA.v_IM_TAUX
(IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, 
 UTL_ORDRE, DATE_CREATION, DATE_MODIFICATION)
AS 
select IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, 
 UTL_ORDRE, DATE_CREATION, DATE_MODIFICATION from jefy_admin.im_taux;
 

create or replace view maracuja.v_depense_depense_papier(mara_dep_id, dpco_id, jd_dep_id, dpp_id, man_id) 
as
    select mdep.dep_id mara_dep_id, dpco_id,  jdb.dep_id as jd_dep_id,jdb.dpp_id, dpco.man_id
    from maracuja.depense mdep, 
    jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget jdb
    where mdep.man_id = dpco.man_id
    and mdep.dep_ordre = dpco.dpco_id
    and dpco.dep_id=jdb.dep_id; 



CREATE OR REPLACE FORCE VIEW MARACUJA.V_JD_DEPENSE_PAPIER
(DPP_ID, EXE_ORDRE, DPP_NUMERO_FACTURE, DPP_HT_SAISIE, DPP_TVA_SAISIE, 
 DPP_TTC_SAISIE, FOU_ORDRE, RIB_ORDRE, MOD_ORDRE, DPP_DATE_FACTURE, 
 DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, DPP_NB_PIECE, UTL_ORDRE, 
 DPP_ID_REVERSEMENT, DPP_HT_INITIAL, DPP_TVA_INITIAL, DPP_TTC_INITIAL, DPP_IM_DGP, 
 DATE_DEBUT_DGP)
AS 
select DPP_ID, EXE_ORDRE, DPP_NUMERO_FACTURE, DPP_HT_SAISIE, DPP_TVA_SAISIE, 
 DPP_TTC_SAISIE, FOU_ORDRE, RIB_ORDRE, MOD_ORDRE, DPP_DATE_FACTURE, 
 DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, DPP_NB_PIECE, UTL_ORDRE, 
 DPP_ID_REVERSEMENT, DPP_HT_INITIAL, DPP_TVA_INITIAL, DPP_TTC_INITIAL, dpp_im_dgp, greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION)) date_debut_dgp 
 from jefy_depense.depense_papier;





CREATE OR REPLACE FORCE VIEW MARACUJA.V_JD_DEPENSE_CTRL_PLANCO
(DPCO_ID, EXE_ORDRE, DEP_ID, PCO_NUM, MAN_ID, 
 DPCO_MONTANT_BUDGETAIRE, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, TBO_ORDRE, 
 ECD_ORDRE, DPP_ID, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, DPP_NUMERO_FACTURE, 
 DPP_DATE_FACTURE, DPP_IM_DGP, DATE_DEBUT_DGP)
AS 
select DPCO_ID, dpco.EXE_ORDRE, dpco.DEP_ID, PCO_NUM, MAN_ID, 
 DPCO_MONTANT_BUDGETAIRE, DPCO_HT_SAISIE, DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, TBO_ORDRE, 
 ECD_ORDRE, db.dpp_id, dpp.dpp_date_reception, dpp.dpp_date_service_fait, dpp.dpp_numero_facture , dpp.dpp_date_facture, dpp_im_dgp, greatest(trunc(DPP_DATE_SERVICE_FAIT), trunc(DPP_DATE_RECEPTION)) date_debut_dgp 
 from jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
 where dpco.dep_id=db.dep_id                                                                                                     
 and db.dpp_id=dpp.dpp_id;






 
 
 
 
 
 -------------------
 
 
 
 
 
 SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Api_echeancier IS

-- API PUBLIQUE pour creer des echeanciers / echeances --

-- creer un echeancier Invalide (qui doit etre validé)
function creerEcheancier (exeOrdre integer, persId number, fouOrdre Number, echelibelle varchar, echeancierReference varchar,  montantTotal number, nbEcheances integer, premieredate date) return number;

-- creer une  echeance INVALIDE
function creerEcheance(echeId number, persId number, fouOrdre Number, commentaire varchar, echeanceNumero integer, echeanceMontant number, echeanceDatePrevue date, ribOrdre integer) return number; 

-- verifie l'echeancier et ses echeances. L'echeancier doit etre coherent avec ses echeances, sinon exception.
procedure verifierEcheancier(echeId number);

procedure validerEcheancier(echeId number);

-- supprime l''echeancier, exception si echeancier non INVALIDE
procedure supprimerEcheancier(echeId number);

-- exception si echeancier n''existe pas
procedure checkEcheancierExiste(echeId number);

-- assocoer un echeancier a un bordereauBrouillard
procedure associerEcheancierEtBob(echeId number, bobOrdre number);

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_echeancier IS
-- PUBLIC --

function creerEcheancier (exeOrdre integer, persId number, fouOrdre Number, echelibelle varchar, echeancierReference varchar,  montantTotal number, nbEcheances integer, premieredate date) return number
IS

    echeId number;
    flag integer;
    leFournis grhum.fournis_ulr%rowtype;
    laPersonne grhum.personne%rowtype;
    montantTotalLettres echeancier.ECHE_MONTANT_EN_LETTRES%type;

BEGIN
    
    -- verifications
    
    -- persId ou fouOrdre ne doivent pas etre null 
    if (persId = null) then
        RAISE_APPLICATION_ERROR (-20001,'Le persId est obligatoire.');
    end if;       
    
    select count(*) into flag from grhum.personne where pers_id = persId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.PERSONNE pour pers_id = '|| persId ||'.');    
    end if; 
    
    select * into laPersonne from grhum.personne where pers_id= persId;    
    
    if (fouOrdre is not null) then
        select count(*) into flag from grhum.fournis_ulr where fou_ordre= fouOrdre;
        if (flag = 0) then
            RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.FOURNIS_ULR pour fou_Ordre = '|| fouOrdre ||'.');    
        end if; 
    
        select * into leFournis from grhum.fournis_ulr where fou_ordre= fouOrdre;
        -- verifier que le fournisseur n'est pas annule. Il peut etre en attente de validation
        if ( leFournis.fou_valide='A' )  then
            RAISE_APPLICATION_ERROR (-20001,'Le fournissseur (client) (fou_Ordre '|| fouOrdre ||') est annule, impossible de l''utiliser.');        
        end if;
        
    end if;
    
    
    
    if (montantTotal = null or montantTotal <= 0) then
        RAISE_APPLICATION_ERROR (-20001,'Le montant total ne peut etre null ou negatif.');
    end if;  

    montantTotalLettres := GRHUM.NOMBRE_EN_LETTRE(montantTotal);



    -- recuperer un nouvel id d'echeancier
    select jefy_echeancier.echeancier_seq.nextval into echeId from dual;


    INSERT INTO MARACUJA.ECHEANCIER (
        ECHE_AUTORIS_SIGNEE, 
        FOU_ORDRE_CLIENT, 
        CON_ORDRE,
        ECHE_DATE_1ERE_ECHEANCE, 
        ECHE_DATE_CREATION, 
        ECHE_DATE_MODIF,
        ECHE_ECHEANCIER_ORDRE,
        ECHE_ETAT_PRELEVEMENT, 
        FT_ORDRE, 
        ECHE_LIBELLE, 
        ECHE_MONTANT, 
        ECHE_MONTANT_EN_LETTRES,
        ECHE_NOMBRE_ECHEANCES, 
        ECHE_NUMERO_INDEX, 
        ORG_ORDRE, 
        PREST_ORDRE, 
        ECHE_PRISE_EN_CHARGE,
        ECHE_REF_FACTURE_EXTERNE, 
        ECHE_SUPPRIME, 
        EXE_ORDRE, 
        TIT_ID, 
        REC_ID, 
        TIT_ORDRE, 
        ORI_ORDRE,
        PERS_ID, 
        ORG_ID, 
        PERS_DESCRIPTION)  VALUES
    (
        'O'  ,--ECHE_AUTORIS_SIGNEE
        fouOrdre  ,--FOU_ORDRE_CLIENT
        null,--CON_ORDRE
        premiereDate,--ECHE_DATE_1ERE_ECHEANCE
        sysdate,--ECHE_DATE_CREATION
        sysdate,--ECHE_DATE_MODIF
        echeId,  --ECHE_ECHEANCIER_ORDRE
        'I',--ECHE_ETAT_PRELEVEMENT
        null,--FT_ORDRE
        echelibelle,--echancier_data.LIBELLE,--ECHE_LIBELLE
        montantTotal  ,--ECHE_MONTANT
        montantTotalLettres  ,--ECHE_MONTANT_EN_LETTRES
        nbEcheances  ,--ECHE_NOMBRE_ECHEANCES
        echeId,--ECHE_NUMERO_INDEX
        null,--ORG_ORDRE
        null,--PREST_ORDRE
        'O'  ,--ECHE_PRISE_EN_CHARGE
        echeancierReference,--ECHE_REF_FACTURE_EXTERNE
        'N'  ,--ECHE_SUPPRIME
        exeOrdre  ,--EXE_ORDRE
        null,
        null ,--REC_ID,
        null,
        null,--ORI_ORDRE,
        persId, --PERS_ID
        null,--orgid a faire plus tard....
        laPersonne.PERS_LIBELLE --    PERS_DESCRIPTION
    );
    
    return echeId;

END;


function creerEcheance(echeId number, persId number, fouOrdre Number, commentaire varchar, echeanceNumero integer, echeanceMontant number, echeanceDatePrevue date, ribOrdre integer) return number
is
    flag integer;
    echeanceId prelevement.PREL_PRELEV_ORDRE%type;

begin

    -- verifier que l'echeancier existe
    checkEcheancierExiste(echeId);

   if (persId = null) then
        RAISE_APPLICATION_ERROR (-20001,'Le persId est obligatoire.');
    end if;       
    
    
    -- verifier que le persid est le meme que celui de l'echeancier    
    select count(*) into flag from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE = echeId and pers_id= persId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Le persId specifie ('|| persId || ') ne correspond a celui de l''echeancier (ECHE_ECHEANCIER_ORDRE=' || echeId ||  ').');    
    end if; 
    
    
    -- verifier que le fouOrdre est le meme que celui de l'echeancier    
    if (fouOrdre is not null) then
        select count(*) into flag from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE = echeId and FOU_ORDRE_CLIENT= fouOrdre;
        if (flag = 0) then
            RAISE_APPLICATION_ERROR (-20001,'Le fouOrdre specifie ('|| fouOrdre || ') ne correspond a celui de l''echeancier (ECHE_ECHEANCIER_ORDRE=' || echeId ||  ').');    
        end if;
    end if; 
    
    --select * into laPersonne from grhum.personne where pers_id= persId;
    
    
    if (echeanceMontant = null or echeanceMontant <= 0) then
        RAISE_APPLICATION_ERROR (-20001,'Le montant de l''echeance ne peut etre null ou negatif.');
    end if;      
    
    
    -- recuperer un nouvel id d'echeance
    select jefy_echeancier.echeancier_detail_seq.nextval into echeanceId from dual;

    INSERT INTO MARACUJA.PRELEVEMENT (
        ECHE_ECHEANCIER_ORDRE,
        RECO_ORDRE, 
        FOU_ORDRE,
        PREL_COMMENTAIRE, 
        PREL_DATE_MODIF, 
        PREL_DATE_PRELEVEMENT,
        PREL_PRELEV_DATE_SAISIE,
        PREL_PRELEV_ETAT, 
        PREL_NUMERO_INDEX, 
        PREL_PRELEV_MONTANT,
        PREL_PRELEV_ORDRE, 
        RIB_ORDRE, 
        PREL_ETAT_MARACUJA)
        values (
            echeId,--ECHE_ECHEANCIER_ORDRE
            null,--PREL_FICP_ORDRE
            fouOrdre,--FOU_ORDRE
            commentaire,--PREL_COMMENTAIRE
            sysdate,--DATE_MODIF,--PREL_DATE_MODIF
            echeanceDatePrevue,--PREL_DATE_PRELEVEMENT
            sysdate,--,--PREL_PRELEV_DATE_SAISIE
            'INVALIDE',--PREL_PRELEV_ETAT
            echeanceNumero,--PREL_NUMERO_INDEX
            echeanceMontant,--PREL_PRELEV_MONTANT
            echeanceId,--PREL_PRELEV_ORDRE
            ribOrdre,--RIB_ORDRE
            'INVALIDE'--PREL_ETAT_MARACUJA
            );
            
       
        
        return echeanceId;
end;


procedure verifierEcheancier(echeId number)
is
    flag integer;
    montantCalcule number;
    montant number;
    echeancier_data maracuja.echeancier%rowtype;
begin

    checkEcheancierExiste(echeId);
    
    select count(*) into flag from MARACUJA.PRELEVEMENT where ECHE_ECHEANCIER_ORDRE = echeId;
    if (flag=0) then
        RAISE_APPLICATION_ERROR (-20001,'L''echeancier ('|| echeId || ') n''a pas de prelevements (echeances) associes.');
    end if;    
    if (flag <> echeancier_data.ECHE_NOMBRE_ECHEANCES) then
         RAISE_APPLICATION_ERROR (-20001,'Le nombre d''echeances prevu ('|| echeancier_data.ECHE_NOMBRE_ECHEANCES ||') pour l''echeancier ('|| echeId || ') est incoherent avec le nombre de prelevements trouves ('|| flag ||').');
    end if;
        
    

    -- verifier que le total de l'echeancier est egal a la somme des echeances.
    select  nvl(sum(PREL_PRELEV_MONTANT),0) into montantCalcule   from  MARACUJA.PRELEVEMENT where ECHE_ECHEANCIER_ORDRE = echeId;   
    
    if (montantCalcule <> echeancier_data.ECHE_MONTANT) then
        RAISE_APPLICATION_ERROR (-20001,'Le montant total des echeances ('|| montantCalcule ||') depasse le montant de l''echeancier ('|| montant ||').');
    end if;
    
end;



procedure validerEcheancier(echeId number)
is
    flag integer;
    echeancier_data maracuja.echeancier%rowtype;
begin
    verifierEcheancier(echeId);
    
    select * into echeancier_data   from  MARACUJA.echeancier where ECHE_ECHEANCIER_ORDRE = echeId;   
    
    if (echeancier_data.ECHE_ETAT_PRELEVEMENT <> 'I') then
        RAISE_APPLICATION_ERROR (-20001,'L''echeancier ('|| echeId || ') a deja ete valide (etat <> I).');
    end if;   
    
    select count(*) into flag from MARACUJA.PRELEVEMENT where ECHE_ECHEANCIER_ORDRE = echeId and PREL_ETAT_MARACUJA<>'INVALIDE';
    if (flag>0) then
        RAISE_APPLICATION_ERROR (-20001,'L''echeancier ('|| echeId || ') possede des prelevements non invalides, impossible de les valider.');
    end if;      
    
    
    update maracuja.prelevement set PREL_PRELEV_ETAT='ATTENTE', PREL_ETAT_MARACUJA='ATTENTE' where  ECHE_ECHEANCIER_ORDRE = echeId;    
    update maracuja.echeancier set ECHE_ETAT_PRELEVEMENT='V' where  ECHE_ECHEANCIER_ORDRE = echeId;
     

end;


procedure supprimerEcheancier(echeId number) is
flag integer;
begin
    checkEcheancierExiste(echeId);
    -- verifier que l'echeancier est supprimable (tous les prelevements doivent etre a INVALIDE)
    select count(*) into flag from maracuja.prelevement where eche_echeancier_ordre=echeId and (PREL_PRELEV_ETAT<>'INVALIDE' or PREL_ETAT_MARACUJA <>'INVALIDE');
    if (flag>0) then
        RAISE_APPLICATION_ERROR (-20001,'L''echeancier specifie ('|| echeId || ') est lie a des prelevements non INVALIDES. Impossible de le supprimer.');
    end if; 

    delete from maracuja.echeancier_brouillard where ECHE_ECHEANCIER_ORDRE = echeId;
    delete from maracuja.prelevement where ECHE_ECHEANCIER_ORDRE = echeId;
    delete from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE = echeId;

end;

procedure checkEcheancierExiste(echeId number)
is
    flag integer;
begin
    select count(*) into flag from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE = echeId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Le ECHE_ECHEANCIER_ORDRE specifie ('|| echeId || ') ne correspond pas a un enregistrement de maracuja.echeancier.');    
    end if; 

end;


procedure associerEcheancierEtBob(echeId number, bobOrdre number) is
    cle integer;
begin
    checkEcheancierExiste(echeId);
     -- recuperer un nouvel id 
    select maracuja.echeancier_brouillard_seq.nextval into cle from dual;

    insert into maracuja.echeancier_brouillard(EBR_ID, ECHE_ECHEANCIER_ORDRE, BOB_ORDRE) values (cle, echeId, bobOrdre);
end;

END;
/


GRANT EXECUTE ON  MARACUJA.API_ECHEANCIER TO GARNUCHE;

 
 
 
 CREATE OR REPLACE PACKAGE MARACUJA.scol_echeancier IS

-- API PUBLIQUE pour creer des echeanciers / echeances --

-- renvoie un identifiant d'echeancier
function creerEcheancier3Echeances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscripion date) return number;

-- affecte le bordereau pour l'echeancier
procedure associerEcheancierEtBordereau(echeId number, borId number);

-- affecte l'echeancier au bordereau_brouillard
procedure associerEcheancierEtBob(echeId number, bobOrdre number);

-- verifie l echeancier (controle que l''echeancier est coherent avec ses prelevements)
procedure verifierEcheancier(echeId number);

-- supprime l'echeancier (si c'est possible, sinon exception)
procedure supprimerEcheancier(echeId number);

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.scol_echeancier IS
-- PUBLIC --

function creerEcheancier3Echeances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscripion date) return number
IS
    echeId number;
    flag integer;
    
    bordereau_data maracuja.bordereau%rowtype;
    personne_data  grhum.personne%rowtype;    
   
    echeLibelle maracuja.echeancier.eche_Libelle%type;
    echeReference maracuja.echeancier.ECHE_REF_FACTURE_EXTERNE%type;
    nbEcheances integer;
    date1 date;
    date2 date;
    date3 date;
    moisSuivant date;
    montant1 number(12,2);
    montant2 number(12,2);
    montant3 number(12,2);    
    echeanceId maracuja.prelevement.PREL_PRELEV_ORDRE%type;
    
    delaiMinimalTraitement integer;
    
    
BEGIN
    
    -- verif

    select count(*) into flag from grhum.personne where pers_id = persId;
    if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table GRHUM.PERSONNE pour pers_id = '|| persId ||'.');    
    end if; 
    
    select * into personne_data from grhum.personne where pers_id= persId;  


    -- determiner les valeurs
    --exeOrdre := bordereau_data.exe_ordre;
    echeLibelle := 'INSCRIPTION ETUDIANT '|| personne_data.pers_libelle || ' ' || personne_data.pers_lc;
    echeReference := '';
    nbEcheances := 3;
    delaiMinimalTraitement := 6;
    
    moisSuivant := add_months(dateInscripion, 1);
    
    date1 := to_date( '05/' || to_char(moisSuivant, 'mm/yyyy')   ,'dd/mm/yyyy');
    date2 := add_months(date1,1);
    date3 := add_months(date2,1);
        
    montant3 := floor(montantTotal/3);
    montant2 := floor(montantTotal/3);
    montant1 := montantTotal - montant2 - montant3;
    
    
    
    -- creer l'echeancier
    echeId := MARACUJA.API_ECHEANCIER.CREERECHEANCIER ( exeOrdre, persId, fouOrdre, echeLibelle, echeReference, montantTotal, nbEcheances, date1 );        
    
    -- creer les echeances    
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 1/3', 1, montant1, date1, ribOrdre );
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 2/3', 2, montant2, date2, ribOrdre );
    echeanceId := MARACUJA.API_ECHEANCIER.CREERECHEANCE ( echeId, persId, fouOrdre, echeLibelle || ' - Echéance 3/3', 3, montant3, date3, ribOrdre );
    return echeId;
END;


procedure associerEcheancierEtBordereau(echeId number, borId number)
is
    flag integer;
begin

    select count(*) into flag from maracuja.bordereau where bor_id=borId;
        if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table MARACUJA.BORDEREAU pour bor_id = '|| borId ||'.');    
    end if; 


    select count(*) into flag from maracuja.echeancier where ECHE_ECHEANCIER_ORDRE=echeId;
        if (flag = 0) then
        RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans la table MARACUJA.ECHEANCIER pour ECHE_ECHEANCIER_ORDRE = '|| echeId ||'.');    
    end if; 

    -- 


    update maracuja.echeancier set bor_id=borId where ECHE_ECHEANCIER_ORDRE=echeId;


end;


procedure verifierEcheancier(echeId number)
is
begin
    MARACUJA.API_ECHEANCIER.verifierEcheancier ( echeId );
end;

procedure supprimerEcheancier(echeId number)
is
begin
    MARACUJA.API_ECHEANCIER.supprimerEcheancier ( echeId );
end;

procedure associerEcheancierEtBob(echeId number, bobOrdre number) is
begin
    MARACUJA.API_ECHEANCIER.associerEcheancierEtBob ( echeId, bobOrdre );
end;

END;
/


GRANT EXECUTE ON  MARACUJA.SCOL_ECHEANCIER TO GARNUCHE;

 
CREATE OR REPLACE PACKAGE MARACUJA.API_EMARGEMENT  IS

    function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer;
     
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER);
    
    function creerEmargement1D1C(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type, utlOrdre integer) return integer;
    
    
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.API_EMARGEMENT IS

    function creerEmargement1D1C(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type, utlOrdre integer) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens = ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail sont dans le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme compte pco_num ('|| ecdSource.pco_num ||', '|| ecdDest.pco_num||'), ecd_ordre ('|| ecdOrdreSource || ', '|| ecdordreDest ||') .');
            end if;

            -- verifier que les reste a emarger sont suffisants
            if (ecdSource.ecd_reste_emarger = 0) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja totalement emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger = 0) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja totalement emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices des ecritures sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(ecd_reste_emarger) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            select com_ordre into comordre from ecriture where ecr_ordre = ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre = ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            
            utlOrdre ecriture.utl_ordre%type;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les ecriture_detail ne sont pas emargees
            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les montant s'annulent
            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            -- trouver l'utilisateur
            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       
       
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER) AS
        flag integer;
        emargementdetail EMARGEMENT_DETAIL%ROWTYPE;
        CURSOR c1 IS SELECT * INTO emargementdetail FROM EMARGEMENT_DETAIL WHERE ema_ordre = emaordre;
    BEGIN
        select count(*) into flag from emargement where ema_etat='VALIDE' and ema_ordre = emaordre;
        
        if (flag=1) then
             OPEN c1;
            LOOP
            FETCH c1 INTO emargementdetail;
            EXIT WHEN c1%NOTFOUND;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_source;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_destination;

            END LOOP;
            CLOSE c1;
   
            -- annulation de la emargement
            UPDATE EMARGEMENT SET ema_etat = 'ANNULE'
            WHERE ema_ordre = emaordre;        
        
        end if;

       
   
    END;
    
    
    
        
END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER);

PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;

PROCEDURE prv_apres_visa_bordereau_2006 (borid INTEGER);
PROCEDURE prv_apres_visa_bordereau (borid INTEGER);
PROCEDURE prv_apres_visa_reversement(borid INTEGER);
PROCEDURE prv_apres_reimputation_2006 (reiordre INTEGER);


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Afaireaprestraitement IS

-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007


PROCEDURE apres_visa_bordereau (borid INTEGER)
IS
  ex INTEGER;
BEGIN
     SELECT exe_ordre INTO ex FROM BORDEREAU WHERE bor_id=borid;
     IF (ex < 2007) THEN
         prv_apres_visa_bordereau_2006(borid);
     ELSE
          prv_apres_visa_bordereau(borid);
     END IF;

END;



PROCEDURE prv_apres_visa_bordereau(borid INTEGER)
IS
  leBordereau BORDEREAU%ROWTYPE;
  leSousType TYPE_BORDEREAU.TBO_SOUS_TYPE%TYPE;
BEGIN
     SELECT * INTO leBordereau FROM BORDEREAU WHERE bor_id=borid;
     SELECT TBO_SOUS_TYPE INTO leSousType FROM TYPE_BORDEREAU WHERE tbo_ordre = leBordereau.tbo_ordre;
     IF leBordereau.bor_etat = 'VISE' THEN
             IF (leSousType = 'REVERSEMENTS') THEN
                 prv_apres_visa_reversement(borid);
             END IF;
              emarger_visa_bord_prelevement(borid);
    END IF;

END;


-- permet de transmettre a jefy_depense les mandats de reversements vises
-- les mandats rejetes sont traites au moment du visa du bordereau de rejet
PROCEDURE prv_apres_visa_reversement(borid INTEGER)
IS
  manid MANDAT.man_id%TYPE;

  CURSOR c_mandatVises IS
           SELECT man_id FROM MANDAT WHERE bor_id=borid AND man_etat='VISE';

BEGIN
     OPEN c_mandatVises;
        LOOP
            FETCH c_mandatVises INTO manid;
                  EXIT WHEN c_mandatVises%NOTFOUND;
            jefy_depense.apres_visa.viser_reversement(manid);
        END LOOP;
        CLOSE c_mandatVises;
END;



PROCEDURE prv_apres_visa_bordereau_2006 (borid INTEGER)
IS

  tbotype  TYPE_BORDEREAU.tbo_type%TYPE;
  cpt INTEGER;
  lebordereau BORDEREAU%ROWTYPE;
  brjordre MANDAT.brj_ordre%TYPE;
  exeordre MANDAT.exe_ordre%TYPE;
  nbmandats INTEGER;
  nbtitres INTEGER;
  ex INTEGER;


  CURSOR c1 IS SELECT DISTINCT brj_ordre,exe_ordre FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
  CURSOR c2 IS SELECT DISTINCT brj_ordre,exe_ordre FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;

BEGIN
  SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

  SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;


  --raise_application_error (-20001, ''||ex|| '=' ||lebordereau.exe_ordre);


  IF lebordereau.exe_ordre=ex THEN

          -- mettre a jour le bordereau de jefy
          UPDATE jefy.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
            WHERE bor_ordre=lebordereau.bor_ordre;



            SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

            IF (nbmandats > 0) THEN
                -- mettre a jour les mandats VISE
                UPDATE jefy.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de mandats
                  OPEN C1;
                LOOP
                  FETCH C1 INTO brjordre,exeordre;
                  EXIT WHEN c1%NOTFOUND;
                  Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
                END LOOP;
                CLOSE C1;
            END IF;



            SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

            IF (nbtitres > 0) THEN
                -- mettre a jour les titres VISE
                UPDATE jefy.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de titres
                  OPEN C2;
                LOOP
                  FETCH C2 INTO brjordre,exeordre;
                  EXIT WHEN c2%NOTFOUND;
                  Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
                END LOOP;
                CLOSE C2;
            END IF;


            emarger_visa_bord_prelevement(borid);


  ELSE
        IF lebordereau.exe_ordre=2005 THEN

          -- mettre a jour le bordereau de jefy
          UPDATE jefy05.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
            WHERE bor_ordre=lebordereau.bor_ordre;



            SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

            IF (nbmandats > 0) THEN
                -- mettre a jour les mandats VISE
                UPDATE jefy05.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de mandats
                  OPEN C1;
                LOOP
                  FETCH C1 INTO brjordre,exeordre;
                  EXIT WHEN c1%NOTFOUND;
                  Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
                END LOOP;
                CLOSE C1;
            END IF;



            SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

            IF (nbtitres > 0) THEN
                -- mettre a jour les titres VISE
                UPDATE jefy05.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

                -- expedier le bordereau de rejet de titres
                  OPEN C2;
                LOOP
                  FETCH C2 INTO brjordre,exeordre;
                  EXIT WHEN c2%NOTFOUND;
                  Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
                END LOOP;
                CLOSE C2;
            END IF;
         ELSE
              RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');
      END IF;
  END IF;

END ;



PROCEDURE apres_reimputation (reiordre INTEGER)
IS
    ex INTEGER;
    manid mandat.man_id%type;
    titid titre.tit_id%type;
    pconumnouveau plan_comptable.pco_num%type;
    pconumancien plan_comptable.pco_num%type;
BEGIN

     SELECT exe_ordre INTO ex FROM REIMPUTATION WHERE rei_ordre=reiordre;
     IF (ex < 2007) THEN
         prv_apres_reimputation_2006 (reiordre);
     ELSE          
        SELECT man_id, tit_id, pco_num_nouveau, pco_num_ancien INTO manid, titid, pconumnouveau, pconumancien FROM REIMPUTATION WHERE rei_ordre=reiordre;
        IF manid IS NOT NULL THEN
            update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;        
        end if;          
          
        IF titid IS NOT NULL THEN
            update jefy_recette.recette_ctrl_planco set pco_num=pconumnouveau where tit_id=titid and pco_num=pconumancien;        
        end if;          
          
     END IF;

END;

PROCEDURE prv_apres_reimputation_2006 (reiordre INTEGER)
IS
cpt INTEGER;
manid INTEGER;
titid INTEGER;
manordre INTEGER;
titordre INTEGER;
pconumnouveau INTEGER;
depordre INTEGER;
exeordre INTEGER;
ex INTEGER;
BEGIN

     SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

    -- mettre a jour le mandat ou le titre dans JEFY
    SELECT man_id, tit_id, pco_num_nouveau INTO manid, titid, pconumnouveau FROM REIMPUTATION WHERE rei_ordre=reiordre;

    IF manid IS NOT NULL THEN
       SELECT man_ordre, MANDAT.exe_ordre INTO manordre, exeordre FROM MANDAT, BORDEREAU b WHERE man_id = manid AND MANDAT.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTME','BTMS','BTPI'));

       IF exeordre=ex THEN
           UPDATE jefy.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
           UPDATE jefy.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
       ELSE IF exeordre=2005 THEN
               UPDATE jefy05.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
               UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
            END IF;
       END IF;
    END IF;


    IF titid IS NOT NULL THEN
       SELECT tit_ordre, TITRE.exe_ordre INTO titordre, exeordre FROM TITRE, BORDEREAU b WHERE tit_id = titid  AND TITRE.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTTE','BTPI'));


       IF exeordre=ex THEN
           UPDATE jefy.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
           -- pour les ORV, des fois que...
           SELECT dep_ordre INTO depordre FROM jefy.TITRE WHERE tit_ordre=titordre;
           IF (depordre IS NOT NULL ) THEN
                 UPDATE jefy.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
           END IF;

       ELSE IF exeordre=2005 THEN
           UPDATE jefy05.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
           -- pour les ORV, des fois que...
           SELECT dep_ordre INTO depordre FROM jefy05.TITRE WHERE tit_ordre=titordre;
           IF (depordre IS NOT NULL ) THEN
                 UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
           END IF;

            END IF;
       END IF;

    END IF;

END ;

PROCEDURE apres_paiement (paiordre INTEGER)
IS
cpt INTEGER;
BEGIN
             --SELECT 1 INTO  cpt FROM dual;
         emarger_paiement(paiordre);

END ;


PROCEDURE emarger_paiement(paiordre INTEGER)
IS

CURSOR mandats (lepaiordre INTEGER ) IS
 SELECT * FROM MANDAT
 WHERE pai_ordre = lepaiordre;

CURSOR non_emarge_debit (lemanid INTEGER) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='D';

CURSOR non_emarge_credit_compte (lemanid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='C';

lemandat maracuja.MANDAT%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lepaiement maracuja.PAIEMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lepaiement FROM PAIEMENT
 WHERE pai_ordre = paiordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lepaiement.exe_ordre,
  3,
  lepaiement.UTL_ORDRE,
  lepaiement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les mandats du paiement
OPEN mandats(paiordre);
LOOP
FETCH mandats INTO lemandat;
EXIT WHEN mandats%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_debit (lemandat.man_id);
 LOOP
 FETCH non_emarge_debit INTO ecriture_debit;
 EXIT WHEN non_emarge_debit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_credit_compte (lemandat.man_id,ecriture_debit.pco_num);
 LOOP
 FETCH non_emarge_credit_compte INTO ecriture_credit;
 EXIT WHEN non_emarge_credit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_debit.ecd_ordre,
  ecriture_credit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  lemandat.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_credit_compte;

END LOOP;
CLOSE non_emarge_debit;

END LOOP;
CLOSE mandats;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;




PROCEDURE apres_recouvrement_releve(recoordre INTEGER)
IS
BEGIN
         emarger_prelevement_releve(recoordre);
END ;

PROCEDURE emarger_prelevement_releve(recoordre INTEGER)
IS
cpt INTEGER;
BEGIN
     -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
             SELECT 1 INTO  cpt FROM dual;
                        emarger_prelevement(recoordre);
END;


PROCEDURE apres_recouvrement (recoordre INTEGER)
IS
BEGIN
         emarger_prelevement(recoordre);
END ;

PROCEDURE emarger_prelevement(recoordre INTEGER)
IS
begin
    if (recoordre is null) then
        RAISE_APPLICATION_ERROR (-20001,'Le parametre recoordre est null.');
    end if;
    emarger_prelev_ac_titre(recoordre);
    emarger_prelev_ac_ecr(recoordre);
end;



-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER) is

    CURSOR titres (lerecoordre INTEGER ) IS
     SELECT * FROM TITRE
     WHERE tit_id IN
     (SELECT tit_id FROM PRELEVEMENT p , ECHEANCIER e
     WHERE e.eche_echeancier_ordre  = p.eche_echeancier_ordre
     AND p.reco_ordre = lerecoordre)
     ;

    CURSOR non_emarge_credit (letitid INTEGER) IS
     SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
     WHERE tit_id = letitid
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger != 0
     AND e.ecd_sens ='C';

    CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
     SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
     WHERE tit_id = letitid
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger != 0
     AND pco_num = lepconum
     AND e.ecd_sens ='D';

    letitre maracuja.TITRE%ROWTYPE;
    ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
    ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
    EMAORDRE EMARGEMENT.ema_ordre%TYPE;
    lerecouvrement maracuja.RECOUVREMENT%ROWTYPE;
    cpt INTEGER;

BEGIN

        -- recup infos
         SELECT * INTO lerecouvrement FROM RECOUVREMENT
         WHERE reco_ordre = recoordre;

        -- creation de l emargement !
         SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

         INSERT INTO EMARGEMENT
          (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
         VALUES
          (
          SYSDATE,
          -1,
          EMAORDRE,
          lerecouvrement.exe_ordre,
          3,
          lerecouvrement.UTL_ORDRE,
          lerecouvrement.COM_ORDRE,
          0,
          'VALIDE'
          );

        -- on fetch les titres du recrouvement
        OPEN titres(recoordre);
        LOOP
        FETCH titres INTO letitre;
        EXIT WHEN titres%NOTFOUND;
        -- on recupere les ecritures non emargees debits
         OPEN non_emarge_credit (letitre.tit_id);
         LOOP
         FETCH non_emarge_credit INTO ecriture_credit;
         EXIT WHEN non_emarge_credit%NOTFOUND;

        -- on recupere les ecritures non emargees credit
         OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
         LOOP
         FETCH non_emarge_debit_compte INTO ecriture_debit;
         EXIT WHEN non_emarge_debit_compte%NOTFOUND;

         IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
         THEN
          -- creation de l emargement detail !
          INSERT INTO EMARGEMENT_DETAIL
          (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
          VALUES
          (
          ecriture_credit.ecd_ordre,
          ecriture_debit.ecd_ordre,
          EMAORDRE,
          ecriture_credit.ecd_reste_emarger,
          emargement_detail_seq.NEXTVAL,
          ecriture_debit.exe_ordre
          );

          -- maj de l ecriture debit
          UPDATE ECRITURE_DETAIL SET
          ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
          WHERE ecd_ordre = ecriture_debit.ecd_ordre;

          -- maj de lecriture credit
          UPDATE ECRITURE_DETAIL SET
          ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
          WHERE ecd_ordre = ecriture_credit.ecd_ordre;
         END IF;

         END LOOP;
         CLOSE non_emarge_debit_compte;

        END LOOP;
        CLOSE non_emarge_credit;

        END LOOP;
        CLOSE titres;
        -- suppression de lemagenet si pas de details;
        SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
        WHERE ema_ordre = EMAORDRE;

        IF cpt = 0 THEN
            DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
        else
            Numerotationobject.numeroter_emargement(EMAORDRE);
        END IF;



END;




-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER)
is
  CURSOR lesPrelevements (lerecoordre INTEGER ) IS
     SELECT * FROM maracuja.prelevement
     WHERE reco_ordre=lerecoordre;
     
  ecrDetailCredit ecriture_detail%rowtype;       
  ecrDetailDebit ecriture_detail%rowtype;
    ecriture_credit ecriture_detail%rowtype;    
    ecriture_debit ecriture_detail%rowtype;         
  lePrelevement maracuja.prelevement%rowtype;
  flag integer;
  flag2 integer;
  retVal integer;
  utlOrdre integer;
  
    CURSOR non_emarge_credit (prelevOrdre INTEGER) IS
     SELECT e.* FROM prelevement_DETAIL_ECR t , ECRITURE_DETAIL e
     WHERE PREL_PRELEV_ORDRE = prelevOrdre
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger <> 0
     AND e.ecd_sens ='C';

    CURSOR non_emarge_debit_compte (prelevOrdre INTEGER,lepconum VARCHAR) IS
     SELECT e.* FROM prelevement_DETAIL_ECR t , ECRITURE_DETAIL e
     WHERE PREL_PRELEV_ORDRE = prelevOrdre
     AND e.ecd_ordre = t.ecd_ordre
     AND ecd_reste_emarger <> 0
     AND pco_num = lepconum
     AND e.ecd_sens ='D';  
    
begin

        select utl_ordre into utlOrdre from recouvrement where reco_ordre=recoordre;


        OPEN lesPrelevements(recoordre);
        LOOP
            FETCH lesPrelevements INTO lePrelevement;
            EXIT WHEN lesPrelevements%NOTFOUND;
            ecrDetailCredit := null;
            ecrDetailDebit := null;
            
            
            -- recuperation de l'ecriture de l'echeancier non emargee en debit
            SELECT count(*) into flag2 FROM maracuja.Ecriture_detail 
             where ecd_reste_emarger<>0 and ecd_debit<>0 and ecd_ordre in (
             select ecd_ordre from maracuja.echeancier_detail_ecr  
             WHERE ECHE_ECHEANCIER_ORDRE=lePrelevement.ECHE_ECHEANCIER_ORDRE);
            if (flag2=1) then
                SELECT * into ecrDetailDebit FROM maracuja.Ecriture_detail 
                 where ecd_reste_emarger<>0 and ecd_debit<>0 and ecd_ordre in (
                 select ecd_ordre from maracuja.echeancier_detail_ecr  
                 WHERE ECHE_ECHEANCIER_ORDRE=lePrelevement.ECHE_ECHEANCIER_ORDRE);
             
             
                 -- s'il y a une ecriture non emargee en debit sur l'echeancier, 
                 -- on emarge avec le credit correspondant du prelevement
                 -- recuperation de l'ecriture du prelevement non emargee en credit
             
                SELECT count(*) into flag FROM maracuja.Ecriture_detail 
                 where ecd_reste_emarger<>0 and ecd_credit<>0 and pco_num=ecrDetailDebit.pco_num and ecd_ordre in (
                 select ecd_ordre from maracuja.prelevement_detail_ecr  
                 WHERE PREL_PRELEV_ORDRE=lePrelevement.PREL_PRELEV_ORDRE);
                if (flag=1) then
                     SELECT * into ecrDetailCredit FROM maracuja.Ecriture_detail 
                     where ecd_reste_emarger<>0 and ecd_credit<>0 and pco_num=ecrDetailDebit.pco_num and ecd_ordre in (
                     select ecd_ordre from maracuja.prelevement_detail_ecr  
                     WHERE PREL_PRELEV_ORDRE=lePrelevement.PREL_PRELEV_ORDRE);
                    -- si les deux ecritures ont ete recuperees, on les emarge  
                    IF (Afaireaprestraitement.verifier_emar_exercice(ecrDetailCredit.ecr_ordre,ecrDetailDebit.ecr_ordre) = 1) then          
                        retVal := MARACUJA.API_EMARGEMENT.CREEREMARGEMENT1D1C ( ecrDetailCredit.ecd_ordre, ecrDetailDebit.ecd_ordre, 3, UTLORDRE );
                    end if;             
                end if;

            end if;                        
            
            
            -- emargement entre les debits et credits associes au prelevement 
            -- (suite a saisie releve)
             
            -- on recupere les ecritures non emargees debits
             OPEN non_emarge_credit (lePrelevement.prel_prelev_ordre );
             LOOP
             FETCH non_emarge_credit INTO ecriture_credit;
             EXIT WHEN non_emarge_credit%NOTFOUND;

                -- on recupere les ecritures non emargees credit
                 OPEN non_emarge_debit_compte (lePrelevement.prel_prelev_ordre,ecriture_credit.pco_num);
                 LOOP
                 FETCH non_emarge_debit_compte INTO ecriture_debit;
                 EXIT WHEN non_emarge_debit_compte%NOTFOUND;
                    
                     IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
                     THEN
                        retVal := MARACUJA.API_EMARGEMENT.CREEREMARGEMENT1D1C ( ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, UTLORDRE );         
                     END IF;

                 END LOOP;
                 CLOSE non_emarge_debit_compte;

            END LOOP;
            CLOSE non_emarge_credit;            
            
            
            
            

            
   
            
         END LOOP;
        CLOSE lesPrelevements;

end;





PROCEDURE emarger_visa_bord_prelevement(borid INTEGER)
IS


CURSOR titres (leborid INTEGER ) IS
 SELECT * FROM TITRE
 WHERE bor_id = leborid;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lebordereau maracuja.BORDEREAU%ROWTYPE;
cpt INTEGER;
comordre INTEGER;
BEGIN

-- recup infos
 SELECT * INTO lebordereau FROM BORDEREAU
 WHERE bor_id = borid;
-- recup du com_ordre
SELECT com_ordre  INTO comordre
FROM GESTION WHERE ges_ordre = lebordereau.ges_code;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lebordereau.exe_ordre,
  3,
  lebordereau.UTL_ORDRE,
  comordre,
  0,
  'VALIDE'
  );

-- on fetch les titres du recouvrement
OPEN titres(borid);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
    DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
else
    Numerotationobject.numeroter_emargement(EMAORDRE);
END IF;

END;


FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER
IS
reponse INTEGER;
execredit EXERCICE.EXE_ORDRE%TYPE;
exedebit EXERCICE.EXE_ORDRE%TYPE;
BEGIN
-- init
reponse :=0;

SELECT exe_ordre INTO execredit FROM ECRITURE
WHERE ecr_ordre = ecrcredit;

SELECT exe_ordre INTO exedebit FROM ECRITURE
WHERE ecr_ordre = ecrdebit;

IF exedebit = execredit THEN
 RETURN 1;
ELSE
 RETURN 0;
END IF;


END;


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Util  IS

    PROCEDURE annuler_visa_bor_mandat(borid INTEGER);

    PROCEDURE supprimer_visa_btme(borid INTEGER);
    
    PROCEDURE supprimer_visa_btms(borid INTEGER);

    PROCEDURE supprimer_visa_btte(borid INTEGER);

    procedure creer_ecriture_annulation(ecrordre INTEGER);
    
    function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer;
     
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER);
    
    procedure supprimer_bordereau_dep(borid integer);
    
    procedure supprimer_bordereau_rec(borid integer);
    
    procedure supprimer_bordereau_btms(borid integer);
    
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer);
    
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Util IS

    -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
    -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

 -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE annuler_visa_bor_mandat(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;
                 cursor lesecrs is
                        select distinct ecr_ordre from ecriture_detail ecd, mandat_detail_ecriture mde where ecd.ecd_ordre=mde.ecd_ordre and mde.man_id=tmpmanid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME' and tbotype <> 'BTRU') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME ou BTRU');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandats associ�s au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                                open lesecrs;
                                loop
                                    fetch lesecrs into tmpEcrOrdre;
                                    EXIT WHEN lesecrs%NOTFOUND;
                                    creer_ecriture_annulation(tmpecrOrdre);
                                   
                                end loop;
                                close lesecrs;
                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
--             
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

--                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

--                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;



       -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
       PROCEDURE supprimer_visa_btme(borid INTEGER) IS
                    flag INTEGER;                 
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmpmanid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lesmandats IS
                         SELECT man_id FROM MANDAT WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTME') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTME');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lesmandats;
             LOOP
                              FETCH lesmandats INTO tmpmanid;
                             EXIT WHEN lesmandats%NOTFOUND;
                            
                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
                            if (flag>0) then
                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
                            end if;
                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;


             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;


 -- suppression des ecritures du visa d'un bordereau de depense BTMS 
       PROCEDURE supprimer_visa_btms(borid INTEGER) IS
         flag INTEGER;
         lebordereau BORDEREAU%ROWTYPE;
         tbotype TYPE_BORDEREAU.tbo_type%TYPE;
         tmpmanid MANDAT.man_id%TYPE;
         tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

         CURSOR lesecritures IS
         SELECT distinct ed.ecr_ordre from ecriture_detail ed, mandat_detail_ecriture mde, mandat m WHERE m.man_id = mde.man_id and mde.ecd_ordre= ed.ecd_ordre and m.bor_id = borid;

         CURSOR lesmandats IS
         SELECT man_id FROM MANDAT WHERE bor_id = borid;

         BEGIN
           SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTMS') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTMS');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat = 'VALIDE'  ) THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas VISE ');
            END IF;

                  -- verif bordereau de mandats
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun mandat associ� au bordereau bor_id= '|| borid);
               END IF;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ�s au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

            --verif si mandat rejete
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ont ete emargees, Impossible d''annuler le visa.');
               END IF;
--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

--            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associ� au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;

            OPEN lesecritures;
             LOOP
              FETCH lesecritures INTO tmpEcrOrdre;
             EXIT WHEN lesecritures%NOTFOUND;

                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE ecd_ordre in (select ecd_ordre from ecriture_detail where ecr_ordre = tmpEcrOrdre);

                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;

                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

             END LOOP;
             CLOSE lesecritures;


            OPEN lesmandats;
            LOOP
            FETCH lesmandats INTO tmpmanid;
            EXIT WHEN lesmandats%NOTFOUND;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
                UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;

             END LOOP;
             CLOSE lesmandats;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

            UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_visa = null,ecr_ordre_sacd = null,ecr_ordre_opp = null  WHERE bor_id IN (borid);

       END;




   -- annulation du visa d'un bordereau de depense BTTE
       PROCEDURE supprimer_visa_btte(borid INTEGER) IS
                    flag INTEGER;
                 lebordereau BORDEREAU%ROWTYPE;
                 tbotype TYPE_BORDEREAU.tbo_type%TYPE;
                 tmptitid MANDAT.man_id%TYPE;
                 tmpecrordre ECRITURE_DETAIL.ecr_ordre%TYPE;

                 CURSOR lestitres IS
                         SELECT tit_id FROM TITRE WHERE bor_id = borid;

       BEGIN
               SELECT COUNT(*) INTO flag FROM BORDEREAU WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun bordereau correspondant au bor_id= '|| borid);
               END IF;

            SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;

            -- verif si exercice >2006
            IF (lebordereau.exe_ordre < 2007) THEN
               RAISE_APPLICATION_ERROR (-20001,'Impossible d''annuler le visa d''un bordereau emis avant 2007');
            END IF;

            -- verif type bordereau BTME
            SELECT  TBO_TYPE INTO tbotype FROM TYPE_BORDEREAU WHERE tbo_ordre=lebordereau.tbo_ordre;
            IF (tbotype <> 'BTTE') THEN
                    RAISE_APPLICATION_ERROR (-20001,'Le type de bordereau n''est pas BTTE');
               END IF;

            -- verif si le bordereau n''est pas vise
            IF (lebordereau.bor_etat <> 'VISE') THEN
               RAISE_APPLICATION_ERROR (-20001,'Le bordereau correspondant au bor_id= '|| borid || ' n''est pas a l''etat VISE ');
            END IF;

                  -- verif bordereau de titres
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid;
            IF (flag=0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Aucun titre associ� au bordereau bor_id= '|| borid);
               END IF;


            --verif si titre rejete
            SELECT COUNT(*) INTO flag FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associ�s au bordereau bor_id= '|| borid || ' ont ete rejetes, Impossible d''annuler le visa.');
               END IF;

            -- verifier si ecritures emargees
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.ecd_ordre=ecd.ecd_ordre AND ABS(ecd.ECD_RESTE_EMARGER)<>ABS(ecd_montant) ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux titres ont ete emargees, Impossible d''annuler le visa.');
               END IF;
            -- verifier si ecritures <> VISA
            SELECT COUNT(*) INTO flag FROM TITRE m , TITRE_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.TIT_ID=m.TIT_id AND mde.tde_origine<>'VISA' ;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associ�es aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
               END IF;

            -- verifier si reimputations
            SELECT COUNT(*) INTO flag FROM TITRE m , REIMPUTATION r WHERE bor_id=borid AND r.TIT_ID=m.TIT_id;
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains titres associ�s au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
               END IF;



            OPEN lestitres;
             LOOP
                              FETCH lestitres INTO tmptitid;
                             EXIT WHEN lestitres%NOTFOUND;

                            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, TITRE_DETAIL_ECRITURE mde
                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.tit_id=tmptitid;

                            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
                            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

                            DELETE FROM TITRE_DETAIL_ECRITURE WHERE tit_id = tmptitid;
                            UPDATE TITRE SET tit_etat='ATTENTE' WHERE tit_id=tmptitid;


             END LOOP;
             CLOSE lestitres;

            UPDATE BORDEREAU SET bor_etat='VALIDE' WHERE bor_id=borid;
            UPDATE BORDEREAU SET bor_date_visa=NULL WHERE bor_id=borid;
            UPDATE BORDEREAU SET utl_ordre_visa=NULL WHERE bor_id=borid;

       END;
       
       
       
       procedure creer_ecriture_annulation(ecrordre INTEGER) is
            ecr maracuja.ecriture%rowtype;
            ecd maracuja.ecriture_detail%rowtype;
            flag integer;
            str ecriture.ecr_libelle%type;
            newEcrOrdre ecriture.ecr_ordre%type;
            newEcdOrdre ecriture_detail.ecd_ordre%type;
            x  integer;
            cursor c1 is select * from maracuja.ecriture_detail where ecr_ordre=ecrordre;
       
       begin
            
            
            select count(*) into flag from maracuja.ecriture where ecr_ordre=ecrordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture retrouvee pour ecr_ordre= '|| ecrordre || ' .');
            end if;

            select * into ecr from maracuja.ecriture where ecr_ordre=ecrordre;              
            
            str := 'Annulation Ecriture '|| ecr.exe_ordre || '/'||ecr.ecr_numero;
            
            -- verifier que l'ecriture n'a pas deja ete annulee
            select count(*) into flag from maracuja.ecriture where ecr_libelle = str; 
            if (flag > 0) then
                select ecr_numero into flag from maracuja.ecriture where ecr_libelle = str; 
                RAISE_APPLICATION_ERROR (-20001,'L''ecriture numero '||ecr.ecr_numero ||' (ecr_ordre= '|| ecrordre || ') a deja ete annulee par l''ecriture numero '|| flag ||'.');
            end if;
                   
            
            -- verifier que les details ne sont pas emarges
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                if (ecd.ecd_reste_emarger<abs(ecd.ecd_montant) ) then
                    RAISE_APPLICATION_ERROR (-20001,'L''ecriture '||ecr.ecr_numero ||' ecr_ordre= '|| ecrordre || ' a ete emargee pour le compte '||ecd.pco_num||'. Impossible d''annuler');                    
                end if;

             END LOOP;
             CLOSE c1;


--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...              
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--                    
--                    

--             END LOOP;
--             CLOSE c1;

            newEcrOrdre := api_plsql_journal.creerecriture(
                ecr.com_ordre,
                SYSDATE,
                str,
                ecr.exe_ordre,
                ecr.ori_ordre,
                ecr.tjo_ordre,
                ecr.top_ordre,
                ecr.utl_ordre);            
            
            OPEN c1;
             LOOP
                 FETCH c1 INTO ecd;
                    EXIT WHEN c1%NOTFOUND;
                newEcdOrdre := api_plsql_journal.creerEcritureDetail (
                        ecd.ecd_commentaire,
                        ecd.ecd_libelle,  
                        -ecd.ECD_MONTANT,
                        ecd.ECD_SECONDAIRE,
                        ecd.ECD_SENS,
                        newEcrOrdre,
                        ecd.GES_CODE,
                        ecd.PCO_NUM );
                        
                x := creerEmargementMemeSens(ecd.ecd_ordre, newEcdOrdre, 1);
             END LOOP;
             CLOSE c1;                  
            NUMEROTATIONOBJECT.numeroter_ecriture(newEcrOrdre);

       end;
           
       
       
       
       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
            emaordre EMARGEMENT.EMA_ORDRE%type;
            ecdSource ecriture_detail%rowtype;       
            ecdDest ecriture_detail%rowtype;
            
            utlOrdre ecriture.utl_ordre%type;
            comOrdre ecriture.com_ordre%type;
            exeOrdre ecriture.exe_ordre%type;
            emaMontant emargement.ema_montant%type;
            flag integer;
       BEGIN

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
            end if;            

            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
            end if;            


            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

            -- verifier que les ecriture_detail sont sur le meme sens
            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;
            
            -- verifier que les ecriture_detail ont le meme compte
            if (ecdSource.pco_num <> ecdDest.pco_num) then
                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les ecriture_detail ne sont pas emargees
            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
            end if;
            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
            end if;            


            -- verifier que les montant s'annulent
            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;

            -- verifier que les exercices sont les memes
            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
            end if;


            -- trouver le montant a emarger
            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;   

            -- trouver l'utilisateur
            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;


            -- creation de l emargement
             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

             INSERT INTO EMARGEMENT
              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
             VALUES
              (
              SYSDATE,
              0,
              EMAORDRE,
              ecdSource.exe_ordre,
              typeEmargement,
              utlOrdre,
              comordre,
              emaMontant,
              'VALIDE'
              );

              -- creation de l emargement detail 
              INSERT INTO EMARGEMENT_DETAIL
              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
              VALUES
              (
              ecdSource.ecd_ordre,
              ecdDest.ecd_ordre,
              EMAORDRE,
              emaMontant,
              emargement_detail_seq.NEXTVAL,
              exeOrdre
              );

              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;


              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);  

              return emaOrdre;
       END;
       
       
       
    PROCEDURE ANNULER_EMARGEMENT (emaordre INTEGER) AS
        flag integer;
        emargementdetail EMARGEMENT_DETAIL%ROWTYPE;
        CURSOR c1 IS SELECT * INTO emargementdetail FROM EMARGEMENT_DETAIL WHERE ema_ordre = emaordre;
    BEGIN
        select count(*) into flag from emargement where ema_etat='VALIDE' and ema_ordre = emaordre;
        
        if (flag=1) then
             OPEN c1;
            LOOP
            FETCH c1 INTO emargementdetail;
            EXIT WHEN c1%NOTFOUND;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_source;

                UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger + emargementdetail.emd_montant
                WHERE ecd_ordre = emargementdetail.ecd_ordre_destination;

            END LOOP;
            CLOSE c1;
   
            -- annulation de la emargement
            UPDATE EMARGEMENT SET ema_etat = 'ANNULE'
            WHERE ema_ordre = emaordre;        
        
        end if;

       
   
    END;
    
    
    
        
    procedure supprimer_bordereau_dep(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des mandats
        select count(*) into flag from mandat where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de depense.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les mandat_brouillards
        DELETE FROM maracuja.mandat_BROUILLARD WHERE man_id in (select man_id from mandat where bor_id=borid);
                
        -- supprimer les depenses
        DELETE FROM maracuja.depense WHERE man_id in (select man_id from mandat where bor_id=borid);

        -- mettre a jour les depense_ctrl_planco
        UPDATE jefy_depense.depense_ctrl_planco SET man_id = NULL WHERE man_id IN (SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid) ;

        -- supprimer les mandats        
        delete from  maracuja.mandat where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
        
        
    end;
    
    
    procedure supprimer_bordereau_btms(borid integer) as
        flag number;
    begin
        select count(*) into flag from bordereau b, type_bordereau tb where bor_id=borid and b.tbo_ordre=tb.tbo_ordre and tb.TBO_TYPE='BTMS';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas ou n''est pas de type BTMS');
        end if;
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;          
        
        UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
        
        supprimer_bordereau_dep(borid);
        
    end;
    
    
    
    procedure supprimer_bordereau_rec(borid integer)  as
        flag number;
    begin
        select count(*) into flag from bordereau where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''existe pas.');
        end if;
        
        --verifier que le bordereau a bien des titres
        select count(*) into flag from titre where bor_id=borid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' n''est pas un bordereau de recette.');
        end if;
        
        
        -- verifier que le bordereau n'est pas vise
        select count(*) into flag from bordereau where bor_id=borid and bor_etat='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le bordereau borid= '|| borid ||' a deja ete vise.');
        end if;        
        
        -- supprimer les BORDEREAU_INFO
        DELETE FROM maracuja.BORDEREAU_INFO WHERE bor_id IN (borid);
        
        -- supprimer les BORDEREAU_BROUILLARD
        DELETE FROM maracuja.BORDEREAU_BROUILLARD WHERE bor_id IN (borid);

        -- supprimer les titre_brouillards
        DELETE FROM maracuja.titre_BROUILLARD WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);
                
        -- supprimer les recettes
        DELETE FROM maracuja.recette WHERE tit_id in (select tit_id from maracuja.titre where bor_id=borid);

        -- mettre a jour les recette_ctrl_planco
        UPDATE jefy_recette.recette_ctrl_planco SET tit_id = NULL WHERE tit_id IN (SELECT tit_id FROM maracuja.titre WHERE bor_id = borid) ;

        -- supprimer les titres        
        delete from  maracuja.titre where bor_id=borid;
        
        -- supprimer le bordereau        
        delete from  maracuja.bordereau where bor_id=borid;
    
    end;
 



    /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
    PROCEDURE annuler_recouv_prelevement(recoordre INTEGER, ecrNumeroMin integer, ecrNumeroMax integer) IS
        flag INTEGER;                 
        leRecouvrement recouvrement%rowtype;
        tmpemaOrdre emargement.ema_ordre%type;
        tmpEcrOrdre ecriture.ecr_ordre%type;

        cursor emargements is 
                select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' 
                        and rec_id in (
                                select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ))) 
                    union
                        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
                        where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and 
                        ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where 
                        eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )));

                    
       cursor ecritures is
               select distinct e.ecr_ordre from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) ));

        begin
        
            -- recuperer les numeros des ecritures min et max pour les passer en parametre
            --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
        
            select count(*) into flag from prelevement where RECO_ORDRE = recoordre;
            if (flag=0) then
                RAISE_APPLICATION_ERROR (-20001,'Aucun prelevement correspondant a recoordre= '|| recoordre);
            end if;
            
            select * into leRecouvrement from recouvrement where RECO_ORDRE = recoordre;
            
            -- verifier les dates des ecritures avec la date du prelevement
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMin;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if; 
            select ecr_date_saisie - leRecouvrement.RECO_DATE_CREATION into flag  from ecriture where exe_ordre=leRecouvrement.exe_ordre and ecr_numero = ecrNumeroMax;
            if (flag <>0) then
                RAISE_APPLICATION_ERROR (-20001,'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
            end if;             
            

        -- supprimer les emargements
        OPEN emargements;
             LOOP
                 FETCH emargements INTO tmpemaOrdre;
                      EXIT WHEN emargements%NOTFOUND;
                      
                annuler_emargement(tmpEmaOrdre);      
        END LOOP;
        CLOSE emargements;        
        
        -- modifier les etats des prelevements
        update prelevement set PREL_ETAT_MARACUJA = 'ATTENTE' where reco_ordre=recoOrdre;

        --supprimer le contenu du fichier (prelevement_fichier)
        delete from prelevement_fichier where reco_ordre=recoordre; 

        -- supprimer les �critures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
        OPEN ecritures;
             LOOP
                 FETCH ecritures INTO tmpecrOrdre;
                      EXIT WHEN ecritures%NOTFOUND;
                      
                creer_ecriture_annulation(tmpEcrOrdre);
        END LOOP;
        CLOSE ecritures;         

        delete from titre_detail_ecriture where ecd_ordre in (select distinct ecd.ecd_ordre from ecriture_detail ecd, ecriture e 
        where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=ecrNumeroMin and e.ecr_numero<=ecrNumeroMax and ecd_ordre in (select ecd_ordre 
        from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in 
        (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = recoordre) )) );

        -- supprimer association des pr�l�vements au recouvrement
        update  prelevement set reco_ordre=null where reco_ordre=recoOrdre;

        -- supprimer l'objet recouvrement
        delete from recouvrement where reco_ordre=recoOrdre;





--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))


--        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))) 
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--                 
                 
                 
                     
    end;
END;
/ 