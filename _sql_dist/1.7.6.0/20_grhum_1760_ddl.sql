
create table maracuja.IM_SUSPENSION (
    IMSUS_ID number(10) not null, 
    IMSUS_type varchar2(1) not null, 
    IMSUS_debut date not null, 
    IMSUS_fin date, 
    IMSUS_commentaire varchar2(1000), 
    IMSUS_etat number(10) not null, 
    utl_ordre number(10) not null, 
    date_creation date not null, 
    date_modification date not null, 
    DPP_ID number(10) not null, 
    primary key (IMSUS_ID));

comment on table maracuja.IM_SUSPENSION is 'Les suspensions de DGP saisies par l''ordonnateur ou le comptable pour une facture.';
comment on column maracuja.IM_SUSPENSION.IMSUS_ID is 'Identifiant';
comment on column maracuja.IM_SUSPENSION.IMSUS_type is 'Type de suspension (O: ordonnateur, C: comptable)';
comment on column maracuja.IM_SUSPENSION.IMSUS_debut is 'Debut de la periode de suspension';
comment on column maracuja.IM_SUSPENSION.IMSUS_fin is 'Fin de la periode de suspension';
comment on column maracuja.IM_SUSPENSION.IMSUS_commentaire is 'Commentaire lie a la suspension (detail des pieces demandees etc.)';
comment on column maracuja.IM_SUSPENSION.IMSUS_etat is 'Etat de la suspension (valide, annulee). Lien vers Jefy_amin.type_etat';

create table maracuja.IM (
    IM_ID number(10) not null, 
    IM_DGP number(10) not null, 
    IM_numero varchar2(20), 
    IM_date_depart_DGP date not null, 
    IM_date_fin_DGP_theorique date not null, 
    IM_date_fin_DGP_reelle date not null, 
    IM_duree_susp number(10) not null, 
    IM_nb_jours_depassement number(10), 
    IM_libelle_taux varchar2(255) not null, 
    IM_TAUX_REFERENCE          NUMBER(10)         NOT NULL,
    IM_taux_applicable number(19, 2) not null, 
    IM_montant number(19, 2), 
    im_commentaires varchar2(1000), 
    utl_ordre number(10) not null, 
    date_creation date not null, 
    date_modification date not null, 
    dep_id number(10) not null,       
    tyet_id number(10) not null,
    primary key (IM_ID));

comment on table maracuja.IM is 'Les interets moratoires associ�s a une facture';
comment on column maracuja.IM.IM_DGP is 'Delai global de paiement applicable (recup�r� depuis la table IM_DGP)';
comment on column maracuja.IM.IM_numero is 'Numero de l''interet moratoire';
comment on column maracuja.IM.IM_date_depart_DGP is 'Date de d�part pour le calcul du DGP (logiquement date de r�ception de la facture ou date de service fait si celle-ci est post�rieure � la date de reception de la facture). Cette date est pr�calcul�e mais peut �tre modifi�e par le comptable.';
comment on column maracuja.IM.IM_date_fin_DGP_theorique is 'Date de fin du DGP theorique (sans les suspensions). Cette date est calcul�e � partir de la date de depart du DGP en ajoutant le DGP affecte a la facture';
comment on column maracuja.IM.IM_date_fin_DGP_reelle is 'Date de fin du DGP r�elle (tenant compte des suspensions). = date fin theorique + duree suspensions';
comment on column maracuja.IM.IM_duree_susp is 'Duree de suspension calculee a partir de la table im_suspension';
comment on column maracuja.IM.IM_nb_jours_depassement is 'Nombre de jours de depassement du DGP (ordo + comptable) (calcul� en fonction de la date de fin DGP et de la date de paiement.';
comment on column maracuja.IM.IM_libelle_taux is 'Libelle du taux applicable pour le calcul de l''interet moratoire';
comment on column maracuja.IM.IM_taux_reference is 'Taux de reference pour le calcul de L''IM.';
comment on column maracuja.IM.IM_taux_applicable is 'Taux applicable pour le calcul de l''int�r�t moratoire (depend du taux de reference). Modifiable par le comptable';
comment on column maracuja.IM.IM_montant is 'Montant de l''interet moratoire, calcule a partir du montant regle, du taux applicable et du nombre de jours de depassement.';
comment on column maracuja.IM.im_commentaires is 'Commentaire libre associe a l''IM.';
comment on column maracuja.IM.dep_id is 'Reference maracuja.depense sur laquelle porte l''IM';
comment on column maracuja.IM.tyet_id is 'Reference jefy_admin.type_etat pour indiquer l''�tat de l''IM (ATTENTE, VALIDE, ANNULE)';





grant select, references on maracuja.IM to jefy_depense;
grant select, references on JEFY_DEPENSE.DEPENSE_PAPIER to maracuja;
grant select, references on JEFY_DEPENSE.DEPENSE_CTRL_PLANCO to maracuja;


alter table maracuja.IM add constraint FK_IM_dep_id foreign key (dep_id) references MARACUJA.depense;
alter table maracuja.IM add constraint FK_IM_tyet_id foreign key (tyet_id) references JEFY_ADMIN.TYPE_ETAT;
alter table maracuja.IM add constraint FK_IM_TAUX_REF foreign key (IM_TAUX_REFERENCE) references JEFY_ADMIN.IM_TAUX;

alter table maracuja.IM_SUSPENSION add constraint FK_IM_SUSPENS_DPP_ID foreign key (DPP_ID) references JEFY_DEPENSE.DEPENSE_PAPIER;
--alter table maracuja.IM add constraint FK_IM_dpco_id foreign key (dpco_id) references JEFY_DEPENSE.DEPENSE_CTRL_PLANCO;


create sequence maracuja.IM_SUSPENSION_seq NOCYCLE  NOCACHE  NOORDER;
create sequence maracuja.IM_seq NOCYCLE  NOCACHE  NOORDER;



grant select, references on jefy_admin.im_taux to maracuja with grant option;