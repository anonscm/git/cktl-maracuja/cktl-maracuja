declare 
    utlOrdre number;
	
begin
	-- indiquer eventuellement l'identifiant de l'agent comptable
	
	utlOrdre := 0;

	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 562, 'PAYEPAF', 'Visa/R�imputations', 'Visa des bordereaux provenant de l''application de Paye à façon', 'Bordereaux PAF', 'N', 'N', 4 );

    JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 563, 'IMSUSP', 'Int�r�ts moratoires','Saisie d''une suspension de DGP', 'Saisie d''une suspension de DGP',  'N', 'N', 4 );
    JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 564, 'IMSAIS', 'Int�r�ts moratoires', 'Cr�ation des int�r�ts moratoires', 'Cr�ation des int�r�ts moratoires', 'N', 'N', 4 );


    update jefy_admin.im_type_taux set imtt_priorite=10 where imtt_code='TBCE';
    update jefy_admin.im_type_taux set imtt_code='TBCE Refi' where imtt_code='TBCE';
    
    delete from JEFY_ADMIN.IM_TAUX where imtt_id in (select imtt_id from jefy_admin.im_type_taux where imtt_code='TBCE Refi');	

    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '14/03/2007', 'DD/MM/YYYY'), TO_Date( '12/06/2007', 'DD/MM/YYYY'), 
        3.75, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';

    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '13/06/2007', 'DD/MM/YYYY'), TO_Date( '08/07/2008', 'DD/MM/YYYY'), 
        4.00, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';

    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '09/07/2008', 'DD/MM/YYYY'), TO_Date( '07/10/2008', 'DD/MM/YYYY'), 
        4.25, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';

    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '08/10/2008', 'DD/MM/YYYY'), TO_Date( '05/11/2008', 'DD/MM/YYYY'), 
        3.75, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';


    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '06/11/2008', 'DD/MM/YYYY'), TO_Date( '03/12/2008', 'DD/MM/YYYY'), 
        3.25, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';
        

    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '04/12/2008', 'DD/MM/YYYY'), TO_Date( '14/01/2009', 'DD/MM/YYYY'), 
        2.50, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';
        
    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '05/01/2009', 'DD/MM/YYYY'), TO_Date( '04/03/2009', 'DD/MM/YYYY'), 
        2.00, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';
        
    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '05/03/2009', 'DD/MM/YYYY'), TO_Date( '01/04/2009', 'DD/MM/YYYY'), 
        1.50, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';
        
        
    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '02/04/2009', 'DD/MM/YYYY'), TO_Date( '06/05/2009', 'DD/MM/YYYY'), 
        1.25, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';
                
        
    INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
    DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  
        TO_Date( '07/05/2009', 'DD/MM/YYYY'), null, 
        1.00, utlOrdre
        ,  sysdate,  sysdate from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE Refi';        
		


    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.7.6.0',  SYSDATE, '');

commit;

end;