SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, GES_LIB, IMP_LIB, BOR_DATE_VISA, BOR_NUM, 
 MAN_TTC)
AS 
SELECT mr.exe_ordre, mr.ecr_date_saisie, mr.ecr_date,
      mr.ges_code, mr.pco_num, mr.bor_id, mr.tbo_ordre, mr.man_id, mr.man_num, mr.man_lib,
      mr.man_mont, sign(man_mont)*mr.man_tva, mr.man_etat, mr.fou_ordre, mr.ecr_ordre,
      mr.ecr_sacd, o.org_lib, api_planco.get_pco_libelle(mr.pco_num, mr.exe_ordre) pco_libelle, b.bor_date_visa, b.bor_num,
      man_mont + sign(man_mont)*man_tva
 FROM v_mandat_reimp_0 mr, v_organ_exer o, BORDEREAU b
WHERE mr.ges_code = o.org_ub
  AND o.org_niv = 2
  AND mr.EXE_ORDRE=o.exe_ordre
  AND mr.bor_id = b.bor_id;
/



CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte_ext (exeordre number, pco_condition VARCHAR2, sens_compte CHAR, comp VARCHAR2, sacd CHAR, vue varchar2)
RETURN NUMBER

IS
solde NUMBER(12,2);
 LC$req varchar2(1000);
 lavue varchar2(50);
BEGIN

lavue := vue;
if lavue is null then
  lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
end if;

IF exeordre >= 2005 THEN
   IF sens_compte = 'D'   THEN
       IF sacd = 'O' THEN
          LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND GES_CODE = '''||comp||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
       ELSE
           LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
       END IF;
   ELSE
       IF sacd = 'O' THEN
          LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND GES_CODE = '''||comp||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
       ELSE
           LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
       END IF;
   END IF;
ELSE
   --- A Refaire ---
   solde := 0;
END IF;

-- dbms_output.put_line( LC$req  );

EXECUTE IMMEDIATE LC$req INTO solde ;

IF solde < 0
   THEN solde := 0;
END IF;



RETURN solde;
END ;
/ 