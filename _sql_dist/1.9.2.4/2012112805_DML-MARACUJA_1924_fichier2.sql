SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°3/3
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.4
-- Date de publication : 28/11/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- L'execution de ice script installe les modes de paiement spécifiques à l'extourne.
-- Une erreur dans l'execution de ce script ne remet pas en cause les deux premiers scripts du patch.
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;
	execute maracuja.inst_mp_extourne;
commit;
