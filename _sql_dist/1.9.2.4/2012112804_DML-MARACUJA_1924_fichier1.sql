SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/3
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.4
-- Date de publication : 28/11/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- 
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_maracuja_1924;
commit;

drop procedure grhum.inst_patch_maracuja_1924;


