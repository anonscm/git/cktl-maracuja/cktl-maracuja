CREATE OR REPLACE PACKAGE MARACUJA.scol_echeancier IS

-- API PUBLIQUE pour creer des echeanciers / echeances --

-- renvoie la date de premiere echeance a partir de la date d'inscription
function getDatePremiereEcheance(exeOrdre number, dateInscription date) return date;

-- renvoie un identifiant d'echeancier
function creerEcheancier3Echeances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscription date) return number;

-- renvoie un identifiant d'echeancier
function creerEcheancierXEcheances (exeOrdre number, persId number, fouOrdre Number, ribOrdre number, montantTotal number, dateInscription date, nbEcheances integer, intervalleEcheancesMois integer) return number;


-- affecte le bordereau pour l'echeancier
procedure associerEcheancierEtBordereau(echeId number, borId number);

-- affecte l'echeancier au bordereau_brouillard
procedure associerEcheancierEtBob(echeId number, bobOrdre number);

-- verifie l echeancier (controle que l''echeancier est coherent avec ses prelevements)
procedure verifierEcheancier(echeId number);

-- supprime l'echeancier (si c'est possible, sinon exception)
procedure supprimerEcheancier(echeId number);

END;
/





GRANT EXECUTE ON MARACUJA.SCOL_ECHEANCIER TO GARNUCHE;

