

create or replace procedure grhum.inst_patch_maracuja_1924
is
begin
	-- nettoyage de certains parametres obsoletes
	delete from maracuja.parametre where par_key ='BTME GENERIQUE';
	delete from maracuja.parametre where par_key ='BTTE GENERIQUE';
	delete from maracuja.parametre where par_key ='CONTRE PARTIE VISA';
	delete from maracuja.parametre where par_key ='DATE_AVANT_SOLDE_BALANCE';
	delete from maracuja.parametre where par_key ='DATE_APRES_SOLDE_BALANCE';

	
	update maracuja.parametre set par_description='Instruction destinée aux client pour règlement par chèque, est affiché sur les lettres de relance, titres de recette, ordre de reversement, factures de PIE etc. Laisser vide si non permis.' where par_key='REGLEMENT_CHEQUE';
    update maracuja.parametre set par_description='Instruction destinée aux client pour règlement par virement, est affiché sur les lettres de relance, titres de recette, ordre de reversement, factures de PIE etc. Laisser vide si non permis.' where par_key='REGLEMENT_VIREMENT';
    update maracuja.parametre set par_description='Instruction destinée aux client pour règlement par virement, est affiché sur les factures (en anglais) de PIE etc. Laisser vide si non permis. ' where par_key='REGLEMENT_VIREMENT_ANGLAIS';
    update maracuja.parametre set par_description='Instruction destinée aux client pour règlement par carte bancaire, est affiché sur les lettres de relance, titres de recette, ordre de reversement, factures de PIE etc. Laisser vide si non permis.' where par_key='REGLEMENT_CARTE';
    update maracuja.parametre set par_description='Instruction destinée aux client pour règlement en numéraire, est affiché sur les lettres de relance, titres de recette, ordre de reversement, factures de PIE etc. Laisser vide si non permis.' where par_key='REGLEMENT_NUMERAIRE';
    
    update maracuja.parametre set par_description='Moyen de paiement par carte bancaire (vider si non permis). Affiché sur l''avis des sommes à payer.' where par_key='ABR_RECETTE_MP_CARTE';
    update maracuja.parametre set par_description='Moyen de paiement par chèque (vider si non permis). Affiché sur l''avis des sommes à payer.' where par_key='ABR_RECETTE_MP_CHEQUE';
    update maracuja.parametre set par_description='Moyen de paiement par numeraire (vider si non permis). Affiché sur l''avis des sommes à payer.' where par_key='ABR_RECETTE_MP_NUMERAIRE';

    update maracuja.parametre set par_description='Type de numérotation des bordereaux, mandats et titres. COMPOSANTE : une série de numérotation par UB. ETABLISSEMENT : une seule série de numérotation pour toutes les UB' where par_key='NUMERO BORDEREAU';
    update maracuja.parametre set par_description='Racine des comptes de TVA. Permet à Maracuja d''identifier les comptes de TVA.' where par_key='org.cocktail.gfc.comptabilite.comptetva.racine';
	
    
    
    
	
	
	insert into maracuja.type_numerotation
	            (tnu_entite,
	             tnu_libelle,
	             tnu_ordre
	            )
	select      'BORDEREAU', 'BORDEREAU DE MANDATS D''EXTOURNE', 25 from dual where not exists (select * from maracuja.type_numerotation where tnu_ordre=25 and tnu_libelle='BORDEREAU DE MANDATS D''EXTOURNE');
	
	insert into maracuja.type_numerotation
	            (tnu_entite,
	             tnu_libelle,
	             tnu_ordre
	            )
	select      'MANDAT', 'MANDAT EXTOURNE', 26 from dual where not exists (select * from maracuja.type_numerotation where tnu_ordre=26 and tnu_libelle='MANDAT EXTOURNE');
	
	insert into maracuja.type_bordereau
	            (tbo_libelle,
	             tbo_ordre,
	             tbo_sous_type,
	             tbo_type,
	             tbo_type_creation,
	             tnu_ordre
	            )
	select      'BORDEREAU DE MANDATS D''EXTOURNE', 50, 'EXTOURNE', 'BTMEX', null, 25 from dual where not exists(select * from maracuja.type_bordereau where tbo_ordre=50 and tbo_libelle='BORDEREAU DE MANDATS D''EXTOURNE');
	commit ;
	
	
	
--	         -- mise à jour de l'origine pour les brouillards de TVA
--    update maracuja.titre_brouillard set tib_operation='VISA TVA' where pco_num like '445%' and tib_operation<>'VISA TVA'; 
--    
--    -- reinit brouillards
--     update maracuja.titre_brouillard set  tib_operation='VISA TITRE' where tib_operation='VISA CTP';
--    
--    -- mise à jour de l'origine pour les brouillards de CTP
--    update maracuja.titre_brouillard set tib_operation='VISA CTP' where tib_ordre in (
--    select tib_ordre from maracuja.titre_brouillard tib inner join maracuja.titre t on (t.tit_id=tib.tit_id) 
--    where  t.pco_num<>tib.pco_num and ('18'||t.pco_num<>tib.pco_num) and tib_operation<>'VISA TVA' );
--    
   jefy_admin.patch_util.end_patch (4, '1.9.2.4');
end;
/

