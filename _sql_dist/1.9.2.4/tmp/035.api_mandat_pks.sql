CREATE OR REPLACE PACKAGE MARACUJA.api_mandat
is
   procedure viser_mandat (manid mandat.man_id%type, datevisa date, utlordre jefy_admin.utilisateur.utl_ordre%type, ecdlibelleprefix varchar2);
   procedure check_mandat_avant_visa(manid mandat.man_id%type);
   function creer_ecd_libelle(mab mandat_brouillard%rowtype, lemandat mandat%rowtype, lebordereau bordereau%rowtype) return ecriture_detail.ecd_libelle%type;
   function creer_ecr_libelle(lemandat mandat%rowtype, lebordereau bordereau%rowtype) return ecriture.ecr_libelle%type;
  
end;
/
