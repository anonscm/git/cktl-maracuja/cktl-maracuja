
CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_impressions
is
   -- renvoie l'origne des credits utilises pour la depense (Budgét (vide) ou Extourne)
   function get_depense_origine_credits (depid integer)
      return varchar2
   as
      exeordre          integer;
      originecredits   varchar2 (500);
      flag integer;
   begin
      originecredits := '';
      flag := is_depense_sur_extourne(depid);

      if (flag>0) then
         select exe_ordre into exeordre from jefy_depense.depense_budget where dep_id=depid;
        exeordre := exeordre-1;
      
         originecredits := 'extourne '||to_char(exeordre);
      end if;

      return originecredits;
   end;

   function get_depense_marche (depid integer)
      return varchar2
   as
      lemarche   varchar2 (2000);
      a_depid    integer;
      flag       integer;
   begin
      lemarche := '';
      a_depid := depid;

      select count (*)
      into   flag
      from   jefy_depense.depense_ctrl_marche dcm inner join jefy_marches.attribution a on dcm.att_ordre = a.att_ordre
             inner join jefy_marches.lot l on l.lot_ordre = a.lot_ordre
             inner join jefy_marches.marche m on m.mar_ordre = l.mar_ordre
      where  dcm.dep_id = a_depid;

      if (flag > 0) then
         select decode (m.exe_ordre || '/' || m.mar_index || '/' || l.lot_index, '//', ' ', m.exe_ordre || '/' || m.mar_index || '/' || l.lot_index || ': ' || l.lot_libelle)
         into   lemarche
         from   jefy_depense.depense_ctrl_marche dcm inner join jefy_marches.attribution a on dcm.att_ordre = a.att_ordre
                inner join jefy_marches.lot l on l.lot_ordre = a.lot_ordre
                inner join jefy_marches.marche m on m.mar_ordre = l.mar_ordre
         where  dcm.dep_id = a_depid;
      end if;

      return lemarche;
   end;

   function get_depense_cn (depid integer)
      return varchar2
   as
      ceordre            integer;
      lescodes           varchar2 (2000);
      lescodesdepenses   varchar2 (2000);
      a_depid            integer;

      cursor c1
      is
         select ce_ordre
         from   jefy_depense.depense_ctrl_hors_marche
         where  dep_id = a_depid;
   begin
      lescodesdepenses := ' ';
      a_depid := get_dep_id_initial_si_extourne (depid);

      open c1;

      loop
         fetch c1
         into  ceordre;

         exit when c1%notfound;

         select cm.cm_code || ' ' || cm_lib
         into   lescodes
         from   jefy_marches.code_exer ce, jefy_marches.code_marche cm
         where  ce.cm_ordre = cm.cm_ordre and ce.ce_ordre = ceordre;

         lescodesdepenses := lescodesdepenses || ' ' || lescodes;
      end loop;

      close c1;

      return lescodesdepenses;
   end;

   function get_depense_conv (depid integer)
      return varchar2
   as
      lesconv   varchar2 (2000);
      convref   varchar2 (500);

      cursor c1
      is
         select conv_reference
         from   jefy_depense.depense_ctrl_convention dcv, jefy_depense.v_convention c
         where  dcv.dep_id = depid and dcv.conv_ordre = c.conv_ordre;
   begin
      lesconv := null;

      open c1;

      loop
         fetch c1
         into  convref;

         exit when c1%notfound;

         if lesconv is not null then
            lesconv := lesconv || ', ';
         end if;

         lesconv := lesconv || convref;
      end loop;

      close c1;

      return lesconv;
   end;

   function get_recette_conv (recid integer)
      return varchar2
   as
      lesconv   varchar2 (2000);
      convref   varchar2 (500);

      cursor c1
      is
         --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
         select con_reference_externe
         from   jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  dcv.con_ordre = c.con_ordre and dcv.rec_id = rpco.rec_id and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      lesconv := null;

      open c1;

      loop
         fetch c1
         into  convref;

         exit when c1%notfound;

         if lesconv is not null then
            lesconv := lesconv || ', ';
         end if;

         lesconv := lesconv || convref;
      end loop;

      close c1;

      return lesconv;
   end;

   function get_depense_actions (depid integer)
      return varchar2
   as
      leslolf    varchar2 (2000);
      lolfcode   varchar2 (500);
      a_depid    integer;

      cursor c1
      is
         select lolf_code
         from   jefy_depense.depense_ctrl_action dca, jefy_admin.v_lolf_nomenclature_depense c
         where  dca.dep_id = a_depid and dca.tyac_id = c.lolf_id and dca.exe_ordre = c.exe_ordre;
   begin
      leslolf := null;
      a_depid := get_dep_id_initial_si_extourne (depid);

      open c1;

      loop
         fetch c1
         into  lolfcode;

         exit when c1%notfound;

         if leslolf is not null then
            leslolf := leslolf || ', ';
         end if;

         leslolf := leslolf || lolfcode;
      end loop;

      close c1;

      return leslolf;
   end;

   function get_recette_actions (recid integer)
      return varchar2
   as
      leslolf    varchar2 (2000);
      lolfcode   varchar2 (500);

      cursor c1
      is
         --SELECT con_reference_externe FROM jefy_recette.recette_ctrl_convention dcv, jefy_recette.v_convention c WHERE dcv.rec_id = recid AND dcv.con_ordre = c.con_ordre;
         select lolf_code
         from   jefy_recette.recette_ctrl_action rca, jefy_recette.v_lolf_nomenclature_recette l, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  rca.lolf_id = l.lolf_id and rca.rec_id = rpco.rec_id and rca.exe_ordre = l.exe_ordre and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      leslolf := null;

      open c1;

      loop
         fetch c1
         into  lolfcode;

         exit when c1%notfound;

         if leslolf is not null then
            leslolf := leslolf || ', ';
         end if;

         leslolf := leslolf || lolfcode;
      end loop;

      close c1;

      return leslolf;
   end;

   function get_depense_inventaire (dpcoid integer)
      return varchar2
   as
      lesinv   varchar2 (4000);
      leinv    varchar2 (4000);

      cursor c1
      is
         select ci.clic_num_complet
         from   jefy_inventaire.inventaire_comptable ic, jefy_inventaire.cle_inventaire_comptable ci
         where  ic.clic_id = ci.clic_id and dpco_id = dpcoid;
   begin
      lesinv := null;

      open c1;

      loop
         fetch c1
         into  leinv;

         exit when c1%notfound;

         if leinv is not null then
            leinv := leinv || ' ';
         end if;

         lesinv := lesinv || leinv;
      end loop;

      close c1;

      return lesinv;
   end;

   function get_depense_lbud (depid integer)
      return varchar2
   as
      lbudlib   varchar2 (2000);
   begin
      select o.org_ub || ' / ' || org_cr || ' / ' || org_souscr lbud
      into   lbudlib
      from   jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.organ o
      where  db.dep_id = depid and o.org_id = eb.org_id and eb.eng_id = db.eng_id;

      return lbudlib;
   end;

   function get_depense_infos (depid integer)
      return varchar2
   as
   begin
      return abricot_impressions.get_depense_cn (depid) || ' ' || abricot_impressions.get_depense_lbud (depid);
   end;

   function get_recette_analytiques (recid integer)
      return varchar2
   as
      lescodes   varchar2 (2000);
      coderef    varchar2 (500);

      cursor c1
      is
         select a.can_code
         from   jefy_recette.recette_ctrl_analytique rca, jefy_admin.code_analytique a, jefy_recette.recette_ctrl_planco rpco, maracuja.titre t, maracuja.recette mr
         where  rca.can_id = a.can_id and rca.rec_id = rpco.rec_id and rpco.tit_id = t.tit_id and t.tit_id = mr.tit_id and mr.rec_id = recid;
   begin
      lescodes := null;

      open c1;

      loop
         fetch c1
         into  coderef;

         exit when c1%notfound;

         if lescodes is not null then
            lescodes := lescodes || ', ';
         end if;

         lescodes := lescodes || coderef;
      end loop;

      close c1;

      return lescodes;
   end;

   function get_depense_analytiques (depid integer)
      return varchar2
   as
      lescanal    varchar2 (2000);
      canalcode   varchar2 (500);

      cursor c1
      is
         select can_code
         from   jefy_depense.depense_ctrl_analytique dca, jefy_admin.v_code_analytique ca
         where  dca.dep_id = depid and dca.can_id = ca.can_id and dca.exe_ordre = ca.exe_ordre;
   begin
      lescanal := null;

      open c1;

      loop
         fetch c1
         into  canalcode;

         exit when c1%notfound;

         if lescanal is not null then
            lescanal := lescanal || ', ';
         end if;

         lescanal := lescanal || canalcode;
      end loop;

      close c1;

      return lescanal;
   end;

     -- s'il s'agit d'une liquidation sur credits d'extourne flêchés,
   --on récupère l'id de la liquidation d'origine (en N-1)
   -- sinon on renvoie depid
   function get_dep_id_initial_si_extourne (depid integer)
      return integer
   as
      a_depid   integer;
      flag      integer;
   begin
      a_depid := depid;

      select count (*)
      into   flag
      from   jefy_depense.extourne_liq_def eld inner join jefy_depense.extourne_liq_repart elr on (eld.eld_id = elr.eld_id)
             inner join jefy_depense.extourne_liq el on (el.el_id = elr.el_id)
      where  eld.dep_id = a_depid and eld.tyet_id = 220 and el.dep_id_n is not null;

      if (flag > 0) then
         select el.dep_id_n
         into   a_depid
         from   jefy_depense.extourne_liq_def eld inner join jefy_depense.extourne_liq_repart elr on (eld.eld_id = elr.eld_id)
                inner join jefy_depense.extourne_liq el on (el.el_id = elr.el_id)
         where  eld.dep_id = a_depid and eld.tyet_id = 220;
      end if;

      return a_depid;
   end;
   
   function is_depense_sur_extourne(depid integer) return integer
   as
    flag integer;
   begin
    select count (*)
    into   flag
      from   jefy_depense.extourne_liq_def eld 
      where  eld.dep_id = depid and eld.tyet_id = 220;
      
     return flag; 
   end;
   
   
end abricot_impressions;
/
