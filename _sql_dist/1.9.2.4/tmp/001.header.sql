set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.2.4
-- Date de publication : 28/11/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Remplace les patchs 1920,1921,1922,1923 (Versions beta)
-- * Ajout Modes de paiement pour fonctionalités d'extourne. Ces ajouts sont effectués via la procedure maracuja.inst_mp_extourne. 
--Le but de la procedure est de créer deux modes de paiements : A extourner (hors masse salariale) et A extourner (masse salariale). 
--Pour chaque mode de paiement, un compte de TVA et un compte de Visa doivent êre affectés. Vous obtiendrez un message d'erreur si 
--les comptes en question ne sont pas actifs dans votre plan comptable. Si c'est le cas contactez l'agence comptable pour résoudre 
--le problème et exécutez à nouveau la procédure. Une autre solution est de ne pas installer les modes de paiements 
--en question et de demander à l'agence comptable de se charger de la création des modes de paiements du domaine "Extourne". 
--La présence de ces modes de paiements dans la base de données sera indispensable pour l'utilisation de l'extourne en fin d'année. 
-- * Compte_fi/Procedure de calcul de la CAF : ajout d'un message d'erreur quand les SIG n'ont pas été calculés 
-- * ajout des types de bordereaux pour gestion de l'extourne
-- * correction bug wo54 avec les preference
-- * Modifications dans divers packages pour gestion de l'extourne
-- * DT #4928 - Visa dépenses : Régression/erreur lors du rejet total d'un bordereau de mandat
-- * DT #4931 - Lancement : Impossible de lancer Maracuja via ZAP
-- * DT #4932 - Visa dépenses : Erreur lors du rejet total d'un bordereau de mandat d'extourne
-- * DT #5061 - Affichage de la mention "extourne" sur le mandat quand c'est nécessaire, affichage des actions et marchés récupérés à partir de la liquidation initiale en N
-- * DT #5021 - Numérotation spcécifique pour les mandats d'extourne
-- Suppression de parametres obsoletes : BTME GENERIQUE, BTTE GENERIQUE,CONTRE PARTIE VISA,DATE_AVANT_SOLDE_BALANCE,DATE_APRES_SOLDE_BALANCE. 
-- Modification des descriptions de certains parametres
----------------------------------------------
whenever sqlerror exit sql.sqlcode;



exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.1.4', 'MARACUJA' );

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2104';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.2.4', null );
commit ;

grant select on jefy_depense.extourne_liq_def to maracuja with grant option;

