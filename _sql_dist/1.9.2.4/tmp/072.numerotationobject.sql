CREATE OR REPLACE PACKAGE BODY MARACUJA."NUMEROTATIONOBJECT"
is
   procedure private_numeroter_ecriture (ecrordre integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   ecriture
      where  ecr_ordre = ecrordre;

      if cpt = 0 then
         raise_application_error (-20001, ' ECRITURE INEXISTANTE , CLE ' || ecrordre);
      end if;

      select ecr_numero
      into   cpt
      from   ecriture
      where  ecr_ordre = ecrordre;

      if cpt = 0 then
-- recup des infos de l objet -
         select com_ordre,
                exe_ordre
         into   comordre,
                exeordre
         from   ecriture
         where  ecr_ordre = ecrordre;

         select tnu_ordre
         into   tnuordre
         from   type_numerotation
         where  tnu_libelle = 'ECRITURE DU JOURNAL';

-- recup du numero -
         lenumero := numeroter (comordre, exeordre, null, tnuordre);

-- affectation du numero -
         update ecriture
            set ecr_numero = lenumero
          where ecr_ordre = ecrordre;
      end if;
   end;

   procedure numeroter_ecriture (ecrordre integer)
   is
      monecriture   ecriture%rowtype;
   begin
      numerotationobject.private_numeroter_ecriture (ecrordre);

      select *
      into   monecriture
      from   ecriture
      where  ecr_ordre = ecrordre;

      numerotationobject.numeroter_ecriture_verif (monecriture.com_ordre, monecriture.exe_ordre);
   end;

   procedure numeroter_ecriture_verif (comordre integer, exeordre integer)
   is
      ecrordre   integer;
      lenumero   integer;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;

      cursor lesecrituresnonnumerotees
      is
         select   ecr_ordre
         from     ecriture
         where    com_ordre = comordre and exe_ordre = exeordre and ecr_numero = 0 and bro_ordre is null
         order by ecr_ordre;
   begin
      open lesecrituresnonnumerotees;

      loop
         fetch lesecrituresnonnumerotees
         into  ecrordre;

         exit when lesecrituresnonnumerotees%notfound;
         numerotationobject.private_numeroter_ecriture (ecrordre);
      end loop;

      close lesecrituresnonnumerotees;
   end;

   procedure numeroter_brouillard (broid integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   brouillard
      where  bro_id = broid;

      if cpt = 0 then
         raise_application_error (-20001, ' BROUILLARD INEXISTANTE, CLE ' || broid);
      end if;

-- recup des infos de l objet -
      select com_ordre,
             exe_ordre
      into   comordre,
             exeordre
      from   brouillard
      where  bro_id = broid;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'BROUILLARD';

-- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

-- affectation du numero -
      update brouillard
         set bro_numero = lenumero
       where bro_id = broid;
   end;

   procedure numeroter_bordereaurejet (brjordre integer)
   is
      cpt            integer;
      lenumero       integer;
      comordre       comptabilite.com_ordre%type;
      exeordre       exercice.exe_ordre%type;
      gescode        gestion.ges_ordre%type;
      tnuordre       type_numerotation.tnu_ordre%type;
      tbotype        type_bordereau.tbo_type%type;
      parvalue       parametre.par_value%type;
      mandat_rejet   mandat%rowtype;
      titre_rejet    titre%rowtype;

      cursor mdt_rejet
      is
         select *
         from   mandat
         where  brj_ordre = brjordre;

      cursor tit_rejet
      is
         select *
         from   titre
         where  brj_ordre = brjordre;
   begin
      select count (*)
      into   cpt
      from   bordereau_rejet
      where  brj_ordre = brjordre;

      if cpt = 0 then
         raise_application_error (-20001, ' BORDEREAU REJET INEXISTANT , CLE ' || brjordre);
      end if;

-- recup des infos de l objet -
      select t.tbo_type,
             g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   tbotype,
             comordre,
             exeordre,
             gescode
      from   bordereau_rejet b, gestion g, comptabilite c, type_bordereau t
      where  b.brj_ordre = brjordre and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

      if (tbotype = 'BTMNA') then
         select tnu_ordre
         into   tnuordre
         from   type_numerotation
         where  tnu_libelle = 'BORD. DEPENSE NON ADMIS';
      else
         select tnu_ordre
         into   tnuordre
         from   type_numerotation
         where  tnu_libelle = 'BORD. RECETTE NON ADMIS';
      end if;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

      if parvalue = 'COMPOSANTE' then
         lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
      else
         lenumero := numeroter (comordre, exeordre, null, tnuordre);
      end if;

-- affectation du numero -
      update bordereau_rejet
         set brj_num = lenumero
       where brj_ordre = brjordre;

-- numeroter les titres rejetes -
      if (tbotype = 'BTMNA') then
         open mdt_rejet;

         loop
            fetch mdt_rejet
            into  mandat_rejet;

            exit when mdt_rejet%notfound;
            numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
         end loop;

         close mdt_rejet;
      end if;

-- numeroter les mandats rejetes -
      if (tbotype = 'BTTNA') then
         open tit_rejet;

         loop
            fetch tit_rejet
            into  titre_rejet;

            exit when tit_rejet%notfound;
            numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
         end loop;

         close tit_rejet;
      end if;
   end;

   procedure numeroter_bordereaucheques (borid integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      tbotype    type_bordereau.tbo_type%type;
      parvalue   parametre.par_value%type;
   begin
      select count (*)
      into   cpt
      from   bordereau
      where  bor_id = borid;

      if cpt = 0 then
         raise_application_error (-20001, ' BORDEREAU DE CHEQUES INEXISTANT , CLE ' || borid);
      end if;

-- recup des infos de l objet -
      select t.tbo_type,
             g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   tbotype,
             comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

-- TNU_ORDRE A DEFINIR
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'BORDEREAU CHEQUE';

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

--END IF;

      -- affectation du numero -
      update bordereau
         set bor_num = lenumero
       where bor_id = borid;
   end;

   procedure numeroter_emargement (emaordre integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   emargement
      where  ema_ordre = emaordre;

      if cpt = 0 then
         raise_application_error (-20001, ' EMARGEMENT INEXISTANT , CLE ' || emaordre);
      end if;

-- recup des infos de l objet -
      select com_ordre,
             exe_ordre
      into   comordre,
             exeordre
      from   emargement
      where  ema_ordre = emaordre;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

-- affectation du numero -
      update emargement
         set ema_numero = lenumero
       where ema_ordre = emaordre;
   end;

   procedure numeroter_ordre_paiement (odpordre integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   ordre_de_paiement
      where  odp_ordre = odpordre;

      if cpt = 0 then
         raise_application_error (-20001, ' ORDRE DE PAIEMENT INEXISTANT , CLE ' || odpordre);
      end if;

-- recup des infos de l objet -
      select com_ordre,
             exe_ordre
      into   comordre,
             exeordre
      from   ordre_de_paiement
      where  odp_ordre = odpordre;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'ORDRE DE PAIEMENT';

-- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

-- affectation du numero -
      update ordre_de_paiement
         set odp_numero = lenumero
       where odp_ordre = odpordre;
   end;

   procedure numeroter_paiement (paiordre integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   paiement
      where  pai_ordre = paiordre;

      if cpt = 0 then
         raise_application_error (-20001, ' PAIEMENT INEXISTANT , CLE ' || paiordre);
      end if;

      -- recup des infos de l objet -
      select com_ordre,
             exe_ordre
      --select 1,1
      into   comordre,
             exeordre
      from   paiement
      where  pai_ordre = paiordre;

      --SELECT tnu_ordre INTO tnuordre
      --FROM TYPE_NUMEROTATION
      --WHERE tnu_libelle ='VIREMENT';

      ---- recup du numero -
      --lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
      lenumero := next_numero_paiement (comordre, exeordre);

      -- affectation du numero -
      update paiement
         set pai_numero = lenumero
       where pai_ordre = paiordre;
   end;

   function next_numero_paiement (comordre integer, exeordre integer)
      return integer
   is
      lenumero   integer;
      tnuordre   integer;
   begin
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'VIREMENT';

      -- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);
      return lenumero;
   end;

   procedure numeroter_recouvrement (recoordre integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   recouvrement
      where  reco_ordre = recoordre;

      if cpt = 0 then
         raise_application_error (-20001, ' RECOUVREMENT INEXISTANT , CLE ' || recoordre);
      end if;

-- recup des infos de l objet -
      select com_ordre,
             exe_ordre
--select 1,1
      into   comordre,
             exeordre
      from   recouvrement
      where  reco_ordre = recoordre;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'RECOUVREMENT';

-- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

-- affectation du numero -
      update recouvrement
         set reco_numero = lenumero
       where reco_ordre = recoordre;
   end;

   procedure numeroter_retenue (retordre integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
   begin
      select count (*)
      into   cpt
      from   retenue
      where  ret_id = retordre;

      if cpt = 0 then
         raise_application_error (-20001, ' RETENUE INEXISTANTE , CLE ' || retordre);
      end if;

-- recup des infos de l objet -
      select com_ordre,
             exe_ordre
      into   comordre,
             exeordre
      from   retenue
      where  ret_id = retordre;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'RETENUE';

-- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

--affectation du numero -
      update retenue
         set ret_numero = lenumero
       where ret_id = retordre;
   end;

   procedure numeroter_reimputation (reiordre integer)
   is
      cpt          integer;
      lenumero     integer;
      comordre     comptabilite.com_ordre%type;
      exeordre     exercice.exe_ordre%type;
      gescode      gestion.ges_ordre%type;
      tnuordre     type_numerotation.tnu_ordre%type;
      tnulibelle   type_numerotation.tnu_libelle%type;
   begin
      select count (*)
      into   cpt
      from   reimputation
      where  rei_ordre = reiordre;

      if cpt = 0 then
         raise_application_error (-20001, ' REIMPUTATION INEXISTANTE , CLE ' || reiordre);
      end if;

      select count (*)
--select 1,1
      into   cpt
      from   reimputation r, mandat m, gestion g
      where  rei_ordre = reiordre and m.man_id = r.man_id and m.ges_code = g.ges_code;

      if cpt != 0 then
-- recup des infos de l objet -
--select com_ordre,exe_ordre
         select distinct g.com_ordre,
                         r.exe_ordre,
                         'REIMPUTATION MANDAT'
--select 1,1
         into            comordre,
                         exeordre,
                         tnulibelle
         from            reimputation r, mandat m, gestion g
         where           rei_ordre = reiordre and m.man_id = r.man_id and m.ges_code = g.ges_code;
      else
--select com_ordre,exe_ordre
         select distinct g.com_ordre,
                         r.exe_ordre,
                         'REIMPUTATION TITRE'
--select 1,1
         into            comordre,
                         exeordre,
                         tnulibelle
         from            reimputation r, titre t, gestion g
         where           rei_ordre = reiordre and t.tit_id = r.tit_id and t.ges_code = g.ges_code;
      end if;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = tnulibelle;

-- recup du numero -
      lenumero := numeroter (comordre, exeordre, null, tnuordre);

--affectation du numero -
      update reimputation
         set rei_numero = lenumero
       where rei_ordre = reiordre;
   end;

   procedure numeroter_mandat_rejete (manid integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;
   begin
      select count (*)
      into   cpt
      from   mandat
      where  man_id = manid;

      if cpt = 0 then
         raise_application_error (-20001, ' MANDAT REJETE INEXISTANT , CLE ' || manid);
      end if;

-- recup des infos de l objet -
      select g.com_ordre,
             m.exe_ordre,
             m.ges_code
--select 1,1
      into   comordre,
             exeordre,
             gescode
      from   mandat m, gestion g
      where  man_id = manid and m.ges_code = g.ges_code;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'MANDAT REJET';

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

      if parvalue = 'COMPOSANTE' then
         lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
      else
         lenumero := numeroter (comordre, exeordre, null, tnuordre);
      end if;

-- affectation du numero -
      update mandat
         set man_numero_rejet = lenumero
       where man_id = manid;
   end;

   procedure numeroter_titre_rejete (titid integer)
   is
      cpt        integer;
      lenumero   integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;
   begin
      select count (*)
      into   cpt
      from   titre
      where  tit_id = titid;

      if cpt = 0 then
         raise_application_error (-20001, ' TITRE REJETE INEXISTANT , CLE ' || titid);
      end if;

-- recup des infos de l objet -
      select g.com_ordre,
             t.exe_ordre,
             t.ges_code
--select 1,1
      into   comordre,
             exeordre,
             gescode
      from   titre t, gestion g
      where  t.tit_id = titid and t.ges_code = g.ges_code;

      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'TITRE REJET';

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

      if parvalue = 'COMPOSANTE' then
         lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
      else
         lenumero := numeroter (comordre, exeordre, null, tnuordre);
      end if;

-- affectation du numero -
      update titre
         set tit_numero_rejet = lenumero
       where tit_id = titid;
   end;

   function numeroter (comordre comptabilite.com_ordre%type, exeordre exercice.exe_ordre%type, gescode gestion.ges_ordre%type, tnuordre type_numerotation.tnu_ordre%type)
      return integer
   is
      cpt        integer;
      numordre   integer;
   begin
      lock table numerotation in exclusive mode;

      --COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE
      if gescode is null then
         select count (*)
         into   cpt
         from   numerotation
         where  com_ordre = comordre and exe_ordre = exeordre and ges_code is null and tnu_ordre = tnuordre;
      else
         select count (*)
         into   cpt
         from   numerotation
         where  com_ordre = comordre and exe_ordre = exeordre and ges_code = gescode and tnu_ordre = tnuordre;
      end if;

      if cpt = 0 then
         --raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
         select numerotation_seq.nextval
         into   numordre
         from   dual;

         insert into numerotation
                     (com_ordre,
                      exe_ordre,
                      ges_code,
                      num_numero,
                      num_ordre,
                      tnu_ordre
                     )
         values      (comordre,
                      exeordre,
                      gescode,
                      1,
                      numordre,
                      tnuordre
                     );

         return 1;
      else
         if gescode is not null then
            select num_numero + 1
            into   cpt
            from   numerotation
            where  com_ordre = comordre and exe_ordre = exeordre and ges_code = gescode and tnu_ordre = tnuordre;

            update numerotation
               set num_numero = cpt
             where com_ordre = comordre and exe_ordre = exeordre and ges_code = gescode and tnu_ordre = tnuordre;
         else
            select num_numero + 1
            into   cpt
            from   numerotation
            where  com_ordre = comordre and exe_ordre = exeordre and ges_code is null and tnu_ordre = tnuordre;

            update numerotation
               set num_numero = cpt
             where com_ordre = comordre and exe_ordre = exeordre and ges_code is null and tnu_ordre = tnuordre;
         end if;

         return cpt;
      end if;
   end;

   procedure numeroter_bordereau (borid integer)
   is
      cpt           integer;
      lenumero      integer;
      comordre      comptabilite.com_ordre%type;
      exeordre      exercice.exe_ordre%type;
      gescode       gestion.ges_ordre%type;
      tnuordre      type_numerotation.tnu_ordre%type;
      tbotype       type_bordereau.tbo_type%type;
      tboordre      type_bordereau.tbo_ordre%type;
      parvalue      parametre.par_value%type;
      lebordereau   bordereau%rowtype;
   begin
      select count (*)
      into   cpt
      from   maracuja.bordereau
      where  bor_id = borid;

      if cpt = 0 then
         raise_application_error (-20001, ' BORDEREAU  INEXISTANT , CLE ' || borid);
      end if;

-- recup des infos de l objet -
      select t.tbo_type,
             t.tbo_ordre,
             g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   tbotype,
             tboordre,
             comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  b.bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

      select count (*)
      into   cpt
      from   maracuja.type_bordereau tb, maracuja.type_numerotation tn
      where  tn.tnu_ordre = tb.tnu_ordre and tb.tbo_ordre = tboordre;

      if cpt = 1 then
         select tn.tnu_ordre
         into   tnuordre
         from   maracuja.type_bordereau tb, type_numerotation tn
         where  tn.tnu_ordre = tb.tnu_ordre and tb.tbo_ordre = tboordre;
      else
         raise_application_error (-20001, 'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION' || borid);
      end if;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

      if parvalue = 'COMPOSANTE' then
         lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
      else
         lenumero := numeroter (comordre, exeordre, null, tnuordre);
      end if;

      if lenumero is null then
         raise_application_error (-20001, 'PROBLEME DE NUMEROTATION' || borid);
      end if;

-- affectation du numero -
      update maracuja.bordereau
         set bor_num = lenumero
       where bor_id = borid;
   end;

   procedure numeroter_mandat (borid integer)
   is
      cpt        integer;
      lenumero   integer;
      manid      integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;
      tboordre   bordereau.tbo_ordre%type;

      cursor a_numeroter
      is
         select   man_id
         from     mandat
         where    bor_id = borid
         order by pco_num;
   begin

      select tbo_ordre
      into   tboordre
      from   bordereau
      where  bor_id = borid;

      select exe_ordre
      into   exeordre
      from   bordereau
      where  bor_id = borid;

      if (exeordre > 2010) then
        if (tboordre=50) then
            numeroter_mandat_extourne(borid);
            return;
        end if;
      
         if (tboordre in (8, 18, 21)) then
            numeroter_orv (borid);
            return;
         end if;
      else
         if (tboordre in (8, 18)) then
            numeroter_orv (borid);
            return;
         end if;
      end if;

-- recup des infos de l objet -
      select g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  b.bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

-- type de numerotation  = MANDAT
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'MANDAT';

      open a_numeroter;

      loop
         fetch a_numeroter
         into  manid;

         exit when a_numeroter%notfound;

         if parvalue = 'COMPOSANTE' then
            lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
         else
            lenumero := numeroter (comordre, exeordre, null, tnuordre);
         end if;

         update maracuja.mandat
            set man_numero = lenumero
          where bor_id = borid and man_id = manid;
      end loop;

      close a_numeroter;
   end;

   procedure numeroter_mandat_extourne (borid integer)
   is
      cpt        integer;
      lenumero   integer;
      manid      integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;
      tboordre   bordereau.tbo_ordre%type;

      cursor a_numeroter
      is
         select   man_id
         from     mandat
         where    bor_id = borid
         order by pco_num;
   begin
      select tbo_ordre
      into   tboordre
      from   bordereau
      where  bor_id = borid;

      select exe_ordre
      into   exeordre
      from   bordereau
      where  bor_id = borid;

-- recup des infos de l objet -
      select g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  b.bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

-- type de numerotation  = MANDAT EXTOURNE
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'MANDAT EXTOURNE';

      open a_numeroter;

      loop
         fetch a_numeroter
         into  manid;

         exit when a_numeroter%notfound;

         if parvalue = 'COMPOSANTE' then
            lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
         else
            lenumero := numeroter (comordre, exeordre, null, tnuordre);
         end if;

         update maracuja.mandat
            set man_numero = lenumero
          where bor_id = borid and man_id = manid;
      end loop;

      close a_numeroter;
   end;

   procedure numeroter_titre (borid integer)
   is
      cpt        integer;
      lenumero   integer;
      titid      integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;
      tboordre   bordereau.tbo_ordre%type;

      cursor a_numeroter
      is
         select   tit_id
         from     titre
         where    bor_id = borid
         order by pco_num, tit_id;
   begin
-- si c un bordereau aor
      select tbo_ordre
      into   tboordre
      from   bordereau
      where  bor_id = borid;

      if (tboordre = 9) then
         numeroter_aor (borid);
         return;
      end if;

-- recup des infos de l objet -
      select g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  b.bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

-- type de numerotation  = MANDAT
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'TITRE';

      open a_numeroter;

      loop
         fetch a_numeroter
         into  titid;

         exit when a_numeroter%notfound;

         if parvalue = 'COMPOSANTE' then
            lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
         else
            lenumero := numeroter (comordre, exeordre, null, tnuordre);
         end if;

         update maracuja.titre
            set tit_numero = lenumero
          where bor_id = borid and tit_id = titid;
      end loop;

      close a_numeroter;
   end;

   procedure numeroter_orv (borid integer)
   is
      cpt        integer;
      lenumero   integer;
      manid      integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;

      cursor a_numeroter
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
-- recup des infos de l objet -
      select g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  b.bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

-- type de numerotation  = MANDAT
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'ORV';

      open a_numeroter;

      loop
         fetch a_numeroter
         into  manid;

         exit when a_numeroter%notfound;

         if parvalue = 'COMPOSANTE' then
            lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
         else
            lenumero := numeroter (comordre, exeordre, null, tnuordre);
         end if;

         update maracuja.mandat
            set man_numero = lenumero
          where bor_id = borid and man_id = manid;
      end loop;

      close a_numeroter;
   end;

   procedure numeroter_aor (borid integer)
   is
      cpt        integer;
      lenumero   integer;
      titid      integer;
      comordre   comptabilite.com_ordre%type;
      exeordre   exercice.exe_ordre%type;
      gescode    gestion.ges_ordre%type;
      tnuordre   type_numerotation.tnu_ordre%type;
      parvalue   parametre.par_value%type;

      cursor a_numeroter
      is
         select tit_id
         from   titre
         where  bor_id = borid;
   begin
-- recup des infos de l objet -
      select g.com_ordre,
             b.exe_ordre,
             b.ges_code
      into   comordre,
             exeordre,
             gescode
      from   bordereau b, gestion g, comptabilite c, type_bordereau t
      where  b.bor_id = borid and g.ges_code = b.ges_code and t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
      select par_value
      into   parvalue
      from   parametre
      where  par_key = 'NUMERO BORDEREAU' and exe_ordre = exeordre;

-- type de numerotation  = MANDAT
      select tnu_ordre
      into   tnuordre
      from   type_numerotation
      where  tnu_libelle = 'AOR';

      open a_numeroter;

      loop
         fetch a_numeroter
         into  titid;

         exit when a_numeroter%notfound;

         if parvalue = 'COMPOSANTE' then
            lenumero := numeroter (comordre, exeordre, gescode, tnuordre);
         else
            lenumero := numeroter (comordre, exeordre, null, tnuordre);
         end if;

         update maracuja.titre
            set tit_numero = lenumero
          where bor_id = borid and tit_id = titid;
      end loop;

      close a_numeroter;
   end;
end numerotationobject;
/



