
create or replace procedure maracuja.inst_mp_extourne
is
   modcodeextournehorssal   maracuja.mode_paiement.mod_code%type;
   modcodeextournesal       maracuja.mode_paiement.mod_code%type;
   pconumtvahorssal         maracuja.mode_paiement.pco_num_tva%type;
   pconumtvasal             maracuja.mode_paiement.pco_num_tva%type;
   pconumvisahorssal        maracuja.mode_paiement.pco_num_visa%type;
   pconumvisasal            maracuja.mode_paiement.pco_num_visa%type;
   flag                     integer;
   compteabsents            varchar2 (500);
begin
   modcodeextournehorssal := '991';
   modcodeextournesal := '992';
   pconumtvahorssal := '44586';
   pconumtvasal := '44586';
   pconumvisahorssal := '4081';
   pconumvisasal := '4286';
   

   -- verifier la presence des codes paiement
   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournehorssal and mod_dom <> 'A EXTOURNER';

   if (flag > 0) then
      raise_application_error (-20001,
                                  'Le code '
                               || modcodeextournehorssal
                               || ' est deja utilise dans les modes de paiement. Vous devez specifier un nouveau code pour le mode de paiement "Charge à payer à extourner (hors masse salariale)" dans la procedure maracuja.inst_mp_extourne'
                              );
   end if;

   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournesal and mod_dom <> 'A EXTOURNER';

   if (flag > 0) then
      raise_application_error (-20001,
                               'Le code ' || modcodeextournehorssal
                               || ' est deja utilise dans les modes de paiement. Vous devez specifier un nouveau code pour le mode de paiement "Charge à payer à extourner (masse salariale)" dans la procedure maracuja.inst_mp_extourne'
                              );
   end if;

   -- verifier presence des comptes de TVA
   compteabsents := '';

   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumtvahorssal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumtvahorssal || ' ';
   end if;

   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumtvasal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumtvasal || ' ';
   end if;

   -- verifier presence des comptes de VISA
   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumvisahorssal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumvisahorssal || ' ';
   end if;

   select count (*)
   into   flag
   from   maracuja.plan_comptable_exer pco, jefy_admin.exercice ex
   where  pco.exe_ordre = ex.exe_ordre and exe_stat = 'O' and pco_validite = 'VALIDE' and pco_num = pconumvisasal;

   if (flag = 0) then
      compteabsents := compteabsents || pconumvisasal || ' ';
   end if;

   compteabsents := trim (compteabsents);
   compteabsents := replace (compteabsents, ' ', ', ');

   if (length (compteabsents) > 0) then
      raise_application_error (-20001,
                                  'Le(s) compte(s) '
                               || compteabsents
                               || ' devrai(en)t être actif(s) dans le plan comptable pour l''exercice en cours. Si vous ne voulez pas utiliser ces comptes, modifiez la procedure maracuja.inst_mp_extourne pour utiliser d''autres comptes.'
                              );
   end if;

   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournehorssal and mod_dom = 'A EXTOURNER';

   if (flag = 0) then
      insert into maracuja.mode_paiement
                  (exe_ordre,
                   mod_libelle,
                   mod_ordre,
                   mod_validite,
                   pco_num_paiement,
                   pco_num_visa,
                   mod_code,
                   mod_dom,
                   mod_visa_type,
                   mod_ema_auto,
                   mod_contrepartie_gestion,
                   pco_num_tva,
                   pco_num_tva_ctp,
                   mod_paiement_ht
                  )
         select exe_ordre,
                'Charge à payer à extourner (hors masse salariale)',
                maracuja.mode_paiement_seq.nextval,
                'VALIDE',
                null,
                pconumvisahorssal,
                modcodeextournehorssal,
                'A EXTOURNER',
                null,
                0,
                null,
                pconumtvahorssal,
                null,
                'N'
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');
   end if;

   select count (*)
   into   flag
   from   maracuja.mode_paiement mp, jefy_admin.exercice ex
   where  mp.exe_ordre = ex.exe_ordre and exe_stat in ('O', 'P') and mod_code = modcodeextournesal and mod_dom = 'A EXTOURNER';

   if (flag = 0) then
      insert into maracuja.mode_paiement
                  (exe_ordre,
                   mod_libelle,
                   mod_ordre,
                   mod_validite,
                   pco_num_paiement,
                   pco_num_visa,
                   mod_code,
                   mod_dom,
                   mod_visa_type,
                   mod_ema_auto,
                   mod_contrepartie_gestion,
                   pco_num_tva,
                   pco_num_tva_ctp,
                   mod_paiement_ht
                  )
         select exe_ordre,
                'Charge à payer à extourner (masse salariale)',
                maracuja.mode_paiement_seq.nextval,
                'VALIDE',
                null,
                pconumvisasal,
                modcodeextournesal,
                'A EXTOURNER',
                null,
                0,
                null,
                pconumtvasal,
                null,
                'N'
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');
   end if;
end;
/

