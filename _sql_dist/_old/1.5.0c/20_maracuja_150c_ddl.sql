SET define OFF
set scan off;
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- Correction erreur de mandatement Papaye
-----------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY maracuja.Bordereau_Papaye IS
--version 1.1.8b - recup du tcd_ordre dans depense
--version 1.1.9 - modifs pour subrogation

PROCEDURE Recup_Brouillards_Payes (moisordre NUMBER)
IS

-- Recuperation de tous les bordereaux de paye deja
CURSOR bordereaux IS
SELECT DISTINCT bor_ordre FROM jefy.bordero WHERE bor_ordre IN (
SELECT bor_ordre FROM jefy.MANDAT WHERE man_ordre IN (
SELECT man_ordre FROM jefy.facture WHERE agt_ordre IN (
SELECT TO_NUMBER(param_value) FROM papaye.paye_parametres WHERE param_key = 'AGENT_JEFY')
AND dep_fact IN (SELECT mois_complet FROM papaye.paye_mois WHERE mois_ordre = moisordre)));
--select distinct bor_ordre from jefy.papaye_compta where mois_ordre = moisordre;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
tboordre maracuja.TYPE_BORDEREAU.tbo_ordre%TYPE;
cpt INTEGER;

BEGIN

  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO borordre;
    EXIT WHEN bordereaux%NOTFOUND;

	maracuja.Bordereau_Papaye.maj_brouillards_payes(moisordre, borordre);

  END LOOP;
  CLOSE bordereaux;

END;

PROCEDURE maj_brouillards_payes (moisordre NUMBER, borordre NUMBER)
IS

borid  	 maracuja.BORDEREAU.bor_id%TYPE;
tboordre maracuja.TYPE_BORDEREAU.tbo_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;

BEGIN

	 SELECT ges_code INTO gescode FROM jefy.bordero WHERE bor_ordre = borordre;

	 -- On verifie que les ecritures aient bien ete generees pour la composante en question.
	 SELECT COUNT(*) INTO cpt
	 FROM papaye.jefy_ecritures WHERE ecr_comp = gescode AND mois_ordre = moisordre;

	 IF (cpt = 0)
	 THEN
	 	 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas pr�par� les �critures pour la composante '||gescode||' !');
	 END IF;

	 -- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
	 SELECT SUM(ecr_mont) INTO sumdebits FROM papaye.jefy_ecritures
	 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' AND ecr_comp = gescode AND ecr_sens = 'D';

	 SELECT SUM(ecr_mont) INTO sumcredits FROM papaye.jefy_ecritures
	 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' AND ecr_comp = gescode AND ecr_sens = 'C';

	 IF (sumcredits <> sumdebits)
	 THEN
	 	 RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||gescode||', la somme des DEBITS ('||sumdebits||') est diff�rente de la somme des CREDITS ('||sumcredits||') !');
	 END IF;


  	SELECT mois_annee INTO exeordre FROM papaye.paye_mois WHERE mois_ordre = moisordre;
	tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

	SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
	WHERE bor_ordre = borordre AND tbo_ordre = tboordre AND exe_ordre = exeordre;

	IF cpt = 0
	THEN

		Bordereau_Papaye.get_btme_jefy(borordre, exeordre);

		SELECT bor_id INTO borid FROM maracuja.BORDEREAU WHERE bor_ordre = borordre AND tbo_ordre = tboordre AND exe_ordre = exeordre;

		Bordereau_Papaye.set_bord_brouillard_visa(borid);

		Bordereau_Papaye.set_bord_brouillard_retenues(borid);

		Bordereau_Papaye.set_bord_brouillard_sacd(borid);

	END IF;

  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Papaye.set_bord_brouillard_paiement(moisordre, borid, exeordre);

END;


PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	utilisateur.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN
SELECT bor_stat INTO etat FROM jefy.bordero WHERE bor_ordre = borordre;

IF etat = 'N' OR etat = 'X'
THEN

-- est ce un bordereau de titre --
SELECT COUNT(*) INTO cpt FROM jefy.MANDAT
WHERE bor_ordre = borordre;

IF cpt != 0 THEN

tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

-- le bordereau est il deja vis� ? --
SELECT COUNT(*) INTO cpt
FROM BORDEREAU
WHERE bor_ordre = borordre
AND exe_ordre = exeordre
AND tbo_ordre = tboordre;

-- l exercice est il encore ouvert ? --

-- pas de raise mais pas de recup s il est deja dans maracuja --
IF cpt = 0 THEN

-- recup des infos --
SELECT * INTO lebordereau FROM jefy.bordero WHERE bor_ordre = borordre;

-- creation du bor_id --
SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

-- creation du bordereau --
INSERT INTO BORDEREAU VALUES (
NULL , 	 	 	      --BOR_DATE_VISA,
'VALIDE',			  --BOR_ETAT,
borid,				  --BOR_ID,
leBordereau.bor_num,  --BOR_NUM,
leBordereau.bor_ordre,--BOR_ORDRE,
exeordre,	  		  --EXE_ORDRE,
leBordereau.ges_code, --GES_CODE,
tboordre,			  --TBO_ORDRE,
1,		 	  		  --UTL_ORDRE,
NULL				  --UTL_ORDRE_VISA
);

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre = borordre);

INSERT INTO BORDEREAU_INFO VALUES
(
borid,
moislibelle,
NULL
);

Bordereau_Papaye.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

END IF;
--else
-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
END IF;
END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

lejefymandat jefy.MANDAT%ROWTYPE;
gescode 	 GESTION.ges_code%TYPE;
manid 		 MANDAT.man_id%TYPE;

MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
PRESTID 	   MANDAT.PREST_ID%TYPE;
TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
modordre	   MANDAT.mod_ordre%TYPE;
CURSOR lesJefyMandats IS
SELECT *
FROM jefy.MANDAT
WHERE bor_ordre =borordre;


BEGIN


--boucle de traitement --
OPEN lesJefyMandats;
LOOP
FETCH lesJefyMandats INTO lejefymandat;
EXIT WHEN lesJefyMandats%NOTFOUND;

-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
MANORGINE_KEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
MANORIGINE_LIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
IF (lejefymandat.conv_ordre IS NOT NULL) THEN
  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
ELSE
  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
END IF;

--PRESTID : PRESTATION INTERNE --
PRESTID :=NULL;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

--MOD_ORDRE --
SELECT mod_ordre INTO modordre
FROM MODE_PAIEMENT
WHERE mod_code =
(
 SELECT MAX(mod_code)
 FROM jefy.factures
 WHERE man_ordre = lejefymandat.man_ordre
)
AND exe_ordre=exeordre;

-- creation du man_id --
SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

-- creation du mandat --
INSERT INTO MANDAT (BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE, 
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT, 
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET, 
		MAN_NB_PIECE, MAN_NUMERO, MAN_NUMERO_REJET, 
		MAN_ORDRE, MAN_ORGINE_KEY, MAN_ORIGINE_LIB, MAN_TTC, 
		MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM, 
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE, 
		RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE, 
		MAN_ATTENTE_PAIEMENT, 
		MAN_ATTENTE_DATE, 
		MAN_ATTENTE_OBJET, 
		UTL_ORDRE_ATTENTE) VALUES
(
 borid ,		   		--BOR_ID,
 NULL, 			   		--BRJ_ORDRE,
 exeordre,		   		--EXE_ORDRE,
 lejefymandat.fou_ordre,--FOU_ORDRE,
 gescode,				--GES_CODE,
 NULL,				    --MAN_DATE_REMISE,
 NULL,					--MAN_DATE_VISA_PRINC,
 'ATTENTE',				--MAN_ETAT,
 'ATTENTE',			    --MAN_ETAT_REMISE,
 lejefymandat.man_mont, --MAN_HT,
 manid,					--MAN_ID,
 NULL,					--MAN_MOTIF_REJET,
 lejefymandat.man_piece,--MAN_NB_PIECE,
 lejefymandat.man_num,	--MAN_NUMERO,
 NULL,					--MAN_NUMERO_REJET,
 lejefymandat.man_ordre,--MAN_ORDRE,
 MANORGINE_KEY,			--MAN_ORGINE_KEY,
 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
 lejefymandat.man_tva,  --MAN_TVA,
 modordre,				--MOD_ORDRE,
 ORIORDRE,				--ORI_ORDRE,
 lejefymandat.pco_num,	--PCO_NUM,
 lejefymandat.PRES_ORDRE,				--PREST_ID,
 TORORDRE,				--TOR_ORDRE,
 VIRORDRE,				--VIR_ORDRE
 lejefymandat.org_ordre,  --org_ordre
 lejefymandat.rib_ordre, --rib ordo
 lejefymandat.rib_ordre, -- rib_comptable
 4,
 NULL,
 NULL,
 NULL
);

Bordereau_Papaye.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
Bordereau_Papaye.set_mandat_brouillard(manid);

END LOOP;

CLOSE lesJefyMandats;

END;


-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
moislibelle papaye.paye_mois.mois_complet%TYPE;

cpt INTEGER;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

tboordre:=Gestionorigine.recup_type_bordereau_mandat(currentBordereau.bor_ordre, exeordre);

SELECT mois_complet INTO moislibelle FROM papaye.paye_mois WHERE mois_ordre = moisordre;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id IN (
SELECT bor_id FROM BORDEREAU WHERE tbo_ordre = tboordre)
AND bob_operation LIKE '%PAIEMENT%' AND bob_libelle2 = moislibelle;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'PAIEMENT SALAIRES',
	currentecriture.pco_num,
	'PAIEMENT SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans PAPAYE.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from papaye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.JEFY_ECRITURES WHERE mois_ordre = mois AND ecr_comp = gescode  AND ecr_type='64' AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||gescode||' , moisordre : '||moisordre);


  OPEN ecriturescredit64(moisordre, gescode);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'VISA SALAIRES',
	currentecriture.pco_num,
	'VISA SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecriturescredit64;

END;

-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans Papaye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = mois AND ecr_comp = gescode AND ecr_type='44';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, gescode);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'RETENUES SALAIRES',
	currentecriture.pco_num,
	'RETENUES SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans Papaye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = mois AND ecr_comp = gescode AND ecr_type='18';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, gescode);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'SACD SALAIRES',
	currentecriture.pco_num,
	'SACD SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecrituressacd;

END;


-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat  	  MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,  						   --ECD_ORDRE,
lemandat.exe_ordre,			   --EXE_ORDRE,
lemandat.ges_code,			   --GES_CODE,
lemandat.man_ht,			   --MAB_MONTANT,
'VISA SALAIRES',				   --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',						   --MAB_SENS,
manid,						   --MAN_ID,
lemandat.pco_num			   --PCO_NU
);

END;

PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid 	  		 DEPENSE.dep_id%TYPE;
jefyfacture 	 jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse  	 DEPENSE.dep_adresse%TYPE;
founom  		 DEPENSE.dep_fournisseur%TYPE;
lotordre  		 DEPENSE.dep_lot%TYPE;
marordre		 DEPENSE.dep_marches%TYPE;
fouordre		 DEPENSE.fou_ordre%TYPE;
gescode			 DEPENSE.ges_code%TYPE;
cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
 			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,				   --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,			   --DEP_ETAT,
founom ,			   --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
lotordre ,			   --DEP_LOT,
marordre ,			   --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,				   --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,				   --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,			   --EXE_ORDRE,
fouordre, 			   --FOU_ORDRE,
gescode,  			   --GES_CODE,
manid ,				   --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
jefyfacture.mod_code,  --MOD_ORDRE,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,    		   --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL -- ecd_ordre_ema
);

END LOOP;
CLOSE factures;

END;


END;
/
