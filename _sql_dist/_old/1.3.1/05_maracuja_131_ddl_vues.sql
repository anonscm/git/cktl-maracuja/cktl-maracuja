CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT, GES_LIB, IMP_LIB, DEBITEUR, BOR_NUM)
AS 
SELECT tr.*, o.ORG_LIB, pc.PCO_LIBELLE, o2.ORG_COMP||' '||o2.ORG_LBUD||' '||o2.ORG_UC, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, v_organ_exer o2, PLAN_COMPTABLE pc, BORDEREAU b
WHERE tr.ges_code = o.org_comp AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND rec_interne = o2.org_ordre AND tr.exe_ordre = o2.exe_ordre and o2.org_niv >=2
AND tr.pco_num = pc.pco_num
AND tr.bor_id = b.bor_id
AND tr.rec_interne IS NOT NULL
UNION
SELECT tr.*, o.ORG_LIB, pc.PCO_LIBELLE, f.ADR_NOM||' '||f.ADR_PRENOM, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, PLAN_COMPTABLE pc, v_fournisseur f, BORDEREAU b
WHERE tr.ges_code = o.org_comp AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.pco_num = pc.pco_num
AND tr.fou_ordre = f.fou_ordre
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NOT NULL AND tr.rec_interne IS NULL
UNION
SELECT tr.*, o.ORG_LIB, pc.PCO_LIBELLE, tr.rec_debiteur, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, PLAN_COMPTABLE pc, BORDEREAU b
WHERE tr.ges_code = o.org_comp AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.pco_num = pc.pco_num
AND tr.bor_id = b.bor_id
AND tr.fou_ordre is NULL AND tr.rec_debiteur IS NOT NULL and length(tr.REC_DEBITEUR)<>0; 
/
---------------------------------