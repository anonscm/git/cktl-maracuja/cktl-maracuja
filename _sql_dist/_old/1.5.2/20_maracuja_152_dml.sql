
-----------------------------------------------------------------------
-----------------------------------------------------------------------
-----------------------------------------------------------------------

-- initialiser les planco_credit pour les recettes
INSERT INTO MARACUJA.PLANCO_CREDIT (
   PCC_ORDRE, TCD_ORDRE, PCO_NUM, 
   PLA_QUOI, PCC_ETAT) 
SELECT MARACUJA.planco_credit_seq.NEXTVAL ,  tcd_ordre, pco_num, 'R', 'VALIDE' 
FROM jefy_admin.TYPE_CREDIT,  maracuja.PLAN_COMPTABLE 
WHERE exe_ordre=2007 AND tcd_type='RECETTE'
AND  tcd_code='01'
AND tcd_ordre NOT IN (SELECT tcd_ordre FROM MARACUJA.PLANCO_CREDIT)
AND pco_num LIKE '7%' AND pco_validite='VALIDE' ;
COMMIT;


INSERT INTO MARACUJA.PLANCO_CREDIT (
   PCC_ORDRE, TCD_ORDRE, PCO_NUM, 
   PLA_QUOI, PCC_ETAT) 
SELECT MARACUJA.planco_credit_seq.NEXTVAL ,  tcd_ordre, pco_num, 'R', 'VALIDE' 
FROM jefy_admin.TYPE_CREDIT,  maracuja.PLAN_COMPTABLE 
WHERE exe_ordre=2007 AND tcd_type='RECETTE'
AND  tcd_code='02'
AND tcd_ordre NOT IN (SELECT tcd_ordre FROM MARACUJA.PLANCO_CREDIT)
AND pco_num LIKE '1%' AND pco_validite='VALIDE' ;
COMMIT;


