SET define OFF
SET scan OFF
--
-----------------------------------------------------------
-- compatibilité avec user accords + initialisation des 
-- planco_credit recettes + ...
-----------------------------------------------------------

drop table maracuja.version_histo cascade constraint;

-- correction erreurs de contrainte aleatoires sur paiement
ALTER TABLE maracuja.MANDAT DROP CONSTRAINT fk_mandat_pai_ordre;
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre)  DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE maracuja.ORDRE_DE_PAIEMENT DROP CONSTRAINT fk_ordre_de_paiement_pai_ordre;
ALTER TABLE maracuja.ORDRE_DE_PAIEMENT ADD (CONSTRAINT fk_ordre_de_paiement_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre)  DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE maracuja.TITRE DROP CONSTRAINT fk_titre_pai_ordre;
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre)  DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE maracuja.VIREMENT_FICHIER DROP CONSTRAINT FK_VIREMENT_FICHIER_PAI_ORDRE;
ALTER TABLE maracuja.VIREMENT_FICHIER ADD  CONSTRAINT FK_VIREMENT_FICHIER_PAI_ORDRE FOREIGN KEY (PAI_ORDRE) REFERENCES maracuja.PAIEMENT (PAI_ORDRE) DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE MARACUJA.PLANCO_AMORTISSEMENT ADD (PCA_DUREE NUMBER default 5 not null);

ALTER TABLE MARACUJA.BORDEREAU  ADD BOR_DATE_CREATION DATE DEFAULT SYSDATE NOT NULL;
ALTER TABLE MARACUJA.DEPENSE  ADD DEP_DATE_FOURNIS DATE;


-------------------------------------------------------------------
-------------------------------------------------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.ZLD_V_CONVENTION_LIMITATIVE
(CON_ORDRE, EXE_ORDRE, ORG_ID, MONTANT)
AS 
SELECT CON_ORDRE,EXE_ORDRE,ORG_ID,SUM(CL_MONTANT) montant
FROM convention.CONVENTION_LIMITATIVE
GROUP BY CON_ORDRE,EXE_ORDRE,ORG_ID;

CREATE OR REPLACE FORCE VIEW MARACUJA.V_CONVENTION_LIMITATIVE
(CON_ORDRE, EXE_ORDRE, ORG_ID, MONTANT)
AS 
SELECT CON_ORDRE,EXE_ORDRE,ORG_ID,SUM(CL_MONTANT) montant
FROM accords.CONVENTION_LIMITATIVE
GROUP BY CON_ORDRE,EXE_ORDRE,ORG_ID;



------------------------------------------------
------------------------------------------------
-- correction vue pour oracle 8i
CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN2
(ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, 
 ORG_UB, ORG_CR, ORG_SOUSCR, ORG_LIB, ORG_LUCRATIVITE, 
 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID)
AS 
SELECT ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_SOUSCR, ORG_LIB, ORG_LUCRATIVITE, ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID
 FROM jefy_admin.organ
UNION
SELECT ORG_ORDRE,org_niv, org_rat,to_char(NULL),ORG_UNIT AS org_etab, ORG_COMP AS org_ub, ORG_LBUD AS org_cr, ORG_UC AS org_sous_cr, ORG_LIB, ORG_LUCRATIVITE, TO_DATE('01/01/2005','dd/mm/yyyy'),TO_DATE('31/12/2006','dd/mm/yyyy'),
to_char(NULL), to_number(NULL),1
FROM v_organ
WHERE org_ordre IN (SELECT org_ordre FROM v_organ MINUS SELECT org_id FROM jefy_admin.organ); 

------------------------------------------------
------------------------------------------------
