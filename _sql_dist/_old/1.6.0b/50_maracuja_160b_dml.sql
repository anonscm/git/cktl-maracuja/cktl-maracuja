-- Correction des erreurs de generation des libelles ecriture_detail pour les OP (depuis la version 1.6.0)
-- les libelle sont du type 'FOURNISSEUR + null'
--------------------------------------------------------------------------------------
DECLARE
	ecdordre ECRITURE_DETAIL.ecd_ordre%TYPE;
	odpnumero ORDRE_DE_PAIEMENT.odp_numero%TYPE;
	odplibelle ORDRE_DE_PAIEMENT.odp_libelle%TYPE;
	adrnom v_fournisseur.adr_nom%TYPE;

CURSOR c1 IS
	SELECT ecd.ecd_ordre, odp.odp_numero, odp.odp_libelle, f.ADR_NOM 
	 FROM ODPAIEM_DETAIL_ECRITURE ocd, ECRITURE_DETAIL ecd, ORDRE_DE_PAIEMENT odp, v_fournisseur f 
	WHERE ocd.ECD_ORDRE = ecd.ecd_ordre
	AND ocd.odp_ordre=odp.odp_ordre
	AND odp.fou_ordre=f.fou_ordre
	AND ecd.exe_ordre=2007
	AND ecd_libelle LIKE '%null%'
	AND OPE_origine='VISA' ;

BEGIN

	OPEN c1;
	LOOP
	FETCH C1 INTO ecdordre, odpnumero, odplibelle, adrnom;
	EXIT WHEN c1%NOTFOUND;
		 UPDATE ECRITURE_DETAIL 
		 SET ecd_libelle = 'VISA O.P. '|| odpnumero || ' ' || adrnom || ' - ' ||  odpLIBELLE
		 WHERE ecd_ordre=ecdordre;
	
	END LOOP;
	CLOSE c1;
	commit;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.6.0b',  SYSDATE, '');

commit;

END;
/