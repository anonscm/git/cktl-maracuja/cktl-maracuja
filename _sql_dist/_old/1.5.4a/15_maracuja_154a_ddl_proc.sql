-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Basculer_Be IS

/*
CRI G guadeloupe - Rivalland Frederic.
CRI LR - Prin Rodolphe

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja lors
du passage des ecritures de balance entree

*/
/*
create table ecriture_detail_be_log
(
edb_ordre integer,
edb_date date,
utl_ordre integer,
ecd_ordre integer)

create sequence basculer_solde_du_copmpte_seq start with 1 nocache;


INSERT INTO TYPE_OPERATION ( TOP_LIBELLE, TOP_ORDRE, TOP_TYPE ) VALUES (
'BALANCE D ENTREE AUTOMATIQUE', 11, 'PRIVEE');

*/
-- PUBLIC --

-- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
PROCEDURE basculer_solde_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
PROCEDURE basculer_DC_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);


PROCEDURE basculer_DC_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER);
PROCEDURE basculer_detail_du_cpt_ges_n(pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT LONG);


-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
PROCEDURE basculer_solde_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);



PROCEDURE basculer_DC_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_DC_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR);

PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER);

-- PRIVATE --
PROCEDURE creer_detail_890 (ecrordre INTEGER,pconumlibelle VARCHAR, gescode VARCHAR);
PROCEDURE priv_archiver_la_bascule( pconum VARCHAR,gescode VARCHAR,utlordre INTEGER, exeordre INTEGER);
 PROCEDURE priv_archiver_la_bascule_ecd ( ecdordre	INTEGER, utlordre   INTEGER);
FUNCTION priv_get_exeordre_prec(exeordre INTEGER) RETURN INTEGER;
PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER);
PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER);
FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR;

FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER;
FUNCTION priv_getTopOrdre RETURN NUMBER;
FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode varchar) RETURN varchar;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Basculer_Be
IS
-- PUBLIC --
   PROCEDURE basculer_solde_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	  topordre := priv_getTopOrdre;
	  
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
-- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

--raise_application_error (-20001,' '||exeordreprec);
		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );
-- 		
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND pco_num = pconumtmp;

--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
--
--          --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);
--          SELECT ABS (lesdebits - lescredits)
--            INTO lesolde
--            FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );		 
		 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
--         basculer_be.archiver_la_bascule (pconumtmp, gescode, utlordre);
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

-- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );		   
	   
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;

         --raise_application_error (-20001,' '||lesdebits||' '||lesens||' '||lesens890||' '||lesolde);
--          SELECT top_ordre
--            INTO topordre
--            FROM TYPE_OPERATION
--           WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
-- 
--          SELECT com_ordre, ges_code
--            INTO comordre, gescodeagence
--            FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );		
	  
	  
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND ges_code = gescode
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde);
      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;


         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, gescode, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );			  

--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
-- --and ges_code = gescode
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
-- --and ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)            --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconum)             --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
	  priv_nettoie_ecriture(monecdordre);
   END;

   -------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	  
	  chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

	   
		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );				 
		 
-- 		 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
-- --and ges_code = gescode
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
-- --and ges_code = gescode
--             AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)          --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

   
 -- Cr�e une ecriture par ecriture_detail recupere
PROCEDURE basculer_detail_du_cpt_ges_n (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
	  gescode			VARCHAR,
      ecrordres   OUT   LONG
   )
   IS
      cpt             INTEGER;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
	  leGesCode       ECRITURE_DETAIL.ges_code%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monEcrOrdre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesEcd
      IS
         SELECT ecd_ordre, ecd_sens, ges_code, ecd_libelle,NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_reste_emarger <> 0
            AND pco_num = pconum
			AND ges_code=gescode;

   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
	  ecrordres := '';
      OPEN lesEcd;

      LOOP
         FETCH lesEcd
          INTO ecdordreOld, lesens, leGesCode, ecdlibelle, lemontant;
         EXIT WHEN lesEcd%NOTFOUND;
		 
		-- creation de lecriture  --
		monEcrOrdre := maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );		 
		 
         -- creation du detail ecriture --
         ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,       --ECDSENS,
                                                     monecrordre,  --ECRORDRE,
                                                     leGesCode, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
		  -- basculer les titre_ecriture_detail  -- 
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);
		  
		  -- creation de du log pour ne plus retraiter les ecritures ! --
		  priv_archiver_la_bascule_ecd(ecdordreOld, utlordre);
		  
		  IF (lesens='C') THEN
			lesens890 := 'D';
		  ELSE
			lesens890 := 'C';
		  END IF;
		  -- creer contrepartie
		  
		  
		  
		  ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                    'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '|| pconum,--ECDLIBELLE,
                                                    lemontant,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    monecrordre,      --ECRORDRE,
                                                    priv_getGesCodeCtrePartie(exeordre, legescode),  --GESCODE,
                                                    '890'             --PCONUM
                                                   );		  
		  maracuja.Api_Plsql_Journal.validerecriture (monEcrOrdre);
		  ecrordres := ecrordres || monecrordre || '$';
		  
      END LOOP;
      CLOSE lesEcd;

      
   END;   
   
   
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   	  
			  chaine := lespconums;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );					   
		   
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND ges_code = gescode
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;


         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconum)            --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
		   chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );					 
		 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
-- 
--       SELECT com_ordre, ges_code
--         INTO comordre, gescodeagence
--         FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      'BE ' || TO_CHAR(exeordre)||' - '||ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      'BE ' || TO_CHAR(exeordre)||' - ' || ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   	  
chaine := lespconums;
     

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      'BE ' || TO_CHAR(exeordre)||' - '|| ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - '|| ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
   END;






------------------------------------------
------------------------------------------
   -- permet de retrouver le compte dans Maracuja comme s'il n'avait pas ete transfere en BE
    PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER)
       IS
       cpt INTEGER;
       BEGIN

	   SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL_BE_LOG edb, ECRITURE_DETAIL ecd WHERE edb.ecd_ordre=ecd.ecd_ordre AND ecd.pco_num=pconum AND ecd.exe_ordre=exeordreold;
    	IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture de n'' a �t� transf�r�e en BE pour le compte '|| pconum);
    	END IF;

		DELETE FROM ECRITURE_DETAIL_BE_LOG WHERE ecd_ordre IN (SELECT ecd_ordre FROM ECRITURE_DETAIL WHERE pco_num=pconum AND exe_ordre=exeordreold);


    END;



-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- 
--    -- PRIVATE --
--    PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR)
--    IS
--       cpt             INTEGER;
--       lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
--       lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
--       topordre        TYPE_OPERATION.top_ordre%TYPE;
--       comordre        COMPTABILITE.com_ordre%TYPE;
--       ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
--       gescodeagence   COMPTABILITE.ges_code%TYPE;
-- 	  exeordre		  ECRITURE.EXE_ORDRE%TYPE;
--       monecdordre     INTEGER;
--    BEGIN
--       SELECT com_ordre, ges_code
--         INTO comordre, gescodeagence
--         FROM COMPTABILITE;
-- 
-- 		SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;
-- 		
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL
--        WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL
--        WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';
-- 
--       IF (ABS (lesdebits) >= ABS (lescredits))
--       THEN
--          lesens890 := 'C';
--          lesolde := lesdebits - lescredits;
--       ELSE
--          lesens890 := 'D';
--          lesolde := lescredits - lesdebits;
--       END IF;
-- 
-- --       SELECT ABS (lesdebits - lescredits)
-- --         INTO lesolde
-- --         FROM DUAL;
-- --
--  --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde||' '||ecrordre);
--       IF lesolde != 0
--       THEN
-- --           IF (lesdebits >= lescredits)
-- --           THEN
-- --              lesens890 := 'C';
-- --           ELSE
-- --              lesens890 := 'D';
-- --           END IF;
-- 
--          -- creation du detail pour l'ecriture selon le CREDIT --
--          ecdordre :=
--             maracuja.Api_Plsql_Journal.creerecrituredetail
--                                                    (NULL,    --ECDCOMMENTAIRE,
--                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
--                                                     || pconumlibelle,
--                                                     --ECDLIBELLE,
--                                                     lesolde,     --ECDMONTANT,
--                                                     NULL,     --ECDSECONDAIRE,
--                                                     lesens890,      --ECDSENS,
--                                                     ecrordre,      --ECRORDRE,
--                                                     gescodeagence,  --GESCODE,
--                                                     '890'             --PCONUM
--                                                    );
-- -- Rod : ne pas bloquer (sinon pas possible de reprendre detail equilibre)
-- --      ELSE
-- --          raise_application_error (-20001,
-- --                                      'OUPS PROBLEME ECRITURE NON fred!!! '
-- --                                   || lesdebits
-- --                                   || ' '
-- --                                   || lescredits
-- --                                  );
--       END IF;
--    END;

   
   
PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR, gescode VARCHAR)
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
	  exeordre		  ECRITURE.EXE_ORDRE%TYPE;
      monecdordre     INTEGER;
   BEGIN
      SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;
	  
	  SELECT com_ordre, ges_code INTO comordre, gescodeagence
        FROM COMPTABILITE;
		
		-- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
		gescodeagence := priv_getGesCodeCtrePartie(exeordre, gescode);
		
      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         -- creation du detail pour l'ecriture
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumlibelle,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    ecrordre,      --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
      END IF;
   END;
   
   
   
   
   
   
   PROCEDURE priv_archiver_la_bascule_ecd (
      ecdordre	INTEGER,
      utlordre   INTEGER
   )
   IS
   BEGIN
   
		INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre, ecdordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre = ecdordre 
			 AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG);  
   
   END;
   
   
   PROCEDURE priv_archiver_la_bascule (
      pconum     VARCHAR,
      gescode    VARCHAR,
      utlordre   INTEGER,
      exeordre   INTEGER
   )
   IS
   BEGIN
      IF gescode IS NULL
      THEN
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND exe_ordre = exeordre
               AND pco_num = pconum;
      ELSE
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND pco_num = pconum
               AND exe_ordre = exeordre
               AND ges_code = gescode;
      END IF;
   END;

-- Renvoie l'identifiant de l'exercice precedent celui identifie par exeordre
   FUNCTION priv_get_exeordre_prec (exeordre INTEGER)
      RETURN INTEGER
   IS
      reponse           INTEGER;
      exeexerciceprec   EXERCICE.exe_exercice%TYPE;
   BEGIN
      SELECT exe_exercice - 1
        INTO exeexerciceprec
        FROM EXERCICE
       WHERE exe_ordre = exeordre;

      SELECT exe_ordre
        INTO reponse
        FROM EXERCICE
       WHERE exe_exercice = exeexerciceprec;

      RETURN reponse;
   END;


   PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER) IS
   BEGIN
   		DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = ecrOrdre AND ecd_montant=0 AND ecd_debit=0 AND ecd_credit=0;

   END;



   -- renvoie le compte de BE a utiliser (par ex renvoie 4011 pour 4012)
   -- ce compte est indique dans la table plan_comptable.
   -- si non precise, renvoie le compte passe en parametre
   FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR
   IS
   	 pconumnew PLAN_COMPTABLE.pco_num%TYPE;
   BEGIN
   		SELECT pco_compte_be INTO pconumnew FROM PLAN_COMPTABLE WHERE pco_num=pconumold;
		IF pconumnew IS NULL THEN
		   pconumnew := pconumold;
		END IF;
		RETURN pconumnew;
   END;




    -- Permet de suivre le detailecriture cree lors de la BE pour un titre.
    PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER)
       IS
       cpt INTEGER;
       infos TITRE_DETAIL_ECRITURE%ROWTYPE;
       BEGIN
       -- est un ecdordre pour un titre ?
          SELECT COUNT(*)
            INTO cpt
            FROM TITRE_DETAIL_ECRITURE
           WHERE ecd_ordre = ecdordreold;
    	IF cpt = 1 THEN
    		SELECT * INTO infos
       		FROM TITRE_DETAIL_ECRITURE
       	 WHERE ecd_ordre = ecdordreold;
      	INSERT INTO TITRE_DETAIL_ECRITURE (ECD_ORDRE, EXE_ORDRE, ORI_ORDRE, TDE_DATE, TDE_ORDRE, TDE_ORIGINE, TIT_ID, REC_ID)
			   VALUES (ecdordrenew, exeordrenew, infos.ORI_ORDRE, SYSDATE, titre_detail_ecriture_seq.NEXTVAL, infos.TDE_ORIGINE, infos.TIT_ID, infos.rec_id);
    	END IF;


    END;

	-- renvoi le solde non emarge des ecritures non prises en compte en BE
	FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER
	IS
	  lesolde NUMBER; 
	BEGIN
	   IF (gescode IS NULL) THEN	 
		      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
		        INTO lesolde
		        FROM ECRITURE_DETAIL ecd, ECRITURE e
		       WHERE e.ecr_ordre = ecd.ecr_ordre
		         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
		         AND e.exe_ordre = exeordreprec
		         AND ecd_ordre NOT IN (SELECT ecd_ordre
		                                 FROM ECRITURE_DETAIL_BE_LOG)
		         AND ecd_sens = sens
		         AND pco_num = pconum;	
		ELSE
		      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
		        INTO lesolde
		        FROM ECRITURE_DETAIL ecd, ECRITURE e
		       WHERE e.ecr_ordre = ecd.ecr_ordre
		         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
		         AND e.exe_ordre = exeordreprec
				 AND ges_code = gescode
		         AND ecd_ordre NOT IN (SELECT ecd_ordre
		                                 FROM ECRITURE_DETAIL_BE_LOG)
		         AND ecd_sens = sens
		         AND pco_num = pconum;			
		END IF;
		RETURN lesolde;
	END;
	
	
	FUNCTION priv_getTopOrdre RETURN NUMBER
	IS
	  topordre NUMBER;
	BEGIN
	      SELECT top_ordre INTO topordre
        		FROM TYPE_OPERATION
       			WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
			RETURN  topordre;
	END;
	
	-- renvoie gescode si sacd ou gescodeagence
	FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR
	IS
	  gescodeagence COMPTABILITE.ges_code%TYPE;
	  cpt INTEGER;
	BEGIN
	  SELECT ges_code INTO gescodeagence
        FROM COMPTABILITE;
		
		-- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
		IF gescode IS NOT NULL THEN		
		   		   SELECT COUNT(*) INTO cpt FROM GESTION_EXERCICE 
				   		  WHERE exe_ordre = exeordre AND ges_code=gescode AND pco_num_185 IS NOT NULL;
					IF cpt<>0 THEN
					   		  gescodeagence := gescode;
					END IF;
		END IF; 
		
		RETURN gescodeagence;
		
	END;
	
	
	
END;
/


-----------------------------------------------
-----------------------------------------------
-----------------------------------------------


CREATE OR REPLACE PACKAGE MARACUJA.Numerotationobject IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (ecrordre INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);
PROCEDURE numeroter_bordereau (borid INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

PROCEDURE numeroter_mandat (borid INTEGER);

PROCEDURE numeroter_titre (borid INTEGER);

PROCEDURE numeroter_orv (borid INTEGER);

PROCEDURE numeroter_aor (borid INTEGER);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Numerotationobject IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE BROUILLARD INEXISTANTE , CLE '||ecrordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero_brouillard = lenumero
WHERE ecr_ordre = ecrordre;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM PAIEMENT
WHERE pai_ordre = paiordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='VIREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE PAIEMENT SET pai_numero = lenumero
WHERE pai_ordre = paiordre;

END;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_ordre = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
--select com_ordre,exe_ordre
SELECT NULL,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_ordre = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
--update retenue set ret_numero = lenumero
--where ret_ordre = retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

--COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

IF gescode IS NULL THEN
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code IS NULL
AND tnu_ordre = tnuordre;
ELSE
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code = gescode
AND tnu_ordre = tnuordre;
END IF;

IF cpt  = 0 THEN
--raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
 SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


 INSERT INTO NUMEROTATION
 ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
 VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
 RETURN 1;
ELSE
 IF gescode IS NOT NULL THEN
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;
 ELSE
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;
 END IF;
 RETURN cpt;
END IF;
END ;




PROCEDURE numeroter_bordereau (borid INTEGER) IS


cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

lebordereau BORDEREAU%ROWTYPE;


BEGIN

SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU  INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,t.tbo_ordre,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,tboordre,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



SELECT  COUNT(*) INTO cpt
FROM maracuja.TYPE_BORDEREAU tb , maracuja.TYPE_NUMEROTATION tn
WHERE tn.tnu_ordre = tb.tnu_ordre
AND tb.tbo_ordre = tboordre;

IF cpt = 1 THEN
 SELECT tn.tnu_ordre INTO tnuordre
 FROM maracuja.TYPE_BORDEREAU tb , TYPE_NUMEROTATION tn
 WHERE tn.tnu_ordre = tb.tnu_ordre
 AND tb.tbo_ordre = tboordre;
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION'||borid);
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

IF lenumero IS NULL THEN
 RAISE_APPLICATION_ERROR (-20001,'PROBLEME DE NUMEROTATION'||borid);
END IF;


-- affectation du numero -
UPDATE maracuja.BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;

END;


PROCEDURE numeroter_mandat (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid ORDER BY pco_num;

BEGIN


-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;



-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_titre (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid ORDER BY pco_num;


BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
  
 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;



PROCEDURE numeroter_orv (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORV';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_aor (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='AOR';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;

END;
/


CREATE OR REPLACE
PACKAGE BODY          Gestionorigine IS
--version 1.1.7 31/05/2005 - mise a jour libelle origine dans le cas des conventions
--version 1.1.6 27/04/2005 - ajout maj_origine
--version 1.2.0 20/12/2005 - Multi exercices jefy
--version 1.2.1 04/01/2006

FUNCTION traiter_orgordre (orgordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

IF orgordre IS NULL THEN RETURN NULL; END IF;

-- recup du type_origine --
SELECT top_ordre INTO topordre FROM TYPE_OPERATION
WHERE top_libelle = 'OPERATION LUCRATIVE';



-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ORDRE'
AND ORI_ENTITE ='JEFY.ORGAN'
AND ORI_KEY_ENTITE	=orgordre;

IF cpt >= 1 THEN
	SELECT ori_ordre INTO cpt FROM ORIGINE
	WHERE ORI_KEY_NAME = 'ORG_ORDRE'
	AND ORI_ENTITE ='JEFY.ORGAN'
	AND ORI_KEY_ENTITE	=orgordre
	AND ROWNUM=1;

ELSE
	SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

	-- recup de la LIGNE BUDGETAIRE --
	--le libelle utilisateur pour le suivie en compta --
	SELECT org_comp||'-'||org_lbud||'-'||org_uc
	 INTO orilibelle
	FROM jefy.organ
	WHERE org_ordre = orgordre;

	INSERT INTO ORIGINE
	(ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
	VALUES ('JEFY.ORGAN','ORG_ORDRE',orilibelle,cpt,orgordre,topordre);

END IF;

RETURN cpt;

END;


FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
BEGIN

IF orgid IS NULL THEN RETURN NULL; END IF;

-- SELECT COUNT(*) INTO cpt
-- FROM convention.CONVENTION_LIMITATIVE
-- WHERE org_id = orgid AND exe_ordre = exeordre;

SELECT COUNT(*) INTO cpt
FROM accords.CONVENTION_LIMITATIVE
WHERE org_id = orgid AND exe_ordre = exeordre;


IF cpt >0 THEN
 -- recup du type_origine CONVENTION--
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

--  SELECT DISTINCT con_ordre INTO convordre
--  FROM convention.CONVENTION_LIMITATIVE
--  WHERE org_id = orgid
--  AND exe_ordre = exeordre;

  SELECT max(con_ordre) INTO convordre
 FROM accords.CONVENTION_LIMITATIVE
 WHERE org_id = orgid
 AND exe_ordre = exeordre;

--  SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET)
--  INTO orilibelle
--  FROM convention.contrat
--  WHERE con_ordre = convordre;

  SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET)
 INTO orilibelle
 FROM accords.CONTRAT
 WHERE con_ordre = convordre;

ELSE
 SELECT COUNT(*) INTO cpt
 FROM jefy_admin.organ
 WHERE org_id = orgid
 AND org_lucrativite = 1;

 IF cpt = 1 THEN
 -- recup du type_origine OPERATION LUCRATIVE --
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'OPERATION LUCRATIVE';

 --le libelle utilisateur pour le suivie en compta --
 SELECT org_UB||'-'||org_CR||'-'||org_souscr
 INTO orilibelle
 FROM jefy_admin.organ
 WHERE org_id = orgid;

 ELSE
  RETURN NULL;
 END IF;
END IF;

-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ID'
AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
AND ORI_KEY_ENTITE	=orgid;

IF cpt >= 1 THEN
	SELECT ori_ordre INTO cpt FROM ORIGINE
	WHERE ORI_KEY_NAME = 'ORG_ID'
	AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
	AND ORI_KEY_ENTITE	=orgid
	AND ROWNUM=1;

ELSE
	SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

	INSERT INTO ORIGINE (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
	VALUES ('JEFY_ADMIN','ORG_ID',orilibelle,cpt,orgid,topordre);

END IF;

RETURN cpt;

END;


FUNCTION traiter_convordre (convordre INTEGER)
RETURN INTEGER

IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	IF convordre IS NULL THEN RETURN NULL; END IF;

	-- recup du type_origine --
	SELECT top_ordre INTO topordre FROM TYPE_OPERATION
	WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

	-- l origine est t elle deja  suivie --
	SELECT COUNT(*)INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;


	-- recup de la convention --
	--le libelle utilisateur pour le suivie en compta --
--	select CON_REFERENCE_EXTERNE||' '||CON_OBJET into orilibelle from convention.contrat where con_ordre=convordre;
--on change le libelle (demande INP Toulouse 30/05/2005)
--	SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM convention.contrat WHERE con_ordre=convordre;
		SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM accords.CONTRAT WHERE con_ordre=convordre;


	-- l'origine est deja referencee
	IF cpt = 1 THEN
		SELECT ori_ordre INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;

		--on met a jour le libelle
		UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

	-- il faut creer l'origine
	ELSE
		SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
		INSERT INTO ORIGINE VALUES (
				'CONVENTION.CONTRAT' ,--	  USER.TABLE  -
				'CONV_ORDRE',-- 	  PRIMARY KEY -
				ORILIBELLE,-- 	  LIBELLE UTILISATEUR -
				cpt ,--		  ID -
				convordre,--	  VALEUR DE LA KEY -
				topordre--		  type d origine -
			);

	END IF;

	RETURN cpt;
END;


FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
letitrejefy jefy.TITRE%ROWTYPE;
BEGIN


IF exeordre=2005 THEN
	SELECT * INTO lebordereau
	FROM jefy05.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT * INTO letitrejefy FROM jefy05.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
	   FROM jefy05.TITRE
	   WHERE bor_ordre =lebordereau.bor_ordre);
ELSE
	SELECT * INTO lebordereau
	FROM jefy.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT * INTO letitrejefy FROM jefy.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
	   FROM jefy.TITRE
	   WHERE bor_ordre =lebordereau.bor_ordre);
END IF;




IF letitrejefy.tit_type ='V' THEN
   SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REVERSEMENTS';
ELSE
  IF letitrejefy.tit_type ='D' THEN
    SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REDUCTIONS';
  ELSE
    IF letitrejefy.tit_type ='P' THEN
      SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU D ORDRE DE PAIEMENT';
    ELSE
	  SELECT tbo_ordre INTO tboordre
      FROM TYPE_BORDEREAU
      WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' AND EXE_ORDRE=exeordre);
    END IF;
  END IF;
END IF;


RETURN tboordre;
END;

FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;
fouordrepapaye jefy.MANDAT.fou_ordre%TYPE;
agtordre 	jefy.facture.agt_ordre%TYPE;
fouordre 	jefy.MANDAT.fou_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN

IF exeordre=2005 THEN
	-- recup des infos --
	SELECT * INTO lebordereau
	FROM jefy05.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT MAX(agt_ordre) INTO agtordre
	FROM jefy05.facture
	WHERE man_ordre =
	 ( SELECT MAX(man_ordre)
	   FROM jefy05.MANDAT
	   WHERE bor_ordre =borordre
	  );

	SELECT MAX(fou_ordre) INTO fouordre
	   FROM jefy05.MANDAT
	   WHERE bor_ordre =borordre;
ELSE
	-- recup des infos --
	SELECT * INTO lebordereau
	FROM jefy.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT MAX(agt_ordre) INTO agtordre
	FROM jefy.facture
	WHERE man_ordre =
	 ( SELECT MAX(man_ordre)
	   FROM jefy.MANDAT
	   WHERE bor_ordre =borordre
	  );

	SELECT MAX(fou_ordre) INTO fouordre
	   FROM jefy.MANDAT
	   WHERE bor_ordre =borordre;
END IF;

-- recuperation du type de bordereau --
SELECT COUNT(*) INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
 (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');


IF (fouordrepapaye <> 0) THEN
   SELECT fou_ordre INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
   (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
ELSE
	fouordrepapaye := NULL;
END IF;



IF fouordre =fouordrepapaye THEN
  SELECT tbo_ordre INTO tboordre
  FROM TYPE_BORDEREAU
  WHERE tbo_libelle='BORDEREAU DE MANDAT SALAIRES';
ELSE
  SELECT COUNT(*) INTO cpt
  FROM OLD_AGENT_TYPE_BORD
  WHERE agt_ordre = agtordre;

  IF cpt <> 0 THEN
   SELECT tbo_ordre INTO tboordre
   FROM OLD_AGENT_TYPE_BORD
   WHERE agt_ordre = agtordre;
  ELSE
   SELECT tbo_ordre INTO tboordre
   FROM TYPE_BORDEREAU
   WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTME GENERIQUE' AND exe_ordre = exeordre);
  END IF;

END IF;


RETURN tboordre;
END;

FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
BEGIN

RETURN 1;
END;


PROCEDURE Maj_Origine
IS
-- 	CURSOR c1 IS
-- 	SELECT org_ordre FROM jefy.organ
-- 	WHERE org_lucrativite !=0;

	CURSOR c1 IS
	SELECT org_id, exe_ordre FROM v_organ_exer
	WHERE org_lucrativite !=0;

-- 	CURSOR c2 IS
-- 	SELECT con_ordre FROM convention.contrat
-- 	WHERE con_suppr='N';

	CURSOR c2 IS
	SELECT con_ordre FROM accords.CONTRAT
	WHERE con_suppr='N';
	orgordre INTEGER;
	conordre INTEGER;
	exeordre INTEGER;
	cpt INTEGER;
BEGIN

-- 	OPEN c1;
-- 	LOOP
-- 		FETCH C1 INTO orgordre;
-- 			  EXIT WHEN c1%NOTFOUND;
-- 			  cpt := TRAITER_ORGORDRE ( orgordre );
-- 	END LOOP;
-- 	CLOSE c1;
	OPEN c1;
	LOOP
		FETCH C1 INTO orgordre, exeordre;
			  EXIT WHEN c1%NOTFOUND;
			  cpt := TRAITER_ORGid ( orgordre , exeordre);
	END LOOP;
	CLOSE c1;

	OPEN c2;
	LOOP
		FETCH C2 INTO conordre;
			  EXIT WHEN c2%NOTFOUND;
			  cpt := TRAITER_CONVORDRE ( conordre );
	END LOOP;
	CLOSE c2;


END;


END;
