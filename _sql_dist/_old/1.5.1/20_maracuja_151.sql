SET define OFF
set scan off
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- 
-----------------------------------------------------------

ALTER TABLE maracuja.VIREMENT_PARAM_BDF ADD vpb_nom_tpg VARCHAR2(100);
ALTER TABLE maracuja.PRELEVEMENT_PARAM_BDF ADD ppb_nom_tpg VARCHAR2(100);

COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_BDF.vpb_nom_tpg IS 'Nom de la TPG (apparait sur le bordereau)';
COMMENT ON COLUMN MARACUJA.PRELEVEMENT_PARAM_BDF.ppb_nom_tpg IS 'Nom de la TPG (apparait sur le bordereau)';

UPDATE maracuja.VIREMENT_PARAM_BDF SET vpb_nom_tpg = (SELECT par_value FROM maracuja.PARAMETRE WHERE exe_ordre=2007 AND par_key='NOM_TPG');
UPDATE maracuja.PRELEVEMENT_PARAM_BDF SET ppb_nom_tpg = (SELECT par_value FROM maracuja.PARAMETRE WHERE exe_ordre=2007 AND par_key='NOM_TPG');

commit;

ALTER TABLE maracuja.VIREMENT_PARAM_BDF ADD vpb_nom_remettant VARCHAR2(100);
ALTER TABLE maracuja.PRELEVEMENT_PARAM_BDF ADD ppb_nom_remettant VARCHAR2(100);

COMMENT ON COLUMN MARACUJA.VIREMENT_PARAM_BDF.vpb_nom_remettant IS 'Nom de l etablissement remettant (apparait sur le bordereau)';
COMMENT ON COLUMN MARACUJA.PRELEVEMENT_PARAM_BDF.ppb_nom_remettant IS 'Nom de l etablissement remettant (apparait sur le bordereau)';


UPDATE maracuja.VIREMENT_PARAM_BDF SET vpb_nom_remettant = (SELECT par_value FROM maracuja.PARAMETRE WHERE exe_ordre=2007 AND par_key='UNIV_TPG_REMETTANT');
UPDATE maracuja.PRELEVEMENT_PARAM_BDF SET ppb_nom_remettant = (SELECT par_value FROM maracuja.PARAMETRE WHERE exe_ordre=2007 AND par_key='UNIV_TPG_REMETTANT');
commit;

-- on ne supprime pas les parametres tout de suite
--delete from maracuja.parametre where exe_ordre=2007 AND par_key='UNIV_TPG_REMETTANT';
--delete from maracuja.parametre where exe_ordre=2007 AND par_key='NOM_TPG';
--delete from maracuja.parametre where exe_ordre=2007 AND par_key='COMPTE_TPG';
--delete from maracuja.parametre where exe_ordre=2007 AND par_key='ID_TPG';
--delete from maracuja.parametre where exe_ordre=2007 AND par_key='COMPTE_TPG_RECETTE';

INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.5.1',  SYSDATE, '');
COMMIT; 
