-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PROCEDURE COMPTEFI.Prepare_Bilan (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  totalbrut NUMBER(12,2);
  totalamort NUMBER(12,2);
  totalnet NUMBER(12,2);
  totalnetant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);

  -- version du 20/02/2005

BEGIN

--*************** CREATION TABLE ACTIF *********************************
IF sacd = 'O' THEN
	DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
	DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- ACTIF IMMOBILISE ----
lib1:= 'ACTIF IMMOBILISE';
---- Immobilisations Incorporelles ---
lib2:= 'IMMOBILISATIONS INCORPORELLES';

-- Compte 201 ---
	totalbrut := Solde_Compte(exeordre, '201%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2801%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2831%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '201%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2801%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2831%', 'C', gescode, sacd));
	lib := 'Frais d''�tablissement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 203 ---
	totalbrut := Solde_Compte(exeordre, '203%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2803%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2833%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '203%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2803%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2833%', 'C', gescode, sacd));
	lib := 'Frais de recherche et de d�veloppement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 205 ---
	totalbrut := Solde_Compte(exeordre, '205%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2805%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2835%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2905%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '205%','D', gescode, sacd)
		- (Solde_Compte(exeordre-1, '2805%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2835%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2905%', 'C', gescode, sacd));
	lib := 'Concessions et droits similaires';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 206 ---
	totalbrut := Solde_Compte(exeordre, '206%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2906%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '206%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '2906%', 'C', gescode, sacd);
	lib := 'Droit au bail';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 208 ---
	totalbrut := Solde_Compte(exeordre, '208%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2808%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2908%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2838%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '208%','D', gescode, sacd)
		- (Solde_Compte(exeordre-1, '2808%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2908%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2838%', 'C', gescode, sacd));
	lib := 'Autres immobilisations incorporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 232 ---
	totalbrut := Solde_Compte(exeordre, '232%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2932%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '232%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '2932%', 'C', gescode, sacd);
	lib := 'Immobilisations incorporelles en cours';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 237 ---
	totalbrut := Solde_Compte(exeordre, '237%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '237%','D', gescode, sacd);
	lib := 'Avances et acomptes vers�s sur immobilisations incorporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);



---- Immobilisations Corporelles ---
lib2:= 'IMMOBILISATIONS CORPORELLES';

-- Compte 211 et 212 ---
	totalbrut := Solde_Compte(exeordre, '211%','D', gescode, sacd)
		+Solde_Compte(exeordre, '212%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2812%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2911%', 'C', 	gescode, sacd)
		+Solde_Compte(exeordre, '2842%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '211%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '212%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2812%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2911%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2842%', 'C', gescode, sacd));
	lib := 'Terrains, agencements et am�nagements de terrain';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 213 et 214 ---
	totalbrut := Solde_Compte(exeordre, '213%','D', gescode, sacd)
		+Solde_Compte(exeordre, '214%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2813%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2814%', 'C', 	gescode, sacd)
		+Solde_Compte(exeordre, '2843%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2844%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '213%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '214%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2813%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2814%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2843%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2844%', 'C', gescode, sacd));
	lib := 'Constructions et constructions sur sol d''autrui';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 215 ---
	totalbrut := Solde_Compte(exeordre, '215%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2815%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2845%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '215%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2815%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2845%', 'C', gescode, sacd));
	lib := 'Installations techniques, mat�riels et outillage';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 216 ---
	totalbrut := Solde_Compte(exeordre, '216%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2816%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2846%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '216%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2816%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2846%', 'C', gescode, sacd));
	lib := 'Collections';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 218 ---
	totalbrut := Solde_Compte(exeordre, '218%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2818%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2848%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '218%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2818%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2848%', 'C', gescode, sacd));
	lib := 'Autres immobilisations corporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 231 ---
	totalbrut := Solde_Compte(exeordre, '231%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2931%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '231%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '2931%', 'C', gescode, sacd);
	lib := 'Immobilisations corporelles en cours';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 238 ---
	totalbrut := Solde_Compte(exeordre, '238%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '238%','D', gescode, sacd);
	lib := 'Avances et acomptes vers�s sur commandes d''immobilisation corporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Immobilisations Financi�res ---
lib2:= 'IMMOBILISATIONS FINANCIERES';

-- Compte 261 et 266 ---
	totalbrut := Solde_Compte(exeordre, '261%','D', gescode, sacd)
		+Solde_Compte(exeordre, '266%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2961%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2966%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '261%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '266%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2961%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2966%', 'C', gescode, sacd));
	lib := 'Participations';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 267 et 268 ---
	totalbrut := Solde_Compte(exeordre, '267%','D', gescode, sacd)
		+Solde_Compte(exeordre, '268%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2967%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2968%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '267%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '268%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2967%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2968%', 'C', gescode, sacd));
	lib := 'Cr�ances rattach�es � des participations';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 271 et 272 ---
	totalbrut := Solde_Compte(exeordre, '271%','D', gescode, sacd)
		+Solde_Compte(exeordre, '272%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2971%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2972%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '271%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '272%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2971%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2972%', 'C', gescode, sacd));
	lib := 'Autres titres immobilis�s';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 274 --
	totalbrut := Solde_Compte(exeordre, '274%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2974%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '274%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '2974%', 'C', gescode, sacd);
	lib := 'Pr�ts';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 275 et 2761  --
totalbrut := Solde_Compte(exeordre, '275%','D', gescode, sacd)
	+Solde_Compte(exeordre, '2761%','D', gescode, sacd)
	+Solde_Compte(exeordre, '2768%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2975%', 'C', gescode, sacd)
	+Solde_Compte(exeordre, '2976%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '275%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '2761%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '2768%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2975%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2976%', 'C', gescode, sacd));
	lib := 'Autres immobilisations financi�res';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


----- ACTIF CIRCULANT ----
lib1:= 'ACTIF CIRCULANT';
---- Stocks ---
lib2:= 'STOCKS ET EN-COURS';

-- Compte 31 et 32---
	totalbrut := Solde_Compte(exeordre, '31%','D', gescode, sacd)
		+Solde_Compte(exeordre, '32%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '391%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '392%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '31%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '31%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '391%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '392%', 'C', gescode, sacd));
	lib := 'Mati�res premi�res et autres approvisionnements';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 33 et 34 ---
	totalbrut := Solde_Compte(exeordre, '33%','D', gescode, sacd)
		+Solde_Compte(exeordre, '34%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '393%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '394%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '33%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '34%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '393%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '394%', 'C', gescode, sacd));
	lib := 'En-cours de production de biens et de services';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 35 ---
	totalbrut := Solde_Compte(exeordre, '35%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '395%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '35%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '395%', 'C', gescode, sacd);
	lib := 'Produits interm�diaires et finis';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 37 ---
	totalbrut := Solde_Compte(exeordre, '37%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '397%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '37%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '397%', 'C', gescode, sacd);
	lib := 'Marchandises';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Avances et acomptes vers�s sur commande ---
lib2:= 'AVANCES ET ACOMPTES';
-- Compte 4091 et 4092 ---
	totalbrut := Solde_Compte(exeordre, '4091%','D', gescode, sacd)
		+Solde_Compte(exeordre, '4092%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4091%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '4092%','D', gescode, sacd);
	lib := 'Avances et acomptes vers�s sur commandes';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Cr�ances exploitations ---
lib2:= 'CREANCES D''EXPLOITATION';

-- Compte 411, 412, 413, 416 et 418 ---
	totalbrut := Solde_Compte(exeordre, '411%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '412%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '413%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '416%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '418%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '491%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '411%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '412%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '413%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '416%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '418%','D', gescode, sacd);
	lib := 'Cr�ances clients et comptes rattach�s';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte autres ---
	totalbrut := Solde_Compte(exeordre, '4096%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4098%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '425%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4287%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4387%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4417%','D', gescode, sacd)

		+ Solde_Compte(exeordre, '443%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4487%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4684%','D', gescode, sacd)
		+ (Solde_Compte_Ext(exeordre, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
		- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
--		+ Solde_Compte(exeordre, '472%','D', gescode, sacd)
--		--- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
		+ Solde_Compte(exeordre, '4735%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '478%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4096%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4098%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '425%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4287%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4387%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4417%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '443%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4487%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4684%','D', gescode, sacd)
		+ (Solde_Compte_Ext(exeordre-1, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
		- Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd))
		+ Solde_Compte(exeordre-1, '472%','D', gescode, sacd)		
		---Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4735%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '478%','D', gescode, sacd);
	lib := 'Autres cr�ances d''exploitation';
	dbms_output.put_line(lib||' '||totalbrut);
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Cr�ances diverses ---
lib2:= 'CREANCES DIVERSES';

-- Compte TVA ---
	totalbrut := Solde_Compte(exeordre, '4456%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '44581%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '44583%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4456%','D', gescode, sacd)+Solde_Compte(exeordre-1, '44581%','D', gescode, sacd)+Solde_Compte(exeordre-1, '44583%','D', gescode, sacd);
	lib := 'TVA';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Comptes Autres --- Sp�cial LR 4412 4413 !!
	totalbrut := Solde_Compte(exeordre, '4411%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4412%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4413%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4418%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '444%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '462%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '463%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '465%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '467%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4687%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '495%', 'C', gescode, sacd)
	 	+ Solde_Compte(exeordre, '496%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '4411%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4418%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '444%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '462%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '463%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '465%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '467%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4687%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '495%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '496%', 'C', gescode, sacd));
	lib := 'Autres cr�ances diverses';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Tr�sorerie ---
lib2:= 'TRESORERIE';

-- Compte 50---
	totalbrut := Solde_Compte(exeordre, '50%','D', gescode, sacd)
		- Solde_Compte(exeordre, '509%', 'C', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '590%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '50%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '509%', 'C', gescode, sacd))
		- Solde_Compte(exeordre-1, '590%', 'C', gescode, sacd);
	lib := 'Valeurs mobili�res de placement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte Disponibilit�s ---
	totalbrut := Solde_Compte(exeordre, '51%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '53%', 'D', gescode, sacd)
		+ Solde_Compte(exeordre, '54%','D', gescode, sacd)
		- Solde_Compte(exeordre, '51%','C', gescode, sacd)
		- Solde_Compte(exeordre, '54%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '185%','D', gescode, sacd)
		- Solde_Compte(exeordre, '185%', 'C', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '51%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '53%', 'D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '54%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '51%','C', gescode, sacd)
		- Solde_Compte(exeordre-1, '54%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '185%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '185%', 'C', gescode, sacd);
	lib:= 'Disponibilit�s';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 486 ---
	totalbrut := Solde_Compte(exeordre, '486%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '486%','D', gescode, sacd);
	lib := 'Charges constat�es d''avance';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '';

-- Compte 481---
	totalbrut := Solde_Compte(exeordre, '481%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '481%','D', gescode, sacd);
	lib := 'Charges � r�partir sur plusieurs exercices';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 476 ---
	totalbrut := Solde_Compte(exeordre, '476%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '476%','D', gescode, sacd);
	lib := 'Diff�rences de conversion sur op�rations en devises';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


--*************** CREATION TABLE PASSIF *********************************
IF sacd = 'O' THEN
	DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
	DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- CAPITAUX PROPRES ----
lib1:= 'CAPITAUX PROPRES';
---- Capital---
lib2:= 'CAPITAL ET RESERVES';

-- Compte 1021 ---
	totalnet := Solde_Compte(exeordre, '1021%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1021%','C', gescode, sacd);
	lib := 'Dotation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1022 ---
	totalnet := Solde_Compte(exeordre, '1022%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1022%','C', gescode, sacd);
	lib := 'Compl�ment de dotation (Etat)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1023 ---
	totalnet := Solde_Compte(exeordre, '1023%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1023%','C', gescode, sacd);
	lib := 'Compl�ment de dotation (autres organismes)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1027 ---
	totalnet := Solde_Compte(exeordre, '1027%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1027%','C', gescode, sacd);
	lib := 'Affectation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 103 ---
	totalnet := Solde_Compte(exeordre, '103%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '103%','C', gescode, sacd);
	lib := 'Biens remis en pleine propri�t� aux �tablissements';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 105 ---
	totalnet := Solde_Compte(exeordre, '105%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '105%','C', gescode, sacd);
	lib := 'Ecarts de r��valuation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1068 ---
	totalnet := Solde_Compte(exeordre, '1068%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1068%','C', gescode, sacd);
	lib := 'R�serves';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1069 ---
	totalnet := Solde_Compte(exeordre, '1069%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1069%','D', gescode, sacd);
	lib := 'D�pr�ciation de l''actif';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, -totalnet, -totalnetant, gescode,exeordre);

-- Compte 110 ou 119 ---
	totalnet := Solde_Compte(exeordre, '110%','C', gescode, sacd)
		- Solde_Compte(exeordre,'119%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '110%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'119%','D', gescode, sacd);
	lib := 'Report � nouveau';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 120 ou 129 ---
	totalnet := Solde_Compte(exeordre, '120%','C', gescode, sacd)
		- Solde_Compte(exeordre,'129%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '120%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'129%','D', gescode, sacd);
	lib := 'R�sultat de l''exercice (b�n�fice ou perte)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

-- Compte 13 --- Sp�cial LR 130 !
	totalnet := Solde_Compte(exeordre, '130%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre, '131%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '138%','C', gescode, sacd)
		- Solde_Compte(exeordre,'139%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '131%','C', gescode, sacd)
	 	+ Solde_Compte(exeordre-1, '138%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'139%','D', gescode, sacd);
	lib := 'Subventions d''investissement';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

----- PROVISIONS POUR RISQUES ET CHARGES ----
lib1:= 'PROVISIONS POUR RISQUES ET CHARGES';
lib2:= '  ';

-- Compte 151 ---
	totalnet := Solde_Compte(exeordre, '151%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '151%','C', gescode, sacd);
	lib := 'Provisions pour risques';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 157 et 158 ---
	totalnet := Solde_Compte(exeordre, '157%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '158%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '157%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '158%','C', gescode, sacd);
	lib := 'Provisions pour charges';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- DETTES ----
	lib1:= 'DETTES';
---- DETTES FINANCIERES ---
lib2:= 'DETTES FINANCIERES';

-- Compte Emprunts etab cr�dits ---
	totalnet := Solde_Compte(exeordre, '164%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '164%','C', gescode, sacd);
	lib := 'Emprunts aupr�s des �tablissements de cr�dit';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres emprunts ---
	totalnet := Solde_Compte(exeordre, '165%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '167%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '168%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '17%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '165%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '167%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '168%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '17%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','C', gescode, sacd);
	lib := 'Emprunts divers';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 419 ---
	totalnet := Solde_Compte(exeordre, '4191%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4192%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '4191%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4192%','C', gescode, sacd);
	lib := 'Avances et acomptes re�us sur commandes';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

---- DETTES EXPLOITATION ---
lib2:= 'DETTES D''EXPLOITATION';

-- Compte Dettes fournisseurs ---
	totalnet := Solde_Compte(exeordre, '401%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '403%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4081%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4088%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '401%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '403%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4081%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4088%','C', gescode, sacd);
	lib := 'Fournisseurs et comptes rattach�s';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Dettes fiscales ---
	totalnet := Solde_Compte(exeordre,'421%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '422%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '427%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4282%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4286%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '431%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '437%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4382%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4386%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '443%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '444%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4452%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4455%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '44584%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '44587%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4457%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '447%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4482%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4486%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1,'421%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '422%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '427%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4282%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4286%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '431%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '437%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4382%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4386%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '443%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '444%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4452%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4455%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '44584%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '44587%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4457%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '447%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4482%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4486%','C', gescode, sacd);
	lib := 'Dettes fiscales et sociales';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres dettes ---
	totalnet := Solde_Compte(exeordre, '4196%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4197%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4198%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4682%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '471%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4731%','C', gescode, sacd)
		--+ Solde_Compte(exeordre, '4729%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre, '478%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '4196%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4197%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4198%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4682%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '471%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4731%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '478%','C', gescode, sacd);
	lib := 'Autres dettes d''exploitation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

--- DETTES diverses ---
lib2:= 'DETTES DIVERSES';

-- Compte Dettes sur immo ---
	totalnet := Solde_Compte(exeordre, '269%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '404%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '405%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4084%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '269%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '404%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '405%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4084%','C', gescode, sacd);
	lib := 'Dettes sur immobilisations et comptes rattach�s';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Autres Dettes ---
	totalnet := Solde_Compte(exeordre, '429%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '464%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '466%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '467%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4686%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '509%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '429%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '464%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '466%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '467%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4686%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '509%','C', gescode, sacd);
	lib := 'Autres dettes diverses';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 487 ---
	totalnet := Solde_Compte(exeordre, '487%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '487%','C', gescode, sacd);
	lib := 'Produits constat�s d''avance';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '  ';

-- Compte Emprunts etab cr�dits ---
	totalnet := Solde_Compte(exeordre, '477%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '477%','C', gescode, sacd);
	lib := 'Diff�rences de conversion sur op�rations en devises' ;
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

END;
/


