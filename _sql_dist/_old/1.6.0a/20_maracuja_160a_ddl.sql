SET define OFF
SET scan OFF

ALTER TABLE MARACUJA.VISA_AV_MISSION  ADD GES_CODE VARCHAR2(10) not null;
COMMENT ON COLUMN MARACUJA.VISA_AV_MISSION.GES_CODE IS 'Reference a la table gestion (detection via org_ub de organ)';

    