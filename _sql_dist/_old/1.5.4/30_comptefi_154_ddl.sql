-- Corrections user comptefi


CREATE OR REPLACE FORCE VIEW COMPTEFI.V_SITU_EXEBUDGDEP0
(EXE_ORDRE, GES_CODE, TCD_CODE, SSDST_CODE, SUM_DEP10, 
 SUM_DEP20, SUM_DEP30)
AS 
SELECT s.exe_ordre, org_ub, tc.tcd_code, s.dst_code, SUM(dep_ttc) SUM_DEP10, 0 SUM_DEP20 , 0 SUM_DEP30
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o, maracuja.type_credit tc
WHERE s.org_ordre = o.org_id AND tc.tcd_code=s.tcd_code AND s.exe_ordre=tc.exe_ordre AND tc.tcd_sect=1 AND tc.tcd_code<>30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tc.tcd_code, s.dst_code
UNION ALL
SELECT s.exe_ordre, org_ub, tc.tcd_code, s.dst_code, 0 SUM_DEP10, SUM(dep_ttc) SUM_DEP20 , 0 SUM_DEP30
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o, maracuja.type_credit tc
WHERE s.org_ordre = o.org_id AND tc.tcd_code=s.tcd_code AND s.exe_ordre=tc.exe_ordre AND tc.tcd_sect=2 AND tc.tcd_code<>30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tc.tcd_code, s.dst_code
UNION ALL
SELECT s.exe_ordre, org_ub, tcd_code, s.dst_code, 0 SUM_DEP10, 0 SUM_DEP20, SUM(dep_ttc) SUM_DEP30
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o
WHERE s.org_ordre = o.org_id AND tcd_code = 30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tcd_code, s.dst_code;


