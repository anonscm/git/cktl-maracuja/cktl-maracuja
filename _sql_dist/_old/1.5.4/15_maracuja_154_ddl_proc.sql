Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Av_Mission IS
/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- rodolphe prin

-- permet d ajouter des details a une ecriture.
FUNCTION creerAvanceMission (misOrdre INTEGER,
		 exeOrdre INTEGER,    -- exeOrdre sur lequel generer l'avance
		 fouOrdre INTEGER, 
		 ribOrdre INTEGER,
		 orgId NUMBER,
		 vamMontant NUMBER,
		 modOrdreAvance NUMBER,
		 modOrdreRegul NUMBER,
		 utlOrdreDemandeur NUMBER)
		  RETURN INTEGER;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Av_Mission IS
-- PUBLIC --



FUNCTION creerAvanceMission (
		 misOrdre INTEGER,
		 exeOrdre INTEGER,    -- exeOrdre sur lequel generer l'avance
		 fouOrdre INTEGER, 
		 ribOrdre INTEGER,
		 orgId NUMBER,
		 vamMontant NUMBER,
		 modOrdreAvance NUMBER,
		 modOrdreRegul NUMBER,
		 utlOrdreDemandeur NUMBER
) RETURN INTEGER
IS
  flag              NUMBER;
  s					VARCHAR2(1000);
  mpAv			MODE_PAIEMENT%ROWTYPE;
  mpReg			MODE_PAIEMENT%ROWTYPE;
  miss				v_mission%ROWTYPE;
  vamId				VISA_AV_MISSION.vam_id%TYPE;
BEGIN
		s := '';
		
		
		IF misOrdre IS NULL THEN
		   s := s || 'misOrdre, ';
		END IF;
		
		IF exeOrdre IS NULL THEN
		   s := s || 'exeOrdre, ';
		END IF;
		
		IF fouOrdre IS NULL THEN
		   s := s || 'fouOrdre, ';
		END IF;
		
		IF (vamMontant IS NULL OR vamMontant=0) THEN
		   s := s || 'vamMontant, ';
		END IF;
		
		IF modOrdreAvance IS NULL THEN
		   s := s || 'modOrdreAvance, ';
		END IF;
		
		
		IF utlOrdreDemandeur IS NULL THEN
		   s := s || 'utlOrdreDemandeur, ';
		END IF;
		
		
		IF (LENGTH(s)>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Les parametres suivants sont obligatoires : '|| s ||'.'  );
		END IF;
		
		
		
		-- verifier que la mission existe
		SELECT COUNT(*) INTO flag FROM v_mission WHERE mis_ordre=misOrdre;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer une mission correspondant au mis_ordre ='|| misOrdre ||'.'  );
		END IF;
		
		-- Verifier exercice ouvert (pas possible de payer un OP sinon)
		SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_ordre=exeOrdre AND  exe_stat='O';
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Exercice ' || exeOrdre || ' non defini ou non ouvert. Impossible de creer une avance sur un exercice autre que l''exercice de tr�sorerie.'  );
		END IF;
		
		-- Verifier fournisseur existe
		SELECT COUNT(*) INTO flag FROM v_fournisseur WHERE fou_ordre=fouOrdre;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un fournisseur correspondant au fou_ordre ='|| fouOrdre ||'.'  );
		END IF;
		
		-- Verifier organ existe pour l'exercice
		SELECT COUNT(*) INTO flag FROM v_organ_exer WHERE exe_ordre=exeOrdre AND org_id=orgId;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un organ correspondant a org_id ='|| orgId ||' et exe_ordre='||exeOrdre  );
		END IF;
		
		-- Verifier mode paiement avance existe pour l'exercice
		SELECT COUNT(*) INTO flag FROM MODE_PAIEMENT WHERE exe_ordre=exeOrdre AND mod_ordre=modOrdreAvance;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un mode_paiement (modOrdreAvance) correspondant a mod_ordre ='|| modOrdreAvance ||' et exe_ordre='||exeOrdre  );
		END IF;
		
		-- Verifier mode paiement regul existe pour l'exercice
		SELECT COUNT(*) INTO flag FROM MODE_PAIEMENT WHERE exe_ordre=exeOrdre AND mod_ordre=modOrdreRegul;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un mode_paiement (modOrdreRegul) correspondant a mod_ordre ='|| modOrdreRegul ||' et exe_ordre='||exeOrdre  );
		END IF;
		
		-- Verifier utilisateur existe
		SELECT COUNT(*) INTO flag FROM utilisateur WHERE utl_ordre=utlOrdreDemandeur;
		IF flag = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un utilisateur correspondant au utl_ordre ='|| utlOrdreDemandeur ||'.'  );
		END IF;
		
		-- 
		SELECT * INTO mpAv FROM MODE_PAIEMENT WHERE mod_ordre=modOrdreAvance;
		SELECT * INTO mpReg FROM MODE_PAIEMENT WHERE mod_ordre=modOrdreRegul;
		SELECT * INTO miss FROM v_mission WHERE mis_ordre=misOrdre; 
		
		-- verifier que RIB specifie si on a un mode de paiement du domaine virement
		IF (mpAv.MOD_DOM = 'VIREMENT') THEN
		   IF (ribOrdre IS NULL) THEN
		   	  RAISE_APPLICATION_ERROR (-20001,'Le ribOrdre est obligatoire pour un mode de paiement qui donne lieu a un VIREMENT' ||'.'  );
		   END IF;
		END IF;
		
		IF (mpAv.MOD_VALIDITE <> 'VALIDE') THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement avance mod_ordre ='|| modOrdreAvance ||' n''est pas VALIDE.'  );
		END IF;
		
		IF (mpReg.MOD_VALIDITE <> 'VALIDE') THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement regul mod_ordre ='|| modOrdreRegul ||' n''est pas VALIDE.'  );
		END IF;
		
		-- Si precise Verifier rib existe (si rib non valide, il y aura un warning lors de la gen de la disquette)
		IF (ribOrdre IS NOT NULL) THEN
			SELECT COUNT(*) INTO flag FROM v_rib WHERE rib_ordre=ribOrdre;
			IF flag = 0 THEN
			   RAISE_APPLICATION_ERROR (-20001,'Impossible de recuperer un rib correspondant au rib_ordre ='|| ribOrdre ||'.'  );
			END IF;
			
			-- verifier que le rib est bien affecte au fournisseur
			SELECT COUNT(*) INTO flag FROM v_rib WHERE rib_ordre=ribOrdre AND fou_ordre=fouOrdre;
			IF flag = 0 THEN
			   RAISE_APPLICATION_ERROR (-20001,'Le rib rib_ordre ='|| ribOrdre ||' n''est pas affecte au fournisseur  correspondant pas au fou_ordre '|| fouOrdre );
			END IF;
		END IF;
		
		---------
		-- verifier les pco_num
		IF (mpAv.pco_num_paiement IS NULL) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement avance mod_ordre='|| modOrdreAvance ||' n''a pas de PCO_NUM_PAIEMENT defini.' );
		END IF;
		IF (mpReg.pco_num_visa IS NULL) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Le mode paiement de regul mod_ordre='|| modOrdreRegul ||' n''a pas de PCO_NUM_VISA defini.' );
		END IF;
		
		
		----- TODO : verifier l''etat de la mission
		
		
		SELECT maracuja.visa_av_mission_SEQ.NEXTVAL INTO vamId FROM dual;
		
		INSERT INTO MARACUJA.VISA_AV_MISSION (
		   VAM_ID, MIS_ORDRE, EXE_ORDRE, 
		   FOU_ORDRE, ORG_ID, VAM_MONTANT, 
		   MOD_ORDRE_AVANCE, MOD_ORDRE_REGUL, RIB_ORDRE, 
		   VAM_DATE_DEM, UTL_ORDRE_DEMANDEUR, TYET_ID, 
		   VAM_DATE_VISA, UTL_ORDRE_VALIDEUR, ODP_ORDRE) 
		VALUES ( 
			   vamId, 
			   misOrdre, 
			   exeOrdre, 
		   	   fouOrdre, 
			   orgId, 
			   vamMontant, 
		   	   modOrdreAvance, --MOD_ORDRE_AVANCE, 
			   modOrdreRegul, --MOD_ORDRE_REGUL, 
			   ribOrdre, --RIB_ORDRE, 
		   	   SYSDATE, --VAM_DATE_DEM, 
			   utlOrdreDemandeur, --UTL_ORDRE_DEMANDEUR, 
			   6, --TYET_ID, 
		   	   NULL, --VAM_DATE_VISA, 
			   NULL, --UTL_ORDRE_VALIDEUR, 
			   NULL --ODP_ORDRE 
			   );
		   
		   RETURN vamId;

END;

END;
/

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------

CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Papaye IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures SACD   d un bordereau --
PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- permet de passer les  ecritures SACD  d un mois de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un bordereau --
PROCEDURE passerEcritureOppRetBord ( borid INTEGER, passer_ecritures VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Papaye IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA SALAIRES';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA SALAIRES' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 3 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

	SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentmab.mab_montant,
	NULL,
	currentmab.mab_sens,
	ecrordre,
	currentmab.ges_code,
	currentmab.pco_num
	);

	SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

	-- Insertion des mandat_detail_ecritures
	INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
	ecdordre,
	currentmab.exe_ordre,
	currentmab.man_id,
	SYSDATE,
	mdeordre,
	'VISA',
	oriordre
	);



	END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation LIKE 'RETENUES SALAIRES' AND bob_etat = 'VALIDE';

  -- Mise a jour de l'etat du bordereau (RETENUES OU PAIEMENT)
  IF (cpt > 0)
  THEN
  	  boretat := 'RETENUES';
  ELSE
  	  boretat := 'PAIEMENT';
  END IF;

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

  IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET etat = boretat, jou_ordre_visa = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = boretat, ecr_ordre_visa = ecrordre WHERE bor_id = borid;
  END IF;

  passerecrituresacdbord(borid);

END IF;

END ;



-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;

-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)

END ;



-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD SALAIRES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD SALAIRES' AND bor_id = borid;

   IF (cpt > 0)
   THEN

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

    IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET jou_ordre_sacd = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_sacd = ecrordre WHERE bor_id = borid;
  END IF;

  END IF;

END ;

-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR)
IS
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)


cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;


END ;


-- permet de passer les  ecritures d opp ret d un bordereau de paye --
PROCEDURE passerEcritureOppRetBord (borid INTEGER, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'RETENUES SALAIRES' AND bob_etat = 'RETENUES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

bobetat  maracuja.BORDEREAU_BROUILLARD.bob_etat%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF (passer_ecritures = 'O')
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT DISTINCT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT DISTINCT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO bobetat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (bobetat = 'RETENUES')
THEN
	ecrordre := creerecriture(
	1,
	SYSDATE,
	'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;

    UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAIEMENT'
  WHERE bor_id = borid;

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;
  UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;

  END IF;

ELSE
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;
  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAIEMENT'
  WHERE bor_id = borid;

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;


      IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'PAIEMENT', ECR_ORDRE_OPP = ecrordre WHERE bor_id = borid;
  END IF;

END IF;

END ;

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureOppRet (borid INTEGER)


END ;

-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT SALAIRES'
AND bob_etat = 'PAIEMENT';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT SALAIRES '||borlibelle_mois;

   IF (cpt = 0)
   THEN
	ecrordre := creerecriture(
	1,
	SYSDATE,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	exeordre,
	oriordre,
	14,
	6,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);


	      IF (exeordre<2007) THEN
	     UPDATE jefy.papaye_compta SET etat = 'TERMINEE', jou_ordre_dsk = ecrordre WHERE mois = borlibelle_mois;
	  ELSE
	  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE', ECR_ORDRE_DSK = ecrordre WHERE mois = borlibelle_mois;
	  END IF;
  END IF;

 ELSE
	      IF (exeordre<2007) THEN
	       UPDATE jefy.papaye_compta SET etat = 'TERMINEE' WHERE mois = borlibelle_mois;
	  ELSE
	  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE' WHERE mois = borlibelle_mois;
	  END IF;


END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bob_libelle2 = borlibelle_mois;

    UPDATE MANDAT
 SET
  man_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
	localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
	localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE 		--		INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/


-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.cptefiutil IS
    PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER,libelle VARCHAR, rtatcomp VARCHAR);
    PROCEDURE annuler_ecriture (monecdordre INTEGER);
    PROCEDURE numeroter_ecriture (ecrordre INTEGER);
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Cptefiutil IS
PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER, libelle VARCHAR, rtatcomp VARCHAR)
IS

-- version du 03/03/2005 - Prestations internes

/*
RIVALLAND FREDRIC
CRIG
18/01/2005 */

legescode GESTION.ges_code%TYPE;
lepconum PLAN_COMPTABLE.pco_num%TYPE;
lesolde  NUMBER(12,2);
debit  NUMBER(12,2);
credit NUMBER(12,2);
credit120 NUMBER(12,2);
debit120 NUMBER(12,2);
credit129 NUMBER(12,2);
debit129 NUMBER(12,2);


CURSOR solde6 IS
 SELECT ges_code,pco_num, solde_crediteur
 FROM CFI_solde_6_7
 WHERE solde_crediteur != 0
 AND (pco_num LIKE '6%' OR pco_num LIKE '186%')
 AND exe_ordre = exeordre;

CURSOR solde7 IS
 SELECT ges_code,pco_num, solde_crediteur
 FROM CFI_solde_6_7
 WHERE solde_crediteur != 0
 AND (pco_num LIKE '7%' OR pco_num LIKE '187%')
 AND exe_ordre = exeordre;

CURSOR lesgescode IS
 SELECT DISTINCT ges_code, pco_num_185
 FROM GESTION_EXERCICE
 WHERE exe_ordre = exeordre;

 CURSOR lessacds IS
 SELECT DISTINCT ges_code
 FROM GESTION_EXERCICE
 WHERE exe_ordre = exeordre
 AND pco_num_185 IS NOT NULL;


 nb INTEGER;
 gescodeagence COMPTABILITE.ges_code%TYPE;
 comordre COMPTABILITE.com_ordre%TYPE;
 topordre TYPE_OPERATION.top_ordre%TYPE;
 TJOORDRE TYPE_JOURNAL.tjo_ordre%TYPE;
 monecdordre ECRITURE.ecr_ordre%TYPE;
 ecdordre ECRITURE_DETAIL.ecd_ordre%TYPE;

 exeexercice exercice.exe_exercice%TYPE;

BEGIN


-- creation de l'ecriture de solde --
SELECT top_ordre
INTO topordre
FROM TYPE_OPERATION
WHERE top_libelle = 'ECRITURE SOLDE 6 ET 7';


SELECT tjo_ordre
INTO TJOORDRE
FROM TYPE_JOURNAL
WHERE tjo_libelle = 'JOURNAL FIN EXERCICE';




SELECT com_ordre, ges_code
INTO comordre, gescodeagence
FROM COMPTABILITE;


SELECT exe_exercice INTO exeexercice FROM exercice WHERE exe_ordre = exeordre;


-- creation de lecriture  --
monecdordre := maracuja.Api_Plsql_Journal.creerecriture(
comordre,
TO_DATE('31/12/'||exeexercice, 'dd/mm/yyyy'),
libelle,
exeordre,
NULL,           --ORIORDRE,
tjoordre,
topordre,
utlordre
);


-- Calcul du r�sultat par composante ---
-- Etablissement + SACD --
IF rtatcomp = 'O' THEN

	-- creation de l ecriture de resultat --
	OPEN lesgescode;
	LOOP
	FETCH lesgescode INTO legescode,lepconum;
	EXIT WHEN lesgescode%NOTFOUND;

	SELECT COUNT(*) INTO nb FROM cfi_solde_6_7
	WHERE exe_ordre = exeordre
	AND ges_code = legescode;
	IF nb != 0 THEN
	--  composante SACD --
	--       IF lepco_num is not null  THEN
           SELECT NVL(-SUM(solde_crediteur),0) INTO debit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '6%' OR pco_num LIKE '186%')
		   AND ges_code = legescode AND exe_ordre = exeordre;


           SELECT NVL(SUM(solde_crediteur),0) INTO credit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '7%' OR pco_num LIKE '187%')
		   AND ges_code = legescode AND exe_ordre=exeordre;

		IF debit > credit THEN
	   	-- RESULTAT SOLDE DEBITEUR 129
	   	-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
       	--  contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
		ELSE
		-- RESULTAT SOLDE CREDITEUR 120 --
		-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
       	                          (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

	   	-- contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

		END IF;

	END IF;
	END LOOP;
	CLOSE lesgescode;

ELSE
	-- resultat = 'Etablissement' --

	SELECT COUNT(*) INTO nb FROM cfi_solde_6_7
	WHERE exe_ordre = exeordre AND ges_code IN
	(SELECT ges_code FROM GESTION_EXERCICE WHERE exe_ordre = exeordre AND pco_num_185 IS NULL);

	IF nb != 0 THEN
           SELECT NVL(-SUM(solde_crediteur),0) INTO debit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '6%' OR pco_num LIKE '186%') AND exe_ordre = exeordre AND ges_code IN
	(SELECT ges_code FROM GESTION_EXERCICE WHERE exe_ordre = exeordre AND pco_num_185 IS NULL);


           SELECT NVL(SUM(solde_crediteur),0) INTO credit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '7%' OR pco_num LIKE '187%') AND exe_ordre = exeordre AND ges_code IN
	(SELECT ges_code FROM GESTION_EXERCICE WHERE exe_ordre = exeordre AND pco_num_185 IS NULL);


		IF debit > credit THEN
	   	-- RESULTAT SOLDE DEBITEUR 129
	   	-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '129'           --PCONUM
                                 );
       	--  contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '129'           --PCONUM
                                 );
		ELSE
		-- RESULTAT SOLDE CREDITEUR 120 --
		-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
       	                          (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '120'           --PCONUM
                                 );

	   	-- contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '120'           --PCONUM
                                 );

		END IF;
	END IF;

	-- R�sultat des SACD ---

	OPEN lessacds;
	LOOP
	FETCH lessacds INTO legescode;
	EXIT WHEN lessacds%NOTFOUND;


	SELECT COUNT(*) INTO nb FROM cfi_solde_6_7
	WHERE exe_ordre = exeordre
	AND ges_code = legescode;
	IF nb != 0 THEN
	--  SACD --
	--       IF lepco_num is not null  THEN
           SELECT NVL(-SUM(solde_crediteur),0) INTO debit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '6%' OR pco_num LIKE '186%')
		   AND ges_code = legescode AND exe_ordre = exeordre;


           SELECT NVL(SUM(solde_crediteur),0) INTO credit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '7%' OR pco_num LIKE '187%')
		   AND ges_code = legescode AND exe_ordre=exeordre;


		IF debit > credit THEN
	   	-- RESULTAT SOLDE DEBITEUR 129
	   	-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
       	--  contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
		ELSE
		-- RESULTAT SOLDE CREDITEUR 120 --
		-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
       	                          (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

	   	-- contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

		END IF;

	END IF;
	END LOOP;
	CLOSE lessacds;

END IF;



-- Solde classe 6 et 186

OPEN solde6;
LOOP
FETCH solde6 INTO legescode,lepconum,lesolde;
EXIT WHEN solde6%NOTFOUND;
IF lesolde < 0 THEN
       ecdordre :=
       maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  -lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
        ELSE

       ecdordre :=
       maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
END IF;


END LOOP;
CLOSE solde6;


-- Solde classe 7 et 187

OPEN solde7;
LOOP
FETCH solde7 INTO legescode,lepconum,lesolde;
EXIT WHEN solde7%NOTFOUND;
IF lesolde < 0 THEN
       ecdordre :=
       maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  -lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
        ELSE

       ecdordre :=
       maracuja.Api_Plsql_Journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
END IF;


END LOOP;
CLOSE solde7;

-- on numerote pour eviter une numerotation automatique.
-- voir procedure numeroter_ecriture
UPDATE ECRITURE SET ecr_numero = 99999999 WHERE ecr_ordre = monecdordre;


END;


--------------------------------------------------------------------------------


PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS


/*
/*
RIVALLAND FREDRIC
CRIG
18/01/2005


a utiliser uniquement pour numeroter
definitivement lecriture de solde des
classes 6 et 7


*/
numero INTEGER;
BEGIN


SELECT ecr_numero INTO numero FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF numero != 99999999 THEN
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE MODIFIER UNE ECRITURE DU JOURNAL !');
ELSE
UPDATE ECRITURE SET ecr_numero = 0 WHERE ecr_ordre = ecrordre;
maracuja.Api_Plsql_Journal.validerEcriture (ecrordre);
END IF;
END;


--------------------------------------------------------------------------------



PROCEDURE annuler_ecriture (monecdordre INTEGER)
IS


/*
/*
RIVALLAND FREDRIC
CRIG
18/01/2005


a utiliser uniquement pour ANNULER
definitivement l'ecriture de solde des
classes 6 et 7


*/
numero INTEGER;
BEGIN


SELECT ecr_numero INTO numero FROM ECRITURE WHERE ecr_ordre = monecdordre;
IF numero != 99999999 THEN
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
ELSE
 DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = monecdordre;
 DELETE FROM ECRITURE WHERE ecr_ordre = monecdordre;
END IF;
END;



END;
/




