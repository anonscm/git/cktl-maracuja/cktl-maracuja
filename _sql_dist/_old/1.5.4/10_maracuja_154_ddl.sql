SET define OFF
SET scan OFF

ALTER TABLE MARACUJA.RECETTE MODIFY (REC_LIBELLE  VARCHAR2(500) );




create table maracuja.visa_av_mission (
	vam_id				number not null,
	mis_ordre 			number not null,
	exe_ordre 			number not null,
	fou_ordre 			number not null,
	org_id 				number not null,
	vam_montant 		number(12,2) not null, 
	mod_ordre_avance 	number not null, 
	mod_ordre_regul 	number not null, 
	rib_ordre 			number null,	
	vam_date_dem 		date not null ,	
	utl_ordre_demandeur number not null, 
	tyet_id 			number not null, 
	vam_date_visa		date null,
	utl_ordre_valideur 	number null, 
	odp_ordre 			number null
);

ALTER TABLE MARACUJA.visa_av_mission ADD (constraint pk_visa_av_mission PRIMARY KEY  (vam_id) USING INDEX TABLESPACE GFC_INDX );
  
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_fou_ordre FOREIGN KEY (fou_ordre) REFERENCES grhum.fournis_ulr(fou_ordre) );
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_mis_ordre FOREIGN KEY (mis_ordre) REFERENCES jefy_mission.mission(mis_ordre) );
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_rib_ordre FOREIGN KEY (rib_ordre) REFERENCES grhum.ribfour_ulr(rib_ordre) );
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_org_id FOREIGN KEY (org_id) REFERENCES jefy_admin.organ(org_id) );
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_mpav FOREIGN KEY (mod_ordre_avance) REFERENCES maracuja.mode_paiement(mod_ordre));
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_mpreg FOREIGN KEY (mod_ordre_regul) REFERENCES maracuja.mode_paiement(mod_ordre));
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_tyet_id FOREIGN KEY (tyet_id) REFERENCES jefy_admin.type_etat(tyet_id));
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_utl_dem FOREIGN KEY (utl_ordre_demandeur) REFERENCES jefy_admin.utilisateur(utl_ordre));
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_utl_val FOREIGN KEY (utl_ordre_valideur) REFERENCES jefy_admin.utilisateur(utl_ordre));
ALTER TABLE maracuja.visa_av_mission ADD (CONSTRAINT fk_visa_av_mission_odp_ordre FOREIGN KEY (odp_ordre) REFERENCES maracuja.ordre_de_paiement(odp_ordre));


CREATE SEQUENCE MARACUJA.visa_av_mission_SEQ START WITH 1 MAXVALUE 1E27  MINVALUE 1   NOCYCLE   NOCACHE   NOORDER;


COMMENT ON COLUMN MARACUJA.visa_av_mission.utl_ordre_demandeur IS 'R�f�rence � l''utilisateur qui genere la demande a partir de mission (kiwi)';
COMMENT ON COLUMN MARACUJA.visa_av_mission.utl_ordre_valideur IS 'R�f�rence � l''utilisateur qui valide/rejete la demande a partir de maracuja';
COMMENT ON COLUMN MARACUJA.visa_av_mission.fou_ordre IS 'R�f�rence au fournisseur (missionnaire)';
COMMENT ON COLUMN MARACUJA.visa_av_mission.rib_ordre IS 'R�f�rence au rib du missionnaire';
COMMENT ON COLUMN MARACUJA.visa_av_mission.vam_montant IS 'Montant de l''avance demand�e';
COMMENT ON COLUMN MARACUJA.visa_av_mission.tyet_id IS 'Reference a type_etat (etat de la demande)';
COMMENT ON COLUMN MARACUJA.visa_av_mission.odp_ordre IS 'Reference a l''ordre de paiement si la demande a ete acceptee';
COMMENT ON COLUMN MARACUJA.visa_av_mission.org_id IS 'Reference a la ligne budgetaire sur laquelle est effectuee la demande';
COMMENT ON COLUMN MARACUJA.visa_av_mission.exe_ordre IS 'Reference a l''exercice sur lequal la demande d''avance est effectuee';
COMMENT ON COLUMN MARACUJA.visa_av_mission.mis_ordre IS 'Reference a la mission a partir de laquelle la demande est effectuee';
COMMENT ON COLUMN MARACUJA.visa_av_mission.mod_ordre_avance IS 'Reference au mode de paiement a utiliser pour payer l''avance';
COMMENT ON COLUMN MARACUJA.visa_av_mission.mod_ordre_regul IS 'Reference au mode de paiement qui sera affecte a la liquidation de la mission (mode de paiement interne)';
COMMENT ON COLUMN MARACUJA.visa_av_mission.vam_date_dem IS 'Date a laquelle est effectuee la demande';
COMMENT ON COLUMN MARACUJA.visa_av_mission.vam_date_visa IS 'Date a laquelle est acceptee ou rejetee la demande';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MISSION
(MIS_ORDRE, MIS_NUMERO, EXE_ORDRE, EXE_ORDRE_ORIGINE, TIT_ORDRE, 
 FOU_ORDRE, MIS_DEBUT, MIS_FIN, MIS_ETAT, C_CORPS, 
 MIS_CORPS, GRM_ORDRE, MIS_MOTIF, MIS_OBSERVATION, COD_PAYEUR, 
 MIS_PAYEUR, MIS_RESIDENCE, NUM_QUOTIENT_REMB, DEN_QUOTIENT_REMB, UTL_ORDRE_CREATION, 
 UTL_ORDRE_MODIFICATION, MIS_CREATION)
AS 
SELECT 
MIS_ORDRE, MIS_NUMERO, EXE_ORDRE, 
   EXE_ORDRE_ORIGINE, TIT_ORDRE, FOU_ORDRE, 
   MIS_DEBUT, MIS_FIN, MIS_ETAT, 
   C_CORPS, MIS_CORPS, GRM_ORDRE, 
   MIS_MOTIF, MIS_OBSERVATION, COD_PAYEUR, 
   MIS_PAYEUR, MIS_RESIDENCE, NUM_QUOTIENT_REMB, 
   DEN_QUOTIENT_REMB, UTL_ORDRE_CREATION, UTL_ORDRE_MODIFICATION, 
   MIS_CREATION
FROM JEFY_MISSION.MISSION;


