SET define OFF;
SET scan OFF;

-----------------------------------------------------------------------
-----------------------------------------------------------------------
-----------------------------------------------------------------------
begin
	UPDATE maracuja.MODE_RECOUVREMENT SET mod_dom='INTERNE' WHERE mod_libelle='PRESTATION INTERNE';
	UPDATE maracuja.MODE_RECOUVREMENT SET mod_dom='INTERNE' WHERE mod_libelle='AGENT COMPTABLE, POUR ORDRE';
	
	UPDATE maracuja.MODE_RECOUVREMENT SET mod_validite='ANNULE' WHERE mod_libelle = 'VIREMENT';
	UPDATE maracuja.MODE_RECOUVREMENT SET mod_validite='ANNULE' WHERE mod_libelle = 'PRESTATION EXTERNE';
	
	-- inversion
	update maracuja.mode_recouvrement set pco_num_paiement=pco_num_visa where mod_libelle = 'CHEQUE';
	update maracuja.mode_recouvrement set pco_num_visa=null where mod_libelle = 'CHEQUE';
	update maracuja.mode_recouvrement set pco_num_paiement=pco_num_visa where mod_libelle = 'AGENT COMPTABLE, POUR ORDRE';
	update maracuja.mode_recouvrement set pco_num_visa=null where mod_libelle = 'AGENT COMPTABLE, POUR ORDRE';
	
	update maracuja.mode_recouvrement set pco_num_paiement=null, pco_num_visa=null where mod_libelle = 'PRESTATION INTERNE';
	update maracuja.mode_recouvrement set pco_num_paiement=null, pco_num_visa=null where mod_libelle = 'PRESTATION EXTERNE';
	update maracuja.mode_recouvrement set pco_num_paiement=null, pco_num_visa=null where mod_libelle = 'VIREMENT';
	
	commit;


	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 560, 'VIAVMIS', 'Paiements/OP', 'Permet de g�n�rer des OP � partir d''une demande d''avance de mission', 'Visa des avances de mission' ,  'N', 'N', 4 );
	commit;
	
	INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
	TYAV_COMMENT ) VALUES ( 
	jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.5.4',  SYSDATE, '');
	COMMIT; 	
end;



