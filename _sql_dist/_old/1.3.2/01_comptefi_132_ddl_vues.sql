CREATE OR REPLACE VIEW COMPTEFI.V_BUDNAT
(
  EXE_ORDRE,
  GES_CODE,
  PCO_NUM,
  BDN_QUOI,
  BDN_NUMERO,
  DATE_CO,
  CO
)
AS 
select 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim)
from jefy.budnat b, jefy.exer e, jefy.organ o
where e.EXR_TYPE = 'PRIM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
union all
select 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim)
from jefy.budnat b, jefy.exer e, jefy.organ o
where e.EXR_TYPE = 'PRIM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
union all
select 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, to_date('01/01/2006','DD/MM/YYYY') exr_cloture, sum (bdn_reliq)
from jefy.budnat b, jefy.organ o
where b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, to_date('01/01/2006','DD/MM/YYYY')
union all
select 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, to_date('01/01/2006','DD/MM/YYYY') exr_cloture, sum (bdn_reliq)
from jefy.budnat b, jefy.organ o
where b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, to_date('01/01/2006','DD/MM/YYYY')
union all
select 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
sum (bdn_dbm)
from jefy.budnat_dbm b, jefy.exer e , jefy.organ o
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
union all
select 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
sum (bdn_dbm)
from jefy.budnat_dbm b, jefy.exer e , jefy.organ o
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture
union all
select 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim)
from jefy05.budnat b, jefy05.exer e, jefy05.organ o
where e.EXR_TYPE = 'PRIM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
union all
select 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim)
from jefy05.budnat b, jefy05.exer e, jefy05.organ o
where e.EXR_TYPE = 'PRIM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
union all
select 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_reliq)
from jefy05.budnat b, jefy05.exer e, jefy05.organ o
where e.EXR_TYPE = 'RELI'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
union all
select 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_reliq)
from jefy05.budnat b, jefy05.exer e, jefy05.organ o
where e.EXR_TYPE = 'RELI'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
union all
select 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
sum (bdn_dbm)
from jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
union all
select 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
sum (bdn_dbm)
from jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM'
and b.org_ordre = o.org_ordre
and o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture

