CREATE OR REPLACE FORCE VIEW MARACUJA.V_FOURNISSEUR
(FOU_ORDRE, PERS_ID, ADR_ORDRE, FOU_CODE, FOU_DATE, 
 FOU_MARCHE, FOU_VALIDE, AGT_ORDRE, FOU_TYPE, D_CREATION, 
 D_MODIFICATION, CPT_ORDRE, FOU_ETRANGER, ADR_ADRESSE1, ADR_ADRESSE2, 
 ADR_CP, ADR_VILLE, ADR_NOM, ADR_PRENOM, ADR_CIVILITE , CP_ETRANGER, LC_PAYS)
AS 
SELECT f.*, a.adr_adresse1, a.adr_adresse2, a.code_postal as adr_ADR_CP, a.ville as adr_ville,
       p.pers_libelle, p.pers_lc, p.pers_type, cp_etranger, lc_pays
  FROM grhum.fournis_ulr f, grhum.adresse a, grhum.personne p, grhum.pays pa
 WHERE f.adr_ordre = a.adr_ordre
   AND f.pers_id = p.pers_id
   AND pa.c_pays = a.c_pays;
   
CREATE OR REPLACE FORCE VIEW MARACUJA.V_PLANCO_CREDIT_DEP
(PCC_ORDRE, TCD_ORDRE, PCO_NUM, PCC_ETAT)
AS 
SELECT pcc_ordre, tcd_ordre, pco_num, pcc_etat
  FROM maracuja.planco_credit
 WHERE pla_quoi = 'D';
 
 CREATE OR REPLACE FORCE VIEW MARACUJA.V_PLANCO_CREDIT_REC
(PCC_ORDRE, TCD_ORDRE, PCO_NUM, PCC_ETAT)
AS 
SELECT pcc_ordre, tcd_ordre, pco_num, pcc_etat
  FROM maracuja.planco_credit
 WHERE pla_quoi = 'R';   
   
   
   