SET define OFF
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- Correction erreur de mandatement
-----------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY maracuja.Bordereau_Mandat IS
-- version 1.1.8 du 07/07/2005 - modifs sur le mod_ordre dans depense
-- version 1.1.7 du 31/05/2005 - modifs concernant les prestations internes
-- version 1.1.6 du 25/04/2005 - modifs concernant les prestations internes
-- version 1.1.8 du 15/09/2005 - modifs pour compatibilite avec nouveau gestion_exercice
-- version 1.1.9 du 26/09/2005 - modifs pour emargements semi-auto
-- version 1.5.0 du 08/12/2006 -- modifs pour attentes de paiement

PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	etat		jefy.bordero.bor_stat%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';
	   
	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;
	   
	 IF (ex <> ex2) THEN
	 	RAISE_APPLICATION_ERROR (-20001,'MAUVAIS EXERCICE');
	 END IF;


	SELECT bor_stat INTO etat FROM jefy.bordero
		   WHERE bor_ordre = borordre;

	IF etat = 'N' THEN

		-- est ce un bordereau de titre --
		SELECT COUNT(*) INTO cpt FROM jefy.MANDAT
		WHERE bor_ordre = borordre;

		IF cpt != 0 THEN

			tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

			-- le bordereau est il deja vis� ? --
			SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;

			-- l exercice est il encore ouvert ? --


			-- pas de raise mais pas de recup s il est deja dans maracuja --
			IF cpt = 0 THEN
				-- recup des infos --
				SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;

				-- recup de l agent de ce bordereau --
				/*select max(agt_ordre) into agtordre
				from jefy.facture
				where man_ordre =
				 ( select max(man_ordre)
				   from jefy.mandat
				   where bor_ordre =borordre
				  );

				-- recuperation du type de bordereau --
				select count(*) into cpt
				from old_agent_type_bord
				where agt_ordre = agtordre;

				if cpt <> 0 then
				 select tbo_ordre into tboordre
				 from old_agent_type_bord
				 where agt_ordre = agtordre;
				else
				 select tbo_ordre into tboordre
				 from type_bordereau
				 where tbo_ordre = (select par_value from parametre where par_key ='BTME GENERIQUE' and exe_ordre = exeordre);
				end if;
				*/


				-- creation du bor_id --
				SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

				-- recuperation de l utilisateur --
				/*select utl_ordre into utlordre
				from utilisateur
				where agt_ordre = agtordre;
				*/
				-- TEST  TEST  TEST  TEST  TEST
				SELECT 1 INTO utlordre
				FROM dual;
				-- TEST  TEST  TEST  TEST  TEST

				-- creation du bordereau --
				INSERT INTO BORDEREAU VALUES (
					NULL , 	 	 	      --BOR_DATE_VISA,
					'VALIDE',			  --BOR_ETAT,
					borid,				  --BOR_ID,
					leBordereau.bor_num,  --BOR_NUM,
					leBordereau.bor_ordre,--BOR_ORDRE,
					exeordre,	  		  --EXE_ORDRE,
					leBordereau.ges_code, --GES_CODE,
					tboordre,			  --TBO_ORDRE,
					utlordre,		 	  --UTL_ORDRE,
					NULL				  --UTL_ORDRE_VISA
					);

				--get_mandat_jefy_proc(exeordre,borordre,borid,utlordre,agtordre);
				Bordereau_Mandat.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

			END IF;
			--else
			-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
		END IF;
	END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	cpt 	 	INTEGER;

	lejefymandat jefy.MANDAT%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	manid 		 MANDAT.man_id%TYPE;

	MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
	MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
	ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
	PRESTID 	   MANDAT.PREST_ID%TYPE;
	TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
	VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
	modordre	   MANDAT.mod_ordre%TYPE;
	CURSOR lesJefyMandats IS
		SELECT *
			FROM jefy.MANDAT
			WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefyMandats;
	LOOP
		FETCH lesJefyMandats INTO lejefymandat;
			  EXIT WHEN lesJefyMandats%NOTFOUND;

		-- recuperation du ges_code --
		SELECT ges_code INTO gescode
			FROM BORDEREAU
			WHERE bor_id = borid;

		-- recuperations --
		--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
		MANORGINE_KEY:=NULL;

		--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
		MANORIGINE_LIB:=NULL;

		--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
		IF (lejefymandat.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
		END IF;

		--PRESTID : PRESTATION INTERNE --
		PRESTID :=NULL;

		--TORORDRE : ORIGINE DU MANDAT --
		TORORDRE := 1;

		--VIRORDRE --
		VIRORDRE := NULL;

		--MOD_ORDRE --
		SELECT mod_ordre INTO modordre
		FROM MODE_PAIEMENT
		WHERE mod_code =
		(
		 SELECT MAX(mod_code)
		 FROM jefy.factures
		 WHERE man_ordre = lejefymandat.man_ordre
		)
		AND exe_ordre=exeordre;

		-- creation du man_id --
		SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

		-- creation du mandat --
		INSERT INTO MANDAT(BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE, 
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT, 
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET, 
		MAN_NB_PIECE, MAN_NUMERO, MAN_NUMERO_REJET, 
		MAN_ORDRE, MAN_ORGINE_KEY, MAN_ORIGINE_LIB, MAN_TTC, 
		MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM, 
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE, 
		RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE, 
		MAN_ATTENTE_PAIEMENT, 
		MAN_ATTENTE_DATE, 
		MAN_ATTENTE_OBJET, 
		UTL_ORDRE_ATTENTE) VALUES (
		 borid ,		   		--BOR_ID,
		 NULL, 			   		--BRJ_ORDRE,
		 exeordre,		   		--EXE_ORDRE,
		 lejefymandat.fou_ordre,--FOU_ORDRE,
		 gescode,				--GES_CODE,
		 NULL,				    --MAN_DATE_REMISE,
		 NULL,					--MAN_DATE_VISA_PRINC,
		 'ATTENTE',				--MAN_ETAT,
		 'ATTENTE',			    --MAN_ETAT_REMISE,
		 lejefymandat.man_mont, --MAN_HT,
		 manid,					--MAN_ID,
		 NULL,					--MAN_MOTIF_REJET,
		 lejefymandat.man_piece,--MAN_NB_PIECE,
		 lejefymandat.man_num,	--MAN_NUMERO,
		 NULL,					--MAN_NUMERO_REJET,
		 lejefymandat.man_ordre,--MAN_ORDRE,
		 MANORGINE_KEY,			--MAN_ORGINE_KEY,
		 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
		 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
		 lejefymandat.man_tva,  --MAN_TVA,
		 modordre,				--MOD_ORDRE,
		 ORIORDRE,				--ORI_ORDRE,
		 lejefymandat.pco_num,	--PCO_NUM,
		 lejefymandat.PRES_ORDRE,				--PREST_ID,
		 TORORDRE,				--TOR_ORDRE,
		 VIRORDRE,				--VIR_ORDRE
		 lejefymandat.org_ordre,  --org_ordre
		 lejefymandat.rib_ordre, --rib ordo
		 lejefymandat.rib_ordre, -- rib_comptable
		 4, 					   	--MAN_ATTENTE_PAIEMENT, 
		NULL, 					--MAN_ATTENTE_DATE, 
		NULL, 					--MAN_ATTENTE_OBJET, 
		NULL 					--UTL_ORDRE_ATTENTE
		);

		--on met a jour le type de bordereau s'il s'agit d'un mandat faisant partie d'une prestation interne

		--select count(*) into cpt from v_titre_prest_interne where man_ordre=lejefymandat.man_ordre and tit_ordre is not null;
		--if cpt != 0 then
		IF lejefymandat.PRES_ORDRE IS NOT NULL THEN
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;


		Bordereau_Mandat.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
		Bordereau_Mandat.set_mandat_brouillard(manid);

	END LOOP;

	CLOSE lesJefyMandats;
	/*
	EXCEPTION
	WHEN OTHERS THEN
	 RAISE_APPLICATION_ERROR (-20002,'mandat :'||lejefymandat.man_ordre);
	*/
END;



PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;



--	select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--	if cpt = 0 then
	IF lemandat.prest_id IS NULL THEN
		-- creation du mandat_brouillard visa DEBIT--
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			lemandat.exe_ordre,			   --EXE_ORDRE,
			lemandat.ges_code,			   --GES_CODE,
			lemandat.man_ht,			   --MAB_MONTANT,
			'VISA MANDAT',				   --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
			'D',						   --MAB_SENS,
			manid,						   --MAN_ID,
			lemandat.pco_num			   --PCO_NU
			);


		-- credit=ctrepartie
		--debit = ordonnateur
		-- recup des infos du VISA CREDIT --
		SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

		IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
		END IF;

		SELECT pco_num_ctrepartie, PCO_NUM_TVA
			INTO pconum_ctrepartie, PCONUM_TVA
			FROM PLANCO_VISA
			WHERE pco_num_ordonnateur = lemandat.pco_num;


		SELECT  COUNT(*) INTO cpt
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;

		IF cpt != 0 THEN
			SELECT  pco_num_visa INTO pconum_ctrepartie
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;
		END IF;



		-- recup de l agence comptable --
-- 		SELECT c.ges_code,PCO_NUM_185
-- 			INTO gescodecompta,pconum_185
-- 			FROM GESTION g, COMPTABILITE c
-- 			WHERE g.ges_code = lemandat.ges_code
-- 			AND g.com_ordre = c.com_ordre;
--
			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;


		SELECT par_value   INTO parvalue
			FROM PARAMETRE
			WHERE par_key ='CONTRE PARTIE VISA'
			AND exe_ordre = lemandat.exe_ordre;

		IF parvalue = 'COMPOSANTE' THEN
		   gescodecompta := lemandat.ges_code;
		END IF;

		IF pconum_185 IS NULL THEN
			-- creation du mandat_brouillard visa CREDIT --
			INSERT INTO MANDAT_BROUILLARD VALUES (
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			gescodecompta,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		ELSE
			--au SACD --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		END IF;

		IF lemandat.man_tva != 0 THEN
			-- creation du mandat_brouillard visa CREDIT TVA --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
		END IF;

		/*
		if pconum_185 is not null then

		-- creation du mandat_brouillard visa SACD DEBIT --
		-- creation du mandat_brouillard visa DEBIT--
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'C',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);


		-- creation du mandat_brouillard visa SACD CREDIT --
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			       --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);

		end if;

		*/
	ELSE
		Bordereau_Mandat.set_mandat_brouillard_intern(manid);
	END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;
	leplancomptable 	  PLAN_COMPTABLE%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;


-- 	-- recup de l agence comptable --
-- 	SELECT c.ges_code,PCO_NUM_185
-- 		INTO gescodecompta,pconum_185
-- 		FROM GESTION g, COMPTABILITE c
-- 		WHERE g.ges_code = lemandat.ges_code
-- 		AND g.com_ordre = c.com_ordre;


			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;



	-- recup des infos du VISA CREDIT --
	SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	IF cpt = 0 THEN
	   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
	END IF;
	SELECT pco_num_ctrepartie, PCO_NUM_TVA
		INTO pconum_ctrepartie, PCONUM_TVA
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||lemandat.pco_num;

	IF cpt = 0 THEN
	 SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
	 WHERE pco_num = lemandat.pco_num;

	 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
	END IF;

--	lemandat.pco_num := '18'||lemandat.pco_num;

	-- creation du mandat_brouillard visa DEBIT--
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ht,			   --MAB_MONTANT,
		'VISA MANDAT',				   --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		'18'||lemandat.pco_num			   --PCO_NU
		);

	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '181';

	IF cpt = 0 THEN
		 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');
		--ELSE
		-- SELECT * INTO leplancomptable FROM plan_comptable
		-- WHERE pco_num = '181';
	END IF;


	-- planco de CREDIT 181
	-- creation du mandat_brouillard visa CREDIT --
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						  --ECD_ORDRE,
		lemandat.exe_ordre,			  --EXE_ORDRE,
		gescodecompta,				  --GES_CODE,
		lemandat.man_ttc,		  	  --MAB_MONTANT,
		'VISA MANDAT',				  --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
		'C',						  --MAB_SENS,
		manid,						  --MAN_ID,
		'181'				  --PCO_NU
		);



	IF lemandat.man_tva != 0 THEN
		-- creation du mandat_brouillard visa CREDIT TVA --
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
	END IF;

END;

PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
	niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


PROCEDURE get_facture_jefy
	(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefyfacture 	 jefy.factures%ROWTYPE;
	lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	fouadresse  	 DEPENSE.dep_adresse%TYPE;
	founom  		 DEPENSE.dep_fournisseur%TYPE;
	lotordre  		 DEPENSE.dep_lot%TYPE;
	marordre		 DEPENSE.dep_marches%TYPE;
	fouordre		 DEPENSE.fou_ordre%TYPE;
	gescode			 DEPENSE.ges_code%TYPE;
	modordre		 DEPENSE.mod_ordre%TYPE;
	cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;
	ecd_ordre_ema	 ECRITURE_DETAIL.ecd_ordre%TYPE;

	CURSOR factures IS
	 SELECT * FROM jefy.factures
	 WHERE man_ordre = manordre;

BEGIN

	OPEN factures;
	LOOP
		FETCH factures INTO jefyfacture;
			  EXIT WHEN factures%NOTFOUND;

		-- creation du depid --
		SELECT depense_seq.NEXTVAL INTO depid FROM dual;


		SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		 IF cpt = 0 THEN
			-- creation de lignebudgetaire--
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM jefy.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);
		ELSE
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT  MAX(org_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);
		END IF;

		-- recuperations --

		-- gescode --
		SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		IF cpt = 0 THEN

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;



			SELECT org_comp
				INTO gescode
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM jefy.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouordre --
			 SELECT fou_ordre INTO fouordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				 SELECT lot_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
				   SELECT lot_ordre
				   FROM jefy.commande
				   WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;



		ELSE
			 --recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.facture_ext WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

			SELECT org_comp
				INTO gescode
				FROM jefy.organ
				WHERE org_ordre =
				(
				SELECT MAX(org_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre  FROM MANDAT
				 WHERE man_id = manid
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre
				 FROM MANDAT
				 WHERE man_id = manid
				);



			-- fouordre --
			SELECT fou_ordre INTO fouordre
			 FROM MANDAT
			 WHERE man_id = manid;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				SELECT MAX(lot_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
					  SELECT MAX(lot_ordre)
					  FROM jefy.facture_ext
					  WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;
		END IF;




		-- marordre --
		SELECT COUNT(*) INTO cpt
			FROM marches.lot
			WHERE lot_ordre = lotordre;

		IF cpt = 0 THEN
		   	   marordre :=NULL;
		ELSE
			 SELECT mar_ordre
				 INTO marordre
				 FROM marches.lot
				 WHERE lot_ordre = lotordre;
		END IF;



			--MOD_ORDRE --
			SELECT mod_ordre INTO modordre
			FROM MODE_PAIEMENT
			WHERE mod_code =jefyfacture.mod_code AND exe_ordre=exeordre;


		-- recuperer l'ecriture_detail pour emargements semi-auto
		SELECT MAX(ecd_ordre) INTO ecd_ordre_ema FROM jefy.facture_emargement WHERE dep_ordre=jefyfacture.dep_ordre;




		-- creation de la depense --
		INSERT INTO DEPENSE VALUES
			(
			fouadresse ,           --DEP_ADRESSE,
			NULL ,				   --DEP_DATE_COMPTA,
			jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
			jefyfacture.dep_date , --DEP_DATE_SERVICE,
			'VALIDE' ,			   --DEP_ETAT,
			founom ,			   --DEP_FOURNISSEUR,
			jefyfacture.dep_mont , --DEP_HT,
			depense_seq.NEXTVAL ,  --DEP_ID,
			lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
			lotordre ,			   --DEP_LOT,
			marordre ,			   --DEP_MARCHES,
			jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
			jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
			jefyfacture.dep_fact  ,--DEP_NUMERO,
			jefyfacture.dep_ordre ,--DEP_ORDRE,
			NULL ,				   --DEP_REJET,
			jefyfacture.rib_ordre ,--DEP_RIB,
			'NON' ,				   --DEP_SUPPRESSION,
			jefyfacture.dep_ttc ,  --DEP_TTC,
			jefyfacture.dep_ttc - jefyfacture.dep_mont, -- DEP_TVA,
			exeordre ,			   --EXE_ORDRE,
			fouordre, 			   --FOU_ORDRE,
			gescode,  			   --GES_CODE,
			manid ,				   --MAN_ID,
			jefyfacture.man_ordre, --MAN_ORDRE,
--			jefyfacture.mod_code,  --MOD_ORDRE,
			modordre,
			jefyfacture.pco_num ,  --PCO_ORDRE,
			utlordre,    		   --UTL_ORDRE
			NULL, --org_ordre
			tcdordre,
			ecd_ordre_ema -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
			);

	END LOOP;
	CLOSE factures;

END;

END;
/
