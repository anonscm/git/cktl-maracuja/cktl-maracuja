set define off
--
-- executer a partir de maracuja
-----------------------------------------------------------

ALTER TABLE maracuja.MANDAT drop CONSTRAINT FK_MANDAT_man_attente_paiement;

update maracuja.mandat set MAN_ATTENTE_PAIEMENT=4 where MAN_ATTENTE_PAIEMENT=0;
commit;


ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT FK_MANDAT_man_attente_paiement FOREIGN KEY (man_attente_paiement) REFERENCES jefy_admin.type_ETAT(tyet_id)); 
