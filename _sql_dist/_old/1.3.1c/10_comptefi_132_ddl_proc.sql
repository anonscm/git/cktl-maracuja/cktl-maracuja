CREATE OR REPLACE FUNCTION comptefi.Solde_Compte_ext_avt_S67 (exeordre number, listeCompte VARCHAR2, sens_solde CHAR, comp VARCHAR2, sacd CHAR)
RETURN NUMBER

IS
  pco_condition varchar2(500);
  lavue varchar2(50);
  LC$Token varchar2(20);
  i          PLS_INTEGER := 1 ;
BEGIN
	 lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';

 	 LOOP
        LC$Token := zSplit( listeCompte, i , ',') ;
        EXIT WHEN LC$Token IS NULL ;
		if i>1 then
		   pco_condition := pco_condition || ' or ';
		end if;
		--dbms_output.put_line( '>' || LC$Token );
		pco_condition := pco_condition || ' pco_num like '''|| LC$Token ||'''';
       	i := i + 1 ;
     END LOOP ;
	 pco_condition := '('||pco_condition || ' )';
     --dbms_output.put_line( pco_condition );

   	 return Solde_Compte_ext(exeordre, pco_condition , sens_solde , comp , sacd, lavue);
END ;
/

--------------------------------------------------------
--------------------------------------------------------

CREATE OR REPLACE PROCEDURE comptefi.prepare_SIG (exeordre number, gescode varchar2, sacd char)
IS

  -- version du 08/03/2006 
  
  montant NUMBER(12,2);
  montant_charges NUMBER(12,2);
  montant_produits NUMBER(12,2);
  montant_groupe number(12,2);

  va number(12,2);
  ebe number(12,2);
  resultat_exploitation number(12,2);
  resultat_courant number(12,2);
  resultat_exceptionnel number(12,2);
  resultat_net number(12,2);


  anneeExer NUMBER;

  pconum number;
  groupe1 varchar(50);
  groupe2 varchar(50);

  lib varchar(100);
  formule varchar(1000);
  commentaire varchar(1000);

BEGIN

--*************** CREATION TABLE *********************************
IF sacd = 'O' THEN
 DELETE FROM SIG where exe_ordre = exeordre and ges_code = gescode;
ELSE
 DELETE FROM SIG where exe_ordre = exeordre and ges_code = 'ETAB';
END IF;



----- VALEUR AJOUTEE ----
   groupe1 := 'Valeur ajout�e';
   montant_charges := 0;
   montant_produits := 0;

   groupe2 := 'produits';

    lib := 'Vente et prestations de services (C.A.)';
    formule := 'SC(70+1870)- SD(709+18709)';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '70%,1870%', 'C', gescode, sacd) - Solde_Compte_ext_avt_S67(exeordre, '709%,18709%', 'D', gescode, sacd);
    montant_produits := montant_produits + montant;
    insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

    lib := 'Production stock�e';
    formule := 'SC713+18713 - SD 713+18713';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '713%,18713%', 'C', gescode, sacd) - Solde_Compte_ext_avt_S67(exeordre, '713%,18713%', 'D', gescode, sacd);
    montant_produits := montant_produits + montant;
    insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

    lib := 'Production immobilis�e';
    formule := 'SC72+1872';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '72%,1872%', 'C', gescode, sacd);
    montant_produits := montant_produits + montant;
    insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );


   groupe2 := 'charges';

    lib := 'Achats';
    formule := 'SD(601+602+604+605+606+607+608)+SD603-SC603-SC609';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_ext_avt_S67(exeordre, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre, '609%,18609%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
    insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire);

    lib := 'Services exterieurs';
    formule := 'SD61 - SC619';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre, '619%,18619%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
    insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire);

    lib := 'Autres services exterieurs (sauf personnel exterieur)';
    formule := 'SD(62 - 621) - SC629';
    commentaire := '';
    montant := Solde_Compte_ext(exeordre, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE')+ Solde_Compte_ext(exeordre, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_ext_avt_S67(exeordre, '629%,18629%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
    insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire);


    va := montant_produits - montant_charges;

-----------------------------------------
 groupe1 := 'Exc�dent/Insuffisance brut(e) d''exploitation';
  montant_charges := 0;
  montant_produits := 0;
  if (va>=0) then
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'produits', 'Valeur ajout�e', va, '','');
   montant_produits := va;
  else
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'charges', 'Valeur ajout�e', -va, '','');
   montant_charges := -va;
  end if;

 groupe2 := 'produits';

  lib := 'Subventions d''exploitation d''etat';
  formule := 'SC741';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '741%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Subventions d''exploitation collectivit�s publiques et organismes internationaux';
  formule := 'SC744';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '744%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Dons / legs et autres subventions d''exploitation';
  formule := 'SC(746+748)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '746%,748%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );


 groupe2 := 'charges';

  lib := 'Charges de personnel (y c. le personnel ext�rieur)';
  formule := 'SD(64+621)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '64%,621%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Impots, taxes et versements assimil�s s/ r�mun�rations';
  formule := 'SD(631+632+633)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '631%,632%,633%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Autres Impots, taxes et versements assimil�s';
  formule := 'SD(635+637)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '635%,637%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  ebe := montant_produits - montant_charges;
-----------------------------------------

 groupe1 := 'R�sultat d''exploitation';
  montant_charges := 0;
  montant_produits := 0;
  if (ebe >= 0) then
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'produits', 'Exc�dent brut d''exploitation', ebe, '','EBE');
   montant_produits := ebe;
  else
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'charges', 'Insuffisance brute d''exploitation', -ebe, '','EBE');
   montant_charges := -ebe;
  end if;



 groupe2 := 'produits';

  lib := 'Reprise sur amortissements et provisions';
  formule := 'SC781';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '781%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Transfert de charges d''exploitation';
  formule := 'SC791';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '791%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Autres produits';
  formule := 'SC75+SC187%';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '75%,187%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

 groupe2 := 'charges';

  lib := 'Autres charges';
  formule := 'SD65+SD186%';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '65%,186%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  lib := 'Dotations aux amortissements et provisions';
  formule := 'SD681';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '681%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

 groupe2 := 'produits';

  lib := 'Produits issus de la neutralisation des amortissements';
  formule := 'SC776';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '776%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );


  lib := 'Quote-part des subventions d''investissement vir�e au r�sultat de l''exercice';
  formule := 'SC777';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '777%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

  resultat_exploitation := montant_produits - montant_charges;

----------------------------------------------------------------------

 groupe1 := 'Resultat courant';
  montant_charges := 0;
  montant_produits := 0;

  if (resultat_exploitation >= 0) then
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'produits', 'Resultat d''exploitation', resultat_exploitation, '','');
   montant_produits := resultat_exploitation;
  else
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'charges', 'Resultat d''exploitation', -resultat_exploitation, '','');
   montant_charges := -resultat_exploitation;
  end if;




 groupe2 := 'produits';

  lib := 'Produits financiers';
  formule := 'SC(76+786+796)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '76%,786%,796%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

 groupe2 := 'charges';

  lib := 'Charges financi�res';
  formule := 'SD(66+686)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '66%,686%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );


  resultat_courant := montant_produits - montant_charges;
----------------------------------------------------------------------------------

 groupe1 := 'Resultat exceptionnel';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits exceptionnels (sauf c/ 776 et 776)';
  formule := 'SC(77 - 776 -777) + SC(787 + 797)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '77,771%,772%,773%,774%,775%,778%,779%', 'C', gescode, sacd) +
     Solde_Compte_ext_avt_S67(exeordre, '787%,797%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

 groupe2 := 'charges';

  lib := 'Charges exceptionnelles';
  formule := 'SD(67+687)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '67%,687%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

 resultat_exceptionnel := montant_produits - montant_charges;

-------------------------------------------------------------------

 groupe1 := 'Resultat net';
  montant_charges := 0;
  montant_produits := 0;

  if (resultat_courant >= 0) then
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'produits', 'Resultat courant',resultat_courant, '','');
  else
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'charges', 'Resultat courant', -resultat_courant, '','');
  end if;

  if (resultat_exceptionnel >= 0) then
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'produits', 'Resultat exceptionnel',resultat_exceptionnel, '','');
  else
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'charges', 'Resultat exceptionnel', -resultat_exceptionnel, '','');
  end if;


  resultat_net := resultat_courant + resultat_exceptionnel;
--------------------------------------------------------------
 groupe1 := 'Resultat net apr�s impots';
  montant_charges := 0;
  montant_produits := 0;

  if (resultat_net >= 0) then
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'produits', 'Resultat net',resultat_net, '','');
  else
   insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, 'charges', 'Resultat net', -resultat_net, '','');
  end if;

 groupe2 := 'charges';

  lib := 'Impots sur les b�n�fices et impots assimil�s';
  formule := 'SD(695+697+699)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '695%,697%,699%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

---------------------------------------------------------------------

 groupe1 := 'Plus ou moins-value sur cession d''actif';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits des cessions d''�l�ments d''actif';
  formule := 'SC775';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '775%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );

 groupe2 := 'charges';

  lib := 'Valeurs comptable des �l�ments d''actif c�d�s';
  formule := 'SD675';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '675%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  insert into SIG values (SIG_SEQ.nextval, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire );


end;
/

--------------------------------------------------------
--------------------------------------------------------

CREATE OR REPLACE PROCEDURE comptefi.PREPARE_DETERMINATION_CAF (exeordre number, gescode varchar2, sacd char, methodeEBE char)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib varchar2(100);
  lib1 varchar2(50);
  total_produits NUMBER(12,2);
  total_charges NUMBER(12,2);
  formule varchar2(50);
  ebe NUMBER(12,2);
  resultat NUMBER(12,2);
  
  -- version du 24/02/2005 

BEGIN

--*************** DETERMINATION A PARTIR DE EBE  *********************************
IF methodeEBE = 'O' THEN

	IF sacd = 'O' THEN
		DELETE CAF where exe_ordre = exeordre and ges_code = gescode and methode_ebe = 'O';
	ELSE
		DELETE CAF where exe_ordre = exeordre and ges_code = 'ETAB' and methode_ebe = 'O';
	END IF;

	total_produits :=0 ;
	total_charges :=0;
	formule := '';

	--**** RECUPERATION EBE ********
	select nvl(sig_montant,0), groupe2, sig_libelle into ebe, lib1, lib from sig
	where gescode = ges_code and exe_ordre = exeordre
	and groupe1 = 'R�sultat d''exploitation' and commentaire = 'EBE';
	totalant := 0;
	if lib1 = 'charges' then
		ebe := -ebe;
	end if;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, ebe, totalant, formule);

	-- ********  Produits ***********
	lib1 := 'produits';

	total := resultat_compte(exeordre, '75%', gescode, sacd)
		+ resultat_compte(exeordre, '1875%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '75%', gescode, sacd)
		+ resultat_compte(exeordre-1, '1875%', gescode, sacd);
	lib := '+ Autres produits "encaissables" d''exploitation';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


	total := resultat_compte(exeordre, '791%', gescode, sacd)
		+ resultat_compte(exeordre, '18791%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '791%', gescode, sacd)
		+ resultat_compte(exeordre-1, '18791%', gescode, sacd);
	lib := '+ Transferts de charges';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	total := resultat_compte(exeordre, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+resultat_compte(exeordre, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+ resultat_compte(exeordre-1, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
	lib := '+ Produits financiers "encaissables" (a)';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	total := resultat_compte(exeordre, '771%', gescode, sacd)+resultat_compte(exeordre, '778%', gescode, sacd)+resultat_compte(exeordre, '797%', gescode, sacd)+resultat_compte(exeordre, '18771%', gescode, sacd)+resultat_compte(exeordre, '18778%', gescode, sacd)+resultat_compte(exeordre, '18797%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '771%', gescode, sacd)+resultat_compte(exeordre-1, '778%', gescode, sacd)+resultat_compte(exeordre-1, '797%', gescode, sacd)+resultat_compte(exeordre-1, '18771%', gescode, sacd)+resultat_compte(exeordre-1, '18778%', gescode, sacd)+resultat_compte(exeordre-1, '18797%', gescode, sacd);
	lib := '+ Produits exceptionnels "encaissables" (b)';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	-- ********  charges ***********
	lib1 := 'charges';

	total := resultat_compte(exeordre, '65%', gescode, sacd)
		+ resultat_compte(exeordre, '1865%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '65%', gescode, sacd)
		+ resultat_compte(exeordre-1, '1865%', gescode, sacd);
	lib := '- Autres charges "d�caissables" d''exploitation';
	total_charges := total_charges+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '66%', gescode, sacd)+resultat_compte(exeordre, '1866%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '66%', gescode, sacd)+resultat_compte(exeordre-1, '1866%', gescode, sacd);
	lib := '- Charges financi�res "d�caissables" (c)';
	total_charges := total_charges+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '671%', gescode, sacd)+resultat_compte(exeordre, '678%', gescode, sacd)+resultat_compte(exeordre, '18671%', gescode, sacd)+resultat_compte(exeordre, '18678%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '671%', gescode, sacd)+resultat_compte(exeordre-1, '678%', gescode, sacd)+resultat_compte(exeordre-1, '18671%', gescode, sacd)+resultat_compte(exeordre-1, '18678%', gescode, sacd);
	lib := '- Charges exceptionnelles "d�caissables" (d)';
	total_charges := total_charges+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '695%', gescode, sacd)+resultat_compte(exeordre, '18695%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '695%', gescode, sacd)+resultat_compte(exeordre-1, '18695%', gescode, sacd);
	lib := '- Imp�ts sur les b�n�fices';
	total_charges := total_charges+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	--- ************ Calcul de la caf *****************
	total := ebe+total_produits-total_charges;
	totalant := 0;
	if total >= 0 then
		lib1:= 'produits';
		lib := '= CAPACITE D''AUTOFINANCEMENT';
	else
		lib1:= 'charges';
		lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
	end if;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


ELSE

	--********* D�termination � partir du r�sultat **********************

	IF sacd = 'O' THEN
		DELETE caf where exe_ordre = exeordre and ges_code = gescode and methode_ebe = 'N';
	ELSE
		DELETE caf where exe_ordre = exeordre and ges_code = 'ETAB' and methode_ebe = 'N';
	END IF;

	total_produits :=0 ;
	total_charges :=0;
	formule := '';

	--**** RECUPERATION RESULTAT ********
	totalant := 0;
	lib := 'R�sultat de l''exercice';
	IF sacd = 'O' THEN
		select sum(credit)- sum(debit) into resultat from maracuja.cfi_ecritures
		where (pco_num = '120' or pco_num = '129') and ges_code = gescode and exe_ordre = exeordre;
	ELSE
		select sum(credit)- sum(debit) into resultat from maracuja.cfi_ecritures
		where (pco_num = '120' or pco_num = '129') and ecr_sacd = 'N' and exe_ordre = exeordre;
	END IF;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, resultat, totalant, formule);

	-- ********  Charges ***********
	lib1 := 'charges';

	total := resultat_compte(exeordre, '681%', gescode, sacd)+resultat_compte(exeordre, '686%', gescode, sacd)+resultat_compte(exeordre, '687%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '681%', gescode, sacd)+resultat_compte(exeordre-1, '686%', gescode, sacd)+resultat_compte(exeordre-1, '687%', gescode, sacd);
	lib := '+ Dotations aux amortissements et provisions';
	total_charges := total_charges+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	total := resultat_compte(exeordre, '675%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
	lib := '+ Valeur comptable des �l�ments actifs c�d�s';
	total_charges := total_charges+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	-- ********  Produits ***********
	lib1 := 'produits';

	total := resultat_compte(exeordre, '781%', gescode, sacd)+resultat_compte(exeordre, '786%', gescode, sacd)+resultat_compte(exeordre, '787%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '781%', gescode, sacd)+resultat_compte(exeordre-1, '786%', gescode, sacd)+resultat_compte(exeordre-1, '787%', gescode, sacd);
	lib := '- Reprises sur amortissements et provisions';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '775%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
	lib := '- Produits de cessions des �l�ments actifs c�d�s';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '776%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
	lib := '- Produits issus de la neutralisation des amortissements';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '777%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
	lib := '- Quote-part des subventions d''investissement vir�es au compte de r�sultat';
	total_produits := total_produits+total;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	--- ************ Calcul de la caf *****************
	total := resultat+total_charges-total_produits;
	totalant := 0;
	formule := 'CAF';
	if total >= 0 then
		lib1:= 'produits';
		lib := '= CAPACITE D''AUTOFINANCEMENT';
	else
		lib1:= 'charges';
		lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
	end if;
	insert into caf values (caf_seq.nextval, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


END IF;

end;
/

