SET define OFF
SET scan OFF



CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_TITRE
(EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE)
AS 
SELECT 2006 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE,
TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE,
TIT_VIREMENT, MOD_CODE,
DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
FROM jefy.TITRE f
WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
SELECT 2005 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '' AS can_code, -1 AS pres_ordre
FROM jefy05.TITRE f
UNION ALL
SELECT 2004 AS exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 AS conv_ordre, '' AS can_code, -1 AS pres_ordre
FROM jefy04.TITRE f
UNION ALL
SELECT exe_ordre AS exe_exercice, tit_ordre, TIT_DATE_REMISE AS tit_date, REPLACE( TO_CHAR(TIT_TTC),',','.' )AS tit_mont,  REPLACE( TO_CHAR(TIT_TVA),',','.' )AS tit_monttva, tit_libelle AS tit_lib, NULL AS tit_type, tit_nb_piece AS tit_piece, NULL AS jou_ordre, f.fou_ordre, pco_num, NULL AS tit_imputtva, bor_ordre, org_ordre,
NULL AS tit_interne,  TIT_NUMero AS tit_num,utl_ORDRE AS agt_ordre, NULL AS TIT_STAT, GES_CODE,  f.ADR_NOM || ' ' || f.adr_prenom    AS TIT_DEBITEUR, NULL AS DEP_ORDRE, NULL AS RIB_ORDRE, NULL AS TIT_MONNAIE,
NULL AS TIT_VIREMENT, NULL AS MOD_CODE,
NULL AS DST_CODE, NULL AS TIT_REF, NULL AS CONV_ORDRE, NULL AS CAN_CODE, NULL AS PRES_ORDRE
FROM maracuja.TITRE t, v_fournisseur f
WHERE t.exe_ordre>2006 AND t.fou_ordre=f.fou_ordre;
/


CREATE OR REPLACE FORCE VIEW EPN.EPN_REC_BUD
(EXE_ORDRE, PCO_NUM, GES_CODE, EPN_DATE, MONT_REC, 
 ANNUL_TIT_REC)
AS 
SELECT t.exe_ordre, t.pco_num, b.ges_code, b.BOR_date_VISA, SUM(t.TIT_ht), 0
FROM maracuja.TITRE t, maracuja.BORDEREAU b
--  titres de recette
WHERE (b.TBO_ORDRE = 7 OR b.tbo_ordre=11) AND b.BOR_ETAT = 'VISE' AND tit_etat = 'VISE'
AND t.bor_id = b.bor_id
GROUP BY t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa
UNION ALL
-- r�duction de recette
SELECT t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa, 0, SUM(t.TIT_ht)
FROM   maracuja.TITRE t, maracuja.BORDEREAU b
WHERE b.tbo_ordre = 9 AND b.BOR_ETAT = 'VISE' AND tit_etat = 'VISE'
AND t.bor_id = b.bor_id
GROUP BY t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa;
/

CREATE OR REPLACE FORCE VIEW EPN.EPN_DEP_BUD
(EXE_ORDRE, PCO_NUM, GES_CODE, EPN_DATE, DEP_MONT, 
 REVERS)
AS 
SELECT m.EXE_ORDRE, pco_num, m.ges_code , bor_date_visa, SUM(man_ht), 0 
-- Mandat d�penses 
 FROM maracuja.MANDAT m, maracuja.BORDEREAU b 
 WHERE m.bor_id = b.bor_id AND (tbo_ordre <> 8 AND tbo_ordre <> 18) 
 AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE') 
 AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
 AND b.tbo_ordre<>16
 GROUP BY m.exe_ordre, pco_num, m.ges_code, bor_date_visa 
UNION ALL 
-- Ordre de reversement avant 2007 
 SELECT t.exe_ordre, pco_num , t.ges_code , bor_date_visa , 0 , SUM (tit_ht) 
 FROM maracuja.TITRE t, maracuja.BORDEREAU b 
 WHERE t.bor_id = b.bor_id AND tbo_ordre = 8 
 AND b.BOR_ETAT = 'VISE' AND t.TIT_ETAT = 'VISE' 
 GROUP BY t.exe_ordre, pco_num , t.ges_code , bor_date_visa 
UNION ALL 
 SELECT m.EXE_ORDRE, pco_num, m.ges_code , bor_date_visa, 0, -SUM(man_ht) 
-- Ordre de reversement � partir de 2007 
 FROM maracuja.MANDAT m, maracuja.BORDEREAU b 
 WHERE m.bor_id = b.bor_id AND (tbo_ordre = 8 or tbo_ordre=18)
 AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE') 
 AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
 GROUP BY m.exe_ordre, pco_num, m.ges_code, bor_date_visa;
 
 
/


CREATE OR REPLACE FORCE VIEW COMPTEFI.V_DVLOP_DEP_LISTE
(EXE_ORDRE, GES_CODE, SECTION, PCO_NUM_BDN, PCO_NUM, 
 MANDATS, REVERSEMENTS, CR_OUVERTS, DEP_DATE)
AS 
SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, SUM(man_ht), 0, 0, e.DATE_SAISIE 
 -- Mandat d�penses 
 FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, ( 
   SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie 
   FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m 
   WHERE 
   m.man_id=mde.MAN_ID 
   AND mde.ecd_ordre=ecd.ecd_ordre 
   AND ecd.ecr_ordre=e.ecr_ordre 
   AND SUBSTR(e.ecr_etat,1,1)='V' 
   GROUP BY m.man_id ) e 
 WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND tbo_ordre <> 8 and tbo_ordre <>18 AND tbo_ordre <> 16 
 AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE') 
 AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
 GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE 
UNION ALL 
-- ordre de reversement avant 2007 
 SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0, SUM(tit_ht), 0, e.DATE_SAISIE 
 FROM maracuja.TITRE t, maracuja.bordereau b, comptefi.v_planco p, ( 
   SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie 
   FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m 
   WHERE 
   m.tit_id=mde.tit_ID 
   AND mde.ecd_ordre=ecd.ecd_ordre 
   AND ecd.ecr_ordre=e.ecr_ordre 
   AND SUBSTR(e.ecr_etat,1,1)='V' 
   GROUP BY m.tit_id ) e 
 WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND tbo_ordre = 8 
 AND b.BOR_ETAT = 'VISE' AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id 
 GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num, e.DATE_SAISIE 
UNION ALL 
-- ordre de reversement � partir de 2007 
 SELECT m.EXE_ORDRE, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, 0, -SUM(man_ht), 0, e.DATE_SAISIE 
 FROM maracuja.MANDAT m, maracuja.BORDEREAU b, comptefi.v_planco p, ( 
   SELECT m.man_id, MIN(e.ECR_DATE_SAISIE) date_saisie 
   FROM maracuja.ecriture_detail ecd, maracuja.MANDAT_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.mandat m 
   WHERE 
   m.man_id=mde.MAN_ID 
   AND mde.ecd_ordre=ecd.ecd_ordre 
   AND ecd.ecr_ordre=e.ecr_ordre 
   AND SUBSTR(e.ecr_etat,1,1)='V' 
   GROUP BY m.man_id ) e 
 WHERE m.PCO_NUM = p.PCO_NUM AND m.bor_id = b.bor_id AND m.man_id = e.man_id AND (tbo_ordre = 8 or tbo_ordre=18)
 AND (b.bor_etat = 'VISE' OR b.bor_etat = 'PAYE') 
 AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE') 
 GROUP BY m.exe_ordre, m.ges_code, p.SECTION, p.pco_num_bdn, m.pco_num, e.DATE_SAISIE 
UNION ALL 
 SELECT t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num , 0, SUM(tit_ht), 0, e.DATE_SAISIE 
 FROM maracuja.TITRE t, maracuja.bordereau b, comptefi.v_planco p, ( 
   SELECT m.tit_id, MIN(e.ECR_DATE_SAISIE) date_saisie 
   FROM maracuja.ecriture_detail ecd, maracuja.titre_DETAIL_ECRITURE mde, maracuja.ecriture e, maracuja.titre m 
   WHERE 
   m.tit_id=mde.tit_ID 
   AND mde.ecd_ordre=ecd.ecd_ordre 
   AND ecd.ecr_ordre=e.ecr_ordre 
   AND SUBSTR(e.ecr_etat,1,1)='V' 
   GROUP BY m.tit_id ) e 
 WHERE t.pco_num = p.pco_num AND t.bor_id = b.bor_id AND (tbo_ordre = 8 or tbo_ordre=18) 
 AND b.BOR_ETAT = 'VISE' AND t.TIT_ETAT = 'VISE' AND t.tit_id = e.tit_id 
 GROUP BY t.exe_ordre, t.ges_code, p.SECTION, p.pco_num_bdn, t.pco_num, e.DATE_SAISIE 
UNION ALL 
-- Cr�dits ouverts 
SELECT EXE_ORDRE, GES_CODE, '1' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO 
FROM comptefi.v_budnat WHERE (pco_num LIKE '6%' OR pco_num LIKE '7%') 
AND BDN_QUOI ='D' AND co <> 0 
UNION ALL 
SELECT EXE_ORDRE, GES_CODE, '2' SECTION, PCO_NUM, pco_num, 0,0, CO, DATE_CO 
FROM comptefi.v_budnat WHERE (pco_num LIKE '1%' OR pco_num LIKE '2%') 
AND BDN_QUOI ='D' AND co <> 0; 

/


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, MDE_ORIGINE, ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num, 
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, (SIGN(ecd_debit))*m.man_tva, 
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant 
 --- mandats des d�penses ---
 FROM MANDAT m, 
      MANDAT_DETAIL_ECRITURE mde, 
       ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      BORDEREAU b, 
      TYPE_BORDEREAU tb 
WHERE m.man_id = mde.man_id 
       AND m.BOR_ID=b.bor_id 
      AND b.tbo_ordre=tb.tbo_ordre 
  AND mde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND mde_origine IN ('VISA', 'REIMPUTATION') 
  AND man_etat IN ('VISE', 'PAYE') 
  AND ecd_debit<>0 
  AND ABS (ecd_montant)=ABS(man_ht) 
  AND ED.pco_num NOT LIKE '185%' 
  AND (tb.TBO_TYPE='BTME' OR tb.TBO_TYPE='BTMS') 
  AND tb.tbo_sous_type <> 'REVERSEMENTS'  -- pour �liminer les �critures de d�bit des OR 
UNION ALL 
 ---  mandats des PI --- 
 SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num, 
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, (SIGN(ecd_debit))*m.man_tva, 
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant 
 FROM MANDAT m, 
      MANDAT_DETAIL_ECRITURE mde, 
       ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      BORDEREAU b, 
      TYPE_BORDEREAU tb 
WHERE m.man_id = mde.man_id 
       AND m.BOR_ID=b.bor_id 
      AND b.tbo_ordre=tb.tbo_ordre 
  AND mde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND mde_origine IN ('VISA', 'REIMPUTATION') 
  AND man_etat IN ('VISE', 'PAYE') 
  AND ecd_debit<>0 
  AND ABS (ecd_montant)=ABS(man_ht) 
  AND ED.pco_num NOT LIKE '185%' 
  AND (tb.TBO_TYPE='BTPI' OR tb.TBO_TYPE='BTPID') -- ajout du nouveau type BTPID -- 
UNION ALL 
-- Ordres de Reversements � partir de 2007 -- 
 SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num, 
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_credit, (SIGN(ecd_credit))*m.man_tva, 
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant 
 FROM MANDAT m, 
      MANDAT_DETAIL_ECRITURE mde, 
       ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      BORDEREAU b, 
      TYPE_BORDEREAU tb 
WHERE m.man_id = mde.man_id 
       AND m.BOR_ID=b.bor_id 
      AND b.tbo_ordre=tb.tbo_ordre 
  AND mde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND mde_origine IN ('VISA', 'REIMPUTATION') 
  AND man_etat IN ('VISE', 'PAYE') 
  AND ecd_credit <> 0 
  AND ABS (ecd_montant)=ABS(man_ht) 
  AND ED.pco_num NOT LIKE '185%' 
  AND tb.tbo_sous_type = 'REVERSEMENTS' -- Bordereau des OR;

  