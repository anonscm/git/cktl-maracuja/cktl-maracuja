SET define OFF
SET scan OFF


INSERT INTO maracuja.TYPE_BORDEREAU ( TBO_LIBELLE, TBO_ORDRE, TBO_SOUS_TYPE, TBO_TYPE, TBO_TYPE_CREATION,
TNU_ORDRE ) VALUES ( 
'BORDEREAU DE REVERSEMENTS DE SALAIRES', 18, 'REVERSEMENTS', 'BTME', 'DEP_18', 20);


commit;



-- suppression des doublons
DELETE FROM maracuja.PLANCO_AMORTISSEMENT WHERE pca_id IN ( 
SELECT pca_id FROM  
(SELECT pcoa_id, exe_ordre, pco_num, pca_duree, MIN(pca_id) pca_id FROM maracuja.PLANCO_AMORTISSEMENT  GROUP BY  pcoa_id, exe_ordre, pco_num, pca_duree HAVING COUNT(*)>1) x
);
commit;


INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.6.0',  SYSDATE, '');