-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Basculer_Be IS

/*
CRI G guadeloupe - Rivalland Frederic.
CRI LR - Prin Rodolphe

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja lors
du passage des ecritures de balance entree

*/
/*
create table ecriture_detail_be_log
(
edb_ordre integer,
edb_date date,
utl_ordre integer,
ecd_ordre integer)

create sequence basculer_solde_du_copmpte_seq start with 1 nocache;


INSERT INTO TYPE_OPERATION ( TOP_LIBELLE, TOP_ORDRE, TOP_TYPE ) VALUES (
'BALANCE D ENTREE AUTOMATIQUE', 11, 'PRIVEE');

*/
-- PUBLIC --

-- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
PROCEDURE basculer_solde_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
PROCEDURE basculer_DC_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);


PROCEDURE basculer_DC_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER);
PROCEDURE basculer_detail_du_cpt_ges_n(pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT LONG);


-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
PROCEDURE basculer_solde_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);



PROCEDURE basculer_DC_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_DC_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR);

PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER);

-- PRIVATE --
PROCEDURE creer_detail_890 (ecrordre INTEGER,pconumlibelle VARCHAR, gescode VARCHAR);
PROCEDURE priv_archiver_la_bascule( pconum VARCHAR,gescode VARCHAR,utlordre INTEGER, exeordre INTEGER);
 PROCEDURE priv_archiver_la_bascule_ecd ( ecdordre	INTEGER, utlordre   INTEGER);
FUNCTION priv_get_exeordre_prec(exeordre INTEGER) RETURN INTEGER;
PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER);
PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER);
FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR;

FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER;
FUNCTION priv_getTopOrdre RETURN NUMBER;
FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode varchar) RETURN varchar;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Basculer_Be
IS
-- PUBLIC --
   PROCEDURE basculer_solde_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	  topordre := priv_getTopOrdre;
	  
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
-- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

--raise_application_error (-20001,' '||exeordreprec);
		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );
-- 		
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND pco_num = pconumtmp;

--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
--
--          --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);
--          SELECT ABS (lesdebits - lescredits)
--            INTO lesolde
--            FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );		 
		 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
--         basculer_be.archiver_la_bascule (pconumtmp, gescode, utlordre);
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

-- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );		   
	   
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;

         --raise_application_error (-20001,' '||lesdebits||' '||lesens||' '||lesens890||' '||lesolde);
--          SELECT top_ordre
--            INTO topordre
--            FROM TYPE_OPERATION
--           WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
-- 
--          SELECT com_ordre, ges_code
--            INTO comordre, gescodeagence
--            FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );		
	  
	  
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND ges_code = gescode
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde);
      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;


         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, gescode, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );			  

--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
-- --and ges_code = gescode
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
-- --and ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)            --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconum)             --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
	  priv_nettoie_ecriture(monecdordre);
   END;

   -------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	   
	  
	  chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

	   
		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );				 
		 
-- 		 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
-- --and ges_code = gescode
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
-- --and ges_code = gescode
--             AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)          --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

   
 -- Cr�e une ecriture par ecriture_detail recupere
PROCEDURE basculer_detail_du_cpt_ges_n (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
	  gescode			VARCHAR,
      ecrordres   OUT   LONG
   )
   IS
      cpt             INTEGER;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
	  leGesCode       ECRITURE_DETAIL.ges_code%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monEcrOrdre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesEcd
      IS
         SELECT ecd_ordre, ecd_sens, ges_code, ecd_libelle,NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_reste_emarger <> 0
            AND pco_num = pconum
			AND ges_code=gescode;

   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
	  ecrordres := '';
      OPEN lesEcd;

      LOOP
         FETCH lesEcd
          INTO ecdordreOld, lesens, leGesCode, ecdlibelle, lemontant;
         EXIT WHEN lesEcd%NOTFOUND;
		 
		-- creation de lecriture  --
		monEcrOrdre := maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );		 
		 
         -- creation du detail ecriture --
         ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,       --ECDSENS,
                                                     monecrordre,  --ECRORDRE,
                                                     leGesCode, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
		  -- basculer les titre_ecriture_detail  -- 
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);
		  
		  -- creation de du log pour ne plus retraiter les ecritures ! --
		  priv_archiver_la_bascule_ecd(ecdordreOld, utlordre);
		  
		  IF (lesens='C') THEN
			lesens890 := 'D';
		  ELSE
			lesens890 := 'C';
		  END IF;
		  -- creer contrepartie
		  
		  
		  
		  ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                    'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '|| pconum,--ECDLIBELLE,
                                                    lemontant,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    monecrordre,      --ECRORDRE,
                                                    priv_getGesCodeCtrePartie(exeordre, legescode),  --GESCODE,
                                                    '890'             --PCONUM
                                                   );		  
		  maracuja.Api_Plsql_Journal.validerecriture (monEcrOrdre);
		  ecrordres := ecrordres || monecrordre || '$';
		  
      END LOOP;
      CLOSE lesEcd;

      
   END;   
   
   
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   	  
			  chaine := lespconums;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     'BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
		lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );					   
		   
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'D'
--          AND ges_code = gescode
--          AND pco_num = pconum;
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL ecd, ECRITURE e
--        WHERE e.ecr_ordre = ecd.ecr_ordre
--          AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--          AND e.exe_ordre = exeordreprec
--          AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                  FROM ECRITURE_DETAIL_BE_LOG)
--          AND ecd_sens = 'C'
--          AND ges_code = gescode
--          AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;


         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconum)            --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
		   chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

		lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
		lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );					 
		 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lesdebits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'D'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;
-- 
--          SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--            INTO lescredits
--            FROM ECRITURE_DETAIL ecd, ECRITURE e
--           WHERE e.ecr_ordre = ecd.ecr_ordre
--             AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--             AND e.exe_ordre = exeordreprec
--             AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                     FROM ECRITURE_DETAIL_BE_LOG)
--             AND ecd_sens = 'C'
--             AND ges_code = gescode
--             AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
-- 
--       SELECT com_ordre, ges_code
--         INTO comordre, gescodeagence
--         FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      'BE ' || TO_CHAR(exeordre)||' - '||ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      'BE ' || TO_CHAR(exeordre)||' - ' || ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
	   topordre := priv_getTopOrdre;
	   
         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;	
		   	  
chaine := lespconums;
     

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      'BE ' || TO_CHAR(exeordre)||' - '|| ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - '|| ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
   END;






------------------------------------------
------------------------------------------
   -- permet de retrouver le compte dans Maracuja comme s'il n'avait pas ete transfere en BE
    PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER)
       IS
       cpt INTEGER;
       BEGIN

	   SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL_BE_LOG edb, ECRITURE_DETAIL ecd WHERE edb.ecd_ordre=ecd.ecd_ordre AND ecd.pco_num=pconum AND ecd.exe_ordre=exeordreold;
    	IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture de n'' a �t� transf�r�e en BE pour le compte '|| pconum);
    	END IF;

		DELETE FROM ECRITURE_DETAIL_BE_LOG WHERE ecd_ordre IN (SELECT ecd_ordre FROM ECRITURE_DETAIL WHERE pco_num=pconum AND exe_ordre=exeordreold);


    END;



-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- 
--    -- PRIVATE --
--    PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR)
--    IS
--       cpt             INTEGER;
--       lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
--       lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
--       lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
--       topordre        TYPE_OPERATION.top_ordre%TYPE;
--       comordre        COMPTABILITE.com_ordre%TYPE;
--       ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
--       gescodeagence   COMPTABILITE.ges_code%TYPE;
-- 	  exeordre		  ECRITURE.EXE_ORDRE%TYPE;
--       monecdordre     INTEGER;
--    BEGIN
--       SELECT com_ordre, ges_code
--         INTO comordre, gescodeagence
--         FROM COMPTABILITE;
-- 
-- 		SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;
-- 		
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lesdebits
--         FROM ECRITURE_DETAIL
--        WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';
-- 
--       SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
--         INTO lescredits
--         FROM ECRITURE_DETAIL
--        WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';
-- 
--       IF (ABS (lesdebits) >= ABS (lescredits))
--       THEN
--          lesens890 := 'C';
--          lesolde := lesdebits - lescredits;
--       ELSE
--          lesens890 := 'D';
--          lesolde := lescredits - lesdebits;
--       END IF;
-- 
-- --       SELECT ABS (lesdebits - lescredits)
-- --         INTO lesolde
-- --         FROM DUAL;
-- --
--  --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde||' '||ecrordre);
--       IF lesolde != 0
--       THEN
-- --           IF (lesdebits >= lescredits)
-- --           THEN
-- --              lesens890 := 'C';
-- --           ELSE
-- --              lesens890 := 'D';
-- --           END IF;
-- 
--          -- creation du detail pour l'ecriture selon le CREDIT --
--          ecdordre :=
--             maracuja.Api_Plsql_Journal.creerecrituredetail
--                                                    (NULL,    --ECDCOMMENTAIRE,
--                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
--                                                     || pconumlibelle,
--                                                     --ECDLIBELLE,
--                                                     lesolde,     --ECDMONTANT,
--                                                     NULL,     --ECDSECONDAIRE,
--                                                     lesens890,      --ECDSENS,
--                                                     ecrordre,      --ECRORDRE,
--                                                     gescodeagence,  --GESCODE,
--                                                     '890'             --PCONUM
--                                                    );
-- -- Rod : ne pas bloquer (sinon pas possible de reprendre detail equilibre)
-- --      ELSE
-- --          raise_application_error (-20001,
-- --                                      'OUPS PROBLEME ECRITURE NON fred!!! '
-- --                                   || lesdebits
-- --                                   || ' '
-- --                                   || lescredits
-- --                                  );
--       END IF;
--    END;

   
   
PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR, gescode VARCHAR)
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
	  exeordre		  ECRITURE.EXE_ORDRE%TYPE;
      monecdordre     INTEGER;
   BEGIN
      SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;
	  
	  SELECT com_ordre, ges_code INTO comordre, gescodeagence
        FROM COMPTABILITE;
		
		-- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
		gescodeagence := priv_getGesCodeCtrePartie(exeordre, gescode);
		
      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         -- creation du detail pour l'ecriture
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumlibelle,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    ecrordre,      --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
      END IF;
   END;
   
   
   
   
   
   
   PROCEDURE priv_archiver_la_bascule_ecd (
      ecdordre	INTEGER,
      utlordre   INTEGER
   )
   IS
   BEGIN
   
		INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre, ecdordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre = ecdordre 
			 AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG);  
   
   END;
   
   
   PROCEDURE priv_archiver_la_bascule (
      pconum     VARCHAR,
      gescode    VARCHAR,
      utlordre   INTEGER,
      exeordre   INTEGER
   )
   IS
   BEGIN
      IF gescode IS NULL
      THEN
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND exe_ordre = exeordre
               AND pco_num = pconum;
      ELSE
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND pco_num = pconum
               AND exe_ordre = exeordre
               AND ges_code = gescode;
      END IF;
   END;

-- Renvoie l'identifiant de l'exercice precedent celui identifie par exeordre
   FUNCTION priv_get_exeordre_prec (exeordre INTEGER)
      RETURN INTEGER
   IS
      reponse           INTEGER;
      exeexerciceprec   EXERCICE.exe_exercice%TYPE;
   BEGIN
      SELECT exe_exercice - 1
        INTO exeexerciceprec
        FROM EXERCICE
       WHERE exe_ordre = exeordre;

      SELECT exe_ordre
        INTO reponse
        FROM EXERCICE
       WHERE exe_exercice = exeexerciceprec;

      RETURN reponse;
   END;


   PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER) IS
   BEGIN
   		DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = ecrOrdre AND ecd_montant=0 AND ecd_debit=0 AND ecd_credit=0;

   END;



   -- renvoie le compte de BE a utiliser (par ex renvoie 4011 pour 4012)
   -- ce compte est indique dans la table plan_comptable.
   -- si non precise, renvoie le compte passe en parametre
   FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR
   IS
   	 pconumnew PLAN_COMPTABLE.pco_num%TYPE;
   BEGIN
   		SELECT pco_compte_be INTO pconumnew FROM PLAN_COMPTABLE WHERE pco_num=pconumold;
		IF pconumnew IS NULL THEN
		   pconumnew := pconumold;
		END IF;
		RETURN pconumnew;
   END;




    -- Permet de suivre le detailecriture cree lors de la BE pour un titre.
    PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER)
       IS
       cpt INTEGER;
       infos TITRE_DETAIL_ECRITURE%ROWTYPE;
       BEGIN
       -- est un ecdordre pour un titre ?
          SELECT COUNT(*)
            INTO cpt
            FROM TITRE_DETAIL_ECRITURE
           WHERE ecd_ordre = ecdordreold;
    	IF cpt = 1 THEN
    		SELECT * INTO infos
       		FROM TITRE_DETAIL_ECRITURE
       	 WHERE ecd_ordre = ecdordreold;
      	INSERT INTO TITRE_DETAIL_ECRITURE (ECD_ORDRE, EXE_ORDRE, ORI_ORDRE, TDE_DATE, TDE_ORDRE, TDE_ORIGINE, TIT_ID, REC_ID)
			   VALUES (ecdordrenew, exeordrenew, infos.ORI_ORDRE, SYSDATE, titre_detail_ecriture_seq.NEXTVAL, infos.TDE_ORIGINE, infos.TIT_ID, infos.rec_id);
    	END IF;


    END;

	-- renvoi le solde non emarge des ecritures non prises en compte en BE
	FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER
	IS
	  lesolde NUMBER; 
	BEGIN
	   IF (gescode IS NULL) THEN	 
		      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
		        INTO lesolde
		        FROM ECRITURE_DETAIL ecd, ECRITURE e
		       WHERE e.ecr_ordre = ecd.ecr_ordre
		         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
		         AND e.exe_ordre = exeordreprec
		         AND ecd_ordre NOT IN (SELECT ecd_ordre
		                                 FROM ECRITURE_DETAIL_BE_LOG)
		         AND ecd_sens = sens
		         AND pco_num = pconum;	
		ELSE
		      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
		        INTO lesolde
		        FROM ECRITURE_DETAIL ecd, ECRITURE e
		       WHERE e.ecr_ordre = ecd.ecr_ordre
		         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
		         AND e.exe_ordre = exeordreprec
				 AND ges_code = gescode
		         AND ecd_ordre NOT IN (SELECT ecd_ordre
		                                 FROM ECRITURE_DETAIL_BE_LOG)
		         AND ecd_sens = sens
		         AND pco_num = pconum;			
		END IF;
		RETURN lesolde;
	END;
	
	
	FUNCTION priv_getTopOrdre RETURN NUMBER
	IS
	  topordre NUMBER;
	BEGIN
	      SELECT top_ordre INTO topordre
        		FROM TYPE_OPERATION
       			WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
			RETURN  topordre;
	END;
	
	-- renvoie gescode si sacd ou gescodeagence
	FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR
	IS
	  gescodeagence COMPTABILITE.ges_code%TYPE;
	  cpt INTEGER;
	BEGIN
	  SELECT ges_code INTO gescodeagence
        FROM COMPTABILITE;
		
		-- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
		IF gescode IS NOT NULL THEN		
		   		   SELECT COUNT(*) INTO cpt FROM GESTION_EXERCICE 
				   		  WHERE exe_ordre = exeordre AND ges_code=gescode AND pco_num_185 IS NOT NULL;
					IF cpt<>0 THEN
					   		  gescodeagence := gescode;
					END IF;
		END IF; 
		
		RETURN gescodeagence;
		
	END;
	
	
	
END;
/


-----------------------------------------------
-----------------------------------------------
-----------------------------------------------

-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.number_to_lettres IS


-- PUBLIC -
FUNCTION transformer (nb NUMBER) return varchar;


-- PRIVATE --
Function ZNb_En_Texte(nb  Number) return varchar;
function ZtraiteDixCent(nb number) return varchar;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Number_To_Lettres IS
-- version 1.0
-- rodolphe prin - cri La Rochelle

FUNCTION transformer (nb NUMBER) RETURN VARCHAR IS
BEGIN
	 RETURN ZNb_En_Texte(nb);
END;


FUNCTION ZtraiteDixCent(nb NUMBER) RETURN VARCHAR IS
	TYPE CHIFFRES IS VARRAY (20) OF VARCHAR(10);
	TYPE DIZAINES IS VARRAY (9) OF VARCHAR(16);

	chiffre CHIFFRES;
	dizaine DIZAINES;
	varlet VARCHAR(300);

	varnumD NUMBER(38);
	varnumU NUMBER(38);
	varnum NUMBER(38);

BEGIN

	chiffre := CHIFFRES('un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept','huit','neuf','dix','onze','douze','treize','quatorze','quinze', 'seize','dix-sept','dix-huit', 'dix-neuf');
	dizaine := DIZAINES('dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante-dix', 'quatre-vingt', 'quatre-vingt dix');

	varnum := nb;

	IF varnum>=1000000000 THEN
	   RAISE_APPLICATION_ERROR (-20001,'Impossible de transformer en lettres les valeurs superieures a 1000000000');
	END IF;

	---Traitement des centaines
	IF varnum >= 100 THEN
	   varlet := chiffre(TRUNC(varnum / 100));
	   varnum := varnum MOD 100;
		IF varlet = 'un' THEN
		   varlet := 'cent ';
		ELSE
			varlet := varlet || ' cent ';
		END IF;
	END IF;

	---Traitement des dizaines
	IF varnum <= 19 THEN ---Cas o� la dizaine est <20
		IF varnum > 0 THEN
		   varlet := varlet || chiffre(varnum);
		END IF;
	ELSE
		varnumD := TRUNC(varnum / 10); ---chiffre des dizaines
		varnumU := varnum MOD 10; ---chiffre des unit�s
		IF varnumD <= 5 THEN
		   varlet := varlet || dizaine(varnumD);
		ELSE
			IF varnumD IN (6,7) THEN
			   varlet := varlet || dizaine(6);
			END IF;
			IF varnumD IN (8,9) THEN
			   varlet := varlet || dizaine(8);
			END IF;
		END IF;

		---traitement du s�parateur des dizaines et unit�s
		IF varnumU = 1 AND varnumD < 8 THEN
		   varlet := varlet || ' et ';
		ELSE
			IF varnumU <> 0 OR varnumD = 7 OR varnumD = 9 THEN
			   --varlet := varlet || ' ';
			   varlet := varlet || '-';
			END IF;
		END IF;
		---g�n�ration des unit�s
		IF varnumD = 7 OR varnumD = 9 THEN
		   varnumU := varnumU + 10;
		END IF;
		IF varnumU <> 0 THEN
		   varlet := varlet || chiffre(varnumU);
		END IF;
	END IF;
	varlet := RTRIM(varlet, ' ');
	RETURN varlet;

END;






FUNCTION ZNb_En_Texte(nb  NUMBER) RETURN VARCHAR IS
	devise VARCHAR(50);
	centimes VARCHAR(10);
	resultat VARCHAR(500);
	varnum NUMBER(38);
	varlet VARCHAR(300);
	separateurMillion VARCHAR(3);
	exemax NUMBER;
BEGIN
	 -- Recuperer la devise locale
	 SELECT MAX(exe_ordre) INTO exemax FROM jefy_admin.PARAMETRE WHERE par_key = 'DEVISE_LIBELLE';	 
	 SELECT par_value INTO devise FROM jefy_admin.PARAMETRE WHERE exe_ordre = exemax AND par_key =   'DEVISE_LIBELLE';
--	 devise := 'euro';

	 centimes := 'cent';
	 separateurMillion := 'd''';

	 ---Traitement du cas z�ro
	IF nb <= 1 THEN
		resultat := 'zero';
	END IF;


	---Traitement des millions
	varnum := TRUNC (nb / 1000000);
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
	   resultat := varlet || ' million';
	   IF varlet <> 'un' THEN
	   	  resultat := resultat || 's';
	   END IF;
	END IF;

	---Traitement des milliers
	varnum := TRUNC(nb) MOD 1000000;
	varnum := TRUNC(varnum / 1000);
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
	   IF varlet <> 'un' THEN
	   	  resultat := resultat || ' ' || varlet;
	   END IF;
	   resultat := resultat || ' mille';
	END IF;

	---Traitement des centaines et dizaines
	varnum := TRUNC(nb) MOD 1000;
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
	   resultat := resultat || ' ' || varlet;
	END IF;
	resultat := LTRIM(resultat, ' ');



	---Traitement du 's' final pour vingt et cent et du 'de' pour million
	varlet := SUBSTR(resultat, -4);
	IF varlet IN ('cent', 'ingt') THEN
	   resultat := resultat || 's';
	END IF;




	---Indication de la devise
 	IF varlet IN ('lion', 'ions', 'iard', 'ards') THEN
 	   resultat := resultat || ' ' || separateurMillion || devise;
	ELSE
		resultat := resultat || ' ' || devise;
 	END IF;

	IF nb >= 2 THEN
	   resultat := resultat || 's';
	END IF;

	---Traitement des centimes
	varnum := TRUNC((nb - TRUNC(nb)) * 100);
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
		resultat := resultat || ' et ' || varlet || ' ' ||centimes;
		IF varnum > 1 THEN
		   resultat := resultat || 's';
		END IF;
	END IF;

	RETURN resultat;
END;


END;
/


GRANT EXECUTE ON  MARACUJA.NUMBER_TO_LETTRES TO JEFY_RECETTE;

----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
----------------------------------------------------------------------
