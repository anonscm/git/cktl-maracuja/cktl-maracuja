SET define OFF
SET scan OFF


CREATE OR REPLACE FORCE VIEW MARACUJA.ZLD_V_ECRITURE_INFOS
(ECD_ORDRE, ECR_SACD)
AS 
SELECT d.ecd_ordre , 'O' FROM ECRITURE_DETAIL d, GESTION g, GESTION_EXERCICE ge WHERE d.ges_code=g.ges_code AND g.ges_code=ge.ges_code AND ge.exe_ordre=d.EXE_ORDRE AND ge.PCO_NUM_185 IS NOT NULL
UNION ALL
SELECT d.ecd_ordre , 'N' FROM ECRITURE_DETAIL d, GESTION g, GESTION_EXERCICE ge  WHERE d.ges_code=g.ges_code AND g.ges_code=ge.ges_code AND ge.exe_ordre=d.EXE_ORDRE AND ge.PCO_NUM_185 IS NULL;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECRITURE_INFOS
(ECD_ORDRE, ECR_SACD)
AS 
SELECT d.ecd_ordre , DECODE(ge.pco_num_185 ,NULL,  'N', 'O') ecr_sacd 
FROM ECRITURE_DETAIL d, GESTION_EXERCICE ge WHERE d.EXE_ORDRE =ge.exe_ordre AND d.ges_code=ge.ges_code;


--------------------------
--------------------------
--------------------------

CREATE OR REPLACE FORCE VIEW MARACUJA.CFI_ECRITURES
(EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, ECR_SACD, 
 CREDIT, DEBIT)
AS 
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, p.pco_libelle, ei.ECR_SACD ,SUM(ed.ecd_credit) credit, 0
        FROM ECRITURE_DETAIL ed, ECRITURE e, (SELECT d.ecd_ordre , DECODE(ge.pco_num_185 ,NULL,  'N', 'O') ecr_sacd 
FROM ECRITURE_DETAIL d, GESTION_EXERCICE ge WHERE d.EXE_ORDRE =ge.exe_ordre AND d.ges_code=ge.ges_code ) ei, 
PLAN_COMPTABLE p
        WHERE ed.ecd_credit<>0
        AND ed.ecr_ordre = e.ecr_ordre
		AND ed.ECD_ORDRE = ei.ecd_ordre
				AND ed.pco_num = p.pco_num
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,p.pco_libelle, ei.ecr_sacd
UNION ALL
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, p.pco_libelle, ei.ECR_SACD, 0, SUM(ed.ecd_debit) debit
        FROM ECRITURE_DETAIL ed, ECRITURE e, (SELECT d.ecd_ordre , DECODE(ge.pco_num_185 ,NULL,  'N', 'O') ecr_sacd 
FROM ECRITURE_DETAIL d, GESTION_EXERCICE ge WHERE d.EXE_ORDRE =ge.exe_ordre AND d.ges_code=ge.ges_code ) ei, PLAN_COMPTABLE p
        WHERE ed.ecd_debit<>0
        AND ed.ecr_ordre = e.ecr_ordre
		AND ed.ECD_ORDRE = ei.ecd_ordre
		AND ed.pco_num = p.pco_num
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,p.pco_libelle, ei.ecr_sacd;


CREATE OR REPLACE FORCE VIEW MARACUJA.ZLD_CFI_ECRITURES
(EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, ECR_SACD, 
 CREDIT, DEBIT)
AS 
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, p.pco_libelle, ei.ECR_SACD ,SUM(ed.ecd_credit) credit, 0
        FROM ECRITURE_DETAIL ed, ECRITURE e, v_ECRITURE_INFOS ei, PLAN_COMPTABLE p
        WHERE ed.pco_num (+) = p.pco_num
        AND ed.ecd_sens = 'C'
        AND ed.ecr_ordre = e.ecr_ordre
		AND ed.ECD_ORDRE = ei.ecd_ordre
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,p.pco_libelle, ei.ecr_sacd
UNION ALL
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, p.pco_libelle, ei.ECR_SACD, 0, SUM(ed.ecd_debit) debit
        FROM ECRITURE_DETAIL ed, ECRITURE e, v_ECRITURE_INFOS ei, PLAN_COMPTABLE p
        WHERE ed.pco_num (+) = p.pco_num
        AND ed.ecd_sens = 'D'
        AND ed.ecr_ordre = e.ecr_ordre
		AND ed.ECD_ORDRE = ei.ecd_ordre
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,p.pco_libelle, ei.ecr_sacd;
        
/

CREATE OR REPLACE FORCE VIEW MARACUJA.ZLD_V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd,
         ECRITURE e
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
	 AND ecd.ecd_credit=0
	 AND ecd.ecd_debit<>0
     AND ecd.ecr_ordre = e.ecr_ordre	 
     AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
GROUP BY r.rec_id;
/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd,
         ECRITURE e
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
	 AND ecd.ecd_credit=0
	 AND ecd.ecd_debit<>0
     AND ecd.ecr_ordre = e.ecr_ordre	 
     AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
	 AND ecd.exe_ordre = (SELECT exe_ordre FROM exercice WHERE exe_stat='O')
GROUP BY r.rec_id
/
        
        
        
        
ALTER TABLE ODPAIEM_DETAIL_ECRITURE ADD (
  CONSTRAINT FK_ODPAIEM_DETAIL_ECRITURE_ODP FOREIGN KEY (ODP_ORDRE) 
    REFERENCES ORDRE_DE_PAIEMENT (ODP_ORDRE)
    DEFERRABLE INITIALLY deferred);      
    
ALTER TABLE ODPAIEMENT_BROUILLARD ADD (
  CONSTRAINT FK_ODPAIEMENT_BROUILLARD_ODP FOREIGN KEY (ODP_ORDRE) 
    REFERENCES ORDRE_DE_PAIEMENT (ODP_ORDRE)
    DEFERRABLE INITIALLY deferred);      
    