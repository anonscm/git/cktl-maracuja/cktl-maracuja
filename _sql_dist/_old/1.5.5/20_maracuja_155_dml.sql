SET define OFF;

BEGIN


	INSERT INTO PARAMETRE ( EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_ORDRE,
	PAR_VALUE ) SELECT 2007, 'Moyen de paiement par numeraire (vider si non permis) - relances', 'REGLEMENT_NUMERAIRE', maracuja.parametre_seq.NEXTVAL,
	PAR_VALUE FROM MARACUJA.PARAMETRE WHERE EXE_ORDRE = 2007 AND par_key = 'ABR_RECETTE_MP_NUMERAIRE' AND NOT EXISTS (SELECT * FROM maracuja.PARAMETRE WHERE exe_ordre=2007 AND par_key='REGLEMENT_NUMERAIRE');
	 
	 INSERT INTO PARAMETRE ( EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_ORDRE,
	PAR_VALUE ) SELECT 2007, 'Moyen de paiement par carte bancaire (vider si non permis) - relances', 'REGLEMENT_CARTE', maracuja.parametre_seq.NEXTVAL,
	PAR_VALUE FROM MARACUJA.PARAMETRE WHERE EXE_ORDRE = 2007 AND par_key = 'ABR_RECETTE_MP_CARTE' AND NOT EXISTS (SELECT * FROM maracuja.PARAMETRE WHERE exe_ordre=2007 AND par_key='REGLEMENT_CARTE');
	
	INSERT INTO PARAMETRE ( EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_ORDRE,
	PAR_VALUE ) VALUES (2006, 'Code de nomenclature comptable utilisee pour les remontees infocentre (pour M9-3, code=09, pour M9-1 code=02)', 'EPN_CODE_NOMENCLATURE', maracuja.parametre_seq.NEXTVAL,'09');

	INSERT INTO PARAMETRE ( EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_ORDRE,
	PAR_VALUE ) VALUES (2007, 'Code de nomenclature comptable utilisee pour les remontees infocentre (pour M9-3, code=09, pour M9-1 code=02)', 'EPN_CODE_NOMENCLATURE', maracuja.parametre_seq.NEXTVAL,'09');

	COMMIT;

	
	INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
	TYAV_COMMENT ) VALUES ( 
	jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.5.5',  SYSDATE, '');
	COMMIT; 	
END;



