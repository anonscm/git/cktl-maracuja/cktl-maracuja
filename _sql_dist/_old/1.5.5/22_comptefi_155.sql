SET define OFF;
SET scan OFF;

-----------------------------------------------------------------------
-----------------------------------------------------------------------
-----------------------------------------------------------------------

   
update COMPTEFI.SIG set commentaire = 'VAL' where groupe1 like 'Exc�dent/Insuffisance brut(e) d''exploitation' and sig_libelle = 'Valeur ajout�e';
update COMPTEFI.SIG set commentaire = 'R_EXPL' where groupe1 = 'Resultat courant' and sig_libelle = 'Resultat d''exploitation';
update COMPTEFI.SIG set commentaire = 'R_EXCEP' where groupe1 = 'Resultat net' and sig_libelle = 'Resultat exceptionnel';
update COMPTEFI.SIG set commentaire = 'R_COUR' where groupe1 = 'Resultat net' and sig_libelle = 'Resultat courant';

-- Table CAF Tag du Rtat 

update COMPTEFI.CAF set formule = 'RTAT' where caf_libelle = 'R�sultat de l''exercice' and groupe1 is null;
commit;   