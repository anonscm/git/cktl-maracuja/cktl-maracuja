-- "Set scan off" turns off substitution variables. 
Set scan off; 


ALTER TABLE COMPTEFI.SIG ADD (SIG_MONTANT_ANT NUMBER(12,2),GROUPE_ANT VARCHAR2(50));

--------------------------
--------------------------
--------------------------

CREATE OR REPLACE PROCEDURE COMPTEFI.Prepare_Bilan (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS
  totalbrut NUMBER(12,2);
  totalamort NUMBER(12,2);
  totalnet NUMBER(12,2);
  totalnetant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  lib2 VARCHAR2(50);

  -- version du 12/03/2006

BEGIN

--*************** CREATION TABLE ACTIF *********************************
IF sacd = 'O' THEN
	DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
	DELETE BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- ACTIF IMMOBILISE ----
lib1:= 'ACTIF IMMOBILISE';
---- Immobilisations Incorporelles ---
lib2:= 'IMMOBILISATIONS INCORPORELLES';

-- Compte 201 ---
	totalbrut := Solde_Compte(exeordre, '201%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2801%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2831%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '201%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2801%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2831%', 'C', gescode, sacd));
	lib := 'Frais d''�tablissement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 203 ---
	totalbrut := Solde_Compte(exeordre, '203%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2803%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2833%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '203%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2803%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2833%', 'C', gescode, sacd));
	lib := 'Frais de recherche et de d�veloppement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 205 ---
	totalbrut := Solde_Compte(exeordre, '205%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2805%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2835%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2905%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '205%','D', gescode, sacd)
		- (Solde_Compte(exeordre-1, '2805%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2835%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2905%', 'C', gescode, sacd));
	lib := 'Concessions et droits similaires';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 206 ---
	totalbrut := Solde_Compte(exeordre, '206%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2906%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '206%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '2906%', 'C', gescode, sacd);
	lib := 'Droit au bail';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 208 ---
	totalbrut := Solde_Compte(exeordre, '208%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2808%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2908%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2838%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '208%','D', gescode, sacd)
		- (Solde_Compte(exeordre-1, '2808%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2908%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2838%', 'C', gescode, sacd));
	lib := 'Autres immobilisations incorporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 232 ---
	totalbrut := Solde_Compte(exeordre, '232%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2932%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '232%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '2932%', 'C', gescode, sacd);
	lib := 'Immobilisations incorporelles en cours';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

--- Compte 237 ---
	totalbrut := Solde_Compte(exeordre, '237%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '237%','D', gescode, sacd);
	lib := 'Avances et acomptes vers�s sur immobilisations incorporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);



---- Immobilisations Corporelles ---
lib2:= 'IMMOBILISATIONS CORPORELLES';

-- Compte 211 et 212 ---
	totalbrut := Solde_Compte(exeordre, '211%','D', gescode, sacd)
		+Solde_Compte(exeordre, '212%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2812%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2911%', 'C', 	gescode, sacd)
		+Solde_Compte(exeordre, '2842%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '211%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '212%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2812%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2911%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2842%', 'C', gescode, sacd));
	lib := 'Terrains, agencements et am�nagements de terrain';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 213 et 214 ---
	totalbrut := Solde_Compte(exeordre, '213%','D', gescode, sacd)
		+Solde_Compte(exeordre, '214%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2813%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2814%', 'C', 	gescode, sacd)
		+Solde_Compte(exeordre, '2843%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2844%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '213%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '214%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2813%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2814%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2843%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2844%', 'C', gescode, sacd));
	lib := 'Constructions et constructions sur sol d''autrui';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 215 ---
	totalbrut := Solde_Compte(exeordre, '215%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2815%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2845%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '215%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2815%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2845%', 'C', gescode, sacd));
	lib := 'Installations techniques, mat�riels et outillage';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 216 ---
	totalbrut := Solde_Compte(exeordre, '216%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2816%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2846%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '216%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2816%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2846%', 'C', gescode, sacd));
	lib := 'Collections';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 218 ---
	totalbrut := Solde_Compte(exeordre, '218%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2818%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2848%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '218%','D', gescode, sacd)
		-(Solde_Compte(exeordre-1, '2818%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2848%', 'C', gescode, sacd));
	lib := 'Autres immobilisations corporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 231 ---
	totalbrut := Solde_Compte(exeordre, '231%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2931%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '231%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '2931%', 'C', gescode, sacd);
	lib := 'Immobilisations corporelles en cours';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 238 ---
	totalbrut := Solde_Compte(exeordre, '238%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '238%','D', gescode, sacd);
	lib := 'Avances et acomptes vers�s sur commandes d''immobilisation corporelles';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Immobilisations Financi�res ---
lib2:= 'IMMOBILISATIONS FINANCIERES';

-- Compte 261 et 266 ---
	totalbrut := Solde_Compte(exeordre, '261%','D', gescode, sacd)
		+Solde_Compte(exeordre, '266%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2961%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2966%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '261%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '266%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2961%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2966%', 'C', gescode, sacd));
	lib := 'Participations';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 267 et 268 ---
	totalbrut := Solde_Compte(exeordre, '267%','D', gescode, sacd)
		+Solde_Compte(exeordre, '268%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2967%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2968%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '267%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '268%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2967%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2968%', 'C', gescode, sacd));
	lib := 'Cr�ances rattach�es � des participations';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 271 et 272 ---
	totalbrut := Solde_Compte(exeordre, '271%','D', gescode, sacd)
		+Solde_Compte(exeordre, '272%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2971%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '2972%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '271%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '272%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2971%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2972%', 'C', gescode, sacd));
	lib := 'Autres titres immobilis�s';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 274 --
	totalbrut := Solde_Compte(exeordre, '274%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2974%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '274%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '2974%', 'C', gescode, sacd);
	lib := 'Pr�ts';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 275 et 2761  --
totalbrut := Solde_Compte(exeordre, '275%','D', gescode, sacd)
	+Solde_Compte(exeordre, '2761%','D', gescode, sacd)
	+Solde_Compte(exeordre, '2768%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '2975%', 'C', gescode, sacd)
	+Solde_Compte(exeordre, '2976%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '275%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '2761%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '2768%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '2975%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '2976%', 'C', gescode, sacd));
	lib := 'Autres immobilisations financi�res';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


----- ACTIF CIRCULANT ----
lib1:= 'ACTIF CIRCULANT';
---- Stocks ---
lib2:= 'STOCKS ET EN-COURS';

-- Compte 31 et 32---
	totalbrut := Solde_Compte(exeordre, '31%','D', gescode, sacd)
		+Solde_Compte(exeordre, '32%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '391%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '392%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '31%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '32%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '391%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '392%', 'C', gescode, sacd));
	lib := 'Mati�res premi�res et autres approvisionnements';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 33 et 34 ---
	totalbrut := Solde_Compte(exeordre, '33%','D', gescode, sacd)
		+Solde_Compte(exeordre, '34%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '393%', 'C', gescode, sacd)
		+Solde_Compte(exeordre, '394%', 'C', 	gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '33%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '34%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '393%', 'C', gescode, sacd)
		+Solde_Compte(exeordre-1, '394%', 'C', gescode, sacd));
	lib := 'En-cours de production de biens et de services';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 35 ---
	totalbrut := Solde_Compte(exeordre, '35%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '395%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '35%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '395%', 'C', gescode, sacd);
	lib := 'Produits interm�diaires et finis';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 37 ---
	totalbrut := Solde_Compte(exeordre, '37%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '397%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '37%','D', gescode, sacd)
		-Solde_Compte(exeordre-1, '397%', 'C', gescode, sacd);
	lib := 'Marchandises';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Avances et acomptes vers�s sur commande ---
lib2:= 'AVANCES ET ACOMPTES';
-- Compte 4091 et 4092 ---
	totalbrut := Solde_Compte(exeordre, '4091%','D', gescode, sacd)
		+Solde_Compte(exeordre, '4092%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4091%','D', gescode, sacd)
		+Solde_Compte(exeordre-1, '4092%','D', gescode, sacd);
	lib := 'Avances et acomptes vers�s sur commandes';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Cr�ances exploitations ---
lib2:= 'CREANCES D''EXPLOITATION';

-- Compte 411, 412, 413, 416 et 418 ---
	totalbrut := Solde_Compte(exeordre, '411%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '412%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '413%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '416%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '418%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '491%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '411%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '412%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '413%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '416%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '418%','D', gescode, sacd);
	lib := 'Cr�ances clients et comptes rattach�s';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte autres ---
	totalbrut := Solde_Compte(exeordre, '4096%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4098%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '425%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4287%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4387%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4417%','D', gescode, sacd)

		+ Solde_Compte(exeordre, '443%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4487%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4684%','D', gescode, sacd)
		+ (Solde_Compte_Ext(exeordre, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
		- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
--		+ Solde_Compte(exeordre, '472%','D', gescode, sacd)
--		--- Solde_Compte(exeordre, '4729%', 'C', gescode, sacd))
		+ Solde_Compte(exeordre, '4735%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '478%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4096%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4098%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '425%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4287%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4387%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4417%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '443%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4487%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4684%','D', gescode, sacd)
		+ (Solde_Compte_Ext(exeordre-1, '(pco_num like ''472%'' and pco_num not like ''4729%'')', 'D', gescode, sacd, 'MARACUJA.cfi_ecritures')
		- Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd))
--		+ Solde_Compte(exeordre-1, '472%','D', gescode, sacd)
		---Solde_Compte(exeordre-1, '4729%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4735%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '478%','D', gescode, sacd);
	lib := 'Autres cr�ances d''exploitation';
	dbms_output.put_line(lib||' '||totalbrut);
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Cr�ances diverses ---
lib2:= 'CREANCES DIVERSES';

-- Compte TVA ---
	totalbrut := Solde_Compte(exeordre, '4456%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '44581%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '44583%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '4456%','D', gescode, sacd)+Solde_Compte(exeordre-1, '44581%','D', gescode, sacd)+Solde_Compte(exeordre-1, '44583%','D', gescode, sacd);
	lib := 'TVA';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Comptes Autres --- Sp�cial LR 4412 4413 !!
	totalbrut := 
		Solde_Compte(exeordre, '429%','D', gescode, sacd)  
		+ Solde_Compte(exeordre, '4411%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4412%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4413%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4418%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '444%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '462%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '463%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '465%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '467%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '4687%','D', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '495%', 'C', gescode, sacd)
	 	+ Solde_Compte(exeordre, '496%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (
		Solde_Compte(exeordre-1, '429%','D', gescode, sacd)  
		+ Solde_Compte(exeordre-1, '4411%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4418%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '444%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '462%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '463%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '465%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '467%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4687%','D', gescode, sacd))
		-(Solde_Compte(exeordre-1, '495%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '496%', 'C', gescode, sacd));
	lib := 'Autres cr�ances diverses';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

---- Tr�sorerie ---
lib2:= 'TRESORERIE';

-- Compte 50---
	totalbrut := Solde_Compte(exeordre, '50%','D', gescode, sacd)
		- Solde_Compte(exeordre, '509%', 'C', gescode, sacd);
	totalamort := Solde_Compte(exeordre, '590%', 'C', gescode, sacd);
	totalnet := totalbrut - totalamort;
	totalnetant := (Solde_Compte(exeordre-1, '50%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '509%', 'C', gescode, sacd))
		- Solde_Compte(exeordre-1, '590%', 'C', gescode, sacd);
	lib := 'Valeurs mobili�res de placement';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte Disponibilit�s ---
	totalbrut := Solde_Compte(exeordre, '51%','D', gescode, sacd)
		+ Solde_Compte(exeordre, '53%', 'D', gescode, sacd)
		+ Solde_Compte(exeordre, '54%','D', gescode, sacd)
		- Solde_Compte(exeordre, '51%','C', gescode, sacd)
		- Solde_Compte(exeordre, '54%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '185%','D', gescode, sacd)
		- Solde_Compte(exeordre, '185%', 'C', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '51%','D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '53%', 'D', gescode, sacd)
		+ Solde_Compte(exeordre-1, '54%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '51%','C', gescode, sacd)
		- Solde_Compte(exeordre-1, '54%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '185%','D', gescode, sacd)
		- Solde_Compte(exeordre-1, '185%', 'C', gescode, sacd);
	lib:= 'Disponibilit�s';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 486 ---
	totalbrut := Solde_Compte(exeordre, '486%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '486%','D', gescode, sacd);
	lib := 'Charges constat�es d''avance';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '';

-- Compte 481---
	totalbrut := Solde_Compte(exeordre, '481%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '481%','D', gescode, sacd);
	lib := 'Charges � r�partir sur plusieurs exercices';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);

-- Compte 476 ---
	totalbrut := Solde_Compte(exeordre, '476%','D', gescode, sacd);
	totalamort := 0;
	totalnet := totalbrut - totalamort;
	totalnetant := Solde_Compte(exeordre-1, '476%','D', gescode, sacd);
	lib := 'Diff�rences de conversion sur op�rations en devises';
	INSERT INTO BILAN_ACTIF VALUES (seq_bilan_actif.NEXTVAL,lib1, lib2, lib, totalbrut, totalamort, totalnet, totalnetant, gescode,exeordre);


--*************** CREATION TABLE PASSIF *********************************
IF sacd = 'O' THEN
	DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
	DELETE BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;


----- CAPITAUX PROPRES ----
lib1:= 'CAPITAUX PROPRES';
---- Capital---
lib2:= 'CAPITAL ET RESERVES';

-- Compte 1021 ---
	totalnet := Solde_Compte(exeordre, '1021%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1021%','C', gescode, sacd);
	lib := 'Dotation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1022 ---
	totalnet := Solde_Compte(exeordre, '1022%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1022%','C', gescode, sacd);
	lib := 'Compl�ment de dotation (Etat)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1023 ---
	totalnet := Solde_Compte(exeordre, '1023%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1023%','C', gescode, sacd);
	lib := 'Compl�ment de dotation (autres organismes)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1027 ---
	totalnet := Solde_Compte(exeordre, '1027%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1027%','C', gescode, sacd);
	lib := 'Affectation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 103 ---
	totalnet := Solde_Compte(exeordre, '103%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '103%','C', gescode, sacd);
	lib := 'Biens remis en pleine propri�t� aux �tablissements';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 105 ---
	totalnet := Solde_Compte(exeordre, '105%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '105%','C', gescode, sacd);
	lib := 'Ecarts de r��valuation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1068 ---
	totalnet := Solde_Compte(exeordre, '1068%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1068%','C', gescode, sacd);
	lib := 'R�serves';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 1069 ---
	totalnet := Solde_Compte(exeordre, '1069%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '1069%','D', gescode, sacd);
	lib := 'D�pr�ciation de l''actif';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, -totalnet, -totalnetant, gescode,exeordre);

-- Compte 110 ou 119 ---
	totalnet := Solde_Compte(exeordre, '110%','C', gescode, sacd)
		- Solde_Compte(exeordre,'119%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '110%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'119%','D', gescode, sacd);
	lib := 'Report � nouveau';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 120 ou 129 ---
	totalnet := Solde_Compte(exeordre, '120%','C', gescode, sacd)
		- Solde_Compte(exeordre,'129%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '120%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'129%','D', gescode, sacd);
	lib := 'R�sultat de l''exercice (b�n�fice ou perte)';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

-- Compte 13 --- Sp�cial LR 130 !
	totalnet := Solde_Compte(exeordre, '130%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre, '131%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '138%','C', gescode, sacd)
		- Solde_Compte(exeordre,'139%','D', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '131%','C', gescode, sacd)
	 	+ Solde_Compte(exeordre-1, '138%','C', gescode, sacd)
		- Solde_Compte(exeordre-1,'139%','D', gescode, sacd);
	lib := 'Subventions d''investissement';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode, exeordre);

----- PROVISIONS POUR RISQUES ET CHARGES ----
lib1:= 'PROVISIONS POUR RISQUES ET CHARGES';
lib2:= '  ';

-- Compte 151 ---
	totalnet := Solde_Compte(exeordre, '151%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '151%','C', gescode, sacd);
	lib := 'Provisions pour risques';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 157 et 158 ---
	totalnet := Solde_Compte(exeordre, '157%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '158%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '157%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '158%','C', gescode, sacd);
	lib := 'Provisions pour charges';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- DETTES ----
	lib1:= 'DETTES';
---- DETTES FINANCIERES ---
lib2:= 'DETTES FINANCIERES';

-- Compte Emprunts etab cr�dits ---
	totalnet := Solde_Compte(exeordre, '164%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '164%','C', gescode, sacd);
	lib := 'Emprunts aupr�s des �tablissements de cr�dit';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres emprunts ---
	totalnet := Solde_Compte(exeordre, '165%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '167%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '168%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '17%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '165%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '167%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '168%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '17%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','C', gescode, sacd);
	lib := 'Emprunts divers';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 419 ---
	totalnet := Solde_Compte(exeordre, '4191%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4192%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '4191%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4192%','C', gescode, sacd);
	lib := 'Avances et acomptes re�us sur commandes';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

---- DETTES EXPLOITATION ---
lib2:= 'DETTES D''EXPLOITATION';

-- Compte Dettes fournisseurs ---
	totalnet := Solde_Compte(exeordre, '401%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '403%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4081%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4088%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '401%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '403%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4081%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4088%','C', gescode, sacd);
	lib := 'Fournisseurs et comptes rattach�s';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Dettes fiscales ---
	totalnet := Solde_Compte(exeordre,'421%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '422%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '427%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4282%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4286%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '431%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '437%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4382%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4386%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '443%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '444%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4452%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4455%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '44584%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '44587%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4457%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '447%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4482%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4486%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1,'421%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '422%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '427%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4282%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4286%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '431%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '437%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4382%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4386%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '443%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '444%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4452%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4455%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '44584%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '44587%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4457%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '447%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4482%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4486%','C', gescode, sacd);
	lib := 'Dettes fiscales et sociales';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte autres dettes ---
	totalnet := Solde_Compte(exeordre, '4196%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4197%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4198%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4682%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '471%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4731%','C', gescode, sacd)
		--+ Solde_Compte(exeordre, '4729%', 'C', gescode, sacd)
		+ Solde_Compte(exeordre, '478%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '4196%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4197%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4198%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4682%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '471%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4731%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '478%','C', gescode, sacd);
	lib := 'Autres dettes d''exploitation';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

--- DETTES diverses ---
lib2:= 'DETTES DIVERSES';

-- Compte Dettes sur immo ---
	totalnet := Solde_Compte(exeordre, '269%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '404%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '405%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4084%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '269%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '404%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '405%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4084%','C', gescode, sacd);
	lib := 'Dettes sur immobilisations et comptes rattach�s';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte Autres Dettes ---
	totalnet := Solde_Compte(exeordre, '429%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '45%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '464%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '466%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '467%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '4686%','C', gescode, sacd)
		+ Solde_Compte(exeordre, '509%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '429%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '45%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '464%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '466%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '467%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '4686%','C', gescode, sacd)
		+ Solde_Compte(exeordre-1, '509%','C', gescode, sacd);
	lib := 'Autres dettes diverses';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

-- Compte 487 ---
	totalnet := Solde_Compte(exeordre, '487%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '487%','C', gescode, sacd);
	lib := 'Produits constat�s d''avance';
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

----- REGULARISATION ----
lib1:= 'COMPTES DE REGULARISATION';
lib2:= '  ';

-- Compte Emprunts etab cr�dits ---
	totalnet := Solde_Compte(exeordre, '477%','C', gescode, sacd);
	totalnetant := Solde_Compte(exeordre-1, '477%','C', gescode, sacd);
	lib := 'Diff�rences de conversion sur op�rations en devises' ;
	INSERT INTO BILAN_PASSIF VALUES (seq_bilan_passif.NEXTVAL,lib1, lib2, lib, totalnet, totalnetant, gescode,exeordre);

END;
/


--------------------------------------
--------------------------------------
--------------------------------------
--------------------------------------


-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PROCEDURE COMPTEFI.Prepare_Variation_Frng (exeordre NUMBER, gescode VARCHAR, sacd CHAR)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR(100);
  lib1 VARCHAR(50);
  total_emplois NUMBER(12,2);
  total_ressources NUMBER(12,2);
  total_emploisant number(12,2);
  total_ressourcesAnt NUMBER(12,2);
  formule VARCHAR(50);
  vcaf NUMBER(12,2);
  flag INTEGER;
  --resultat NUMBER(12,2);

BEGIN

	IF sacd = 'O' THEN
		DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = gescode;
	ELSE
		DELETE FRNG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
	END IF;

	total_emplois :=0 ;
	total_ressources :=0;
	total_emploisant :=0;
	total_ressourcesant :=0;
	totalant := 0;
	formule := '';
	lib1 := 'RESSOURCES';

	--**** RECUPERATION CAF ou IAF ********
	SELECT NVL(caf_montant,0), caf_libelle INTO total, lib FROM CAF
	WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N' AND formule = 'CAF';
	total_ressources := total_ressources+total;
	lib := SUBSTR(lib,3);

	SELECT COUNT(*) INTO flag FROM CAF
	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND methode_ebe = 'N' AND formule = 'CAF';
	IF (	flag >0 ) THEN
		SELECT NVL(caf_montant,0) INTO totalant FROM CAF
		WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND methode_ebe = 'N' AND formule = 'CAF';
		total_ressourcesant := total_ressourcesant+totalant;
	END IF;

	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);

	-- ********  Ressources ***********

	total := Execution_Bud(exeordre, '7751%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '7752%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '7756%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '274%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '275%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '276%', gescode, sacd, 'R');
	totalant := Execution_Bud(exeordre-1, '7751%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '7752%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '7756%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '274%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '275%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '276%', gescode, sacd, 'R');
	lib := '+ Cessions ou r�ductions de l''actif immobilis�';
	total_ressources := total_ressources+total;
	total_ressourcesant := total_ressourcesant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


	total := Execution_Bud(exeordre, '102%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '103%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '131%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '138%', gescode, sacd, 'R');
	totalant := Execution_Bud(exeordre-1, '102%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '103%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '131%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '138%', gescode, sacd, 'R');
	lib := '+ Augmentation des capitaux propres';
	total_ressources := total_ressources+total;
	total_ressourcesant := total_ressourcesant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);

	total := Execution_Bud(exeordre, '16%', gescode, sacd, 'R')
		- Execution_Bud(exeordre, '1688%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '17%', gescode, sacd, 'R');
	totalant := Execution_Bud(exeordre-1, '16%', gescode, sacd, 'R')
		- Execution_Bud(exeordre-1, '1688%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '17%', gescode, sacd, 'R');
	lib := '+ Augmentation des dettes financi�res';
	total_ressources := total_ressources+total;
	total_ressourcesant := total_ressourcesant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


	-- ********  Emplois ***********
	lib1 := 'EMPLOIS';

	total := Execution_Bud(exeordre, '20%', gescode, sacd, 'D')
		- Execution_Bud(exeordre, '20%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '21%', gescode, sacd, 'D')
		- Execution_Bud(exeordre, '21%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '23%', gescode, sacd, 'D')
		- Execution_Bud(exeordre, '23%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre, '26%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '27%', gescode, sacd, 'D');
	totalant :=Execution_Bud(exeordre-1, '20%', gescode, sacd, 'D')
		- Execution_Bud(exeordre-1, '20%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '21%', gescode, sacd, 'D')
		- Execution_Bud(exeordre-1, '21%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '23%', gescode, sacd, 'D')
		- Execution_Bud(exeordre-1, '23%', gescode, sacd, 'R')
		+ Execution_Bud(exeordre-1, '26%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre-1, '27%', gescode, sacd, 'D');
	lib := '- Acquisition d''actifs immobilis�s';
	total_emplois := total_emplois+total;
	total_emploisant := total_emploisant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


	total := Solde_Compte(exeordre, '481%', 'D', gescode, sacd);
	totalant := Solde_Compte(exeordre-1, '481%', 'D', gescode, sacd);
	lib := '- Charges � r�partir sur plusieurs exercices';
	total_emplois := total_emplois+total;
	total_emploisant := total_emploisant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


	total := Execution_Bud(exeordre, '102%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '103%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '131%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '138%', gescode, sacd, 'D');
	totalant := Execution_Bud(exeordre-1, '102%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre-1, '103%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre-1, '131%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre-1, '138%', gescode, sacd, 'D');
	lib := '- R�duction de capitaux propres';
	total_emplois := total_emplois+total;
	total_emploisant := total_emploisant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);

	total := Execution_Bud(exeordre, '16%', gescode, sacd, 'D')
		- Execution_Bud(exeordre, '1688%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre, '17%', gescode, sacd, 'D');
	totalant := Execution_Bud(exeordre-1, '16%', gescode, sacd, 'D')
		- Execution_Bud(exeordre-1, '1688%', gescode, sacd, 'D')
		+ Execution_Bud(exeordre-1, '17%', gescode, sacd, 'D');
	lib := '- Remboursement de dettes financi�res';
	total_emplois := total_emplois+total;
	total_emploisant := total_emploisant+totalant;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);


	--- ************ Calcul du FRNG *****************
	total := total_ressources-total_emplois;
	totalant := total_ressourcesant-total_emploisant;
	lib1 := ' ';
	IF total >= 0 THEN
		lib := '= VARIATION DU FONDS DE ROULEMENT NET GLOBAL (ressource nette)';
	ELSE
		lib := '= VARIATION DU FONDS DE ROULEMENT NET GLOBAL (emploi net)';
	END IF;
	INSERT INTO FRNG VALUES (frng_seq.NEXTVAL, exeordre, gescode, lib1, lib, total, totalant, formule);



END;
/
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------

CREATE OR REPLACE PROCEDURE comptefi.prepare_SIG (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS

  -- version du 14/03/2007  
  
  montant NUMBER(12,2);
  montant_charges NUMBER(12,2);
  montant_produits NUMBER(12,2);
  montant_groupe NUMBER(12,2);
  -- MAJ SIG exercice ant�rieur 
  montant_ant NUMBER(12,2); 
  montant_charges_ant NUMBER(12,2); 
  montant_produits_ant NUMBER(12,2); 
  montant_groupe_ant NUMBER(12,2);


  va NUMBER(12,2);
  ebe NUMBER(12,2);
  resultat_exploitation NUMBER(12,2);
  resultat_courant NUMBER(12,2);
  resultat_exceptionnel NUMBER(12,2);
  resultat_net NUMBER(12,2);
  -- Calcul SIG exercice ant�rieur 
  va_ant NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat_exploitation_ant NUMBER(12,2);
  resultat_courant_ant NUMBER(12,2);
  resultat_exceptionnel_ant NUMBER(12,2);
  resultat_net_ant NUMBER(12,2);


  anneeExer NUMBER;

  pconum NUMBER;
  groupe1 VARCHAR(50);
  groupe2 VARCHAR(50);
  groupe_ant VARCHAR(50);

  lib VARCHAR(100);
  formule VARCHAR(1000);
  commentaire VARCHAR(1000);
  cpt NUMBER;

BEGIN

--*************** CREATION TABLE *********************************
IF sacd = 'O' THEN
 DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
 DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;



----- VALEUR AJOUTEE ----
   groupe1 := 'Valeur ajout�e';
   montant_charges := 0;
   montant_produits := 0;

   groupe2 := 'produits';

    lib := 'Vente et prestations de services (C.A.)';
    formule := 'SC(70+1870) - SD (709+18709)';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '70%,1870%', 'C', gescode, sacd)
		- Solde_Compte_ext_avt_S67(exeordre, '709%,18709%', 'D', gescode, sacd);
	montant_produits := montant_produits + montant;
	-- N-1 
	montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '70%,1870%', 'C', gescode, sacd)
		- Solde_Compte_ext_avt_S67(exeordre-1, '709%,18709%', 'D', gescode, sacd);
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production stock�e';
	formule := 'SC713+18713 - SD 713+18713';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '713%,18713%', 'C', gescode, sacd) 
		- Solde_Compte_ext_avt_S67(exeordre, '713%,18713%', 'D', gescode, sacd);
    montant_produits := montant_produits + montant;
	-- N-1 
	montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '713%,18713%', 'C', gescode, sacd) 
		- Solde_Compte_ext_avt_S67(exeordre-1, '713%,18713%', 'D', gescode, sacd);
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production immobilis�e';
    formule := 'SC72+1872';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '72%,1872%', 'C', gescode, sacd);
    montant_produits := montant_produits + montant;
	-- N-1 
	montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '72%,1872%', 'C', gescode, sacd);
    INSERT INTO SIG VALUES 
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire, montant_ant, groupe2);


   groupe2 := 'charges';

    lib := 'Achats';
    formule := 'SD(601+602+604+605+606+607+608)+SD603-SC603-SC609';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_ext_avt_S67(exeordre, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre, '609%,18609%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
	-- N-1 
	montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_ext_avt_S67(exeordre-1, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre-1, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre-1, '609%,18609%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES 
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Services exterieurs';
    formule := 'SD61 - SC619';
    commentaire := '';
    montant := Solde_Compte_ext_avt_S67(exeordre, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre, '619%,18619%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
	-- N-1 
	montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_ext_avt_S67(exeordre-1, '619%,18619%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Autres services exterieurs (sauf personnel exterieur)';
    formule := 'SD(62 - 621) - SC629';
    commentaire := '';
    montant := Solde_Compte_ext(exeordre, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_ext(exeordre, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_ext_avt_S67(exeordre, '629%,18629%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
	-- N-1 
	montant_ant := Solde_Compte_ext(exeordre-1, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_ext(exeordre-1, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_ext_avt_S67(exeordre-1, '629%,18629%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES 
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


    va := montant_produits - montant_charges;

-----------------------------------------
 groupe1 := 'Exc�dent/Insuffisance brut(e) d''exploitation';
  montant_charges := 0;
  montant_produits := 0;
  
  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
   WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'VAL';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO va_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'VAL';
   END IF;
   /*IF groupe_ant = 'charges' THEN
   		va_ant := -va_ant;
   END IF;*/
   
  IF (va>=0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Valeur ajout�e', va, ' ', 'VAL', va_ant, groupe_ant);
   montant_produits := va;
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Valeur ajout�e', -va, ' ', 'VAL', va_ant, groupe_ant);
   montant_charges := -va;
  END IF;

 groupe2 := 'produits';

  lib := 'Subventions d''exploitation d''etat';
  formule := 'SC741';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '741%,18741%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '741%,18741%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Subventions d''exploitation collectivit�s publiques et organismes internationaux';
  formule := 'SC744';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '744%,18744%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '744%,18744%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dons / legs et autres subventions d''exploitation';
  formule := 'SC(746+748)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '746%,748%,18746%,18748%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '746%,748%,18746%,18748%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


 groupe2 := 'charges';

  lib := 'Charges de personnel (y c. le personnel ext�rieur)';
  formule := 'SD(64+621)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '64%,1864%,621%,18621%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '64%,1864%,621%,18621%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Impots, taxes et versements assimil�s s/ r�mun�rations';
  formule := 'SD(631+632+633)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '631%,632%,633%,18631%,18632%,18633%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '631%,632%,633%,18631%,18632%,18633%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres Impots, taxes et versements assimil�s';
  formule := 'SD(635+637)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '635%,637%,18635%,18637%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '635%,637%,18635%,18637%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  ebe := montant_produits - montant_charges;
-----------------------------------------

 groupe1 := 'R�sultat d''exploitation';
  montant_charges := 0;
  montant_produits := 0;
  
  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'EBE';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO ebe_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'EBE';
   END IF;
   
  IF (ebe >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Exc�dent brut d''exploitation', 
		ebe, '','EBE', ebe_ant, groupe_ant);
   montant_produits := ebe;
  ELSE
   INSERT INTO SIG VALUES 
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Insuffisance brute d''exploitation', 
		-ebe, '','EBE', ebe_ant, groupe_ant);
   montant_charges := -ebe;
  END IF;



 groupe2 := 'produits';

  lib := 'Reprise sur amortissements et provisions';
  formule := 'SC781';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '781%,18781%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '781%,18781%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Transfert de charges d''exploitation';
  formule := 'SC791';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '791%,18791%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '791%,18791%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres produits';
  formule := 'SC75+SC187%';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '75%,1875%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '75%,1875%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Autres charges';
  formule := 'SD65+SD186%';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '65%,1865%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '65%,1865%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dotations aux amortissements et provisions';
  formule := 'SD681';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '681%,18681%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '681%,18681%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'produits';

  lib := 'Produits issus de la neutralisation des amortissements';
  formule := 'SC776';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '776%,18776%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '776%,18776%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  lib := 'Quote-part des subventions d''investissement vir�e au r�sultat de l''exercice';
  formule := 'SC777';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '777%,18777%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '777%,18777%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  resultat_exploitation := montant_produits - montant_charges;

----------------------------------------------------------------------

  groupe1 := 'Resultat courant';
  montant_charges := 0;
  montant_produits := 0;
  
  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXPL' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exploitation_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXPL';
   END IF;

  IF (resultat_exploitation >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat d''exploitation', 
		resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_produits := resultat_exploitation;
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat d''exploitation',
		 -resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_charges := -resultat_exploitation;
  END IF;




 groupe2 := 'produits';

  lib := 'Produits financiers';
  formule := 'SC(76+786+796)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '76%,786%,796%,1876%,18786%,18796%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '76%,786%,796%,1876%,18786%,18796%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges financi�res';
  formule := 'SD(66+686)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '66%,686%,1866%,18686%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '66%,686%,1866%,18686%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  resultat_courant := montant_produits - montant_charges;
----------------------------------------------------------------------------------

 groupe1 := 'Resultat exceptionnel';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits exceptionnels (sauf c/ 776 et 776)';
  formule := 'SC(77 - 776 -777) + SC(787 + 797)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', gescode, sacd) +
     Solde_Compte_ext_avt_S67(exeordre, '787%,797%,18787%,18797%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', gescode, sacd) +
     Solde_Compte_ext_avt_S67(exeordre-1, '787%,797%,18787%,18797%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges exceptionnelles';
  formule := 'SD(67+687)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '67%,687%,1867%,18687%', 'D', gescode, sacd);
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '67%,687%,1867%,18687%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 resultat_exceptionnel := montant_produits - montant_charges;

-------------------------------------------------------------------

 groupe1 := 'Resultat net';
  montant_charges := 0;
  montant_produits := 0;
  
  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_COUR' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_courant_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_COUR';
   END IF;

  IF (resultat_courant >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat courant', 
		resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES 
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat courant', 
		-resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  END IF;
  
  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXCEP' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exceptionnel_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXCEP';
   END IF;

  IF (resultat_exceptionnel >= 0) THEN
   INSERT INTO SIG VALUES 
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat exceptionnel',
		resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES 
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat exceptionnel', 
		-resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  END IF;


  resultat_net := resultat_courant + resultat_exceptionnel;
  resultat_net_ant := resultat_courant_ant + resultat_exceptionnel_ant;
--------------------------------------------------------------
 groupe1 := 'Resultat net apr�s impots';
  montant_charges := 0;
  montant_produits := 0;
  -- N-1 
  IF resultat_net_ant >=0 THEN
  	groupe_ant := 'produits';
  ELSE
  	groupe_ant := 'charges';
  END IF;	

  IF (resultat_net >= 0) THEN
   INSERT INTO SIG VALUES 
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat net',resultat_net, '','', resultat_net_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES 
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat net', -resultat_net, '','', resultat_net_ant, groupe_ant);
  END IF;

 groupe2 := 'charges';

  lib := 'Impots sur les b�n�fices et impots assimil�s';
  formule := 'SD(695+697+699)';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '695%,697%,699%,18695%,18697%,18699%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '695%,697%,699%,18695%,18697%,18699%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

---------------------------------------------------------------------

 groupe1 := 'Plus ou moins-value sur cession d''actif';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits des cessions d''�l�ments d''actif';
  formule := 'SC775';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '775%,18775%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '775%,18775%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Valeurs comptable des �l�ments d''actif c�d�s';
  formule := 'SD675';
  commentaire := '';
  montant := Solde_Compte_ext_avt_S67(exeordre, '675%,18675%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1 
  montant_ant := Solde_Compte_ext_avt_S67(exeordre-1, '675%,18675%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES 
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


END;
/

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

CREATE OR REPLACE PROCEDURE comptefi.PREPARE_DETERMINATION_CAF (exeordre NUMBER, gescode VARCHAR2, sacd CHAR, methodeEBE VARCHAR2)

IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib VARCHAR2(100);
  lib1 VARCHAR2(50);
  libant VARCHAR2(50);
  total_produits NUMBER(12,2);
  total_produits_ant NUMBER(12,2);
  total_charges NUMBER(12,2);
  total_charges_ant NUMBER(12,2);
  formule VARCHAR2(50);
  ebe NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat NUMBER(12,2);
  resultat_ant NUMBER(12,2);
  cpt NUMBER;
  
  -- version du 14/03/2007  

BEGIN

--*************** DETERMINATION A PARTIR DE EBE  *********************************
IF methodeEBE = 'O' THEN

	IF sacd = 'O' THEN
		DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'O';
	ELSE
		DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'O';
	END IF;

	total_produits :=0 ;
	total_charges :=0;
	total_produits_ant :=0 ;
	total_charges_ant :=0;
	formule := '';

	--**** RECUPERATION EBE *******
	SELECT NVL(sig_montant,0), groupe2, sig_libelle, NVL(sig_montant_ant,0), groupe_ant
	INTO ebe, lib1, lib, ebe_ant, libant FROM SIG
	WHERE gescode = ges_code AND exe_ordre = exeordre
	AND groupe1 = 'R�sultat d''exploitation' AND commentaire = 'EBE';
	IF lib1 = 'charges' THEN
		ebe := -ebe;
	END IF;
	IF libant = 'charges' THEN
		ebe_ant := -ebe_ant;
	END IF;
	
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, ebe, ebe_ant, formule);

	-- ********  Produits ***********
	lib1 := 'produits';

	total := resultat_compte(exeordre, '75%', gescode, sacd)
		+ resultat_compte(exeordre, '1875%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '75%', gescode, sacd)
		+ resultat_compte(exeordre-1, '1875%', gescode, sacd);
	lib := '+ Autres produits "encaissables" d''exploitation';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


	total := resultat_compte(exeordre, '791%', gescode, sacd)
		+ resultat_compte(exeordre, '18791%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '791%', gescode, sacd)
		+ resultat_compte(exeordre-1, '18791%', gescode, sacd);
	lib := '+ Transferts de charges';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	total := resultat_compte(exeordre, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+resultat_compte(exeordre, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '76%', gescode, sacd)+resultat_compte(exeordre, '796%', gescode, sacd)+ resultat_compte(exeordre-1, '1876%', gescode, sacd)+resultat_compte(exeordre, '18796%', gescode, sacd);
	lib := '+ Produits financiers "encaissables" (a)';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	total := resultat_compte(exeordre, '771%', gescode, sacd)+resultat_compte(exeordre, '778%', gescode, sacd)+resultat_compte(exeordre, '797%', gescode, sacd)+resultat_compte(exeordre, '18771%', gescode, sacd)+resultat_compte(exeordre, '18778%', gescode, sacd)+resultat_compte(exeordre, '18797%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '771%', gescode, sacd)+resultat_compte(exeordre-1, '778%', gescode, sacd)+resultat_compte(exeordre-1, '797%', gescode, sacd)+resultat_compte(exeordre-1, '18771%', gescode, sacd)+resultat_compte(exeordre-1, '18778%', gescode, sacd)+resultat_compte(exeordre-1, '18797%', gescode, sacd);
	lib := '+ Produits exceptionnels "encaissables" (b)';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	-- ********  charges ***********
	lib1 := 'charges';

	total := resultat_compte(exeordre, '65%', gescode, sacd)
		+ resultat_compte(exeordre, '1865%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '65%', gescode, sacd)
		+ resultat_compte(exeordre-1, '1865%', gescode, sacd);
	lib := '- Autres charges "d�caissables" d''exploitation';
	total_charges := total_charges+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '66%', gescode, sacd)+resultat_compte(exeordre, '1866%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '66%', gescode, sacd)+resultat_compte(exeordre-1, '1866%', gescode, sacd);
	lib := '- Charges financi�res "d�caissables" (c)';
	total_charges := total_charges+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '671%', gescode, sacd)+resultat_compte(exeordre, '678%', gescode, sacd)+resultat_compte(exeordre, '18671%', gescode, sacd)+resultat_compte(exeordre, '18678%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '671%', gescode, sacd)+resultat_compte(exeordre-1, '678%', gescode, sacd)+resultat_compte(exeordre-1, '18671%', gescode, sacd)+resultat_compte(exeordre-1, '18678%', gescode, sacd);
	lib := '- Charges exceptionnelles "d�caissables" (d)';
	total_charges := total_charges+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '695%', gescode, sacd)+resultat_compte(exeordre, '18695%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '695%', gescode, sacd)+resultat_compte(exeordre-1, '18695%', gescode, sacd);
	lib := '- Imp�ts sur les b�n�fices';
	total_charges := total_charges+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	--- ************ Calcul de la caf *****************
	total := ebe+total_produits-total_charges;
	SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
	IF (cpt > 0) THEN
		SELECT NVL(caf_montant,0) INTO totalant FROM CAF
		WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
	ELSE
		totalant := 0;
	END IF;	
		   
    
	IF total >= 0 THEN
		lib1:= 'produits';
		lib := '= CAPACITE D''AUTOFINANCEMENT';
	ELSE
		lib1:= 'charges';
		lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
	END IF;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


ELSE

	--********* D�termination � partir du r�sultat **********************

	IF sacd = 'O' THEN
		DELETE CAF WHERE exe_ordre = exeordre AND ges_code = gescode AND methode_ebe = 'N';
	ELSE
		DELETE CAF WHERE exe_ordre = exeordre AND ges_code = 'ETAB' AND methode_ebe = 'N';
	END IF;

	total_produits :=0 ;
	total_charges :=0;
	formule := '';

	--**** RECUPERATION RESULTAT ********
	totalant := 0;
	lib := 'R�sultat de l''exercice';
	IF sacd = 'O' THEN
		SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
		WHERE (pco_num = '120' OR pco_num = '129') AND ges_code = gescode AND exe_ordre = exeordre;
	ELSE
		SELECT SUM(credit)- SUM(debit) INTO resultat FROM maracuja.cfi_ecritures
		WHERE (pco_num = '120' OR pco_num = '129') AND ecr_sacd = 'N' AND exe_ordre = exeordre;
	END IF;
	-- N-1 
	SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'RTAT';
	IF (cpt > 0) THEN 
		SELECT NVL(caf_montant,0) INTO totalant FROM CAF
		WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'RTAT';
    ELSE
		totalant := 0;
    END IF;				
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, resultat, totalant, 'RTAT');

	-- ********  Charges ***********
	lib1 := 'charges';

	total := resultat_compte(exeordre, '681%', gescode, sacd)+resultat_compte(exeordre, '686%', gescode, sacd)+resultat_compte(exeordre, '687%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '681%', gescode, sacd)+resultat_compte(exeordre-1, '686%', gescode, sacd)+resultat_compte(exeordre-1, '687%', gescode, sacd);
	lib := '+ Dotations aux amortissements et provisions';
	total_charges := total_charges+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	total := resultat_compte(exeordre, '675%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
	lib := '+ Valeur comptable des �l�ments actifs c�d�s';
	total_charges := total_charges+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);

	-- ********  Produits ***********
	lib1 := 'produits';

	total := resultat_compte(exeordre, '781%', gescode, sacd)+resultat_compte(exeordre, '786%', gescode, sacd)+resultat_compte(exeordre, '787%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '781%', gescode, sacd)+resultat_compte(exeordre-1, '786%', gescode, sacd)+resultat_compte(exeordre-1, '787%', gescode, sacd);
	lib := '- Reprises sur amortissements et provisions';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '775%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
	lib := '- Produits de cessions des �l�ments actifs c�d�s';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '776%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
	lib := '- Produits issus de la neutralisation des amortissements';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	total := resultat_compte(exeordre, '777%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
	lib := '- Quote-part des subventions d''investissement vir�es au compte de r�sultat';
	total_produits := total_produits+total;
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, -total, -totalant, formule);

	--- ************ Calcul de la caf *****************
	total := resultat+total_charges-total_produits;
	totalant := 0;
	formule := 'CAF';
	IF total >= 0 THEN
		lib1:= 'produits';
		lib := '= CAPACITE D''AUTOFINANCEMENT';
	ELSE
		lib1:= 'charges';
		lib := '= INSUFFISANCE D''AUTOFINANCEMENT';
	END IF;
	
	-- N-1 
	SELECT COUNT(*) INTO cpt FROM CAF WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
	IF (cpt > 0) THEN
		SELECT NVL(caf_montant,0) INTO totalant FROM CAF
		WHERE ges_code = gescode AND exe_ordre = exeordre-1 AND formule = 'CAF';
	ELSE
		totalant := 0;
	END IF;		   
	INSERT INTO CAF VALUES (caf_seq.NEXTVAL, exeordre, gescode, methodeEBE, lib1, lib, total, totalant, formule);


END IF;

END;
/

