set define off;

CREATE OR REPLACE FORCE VIEW COMPTEFI.ZLD_V_ECR_NON_BUD_EXER
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, ECD_LIBELLE, 
 ECD_DEBIT, ECD_CREDIT, ECR_ORDRE, ECR_LIBELLE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num,
       ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle
  FROM maracuja.ecriture_detail ed,
       maracuja.ecriture e,
       maracuja.mandat_detail_ecriture mde,
       maracuja.titre_detail_ecriture tde
 WHERE ed.ecd_ordre = mde.ecd_ordre(+)
   AND ed.ecd_ordre = tde.ecd_ordre(+)
   AND mde.ecd_ordre IS NULL
   AND tde.ecd_ordre IS NULL
   AND ed.ecr_ordre = e.ecr_ordre
   AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
   AND e.tjo_ordre <> 2;
/

CREATE OR REPLACE FORCE VIEW V_ECR_NON_BUD_EXER
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, ECD_LIBELLE, 
 ECD_DEBIT, ECD_CREDIT, ECR_ORDRE, ECR_LIBELLE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num, 
       ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle 
  FROM maracuja.ecriture_detail ed, 
       maracuja.ecriture e, 
       maracuja.mandat_detail_ecriture mde, 
       maracuja.titre_detail_ecriture tde 
 WHERE ed.ecd_ordre = mde.ecd_ordre(+) 
   AND ed.ecd_ordre = tde.ecd_ordre(+) 
   AND (mde.ecd_ordre IS NULL) 
   AND tde.ecd_ordre IS NULL 
   AND ed.ecr_ordre = e.ecr_ordre 
   AND SUBSTR (e.ecr_etat, 1, 1) = 'V' 
   AND e.tjo_ordre <> 2 
UNION ALL 
SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num, 
       ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle 
  FROM maracuja.ecriture_detail ed, 
       maracuja.ecriture e, 
       maracuja.mandat_detail_ecriture mde, 
	   maracuja.mandat m
 WHERE ed.ecd_ordre = mde.ecd_ordre 
   AND mde.man_id = m.man_id 
   AND ed.ecr_ordre = e.ecr_ordre 
   AND SUBSTR (e.ecr_etat, 1, 1) = 'V' 
   AND e.tjo_ordre <> 2 
   AND m.pco_num <> ed.pco_num AND ed.pco_num<>'18'||m.pco_num 
UNION ALL 
SELECT ed.exe_ordre, e.ecr_date_saisie, ed.ges_code, ed.pco_num, 
       ed.ecd_libelle, ecd_debit, ecd_credit, ed.ecr_ordre, e.ecr_libelle 
  FROM maracuja.ecriture_detail ed, 
       maracuja.ecriture e, 
       maracuja.titre_detail_ecriture mde, 
	   maracuja.titre m 
 WHERE ed.ecd_ordre = mde.ecd_ordre 
   AND mde.tit_id = m.tit_id 
   AND ed.ecr_ordre = e.ecr_ordre 
   AND SUBSTR (e.ecr_etat, 1, 1) = 'V' 
   AND e.tjo_ordre <> 2 
   AND m.pco_num <> ed.pco_num AND ed.pco_num<>'18'||m.pco_num;
/
----------------------

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_TITRES_CREDITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 TIT_ID, TIT_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 TIT_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id, 
       m.tit_id, m.tit_numero, ed.ecd_libelle, ecd_montant, 
       (SIGN (ecd_montant)) * m.tit_tva tva, m.tit_etat, m.fou_ordre, 
       ed.ecr_ordre, ei.ecr_sacd, tde_origine 
  FROM maracuja.titre m, 
       maracuja.bordereau b, 
       maracuja.titre_detail_ecriture mde, 
       maracuja.ecriture_detail ed, 
       maracuja.v_ecriture_infos ei, 
       maracuja.ecriture e 
 WHERE m.bor_id = b.bor_id 
   AND m.tit_id = mde.tit_id 
   AND mde.ecd_ordre = ed.ecd_ordre 
   AND ed.ecd_ordre = ei.ecd_ordre 
   AND ed.ecr_ordre = e.ecr_ordre 
   --AND r.pco_num_ancien = ed.pco_num 
   AND tde_origine IN ('VISA', 'REIMPUTATION') 
   AND tit_etat IN ('VISE') 
   AND ecd_credit <> 0 
   AND tbo_ordre IN (7, 11, 200) 
   AND ABS (ecd_montant) = ABS (tit_ht);
   
/   