SET define OFF
SET scan OFF

CREATE OR REPLACE PROCEDURE comptefi.Prepare_Sig (exeordre NUMBER, gescode VARCHAR2, sacd CHAR)
IS

  -- version du 11/04/2007

  montant NUMBER(12,2);
  montant_charges NUMBER(12,2);
  montant_produits NUMBER(12,2);
  montant_groupe NUMBER(12,2);
  -- MAJ SIG exercice ant�rieur
  montant_ant NUMBER(12,2);
  montant_charges_ant NUMBER(12,2);
  montant_produits_ant NUMBER(12,2);
  montant_groupe_ant NUMBER(12,2);


  va NUMBER(12,2);
  ebe NUMBER(12,2);
  resultat_exploitation NUMBER(12,2);
  resultat_courant NUMBER(12,2);
  resultat_exceptionnel NUMBER(12,2);
  resultat_net NUMBER(12,2);
  -- Calcul SIG exercice ant�rieur
  va_ant NUMBER(12,2);
  ebe_ant NUMBER(12,2);
  resultat_exploitation_ant NUMBER(12,2);
  resultat_courant_ant NUMBER(12,2);
  resultat_exceptionnel_ant NUMBER(12,2);
  resultat_net_ant NUMBER(12,2);


  anneeExer NUMBER;

  pconum NUMBER;
  groupe1 VARCHAR(50);
  groupe2 VARCHAR(50);
  groupe_ant VARCHAR(50);

  lib VARCHAR(100);
  formule VARCHAR(1000);
  commentaire VARCHAR(1000);
  cpt NUMBER;

BEGIN

--*************** CREATION TABLE *********************************
IF sacd = 'O' THEN
 DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = gescode;
ELSE
 DELETE FROM SIG WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
END IF;



----- VALEUR AJOUTEE ----
   groupe1 := 'Valeur ajout�e';
   montant_charges := 0;
   montant_produits := 0;

   groupe2 := 'produits';

    lib := 'Vente et prestations de services (C.A.)';
    formule := 'SC(70+1870) - SD (709+18709)';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '70%,1870%', 'C', gescode, sacd)
		- Solde_Compte_Ext_Avt_S67(exeordre, '709%,18709%', 'D', gescode, sacd);
	montant_produits := montant_produits + montant;
	-- N-1
	montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '70%,1870%', 'C', gescode, sacd)
		- Solde_Compte_Ext_Avt_S67(exeordre-1, '709%,18709%', 'D', gescode, sacd);
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production stock�e';
	formule := 'SC713+18713 - SD 713+18713';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '713%,18713%', 'C', gescode, sacd)
		- Solde_Compte_Ext_Avt_S67(exeordre, '713%,18713%', 'D', gescode, sacd);
    montant_produits := montant_produits + montant;
	-- N-1
	montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '713%,18713%', 'C', gescode, sacd)
		- Solde_Compte_Ext_Avt_S67(exeordre-1, '713%,18713%', 'D', gescode, sacd);
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Production immobilis�e';
    formule := 'SC72+1872';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '72%,1872%', 'C', gescode, sacd);
    montant_produits := montant_produits + montant;
	-- N-1
	montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '72%,1872%', 'C', gescode, sacd);
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule,commentaire, montant_ant, groupe2);


   groupe2 := 'charges';

    lib := 'Achats';
    formule := 'SD(601+602+604+605+606+607+608)+SD603-SC603-SC609';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre, '609%,18609%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
	-- N-1
	montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '601%,602%,604%,605%,606%,607%,608%,18601%,18602%,18604%,18605%,18606%,18607%,18608%', 'D', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre-1, '603%,18603%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '603%,18603%', 'C', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '609%,18609%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Services exterieurs';
    formule := 'SD61 - SC619';
    commentaire := '';
    montant := Solde_Compte_Ext_Avt_S67(exeordre, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre, '619%,18619%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
	-- N-1
	montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '61%,1861%', 'D', gescode, sacd) -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '619%,18619%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

    lib := 'Autres services exterieurs (sauf personnel exterieur)';
    formule := 'SD(62 - 621) - SC629';
    commentaire := '';
    montant := Solde_Compte_Ext(exeordre, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_Ext(exeordre, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_Ext_Avt_S67(exeordre, '629%,18629%', 'C', gescode, sacd) ;
    montant_charges := montant_charges + montant;
	-- N-1
	montant_ant := Solde_Compte_Ext(exeordre-1, '(pco_num like ''62%'' and pco_num not like ''621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') + Solde_Compte_Ext(exeordre-1, '(pco_num like ''1862%'' and pco_num not like ''18621%'')', 'D', gescode, sacd, 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE') -
     Solde_Compte_Ext_Avt_S67(exeordre-1, '629%,18629%', 'C', gescode, sacd) ;
    INSERT INTO SIG VALUES
		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


    va := montant_produits - montant_charges;

-----------------------------------------
 groupe1 := 'Exc�dent/Insuffisance brut(e) d''exploitation';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
   WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'VAL';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO va_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'VAL';
   END IF;
   /*IF groupe_ant = 'charges' THEN
   		va_ant := -va_ant;
   END IF;*/

  IF (va>=0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Valeur ajout�e', va, ' ', 'VAL', va_ant, groupe_ant);
   montant_produits := va;
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Valeur ajout�e', -va, ' ', 'VAL', va_ant, groupe_ant);
   montant_charges := -va;
  END IF;

 groupe2 := 'produits';

  lib := 'Subventions d''exploitation d''etat';
  formule := 'SC741';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '741%,18741%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '741%,18741%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Subventions d''exploitation collectivit�s publiques et organismes internationaux';
  formule := 'SC744';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '744%,18744%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '744%,18744%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dons / legs et autres subventions d''exploitation';
  formule := 'SC(746+748)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '746%,748%,18746%,18748%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '746%,748%,18746%,18748%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


 groupe2 := 'charges';

  lib := 'Charges de personnel (y c. le personnel ext�rieur)';
  formule := 'SD(64+621)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '64%,1864%,621%,18621%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '64%,1864%,621%,18621%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Impots, taxes et versements assimil�s s/ r�mun�rations';
  formule := 'SD(631+632+633)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '631%,632%,633%,18631%,18632%,18633%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '631%,632%,633%,18631%,18632%,18633%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres Impots, taxes et versements assimil�s';
  formule := 'SD(635+637)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '635%,637%,18635%,18637%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '635%,637%,18635%,18637%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  ebe := montant_produits - montant_charges;
-----------------------------------------

 groupe1 := 'R�sultat d''exploitation';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'EBE';
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO ebe_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'EBE';
   END IF;

  IF (ebe >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Exc�dent brut d''exploitation',
		ebe, '','EBE', ebe_ant, groupe_ant);
   montant_produits := ebe;
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Insuffisance brute d''exploitation',
		-ebe, '','EBE', ebe_ant, groupe_ant);
   montant_charges := -ebe;
  END IF;



 groupe2 := 'produits';

  lib := 'Reprise sur amortissements et provisions';
  formule := 'SC781';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '781%,18781%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '781%,18781%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Transfert de charges d''exploitation';
  formule := 'SC791';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '791%,18791%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '791%,18791%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Autres produits';
  formule := 'SC75+SC187%';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '75%,1875%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '75%,1875%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Autres charges';
  formule := 'SD65+SD186%';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '65%,1865%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '65%,1865%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  lib := 'Dotations aux amortissements et provisions';
  formule := 'SD681';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '681%,18681%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '681%,18681%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'produits';

  lib := 'Produits issus de la neutralisation des amortissements';
  formule := 'SC776';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '776%,18776%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '776%,18776%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  lib := 'Quote-part des subventions d''investissement vir�e au r�sultat de l''exercice';
  formule := 'SC777';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '777%,18777%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '777%,18777%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

  resultat_exploitation := montant_produits - montant_charges;

----------------------------------------------------------------------

  groupe1 := 'Resultat courant';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXPL' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exploitation_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXPL';
   END IF;

  IF (resultat_exploitation >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat d''exploitation',
		resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_produits := resultat_exploitation;
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat d''exploitation',
		 -resultat_exploitation, '','R_EXPL', resultat_exploitation_ant, groupe_ant);
   montant_charges := -resultat_exploitation;
  END IF;




 groupe2 := 'produits';

  lib := 'Produits financiers';
  formule := 'SC(76+786+796)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '76%,786%,796%,1876%,18786%,18796%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '76%,786%,796%,1876%,18786%,18796%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges financi�res';
  formule := 'SD(66+686)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '66%,686%,1866%,18686%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '66%,686%,1866%,18686%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


  resultat_courant := montant_produits - montant_charges;
----------------------------------------------------------------------------------

 groupe1 := 'Resultat exceptionnel';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits exceptionnels (sauf c/ 776 et 777)';
  formule := 'SC(77 - 776 -777) + SC(787 + 797)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre, '787%,797%,18787%,18797%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '77,771%,772%,773%,774%,775%,778%,779%,1877,18771%,18772%,18773%,18774%,18775%,18778%,18779%', 'C', gescode, sacd) +
     Solde_Compte_Ext_Avt_S67(exeordre-1, '787%,797%,18787%,18797%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Charges exceptionnelles';
  formule := 'SD(67+687)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '67%,687%,1867%,18687%', 'D', gescode, sacd);
   montant_charges := montant_charges + montant;

  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '67%,687%,1867%,18687%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 resultat_exceptionnel := montant_produits - montant_charges;

-------------------------------------------------------------------

 groupe1 := 'Resultat net';
  montant_charges := 0;
  montant_produits := 0;

  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_COUR' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_courant_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_COUR';
   END IF;

  IF (resultat_courant >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat courant',
		resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat courant',
		-resultat_courant, '','R_COUR', resultat_courant_ant, groupe_ant);
  END IF;
  
  IF (groupe_ant = 'charges') THEN resultat_courant_ant := -resultat_courant_ant;
  END IF;

  -- N-1 
  SELECT COUNT(*) INTO cpt FROM SIG
  	WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXCEP' ;
   IF ( cpt > 0 ) THEN
       SELECT NVL(sig_montant,0), groupe2 INTO resultat_exceptionnel_ant, groupe_ant FROM SIG
       WHERE exe_ordre = exeordre-1 AND ges_code = gescode AND commentaire = 'R_EXCEP';
   END IF;

  IF (resultat_exceptionnel >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat exceptionnel',
		resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat exceptionnel',
		-resultat_exceptionnel, '','R_EXCEP', resultat_exceptionnel_ant, groupe_ant);
  END IF;

  IF (groupe_ant = 'charges') THEN resultat_exceptionnel_ant := -resultat_exceptionnel_ant;
  END IF;

  resultat_net := resultat_courant + resultat_exceptionnel;
  resultat_net_ant := resultat_courant_ant + resultat_exceptionnel_ant;
--------------------------------------------------------------
 groupe1 := 'Resultat net apr�s impots';
  montant_charges := 0;
  montant_produits := 0;
  -- N-1 
  IF resultat_net_ant >=0 THEN
  	groupe_ant := 'produits';
  ELSE
  	groupe_ant := 'charges';
  END IF;

  IF (resultat_net >= 0) THEN
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'produits', 'Resultat net',resultat_net, '','', abs(resultat_net_ant), groupe_ant);
  ELSE
   INSERT INTO SIG VALUES
   		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, 'charges', 'Resultat net', -resultat_net, '','', abs(resultat_net_ant), groupe_ant);
  END IF;

 groupe2 := 'charges';

  lib := 'Impots sur les b�n�fices et impots assimil�s';
  formule := 'SD(695+697+699)';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '695%,697%,699%,18695%,18697%,18699%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '695%,697%,699%,18695%,18697%,18699%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

---------------------------------------------------------------------

 groupe1 := 'Plus ou moins-value sur cession d''actif';

  montant_charges := 0;
  montant_produits := 0;

 groupe2 := 'produits';

  lib := 'Produits des cessions d''�l�ments d''actif';
  formule := 'SC775';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '775%,18775%', 'C', gescode, sacd);
  montant_produits := montant_produits + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '775%,18775%', 'C', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);

 groupe2 := 'charges';

  lib := 'Valeurs comptable des �l�ments d''actif c�d�s';
  formule := 'SD675';
  commentaire := '';
  montant := Solde_Compte_Ext_Avt_S67(exeordre, '675%,18675%', 'D', gescode, sacd);
  montant_charges := montant_charges + montant;
  -- N-1
  montant_ant := Solde_Compte_Ext_Avt_S67(exeordre-1, '675%,18675%', 'D', gescode, sacd);
  INSERT INTO SIG VALUES
  		(SIG_SEQ.NEXTVAL, exeordre, gescode, groupe1, groupe2, lib, montant, formule, commentaire, montant_ant, groupe2);


END;
/
