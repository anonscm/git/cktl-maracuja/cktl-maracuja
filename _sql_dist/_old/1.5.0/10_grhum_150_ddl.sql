SET define OFF
--
-- executer a partir de GRHUM ou d'un user DBA
-----------------------------------------------------------
-- 
-- Affectation des privileges + corrections d index
-- 
-----------------------------------------------------------


GRANT SELECT, references on JEFY_ADMIN.FONCTION to maracuja;
GRANT SELECT, references on JEFY_ADMIN.preference to maracuja;
GRANT SELECT, references on JEFY_ADMIN.UTILISATEUR to maracuja with grant option;
GRANT SELECT, references on JEFY_ADMIN.UTILISATEUR_FONCT to maracuja;
GRANT SELECT, references on JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE to maracuja;
GRANT SELECT, references on JEFY_ADMIN.UTILISATEUR_FONCT_gestion to maracuja;
GRANT SELECT, references on JEFY_ADMIN.UTILISATEUR_preference to maracuja;
GRANT SELECT, references on JEFY_ADMIN.TYPE_ETAT to maracuja;
GRANT SELECT, references on JEFY_ADMIN.TYPE_CREDIT to maracuja with grant option;
GRANT SELECT, references on JEFY_ADMIN.TVA to maracuja;
GRANT SELECT, references, UPDATE, insert ON JEFY_ADMIN.EXERCICE TO maracuja with grant option;
GRANT SELECT, references, UPDATE, insert ON JEFY_ADMIN.typap_version TO maracuja;
GRANT SELECT ON JEFY_ADMIN.typap_version_seq TO maracuja;

grant execute on jefy_admin.api_application to maracuja;
grant execute on jefy_admin.api_utilisateur to maracuja;
grant execute on jefy_admin.api_preference to maracuja;

GRANT SELECT, REFERENCES ON  jefy_admin.organ TO maracuja WITH GRANT OPTION;
GRANT SELECT ON  jefy_admin.v_organ TO maracuja WITH GRANT OPTION;
GRANT SELECT ON  JEFY_ADMIN.TYPE_CREDIT_DEP_EXECUTOIRE TO maracuja  WITH GRANT OPTION;

GRANT SELECT, references on grhum.fournis_ulr to maracuja;
grant select on grhum.adresse to maracuja with grant option;
GRANT SELECT, references on grhum.personne to maracuja;
GRANT SELECT, references on grhum.individu_ulr to maracuja;
GRANT SELECT, references on grhum.structure_ulr to maracuja;
grant SELECT ON  grhum.pays TO maracuja with grant option;
GRANT SELECT, references on grhum.ribfour_ulr to maracuja;
GRANT SELECT, references on grhum.banque to maracuja;

grant select on grhum.lolf_nomenclature to jefy with grant option;
grant select on grhum.lolf_nomenclature_recette to jefy with grant option;

grant select on jefy.parametres to maracuja with grant option;
grant select on jefy.destin to maracuja with grant option;
grant select on jefy.destin_rec to maracuja with grant option;
grant select on jefy05.destin to maracuja with grant option;
grant select on jefy05.destin_rec to maracuja with grant option;
grant select on jefy04.destin to maracuja with grant option;
grant select on jefy04.destin_rec to maracuja with grant option;
grant select on jefy.titre to maracuja with grant option;
grant select on jefy05.titre to maracuja with grant option;
grant select on jefy04.titre to maracuja with grant option;


grant select on jefy_budget.budget_param_editions to epn;
grant select on jefy_budget.budget_saisie_nature to epn;
grant select on jefy_budget.budget_saisie to epn; 

grant select on jefy_budget.budget_param_editions to comptefi;
grant select on jefy_budget.budget_saisie_nature to comptefi;
grant select on jefy_budget.budget_saisie to comptefi; 


GRANT SELECT, references on convention.CONVENTION_LIMITATIVE to maracuja;

GRANT SELECT ON INVENTAIRE.planco_amort TO maracuja;




----

-- Corrections des index erronnes sur comptefi
---------------------------------------------------

ALTER TABLE comptefi.BILAN_ACTIF DROP CONSTRAINT PK_BILAN_ACTIF ;
ALTER TABLE comptefi.BILAN_PASSIF DROP CONSTRAINT PK_BILAN_PASSIF ;
ALTER TABLE comptefi.CAF DROP CONSTRAINT CAF_PK ;
ALTER TABLE comptefi.FRNG DROP CONSTRAINT FRNG_PK ;
ALTER TABLE comptefi.CPTE_RTAT_CHARGES DROP CONSTRAINT PK_CR_CHARGES ;
ALTER TABLE comptefi.CPTE_RTAT_PRODUITS DROP CONSTRAINT PK_CR_PRODUITS ;


ALTER TABLE COMPTEFI.BILAN_ACTIF ADD (CONSTRAINT PK_BILAN_ACTIF PRIMARY KEY (BA_ORDRE));
ALTER TABLE COMPTEFI.BILAN_PASSIF ADD (CONSTRAINT PK_BILAN_PASSIF PRIMARY KEY (BP_ORDRE));
ALTER TABLE COMPTEFI.CAF ADD (  CONSTRAINT CAF_PK PRIMARY KEY (CAF_ORDRE));
ALTER TABLE COMPTEFI.FRNG ADD (  CONSTRAINT FRNG_PK PRIMARY KEY (FRNG_ORDRE));
ALTER TABLE COMPTEFI.CPTE_RTAT_CHARGES ADD (  CONSTRAINT PK_CR_CHARGES PRIMARY KEY (CRC_ORDRE));
ALTER TABLE COMPTEFI.CPTE_RTAT_PRODUITS ADD (  CONSTRAINT PK_CR_PRODUITS PRIMARY KEY (CRP_ORDRE));









