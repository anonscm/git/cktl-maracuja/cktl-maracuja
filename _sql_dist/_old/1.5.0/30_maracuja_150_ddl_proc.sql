SET define OFF
SET scan OFF; 
--
-- executer a partir de maracuja
-----------------------------------------------------------
--
-- Modifications de procedures pour compatibilite avec jefy 2007
--
-----------------------------------------------------------

CREATE OR REPLACE PACKAGE MARACUJA.bordereau_mandat_non_admis IS

PROCEDURE expedier_btmna (brjordre INTEGER,exeordre INTEGER);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_titre_non_admis IS

PROCEDURE expedier_bttna (brjordre INTEGER,exeordre INTEGER);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.gestionOrigine IS

/*
ORI_ENTITE  	  USER.TABLE  -
ORI_KEY_NAME 	  PRIMARY KEY -
ORI_LIBELLE 	  LIBELLE UTILISATEUR -
ORI_ORDRE 		  ID -
ORI_KEY_ENTITE	  VALEUR DE LA KEY -
TOP_ORDRE		  type d origine -
*/

FUNCTION traiter_orgordre (orgordre INTEGER) RETURN INTEGER;
FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION traiter_convordre (convordre INTEGER) RETURN  INTEGER;
FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
PROCEDURE maj_origine;

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.number_to_lettres IS


-- PUBLIC -
FUNCTION transformer (nb NUMBER) RETURN VARCHAR;


-- PRIVATE --
FUNCTION ZNb_En_Texte(nb  NUMBER) RETURN VARCHAR;
FUNCTION ZtraiteDixCent(nb NUMBER) RETURN VARCHAR;

END;
/





CREATE OR REPLACE PACKAGE BODY MARACUJA.Afaireaprestraitement IS

-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007


PROCEDURE apres_visa_bordereau (borid INTEGER)
IS
  ex INTEGER;
BEGIN
	 SELECT exe_ordre INTO ex FROM BORDEREAU WHERE bor_id=borid;
	 IF (ex < 2007) THEN
	 	prv_apres_visa_bordereau_2006(borid);
	 --ELSE
	 	 -- TODO si necessaire		 
	 END IF;	 
	 
END;
	 


PROCEDURE prv_apres_visa_bordereau_2006 (borid INTEGER)
IS

  tbotype  TYPE_BORDEREAU.tbo_type%TYPE;
  cpt INTEGER;
  lebordereau BORDEREAU%ROWTYPE;
  brjordre MANDAT.brj_ordre%TYPE;
  exeordre MANDAT.exe_ordre%TYPE;
  nbmandats INTEGER;
  nbtitres INTEGER;
  ex INTEGER;


  CURSOR c1 IS SELECT DISTINCT brj_ordre,exe_ordre FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
  CURSOR c2 IS SELECT DISTINCT brj_ordre,exe_ordre FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;

BEGIN
  SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

  SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;


  --raise_application_error (-20001, ''||ex|| '=' ||lebordereau.exe_ordre);


  IF lebordereau.exe_ordre=ex THEN

		  -- mettre a jour le bordereau de jefy
		  UPDATE jefy.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
		    WHERE bor_ordre=lebordereau.bor_ordre;



			SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

			IF (nbmandats > 0) THEN
			    -- mettre a jour les mandats VISE
			    UPDATE jefy.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

				-- expedier le bordereau de rejet de mandats
			  	OPEN C1;
			    LOOP
			      FETCH C1 INTO brjordre,exeordre;
			      EXIT WHEN c1%NOTFOUND;
			      Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C1;
			END IF;



			SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

			IF (nbtitres > 0) THEN
			    -- mettre a jour les titres VISE
			    UPDATE jefy.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

			    -- expedier le bordereau de rejet de titres
			  	OPEN C2;
			    LOOP
			      FETCH C2 INTO brjordre,exeordre;
			      EXIT WHEN c2%NOTFOUND;
			      Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C2;
			END IF;


			emarger_visa_bord_prelevement(borid);


  ELSE
  	  IF lebordereau.exe_ordre=2005 THEN

		  -- mettre a jour le bordereau de jefy
		  UPDATE jefy05.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
		    WHERE bor_ordre=lebordereau.bor_ordre;



			SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

			IF (nbmandats > 0) THEN
			    -- mettre a jour les mandats VISE
			    UPDATE jefy05.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

				-- expedier le bordereau de rejet de mandats
			  	OPEN C1;
			    LOOP
			      FETCH C1 INTO brjordre,exeordre;
			      EXIT WHEN c1%NOTFOUND;
			      Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C1;
			END IF;



			SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

			IF (nbtitres > 0) THEN
			    -- mettre a jour les titres VISE
			    UPDATE jefy05.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

			    -- expedier le bordereau de rejet de titres
			  	OPEN C2;
			    LOOP
			      FETCH C2 INTO brjordre,exeordre;
			      EXIT WHEN c2%NOTFOUND;
			      Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C2;
			END IF;
		 ELSE
		 	 RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');
	  END IF;
  END IF;

END ;



PROCEDURE apres_reimputation (reiordre INTEGER)
IS
ex INTEGER;
BEGIN

	 SELECT exe_ordre INTO ex FROM REIMPUTATION WHERE rei_ordre=reiordre;
	 IF (ex < 2007) THEN
	 	prv_apres_reimputation_2006 (reiordre);
	 --ELSE
	 	 -- TODO si necessaire		 
	 END IF;		 
	 
END;

PROCEDURE prv_apres_reimputation_2006 (reiordre INTEGER)
IS
cpt INTEGER;
manid INTEGER;
titid INTEGER;
manordre INTEGER;
titordre INTEGER;
pconumnouveau INTEGER;
depordre INTEGER;
exeordre INTEGER;
ex INTEGER;
BEGIN

	 SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

	-- mettre a jour le mandat ou le titre dans JEFY
	SELECT man_id, tit_id, pco_num_nouveau INTO manid, titid, pconumnouveau FROM REIMPUTATION WHERE rei_ordre=reiordre;

	IF manid IS NOT NULL THEN
	   SELECT man_ordre, MANDAT.exe_ordre INTO manordre, exeordre FROM MANDAT, BORDEREAU b WHERE man_id = manid AND MANDAT.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTME','BTMS','BTPI'));

	   IF exeordre=ex THEN
		   UPDATE jefy.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
		   UPDATE jefy.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
	   ELSE IF exeordre=2005 THEN
			   UPDATE jefy05.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
			   UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
		    END IF;
	   END IF;
	END IF;


	IF titid IS NOT NULL THEN
	   SELECT tit_ordre, TITRE.exe_ordre INTO titordre, exeordre FROM TITRE, BORDEREAU b WHERE tit_id = titid  AND TITRE.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTTE','BTPI'));


	   IF exeordre=ex THEN
		   UPDATE jefy.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
		   -- pour les ORV, des fois que...
		   SELECT dep_ordre INTO depordre FROM jefy.TITRE WHERE tit_ordre=titordre;
		   IF (depordre IS NOT NULL ) THEN
		   	  UPDATE jefy.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
		   END IF;

	   ELSE IF exeordre=2005 THEN
		   UPDATE jefy05.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
		   -- pour les ORV, des fois que...
		   SELECT dep_ordre INTO depordre FROM jefy05.TITRE WHERE tit_ordre=titordre;
		   IF (depordre IS NOT NULL ) THEN
		   	  UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
		   END IF;

		    END IF;
	   END IF;

	END IF;

END ;

PROCEDURE apres_paiement (paiordre INTEGER)
IS
cpt INTEGER;
BEGIN
	 		--SELECT 1 INTO  cpt FROM dual;
		 emarger_paiement(paiordre);

END ;


PROCEDURE emarger_paiement(paiordre INTEGER)
IS

CURSOR mandats (lepaiordre INTEGER ) IS
 SELECT * FROM MANDAT
 WHERE pai_ordre = lepaiordre;

CURSOR non_emarge_debit (lemanid INTEGER) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='D';

CURSOR non_emarge_credit_compte (lemanid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='C';

lemandat maracuja.MANDAT%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lepaiement maracuja.PAIEMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lepaiement FROM PAIEMENT
 WHERE pai_ordre = paiordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lepaiement.exe_ordre,
  3,
  lepaiement.UTL_ORDRE,
  lepaiement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les mandats du paiement
OPEN mandats(paiordre);
LOOP
FETCH mandats INTO lemandat;
EXIT WHEN mandats%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_debit (lemandat.man_id);
 LOOP
 FETCH non_emarge_debit INTO ecriture_debit;
 EXIT WHEN non_emarge_debit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_credit_compte (lemandat.man_id,ecriture_debit.pco_num);
 LOOP
 FETCH non_emarge_credit_compte INTO ecriture_credit;
 EXIT WHEN non_emarge_credit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_debit.ecd_ordre,
  ecriture_credit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  lemandat.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_credit_compte;

END LOOP;
CLOSE non_emarge_debit;

END LOOP;
CLOSE mandats;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;




PROCEDURE apres_recouvrement_releve(recoordre INTEGER)
IS
BEGIN
		 emarger_prelevement_releve(recoordre);
END ;

PROCEDURE emarger_prelevement_releve(recoordre INTEGER)
IS
cpt INTEGER;
BEGIN
	 -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
	 		SELECT 1 INTO  cpt FROM dual;
                        emarger_prelevement(recoordre);
END;


PROCEDURE apres_recouvrement (recoordre INTEGER)
IS
BEGIN
		 emarger_prelevement(recoordre);

END ;

PROCEDURE emarger_prelevement(recoordre INTEGER)
IS

CURSOR titres (lerecoordre INTEGER ) IS
 SELECT * FROM TITRE
 WHERE tit_id IN
 (SELECT tit_id FROM PRELEVEMENT p , ECHEANCIER e
 WHERE e.eche_echeancier_ordre  = p.eche_echeancier_ordre
 AND p.reco_ordre = lerecoordre)
 ;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lerecouvrement maracuja.RECOUVREMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lerecouvrement FROM RECOUVREMENT
 WHERE reco_ordre = recoordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lerecouvrement.exe_ordre,
  3,
  lerecouvrement.UTL_ORDRE,
  lerecouvrement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les titres du recrouvement
OPEN titres(recoordre);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;


PROCEDURE emarger_visa_bord_prelevement(borid INTEGER)
IS


CURSOR titres (leborid INTEGER ) IS
 SELECT * FROM TITRE
 WHERE bor_id = leborid;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lebordereau maracuja.BORDEREAU%ROWTYPE;
cpt INTEGER;
comordre INTEGER;
BEGIN

-- recup infos
 SELECT * INTO lebordereau FROM BORDEREAU
 WHERE bor_id = borid;
-- recup du com_ordre
SELECT com_ordre  INTO comordre
FROM GESTION WHERE ges_ordre = lebordereau.ges_code;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lebordereau.exe_ordre,
  3,
  lebordereau.UTL_ORDRE,
  comordre,
  0,
  'VALIDE'
  );

-- on fetch les titres du recouvrement
OPEN titres(borid);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;


FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER
IS
reponse INTEGER;
execredit EXERCICE.EXE_ORDRE%TYPE;
exedebit EXERCICE.EXE_ORDRE%TYPE;
BEGIN
-- init
reponse :=0;

SELECT exe_ordre INTO execredit FROM ECRITURE
WHERE ecr_ordre = ecrcredit;

SELECT exe_ordre INTO exedebit FROM ECRITURE
WHERE ecr_ordre = ecrdebit;

IF exedebit = execredit THEN
 RETURN 1;
ELSE
 RETURN 0;
END IF;


END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Journal IS
-- PUBLIC --

PROCEDURE validerEcriture (ecrordre INTEGER)
IS
cpt INTEGER;
BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL
WHERE ecr_ordre = ecrordre;

IF cpt >=2 THEN
Numerotationobject.numeroter_ecriture(ecrordre);
ELSE
 RAISE_APPLICATION_ERROR (-20001,'MAUVAIS FORMAT D ECRITURE !');
END IF;

END;

PROCEDURE annulerEcriture (ecrordre INTEGER)
IS
cpt INTEGER;
BEGIN
 SELECT ecr_numero INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
-- UPDATE ECRITURE SET ecr_etat = 'ANNULE'
-- WHERE ecr_ordre = ecrordre;
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
END IF;

END;

FUNCTION creerEcritureBE (

  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,

  )RETURN INTEGER
IS
  TJOORDRE              NUMBER;
BEGIN
-- recup du type_journal....
SELECT tjo_ordre INTO tjoordre FROM TYPE_JOURNAL
WHERE tjo_libelle ='JOURNAL BALANCE ENTREE';


RETURN Api_Plsql_Journal.creerEcriture
(
  COMORDRE              ,--        NOT NULL,
  ECRDATE               ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureExerciceType (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER,--        NOT NULL,
  TJOORDRE 				INTEGER
  )RETURN INTEGER
IS
  cpt              NUMBER;
BEGIN
-- recup du type_journal....
SELECT COUNT(*) INTO cpt FROM TYPE_JOURNAL
WHERE tjo_ordre =tjoordre;

IF cpt = 0 THEN
RAISE_APPLICATION_ERROR (-20001,'TYPE DE JOURNAL INCONNU !');
END IF;

RETURN Api_Plsql_Journal.creerEcriture
(
  COMORDRE              ,--        NOT NULL,
  ECRDATE               ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureCloture (brjordre INTEGER,exeordre INTEGER)RETURN INTEGER
IS
cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
RETURN cpt;
END;

FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;
BEGIN

RETURN Api_Plsql_Journal.creerEcriturePrivate(
  NULL              ,
  COMORDRE              ,--        NOT NULL,
  ECRDATE                ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
 NULL  ,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetailPrivate
(
  ECDCOMMENTAIRE    ,-- (200),
  ECDLIBELLE        ,-- (200),
  ECDMONTANT        ,-- (12,2) NOT NULL,
  ECDSECONDAIRE     ,-- (20),
  ECDSENS           ,-- (1)  NOT NULL,
  ECRORDRE          ,--        NOT NULL,
  EXEORDRE          ,--        NOT NULL,
  GESCODE           ,-- (10)  NOT NULL,
  PCONUM            -- (20)  NOT NULL
  );


END;







-- PRIVATE --
FUNCTION creerEcriturePrivate (
  BROORDRE              NUMBER,
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  --ECRNUMERO             NUMBER,-- (32),
 ECRNUMERO_BROUILLARD  NUMBER,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )
  RETURN INTEGER
IS
ECRORDRE INTEGER;

BEGIN
SELECT ecriture_seq.NEXTVAL INTO ECRORDRE FROM dual;

INSERT INTO ECRITURE VALUES
(BROORDRE,
COMORDRE,
SYSDATE,
ECRDATE,
'VALIDE',
ecrlibelle,
0,
ECRNUMERO_BROUILLARD,
ECRORDRE,
ECRLIBELLE,
EXEORDRE,
ORIORDRE,
TJOORDRE,
TOPORDRE,
UTLORDRE
);

RETURN ECRORDRE;
END;


FUNCTION creerEcritureDetailPrivate (
  ECDCOMMENTAIRE    VARCHAR2,-- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
ECDORDRE          NUMBER;
ECDCREDIT         NUMBER;
ECDDEBIT          NUMBER;
BEGIN

SELECT ecriture_detail_seq.NEXTVAL  INTO ECDORDRE FROM dual;

IF ECDSENS = 'C' THEN
ECDCREDIT := ecdmontant;
ECDDEBIT := 0;
ELSE
ECDCREDIT := 0;
ECDDEBIT := ecdmontant;
END IF;

dbms_output.put_line('INSERTION DETAIL : '||ecdmontant||' , SENS : '||ecdsens||' , PCO : '||pconum||' , ECR ORDRE : '||ecrordre);

INSERT INTO ECRITURE_DETAIL VALUES (
  ECDCOMMENTAIRE    ,-- (200),
  ECDCREDIT         ,-- (12,2),
  ECDDEBIT          ,-- (12,2),
  ECRORDRE          ,--        NOT NULL,
  ECDLIBELLE        ,-- (200),
  ECDMONTANT        ,-- (12,2) NOT NULL,
  ECDORDRE          ,--        NOT NULL,
  NULL         ,-- (200),
  ABS(ECDMONTANT)  ,-- (12,2) NOT NULL,
  ECDSECONDAIRE     ,-- (20),
  ECDSENS           ,-- (1)  NOT NULL,
  ECRORDRE          ,--        NOT NULL,
  EXEORDRE          ,--        NOT NULL,
  GESCODE           ,-- (10)  NOT NULL,
  PCONUM            -- (20)  NOT NULL
  );


RETURN ECDORDRE;
END;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Papaye IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA SALAIRES';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA SALAIRES' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 3 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

	SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentmab.mab_montant,
	NULL,
	currentmab.mab_sens,
	ecrordre,
	currentmab.ges_code,
	currentmab.pco_num
	);

	SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

	-- Insertion des mandat_detail_ecritures
	INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
	ecdordre,
	currentmab.exe_ordre,
	currentmab.man_id,
	SYSDATE,
	mdeordre,
	'VISA',
	oriordre
	);



	END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation LIKE 'RETENUES SALAIRES' AND bob_etat = 'VALIDE';

  -- Mise a jour de l'etat du bordereau (RETENUES OU PAIEMENT)
  IF (cpt > 0)
  THEN
  	  boretat := 'RETENUES';
  ELSE
  	  boretat := 'PAIEMENT';
  END IF;

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;
  UPDATE jefy.papaye_compta SET etat = boretat, jou_ordre_visa = ecrordre WHERE bor_ordre = borordre;

  passerecrituresacdbord(borid);

END IF;

END ;



-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;

-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)

END ;



-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD SALAIRES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD SALAIRES' AND bor_id = borid;

   IF (cpt > 0)
   THEN

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);
  UPDATE jefy.papaye_compta SET jou_ordre_sacd = ecrordre WHERE bor_ordre = borordre;

  END IF;

END ;

-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR)
IS
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)


cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;


END ;


-- permet de passer les  ecritures d opp ret d un bordereau de paye --
PROCEDURE passerEcritureOppRetBord (borid INTEGER, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'RETENUES SALAIRES' AND bob_etat = 'RETENUES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

bobetat  maracuja.BORDEREAU_BROUILLARD.bob_etat%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF (passer_ecritures = 'O')
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT DISTINCT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT DISTINCT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO bobetat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (bobetat = 'RETENUES')
THEN
	ecrordre := creerecriture(
	1,
	SYSDATE,
	'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;

    UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAIEMENT'
  WHERE bor_id = borid;

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;
  UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;

  END IF;

ELSE
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;
  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAIEMENT'
  WHERE bor_id = borid;

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;
  UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;

END IF;

END ;

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureOppRet (borid INTEGER)


END ;

-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT SALAIRES'
AND bob_etat = 'PAIEMENT';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT SALAIRES '||borlibelle_mois;

   IF (cpt = 0)
   THEN
	ecrordre := creerecriture(
	1,
	SYSDATE,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	exeordre,
	oriordre,
	14,
	6,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

  UPDATE jefy.papaye_compta SET etat = 'TERMINEE', jou_ordre_dsk = ecrordre WHERE mois = borlibelle_mois;
  END IF;

 ELSE

  UPDATE jefy.papaye_compta SET etat = 'TERMINEE' WHERE mois = borlibelle_mois;

END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bob_libelle2 = borlibelle_mois;

    UPDATE MANDAT
 SET
  man_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
	localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
	localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE 		--		INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Basculer_Be
IS
-- PUBLIC --
   PROCEDURE basculer_solde_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
--raise_application_error (-20001,' '||lespconums||','|| exeordre ||','||utlordre);
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
-- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

--raise_application_error (-20001,' '||exeordreprec);
         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lesdebits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND pco_num = pconumtmp;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lescredits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND pco_num = pconumtmp;

--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
--
--          --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);
--          SELECT ABS (lesdebits - lescredits)
--            INTO lesolde
--            FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lesdebits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ges_code = gescode
            AND pco_num = pconumtmp;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lescredits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ges_code = gescode
            AND pco_num = pconumtmp;

--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
--
--          SELECT ABS (lesdebits - lescredits)
--            INTO lesolde
--            FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retrait� les ecritures ! --
--         basculer_be.archiver_la_bascule (pconumtmp, gescode, utlordre);
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

-- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'D'
         AND pco_num = pconum;

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'C'
         AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;

         --raise_application_error (-20001,' '||lesdebits||' '||lesens||' '||lesens890||' '||lesolde);
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_solde_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'D'
         AND ges_code = gescode
         AND pco_num = pconum;

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'C'
         AND ges_code = gescode
         AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde);
      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, gescode, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'D'
--and ges_code = gescode
         AND pco_num = pconum;

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'C'
--and ges_code = gescode
         AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)            --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconum)             --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
	  priv_nettoie_ecriture(monecdordre);
   END;

----- **************
----- TODO
----- **************

   -------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
chaine := lespconums;
      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lesdebits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
--and ges_code = gescode
            AND pco_num = pconumtmp;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lescredits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
--and ges_code = gescode
            AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)          --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
	      priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
chaine := lespconums;
      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     ecdlibelle,
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp)        --PCONUM
                                                    );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'D'
         AND ges_code = gescode
         AND pco_num = pconum;

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL ecd, ECRITURE e
       WHERE e.ecr_ordre = ecd.ecr_ordre
         AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
         AND e.exe_ordre = exeordreprec
         AND ecd_ordre NOT IN (SELECT ecd_ordre
                                 FROM ECRITURE_DETAIL_BE_LOG)
         AND ecd_sens = 'C'
         AND ges_code = gescode
         AND pco_num = pconum;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum)           --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconum)            --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retrait� les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
chaine := lespconums;
      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lesdebits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ges_code = gescode
            AND pco_num = pconumtmp;

         SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
           INTO lescredits
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ges_code = gescode
            AND pco_num = pconumtmp;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
--          IF (lesdebits >= lescredits)
--          THEN
--             lesens := 'D';
--             lesens890 := 'C';
--          ELSE
--             lesens := 'C';
--             lesens890 := 'D';
--          END IF;
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retrait� les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
	  priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);

      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum)          --PCONUM
                                                     );
			priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retrait� les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
	  ecdordreOld	  ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
chaine := lespconums;
      SELECT top_ordre
        INTO topordre
        FROM TYPE_OPERATION
       WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      ecdlibelle,
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp)       --PCONUM
                                                     );
				priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retrait� les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
   END;






------------------------------------------
------------------------------------------
   -- permet de retrouver le compte dans Maracuja comme s'il n'avait pas ete transfere en BE
    PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER)
       IS
       cpt INTEGER;
       BEGIN

	   SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL_BE_LOG edb, ECRITURE_DETAIL ecd WHERE edb.ecd_ordre=ecd.ecd_ordre AND ecd.pco_num=pconum AND ecd.exe_ordre=exeordreold;
    	IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture de n'' a �t� transf�r�e en BE pour le compte '|| pconum);
    	END IF;

		DELETE FROM ECRITURE_DETAIL_BE_LOG WHERE ecd_ordre IN (SELECT ecd_ordre FROM ECRITURE_DETAIL WHERE pco_num=pconum AND exe_ordre=exeordreold);


    END;



-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-------------------------------------------------------------------------------
------------------------------------------------------------------------------

   -- PRIVATE --
   PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR)
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
   BEGIN
      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--       SELECT ABS (lesdebits - lescredits)
--         INTO lesolde
--         FROM DUAL;
--
 --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde||' '||ecrordre);
      IF lesolde != 0
      THEN
--           IF (lesdebits >= lescredits)
--           THEN
--              lesens890 := 'C';
--           ELSE
--              lesens890 := 'D';
--           END IF;

         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE SOLDE DU COMPTE '
                                                    || pconumlibelle,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    ecrordre,      --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
-- Rod : ne pas bloquer (sinon pas possible de reprendre detail equilibre)
--      ELSE
--          raise_application_error (-20001,
--                                      'OUPS PROBLEME ECRITURE NON fred!!! '
--                                   || lesdebits
--                                   || ' '
--                                   || lescredits
--                                  );
      END IF;
   END;

   PROCEDURE priv_archiver_la_bascule (
      pconum     VARCHAR,
      gescode    VARCHAR,
      utlordre   INTEGER,
      exeordre   INTEGER
   )
   IS
   BEGIN
      IF gescode IS NULL
      THEN
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND exe_ordre = exeordre
               AND pco_num = pconum;
      ELSE
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND pco_num = pconum
               AND exe_ordre = exeordre
               AND ges_code = gescode;
      END IF;
   END;

-- Renvoie l'identifiant de l'exercice precedent celui identifie par exeordre
   FUNCTION priv_get_exeordre_prec (exeordre INTEGER)
      RETURN INTEGER
   IS
      reponse           INTEGER;
      exeexerciceprec   EXERCICE.exe_exercice%TYPE;
   BEGIN
      SELECT exe_exercice - 1
        INTO exeexerciceprec
        FROM EXERCICE
       WHERE exe_ordre = exeordre;

      SELECT exe_ordre
        INTO reponse
        FROM EXERCICE
       WHERE exe_exercice = exeexerciceprec;

      RETURN reponse;
   END;


   PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER) IS
   BEGIN
   		DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = ecrOrdre AND ecd_montant=0 AND ecd_debit=0 AND ecd_credit=0;

   END;



   -- renvoie le compte de BE a utiliser (par ex renvoie 4011 pour 4012)
   -- ce compte est indique dans la table plan_comptable.
   -- si non precise, renvoie le compte passe en parametre
   FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR
   IS
   	 pconumnew PLAN_COMPTABLE.pco_num%TYPE;
   BEGIN
   		SELECT pco_compte_be INTO pconumnew FROM PLAN_COMPTABLE WHERE pco_num=pconumold;
		IF pconumnew IS NULL THEN
		   pconumnew := pconumold;
		END IF;
		RETURN pconumnew;
   END;




    -- Permet de suivre le detailecriture cree lors de la BE pour un titre.
    PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER)
       IS
       cpt INTEGER;
       infos TITRE_DETAIL_ECRITURE%ROWTYPE;
       BEGIN
       -- est un ecdordre pour un titre ?
          SELECT COUNT(*)
            INTO cpt
            FROM TITRE_DETAIL_ECRITURE
           WHERE ecd_ordre = ecdordreold;
    	IF cpt = 1 THEN
    		SELECT * INTO infos
       		FROM TITRE_DETAIL_ECRITURE
       	 WHERE ecd_ordre = ecdordreold;
      	INSERT INTO TITRE_DETAIL_ECRITURE (ECD_ORDRE, EXE_ORDRE, ORI_ORDRE, TDE_DATE, TDE_ORDRE, TDE_ORIGINE, TIT_ID, REC_ID)
			   VALUES (ecdordrenew, exeordrenew, infos.ORI_ORDRE, SYSDATE, titre_detail_ecriture_seq.NEXTVAL, infos.TDE_ORIGINE, infos.TIT_ID, infos.rec_id);
    	END IF;


    END;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Mandat IS
-- version 1.1.8 du 07/07/2005 - modifs sur le mod_ordre dans depense
-- version 1.1.7 du 31/05/2005 - modifs concernant les prestations internes
-- version 1.1.6 du 25/04/2005 - modifs concernant les prestations internes
-- version 1.1.8 du 15/09/2005 - modifs pour compatibilite avec nouveau gestion_exercice
-- version 1.1.9 du 26/09/2005 - modifs pour emargements semi-auto
-- version 1.5.0 du 08/12/2006 -- modifs pour attentes de paiement

PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	etat		jefy.bordero.bor_stat%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';
	   
	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;
	   
	 IF (ex <> ex2) THEN
	 	RAISE_APPLICATION_ERROR (-20001,'MAUVAIS EXERCICE');
	 END IF;


	SELECT bor_stat INTO etat FROM jefy.bordero
		   WHERE bor_ordre = borordre;

	IF etat = 'N' THEN

		-- est ce un bordereau de titre --
		SELECT COUNT(*) INTO cpt FROM jefy.MANDAT
		WHERE bor_ordre = borordre;

		IF cpt != 0 THEN

			tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

			-- le bordereau est il deja vis� ? --
			SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;

			-- l exercice est il encore ouvert ? --


			-- pas de raise mais pas de recup s il est deja dans maracuja --
			IF cpt = 0 THEN
				-- recup des infos --
				SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;

				-- recup de l agent de ce bordereau --
				/*select max(agt_ordre) into agtordre
				from jefy.facture
				where man_ordre =
				 ( select max(man_ordre)
				   from jefy.mandat
				   where bor_ordre =borordre
				  );

				-- recuperation du type de bordereau --
				select count(*) into cpt
				from old_agent_type_bord
				where agt_ordre = agtordre;

				if cpt <> 0 then
				 select tbo_ordre into tboordre
				 from old_agent_type_bord
				 where agt_ordre = agtordre;
				else
				 select tbo_ordre into tboordre
				 from type_bordereau
				 where tbo_ordre = (select par_value from parametre where par_key ='BTME GENERIQUE' and exe_ordre = exeordre);
				end if;
				*/


				-- creation du bor_id --
				SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

				-- recuperation de l utilisateur --
				/*select utl_ordre into utlordre
				from utilisateur
				where agt_ordre = agtordre;
				*/
				-- TEST  TEST  TEST  TEST  TEST
				SELECT 1 INTO utlordre
				FROM dual;
				-- TEST  TEST  TEST  TEST  TEST

				-- creation du bordereau --
				INSERT INTO BORDEREAU VALUES (
					NULL , 	 	 	      --BOR_DATE_VISA,
					'VALIDE',			  --BOR_ETAT,
					borid,				  --BOR_ID,
					leBordereau.bor_num,  --BOR_NUM,
					leBordereau.bor_ordre,--BOR_ORDRE,
					exeordre,	  		  --EXE_ORDRE,
					leBordereau.ges_code, --GES_CODE,
					tboordre,			  --TBO_ORDRE,
					utlordre,		 	  --UTL_ORDRE,
					NULL				  --UTL_ORDRE_VISA
					);

				--get_mandat_jefy_proc(exeordre,borordre,borid,utlordre,agtordre);
				Bordereau_Mandat.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

			END IF;
			--else
			-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
		END IF;
	END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	cpt 	 	INTEGER;

	lejefymandat jefy.MANDAT%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	manid 		 MANDAT.man_id%TYPE;

	MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
	MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
	ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
	PRESTID 	   MANDAT.PREST_ID%TYPE;
	TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
	VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
	modordre	   MANDAT.mod_ordre%TYPE;
	CURSOR lesJefyMandats IS
		SELECT *
			FROM jefy.MANDAT
			WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefyMandats;
	LOOP
		FETCH lesJefyMandats INTO lejefymandat;
			  EXIT WHEN lesJefyMandats%NOTFOUND;

		-- recuperation du ges_code --
		SELECT ges_code INTO gescode
			FROM BORDEREAU
			WHERE bor_id = borid;

		-- recuperations --
		--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
		MANORGINE_KEY:=NULL;

		--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
		MANORIGINE_LIB:=NULL;

		--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
		IF (lejefymandat.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
		END IF;

		--PRESTID : PRESTATION INTERNE --
		PRESTID :=NULL;

		--TORORDRE : ORIGINE DU MANDAT --
		TORORDRE := 1;

		--VIRORDRE --
		VIRORDRE := NULL;

		--MOD_ORDRE --
		SELECT mod_ordre INTO modordre
		FROM MODE_PAIEMENT
		WHERE mod_code =
		(
		 SELECT MAX(mod_code)
		 FROM jefy.factures
		 WHERE man_ordre = lejefymandat.man_ordre
		)
		AND exe_ordre=exeordre;

		-- creation du man_id --
		SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

		-- creation du mandat --
		INSERT INTO MANDAT(BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE, 
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT, 
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET, 
		MAN_NB_PIECE, MAN_NUMERO, MAN_NUMERO_REJET, 
		MAN_ORDRE, MAN_ORGINE_KEY, MAN_ORIGINE_LIB, MAN_TTC, 
		MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM, 
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE, 
		RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE, 
		MAN_ATTENTE_PAIEMENT, 
		MAN_ATTENTE_DATE, 
		MAN_ATTENTE_OBJET, 
		UTL_ORDRE_ATTENTE) VALUES (
		 borid ,		   		--BOR_ID,
		 NULL, 			   		--BRJ_ORDRE,
		 exeordre,		   		--EXE_ORDRE,
		 lejefymandat.fou_ordre,--FOU_ORDRE,
		 gescode,				--GES_CODE,
		 NULL,				    --MAN_DATE_REMISE,
		 NULL,					--MAN_DATE_VISA_PRINC,
		 'ATTENTE',				--MAN_ETAT,
		 'ATTENTE',			    --MAN_ETAT_REMISE,
		 lejefymandat.man_mont, --MAN_HT,
		 manid,					--MAN_ID,
		 NULL,					--MAN_MOTIF_REJET,
		 lejefymandat.man_piece,--MAN_NB_PIECE,
		 lejefymandat.man_num,	--MAN_NUMERO,
		 NULL,					--MAN_NUMERO_REJET,
		 lejefymandat.man_ordre,--MAN_ORDRE,
		 MANORGINE_KEY,			--MAN_ORGINE_KEY,
		 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
		 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
		 lejefymandat.man_tva,  --MAN_TVA,
		 modordre,				--MOD_ORDRE,
		 ORIORDRE,				--ORI_ORDRE,
		 lejefymandat.pco_num,	--PCO_NUM,
		 lejefymandat.PRES_ORDRE,				--PREST_ID,
		 TORORDRE,				--TOR_ORDRE,
		 VIRORDRE,				--VIR_ORDRE
		 lejefymandat.org_ordre,  --org_ordre
		 lejefymandat.rib_ordre, --rib ordo
		 lejefymandat.rib_ordre, -- rib_comptable
		 0, 					   	--MAN_ATTENTE_PAIEMENT, 
		NULL, 					--MAN_ATTENTE_DATE, 
		NULL, 					--MAN_ATTENTE_OBJET, 
		NULL 					--UTL_ORDRE_ATTENTE
		);

		--on met a jour le type de bordereau s'il s'agit d'un mandat faisant partie d'une prestation interne

		--select count(*) into cpt from v_titre_prest_interne where man_ordre=lejefymandat.man_ordre and tit_ordre is not null;
		--if cpt != 0 then
		IF lejefymandat.PRES_ORDRE IS NOT NULL THEN
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;


		Bordereau_Mandat.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
		Bordereau_Mandat.set_mandat_brouillard(manid);

	END LOOP;

	CLOSE lesJefyMandats;
	/*
	EXCEPTION
	WHEN OTHERS THEN
	 RAISE_APPLICATION_ERROR (-20002,'mandat :'||lejefymandat.man_ordre);
	*/
END;



PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;



--	select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--	if cpt = 0 then
	IF lemandat.prest_id IS NULL THEN
		-- creation du mandat_brouillard visa DEBIT--
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			lemandat.exe_ordre,			   --EXE_ORDRE,
			lemandat.ges_code,			   --GES_CODE,
			lemandat.man_ht,			   --MAB_MONTANT,
			'VISA MANDAT',				   --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
			'D',						   --MAB_SENS,
			manid,						   --MAN_ID,
			lemandat.pco_num			   --PCO_NU
			);


		-- credit=ctrepartie
		--debit = ordonnateur
		-- recup des infos du VISA CREDIT --
		SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

		IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
		END IF;

		SELECT pco_num_ctrepartie, PCO_NUM_TVA
			INTO pconum_ctrepartie, PCONUM_TVA
			FROM PLANCO_VISA
			WHERE pco_num_ordonnateur = lemandat.pco_num;


		SELECT  COUNT(*) INTO cpt
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;

		IF cpt != 0 THEN
			SELECT  pco_num_visa INTO pconum_ctrepartie
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;
		END IF;



		-- recup de l agence comptable --
-- 		SELECT c.ges_code,PCO_NUM_185
-- 			INTO gescodecompta,pconum_185
-- 			FROM GESTION g, COMPTABILITE c
-- 			WHERE g.ges_code = lemandat.ges_code
-- 			AND g.com_ordre = c.com_ordre;
--
			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;


		SELECT par_value   INTO parvalue
			FROM PARAMETRE
			WHERE par_key ='CONTRE PARTIE VISA'
			AND exe_ordre = lemandat.exe_ordre;

		IF parvalue = 'COMPOSANTE' THEN
		   gescodecompta := lemandat.ges_code;
		END IF;

		IF pconum_185 IS NULL THEN
			-- creation du mandat_brouillard visa CREDIT --
			INSERT INTO MANDAT_BROUILLARD VALUES (
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			gescodecompta,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		ELSE
			--au SACD --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		END IF;

		IF lemandat.man_tva != 0 THEN
			-- creation du mandat_brouillard visa CREDIT TVA --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
		END IF;

		/*
		if pconum_185 is not null then

		-- creation du mandat_brouillard visa SACD DEBIT --
		-- creation du mandat_brouillard visa DEBIT--
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'C',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);


		-- creation du mandat_brouillard visa SACD CREDIT --
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			       --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);

		end if;

		*/
	ELSE
		Bordereau_Mandat.set_mandat_brouillard_intern(manid);
	END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;
	leplancomptable 	  PLAN_COMPTABLE%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;


-- 	-- recup de l agence comptable --
-- 	SELECT c.ges_code,PCO_NUM_185
-- 		INTO gescodecompta,pconum_185
-- 		FROM GESTION g, COMPTABILITE c
-- 		WHERE g.ges_code = lemandat.ges_code
-- 		AND g.com_ordre = c.com_ordre;


			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;



	-- recup des infos du VISA CREDIT --
	SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	IF cpt = 0 THEN
	   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
	END IF;
	SELECT pco_num_ctrepartie, PCO_NUM_TVA
		INTO pconum_ctrepartie, PCONUM_TVA
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||lemandat.pco_num;

	IF cpt = 0 THEN
	 SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
	 WHERE pco_num = lemandat.pco_num;

	 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
	END IF;

--	lemandat.pco_num := '18'||lemandat.pco_num;

	-- creation du mandat_brouillard visa DEBIT--
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ht,			   --MAB_MONTANT,
		'VISA MANDAT',				   --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		'18'||lemandat.pco_num			   --PCO_NU
		);

	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '181';

	IF cpt = 0 THEN
		 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');
		--ELSE
		-- SELECT * INTO leplancomptable FROM plan_comptable
		-- WHERE pco_num = '181';
	END IF;


	-- planco de CREDIT 181
	-- creation du mandat_brouillard visa CREDIT --
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						  --ECD_ORDRE,
		lemandat.exe_ordre,			  --EXE_ORDRE,
		gescodecompta,				  --GES_CODE,
		lemandat.man_ttc,		  	  --MAB_MONTANT,
		'VISA MANDAT',				  --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
		'C',						  --MAB_SENS,
		manid,						  --MAN_ID,
		'181'				  --PCO_NU
		);



	IF lemandat.man_tva != 0 THEN
		-- creation du mandat_brouillard visa CREDIT TVA --
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
	END IF;

END;

PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
	niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


PROCEDURE get_facture_jefy
	(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefyfacture 	 jefy.factures%ROWTYPE;
	lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	fouadresse  	 DEPENSE.dep_adresse%TYPE;
	founom  		 DEPENSE.dep_fournisseur%TYPE;
	lotordre  		 DEPENSE.dep_lot%TYPE;
	marordre		 DEPENSE.dep_marches%TYPE;
	fouordre		 DEPENSE.fou_ordre%TYPE;
	gescode			 DEPENSE.ges_code%TYPE;
	modordre		 DEPENSE.mod_ordre%TYPE;
	cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;
	ecd_ordre_ema	 ECRITURE_DETAIL.ecd_ordre%TYPE;

	CURSOR factures IS
	 SELECT * FROM jefy.factures
	 WHERE man_ordre = manordre;

BEGIN

	OPEN factures;
	LOOP
		FETCH factures INTO jefyfacture;
			  EXIT WHEN factures%NOTFOUND;

		-- creation du depid --
		SELECT depense_seq.NEXTVAL INTO depid FROM dual;


		SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		 IF cpt = 0 THEN
			-- creation de lignebudgetaire--
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM jefy.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);
		ELSE
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT  MAX(org_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);
		END IF;

		-- recuperations --

		-- gescode --
		SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		IF cpt = 0 THEN

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;



			SELECT org_comp
				INTO gescode
				FROM jefy.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM jefy.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouordre --
			 SELECT fou_ordre INTO fouordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				 SELECT lot_ordre
				 FROM jefy.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
				   SELECT lot_ordre
				   FROM jefy.commande
				   WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;



		ELSE
			 --recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.facture_ext WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

			SELECT org_comp
				INTO gescode
				FROM jefy.organ
				WHERE org_ordre =
				(
				SELECT MAX(org_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre  FROM MANDAT
				 WHERE man_id = manid
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre
				 FROM MANDAT
				 WHERE man_id = manid
				);



			-- fouordre --
			SELECT fou_ordre INTO fouordre
			 FROM MANDAT
			 WHERE man_id = manid;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				SELECT MAX(lot_ordre)
				 FROM jefy.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
					  SELECT MAX(lot_ordre)
					  FROM jefy.facture_ext
					  WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;
		END IF;




		-- marordre --
		SELECT COUNT(*) INTO cpt
			FROM marches.lot
			WHERE lot_ordre = lotordre;

		IF cpt = 0 THEN
		   	   marordre :=NULL;
		ELSE
			 SELECT mar_ordre
				 INTO marordre
				 FROM marches.lot
				 WHERE lot_ordre = lotordre;
		END IF;



			--MOD_ORDRE --
			SELECT mod_ordre INTO modordre
			FROM MODE_PAIEMENT
			WHERE mod_code =jefyfacture.mod_code AND exe_ordre=exeordre;


		-- recuperer l'ecriture_detail pour emargements semi-auto
		SELECT MAX(ecd_ordre) INTO ecd_ordre_ema FROM jefy.facture_emargement WHERE dep_ordre=jefyfacture.dep_ordre;




		-- creation de la depense --
		INSERT INTO DEPENSE VALUES
			(
			fouadresse ,           --DEP_ADRESSE,
			NULL ,				   --DEP_DATE_COMPTA,
			jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
			jefyfacture.dep_date , --DEP_DATE_SERVICE,
			'VALIDE' ,			   --DEP_ETAT,
			founom ,			   --DEP_FOURNISSEUR,
			jefyfacture.dep_mont , --DEP_HT,
			depense_seq.NEXTVAL ,  --DEP_ID,
			lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
			lotordre ,			   --DEP_LOT,
			marordre ,			   --DEP_MARCHES,
			jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
			jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
			jefyfacture.dep_fact  ,--DEP_NUMERO,
			jefyfacture.dep_ordre ,--DEP_ORDRE,
			NULL ,				   --DEP_REJET,
			jefyfacture.rib_ordre ,--DEP_RIB,
			'NON' ,				   --DEP_SUPPRESSION,
			jefyfacture.dep_ttc ,  --DEP_TTC,
			jefyfacture.dep_ttc - jefyfacture.dep_mont, -- DEP_TVA,
			exeordre ,			   --EXE_ORDRE,
			fouordre, 			   --FOU_ORDRE,
			gescode,  			   --GES_CODE,
			manid ,				   --MAN_ID,
			jefyfacture.man_ordre, --MAN_ORDRE,
--			jefyfacture.mod_code,  --MOD_ORDRE,
			modordre,
			jefyfacture.pco_num ,  --PCO_ORDRE,
			utlordre,    		   --UTL_ORDRE
			NULL, --org_ordre
			tcdordre,
			ecd_ordre_ema -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
			);

	END LOOP;
	CLOSE factures;

END;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Mandat_Non_Admis
IS
--version 1.2.0 20/12/2005 - Multi exercices jefy

   PROCEDURE expedier_btmna (brjordre INTEGER, exeordre INTEGER)
   IS
      cpt            INTEGER;
      borordrejefy   INTEGER;
      tbolibelle     TYPE_BORDEREAU.tbo_libelle%TYPE;
      manordre       MANDAT.man_ordre%TYPE;
      lemandat       MANDAT%ROWTYPE;
      ladepense      DEPENSE%ROWTYPE;
      manid          MANDAT.man_id%TYPE;
      brjnum         BORDEREAU_REJET.brj_num%TYPE;
      lesmanordres   VARCHAR2 (2000);
      lesdepordres   VARCHAR2 (1000);
      ex             INTEGER;

-- les mandats rejetes --
      CURSOR c1
      IS
         SELECT *
           FROM MANDAT
          WHERE brj_ordre = brjordre;

-- depenses a supprimer --
      CURSOR c2
      IS
         SELECT *
           FROM DEPENSE
          WHERE man_id = manid AND dep_suppression = 'OUI';
   BEGIN
      SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

/*
lesmanordre
   "manordre$expli$manordre$expli$.......$manordre$expli$$"
   lesdepordres
   "depordre$depordre$.......$depordre$$"
   */
      lesmanordres := '';
      lesdepordres := '';

      SELECT f.tbo_libelle, b.brj_num
        INTO tbolibelle, brjnum
        FROM BORDEREAU_REJET b, TYPE_BORDEREAU f
       WHERE b.brj_ordre = brjordre AND b.tbo_ordre = f.tbo_ordre;

      SELECT bor_ordre
        INTO borordrejefy
        FROM BORDEREAU
       WHERE bor_id IN (SELECT MAX (bor_id)
                          FROM MANDAT
                         WHERE brj_ordre = brjordre);


	   IF ex IS NULL
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'PARAMETRE EXERCICE NON DEFINI');
            END IF;

      IF exeordre = ex
      THEN
         SELECT COUNT (*)
           INTO cpt
           FROM jefy.bordero_rejet
          WHERE bor_ordre = borordrejefy + 1000000;

         IF cpt = 0
         THEN
            IF tbolibelle != 'BORDEREAU DE MANDAT NON ADMIS'
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTMNA');
            END IF;

            OPEN c1;

            LOOP
               FETCH c1
                INTO lemandat;

               EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
               UPDATE jefy.MANDAT
                  SET man_stat = 'A'
                WHERE man_ordre = lemandat.man_ordre;

-- les facture_rejet --
               INSERT INTO jefy.facture_rejet
                  SELECT dep_ordre, dep_fact, dep_date, dep_lib, dep_ttc,
                         dep_mont, dep_piece, dep_sect, can_code, rib_ordre,
                         mod_code, pco_num, dst_code, agt_ordre, man_ordre,
                         cde_ordre, dep_monnaie, dep_virement, dep_inventaire,
                         dep_ht_saisie, dep_creation, dep_prorata, cm_ordre,
                         cm_type, cm_achat, 'N',
                         NVL (dep_datecomposante, SYSDATE),
                         NVL (dep_datedaf, SYSDATE), dep_lucrativite
                    FROM jefy.factures
                   WHERE man_ordre = lemandat.man_ordre;

               lesmanordres :=
                     lemandat.man_ordre
                  || '$'
                  || SUBSTR (lemandat.man_motif_rejet, 1, 200)
                  || '$'
                  || lesmanordres;
               manid := lemandat.man_id;

               OPEN c2;

               LOOP
                  FETCH c2
                   INTO ladepense;

                  EXIT WHEN c2%NOTFOUND;
                  lesdepordres := ladepense.dep_ordre || '$' || lesdepordres;
               END LOOP;

               CLOSE c2;
            END LOOP;

            CLOSE c1;

            lesmanordres := lesmanordres || '$$';
            lesdepordres := lesdepordres || '$$';
            jefy.maj_mandat_rejet (lesmanordres, lesdepordres, brjnum);
         END IF;
      ELSE
         IF exeordre = 2005
         THEN
            SELECT COUNT (*)
              INTO cpt
              FROM jefy05.bordero_rejet
             WHERE bor_ordre = borordrejefy + 1000000;

            IF cpt = 0
            THEN
               IF tbolibelle != 'BORDEREAU DE MANDAT NON ADMIS'
               THEN
                  RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTMNA');
               END IF;

               OPEN c1;

               LOOP
                  FETCH c1
                   INTO lemandat;

                  EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
                  UPDATE jefy05.MANDAT
                     SET man_stat = 'A'
                   WHERE man_ordre = lemandat.man_ordre;

-- les facture_rejet --
                  INSERT INTO jefy05.facture_rejet
                     SELECT dep_ordre, dep_fact, dep_date, dep_lib, dep_ttc,
                            dep_mont, dep_piece, dep_sect, can_code,
                            rib_ordre, mod_code, pco_num, dst_code, agt_ordre,
                            man_ordre, cde_ordre, dep_monnaie, dep_virement,
                            dep_inventaire, dep_ht_saisie, dep_creation,
                            dep_prorata, cm_ordre, cm_type, cm_achat, 'N',
                            NVL (dep_datecomposante, SYSDATE),
                            NVL (dep_datedaf, SYSDATE), dep_lucrativite
                       FROM jefy05.factures
                      WHERE man_ordre = lemandat.man_ordre;

                  lesmanordres :=
                        lemandat.man_ordre
                     || '$'
                     || SUBSTR (lemandat.man_motif_rejet, 1, 200)
                     || '$'
                     || lesmanordres;
                  manid := lemandat.man_id;

                  OPEN c2;

                  LOOP
                     FETCH c2
                      INTO ladepense;

                     EXIT WHEN c2%NOTFOUND;
                     lesdepordres :=
                                   ladepense.dep_ordre || '$' || lesdepordres;
                  END LOOP;

                  CLOSE c2;
               END LOOP;

               CLOSE c1;

               lesmanordres := lesmanordres || '$$';
               lesdepordres := lesdepordres || '$$';
               jefy05.maj_mandat_rejet (lesmanordres, lesdepordres,brjnum);
            END IF;
		 ELSE
		 	 RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');
         END IF;
      END IF;
   END;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Mandat_05 IS
-- version 1.1.8 du 07/07/2005 - modifs sur le mod_ordre dans depense
-- version 1.1.7 du 31/05/2005 - modifs concernant les prestations internes
-- version 1.1.6 du 25/04/2005 - modifs concernant les prestations internes
-- version 1.1.8 du 15/09/2005 - modifs pour compatibilite avec nouveau gestion_exercice
-- version 1.1.9 du 26/09/2005 - modifs pour emargements semi-auto
-- version 1.2.0 du 20/12/2005 - modifs pour multi exercice jefy


PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

	cpt 	 	INTEGER;

	lebordereau JEFY05.bordero%ROWTYPE;

	agtordre 	JEFY05.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	etat		JEFY05.bordero.bor_stat%TYPE;
BEGIN

	SELECT bor_stat INTO etat FROM JEFY05.bordero
		   WHERE bor_ordre = borordre;

	IF etat = 'N' THEN

		-- est ce un bordereau de titre --
		SELECT COUNT(*) INTO cpt FROM JEFY05.MANDAT
		WHERE bor_ordre = borordre;

		IF cpt != 0 THEN

			tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

			-- le bordereau est il deja vis  ? --
			SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;

			-- l exercice est il encore ouvert ? --


			-- pas de raise mais pas de recup s il est deja dans maracuja --
			IF cpt = 0 THEN
				-- recup des infos --
				SELECT * INTO lebordereau
				FROM JEFY05.bordero
				WHERE bor_ordre = borordre;

				-- recup de l agent de ce bordereau --
				/*select max(agt_ordre) into agtordre
				from JEFY05.facture
				where man_ordre =
				 ( select max(man_ordre)
				   from JEFY05.mandat
				   where bor_ordre =borordre
				  );

				-- recuperation du type de bordereau --
				select count(*) into cpt
				from old_agent_type_bord
				where agt_ordre = agtordre;

				if cpt <> 0 then
				 select tbo_ordre into tboordre
				 from old_agent_type_bord
				 where agt_ordre = agtordre;
				else
				 select tbo_ordre into tboordre
				 from type_bordereau
				 where tbo_ordre = (select par_value from parametre where par_key ='BTME GENERIQUE' and exe_ordre = exeordre);
				end if;
				*/


				-- creation du bor_id --
				SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

				-- recuperation de l utilisateur --
				/*select utl_ordre into utlordre
				from utilisateur
				where agt_ordre = agtordre;
				*/
				-- TEST  TEST  TEST  TEST  TEST
				SELECT 1 INTO utlordre
				FROM dual;
				-- TEST  TEST  TEST  TEST  TEST

				-- creation du bordereau --
				INSERT INTO BORDEREAU VALUES (
					NULL , 	 	 	      --BOR_DATE_VISA,
					'VALIDE',			  --BOR_ETAT,
					borid,				  --BOR_ID,
					leBordereau.bor_num,  --BOR_NUM,
					leBordereau.bor_ordre,--BOR_ORDRE,
					exeordre,	  		  --EXE_ORDRE,
					leBordereau.ges_code, --GES_CODE,
					tboordre,			  --TBO_ORDRE,
					utlordre,		 	  --UTL_ORDRE,
					NULL				  --UTL_ORDRE_VISA
					);

				--get_mandat_JEFY_proc(exeordre,borordre,borid,utlordre,agtordre);
				Bordereau_Mandat_05.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

			END IF;
			--else
			-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
		END IF;
	END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	cpt 	 	INTEGER;

	lejefymandat JEFY05.MANDAT%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	manid 		 MANDAT.man_id%TYPE;

	MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
	MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
	ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
	PRESTID 	   MANDAT.PREST_ID%TYPE;
	TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
	VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
	modordre	   MANDAT.mod_ordre%TYPE;
	CURSOR lesJefyMandats IS
		SELECT *
			FROM JEFY05.MANDAT
			WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefyMandats;
	LOOP
		FETCH lesJefyMandats INTO lejefymandat;
			  EXIT WHEN lesJefyMandats%NOTFOUND;

		-- recuperation du ges_code --
		SELECT ges_code INTO gescode
			FROM BORDEREAU
			WHERE bor_id = borid;

		-- recuperations --
		--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
		MANORGINE_KEY:=NULL;

		--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
		MANORIGINE_LIB:=NULL;

		--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
		IF (lejefymandat.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
		END IF;

		--PRESTID : PRESTATION INTERNE --
		PRESTID :=NULL;

		--TORORDRE : ORIGINE DU MANDAT --
		TORORDRE := 1;

		--VIRORDRE --
		VIRORDRE := NULL;

		--MOD_ORDRE --
		SELECT mod_ordre INTO modordre
		FROM MODE_PAIEMENT
		WHERE mod_code =
		(
		 SELECT MAX(mod_code)
		 FROM JEFY05.factures
		 WHERE man_ordre = lejefymandat.man_ordre
		)
		AND exe_ordre=exeordre;

		-- creation du man_id --
		SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

		-- creation du mandat --
		INSERT INTO MANDAT (BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE, 
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT, 
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET, MAN_NB_PIECE, 
		MAN_NUMERO, MAN_NUMERO_REJET, MAN_ORDRE, MAN_ORGINE_KEY, 
		MAN_ORIGINE_LIB, MAN_TTC, MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM, 
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE, RIB_ORDRE_ORDONNATEUR, 
		RIB_ORDRE_COMPTABLE, MAN_ATTENTE_PAIEMENT, MAN_ATTENTE_DATE, 
		MAN_ATTENTE_OBJET, UTL_ORDRE_ATTENTE) VALUES (
		 borid ,		   		--BOR_ID,
		 NULL, 			   		--BRJ_ORDRE,
		 exeordre,		   		--EXE_ORDRE,
		 lejefymandat.fou_ordre,--FOU_ORDRE,
		 gescode,				--GES_CODE,
		 NULL,				    --MAN_DATE_REMISE,
		 NULL,					--MAN_DATE_VISA_PRINC,
		 'ATTENTE',				--MAN_ETAT,
		 'ATTENTE',			    --MAN_ETAT_REMISE,
		 lejefymandat.man_mont, --MAN_HT,
		 manid,					--MAN_ID,
		 NULL,					--MAN_MOTIF_REJET,
		 lejefymandat.man_piece,--MAN_NB_PIECE,
		 lejefymandat.man_num,	--MAN_NUMERO,
		 NULL,					--MAN_NUMERO_REJET,
		 lejefymandat.man_ordre,--MAN_ORDRE,
		 MANORGINE_KEY,			--MAN_ORGINE_KEY,
		 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
		 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
		 lejefymandat.man_tva,  --MAN_TVA,
		 modordre,				--MOD_ORDRE,
		 ORIORDRE,				--ORI_ORDRE,
		 lejefymandat.pco_num,	--PCO_NUM,
		 lejefymandat.PRES_ORDRE,				--PREST_ID,
		 TORORDRE,				--TOR_ORDRE,
		 VIRORDRE,				--VIR_ORDRE
		 lejefymandat.org_ordre,  --org_ordre
		 lejefymandat.rib_ordre, --rib ordo
		 lejefymandat.rib_ordre, -- rib_comptable
		 0,
		 NULL,
		 NULL,
		 NULL
		);

		--on met a jour le type de bordereau s'il s'agit d'un mandat faisant partie d'une prestation interne

		--select count(*) into cpt from v_titre_prest_interne where man_ordre=lejefymandat.man_ordre and tit_ordre is not null;
		--if cpt != 0 then
		IF lejefymandat.PRES_ORDRE IS NOT NULL THEN
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;


		Bordereau_Mandat_05.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
		Bordereau_Mandat_05.set_mandat_brouillard(manid);

	END LOOP;

	CLOSE lesJefyMandats;
	/*
	EXCEPTION
	WHEN OTHERS THEN
	 RAISE_APPLICATION_ERROR (-20002,'mandat :'||lejefymandat.man_ordre);
	*/
END;



PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;



--	select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--	if cpt = 0 then
	IF lemandat.prest_id IS NULL THEN
		-- creation du mandat_brouillard visa DEBIT--
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			lemandat.exe_ordre,			   --EXE_ORDRE,
			lemandat.ges_code,			   --GES_CODE,
			lemandat.man_ht,			   --MAB_MONTANT,
			'VISA MANDAT',				   --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
			'D',						   --MAB_SENS,
			manid,						   --MAN_ID,
			lemandat.pco_num			   --PCO_NU
			);


		-- credit=ctrepartie
		--debit = ordonnateur
		-- recup des infos du VISA CREDIT --
		SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

		IF cpt = 0 THEN
		   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
		END IF;

		SELECT pco_num_ctrepartie, PCO_NUM_TVA
			INTO pconum_ctrepartie, PCONUM_TVA
			FROM PLANCO_VISA
			WHERE pco_num_ordonnateur = lemandat.pco_num;


		SELECT  COUNT(*) INTO cpt
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;

		IF cpt != 0 THEN
			SELECT  pco_num_visa INTO pconum_ctrepartie
			FROM MODE_PAIEMENT
			WHERE exe_ordre = lemandat.exe_ordre
			AND mod_ordre = lemandat.mod_ordre
			AND pco_num_visa IS NOT NULL;
		END IF;



		-- recup de l agence comptable --
-- 		SELECT c.ges_code,PCO_NUM_185
-- 			INTO gescodecompta,pconum_185
-- 			FROM GESTION g, COMPTABILITE c
-- 			WHERE g.ges_code = lemandat.ges_code
-- 			AND g.com_ordre = c.com_ordre;
--
			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;


		SELECT par_value   INTO parvalue
			FROM PARAMETRE
			WHERE par_key ='CONTRE PARTIE VISA'
			AND exe_ordre = lemandat.exe_ordre;

		IF parvalue = 'COMPOSANTE' THEN
		   gescodecompta := lemandat.ges_code;
		END IF;

		IF pconum_185 IS NULL THEN
			-- creation du mandat_brouillard visa CREDIT --
			INSERT INTO MANDAT_BROUILLARD VALUES (
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			gescodecompta,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		ELSE
			--au SACD --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_ttc,		  	  --MAB_MONTANT,
			'VISA MANDAT',				  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'C',						  --MAB_SENS,
			manid,						  --MAN_ID,
			pconum_ctrepartie				  --PCO_NU
			);
		END IF;

		IF lemandat.man_tva != 0 THEN
			-- creation du mandat_brouillard visa CREDIT TVA --
			INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
		END IF;

		/*
		if pconum_185 is not null then

		-- creation du mandat_brouillard visa SACD DEBIT --
		-- creation du mandat_brouillard visa DEBIT--
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'C',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);


		-- creation du mandat_brouillard visa SACD CREDIT --
		insert into mandat_brouillard values
		(
		null,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			       --GES_CODE,
		lemandat.man_ttc,			   --MAB_MONTANT,
		'VISA SACD',				   --MAB_OPERATION,
		mandat_brouillard_seq.nextval, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		pconum_185			   		   --PCO_NU
		);

		end if;

		*/
	ELSE
		Bordereau_Mandat_05.set_mandat_brouillard_intern(manid);
	END IF;
END;

PROCEDURE set_mandat_brouillard_intern(manid INTEGER)
IS

	lemandat  	  MANDAT%ROWTYPE;
	leplancomptable 	  PLAN_COMPTABLE%ROWTYPE;

	pconum_ctrepartie MANDAT.PCO_NUM%TYPE;
	PCONUM_TVA 	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	gescodecompta MANDAT.ges_code%TYPE;
	pconum_185	  PLANCO_VISA.PCO_NUM_tva%TYPE;
	parvalue	  PARAMETRE.par_value%TYPE;
	cpt INTEGER;

BEGIN

	SELECT * INTO lemandat
		FROM MANDAT
		WHERE man_id = manid;


-- 	-- recup de l agence comptable --
-- 	SELECT c.ges_code,PCO_NUM_185
-- 		INTO gescodecompta,pconum_185
-- 		FROM GESTION g, COMPTABILITE c
-- 		WHERE g.ges_code = lemandat.ges_code
-- 		AND g.com_ordre = c.com_ordre;


			-- modif 15/09/2005 compatibilite avec new gestion_exercice
		SELECT c.ges_code,ge.PCO_NUM_185
			INTO gescodecompta,pconum_185
			FROM GESTION g, COMPTABILITE c, GESTION_EXERCICE ge
			WHERE g.ges_code = lemandat.ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=lemandat.EXE_ORDRE;



	-- recup des infos du VISA CREDIT --
	SELECT COUNT(*) INTO cpt
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	IF cpt = 0 THEN
	   RAISE_APPLICATION_ERROR (-20001,'PROBLEM DE CONTRE PARTIE '||lemandat.pco_num);
	END IF;
	SELECT pco_num_ctrepartie, PCO_NUM_TVA
		INTO pconum_ctrepartie, PCONUM_TVA
		FROM PLANCO_VISA
		WHERE pco_num_ordonnateur = lemandat.pco_num;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||lemandat.pco_num;

	IF cpt = 0 THEN
	 SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
	 WHERE pco_num = lemandat.pco_num;

	 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
	END IF;

--	lemandat.pco_num := '18'||lemandat.pco_num;

	-- creation du mandat_brouillard visa DEBIT--
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		lemandat.exe_ordre,			   --EXE_ORDRE,
		lemandat.ges_code,			   --GES_CODE,
		lemandat.man_ht,			   --MAB_MONTANT,
		'VISA MANDAT',				   --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
		'D',						   --MAB_SENS,
		manid,						   --MAN_ID,
		'18'||lemandat.pco_num			   --PCO_NU
		);

	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '181';

	IF cpt = 0 THEN
		 maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');
		--ELSE
		-- SELECT * INTO leplancomptable FROM plan_comptable
		-- WHERE pco_num = '181';
	END IF;


	-- planco de CREDIT 181
	-- creation du mandat_brouillard visa CREDIT --
	INSERT INTO MANDAT_BROUILLARD VALUES
		(
		NULL,  						  --ECD_ORDRE,
		lemandat.exe_ordre,			  --EXE_ORDRE,
		gescodecompta,				  --GES_CODE,
		lemandat.man_ttc,		  	  --MAB_MONTANT,
		'VISA MANDAT',				  --MAB_OPERATION,
		mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
		'C',						  --MAB_SENS,
		manid,						  --MAN_ID,
		'181'				  --PCO_NU
		);



	IF lemandat.man_tva != 0 THEN
		-- creation du mandat_brouillard visa CREDIT TVA --
		INSERT INTO MANDAT_BROUILLARD VALUES
			(
			NULL,  						  --ECD_ORDRE,
			lemandat.exe_ordre,			  --EXE_ORDRE,
			lemandat.ges_code,				  --GES_CODE,
			lemandat.man_tva,		      --MAB_MONTANT,
			'VISA TVA',						  --MAB_OPERATION,
			mandat_brouillard_seq.NEXTVAL,--MAB_ORDRE,
			'D',						  --MAB_SENS,
			manid,						  --MAN_ID,
			PCONUM_TVA					  --PCO_NU
			);
	END IF;

END;

PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
	niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


PROCEDURE get_facture_jefy
	(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefyfacture 	 JEFY05.factures%ROWTYPE;
	lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	fouadresse  	 DEPENSE.dep_adresse%TYPE;
	founom  		 DEPENSE.dep_fournisseur%TYPE;
	lotordre  		 DEPENSE.dep_lot%TYPE;
	marordre		 DEPENSE.dep_marches%TYPE;
	fouordre		 DEPENSE.fou_ordre%TYPE;
	gescode			 DEPENSE.ges_code%TYPE;
	modordre		 DEPENSE.mod_ordre%TYPE;
	cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;
	ecd_ordre_ema	 ECRITURE_DETAIL.ecd_ordre%TYPE;

	CURSOR factures IS
	 SELECT * FROM JEFY05.factures
	 WHERE man_ordre = manordre;

BEGIN

	OPEN factures;
	LOOP
		FETCH factures INTO jefyfacture;
			  EXIT WHEN factures%NOTFOUND;

		-- creation du depid --
		SELECT depense_seq.NEXTVAL INTO depid FROM dual;


		SELECT COUNT(*) INTO cpt FROM JEFY05.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		 IF cpt = 0 THEN
			-- creation de lignebudgetaire--
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM JEFY05.engage
				 WHERE cde_ordre = JEFYfacture.cde_ordre
				 AND eng_stat !='A'
				);
		ELSE
			SELECT org_comp||' '||org_lbud||' '||org_uc
				INTO lignebudgetaire
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				 SELECT  MAX(org_ordre)
				 FROM JEFY05.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);
		END IF;

		-- recuperations --

		-- gescode --
		SELECT COUNT(*) INTO cpt FROM JEFY05.facture_ext
			   WHERE cde_ordre = jefyfacture.cde_ordre;

		IF cpt = 0 THEN

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM JEFY05.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;



			SELECT org_comp
				INTO gescode
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				 SELECT org_ordre
				 FROM JEFY05.engage
				 WHERE cde_ordre = jefyfacture.cde_ordre
				 AND eng_stat !='A'
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				 SELECT fou_ordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouordre --
			 SELECT fou_ordre INTO fouordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				 SELECT lot_ordre
				 FROM JEFY05.commande
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
				   SELECT lot_ordre
				   FROM JEFY05.commande
				   WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;



		ELSE
			 --recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM JEFY05.facture_ext WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

			SELECT org_comp
				INTO gescode
				FROM JEFY05.organ
				WHERE org_ordre =
				(
				SELECT MAX(org_ordre)
				 FROM JEFY05.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			-- fouadresse --
			SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
				INTO fouadresse
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre  FROM MANDAT
				 WHERE man_id = manid
				);


			-- founom --
			SELECT adr_nom||' '||adr_prenom
				INTO founom
				FROM v_fournisseur
				WHERE fou_ordre =
				(
				SELECT fou_ordre
				 FROM MANDAT
				 WHERE man_id = manid
				);



			-- fouordre --
			SELECT fou_ordre INTO fouordre
			 FROM MANDAT
			 WHERE man_id = manid;

			-- lotordre --
			SELECT COUNT(*) INTO cpt
				FROM marches.attribution
				WHERE att_ordre =
				(
				SELECT MAX(lot_ordre)
				 FROM JEFY05.facture_ext
				 WHERE cde_ordre = jefyfacture.cde_ordre
				);

			 IF cpt = 0 THEN
			  	lotordre :=NULL;
			 ELSE
				  SELECT lot_ordre
				  INTO lotordre
				  FROM marches.attribution
				  WHERE att_ordre =
				  (
					  SELECT MAX(lot_ordre)
					  FROM JEFY05.facture_ext
					  WHERE cde_ordre = jefyfacture.cde_ordre
				  );
			 END IF;
		END IF;




		-- marordre --
		SELECT COUNT(*) INTO cpt
			FROM marches.lot
			WHERE lot_ordre = lotordre;

		IF cpt = 0 THEN
		   	   marordre :=NULL;
		ELSE
			 SELECT mar_ordre
				 INTO marordre
				 FROM marches.lot
				 WHERE lot_ordre = lotordre;
		END IF;



			--MOD_ORDRE --
			SELECT mod_ordre INTO modordre
			FROM MODE_PAIEMENT
			WHERE mod_code =jefyfacture.mod_code AND exe_ordre=exeordre;


		-- recuperer l'ecriture_detail pour emargements semi-auto
		SELECT MAX(ecd_ordre) INTO ecd_ordre_ema FROM JEFY05.facture_emargement WHERE dep_ordre=jefyfacture.dep_ordre;




		-- creation de la depense --
		INSERT INTO DEPENSE VALUES
			(
			fouadresse ,           --DEP_ADRESSE,
			NULL ,				   --DEP_DATE_COMPTA,
			jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
			jefyfacture.dep_date , --DEP_DATE_SERVICE,
			'VALIDE' ,			   --DEP_ETAT,
			founom ,			   --DEP_FOURNISSEUR,
			jefyfacture.dep_mont , --DEP_HT,
			depense_seq.NEXTVAL ,  --DEP_ID,
			lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
			lotordre ,			   --DEP_LOT,
			marordre ,			   --DEP_MARCHES,
			jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
			jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
			jefyfacture.dep_fact  ,--DEP_NUMERO,
			jefyfacture.dep_ordre ,--DEP_ORDRE,
			NULL ,				   --DEP_REJET,
			jefyfacture.rib_ordre ,--DEP_RIB,
			'NON' ,				   --DEP_SUPPRESSION,
			jefyfacture.dep_ttc ,  --DEP_TTC,
			jefyfacture.dep_ttc - jefyfacture.dep_mont, -- DEP_TVA,
			exeordre ,			   --EXE_ORDRE,
			fouordre, 			   --FOU_ORDRE,
			gescode,  			   --GES_CODE,
			manid ,				   --MAN_ID,
			jefyfacture.man_ordre, --MAN_ORDRE,
--			jefyfacture.mod_code,  --MOD_ORDRE,
			modordre,
			jefyfacture.pco_num ,  --PCO_ORDRE,
			utlordre,    		   --UTL_ORDRE
			NULL, --org_ordre
			tcdordre,
			ecd_ordre_ema -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
			);

	END LOOP;
	CLOSE factures;

END;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Papaye IS
--version 1.1.8b - recup du tcd_ordre dans depense
--version 1.1.9 - modifs pour subrogation

PROCEDURE Recup_Brouillards_Payes (moisordre NUMBER)
IS

-- Recuperation de tous les bordereaux de paye deja
CURSOR bordereaux IS
SELECT DISTINCT bor_ordre FROM jefy.bordero WHERE bor_ordre IN (
SELECT bor_ordre FROM jefy.MANDAT WHERE man_ordre IN (
SELECT man_ordre FROM jefy.facture WHERE agt_ordre IN (
SELECT TO_NUMBER(param_value) FROM papaye.paye_parametres WHERE param_key = 'AGENT_JEFY')
AND dep_fact IN (SELECT mois_complet FROM papaye.paye_mois WHERE mois_ordre = moisordre)));
--select distinct bor_ordre from jefy.papaye_compta where mois_ordre = moisordre;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
tboordre maracuja.TYPE_BORDEREAU.tbo_ordre%TYPE;
cpt INTEGER;

BEGIN

  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO borordre;
    EXIT WHEN bordereaux%NOTFOUND;

	maracuja.Bordereau_Papaye.maj_brouillards_payes(moisordre, borordre);

  END LOOP;
  CLOSE bordereaux;

END;

PROCEDURE maj_brouillards_payes (moisordre NUMBER, borordre NUMBER)
IS

borid  	 maracuja.BORDEREAU.bor_id%TYPE;
tboordre maracuja.TYPE_BORDEREAU.tbo_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;

BEGIN

	 SELECT ges_code INTO gescode FROM jefy.bordero WHERE bor_ordre = borordre;

	 -- On verifie que les ecritures aient bien ete generees pour la composante en question.
	 SELECT COUNT(*) INTO cpt
	 FROM papaye.jefy_ecritures WHERE ecr_comp = gescode AND mois_ordre = moisordre;

	 IF (cpt = 0)
	 THEN
	 	 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas pr�par� les �critures pour la composante '||gescode||' !');
	 END IF;

	 -- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
	 SELECT SUM(ecr_mont) INTO sumdebits FROM papaye.jefy_ecritures
	 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' AND ecr_comp = gescode AND ecr_sens = 'D';

	 SELECT SUM(ecr_mont) INTO sumcredits FROM papaye.jefy_ecritures
	 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' AND ecr_comp = gescode AND ecr_sens = 'C';

	 IF (sumcredits <> sumdebits)
	 THEN
	 	 RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||gescode||', la somme des DEBITS ('||sumdebits||') est diff�rente de la somme des CREDITS ('||sumcredits||') !');
	 END IF;


  	SELECT mois_annee INTO exeordre FROM papaye.paye_mois WHERE mois_ordre = moisordre;
	tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

	SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
	WHERE bor_ordre = borordre AND tbo_ordre = tboordre AND exe_ordre = exeordre;

	IF cpt = 0
	THEN

		Bordereau_Papaye.get_btme_jefy(borordre, exeordre);

		SELECT bor_id INTO borid FROM maracuja.BORDEREAU WHERE bor_ordre = borordre AND tbo_ordre = tboordre AND exe_ordre = exeordre;

		Bordereau_Papaye.set_bord_brouillard_visa(borid);

		Bordereau_Papaye.set_bord_brouillard_retenues(borid);

		Bordereau_Papaye.set_bord_brouillard_sacd(borid);

	END IF;

  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Papaye.set_bord_brouillard_paiement(moisordre, borid, exeordre);

END;


PROCEDURE GET_BTME_JEFY
(borordre NUMBER,exeordre NUMBER)

IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	utilisateur.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN
SELECT bor_stat INTO etat FROM jefy.bordero WHERE bor_ordre = borordre;

IF etat = 'N' OR etat = 'X'
THEN

-- est ce un bordereau de titre --
SELECT COUNT(*) INTO cpt FROM jefy.MANDAT
WHERE bor_ordre = borordre;

IF cpt != 0 THEN

tboordre:=Gestionorigine.recup_type_bordereau_mandat(borordre, exeordre);

-- le bordereau est il deja vis� ? --
SELECT COUNT(*) INTO cpt
FROM BORDEREAU
WHERE bor_ordre = borordre
AND exe_ordre = exeordre
AND tbo_ordre = tboordre;

-- l exercice est il encore ouvert ? --

-- pas de raise mais pas de recup s il est deja dans maracuja --
IF cpt = 0 THEN

-- recup des infos --
SELECT * INTO lebordereau FROM jefy.bordero WHERE bor_ordre = borordre;

-- creation du bor_id --
SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

-- creation du bordereau --
INSERT INTO BORDEREAU VALUES (
NULL , 	 	 	      --BOR_DATE_VISA,
'VALIDE',			  --BOR_ETAT,
borid,				  --BOR_ID,
leBordereau.bor_num,  --BOR_NUM,
leBordereau.bor_ordre,--BOR_ORDRE,
exeordre,	  		  --EXE_ORDRE,
leBordereau.ges_code, --GES_CODE,
tboordre,			  --TBO_ORDRE,
1,		 	  		  --UTL_ORDRE,
NULL				  --UTL_ORDRE_VISA
);

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre = borordre);

INSERT INTO BORDEREAU_INFO VALUES
(
borid,
moislibelle,
NULL
);

Bordereau_Papaye.get_mandat_jefy(exeordre,borordre,borid,utlordre,agtordre);

END IF;
--else
-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
END IF;
END IF;
END;

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

lejefymandat jefy.MANDAT%ROWTYPE;
gescode 	 GESTION.ges_code%TYPE;
manid 		 MANDAT.man_id%TYPE;

MANORGINE_KEY  MANDAT.MAN_ORGINE_KEY%TYPE;
MANORIGINE_LIB MANDAT.MAN_ORIGINE_LIB%TYPE;
ORIORDRE 	   MANDAT.ORI_ORDRE%TYPE;
PRESTID 	   MANDAT.PREST_ID%TYPE;
TORORDRE 	   MANDAT.TOR_ORDRE%TYPE;
VIRORDRE 	   MANDAT.PAI_ORDRE%TYPE;
modordre	   MANDAT.mod_ordre%TYPE;
CURSOR lesJefyMandats IS
SELECT *
FROM jefy.MANDAT
WHERE bor_ordre =borordre;


BEGIN


--boucle de traitement --
OPEN lesJefyMandats;
LOOP
FETCH lesJefyMandats INTO lejefymandat;
EXIT WHEN lesJefyMandats%NOTFOUND;

-- recuperation du ges_code --
SELECT ges_code INTO gescode
FROM BORDEREAU
WHERE bor_id = borid;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
MANORGINE_KEY:=NULL;

--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
MANORIGINE_LIB:=NULL;

--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
IF (lejefymandat.conv_ordre IS NOT NULL) THEN
  ORIORDRE :=Gestionorigine.traiter_convordre(lejefymandat.conv_ordre);
ELSE
  ORIORDRE :=Gestionorigine.traiter_orgordre(lejefymandat.org_ordre);
END IF;

--PRESTID : PRESTATION INTERNE --
PRESTID :=NULL;

--TORORDRE : ORIGINE DU MANDAT --
TORORDRE := 1;

--VIRORDRE --
VIRORDRE := NULL;

--MOD_ORDRE --
SELECT mod_ordre INTO modordre
FROM MODE_PAIEMENT
WHERE mod_code =
(
 SELECT MAX(mod_code)
 FROM jefy.factures
 WHERE man_ordre = lejefymandat.man_ordre
)
AND exe_ordre=exeordre;

-- creation du man_id --
SELECT mandat_seq.NEXTVAL INTO manid FROM dual;

-- creation du mandat --
INSERT INTO MANDAT (BOR_ID, BRJ_ORDRE, EXE_ORDRE, FOU_ORDRE, 
		GES_CODE, MAN_DATE_REMISE, MAN_DATE_VISA_PRINC, MAN_ETAT, 
		MAN_ETAT_REMISE, MAN_HT, MAN_ID, MAN_MOTIF_REJET, 
		MAN_NB_PIECE, MAN_NUMERO, MAN_NUMERO_REJET, 
		MAN_ORDRE, MAN_ORGINE_KEY, MAN_ORIGINE_LIB, MAN_TTC, 
		MAN_TVA, MOD_ORDRE, ORI_ORDRE, PCO_NUM, 
		PREST_ID, TOR_ORDRE, PAI_ORDRE, ORG_ORDRE, 
		RIB_ORDRE_ORDONNATEUR, RIB_ORDRE_COMPTABLE, 
		MAN_ATTENTE_PAIEMENT, 
		MAN_ATTENTE_DATE, 
		MAN_ATTENTE_OBJET, 
		UTL_ORDRE_ATTENTE) VALUES
(
 borid ,		   		--BOR_ID,
 NULL, 			   		--BRJ_ORDRE,
 exeordre,		   		--EXE_ORDRE,
 lejefymandat.fou_ordre,--FOU_ORDRE,
 gescode,				--GES_CODE,
 NULL,				    --MAN_DATE_REMISE,
 NULL,					--MAN_DATE_VISA_PRINC,
 'ATTENTE',				--MAN_ETAT,
 'ATTENTE',			    --MAN_ETAT_REMISE,
 lejefymandat.man_mont, --MAN_HT,
 manid,					--MAN_ID,
 NULL,					--MAN_MOTIF_REJET,
 lejefymandat.man_piece,--MAN_NB_PIECE,
 lejefymandat.man_num,	--MAN_NUMERO,
 NULL,					--MAN_NUMERO_REJET,
 lejefymandat.man_ordre,--MAN_ORDRE,
 MANORGINE_KEY,			--MAN_ORGINE_KEY,
 MANORIGINE_LIB,		--MAN_ORIGINE_LIB,
 lejefymandat.man_mont+lejefymandat.man_tva, --MAN_TTC,
 lejefymandat.man_tva,  --MAN_TVA,
 modordre,				--MOD_ORDRE,
 ORIORDRE,				--ORI_ORDRE,
 lejefymandat.pco_num,	--PCO_NUM,
 lejefymandat.PRES_ORDRE,				--PREST_ID,
 TORORDRE,				--TOR_ORDRE,
 VIRORDRE,				--VIR_ORDRE
 lejefymandat.org_ordre,  --org_ordre
 lejefymandat.rib_ordre, --rib ordo
 lejefymandat.rib_ordre, -- rib_comptable
 0,
 NULL,
 NULL,
 NULL
);

Bordereau_Papaye.get_facture_jefy(exeordre,manid,lejefymandat.man_ordre,utlordre);
Bordereau_Papaye.set_mandat_brouillard(manid);

END LOOP;

CLOSE lesJefyMandats;

END;


-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
moislibelle papaye.paye_mois.mois_complet%TYPE;

cpt INTEGER;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

tboordre:=Gestionorigine.recup_type_bordereau_mandat(currentBordereau.bor_ordre, exeordre);

SELECT mois_complet INTO moislibelle FROM papaye.paye_mois WHERE mois_ordre = moisordre;

SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id IN (
SELECT bor_id FROM BORDEREAU WHERE tbo_ordre = tboordre)
AND bob_operation LIKE '%PAIEMENT%' AND bob_libelle2 = moislibelle;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'PAIEMENT SALAIRES',
	currentecriture.pco_num,
	'PAIEMENT SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans PAPAYE.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from papaye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.JEFY_ECRITURES WHERE mois_ordre = mois AND ecr_comp = gescode  AND ecr_type='64' AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||gescode||' , moisordre : '||moisordre);


  OPEN ecriturescredit64(moisordre, gescode);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'VISA SALAIRES',
	currentecriture.pco_num,
	'VISA SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecriturescredit64;

END;

-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans Papaye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = mois AND ecr_comp = gescode AND ecr_type='44';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, gescode);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'RETENUES SALAIRES',
	currentecriture.pco_num,
	'RETENUES SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans Papaye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(mois NUMBER , gescode VARCHAR2) IS
SELECT * FROM papaye.jefy_ecritures WHERE mois_ordre = mois AND ecr_comp = gescode AND ecr_type='18';

currentecriture papaye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle papaye.paye_mois.mois_complet%TYPE;

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_fact INTO moislibelle FROM jefy.facture WHERE man_ordre IN (
SELECT man_ordre FROM jefy.MANDAT WHERE bor_ordre IN (
SELECT bor_ordre FROM maracuja.BORDEREAU WHERE bor_id = borid));

SELECT mois_ordre INTO moisordre FROM papaye.paye_mois WHERE mois_complet  = moislibelle;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, gescode);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'SACD SALAIRES',
	currentecriture.pco_num,
	'SACD SALAIRES '||moislibelle,
	moislibelle,
	NULL
	);

	END LOOP;
  CLOSE ecrituressacd;

END;


-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat  	  MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,  						   --ECD_ORDRE,
lemandat.exe_ordre,			   --EXE_ORDRE,
lemandat.ges_code,			   --GES_CODE,
lemandat.man_ht,			   --MAB_MONTANT,
'VISA SALAIRES',				   --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',						   --MAB_SENS,
manid,						   --MAN_ID,
lemandat.pco_num			   --PCO_NU
);

END;

PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid 	  		 DEPENSE.dep_id%TYPE;
jefyfacture 	 jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse  	 DEPENSE.dep_adresse%TYPE;
founom  		 DEPENSE.dep_fournisseur%TYPE;
lotordre  		 DEPENSE.dep_lot%TYPE;
marordre		 DEPENSE.dep_marches%TYPE;
fouordre		 DEPENSE.fou_ordre%TYPE;
gescode			 DEPENSE.ges_code%TYPE;
cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
 			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,				   --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,			   --DEP_ETAT,
founom ,			   --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
lotordre ,			   --DEP_LOT,
marordre ,			   --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,				   --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,				   --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,			   --EXE_ORDRE,
fouordre, 			   --FOU_ORDRE,
gescode,  			   --GES_CODE,
manid ,				   --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
jefyfacture.mod_code,  --MOD_ORDRE,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,    		   --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL -- ecd_ordre_ema
);

END LOOP;
CLOSE factures;

END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Titre IS
-- version 1.5.0 08/11/2006 - modifs pour le rec_id dans titre_brouillard 
--version 1.1.9 04/05/2006 - modif pour plusieurs ventilations
--version 1.1.8 31/05/2005 - modifs prestations internes
--version 1.1.7 26/04/2005 - prestations internes
--version 1.1.8 23/06/2005 - structure table recette

PROCEDURE Get_Btte_Jefy
(borordre NUMBER,exeordre NUMBER)

IS
	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;
	dbms_output.put_line('exercice : '||exeordre);

	-- est ce un bordereau de titre --
	SELECT COUNT(*) INTO cpt FROM jefy.TITRE
		   WHERE bor_ordre = borordre;

	IF cpt != 0 THEN

		-- le bordereau est il deja vis? ? --
		tboordre:=Gestionorigine.recup_type_bordereau_titre(borordre, exeordre);

		SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;




		-- l exercice est il encore ouvert ? --


		-- pas de raise mais pas de recup s il est deja dans maracuja --
		IF cpt = 0 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;
			/*
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy.TITRE
			WHERE tit_ordre =
			 ( SELECT MAX(tit_ordre)
			   FROM jefy.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre
			  );

			-- recuperation du type de bordereau --
			SELECT COUNT(*) INTO cpt
			FROM OLD_AGENT_TYPE_BORD
			WHERE agt_ordre = agtordre;

			IF cpt <> 0 THEN
			 SELECT tbo_ordre INTO tboordre
			 FROM OLD_AGENT_TYPE_BORD
			 WHERE agt_ordre = agtordre;
			ELSE
			 SELECT tbo_ordre INTO tboordre
			 FROM TYPE_BORDEREAU
			 WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' and EXE_ORDRE=exeordre);
			END IF;
			*/

			-- creation du bor_id --
			SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

			-- recuperation de l utilisateur --
			/*select utl_ordre into utlordre
			from utilisateur
			where agt_ordre = agtordre;
			*/
			-- TEST  TEST  TEST  TEST  TEST
			SELECT 1 INTO utlordre
				   FROM dual;
			-- TEST  TEST  TEST  TEST  TEST

			-- creation du bordereau --
			INSERT INTO BORDEREAU VALUES (
				NULL , 	 	 	      --BOR_DATE_VISA,
				'VALIDE',			  --BOR_ETAT,
				borid,				  --BOR_ID,
				leBordereau.bor_num,  --BOR_NUM,
				leBordereau.bor_ordre,--BOR_ORDRE,
				exeordre,	  		  --EXE_ORDRE,
				leBordereau.ges_code, --GES_CODE,
				tboordre,			  --TBO_ORDRE,
				utlordre,		 	  --UTL_ORDRE,
				NULL				  --UTL_ORDRE_VISA
				);

			Get_Titre_Jefy(exeordre,borordre,borid,utlordre,agtordre);

		END IF;
		--else
		-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
	END IF;

END;


PROCEDURE Get_Titre_Jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	jefytitre jefy.TITRE%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	titid 		 TITRE.tit_id%TYPE;

	titoriginekey  TITRE.tit_ORGINE_KEY%TYPE;
	titoriginelib TITRE.tit_ORIGINE_LIB%TYPE;
	ORIORDRE 	   TITRE.ORI_ORDRE%TYPE;
	PRESTID 	   TITRE.PREST_ID%TYPE;
	TORORDRE 	   TITRE.TOR_ORDRE%TYPE;
	modordre	   TITRE.mod_ordre%TYPE;
	presid		   INTEGER;
	cpt			   INTEGER;

	CURSOR lesJefytitres IS
	SELECT *
	FROM jefy.TITRE
	WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefytitres;
	LOOP
		FETCH lesJefytitres INTO jefytitre;
			  EXIT WHEN lesJefytitres%NOTFOUND;

		-- recup ??
		IF (jefytitre.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(jefytitre.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(jefytitre.org_ordre);
		END IF;

		titoriginekey  :=NULL;
		titoriginelib   :=NULL;

		-- recup du torordre origine JEFYCO
		TORORDRE 	     :=1;
		-- JEFYCO
		torordre := 1;
		-- recup du mode de paiement OP ordo
		modordre	     :=NULL;

		dbms_output.put_line('titreOrdre : '||jefytitre.tit_ordre);
		dbms_output.put_line('mod_code : '||jefytitre.mod_code);


		IF jefytitre.tit_type = 'C' THEN
		   SELECT mod_ordre INTO modordre
		   FROM MODE_PAIEMENT
		   WHERE mod_code = jefytitre.mod_code
		   AND exe_ordre = exeordre;
		ELSE
			modordre := NULL;
		END IF;



		-- recup du prestid
		presid		     :=NULL;


		--TODO des gescode sont nuls, recuperer le gescode de la table comptabilite ?
		IF jefytitre.ges_code IS NULL THEN
			 SELECT DISTINCT ges_code INTO gescode
			 FROM BORDEREAU
			 WHERE bor_id=borid;
		ELSE
		 	gescode:=jefytitre.ges_code;
		END IF;

		SELECT titre_seq.NEXTVAL INTO titid FROM dual;

		INSERT INTO TITRE (
		   BOR_ID,
		   BOR_ORDRE,
		   BRJ_ORDRE,
		   EXE_ORDRE,
		   GES_CODE,
		   MOD_ORDRE,
		   ORI_ORDRE,
		   PCO_NUM,
		   PREST_ID,
		   TIT_DATE_REMISE,
		   TIT_DATE_VISA_PRINC,
		   TIT_ETAT,
		   TIT_ETAT_REMISE,
		   TIT_HT,
		   TIT_ID,
		   TIT_MOTIF_REJET,
		   TIT_NB_PIECE,
		   TIT_NUMERO,
		   TIT_NUMERO_REJET,
		   TIT_ORDRE,
		   TIT_ORGINE_KEY,
		   TIT_ORIGINE_LIB,
		   TIT_TTC,
		   TIT_TVA,
		   TOR_ORDRE,
		   UTL_ORDRE,
		   ORG_ORDRE,
		   FOU_ORDRE,
		   MOR_ORDRE,
		   PAI_ORDRE,
		   rib_ordre_ordonnateur,
		   rib_ordre_comptable,
		   tit_libelle)
		VALUES
			(
			borid,--BOR_ID,
			borordre,--BOR_ORDRE,
			NULL,--BRJ_ORDRE,
			exeordre,--EXE_ORDRE,
			gescode,--GES_CODE,
			modordre,--MOD_ORDRE,
			oriordre,--ORI_ORDRE,
			jefytitre.pco_num,--PCO_NUM,
			jefytitre.pres_ordre,--PREST_ID,
			NULL,--TIT_DATE_REMISE,
			NULL,--TIT_DATE_VISA_PRINC,
			'ATTENTE',--TIT_ETAT,
			'ATTENTE',--TIT_ETAT_REMISE,
			NVL(En_Nombre(jefytitre.tit_mont),0),--TIT_HT,
			titid,--TIT_ID,
			NULL,--TIT_MOTIF_REJET,
			jefytitre.tit_piece,--TIT_NB_PIECE,
			jefytitre.tit_num,--TIT_NUMERO,
			NULL,--TIT_NUMERO_REJET,
			jefytitre.tit_ordre,--TIT_ORDRE,
			titoriginekey,--TIT_ORGINE_KEY,
			titoriginelib,--TIT_ORIGINE_LIB,
			NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TIT_TTC,
			NVL(En_Nombre(jefytitre.tit_monttva),0),--TIT_TVA,
			torordre,--TOR_ORDRE,
			utlordre,--UTL_ORDRE
			jefytitre.org_ordre,		--ORG_ORDRE,
			jefytitre.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
			modordre,				 --MOR_ORDRE
			NULL, -- VIR_ORDRE
			jefytitre.rib_ordre,
			jefytitre.rib_ordre,
			jefytitre.tit_lib
			);

		--on met a jour le type de bordereau s'il s'agit d'un bordereau de prestation interne
		SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=jefytitre.tit_ordre AND exe_ordre=exeordre;
		IF cpt != 0 THEN
--		if jefytitre.pres_ordre is not null then
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;

		Get_Recette_Jefy(exeordre,titid,jefytitre.tit_ordre,utlordre);
		Set_Titre_Brouillard(titid);

	END LOOP;

	CLOSE lesJefytitres;

END;




PROCEDURE Get_Recette_Jefy
(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefytitre		 jefy.TITRE%ROWTYPE;
	--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	--fouadresse  	 DEPENSE.dep_adresse%TYPE;
	--founom  		 DEPENSE.dep_fournisseur%TYPE;
	--lotordre  		 DEPENSE.dep_lot%TYPE;
	--marordre		 DEPENSE.dep_marches%TYPE;
	--fouordre		 DEPENSE.fou_ordre%TYPE;
	--gescode			 DEPENSE.ges_code%TYPE;
	cpt				 INTEGER;
	recid			 RECETTE.rec_id%TYPE;
	rectype			 RECETTE.rec_type%TYPE;
	gescode			 TITRE.GES_CODE%TYPE;

	modordre	   DEPENSE.mod_ordre%TYPE;


BEGIN

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = titordre;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;

	rectype := NULL;

	-- ajout rod
	IF jefytitre.tit_type = 'C' THEN
	   SELECT mod_ordre INTO modordre
	   FROM MODE_PAIEMENT
	   WHERE mod_code = jefytitre.mod_code
	   AND exe_ordre = exeordre;
	ELSE
		modordre := NULL;
	END IF;


-- 	IF jefytitre.ges_code IS NULL THEN
-- 	 SELECT distinct ges_code INTO gescode
-- 	 FROM jefy.ventil_titre
-- 	 WHERE tit_ordre = jefytitre.tit_ordre;
-- 	else
-- 	 gescode:=jefytitre.ges_code;
-- 	END IF;


	 SELECT DISTINCT ges_code INTO gescode
	 FROM maracuja.TITRE
	 WHERE tit_id = titid;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;


	INSERT INTO RECETTE VALUES
		(
		exeordre,--EXE_ORDRE,
		gescode,--GES_CODE,
		jefytitre.mod_code,--MOD_CODE,
		jefytitre.pco_num,--PCO_NUM,
		jefytitre.tit_date,-- REC_DATE,
		jefytitre.tit_debiteur,-- REC_DEBITEUR,
		recid,-- REC_ID,
		jefytitre.pco_num,-- REC_IMPUTTVA,
		jefytitre.tit_interne,-- REC_INTERNE,
		jefytitre.tit_lib,-- REC_LIBELLE,
		jefytitre.org_ordre,-- REC_LIGNE_BUDGETAIRE,
		'E',-- REC_MONNAIE,
		NVL(En_Nombre(jefytitre.tit_mont),0),--HT,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TTC,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--DISQUETTE,
		NVL(En_Nombre(jefytitre.tit_monttva),0),--   REC_MONTTVA,
		jefytitre.tit_num,--   REC_NUM,
		jefytitre.tit_ordre,--   REC_ORDRE,
		jefytitre.tit_piece,--   REC_PIECE,
		jefytitre.tit_ref,--   REC_REF,
		'VALIDE',--   REC_STAT,
		'NON',--    REC_SUPPRESSION,  Modif Rod
		jefytitre.tit_type,--	 REC_TYPE,
		NULL,--	 REC_VIREMENT,
		titid,--	  TIT_ID,
		titordre,--	  TIT_ORDRE,
		utlordre,--	   UTL_ORDRE
		jefytitre.org_ordre,		--	   ORG_ORDRE --ajout rod
		jefytitre.fou_ordre,   --FOU_ORDRE --ajout rod
		modordre, --mod_ordre
		NULL,  --mor_ordre
		jefytitre.rib_ordre,
		NULL
		);

-- recuperation des echeanciers
Bordereau_Titre.Get_recette_prelevements (exeordre ,titid ,titordre ,utlordre,recid );


END;



PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
echancier_data PRELEV.ECHEANCIER%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;
personne_data grhum.v_personne%ROWTYPE;
ORIORDRE INTEGER;
modordre INTEGER;

BEGIN

-- verifier s il y a une facture_titre associee
SELECT COUNT(*) INTO cpt FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;
IF (cpt=0) THEN
	  RETURN;
END IF;


-- recup du facture_titre pour le titre concerne
SELECT * INTO FACTURE_TITRE_data
FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;

dbms_output.put_line ('titordre='||TITORDRE);


SELECT COUNT(*) INTO cpt FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';
IF (cpt=0) THEN
	  RETURN;
END IF;


SELECT * INTO echancier_data
FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';





SELECT * INTO CLIENT_data
FROM PRELEV.CLIENT
WHERE CLIENT_ORDRE = echancier_data.client_ordre
AND supprime = 'N';




-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;



-- recup ??
IF (echancier_data.con_ordre IS NOT NULL) THEN
 ORIORDRE :=Gestionorigine.traiter_convordre(echancier_data.con_ordre);
ELSE
 ORIORDRE :=Gestionorigine.traiter_orgordre(echancier_data.org_ordre);
END IF;

SELECT * INTO personne_data
FROM GRHUM.V_PERSONNE WHERE pers_id = CLIENT_data.pers_id;

INSERT INTO MARACUJA.ECHEANCIER (ECHE_AUTORIS_SIGNEE, 
FOU_ORDRE_CLIENT, CON_ORDRE, 
ECHE_DATE_1ERE_ECHEANCE, ECHE_DATE_CREATION, ECHE_DATE_MODIF, 
ECHE_ECHEANCIER_ORDRE, 
ECHE_ETAT_PRELEVEMENT, FT_ORDRE, ECHE_LIBELLE, ECHE_MONTANT, ECHE_MONTANT_EN_LETTRES, 
ECHE_NOMBRE_ECHEANCES, ECHE_NUMERO_INDEX, ORG_ORDRE, PREST_ORDRE, ECHE_PRISE_EN_CHARGE, 
ECHE_REF_FACTURE_EXTERNE, ECHE_SUPPRIME, EXE_ORDRE, TIT_ID, REC_ID, TIT_ORDRE, ORI_ORDRE, 
PERS_ID, ORG_ID, PERS_DESCRIPTION)  VALUES
(
echancier_data.AUTORIS_SIGNEE  ,--ECHE_AUTORIS_SIGNEE
echancier_data.CLIENT_ORDRE  ,--FOU_ORDRE_CLIENT
echancier_data.CON_ORDRE  ,--CON_ORDRE
echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
echancier_data.FT_ORDRE  ,--FT_ORDRE
echancier_data.LIBELLE,--ECHE_LIBELLE
echancier_data.MONTANT  ,--ECHE_MONTANT
echancier_data.MONTANT_EN_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echancier_data.NOMBRE_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
echancier_data.ORG_ORDRE  ,--ORG_ORDRE
echancier_data.PREST_ORDRE  ,--PREST_ORDRE
echancier_data.PRISE_EN_CHARGE  ,--ECHE_PRISE_EN_CHARGE
echancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
echancier_data.SUPPRIME  ,--ECHE_SUPPRIME
2006  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
TITORDRE,
ORIORDRE,--ORI_ORDRE,
CLIENT_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
NULL,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT (ECHE_ECHEANCIER_ORDRE, 
RECO_ORDRE, FOU_ORDRE, 
PREL_COMMENTAIRE, PREL_DATE_MODIF, PREL_DATE_PRELEVEMENT, 
PREL_PRELEV_DATE_SAISIE, 
PREL_PRELEV_ETAT, PREL_NUMERO_INDEX, PREL_PRELEV_MONTANT, 
PREL_PRELEV_ORDRE, RIB_ORDRE, PREL_ETAT_MARACUJA)
SELECT
ECHEANCIER_ORDRE,--ECHE_ECHEANCIER_ORDRE
FICP_ORDRE,--PREL_FICP_ORDRE
FOU_ORDRE,--FOU_ORDRE
COMMENTAIRE,--PREL_COMMENTAIRE
DATE_MODIF,--PREL_DATE_MODIF
DATE_PRELEVEMENT,--PREL_DATE_PRELEVEMENT
PRELEV_DATE_SAISIE,--PREL_PRELEV_DATE_SAISIE
PRELEV_ETAT,--PREL_PRELEV_ETAT
NUMERO_INDEX,--PREL_NUMERO_INDEX
PRELEV_MONTANT,--PREL_PRELEV_MONTANT
PRELEV_ORDRE,--PREL_PRELEV_ORDRE
RIB_ORDRE,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM PRELEV.PRELEVEMENT
WHERE ECHEANCIER_ORDRE = echancier_data.ECHEANCIER_ORDRE;

END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
	exeordre	  INTEGER;
	recid		  INTEGER;
BEGIN

	SELECT * INTO letitre
		FROM TITRE
		WHERE tit_id = titid;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

		SELECT rec_id INTO recid FROM RECETTE WHERE tit_id=titid AND ROWNUM=1;


	SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=letitre.tit_ordre AND exe_ordre=letitre.exe_ordre;
	IF cpt = 0 THEN
--	IF letitre.prest_id IS NULL THEN

		-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
		IF jefytitre.tit_type IN ('C','D') THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;


		-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			letitre.pco_num,			   --PCO_NUM
			letitre.tit_ht,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid,						   --TIT_ID,
			NULL
			);

		IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
			-- creation du titre_brouillard visa --
			INSERT INTO TITRE_BROUILLARD VALUES
				(
				NULL,  						   --ECD_ORDRE,
				letitre.exe_ordre,			   --EXE_ORDRE,
				letitre.ges_code,			   --GES_CODE,
				jefytitre.tit_imputtva,			   --PCO_NUM
				letitre.tit_tva,			   --TIB_MONTANT,
				'VISA TITRE',				   --TIB_OPERATION,
				titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
				sens,						   --TIB_SENS,
				titid	,					   --TIT_ID,
				NULL
				);
		END IF;

		-- on inverse le sens
		IF sens = 'C' THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;

		-- creation du titre_brouillard contre partie --
		INSERT INTO TITRE_BROUILLARD
			SELECT
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			ges_code,			   --GES_CODE,
			pco_num,			   --PCO_NUM
			NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid	,					   --TIT_ID,
			recid
			FROM jefy.ventil_titre
			WHERE tit_ordre = letitre.tit_ordre;


	ELSE
		Bordereau_Titre.Set_Titre_Brouillard_intern(titid);
	END IF;

END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	leplancomptable maracuja.PLAN_COMPTABLE%ROWTYPE;
	gescodecompta TITRE.ges_code%TYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
	recid		  INTEGER;
BEGIN


	SELECT * INTO letitre
	FROM TITRE
	WHERE tit_id = titid;

		SELECT rec_id INTO recid FROM RECETTE WHERE tit_id=titid AND ROWNUM=1;

	SELECT c.ges_code
		INTO gescodecompta
		FROM GESTION g, COMPTABILITE c
		WHERE g.ges_code = letitre.ges_code
		AND g.com_ordre = c.com_ordre;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

	-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
	IF jefytitre.tit_type IN ('C','D') THEN
	 sens := 'D';
	ELSE
	 sens := 'C';
	END IF;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||letitre.pco_num;

	IF cpt = 0 THEN
		SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
		WHERE pco_num = letitre.pco_num;

		maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||letitre.pco_num);
	END IF;
--	letitre.pco_num := '18'||letitre.pco_num;

	-- creation du titre_brouillard visa --
	INSERT INTO TITRE_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		letitre.ges_code,			   --GES_CODE,
		'18'||letitre.pco_num,			   --PCO_NUM
		letitre.tit_ht,			   --TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid	,					   --TIT_ID,
		NULL
		);

	IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
	-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			jefytitre.tit_imputtva,			   --PCO_NUM
			letitre.tit_tva,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid		,				   --TIT_ID,
			NULL
			);
	END IF;

	-- on inverse le sens
	IF sens = 'C' THEN
	   sens := 'D';
	ELSE
		sens := 'C';
	END IF;

	-- creation du titre_brouillard contre partie --
	INSERT INTO TITRE_BROUILLARD
		SELECT
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			   --GES_CODE,
		'181',			   --PCO_NUM
		NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid	,					   --TIT_ID,
		recid
		FROM jefy.ventil_titre
		WHERE tit_ordre = letitre.tit_ordre;



END;



PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
  niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Titre_Non_Admis
IS

--version 1.2.0 20/12/2005 - Multi exercices jefy

   PROCEDURE expedier_bttna (brjordre INTEGER, exeordre INTEGER)
   IS
      cpt            INTEGER;
      borordre       INTEGER;
      nume           INTEGER;
      nb             INTEGER;
      tbolibelle     TYPE_BORDEREAU.tbo_libelle%TYPE;
      titordre       TITRE.tit_ordre%TYPE;
      letitre        TITRE%ROWTYPE;
--ladepense  depense%rowtype;
      titid          TITRE.tit_id%TYPE;
      brjnum         BORDEREAU_REJET.brj_num%TYPE;
      lestitordres   VARCHAR2 (1000);
      ex             INTEGER;

--lesdepordres varchar2(1000);

      -- les mandats rejetes --
      CURSOR c1
      IS
         SELECT *
           FROM TITRE
          WHERE brj_ordre = brjordre;
-- depenses a supprimer --
/*cursor c2 is
select * from depense
where man_id = manid
and dep_suppression = 'OUI' ;
*/
   BEGIN
/*
lesmanordre
   "manordre$expli$manordre$expli$.......$manordre$expli$$"
   lesdepordres
   "depordre$depordre$.......$depordre$$"
   */
      lestitordres := NULL;

      SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';


	   IF ex IS NULL
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'PARAMETRE EXERCICE NON DEFINI');
            END IF;


      IF exeordre = ex
      THEN
--lesdepordres:=null;
         SELECT bor_ordre
           INTO borordre
           FROM jefy.bordero
          WHERE bor_ordre IN (SELECT MAX (bor_ordre)
                                FROM TITRE
                               WHERE brj_ordre = brjordre);

         SELECT brj_num
           INTO nume
           FROM BORDEREAU_REJET
          WHERE brj_ordre = brjordre;

         SELECT COUNT (*)
           INTO nb
           FROM jefy.bordero_rejet
          WHERE bor_ordre = borordre + 1000000;

         IF nb = 0
         THEN
            INSERT INTO jefy.bordero_rejet
               SELECT bor_ordre + 1000000, bor_date, 'N', bor_visa, bor_quoi,
                      bor_num, ges_code, dsk_numero, nume, NULL
                 FROM jefy.bordero
                WHERE bor_ordre = borordre;

            SELECT f.tbo_libelle, b.brj_num
              INTO tbolibelle, brjnum
              FROM BORDEREAU_REJET b, TYPE_BORDEREAU f
             WHERE b.brj_ordre = brjordre AND b.tbo_ordre = f.tbo_ordre;

            IF tbolibelle != 'BORDEREAU DE TITRE NON ADMIS'
            THEN
               RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTTNA');
            END IF;

            OPEN c1;

            LOOP
               FETCH c1
                INTO letitre;

               EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
               UPDATE jefy.TITRE
                  SET tit_stat = 'A'
                WHERE tit_ordre = letitre.tit_ordre;

               lestitordres :=
                     lestitordres
                  || '$'
                  || letitre.tit_ordre
                  || '$'
                  || letitre.tit_motif_rejet;
               titid := letitre.tit_ordre;
            END LOOP;

            CLOSE c1;

-- enlever le premier $ --
            SELECT SUBSTR (lestitordres, 2, LENGTH (lestitordres))
              INTO lestitordres
              FROM DUAL;

            lestitordres := lestitordres || '$$';
            jefy.maj_titre_rejet (lestitordres);
         END IF;
      ELSE
         IF exeordre = 2005
         THEN
--lesdepordres:=null;
            SELECT bor_ordre
              INTO borordre
              FROM jefy05.bordero
             WHERE bor_ordre IN (SELECT MAX (bor_ordre)
                                   FROM TITRE
                                  WHERE brj_ordre = brjordre);

            SELECT brj_num
              INTO nume
              FROM BORDEREAU_REJET
             WHERE brj_ordre = brjordre;

            SELECT COUNT (*)
              INTO nb
              FROM jefy05.bordero_rejet
             WHERE bor_ordre = borordre + 1000000;

            IF nb = 0
            THEN
               INSERT INTO jefy05.bordero_rejet
                  SELECT bor_ordre + 1000000, bor_date, 'N', bor_visa,
                         bor_quoi, bor_num, ges_code, dsk_numero, nume, NULL
                    FROM jefy05.bordero
                   WHERE bor_ordre = borordre;

               SELECT f.tbo_libelle, b.brj_num
                 INTO tbolibelle, brjnum
                 FROM BORDEREAU_REJET b, TYPE_BORDEREAU f
                WHERE b.brj_ordre = brjordre AND b.tbo_ordre = f.tbo_ordre;

               IF tbolibelle != 'BORDEREAU DE TITRE NON ADMIS'
               THEN
                  RAISE_APPLICATION_ERROR (-20001, 'CECI N EST PAS UN BTTNA');
               END IF;

               OPEN c1;

               LOOP
                  FETCH c1
                   INTO letitre;

                  EXIT WHEN c1%NOTFOUND;

-- mise a jour de l etat --
                  UPDATE jefy05.TITRE
                     SET tit_stat = 'A'
                   WHERE tit_ordre = letitre.tit_ordre;

                  lestitordres :=
                        lestitordres
                     || '$'
                     || letitre.tit_ordre
                     || '$'
                     || letitre.tit_motif_rejet;
                  titid := letitre.tit_ordre;
               END LOOP;

               CLOSE c1;

-- enlever le premier $ --
               SELECT SUBSTR (lestitordres, 2, LENGTH (lestitordres))
                 INTO lestitordres
                 FROM DUAL;

               lestitordres := lestitordres || '$$';
               jefy05.maj_titre_rejet (lestitordres);
            END IF;
		 ELSE
		 	 RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');

         END IF;
      END IF;
   END;
END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Titre_05 IS
--version 1.1.8 31/05/2005 - modifs prestations internes
--version 1.1.7 26/04/2005 - prestations internes
--version 1.1.8 23/06/2005 - structure table recette
--version 1.2.0 20/12/2005 - Multi exercices jefy
PROCEDURE Get_Btte_Jefy
(borordre NUMBER,exeordre NUMBER)

IS
	cpt 	 	INTEGER;

	lebordereau jefy05.bordero%ROWTYPE;

	agtordre 	jefy05.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;

BEGIN


	dbms_output.put_line('exercice : '||exeordre);

	-- est ce un bordereau de titre --
	SELECT COUNT(*) INTO cpt FROM jefy05.TITRE
		   WHERE bor_ordre = borordre;

	IF cpt != 0 THEN

		-- le bordereau est il deja vis  ? --
		tboordre:=Gestionorigine.recup_type_bordereau_titre(borordre, exeordre);

		SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;




		-- l exercice est il encore ouvert ? --


		-- pas de raise mais pas de recup s il est deja dans maracuja --
		IF cpt = 0 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
				FROM jefy05.bordero
				WHERE bor_ordre = borordre;
			/*
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy05.TITRE
			WHERE tit_ordre =
			 ( SELECT MAX(tit_ordre)
			   FROM jefy05.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre
			  );

			-- recuperation du type de bordereau --
			SELECT COUNT(*) INTO cpt
			FROM OLD_AGENT_TYPE_BORD
			WHERE agt_ordre = agtordre;

			IF cpt <> 0 THEN
			 SELECT tbo_ordre INTO tboordre
			 FROM OLD_AGENT_TYPE_BORD
			 WHERE agt_ordre = agtordre;
			ELSE
			 SELECT tbo_ordre INTO tboordre
			 FROM TYPE_BORDEREAU
			 WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' and EXE_ORDRE=exeordre);
			END IF;
			*/

			-- creation du bor_id --
			SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

			-- recuperation de l utilisateur --
			/*select utl_ordre into utlordre
			from utilisateur
			where agt_ordre = agtordre;
			*/
			-- TEST  TEST  TEST  TEST  TEST
			SELECT 1 INTO utlordre
				   FROM dual;
			-- TEST  TEST  TEST  TEST  TEST

			-- creation du bordereau --
			INSERT INTO BORDEREAU VALUES (
				NULL , 	 	 	      --BOR_DATE_VISA,
				'VALIDE',			  --BOR_ETAT,
				borid,				  --BOR_ID,
				leBordereau.bor_num,  --BOR_NUM,
				leBordereau.bor_ordre,--BOR_ORDRE,
				exeordre,	  		  --EXE_ORDRE,
				leBordereau.ges_code, --GES_CODE,
				tboordre,			  --TBO_ORDRE,
				utlordre,		 	  --UTL_ORDRE,
				NULL				  --UTL_ORDRE_VISA
				);

			Get_Titre_Jefy(exeordre,borordre,borid,utlordre,agtordre);

		END IF;
		--else
		-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
	END IF;

END;


PROCEDURE Get_Titre_Jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	jefytitre jefy05.TITRE%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	titid 		 TITRE.tit_id%TYPE;

	titoriginekey  TITRE.tit_ORGINE_KEY%TYPE;
	titoriginelib TITRE.tit_ORIGINE_LIB%TYPE;
	ORIORDRE 	   TITRE.ORI_ORDRE%TYPE;
	PRESTID 	   TITRE.PREST_ID%TYPE;
	TORORDRE 	   TITRE.TOR_ORDRE%TYPE;
	modordre	   TITRE.mod_ordre%TYPE;
	presid		   INTEGER;
	cpt			   INTEGER;

	CURSOR lesJefytitres IS
	SELECT *
	FROM jefy05.TITRE
	WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefytitres;
	LOOP
		FETCH lesJefytitres INTO jefytitre;
			  EXIT WHEN lesJefytitres%NOTFOUND;

		-- recup ??
		IF (jefytitre.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(jefytitre.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(jefytitre.org_ordre);
		END IF;

		titoriginekey  :=NULL;
		titoriginelib   :=NULL;

		-- recup du torordre origine JEFYCO
		TORORDRE 	     :=1;
		-- JEFYCO
		torordre := 1;
		-- recup du mode de paiement OP ordo
		modordre	     :=NULL;

		dbms_output.put_line('titreOrdre : '||jefytitre.tit_ordre);
		dbms_output.put_line('mod_code : '||jefytitre.mod_code);


		IF jefytitre.tit_type = 'C' THEN
		   SELECT mod_ordre INTO modordre
		   FROM MODE_PAIEMENT
		   WHERE mod_code = jefytitre.mod_code
		   AND exe_ordre = exeordre;
		ELSE
			modordre := NULL;
		END IF;



		-- recup du prestid
		presid		     :=NULL;


		--TODO des gescode sont nuls, recuperer le gescode de la table comptabilite ?
		IF jefytitre.ges_code IS NULL THEN
			 SELECT DISTINCT ges_code INTO gescode
			 FROM BORDEREAU
			 WHERE bor_id=borid;
		ELSE
		 	gescode:=jefytitre.ges_code;
		END IF;

		SELECT titre_seq.NEXTVAL INTO titid FROM dual;

		INSERT INTO TITRE (
		   BOR_ID,
		   BOR_ORDRE,
		   BRJ_ORDRE,
		   EXE_ORDRE,
		   GES_CODE,
		   MOD_ORDRE,
		   ORI_ORDRE,
		   PCO_NUM,
		   PREST_ID,
		   TIT_DATE_REMISE,
		   TIT_DATE_VISA_PRINC,
		   TIT_ETAT,
		   TIT_ETAT_REMISE,
		   TIT_HT,
		   TIT_ID,
		   TIT_MOTIF_REJET,
		   TIT_NB_PIECE,
		   TIT_NUMERO,
		   TIT_NUMERO_REJET,
		   TIT_ORDRE,
		   TIT_ORGINE_KEY,
		   TIT_ORIGINE_LIB,
		   TIT_TTC,
		   TIT_TVA,
		   TOR_ORDRE,
		   UTL_ORDRE,
		   ORG_ORDRE,
		   FOU_ORDRE,
		   MOR_ORDRE,
		   PAI_ORDRE,
		   rib_ordre_ordonnateur,
		   rib_ordre_comptable,
		   tit_libelle)
		VALUES
			(
			borid,--BOR_ID,
			borordre,--BOR_ORDRE,
			NULL,--BRJ_ORDRE,
			exeordre,--EXE_ORDRE,
			gescode,--GES_CODE,
			modordre,--MOD_ORDRE,
			oriordre,--ORI_ORDRE,
			jefytitre.pco_num,--PCO_NUM,
			jefytitre.pres_ordre,--PREST_ID,
			NULL,--TIT_DATE_REMISE,
			NULL,--TIT_DATE_VISA_PRINC,
			'ATTENTE',--TIT_ETAT,
			'ATTENTE',--TIT_ETAT_REMISE,
			NVL(En_Nombre(jefytitre.tit_mont),0),--TIT_HT,
			titid,--TIT_ID,
			NULL,--TIT_MOTIF_REJET,
			jefytitre.tit_piece,--TIT_NB_PIECE,
			jefytitre.tit_num,--TIT_NUMERO,
			NULL,--TIT_NUMERO_REJET,
			jefytitre.tit_ordre,--TIT_ORDRE,
			titoriginekey,--TIT_ORGINE_KEY,
			titoriginelib,--TIT_ORIGINE_LIB,
			NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TIT_TTC,
			NVL(En_Nombre(jefytitre.tit_monttva),0),--TIT_TVA,
			torordre,--TOR_ORDRE,
			utlordre,--UTL_ORDRE
			jefytitre.org_ordre,		--ORG_ORDRE,
			jefytitre.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
			modordre,				 --MOR_ORDRE
			NULL, -- VIR_ORDRE
			jefytitre.rib_ordre,
			jefytitre.rib_ordre,
			jefytitre.tit_lib
			);

		--on met a jour le type de bordereau s'il s'agit d'un bordereau de prestation interne
		SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=jefytitre.tit_ordre;
		IF cpt != 0 THEN
--		if jefytitre.pres_ordre is not null then
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;

		Get_Recette_Jefy(exeordre,titid,jefytitre.tit_ordre,utlordre);
		Set_Titre_Brouillard(titid);

	END LOOP;

	CLOSE lesJefytitres;

END;




PROCEDURE Get_Recette_Jefy
(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefytitre		 jefy05.TITRE%ROWTYPE;
	--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	--fouadresse  	 DEPENSE.dep_adresse%TYPE;
	--founom  		 DEPENSE.dep_fournisseur%TYPE;
	--lotordre  		 DEPENSE.dep_lot%TYPE;
	--marordre		 DEPENSE.dep_marches%TYPE;
	--fouordre		 DEPENSE.fou_ordre%TYPE;
	--gescode			 DEPENSE.ges_code%TYPE;
	cpt				 INTEGER;
	recid			 RECETTE.rec_id%TYPE;
	rectype			 RECETTE.rec_type%TYPE;
	gescode			 TITRE.GES_CODE%TYPE;

	modordre	   DEPENSE.mod_ordre%TYPE;


BEGIN

	SELECT * INTO jefytitre
		FROM jefy05.TITRE
		WHERE tit_ordre = titordre;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;

	rectype := NULL;

	-- ajout rod
	IF jefytitre.tit_type = 'C' THEN
	   SELECT mod_ordre INTO modordre
	   FROM MODE_PAIEMENT
	   WHERE mod_code = jefytitre.mod_code
	   AND exe_ordre = exeordre;
	ELSE
		modordre := NULL;
	END IF;


	IF jefytitre.ges_code IS NULL THEN
	 SELECT DISTINCT ges_code INTO gescode
	 FROM jefy05.ventil_titre
	 WHERE tit_ordre = jefytitre.tit_ordre;
	ELSE
	 gescode:=jefytitre.ges_code;
	END IF;

	SELECT recette_seq.NEXTVAL INTO recid FROM dual;


	INSERT INTO RECETTE VALUES
		(
		exeordre,--EXE_ORDRE,
		gescode,--GES_CODE,
		jefytitre.mod_code,--MOD_CODE,
		jefytitre.pco_num,--PCO_NUM,
		jefytitre.tit_date,-- REC_DATE,
		jefytitre.tit_debiteur,-- REC_DEBITEUR,
		recid,-- REC_ID,
		jefytitre.pco_num,-- REC_IMPUTTVA,
		jefytitre.tit_interne,-- REC_INTERNE,
		jefytitre.tit_lib,-- REC_LIBELLE,
		jefytitre.org_ordre,-- REC_LIGNE_BUDGETAIRE,
		'E',-- REC_MONNAIE,
		NVL(En_Nombre(jefytitre.tit_mont),0),--HT,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TTC,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--DISQUETTE,
		NVL(En_Nombre(jefytitre.tit_monttva),0),--   REC_MONTTVA,
		jefytitre.tit_num,--   REC_NUM,
		jefytitre.tit_ordre,--   REC_ORDRE,
		jefytitre.tit_piece,--   REC_PIECE,
		jefytitre.tit_ref,--   REC_REF,
		'VALIDE',--   REC_STAT,
		'NON',--    REC_SUPPRESSION,  Modif Rod
		jefytitre.tit_type,--	 REC_TYPE,
		NULL,--	 REC_VIREMENT,
		titid,--	  TIT_ID,
		titordre,--	  TIT_ORDRE,
		utlordre,--	   UTL_ORDRE
		jefytitre.org_ordre,		--	   ORG_ORDRE --ajout rod
		jefytitre.fou_ordre,   --FOU_ORDRE --ajout rod
		modordre, --mod_ordre
		NULL,  --mor_ordre
		jefytitre.rib_ordre,
		NULL
		);



END;


PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy05.TITRE%ROWTYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
BEGIN

	SELECT * INTO letitre
		FROM TITRE
		WHERE tit_id = titid;

	SELECT * INTO jefytitre
		FROM jefy05.TITRE
		WHERE tit_ordre = letitre.tit_ordre;



	SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=letitre.tit_ordre;
	IF cpt = 0 THEN
--	IF letitre.prest_id IS NULL THEN

		-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
		IF jefytitre.tit_type IN ('C','D') THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;


		-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			letitre.pco_num,			   --PCO_NUM
			letitre.tit_ht,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);

		IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
			-- creation du titre_brouillard visa --
			INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
				(
				NULL,  						   --ECD_ORDRE,
				letitre.exe_ordre,			   --EXE_ORDRE,
				letitre.ges_code,			   --GES_CODE,
				jefytitre.tit_imputtva,			   --PCO_NUM
				letitre.tit_tva,			   --TIB_MONTANT,
				'VISA TITRE',				   --TIB_OPERATION,
				titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
				sens,						   --TIB_SENS,
				titid						   --TIT_ID,
				);
		END IF;

		-- on inverse le sens
		IF sens = 'C' THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;

		-- creation du titre_brouillard contre partie --
		INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID)
			SELECT
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			ges_code,			   --GES_CODE,
			pco_num,			   --PCO_NUM
			NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			FROM jefy05.ventil_titre
			WHERE tit_ordre = letitre.tit_ordre;


	ELSE
		Bordereau_Titre_05.Set_Titre_Brouillard_intern(titid);
	END IF;

END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy05.TITRE%ROWTYPE;
	leplancomptable PLAN_COMPTABLE%ROWTYPE;
	gescodecompta TITRE.ges_code%TYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
BEGIN


	SELECT * INTO letitre
	FROM TITRE
	WHERE tit_id = titid;



	SELECT c.ges_code
		INTO gescodecompta
		FROM GESTION g, COMPTABILITE c
		WHERE g.ges_code = letitre.ges_code
		AND g.com_ordre = c.com_ordre;

	SELECT * INTO jefytitre
		FROM jefy05.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

	-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
	IF jefytitre.tit_type IN ('C','D') THEN
	 sens := 'D';
	ELSE
	 sens := 'C';
	END IF;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||letitre.pco_num;

	IF cpt = 0 THEN
		SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
		WHERE pco_num = letitre.pco_num;

		maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||letitre.pco_num);
	END IF;
--	letitre.pco_num := '18'||letitre.pco_num;

	-- creation du titre_brouillard visa --
	INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
		(
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		letitre.ges_code,			   --GES_CODE,
		'18'||letitre.pco_num,			   --PCO_NUM
		letitre.tit_ht,			   --TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		);

	IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
	-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID) VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			jefytitre.tit_imputtva,			   --PCO_NUM
			letitre.tit_tva,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);
	END IF;

	-- on inverse le sens
	IF sens = 'C' THEN
	   sens := 'D';
	ELSE
		sens := 'C';
	END IF;

	-- creation du titre_brouillard contre partie --
	INSERT INTO TITRE_BROUILLARD(ECD_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, TIB_MONTANT, TIB_OPERATION, TIB_ORDRE, TIB_SENS, TIT_ID)
		SELECT
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			   --GES_CODE,
		'181',			   --PCO_NUM
		NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		FROM jefy05.ventil_titre
		WHERE tit_ordre = letitre.tit_ordre;



END;



PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
  niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.cptefiutil IS
PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER, libelle VARCHAR, rtatcomp VARCHAR)
IS

-- version du 03/03/2005 - Prestations internes

/*
RIVALLAND FREDRIC
CRIG
18/01/2005 */

legescode GESTION.ges_code%TYPE;
lepconum PLAN_COMPTABLE.pco_num%TYPE;
lesolde  NUMBER(12,2);
debit  NUMBER(12,2);
credit NUMBER(12,2);
credit120 NUMBER(12,2);
debit120 NUMBER(12,2);
credit129 NUMBER(12,2);
debit129 NUMBER(12,2);


CURSOR solde6 IS
 SELECT ges_code,pco_num, solde_crediteur
 FROM CFI_solde_6_7
 WHERE solde_crediteur != 0
 AND (pco_num LIKE '6%' OR pco_num LIKE '186%')
 AND exe_ordre = exeordre;

CURSOR solde7 IS
 SELECT ges_code,pco_num, solde_crediteur
 FROM CFI_solde_6_7
 WHERE solde_crediteur != 0
 AND (pco_num LIKE '7%' OR pco_num LIKE '187%')
 AND exe_ordre = exeordre;

CURSOR lesgescode IS
 SELECT DISTINCT ges_code, pco_num_185
 FROM GESTION_EXERCICE
 WHERE exe_ordre = exeordre;

 CURSOR lessacds IS
 SELECT DISTINCT ges_code
 FROM GESTION_EXERCICE
 WHERE exe_ordre = exeordre
 AND pco_num_185 IS NOT NULL;


 nb INTEGER;
 gescodeagence COMPTABILITE.ges_code%TYPE;
 comordre COMPTABILITE.com_ordre%TYPE;
 topordre TYPE_OPERATION.top_ordre%TYPE;
 TJOORDRE TYPE_JOURNAL.tjo_ordre%TYPE;
 monecdordre ECRITURE.ecr_ordre%TYPE;
 ecdordre ECRITURE_DETAIL.ecd_ordre%TYPE;

 exeexercice exercice.exe_exercice%TYPE;

BEGIN


-- creation de l'ecriture de solde --
SELECT top_ordre
INTO topordre
FROM TYPE_OPERATION
WHERE top_libelle = 'ECRITURE SOLDE 6 ET 7';


SELECT tjo_ordre
INTO TJOORDRE
FROM TYPE_JOURNAL
WHERE tjo_libelle = 'JOURNAL FIN EXERCICE';




SELECT com_ordre, ges_code
INTO comordre, gescodeagence
FROM COMPTABILITE;


SELECT exe_exercice INTO exeexercice FROM exercice WHERE exe_ordre = exeordre;


-- creation de lecriture  --
monecdordre := maracuja.api_plsql_journal.creerecriture(
comordre,
TO_DATE('31/12/'||exeexercice, 'dd/mm/yyyy'),
libelle,
exeordre,
NULL,           --ORIORDRE,
tjoordre,
topordre,
utlordre
);


-- Calcul du r�sultat par composante ---
-- Etablissement + SACD --
IF rtatcomp = 'O' THEN

	-- creation de l ecriture de resultat --
	OPEN lesgescode;
	LOOP
	FETCH lesgescode INTO legescode,lepconum;
	EXIT WHEN lesgescode%NOTFOUND;

	SELECT COUNT(*) INTO nb FROM cfi_solde_6_7
	WHERE exe_ordre = exeordre
	AND ges_code = legescode;
	IF nb != 0 THEN
	--  composante SACD --
	--       IF lepco_num is not null  THEN
           SELECT NVL(-SUM(solde_crediteur),0) INTO debit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '6%' OR pco_num LIKE '186%')
		   AND ges_code = legescode AND exe_ordre = exeordre;


           SELECT NVL(SUM(solde_crediteur),0) INTO credit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '7%' AND pco_num LIKE '187%')
		   AND ges_code = legescode AND exe_ordre=exeordre;

		IF debit > credit THEN
	   	-- RESULTAT SOLDE DEBITEUR 129
	   	-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
       	--  contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
		ELSE
		-- RESULTAT SOLDE CREDITEUR 120 --
		-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
       	                          (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

	   	-- contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

		END IF;

	END IF;
	END LOOP;
	CLOSE lesgescode;

ELSE
	-- resultat = 'Etablissement' --

	SELECT COUNT(*) INTO nb FROM cfi_solde_6_7
	WHERE exe_ordre = exeordre AND ges_code IN
	(SELECT ges_code FROM GESTION_EXERCICE WHERE exe_ordre = exeordre AND pco_num_185 IS NULL);

	IF nb != 0 THEN
           SELECT NVL(-SUM(solde_crediteur),0) INTO debit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '6%' OR pco_num LIKE '186%') AND exe_ordre = exeordre AND ges_code IN
	(SELECT ges_code FROM GESTION_EXERCICE WHERE exe_ordre = exeordre AND pco_num_185 IS NULL);


           SELECT NVL(SUM(solde_crediteur),0) INTO credit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '7%' OR pco_num LIKE '187%') AND exe_ordre = exeordre AND ges_code IN
	(SELECT ges_code FROM GESTION_EXERCICE WHERE exe_ordre = exeordre AND pco_num_185 IS NULL);


		IF debit > credit THEN
	   	-- RESULTAT SOLDE DEBITEUR 129
	   	-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '129'           --PCONUM
                                 );
       	--  contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '129'           --PCONUM
                                 );
		ELSE
		-- RESULTAT SOLDE CREDITEUR 120 --
		-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
       	                          (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '120'           --PCONUM
                                 );

	   	-- contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  gescodeagence, --GESCODE,
                                  '120'           --PCONUM
                                 );

		END IF;
	END IF;

	-- R�sultat des SACD ---

	OPEN lessacds;
	LOOP
	FETCH lessacds INTO legescode;
	EXIT WHEN lessacds%NOTFOUND;


	SELECT COUNT(*) INTO nb FROM cfi_solde_6_7
	WHERE exe_ordre = exeordre
	AND ges_code = legescode;
	IF nb != 0 THEN
	--  SACD --
	--       IF lepco_num is not null  THEN
           SELECT NVL(-SUM(solde_crediteur),0) INTO debit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '6%' OR pco_num LIKE '186%')
		   AND ges_code = legescode AND exe_ordre = exeordre;


           SELECT NVL(SUM(solde_crediteur),0) INTO credit FROM cfi_solde_6_7
           WHERE (pco_num LIKE '7%' OR pco_num LIKE '187%')
		   AND ges_code = legescode AND exe_ordre=exeordre;


		IF debit > credit THEN
	   	-- RESULTAT SOLDE DEBITEUR 129
	   	-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
       	--  contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '129'           --PCONUM
                                 );
		ELSE
		-- RESULTAT SOLDE CREDITEUR 120 --
		-- contre-partie classe 7 cr�dit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
       	                          (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  credit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

	   	-- contre-partie classe 6 d�bit
       	ecdordre :=
       	maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  debit,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  '120'           --PCONUM
                                 );

		END IF;

	END IF;
	END LOOP;
	CLOSE lessacds;

END IF;



-- Solde classe 6 et 186

OPEN solde6;
LOOP
FETCH solde6 INTO legescode,lepconum,lesolde;
EXIT WHEN solde6%NOTFOUND;
IF lesolde < 0 THEN
       ecdordre :=
       maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  -lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
        ELSE

       ecdordre :=
       maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
END IF;


END LOOP;
CLOSE solde6;


-- Solde classe 7 et 187

OPEN solde7;
LOOP
FETCH solde7 INTO legescode,lepconum,lesolde;
EXIT WHEN solde7%NOTFOUND;
IF lesolde < 0 THEN
       ecdordre :=
       maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  -lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'C',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
        ELSE

       ecdordre :=
       maracuja.api_plsql_journal.creerecrituredetail
                                 (NULL,   --ECDCOMMENTAIRE,
                                  libelle,
                                  --ECDLIBELLE,
                                  lesolde,  --ECDMONTANT,
                                  NULL,    --ECDSECONDAIRE,
                                  'D',           --ECDSENS,
                                  monecdordre,  --ECRORDRE,
                                  legescode, --GESCODE,
                                  lepconum          --PCONUM
                                 );
END IF;


END LOOP;
CLOSE solde7;

-- on numerote pour eviter une numerotation automatique.
-- voir procedure numeroter_ecriture
UPDATE ECRITURE SET ecr_numero = 99999999 WHERE ecr_ordre = monecdordre;


END;


--------------------------------------------------------------------------------


PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS


/*
/*
RIVALLAND FREDRIC
CRIG
18/01/2005


a utiliser uniquement pour numeroter
definitivement lecriture de solde des
classes 6 et 7


*/
numero INTEGER;
BEGIN


SELECT ecr_numero INTO numero FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF numero != 99999999 THEN
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE MODIFIER UNE ECRITURE DU JOURNAL !');
ELSE
UPDATE ECRITURE SET ecr_numero = 0 WHERE ecr_ordre = ecrordre;
maracuja.api_plsql_journal.validerEcriture (ecrordre);
END IF;
END;


--------------------------------------------------------------------------------



PROCEDURE annuler_ecriture (monecdordre INTEGER)
IS


/*
/*
RIVALLAND FREDRIC
CRIG
18/01/2005


a utiliser uniquement pour ANNULER
definitivement l'ecriture de solde des
classes 6 et 7


*/
numero INTEGER;
BEGIN


SELECT ecr_numero INTO numero FROM ECRITURE WHERE ecr_ordre = monecdordre;
IF numero != 99999999 THEN
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
ELSE
 DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = monecdordre;
 DELETE FROM ECRITURE WHERE ecr_ordre = monecdordre;
END IF;
END;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Gestionorigine IS
--version 1.1.7 31/05/2005 - mise a jour libelle origine dans le cas des conventions
--version 1.1.6 27/04/2005 - ajout maj_origine
--version 1.2.0 20/12/2005 - Multi exercices jefy
--version 1.2.1 04/01/2006

FUNCTION traiter_orgordre (orgordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

IF orgordre IS NULL THEN RETURN NULL; END IF;

-- recup du type_origine --
SELECT top_ordre INTO topordre FROM TYPE_OPERATION
WHERE top_libelle = 'OPERATION LUCRATIVE';


        
-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ORDRE'
AND ORI_ENTITE ='JEFY.ORGAN'
AND ORI_KEY_ENTITE	=orgordre;

IF cpt >= 1 THEN
	SELECT ori_ordre INTO cpt FROM ORIGINE
	WHERE ORI_KEY_NAME = 'ORG_ORDRE'
	AND ORI_ENTITE ='JEFY.ORGAN'
	AND ORI_KEY_ENTITE	=orgordre
	AND ROWNUM=1;

ELSE
	SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

	-- recup de la LIGNE BUDGETAIRE --
	--le libelle utilisateur pour le suivie en compta --
	SELECT org_comp||'-'||org_lbud||'-'||org_uc
	 INTO orilibelle
	FROM jefy.organ
	WHERE org_ordre = orgordre;

	INSERT INTO ORIGINE
	(ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
	VALUES ('JEFY.ORGAN','ORG_ORDRE',orilibelle,cpt,orgordre,topordre);

END IF;

RETURN cpt;

END;


FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
BEGIN

IF orgid IS NULL THEN RETURN NULL; END IF;

SELECT COUNT(*) INTO cpt 
FROM convention.CONVENTION_LIMITATIVE
WHERE org_id = orgid AND exe_ordre = exeordre;

IF cpt >0 THEN
 -- recup du type_origine CONVENTION--
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

 SELECT DISTINCT con_ordre INTO convordre 
 FROM convention.CONVENTION_LIMITATIVE
 WHERE org_id = orgid
 AND exe_ordre = exeordre;


 SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) 
 INTO orilibelle 
 FROM convention.contrat
 WHERE con_ordre = convordre;

ELSE
 SELECT COUNT(*) INTO cpt 
 FROM jefy_admin.organ 
 WHERE org_id = orgid
 AND org_lucrativite = 1;

 IF cpt = 1 THEN
 -- recup du type_origine OPERATION LUCRATIVE --
 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
 WHERE top_libelle = 'OPERATION LUCRATIVE';
 
 --le libelle utilisateur pour le suivie en compta --
 SELECT org_UB||'-'||org_CR||'-'||org_souscr
 INTO orilibelle
 FROM jefy_admin.organ
 WHERE org_id = orgid;
        
 ELSE
  RETURN NULL;
 END IF;
END IF;

-- l origine est t elle deja  suivie --
SELECT COUNT(*)INTO cpt FROM ORIGINE
WHERE ORI_KEY_NAME = 'ORG_ID'
AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
AND ORI_KEY_ENTITE	=orgid;

IF cpt >= 1 THEN
	SELECT ori_ordre INTO cpt FROM ORIGINE
	WHERE ORI_KEY_NAME = 'ORG_ID'
	AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
	AND ORI_KEY_ENTITE	=orgid
	AND ROWNUM=1;

ELSE
	SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

	INSERT INTO ORIGINE (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
	VALUES ('JEFY_ADMIN','ORG_ID',orilibelle,cpt,orgid,topordre);

END IF;

RETURN cpt;

END;


FUNCTION traiter_convordre (convordre INTEGER)
RETURN INTEGER

IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	IF convordre IS NULL THEN RETURN NULL; END IF;

	-- recup du type_origine --
	SELECT top_ordre INTO topordre FROM TYPE_OPERATION
	WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

	-- l origine est t elle deja  suivie --
	SELECT COUNT(*)INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;


	-- recup de la convention --
	--le libelle utilisateur pour le suivie en compta --
--	select CON_REFERENCE_EXTERNE||' '||CON_OBJET into orilibelle from convention.contrat where con_ordre=convordre;
--on change le libelle (demande INP Toulouse 30/05/2005)
	SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM convention.contrat WHERE con_ordre=convordre;


	-- l'origine est deja referencee
	IF cpt = 1 THEN
		SELECT ori_ordre INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;

		--on met a jour le libelle
		UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

	-- il faut creer l'origine
	ELSE
		SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
		INSERT INTO ORIGINE VALUES (
				'CONVENTION.CONTRAT' ,--	  USER.TABLE  -
				'CONV_ORDRE',-- 	  PRIMARY KEY -
				ORILIBELLE,-- 	  LIBELLE UTILISATEUR -
				cpt ,--		  ID -
				convordre,--	  VALEUR DE LA KEY -
				topordre--		  type d origine -
			);

	END IF;

	RETURN cpt;
END;


FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
letitrejefy jefy.TITRE%ROWTYPE;
BEGIN


IF exeordre=2005 THEN
	SELECT * INTO lebordereau
	FROM jefy05.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT * INTO letitrejefy FROM jefy05.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
	   FROM jefy05.TITRE
	   WHERE bor_ordre =lebordereau.bor_ordre);
ELSE
	SELECT * INTO lebordereau
	FROM jefy.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT * INTO letitrejefy FROM jefy.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
	   FROM jefy.TITRE
	   WHERE bor_ordre =lebordereau.bor_ordre);
END IF;




IF letitrejefy.tit_type ='V' THEN
   SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REVERSEMENTS';
ELSE
  IF letitrejefy.tit_type ='D' THEN
    SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REDUCTIONS';
  ELSE
    IF letitrejefy.tit_type ='P' THEN
      SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU D ORDRE DE PAIEMENT';
    ELSE
	  SELECT tbo_ordre INTO tboordre
      FROM TYPE_BORDEREAU
      WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' AND EXE_ORDRE=exeordre);
    END IF;
  END IF;
END IF;


RETURN tboordre;
END;

FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;
fouordrepapaye jefy.MANDAT.fou_ordre%TYPE;
agtordre 	jefy.facture.agt_ordre%TYPE;
fouordre 	jefy.MANDAT.fou_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN

IF exeordre=2005 THEN
	-- recup des infos --
	SELECT * INTO lebordereau
	FROM jefy05.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT MAX(agt_ordre) INTO agtordre
	FROM jefy05.facture
	WHERE man_ordre =
	 ( SELECT MAX(man_ordre)
	   FROM jefy05.MANDAT
	   WHERE bor_ordre =borordre
	  );

	SELECT MAX(fou_ordre) INTO fouordre
	   FROM jefy05.MANDAT
	   WHERE bor_ordre =borordre;
ELSE
	-- recup des infos --
	SELECT * INTO lebordereau
	FROM jefy.bordero
	WHERE bor_ordre = borordre;

	-- recup de l agent de ce bordereau --
	SELECT MAX(agt_ordre) INTO agtordre
	FROM jefy.facture
	WHERE man_ordre =
	 ( SELECT MAX(man_ordre)
	   FROM jefy.MANDAT
	   WHERE bor_ordre =borordre
	  );

	SELECT MAX(fou_ordre) INTO fouordre
	   FROM jefy.MANDAT
	   WHERE bor_ordre =borordre;
END IF;

-- recuperation du type de bordereau --
SELECT COUNT(*) INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
 (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');


IF (fouordrepapaye <> 0) THEN
   SELECT fou_ordre INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
   (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
ELSE
	fouordrepapaye := NULL;
END IF;



IF fouordre =fouordrepapaye THEN
  SELECT tbo_ordre INTO tboordre
  FROM TYPE_BORDEREAU
  WHERE tbo_libelle='BORDEREAU DE MANDAT SALAIRES';
ELSE
  SELECT COUNT(*) INTO cpt
  FROM OLD_AGENT_TYPE_BORD
  WHERE agt_ordre = agtordre;

  IF cpt <> 0 THEN
   SELECT tbo_ordre INTO tboordre
   FROM OLD_AGENT_TYPE_BORD
   WHERE agt_ordre = agtordre;
  ELSE
   SELECT tbo_ordre INTO tboordre
   FROM TYPE_BORDEREAU
   WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTME GENERIQUE' AND exe_ordre = exeordre);
  END IF;

END IF;


RETURN tboordre;
END;

FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
BEGIN

RETURN 1;
END;


PROCEDURE Maj_Origine
IS
	CURSOR c1 IS
	SELECT org_ordre FROM jefy.organ
	WHERE org_lucrativite !=0;


	CURSOR c2 IS
	SELECT con_ordre FROM convention.contrat
	WHERE con_suppr='N';


	orgordre INTEGER;
	conordre INTEGER;
	cpt INTEGER;
BEGIN

	OPEN c1;
	LOOP
		FETCH C1 INTO orgordre;
			  EXIT WHEN c1%NOTFOUND;
			  cpt := TRAITER_ORGORDRE ( orgordre );
	END LOOP;
	CLOSE c1;


	OPEN c2;
	LOOP
		FETCH C2 INTO conordre;
			  EXIT WHEN c2%NOTFOUND;
			  cpt := TRAITER_CONVORDRE ( conordre );
	END LOOP;
	CLOSE c2;


END;


END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.number_to_lettres IS
-- version 1.0
-- rodolphe prin - cri La Rochelle

FUNCTION transformer (nb NUMBER) RETURN VARCHAR IS
BEGIN
	 RETURN ZNb_En_Texte(nb);
END;


FUNCTION ZtraiteDixCent(nb NUMBER) RETURN VARCHAR IS
	TYPE CHIFFRES IS VARRAY (20) OF VARCHAR(10);
	TYPE DIZAINES IS VARRAY (9) OF VARCHAR(16);

	chiffre CHIFFRES;
	dizaine DIZAINES;
	varlet VARCHAR(300);

	varnumD NUMBER(38);
	varnumU NUMBER(38);
	varnum NUMBER(38);

BEGIN

	chiffre := CHIFFRES('un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept','huit','neuf','dix','onze','douze','treize','quatorze','quinze', 'seize','dix-sept','dix-huit', 'dix-neuf');
	dizaine := DIZAINES('dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante-dix', 'quatre-vingt', 'quatre-vingt dix');

	varnum := nb;

	IF varnum>=1000000000 THEN
	   RAISE_APPLICATION_ERROR (-20001,'Impossible de transformer en lettres les valeurs superieures a 1000000000');
	END IF;

	---Traitement des centaines
	IF varnum >= 100 THEN
	   varlet := chiffre(TRUNC(varnum / 100));
	   varnum := varnum MOD 100;
		IF varlet = 'un' THEN
		   varlet := 'cent ';
		ELSE
			varlet := varlet || ' cent ';
		END IF;
	END IF;

	---Traitement des dizaines
	IF varnum <= 19 THEN ---Cas o� la dizaine est <20
		IF varnum > 0 THEN
		   varlet := varlet || chiffre(varnum);
		END IF;
	ELSE
		varnumD := TRUNC(varnum / 10); ---chiffre des dizaines
		varnumU := varnum MOD 10; ---chiffre des unit�s
		IF varnumD <= 5 THEN
		   varlet := varlet || dizaine(varnumD);
		ELSE
			IF varnumD IN (6,7) THEN
			   varlet := varlet || dizaine(6);
			END IF;
			IF varnumD IN (8,9) THEN
			   varlet := varlet || dizaine(8);
			END IF;
		END IF;

		---traitement du s�parateur des dizaines et unit�s
		IF varnumU = 1 AND varnumD < 8 THEN
		   varlet := varlet || ' et ';
		ELSE
			IF varnumU <> 0 OR varnumD = 7 OR varnumD = 9 THEN
			   --varlet := varlet || ' ';
			   varlet := varlet || '-';
			END IF;
		END IF;
		---g�n�ration des unit�s
		IF varnumD = 7 OR varnumD = 9 THEN
		   varnumU := varnumU + 10;
		END IF;
		IF varnumU <> 0 THEN
		   varlet := varlet || chiffre(varnumU);
		END IF;
	END IF;
	varlet := RTRIM(varlet, ' ');
	RETURN varlet;

END;






FUNCTION ZNb_En_Texte(nb  NUMBER) RETURN VARCHAR IS
	devise VARCHAR(10);
	centimes VARCHAR(10);
	resultat VARCHAR(500);
	varnum NUMBER(38);
	varlet VARCHAR(300);
	separateurMillion VARCHAR(3);
BEGIN
	 devise := 'euro';
	 centimes := 'cent';
	 separateurMillion := 'd''';

	 ---Traitement du cas z�ro
	IF nb <= 1 THEN
		resultat := 'zero';
	END IF;


	---Traitement des millions
	varnum := TRUNC (nb / 1000000);
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
	   resultat := varlet || ' million';
	   IF varlet <> 'un' THEN
	   	  resultat := resultat || 's';
	   END IF;
	END IF;

	---Traitement des milliers
	varnum := TRUNC(nb) MOD 1000000;
	varnum := TRUNC(varnum / 1000);
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
	   IF varlet <> 'un' THEN
	   	  resultat := resultat || ' ' || varlet;
	   END IF;
	   resultat := resultat || ' mille';
	END IF;

	---Traitement des centaines et dizaines
	varnum := TRUNC(nb) MOD 1000;
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
	   resultat := resultat || ' ' || varlet;
	END IF;
	resultat := LTRIM(resultat, ' ');



	---Traitement du 's' final pour vingt et cent et du 'de' pour million
	varlet := SUBSTR(resultat, -4);
	IF varlet IN ('cent', 'ingt') THEN
	   resultat := resultat || 's';
	END IF;




	---Indication de la devise
 	IF varlet IN ('lion', 'ions', 'iard', 'ards') THEN
 	   resultat := resultat || ' ' || separateurMillion || devise;
	ELSE
		resultat := resultat || ' ' || devise;
 	END IF;

	IF nb >= 2 THEN
	   resultat := resultat || 's';
	END IF;

	---Traitement des centimes
	varnum := TRUNC((nb - TRUNC(nb)) * 100);
	IF varnum > 0 THEN
	   varlet := ZtraiteDixCent(varnum);
		resultat := resultat || ' et ' || varlet || ' ' ||centimes;
		IF varnum > 1 THEN
		   resultat := resultat || 's';
		END IF;
	END IF;

	RETURN resultat;
END;


END;
/


-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Numerotationobject IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (ecrordre INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);
PROCEDURE numeroter_bordereau (borid INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

PROCEDURE numeroter_mandat (borid integer);

PROCEDURE numeroter_titre (borid integer);

PROCEDURE numeroter_orv (borid integer);

PROCEDURE numeroter_aor (borid integer);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Numerotationobject IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE BROUILLARD INEXISTANTE , CLE '||ecrordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero_brouillard = lenumero
WHERE ecr_ordre = ecrordre;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM PAIEMENT
WHERE pai_ordre = paiordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='VIREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE PAIEMENT SET pai_numero = lenumero
WHERE pai_ordre = paiordre;

END;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_ordre = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
--select com_ordre,exe_ordre
SELECT NULL,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_ordre = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
--update retenue set ret_numero = lenumero
--where ret_ordre = retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

--COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

IF gescode IS NULL THEN
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code IS NULL
AND tnu_ordre = tnuordre;
ELSE
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code = gescode
AND tnu_ordre = tnuordre;
END IF;

IF cpt  = 0 THEN
--raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
 SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


 INSERT INTO NUMEROTATION
 ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
 VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
 RETURN 1;
ELSE
 IF gescode IS NOT NULL THEN
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;
 ELSE
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;
 END IF;
 RETURN cpt;
END IF;
END ;




PROCEDURE numeroter_bordereau (borid INTEGER) is


cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

lebordereau bordereau%ROWTYPE;


BEGIN

SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU  INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,t.tbo_ordre,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,tboordre,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



select  count(*) into cpt
from maracuja.type_bordereau tb , maracuja.type_numerotation tn
where tn.tnu_ordre = tb.tnu_ordre
and tb.tbo_ordre = tboordre;

if cpt = 1 then
 select tn.tnu_ordre into tnuordre
 from maracuja.type_bordereau tb , type_numerotation tn
 where tn.tnu_ordre = tb.tnu_ordre
 and tb.tbo_ordre = tboordre;
else
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION'||borid);
end if;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

if lenumero is null then 
 RAISE_APPLICATION_ERROR (-20001,'PROBLEME DE NUMEROTATION'||borid);
end if;


-- affectation du numero -
UPDATE maracuja.BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;

end;


PROCEDURE numeroter_mandat (borid integer)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

cursor a_numeroter is
select man_id from mandat where bor_id = borid;

begin 
-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre
INTO comordre,exeordre
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT'; 
open a_numeroter;
loop
fetch a_numeroter into manid;
exit when a_numeroter%notfound;
 lenumero := numeroter(COMORDRE,EXEORDRE,null,TNUORDRE);
 update maracuja.mandat set man_numero = lenumero
 where bor_id = borid and man_id = manid;
end loop;
close a_numeroter;
end ;

PROCEDURE numeroter_titre (borid integer)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

cursor a_numeroter is
select tit_id from titre where bor_id = borid;

begin 
-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre
INTO comordre,exeordre
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE'; 
open a_numeroter;
loop
fetch a_numeroter into titid;
exit when a_numeroter%notfound;
 lenumero := numeroter(COMORDRE,EXEORDRE,null,TNUORDRE);
 update maracuja.titre set tit_numero =  lenumero
 where bor_id = borid and tit_id = titid;
end loop;
close a_numeroter;

END;



PROCEDURE numeroter_orv (borid integer)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

cursor a_numeroter is
select man_id from mandat where bor_id = borid;

begin 
-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre
INTO comordre,exeordre
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORV'; 
open a_numeroter;
loop
fetch a_numeroter into manid;
exit when a_numeroter%notfound;
 lenumero := numeroter(COMORDRE,EXEORDRE,null,TNUORDRE);
 update maracuja.mandat set man_numero = lenumero
 where bor_id = borid and man_id = manid;
end loop;
close a_numeroter;
end ;

PROCEDURE numeroter_aor (borid integer)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

cursor a_numeroter is
select tit_id from titre where bor_id = borid;

begin 
-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre
INTO comordre,exeordre
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='AOR'; 
open a_numeroter;
loop
fetch a_numeroter into titid;
exit when a_numeroter%notfound;
 lenumero := numeroter(COMORDRE,EXEORDRE,null,TNUORDRE);
 update maracuja.titre set tit_numero =  lenumero
 where bor_id = borid and tit_id = titid;
end loop;
close a_numeroter;

END;

END;
/





CREATE OR REPLACE PACKAGE MARACUJA.Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;

PROCEDURE prv_apres_visa_bordereau_2006 (borid INTEGER);
PROCEDURE prv_apres_reimputation_2006 (reiordre INTEGER);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Journal IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

Apres avoir creer l ecriture et ses details
il faut valider l ecriture :
l' ecriture prend un numero dans le journal
de l exerice ET IL INTERDIT /IMPOSSIBLE DE LA SUPPRIMER !!

*/

-- API PUBLIQUE pour creer / valider / annuler une ecriture --
-- permet de valider une ecriture saisie .
PROCEDURE validerEcriture (ecrordre INTEGER) ;
-- permet d'annuler une ecriture saisie .
PROCEDURE annulerEcriture (ecrordre INTEGER) ;

-- permet de creer une ecriture de balance d entree --
FUNCTION creerEcritureBE (
COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  ) RETURN INTEGER;

 -- permet de creer une ecriture d exercice --
FUNCTION creerEcritureExerciceType (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER,--        NOT NULL,
  TJOORDRE 				INTEGER
  ) RETURN INTEGER;
 -- permet de creer une ecriture de fin d exercice --
FUNCTION creerEcritureCloture (brjordre INTEGER,exeordre INTEGER) RETURN INTEGER;
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;


-- PRIVATE --
/*
INTERDICTION DE FAIRE DES APPELS DE CES PROCEDURES EN DEHORS DU PACKAGE.
*/
FUNCTION creerEcriturePrivate (

  BROORDRE              NUMBER,
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  --ECRNUMERO             NUMBER,-- (32),
 ECRNUMERO_BROUILLARD  NUMBER,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  ) RETURN INTEGER;

FUNCTION creerEcritureDetailPrivate (
  ECDCOMMENTAIRE    VARCHAR2,-- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Papaye IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures SACD   d un bordereau --
PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- permet de passer les  ecritures SACD  d un mois de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un bordereau --
PROCEDURE passerEcritureOppRetBord ( borid INTEGER, passer_ecritures VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Basculer_BE IS

/*
CRI G guadeloupe - Rivalland Frederic.
CRI LR - Prin Rodolphe

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja lors
du passage des ecritures de balance entree

*/
/*
create table ecriture_detail_be_log
(
edb_ordre integer,
edb_date date,
utl_ordre integer,
ecd_ordre integer)

create sequence basculer_solde_du_copmpte_seq start with 1 nocache;


INSERT INTO TYPE_OPERATION ( TOP_LIBELLE, TOP_ORDRE, TOP_TYPE ) VALUES (
'BALANCE D ENTREE AUTOMATIQUE', 11, 'PRIVEE');

*/
-- PUBLIC --

-- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
PROCEDURE basculer_solde_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
PROCEDURE basculer_DC_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);


PROCEDURE basculer_DC_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER);



-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
PROCEDURE basculer_solde_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);



PROCEDURE basculer_DC_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_DC_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR);

PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER);

-- PRIVATE --
PROCEDURE creer_detail_890 (ecrordre INTEGER,pconumlibelle VARCHAR);
PROCEDURE priv_archiver_la_bascule( pconum VARCHAR,gescode VARCHAR,utlordre INTEGER, exeordre INTEGER);
FUNCTION priv_get_exeordre_prec(exeordre INTEGER) RETURN INTEGER;
PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER);
PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER);
FUNCTION priv_getCompteBe(pconumold VARCHAR) RETURN VARCHAR;

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Bordereau_Mandat IS

PROCEDURE GET_BTME_JEFY(borordre NUMBER,exeordre NUMBER);

PROCEDURE set_mandat_brouillard(manid INTEGER);

PROCEDURE get_facture_jefy(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER);

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 );

PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Bordereau_Mandat_05 IS
PROCEDURE GET_BTME_JEFY(borordre NUMBER,exeordre NUMBER);

PROCEDURE set_mandat_brouillard(manid INTEGER);

PROCEDURE get_facture_jefy(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER);

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 );

PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

END;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_papaye IS

PROCEDURE recup_brouillards_payes (moisordre NUMBER);

PROCEDURE maj_brouillards_payes (moisordre NUMBER, borordre NUMBER);

PROCEDURE GET_BTME_JEFY(borordre NUMBER,exeordre NUMBER);

PROCEDURE set_mandat_brouillard(manid INTEGER);

PROCEDURE set_bord_brouillard_visa(borid INTEGER);

PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER);

PROCEDURE set_bord_brouillard_retenues(borid NUMBER);

PROCEDURE set_bord_brouillard_sacd(borid NUMBER);

PROCEDURE get_facture_jefy(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER);

PROCEDURE get_mandat_jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 );


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.Bordereau_Titre IS

PROCEDURE Get_Titre_Jefy ( exeordre INTEGER, borordre INTEGER, borid INTEGER, utlordre INTEGER, agtordre INTEGER );
PROCEDURE Get_Recette_Jefy(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER);
PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER);
PROCEDURE Get_Btte_Jefy(borordre NUMBER,exeordre NUMBER);
PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.bordereau_titre_05 IS

PROCEDURE Get_Titre_Jefy ( exeordre INTEGER, borordre INTEGER, borid INTEGER, utlordre INTEGER, agtordre INTEGER );
PROCEDURE Get_Recette_Jefy(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER);
PROCEDURE Get_Btte_Jefy(borordre NUMBER,exeordre NUMBER);
PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);


END;
/


CREATE OR REPLACE PACKAGE MARACUJA.cptefiutil IS
    PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER,libelle VARCHAR, rtatcomp VARCHAR);
    PROCEDURE annuler_ecriture (monecdordre INTEGER);
    PROCEDURE numeroter_ecriture (ecrordre INTEGER);
END;
/






---------------------------------------------------------------------
---------------------------------------------------------------------
---------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE MARACUJA.RECUP_PLANCO_AMORT AS

/*

procedure de recuperation de l ancienne table inventaire.planco_amort
dans le user maracuja :
2 tables concern�es :
PLANCO_AMORTISSEMENT
plan_comptable_amo

RIVALLAND Frederic.

pre-requis :
create table log_recup_planco_amort (logs varchar(1000))


scripts:
begin MARACUJA.RECUP_PLANCO_AMORT();end;
create table log_recup_planco_amort (logs varchar(1000))
select PCOA_ID,PCOA_NUM,PCOA_LIBELLE,TYET_ID from MARACUJA.PLAN_COMPTABLE_AMO;
select PCA_ID,PCOA_ID,EXE_ORDRE,PCO_NUM from MARACUJA.PLANCO_AMORTISSEMENT;
select * from log_recup_planco_amort;


*/
tmp_inv_planco inventaire.planco_amort%ROWTYPE;
cle INTEGER;

CURSOR c_planco IS
SELECT *
FROM inventaire.planco_amort;

cpt INTEGER;

BEGIN

SELECT COUNT(*)INTO cpt FROM PLANCO_AMORTISSEMENT;

IF cpt != 0 THEN
RAISE_APPLICATION_ERROR (-20001,'IMPORT DEJA EFFECTUE');
END IF;

OPEN c_planco;
LOOP
FETCH c_planco INTO tmp_inv_planco;
EXIT WHEN c_planco%NOTFOUND;

SELECT PLANCO_AMORTISSEMENT_seq.NEXTVAL INTO cle FROM dual;

SELECT COUNT(*) INTO cpt FROM maracuja.PLAN_COMPTABLE_AMO WHERE PCOA_NUM =tmp_inv_planco.pco_amort;

IF cpt = 0 THEN
-- etat valide  1 a l import
INSERT INTO PLAN_COMPTABLE_AMO (PCOA_ID,PCOA_NUM,PCOA_LIBELLE,TYET_ID)
VALUES
(
cle,
tmp_inv_planco.pco_amort,
NVL(tmp_inv_planco.pco_libelle_amort,'A DEFINIR'),
1
);



-- PCA_ID,PCOA_ID,EXE_ORDRE,PCO_NUM from MARACUJA.PLANCO_AMORTISSEMENT;
SELECT COUNT(*) INTO cpt FROM maracuja.PLAN_COMPTABLE WHERE pco_num =tmp_inv_planco.pco_num;

IF cpt !=0 THEN
 INSERT INTO  PLANCO_AMORTISSEMENT 
  SELECT 
  PLANCO_AMORTISSEMENT_seq.NEXTVAL,
  cle,
  exe_ordre,
  tmp_inv_planco.pco_num
  FROM jefy_admin.exercice;
 ELSE
  INSERT INTO LOG_RECUP_PLANCO_AMORT VALUES ('probleme de compte inexistant '||tmp_inv_planco.pco_num);
 END IF;

 ELSE
  INSERT INTO LOG_RECUP_PLANCO_AMORT VALUES ('probleme de compte duplique '||tmp_inv_planco.pco_amort);
 END IF;
END LOOP;
CLOSE c_planco;


END;
/






---------------------------------------------------------------------
---------------------------------------------------------------------
---------------------------------------------------------------------


CREATE OR REPLACE PROCEDURE maracuja.Prc_Saveunqpreferenceforutil(
	  			utlOrdre NUMBER,
			  prefKey VARCHAR2,
			  upValue VARCHAR2,
			 prefDefaultValue VARCHAR2,
			 prefDescription VARCHAR2,
			 upId OUT NUMBER
	  ) IS

	  BEGIN
	  	   jefy_admin. api_preference.Prc_Saveunqpreferenceforutil(utlOrdre, 4,prefKey,
		   upValue, prefDefaultValue, prefDescription ,upId);
	  END;
/


---------------------------------------------------------------------
---------------------------------------------------------------------
---------------------------------------------------------------------


CREATE OR REPLACE PROCEDURE maracuja.Init_Old_Exercice (
	   exer NUMBER
)
IS
-- **************************************************************************
-- Creation des anciens exercices dans jefy_admin
-- **************************************************************************
--
--
-- Version du 12/12/2006
--
--

flag NUMBER;


BEGIN
	 SELECT COUNT(*) INTO flag FROM jefy_admin.exercice WHERE exe_exercice = exer;
	 IF (flag = 0) THEN
	 	INSERT INTO JEFY_ADMIN.EXERCICE (
   			   							EXE_CLOTURE, 
										EXE_EXERCICE, 
										EXE_INVENTAIRE, 
   										EXE_ORDRE, 
										EXE_OUVERTURE, 
										EXE_STAT, 
   										EXE_TYPE, 
										EXE_STAT_ENG, 
										EXE_STAT_FAC) 
					VALUES ( 
						   	 			TO_DATE( '31/12/'|| exer   ,'dd/mm/yyyy'), 
										exer, 
										TO_DATE( '21/12/'|| exer   ,'dd/mm/yyyy'), 
   										exer, 
										TO_DATE( '01/01/'|| exer   ,'dd/mm/yyyy'), 
										'C', 
   										'C', 
										'C', 
										'C');
	 END IF;



END;
/



CREATE OR REPLACE PROCEDURE maracuja.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de pr�parer la base de donnees de Maracuja
-- pour un nouvel exercice (exercice doit exister dans jefy_admin)
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 06/12/2006
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

precedentExercice := nouvelExercice - 1;

-- -------------------------------------------------------
-- V�rifications concernant l'exercice precedent


-- Verif que l'exercice precedent existe
SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
IF (flag=0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de pr�parer l''exercice '|| nouvelExercice ||'.');
END IF;

SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
IF (flag=0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de pr�parer l''exercice '|| nouvelExercice ||'.');
END IF;

--  -------------------------------------------------------
-- Preparation des parametres
INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
	   				  FROM PARAMETRE
					  WHERE exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- R�cup�ration des codes gestion
INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185
	   						 FROM GESTION_EXERCICE
							 WHERE exe_ordre=precedentExercice) ;


--  -------------------------------------------------------
-- R�cup�ration des modes de paiement
INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
	   					  MOD_VISA_TYPE, MOD_EMA_AUTO
	   					  FROM MODE_PAIEMENT
						  WHERE exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- R�cup�ration des modes de recouvrement
INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, 
MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
	   					  FROM MODE_RECOUVREMENT
						  WHERE exe_ordre=precedentExercice);

						  
						  
						  
--  -------------------------------------------------------
-- R�cup�ration des planco_credit
-- les nouveaux types de credit doivent exister
INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
 FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
WHERE 
tcold.exe_ordre=precedentExercice
AND tcnew.exe_ordre=nouvelExercice
AND tcold.tcd_code=tcnew.tcd_code
AND tcnew.tyet_id=1
AND tcold.tcd_type=tcnew.tcd_type
) x
WHERE 
pcc.tcd_ordre=x.tcd_ordre_old
AND pcc.pcc_etat='VALIDE'
);


--  -------------------------------------------------------
-- R�cup�ration des planco_amortissment
INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM) (
SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, 2007, PCO_NUM
FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
WHERE exe_ordre=2006
AND p.PCOA_ID = a.PCOA_ID
AND a.TYET_ID=1
);
						  
						  
						  
END;

/


GRANT EXECUTE ON  MARACUJA.PREPARE_EXERCICE TO JEFY_ADMIN;


---------------------------------------------------------------------
---------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE maracuja.Recup_Droits_Utilisateurs 
IS
-- **************************************************************************
-- Recuperation des droits utilisateurs de Maracuja
-- **************************************************************************
--
--
-- Version du 08/12/2006
--
--

flag NUMBER;
oldUtlOrdre NUMBER;
oldAutOrdre NUMBER;
newUtlOrdre NUMBER;

oldUtilisateur ZLD_UTILISATEUR%ROWTYPE;
oldAutorisation ZLD_AUTORISATION%ROWTYPE;
oldAutorisationGes ZLD_AUTORISATION_GESTION%ROWTYPE;
oldFonction ZLD_FONCTION%ROWTYPE;

ufOrdre NUMBER;
ufgId NUMBER;
newFonOrdre NUMBER;

CURSOR c1 IS  SELECT * FROM ZLD_UTILISATEUR  WHERE UTL_ETAT='VALIDE';
CURSOR cAutorisation IS  SELECT * FROM ZLD_AUTORISATION  WHERE utl_ordre=oldUtlOrdre;
CURSOR cAutorisationGes IS  SELECT * FROM ZLD_AUTORISATION_GESTION  WHERE AUT_ORDRE=oldAutorisation.aut_ordre;





BEGIN
	 -- balayer les anciens utilisateurs

	OPEN C1;
	LOOP
		FETCH c1 INTO oldUtilisateur;
			  EXIT WHEN c1%NOTFOUND ;

		oldUtlOrdre := oldUtilisateur.utl_ordre;
		SELECT COUNT(*) INTO flag FROM jefy_admin.utilisateur WHERE no_individu=oldUtilisateur.no_individu AND  TYET_ID=1 AND ROWNUM=1;
		IF (flag>0) THEN
		       	 -- recuperer l'utilisateur correspondant dans les nouveaux
		   		SELECT utl_ordre INTO newUtlOrdre FROM jefy_admin.utilisateur WHERE no_individu=oldUtilisateur.no_individu AND  TYET_ID=1 AND ROWNUM=1;
					OPEN cAutorisation;
						LOOP
							FETCH cAutorisation INTO oldAutorisation;
			  					  EXIT WHEN cAutorisation%NOTFOUND ;
								  	    -- verifier que la fonction existe
										SELECT * INTO oldFonction FROM ZLD_FONCTION WHERE fon_ordre=oldAutorisation.FON_ORDRE;
										SELECT COUNT(*) INTO flag FROM jefy_admin.fonction WHERE fon_id_interne=oldFonction.fon_id_interne AND tyap_id=4;
										IF (flag=1) THEN
										   			SELECT  fon_ordre INTO newFonOrdre FROM jefy_admin.fonction WHERE fon_id_interne=oldFonction.fon_id_interne AND tyap_id=4;
										  	   	 -- creer une nouvelle utilisateur_ fonct pour chaque autorisation
												 ufOrdre := NULL;
												 
												 dbms_output.put_line('prc_CreerUtilisateurFonction ('||newUtlOrdre || ', ' ||newFonOrdre ||')');
												 jefy_admin.API_UTILISATEUR.prc_CreerUtilisateurFonction(newUtlOrdre, newFonOrdre, ufOrdre); 
												 dbms_output.put_line('Res= '||ufOrdre); 
												 
												 SELECT COUNT(*) INTO flag FROM jefy_admin.fonction WHERE tyap_id=4 AND fon_ordre=newFonOrdre AND FON_SPEC_GESTION ='O';
												 
										        IF (flag>0 AND ufOrdre IS NOT NULL) THEN
									  					OPEN cAutorisationGes;
															LOOP
																FETCH cAutorisationGes INTO oldAutorisationGes;
												  					  EXIT WHEN cAutorisationGes%NOTFOUND ;
																	  
																	   -- creer les utilisateur_fonct_gestion pour chaque autorisation_gestion
																	   dbms_output.put_line('prc_CreerUtilisateurFonctGes ('||ufOrdre || ', ' || oldAutorisationGes.ges_code ||')');
																	   jefy_admin.API_UTILISATEUR.prc_CreerUtilisateurFonctGes(ufOrdre, oldAutorisationGes.ges_code,ufgId);
																	  dbms_output.put_line('Res= '||ufgId); 
															END LOOP;
															CLOSE cAutorisationGes;
												END IF;
										END IF;
						END LOOP;
						CLOSE cAutorisation;
		END IF;


	END LOOP;
	CLOSE C1;

END;
/


---------------------------------------------------------------------



