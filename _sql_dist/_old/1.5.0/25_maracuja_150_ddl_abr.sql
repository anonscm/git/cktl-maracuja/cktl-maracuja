SET define OFF
--
-- executer a partir de maracuja
-----------------------------------------------------------
--
-- Abricot
-- 
-----------------------------------------------------------


CREATE TABLE MARACUJA.ABRICOT_BORD_SELECTION (
  ABR_ID        NUMBER                          NOT NULL,
  UTL_ORDRE     NUMBER                          NOT NULL,
  DEP_ID        NUMBER,
  REC_ID        NUMBER,
  EXE_ORDRE     NUMBER                          NOT NULL,
  TBO_ORDRE     NUMBER                          NOT NULL,
  ABR_ETAT      VARCHAR2(25),
  ABR_GROUP_BY  VARCHAR2(200),
  GES_CODE      VARCHAR2(10)                    NOT NULL
);

ALTER TABLE MARACUJA.ABRICOT_BORD_SELECTION
 ADD CONSTRAINT PK_ABRICOT_BORD_SELECTION
 PRIMARY KEY (ABR_ID);
 
 
ALTER TABLE MARACUJA.ABRICOT_BORD_SELECTION ADD (
  CONSTRAINT FK_ABRICOT_BORD_SELECTION_EXE_ FOREIGN KEY (EXE_ORDRE) 
    REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE));

ALTER TABLE MARACUJA.ABRICOT_BORD_SELECTION ADD (
  CONSTRAINT FK_ABRICOT_BORD_SELECTION_GES_ FOREIGN KEY (GES_CODE) 
    REFERENCES MARACUJA.GESTION (GES_CODE));

ALTER TABLE MARACUJA.ABRICOT_BORD_SELECTION ADD (
  CONSTRAINT FK_ABRICOT_BORD_SELECTION_TBO_ FOREIGN KEY (TBO_ORDRE) 
    REFERENCES MARACUJA.TYPE_BORDEREAU (TBO_ORDRE));
    
    

CREATE SEQUENCE MARACUJA.ABRICOT_BORD_SELECTION_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


    
----------------------------------------------------------------    
----------------------------------------------------------------    

CREATE OR REPLACE FORCE VIEW MARACUJA.V_ABRICOT_DEPENSE_A_MANDATER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, PERS_TYPE, 
 PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, ORG_UB, 
 ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, 
 DPP_NUMERO_FACTURE, DPP_ID, DPP_HT_SAISIE, DPP_TVA_SAISIE, DPP_TTC_SAISIE, 
 DPP_DATE_FACTURE, DPP_DATE_SAISIE, DPP_DATE_RECEPTION, DPP_DATE_SERVICE_FAIT, DPP_NB_PIECE, 
 UTL_ORDRE, TBO_ORDRE, DEP_ID, PCO_NUM, DPCO_HT_SAISIE, 
 DPCO_TVA_SAISIE, DPCO_TTC_SAISIE, ADR_CIVILITE, ADR_NOM, ADR_PRENOM, 
 ADR_ADRESSE1, ADR_ADRESSE2, ADR_VILLE, ADR_CP, UTLNOMPRENOM)
AS 
select 
r.C_BANQUE,
r.C_GUICHET,
r.NO_COMPTE,
r.iban,
r.bic,
bq.DOMICILIATION,
mp.MOD_libelle,
mp.MOD_CODE,
mp.MOD_DOM,
p.pers_type,
p.pers_libelle,
p.pers_lc,
e.EXE_EXERCICE,
eb.org_id,
o.ORG_UB,
o.ORG_CR,
o.ORG_SOUSCR,
eb.tcd_ordre,
tc.tcd_code,
tc.tcd_libelle,
dp.dpp_numero_facture,
dp.DPP_ID,
dp.DPP_HT_SAISIE,
dp.DPP_TVA_SAISIE,
dp.DPP_TTC_SAISIE,
dp.DPP_DATE_FACTURE,
dp.DPP_DATE_SAISIE,
dp.DPP_DATE_RECEPTION,
dp.DPP_DATE_SERVICE_FAIT,
dp.DPP_NB_PIECE,
dp.utl_ordre,
dcp.tbo_ordre,
dcp.dep_id+tcd.tcd_ordre dep_id,
dcp.pco_num,
dcp.DPCO_HT_SAISIE,
dcp.DPCO_TVA_SAISIE,
dcp.DPCO_TTC_SAISIE,
f.ADR_CIVILITE,
f.ADR_NOM,
f.ADR_PRENOM,
f.ADR_ADRESSE1,
f.ADR_ADRESSE2,
f.ADR_VILLE,
f.ADR_CP,
pu.pers_libelle||' '||pu.pers_lc utlNomPrenom
from
jefy_depense.engage_budget eb,
jefy_depense.depense_budget db,
jefy_depense.depense_papier dp,
jefy_depense.depense_ctrl_planco dcp,
grhum.v_fournis_grhum f,
jefy_admin.EXERCICE e,
jefy_admin.utilisateur u,
grhum.personne p,
grhum.personne pu,
grhum.ribfour_ulr r,
grhum.banque bq,
maracuja.MODE_PAIEMENT mp,
jefy_admin.type_credit tc,
jefy_admin.organ o,
jefy_admin.type_credit tcd
where eb.eng_id = db.eng_id
and   db.dpp_id = dp.dpp_id
and   db.dep_id = dcp.dep_id
and   dp.fou_ordre = f.fou_ordre
and   dp.exe_ordre = e.exe_ordre
and   dp.utl_ordre = u.utl_ordre
and   p.pers_id = u.pers_id
and   dp.rib_ordre = r.rib_ordre(+)
and   r.C_BANQUE = bq.C_BANQUE(+)
and   r.C_GUICHET = bq.C_GUICHET(+)
and   mp.mod_ordre = dp.mod_ordre
and   eb.tcd_ordre = tc.tcd_ordre
and   o.org_id = eb.org_id
and u.pers_id = pu.pers_id
and   r.rib_valide ='O'
and   man_id is null;

----------------------------------------------------------------    
---------------------------------------------------------------- 

CREATE OR REPLACE FORCE VIEW MARACUJA.V_ABRICOT_RECETTE_A_TITRER
(C_BANQUE, C_GUICHET, NO_COMPTE, IBAN, BIC, 
 DOMICILIATION, MOD_LIBELLE, MOD_CODE, MOD_DOM, PERS_TYPE, 
 PERS_LIBELLE, PERS_LC, EXE_EXERCICE, ORG_ID, ORG_UB, 
 ORG_CR, ORG_SOUSCR, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, 
 FAC_NUMERO, REC_ID, REC_HT_SAISIE, REC_TVA_SAISIE, REC_TTC_SAISIE, 
 FAC_DATE_SAISIE, REC_NB_PIECE, UTL_ORDRE, TBO_ORDRE, PCO_NUM, 
 RPCO_HT_SAISIE, TVA, TTC, ADR_CIVILITE, ADR_NOM, 
 ADR_PRENOM, ADR_ADRESSE1, ADR_ADRESSE2, ADR_VILLE, ADR_CP, 
 UTLNOMPRENOM)
AS 
select 
r.C_BANQUE,
r.C_GUICHET,
r.NO_COMPTE,
r.iban,
r.bic,
bq.DOMICILIATION,
mr.MOD_libelle,
mr.MOD_CODE,
mr.MOD_DOM,
p.pers_type,
p.pers_libelle,
p.pers_lc,
e.EXE_EXERCICE,
fac.org_id,
o.ORG_UB,
o.ORG_CR,
o.ORG_SOUSCR,
fac.tcd_ordre,
tc.tcd_code,
tc.tcd_libelle,
fac.fac_numero,
rec.REC_ID,
rec.REC_HT_SAISIE,
rec.REC_TVA_SAISIE,
rec.REC_TTC_SAISIE,
FAC.FAC_DATE_SAISIE,
0 REC_nb_piece,
rec.utl_ordre,
rcp.tbo_ordre,
--rcp.rec_id+tcd.tcd_ordre rec_id,
rcp.pco_num,
rcp.RPCO_HT_SAISIE,
rcp.RPCO_HT_SAISIE TVA,
rcp.RPCO_HT_SAISIE TTC,
f.ADR_CIVILITE,
f.ADR_NOM,
f.ADR_PRENOM,
f.ADR_ADRESSE1,
f.ADR_ADRESSE2,
f.ADR_VILLE,
f.ADR_CP,
pu.pers_libelle||' '||pu.pers_lc utlNomPrenom
from
jefy_recette.facture fac,
jefy_recette.recette_budget rec,
jefy_recette.recette_papier recp,
jefy_recette.recette_ctrl_planco rcp,
grhum.v_fournis_grhum f,
jefy_admin.EXERCICE e,
jefy_admin.utilisateur u,
grhum.personne p,
grhum.personne pu,
grhum.ribfour_ulr r,
grhum.banque bq,
maracuja.MODE_RECOUVREMENT mr,
jefy_admin.type_credit tc,
jefy_admin.organ o
where fac.fac_id = rec.fac_id
and rec.rpp_id = recp.rpp_id
and rec.rec_id = rcp.rec_id
and fac.fou_ordre = f.fou_ordre
and rec.exe_ordre = e.exe_ordre
and fac.utl_ordre = u.utl_ordre
and p.pers_id = u.pers_id
and recp.rib_ordre = r.rib_ordre(+)
and r.C_BANQUE = bq.C_BANQUE(+)
and r.C_GUICHET = bq.C_GUICHET(+)
and mr.mod_ordre = recp.mor_ordre
and fac.tcd_ordre = tc.tcd_ordre
and o.org_id = fac.org_id
and u.pers_id = pu.pers_id
and rcp.tit_id is null;


----------------------------------------------------------------    
---------------------------------------------------------------- 




    