set define off
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- 
-- Script mettant a jour des donnees necessaire au fonctionnement de Maracuja
-- 
-----------------------------------------------------------

begin

---------------------------------------------
-- Ajout d'une référence aux recettes
UPDATE maracuja.TITRE_DETAIL_ECRITURE tde SET rec_id = (SELECT rec_id FROM maracuja.RECETTE r, maracuja.TITRE t WHERE t.tit_id=r.tit_id AND t.tit_id=tde.tit_id) ;
commit;
UPDATE maracuja.TITRE_BROUILLARD tde SET rec_id = (SELECT rec_id FROM maracuja.RECETTE r, maracuja.TITRE t WHERE t.tit_id=r.tit_id AND t.tit_id=tde.tit_id) ;
commit;
---------------------------------------------
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 'BORDEREAU_EMIS', 'BORD. MANDAT', 14); 
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 'BORDEREAU_EMIS', 'BORD. TITRE', 15); 
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 'MANDAT', 'MANDAT', 16); 
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 'TITRE', 'TITRE', 17); 
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 'ORV', 'ORV', 18); 
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 'AOR', 'AOR', 19); 


commit;

---------------------------------------------

-- suppression des types de bordereaux inutiles
delete from maracuja.type_bordereau where tbo_ordre in (5,6,12,13,103);

-- ajout des nouveaux
INSERT INTO maracuja.TYPE_BORDEREAU ( TBO_LIBELLE, TBO_ORDRE, TBO_SOUS_TYPE, TBO_TYPE,
TBO_TYPE_CREATION ) VALUES ( 
'BORDEREAU DE PRESTATION INTERNE RECETTE', 200, 'PREST. INTERNE REC.', 'BTPIR', 'PIR_200'); 
INSERT INTO maracuja.TYPE_BORDEREAU ( TBO_LIBELLE, TBO_ORDRE, TBO_SOUS_TYPE, TBO_TYPE,
TBO_TYPE_CREATION ) VALUES ( 
'BORDEREAU DE PRESTATION INTERNE DEPENSE', 201, 'PREST. INTERNE DEP.', 'BTPID', NULL); 

commit;

-- mise a jour pour abricot
---------------------------------------------
update maracuja.type_bordereau set tbo_type_creation='VIS_102' where tbo_ordre=102;
update maracuja.type_bordereau set tbo_type_creation='VIS_101' where tbo_ordre=101;
update maracuja.type_bordereau set tbo_type_creation='DEP_1' where tbo_ordre=1;
update maracuja.type_bordereau set tbo_type_creation='DEP_2' where tbo_ordre=2;
update maracuja.type_bordereau set tbo_type_creation='DEP_3' where tbo_ordre=3;
update maracuja.type_bordereau set tbo_type_creation='DEP_4' where tbo_ordre=4;
update maracuja.type_bordereau set tbo_type_creation='DEP_5' where tbo_ordre=5;
update maracuja.type_bordereau set tbo_type_creation='REC_7' where tbo_ordre=7;
update maracuja.type_bordereau set tbo_type_creation='DEP_8' where tbo_ordre=8;
update maracuja.type_bordereau set tbo_type_creation='REC_9' where tbo_ordre=9;
update maracuja.type_bordereau set tbo_type_creation='PID_11' where tbo_ordre=11;
update maracuja.type_bordereau set tbo_type_creation='PIR_200' where tbo_ordre=200;

update maracuja.type_bordereau set tnu_ordre=3 where tbo_ordre=102;
update maracuja.type_bordereau set tnu_ordre=2 where tbo_ordre=101;
update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=1;
update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=2;
update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=3;
update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=4;
update maracuja.type_bordereau set tnu_ordre=15 where tbo_ordre=7;
update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=8;
update maracuja.type_bordereau set tnu_ordre=15 where tbo_ordre=9;
update maracuja.type_bordereau set tnu_ordre=15 where tbo_ordre=17;
update maracuja.type_bordereau set tnu_ordre=15 where tbo_ordre=200;
update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=201;

commit;
---------------------------------------------

-- ajout des fonctions dans jefy_admin
jefy_admin.api_application.creerFonction(
			 558,
			 'PRELEV',
			 'Prélèvements',
			 'Gestion des prélèvements',
			 'Gestion des prélèvements',
			 'N',
			 'N',
			 4
	  ) ;

jefy_admin.api_application.creerFonction(
			 559, --fonOrdre NUMBER,
			'BDFCAL', --fonIdInterne VARCHAR2,
			 'Administration', --fondCategorie VARCHAR2,
			'Gestion du calendrier BDF', --fonDescription VARCHAR2,
			 'Gestion du calendrier BDF', --fonLibelle VARCHAR2,
			 'N',--fonSpecGestion VARCHAR2,
			 'N',--fonSpecExercice VARCHAR2,
			 4 --tyapId NUMBER
	  ) ;
commit;



---------------------------------------------
-- Creation des anciens exercices dans jefy_admin
-- pour reprise inventaire etc.
maracuja.init_old_Exercice(1990);
maracuja.init_old_Exercice(1991);
maracuja.init_old_Exercice(1992);
maracuja.init_old_Exercice(1993);
maracuja.init_old_Exercice(1994);
maracuja.init_old_Exercice(1995);
maracuja.init_old_Exercice(1996);
maracuja.init_old_Exercice(1997);
maracuja.init_old_Exercice(1998);
maracuja.init_old_Exercice(1999);
maracuja.init_old_Exercice(2000);
maracuja.init_old_Exercice(2001);
maracuja.init_old_Exercice(2002);
maracuja.init_old_Exercice(2003);
maracuja.init_old_Exercice(2004);
maracuja.init_old_Exercice(2005);
maracuja.init_old_Exercice(2006);

commit;
	


---------------------------------------------
-- Initialisation des comptes d'amortissement
-- a partir d'inventaire
MARACUJA.RECUP_PLANCO_AMORT();
commit;
	

	
---------------------------------------------
-- preparation exercice (dupli mode paiement, 
-- recouvrements, planco_credit etc.)
maracuja.Prepare_Exercice(2007);
commit;


---------------------------------------------
-- Recuperation des droits utilisateurs vers jefy_admin
maracuja.Recup_Droits_Utilisateurs();
commit;




end;

