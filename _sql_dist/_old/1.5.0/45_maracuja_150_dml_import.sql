set define off

--
-- executer a partir de comptefi ou grhum ou dba
-----------------------------------------------------------
-- 
-- Initialisation de la nomenclature officielle comptable
-- 
-----------------------------------------------------------


DELETE FROM maracuja.PLAN_COMPTABLE_REF;
COMMIT;
 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7444', 'Subventions des communes et groupements de communes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7445', 'CNASEA'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74451', 'R�mun�ration CNASEA au titre des CES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74452', 'Frais de formation CNASEA au titre des CES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74453', 'Aide de l''Etat vers�e par le CNASEA au titre des emplois-jeunes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74454', 'Aide de l''Etat vers�e par le CNASEA au titre des emplois consolid�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7446', 'Subventions Union Europ�enne'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7447', 'Subventions d''organismes internationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7448', 'Subventions d''autres collectivit�s publiques et organismes internationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'746', 'Dons et legs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'748', 'Autres subventions d''exploitation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7481', 'Produits des versements lib�ratoires ouvrant droit � l''exon�ration de la taxe d''apprentissage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7488', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'75', 'AUTRES PRODUITS DE GESTION COURANTE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'751', 'Redevances pour concessions, brevets, licences, marques, proc�d�s, logiciels, droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7511', 'Redevances pour concessions, brevets, licences, marques, proc�d�s et logiciels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7516', 'Droits d''auteur et de reproduction'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7518', 'Autres droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'752', 'Revenus des immeubles non affect�s aux activit�s de l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'755', 'Quote-part de r�sultats sur op�rations faites en commun (GIE)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'757', 'Produits sp�cifiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'758', 'Produits divers de gestion courante'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7581', 'Frais de gestion sur ressources affect�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7583', 'Produits de gestion courante provenant de l''annulation de mandats des exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7584', 'Frais de poursuite et de contentieux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7588', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'76', 'PRODUITS FINANCIERS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'761', 'Produits des participations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7611', 'Revenus des titres de participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7616', 'Revenus sur autres formes de participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7617', 'Revenus des cr�ances rattach�es � des participations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'762', 'Produits des autres immobilisations financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7621', 'Revenus des titres immobilis�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7624', 'Revenus des pr�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7627', 'Revenus des cr�ances immobilis�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'763', 'Revenus des autres cr�ances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7638', 'Revenus sur cr�ances diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'764', 'Revenus des valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'765', 'Escomptes obtenus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'766', 'Gains de change'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'767', 'Produits nets sur cessions de valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'768', 'Autres produits financiers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'77', 'PRODUITS EXCEPTIONNELS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'771', 'Produits exceptionnels sur op�rations de gestion'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7711', 'D�dits et p�nalit�s per�us sur achats et ventes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7712', 'D�gr�vements d''imp�ts (autres qu''imp�ts sur les b�n�fices)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7713', 'Lib�ralit�s re�ues'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7714', 'Int�r�ts issus d''arr�t�s de d�bets'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7715', 'Condamnations p�cuniaires prononc�es par le juge des comptes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7716', 'Recouvrements sur cr�ances admises en non-valeur'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7717', 'Dettes atteintes par la prescription quadriennale'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7718', 'Autres produits exceptionnels sur op�rations de gestion de l''exercice'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'77181', 'Subvention d''�quilibre'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'77182', 'Produits exceptionnels provenant de l''annulation de mandats des exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'775', 'Produits des cessions d''�l�ments d''actif'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7751', 'Immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7752', 'Immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7756', 'Immobilisations financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7758', 'Autres �l�ments d''actif1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'776', 'Produits issus de la neutralisation des amortissements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'777', 'Quote-part des subventions d''investissement vir�e au r�sultat de l''exercice'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'778', 'Autres produits exceptionnels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78', 'REPRISES SUR AMORTISSEMENTS ET PROVISIONS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'781', 'Reprises sur amortissements et provisions (� inscrire dans les produits d''exploitation)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7811', 'Reprises sur amortissements des immobilisations incorporelles et corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78111', 'Immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78112', 'Immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7815', 'Reprises sur provisions pour risques et charges d''exploitation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7816', 'Reprises sur provisions pour d�pr�ciation des immobilisations incorporelles et corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78161', 'Immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78162', 'Immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7817', 'Reprises sur provisions pour d�pr�ciation des actifs circulants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78173', 'Stocks et en-cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78174', 'Cr�ances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'786', 'Reprises sur provisions pour risques (� inscrire dans les produits financiers)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7865', 'Reprises sur provisions pour risques et charges financiers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7866', 'Reprises sur provisions pour d�pr�ciation des �l�ments financiers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78662', 'Immobilisations financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'78665', 'Valeurs mobili�res de placement.'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'787', 'Reprises sur provisions (� inscrire dans les produits exceptionnels)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7875', 'Reprises sur provisions pour risques et charges exceptionnels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7876', 'Reprises sur provisions pour d�pr�ciations exceptionnelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'79', 'TRANSFERTS DE CHARGES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'791', 'Transferts de charges d''exploitation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'796', 'Transferts de charges financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'797', 'Transferts de charges exceptionnelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8', 'COMPTES SPECIAUX'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'80', 'ENGAGEMENTS HORS BILAN'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'801', 'Engagements donn�s par l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8011', 'Avals, cautions, garanties'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8016', 'Redevances cr�dit-bail restant � courir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'80161', 'Cr�dit-bail mobilier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'80165', 'Cr�dit-bail immobilier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8018', 'Autres engagements donn�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'802', 'Engagements re�us par l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8021', 'Avals, cautions, garanties'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8026', 'Engagements re�us pour utilisation en cr�dit-bail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'80261', 'Cr�dit-bail mobilier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'80265', 'cr�dit-bail immobilier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8028', 'Autres engagements re�us'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'803', 'Autorisations de programme'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'804', 'Engagements juridiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'805', 'Cr�dits de paiement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'806', 'Engagements comptables annuels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'807', 'Mandatement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'808', 'Recettes pluriannuelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8081', 'Pr�vision annuelle du financement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8082', 'Constatation des droits pluriannuels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8083', 'Constatation de la part annuelle du financement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8084', 'Encaissement attendu'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'809', 'Contrepartie des engagements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8091', 'Donn�s par l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'8092', 're�us par l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'86', 'VALEURS INACTIVES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'861', 'Comptes de position : titres et valeurs en portefeuille'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'862', 'Comptes de position : titres et valeurs chez les correspondants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'863', 'Comptes de prise en charge'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'88', 'RESULTAT EN INSTANCE D''AFFECTATION (FACULTATIF)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'89', 'BILAN (FACULTATIF)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'890', 'Bilan d''ouverture'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'891', 'Bilan de cl�ture'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'648', 'Autres charges de personnel'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'65', 'AUTRES CHARGES DE GESTION COURANTE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'651', 'Redevances pour concessions, brevets, licences, marques, proc�d�s, logiciels, droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6511', 'Redevances pour concessions, brevets, licences, marques et proc�d�s et logiciels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6516', 'Droits d''auteurs et de reproduction'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6518', 'Autres droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'654', 'Charges sur cr�ances irr�couvrables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'655', 'Quote-part de r�sultat sur op�rations faites en commun (GIE)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'657', 'Charges sp�cifiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6571', 'Bourses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6575', 'Subventions re�ues et r�parties par l''EPSCP'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6576', 'Subventions diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6578', 'Autres charges sp�cifiques 1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'658', 'Charges diverses de gestion courante'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6583', 'Charges de gestion courante provenant de l''annulation de titres de recettes des exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6588', 'Autres charges diverses de gestion courante'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'66', 'CHARGES FINANCIERES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'661', 'Charges d''int�r�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6611', 'Int�r�ts des emprunts et des dettes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6616', 'Int�r�ts bancaires sur op�rations de financement (escompte)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6618', 'Autres charges d''int�r�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'66181', 'Int�r�ts des dettes commerciales'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'664', 'Pertes sur cr�ances li�es � des participations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'665', 'Escomptes accord�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'666', 'Pertes de change'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'667', 'Charges nettes sur cessions de valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'668', 'Autres charges financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6683', 'Charges financi�res provenant de l''annulation de titres de recettes des exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6685', 'Charges financi�res provenant de l''encaissement des ch�ques vacances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6688', 'Autres charges financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'67', 'CHARGES EXCEPTIONNELLES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'671', 'Charges exceptionnelles sur op�rations de gestion'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6711', 'P�nalit�s sur contrats et conventions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6712', 'P�nalit�s, amendes fiscales et p�nales'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6713', 'Dons, lib�ralit�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6714', 'Cr�ances devenues irr�couvrables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6715', 'Subventions accord�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6716', 'D�ficits ou d�bets admis en d�charge ou en remise gracieuse'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6717', 'Int�r�ts sur d�bets admis en remise gracieuse'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6718', 'Autres charges exceptionnelles sur op�rations de gestion'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'67181', 'Rappels d''imp�t(autres qu''IS)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'67182', 'Charges exceptionnelles provenant de l''annulation de titres de recettes des exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'67188', 'Autres charges exceptionnelles diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'675', 'Valeurs comptables des �l�ments d''actif c�d�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6751', 'Immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6752', 'Immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6756', 'Immobilisations financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6758', 'Autres �l�ments d''actifs1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'678', 'Autres charges exceptionnelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68', 'DOTATIONS AUX AMORTISSEMENTS ET AUX PROVISIONS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681', 'Dotations aux amortissements et aux provisions - Charges d''exploitation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6811', 'Dotations aux amortissements sur immobilisations incorporelles et corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68111', 'Immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681111', 'Frais d''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681113', 'Frais de recherche et de d�veloppement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681115', 'Concessions et droits similaires, brevets, licences, droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681116', 'Droit au bail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681118', 'Autres immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68112', 'Immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681122', 'Agencements, am�nagements de terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681123', 'Constructions (� subdiviser comme le compte 213)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681124', 'Constructions sur sol d''autrui (� subdiviser comme le compte 214)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681125', 'Installations techniques, mat�riel et outillage (� subdiviser comme le compte 215)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681126', 'Collections (� subdiviser comme le compte 216)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'681128', 'Autres immobilisations corporelles ( � subdiviser comme le compte 218)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6812', 'Dotations aux amortissements des charges d''exploitation � r�partir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68121', 'Charges diff�r�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68122', 'Frais d''acquisition des immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68128', 'Charges � �taler'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6815', 'Dotations aux provisions pour risques et charges d''exploitation (� subdiviser comme le compte 15)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6816', 'Dotations aux provisions pour d�pr�ciation des immobilisations incorporelles et corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68161', 'Immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68162', 'Immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6817', 'Dotations aux provisions pour d�pr�ciation des actifs circulants (autres que VMP)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68173', 'Stocks et en-cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68174', 'Cr�ances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'686', 'Dotations aux amortissements et aux provisions - Charges financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6865', 'Dotations aux provisions pour risques et charges financiers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6866', 'Dotations aux provisions pour d�pr�ciation des �l�ments financiers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68662', 'Immobilisations financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'68665', 'Valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'687', 'Dotations aux amortissements et aux provisions - Charges exceptionnelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6871', 'Dotations aux amortissements exceptionnels des immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6875', 'Dotations aux provisions pour risques et charges exceptionnels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6876', 'Dotations aux provisions pour d�pr�ciation exceptionnelle'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'69', 'IMPOT SUR LES BENEFICES ET IMPOTS ASSIMILES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'695', 'Imp�t sur les b�n�fices'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'697', 'Imposition forfaitaire annuelle'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'699', 'Produits - report en arri�re des d�ficits et cr�dits d''imp�t'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7', 'COMPTES DE PRODUITS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'70', 'VENTES DE PRODUITS FABRIQUES, PRESTATIONS DE SERVICES, MARCHANDISES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'701', 'Ventes de produits finis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'702', 'Ventes de produits interm�diaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'703', 'Ventes de produits r�siduels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'704', 'Travaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'705', 'Etudes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'706', 'Prestations de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7061', 'Droits de scolarit� et redevances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'70611', 'Droits de scolarit� applicables aux dipl�mes nationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'70612', 'Droits des dipl�mes propres � chaque �tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'70613', 'Redevances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7062', 'Prestations de recherche'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7063', 'Mesures et expertises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7064', 'Prestations et travaux informatiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7065', 'Prestations de formation continue'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7066', 'Colloques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7067', 'Ventes de publications'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7068', 'Autres prestations de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'707', 'Ventes de marchandises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'708', 'Produits des activit�s annexes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7081', 'Produits des services exploit�s dans l''int�r�t du personnel'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7082', 'Commissions et courtages'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7083', 'Locations diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7084', 'Mise � disposition de personnel factur�e'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7085', 'Ports et frais accessoires factur�s aux clients'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7087', 'H�bergements et restauration'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7088', 'Autres produits d''activit�s annexes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'709', 'Rabais, remises et ristournes accord�s par l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7091', 'Sur ventes de produits finis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7092', 'Sur ventes de produits interm�diaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7093', 'Sur ventes de produits r�siduels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7094', 'Sur travaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7095', 'Sur �tudes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7096', 'Sur prestations de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7097', 'Sur ventes de marchandises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7098', 'Sur produits des activit�s annexes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'71', 'PRODUCTION STOCKEE (variation de l''exercice)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'713', 'Variation des stocks (en-cours de production, produits)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7133', 'Variation des en-cours de production de biens'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7134', 'Variation des en-cours de production de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7135', 'Variation des stocks de produits'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'72', 'PRODUCTION IMMOBILISEE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'721', 'Production immobilis�e - Immobilisations incorporelles ( � subdiviser comme le compte 20)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'722', 'Production immobilis�e - Immobilisations corporelles (� subdiviser comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74', 'SUBVENTIONS D''EXPLOITATION'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'741', 'Etat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7411', 'Minist�re de tutelle'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7418', 'Autres minist�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74186', 'Participation de l''Etat � la r�mun�ration des objecteurs de conscience'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'74188', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'744', 'Collectivit�s publiques et organismes internationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7442', 'Subventions de la r�gion'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'7443', 'Subventions du d�partement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6478', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10211', 'Biens remis en dotation et dont la charge du renouvellement n''incombe pas � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10212', 'Biens remis en dotation et dont la charge du renouvellement incombe � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1022', 'Compl�ment de dotation (Etat)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10221', 'Biens remis en compl�ment de dotation et dont la charge du renouvellement n''incombe pas � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10222', 'Biens remis en compl�ment de dotation et dont la charge du renouvellement incombe � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1023', 'Compl�ment de dotation (autres organismes)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10231', 'Biens remis en compl�ment de dotation et dont la charge du renouvellement n''incombe pas � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10232', 'Biens remis en compl�ment de dotation et dont la charge du renouvellement incombe � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1027', 'Affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10271', 'Biens remis en affectation dont la charge du renouvellement n''incombe pas � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10272', 'Biens remis en affectation dont la charge du renouvellement incombe � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'103', 'Biens remis en pleine propri�t� aux �tablissements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1031', 'Fonds propres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1032', 'Autres compl�ments de dotation - Etat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1033', 'Autres compl�ments de dotation - Autres organismes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1035', 'Dons et legs en capital'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'105', 'Ecarts de r��valuation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'106', 'R�serves'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1068', 'Autres r�serves'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10681', 'R�serve de propre assureur'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10682', 'R�serves facultatives'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'11', 'REPORT A NOUVEAU (solde cr�diteur ou d�biteur)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'110', 'Report � nouveau (solde cr�diteur)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'119', 'Report � nouveau (solde d�biteur)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'12', 'RESULTAT DE L''EXERCICE (b�n�fice ou perte)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'120', 'R�sultat de l''exercice (solde cr�diteur)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'129', 'R�sultat de l''exercice (solde d�biteur)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13', 'SUBVENTIONS D''INVESTISSEMENT'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'131', 'Subventions d''�quipement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1311', 'Etat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1312', 'R�gions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1313', 'D�partements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1314', 'Communes et groupements de communes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1315', 'Autres collectivit�s et �tablissements publics'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1316', 'Union Europ�enne'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1317', 'Autres organismes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1318', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'138', 'Autres subventions d''investissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'139', 'Subventions d''investissement inscrites au compte de r�sultat (soldes d�biteurs)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1391', 'Subventions d''�quipement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13911', 'Etat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13912', 'R�gions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13913', 'D�partements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13914', 'Communes et groupements de communes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13915', 'Autres collectivit�s et �tablissements publics'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13916', 'Union Europ�enne'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13917', 'Autres organismes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'13918', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1398', 'Autres subventions d''investissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'15', 'PROVISIONS POUR RISQUES ET CHARGES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'151', 'Provisions pour risques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1511', 'Provisions pour litiges'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1515', 'Provisions pour pertes de change'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1518', 'Autres provisions pour risques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'157', 'Provisions pour charges � r�partir sur plusieurs exercices'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1572', 'Provisions pour grosses r�parations.'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'158', 'Autres provisions pour risques et charges'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1582', 'Provisions pour charges sociales et fiscales sur cong�s � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1587', 'Provisions pour allocation perte d''emploi d''indemnit�s de licenciement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'16', 'EMPRUNTS ET DETTES ASSIMILEES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'164', 'Emprunts aupr�s des �tablissements de cr�dit'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'165', 'D�p�ts et cautionnements re�us'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1651', 'D�p�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1655', 'Cautionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'167', 'Emprunts et dettes assortis de conditions particuli�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1674', 'Avances de l''Etat et des collectivit�s publiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1678', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'168', 'Autres emprunts et dettes assimil�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1688', 'Int�r�ts courus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'17', 'DETTES RATTACHEES A DES PARTICIPATIONS1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'171', 'Dettes rattach�es � des participations (groupe)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'174', 'Dettes rattach�es � des participations (hors groupe)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'178', 'Dettes rattach�es � des soci�t�s en participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'18', 'COMPTES DE LIAISON'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'181', 'Comptes de liaison'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'185', 'Op�rations de tr�sorerie inter-services (SACD)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'186', 'Biens et prestations de services �chang�s entre composantes et services communs dot�s d''un budget propre int�gr� (charges)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'187', 'Biens et prestations de services �chang�s entre composantes et services communs dot�s d''un budget propre int�gr� (produits)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2', 'COMPTES D''IMMOBILISATIONS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'20', 'IMMOBILISATIONS INCORPORELLES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'201', 'Frais d''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'203', 'Frais de recherche et de d�veloppement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'205', 'Concessions et droits similaires, brevets, licences, marques, proc�d�s, logiciels, droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2053', 'Logiciels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'20531', 'Logiciels acquis ou sous-trait�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'20532', 'Logiciels cr��s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2058', 'Autres concessions et droits similaires, brevets, licences, marques, proc�d�s, droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'206', 'Droit au bail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'208', 'Autres immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21', 'IMMOBILISATIONS CORPORELLES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'211', 'Terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2111', 'Terrains nus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21116', 'Re�us en dotation ou en affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21117', 'Acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21118', 'Autres terrains nus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2112', 'Terrains am�nag�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21126', 'Re�us en dotation ou en affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21127', 'Acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21128', 'Autres terrains am�nag�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2115', 'Terrains b�tis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21156', 'Re�us en dotation ou en affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21157', 'Acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21158', 'Autres terrains b�tis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'212', 'Agencements et am�nagements de terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2121', 'Agencements, am�nagements de terrains nus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21216', 'Re�us en dotation ou en affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21217', 'Acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21218', 'Autres terrains nus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2122', 'Agencements, am�nagements de terrains am�nag�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21226', 'Re�us en dotation ou en affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21227', 'Acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21228', 'Autres terrains am�nag�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2125', 'Agencements, am�nagements de terrains b�tis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21256', 'Re�us en dotation ou en affectation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21257', 'Acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21258', 'Autres terrains b�tis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'213', 'Constructions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2131', 'B�timents'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21316', 'B�timents affect�s ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21317', 'B�timents acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21318', 'Autres b�timents'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2135', 'Installations g�n�rales, agencements, am�nagements des constructions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21356', 'Installations g�n�rales, agencements, am�nagements des constructions affect�es ou remises en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21357', 'Installations g�n�rales, agencements, am�nagements des constructions acquises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21358', 'Autres installations g�n�rales, agencements, am�nagements des constructions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'214', 'Constructions sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2141', 'B�timents sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21416', 'B�timents sur sol d''autrui affect�s ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21417', 'B�timents sur sol d''autrui acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21418', 'Autres b�timents sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2145', 'Installations g�n�rales, agencements, am�nagements des constructions sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21456', 'Installations g�n�rales, agencements, am�nagements des constructions sur sol d''autrui affect�s ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21457', 'Installations g�n�rales, agencements, am�nagements des constructions sur sol d''autrui acquises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21458', 'Autres installations g�n�rales, agencements, am�nagements des constructions sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'215', 'Installations techniques, mat�riels et outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2151', 'Installations techniques complexes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21516', 'Installations techniques complexes affect�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21517', 'Installations techniques complexes acquises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21518', 'Autres Installations techniques complexes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2153', 'Mat�riel scientifique'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21536', 'Mat�riel scientifique affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21537', 'Mat�riel scientifique acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21538', 'Autres mat�riels scientifiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2154', 'Mat�riel d''enseignement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1', 'COMPTES DE CAPITAUX'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'10', 'CAPITAL ET RESERVES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'102', 'Biens mis � disposition des �tablissements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'1021', 'Dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21546', 'Mat�riel d''enseignement affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21547', 'Mat�riel d''enseignement acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21548', 'Autres mat�riels d''enseignement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2155', 'Outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21556', 'Outillage affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21557', 'Outillage acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21558', 'Autres outillages'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2157', 'Agencements et am�nagements du mat�riel et outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21576', 'Agencements et am�nagements du mat�riel et outillage affect�s ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21577', 'Agencements et am�nagements du mat�riel et outillage acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21578', 'Autres agencements et am�nagements du mat�riel et outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'216', 'Collections'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2161', 'Collections de documentation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21616', 'Collections affect�es ou remises en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21617', 'Collections acquises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21618', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2164', 'Collections litt�raires, scientifiques, artistiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21646', 'Collections litt�raires, scientifiques, artistiques affect�es ou remises en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21647', 'Collections litt�raires, scientifiques, artistiques acquises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21648', 'Autres collections litt�raires, scientifiques, artistiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'218', 'Autres immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2181', 'Installations g�n�rales, agencements, am�nagements divers (dans des constructions dont l''�tablissement n''est pas propri�taire ou affectataire ou qu''il n''a pas re�ues en dotation)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21816', 'Installations g�n�rales, agencements, am�nagements divers affect�s ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21817', 'Installations g�n�rales, agencements, am�nagements divers acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21818', 'Autres installations g�n�rales, agencements, am�nagements divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2182', 'Mat�riel de transport'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21826', 'Mat�riel de transport affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21827', 'Mat�riel de transport acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21828', 'Autre mat�riel de transport'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2183', 'Mat�riel de bureau'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21836', 'Mat�riel de bureau affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21837', 'Mat�riel de bureau acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21838', 'Autre mat�riel de bureau'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2184', 'Mobilier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21846', 'Mobilier affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21847', 'Mobilier acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21848', 'Autre mobilier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2185', 'Cheptel'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2186', 'Emballages r�cup�rables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2187', 'Mat�riel informatique'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21876', 'Mat�riel informatique affect� ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21877', 'Mat�riel informatique acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21878', 'Autre mat�riel informatique'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2188', 'Mat�riels divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21886', 'Mat�riels divers affect�s ou remis en dotation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21887', 'Mat�riels divers acquis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'21888', 'Autres mat�riels divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23', 'IMMOBILISATIONS EN COURS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'231', 'Immobilisations corporelles en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2312', 'Terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2313', 'Constructions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2314', 'Constructions sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2315', 'Installations techniques, mat�riel et outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2318', 'Autres immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'232', 'Immobilisations incorporelles en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2325', 'Logiciels'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23251', 'Logiciels sous-trait�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23252', 'Logiciels cr��s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'237', 'Avances et acomptes vers�s sur immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2371', 'Avances vers�es sur immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2372', 'Acomptes vers�s sur immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'238', 'Avances et acomptes vers�s sur commandes d''immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2381', 'Avances vers�es sur immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23812', 'Terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23813', 'Constructions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23814', 'Constructions sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23815', 'Installations techniques, mat�riel et outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23818', 'Autres immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2382', 'Acomptes vers�s sur immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23822', 'Terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23823', 'Constructions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23824', 'Constructions sur sol d''autrui'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23825', 'Installations techniques, mat�riel et outillage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'23828', 'Autres immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'26', 'PARTICIPATIONS ET CREANCES RATTACHEES � DES PARTICIPATIONS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'261', 'Titres de participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'266', 'Autres formes de participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'267', 'Cr�ances rattach�es � des participations1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2671', 'Cr�ances rattach�es � des participations (groupe)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2674', 'Cr�ances rattach�es � des participations (hors groupe)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2676', 'Avances consolidables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2677', 'Autres cr�ances rattach�es � des participations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'268', 'Cr�ances rattach�es � des soci�t�s en participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'269', 'Versement � effectuer sur titres de participation non lib�r�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'27', 'AUTRES IMMOBILISATIONS FINANCIERES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'271', 'Titres immobilis�s (droit de propri�t�)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2711', 'Actions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2718', 'Autres titres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'272', 'Titres immobilis�s (droits de cr�ance)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2721', 'Obligations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2722', 'Bons'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2728', 'Autres titres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'274', 'Pr�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2741', 'Pr�ts �tudiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2748', 'Autres pr�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'275', 'D�p�ts et cautionnements vers�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2751', 'D�p�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2755', 'Cautionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'276', 'Autres cr�ances immobilis�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2761', 'Cr�ances diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2768', 'Int�r�ts courus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'28', 'AMORTISSEMENTS DES IMMOBILISATIONS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'280', 'Amortissements des immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2801', 'Frais d''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2803', 'Frais de recherche et de d�veloppement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2805', 'Concessions et droits similaires, brevets, licences, droits et valeurs similaires (m�me ventilation que celle du compte 205)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2808', 'Autres immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'281', 'Amortissements des immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2812', 'Agencements et am�nagements de terrains (m�me ventilation que celle du compte 212)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2813', 'Construction (m�me ventilation que celle du compte 213)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2814', 'Construction sur sol d''autrui (m�me ventilation que celle du compte 214)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2815', 'Installations techniques, mat�riel et outillage (m�me ventilation que celle du compte 215)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2816', 'Collections'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'28161', 'Collections de documentation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2818', 'Autres immobilisations corporelles (m�me ventilation que celle du compte 218)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'283', 'Amortissement des immobilisations incorporelles dont la charge du renouvellement n''incombe pas � l''�tablissement (m�me subdivisions que le compte 280)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'284', 'Amortissement des immobilisations corporelles dont la charge du renouvellement n''incombe pas � l''�tablissement (m�mes subdivisions que le compte 281)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29', 'PROVISIONS POUR DEPRECIATION DES IMMOBILISATIONS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'290', 'Provisions pour d�pr�ciation des immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2905', 'Marques, proc�d�s, droits et valeurs similaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2906', 'Droit au bail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2908', 'Autres immobilisations incorporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'291', 'Provisions pour d�pr�ciation des immobilisations corporelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2911', 'Terrains'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'293', 'Provisions pour d�pr�ciation des immobilisations en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2931', 'Immobilisations corporelles en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2932', 'Immobilisations incorporelles en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'296', 'Provisions pour d�pr�ciation des participations et cr�ances rattach�es � des participations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2961', 'Titres de participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2966', 'Autres formes de participation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2967', 'Cr�ances rattach�es � des participations1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2968', 'Cr�ances rattach�es � des soci�t�s en participations2'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'297', 'Provisions pour d�pr�ciation des autres immobilisations financi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2971', 'Titres immobilis�s (droit de propri�t�)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2972', 'Titres immobilis�s (droits de cr�ance)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2974', 'Pr�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29741', 'Pr�ts �tudiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29748', 'Autres pr�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2975', 'D�p�ts et cautionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29751', 'D�p�ts'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29755', 'Cautionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'2976', 'Autres cr�ances immobilis�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29761', 'Cr�ances diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'29768', 'Int�r�ts courus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3', 'COMPTES DE STOCKS ET D''EN-COURS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'31', 'MATIERES PREMIERES (et fournitures)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'311', 'Mati�re A'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'312', 'Mati�re B'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'317', 'Fournitures A,B,C...'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'32', 'AUTRES APPROVISIONNEMENTS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'321', 'Mati�res consommables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3211', 'Carburants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'322', 'fournitures consommables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3221', 'Combustibles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3222', 'Produits d''entretien'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3223', 'Fournitures d''atelier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3224', 'Fournitures de magasin'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3225', 'Fournitures de bureau'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'3228', 'Autres fournitures consommables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'326', 'Emballages'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'33', 'EN-COURS DE PRODUCTION DE BIENS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'331', 'Produits en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'335', 'Travaux en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'34', 'EN-COURS DE PRODUCTION DE SERVICES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'341', 'Etudes en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'345', 'Prestations de services en cours'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'35', 'STOCKS DE PRODUITS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'351', 'Produits interm�diaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'355', 'Produits finis'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'358', 'Produits r�siduels (ou mati�res de r�cup�ration)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'37', 'STOCKS DE MARCHANDISES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'371', 'Marchandises A'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'372', 'Marchandises B'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'39', 'PROVISIONS POUR DEPRECIATION DES STOCKS ET EN-COURS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'391', 'Provisions pour d�pr�ciation des mati�res premi�res (et fournitures)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'392', 'Provisions pour d�pr�ciation des autres approvisionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'393', 'Provisions pour d�pr�ciation des en-cours de production de biens'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'394', 'Provisions pour d�pr�ciation des en-cours de production de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'395', 'Provisions pour d�pr�ciation des stocks de produits'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'397', 'Provisions pour d�pr�ciation des stocks de marchandises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4', 'COMPTES DE TIERS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'40', 'FOURNISSEURS ET COMPTES RATTACHES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'401', 'Fournisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4011', 'Fournisseurs - Exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4012', 'Fournisseurs - Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4017', 'Retenues de garanties et oppositions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'403', 'Fournisseurs - Effets � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'404', 'Fournisseurs d''immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4041', 'Fournisseurs d''immobilisations - Exercice ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4042', 'Fournisseurs d''immobilisations - Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4047', 'Fournisseurs d''immobilisations - Retenues de garantie et oppositions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'405', 'Fournisseurs d''immobilisations - Effets � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'408', 'Fournisseurs - Factures non parvenues'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4081', 'Fournisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4084', 'Fournisseurs d''immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4088', 'Fournisseurs - Int�r�ts courus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'409', 'Fournisseurs d�biteurs (comptes d''actif)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4091', 'Fournisseurs - Avances et acomptes vers�s sur commandes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'40911', 'Avances vers�es sur commandes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'40912', 'Acomptes vers�s sur commandes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4092', 'Avances � l''UGAP'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4096', 'Fournisseurs - Cr�ances pour emballages et mat�riels � rendre'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4098', 'Rabais, remises, ristournes � obtenir et autres avoirs non encore re�us'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'41', 'CLIENTS ET COMPTES RATTACHES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'411', 'Clients divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4111', 'Clients - Exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4112', 'Clients - Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4117', 'Retenues de garanties'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'412', 'Etudiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4121', 'Etudiants - Exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4122', 'Etudiants - Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'413', 'Clients - Effets � recevoir sur ventes de biens ou de prestations de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'416', 'Cr�ances contentieuses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'418', 'Clients - Produits non encore factur�s (ordre de recettes � �tablir)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'419', 'Clients et �tudiants cr�diteurs (comptes de passif)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4191', 'Clients - Avances et acomptes re�us sur commande'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4192', 'Etudiants - avances et acomptes re�us'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4196', 'Clients - Dettes pour emballages et mat�riels consign�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4197', 'Clients et �tudiants - autres avoirs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4198', 'Clients - Rabais, remises, ristournes � accorder et autres avoirs � �tablir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42', 'PERSONNEL ET COMPTES RATTACHES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'421', 'Personnel - R�mun�rations dues'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4211', 'Exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4212', 'Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'422', 'Oeuvres sociales'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'425', 'Personnel - Avances et acomptes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'427', 'Personnel - Oppositions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'428', 'Personnel - charges � payer et produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4282', 'Dettes provisionn�es pour cong�s pay�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4286', 'Autres charges � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4287', 'Produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'429', 'D�ficits et d�bets des comptables et r�gisseurs et redevables d''int�r�ts et condamnations p�cuniaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4291', 'D�ficits constat�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42911', 'Comptables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42912', 'R�gisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4292', 'Ordres de versement �mis suite � constatation de d�ficit'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42921', 'Comptables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42922', 'R�gisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4294', 'D�bets �mis par arr�t� du ministre'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42941', 'Comptables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42942', 'R�gisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4295', 'D�bets �mis par jugement ou arr�t du juge des comptes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42951', 'Comptables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42952', 'R�gisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4296', 'Redevables d''int�r�ts sur d�bet'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42961', 'Comptables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42962', 'R�gisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4297', 'Redevables des condamnations p�cuniaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42971', 'Comptables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'42972', 'R�gisseurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'43', 'SECURITE SOCIALE ET AUTRES ORGANISMES SOCIAUX'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'431', 'S�curit� sociale'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4311', 'Cotisations de s�curit� sociale'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4312', 'Contribution sociale g�n�ralis�e'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4313', 'Contribution au remboursement de la dette sociale (CRDS)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'437', 'Autres organismes sociaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4371', 'Contribution exceptionnelle de solidarit�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4372', 'Contributions et retenues pour pensions civiles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4374', 'IRCANTEC'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4375', 'PREFON'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4376', 'Autres Mutuelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4377', 'ASSEDIC'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4378', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'438', 'Organismes sociaux - Charges � payer et produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4382', 'Charges sociales sur cong�s � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4386', 'Charges � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4387', 'Produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44', 'ETAT ET AUTRES COLLECTIVITES PUBLIQUES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'441', 'Etat et autres collectivit�s publiques - Subventions � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4411', 'Subvention d''investissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44111', 'Etat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44112', 'R�gions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44113', 'D�partements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44114', 'Communes et groupements de communes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44115', 'Autres collectivit�s et �tablissements publics'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44116', 'Union Europ�enne'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44117', 'Autres organismes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4417', 'Subvention d''exploitation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44171', 'Etat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44172', 'R�gions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44173', 'D�partements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44174', 'Communes et groupements de communes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44175', 'Autres collectivit�s et �tablissements publics'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44176', 'Union Europ�enne'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44177', 'Autres organismes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4418', 'Subvention d''�quilibre'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'443', 'Op�rations particuli�res avec l''Etat, les collectivit�s publiques, les organismes internationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4431', 'Cr�ance sur l''Etat r�sultant de la suppression de la r�gle du d�calage d''un mois en mati�re de TVA'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4435', 'Op�rations particuli�res avec le CNASEA'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44351', 'Aide de l''Etat vers�e par le CNASEA au titre des contrats emploi-solidarit�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44353', 'Aide de l''Etat vers�e par le CNASEA au titre des contrats emploi-jeune'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44354', 'Aide de l''Etat vers�e par le CNASEA au titre des contrats emploi-consolid�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4438', 'Int�r�ts courus sur cr�ance sur l''Etat r�sultant de la suppression de la r�gle du d�calage d''un mois en mati�re de TVA'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'444', 'Imp�t sur les b�n�fices'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4441', 'Etat - Cr�ance de carry-back'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4442', 'Etat - Imp�ts sur les b�n�fices - Acomptes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4443', 'Etat - Imposition forfaitaire annuelle'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4449', 'Etat - avances sur subventions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445', 'Etat - Taxe sur le chiffre d''affaires (TVA)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4452', 'TVA due intra-communautaire'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4455', 'TVA � d�caisser'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4456', 'TVA d�ductible'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44562', 'TVA d�ductible sur immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445621', 'TVA d�ductible sur immobilisations - France'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445622', 'TVA d�ductible sur immobilisations intra-communautaire'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445623', 'TVA d�ductible sur immobilisations - Autres pays'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44566', 'TVA d�ductible sur autres biens et services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445661', 'TVA d�ductible sur autres biens et services - France'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445662', 'TVA d�ductible sur autres biens et services intra-communautaire'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445663', 'TVA d�ductible sur autres biens et services - Autres pays'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44567', 'Cr�dit de T.V.A. � reporter'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445671', 'Cr�dit de T.V.A. � reporter sur achats - France'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445672', 'Cr�dit de T.V.A. � reporter sur achats intra-communautaire'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'445673', 'Cr�dit de TVA � reporter sur achats - Autres pays'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4457', 'TVA collect�e par l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4458', 'TVA � r�gulariser ou en attente'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44581', 'Acomptes-r�gime simplifi� d''imposition'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44583', 'Remboursement de TVA demand�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44584', 'TVA r�cup�r�e d''avance'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'44587', 'TVA sur facturation � �tablir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'447', 'Autres imp�ts, taxes et versements assimil�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4472', 'Taxe sur les salaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4478', 'Divers autres imp�ts, taxes et versements assimil�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'448', 'Etat et autres collectivit�s publiques - Charges � payer et produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4482', 'Charges fiscales sur cong�s � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4486', 'Charges � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4487', 'Produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'45', 'GROUPE ET ASSOCIES1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'451', 'Groupe'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'455', 'Associ�s - Compte courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'458', 'Associ�s - Op�rations faites en commun et en GIE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'46', 'DEBITEURS DIVERS ET CREDITEURS DIVERS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'462', 'Cr�ances sur cessions d''immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'463', 'Autres comptes d�biteurs - Ordres de recettes ou ordres de reversement � recouvrer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4631', 'Exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4632', 'Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'464', 'Dettes sur acquisitions de valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'465', 'Cr�ances sur cessions de valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'466', 'Autres comptes cr�diteurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4661', 'Autres comptes cr�diteurs - mandats � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'46611', 'Exercices ant�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'46612', 'Exercice courant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4663', 'Virements � r�imputer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4664', 'Exc�dents de versement � rembourser'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4666', 'Ordres de paiement �mis pour les d�penses � l''�tranger'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4667', 'Oppositions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4668', 'Avis de paiement (� subdiviser par exercice d''origine)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'467', 'Autres comptes d�biteurs ou cr�diteurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4674', 'Taxe d''apprentissage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'468', 'Produits � recevoir et charges � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4682', 'Charges � payer sur conventions et autres ressources affect�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4684', 'Produits � recevoir sur conventions et autres ressources affect�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4686', 'Charges � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4687', 'Produits � recevoir'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47', 'COMPTES TRANSITOIRES OU D''ATTENTE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'471', 'Recettes � classer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4712', 'Recettes des comptables secondaires � v�rifier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4713', 'Recettes per�ues avant �mission de titres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47132', 'Droits de scolarit� et redevances au comptant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'471321', 'Droits de scolarit� applicables aux dipl�mes nationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'471322', 'Droits des dipl�mes propres � chaque �tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'471323', 'Redevances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47138', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4715', 'Recettes des r�gisseurs � v�rifier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4718', 'Autres recettes � classer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'472', 'D�penses � classer et � r�gulariser'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4721', 'D�penses pay�es avant ordonnancement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4722', 'D�penses des comptables secondaires � v�rifier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4725', 'D�penses des r�gisseurs � v�rifier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4728', 'Autres d�penses � r�gulariser'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4729', 'D�penses dont le paiement est diff�r�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'473', 'Recettes et d�penses � transf�rer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4731', 'Recettes � transf�rer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47311', 'S�curit� sociale �tudiante'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47312', 'Mutuelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47314', 'M�decine Pr�ventive'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47315', 'Droits des biblioth�ques universitaires non rattach�es � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'47318', 'Autres recettes � transf�rer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4735', 'D�penses � transf�rer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'476', 'Diff�rences de conversion sur op�rations en devises - ACTIF'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4761', 'Diminution des cr�ances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4762', 'Augmentation des dettes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4768', 'Diff�rences compens�es par couverture de change'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'477', 'Diff�rences de conversion sur op�rations en devises - PASSIF'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4771', 'Augmentation des cr�ances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4772', 'Diminution des dettes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4778', 'Diff�rences compens�es par couverture de change'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'478', 'Autres comptes transitoires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'48', 'COMPTES DE REGULARISATION'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'481', 'Charges � r�partir sur plusieurs exercices (budg�tis�es)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4811', 'Charges diff�r�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4812', 'Frais d''acquisitions des immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4818', 'Charges � �taler'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'486', 'Charges constat�es d''avance (imputables � l''exercice suivant)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'487', 'Produits constat�s d''avance (� rattacher � l''exercice suivant)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'49', 'PROVISIONS POUR DEPRECIATION DES COMPTES DE TIERS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'491', 'Provisions pour d�pr�ciation des comptes de clients et �tudiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4911', 'Clients divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4912', 'Etudiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'495', 'Provisions pour d�pr�ciation des comptes du groupe et des associ�s1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4951', 'Compte du groupe'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4958', 'Op�rations faites en commun et en GIE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'496', 'Provisions pour d�pr�ciation des comptes de d�biteurs divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4962', 'Cr�ances sur cessions d''immobilisations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4965', 'Cr�ances sur cessions de valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'4967', 'Autres comptes d�biteurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5', 'COMPTES FINANCIERS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'50', 'VALEURS MOBILIERES DE PLACEMENT'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'503', 'Actions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'504', 'Autres titres conf�rant un droit de propri�t�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'506', 'Obligations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'507', 'Bons du Tr�sor et bons de caisse � court terme'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'508', 'Autres valeurs mobili�res de placement et autres cr�ances assimil�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5081', 'Autres valeurs mobili�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5088', 'Int�r�ts courus sur obligations, bons et valeurs assimil�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'509', 'Versements restant � effectuer sur valeurs mobili�res de placement non lib�r�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'51', 'BANQUES, ETABLISSEMENTS FINANCIERS ET ASSIMILES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'511', 'Valeurs � l''encaissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5112', 'Ch�ques bancaires � encaisser'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5113', 'Ch�ques vacances � l''encaissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5114', 'Ch�ques postaux � encaisser'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5115', 'Cartes bancaires � l''encaissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5117', 'Ch�ques impay�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5118', 'Autres valeurs � l''encaissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'512', 'Banques1'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5121', 'Compte en monnaie nationale'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5124', 'Compte en monnaies �trang�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'514', 'Ch�ques postaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5141', 'Ch�ques postaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5149', 'Ch�ques postaux � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'515', 'Tr�sor'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5151', 'Compte au Tr�sor'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5159', 'Ch�ques � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'517', 'Autres organismes financiers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'518', 'Int�r�ts courus'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'53', 'CAISSE'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'531', 'Caisse'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'54', 'REGIES D''AVANCES ET ACCREDITIFS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'541', 'Comptables secondaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'543', 'R�gies d''avances'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'545', 'R�gies de recettes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'548', 'Avances pour menues d�penses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'58', 'VIREMENTS INTERNES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'581', 'Virements internes de comptes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'585', 'Virements internes de fonds'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'59', 'PROVISIONS POUR DEPRECIATION DES COMPTES FINANCIERS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'590', 'Provisions pour d�pr�ciation des valeurs mobili�res de placement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5903', 'Actions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5904', 'Autres titres conf�rant un droit de propri�t�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5906', 'Obligations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'5908', 'Autres valeurs mobili�res de placements et autres cr�ances assimil�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6', 'COMPTES DE CHARGES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60', 'ACHATS ET VARIATION DE STOCKS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'601', 'Achats stock�s - mati�res premi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6011', 'Mati�re A'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6012', 'Mati�re B'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6017', 'Fournitures A,B,C'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'602', 'Achats stock�s - autres approvisionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6021', 'Mati�res consommables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60211', 'Carburants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6022', 'fournitures consommables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60221', 'Combustibles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60222', 'Produits d''entretien'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60223', 'Fournitures d''atelier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60224', 'Fournitures de magasin'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60225', 'Fournitures de bureau'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60228', 'Autres fournitures consommables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6026', 'Emballages stock�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'603', 'Variation des stocks'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6031', 'Variation des stocks de mati�res premi�res (peut �tre subdivis� suivant le m�me d�tail que le compte 601)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6032', 'Variation des stocks des autres approvisionnements (peut �tre subdivis� suivant le m�me d�tail que le compte 602)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6037', 'Variation des stocks de marchandises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'604', 'Achats d''�tudes et prestations de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'605', 'Achat de mat�riel, �quipements et travaux (incorpor�s aux ouvrages et aux produits)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'606', 'Achats non stock�s de mati�res et fournitures'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6061', 'Fournitures non stockables - eau, �nergie'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60611', 'Electricit�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60612', 'Carburants et lubrifiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60613', 'Gaz'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60614', 'Chauffage sur r�seau'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60617', 'Eau'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'60618', 'Autres fournitures non stockables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6062', 'Acquisition de papier'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6063', 'Fournitures d''entretien et de petit �quipement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6064', 'Fournitures administratives'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6065', 'Linge, v�tements de travail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6067', 'Fournitures et mat�riels d''enseignement et de recherche non immobilis�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6068', 'Autres mati�res et fournitures non stock�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'607', 'Achats de marchandises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'608', 'Frais accessoires d''achat'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'609', 'Rabais, remises et ristournes obtenus sur achats'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6091', 'De mati�res premi�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6092', 'D''autres approvisionnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6094', 'D''�tudes et de prestations de services'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6095', 'De mat�riels, �quipements et travaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6096', 'D''approvisionnements non stock�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6097', 'De marchandises'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6098', 'Rabais, remises, ristournes obtenus non affect�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'61', 'SERVICES EXTERIEURS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'611', 'Sous-traitance g�n�rale'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'612', 'Redevances de cr�dit-bail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6122', 'Cr�dit-bail mobilier (peut �tre subdivis� comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6125', 'Cr�dit-bail immobilier (peut �tre subdivis� comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'613', 'Locations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6132', 'Locations immobili�res (peut �tre subdivis� comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6135', 'Locations mobili�res (peut �tre subdivis� comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6136', 'Mali sur emballages restitu�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'614', 'Charges locatives et de copropri�t�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'615', 'Travaux d''entretien et de r�parations'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6152', 'Sur biens immobiliers (� subdiviser comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6155', 'Sur biens mobiliers (� subdiviser comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6156', 'Maintenance (� subdiviser comme le compte 21)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'616', 'Primes d''assurance'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'617', 'Etudes et recherches'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6181', 'Documentation g�n�rale et administrative'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6183', 'Documentation technique et biblioth�que'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'61831', 'Abonnements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618311', 'Fran�ais'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618312', 'Etrangers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'61832', 'Ouvrages'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618321', 'Fran�ais'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618322', 'Etrangers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'61833', 'Ouvrages �lectroniques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618331', 'Fran�ais'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'618332', 'Etrangers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6184', 'Reprographie'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6185', 'Frais de colloques, s�minaires, conf�rences'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'619', 'Rabais, remises et ristournes obtenus sur services ext�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'62', 'AUTRES SERVICES EXTERIEURS'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'621', 'Personnel ext�rieur � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6211', 'Personnel int�rimaire'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6214', 'Personnel d�tach� ou pr�t� � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'622', 'R�mun�rations d''interm�diaires et honoraires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6221', 'Commissions et courtages sur achats'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6222', 'Commissions et courtages sur ventes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6224', 'R�mun�rations des transitaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6226', 'Honoraires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6227', 'Frais d''actes et de contentieux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6228', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'623', 'Publicit�, publications, relations publiques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6231', 'Annonces et insertions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6232', 'Echantillons'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6233', 'Foires et expositions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6236', 'Catalogues et imprim�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6237', 'Publications'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6238', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'624', 'Transports de biens et transports collectifs de personnes (frais pay�s � des tiers)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6241', 'Transports sur achats'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6242', 'Transports sur ventes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6244', 'Transports administratifs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6247', 'Transports collectifs du personnel'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6248', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'625', 'D�placements, missions et r�ceptions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6254', 'Frais d''inscription aux colloques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6255', 'Frais de d�m�nagement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6256', 'Missions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'62561', 'Personnels de l''EPSCP'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'62562', 'Etudiants'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'62563', 'Personnalit�s ext�rieures'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6257', 'R�ceptions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'626', 'Frais postaux et de t�l�communications'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6264', 'T�l�phone'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6265', 'Affranchissements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6266', 'Internet'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6267', 'Liaisons sp�cialis�es'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'627', 'Services bancaires et assimil�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6271', 'Frais sur titres (achat, vente, garde)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6272', 'Commissions sur cartes bancaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6278', 'Autres frais et commissions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'628', 'Divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6281', 'Concours divers (cotisations...)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6282', 'Blanchissage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6283', 'Formation continue du personnel de l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6284', 'Frais de recrutement du personnel'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6286', 'Contrats de nettoyage'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6288', 'Autres prestations ext�rieures diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'62885', 'Facturation des payes � fa�on effectu�es par les services d�concentr�s du Tr�sor'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'62888', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'629', 'Rabais, remises et ristournes obtenus sur autres services ext�rieurs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63', 'IMPOTS, TAXES ET VERSEMENTS ASSIMILES'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'631', 'Imp�ts, taxes et versements assimil�s sur r�mun�rations (administration des imp�ts)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6311', 'Taxe sur les salaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6318', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'632', 'Charges fiscales sur cong�s pay�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'633', 'Imp�ts, taxes et versements assimil�s sur r�mun�rations (autres organismes)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6331', 'Versement de transport'); 
COMMIT;
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6332', 'Cotisation FNAL (article L 834-1-1� du code de la s�curit� sociale)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6338', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'635', 'Autres imp�ts, taxes et versements assimil�s (administration des imp�ts)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6351', 'Imp�ts directs'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63511', 'Taxe professionnelle'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63512', 'Taxes fonci�res'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63513', 'Autres imp�ts ou taxes � caract�re local'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63514', 'Taxe sur les bureaux - r�gion �le de France (article 231 du CGI)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6352', 'Taxes sur le chiffre d''affaires non r�cup�rables'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6353', 'Imp�ts indirects'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6354', 'Droits d''enregistrement et de timbre'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63541', 'Droits de mutation'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'63542', 'Taxe diff�rentielle sur les v�hicules � moteur'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6358', 'Autres droits'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'637', 'Autres imp�ts, taxes et versements assimil�s (autres organismes)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6372', 'Taxes per�ues par les organismes publics internationaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6374', 'Imp�ts et taxes exigibles � l''�tranger'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6378', 'Taxes diverses'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64', 'CHARGES DE PERSONNEL'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'641', 'R�mun�rations du personnel'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6411', 'Salaires, appointements'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6412', 'Cong�s pay�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6413', 'Primes et gratifications'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6414', 'Indemnit�s et avantages divers'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64142', 'Vacations administratives et techniques'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64143', 'Heures suppl�mentaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64144', 'Formation continue'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64145', 'Contrats de recherche'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64148', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6415', 'Suppl�ment familial de traitement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6417', 'Indemnit�s de pr�avis et de licenciement et allocation pour perte d''emploi'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64171', 'Allocation unique d�gressive'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64172', 'Indemnit�s de licenciement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'641721', 'Indemnit� de pr�avis et de licenciement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'641722', 'Allocation pour perte d''emploi'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'642', 'Cours compl�mentaires'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6421', 'Personnel de l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6422', 'Personnel ext�rieur � l''�tablissement'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64221', 'Ext�rieur enseignant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64222', 'Ext�rieur non enseignant'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'643', 'Emplois gag�s sur formation continue'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'644', 'R�mun�rations du personnel recrut� en application de conventions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6443', 'R�mun�rations du personnel recrut� sous contrat emploi-consolid�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6444', 'R�mun�rations du personnel recrut� sous contrat emploi-jeune'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6445', 'R�mun�rations du personnel recrut� sous contrat emploi - solidarit�'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6446', 'R�mun�rations des objecteurs de conscience'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6447', 'Indemnit�s de pr�avis et de licenciement et allocation pour perte d''emploi'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64471', 'Allocation unique d�gressive'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6448', 'Autres r�mun�rations du personnel recrut� en application de conventions'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'645', 'Charges de S�curit� Sociale et de pr�voyance'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6451', 'Cotisations � l''URSSAF'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6452', 'Cotisations aux Mutuelles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6453', 'Cotisations aux caisses de retraites et de pensions civiles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64532', 'Pensions civiles'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64534', 'IRCANTEC'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6454', 'Cotisations aux ASSEDIC'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64541', 'CES-Cotisation assurance ch�mage (part patronale)'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64542', 'emplois jeunes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64543', 'Intermittents du spectacle'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64548', 'Autres'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6455', 'Charges sociales sur cong�s � payer'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6456', 'Prestations directes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64561', 'Accidents du travail'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64562', 'Capital d�c�s'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64563', 'Traitements et indemnit�s des fonctionnaires en cong� de longue dur�e'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'64568', 'Autres prestations directes'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6457', 'Cotisations horaires forfaitaires RMI'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6458', 'Cotisations aux autres organismes sociaux'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'647', 'Autres charges sociales'); 
INSERT INTO MARACUJA.PLAN_COMPTABLE_REF ( REF_PCO_NUM,
REF_PCO_LIBELLE ) VALUES ( 
'6474', 'Oeuvres sociales'); 
COMMIT;
