SET define OFF
--
-- executer a partir de comptefi ou grhum ou dba
-----------------------------------------------------------
-- 
-- Modification de vues pour le fonctionnement du compte financier
-- 
-----------------------------------------------------------


--COMPTEFI.V_BUDNAT
-- (union avec >2006 en pointant sur bibasse)

--COMPTEFI.V_JEFY_MULTIEX_SITU

--EPN.EPN_BUDNAT


-----------------------------------------------------------

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_SITU_EXEBUDGDEP0
(EXE_ORDRE, GES_CODE, TCD_CODE, SSDST_CODE, SUM_DEP10, 
 SUM_DEP20, SUM_DEP30)
AS 
SELECT s.exe_ordre, org_ub, tcd_code, s.dst_code, SUM(dep_ttc), 0 , 0
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o
WHERE s.org_ordre = o.org_id AND tcd_code = 10 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tcd_code, s.dst_code
UNION ALL
SELECT s.exe_ordre, org_ub, tcd_code, s.dst_code, 0, SUM(dep_ttc) , 0
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o
WHERE s.org_ordre = o.org_id AND tcd_code = 20 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tcd_code, s.dst_code
UNION ALL
SELECT s.exe_ordre, org_ub, tcd_code, s.dst_code, 0, 0, SUM(dep_ttc)
FROM v_jefy_multiex_situ s, maracuja.v_organ_exer o
WHERE s.org_ordre = o.org_id AND tcd_code = 30 AND s.exe_ordre = o.EXE_ORDRE
GROUP BY s.exe_ordre, org_ub, tcd_code, s.dst_code; 

-----------------------------------------------------------

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_SITU_EXEBUDGREC0
(EXE_ORDRE, GES_CODE, TCD_SECT, SSDST_CODE, SUM_REC1, 
 SUM_REC2)
AS 
SELECT exe_ordre, org_ub, '1', r.dst_code, SUM(maracuja.en_nombre(tit_mont)), 0
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'R' AND pco_num LIKE '7%'
GROUP BY exe_ordre, org_ub, '1', r.dst_code
UNION ALL
SELECT exe_ordre, org_ub, '2', r.dst_code, 0, SUM(maracuja.en_nombre(tit_mont))
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'R' AND pco_num LIKE '1%'
GROUP BY exe_ordre, org_ub, '2', r.dst_code
UNION ALL
SELECT exe_ordre, org_ub, '1', r.dst_code, -SUM(maracuja.en_nombre(tit_mont)), 0
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'D' AND pco_num LIKE '7%'
GROUP BY exe_ordre, org_ub, '1', r.dst_code
UNION ALL
SELECT exe_ordre, org_ub, '2', r.dst_code, 0, -SUM(maracuja.en_nombre(tit_mont))
FROM maracuja.V_JEFY_MULTIEX_TITRE r , maracuja.v_organ_exer o
WHERE r.org_ordre = o.org_id AND r.EXE_EXERCICE = o.EXE_ORDRE
AND r.TIT_STAT = 'V' AND r.TIT_TYPE = 'D' AND pco_num LIKE '1%'
GROUP BY exe_ordre, org_ub, '2', r.dst_code;

-----------------------------------------------------------
-----------------------------------------------------------


CREATE OR REPLACE FORCE VIEW COMPTEFI.V_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), nature, 0, bdsa_date_validation, sum(bdsn_montant)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_param_editions bpe,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bs.TYSA_ID = 2
and bsn.pco_num like bpe.chapitre||'%' and budgetaire = 'O' and num_section = 1
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), nature, 0, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), nature, 0, bdsa_date_validation, sum(bdsn_montant)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_param_editions bpe,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bs.TYSA_ID = 2
and bsn.pco_num like bpe.chapitre||'%' and budgetaire = 'O' and num_section = 3
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), nature, 0, bdsa_date_validation
union all
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy.budnat b, jefy.exer e, jefy.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy.budnat b, jefy.exer e, jefy.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq)
FROM jefy.budnat b, jefy.organ o
WHERE b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY')
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq)
FROM jefy.budnat b, jefy.organ o
WHERE b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY')
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'RELI'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'RELI'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture; 





-----------------------------------------------------------
-----------------------------------------------------------

CREATE OR REPLACE FORCE VIEW EPN.EPN_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), nature, 0, bdsa_date_validation, sum(bdsn_montant)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_param_editions bpe,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bs.TYSA_ID = 2
and bsn.pco_num like bpe.chapitre||'%' and budgetaire = 'O' and num_section = 1
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), nature, 0, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), nature, 0, bdsa_date_validation, sum(bdsn_montant)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_param_editions bpe,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bs.TYSA_ID = 2
and bsn.pco_num like bpe.chapitre||'%' and budgetaire = 'O' and num_section = 3
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), nature, 0, bdsa_date_validation
union all
select 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim) 
from jefy.budnat b, jefy.exer e, jefy.organ o 
where e.EXR_TYPE = 'PRIM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
union all 
select 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim) 
from jefy.budnat b, jefy.exer e, jefy.organ o 
where e.EXR_TYPE = 'PRIM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
union all 
select 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_reliq) 
from jefy.budnat b, jefy.exer e, jefy.organ o 
where e.EXR_TYPE = 'RELI' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
union all 
select 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_reliq) 
from jefy.budnat b, jefy.exer e, jefy.organ o 
where e.EXR_TYPE = 'RELI' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
union all 
select 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE, sum (bdn_dbm) 
from jefy.budnat_dbm b, jefy.exer e , jefy.organ o 
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture 
union all 
select 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE, sum (bdn_dbm) 
from jefy.budnat_dbm b, jefy.exer e , jefy.organ o 
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture 
union all 
select 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim) 
from jefy05.budnat b, jefy05.exer e, jefy05.organ o 
where e.EXR_TYPE = 'PRIM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
union all 
select 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_prim) 
from jefy05.budnat b, jefy05.exer e, jefy05.organ o 
where e.EXR_TYPE = 'PRIM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
union all 
select 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_reliq) 
from jefy05.budnat b, jefy05.exer e, jefy05.organ o 
where e.EXR_TYPE = 'RELI' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
union all 
select 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, sum (bdn_reliq) 
from jefy05.budnat b, jefy05.exer e, jefy05.organ o 
where e.EXR_TYPE = 'RELI' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
and (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
union all 
select 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE, sum (bdn_dbm) 
from jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o 
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture 
union all 
select 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE, sum (bdn_dbm) 
from jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o 
where b.BDN_NUMERO = e.EXR_NUMERO and e.EXR_TYPE = 'DBM' 
and b.org_ordre = o.org_ordre 
and o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
group by org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture;



grant select on comptefi.v_dvlop_rec to maracuja;
grant select on comptefi.v_dvlop_dep to maracuja;

