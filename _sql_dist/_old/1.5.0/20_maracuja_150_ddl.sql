SET define OFF
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- 
-- modifications pour adaptation a jefy_admin
-- + nouveautes
-----------------------------------------------------------


-- drop des contraintes obsoletes (
----------------------------------
ALTER TABLE MARACUJA.COMPTABILITE_EXERCICE drop constraint COE_EXERCICE_FK;
ALTER TABLE MARACUJA.GESTION_EXERCICE drop CONSTRAINT FK_GESTIONEX_EXERCICE;
ALTER TABLE MARACUJA.RECETTE_RELANCE drop CONSTRAINT RECETTE_RELANCE_FK2;
ALTER TABLE MARACUJA.UTILISATEUR_PREFERENCE DROP CONSTRAINT FK_UP_PREF_ID;
ALTER TABLE MARACUJA.UTILISATEUR_PREFERENCE drop CONSTRAINT FK_UP_UTL_ORDRE;
ALTER TABLE MARACUJA.COMPTABILITE_EXERCICE drop CONSTRAINT COE_COMPTABILITE_FK;
ALTER TABLE MARACUJA.ECRITURE drop CONSTRAINT ECR_FK_COM_ORDRE;
ALTER TABLE MARACUJA.recette_info drop CONSTRAINT RECETTE_INFO_FK1;
ALTER TABLE MARACUJA.recette_relance drop CONSTRAINT RECETTE_RELANCE_FK1;
--ALTER TABLE MARACUJA.TITRE_BROUILLARD DROP CONSTRAINT FK_TB_REC_ID;
--ALTER TABLE MARACUJA.TITRE_DETAIL_ECRITURE DROP CONSTRAINT FK_TDE_REC_ID;






-- renommage des tables obsoletes
----------------------------------
ALTER TABLE MARACUJA.EXERCICE RENAME TO ZLD_EXERCICE;
ALTER TABLE MARACUJA.AUTORISATION RENAME TO ZLD_AUTORISATION;
ALTER TABLE MARACUJA.AUTORISATION_GESTION RENAME TO ZLD_AUTORISATION_GESTION;
ALTER TABLE MARACUJA.UTILISATEUR_GESTION RENAME TO ZLD_UTILISATEUR_GESTION;
ALTER TABLE MARACUJA.UTILISATEUR RENAME TO ZLD_UTILISATEUR;
ALTER TABLE MARACUJA.UTILISATEUR_PREFERENCE RENAME TO ZLD_UTILISATEUR_PREFERENCE;
ALTER TABLE MARACUJA.PREFERENCE RENAME TO ZLD_PREFERENCE;
ALTER TABLE MARACUJA.TYPE_CREDIT RENAME TO ZLD_TYPE_CREDIT;
ALTER TABLE MARACUJA.FONCTION RENAME TO ZLD_FONCTION;


-- sequences obsoletes
----------------------------------
drop sequence maracuja.type_bordereau_seq;
drop sequence maracuja.type_numerotation_seq;


----------------------------------------------------
--- Vues obsoletes
CREATE OR REPLACE FORCE VIEW MARACUJA.zld_V_RIB
(FOU_ORDRE, MOD_CODE, MOD_DOM, MOD_LIB, RIB_BIC, 
 RIB_CLE, RIB_CODBANC, RIB_GUICH, RIB_IBAN, RIB_DOMICIL, 
 RIB_NUM, RIB_ORDRE, RIB_TITCO, RIB_VALIDE)
AS 
sELECT r.FOU_ORDRE, 
	   m.MOD_CODE,
	   m.MOD_DOM, 
	   m.MOD_LIB, 
	   r.BIC , 
	   r.CLE_RIB, 
	   b.c_banque,
	   b.C_GUICHET, 
	   r.IBAN, 
	   b.DOMICILIATION, 
	   r.NO_COMPTE, 
	   r.RIB_ORDRE, 
	   r.RIB_TITCO, 
	   r.RIB_VALIDE
FROM GRHUM.RIBFOUR_ULR r, GRHUM.BANQUE b, jefy.MODPAY m
WHERE r.C_BANQUE = b.C_BANQUE
AND r.C_GUICHET = b.C_GUICHET
AND m.MOD_CODE = r.MOD_CODE;

-----------------


CREATE OR REPLACE FORCE VIEW MARACUJA.zld_V_ORGAN_EXER
(EXE_ORDRE, ORG_ORDRE, ORG_UNIT, ORG_COMP, ORG_LBUD, 
 ORG_UC, ORG_LIB, ORG_NIV, ORG_STAT, ORG_DATE, 
 ORG_TVA, SCT_CODE, ORG_RAT, DST_CODE, ORG_LUCRATIVITE, 
 ORG_TYPE_LIGNE, ORG_TYPE_FLECHAGE, ORG_OUVERTURE_CREDITS, ORG_OBSERVATION, OTC_CODE, 
 ORG_DELEGATION)
AS 
select 2006,ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
from jefy.organ
union all
select 2005,ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
from jefy05.organ;

-----------------

CREATE OR REPLACE FORCE VIEW MARACUJA.ZLD_V_TAUX
(TAU_CODE, TAU_TAUX)
AS 
SELECT TAU_CODE, TAU_TAUX 
	FROM JEFY.TAUX;
	
drop view maracuja.v_taux;

-----------------
--
drop view MARACUJA.V_JEFY_MULTIEX_BORDERO;

CREATE OR REPLACE FORCE VIEW MARACUJA.ZLD_V_JEFY_MULTIEX_BORDERO
(EXE_EXERCICE, BOR_ORDRE, BOR_DATE, BOR_STAT, BOR_VISA, 
 BOR_QUOI, BOR_NUM, GES_CODE, DSK_NUMERO, BOR_COMPTABLE)
AS 
SELECT 2006 AS exe_exercice, f.* FROM jefy.bordero f
WHERE 2006=(SELECT param_value FROM jefy.parametres WHERE param_key='EXERCICE')
UNION ALL
 SELECT 2005 AS exe_exercice, f.* FROM jefy05.bordero f;

-----------------



-----------------------------------------
-- modifs tables
-----------------------------------------

ALTER TABLE MARACUJA.TITRE_BROUILLARD ADD REC_ID NUMBER;
ALTER TABLE MARACUJA.TITRE_DETAIL_ECRITURE ADD REC_ID NUMBER;

ALTER TABLE MARACUJA.TITRE_BROUILLARD
 ADD CONSTRAINT FK_TB_REC_ID
 FOREIGN KEY (REC_ID)
 REFERENCES MARACUJA.RECETTE (REC_ID)
  DEFERRABLE
 INITIALLY DEFERRED ENABLE
 VALIDATE;

ALTER TABLE MARACUJA.TITRE_DETAIL_ECRITURE
 ADD CONSTRAINT FK_TDE_REC_ID
 FOREIGN KEY (REC_ID)
 REFERENCES MARACUJA.RECETTE (REC_ID)  DEFERRABLE
 INITIALLY DEFERRED ENABLE
 VALIDATE;


---------------------------------------


ALTER TABLE MARACUJA.TYPE_BORDEREAU
ADD (TBO_TYPE_CREATION VARCHAR2(20));

ALTER TABLE MARACUJA.TYPE_BORDEREAU  
ADD (tnu_ordre NUMBER);

COMMENT ON COLUMN MARACUJA.TYPE_BORDEREAU.tnu_ordre IS 'Reference au type de numerotation';

ALTER TABLE maracuja.TYPE_BORDEREAU ADD (CONSTRAINT fk_type_bordereau_tnu_ordre FOREIGN KEY (tnu_ordre) REFERENCES maracuja.TYPE_NUMEROTATION(tnu_ordre) DEFERRABLE INITIALLY DEFERRED );


---------------------------------------

-- ***************
-- amortissements
-- ***************

CREATE TABLE MARACUJA.PLAN_COMPTABLE_AMO (
  PCOA_ID NUMBER NOT NULL,
  PCOA_NUM VARCHAR2(20) NOT NULL,
  PCOA_LIBELLE VARCHAR2(200) NOT NULL,
  TYET_ID NUMBER DEFAULT 1 NOT NULL
);

ALTER TABLE MARACUJA.PLAN_COMPTABLE_AMO ADD (
  PRIMARY KEY (PCOA_ID));

ALTER TABLE MARACUJA.PLAN_COMPTABLE_AMO
 ADD CONSTRAINT UNQ_PCOA_PCOA_NUM
 UNIQUE (PCOA_NUM)  ENABLE VALIDATE;

---------------------------------------

CREATE SEQUENCE MARACUJA.PLAN_COMPTABLE_AMO_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;
  
---------------------------------------

CREATE TABLE MARACUJA.PLANCO_AMORTISSEMENT(
  PCA_ID  NUMBER                             NOT NULL,
  PCOA_ID  NUMBER                             			 NOT NULL,
  EXE_ORDRE NUMBER										 NOT NULL,
  PCO_NUM    VARCHAR2(20)                       NOT NULL
);

---------------------------------------

ALTER TABLE MARACUJA.PLANCO_AMORTISSEMENT ADD (
  PRIMARY KEY (PCA_ID));

---------------------------------------

CREATE SEQUENCE MARACUJA.PLANCO_AMORTISSEMENT_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

---------------------------------------

ALTER TABLE MARACUJA.PLAN_COMPTABLE_AMO ADD (
  CONSTRAINT FK_PCOA_TYET_ID FOREIGN KEY (TYET_ID) 
    REFERENCES JEFY_ADMIN.TYPE_ETAT (TYET_ID));
	
ALTER TABLE MARACUJA.PLANCO_AMORTISSEMENT ADD (
  CONSTRAINT FK_PCA_pcoa_ID FOREIGN KEY (pcoa_ID) 
    REFERENCES maracuja.PLAN_COMPTABLE_AMO (pcoa_ID)
	DEFERRABLE INITIALLY DEFERRED ) ;	
	
ALTER TABLE MARACUJA.PLANCO_AMORTISSEMENT ADD (
  CONSTRAINT FK_pca_PCo_num FOREIGN KEY (pco_num) 
    REFERENCES maracuja.PLAN_COMPTABLE (pco_num)
	);	
	
ALTER TABLE MARACUJA.PLANCO_AMORTISSEMENT ADD (
  CONSTRAINT FK_planco_amortissement_exe FOREIGN KEY (exe_ordre) 
    REFERENCES JEFY_ADMIN.EXERCICE (exe_ordre)
	);	

--------

ALTER TABLE MARACUJA.ECHEANCIER MODIFY (ORG_ID NOT NULL);

--------

create table maracuja.log_recup_planco_amort (logs varchar(1000));

--------

-- Plan comptable de reference
CREATE TABLE MARACUJA.PLAN_COMPTABLE_REF (
  REF_PCO_NUM VARCHAR2(30),
  REF_PCO_LIBELLE VARCHAR2(500),
  CONSTRAINT PK_PLAN_COMPTABLE_REF PRIMARY KEY (REF_PCO_NUM )
);



--------

CREATE OR REPLACE TRIGGER MARACUJA.LOG_DELETE_MODE_PAIEMENT
 BEFORE DELETE ON MARACUJA.MODE_PAIEMENT
FOR EACH ROW
BEGIN
RAISE_APPLICATION_ERROR ( -20015,'Interdiction de supprimer un mode de paiement !');

END LOG_DELETE_MODE_PAIEMENT;
/

CREATE OR REPLACE TRIGGER MARACUJA.LOG_DELETE_MODE_RECOUVREMENT
 BEFORE DELETE ON MARACUJA.MODE_RECOUVREMENT
FOR EACH ROW
BEGIN
RAISE_APPLICATION_ERROR ( -20015,'Interdiction de supprimer un mode de recouvrement !');

END LOG_DELETE_MODE_RECOUVREMENT;
/

 
------------------------------------
------------------------------------

ALTER TABLE maracuja.MANDAT 
ADD (
man_attente_paiement  NUMBER DEFAULT 4 NOT NULL,
man_attente_date DATE NULL,
man_attente_objet VARCHAR2(500) NULL,
utl_ordre_attente NUMBER NULL	
);

COMMENT ON COLUMN MARACUJA.MANDAT.MAN_ATTENTE_PAIEMENT IS 'Indique si le paiement est bloque. (4:non bloque, 3: bloque). Ref jefy_admin.type_etat.tyet_id ';
COMMENT ON COLUMN MARACUJA.MANDAT.MAN_ATTENTE_DATE IS 'Date de blocage du paiement';
COMMENT ON COLUMN MARACUJA.MANDAT.MAN_ATTENTE_OBJET IS 'Objet du blocage du paiement';
COMMENT ON COLUMN MARACUJA.MANDAT.UTL_ORDRE_ATTENTE IS 'Utilisateur qui a bloque le paiement';

ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT FK_MANDAT_man_attente_paiement FOREIGN KEY (man_attente_paiement) REFERENCES jefy_admin.type_ETAT(tyet_id)); 







------------------------------------------
------------------------------------------
-- nouvelles vues
-----------------------------------------
CREATE OR REPLACE FORCE VIEW MARACUJA.CFI_SOLDE_6_7
(PCO_NUM, EXE_ORDRE, GES_CODE, CREDIT, DEBIT, 
 SOLDE_CREDITEUR)
AS 
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit,
sum(ecd_credit)-sum(ecd_debit) SOLDE_CREDITEUR
from ecriture_detail ed, ecriture e,gestion g
where ed.ecr_ordre=e.ecr_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '7%'
group by ed.pco_num,e.exe_ordre,ed.ges_code
union all
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit,
sum(ecd_credit)-sum(ecd_debit) SOLDE_CREDITEUR
from ecriture_detail ed, ecriture e,gestion g
where ed.ecr_ordre=e.ecr_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '6%'
group by ed.pco_num,e.exe_ordre,ed.ges_code
union all
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit,
sum(ecd_credit)-sum(ecd_debit) SOLDE_CREDITEUR
from ecriture_detail ed, ecriture e,gestion g
where ed.ecr_ordre=e.ecr_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '187%'
group by ed.pco_num,e.exe_ordre,ed.ges_code
union all
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit,
sum(ecd_credit)-sum(ecd_debit) SOLDE_CREDITEUR
from ecriture_detail ed, ecriture e,gestion g
where ed.ecr_ordre=e.ecr_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '186%'
group by ed.pco_num,e.exe_ordre,ed.ges_code;


CREATE OR REPLACE FORCE VIEW MARACUJA.EXERCICE
(EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE, EXE_ORDRE, EXE_OUVERTURE, 
 EXE_STAT, EXE_TYPE, EXE_STAT_ENG, EXE_STAT_FAC)
AS 
SELECT
EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE,
   EXE_ORDRE, EXE_OUVERTURE, EXE_STAT,
   EXE_TYPE, EXE_STAT_ENG, EXE_STAT_FAC
FROM JEFY_ADMIN.EXERCICE;


CREATE OR REPLACE FORCE VIEW MARACUJA.FONCTION
(FON_ORDRE, FON_ID_INTERNE, FON_CATEGORIE, FON_DESCRIPTION, FON_LIBELLE, 
 FON_SPEC_GESTION, FON_SPEC_ORGAN, FON_SPEC_EXERCICE, TYAP_ID)
AS 
SELECT
FON_ORDRE, FON_ID_INTERNE, FON_CATEGORIE,
   FON_DESCRIPTION, FON_LIBELLE, FON_SPEC_GESTION,
   FON_SPEC_ORGAN, FON_SPEC_EXERCICE, TYAP_ID
FROM JEFY_ADMIN.FONCTION
WHERE tyap_id=4 AND fon_id_interne <> 'ADUT';


CREATE OR REPLACE FORCE VIEW MARACUJA.PREFERENCE
(PREF_ID, PREF_KEY, PREF_DEFAULT_VALUE, PREF_DESCRIPTION, PREF_PERSONNALISABLE)
AS 
SELECT
PREF_ID, PREF_KEY, PREF_DEFAULT_VALUE,
   PREF_DESCRIPTION, PREF_PERSONNALISABLE
FROM JEFY_ADMIN.PREFERENCE
WHERE tyap_id=4;


CREATE OR REPLACE FORCE VIEW MARACUJA.TYPE_CREDIT
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET, TYET_ID)
AS 
SELECT
EXE_ORDRE, TCD_ORDRE, TCD_CODE,
   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,
   TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET,
   TYET_ID
FROM JEFY_ADMIN.TYPE_CREDIT
WHERE TYET_ID=1;


CREATE OR REPLACE FORCE VIEW MARACUJA.UTILISATEUR
(UTL_ORDRE, NO_INDIVIDU, PERS_ID, UTL_OUVERTURE, UTL_FERMETURE, 
 TYET_ID)
AS 
SELECT utl_ordre, no_individu, pers_id, utl_ouverture, utl_fermeture, tyet_id
  FROM jefy_admin.UTILISATEUR;


CREATE OR REPLACE FORCE VIEW MARACUJA.UTILISATEUR_FONCT
(UF_ORDRE, UTL_ORDRE, FON_ORDRE)
AS 
SELECT
uf.UF_ORDRE, uf.UTL_ORDRE, uf.FON_ORDRE
FROM JEFY_ADMIN.UTILISATEUR_FONCT uf, jefy_admin.FONCTION f
WHERE f.fon_ordre=uf.fon_ordre AND f.tyap_id=4;


CREATE OR REPLACE FORCE VIEW MARACUJA.UTILISATEUR_FONCT_EXERCICE
(UFE_ID, UF_ORDRE, EXE_ORDRE)
AS 
SELECT
UFE_ID, ufe.UF_ORDRE, EXE_ORDRE
FROM JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE ufe,
utilisateur_fonct uf
WHERE uf.uf_ordre=ufe.uf_ordre;


CREATE OR REPLACE FORCE VIEW MARACUJA.UTILISATEUR_FONCT_GESTION
(UFG_ID, UF_ORDRE, GES_CODE)
AS 
SELECT
UFG_ID, ufg.UF_ORDRE, GES_CODE
FROM JEFY_ADMIN.UTILISATEUR_FONCT_GESTION ufg,
utilisateur_fonct uf
WHERE uf.uf_ordre=ufg.uf_ordre;


CREATE OR REPLACE FORCE VIEW MARACUJA.UTILISATEUR_PREFERENCE
(UP_ID, UTL_ORDRE, PREF_ID, UP_VALUE)
AS 
SELECT
UP_ID, UTL_ORDRE, PREF_ID,
   UP_VALUE
FROM JEFY_ADMIN.UTILISATEUR_PREFERENCE;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_AGENT_JEFY
(AGT_ORDRE, AGT_NOM, AGT_PRENOM, AGT_LOGIN, AGT_LOCAL, 
 AGT_UID, ADR_ORDRE, AGT_PRE_CDE, AGT_CDE_ENG, AGT_PRE_FACT, 
 AGT_FACT_LIQ, AGT_MANDAT, AGT_COMPTA, AGT_BUDGET, AGT_FOURNIS, 
 AGT_TOUT, AGT_RESP, AGT_MARCHE, AGT_MISSION, AGT_TOUT_ORGAN, 
 AGT_TITRE, AGT_ADM, NO_INDIVIDU, AGT_MAIL, AGT_TAXE, 
 AGT_CONTINU, AGT_UNIQUE)
AS 
select "AGT_ORDRE","AGT_NOM","AGT_PRENOM","AGT_LOGIN","AGT_LOCAL","AGT_UID","ADR_ORDRE","AGT_PRE_CDE","AGT_CDE_ENG","AGT_PRE_FACT","AGT_FACT_LIQ","AGT_MANDAT","AGT_COMPTA","AGT_BUDGET","AGT_FOURNIS","AGT_TOUT","AGT_RESP","AGT_MARCHE","AGT_MISSION","AGT_TOUT_ORGAN","AGT_TITRE","AGT_ADM","NO_INDIVIDU","AGT_MAIL","AGT_TAXE","AGT_CONTINU","AGT_UNIQUE" from jefy.agent;

/*
CREATE OR REPLACE FORCE VIEW MARACUJA.V_ATTRIBUTION
(ATT_DATE, ATT_ORDRE, ATT_SUPPR, ATT_VALIDE, FOU_ORDRE, 
 LOT_ORDRE, ATT_DEBUT, ATT_FIN, ATT_TYPE_CONTROLE, ATT_ACCEPTEE, 
 UTL_ORDRE, ATT_HT, TIT_ORDRE, ATT_INDEX)
AS 
SELECT ATT_DATE,ATT_ORDRE,ATT_SUPPR,ATT_VALIDE,FOU_ORDRE,LOT_ORDRE,ATT_DEBUT,ATT_FIN,ATT_TYPE_CONTROLE,ATT_ACCEPTEE,utl_ordre,ATT_HT,TIT_ORDRE,ATT_INDEX
FROM jefy_marches.ATTRIBUTION;
*/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_BE_REPRISE_CONSOLIDEE
(COM_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, 
 COMPTE_BE, COMPTE_BE_LIBELLE, DEBITS, CREDITS)
AS 
SELECT   e.com_ordre, e.exe_ordre, c.ges_code, p.pco_num,p.pco_libelle, decode(p1.pco_num,null,p.pco_num,p1.pco_num) compte_be, decode(p1.pco_num,null,p.pco_libelle,p1.pco_libelle) compte_be_libelle,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit/ DECODE (ecd_debit, 0, 1, ecd_debit)) AS debits,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit) ) AS credits
    FROM ecriture_detail ecd,
         ecriture e,
         comptabilite c,
		 plan_comptable p,
		 plan_comptable p1,
         (SELECT ecd_ordre
            FROM ecriture_detail ecd, ecriture e
           WHERE ecd.ecr_ordre = e.ecr_ordre AND SUBSTR (ecr_etat, 1, 1) = 'V'
          MINUS
          SELECT ecd_ordre
            FROM ecriture_detail_be_log) x
   WHERE ecd.ecd_ordre = x.ecd_ordre
     AND ecd.ecr_ordre = e.ecr_ordre
     AND e.com_ordre = c.com_ordre
	 and ecd.pco_num = p.pco_num
	 and p.pco_compte_be=p1.pco_num(+)
GROUP BY e.com_ordre, e.exe_ordre, c.ges_code, p.pco_num, p.pco_libelle,p1.pco_num, p1.pco_libelle
  HAVING SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit / DECODE (ecd_debit, 0, 1, ecd_debit)) <> 0
      OR SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit)) <> 0
ORDER BY com_ordre, exe_ordre, ges_code, pco_num;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_BE_REPRISE_GESTION
(COM_ORDRE, EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, 
 COMPTE_BE, COMPTE_BE_LIBELLE, DEBITS, CREDITS)
AS 
SELECT   e.com_ordre, e.exe_ordre, ecd.ges_code, p.pco_num,p.pco_libelle, decode(p1.pco_num,null,p.pco_num,p1.pco_num) compte_be, decode(p1.pco_num,null,p.pco_libelle,p1.pco_libelle) compte_be_libelle,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit/ DECODE (ecd_debit, 0, 1, ecd_debit)) AS debits,
         SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit) ) AS credits
    FROM ecriture_detail ecd,
         ecriture e,
         comptabilite c,
		 plan_comptable p,
		 plan_comptable p1,
         (SELECT ecd_ordre
            FROM ecriture_detail ecd, ecriture e
           WHERE ecd.ecr_ordre = e.ecr_ordre AND SUBSTR (ecr_etat, 1, 1) = 'V'
          MINUS
          SELECT ecd_ordre
            FROM ecriture_detail_be_log) x
   WHERE ecd.ecd_ordre = x.ecd_ordre
     AND ecd.ecr_ordre = e.ecr_ordre
     AND e.com_ordre = c.com_ordre
	 and ecd.pco_num=p.pco_num
	 and p.pco_compte_be=p1.pco_num(+)
GROUP BY e.com_ordre, e.exe_ordre, ecd.ges_code, p.pco_num, p.pco_libelle,p1.pco_num, p1.pco_libelle
  HAVING SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_debit / DECODE (ecd_debit, 0, 1, ecd_debit)) <> 0
      OR SUM (  ecd_reste_emarger * SIGN (ecd_montant) * ecd_credit / DECODE (ecd_credit, 0, 1, ecd_credit)) <> 0
ORDER BY com_ordre, exe_ordre, ges_code, pco_num;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_CONVENTION_LIMITATIVE
(CON_ORDRE, EXE_ORDRE, ORG_ID, MONTANT)
AS 
select CON_ORDRE,EXE_ORDRE,ORG_ID,sum(CL_MONTANT) montant
from convention.CONVENTION_LIMITATIVE
group by CON_ORDRE,EXE_ORDRE,ORG_ID;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECD_EMARGE_ORDO
(ECD_ORDRE, EXE_ORDRE, GES_CODE, ECR_NUMERO, PCO_NUM, 
 ECD_LIBELLE, ECD_SENS, ECD_CREDIT, ECD_DEBIT, ECD_MONTANT, 
 ECD_RESTE_EMARGER, MOD_ORDRE, MOD_CODE)
AS 
SELECT ecd_ordre, e.EXE_ORDRE, GES_CODE, ecr_numero,
PCO_NUM , ECD_LIBELLE, ECD_SENS, ECD_CREDIT, ECD_DEBIT,
ECD_MONTANT, ECD_RESTE_EMARGER, MOD_ORDRE, MOD_CODE
FROM ECRITURE_DETAIL ecd, ECRITURE e, MODE_PAIEMENT mp
WHERE ecd.ecr_ordre=e.ecr_ordre
AND ecd.ecd_reste_emarger<>0
AND ecd.ecd_sens='D'
AND mp.PCO_NUM_VISA=ecd.pco_num
AND mp.exe_ordre=e.exe_ordre
AND mp.MOD_EMA_AUTO='1'
AND SUBSTR(e.ECR_ETAT,1,1)='V'
AND e.ecr_numero>0;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECD_RESTEEMARGER
(ECD_ORDRE, EXE_ORDRE, GES_CODE, ECR_NUMERO, PCO_NUM, 
 ECD_LIBELLE, ECD_SENS, ECD_CREDIT, ECD_DEBIT, ECD_MONTANT, 
 ECD_RESTE_EMARGER)
AS 
select ecd_ordre, e.EXE_ORDRE, GES_CODE, ecr_numero, PCO_NUM , ECD_LIBELLE, ECD_SENS, ECD_CREDIT, ECD_DEBIT, ECD_MONTANT, ECD_RESTE_EMARGER
from ecriture_detail ecd, ecriture e

where ecd.ecr_ordre=e.ecr_ordre

and ecd.ecd_reste_emarger<>0

and e.ecr_numero>0

and e.ECR_ETAT='VALIDE';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECRITURE_INFOS
(ECD_ORDRE, ECR_SACD)
AS 
SELECT d.ecd_ordre , 'O' FROM ECRITURE_DETAIL d, GESTION g, gestion_exercice ge WHERE d.ges_code=g.ges_code and g.ges_code=ge.ges_code and ge.exe_ordre=d.EXE_ORDRE AND ge.PCO_NUM_185 IS NOT NULL
UNION ALL
SELECT d.ecd_ordre , 'N' FROM ECRITURE_DETAIL d, GESTION g, gestion_exercice ge  WHERE d.ges_code=g.ges_code and g.ges_code=ge.ges_code and ge.exe_ordre=d.EXE_ORDRE AND ge.PCO_NUM_185 IS NULL;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ECRITURE_MONTANT
(ECR_ORDRE, ECR_MONTANT)
AS 
select ecr_ordre, sum(ecd_debit) ecr_montant from ecriture_detail group by ecr_ordre;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_FOURNISSEUR
(FOU_ORDRE, PERS_ID, ADR_ORDRE, FOU_CODE, FOU_DATE, 
 FOU_MARCHE, FOU_VALIDE, AGT_ORDRE, FOU_TYPE, D_CREATION, 
 D_MODIFICATION, CPT_ORDRE, FOU_ETRANGER, ADR_ADRESSE1, ADR_ADRESSE2, 
 ADR_CP, ADR_VILLE, ADR_NOM, ADR_PRENOM, ADR_CIVILITE, 
 CP_ETRANGER, LC_PAYS)
AS 
SELECT f.*, a.adr_adresse1, a.adr_adresse2, a.code_postal AS adr_ADR_CP, a.ville AS adr_ville,
       p.pers_libelle, p.pers_lc, p.pers_type, cp_etranger, lc_pays
  FROM grhum.fournis_ulr f, grhum.adresse a, grhum.personne p, grhum.pays pa
 WHERE f.adr_ordre = a.adr_ordre
   AND f.pers_id = p.pers_id
   AND pa.c_pays = a.c_pays;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_INDIVIDU_ULR
(NO_INDIVIDU, PERS_ID, NOM_PATRONYMIQUE, PRENOM, C_CIVILITE, 
 NOM_USUEL, PRENOM2, D_NAISSANCE, VILLE_DE_NAISSANCE, C_DEPT_NAISSANCE, 
 C_PAYS_NAISSANCE, C_PAYS_NATIONALITE, D_NATURALISATION, D_DECES, IND_C_SIT_MILITAIRE, 
 IND_C_SITUATION_FAMILLE, IND_NO_INSEE, IND_CLE_INSEE, IND_QUALITE, IND_PHOTO, 
 IND_ACTIVITE, IND_ORIGINE, D_CREATION, D_MODIFICATION, TEM_SS_DIPLOME, 
 TEM_VALIDE, IND_AGENDA)
AS 
select "NO_INDIVIDU","PERS_ID","NOM_PATRONYMIQUE","PRENOM","C_CIVILITE","NOM_USUEL","PRENOM2","D_NAISSANCE","VILLE_DE_NAISSANCE","C_DEPT_NAISSANCE","C_PAYS_NAISSANCE","C_PAYS_NATIONALITE","D_NATURALISATION","D_DECES","IND_C_SIT_MILITAIRE","IND_C_SITUATION_FAMILLE","IND_NO_INSEE","IND_CLE_INSEE","IND_QUALITE","IND_PHOTO","IND_ACTIVITE","IND_ORIGINE","D_CREATION","D_MODIFICATION","TEM_SS_DIPLOME","TEM_VALIDE","IND_AGENDA" from grhum.individu_ulr;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_DESTIN
(EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV)
AS 
select 2006 as exe_exercice, d.* from jefy.destin d
   where 2006=(select param_value from jefy.parametres where param_key='EXERCICE')
union all
  select 2005 as exe_exercice, d.* from jefy05.destin d
union all
  select 2004 as exe_exercice, d.* from jefy04.destin d;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_DESTIN_REC
(EXE_EXERCICE, DST_CODE, DST_LIB, DST_DLIB, DST_ABREGE, 
 DST_TYPE, DST_NIV)
AS 
select 2006 as exe_exercice, d.* from jefy.destin_rec d
	   where 2006=(select param_value from jefy.parametres where param_key='EXERCICE')
union all
	  select 2005 as exe_exercice, d.* from jefy05.destin_rec d
union all
	  select 2004 as exe_exercice, d.* from jefy04.destin_rec d;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_FACTURE
(EXE_EXERCICE, DEP_ORDRE, DEP_FACT, DEP_DATE, DEP_LIB, 
 DEP_TTC, DEP_MONT, DEP_PIECE, DEP_SECT, CAN_CODE, 
 RIB_ORDRE, MOD_CODE, PCO_NUM, DST_CODE, AGT_ORDRE, 
 MAN_ORDRE, CDE_ORDRE, DEP_MONNAIE, DEP_VIREMENT, DEP_INVENTAIRE, 
 DEP_HT_SAISIE, DEP_CREATION, DEP_PRORATA, CM_ORDRE, CM_TYPE, 
 CM_ACHAT, DEP_DATECOMPOSANTE, DEP_DATEDAF, DEP_LUCRATIVITE, DEP_RGP, 
 DEP_BUDNAT)
AS 
select 2006 as exe_exercice, f.* from jefy.facture f
	   where 2006=(select param_value from jefy.parametres where param_key='EXERCICE')
union all
	  select 2005 as exe_exercice, f.* from jefy05.FACTURE f
union all
	  select 2004 as exe_exercice, f.* from jefy04.FACTURE f;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_MANDAT
(EXE_EXERCICE, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, 
 MAN_STAT, FOU_ORDRE, PCO_NUM, BOR_ORDRE, RIB_ORDRE, 
 JOU_ORDRE, MAN_NUM, MAN_MONNAIE, ORG_ORDRE, CONV_ORDRE, 
 PRES_ORDRE)
AS 
select 2006 as exe_exercice, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, MAN_STAT,
 FOU_ORDRE, PCO_NUM, BOR_ORDRE,
RIB_ORDRE, JOU_ORDRE, MAN_NUM, MAN_MONNAIE, ORG_ORDRE, CONV_ORDRE, PRES_ORDRE
from jefy.mandat f
where 2006=(select param_value from jefy.parametres where param_key='EXERCICE')
union all
select 2005 as exe_exercice, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, MAN_STAT,
	FOU_ORDRE,PCO_NUM,
	BOR_ORDRE, RIB_ORDRE, JOU_ORDRE, MAN_NUM, MAN_MONNAIE,
	ORG_ORDRE, -1 as CONV_ORDRE, -1 as PRES_ORDRE

from jefy05.mandat f
union all
select 2004 as exe_exercice, MAN_ORDRE, MAN_MONT, MAN_TVA, MAN_PIECE, MAN_STAT,
	FOU_ORDRE,PCO_NUM,
	BOR_ORDRE, RIB_ORDRE, JOU_ORDRE, MAN_NUM, MAN_MONNAIE,
	ORG_ORDRE, -1 as CONV_ORDRE, -1 as PRES_ORDRE
from jefy04.mandat f;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_JEFY_MULTIEX_TITRE
(EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE)
AS 
select 2006 as exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE,
TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE,
TIT_VIREMENT, MOD_CODE,
DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
from jefy.titre f
where 2006=(select param_value from jefy.parametres where param_key='EXERCICE')
union all
select 2005 as exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 as conv_ordre, '' as can_code, -1 as pres_ordre
from jefy05.titre f
union all
select 2004 as exe_exercice, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, TIT_LIB, TIT_TYPE,
TIT_PIECE, JOU_ORDRE, FOU_ORDRE, PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, TIT_NUM,
AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT,
MOD_CODE, DST_CODE, TIT_REF, -1 as conv_ordre, '' as can_code, -1 as pres_ordre
from jefy04.titre f;

/*
CREATE OR REPLACE FORCE VIEW MARACUJA.V_LOT
(LOT_CATALOGUE, LOT_DEBUT, LOT_FIN, LOT_HT, LOT_INDEX, 
 LOT_LIBELLE, LOT_MONNAIE, LOT_ORDRE, LOT_SUPPR, LOT_VALIDE, 
 MAR_ORDRE, LOT_SOUSTRAITANTS, LOT_COTITULAIRES)
AS 
SELECT LOT_CATALOGUE,LOT_DEBUT,LOT_FIN,LOT_HT,LOT_INDEX,LOT_LIBELLE,LOT_MONNAIE,LOT_ORDRE,LOT_SUPPR,LOT_VALIDE,MAR_ORDRE,LOT_SOUSTRAITANTS,LOT_COTITULAIRES
FROM jefy_marches.lot;
*/

CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_ECRITURE
(EXE_EXERCICE, DATE_VISA, DATE_SAISIE, MAN_ID, GES_CODE, 
 PCO_NUM, ECD_MONTANT, ECR_ORDRE)
AS 
SELECT DISTINCT ex.exe_exercice, b.BOR_DATE_VISA, e.ECR_DATE_SAISIE , m.man_id,  m.ges_code,   ed.pco_num, ed.ecd_montant, ed.ecr_ordre
FROM MANDAT m, BORDEREAU b, TYPE_BORDEREAU t, MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ed, ECRITURE e, EXERCICE ex
WHERE m.bor_id=b.bor_id
AND b.tbo_ordre=t.tbo_ordre
AND m.man_id=mde.man_id
and m.pco_num = ed.PCO_NUM
AND mde.ecd_ordre=ed.ecd_ordre
AND ed.ecr_ordre=e.ecr_ordre
AND b.exe_ordre=ex.exe_ordre
AND (m.MAN_ETAT = 'VISE' or m.MAN_ETAT = 'PAYE')
AND e.ECR_ETAT='VALIDE';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, MDE_ORIGINE, ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num,
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, (sign(ecd_debit))*m.man_tva,
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant
 FROM mandat m,
      mandat_detail_ecriture mde,
       ecriture_detail ed,
      v_ecriture_infos ei,
      ecriture e,
      bordereau b,
      type_bordereau tb
WHERE m.man_id = mde.man_id
       and m.BOR_ID=b.bor_id
      and b.tbo_ordre=tb.tbo_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND mde_origine in ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  and ecd_debit<>0
  and abs (ecd_montant)=abs(man_ht)
  AND ED.pco_num NOT LIKE '185%'
  and (tb.TBO_TYPE='BTME' or tb.TBO_TYPE='BTMS')
union all
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, substr(ed.pco_num,3,20) as pco_num,
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, (sign(ecd_debit))*m.man_tva,
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant
 FROM mandat m,
      mandat_detail_ecriture mde,
       ecriture_detail ed,
      v_ecriture_infos ei,
      ecriture e,
      bordereau b,
      type_bordereau tb
WHERE m.man_id = mde.man_id
       and m.BOR_ID=b.bor_id
      and b.tbo_ordre=tb.tbo_ordre
  AND mde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND mde_origine in ('VISA', 'REIMPUTATION')
  AND man_etat IN ('VISE', 'PAYE')
  and ecd_debit<>0
  and abs (ecd_montant)=abs(man_ht)
  AND ED.pco_num NOT LIKE '185%'
  and tb.TBO_TYPE='BTPI';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MARCHE
(MAR_SUPPR, EXE_ORDRE, MAR_CLAUSES, MAR_DEBUT, MAR_FIN, 
 MAR_INDEX, MAR_LIBELLE, MAR_ORDRE, MAR_PASSATION, MAR_VALIDE)
AS 
select "MAR_SUPPR","EXE_ORDRE","MAR_CLAUSES","MAR_DEBUT","MAR_FIN","MAR_INDEX","MAR_LIBELLE","MAR_ORDRE","MAR_PASSATION","MAR_VALIDE" 
from marches.marche;



CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN
(ORG_ORDRE, ORG_UNIT, ORG_COMP, ORG_LBUD, ORG_UC, 
 ORG_LIB, ORG_NIV, ORG_STAT, ORG_DATE, ORG_TVA, 
 SCT_CODE, ORG_RAT, DST_CODE, ORG_LUCRATIVITE, ORG_TYPE_LIGNE, 
 ORG_TYPE_FLECHAGE, ORG_OUVERTURE_CREDITS, ORG_OBSERVATION, OTC_CODE, ORG_DELEGATION)
AS 
select ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
from jefy.organ
union all
select ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_STAT,ORG_DATE,ORG_TVA,SCT_CODE,ORG_RAT,DST_CODE,
  ORG_LUCRATIVITE,ORG_TYPE_LIGNE,ORG_TYPE_FLECHAGE,ORG_OUVERTURE_CREDITS,ORG_OBSERVATION,OTC_CODE,ORG_DELEGATION
from jefy05.organ where org_ordre in (select org_ordre from jefy05.organ minus select org_ordre from jefy.organ);


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN_EXER
(EXE_ORDRE, ORG_ID, ORG_ETAB, ORG_UB, ORG_CR, 
 ORG_SOUSCR, ORG_LIB, ORG_NIV, ORG_PERE, ORG_LUCRATIVITE, 
 TYOR_ID)
AS 
SELECT EXE_ORDRE, ORG_ID, org_etab, org_ub, org_cr, org_souscr, org_lib, ORG_NIV, ORG_PERE, ORG_LUCRATIVITE, TYOR_ID
FROM jefy_admin.v_organ
WHERE exe_ordre>2006
UNION ALL
SELECT 2006,	ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_RAT,  ORG_LUCRATIVITE, 1
FROM jefy.organ
UNION ALL
SELECT 2005,ORG_ORDRE, ORG_UNIT,ORG_COMP,ORG_LBUD,ORG_UC,ORG_LIB,ORG_NIV,ORG_RAT,  ORG_LUCRATIVITE, 1
FROM jefy05.organ;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_ORGAN2
(ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, 
 ORG_UB, ORG_CR, ORG_SOUSCR, ORG_LIB, ORG_LUCRATIVITE, 
 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID)
AS 
SELECT ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_SOUSCR, ORG_LIB, ORG_LUCRATIVITE, ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID
 FROM jefy_admin.organ
UNION
SELECT ORG_ORDRE,org_niv, org_rat,NULL,ORG_UNIT AS org_etab, ORG_COMP AS org_ub, ORG_LBUD AS org_cr, ORG_UC AS org_sous_cr, ORG_LIB, ORG_LUCRATIVITE, TO_DATE('01/01/2005','dd/mm/yyyy'),TO_DATE('31/12/2006','dd/mm/yyyy'),NULL,NULL,1 
FROM v_organ 
WHERE org_ordre IN (SELECT org_ordre FROM v_organ MINUS SELECT org_id FROM jefy_admin.organ);


CREATE OR REPLACE FORCE VIEW MARACUJA.V_PLANCO_CHAPITRE
(PCO_NUM, PCO_CLASSE, PCO_CHAPITRE)
AS 
SELECT pco_num, SUBSTR (pco_num, 1, 1) AS pco_classe,
       DECODE (SUBSTR (pco_num, 1, 1),
               1, SUBSTR (pco_num, 1, 3),
               2, SUBSTR (pco_num, 1, 3),
               SUBSTR (pco_num, 1, 2)
              ) AS pco_chapitre
  FROM plan_comptable;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_PLANCO_CREDIT_DEP
(PCC_ORDRE, TCD_ORDRE, PCO_NUM, PCC_ETAT)
AS 
SELECT pcc_ordre, tcd_ordre, pco_num, pcc_etat
  FROM maracuja.PLANCO_CREDIT
 WHERE pla_quoi = 'D';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_PLANCO_CREDIT_REC
(PCC_ORDRE, TCD_ORDRE, PCO_NUM, PCC_ETAT)
AS 
SELECT pcc_ordre, tcd_ordre, pco_num, pcc_etat
  FROM maracuja.PLANCO_CREDIT
 WHERE pla_quoi = 'R';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_RECETTE_REL_INFO
(REC_ID, REC_DATE_LIMITE_PAIEMENT, NB_RELANCES, DATE_PROCHAINE_RELANCE)
AS 
SELECT r.rec_id, r.rec_date_limite_paiement,
       NVL (nb_relances, 0) AS nb_relances,
       NVL (z.date_prochaine_relance,
            rec_date_limite_paiement
           ) AS date_prochaine_relance
  FROM recette r,
       titre t,
       (SELECT r2.rec_id, rer_date_creation, rer_delai_paiement,
               (rer_date_creation + NVL (rer_delai_paiement, 0)
               ) AS date_prochaine_relance,
               nb_relances
          FROM recette_relance r2,
               type_relance tr,
               (SELECT   rec_id, MAX (t.trl_niveau) AS trl_niveau
                    FROM recette_relance rr, type_relance t
                   WHERE t.trl_ordre = rr.trl_ordre
                     AND SUBSTR (rer_etat, 1, 1) = 'V'
                GROUP BY rec_id) x,
               (SELECT   rec_id, COUNT (*) AS nb_relances
                    FROM recette_relance rr
                   WHERE SUBSTR (rer_etat, 1, 1) = 'V'
                GROUP BY rec_id) y
         WHERE r2.rec_id = x.rec_id
           AND r2.rec_id = y.rec_id
           AND SUBSTR (r2.rer_etat, 1, 1) = 'V'
           AND r2.trl_ordre = tr.trl_ordre
           AND tr.trl_niveau = x.trl_niveau) z
 WHERE r.tit_id = t.tit_id AND t.tit_etat = 'VISE' AND r.rec_id = z.rec_id(+);


CREATE OR REPLACE FORCE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS 
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd,
         ECRITURE e
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
	 AND ecd.ecd_credit=0
	 AND ecd.ecd_debit<>0
     AND ecd.ecr_ordre = e.ecr_ordre	 
     AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
GROUP BY r.rec_id;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_RIB
(RIB_ORDRE, FOU_ORDRE, RIB_BIC, RIB_CLE, RIB_CODBANC, 
 RIB_GUICH, RIB_IBAN, RIB_DOMICIL, RIB_NUM, RIB_TITCO, 
 RIB_VALIDE)
AS 
SELECT r.rib_ordre, r.fou_ordre, r.bic, r.cle_rib, b.c_banque, b.c_guichet,
       r.iban, b.domiciliation, r.no_compte,  r.rib_titco,
       r.rib_valide
  FROM grhum.ribfour_ulr r, grhum.banque b
 WHERE r.c_banque = b.c_banque AND r.c_guichet = b.c_guichet;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_ECRITURE
(EXE_EXERCICE, DATE_VISA, DATE_SAISIE, TIT_ID, GES_CODE, 
 PCO_NUM, ECD_MONTANT, ECR_ORDRE)
AS 
SELECT DISTINCT ex.exe_exercice, b.BOR_DATE_VISA, e.ECR_DATE_SAISIE , t.tit_id,  t.ges_code,   ed.pco_num, ed.ecd_montant, ed.ecr_ordre
FROM TITRE t, BORDEREAU b, TYPE_BORDEREAU tb, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ed, ECRITURE e, EXERCICE ex
WHERE t.bor_id=b.bor_id
AND b.tbo_ordre = tb.tbo_ordre
AND t.tit_id=tde.tit_id
and t.pco_num = ed.pco_num
AND tde.ecd_ordre=ed.ecd_ordre
AND ed.ecr_ordre=e.ecr_ordre
AND b.exe_ordre=ex.exe_ordre
AND (t.tit_ETAT = 'VISE' or t.tit_ETAT = 'PAYE')
AND e.ECR_ETAT='VALIDE';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_PREST_INTERNE
(EXE_ORDRE, TIT_ORDRE, FT_ORDRE, DEP_ORDRE, MAN_ORDRE, 
 TIT_BORORDRE, MAN_BORORDRE)
AS 
select   2006, TIT_ORDRE,  FT_ORDRE,DEP_ORDRE,MAN_ORDRE, TIT_BORORDRE , MAN_BORORDRE from jefy.titre_prest_interne
union all
select   2005, TIT_ORDRE,  FT_ORDRE,DEP_ORDRE,MAN_ORDRE, TIT_BORORDRE , MAN_BORORDRE from jefy05.titre_prest_interne;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, ed.pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit,
      (sign(ecd_credit))*t.tit_tva, ecd_credit+((sign(ecd_credit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
      ei.ecr_sacd, tde_origine, ecd_montant
 FROM bordereau b,
      titre t,
      titre_detail_ecriture tde,
      ecriture_detail ed,
      v_ecriture_infos ei,
      ecriture e,
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id
  and t.tit_id = tde.tit_id
  AND tde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  and t.tit_ordre = jt.tit_ordre and jt.exe_exercice = ed.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tde_origine in ('VISA', 'REIMPUTATION')
  AND tit_etat IN ('VISE', 'PAYE')
  and ecd_credit<>0
  and tbo_ordre<>9 and tbo_ordre<>11
  and ed.pco_num not like '185%'
  and abs (ecd_montant)=abs(tit_ht)
union all
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, t.ges_code, substr(ed.pco_num,3,20) as pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_credit,
      (sign(ecd_credit))*t.tit_tva, ecd_credit+((sign(ecd_credit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
      ei.ecr_sacd, tde_origine, ecd_montant
 FROM bordereau b,
      titre t,
      titre_detail_ecriture tde,
      ecriture_detail ed,
      v_ecriture_infos ei,
      ecriture e,
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id
  and t.tit_id = tde.tit_id
  AND tde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  and t.tit_ordre = jt.tit_ordre and jt.exe_exercice = ed.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tde_origine in ('VISA', 'REIMPUTATION')
  AND tit_etat IN ('VISE', 'PAYE')
  and ecd_credit<>0
  and tbo_ordre=11
  and ed.pco_num not like '185%'
  and abs (ecd_montant)=abs(tit_ht)
union all
-- reductions
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date_saisie as ecr_date, t.ges_code, ed.pco_num, t.bor_id,
      b.tbo_ordre, t.tit_id, t.tit_numero, ed.ecd_libelle, ecd_DEBIT,
      (sign(ecd_debit))*t.tit_tva, ecd_debit+((sign(ecd_debit))*t.tit_tva),
      t.tit_etat, jt.fou_ordre, jt.tit_debiteur, jt.tit_interne, ed.ecr_ordre,
      ei.ecr_sacd, tde_origine, ecd_montant
 FROM bordereau b,
      titre t,
      titre_detail_ecriture tde,
      ecriture_detail ed,
      v_ecriture_infos ei,
      ecriture e,
      V_JEFY_MULTIEX_TITRE jt
WHERE t.bor_id = b.bor_id
  and t.tit_id = tde.tit_id
  AND tde.ecd_ordre = ed.ecd_ordre
  AND ed.ecd_ordre = ei.ecd_ordre
  AND ed.ecr_ordre = e.ecr_ordre
  and t.tit_ordre = jt.tit_ordre and jt.exe_exercice = ed.exe_ordre
  --AND r.pco_num_ancien = ed.pco_num
  AND tde_origine in ('VISA', 'REIMPUTATION')
  AND tit_etat IN ('VISE', 'PAYE')
  and ecd_debit<>0
  and tbo_ordre=9
  and ed.pco_num not like '185%'
  and abs (ecd_montant)=abs(tit_ht);


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_RESTE_RECOUVRER
(EXE_ORDRE, EXE_EXERCICE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 TIT_NUMERO, TIT_DATE_EMISSION, REC_DEBITEUR, TIT_ID, FOU_CODE, 
 ADR_NOM, ADR_PRENOM, ADR_CP, ADR_ADRESSE1, ADR_ADRESSE2, 
 ADR_VILLE, TIT_ETAT, TIT_HT, TIT_TTC, TIT_TVA, 
 TIT_LIBELLE, ORI_ENTITE, ORI_KEY_ENTITE, ECD_CREDIT, ECD_DEBIT, 
 RESTE_RECOUVRER)
AS 
select t.exe_ordre, ex.exe_exercice, ecd.pco_num, t.ges_code, p.pco_libelle, t.TIT_NUMERO, x.tit_date_emission,  x.rec_debiteur,
t.tit_id, f.FOU_CODE, f.ADR_NOM, f.ADR_PRENOM, f.ADR_CP, f.ADR_ADRESSE1, f.ADR_ADRESSE2,f.ADR_VILLE,
	t.TIT_ETAT, t.tit_ht, t.TIT_TTC, t.TIT_TVA, t.TIT_LIBELLE, o.ORI_ENTITE ,
o.ORI_KEY_ENTITE, ecd.ECD_CREDIT, ecd.ecd_debit, ecd.ecd_reste_emarger reste_recouvrer
from ecriture_detail ecd,
ecriture e,
plan_comptable p,
titre_detail_ecriture tde,
titre t,
v_fournisseur f,
origine o,
exercice ex,
(select TIT_ID, max(ecd1.EXE_ORDRE) EXE_ORDRE_MAX
from titre_detail_ecriture td1, ecriture_detail ecd1 where td1.ecd_ordre=ecd1.ecd_ordre and ((ecd1.pco_num like '4%' and ecd1.pco_num not like '445%') or ecd1.pco_num like '5%')
group by tit_id) z,
(select tit_id, max(rec_date) tit_date_emission , max(rec_debiteur) rec_debiteur, max(fou_ordre) fou_ordre from recette group by tit_id) x
where
	e.ecr_ordre = ecd.ecr_ordre
and t.exe_ordre=ex.exe_ordre
and ecd.pco_num=p.pco_num
and tde.ecd_ordre=ecd.ecd_ordre
and tde.tit_id=t.tit_id
and tde.tit_id=z.tit_id
and tde.EXE_ORDRE=z.exe_ordre_max
and x.tit_id=t.tit_id
and x.fou_ordre=f.fou_ordre(+)
and t.ori_ordre=o.ori_ordre(+)
and ((ecd.pco_num like '4%' and ecd.pco_num not like '445%') or ecd.pco_num like '5%')
and e.ecr_numero > 0
and substr(e.ecr_etat,1,1)='V';


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_RESTE_RECOUVRER_LIGHT
(TIT_ID, ECD_DEBIT, RESTE_RECOUVRER)
AS 
select tde.tit_id, sum(ecd.ecd_debit) ecd_debit, sum(ecd.ecd_reste_emarger) reste_recouvrer
from ecriture_detail ecd,
ecriture e,
titre_detail_ecriture tde,
(select TIT_ID, max(ecd1.EXE_ORDRE) EXE_ORDRE_MAX
from titre_detail_ecriture td1, ecriture_detail ecd1 where td1.ecd_ordre=ecd1.ecd_ordre and ((ecd1.pco_num like '4%' and ecd1.pco_num not like '445%') or ecd1.pco_num like '5%')
group by tit_id) z
where e.ecr_ordre = ecd.ecr_ordre
and tde.ecd_ordre=ecd.ecd_ordre
and tde.tit_id=z.tit_id
and tde.EXE_ORDRE=z.exe_ordre_max
and ((ecd.pco_num like '4%' and ecd.pco_num not like '445%') or ecd.pco_num like '5%')
and e.ecr_numero > 0
and ecd.ecd_debit>0
and substr(e.ecr_etat,1,1)='V'
group by tde.tit_id;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TVA
(TVA_ID, TVA_TAUX, TYET_ID)
AS 
SELECT 
TVA_ID, TVA_TAUX, TYET_ID
FROM JEFY_ADMIN.TVA;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TYPE_CREDIT_DEP_EXECUTOIRE
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE)
AS 
SELECT EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE,
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE
 FROM JEFY_ADMIN.TYPE_CREDIT_DEP_EXECUTOIRE;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TYPE_ETAT
(TYET_ID, TYET_LIBELLE)
AS 
SELECT 
TYET_ID, TYET_LIBELLE
FROM JEFY_ADMIN.TYPE_ETAT;


CREATE OR REPLACE FORCE VIEW MARACUJA.CFI_ECRITURES
(EXE_ORDRE, GES_CODE, PCO_NUM, PCO_LIBELLE, ECR_SACD, 
 CREDIT, DEBIT)
AS 
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, p.pco_libelle, ei.ECR_SACD ,SUM(ed.ecd_credit) credit, 0
        FROM ECRITURE_DETAIL ed, ECRITURE e, v_ECRITURE_INFOS ei, PLAN_COMPTABLE p
        WHERE ed.pco_num (+) = p.pco_num
        AND ed.ecd_sens = 'C'
        AND ed.ecr_ordre = e.ecr_ordre
		AND ed.ECD_ORDRE = ei.ecd_ordre
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,p.pco_libelle, ei.ecr_sacd
union all
SELECT e.exe_ordre, ed.ges_code, ed.pco_num, p.pco_libelle, ei.ECR_SACD, 0, SUM(ed.ecd_debit) debit
        FROM ECRITURE_DETAIL ed, ECRITURE e, v_ECRITURE_INFOS ei, PLAN_COMPTABLE p
        WHERE ed.pco_num (+) = p.pco_num
        AND ed.ecd_sens = 'D'
        AND ed.ecr_ordre = e.ecr_ordre
		AND ed.ECD_ORDRE = ei.ecd_ordre
        GROUP BY e.exe_ordre,ed.ges_code, ed.pco_num,p.pco_libelle, ei.ecr_sacd;


CREATE OR REPLACE FORCE VIEW MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
(PCO_NUM, EXE_ORDRE, GES_CODE, CREDIT, DEBIT, 
 ECR_SACD)
AS 
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit,ei.ECR_SACD
from ecriture_detail ed, ecriture e,gestion g, v_ECRITURE_INFOS ei
where ed.ecr_ordre=e.ecr_ordre
and ed.ecd_ordre=ei.ecd_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '7%'
and e.top_ordre <> 12
group by ed.pco_num,e.exe_ordre,ed.ges_code, ei.ECR_SACD
union all
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit, ei.ECR_SACD
from ecriture_detail ed, ecriture e,gestion g, v_ECRITURE_INFOS ei
where ed.ecr_ordre=e.ecr_ordre
and eD.ecd_ordre=ei.ecd_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '6%'
and e.top_ordre <> 12
group by ed.pco_num,e.exe_ordre,ed.ges_code, ei.ECR_SACD
union all
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit,ei.ECR_SACD
from ecriture_detail ed, ecriture e,gestion g, v_ECRITURE_INFOS ei
where ed.ecr_ordre=e.ecr_ordre
and ed.ecd_ordre=ei.ecd_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '187%'
and e.top_ordre <> 12
group by ed.pco_num,e.exe_ordre,ed.ges_code, ei.ECR_SACD
union all
select ed.pco_num,e.exe_ordre, ed.ges_code, sum(ecd_credit) credit, sum(ecd_debit) debit, ei.ECR_SACD
from ecriture_detail ed, ecriture e,gestion g, v_ECRITURE_INFOS ei
where ed.ecr_ordre=e.ecr_ordre
and eD.ecd_ordre=ei.ecd_ordre
and substr(ecr_etat,1,1) ='V'
AND ecr_numero>0
and g.ges_code = ed.ges_code
and pco_num like '186%'
and e.top_ordre <> 12
group by ed.pco_num,e.exe_ordre,ed.ges_code, ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VCREDIT
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_credit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, PLAN_COMPTABLE p, v_ECRITURE_INFOS ei, EXERCICE ex
        WHERE e.pco_num = p.pco_num
        AND e.ecd_credit<>0
        AND e.ecr_ordre = j.ecr_ordre
		and substr(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre <> 2
        AND j.exe_ordre=ex.exe_ordre
        AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
        GROUP BY ex.exe_exercice,j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VCREDIT_BE
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_credit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, PLAN_COMPTABLE p, v_ECRITURE_INFOS ei, EXERCICE ex
        WHERE e.pco_num = p.pco_num
        AND e.ecd_credit<>0
        AND e.ecr_ordre = j.ecr_ordre
		and substr(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre = 2
        AND j.exe_ordre=ex.exe_ordre
        AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
        GROUP BY ex.exe_exercice,j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VDEBIT
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(e.ecd_debit) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, PLAN_COMPTABLE p, v_ECRITURE_INFOS ei, EXERCICE ex
        WHERE e.pco_num = p.pco_num
        AND e.ecd_debit<>0
        AND e.ecr_ordre = j.ecr_ordre
		and substr(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre <> 2
        AND j.exe_ordre=ex.exe_ordre
        AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
        GROUP BY ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.VDEBIT_BE
(EXE_EXERCICE, ECR_DATE_SAISIE, PCO_NUM, GES_CODE, PCO_LIBELLE, 
 ECR_SACD, MONTANT)
AS 
SELECT ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ecr_sacd,SUM(en_nombre (e.ecd_debit)) montant
        FROM ECRITURE_DETAIL e, ECRITURE j, PLAN_COMPTABLE p, v_ECRITURE_INFOS ei, EXERCICE ex
        WHERE e.pco_num = p.pco_num
        AND e.ecd_debit<>0
        AND e.ecr_ordre = j.ecr_ordre
		and substr(j.ecr_etat,1,1)='V'
        AND j.tjo_ordre = 2
        AND j.exe_ordre=ex.exe_ordre
        AND e.ECd_ORDRE = ei.ECd_ORDRE (+)
        GROUP BY ex.exe_exercice, j.ecr_date_saisie,e.pco_num,e.ges_code,p.pco_libelle,ei.ECR_SACD;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, GES_LIB, IMP_LIB, BOR_DATE_VISA, BOR_NUM, 
 MAN_TTC)
AS 
SELECT mr.exe_ordre, mr.ecr_date_saisie, mr.ecr_date,
      mr.ges_code, mr.pco_num, mr.bor_id, mr.tbo_ordre, mr.man_id, mr.man_num, mr.man_lib,
      mr.man_mont, mr.man_tva, mr.man_etat, mr.fou_ordre, mr.ecr_ordre,
      mr.ecr_sacd, o.org_lib, pc.pco_libelle, b.bor_date_visa, b.bor_num,
      man_mont + man_tva
 FROM v_mandat_reimp_0 mr, v_organ_exer o, PLAN_COMPTABLE pc, BORDEREAU b
WHERE mr.ges_code = o.org_ub
  AND o.org_niv = 2
  AND mr.EXE_ORDRE=o.exe_ordre
  AND mr.pco_num = pc.pco_num
  AND mr.bor_id = b.bor_id;


CREATE OR REPLACE FORCE VIEW MARACUJA.V_TITRE_REIMP
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, TIT_ID, TIT_NUM, TIT_LIB, 
 TIT_MONT, TIT_TVA, TIT_TTC, TIT_ETAT, FOU_ORDRE, 
 REC_DEBITEUR, REC_INTERNE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE, 
 ECD_MONTANT, GES_LIB, IMP_LIB, DEBITEUR, BOR_NUM)
AS 
SELECT tr.*, o.ORG_LIB, pc.PCO_LIBELLE, o2.ORG_UB||' '||o2.ORG_CR||' '||o2.ORG_souscr, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, v_organ_exer o2, PLAN_COMPTABLE pc, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND rec_interne = o2.org_id AND tr.exe_ordre = o2.exe_ordre AND o2.org_niv >=2
AND tr.pco_num = pc.pco_num
AND tr.bor_id = b.bor_id
AND tr.rec_interne IS NOT NULL
UNION
SELECT tr.*, o.ORG_LIB, pc.PCO_LIBELLE, f.ADR_NOM||' '||f.ADR_PRENOM, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, PLAN_COMPTABLE pc, v_fournisseur f, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.pco_num = pc.pco_num
AND tr.fou_ordre = f.fou_ordre
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NOT NULL AND tr.rec_interne IS NULL
UNION
SELECT tr.*, o.ORG_LIB, pc.PCO_LIBELLE, tr.rec_debiteur, b.bor_num
FROM v_titre_reimp_0 tr, v_organ_exer o, PLAN_COMPTABLE pc, BORDEREAU b
WHERE tr.ges_code = o.org_ub AND o.org_niv = 2
AND tr.exe_ordre = o.exe_ordre
AND tr.pco_num = pc.pco_num
AND tr.bor_id = b.bor_id
AND tr.fou_ordre IS NULL AND tr.rec_debiteur IS NOT NULL AND LENGTH(tr.REC_DEBITEUR)<>0;


CREATE OR REPLACE FORCE VIEW MARACUJA.VBALANCE0
(EXE_EXERCICE, JOU_DATE, IMPUTATION, GESTION, LIBELLE, 
 ECR_SACD, BE_CR, MONTANT_CR, BE_DB, MONTANT_DB)
AS 
SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
        montant BE_CR,0 MONTANT_CR,0 BE_DB,0 MONTANT_DB
        FROM Vcredit_be
        UNION ALL
        SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
        0 BE_CR,montant MONTANT_CR,0 BE_DB,0 MONTANT_DB
        FROM Vcredit
        UNION ALL
        SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
        0 BE_CR,0 MONTANT_CR,montant BE_DB,0 MONTANT_DB
        FROM Vdebit_be
        UNION ALL
        SELECT exe_exercice,ecr_date_saisie,pco_num imputation,ges_code GESTION,pco_libelle libelle, ecr_sacd,
        0 BE_CR,0 MONTANT_CR,0 BE_DB,montant MONTANT_DB
        FROM Vdebit;


GRANT SELECT ON  MARACUJA.CFI_ECRITURES TO COMPTEFI;

GRANT SELECT ON  MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE TO COMPTEFI;

GRANT SELECT ON  MARACUJA.EXERCICE TO COMPTEFI;

GRANT SELECT ON  MARACUJA.EXERCICE TO CONVENTION;

GRANT SELECT ON  MARACUJA.TYPE_CREDIT TO JEFY;

GRANT SELECT ON  MARACUJA.TYPE_CREDIT TO JEFY_ADMIN;

GRANT SELECT ON  MARACUJA.TYPE_CREDIT TO JEFY05;

GRANT SELECT ON  MARACUJA.VBALANCE0 TO EPN;

--GRANT SELECT ON  MARACUJA.V_ECD_EMARGE_ORDO TO JEFY_DEPENSE;

--GRANT SELECT ON  MARACUJA.V_ECD_RESTEEMARGER TO JEFY_DEPENSE;

GRANT SELECT ON  MARACUJA.V_ECRITURE_INFOS TO COMPTEFI;

GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_DESTIN TO COMPTEFI;

GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_DESTIN_REC TO COMPTEFI;

GRANT SELECT ON  MARACUJA.V_JEFY_MULTIEX_TITRE TO COMPTEFI;

GRANT SELECT ON  MARACUJA.V_ORGAN_EXER TO COMPTEFI;
GRANT SELECT ON  MARACUJA.V_ORGAN_EXER TO EPN;

--GRANT SELECT ON  MARACUJA.V_PLANCO_CREDIT_DEP TO JEFY_RECETTE;

--GRANT SELECT ON  MARACUJA.V_PLANCO_CREDIT_REC TO JEFY_RECETTE;

GRANT SELECT ON  MARACUJA.V_TITRE_RESTE_RECOUVRER TO CONVENTION;





