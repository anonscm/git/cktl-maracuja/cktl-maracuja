SET define OFF
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- 
-- Nettoyage des donnees
-- * dans le cas ou un exercice 2007 a ete cree sur maracuja
-- * suppression d'enregistrements "orphelins"
-- 
-----------------------------------------------------------

-- Suppression des mode de paiement 2007
ALTER TRIGGER MARACUJA.LOG_DELETE_MODE_PAIEMENT DISABLE;
DELETE FROM maracuja.MODE_PAIEMENT WHERE exe_ordre>2006;
COMMIT;
ALTER TRIGGER MARACUJA.LOG_DELETE_MODE_PAIEMENT ENABLE;


-- Suppression des mode de recouvrement 2007
DELETE FROM maracuja.MODE_RECOUVREMENT WHERE exe_ordre>2006;
COMMIT;

DELETE FROM maracuja.GESTION_EXERCICE WHERE exe_ordre>2006 ;
COMMIT;

DELETE FROM maracuja.PARAMETRE WHERE exe_ordre>2006 ;
COMMIT;

DELETE FROM maracuja.PLANCO_CREDIT WHERE tcd_ordre IN (SELECT tcd_ordre FROM maracuja.TYPE_CREDIT WHERE exe_ordre>2006);
COMMIT;

DELETE FROM maracuja.TYPE_CREDIT WHERE exe_ordre>2006;
COMMIT;

DELETE FROM maracuja.EXERCICE WHERE exe_ordre>2006 ;
COMMIT;



-- ODPAIEM_DETAIL_ECRITURE sans ecriture_detail
DELETE FROM maracuja.ODPAIEM_DETAIL_ECRITURE WHERE ope_ordre IN (
SELECT ope_ordre FROM (
SELECT o.*, ecd.ecd_ordre AS ecd_ordre1 
FROM maracuja.ODPAIEM_DETAIL_ECRITURE o , maracuja.ECRITURE_DETAIL ecd 
WHERE o.ecd_ordre=ecd.ecd_ordre (+)
) WHERE ecd_ordre1 IS NULL );

COMMIT;

-- MANDAT_DETAIL_ECRITURE sans ecriture_detail
DELETE FROM maracuja.MANDAT_DETAIL_ECRITURE WHERE mde_ordre IN 
(SELECT mde_ordre FROM (
SELECT o.*, ecd.ecd_ordre AS ecd_ordre1 
FROM maracuja.MANDAT_DETAIL_ECRITURE o , maracuja.ECRITURE_DETAIL ecd 
WHERE o.ecd_ordre=ecd.ecd_ordre (+)
) WHERE ecd_ordre1 IS NULL );

COMMIT;

-- MANDAT_DETAIL_ECRITURE sans mandat
DELETE FROM maracuja.MANDAT_DETAIL_ECRITURE WHERE mde_ordre IN 
(SELECT mde_ordre FROM (
SELECT o.*, ecd.man_id AS man_id1 
FROM maracuja.MANDAT_DETAIL_ECRITURE o , maracuja.MANDAT ecd 
WHERE o.man_id=ecd.man_id (+)
) WHERE man_id1 IS NULL );

COMMIT;

-- depenses orphelines (sans mandat)
DELETE FROM maracuja.DEPENSE WHERE dep_id IN (
SELECT dep_id FROM (
SELECT o.*, m.man_id AS man_id1 
FROM maracuja.DEPENSE o ,maracuja.MANDAT m 
WHERE o.man_id=m.man_id (+)
)
WHERE man_id1 IS NULL
);

COMMIT;

-- mandat_brouillard sans mandat
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE MAB_ORDRE IN (
SELECT MAB_ORDRE FROM (
SELECT o.*, m.man_id AS man_id1 
FROM maracuja.MANDAT_BROUILLARD o ,maracuja.MANDAT m 
WHERE o.man_id=m.man_id (+)
)
WHERE man_id1 IS NULL
);

COMMIT;

-- TITRE_DETAIL_ECRITURE sans ecriture_detail
DELETE FROM maracuja.TITRE_DETAIL_ECRITURE WHERE tde_ordre IN 
(SELECT tde_ordre FROM (
SELECT o.*, ecd.ecd_ordre AS ecd_ordre1 
FROM maracuja.TITRE_DETAIL_ECRITURE o , maracuja.ECRITURE_DETAIL ecd 
WHERE o.ecd_ordre=ecd.ecd_ordre (+)
) WHERE ecd_ordre1 IS NULL );

COMMIT;

-- TITRE_DETAIL_ECRITURE sans titre
DELETE FROM maracuja.TITRE_DETAIL_ECRITURE WHERE tde_ordre IN 
(SELECT tde_ordre FROM (
SELECT o.*, ecd.tit_id AS tit_id1 
FROM maracuja.TITRE_DETAIL_ECRITURE o , maracuja.TITRE ecd 
WHERE o.tit_id=ecd.tit_id (+)
) WHERE tit_id1 IS NULL );

COMMIT;

-- recettes orphelines (sans titre)
DELETE FROM maracuja.RECETTE WHERE rec_id IN (
SELECT rec_id FROM (
SELECT o.*, m.tit_id AS tit_id1 
FROM maracuja.RECETTE o ,maracuja.TITRE m 
WHERE o.tit_id=m.tit_id (+)
)
WHERE tit_id1 IS NULL
);


COMMIT;

-- titre_brouillard sans titre
DELETE FROM maracuja.TITRE_BROUILLARD WHERE TIB_ORDRE IN (
SELECT TIB_ORDRE FROM (
SELECT o.*, m.tit_id AS tit_id1 
FROM maracuja.TITRE_BROUILLARD o ,maracuja.TITRE m 
WHERE o.tit_id=m.tit_id (+)
)
WHERE tit_id1 IS NULL
);

COMMIT;

-- corrections (certains depense.mod_ordre contenait un mod_code)
UPDATE maracuja.DEPENSE d SET mod_ordre = (SELECT mp.mod_ordre FROM maracuja.MODE_PAIEMENT mp 
WHERE mp.exe_ordre=d.exe_ordre AND mp.MOD_CODE=to_char(d.mod_ordre))  WHERE mod_ordre NOT IN  
( SELECT mod_ordre FROM maracuja.MODE_PAIEMENT);

COMMIT;

