SET define OFF
--
-- executer a partir de maracuja
-----------------------------------------------------------
-- 
-- Script de creation des contraintes de reference
-- si certaines contraintes ne peuvent pas etre activees
-- notez-les et prevenez moi (rodolphe.prin@univ-lr.fr)
-- 
-----------------------------------------------------------


-- Contraintes vers exe_ordre
ALTER TABLE maracuja.BALANCE ADD (CONSTRAINT fk_balance_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.BORDEREAU ADD (CONSTRAINT fk_bordereau_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.BORDEREAU_BROUILLARD ADD (CONSTRAINT fk_bordereau_brouillard_exe_or FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.BORDEREAU_REJET ADD (CONSTRAINT fk_bordereau_rejet_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.BROUILLARD ADD (CONSTRAINT fk_brouillard_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.CHEQUE_BROUILLARD ADD (CONSTRAINT fk_cheque_brouillard_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.CHEQUE_DETAIL_ECRITURE ADD (CONSTRAINT fk_cheque_detail_ecriture_exe_ FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.COMPTABILITE_EXERCICE ADD (CONSTRAINT fk_comptabilite_exercice_exe_o FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.DEPENSE ADD (CONSTRAINT fk_depense_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.ECHEANCIER ADD (CONSTRAINT fk_echeancier_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.ECRITURE ADD (CONSTRAINT fk_ecriture_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.ECRITURE_DETAIL ADD (CONSTRAINT fk_ecriture_detail_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.EMARGEMENT ADD (CONSTRAINT fk_emargement_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.EMARGEMENT_DETAIL ADD (CONSTRAINT fk_emargement_detail_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.GESTION_EXERCICE ADD (CONSTRAINT fk_gestion_exercice_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.MANDAT_BROUILLARD ADD (CONSTRAINT fk_mandat_brouillard_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.MANDAT_DETAIL_ECRITURE ADD (CONSTRAINT fk_mandat_detail_ecriture_exe_ FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.MODE_PAIEMENT ADD (CONSTRAINT fk_mode_paiement_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.MODE_RECOUVREMENT ADD (CONSTRAINT fk_mode_recouvrement_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.NUMEROTATION ADD (CONSTRAINT fk_numerotation_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.OPE_TRESORERIE ADD (CONSTRAINT fk_ope_tresorerie_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.ORDRE_DE_PAIEMENT ADD (CONSTRAINT fk_ordre_de_paiement_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.PAIEMENT ADD (CONSTRAINT fk_paiement_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.PARAMETRE ADD (CONSTRAINT fk_parametre_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.RECETTE ADD (CONSTRAINT fk_recette_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.RECETTE_RELANCE ADD (CONSTRAINT fk_recette_relance_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.RECOUVREMENT ADD (CONSTRAINT fk_recouvrement_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.REIMPUTATION ADD (CONSTRAINT fk_reimputation_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.RETENUE ADD (CONSTRAINT fk_retenue_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.RETENUE_DETAIL ADD (CONSTRAINT fk_retenue_detail_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.TITRE_BROUILLARD ADD (CONSTRAINT fk_titre_brouillard_exe_ordre FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );
ALTER TABLE maracuja.TITRE_DETAIL_ECRITURE ADD (CONSTRAINT fk_titre_detail_ecriture_exe_o FOREIGN KEY (exe_ordre) REFERENCES jefy_admin.EXERCICE (exe_ordre) );


-- Contraintes vers bor_id
ALTER TABLE maracuja.BORDEREAU_BROUILLARD ADD (CONSTRAINT fk_bordereau_brouillard_bor_id FOREIGN KEY (bor_id) REFERENCES maracuja.BORDEREAU (bor_id) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.BORDEREAU_INFO ADD (CONSTRAINT fk_bordereau_info_bor_id FOREIGN KEY (bor_id) REFERENCES maracuja.BORDEREAU (bor_id) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.CHEQUE ADD (CONSTRAINT fk_cheque_bor_id FOREIGN KEY (bor_id) REFERENCES maracuja.BORDEREAU (bor_id) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_bor_id FOREIGN KEY (bor_id) REFERENCES maracuja.BORDEREAU (bor_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_bor_id FOREIGN KEY (bor_id) REFERENCES maracuja.BORDEREAU (bor_id) DEFERRABLE INITIALLY DEFERRED); 


-- Contraintes vers ecd_ordre
ALTER TABLE maracuja.CHEQUE_DETAIL_ECRITURE ADD (CONSTRAINT fk_cheque_detail_ecriture_ecd_ FOREIGN KEY (ecd_ordre) REFERENCES maracuja.ECRITURE_DETAIL (ecd_ordre)  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.MANDAT_DETAIL_ECRITURE ADD (CONSTRAINT fk_mandat_detail_ecriture_ecd_ FOREIGN KEY (ecd_ordre) REFERENCES maracuja.ECRITURE_DETAIL (ecd_ordre)  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.ODPAIEM_DETAIL_ECRITURE ADD (CONSTRAINT fk_odpaiem_detail_ecriture_ecd FOREIGN KEY (ecd_ordre) REFERENCES maracuja.ECRITURE_DETAIL (ecd_ordre)  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.OPE_TRESORERIE_ECRITURE ADD (CONSTRAINT fk_ope_tresorerie_ecriture_ecd FOREIGN KEY (ecd_ordre) REFERENCES maracuja.ECRITURE_DETAIL (ecd_ordre) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.TITRE_DETAIL_ECRITURE ADD (CONSTRAINT fk_titre_detail_ecriture_ecd_o FOREIGN KEY (ecd_ordre) REFERENCES maracuja.ECRITURE_DETAIL (ecd_ordre) DEFERRABLE INITIALLY DEFERRED );

-- Contraintes vers ecr_ordre
ALTER TABLE maracuja.ORDRE_DE_PAIEMENT ADD (CONSTRAINT fk_ordre_de_paiement_ecr_ordre FOREIGN KEY (ecr_ordre) REFERENCES maracuja.ECRITURE (ecr_ordre) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.REIMPUTATION ADD (CONSTRAINT fk_reimputation_ecr_ordre FOREIGN KEY (ecr_ordre) REFERENCES maracuja.ECRITURE (ecr_ordre) DEFERRABLE INITIALLY DEFERRED );

-- Contraintes vers mod_ordre
ALTER TABLE MARACUJA.DEPENSE ADD (CONSTRAINT FK_DEPENSE_MOD_ORDRE FOREIGN KEY (MOD_ORDRE) REFERENCES maracuja.MODE_PAIEMENT (mod_ordre) ); 
--ALTER TABLE MARACUJA.MANDAT ADD (CONSTRAINT FK_MANDAT_MOD_ORDRE FOREIGN KEY (MOD_ORDRE) REFERENCES maracuja.MODE_PAIEMENT (mod_ordre) ); 
ALTER TABLE MARACUJA.ORDRE_DE_PAIEMENT ADD (CONSTRAINT FK_ORDRE_DE_PAIEMENT_MOD_ORDRE FOREIGN KEY (MOD_ORDRE) REFERENCES maracuja.MODE_PAIEMENT (mod_ordre) ); 
ALTER TABLE MARACUJA.RECETTE ADD (CONSTRAINT FK_RECETTE_MOD_ORDRE FOREIGN KEY (MOD_ORDRE) REFERENCES maracuja.MODE_PAIEMENT (mod_ordre) ); 
ALTER TABLE MARACUJA.TITRE ADD (CONSTRAINT FK_TITRE_MOD_ORDRE FOREIGN KEY (MOD_ORDRE) REFERENCES maracuja.MODE_PAIEMENT (mod_ordre) ); 

-- Contraintes vers pai_ordre
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre) );
ALTER TABLE maracuja.ORDRE_DE_PAIEMENT ADD (CONSTRAINT fk_ordre_de_paiement_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre) );
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre) );
ALTER TABLE maracuja.VIREMENT_FICHIER ADD (CONSTRAINT fk_virement_fichier_pai_ordre FOREIGN KEY (pai_ordre) REFERENCES maracuja.PAIEMENT (pai_ordre) );

-- Contraintes vers ges_code
ALTER TABLE maracuja.BALANCE ADD (CONSTRAINT fk_balance_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.BORDEREAU ADD (CONSTRAINT fk_bordereau_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.BORDEREAU_BROUILLARD ADD (CONSTRAINT fk_bordereau_brouillard_ges_co FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.BORDEREAU_REJET ADD (CONSTRAINT fk_bordereau_rejet_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.CHEQUE_BROUILLARD ADD (CONSTRAINT fk_cheque_brouillard_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.COMPTABILITE ADD (CONSTRAINT fk_comptabilite_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.DEPENSE ADD (CONSTRAINT fk_depense_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.MANDAT_BROUILLARD ADD (CONSTRAINT fk_mandat_brouillard_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.NUMEROTATION ADD (CONSTRAINT fk_numerotation_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.ODPAIEMENT_BROUILLARD ADD (CONSTRAINT fk_odpaiement_brouillard_ges_c FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.RECETTE ADD (CONSTRAINT fk_recette_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );
ALTER TABLE maracuja.TITRE_BROUILLARD ADD (CONSTRAINT fk_titre_brouillard_ges_code FOREIGN KEY (ges_code) REFERENCES maracuja.GESTION (ges_code) );

-- Contraintes vers pco_num
ALTER TABLE maracuja.BALANCE ADD (CONSTRAINT fk_balance_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.BORDEREAU_BROUILLARD ADD (CONSTRAINT fk_bordereau_brouillard_pco_nu FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.BORDEREAU_INFO ADD (CONSTRAINT fk_bordereau_info_pco_num_visa FOREIGN KEY (pco_num_visa) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.CHEQUE_BROUILLARD ADD (CONSTRAINT fk_cheque_brouillard_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.COMPTE_BANCAIRE ADD (CONSTRAINT fk_compte_bancaire_pco_num_emi FOREIGN KEY (pco_num_emi) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.COMPTE_BANCAIRE ADD (CONSTRAINT fk_compte_bancaire_pco_num_enc FOREIGN KEY (pco_num_enc) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.COMPTE_BANCAIRE ADD (CONSTRAINT fk_compte_bancaire_pco_num_ope FOREIGN KEY (pco_num_ope) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
--ALTER TABLE maracuja.ECRITURE_DETAIL ADD (CONSTRAINT fk_ecriture_detail_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.GESTION_EXERCICE ADD (CONSTRAINT fk_gestion_exercice_pco_num181 FOREIGN KEY (pco_num_181) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.GESTION_EXERCICE ADD (CONSTRAINT fk_gestion_exercice_pco_num185 FOREIGN KEY (pco_num_185) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.MANDAT_BROUILLARD ADD (CONSTRAINT fk_mandat_brouillard_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.MODE_PAIEMENT ADD (CONSTRAINT fk_mode_paiement_pco_num_paiem FOREIGN KEY (pco_num_paiement) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.MODE_PAIEMENT ADD (CONSTRAINT fk_mode_paiement_pco_num_visa FOREIGN KEY (pco_num_visa) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.MODE_RECOUVREMENT ADD (CONSTRAINT fk_mode_recouvrement_pco_num_p FOREIGN KEY (pco_num_paiement) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.MODE_RECOUVREMENT ADD (CONSTRAINT fk_mode_recouvrement_pco_num_v FOREIGN KEY (pco_num_visa) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.ODPAIEMENT_BROUILLARD ADD (CONSTRAINT fk_odpaiement_brouillard_pco_n FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.PLANCO_CREDIT ADD (CONSTRAINT fk_planco_credit_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) DEFERRABLE INITIALLY DEFERRED );
--ALTER TABLE maracuja.PLANCO_RELANCE ADD (CONSTRAINT fk_planco_relance_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num)  DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE maracuja.PLANCO_VISA ADD (CONSTRAINT fk_planco_visa_pco_num_ctrepar FOREIGN KEY (pco_num_ctrepartie) REFERENCES maracuja.PLAN_COMPTABLE (pco_num)  DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE maracuja.PLANCO_VISA ADD (CONSTRAINT fk_planco_visa_pco_num_ordonna FOREIGN KEY (pco_num_ordonnateur) REFERENCES maracuja.PLAN_COMPTABLE (pco_num)  DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE maracuja.PLANCO_VISA ADD (CONSTRAINT fk_planco_visa_pco_num_tva FOREIGN KEY (pco_num_tva) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.PLAN_ANALYTIQUE ADD (CONSTRAINT fk_plan_analytique_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.RECETTE ADD (CONSTRAINT fk_recette_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.REIMPUTATION ADD (CONSTRAINT fk_reimputation_pco_num_ancien FOREIGN KEY (pco_num_ancien) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.REIMPUTATION ADD (CONSTRAINT fk_reimputation_pco_num_nouvea FOREIGN KEY (pco_num_nouveau) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.TITRE_BROUILLARD ADD (CONSTRAINT fk_titre_brouillard_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.TYPE_OPERATION_TRESOR ADD (CONSTRAINT fk_type_operation_tresor_pco FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.TYPE_OPERATION_TRESOR ADD (CONSTRAINT fk_type_operation_tresor_pcoct FOREIGN KEY (pco_num_contre_partie) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );
ALTER TABLE maracuja.TYPE_RETENUE ADD (CONSTRAINT fk_type_retenue_pco_num FOREIGN KEY (pco_num) REFERENCES maracuja.PLAN_COMPTABLE (pco_num) );


-- Contraintes vers man_id
ALTER TABLE maracuja.DEPENSE ADD (CONSTRAINT fk_depense_man_id FOREIGN KEY (man_id) REFERENCES maracuja.MANDAT (man_id)   DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.MANDAT_BROUILLARD ADD (CONSTRAINT fk_mandat_brouillard_man_id FOREIGN KEY (man_id) REFERENCES maracuja.MANDAT (man_id)  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.MANDAT_DETAIL_ECRITURE ADD (CONSTRAINT fk_mandat_detail_ecriture_man_ FOREIGN KEY (man_id) REFERENCES maracuja.MANDAT (man_id)  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.REIMPUTATION ADD (CONSTRAINT fk_reimputation_man_id FOREIGN KEY (man_id) REFERENCES maracuja.MANDAT (man_id) );

-- Contraintes vers tit_id
ALTER TABLE maracuja.ECHEANCIER ADD (CONSTRAINT fk_echeancier_tit_id FOREIGN KEY (tit_id) REFERENCES maracuja.TITRE (tit_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE maracuja.RECETTE ADD (CONSTRAINT fk_recette_tit_id FOREIGN KEY (tit_id) REFERENCES maracuja.TITRE (tit_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE maracuja.REIMPUTATION ADD (CONSTRAINT fk_reimputation_tit_id FOREIGN KEY (tit_id) REFERENCES maracuja.TITRE (tit_id) );
ALTER TABLE maracuja.TITRE_BROUILLARD ADD (CONSTRAINT fk_titre_brouillard_tit_id FOREIGN KEY (tit_id) REFERENCES maracuja.TITRE (tit_id) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.TITRE_DETAIL_ECRITURE ADD (CONSTRAINT fk_titre_detail_ecriture_tit_i FOREIGN KEY (tit_id) REFERENCES maracuja.TITRE (tit_id) );

-- contraintes vers brj_ordre
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_brj_ordre FOREIGN KEY (brj_ordre) REFERENCES maracuja.bordereau_rejet (brj_ordre) DEFERRABLE INITIALLY DEFERRED);

-- contraintes vers com_ordre
ALTER TABLE maracuja.BROUILLARD ADD (CONSTRAINT fk_brouillard_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.COMPTABILITE_EXERCICE ADD (CONSTRAINT fk_comptabilite_exercice_com_o FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.COMPTE_BANCAIRE ADD (CONSTRAINT fk_compte_bancaire_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.ECRITURE ADD (CONSTRAINT fk_ecriture_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.EMARGEMENT ADD (CONSTRAINT fk_emargement_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.GESTION ADD (CONSTRAINT fk_gestion_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.NUMEROTATION ADD (CONSTRAINT fk_numerotation_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.ORDRE_DE_PAIEMENT ADD (CONSTRAINT fk_ordre_de_paiement_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.PAIEMENT ADD (CONSTRAINT fk_paiement_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.RECOUVREMENT ADD (CONSTRAINT fk_recouvrement_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));
ALTER TABLE maracuja.RETENUE ADD (CONSTRAINT fk_retenue_com_ordre FOREIGN KEY (com_ordre) REFERENCES maracuja.COMPTABILITE(com_ordre ));

-- contraintes vers rec_id
ALTER TABLE maracuja.ECHEANCIER ADD (CONSTRAINT fk_echeancier_rec_id FOREIGN KEY (rec_id) REFERENCES maracuja.RECETTE(rec_id )  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.RECETTE_INFO ADD (CONSTRAINT fk_recette_info_rec_id FOREIGN KEY (rec_id) REFERENCES maracuja.RECETTE(rec_id ) DEFERRABLE INITIALLY DEFERRED  );
ALTER TABLE maracuja.RECETTE_RELANCE ADD (CONSTRAINT fk_recette_relance_rec_id FOREIGN KEY (rec_id) REFERENCES maracuja.RECETTE(rec_id )  DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.RETENUE_DETAIL ADD (CONSTRAINT fk_retenue_detail_rec_id FOREIGN KEY (rec_id) REFERENCES maracuja.RECETTE(rec_id ));
ALTER TABLE maracuja.TITRE_BROUILLARD ADD (CONSTRAINT fk_titre_brouillard_rec_id FOREIGN KEY (rec_id) REFERENCES maracuja.RECETTE(rec_id ) DEFERRABLE INITIALLY DEFERRED );
ALTER TABLE maracuja.TITRE_DETAIL_ECRITURE ADD (CONSTRAINT fk_titre_detail_ecriture_rec_i FOREIGN KEY (rec_id) REFERENCES maracuja.RECETTE(rec_id ) DEFERRABLE INITIALLY DEFERRED );

-- contraintes vers dep_id
ALTER TABLE MARACUJA.RETENUE_DETAIL ADD (CONSTRAINT FK_RETENUE_DETAIL_DEP_ID FOREIGN KEY (DEP_ID) REFERENCES maracuja.DEPENSE(dep_id ));

-- contraintes vers tbo_ordre
ALTER TABLE maracuja.BORDEREAU ADD (CONSTRAINT fk_bordereau_tbo_ordre FOREIGN KEY (tbo_ordre) REFERENCES maracuja.TYPE_BORDEREAU(tbo_ordre ));
ALTER TABLE maracuja.BORDEREAU_REJET ADD (CONSTRAINT fk_bordereau_rejet_tbo_ordre FOREIGN KEY (tbo_ordre) REFERENCES maracuja.TYPE_BORDEREAU(tbo_ordre ));

-- contraintes vers type_journal.tjo_ordre
ALTER TABLE maracuja.ECRITURE ADD (CONSTRAINT fk_ecriture_tjo_ordre FOREIGN KEY (tjo_ordre) REFERENCES maracuja.TYPE_JOURNAL(tjo_ordre ));

-- contraintes vers type_operation.top_ordre
ALTER TABLE maracuja.ECRITURE ADD (CONSTRAINT fk_ecriture_top_ordre FOREIGN KEY (top_ordre) REFERENCES maracuja.TYPE_OPERATION(top_ordre ));
ALTER TABLE maracuja.TYPE_OPERATION ADD (CONSTRAINT fk_type_operation_top_ordre FOREIGN KEY (top_ordre) REFERENCES maracuja.TYPE_OPERATION(top_ordre ));

-- contraintes vers type_origine_bordereau.tor_ordre
ALTER TABLE maracuja.MANDAT ADD (CONSTRAINT fk_mandat_tor_ordre FOREIGN KEY (tor_ordre) REFERENCES maracuja.TYPE_ORIGINE_BORDEREAU(tor_ordre ));
ALTER TABLE maracuja.TITRE ADD (CONSTRAINT fk_titre_tor_ordre FOREIGN KEY (tor_ordre) REFERENCES maracuja.TYPE_ORIGINE_BORDEREAU(tor_ordre ));

-- contraintes vers type_emargement.tem_ordre
ALTER TABLE maracuja.EMARGEMENT ADD (CONSTRAINT fk_emargement_tem_ordre FOREIGN KEY (tem_ordre) REFERENCES maracuja.TYPE_EMARGEMENT(tem_ordre ));

-- contraintes vers type_CREDIT.tcd_ordre
ALTER TABLE MARACUJA.DEPENSE ADD (CONSTRAINT FK_DEPENSE_TCD_ORDRE FOREIGN KEY (TCD_ORDRE) REFERENCES jefy_admin.type_credit(tcd_ordre )); 
ALTER TABLE MARACUJA.PLANCO_CREDIT ADD (CONSTRAINT FK_PLANCO_CREDIT_TCD_ORDRE FOREIGN KEY (TCD_ORDRE) REFERENCES jefy_admin.type_credit(tcd_ordre )); 


---------------------------------------------

-- 
-- creation des contraintes UNIQUE
-- 
ALTER TABLE MARACUJA.PLANCO_CREDIT ADD CONSTRAINT UNQ_PLANCO_CREDIT_TCD_ORDRE
 UNIQUE (TCD_ORDRE, PCO_NUM, pla_quoi)
 ENABLE
 VALIDATE;
 
ALTER TABLE MARACUJA.PLANCO_VISA ADD CONSTRAINT UNQ_PLANCO_VISA_PCO_NUM
 UNIQUE (PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR)
 ENABLE
 VALIDATE;
 
 
---------------------------------------------
 
INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.5.0',  SYSDATE, 'Version pour Jefyco 2007');
COMMIT; 


