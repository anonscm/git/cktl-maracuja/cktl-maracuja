CREATE OR REPLACE PACKAGE BODY Bordereau_Titre IS
--version 1.1.9 04/05/2006 - modif pour plusieurs ventilations
--version 1.1.8 31/05/2005 - modifs prestations internes
--version 1.1.7 26/04/2005 - prestations internes
--version 1.1.8 23/06/2005 - structure table recette
PROCEDURE Get_Btte_Jefy
(borordre NUMBER,exeordre NUMBER)

IS
	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;
	dbms_output.put_line('exercice : '||exeordre);

	-- est ce un bordereau de titre --
	SELECT COUNT(*) INTO cpt FROM jefy.TITRE
		   WHERE bor_ordre = borordre;

	IF cpt != 0 THEN

		-- le bordereau est il deja vis? ? --
		tboordre:=Gestionorigine.recup_type_bordereau_titre(borordre, exeordre);

		SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;




		-- l exercice est il encore ouvert ? --


		-- pas de raise mais pas de recup s il est deja dans maracuja --
		IF cpt = 0 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;
			/*
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy.TITRE
			WHERE tit_ordre =
			 ( SELECT MAX(tit_ordre)
			   FROM jefy.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre
			  );

			-- recuperation du type de bordereau --
			SELECT COUNT(*) INTO cpt
			FROM OLD_AGENT_TYPE_BORD
			WHERE agt_ordre = agtordre;

			IF cpt <> 0 THEN
			 SELECT tbo_ordre INTO tboordre
			 FROM OLD_AGENT_TYPE_BORD
			 WHERE agt_ordre = agtordre;
			ELSE
			 SELECT tbo_ordre INTO tboordre
			 FROM TYPE_BORDEREAU
			 WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' and EXE_ORDRE=exeordre);
			END IF;
			*/

			-- creation du bor_id --
			SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

			-- recuperation de l utilisateur --
			/*select utl_ordre into utlordre
			from utilisateur
			where agt_ordre = agtordre;
			*/
			-- TEST  TEST  TEST  TEST  TEST
			SELECT 1 INTO utlordre
				   FROM dual;
			-- TEST  TEST  TEST  TEST  TEST

			-- creation du bordereau --
			INSERT INTO BORDEREAU VALUES (
				NULL , 	 	 	      --BOR_DATE_VISA,
				'VALIDE',			  --BOR_ETAT,
				borid,				  --BOR_ID,
				leBordereau.bor_num,  --BOR_NUM,
				leBordereau.bor_ordre,--BOR_ORDRE,
				exeordre,	  		  --EXE_ORDRE,
				leBordereau.ges_code, --GES_CODE,
				tboordre,			  --TBO_ORDRE,
				utlordre,		 	  --UTL_ORDRE,
				NULL				  --UTL_ORDRE_VISA
				);

			Get_Titre_Jefy(exeordre,borordre,borid,utlordre,agtordre);

		END IF;
		--else
		-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
	END IF;

END;


PROCEDURE Get_Titre_Jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	jefytitre jefy.TITRE%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	titid 		 TITRE.tit_id%TYPE;

	titoriginekey  TITRE.tit_ORGINE_KEY%TYPE;
	titoriginelib TITRE.tit_ORIGINE_LIB%TYPE;
	ORIORDRE 	   TITRE.ORI_ORDRE%TYPE;
	PRESTID 	   TITRE.PREST_ID%TYPE;
	TORORDRE 	   TITRE.TOR_ORDRE%TYPE;
	modordre	   TITRE.mod_ordre%TYPE;
	presid		   INTEGER;
	cpt			   INTEGER;

	CURSOR lesJefytitres IS
	SELECT *
	FROM jefy.TITRE
	WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefytitres;
	LOOP
		FETCH lesJefytitres INTO jefytitre;
			  EXIT WHEN lesJefytitres%NOTFOUND;

		-- recup ??
		IF (jefytitre.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(jefytitre.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(jefytitre.org_ordre);
		END IF;

		titoriginekey  :=NULL;
		titoriginelib   :=NULL;

		-- recup du torordre origine JEFYCO
		TORORDRE 	     :=1;
		-- JEFYCO
		torordre := 1;
		-- recup du mode de paiement OP ordo
		modordre	     :=NULL;

		dbms_output.put_line('titreOrdre : '||jefytitre.tit_ordre);
		dbms_output.put_line('mod_code : '||jefytitre.mod_code);


		IF jefytitre.tit_type = 'C' THEN
		   SELECT mod_ordre INTO modordre
		   FROM MODE_PAIEMENT
		   WHERE mod_code = jefytitre.mod_code
		   AND exe_ordre = exeordre;
		ELSE
			modordre := NULL;
		END IF;



		-- recup du prestid
		presid		     :=NULL;


		--TODO des gescode sont nuls, recuperer le gescode de la table comptabilite ?
		IF jefytitre.ges_code IS NULL THEN
			 SELECT DISTINCT ges_code INTO gescode
			 FROM BORDEREAU
			 WHERE bor_id=borid;
		ELSE
		 	gescode:=jefytitre.ges_code;
		END IF;

		SELECT titre_seq.NEXTVAL INTO titid FROM dual;

		INSERT INTO TITRE (
		   BOR_ID,
		   BOR_ORDRE,
		   BRJ_ORDRE,
		   EXE_ORDRE,
		   GES_CODE,
		   MOD_ORDRE,
		   ORI_ORDRE,
		   PCO_NUM,
		   PREST_ID,
		   TIT_DATE_REMISE,
		   TIT_DATE_VISA_PRINC,
		   TIT_ETAT,
		   TIT_ETAT_REMISE,
		   TIT_HT,
		   TIT_ID,
		   TIT_MOTIF_REJET,
		   TIT_NB_PIECE,
		   TIT_NUMERO,
		   TIT_NUMERO_REJET,
		   TIT_ORDRE,
		   TIT_ORGINE_KEY,
		   TIT_ORIGINE_LIB,
		   TIT_TTC,
		   TIT_TVA,
		   TOR_ORDRE,
		   UTL_ORDRE,
		   ORG_ORDRE,
		   FOU_ORDRE,
		   MOR_ORDRE,
		   PAI_ORDRE,
		   rib_ordre_ordonnateur,
		   rib_ordre_comptable,
		   tit_libelle)
		VALUES
			(
			borid,--BOR_ID,
			borordre,--BOR_ORDRE,
			NULL,--BRJ_ORDRE,
			exeordre,--EXE_ORDRE,
			gescode,--GES_CODE,
			modordre,--MOD_ORDRE,
			oriordre,--ORI_ORDRE,
			jefytitre.pco_num,--PCO_NUM,
			jefytitre.pres_ordre,--PREST_ID,
			NULL,--TIT_DATE_REMISE,
			NULL,--TIT_DATE_VISA_PRINC,
			'ATTENTE',--TIT_ETAT,
			'ATTENTE',--TIT_ETAT_REMISE,
			NVL(En_Nombre(jefytitre.tit_mont),0),--TIT_HT,
			titid,--TIT_ID,
			NULL,--TIT_MOTIF_REJET,
			jefytitre.tit_piece,--TIT_NB_PIECE,
			jefytitre.tit_num,--TIT_NUMERO,
			NULL,--TIT_NUMERO_REJET,
			jefytitre.tit_ordre,--TIT_ORDRE,
			titoriginekey,--TIT_ORGINE_KEY,
			titoriginelib,--TIT_ORIGINE_LIB,
			NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TIT_TTC,
			NVL(En_Nombre(jefytitre.tit_monttva),0),--TIT_TVA,
			torordre,--TOR_ORDRE,
			utlordre,--UTL_ORDRE
			jefytitre.org_ordre,		--ORG_ORDRE,
			jefytitre.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
			modordre,				 --MOR_ORDRE
			NULL, -- VIR_ORDRE
			jefytitre.rib_ordre,
			jefytitre.rib_ordre,
			jefytitre.tit_lib
			);

		--on met a jour le type de bordereau s'il s'agit d'un bordereau de prestation interne
		SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=jefytitre.tit_ordre AND exe_ordre=exeordre;
		IF cpt != 0 THEN
--		if jefytitre.pres_ordre is not null then
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;

		Get_Recette_Jefy(exeordre,titid,jefytitre.tit_ordre,utlordre);
		Set_Titre_Brouillard(titid);

	END LOOP;

	CLOSE lesJefytitres;

END;




PROCEDURE Get_Recette_Jefy
(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefytitre		 jefy.TITRE%ROWTYPE;
	--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	--fouadresse  	 DEPENSE.dep_adresse%TYPE;
	--founom  		 DEPENSE.dep_fournisseur%TYPE;
	--lotordre  		 DEPENSE.dep_lot%TYPE;
	--marordre		 DEPENSE.dep_marches%TYPE;
	--fouordre		 DEPENSE.fou_ordre%TYPE;
	--gescode			 DEPENSE.ges_code%TYPE;
	cpt				 INTEGER;
	recid			 RECETTE.rec_id%TYPE;
	rectype			 RECETTE.rec_type%TYPE;
	gescode			 TITRE.GES_CODE%TYPE;

	modordre	   DEPENSE.mod_ordre%TYPE;


BEGIN

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = titordre;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;

	rectype := NULL;

	-- ajout rod
	IF jefytitre.tit_type = 'C' THEN
	   SELECT mod_ordre INTO modordre
	   FROM MODE_PAIEMENT
	   WHERE mod_code = jefytitre.mod_code
	   AND exe_ordre = exeordre;
	ELSE
		modordre := NULL;
	END IF;


-- 	IF jefytitre.ges_code IS NULL THEN
-- 	 SELECT distinct ges_code INTO gescode
-- 	 FROM jefy.ventil_titre
-- 	 WHERE tit_ordre = jefytitre.tit_ordre;
-- 	else
-- 	 gescode:=jefytitre.ges_code;
-- 	END IF;


	 SELECT DISTINCT ges_code INTO gescode
	 FROM maracuja.TITRE
	 WHERE tit_id = titid;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;


	INSERT INTO RECETTE VALUES
		(
		exeordre,--EXE_ORDRE,
		gescode,--GES_CODE,
		jefytitre.mod_code,--MOD_CODE,
		jefytitre.pco_num,--PCO_NUM,
		jefytitre.tit_date,-- REC_DATE,
		jefytitre.tit_debiteur,-- REC_DEBITEUR,
		recid,-- REC_ID,
		jefytitre.pco_num,-- REC_IMPUTTVA,
		jefytitre.tit_interne,-- REC_INTERNE,
		jefytitre.tit_lib,-- REC_LIBELLE,
		jefytitre.org_ordre,-- REC_LIGNE_BUDGETAIRE,
		'E',-- REC_MONNAIE,
		NVL(En_Nombre(jefytitre.tit_mont),0),--HT,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TTC,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--DISQUETTE,
		NVL(En_Nombre(jefytitre.tit_monttva),0),--   REC_MONTTVA,
		jefytitre.tit_num,--   REC_NUM,
		jefytitre.tit_ordre,--   REC_ORDRE,
		jefytitre.tit_piece,--   REC_PIECE,
		jefytitre.tit_ref,--   REC_REF,
		'VALIDE',--   REC_STAT,
		'NON',--    REC_SUPPRESSION,  Modif Rod
		jefytitre.tit_type,--	 REC_TYPE,
		NULL,--	 REC_VIREMENT,
		titid,--	  TIT_ID,
		titordre,--	  TIT_ORDRE,
		utlordre,--	   UTL_ORDRE
		jefytitre.org_ordre,		--	   ORG_ORDRE --ajout rod
		jefytitre.fou_ordre,   --FOU_ORDRE --ajout rod
		modordre, --mod_ordre
		NULL,  --mor_ordre
		jefytitre.rib_ordre,
		NULL
		);

-- recuperation des echeanciers
Bordereau_Titre.Get_recette_prelevements (exeordre ,titid ,titordre ,utlordre,recid );


END;



PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
echancier_data PRELEV.ECHEANCIER%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;
personne_data grhum.v_personne%ROWTYPE;
ORIORDRE INTEGER;
modordre INTEGER;

BEGIN

-- verifier s il y a une facture_titre associee
SELECT COUNT(*) INTO cpt FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;
IF (cpt=0) THEN
	  RETURN;
END IF;


-- recup du facture_titre pour le titre concerne
SELECT * INTO FACTURE_TITRE_data
FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;

dbms_output.put_line ('titordre='||TITORDRE);


SELECT COUNT(*) INTO cpt FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';
IF (cpt=0) THEN
	  RETURN;
END IF;


SELECT * INTO echancier_data
FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';





SELECT * INTO CLIENT_data
FROM PRELEV.CLIENT
WHERE CLIENT_ORDRE = echancier_data.client_ordre
AND supprime = 'N';




-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;



-- recup ??
IF (echancier_data.con_ordre IS NOT NULL) THEN
 ORIORDRE :=Gestionorigine.traiter_convordre(echancier_data.con_ordre);
ELSE
 ORIORDRE :=Gestionorigine.traiter_orgordre(echancier_data.org_ordre);
END IF;

SELECT * INTO personne_data
FROM GRHUM.V_PERSONNE WHERE pers_id = CLIENT_data.pers_id;

INSERT INTO MARACUJA.ECHEANCIER(AUTORIS_SIGNEE, CLIENT_ORDRE, 
CON_ORDRE, DATE_1ERE_ECHEANCE, DATE_CREATION, 
DATE_MODIF, ECHEANCIER_ORDRE, ETAT_PRELEVEMENT, FT_ORDRE, 
LIBELLE, MONTANT, MONTANT_EN_LETTRES, 
NOMBRE_ECHEANCES, NUMERO_INDEX, 
ORG_ORDRE, PREST_ORDRE, PRISE_EN_CHARGE, 
REF_FACTURE_EXTERNE, SUPPRIME) VALUES
(
echancier_data.AUTORIS_SIGNEE  ,--ECHE_AUTORIS_SIGNEE
echancier_data.CLIENT_ORDRE  ,--FOU_ORDRE_CLIENT
echancier_data.CON_ORDRE  ,--CON_ORDRE
echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
echancier_data.FT_ORDRE  ,--FT_ORDRE
echancier_data.LIBELLE,--ECHE_LIBELLE
echancier_data.MONTANT  ,--ECHE_MONTANT
echancier_data.MONTANT_EN_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echancier_data.NOMBRE_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
echancier_data.ORG_ORDRE  ,--ORG_ORDRE
echancier_data.PREST_ORDRE  ,--PREST_ORDRE
echancier_data.PRISE_EN_CHARGE  ,--ECHE_PRISE_EN_CHARGE
echancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
echancier_data.SUPPRIME  ,--ECHE_SUPPRIME
2006  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
TITORDRE,
ORIORDRE,--ORI_ORDRE,
CLIENT_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
NULL,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT (ECHE_ECHEANCIER_ORDRE, RECO_ORDRE, FOU_ORDRE, 
PREL_COMMENTAIRE, PREL_DATE_MODIF, PREL_DATE_PRELEVEMENT, PREL_PRELEV_DATE_SAISIE, 
PREL_PRELEV_ETAT, PREL_NUMERO_INDEX, PREL_PRELEV_MONTANT, 
PREL_PRELEV_ORDRE, RIB_ORDRE, PREL_ETAT_MARACUJA)
SELECT
ECHEANCIER_ORDRE,--ECHE_ECHEANCIER_ORDRE
FICP_ORDRE,--PREL_FICP_ORDRE
FOU_ORDRE,--FOU_ORDRE
COMMENTAIRE,--PREL_COMMENTAIRE
DATE_MODIF,--PREL_DATE_MODIF
DATE_PRELEVEMENT,--PREL_DATE_PRELEVEMENT
PRELEV_DATE_SAISIE,--PREL_PRELEV_DATE_SAISIE
PRELEV_ETAT,--PREL_PRELEV_ETAT
NUMERO_INDEX,--PREL_NUMERO_INDEX
PRELEV_MONTANT,--PREL_PRELEV_MONTANT
PRELEV_ORDRE,--PREL_PRELEV_ORDRE
RIB_ORDRE,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM PRELEV.PRELEVEMENT
WHERE ECHEANCIER_ORDRE = echancier_data.ECHEANCIER_ORDRE;

END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
	exeordre	  INTEGER;
BEGIN

	SELECT * INTO letitre
		FROM TITRE
		WHERE tit_id = titid;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;



	SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=letitre.tit_ordre AND exe_ordre=letitre.exe_ordre;
	IF cpt = 0 THEN
--	IF letitre.prest_id IS NULL THEN

		-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
		IF jefytitre.tit_type IN ('C','D') THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;


		-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			letitre.pco_num,			   --PCO_NUM
			letitre.tit_ht,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);

		IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
			-- creation du titre_brouillard visa --
			INSERT INTO TITRE_BROUILLARD VALUES
				(
				NULL,  						   --ECD_ORDRE,
				letitre.exe_ordre,			   --EXE_ORDRE,
				letitre.ges_code,			   --GES_CODE,
				jefytitre.tit_imputtva,			   --PCO_NUM
				letitre.tit_tva,			   --TIB_MONTANT,
				'VISA TITRE',				   --TIB_OPERATION,
				titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
				sens,						   --TIB_SENS,
				titid						   --TIT_ID,
				);
		END IF;

		-- on inverse le sens
		IF sens = 'C' THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;

		-- creation du titre_brouillard contre partie --
		INSERT INTO TITRE_BROUILLARD
			SELECT
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			ges_code,			   --GES_CODE,
			pco_num,			   --PCO_NUM
			NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			FROM jefy.ventil_titre
			WHERE tit_ordre = letitre.tit_ordre;


	ELSE
		Bordereau_Titre.Set_Titre_Brouillard_intern(titid);
	END IF;

END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	leplancomptable maracuja.PLAN_COMPTABLE%ROWTYPE;
	gescodecompta TITRE.ges_code%TYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
BEGIN


	SELECT * INTO letitre
	FROM TITRE
	WHERE tit_id = titid;



	SELECT c.ges_code
		INTO gescodecompta
		FROM GESTION g, COMPTABILITE c
		WHERE g.ges_code = letitre.ges_code
		AND g.com_ordre = c.com_ordre;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

	-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
	IF jefytitre.tit_type IN ('C','D') THEN
	 sens := 'D';
	ELSE
	 sens := 'C';
	END IF;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||letitre.pco_num;

	IF cpt = 0 THEN
		SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
		WHERE pco_num = letitre.pco_num;

		maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||letitre.pco_num);
	END IF;
--	letitre.pco_num := '18'||letitre.pco_num;

	-- creation du titre_brouillard visa --
	INSERT INTO TITRE_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		letitre.ges_code,			   --GES_CODE,
		'18'||letitre.pco_num,			   --PCO_NUM
		letitre.tit_ht,			   --TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		);

	IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
	-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			jefytitre.tit_imputtva,			   --PCO_NUM
			letitre.tit_tva,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);
	END IF;

	-- on inverse le sens
	IF sens = 'C' THEN
	   sens := 'D';
	ELSE
		sens := 'C';
	END IF;

	-- creation du titre_brouillard contre partie --
	INSERT INTO TITRE_BROUILLARD
		SELECT
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			   --GES_CODE,
		'181',			   --PCO_NUM
		NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		FROM jefy.ventil_titre
		WHERE tit_ordre = letitre.tit_ordre;



END;



PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
  niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


END;
/
