
----------------------------------------------------------------------
----------------------------------------------------------------------

CREATE OR REPLACE PACKAGE bordereau_titre IS

PROCEDURE Get_Titre_Jefy ( exeordre INTEGER, borordre INTEGER, borid INTEGER, utlordre INTEGER, agtordre INTEGER );
PROCEDURE Get_Recette_Jefy(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER);
PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER);
PROCEDURE Get_Btte_Jefy(borordre NUMBER,exeordre NUMBER);
PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);


END;
/

CREATE OR REPLACE PACKAGE BODY Bordereau_Titre IS
--version 1.1.9 04/05/2006 - modif pour plusieurs ventilations
--version 1.1.8 31/05/2005 - modifs prestations internes
--version 1.1.7 26/04/2005 - prestations internes
--version 1.1.8 23/06/2005 - structure table recette
PROCEDURE Get_Btte_Jefy
(borordre NUMBER,exeordre NUMBER)

IS
	cpt 	 	INTEGER;

	lebordereau jefy.bordero%ROWTYPE;

	agtordre 	jefy.facture.agt_ordre%TYPE;
	borid 		BORDEREAU.bor_id%TYPE;
	tboordre 	BORDEREAU.tbo_ordre%TYPE;
	utlordre 	UTILISATEUR.utl_ordre%TYPE;
	ex			INTEGER;
	ex2			INTEGER;
BEGIN

	 SELECT param_value
        INTO ex
        FROM jefy.parametres
       WHERE param_key = 'EXERCICE';

	  SELECT exe_exercice INTO ex2 FROM EXERCICE WHERE exe_ordre=exeordre;
	dbms_output.put_line('exercice : '||exeordre);

	-- est ce un bordereau de titre --
	SELECT COUNT(*) INTO cpt FROM jefy.TITRE
		   WHERE bor_ordre = borordre;

	IF cpt != 0 THEN

		-- le bordereau est il deja vis? ? --
		tboordre:=Gestionorigine.recup_type_bordereau_titre(borordre, exeordre);

		SELECT COUNT(*) INTO cpt
			FROM BORDEREAU
			WHERE bor_ordre = borordre
			AND exe_ordre = exeordre
			AND tbo_ordre = tboordre;




		-- l exercice est il encore ouvert ? --


		-- pas de raise mais pas de recup s il est deja dans maracuja --
		IF cpt = 0 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
				FROM jefy.bordero
				WHERE bor_ordre = borordre;
			/*
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy.TITRE
			WHERE tit_ordre =
			 ( SELECT MAX(tit_ordre)
			   FROM jefy.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre
			  );

			-- recuperation du type de bordereau --
			SELECT COUNT(*) INTO cpt
			FROM OLD_AGENT_TYPE_BORD
			WHERE agt_ordre = agtordre;

			IF cpt <> 0 THEN
			 SELECT tbo_ordre INTO tboordre
			 FROM OLD_AGENT_TYPE_BORD
			 WHERE agt_ordre = agtordre;
			ELSE
			 SELECT tbo_ordre INTO tboordre
			 FROM TYPE_BORDEREAU
			 WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' and EXE_ORDRE=exeordre);
			END IF;
			*/

			-- creation du bor_id --
			SELECT bordereau_seq.NEXTVAL INTO borid FROM dual;

			-- recuperation de l utilisateur --
			/*select utl_ordre into utlordre
			from utilisateur
			where agt_ordre = agtordre;
			*/
			-- TEST  TEST  TEST  TEST  TEST
			SELECT 1 INTO utlordre
				   FROM dual;
			-- TEST  TEST  TEST  TEST  TEST

			-- creation du bordereau --
			INSERT INTO BORDEREAU VALUES (
				NULL , 	 	 	      --BOR_DATE_VISA,
				'VALIDE',			  --BOR_ETAT,
				borid,				  --BOR_ID,
				leBordereau.bor_num,  --BOR_NUM,
				leBordereau.bor_ordre,--BOR_ORDRE,
				exeordre,	  		  --EXE_ORDRE,
				leBordereau.ges_code, --GES_CODE,
				tboordre,			  --TBO_ORDRE,
				utlordre,		 	  --UTL_ORDRE,
				NULL				  --UTL_ORDRE_VISA
				);

			Get_Titre_Jefy(exeordre,borordre,borid,utlordre,agtordre);

		END IF;
		--else
		-- raise_application_error (-20001,'CECI N EST PAS UN BORDEREAU RECETTE');
	END IF;

END;


PROCEDURE Get_Titre_Jefy
 (
 exeordre INTEGER,
 borordre INTEGER,
 borid INTEGER,
 utlordre INTEGER,
 agtordre INTEGER
 )
IS

	jefytitre jefy.TITRE%ROWTYPE;
	gescode 	 GESTION.ges_code%TYPE;
	titid 		 TITRE.tit_id%TYPE;

	titoriginekey  TITRE.tit_ORGINE_KEY%TYPE;
	titoriginelib TITRE.tit_ORIGINE_LIB%TYPE;
	ORIORDRE 	   TITRE.ORI_ORDRE%TYPE;
	PRESTID 	   TITRE.PREST_ID%TYPE;
	TORORDRE 	   TITRE.TOR_ORDRE%TYPE;
	modordre	   TITRE.mod_ordre%TYPE;
	presid		   INTEGER;
	cpt			   INTEGER;

	CURSOR lesJefytitres IS
	SELECT *
	FROM jefy.TITRE
	WHERE bor_ordre =borordre;


BEGIN


	--boucle de traitement --
	OPEN lesJefytitres;
	LOOP
		FETCH lesJefytitres INTO jefytitre;
			  EXIT WHEN lesJefytitres%NOTFOUND;

		-- recup ??
		IF (jefytitre.conv_ordre IS NOT NULL) THEN
		  ORIORDRE :=Gestionorigine.traiter_convordre(jefytitre.conv_ordre);
		ELSE
		  ORIORDRE :=Gestionorigine.traiter_orgordre(jefytitre.org_ordre);
		END IF;

		titoriginekey  :=NULL;
		titoriginelib   :=NULL;

		-- recup du torordre origine JEFYCO
		TORORDRE 	     :=1;
		-- JEFYCO
		torordre := 1;
		-- recup du mode de paiement OP ordo
		modordre	     :=NULL;

		dbms_output.put_line('titreOrdre : '||jefytitre.tit_ordre);
		dbms_output.put_line('mod_code : '||jefytitre.mod_code);


		IF jefytitre.tit_type = 'C' THEN
		   SELECT mod_ordre INTO modordre
		   FROM MODE_PAIEMENT
		   WHERE mod_code = jefytitre.mod_code
		   AND exe_ordre = exeordre;
		ELSE
			modordre := NULL;
		END IF;



		-- recup du prestid
		presid		     :=NULL;


		--TODO des gescode sont nuls, recuperer le gescode de la table comptabilite ?
		IF jefytitre.ges_code IS NULL THEN
			 SELECT DISTINCT ges_code INTO gescode
			 FROM BORDEREAU
			 WHERE bor_id=borid;
		ELSE
		 	gescode:=jefytitre.ges_code;
		END IF;

		SELECT titre_seq.NEXTVAL INTO titid FROM dual;

		INSERT INTO TITRE (
		   BOR_ID,
		   BOR_ORDRE,
		   BRJ_ORDRE,
		   EXE_ORDRE,
		   GES_CODE,
		   MOD_ORDRE,
		   ORI_ORDRE,
		   PCO_NUM,
		   PREST_ID,
		   TIT_DATE_REMISE,
		   TIT_DATE_VISA_PRINC,
		   TIT_ETAT,
		   TIT_ETAT_REMISE,
		   TIT_HT,
		   TIT_ID,
		   TIT_MOTIF_REJET,
		   TIT_NB_PIECE,
		   TIT_NUMERO,
		   TIT_NUMERO_REJET,
		   TIT_ORDRE,
		   TIT_ORGINE_KEY,
		   TIT_ORIGINE_LIB,
		   TIT_TTC,
		   TIT_TVA,
		   TOR_ORDRE,
		   UTL_ORDRE,
		   ORG_ORDRE,
		   FOU_ORDRE,
		   MOR_ORDRE,
		   PAI_ORDRE,
		   rib_ordre_ordonnateur,
		   rib_ordre_comptable,
		   tit_libelle)
		VALUES
			(
			borid,--BOR_ID,
			borordre,--BOR_ORDRE,
			NULL,--BRJ_ORDRE,
			exeordre,--EXE_ORDRE,
			gescode,--GES_CODE,
			modordre,--MOD_ORDRE,
			oriordre,--ORI_ORDRE,
			jefytitre.pco_num,--PCO_NUM,
			jefytitre.pres_ordre,--PREST_ID,
			NULL,--TIT_DATE_REMISE,
			NULL,--TIT_DATE_VISA_PRINC,
			'ATTENTE',--TIT_ETAT,
			'ATTENTE',--TIT_ETAT_REMISE,
			NVL(En_Nombre(jefytitre.tit_mont),0),--TIT_HT,
			titid,--TIT_ID,
			NULL,--TIT_MOTIF_REJET,
			jefytitre.tit_piece,--TIT_NB_PIECE,
			jefytitre.tit_num,--TIT_NUMERO,
			NULL,--TIT_NUMERO_REJET,
			jefytitre.tit_ordre,--TIT_ORDRE,
			titoriginekey,--TIT_ORGINE_KEY,
			titoriginelib,--TIT_ORIGINE_LIB,
			NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TIT_TTC,
			NVL(En_Nombre(jefytitre.tit_monttva),0),--TIT_TVA,
			torordre,--TOR_ORDRE,
			utlordre,--UTL_ORDRE
			jefytitre.org_ordre,		--ORG_ORDRE,
			jefytitre.fou_ordre,  -- FOU_ORDRE  --TOCHECK certains sont nuls...
			modordre,				 --MOR_ORDRE
			NULL, -- VIR_ORDRE
			jefytitre.rib_ordre,
			jefytitre.rib_ordre,
			jefytitre.tit_lib
			);

		--on met a jour le type de bordereau s'il s'agit d'un bordereau de prestation interne
		SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=jefytitre.tit_ordre AND exe_ordre=exeordre;
		IF cpt != 0 THEN
--		if jefytitre.pres_ordre is not null then
		   UPDATE BORDEREAU SET tbo_ordre=11 WHERE bor_id=borid;
		END IF;

		Get_Recette_Jefy(exeordre,titid,jefytitre.tit_ordre,utlordre);
		Set_Titre_Brouillard(titid);

	END LOOP;

	CLOSE lesJefytitres;

END;




PROCEDURE Get_Recette_Jefy
(exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER)
IS

	depid 	  		 DEPENSE.dep_id%TYPE;
	jefytitre		 jefy.TITRE%ROWTYPE;
	--lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
	--fouadresse  	 DEPENSE.dep_adresse%TYPE;
	--founom  		 DEPENSE.dep_fournisseur%TYPE;
	--lotordre  		 DEPENSE.dep_lot%TYPE;
	--marordre		 DEPENSE.dep_marches%TYPE;
	--fouordre		 DEPENSE.fou_ordre%TYPE;
	--gescode			 DEPENSE.ges_code%TYPE;
	cpt				 INTEGER;
	recid			 RECETTE.rec_id%TYPE;
	rectype			 RECETTE.rec_type%TYPE;
	gescode			 TITRE.GES_CODE%TYPE;

	modordre	   DEPENSE.mod_ordre%TYPE;


BEGIN

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = titordre;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;

	rectype := NULL;

	-- ajout rod
	IF jefytitre.tit_type = 'C' THEN
	   SELECT mod_ordre INTO modordre
	   FROM MODE_PAIEMENT
	   WHERE mod_code = jefytitre.mod_code
	   AND exe_ordre = exeordre;
	ELSE
		modordre := NULL;
	END IF;


-- 	IF jefytitre.ges_code IS NULL THEN
-- 	 SELECT distinct ges_code INTO gescode
-- 	 FROM jefy.ventil_titre
-- 	 WHERE tit_ordre = jefytitre.tit_ordre;
-- 	else
-- 	 gescode:=jefytitre.ges_code;
-- 	END IF;


	 SELECT DISTINCT ges_code INTO gescode
	 FROM maracuja.TITRE
	 WHERE tit_id = titid;


	SELECT recette_seq.NEXTVAL INTO recid FROM dual;


	INSERT INTO RECETTE VALUES
		(
		exeordre,--EXE_ORDRE,
		gescode,--GES_CODE,
		jefytitre.mod_code,--MOD_CODE,
		jefytitre.pco_num,--PCO_NUM,
		jefytitre.tit_date,-- REC_DATE,
		jefytitre.tit_debiteur,-- REC_DEBITEUR,
		recid,-- REC_ID,
		jefytitre.pco_num,-- REC_IMPUTTVA,
		jefytitre.tit_interne,-- REC_INTERNE,
		jefytitre.tit_lib,-- REC_LIBELLE,
		jefytitre.org_ordre,-- REC_LIGNE_BUDGETAIRE,
		'E',-- REC_MONNAIE,
		NVL(En_Nombre(jefytitre.tit_mont),0),--HT,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--TTC,
		NVL(En_Nombre(jefytitre.tit_mont)+En_Nombre(jefytitre.tit_monttva),0),--DISQUETTE,
		NVL(En_Nombre(jefytitre.tit_monttva),0),--   REC_MONTTVA,
		jefytitre.tit_num,--   REC_NUM,
		jefytitre.tit_ordre,--   REC_ORDRE,
		jefytitre.tit_piece,--   REC_PIECE,
		jefytitre.tit_ref,--   REC_REF,
		'VALIDE',--   REC_STAT,
		'NON',--    REC_SUPPRESSION,  Modif Rod
		jefytitre.tit_type,--	 REC_TYPE,
		NULL,--	 REC_VIREMENT,
		titid,--	  TIT_ID,
		titordre,--	  TIT_ORDRE,
		utlordre,--	   UTL_ORDRE
		jefytitre.org_ordre,		--	   ORG_ORDRE --ajout rod
		jefytitre.fou_ordre,   --FOU_ORDRE --ajout rod
		modordre, --mod_ordre
		NULL,  --mor_ordre
		jefytitre.rib_ordre,
		NULL
		);

-- recuperation des echeanciers
Bordereau_Titre.Get_recette_prelevements (exeordre ,titid ,titordre ,utlordre,recid );


END;



PROCEDURE Get_recette_prelevements (exeordre INTEGER,titid INTEGER,titordre INTEGER,utlordre INTEGER,recid INTEGER)
IS
cpt INTEGER;
FACTURE_TITRE_data PRESTATION.FACTURE_titre%ROWTYPE;
echancier_data PRELEV.ECHEANCIER%ROWTYPE;
CLIENT_data PRELEV.client%ROWTYPE;
personne_data grhum.v_personne%ROWTYPE;
ORIORDRE INTEGER;
modordre INTEGER;

BEGIN

SELECT COUNT(*) INTO cpt FROM dual;
-- recup du facture_titre pour le titre concerne
SELECT * INTO FACTURE_TITRE_data
FROM PRESTATION.FACTURE_titre
WHERE tit_ordre = TITORDRE
AND EXERCICE = exeordre;

dbms_output.put_line ('titordre='||TITORDRE);


SELECT COUNT(*) INTO cpt FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';
IF (cpt=0) THEN
	  RETURN;
END IF;


SELECT * INTO echancier_data
FROM PRELEV.ECHEANCIER
WHERE ft_ordre = FACTURE_TITRE_data.ft_ordre
AND supprime = 'N';





SELECT * INTO CLIENT_data
FROM PRELEV.CLIENT
WHERE CLIENT_ORDRE = echancier_data.client_ordre
AND supprime = 'N';




-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
   SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
   IF (cpt=0) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
   END IF;
   IF (cpt>1) THEN
   	  RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
   END IF;

   SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

   UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;



-- recup ??
IF (echancier_data.con_ordre IS NOT NULL) THEN
 ORIORDRE :=Gestionorigine.traiter_convordre(echancier_data.con_ordre);
ELSE
 ORIORDRE :=Gestionorigine.traiter_orgordre(echancier_data.org_ordre);
END IF;

SELECT * INTO personne_data
FROM GRHUM.V_PERSONNE WHERE pers_id = CLIENT_data.pers_id;

INSERT INTO MARACUJA.ECHEANCIER VALUES
(
echancier_data.AUTORIS_SIGNEE  ,--ECHE_AUTORIS_SIGNEE
echancier_data.CLIENT_ORDRE  ,--FOU_ORDRE_CLIENT
echancier_data.CON_ORDRE  ,--CON_ORDRE
echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
echancier_data.FT_ORDRE  ,--FT_ORDRE
echancier_data.LIBELLE,--ECHE_LIBELLE
echancier_data.MONTANT  ,--ECHE_MONTANT
echancier_data.MONTANT_EN_LETTRES  ,--ECHE_MONTANT_EN_LETTRES
echancier_data.NOMBRE_ECHEANCES  ,--ECHE_NOMBRE_ECHEANCES
echancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
echancier_data.ORG_ORDRE  ,--ORG_ORDRE
echancier_data.PREST_ORDRE  ,--PREST_ORDRE
echancier_data.PRISE_EN_CHARGE  ,--ECHE_PRISE_EN_CHARGE
echancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
echancier_data.SUPPRIME  ,--ECHE_SUPPRIME
2006  ,--EXE_ORDRE
TITID,
recid ,--REC_ID,
TITORDRE,
ORIORDRE,--ORI_ORDRE,
CLIENT_data.pers_id, --CLIENT_data.pers_id  ,--PERS_ID
NULL,--orgid a faire plus tard....
personne_data.PERS_LIBELLE --    PERS_DESCRIPTION
);


INSERT INTO MARACUJA.PRELEVEMENT
SELECT
ECHEANCIER_ORDRE,--ECHE_ECHEANCIER_ORDRE
FICP_ORDRE,--PREL_FICP_ORDRE
FOU_ORDRE,--FOU_ORDRE
COMMENTAIRE,--PREL_COMMENTAIRE
DATE_MODIF,--PREL_DATE_MODIF
DATE_PRELEVEMENT,--PREL_DATE_PRELEVEMENT
PRELEV_DATE_SAISIE,--PREL_PRELEV_DATE_SAISIE
PRELEV_ETAT,--PREL_PRELEV_ETAT
NUMERO_INDEX,--PREL_NUMERO_INDEX
PRELEV_MONTANT,--PREL_PRELEV_MONTANT
PRELEV_ORDRE,--PREL_PRELEV_ORDRE
RIB_ORDRE,--RIB_ORDRE
'ATTENTE'--PREL_ETAT_MARACUJA
FROM PRELEV.PRELEVEMENT
WHERE ECHEANCIER_ORDRE = echancier_data.ECHEANCIER_ORDRE;

END;



PROCEDURE Set_Titre_Brouillard(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
	exeordre	  INTEGER;
BEGIN

	SELECT * INTO letitre
		FROM TITRE
		WHERE tit_id = titid;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;



	SELECT COUNT(*) INTO cpt FROM v_titre_prest_interne WHERE tit_ordre=letitre.tit_ordre AND exe_ordre=letitre.exe_ordre;
	IF cpt = 0 THEN
--	IF letitre.prest_id IS NULL THEN

		-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
		IF jefytitre.tit_type IN ('C','D') THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;


		-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			letitre.pco_num,			   --PCO_NUM
			letitre.tit_ht,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);

		IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
			-- creation du titre_brouillard visa --
			INSERT INTO TITRE_BROUILLARD VALUES
				(
				NULL,  						   --ECD_ORDRE,
				letitre.exe_ordre,			   --EXE_ORDRE,
				letitre.ges_code,			   --GES_CODE,
				jefytitre.tit_imputtva,			   --PCO_NUM
				letitre.tit_tva,			   --TIB_MONTANT,
				'VISA TITRE',				   --TIB_OPERATION,
				titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
				sens,						   --TIB_SENS,
				titid						   --TIT_ID,
				);
		END IF;

		-- on inverse le sens
		IF sens = 'C' THEN
		 sens := 'D';
		ELSE
		 sens := 'C';
		END IF;

		-- creation du titre_brouillard contre partie --
		INSERT INTO TITRE_BROUILLARD
			SELECT
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			ges_code,			   --GES_CODE,
			pco_num,			   --PCO_NUM
			NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			FROM jefy.ventil_titre
			WHERE tit_ordre = letitre.tit_ordre;


	ELSE
		Bordereau_Titre.Set_Titre_Brouillard_intern(titid);
	END IF;

END;


PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER)
IS

	letitre  	  TITRE%ROWTYPE;
	jefytitre	  jefy.TITRE%ROWTYPE;
	leplancomptable maracuja.PLAN_COMPTABLE%ROWTYPE;
	gescodecompta TITRE.ges_code%TYPE;
	sens		  VARCHAR2(2);
	cpt			  INTEGER;
BEGIN


	SELECT * INTO letitre
	FROM TITRE
	WHERE tit_id = titid;



	SELECT c.ges_code
		INTO gescodecompta
		FROM GESTION g, COMPTABILITE c
		WHERE g.ges_code = letitre.ges_code
		AND g.com_ordre = c.com_ordre;

	SELECT * INTO jefytitre
		FROM jefy.TITRE
		WHERE tit_ordre = letitre.tit_ordre;

	-- si OP ou reduction alors debit 4 sinon credit 7 ou credit 1
	IF jefytitre.tit_type IN ('C','D') THEN
	 sens := 'D';
	ELSE
	 sens := 'C';
	END IF;

	-- verification si le compte existe !
	SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
		   WHERE pco_num = '18'||letitre.pco_num;

	IF cpt = 0 THEN
		SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
		WHERE pco_num = letitre.pco_num;

		maj_plancomptable (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||letitre.pco_num);
	END IF;
--	letitre.pco_num := '18'||letitre.pco_num;

	-- creation du titre_brouillard visa --
	INSERT INTO TITRE_BROUILLARD VALUES
		(
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		letitre.ges_code,			   --GES_CODE,
		'18'||letitre.pco_num,			   --PCO_NUM
		letitre.tit_ht,			   --TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		);

	IF letitre.tit_tva IS NOT NULL AND letitre.tit_tva != 0 THEN
	-- creation du titre_brouillard visa --
		INSERT INTO TITRE_BROUILLARD VALUES
			(
			NULL,  						   --ECD_ORDRE,
			letitre.exe_ordre,			   --EXE_ORDRE,
			letitre.ges_code,			   --GES_CODE,
			jefytitre.tit_imputtva,			   --PCO_NUM
			letitre.tit_tva,			   --TIB_MONTANT,
			'VISA TITRE',				   --TIB_OPERATION,
			titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
			sens,						   --TIB_SENS,
			titid						   --TIT_ID,
			);
	END IF;

	-- on inverse le sens
	IF sens = 'C' THEN
	   sens := 'D';
	ELSE
		sens := 'C';
	END IF;

	-- creation du titre_brouillard contre partie --
	INSERT INTO TITRE_BROUILLARD
		SELECT
		NULL,  						   --ECD_ORDRE,
		letitre.exe_ordre,			   --EXE_ORDRE,
		gescodecompta,			   --GES_CODE,
		'181',			   --PCO_NUM
		NVL(En_Nombre(ven_mont),0),---TIB_MONTANT,
		'VISA TITRE',				   --TIB_OPERATION,
		titre_brouillard_seq.NEXTVAL, --TIB_ORDRE,
		sens,						   --TIB_SENS,
		titid						   --TIT_ID,
		FROM jefy.ventil_titre
		WHERE tit_ordre = letitre.tit_ordre;



END;



PROCEDURE maj_plancomptable (nature VARCHAR,libelle VARCHAR,pconum VARCHAR)
IS
  niv INTEGER;
BEGIN
	--calcul du niveau
	SELECT LENGTH(pconum) INTO niv FROM dual;

	--
	INSERT INTO PLAN_COMPTABLE (PCO_BUDGETAIRE, PCO_EMARGEMENT, PCO_LIBELLE, PCO_NATURE, PCO_NIVEAU, PCO_NUM, PCO_SENS_EMARGEMENT, PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, PCO_J_BE)
		VALUES
		(
		'N',--PCO_BUDGETAIRE,
		'O',--PCO_EMARGEMENT,
		libelle,--PCO_LIBELLE,
		nature,--PCO_NATURE,
		niv,--PCO_NIVEAU,
		pconum,--PCO_NUM,
		2,--PCO_SENS_EMARGEMENT,
		'VALIDE',--PCO_VALIDITE,
		'O',--PCO_J_EXERCICE,
		'N',--PCO_J_FIN_EXERCICE,
		'N'--PCO_J_BE
		);
END;


END;
/



----------------------------------------------------------------------
----------------------------------------------------------------------

CREATE OR REPLACE PACKAGE Numerotationobject IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (ecrordre INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/



CREATE OR REPLACE PACKAGE BODY Numerotationobject IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE BROUILLARD INEXISTANTE , CLE '||ecrordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero_brouillard = lenumero
WHERE ecr_ordre = ecrordre;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM PAIEMENT
WHERE pai_ordre = paiordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='VIREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE PAIEMENT SET pai_numero = lenumero
WHERE pai_ordre = paiordre;

END;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_ordre = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
--select com_ordre,exe_ordre
SELECT NULL,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_ordre = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
--update retenue set ret_numero = lenumero
--where ret_ordre = retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

--COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

IF gescode IS NULL THEN
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code IS NULL
AND tnu_ordre = tnuordre;
ELSE
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code = gescode
AND tnu_ordre = tnuordre;
END IF;

IF cpt  = 0 THEN
--raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
 SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


 INSERT INTO NUMEROTATION
 ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
 VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
 RETURN 1;
ELSE
 IF gescode IS NOT NULL THEN
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;
 ELSE
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;
 END IF;
 RETURN cpt;
END IF;
END ;

END;
/



----------------------------------------------------------------------
----------------------------------------------------------------------


CREATE OR REPLACE PACKAGE Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;
END;
/

CREATE OR REPLACE PACKAGE BODY Afaireaprestraitement IS

-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy

PROCEDURE apres_visa_bordereau (borid INTEGER)
IS

  tbotype  TYPE_BORDEREAU.tbo_type%TYPE;
  cpt INTEGER;
  lebordereau BORDEREAU%ROWTYPE;
  brjordre MANDAT.brj_ordre%TYPE;
  exeordre MANDAT.exe_ordre%TYPE;
  nbmandats INTEGER;
  nbtitres INTEGER;
  ex INTEGER;


  CURSOR c1 IS SELECT DISTINCT brj_ordre,exe_ordre FROM MANDAT WHERE bor_id=borid AND brj_ordre IS NOT NULL;
  CURSOR c2 IS SELECT DISTINCT brj_ordre,exe_ordre FROM TITRE WHERE bor_id=borid AND brj_ordre IS NOT NULL;

BEGIN
  SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

  SELECT * INTO lebordereau FROM BORDEREAU WHERE bor_id=borid;


  --raise_application_error (-20001, ''||ex|| '=' ||lebordereau.exe_ordre);


  IF lebordereau.exe_ordre=ex THEN

		  -- mettre a jour le bordereau de jefy
		  UPDATE jefy.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
		    WHERE bor_ordre=lebordereau.bor_ordre;



			SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

			IF (nbmandats > 0) THEN
			    -- mettre a jour les mandats VISE
			    UPDATE jefy.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

				-- expedier le bordereau de rejet de mandats
			  	OPEN C1;
			    LOOP
			      FETCH C1 INTO brjordre,exeordre;
			      EXIT WHEN c1%NOTFOUND;
			      Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C1;
			END IF;



			SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

			IF (nbtitres > 0) THEN
			    -- mettre a jour les titres VISE
			    UPDATE jefy.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

			    -- expedier le bordereau de rejet de titres
			  	OPEN C2;
			    LOOP
			      FETCH C2 INTO brjordre,exeordre;
			      EXIT WHEN c2%NOTFOUND;
			      Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C2;
			END IF;


			emarger_visa_bord_prelevement(borid);


  ELSE
  	  IF lebordereau.exe_ordre=2005 THEN

		  -- mettre a jour le bordereau de jefy
		  UPDATE jefy05.bordero SET bor_stat='V', bor_visa=lebordereau.bor_date_visa
		    WHERE bor_ordre=lebordereau.bor_ordre;



			SELECT COUNT(*) INTO nbmandats FROM MANDAT WHERE bor_id=lebordereau.bor_id;

			IF (nbmandats > 0) THEN
			    -- mettre a jour les mandats VISE
			    UPDATE jefy05.MANDAT SET man_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

				-- expedier le bordereau de rejet de mandats
			  	OPEN C1;
			    LOOP
			      FETCH C1 INTO brjordre,exeordre;
			      EXIT WHEN c1%NOTFOUND;
			      Bordereau_Mandat_Non_Admis.expedier_btmna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C1;
			END IF;



			SELECT COUNT(*) INTO nbtitres FROM TITRE WHERE bor_id=lebordereau.bor_id;

			IF (nbtitres > 0) THEN
			    -- mettre a jour les titres VISE
			    UPDATE jefy05.TITRE SET tit_stat='V' WHERE bor_ordre=lebordereau.bor_ordre;

			    -- expedier le bordereau de rejet de titres
			  	OPEN C2;
			    LOOP
			      FETCH C2 INTO brjordre,exeordre;
			      EXIT WHEN c2%NOTFOUND;
			      Bordereau_Titre_Non_Admis.expedier_bttna(brjordre,exeordre);
			    END LOOP;
			    CLOSE C2;
			END IF;
		 ELSE
		 	 RAISE_APPLICATION_ERROR (-20001, 'Impossible de determiner le user jefy a utiliser');
	  END IF;
  END IF;

END ;


PROCEDURE apres_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
manid INTEGER;
titid INTEGER;
manordre INTEGER;
titordre INTEGER;
pconumnouveau INTEGER;
depordre INTEGER;
exeordre INTEGER;
ex INTEGER;
BEGIN

	 SELECT param_value INTO ex FROM jefy.parametres WHERE param_key='EXERCICE';

	-- mettre a jour le mandat ou le titre dans JEFY
	SELECT man_id, tit_id, pco_num_nouveau INTO manid, titid, pconumnouveau FROM REIMPUTATION WHERE rei_ordre=reiordre;

	IF manid IS NOT NULL THEN
	   SELECT man_ordre, MANDAT.exe_ordre INTO manordre, exeordre FROM MANDAT, BORDEREAU b WHERE man_id = manid AND MANDAT.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTME','BTMS','BTPI'));

	   IF exeordre=ex THEN
		   UPDATE jefy.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
		   UPDATE jefy.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
	   ELSE IF exeordre=2005 THEN
			   UPDATE jefy05.MANDAT SET pco_num=pconumnouveau WHERE man_ordre = manordre;
			   UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE man_ordre = manordre;
		    END IF;
	   END IF;
	END IF;


	IF titid IS NOT NULL THEN
	   SELECT tit_ordre, TITRE.exe_ordre INTO titordre, exeordre FROM TITRE, BORDEREAU b WHERE tit_id = titid  AND TITRE.bor_id=b.bor_id AND b.tbo_ordre IN (SELECT tbo_ordre FROM maracuja.TYPE_BORDEREAU WHERE tbo_type IN ('BTTE','BTPI'));


	   IF exeordre=ex THEN
		   UPDATE jefy.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
		   -- pour les ORV, des fois que...
		   SELECT dep_ordre INTO depordre FROM jefy.TITRE WHERE tit_ordre=titordre;
		   IF (depordre IS NOT NULL ) THEN
		   	  UPDATE jefy.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
		   END IF;

	   ELSE IF exeordre=2005 THEN
		   UPDATE jefy05.TITRE SET pco_num=pconumnouveau WHERE tit_ordre = titordre ;
		   -- pour les ORV, des fois que...
		   SELECT dep_ordre INTO depordre FROM jefy05.TITRE WHERE tit_ordre=titordre;
		   IF (depordre IS NOT NULL ) THEN
		   	  UPDATE jefy05.facture SET pco_num=pconumnouveau WHERE dep_ordre=depordre;
		   END IF;

		    END IF;
	   END IF;

	END IF;

END ;

PROCEDURE apres_paiement (paiordre INTEGER)
IS
cpt INTEGER;
BEGIN
	 		--SELECT 1 INTO  cpt FROM dual;
		 emarger_paiement(paiordre);

END ;


PROCEDURE emarger_paiement(paiordre INTEGER)
IS

CURSOR mandats (lepaiordre INTEGER ) IS
 SELECT * FROM MANDAT
 WHERE pai_ordre = lepaiordre;

CURSOR non_emarge_debit (lemanid INTEGER) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='D';

CURSOR non_emarge_credit_compte (lemanid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM MANDAT_DETAIL_ECRITURE m , ECRITURE_DETAIL e
 WHERE man_id = lemanid
 AND e.ecd_ordre = m.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='C';

lemandat maracuja.MANDAT%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lepaiement maracuja.PAIEMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lepaiement FROM PAIEMENT
 WHERE pai_ordre = paiordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lepaiement.exe_ordre,
  3,
  lepaiement.UTL_ORDRE,
  lepaiement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les mandats du paiement
OPEN mandats(paiordre);
LOOP
FETCH mandats INTO lemandat;
EXIT WHEN mandats%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_debit (lemandat.man_id);
 LOOP
 FETCH non_emarge_debit INTO ecriture_debit;
 EXIT WHEN non_emarge_debit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_credit_compte (lemandat.man_id,ecriture_debit.pco_num);
 LOOP
 FETCH non_emarge_credit_compte INTO ecriture_credit;
 EXIT WHEN non_emarge_credit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_debit.ecd_ordre,
  ecriture_credit.ecd_ordre,
  EMAORDRE,
  ecriture_credit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  lemandat.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_credit_compte;

END LOOP;
CLOSE non_emarge_debit;

END LOOP;
CLOSE mandats;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;




PROCEDURE apres_recouvrement_releve(recoordre INTEGER)
IS
BEGIN
		 emarger_prelevement_releve(recoordre);
END ;

PROCEDURE emarger_prelevement_releve(recoordre INTEGER)
IS
cpt INTEGER;
BEGIN
	 -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
	 		SELECT 1 INTO  cpt FROM dual;
                        emarger_prelevement(recoordre);
END;


PROCEDURE apres_recouvrement (recoordre INTEGER)
IS
BEGIN
		 emarger_prelevement(recoordre);

END ;

PROCEDURE emarger_prelevement(recoordre INTEGER)
IS

CURSOR titres (lerecoordre INTEGER ) IS
 SELECT * FROM TITRE
 WHERE tit_id IN
 (SELECT tit_id FROM PRELEVEMENT p , ECHEANCIER e
 WHERE e.eche_echeancier_ordre  = p.eche_echeancier_ordre
 AND p.reco_ordre = lerecoordre)
 ;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lerecouvrement maracuja.RECOUVREMENT%ROWTYPE;
cpt INTEGER;

BEGIN

-- recup infos
 SELECT * INTO lerecouvrement FROM RECOUVREMENT
 WHERE reco_ordre = recoordre;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lerecouvrement.exe_ordre,
  3,
  lerecouvrement.UTL_ORDRE,
  lerecouvrement.COM_ORDRE,
  0,
  'VALIDE'
  );

-- on fetch les titres du recrouvement
OPEN titres(recoordre);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;


PROCEDURE emarger_visa_bord_prelevement(borid INTEGER)
IS


CURSOR titres (leborid INTEGER ) IS
 SELECT * FROM TITRE
 WHERE bor_id = leborid;

CURSOR non_emarge_credit (letitid INTEGER) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND e.ecd_sens ='C';

CURSOR non_emarge_debit_compte (letitid INTEGER,lepconum VARCHAR) IS
 SELECT e.* FROM TITRE_DETAIL_ECRITURE t , ECRITURE_DETAIL e
 WHERE tit_id = letitid
 AND e.ecd_ordre = t.ecd_ordre
 AND ecd_reste_emarger != 0
 AND pco_num = lepconum
 AND e.ecd_sens ='D';

letitre maracuja.TITRE%ROWTYPE;
ecriture_debit maracuja.ECRITURE_DETAIL%ROWTYPE;
ecriture_credit maracuja.ECRITURE_DETAIL%ROWTYPE;
EMAORDRE EMARGEMENT.ema_ordre%TYPE;
lebordereau maracuja.BORDEREAU%ROWTYPE;
cpt INTEGER;
comordre INTEGER;
BEGIN

-- recup infos
 SELECT * INTO lebordereau FROM BORDEREAU
 WHERE bor_id = borid;
-- recup du com_ordre
SELECT com_ordre  INTO comordre
FROM GESTION WHERE ges_ordre = lebordereau.ges_code;

-- creation de l emargement !
 SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

 INSERT INTO EMARGEMENT
  (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
 VALUES
  (
  SYSDATE,
  -1,
  EMAORDRE,
  lebordereau.exe_ordre,
  3,
  lebordereau.UTL_ORDRE,
  comordre,
  0,
  'VALIDE'
  );

-- on fetch les titres du recouvrement
OPEN titres(borid);
LOOP
FETCH titres INTO letitre;
EXIT WHEN titres%NOTFOUND;
-- on recupere les ecritures non emargees debits
 OPEN non_emarge_credit (letitre.tit_id);
 LOOP
 FETCH non_emarge_credit INTO ecriture_credit;
 EXIT WHEN non_emarge_credit%NOTFOUND;

-- on recupere les ecritures non emargees credit
 OPEN non_emarge_debit_compte (letitre.tit_id,ecriture_credit.pco_num);
 LOOP
 FETCH non_emarge_debit_compte INTO ecriture_debit;
 EXIT WHEN non_emarge_debit_compte%NOTFOUND;

 IF (Afaireaprestraitement.verifier_emar_exercice(ecriture_credit.ecr_ordre,ecriture_debit.ecr_ordre) = 1)
 THEN
  -- creation de l emargement detail !
  INSERT INTO EMARGEMENT_DETAIL
  (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
  VALUES
  (
  ecriture_credit.ecd_ordre,
  ecriture_debit.ecd_ordre,
  EMAORDRE,
  ecriture_debit.ecd_reste_emarger,
  emargement_detail_seq.NEXTVAL,
  letitre.exe_ordre
  );

  -- maj de l ecriture debit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_debit.ecd_ordre;

  -- maj de lecriture credit
  UPDATE ECRITURE_DETAIL SET
  ecd_reste_emarger = ecd_reste_emarger-ecriture_credit.ecd_reste_emarger
  WHERE ecd_ordre = ecriture_credit.ecd_ordre;
 END IF;

 END LOOP;
 CLOSE non_emarge_debit_compte;

END LOOP;
CLOSE non_emarge_credit;

END LOOP;
CLOSE titres;
-- suppression de lemagenet si pas de details;
SELECT COUNT(*) INTO cpt FROM EMARGEMENT_DETAIL
WHERE ema_ordre = EMAORDRE;

IF cpt = 0 THEN
DELETE FROM EMARGEMENT WHERE ema_ordre =EMAORDRE;
END IF;

END;


FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER
IS
reponse INTEGER;
execredit EXERCICE.EXE_ORDRE%TYPE;
exedebit EXERCICE.EXE_ORDRE%TYPE;
BEGIN
-- init
reponse :=0;

SELECT exe_ordre INTO execredit FROM ECRITURE
WHERE ecr_ordre = ecrcredit;

SELECT exe_ordre INTO exedebit FROM ECRITURE
WHERE ecr_ordre = ecrdebit;

IF exedebit = execredit THEN
 RETURN 1;
ELSE
 RETURN 0;
END IF;


END;


END;
/
