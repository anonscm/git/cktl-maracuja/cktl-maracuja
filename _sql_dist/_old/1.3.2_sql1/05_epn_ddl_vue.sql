--prise en compte des PI
CREATE OR REPLACE FORCE VIEW EPN.EPN_REC_BUD
(EXE_ORDRE, PCO_NUM, GES_CODE, EPN_DATE, MONT_REC, 
 ANNUL_TIT_REC)
AS 
SELECT t.exe_ordre, t.pco_num, b.ges_code, b.BOR_date_VISA, sum(t.TIT_ht), 0
FROM maracuja.TITRE t, maracuja.BORDEREAU b
--  titres de recette
WHERE (b.TBO_ORDRE = 7 or b.tbo_ordre=11) and b.BOR_ETAT = 'VISE' and tit_etat = 'VISE'
AND t.bor_id = b.bor_id
GROUP BY t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa
UNION ALL
-- r�duction de recette
SELECT t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa, 0, sum(t.TIT_ht)
FROM   maracuja.TITRE t, maracuja.BORDEREAU b
WHERE b.tbo_ordre = 9 AND b.BOR_ETAT = 'VISE' and tit_etat = 'VISE'
AND t.bor_ordre = b.bor_ordre
GROUP BY t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa;


