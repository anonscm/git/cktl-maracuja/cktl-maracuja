SET define OFF
SET scan OFF


CREATE OR REPLACE FORCE VIEW MARACUJA.ZTMP_ORIGINE_CORRECT
(ORI_ENTITE, ORI_KEY_NAME, ORI_KEY_ENTITE,ori_libelle,ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
 ORI_ENTITE_NEW, ORI_KEY_NAME_NEW, ORI_KEY_ENTITE_NEW)
AS 
SELECT ori_entite, ori_key_name, ori_key_entite, ori_libelle,ori_ordre_old, ori_ordre_new , ori_entite_new, ori_key_name_new, ori_key_entite_new 
FROM
(
		SELECT  o.ori_entite, o.ori_key_name, o.ori_key_entite, o.ori_ordre AS ori_ordre_old, o.ori_libelle,  x.ori_ordre_new , x.ori_entite_new, x.ori_key_name_new, x.ori_key_entite_new
		FROM ORIGINE o,
		(
			SELECT ori_entite ori_entite_new, ori_key_name ori_key_name_new, ori_key_entite ori_key_entite_new, MIN(ori_ordre) AS ori_ordre_new
			FROM ORIGINE 
			GROUP BY ori_entite, ori_key_name, ori_key_entite
		) x
		WHERE  
		o.ori_entite =  x.ori_entite_new
		AND o.ori_key_name = x.ori_key_name_new 
		AND o.ori_key_entite = x.ori_key_entite_new
)
WHERE ori_ordre_old<>ori_ordre_new;


CREATE TABLE MARACUJA.Z_LOG_ORIGINE_CORRECT
(
  ZLOC_ID          NUMBER                       NOT NULL,
  ENTITE_CORRIGE   VARCHAR2(50)                 NOT NULL,
  ENTITE_KEY_NAME  VARCHAR2(49)                 NOT NULL,
  ENTITE_KEY       NUMBER                       NOT NULL,
  ORI_ORDRE_OLD    NUMBER                       NOT NULL,
  ORI_ORDRE_NEW    NUMBER                       NOT NULL,
  ORI_ENTITE       VARCHAR2(50)                 NOT NULL,
  ORI_KEY_NAME     VARCHAR2(50)                 NOT NULL,
  ORI_ENTITE_KEY   NUMBER                       NOT NULL,
  ORI_LIBELLE      VARCHAR2(2000)               NOT NULL
);


ALTER TABLE MARACUJA.Z_LOG_ORIGINE_CORRECT ADD (constraint pk_Z_LOG_ORIGINE_CORRECT PRIMARY KEY  (zloc_id) USING INDEX TABLESPACE GFC_INDX );

CREATE SEQUENCE MARACUJA.Z_LOG_ORIGINE_CORRECT_seq
START WITH 1
INCREMENT BY 1
MINVALUE 0
NOCACHE 
NOCYCLE 
NOORDER; 



-------------------------------------------
-------------------------------------------
-------------------------------------------



CREATE OR REPLACE PROCEDURE MARACUJA.Corrige_Origines_2007
IS
  CURSOR c_ecritures IS
  		 SELECT ecr_ordre, ori_ordre FROM ECRITURE t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;

  CURSOR c_titres IS
  		 SELECT tit_id, ori_ordre FROM TITRE t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;

  CURSOR c_mandats IS
		 SELECT man_id, ori_ordre FROM mandat t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;
		 
  CURSOR c_mdes IS
		 SELECT mde_ordre, ori_ordre FROM mandat_detail_ecriture t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;
		 
  CURSOR c_tdes IS
		 SELECT tde_ordre, ori_ordre FROM titre_detail_ecriture t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;
		 
  CURSOR c_eches IS
		 SELECT ECHE_ECHEANCIER_ORDRE, ori_ordre FROM echeancier t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;
		 
  CURSOR c_ops IS
		 SELECT odp_ORDRE, ori_ordre FROM ordre_de_paiement t, ztmp_origine_correct z  WHERE t.ori_ordre=z.ori_ordre_old;
		 
		 		 
	originerec 	ztmp_origine_correct%ROWTYPE;	 
	flag INTEGER;
	ecrordre ecriture.ecr_ordre%type;
	titid titre.tit_id%type;
	manid mandat.man_id%type;
	mdeordre mandat_detail_ecriture.mde_ordre%type;
	tdeordre titre_detail_ecriture.tde_ordre%type;
	echecheancierordre ECHEANCIER.ECHE_ECHEANCIER_ORDRE%TYPE;
	odpordre ordre_de_paiement.odp_ordre%type;
	
	oriordre INTEGER;
BEGIN

		OPEN c_titres;
		LOOP
			FETCH c_titres INTO titId, oriordre;
				  EXIT WHEN c_titres%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'TITRE',
				 	   'TIT_ID',
				 	   titid,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE TITRE SET ori_ordre = originerec.ori_ordre_new WHERE tit_id=titid;
			   COMMIT;
		END LOOP;
		CLOSE c_titres;

		OPEN c_eches;
		LOOP
			FETCH c_eches INTO echecheancierordre, oriordre;
				  EXIT WHEN c_eches%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'ECHEANCIER',
				 	   'ECHE_ECHEANCIER_ORDRE',
				 	   echecheancierordre,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE echeancier SET ori_ordre = originerec.ori_ordre_new WHERE ECHE_ECHEANCIER_ORDRE=echecheancierordre;
			   COMMIT;
		END LOOP;
		CLOSE c_eches;
		
		OPEN c_tdes;
		LOOP
			FETCH c_tdes INTO tdeordre, oriordre;
				  EXIT WHEN c_tdes%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'TITRE_DETAIL_ECRITURE',
				 	   'TDE_ORDRE',
				 	   tdeordre,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE TITRE_detail_ecriture SET ori_ordre = originerec.ori_ordre_new WHERE tde_ordre = tdeordre;
			   COMMIT;
		END LOOP;
		CLOSE c_tdes;
		
		
		OPEN c_mandats;
		LOOP
			FETCH c_mandats INTO manid, oriordre;
				  EXIT WHEN c_mandats%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'MANDAT',
				 	   'MAN_ID',
				 	   manid,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE mandat SET ori_ordre = originerec.ori_ordre_new WHERE man_id = manid;
			   COMMIT;
		END LOOP;
		CLOSE c_mandats;			
		
		
		OPEN c_mdes;
		LOOP
			FETCH c_mdes INTO mdeordre, oriordre;
				  EXIT WHEN c_mdes%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'MANDAT_DETAIL_ECRITURE',
				 	   'MDE_ORDRE',
				 	   mdeordre,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE mandat_detail_ecriture SET ori_ordre = originerec.ori_ordre_new WHERE mde_ordre = mdeordre;
			   COMMIT;
		END LOOP;
		CLOSE c_mdes;		
		
	
		OPEN c_ops;
		LOOP
			FETCH c_ops INTO odpordre, oriordre;
				  EXIT WHEN c_ops%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'ORDRE_DE_PAIEMENT',
				 	   'ODP_ORDRE',
				 	   odpordre,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE ordre_de_paiement SET ori_ordre = originerec.ori_ordre_new WHERE odp_ordre = odpordre;
			   COMMIT;
		END LOOP;
		CLOSE c_ops;			
		
		OPEN c_ecritures;
		LOOP
			FETCH c_ecritures INTO ecrordre, oriordre;
				  EXIT WHEN c_ecritures%NOTFOUND;
			   SELECT * INTO originerec FROM ztmp_origine_correct WHERE ori_ordre_old=oriordre;
				INSERT INTO MARACUJA.Z_LOG_ORIGINE_CORRECT (
				   ZLOC_ID, ENTITE_CORRIGE, ENTITE_KEY_NAME, 
				   ENTITE_KEY, ORI_ORDRE_OLD, ORI_ORDRE_NEW, 
				   ORI_ENTITE, ORI_KEY_NAME, ORI_ENTITE_KEY, 
				   ORI_LIBELLE) 
				VALUES (Z_LOG_ORIGINE_CORRECT_seq.NEXTVAL , 
					   'ECRITURE',
				 	   'ECR_ORDRE',
				 	   ecrordre,
				  	   oriordre, 
					  originerec.ori_ordre_new ,
				      originerec.ORI_ENTITE, 
					  originerec.ORI_KEY_NAME, 
					  originerec.ORI_KEY_ENTITE, 
					  originerec.ori_libelle);
			   
			   UPDATE ecriture SET ori_ordre = originerec.ori_ordre_new WHERE ecr_ordre = ecrordre;
			   COMMIT;
		END LOOP;
		CLOSE c_ecritures;				
		
		
		
END;
/






