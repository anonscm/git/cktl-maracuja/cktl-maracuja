SET define OFF
SET scan OFF

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), nature, bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi) 
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_param_editions bpe, 
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o 
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre 
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre 
and bsn.pco_num like bpe.chapitre||'%' and budgetaire = 'O' and num_section = 1 
and bdsa_date_validation is not null 
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), nature, bsn.bdsa_id, bdsa_date_validation 
union all 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), nature, bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi) 
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_param_editions bpe, 
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o 
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre 
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre 
and bsn.pco_num like bpe.chapitre||'%' and budgetaire = 'O' and num_section = 3 
and bdsa_date_validation is not null 
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), nature, bsn.bdsa_id, bdsa_date_validation 
union all 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy.budnat b, jefy.exer e, jefy.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy.budnat b, jefy.exer e, jefy.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq) 
FROM jefy.budnat b, jefy.organ o 
WHERE b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq) 
FROM jefy.budnat b, jefy.organ o 
WHERE b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture 
UNION ALL 
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'PRIM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'RELI' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq) 
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o 
WHERE e.EXR_TYPE = 'RELI' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture 
UNION ALL 
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE, 
SUM (bdn_dbm) 
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o 
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM' 
AND b.org_ordre = o.org_ordre 
AND o.org_niv = 2 
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' ) 
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture; 


-------------------------------------
-------------------------------------
-------------------------------------

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_TITRES_CREDITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 TIT_ID, TIT_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 TIT_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, TDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.tit_id, m.tit_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.tit_tva tva, m.tit_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, tde_origine
  FROM maracuja.titre m,
       maracuja.bordereau b,
       maracuja.titre_detail_ecriture mde,
       maracuja.ecriture_detail ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ecriture e
 WHERE m.bor_id = b.bor_id
   AND m.tit_id = mde.tit_id
   AND mde.ecd_ordre = ed.ecd_ordre
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   AND tde_origine IN ('VISA', 'REIMPUTATION')
   AND tit_etat IN ('VISE')
   AND ecd_credit <> 0
   AND tbo_ordre IN (7, 11, 200)
	AND e.top_ordre<>11
   AND ABS (ecd_montant) = ABS (tit_ht);
   
   
---------------------------------------   
---------------------------------------   
---------------------------------------   
---------------------------------------   


CREATE OR REPLACE PROCEDURE comptefi.PREPARE_CPTE_RTAT (exeordre number, gescode varchar2, sacd char)
IS
  total NUMBER(12,2);
  totalant NUMBER(12,2);
  lib varchar2(100);
  lib1 varchar2(50);
  lib2 varchar2(50);
  benef NUMBER(12,2);
  perte NUMBER(12,2);
  benefant NUMBER(12,2);
  perteant NUMBER(12,2);

  -- version du 03/03/2006 - Prestations Internes 
  
BEGIN

--*************** CHARGES *********************************
IF sacd = 'O' THEN
	DELETE CPTE_RTAT_CHARGES where exe_ordre = exeordre and ges_code = gescode;
ELSE
	DELETE CPTE_RTAT_CHARGES where exe_ordre = exeordre and ges_code = 'ETAB';
END IF;

benef := 0;
perte := 0;
benefant := 0;
perteant := 0;

lib1 := 'I';

----- CHARGES EXPLOITATION ----
lib2:= 'CHARGES D''EXPLOITATION';

-- Marchandises ---
	total := resultat_compte(exeordre, '607%', gescode, sacd)
		+ resultat_compte(exeordre, '6087%', gescode, sacd)
		+ resultat_compte(exeordre, '6097%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '607%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6087%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6097%', gescode, sacd);
	lib := 'Achats de marchandises';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Variation marchandises ---
	total := resultat_compte(exeordre, '6037%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6037%', gescode, sacd);
	lib := 'Variation de stocks de marchandises';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- CONSOMMATION EXERCICE ----
lib2:= 'CONSOMMATION DE L''EXERCICE EN PROVENANCE DES TIERS';

-- Achats mati�res 1�res ---
	total := resultat_compte(exeordre, '601%', gescode, sacd)
		+ resultat_compte(exeordre, '6081%', gescode, sacd)
		+ resultat_compte(exeordre, '6091%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '601%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6081%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6091%', gescode, sacd);
	lib := 'Achats stock�s - Mati�res premi�res';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats approvisionnements ---
	total := resultat_compte(exeordre, '602%', gescode, sacd)
		+ resultat_compte(exeordre, '6082%', gescode, sacd)
		+ resultat_compte(exeordre, '6092%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '602%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6082%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6092%', gescode, sacd);
	lib := 'Achats stock�s - Autres approvisionnements';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- variation ---
	total := resultat_compte(exeordre, '6031%', gescode, sacd)
		+ resultat_compte(exeordre, '6032%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6031%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6032%', gescode, sacd);
	lib := 'Variation de stocks d''approvisionnement';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats sous-traitance ---
	total := resultat_compte(exeordre, '604%', gescode, sacd)
		+ resultat_compte(exeordre, '6084%', gescode, sacd)
		+ resultat_compte(exeordre, '6094%', gescode, sacd)
		+ resultat_compte(exeordre, '605%', gescode, sacd)
		+ resultat_compte(exeordre, '6085%', gescode, sacd)
		+ resultat_compte(exeordre, '6095%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '604%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6084%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6094%', gescode, sacd)
		+ resultat_compte(exeordre-1, '605%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6085%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6095%', gescode, sacd);
	lib := 'Achats de sous-traitance';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Achats mat�riel et fournitures ---
	total := resultat_compte(exeordre, '606%', gescode, sacd)
		+ resultat_compte(exeordre, '6086%', gescode, sacd)
		+ resultat_compte(exeordre, '6096%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '606%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6086%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6096%', gescode, sacd);
	lib := 'Achats non stock�s de mati�res et de fournitures';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Sces ext�rieurs  Prsel int�rim ---
	total := resultat_compte(exeordre, '6211%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6211%', gescode, sacd);
	lib := 'Services ext�rieurs : Personnel int�rimaire';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Sces ext�rieurs loyers cr�dit-bail ---
	total := resultat_compte(exeordre, '612%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '612%', gescode, sacd);
	lib := 'Services ext�rieurs : Loyers en cr�dit-bail';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Autres Sces ext�rieurs ---
	total := resultat_compte(exeordre, '61%', gescode, sacd)
		+ resultat_compte(exeordre, '62%', gescode, sacd)
		-(resultat_compte(exeordre, '612%', gescode, sacd)
		+ resultat_compte(exeordre, '6211%', gescode, sacd))
		+ resultat_compte(exeordre, '619%', gescode, sacd)
		+ resultat_compte(exeordre, '629%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '61%', gescode, sacd)
		+ resultat_compte(exeordre-1, '62%', gescode, sacd)
		-(resultat_compte(exeordre-1, '612%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6211%', gescode, sacd))
		+ resultat_compte(exeordre-1, '619%', gescode, sacd)
		+ resultat_compte(exeordre-1, '629%', gescode, sacd);
	lib := 'Autres services ext�rieurs';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- IMPOTS----
lib2:= 'IMPOTS, TAXES ET VERSEMENTS ASSIMILES';

-- Sur r�mun�rations ---
	total := resultat_compte(exeordre, '631%', gescode, sacd)
		+ resultat_compte(exeordre, '632%', gescode, sacd)
		+ resultat_compte(exeordre, '633%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '631%', gescode, sacd)
		+ resultat_compte(exeordre-1, '632%', gescode, sacd)
		+ resultat_compte(exeordre-1, '633%', gescode, sacd);
	lib := 'Sur r�mun�rations';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres ---
	total := resultat_compte(exeordre, '635%', gescode, sacd)
		+ resultat_compte(exeordre, '637%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '635%', gescode, sacd)
		+ resultat_compte(exeordre-1, '637%', gescode, sacd);
	lib := 'Autres imp�ts, taxes et versements assimil�s';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- CHARGES PERSONNEL ----
lib2:= 'CHARGES DE PERSONNEL';

-- Salaire ---
	total := resultat_compte(exeordre, '641%', gescode, sacd)
		+ resultat_compte(exeordre, '642%', gescode, sacd)
		+ resultat_compte(exeordre, '643%', gescode, sacd)
		+ resultat_compte(exeordre, '644%', gescode, sacd)
		+ resultat_compte(exeordre, '648%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '641%', gescode, sacd)
		+ resultat_compte(exeordre-1, '642%', gescode, sacd)
		+ resultat_compte(exeordre-1, '643%', gescode, sacd)
		+ resultat_compte(exeordre-1, '644%', gescode, sacd)
		+ resultat_compte(exeordre-1, '648%', gescode, sacd);
	lib := 'Salaires et traitements';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Charges sociales ---
	total := resultat_compte(exeordre, '645%', gescode, sacd)
		+ resultat_compte(exeordre, '647%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '645%', gescode, sacd)
		+ resultat_compte(exeordre-1, '647%', gescode, sacd);
	lib := 'Charges sociales';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- AMORTISSEMENTS ET PROVISIONS ----
lib2:= 'DOTATIONS AUX AMORTISSEMENTS ET AUX PROVISIONS';

-- Amort sur Immobilisations ---
	total := resultat_compte(exeordre, '6811%', gescode, sacd)
		+ resultat_compte(exeordre, '6812%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6811%', gescode, sacd)
		+ resultat_compte(exeordre-1, '6812%', gescode, sacd);
	lib := 'Sur immobilisations : Dotations aux amortissements';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov sur Immobilisations ---
	total := resultat_compte(exeordre, '6816%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6816%', gescode, sacd);
	lib := 'Sur immobilisations : Dotations aux provisions';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov sur Actifs circulants ---
	total := resultat_compte(exeordre, '6817%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6817%', gescode, sacd);
	lib := 'Sur actif circulant : Dotations aux provisions';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prov pour Risques et Charges ---
	total := resultat_compte(exeordre, '6815%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '6815%', gescode, sacd);
	lib := 'Pour risques et charges : Dotations aux provisions';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- AUTRES CHARGES ----
lib2:= 'AUTRES CHARGES';

-- Autres charges ---
	total := resultat_compte(exeordre, '65%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '65%', gescode, sacd);
	lib := 'Autres charges de gestion courante' ;
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prestations internes ---
	total := resultat_compte(exeordre, '186%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '186%', gescode, sacd);
	lib := 'Charges de Prestations Internes' ;
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);


--********************************************
lib1 := 'II';

---- CHARGES FINANCIERES----
lib2:= 'CHARGES FINANCIERES';

-- Amort et provisions ---
	total := resultat_compte(exeordre, '686%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '686%', gescode, sacd);
	lib := 'Dotations aux amortissements et aux provisions';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Int�r�ts ---
	total := resultat_compte(exeordre, '661%', gescode, sacd)
		+ resultat_compte(exeordre, '664%', gescode, sacd)
		+ resultat_compte(exeordre, '665%', gescode, sacd)
		+ resultat_compte(exeordre, '668%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '661%', gescode, sacd)
		+ resultat_compte(exeordre-1, '664%', gescode, sacd)
		+ resultat_compte(exeordre-1, '665%', gescode, sacd)
		+ resultat_compte(exeordre-1, '668%', gescode, sacd);
	lib := 'Int�r�ts et charges assimil�es';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Perte de change---
	total := resultat_compte(exeordre, '666%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '666%', gescode, sacd);
	lib := 'Diff�rence n�gative de change';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Charges sur cession VMP ---
	total := resultat_compte(exeordre, '667%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '667%', gescode, sacd);
	lib := 'Charges nettes sur cessions de valeurs mobili�res de placement';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- CHARGES EXCEPTIONNELLES ----
lib2:= 'CHARGES EXCEPTIONNELLES';

-- Op�rations de gestion ---
	total := resultat_compte(exeordre, '671%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '671%', gescode, sacd);
	lib := 'Sur op�ration de gestion';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op�rations en capital VC �lement actifs c�d�s ---
	total := resultat_compte(exeordre, '675%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '675%', gescode, sacd);
	lib := 'Sur op�ration en capital : Valeur comptable des �l�ments d''actif c�d�s';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres Op�rations ---
	total := resultat_compte(exeordre, '678%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '678%', gescode, sacd);
	lib := 'Sur autres op�rations en capital';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Amort et Provisions ---
	total := resultat_compte(exeordre, '687%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '687%', gescode, sacd);
	lib := 'Dotations aux amortissements et aux provisions';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

---- IMPOTS SUR BENEF----
lib2:= 'IMPOTS SUR LES BENEFICES';

-- impots sur les b�n�fices ---
	total := resultat_compte(exeordre, '69%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '69%', gescode, sacd);
	lib := '  ';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- B�n�fice ou Perte --
lib1 := 'III';
lib2 := '  ';
	IF sacd = 'O' THEN
		select sum(credit)- sum(debit) into total from maracuja.cfi_ecritures
		where (pco_num = '120' or pco_num = '129') and ges_code = gescode and exe_ordre = exeordre;
		select sum(credit)-sum(debit) into totalant from maracuja.cfi_ecritures
		where (pco_num = '120' or pco_num = '129') and ges_code = gescode and exe_ordre = exeordre-1;
	ELSE
		select sum(credit)- sum(debit) into total from maracuja.cfi_ecritures
		where (pco_num = '120' or pco_num = '129') and ecr_sacd = 'N' and exe_ordre = exeordre;
		select sum(credit)-sum(debit) into totalant from maracuja.cfi_ecritures
		where (pco_num = '120' or pco_num = '129') and ecr_sacd = 'N' and exe_ordre = exeordre-1;
	END IF;
	IF total >= 0 THEN
		benef := total;
		ELSE perte := -total;
	END IF;
	IF totalant >= 0 THEN
		benefant := totalant;
		ELSE perteant := -totalant;
	END IF;
	lib := 'SOLDE CREDITEUR = BENEFICE';
	insert into cpte_rtat_charges values (seq_cr_charges.nextval, lib1, lib2, lib, benef, benefant, gescode, exeordre);


--*************** PRODUITS *********************************
IF sacd = 'O' THEN
	DELETE CPTE_RTAT_PRODUITS where exe_ordre = exeordre and ges_code = gescode;
ELSE
	DELETE CPTE_RTAT_PRODUITS where exe_ordre = exeordre and ges_code = 'ETAB';
END IF;

lib1 := 'I';

----- PRODUITS EXPLOITATION ----
lib2:= 'PRODUITS D''EXPLOITATION';

-- Marchandises ---
	total := resultat_compte(exeordre, '707%', gescode, sacd)
		+ resultat_compte(exeordre, '7097%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '707%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7097%', gescode, sacd);
	lib := 'Vente de marchandises';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Ventes ---
	total := resultat_compte(exeordre, '701%', gescode, sacd)
		+ resultat_compte(exeordre, '702%', gescode, sacd)
		+ resultat_compte(exeordre, '703%', gescode, sacd)
		+ resultat_compte(exeordre, '7091%', gescode, sacd)
		+ resultat_compte(exeordre, '7092%', gescode, sacd)
		+ resultat_compte(exeordre, '7093%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '701%', gescode, sacd)
		+ resultat_compte(exeordre-1, '702%', gescode, sacd)
		+ resultat_compte(exeordre-1, '703%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7091%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7092%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7093%', gescode, sacd);
	lib := 'Production vendue : Ventes';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Travaux ---
	total := resultat_compte(exeordre, '704%', gescode, sacd)
		+ resultat_compte(exeordre, '7094%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '704%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7094%', gescode, sacd);
	lib := 'Production vendue : Travaux';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Prestations ---
	total := resultat_compte(exeordre, '705%', gescode, sacd)
		+ resultat_compte(exeordre, '706%', gescode, sacd)
		+ resultat_compte(exeordre, '708%', gescode, sacd)
		+ resultat_compte(exeordre, '7095%', gescode, sacd)
		+ resultat_compte(exeordre, '7096%', gescode, sacd)
		+ resultat_compte(exeordre, '7098%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '705%', gescode, sacd)
		+ resultat_compte(exeordre-1, '706%', gescode, sacd)
		+ resultat_compte(exeordre-1, '708%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7095%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7096%', gescode, sacd)
		+ resultat_compte(exeordre-1, '7098%', gescode, sacd);
	lib := 'Production vendue : Prestations de services, �tudes et activit�s annexes';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Biens en-cours ---
	total := resultat_compte(exeordre, '7133%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '7133%', gescode, sacd);
	lib := 'Production stock�e : En-cours de production de biens';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Services en-cours ---
	total := resultat_compte(exeordre, '7134%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '7134%', gescode, sacd);
	lib := 'Production stock�e : En-cours de production de services';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Stocks Produits ---
	total := resultat_compte(exeordre, '7135%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '7135%', gescode, sacd);
	lib := 'Production stock�e : Produits';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Production Immobilis�e ---
	total := resultat_compte(exeordre, '72%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '72%', gescode, sacd);
	lib := 'Production immobilis�e';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Subventions exploitation ---
	total := resultat_compte(exeordre, '74%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '74%', gescode, sacd);
	lib := 'Subventions d''exploitation';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Amort et provisions ---
	total := resultat_compte(exeordre, '781%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '781%', gescode, sacd);
	lib := 'Reprise sur amortissements et sur provisions';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Transfert de charges  ---
	total := resultat_compte(exeordre, '791%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '791%', gescode, sacd);
	lib := 'Transferts de charges d''exploitation';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Autres produits  ---
	total := resultat_compte(exeordre, '75%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '75%', gescode, sacd);
	lib := 'Autres produits';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

--  Prestations internes ---
	total := resultat_compte(exeordre, '187%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '187%', gescode, sacd);
	lib := 'Produits de Prestations internes';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);


lib1 := 'II';

----- PRODUITS FINANCIERS ----
lib2:= 'PRODUITS FINANCIERS';

-- Participations ---
	total := resultat_compte(exeordre, '761%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '761%', gescode, sacd);
	lib := 'De participation';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres VM et cr�ances immobilis�es ---
	total := resultat_compte(exeordre, '762%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '762%', gescode, sacd);
	lib := 'D''autres valeurs mobili�res et cr�ances de l''actif immobilis�';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Autres produits ---
	total := resultat_compte(exeordre, '763%', gescode, sacd)
		+ resultat_compte(exeordre, '764%', gescode, sacd)
		+ resultat_compte(exeordre, '765%', gescode, sacd)
		+ resultat_compte(exeordre, '768%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '763%', gescode, sacd)
		+ resultat_compte(exeordre-1, '764%', gescode, sacd)
		+ resultat_compte(exeordre-1, '765%', gescode, sacd)
		+ resultat_compte(exeordre-1, '768%', gescode, sacd);
	lib := 'Autres int�r�ts et produits assimil�s';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Reprise sur amort et prov ---
	total := resultat_compte(exeordre, '786%', gescode, sacd)
		+ resultat_compte(exeordre, '796%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '786%', gescode, sacd)
		+ resultat_compte(exeordre-1, '796%', gescode, sacd);
	lib := 'Reprise sur provisions et transferts de charges financi�res';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Gain de change ---
	total := resultat_compte(exeordre, '766%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '766%', gescode, sacd);
	lib := 'Diff�rence positive de change';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Pduits sur cession VMP ---
	total := resultat_compte(exeordre, '767%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '767%', gescode, sacd);
	lib := 'Produits nets sur cession de valeurs mobili�res de placement';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

----- PRODUITS EXCEPTIONNELS ----
lib2:= 'PRODUITS EXCEPTIONNELS';

-- Op�ration de gestion ---
	total := resultat_compte(exeordre, '771%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '771%', gescode, sacd);
	lib := 'Sur op�ration de gestion';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital cession �l�m�nts actifs ---
	total := resultat_compte(exeordre, '775%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '775%', gescode, sacd);
	lib := 'Sur op�ration en capital : Produits des cessions d''�lements d''actif';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital subvention exploitation ---
	total := resultat_compte(exeordre, '777%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '777%', gescode, sacd);
	lib := 'Sur op�ration en capital : Subventions d''investissement vir�es au r�sultat';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Op capital AUTRES ---
	total := resultat_compte(exeordre, '778%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '778%', gescode, sacd);
	lib := 'Sur autres op�rations en capital';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Neutralisation des amortissements ---
	total := resultat_compte(exeordre, '776%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '776%', gescode, sacd);
	lib := 'Neutralisation des amortissements';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- Reprise sur amort et prov ---
	total := resultat_compte(exeordre, '787%', gescode, sacd)
		+ resultat_compte(exeordre, '797%', gescode, sacd);
	totalant := resultat_compte(exeordre-1, '787%', gescode, sacd)
		+ resultat_compte(exeordre-1, '797%', gescode, sacd);
	lib := 'Reprise sur provisions et transferts de charges exceptionnelles';
	insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, total, totalant, gescode, exeordre);

-- B�n�fice ou Perte --
lib1 := 'III';
lib2 := '  ';
lib := 'SOLDE DEBITEUR = PERTE';
insert into cpte_rtat_produits values (seq_cr_produits.nextval, lib1, lib2, lib, perte, perteant, gescode, exeordre);

end;
/




