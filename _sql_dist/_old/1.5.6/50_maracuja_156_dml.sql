SET define OFF
SET scan OFF


-- Correction de l'association des mandats et ecriture_detail issus de Papaye
INSERT INTO MARACUJA.MANDAT_DETAIL_ECRITURE (
   ECD_ORDRE, EXE_ORDRE, MAN_ID, 
   MDE_DATE, MDE_ORDRE, MDE_ORIGINE, 
   ORI_ORDRE) 
SELECT
ecd.ecd_ordre, --ECD_ORDRE, 
b.exe_ordre, --EXE_ORDRE, 
m.man_id, --MAN_ID, 
SYSDATE, --MDE_DATE, 
mandat_detail_ecriture_seq.NEXTVAL, 
'VISA', 
m.ORI_ORDRE
FROM BORDEREAU b,  BORDEREAU_INFO bi , MANDAT m, ECRITURE_DETAIL ecd 
WHERE
  b.tbo_ordre=3
 AND bi.bor_id=b.bor_id
 AND b.exe_ordre=2007
 AND m.bor_id=b.bor_id
 AND b.ges_code=ecd.ges_code
AND bi.BOR_LIBELLE LIKE '% 2007'
AND ecd_libelle LIKE 'VISA SALAIRES '||bi.bor_libelle ||  ' Bord. '|| b.bor_num ||' du '||b.ges_code
AND ecd.pco_num LIKE '6%'
AND ecd.ecd_sens='D' 
AND m.pco_num=ecd.pco_num
AND ecd.ecd_ordre NOT IN (SELECT ecd_ordre FROM MANDAT_DETAIL_ECRITURE);


INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.5.6',  SYSDATE, '');


commit;