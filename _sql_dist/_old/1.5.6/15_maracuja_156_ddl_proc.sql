SET define OFF
SET scan OFF

CREATE OR REPLACE PACKAGE MARACUJA.bordereau_abricot_paye is
/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

procedure basculer_bouillard_paye(borid integer);

procedure set_mandat_brouillard(manid integer);
procedure set_bord_brouillard_visa(borid integer);
procedure set_bord_brouillard_paiement(moisordre number, borid number, exeordre number);
procedure set_bord_brouillard_retenues(borid number);
procedure set_bord_brouillard_sacd(borid number);
end;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Bordereau_Abricot_Paye IS


PROCEDURE basculer_bouillard_paye(borid INTEGER) IS

tmpBordereau maracuja.BORDEREAU%ROWTYPE;
mois VARCHAR2(50);
moiscomplet VARCHAR2(50);
moisordre INTEGER;
cpt INTEGER;

sumdebits NUMBER;
sumcredits NUMBER;
manid INTEGER;

CURSOR c1 IS 
SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos du bordereau
SELECT * INTO tmpBordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

-- recup du mois JANVIER XXXX
SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid;

-- recup du moiordre
SELECT mois_ordre,mois_complet INTO moisordre,moiscomplet FROM jefy_paye.paye_mois
WHERE mois_complet = mois;



-- On verifie que les ecritures aient bien ete generees pour la composante en question.
SELECT COUNT(*) INTO cpt FROM jefy_paye.jefy_ecritures WHERE ecr_comp = tmpBordereau.ges_code AND mois_ordre = moisordre;

IF (cpt = 0) THEN
 RAISE_APPLICATION_ERROR(-20001,'Vous n''avez toujours pas pr?par? les ?critures pour la composante '||tmpBordereau.ges_code||' !');
END IF;

-- On verifie que le total des debits soit egal au total des credits  (Pour la composante)
 SELECT SUM(ecr_mont) INTO sumdebits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64'
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'D';

 SELECT SUM(ecr_mont) INTO sumcredits FROM jefy_paye.jefy_ecritures
 WHERE ecr_comp = ges_code AND mois_ordre = moisordre AND ecr_type='64' 
 AND ecr_comp = tmpBordereau.ges_code AND ecr_sens = 'C';

 IF (sumcredits <> sumdebits)
 THEN
 	 RAISE_APPLICATION_ERROR(-20001,'Pour la composante '||tmpBordereau.ges_code||', la somme des DEBITS ('||sumdebits||') est diff?rente de la somme des CREDITS ('||sumcredits||') !');
 END IF;

  SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU
  WHERE bor_id = borid 
  AND tbo_ordre = tmpBordereau.tbo_ordre 
  AND exe_ordre = tmpBordereau.exe_ordre;

  IF cpt = 1 THEN
   Bordereau_Abricot_Paye.set_bord_brouillard_visa(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_retenues(borid);
   Bordereau_Abricot_Paye.set_bord_brouillard_sacd(borid);

   INSERT INTO maracuja.BORDEREAU_INFO VALUES  (borid, moiscomplet,NULL);

  END IF;
  -- Mise a jour des ecritures de paiement dans bordereau_brouillard
  -- Ces ecritures seront associees a la premiere composante qui mandatera ses payes.
  Bordereau_Abricot_Paye.set_bord_brouillard_paiement(moisordre, borid, tmpBordereau.exe_ordre);

-- misea jour dans papaye des tables apres bascule !
  -- maj de l etat de papaye_compta et du borid -
 
 UPDATE jefy_paye.jefy_paye_compta SET bor_id=borid, jpc_etat='MANDATEE'
    WHERE ges_code=tmpBordereau.ges_code 
      AND mois_ordre=moisordre AND jpc_ETAT='LIQUIDEE';
/*  
  update jefy_paye.jefy_liquidations set liq_etat='MANDATEE'
    where ges_code=tmpBordereau.ges_code 
      and mois_ordre=moisordre and liq_ETAT='LIQUIDEE';
*/

-- modifications FRED -> BUG REF ECRITURES MANDAT_DETAIL_ECRITURE VU PAR RODOLPHE 23/03/2007
-- on vide la recuperation
DELETE FROM maracuja.MANDAT_BROUILLARD WHERE man_id IN ( SELECT man_id FROM maracuja.MANDAT WHERE bor_id = borid);

-- on refait les ecritures de Debits dans mandat_brouillard
OPEN c1;
LOOP
FETCH c1 INTO manid;
EXIT WHEN c1%NOTFOUND;
 Bordereau_Abricot_Paye.set_mandat_brouillard(manid);
END LOOP;
CLOSE c1;

END;



-- Ecritures de Paiement (Type 45 dans Jefy_ecritures).
PROCEDURE set_bord_brouillard_paiement(moisordre NUMBER, borid NUMBER, exeordre NUMBER)
IS

CURSOR ecriturespaiement IS
SELECT * FROM jefy_paye.jefy_ecritures WHERE mois_ordre = moisordre AND ecr_type='45';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;
bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;
gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;
--moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
moiscomplet jefy_paye.paye_mois.mois_complet%TYPE;
cpt INTEGER;
mois VARCHAR2(50);

BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;
-- recup du mois_complet
SELECT mois_complet INTO moiscomplet FROM jefy_paye.paye_mois
WHERE mois_ordre  = moisordre; 


SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD 
WHERE bob_operation LIKE '%PAIEMENT%' AND bob_libelle1 = 'PAIEMENT SALAIRES '||moiscomplet;

-- cpt = 0 ==> Aucune ecriture de paiement passee pour ce mois
IF (cpt = 0)
THEN

  OPEN ecriturespaiement;
  LOOP
    FETCH ecriturespaiement INTO currentecriture;
    EXIT WHEN ecriturespaiement%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'PAIEMENT SALAIRES',
	currentecriture.pco_num,
	'PAIEMENT SALAIRES '||moiscomplet,
	moiscomplet,
	NULL
	);

 END LOOP;
 CLOSE ecriturespaiement;
END IF;

END;

-- ECRITURES VISA - Ecritures de credit de type '64' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_visa(borid INTEGER)
IS

--cursor ecriturescredit64(mois number , gescode varchar2) is
--select * from jefy_paye.jefy_ecritures where mois_ordre = mois and ecr_comp = gescode and ecr_sens = 'C' and ecr_type='64';

CURSOR ecriturescredit64(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.JEFY_ECRITURES WHERE mois_ordre = lemois
AND ecr_comp = lacomp  
AND ecr_type='64' 
AND ( ecr_sens = 'C' OR (ecr_sens  = 'D' AND pco_num LIKE '4%' ));


currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid;

-- recup du moiordre
SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

dbms_output.put_line('BROUILLARD VISA : '||currentbordereau.ges_code||' , moisordre : '||mois);


  OPEN ecriturescredit64(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecriturescredit64 INTO currentecriture;
    EXIT WHEN ecriturescredit64%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'VISA SALAIRES',
	currentecriture.pco_num,
	'VISA SALAIRES '||mois,
	mois,
	NULL
	);

	END LOOP;
  CLOSE ecriturescredit64;

END;

-- Ecritures de retenues / Oppositions - Ecritures de type '44' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_retenues(borid NUMBER)
IS

CURSOR ecrituresretenues(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures 
WHERE mois_ordre = lemois 
AND ecr_comp = lacomp AND ecr_type='44';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT mois_ordre INTO moisordre FROM jefy_paye.paye_mois WHERE mois_complet  = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituresretenues(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituresretenues INTO currentecriture;
    EXIT WHEN ecrituresretenues%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'RETENUES SALAIRES',
	currentecriture.pco_num,
	'RETENUES SALAIRES '||mois,
	mois,
	NULL
	);

	END LOOP;
  CLOSE ecrituresretenues;

END;

-- Ecritures SACD - Ecritures de type '18' dans jefy_paye.jefy_ecritures.
PROCEDURE set_bord_brouillard_sacd(borid NUMBER)
IS

CURSOR ecrituressacd(lemois NUMBER , lacomp VARCHAR2) IS
SELECT * FROM jefy_paye.jefy_ecritures 
WHERE mois_ordre = lemois AND ecr_comp = lacomp AND ecr_type='18';

currentecriture jefy_paye.jefy_ecritures%ROWTYPE;
currentbordereau maracuja.BORDEREAU%ROWTYPE;

bobordre maracuja.BORDEREAU_BROUILLARD.bob_ordre%TYPE;

cpt INTEGER;
moisordre INTEGER;

gescode maracuja.BORDEREAU_BROUILLARD.ges_code%TYPE;

moislibelle jefy_paye.paye_mois.mois_complet%TYPE;
mois VARCHAR2(50);
BEGIN

SELECT * INTO currentbordereau FROM maracuja.BORDEREAU WHERE bor_id = borid;

SELECT maracuja.bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

SELECT DISTINCT dep_numero INTO mois
FROM maracuja.DEPENSE d , maracuja.MANDAT m
WHERE d.man_id = m.man_id
AND m.bor_id = borid;

-- recup du moiordre
SELECT mois_ordre,mois_libelle INTO moisordre,moislibelle FROM jefy_paye.paye_mois
WHERE mois_complet = mois;

SELECT ges_code INTO gescode FROM maracuja.BORDEREAU WHERE bor_id = borid;

  OPEN ecrituressacd(moisordre, currentbordereau.ges_code);
  LOOP
    FETCH ecrituressacd INTO currentecriture;
    EXIT WHEN ecrituressacd%NOTFOUND;

	SELECT bordereau_brouillard_seq.NEXTVAL INTO bobordre FROM dual;

	INSERT INTO BORDEREAU_BROUILLARD VALUES
	(
	bobordre,
	borid,
	currentbordereau.exe_ordre,
	currentecriture.ges_code,
	currentecriture.ecr_mont,
	currentecriture.ecr_sens,
	'VALIDE',
	'SACD SALAIRES',
	currentecriture.pco_num,
	'SACD SALAIRES '||mois,
	mois,
	NULL
	);

	END LOOP;
  CLOSE ecrituressacd;

END;


-- Ecritures de visa des payes (Debit 6) .
PROCEDURE set_mandat_brouillard(manid INTEGER)
IS

lemandat  	  MANDAT%ROWTYPE;

BEGIN

SELECT * INTO lemandat FROM MANDAT WHERE man_id = manid;

-- creation du mandat_brouillard visa DEBIT--
INSERT INTO MANDAT_BROUILLARD VALUES
(
NULL,  						   --ECD_ORDRE,
lemandat.exe_ordre,			   --EXE_ORDRE,
lemandat.ges_code,			   --GES_CODE,
lemandat.man_ht,			   --MAB_MONTANT,
'VISA SALAIRES',				   --MAB_OPERATION,
mandat_brouillard_seq.NEXTVAL, --MAB_ORDRE,
'D',						   --MAB_SENS,
manid,						   --MAN_ID,
lemandat.pco_num			   --PCO_NU
);

END;

PROCEDURE get_facture_jefy
(exeordre INTEGER,manid INTEGER,manordre INTEGER,utlordre INTEGER)
IS

depid 	  		 DEPENSE.dep_id%TYPE;
jefyfacture 	 jefy.factures%ROWTYPE;
lignebudgetaire  DEPENSE.DEP_LIGNE_BUDGETAIRE%TYPE;
fouadresse  	 DEPENSE.dep_adresse%TYPE;
founom  		 DEPENSE.dep_fournisseur%TYPE;
lotordre  		 DEPENSE.dep_lot%TYPE;
marordre		 DEPENSE.dep_marches%TYPE;
fouordre		 DEPENSE.fou_ordre%TYPE;
gescode			 DEPENSE.ges_code%TYPE;
cpt				 INTEGER;
	tcdordre		 TYPE_CREDIT.TCD_ORDRE%TYPE;
	tcdcode			 TYPE_CREDIT.tcd_code%TYPE;
	
lemandat MANDAT%ROWTYPE;	

CURSOR factures IS
 SELECT * FROM jefy.factures
 WHERE man_ordre = manordre;

BEGIN

OPEN factures;
LOOP
FETCH factures INTO jefyfacture;
EXIT WHEN factures%NOTFOUND;

-- creation du depid --
SELECT depense_seq.NEXTVAL INTO depid FROM dual;


 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
 			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

-- creation de lignebudgetaire--
SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE

			--recuperer le type de credit a partir de la commande
			SELECT tcd_code INTO tcdcode FROM jefy.commande WHERE cde_ordre =  jefyfacture.cde_ordre;

			SELECT tc.tcd_ordre INTO tcdordre
				FROM TYPE_CREDIT tc
				WHERE tcd_code = tcdcode AND  exe_ordre = exeordre;

SELECT org_comp||' '||org_lbud||' '||org_uc
INTO lignebudgetaire
FROM jefy.organ
WHERE org_ordre =
(
 SELECT  MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- recuperations --

-- gescode --
 SELECT COUNT(*) INTO cpt FROM jefy.facture_ext
WHERE cde_ordre = jefyfacture.cde_ordre;

 IF cpt = 0 THEN
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
 SELECT org_ordre
 FROM jefy.engage
 WHERE cde_ordre = jefyfacture.cde_ordre
 AND eng_stat !='A'
);
ELSE
SELECT org_comp
INTO gescode
FROM jefy.organ
WHERE org_ordre =
(
SELECT MAX(org_ordre)
 FROM jefy.facture_ext
 WHERE cde_ordre = jefyfacture.cde_ordre
);
END IF;

-- fouadresse --
SELECT SUBSTR((ADR_ADRESSE1||' '||ADR_ADRESSE2||' '||ADR_CP||' '||ADR_VILLE),1,196)||'...'
INTO fouadresse
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- founom --
SELECT adr_nom||' '||adr_prenom
INTO founom
FROM v_fournisseur
WHERE fou_ordre =
(
 SELECT fou_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

-- fouordre --
 SELECT fou_ordre INTO fouordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre;

-- lotordre --
SELECT COUNT(*) INTO cpt
FROM marches.attribution
WHERE att_ordre =
(
 SELECT lot_ordre
 FROM jefy.commande
 WHERE cde_ordre = jefyfacture.cde_ordre
);

 IF cpt = 0 THEN
  lotordre :=NULL;
 ELSE
  SELECT lot_ordre
  INTO lotordre
  FROM marches.attribution
  WHERE att_ordre =
  (
   SELECT lot_ordre
   FROM jefy.commande
   WHERE cde_ordre = jefyfacture.cde_ordre
  );
 END IF;

-- marordre --
SELECT COUNT(*) INTO cpt
FROM marches.lot
WHERE lot_ordre = lotordre;

IF cpt = 0 THEN
  marordre :=NULL;
ELSE
 SELECT mar_ordre
 INTO marordre
 FROM marches.lot
 WHERE lot_ordre = lotordre;
END IF;

SELECT * INTO lemandat FROM MANDAT WHERE man_id=manid;




-- creation de la depense --
INSERT INTO DEPENSE VALUES
(
fouadresse ,           --DEP_ADRESSE,
NULL ,				   --DEP_DATE_COMPTA,
jefyfacture.dep_date,  --DEP_DATE_RECEPTION,
jefyfacture.dep_date , --DEP_DATE_SERVICE,
'VALIDE' ,			   --DEP_ETAT,
founom ,			   --DEP_FOURNISSEUR,
jefyfacture.dep_mont , --DEP_HT,
depense_seq.NEXTVAL ,  --DEP_ID,
lignebudgetaire ,	   --DEP_LIGNE_BUDGETAIRE,
lotordre ,			   --DEP_LOT,
marordre ,			   --DEP_MARCHES,
jefyfacture.dep_ttc ,  --DEP_MONTANT_DISQUETTE,
jefyfacture.cm_ordre , --DEP_NOMENCLATURE,
jefyfacture.dep_fact  ,--DEP_NUMERO,
jefyfacture.dep_ordre ,--DEP_ORDRE,
NULL ,				   --DEP_REJET,
jefyfacture.rib_ordre ,--DEP_RIB,
'NON' ,				   --DEP_SUPPRESSION,
jefyfacture.dep_ttc ,  --DEP_TTC,
jefyfacture.dep_ttc
-jefyfacture.dep_mont, -- DEP_TVA,
exeordre ,			   --EXE_ORDRE,
fouordre, 			   --FOU_ORDRE,
gescode,  			   --GES_CODE,
manid ,				   --MAN_ID,
jefyfacture.man_ordre, --MAN_ORDRE,
--jefyfacture.mod_code,  --MOD_ORDRE,
lemandat.mod_ordre,
jefyfacture.pco_num ,  --PCO_ORDRE,
1,    		   --UTL_ORDRE
NULL, --org_ordre
tcdordre,
NULL, -- ecd_ordre_ema
jefyfacture.DEP_DATE
);

END LOOP;
CLOSE factures;

END;


END;
/






-------------------------------------------
-------------------------------------------
-------------------------------------------









CREATE OR REPLACE PACKAGE BODY maracuja.Gestionorigine IS
--version 1.1.7 31/05/2005 - mise a jour libelle origine dans le cas des conventions
--version 1.1.6 27/04/2005 - ajout maj_origine
--version 1.2.0 20/12/2005 - Multi exercices jefy
--version 1.2.1 04/01/2006
-- version 1.5.6 29/03/2007 - corrections doublons

FUNCTION traiter_orgordre (orgordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	 	   IF orgordre IS NULL THEN RETURN NULL; END IF;

		-- recup du type_origine --
		SELECT top_ordre INTO topordre FROM TYPE_OPERATION
		WHERE top_libelle = 'OPERATION LUCRATIVE';


        
		-- l origine est t elle deja  suivie --
		SELECT COUNT(*)INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'ORG_ORDRE'
		AND ORI_ENTITE ='JEFY.ORGAN'
		AND ORI_KEY_ENTITE	=orgordre;

		IF cpt >= 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'ORG_ORDRE'
			AND ORI_ENTITE ='JEFY.ORGAN'
			AND ORI_KEY_ENTITE	=orgordre
			AND ROWNUM=1;
		
		ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
		
			-- recup de la LIGNE BUDGETAIRE --
			--le libelle utilisateur pour le suivie en compta --
			SELECT org_comp||'-'||org_lbud||'-'||org_uc
			 INTO orilibelle
			FROM jefy.organ
			WHERE org_ordre = orgordre;
		
			INSERT INTO ORIGINE
			(ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
			VALUES ('JEFY.ORGAN','ORG_ORDRE',orilibelle,cpt,orgordre,topordre);
		
		END IF;
		
		RETURN cpt;

END;


FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
typeOrgan v_organ_exer.TYOR_ID%TYPE;

BEGIN

	 	  IF orgid IS NULL THEN RETURN NULL; END IF;
		  
		  SELECT tyor_id INTO typeOrgan FROM jefy_admin.organ WHERE org_id=orgid;
		  
			SELECT COUNT(*) INTO cpt 
					FROM accords.CONVENTION_LIMITATIVE
					WHERE org_id = orgid AND exe_ordre = exeordre;
							  
		  -- si c une convention RA
		  IF (typeOrgan = 2 OR cpt>0) THEN
					 -- recup du type_origine CONVENTION RA--
					 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
					 WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';			  
		  
					SELECT COUNT(*) INTO cpt 
							FROM accords.CONVENTION_LIMITATIVE
							WHERE org_id = orgid AND exe_ordre = exeordre;
					
						-- si on a affaire a une convention RA identifiee dans convention
						IF cpt >0 THEN
									-- recup de la convention
									  SELECT MAX(con_ordre) INTO convordre 
									 FROM accords.CONVENTION_LIMITATIVE
									 WHERE org_id = orgid
									 AND exe_ordre = exeordre;
									
									 SELECT SUBSTR(EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET,1,2000) 
									 INTO orilibelle 
									 FROM accords.contrat
									 WHERE con_ordre = convordre;
						 
						ELSE -- c une convention RA mais pas dans accords
									 SELECT COUNT(*) INTO cpt 
									 FROM jefy_admin.organ 
									 WHERE org_id = orgid;
									
									 IF cpt = 1 THEN
											 --le libelle utilisateur pour le suivie en compta --
											 SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
											 INTO orilibelle
											 FROM jefy_admin.organ
											 WHERE org_id = orgid;
									        
									 ELSE -- pas d'organ trouvee
									  	 	RETURN NULL;
									 END IF;
							END IF;		  
		  
		  ELSE
		  -- c pas une convention RA, c peut-etre une ligne lucrative
		  -- a voir s'il ne faut pas la traiter d'une part en tant que convention RA et d'autre par en tant que lucrative si c le cas
							 -- recup du type_origine OPERATION LUCRATIVE --
							 SELECT top_ordre INTO topordre FROM TYPE_OPERATION
							 WHERE top_libelle = 'OPERATION LUCRATIVE';
							 		  
 		  	   	   	   			  SELECT COUNT(*) INTO cpt 
									 FROM jefy_admin.organ 
									 WHERE org_id = orgid
									 AND org_lucrativite = 1;
									
									 IF cpt = 1 THEN
											 --le libelle utilisateur pour le suivie en compta --
										 SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
											 INTO orilibelle
											 FROM jefy_admin.organ
											 WHERE org_id = orgid;
									        
									 ELSE -- pas d'organ lucrative trouvee
									  	 	RETURN NULL;
									 END IF;		  
		  END IF;
		  
		-- l origine est t elle deja  suivie --
		SELECT COUNT(*) INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'ORG_ID'
		AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
		AND ORI_KEY_ENTITE	=orgid;

		IF cpt >= 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'ORG_ID'
			AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
			AND ORI_KEY_ENTITE	=orgid
			AND ROWNUM=1;
			
			--on met a jour le libelle
			UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;
		
		ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;		
			INSERT INTO ORIGINE (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
			VALUES ('JEFY_ADMIN.ORGAN','ORG_ID',orilibelle,cpt,orgid,topordre);
		
		END IF;

		RETURN cpt;

END;


FUNCTION traiter_convordre (convordre INTEGER)
RETURN INTEGER

IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

	IF convordre IS NULL THEN RETURN NULL; END IF;

	-- recup du type_origine --
	SELECT top_ordre INTO topordre FROM TYPE_OPERATION
	WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

	-- l origine est t elle deja  suivie --
	SELECT COUNT(*) INTO cpt FROM ORIGINE
		WHERE ORI_KEY_NAME = 'CONV_ORDRE'
		AND ORI_ENTITE ='CONVENTION.CONTRAT'
		AND ORI_KEY_ENTITE	=convordre;


	-- recup de la convention --
	--le libelle utilisateur pour le suivie en compta --
--	select CON_REFERENCE_EXTERNE||' '||CON_OBJET into orilibelle from convention.contrat where con_ordre=convordre;
--on change le libelle (demande INP Toulouse 30/05/2005)
--	SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM convention.contrat WHERE con_ordre=convordre;
		SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM accords.contrat WHERE con_ordre=convordre;


	-- l'origine est deja referencee
	IF cpt = 1 THEN
			SELECT ori_ordre INTO cpt FROM ORIGINE
			WHERE ORI_KEY_NAME = 'CONV_ORDRE'
			AND ORI_ENTITE ='CONVENTION.CONTRAT'
			AND ORI_KEY_ENTITE	=convordre;
	
			--on met a jour le libelle
			UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

	-- il faut creer l'origine
	ELSE
			SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
			INSERT INTO ORIGINE VALUES (
					'CONVENTION.CONTRAT' ,--	  USER.TABLE  -
					'CONV_ORDRE',-- 	  PRIMARY KEY -
					ORILIBELLE,-- 	  LIBELLE UTILISATEUR -
					cpt ,--		  ID -
					convordre,--	  VALEUR DE LA KEY -
					topordre--		  type d origine -
				);

	END IF;

	RETURN cpt;
END;


FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;

agtordre 	jefy.facture.agt_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
letitrejefy jefy.TITRE%ROWTYPE;
BEGIN
		
		
		IF exeordre=2005 THEN
			SELECT * INTO lebordereau
			FROM jefy05.bordero
			WHERE bor_ordre = borordre;
		
			-- recup de l agent de ce bordereau --
			SELECT * INTO letitrejefy FROM jefy05.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
			   FROM jefy05.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre);
		ELSE
			SELECT * INTO lebordereau
			FROM jefy.bordero
			WHERE bor_ordre = borordre;
		
			-- recup de l agent de ce bordereau --
			SELECT * INTO letitrejefy FROM jefy.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
			   FROM jefy.TITRE
			   WHERE bor_ordre =lebordereau.bor_ordre);
		END IF;
		
		
		
		
		IF letitrejefy.tit_type ='V' THEN
		   SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REVERSEMENTS';
		ELSE
		  IF letitrejefy.tit_type ='D' THEN
		    SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REDUCTIONS';
		  ELSE
		    IF letitrejefy.tit_type ='P' THEN
		      SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU D ORDRE DE PAIEMENT';
		    ELSE
			  SELECT tbo_ordre INTO tboordre
		      FROM TYPE_BORDEREAU
		      WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' AND EXE_ORDRE=exeordre);
		    END IF;
		  END IF;
		END IF;
		
		
		RETURN tboordre;
END;

FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS

cpt 	 	INTEGER;

lebordereau jefy.bordero%ROWTYPE;
fouordrepapaye jefy.MANDAT.fou_ordre%TYPE;
agtordre 	jefy.facture.agt_ordre%TYPE;
fouordre 	jefy.MANDAT.fou_ordre%TYPE;
borid 		BORDEREAU.bor_id%TYPE;
tboordre 	BORDEREAU.tbo_ordre%TYPE;
utlordre 	UTILISATEUR.utl_ordre%TYPE;
etat		jefy.bordero.bor_stat%TYPE;
BEGIN
		
		IF exeordre=2005 THEN
			-- recup des infos --
			SELECT * INTO lebordereau
			FROM jefy05.bordero
			WHERE bor_ordre = borordre;
		
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy05.facture
			WHERE man_ordre =
			 ( SELECT MAX(man_ordre)
			   FROM jefy05.MANDAT
			   WHERE bor_ordre =borordre
			  );
		
			SELECT MAX(fou_ordre) INTO fouordre
			   FROM jefy05.MANDAT
			   WHERE bor_ordre =borordre;
		ELSE
			-- recup des infos --
			SELECT * INTO lebordereau
			FROM jefy.bordero
			WHERE bor_ordre = borordre;
		
			-- recup de l agent de ce bordereau --
			SELECT MAX(agt_ordre) INTO agtordre
			FROM jefy.facture
			WHERE man_ordre =
			 ( SELECT MAX(man_ordre)
			   FROM jefy.MANDAT
			   WHERE bor_ordre =borordre
			  );
		
			SELECT MAX(fou_ordre) INTO fouordre
			   FROM jefy.MANDAT
			   WHERE bor_ordre =borordre;
		END IF;
		
		-- recuperation du type de bordereau --
		SELECT COUNT(*) INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
		 (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
		
		
		IF (fouordrepapaye <> 0) THEN
		   SELECT fou_ordre INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
		   (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
		ELSE
			fouordrepapaye := NULL;
		END IF;
		
		
		
		IF fouordre =fouordrepapaye THEN
		  SELECT tbo_ordre INTO tboordre
		  FROM TYPE_BORDEREAU
		  WHERE tbo_libelle='BORDEREAU DE MANDAT SALAIRES';
		ELSE
		  SELECT COUNT(*) INTO cpt
		  FROM OLD_AGENT_TYPE_BORD
		  WHERE agt_ordre = agtordre;
		
		  IF cpt <> 0 THEN
		   SELECT tbo_ordre INTO tboordre
		   FROM OLD_AGENT_TYPE_BORD
		   WHERE agt_ordre = agtordre;
		  ELSE
		   SELECT tbo_ordre INTO tboordre
		   FROM TYPE_BORDEREAU
		   WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTME GENERIQUE' AND exe_ordre = exeordre);
		  END IF;
		
		END IF;
		
		
		RETURN tboordre;
END;

FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
BEGIN

RETURN 1;
END;


PROCEDURE Maj_Origine
IS
  -- cursor pour recuperer les organ lucratif + conventions RA
	CURSOR c1 IS
	SELECT org_id, exe_ordre FROM v_organ_exer o
	WHERE  exe_ordre>2006 AND (org_lucrativite !=0 OR tyor_id=2);

	CURSOR c2 IS
	SELECT org_id, exe_ordre FROM accords.convention_limitative WHERE org_id>0;
	orgordre INTEGER;
	conordre INTEGER;
	exeordre INTEGER;
	cpt INTEGER;
BEGIN
		-- On balaye les organ lucratives + taggees RA
		OPEN c1;
		LOOP
			FETCH C1 INTO orgordre, exeordre;
				  EXIT WHEN c1%NOTFOUND;
				  cpt := TRAITER_ORGid ( orgordre , exeordre);
		END LOOP;
		CLOSE c1;
		
		-- on balaye les conventions RA 
		OPEN c2;
		LOOP
			FETCH C2 INTO orgordre, exeordre;
				  EXIT WHEN c2%NOTFOUND;
				  cpt := TRAITER_ORGid ( orgordre , exeordre);
		END LOOP;
		CLOSE c2;		
	
-- Les conventions RA sont maintenant gerees a partir d'un Org_id
-- 		OPEN c2;
-- 		LOOP
-- 			FETCH C2 INTO conordre;
-- 				  EXIT WHEN c2%NOTFOUND;
-- 				  cpt := TRAITER_CONVORDRE ( conordre );
-- 		END LOOP;
-- 		CLOSE c2;


END;


END;
/






