SET define OFF
SET scan OFF
--***********************************************************
-- correction des erreurs liees aux origines
-- la table origine va etre nettoyee
-- une sauvegarde est effectuee dans maracuja.zold_origine
-- le script peut �tre (tres) long...
--***********************************************************

-- correction des entites
UPDATE ORIGINE SET ori_entite = 'JEFY.ORGAN' WHERE ori_entite='JEFY_ORGAN';
COMMIT;
UPDATE ORIGINE SET ori_entite = 'JEFY_ADMIN.ORGAN' WHERE ori_entite='JEFY_ADMIN';
COMMIT;

-- sauvegarder la table origine
CREATE TABLE maracuja.zold_origine AS SELECT * FROM maracuja.ORIGINE;
-- supprimer les origines doublons
--DROP TABLE maracuja.ztmp_oc CASCADE CONSTRAINTS;
CREATE TABLE maracuja.ztmp_oc AS SELECT * FROM maracuja.ztmp_origine_correct;

BEGIN

	-- appeler la procedure de correction (commit integre)
	maracuja.Corrige_Origines_2007 ;
	

	DELETE FROM maracuja.ORIGINE WHERE ori_ordre IN (SELECT ori_ordre_old FROM maracuja.ztmp_oc);
	COMMIT;
	
END;	


