SET define OFF
SET scan OFF

INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 
'BORDEREAU_ORV', 'BORD. ORV', 20); 
INSERT INTO maracuja.TYPE_NUMEROTATION ( TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE ) VALUES ( 
'BORDEREAU_AOR', 'BORD. AOR', 21); 

UPDATE maracuja.TYPE_BORDEREAU SET tnu_ordre=20 WHERE tbo_ordre=8;
UPDATE maracuja.TYPE_BORDEREAU SET tbo_type='BTME' WHERE tbo_ordre=8;
UPDATE maracuja.TYPE_BORDEREAU SET tnu_ordre=21 WHERE tbo_ordre=9;

COMMIT;




INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.5.7',  SYSDATE, '');


commit;