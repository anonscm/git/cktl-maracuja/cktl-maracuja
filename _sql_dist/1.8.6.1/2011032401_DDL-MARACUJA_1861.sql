SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.6.1
-- Date de publication : 24/03/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Correction d'une vue pour abricot (edition des titres avec civilité sur nom debiteur)
-- Correction numeros de conventions RA pour les origines
-- correction d'une vue pour le cadre 2 du compte financier dans le cas ou un ordre de reversement était constitué de plusieurs enregistrements de depenses.
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.6.1',  null, '');
commit;
CREATE OR REPLACE PACKAGE MARACUJA.gestionOrigine IS

/*
ORI_ENTITE        USER.TABLE  -
ORI_KEY_NAME       PRIMARY KEY -
ORI_LIBELLE       LIBELLE UTILISATEUR -
ORI_ORDRE           ID -
ORI_KEY_ENTITE      VALEUR DE LA KEY -
TOP_ORDRE          type d origine -
*/

FUNCTION traiter_orgordre (orgordre INTEGER) RETURN INTEGER;
FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION traiter_convordre (convordre INTEGER) RETURN  INTEGER;
FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER) RETURN INTEGER;
PROCEDURE maj_origine;

END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Gestionorigine IS
--version 1.1.7 31/05/2005 - mise a jour libelle origine dans le cas des conventions
--version 1.1.6 27/04/2005 - ajout maj_origine
--version 1.2.0 20/12/2005 - Multi exercices jefy
--version 1.2.1 04/01/2006
-- version 1.5.6 29/03/2007 - corrections doublons

FUNCTION traiter_orgordre (orgordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

-- procedure obsolete

--            IF orgordre IS NULL THEN RETURN NULL; END IF;

--        -- recup du type_origine --
--        SELECT top_ordre INTO topordre FROM TYPE_OPERATION
--        WHERE top_libelle = 'OPERATION LUCRATIVE';



--        -- l origine est t elle deja  suivie --
--        SELECT COUNT(*)INTO cpt FROM ORIGINE
--        WHERE ORI_KEY_NAME = 'ORG_ORDRE'
--        AND ORI_ENTITE ='JEFY.ORGAN'
--        AND ORI_KEY_ENTITE    =orgordre;

--        IF cpt >= 1 THEN
--            SELECT ori_ordre INTO cpt FROM ORIGINE
--            WHERE ORI_KEY_NAME = 'ORG_ORDRE'
--            AND ORI_ENTITE ='JEFY.ORGAN'
--            AND ORI_KEY_ENTITE    =orgordre
--            AND ROWNUM=1;

--        ELSE
--            SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;

--            -- recup de la LIGNE BUDGETAIRE --
--            --le libelle utilisateur pour le suivie en compta --
--            SELECT org_comp||'-'||org_lbud||'-'||org_uc
--             INTO orilibelle
--            FROM jefy.organ
--            WHERE org_ordre = orgordre;

--            INSERT INTO ORIGINE
--            (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
--            VALUES ('JEFY.ORGAN','ORG_ORDRE',orilibelle,cpt,orgordre,topordre);

--        END IF;

--        RETURN cpt;
    return 0;

END;


FUNCTION traiter_orgid (orgid INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
convordre INTEGER;
typeOrgan v_organ_exer.TYOR_ID%TYPE;

BEGIN

           IF orgid IS NULL THEN RETURN NULL; END IF;

          SELECT tyor_id INTO typeOrgan FROM jefy_admin.organ WHERE org_id=orgid;

            SELECT COUNT(*) INTO cpt
                    FROM accords.CONVENTION_LIMITATIVE
                    WHERE org_id = orgid AND exe_ordre = exeordre;

          -- si c une convention RA
          IF (typeOrgan = 2 OR cpt>0) THEN
                     -- recup du type_origine CONVENTION RA--
                     SELECT top_ordre INTO topordre FROM TYPE_OPERATION
                     WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

                    SELECT COUNT(*) INTO cpt
                            FROM accords.CONVENTION_LIMITATIVE
                            WHERE org_id = orgid AND exe_ordre = exeordre;

                        -- si on a affaire a une convention RA identifiee dans convention
                        IF cpt >0 THEN
                                    -- recup de la convention
                                      SELECT MAX(con_ordre) INTO convordre
                                     FROM accords.CONVENTION_LIMITATIVE
                                     WHERE org_id = orgid
                                     AND exe_ordre = exeordre;

                                     SELECT SUBSTR(EXE_ORDRE ||'-'|| LPAD(CON_INDEX,4,'0') ||' '||CON_OBJET,1,2000)
                                     INTO orilibelle
                                     FROM accords.contrat
                                     WHERE con_ordre = convordre;

                        ELSE -- c une convention RA mais pas dans accords
                                     SELECT COUNT(*) INTO cpt
                                     FROM jefy_admin.organ
                                     WHERE org_id = orgid;

                                     IF cpt = 1 THEN
                                             --le libelle utilisateur pour le suivie en compta --
                                             SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
                                             INTO orilibelle
                                             FROM jefy_admin.organ
                                             WHERE org_id = orgid;

                                     ELSE -- pas d'organ trouvee
                                               RETURN NULL;
                                     END IF;
                            END IF;

          ELSE
          -- c pas une convention RA, c peut-etre une ligne lucrative
          -- a voir s'il ne faut pas la traiter d'une part en tant que convention RA et d'autre par en tant que lucrative si c le cas
                             -- recup du type_origine OPERATION LUCRATIVE --
                             SELECT top_ordre INTO topordre FROM TYPE_OPERATION
                             WHERE top_libelle = 'OPERATION LUCRATIVE';

                                              SELECT COUNT(*) INTO cpt
                                     FROM jefy_admin.organ
                                     WHERE org_id = orgid
                                     AND org_lucrativite = 1;

                                     IF cpt = 1 THEN
                                             --le libelle utilisateur pour le suivie en compta --
                                         SELECT SUBSTR(org_UB||'-'||org_CR||'-'||org_souscr || ' (' || org_lib  || ')',1,2000)
                                             INTO orilibelle
                                             FROM jefy_admin.organ
                                             WHERE org_id = orgid;

                                     ELSE -- pas d'organ lucrative trouvee
                                               RETURN NULL;
                                     END IF;
          END IF;

        -- l origine est t elle deja  suivie --
        SELECT COUNT(*) INTO cpt FROM ORIGINE
        WHERE ORI_KEY_NAME = 'ORG_ID'
        AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
        AND ORI_KEY_ENTITE    =orgid;

        IF cpt >= 1 THEN
            SELECT ori_ordre INTO cpt FROM ORIGINE
            WHERE ORI_KEY_NAME = 'ORG_ID'
            AND ORI_ENTITE ='JEFY_ADMIN.ORGAN'
            AND ORI_KEY_ENTITE    =orgid
            AND ROWNUM=1;

            --on met a jour le libelle
            UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

        ELSE
            SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
            INSERT INTO ORIGINE (ORI_ENTITE, ORI_KEY_NAME, ORI_LIBELLE, ORI_ORDRE, ORI_KEY_ENTITE, TOP_ORDRE)
            VALUES ('JEFY_ADMIN.ORGAN','ORG_ID',orilibelle,cpt,orgid,topordre);

        END IF;

        RETURN cpt;

END;


FUNCTION traiter_convordre (convordre INTEGER)
RETURN INTEGER

IS
topordre INTEGER;
cpt INTEGER;
orilibelle ORIGINE.ori_libelle%TYPE;
BEGIN

    IF convordre IS NULL THEN RETURN NULL; END IF;

    -- recup du type_origine --
    SELECT top_ordre INTO topordre FROM TYPE_OPERATION
    WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

    -- l origine est t elle deja  suivie --
    SELECT COUNT(*) INTO cpt FROM ORIGINE
        WHERE ORI_KEY_NAME = 'CONV_ORDRE'
        AND ORI_ENTITE ='CONVENTION.CONTRAT'
        AND ORI_KEY_ENTITE    =convordre;


    -- recup de la convention --
    --le libelle utilisateur pour le suivie en compta --
--    select CON_REFERENCE_EXTERNE||' '||CON_OBJET into orilibelle from convention.contrat where con_ordre=convordre;
--on change le libelle (demande INP Toulouse 30/05/2005)
--    SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM convention.contrat WHERE con_ordre=convordre;
        SELECT (EXE_ORDRE ||'-'|| LPAD(CON_INDEX,5,'0') ||' '||CON_OBJET) INTO orilibelle FROM accords.contrat WHERE con_ordre=convordre;


    -- l'origine est deja referencee
    IF cpt = 1 THEN
            SELECT ori_ordre INTO cpt FROM ORIGINE
            WHERE ORI_KEY_NAME = 'CONV_ORDRE'
            AND ORI_ENTITE ='CONVENTION.CONTRAT'
            AND ORI_KEY_ENTITE    =convordre;

            --on met a jour le libelle
            UPDATE ORIGINE SET ori_libelle=orilibelle WHERE ori_ordre=cpt;

    -- il faut creer l'origine
    ELSE
            SELECT origine_seq.NEXTVAL INTO cpt  FROM dual;
            INSERT INTO ORIGINE VALUES (
                    'CONVENTION.CONTRAT' ,--      USER.TABLE  -
                    'CONV_ORDRE',--       PRIMARY KEY -
                    ORILIBELLE,--       LIBELLE UTILISATEUR -
                    cpt ,--          ID -
                    convordre,--      VALEUR DE LA KEY -
                    topordre--          type d origine -
                );

    END IF;

    RETURN cpt;
END;


FUNCTION recup_type_bordereau_titre (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
--cpt          INTEGER;

--lebordereau jefy.bordero%ROWTYPE;

--agtordre     jefy.facture.agt_ordre%TYPE;
--borid         BORDEREAU.bor_id%TYPE;
--tboordre     BORDEREAU.tbo_ordre%TYPE;
--utlordre     UTILISATEUR.utl_ordre%TYPE;
--letitrejefy jefy.TITRE%ROWTYPE;
BEGIN

-- procedure obsolete
--        IF exeordre=2005 THEN
--            SELECT * INTO lebordereau
--            FROM jefy05.bordero
--            WHERE bor_ordre = borordre;

--            -- recup de l agent de ce bordereau --
--            SELECT * INTO letitrejefy FROM jefy05.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
--               FROM jefy05.TITRE
--               WHERE bor_ordre =lebordereau.bor_ordre);
--        ELSE
--            SELECT * INTO lebordereau
--            FROM jefy.bordero
--            WHERE bor_ordre = borordre;

--            -- recup de l agent de ce bordereau --
--            SELECT * INTO letitrejefy FROM jefy.TITRE WHERE tit_ordre IN (SELECT MAX(tit_ordre)
--               FROM jefy.TITRE
--               WHERE bor_ordre =lebordereau.bor_ordre);
--        END IF;




--        IF letitrejefy.tit_type ='V' THEN
--           SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REVERSEMENTS';
--        ELSE
--          IF letitrejefy.tit_type ='D' THEN
--            SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU DE REDUCTIONS';
--          ELSE
--            IF letitrejefy.tit_type ='P' THEN
--              SELECT tbo_ordre INTO tboordre FROM TYPE_BORDEREAU WHERE tbo_libelle='BORDEREAU D ORDRE DE PAIEMENT';
--            ELSE
--              SELECT tbo_ordre INTO tboordre
--              FROM TYPE_BORDEREAU
--              WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTTE GENERIQUE' AND EXE_ORDRE=exeordre);
--            END IF;
--          END IF;
--        END IF;


--        RETURN tboordre;
    return 0;
END;

FUNCTION recup_type_bordereau_mandat (borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS

--cpt          INTEGER;

--lebordereau jefy.bordero%ROWTYPE;
--fouordrepapaye jefy.MANDAT.fou_ordre%TYPE;
--agtordre     jefy.facture.agt_ordre%TYPE;
--fouordre     jefy.MANDAT.fou_ordre%TYPE;
--borid         BORDEREAU.bor_id%TYPE;
--tboordre     BORDEREAU.tbo_ordre%TYPE;
--utlordre     UTILISATEUR.utl_ordre%TYPE;
--etat        jefy.bordero.bor_stat%TYPE;
BEGIN

--        IF exeordre=2005 THEN
--            -- recup des infos --
--            SELECT * INTO lebordereau
--            FROM jefy05.bordero
--            WHERE bor_ordre = borordre;

--            -- recup de l agent de ce bordereau --
--            SELECT MAX(agt_ordre) INTO agtordre
--            FROM jefy05.facture
--            WHERE man_ordre =
--             ( SELECT MAX(man_ordre)
--               FROM jefy05.MANDAT
--               WHERE bor_ordre =borordre
--              );

--            SELECT MAX(fou_ordre) INTO fouordre
--               FROM jefy05.MANDAT
--               WHERE bor_ordre =borordre;
--        ELSE
--            -- recup des infos --
--            SELECT * INTO lebordereau
--            FROM jefy.bordero
--            WHERE bor_ordre = borordre;

--            -- recup de l agent de ce bordereau --
--            SELECT MAX(agt_ordre) INTO agtordre
--            FROM jefy.facture
--            WHERE man_ordre =
--             ( SELECT MAX(man_ordre)
--               FROM jefy.MANDAT
--               WHERE bor_ordre =borordre
--              );

--            SELECT MAX(fou_ordre) INTO fouordre
--               FROM jefy.MANDAT
--               WHERE bor_ordre =borordre;
--        END IF;

--        -- recuperation du type de bordereau --
--        SELECT COUNT(*) INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
--         (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');


--        IF (fouordrepapaye <> 0) THEN
--           SELECT fou_ordre INTO fouordrepapaye FROM v_fournisseur WHERE fou_code IN
--           (SELECT NVL(param_value,-1) FROM papaye.paye_parametres WHERE param_key='FOURNISSEUR_PAPAYE');
--        ELSE
--            fouordrepapaye := NULL;
--        END IF;



--        IF fouordre =fouordrepapaye THEN
--          SELECT tbo_ordre INTO tboordre
--          FROM TYPE_BORDEREAU
--          WHERE tbo_libelle='BORDEREAU DE MANDAT SALAIRES';
--        ELSE
--          SELECT COUNT(*) INTO cpt
--          FROM OLD_AGENT_TYPE_BORD
--          WHERE agt_ordre = agtordre;

--          IF cpt <> 0 THEN
--           SELECT tbo_ordre INTO tboordre
--           FROM OLD_AGENT_TYPE_BORD
--           WHERE agt_ordre = agtordre;
--          ELSE
--           SELECT tbo_ordre INTO tboordre
--           FROM TYPE_BORDEREAU
--           WHERE tbo_ordre = (SELECT par_value FROM PARAMETRE WHERE par_key ='BTME GENERIQUE' AND exe_ordre = exeordre);
--          END IF;

--        END IF;


--        RETURN tboordre;
    return 0;
END;

FUNCTION recup_utilisateur_bordereau(borordre INTEGER,exeordre INTEGER)
RETURN INTEGER
IS
BEGIN

RETURN 1;
END;


PROCEDURE Maj_Origine
IS
  -- cursor pour recuperer les organ lucratif + conventions RA
    CURSOR c1 IS
    SELECT org_id, exe_ordre FROM v_organ_exer o
    WHERE  exe_ordre>2006 AND (org_lucrativite !=0 OR tyor_id=2);

    CURSOR c2 IS
    SELECT org_id, exe_ordre FROM accords.convention_limitative WHERE org_id>0;
    orgordre INTEGER;
    conordre INTEGER;
    exeordre INTEGER;
    cpt INTEGER;
BEGIN
        -- On balaye les organ lucratives + taggees RA
        OPEN c1;
        LOOP
            FETCH C1 INTO orgordre, exeordre;
                  EXIT WHEN c1%NOTFOUND;
                  cpt := TRAITER_ORGid ( orgordre , exeordre);
        END LOOP;
        CLOSE c1;

        -- on balaye les conventions RA
        OPEN c2;
        LOOP
            FETCH C2 INTO orgordre, exeordre;
                  EXIT WHEN c2%NOTFOUND;
                  cpt := TRAITER_ORGid ( orgordre , exeordre);
        END LOOP;
        CLOSE c2;

-- Les conventions RA sont maintenant gerees a partir d'un Org_id
--         OPEN c2;
--         LOOP
--             FETCH C2 INTO conordre;
--                   EXIT WHEN c2%NOTFOUND;
--                   cpt := TRAITER_CONVORDRE ( conordre );
--         END LOOP;
--         CLOSE c2;


END;


END;
/



CREATE OR REPLACE FORCE VIEW maracuja.v_edition_titre (pco_num,
                                                       exe_ordre,
                                                       tbo_ordre,
                                                       bor_id,
                                                       bor_num,
                                                       tit_id,
                                                       tit_nb_piece,
                                                       tit_numero,
                                                       rec_numero,
                                                       tit_ht,
                                                       tit_ttc,
                                                       tit_tva,
                                                       ges_code,
                                                       ligne_budg,
                                                       org_lib,
                                                       tit_date,
                                                       pco_chapitre,
                                                       debiteur,
                                                       fou_ordre,
                                                       tit_libelle,
                                                       montant_lettre,
                                                       tit_adresse,
                                                       fac_numero
                                                      )
AS
   SELECT t.pco_num, b.exe_ordre, b.tbo_ordre, b.bor_id, bor_num, t.tit_id,
          tit_nb_piece, tit_numero, rec_numero, tit_ht, tit_ttc, tit_tva,
          o.org_ub, o.org_ub || ' ' || o.org_cr || ' ' || o.org_souscr,
          o.org_lib, bor_date_creation, pco_chapitre,
             DECODE (f.adr_civilite, 'STR', '', f.adr_civilite || ' ')
          || f.adr_nom
          || ' '
          || f.adr_prenom,
          t.fou_ordre, tit_libelle,
          maracuja.number_to_lettres.transformer (ABS (tit_ttc)),
             adr_adresse1
          || '-'
          || adr_adresse2
          || ' '
          || CHR (13)
          || adr_cp
          || ' '
          || adr_ville
          || DECODE (cp_etranger, NULL, '', ' ' || cp_etranger)
          || DECODE (lc_pays, NULL, '', 'FRANCE', '', ' - ' || lc_pays),
          NVL (fp.fap_numero, 0)
     FROM bordereau b,
          titre t,
          recette r,
          v_organ_exer o,
          v_planco_chapitre p,
          v_fournisseur f,
          jefy_recette.recette_ctrl_planco rpco,
          jefy_recette.recette_budget rb,
          jefy_recette.facture_budget fb,
          jefy_recette.facture_papier fp
    WHERE b.bor_id = t.bor_id
      AND t.tit_id = r.tit_id
      AND brj_ordre IS NULL
      AND r.org_ordre = o.org_id
      AND r.exe_ordre = o.exe_ordre
      AND r.pco_num = p.pco_num
      AND f.fou_ordre = t.fou_ordre
      AND r.tit_id = rpco.tit_id(+)
      AND r.rec_ordre = rpco.rpco_id(+)
      AND rpco.rec_id = rb.rec_id(+)
      AND rb.fac_id = fb.fac_id(+)
      AND fb.fac_id = fp.fac_id(+);
/

CREATE OR REPLACE PACKAGE MARACUJA.API_BILAN AS

  --FUNCTION add(Param1 IN NUMBER) RETURN NUMBER;
--  PROCEDURE addPlancoBilanPoste(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--        pbpSigne planco_bilan_poste.pbp_signe%type,
--        pbpSens planco_bilan_poste.pbp_sens%type,
--        pconum plan_comptable_exer.pco_num%type,
--        pbpSubDiv planco_bilan_poste.pbp_subdiv%type,
--        pbpExceptSubdiv planco_bilan_poste.PBP_except_subdiv%type,
--        pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

--  procedure deletePlancoBilanPostes(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--         );
--

--  PROCEDURE setPlancoBilanFormule(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         formule varchar2,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
        );

        -- Calcule le poste pour l'exercice en fonction de la formule trouvÃ©e
--  FUNCTION calcPoste(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type,
--              gesCode gestion.ges_code%type,
--              isSacd char
--      )
--      RETURN NUMBER;
    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, gescode VARCHAR2, sacd CHAR, vue VARCHAR2) RETURN VARCHAR2;

  FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              gesCode gestion.ges_code%TYPE,
              isSacd CHAR,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2
      )
      RETURN NUMBER;

--    FUNCTION prv_getCritSqlForExcept(
--              compte VARCHAR2,
--            pbpSubdiv PLANCO_BILAN_POSTE.PBP_SUBDIV%type
--      ) return varchar2;
    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 ;

--  FUNCTION getFormule(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type
--      )
--      RETURN varchar2;

   PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE, exeOrdre bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, sacd CHAR);
   PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR);
   PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR);
   PROCEDURE checkFormule(formule VARCHAR2);

   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER);
   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE);
   
   function checkComptesNotInFormules(exeOrdre integer, pcoRacine varchar2) return varchar2;

END API_BILAN;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.API_BILAN AS

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
  ) IS
    btId bilan_type.bt_id%TYPE;
  BEGIN
    SELECT bt_id INTO btId FROM bilan_type WHERE bt_str_id=btstrid;
    UPDATE bilan_poste SET BP_FORMULE_MONTANT=formuleMontant, BP_FORMULE_AMORTISSEMENT=formuleAmortissement WHERE bt_id=btId AND exe_ordre=exeOrdre AND bp_str_id=bpStrId;
  END setBilanPosteFormule;


    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, gescode VARCHAR2, sacd CHAR, vue VARCHAR2) RETURN VARCHAR2 IS
    signe VARCHAR2(1);
    signeNum INTEGER;
    sens VARCHAR2(2);
    compteext VARCHAR2(2000);
    critStrPlus VARCHAR2(2000);
    res VARCHAR2(4000);
    LC$req VARCHAR2(4000);
    lavue VARCHAR2(50);
   BEGIN
        res := '';
        IF (formule IS NOT NULL AND LENGTH(formule)>0) THEN
             dbms_output.put_line('traitement de ='|| formule);
            signe := SUBSTR(formule,1,1); -- +
            signeNum := 1;
            IF (signe='-') THEN
                signeNum := -1;
            END IF;
            sens :=  SUBSTR(formule,2,2); -- SD
            IF (sens IS NULL OR sens NOT IN ('SD','SC')) THEN
                 RAISE_APPLICATION_ERROR (-20001, formule ||' : ' ||sens||' : le sens doit etre SD ou SC ');
            END IF;
            compteext := SUBSTR(formule,4); -- 4012 ou (4012-40125-40126)
            --subdivStr := '';
            IF (LENGTH(compteext)=0) THEN
                RAISE_APPLICATION_ERROR (-20001, 'compte non recupere dans la formule '|| formule);
            END IF;
            critStrPlus := prv_getCritSqlForSingleFormule(compteext);
           -- sInstructionReelle := sInstructionReelle || ' ' || signe || sens || '(' || critStrPlus ||')';

            dbms_output.put_line('exeordre='|| exeordre);
            dbms_output.put_line('critStrPlus='||critStrPlus);
            dbms_output.put_line('sens='||sens);
            dbms_output.put_line('sacd='||sacd);

            lavue := vue;
            IF lavue IS NULL THEN
              lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
            END IF;

           IF sens = 'SD'   THEN
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           ELSE
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           END IF;
           dbms_output.put_line('req = '|| LC$req  );
            res := LC$req;
        END IF;
        RETURN res;
   END getSoldeReq;

    -- Calcule le montant d'apres une formule de type SDxxx + SC YYY etc.
    FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              gesCode gestion.ges_code%TYPE,
              isSacd CHAR,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2
      )
      RETURN NUMBER IS

    str VARCHAR2(4000);
    compteext VARCHAR2(250);
    --subdivStr planco_bilan_poste.PBP_EXCEPT_SUBDIV%type;
    pos INTEGER;
    nb INTEGER;
    i INTEGER;
    j INTEGER;
    nextS INTEGER;
    signe VARCHAR2(10);
    sens VARCHAR2(10);
    pconum plan_comptable_exer.pco_num%TYPE;
    subdiv plan_comptable_exer.pco_num%TYPE;
    v_array ZtableForSplit;
    --v_arrayExt ZtableForSplit;
    --varray_comptesPlus ZtableForSplit;
    sinstruction VARCHAR2(255);
    --critStrPlus varchar2(2000);
    resTmp NUMBER;
    signeNum NUMBER;
    sInstructionReelle VARCHAR2(4000);
    req VARCHAR2(4000);
    solde NUMBER;
  BEGIN
    /* une formule est composÃ©es de suites de :
            Signe Sens Compte (par exemple - SD 201 ou + SD472)
    */
dbms_output.put_line('');
dbms_output.put_line('Formule brute = ' || formule);
    resTmp := 0;
    sqlReq := '';
    sInstructionReelle := '';
    -- analyser la formule
    -- supprimer tous les espaces
    str := REPLACE(formule, ' ', '');

    IF (LENGTH(str)=0) THEN
     RAISE_APPLICATION_ERROR (-20001, 'formule vide' );
    END IF;

    str := UPPER(str);

    IF (SUBSTR(str,1,1) = 'S') THEN
        str := '+'||str;
    END IF;

    str := REPLACE(str, '+S', '|+S');
    str := REPLACE(str, '-S', '|-S');
    str := SUBSTR(str,2);
dbms_output.put_line('Formule analysee = '|| formule);
    v_array := str2tbl(str, '|');
    FOR i IN 1 .. v_array.COUNT LOOP
        sinstruction := v_array(i);
        sinstruction := trim(sinstruction);
        signe := SUBSTR(sinstruction,1,1); -- +
        signeNum := 1;
        IF (signe='-') THEN
            signeNum := -1;
        END IF;
        dbms_output.put_line('sinstruction = '|| sinstruction);
        req := getSoldeReq(sinstruction, exeOrdre, gescode, issacd,'MARACUJA.cfi_ecritures');
        dbms_output.put_line('REQ = '|| req);
        IF ( NVL(faireCalcul, 1)=1) THEN
            EXECUTE IMMEDIATE req INTO solde ;
        END IF;

        IF (solde IS NULL) THEN
            solde := 0;
        END IF;
        IF solde < 0
           THEN solde := 0;
        END IF;
        resTmp := resTmp + signenum*solde;
        sqlReq := sqlReq || ' ' || signe || req;
    END LOOP;
    --dbms_output.put_line('Formule = '|| formule);
    --dbms_output.put_line('Instruction reelle = '|| req);
    RETURN resTmp;
    END getSoldeMontant;





    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 IS
            varray_comptesPlus ZtableForSplit;
            varray_comptesMoins ZtableForSplit;
            pconum VARCHAR2(2000);
            critStrMoins VARCHAR2(2000);
            critStrPlus VARCHAR2(2000);
            str VARCHAR2(2000);
            pconumTmp VARCHAR2(2000);
            subdiv VARCHAR2(1000);
    BEGIN
            str := formule;
            -- traite les formules du genre (53 + 54 -541 -542 + 55)
            -- on nettoie les Ã©ventuelles parenthese de debut / fin
            IF (SUBSTR(str,1,1) = '(') THEN
               --cas complexe (avec parentheses : ex SD(53 + 54 -541 -542 + 55)
               IF (SUBSTR(str,LENGTH(str),1) <> ')' ) THEN
                RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(str,LENGTH(str),1)) || ': '|| formule);
               END IF;
               str := SUBSTR(str,2,LENGTH(str)-2);
             END IF;

            -- separer les + (pour traiter (53 | 54 -541 -542 | 55) )
            varray_comptesPlus := str2tbl(str, '+');
            critStrPLus := '';
            FOR z IN 1 ..  varray_comptesPlus.COUNT LOOP
                pconum :=  varray_comptesPlus(z);
                -- pconum peut etre de la forme 54 ou bien (54-541-542) ou bien 54-541-542
                -- si parentheses debut/fin, on les vire
                IF (SUBSTR(pconum,1,1)='(' ) THEN
                    IF (SUBSTR(pconum,LENGTH(pconum),1) <> ')' ) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(pconum,LENGTH(pconum),1)) || ': '|| formule);
                    END IF;
                    pconum := SUBSTR(pconum,2,LENGTH(pconum)-2);
                    IF (INSTR(pconum,'(' )>0) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non générée ici :'|| pconum || ': '|| formule);
                    END IF;
                END IF;
                IF (INSTR(pconum,')' )>0) THEN
                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non gérée ici :'|| pconum || ': '|| formule);
                END IF;
                -- on separe les elements exclus
                critStrMoins := '';
                varray_comptesMoins :=  str2tbl(pconum, '-');
                FOR j IN 1 .. varray_comptesMoins.COUNT LOOP
                   IF (j=1) THEN
                            pconumTmp := varray_comptesMoins(j);
                            critStrMoins := 'pco_num like '''|| pconumTmp || '%'' ';
                            dbms_output.put_line(pconumTmp);
                   ELSE
                            subdiv := varray_comptesMoins(j);
                            dbms_output.put_line(subdiv);
                            IF (LENGTH(subdiv)>0) THEN
                                IF (subdiv NOT LIKE pconumTmp||'%') THEN
                                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, seules les subdivisions du compte initial sont autorisÃ©es : '|| pconum);
                                END IF;

                                critStrMoins := critStrMoins || ' and pco_num not like '''|| subdiv || '%'' ';
                            END IF;
                   END IF;
                END LOOP;
                IF (LENGTH(critStrMoins)>0) THEN
                    critStrMoins := '(' || critStrMoins ||')';
                    IF (LENGTH(critStrPlus)>0) THEN
                        critStrPlus := critStrPlus || ' or ';
                    END IF;
                    critStrPlus := critStrPlus || critStrMoins;
                END IF;

            END LOOP;

            RETURN critStrPlus;

    END;


 PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR) AS
    bp_id0 bilan_poste.bp_id%TYPE;
    bp_id1 bilan_poste.bp_id%TYPE;
    bp_id2 bilan_poste.bp_id%TYPE;
    rec_bp1 bilan_poste%ROWTYPE;
    rec_bp2 bilan_poste%ROWTYPE;
    rec_bp3 bilan_poste%ROWTYPE;
    bilan_groupe1 VARCHAR2(1000);
    bilan_groupe2 VARCHAR2(1000);
    bilan_libelle VARCHAR2(1000);
    bilan_montant NUMBER;
     bilan_amort NUMBER;
    bpStrId bilan_poste.bp_str_id%TYPE;
    formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
    formuleAmortissement bilan_poste.BP_FORMULE_AMORTISSEMENT%TYPE;
    CURSOR carbre IS
            SELECT *
            FROM bilan_poste
            WHERE exe_ordre=exeOrdre
            CONNECT BY PRIOR BP_ID = bp_id_pere
            START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bp_str_id=bpStrId);

    CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
    CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
    CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
    sqlreq VARCHAR2(4000);
   flag INTEGER;

   BEGIN
        bpStrId := 'actif';

        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (gesCode IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' gesCode obligatoire');
        END IF;
        IF (isSacd IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' isSacd obligatoire');
        END IF;
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  exercice= '|| exeordre || ', type='||btId);
        END IF;

        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;

--*************** CREATION TABLE ACTIF *********************************
        IF issacd = 'O' THEN
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSIF issacd = 'G' THEN
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        ELSE
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                    dbms_output.put_line('rec_bp1.bp_libelle = '|| rec_bp1.bp_id || ' / '|| rec_bp1.bp_libelle );
                    dbms_output.put_line('rec_bp2.bp_libelle = '|| rec_bp2.bp_id || ' / '||rec_bp2.bp_libelle );
                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                            formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq);
                             
                            bilan_amort := 0;
                            IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq);
                            END IF;
                            INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant+bilan_amort, 0, gescode,exeordre);
                            end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                                    formuleAmortissement := rec_bp3.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                                      gesCode,
                                                      isSacd,
                                                      1,
                                                      sqlReq);
                                    bilan_amort := 0;
                                    IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                        bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                                      gesCode,
                                                      isSacd,
                                                      1,
                                                      sqlReq);
                                    END IF;
                                    INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant-bilan_amort, 0, gescode,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;


    PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR) AS
        bp_id0 bilan_poste.bp_id%TYPE;
        bp_id1 bilan_poste.bp_id%TYPE;
        bp_id2 bilan_poste.bp_id%TYPE;
        rec_bp1 bilan_poste%ROWTYPE;
        rec_bp2 bilan_poste%ROWTYPE;
        rec_bp3 bilan_poste%ROWTYPE;
        bilan_groupe1 VARCHAR2(1000);
        bilan_groupe2 VARCHAR2(1000);
        bilan_libelle VARCHAR2(1000);
        bilan_montant NUMBER;
        -- bilan_amort number;
         formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
        bpStrId bilan_poste.bp_str_id%TYPE;
        CURSOR carbre IS
                SELECT *
                FROM bilan_poste
                WHERE exe_ordre=exeOrdre
                CONNECT BY PRIOR BP_ID = bp_id_pere
                START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bt_id=btId AND bp_str_id=bpStrId);

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
        CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
        CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
        sqlreq VARCHAR2(4000);
        flag INTEGER;


   BEGIN
        bpStrId := 'passif';
        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (gesCode IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' gesCode obligatoire');
        END IF;
        IF (isSacd IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' isSacd obligatoire');
        END IF;

        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  : '|| exeordre || ', type '||btId);
        END IF;
        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId;
        IF issacd = 'O' THEN
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSIF issacd = 'G' THEN
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        ELSE
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;

                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlreq);


                            INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                      end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq);


                                    INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;

    PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE,exeOrdre bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, sacd CHAR) IS
    BEGIN
         prepare_bilan_actif(btId, exeOrdre, gescode, sacd) ;
         prepare_bilan_passif(btId, exeOrdre, gescode, sacd) ;
    END;




    PROCEDURE checkFormule(formule VARCHAR2) IS
        sqlReq VARCHAR2(4000);
        res NUMBER;
    BEGIN
        res := getSoldeMontant(2010, formule, 'AGREGE','G', 0,sqlReq );

        RETURN;
    END checkFormule;


   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER) IS
        rec_bp bilan_poste%ROWTYPE;
        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeordreOld AND BP_ID_PERE IS NULL;
        bpIdNew bilan_poste.bp_id%TYPE;
        flag INTEGER;

   BEGIN
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordreNew AND BP_ID_PERE IS NULL;
        IF (flag >0) THEN
            RAISE_APPLICATION_ERROR (-20001, 'Bilan deja cree pour   : '|| exeordreNew);
        END IF;


       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
            SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;

             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   NULL, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );

            prv_duplicateExercice(exeOrdreNew,rec_bp.bp_id, bpIdNew);
        END LOOP;
        CLOSE c1;
   END;

   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE) IS
        --bp_idPere bilan_poste.bp_id%type;
        rec_bp bilan_poste%ROWTYPE;
        bpIdNew bilan_poste.bp_id%TYPE;

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE bp_id_Pere = racineOld;
   BEGIN




       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
             SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;


             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   racineNew, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );
             prv_duplicateExercice(exeordreNew, rec_bp.bp_id, bpIdNew);


           END LOOP;
       CLOSE c1;

   END prv_duplicateExercice;


    function checkComptesNotInFormules(exeOrdre integer, pcoRacine varchar2) return varchar2
    is  
        cursor c1 is select distinct pco_num from ecriture_detail where exe_ordre=exeOrdre and pco_num like pcoRacine||'%'; 
        cursor c2 is select bp_formule_montant from bilan_poste where exe_ordre=exeOrdre and length(nvl(bp_formule_montant,''))>1 union all select bp_formule_amortissement from bilan_poste where exe_ordre=exeOrdre and length(nvl(bp_formule_amortissement,''))>1;
        pcoNum plan_comptable_exer.pco_num%type; 
        spco varchar2(500); 
        formule bilan_poste.BP_FORMULE_MONTANT%type;
        formuleClean bilan_poste.BP_FORMULE_MONTANT%type;
        v_array ZtableForSplit;
        flag smallint;
        res varchar2(4000);
    begin
        -- verifier si les comptes présents dans les écritures sont tous pris en compte par le bilan
        res := '';
    
       OPEN c1;
           LOOP
             FETCH C1 INTO pcoNum;
               EXIT WHEN c1%NOTFOUND;
             flag := 0;
             
             OPEN c2;
               LOOP
                 FETCH C2 INTO formule;
                   EXIT WHEN c2%NOTFOUND;
                   -- on recupere les comptes utilises dans la formule
                  --dbms_output.put_line('formule = '|| formule );
                 formuleClean := REPLACE(formule, 'SD', ' ');
                 formuleClean := REPLACE(formuleClean, 'SC', ' ');
                 formuleClean := REPLACE(formuleClean, '-', ' ');
                 formuleClean := REPLACE(formuleClean, '+', ' ');
                 formuleClean := REPLACE(formuleClean, '(', ' ');
                 formuleClean := REPLACE(formuleClean, ')', ' ');
                 v_array := str2tbl(formuleClean, ' ');
                 FOR i IN 1 .. v_array.COUNT LOOP
                   spco := v_array(i);
                   spco := trim(spco); 
                   if (length(spco)>0) then
                       --dbms_output.put('pcoNum / spco = '||pcoNum ||' / '|| spco );
                       if (pcoNum like spco||'%') then
                        flag := 1;
                        --dbms_output.put_line('ok ' );
                        exit;
                       end if; 
                        --dbms_output.put_line('ko ' );
                   end if;
                 END LOOP;
                 
                 if (flag =0) then
                    dbms_output.put_line(pconum || ' non trouve dans ' || formule );
                 else
                    dbms_output.put_line(pconum || ' TROUVE dans ' || formule );
                    exit;
                 
                 end if;
                 
                 
                 
                 
                  END LOOP;
                 --dbms_output.put_line(' ' );
               CLOSE c2;
               
               
               
               if (flag =0) then
                 
                res := res || pcoNum || ',';
                if (length(res)>=4000) then
                    RAISE_APPLICATION_ERROR (-20001, 'Comptes non pris en compte dans le bilan : '||res);
                end if;
               end if;
               
                END LOOP;
               
          
       CLOSE c1;
        
        return res;
    
    end;


END API_BILAN;
/



CREATE OR REPLACE FORCE VIEW comptefi.v_dvlop_dep_liste (exe_ordre,
                                                         ges_code,
                                                         section,
                                                         pco_num_bdn,
                                                         pco_num,
                                                         mandats,
                                                         reversements,
                                                         cr_ouverts,
                                                         dep_date,
                                                         section_lib,
                                                         ordre_presentation
                                                        )
AS
   SELECT   m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num,
            SUM (dep_ht), 0, 0, e.date_saisie, s.section_lib,
            s.ordre_presentation
       FROM maracuja.mandat m,
            maracuja.bordereau b,
            comptefi.v_planco p,
            maracuja.v_depense_tcd dtcd,
            maracuja.depense dep,
            comptefi.v_section s,
            (SELECT   m.man_id, MIN (e.ecr_date_saisie) date_saisie
                 FROM maracuja.ecriture_detail ecd,
                      maracuja.mandat_detail_ecriture mde,
                      maracuja.ecriture e,
                      maracuja.mandat m
                WHERE m.man_id = mde.man_id
                  AND mde.ecd_ordre = ecd.ecd_ordre
                  AND ecd.ecr_ordre = e.ecr_ordre
             GROUP BY m.man_id) e
      WHERE m.pco_num = p.pco_num
        AND m.bor_id = b.bor_id
        AND m.man_id = e.man_id
        AND m.man_id = dep.man_id
        AND dep.dep_id = dtcd.dep_id
        AND dtcd.tcd_sect = s.section
        AND s.section_type = 'D'
        AND tbo_ordre <> 8
        AND tbo_ordre <> 21
        AND tbo_ordre <> 18
        AND tbo_ordre <> 16
        AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
   GROUP BY m.exe_ordre,
            m.ges_code,
            s.section,
            p.pco_num_bdn,
            m.pco_num,
            e.date_saisie,
            s.section_lib,
            s.ordre_presentation
   UNION ALL
-- ordre de reversement avant 2007
   SELECT   t.exe_ordre, t.ges_code, p.section, p.pco_num_bdn, t.pco_num, 0,
            SUM (tit_ht), 0, e.date_saisie, p.section_lib,
            p.ordre_presentation
       FROM maracuja.titre t,
            maracuja.bordereau b,
            comptefi.v_planco p,
            (SELECT   m.tit_id, MIN (e.ecr_date_saisie) date_saisie
                 FROM maracuja.ecriture_detail ecd,
                      maracuja.titre_detail_ecriture mde,
                      maracuja.ecriture e,
                      maracuja.titre m
                WHERE m.tit_id = mde.tit_id
                  AND mde.ecd_ordre = ecd.ecd_ordre
                  AND ecd.ecr_ordre = e.ecr_ordre
             GROUP BY m.tit_id) e
      WHERE t.pco_num = p.pco_num
        AND t.bor_id = b.bor_id
        AND tbo_ordre = 8
        AND t.tit_etat = 'VISE'
        AND t.tit_id = e.tit_id
   GROUP BY t.exe_ordre,
            t.ges_code,
            p.section,
            p.pco_num_bdn,
            t.pco_num,
            e.date_saisie,
            p.section_lib,
            p.ordre_presentation
   UNION ALL
-- ordre de reversement à partir de 2007
   SELECT   m.exe_ordre, m.ges_code, s.section, p.pco_num_bdn, m.pco_num, 0,
            -SUM (dep_ht), 0, e.date_saisie, s.section_lib,
            s.ordre_presentation
       FROM maracuja.mandat m,
            maracuja.bordereau b,
            comptefi.v_planco p,
            maracuja.v_depense_tcd dtcd,
            maracuja.depense dep,
            comptefi.v_section s,
            (SELECT   m.man_id, MIN (e.ecr_date_saisie) date_saisie
                 FROM maracuja.ecriture_detail ecd,
                      maracuja.mandat_detail_ecriture mde,
                      maracuja.ecriture e,
                      maracuja.mandat m
                WHERE m.man_id = mde.man_id
                  AND mde.ecd_ordre = ecd.ecd_ordre
                  AND ecd.ecr_ordre = e.ecr_ordre
             GROUP BY m.man_id) e
      WHERE m.pco_num = p.pco_num
        AND m.bor_id = b.bor_id
        AND m.man_id = e.man_id
        AND m.man_id = dep.man_id
        AND dep.dep_id = dtcd.dep_id
        AND dtcd.tcd_sect = s.section
        AND s.section_type = 'D'
        AND (tbo_ordre = 8 OR tbo_ordre = 18 OR tbo_ordre = 21)
        AND (m.man_etat = 'VISE' OR m.man_etat = 'PAYE')
   GROUP BY m.exe_ordre,
            m.ges_code,
            s.section,
            p.pco_num_bdn,
            m.pco_num,
            e.date_saisie,
            s.section_lib,
            s.ordre_presentation
   UNION ALL
-- Crédits ouverts
   SELECT exe_ordre, ges_code, bdn_section section, pco_num, pco_num, 0, 0,
          co, date_co, s.section_lib, s.ordre_presentation
     FROM comptefi.v_budnat2, comptefi.v_section s
    WHERE co <> 0
      AND bdn_quoi = 'D'
      AND bdn_section = s.section
      AND s.section_type = 'D';
/






update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.6.1';       
commit;