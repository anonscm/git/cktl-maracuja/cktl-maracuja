SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/3
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.1
-- Date de publication : 01/02/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Mise a jour prepare_exercice pour se conformer a la nouvelle structure de mode_paiement
-- nettoyage plus poussé que dans le script 1.9.1.0 et re-creation des index et contraintes de references
-- NB : si vous rencontrez des erreurs dans le patch précédent dans les scripts 2012013103_DML-MARACUJA_1910.sql et 2012013104_DDL-MARACUJA_1910.sql, passez directement à ce patch (qui intègre les modifications des 2 derniers fichiers du patch 1.9.1.0). 

----------------------------------------------





INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.9.1.1',  null, '');
commit;








--**************---
CREATE OR REPLACE PROCEDURE MARACUJA.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
--
--
    --nouvelExercice  EXERICE.exe_exercice%TYPE;
    precedentExercice EXERCICE.exe_exercice%TYPE;
    flag NUMBER;

    rec_agregat gestion_agregat%rowtype;
    gaid gestion_agregat.ga_id%type;

    CURSOR cAgregats is
        SELECT G.GA_ID, G.EXE_ORDRE, G.GA_LIBELLE, G.GA_DESCRIPTION FROM MARACUJA.GESTION_AGREGAT G where g.exe_ordre=precedentExercice;


BEGIN

    precedentExercice := nouvelExercice - 1;
    
    -- -------------------------------------------------------
    -- Vérifications concernant l'exercice precedent
    
    
    -- Verif que l'exercice precedent existe
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    --  -------------------------------------------------------
    -- Preparation des parametres
    INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
                             FROM PARAMETRE
                          WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des codes gestion
    INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185, PCO_NUM_185_CTP_SACD
                                    FROM GESTION_EXERCICE
                                 WHERE exe_ordre=precedentExercice) ;
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de paiement
    INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
                                 MOD_VISA_TYPE, MOD_EMA_AUTO, MOD_CONTREPARTIE_GESTION, PCO_NUM_TVA,PCO_NUM_TVA_CTP, MOD_PAIEMENT_HT 
                                 FROM MODE_PAIEMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de recouvrement
    INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE,
    MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
                                 FROM MODE_RECOUVREMENT
                              WHERE exe_ordre=precedentExercice);
    

    --  -------------------------------------------------------
    -- Récupération des plan comptables
    INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
SELECT 
MARACUJA.PLAN_COMPTABLE_EXER_SEQ.NEXTVAL, nouvelExercice, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
FROM MARACUJA.PLAN_COMPTABLE_EXER where exe_ordre=precedentExercice;
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_credit
    -- les nouveaux types de credit doivent exister
    INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
    SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
    FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
     FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
    WHERE
    tcold.exe_ordre=precedentExercice
    AND tcnew.exe_ordre=nouvelExercice
    AND tcold.tcd_code=tcnew.tcd_code
    AND tcnew.tyet_id=1
    AND tcold.tcd_type=tcnew.tcd_type
    ) x
    WHERE
    pcc.tcd_ordre=x.tcd_ordre_old
    AND pcc.pcc_etat='VALIDE'
    );
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_amortissment
    INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM, PCA_DUREE) (
    SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, nouvelExercice, PCO_NUM, PCA_DUREE
    FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
    WHERE exe_ordre=precedentExercice
    AND p.PCOA_ID = a.PCOA_ID
    AND a.TYET_ID=1
    );

    --  -------------------------------------------------------
    -- Récupération des planco_visa
    INSERT INTO maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) (
    SELECT PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, nouvelExercice
    FROM maracuja.planco_visa p
    WHERE exe_ordre=precedentExercice    
    AND p.PVI_ETAT='VALIDE'
    );
 

    -- --------------------------------------------------------
    -- codes budgets pour les epn
    insert into epn.code_budget_sacd(sacd, cod_bud, exe_ordre) 
        select sacd, cod_bud, nouvelExercice from epn.code_budget_sacd where exe_ordre=precedentExercice; 


    -- formules pour le calcul du bilan 
    MARACUJA.API_BILAN.DUPLICATEEXERCICE ( precedentExercice, nouvelExercice ); 
   
    ------
    OPEN cAgregats;
      LOOP
         FETCH cAgregats INTO rec_agregat;
               EXIT WHEN cAgregats%NOTFOUND;
         SELECT GESTION_AGREGAT_SEQ.NEXTVAL into gaid from dual;
         INSERT INTO MARACUJA.GESTION_AGREGAT ( GA_ID, EXE_ORDRE, GA_LIBELLE, GA_DESCRIPTION) 
            VALUES (
                gaid, --GA_ID, 
                nouvelExercice, --EXE_ORDRE, 
                rec_agregat.ga_libelle,--GA_LIBELLE, 
                rec_agregat.ga_description --GA_DESCRIPTION
                );
         INSERT INTO MARACUJA.GESTION_AGREGAT_REPART (GAR_ID, GA_ID, EXE_ORDRE, GES_CODE) 
            select MARACUJA.GESTION_AGREGAT_REPART_SEQ.NEXTVAL, --GAR_ID, 
                gaid, --GA_ID, 
                nouvelExercice, --EXE_ORDRE, 
                ges_code --GES_CODE
                from MARACUJA.GESTION_AGREGAT_REPART where ga_id=rec_agregat.ga_id;
               
      
      end loop;
    close cAgregats;  
END;
/




  

create or replace procedure grhum.inst_patch_maracuja_1911 is
	pconum maracuja.plan_comptable_exer.pco_num%type;
	exeOrdre jefy_admin.exercice.exe_ordre%type;
  cursor c1 is 
	  select distinct pco_num,exe_ordre
      from (
          select distinct pco_num,exe_ordre from maracuja.titre
          union all 
          select distinct pco_num,exe_ordre from maracuja.mandat
          union all 
          select distinct pco_num,exe_ordre from maracuja.mandat_brouillard
          union all 
          select distinct pco_num,exe_ordre from maracuja.titre_brouillard
          union all 
          select distinct pco_num,exe_ordre from maracuja.bordereau_brouillard
          union all 
          select distinct pco_num,exe_ordre from maracuja.CHEQUE_BROUILLARD
          union all 
          select distinct pco_num,exe_ordre from maracuja.recette
          union all  
          select distinct pco_num_ancien pco_num,exe_ordre from maracuja.reimputation
          union all
          select distinct pco_num_nouveau pco_num,exe_ordre from maracuja.reimputation
          union all                                   
          select distinct PCO_NUM_PAIEMENT pco_num,exe_ordre from maracuja.mode_paiement where pco_num_paiement is not null
          union all 
          select distinct PCO_NUM_VISA pco_num,exe_ordre from maracuja.mode_paiement where pco_num_VISA is not null 
          union all 
          select distinct PCO_NUM_PAIEMENT pco_num,exe_ordre from maracuja.mode_recouvrement where pco_num_paiement is not null
          union all 
          select distinct PCO_NUM_VISA pco_num,exe_ordre from maracuja.mode_recouvrement where pco_num_VISA is not null                       
      )
	  where (exe_ordre,PCO_NUM) in (select distinct exe_ordre,pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre,pco_num from maracuja.plan_comptable_exer);

	constraint_not_exists EXCEPTION;
	flag integer;

begin
	delete from maracuja.planco_visa
	where (exe_ordre, PCO_NUM_ORDONNATEUR) in ( select distinct exe_ordre,pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre,pco_num from maracuja.plan_comptable_exer);
	
	delete from maracuja.planco_visa
	where (exe_ordre,PCO_NUM_CTREPARTIE) in (select distinct exe_ordre,pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre,pco_num from maracuja.plan_comptable_exer);
	
	commit;

	delete from maracuja.planco_credit where pcc_ordre in (
	  select pcc.pcc_ordre from maracuja.planco_credit pcc, jefy_admin.type_credit tcd 
	  where pcc.tcd_ordre=tcd.tcd_ordre 
	  and (exe_ordre,pco_num) in (select distinct exe_ordre,pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre,pco_num from maracuja.plan_comptable_exer)
	  );
	commit;  
	
	--Creation des planco_exer non presents
	 open c1;
      loop
         fetch c1 into  pconum,exeordre;
         exit when c1%notfound;
		 INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
		   PCOE_ID, EXE_ORDRE, PCO_NUM, 
		   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
		   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
		   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
		   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
			SELECT MARACUJA.PLAN_COMPTABLE_EXER_seq.nextval,
			exeOrdre,PCO_NUM, 
		   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
		   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
		   'ANNULE', PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
		   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
		FROM MARACUJA.PLAN_COMPTABLE P where pco_num=pconum;        
      end loop;
      close c1;
      commit;
      
      
      
    MARACUJA.UTIL.CREER_PARAMETRE_exenonrestr (  'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section1.compte', '4632', 'Compte de contrepartie à utiliser pour un ORV émis sur la masse de crédit 1 (Fonctionnement)' );
    MARACUJA.UTIL.CREER_PARAMETRE_exenonrestr (  'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section2.compte', '4632', 'Compte de contrepartie à utiliser pour un ORV émis sur la masse de crédit 2 (Equipements)' );
    MARACUJA.UTIL.CREER_PARAMETRE_exenonrestr (  'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section3.compte', '4632', 'Compte de contrepartie à utiliser pour un ORV émis sur la masse de crédit 3 (personnel)' );
 	MARACUJA.UTIL.CREER_PARAMETRE_exenonclos (  'org.cocktail.gfc.comptabilite.comptetva.racine', '445', 'Racine des comptes de TVA' );
        
        -- mettre a jour les mandat_detail_ecriture sur la tva
		update maracuja.mandat_detail_ecriture set mde_origine='VISA TVA' 
		where ecd_ordre in (select ecd_ordre from maracuja.ecriture_detail where pco_num like '4456%');        
        
		-- mettre à jour les ecritures 86* sur le journal des valeurs inactives
		update maracuja.ecriture set tjo_ordre=18 where ecr_ordre in (select distinct ecr_ordre from maracuja.ecriture_detail  
		where exe_ordre>=2011 and pco_num like '86%' and tjo_ordre=1);
        
		commit;
      
      
     Drop_Object('maracuja','IDX_ECD_PCO_NUM_EX','INDEX' );
    	
     
     
	SELECT count(*) into flag FROM all_constraints WHERE owner = upper('MARACUJA') AND constraint_name = upper('FK_ECD_PCOEX');
	if (flag>0) then
		execute immediate 'alter table MARACUJA.ECRITURE_detail drop constraint FK_ECD_PCOEX';
	end if;
     
      
      
end;
/












