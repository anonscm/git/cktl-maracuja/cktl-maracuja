SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/3
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.1
-- Date de publication : 01/02/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- 
----------------------------------------------

-- 

execute grhum.inst_patch_maracuja_1911;
commit;

drop procedure grhum.inst_patch_maracuja_1911;


