set define off

begin

update MARACUJA.PLANCO_VISA set pvi_contrepartie_gestion = (select par_value from maracuja.parametre where exe_ordre=2007 and par_key='CONTRE PARTIE VISA') where pvi_libelle like 'VISA MANDAT%';
update MARACUJA.PLANCO_VISA set pvi_contrepartie_gestion = (select par_value from maracuja.parametre where exe_ordre=2007 and par_key='CONTRE PARTIE VISA RECETTE') where pvi_libelle like 'VISA TITRE%';
commit;

-- mise a jour des planco_visa pour 2008 si exercice 2008 deja cree
insert into maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) 
select PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, 2008
from maracuja.planco_visa p , jefy_admin.exercice e   
where p.exe_ordre=2007 and e.exe_ordre>2007;


commit;

-- suppression d'une fonction obsolete 
delete from jefy_admin.utilisateur_fonct where fon_ordre in (select fon_ordre from jefy_admin.fonction where tyap_id=4 and fon_id_interne='PAEX');
delete from jefy_admin.fonction where tyap_id=4 and fon_id_interne='PAEX';
commit;

end;