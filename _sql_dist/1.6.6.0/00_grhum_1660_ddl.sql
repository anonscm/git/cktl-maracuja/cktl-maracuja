

ALTER TABLE MARACUJA.PLANCO_VISA
 ADD (pvi_contrepartie_gestion  VARCHAR2(20) DEFAULT 'AGENCE' NOT NULL);

ALTER TABLE MARACUJA.PLANCO_VISA
add CONSTRAINT CK_PLANCO_VISA_ctp_gestion
   CHECK (pvi_contrepartie_gestion IN ('AGENCE', 'COMPOSANTE'));


COMMENT ON COLUMN MARACUJA.PLANCO_VISA.pvi_contrepartie_gestion IS 'Indique ou doit passer la contrepartie (AGENCE/COMPOSANTE)';


ALTER TABLE MARACUJA.MODE_PAIEMENT
 ADD (MOD_contrepartie_gestion  VARCHAR2(20) NULL);

COMMENT ON COLUMN MARACUJA.MODE_PAIEMENT.MOD_contrepartie_gestion IS 'Indique ou doit passer la contrepartie (AGENCE/COMPOSANTE)';


ALTER TABLE MARACUJA.PLANCO_VISA ADD (EXE_ORDRE NUMBER   DEFAULT 2007 NOT NULL);
ALTER TABLE MARACUJA.PLANCO_VISA ADD CONSTRAINT FK_PLANCO_VISA_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE);

alter table MARACUJA.PLANCO_VISA drop constraint UNQ_PLANCO_VISA_PCO_NUM;
CREATE UNIQUE INDEX maracuja.UNQ_PLANCO_VISA_PCO_NUM ON maracuja.PLANCO_VISA(exe_ORDRE, pco_num_ordonnateur) TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.PLANCO_VISA ADD (CONSTRAINT UNQ_PLANCO_VISA_PCO_NUM unique (exe_ORDRE, pco_num_ordonnateur) USING INDEX TABLESPACE GFC_INDX);  




CREATE INDEX MARACUJA.IDX_TITRE_DETAIL_REC_ID ON MARACUJA.TITRE_DETAIL_ECRITURE
(REC_ID)
LOGGING
TABLESPACE GFC_INDX;



CREATE OR REPLACE VIEW MARACUJA.V_RECETTE_RESTE_RECOUVRER
(REC_ID, RESTE_RECOUVRER)
AS
SELECT   r.rec_id, SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e 
   WHERE r.rec_id = tde.rec_id
  AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont>=0
  AND ecd.ecd_credit=0
  AND ecd.ecd_debit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='O'  
GROUP BY r.rec_id
UNION ALL
SELECT   r.rec_id, -SUM (ecd_reste_emarger)AS reste_recouvrer
    FROM RECETTE r, TITRE_DETAIL_ECRITURE tde, ECRITURE_DETAIL ecd, jefy_admin.exercice e 
   WHERE r.rec_id = tde.rec_id
     AND tde.ecd_ordre = ecd.ecd_ordre
  AND rec_mont<0
  AND ecd.ecd_debit=0
  AND ecd.ecd_credit<>0
  and ecd.exe_ordre=e.exe_ordre
  and e.exe_stat='O'
GROUP BY r.rec_id;


