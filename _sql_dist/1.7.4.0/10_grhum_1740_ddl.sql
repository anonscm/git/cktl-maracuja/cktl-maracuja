SET DEFINE OFF;
CREATE OR REPLACE PACKAGE EPN.EpN3_BAL
IS

procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_bal(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);
procedure prv_genere_bal_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);

procedure prv_insert_bal2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne in out number, compte varchar2, debit_be NUMBER, debit_exer NUMBER, credit_be NUMBER, credit_exer NUMBER, deviseTaux NUMBER);
procedure prv_insert_bal1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY EPN.Epn3_BAL  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_bal(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_BAL_1 where EPNB1_TYPE_DOC=typeFichier and EPNB1_COD_BUD=codeBudget and EPNB_ordre=rang and EPNB1_EXERCICE=exeOrdre;
    delete from EPN_BAL_2 where EPNB2_TYPE_DOC=typeFichier and EPNB2_COD_BUD=codeBudget and EPNB_ORDRE=rang and EPNB2_EXERCICE=exeOrdre;    
end;

-- genere le fichier de balance comptable
procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_bal(typeFichier, codeBudget, exeOrdre, rang);
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_bal_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_bal_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            prv_genere_bal_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            return;
        end if;
        
    end if;

end;


-- generation des fichiers de balance consolides (type 01)
-- NB : Maracuja ne gere pas les comptabilites secondaires, donc ce fichier sera identique aux fichiers 02 (aggreges) 
procedure prv_genere_bal_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;  
      
    compte EPN_BALANCE.IMPUTATION%TYPE;    
    debit_be NUMBER;
    credit_be NUMBER; 
    debit_exer NUMBER; 
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is 
select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (
    select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier   
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier
    group by pco_num
    )
    group by pco_num
    order by pco_num;          
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '01';
    numLigne := 2;
    
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;
    
   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;



-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_bal_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;  
      
    compte EPN_BALANCE.IMPUTATION%TYPE;    
    debit_be NUMBER;
    credit_be NUMBER; 
    debit_exer NUMBER; 
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is 
select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (
    select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier   
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier
    group by pco_num
    )
    group by pco_num
    order by pco_num;          
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;
    
   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_bal_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;  
      
    compte EPN_BALANCE.IMPUTATION%TYPE;    
    debit_be NUMBER;
    credit_be NUMBER; 
    debit_exer NUMBER; 
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier
    and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier
    and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    group by pco_num
    )
    group by pco_num
    order by pco_num;          
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;
    
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;
    
   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
    
end;


-- generation des fichiers balance detailles (type 03) pour un SACD
procedure prv_genere_bal_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;  
    
    compte EPN_BALANCE.IMPUTATION%TYPE;    
    debit_be NUMBER;
    credit_be NUMBER; 
    debit_exer NUMBER; 
    credit_exer NUMBER;

    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    ( select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier
    and ges_code = gescodeSACD
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and e.ecr_date_saisie<=vDateFichier
    and ges_code = gescodeSACD
    group by pco_num
    )
    group by pco_num
    order by pco_num;           
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    gesCode := gesCodeSACD;
    typeFichier := '03';
    
    numLigne := 2;                    
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;
            
            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;
    
    -- insert entete
     prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   

end;







-- se charge de remplir EPN_BAL_2
procedure prv_insert_bal2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne in out number, compte varchar2, debit_be NUMBER, debit_exer NUMBER, credit_be NUMBER, credit_exer NUMBER, deviseTaux NUMBER) 
is
    debit_total NUMBER; 
    credit_total NUMBER;
    solde_debiteur NUMBER;
    solde_crediteur NUMBER;
    vCompte varchar2(60);
    
    vDebitBe number;
    vDebitExer number;
    vCreditBe number;
    vCreditExer number;
    
    
begin
    
    vDebitBe := round(debit_be*deviseTaux, 2);
    vDebitExer := round(debit_exer*deviseTaux, 2);
    vCreditBe := round(credit_be*deviseTaux, 2);
    vCreditExer := round(credit_exer*deviseTaux, 2);
    
    debit_total  :=  vDebitBe  +  vDebitExer;
    credit_total  :=  vCreditBe  +  vCreditExer;

    if (debit_total=0 and credit_total=0) then
        numLigne := numLigne - 1;
        return;
    end if;

    IF debit_total > credit_total THEN
        solde_debiteur := debit_total - credit_total;
        solde_crediteur := 0;
    ELSE
        solde_debiteur := 0;
        solde_crediteur := credit_total - debit_total;            
    END IF;

    vCompte := compte || '               ';

    INSERT INTO EPN.EPN_BAL_2 
        VALUES (to_number(rang),  --EPNB_ORDRE, 
                gesCode,   --EPNB2_GES_CODE, 
                '2',    --EPNB2_TYPE,
                numLigne,   --EPNB2_NUMERO, 
                1,    --EPNB2_TYPE_CPT, 
                (SUBSTR(vCompte,1,15)),    --EPNB2_COMPTE, 
                 vDebitBe,   --EPNB2_DEBBE, 
                 vDebitExer, --EPNB2_DEBCUM, 
                 debit_total,   --EPNB2_DEBTOT, 
                 vCreditBe,   --EPNB2_CREBE, 
                 vCreditExer,   --EPNB2_CRECUM, 
                 credit_total,   --EPNB2_CRETOT, 
                 solde_debiteur,   --EPNB2_BSDEB, 
                 solde_crediteur,   --EPNB2_BSCRE, 
                  exeOrdre,  --EPNB2_EXERCICE
                  codeBudget,  --EPNB2_COD_BUD
                  typeFichier --EPNB2_TYPE_DOC 
                  );    

end;



procedure prv_insert_bal1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_bal_1.EPNB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
    
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
   
       
   -- Insert du header/footer du fichier
    INSERT INTO EPN.EPN_BAL_1 (EPNB_ORDRE, EPNB1_GES_CODE, EPNB1_TYPE, 
               EPNB1_NUMERO, EPNB1_IDENTIFIANT, EPNB1_TYPE_DOC, 
               EPNB1_COD_NOMEN, EPNB1_COD_BUD, EPNB1_EXERCICE, 
               EPNB1_RANG, EPNB1_DATE, EPNB1_SIREN, 
               EPNB1_SIRET, EPNB1_NB_ENREG) 
        VALUES (   to_number(rang), --EPNB_ORDRE
                    gesCode, --EPNB1_GES_CODE
                    '1', --EPNB1_TYPE
                    1, --EPNB1_NUMERO (numero de ligne dans le fichier)
                    identifiantDGCP, --EPNB1_IDENTIFIANT
                    typeFichier, --EPNB1_TYPE_DOC
                    codeNomenclature, --EPNB1_COD_NOMEN
                    codeBudget, --EPNB1_COD_BUD
                    exeOrdre, --EPNB1_EXERCICE
                    rang, --EPNB1_RANG
                    TO_CHAR(vDateFichier2,'DDMMYYYY'), --EPNB1_DATE
                    siren, --EPNB1_SIREN
                    siret, --EPNB1_SIRET
                    nbEnregistrement --EPNB1_NB_ENREG);
                    );    
        
    

end;


END;
/


GRANT EXECUTE ON  EPN.EPN3_BAL TO MARACUJA;

