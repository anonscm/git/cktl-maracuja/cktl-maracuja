SET DEFINE OFF;
CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Journal IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

Apres avoir creer l ecriture et ses details
il faut valider l ecriture :
l' ecriture prend un numero dans le journal
de l exerice ET IL INTERDIT /IMPOSSIBLE DE LA SUPPRIMER !!

*/

-- API PUBLIQUE pour creer / valider / annuler une ecriture --
-- permet de valider une ecriture saisie .
PROCEDURE validerEcriture (ecrordre INTEGER) ;
-- permet d'annuler une ecriture saisie .
PROCEDURE annulerEcriture (ecrordre INTEGER) ;

-- permet de creer une ecriture de balance d entree --
FUNCTION creerEcritureBE (
COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  ) RETURN INTEGER;

 -- permet de creer une ecriture d exercice --
FUNCTION creerEcritureExerciceType (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER,--        NOT NULL,
  TJOORDRE 				INTEGER
  ) RETURN INTEGER;
 -- permet de creer une ecriture de fin d exercice --
FUNCTION creerEcritureCloture (brjordre INTEGER,exeordre INTEGER) RETURN INTEGER;
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;


-- PRIVATE --
/*
INTERDICTION DE FAIRE DES APPELS DE CES PROCEDURES EN DEHORS DU PACKAGE.
*/
FUNCTION creerEcriturePrivate (

  BROORDRE              NUMBER,
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  --ECRNUMERO             NUMBER,-- (32),
 ECRNUMERO_BROUILLARD  NUMBER,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  ) RETURN INTEGER;

FUNCTION creerEcritureDetailPrivate (
  ECDCOMMENTAIRE    VARCHAR2,-- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;

END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Journal IS
-- PUBLIC --

PROCEDURE validerEcriture (ecrordre INTEGER)
IS
cpt INTEGER;
debit number;
credit number;
BEGIN

SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL
WHERE ecr_ordre = ecrordre;

IF cpt >=2 THEN
    select sum(ecd_debit) into debit from ECRITURE_DETAIL
        WHERE ecr_ordre = ecrordre;
    select sum(ecd_credit) into credit from ECRITURE_DETAIL
        WHERE ecr_ordre = ecrordre;        
    if (credit<>debit) then
        RAISE_APPLICATION_ERROR (-20001,'Ecriture desequilibree');
    end if;    
    Numerotationobject.numeroter_ecriture(ecrordre);
ELSE
 RAISE_APPLICATION_ERROR (-20001,'MAUVAIS FORMAT D ECRITURE !');
END IF;

END;

PROCEDURE annulerEcriture (ecrordre INTEGER)
IS
cpt INTEGER;
BEGIN
 SELECT ecr_numero INTO cpt FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
-- UPDATE ECRITURE SET ecr_etat = 'ANNULE'
-- WHERE ecr_ordre = ecrordre;
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
END IF;

END;

FUNCTION creerEcritureBE (

  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,

  )RETURN INTEGER
IS
  TJOORDRE              NUMBER;
BEGIN
-- recup du type_journal....
SELECT tjo_ordre INTO tjoordre FROM TYPE_JOURNAL
WHERE tjo_libelle ='JOURNAL BALANCE ENTREE';


RETURN Api_Plsql_Journal.creerEcriture
(
  COMORDRE              ,--        NOT NULL,
  ECRDATE               ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureExerciceType (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER,--        NOT NULL,
  TJOORDRE 				INTEGER
  )RETURN INTEGER
IS
  cpt              NUMBER;
BEGIN
-- recup du type_journal....
SELECT COUNT(*) INTO cpt FROM TYPE_JOURNAL
WHERE tjo_ordre =tjoordre;

IF cpt = 0 THEN
RAISE_APPLICATION_ERROR (-20001,'TYPE DE JOURNAL INCONNU !');
END IF;

RETURN Api_Plsql_Journal.creerEcriture
(
  COMORDRE              ,--        NOT NULL,
  ECRDATE               ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureCloture (brjordre INTEGER,exeordre INTEGER)RETURN INTEGER
IS
cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
RETURN cpt;
END;

FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;
BEGIN

RETURN Api_Plsql_Journal.creerEcriturePrivate(
  NULL              ,
  COMORDRE              ,--        NOT NULL,
  ECRDATE                ,--         NOT NULL,
  ECRLIBELLE            ,-- (200)  NOT NULL,
 NULL  ,
  EXEORDRE              ,--        NOT NULL,
  ORIORDRE              ,
  TJOORDRE              ,--        NOT NULL,
  TOPORDRE              ,--        NOT NULL,
  UTLORDRE              --        NOT NULL,
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetailPrivate
(
  ECDCOMMENTAIRE    ,-- (200),
  ECDLIBELLE        ,-- (200),
  ECDMONTANT        ,-- (12,2) NOT NULL,
  ECDSECONDAIRE     ,-- (20),
  ECDSENS           ,-- (1)  NOT NULL,
  ECRORDRE          ,--        NOT NULL,
  EXEORDRE          ,--        NOT NULL,
  GESCODE           ,-- (10)  NOT NULL,
  PCONUM            -- (20)  NOT NULL
  );


END;







-- PRIVATE --
FUNCTION creerEcriturePrivate (
  BROORDRE              NUMBER,
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
--  ECRDATE_SAISIE        DATE ,--         NOT NULL,
--  ECRETAT               VARCHAR2,-- (20)  NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  --ECRNUMERO             NUMBER,-- (32),
 ECRNUMERO_BROUILLARD  NUMBER,
--  ECRORDRE              NUMBER,--        NOT NULL,
--  ECRPOSTIT             VARCHAR2,-- (200),
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )
  RETURN INTEGER
IS
ECRORDRE INTEGER;

BEGIN
SELECT ecriture_seq.NEXTVAL INTO ECRORDRE FROM dual;

INSERT INTO ECRITURE VALUES
(BROORDRE,
COMORDRE,
SYSDATE,
ECRDATE,
'VALIDE',
ecrlibelle,
0,
ECRNUMERO_BROUILLARD,
ECRORDRE,
ECRLIBELLE,
EXEORDRE,
ORIORDRE,
TJOORDRE,
TOPORDRE,
UTLORDRE
);

RETURN ECRORDRE;
END;


FUNCTION creerEcritureDetailPrivate (
  ECDCOMMENTAIRE    VARCHAR2,-- (200),
--  ECDCREDIT         NUMBER,-- (12,2),
--  ECDDEBIT          NUMBER,-- (12,2),
--  ECDINDEX          NUMBER,--        NOT NULL,
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
--  ECDORDRE          NUMBER,--        NOT NULL,
--  ECDPOSTIT         VARCHAR2,-- (200),
--  ECDRESTE_EMARGER  NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
ECDORDRE          NUMBER;
ECDCREDIT         NUMBER;
ECDDEBIT          NUMBER;
BEGIN

SELECT ecriture_detail_seq.NEXTVAL  INTO ECDORDRE FROM dual;

IF ECDSENS = 'C' THEN
ECDCREDIT := ecdmontant;
ECDDEBIT := 0;
ELSE
ECDCREDIT := 0;
ECDDEBIT := ecdmontant;
END IF;

dbms_output.put_line('INSERTION DETAIL : '||ecdmontant||' , SENS : '||ecdsens||' , PCO : '||pconum||' , ECR ORDRE : '||ecrordre);

INSERT INTO ECRITURE_DETAIL VALUES (
  ECDCOMMENTAIRE    ,-- (200),
  ECDCREDIT         ,-- (12,2),
  ECDDEBIT          ,-- (12,2),
  ECRORDRE          ,--        NOT NULL,
  ECDLIBELLE        ,-- (200),
  ECDMONTANT        ,-- (12,2) NOT NULL,
  ECDORDRE          ,--        NOT NULL,
  NULL         ,-- (200),
  ABS(ECDMONTANT)  ,-- (12,2) NOT NULL,
  ECDSECONDAIRE     ,-- (20),
  ECDSENS           ,-- (1)  NOT NULL,
  ECRORDRE          ,--        NOT NULL,
  EXEORDRE          ,--        NOT NULL,
  GESCODE           ,-- (10)  NOT NULL,
  PCONUM            -- (20)  NOT NULL
  );


RETURN ECDORDRE;
END;

END;
/


GRANT EXECUTE ON  MARACUJA.API_PLSQL_JOURNAL TO JEFY_PAYE;

