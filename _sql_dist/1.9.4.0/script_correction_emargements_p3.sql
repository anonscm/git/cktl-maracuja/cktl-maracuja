-- script de suppression des details d'emargements reliant une ecriture sacd et hors sacd

declare


edmordre integer;

cursor c1 is
    select distinct emd_ordre from 
(
select distinct tem.TEM_LIBELLE, em.ema_ordre, emd_ordre, ema_numero, ema_date, ecd1.pco_num, ecd1.ges_code code_gestion1,  e1.ecr_numero ecriture_numero1, ecd2.ges_code code_gestion2, e2.ecr_numero ecriture_numero2 , 
ecd1.ecd_debit, ecd1.ecd_credit, ecd2.ecd_debit, ecd2.ecd_credit, ecd1.ecd_libelle, ecd2.ecd_libelle
from emargement_detail emd, emargement em, ecriture_detail ecd1, ecriture_detail ecd2, ecriture e1, ecriture e2 , type_emargement tem
where 
emd.ema_ordre=em.ema_ordre
and em.tem_ordre=tem.tem_ordre
and emd.ecd_ordre_destination=ecd1.ecd_ordre
and emd.ecd_ordre_source=ecd2.ecd_ordre
and ecd2.ecr_ordre=e2.ecr_ordre
and ecd1.ecr_ordre=e1.ecr_ordre
and em.ema_etat='VALIDE'
and ecd1.GES_CODE <> ecd2.ges_code
and (ecd1.ges_code in (select ges_code from gestion_exercice where exe_ordre=2013 and pco_num_185 is not null)
    or ecd2.ges_code in (select ges_code from gestion_exercice where exe_ordre=2013 and pco_num_185 is not null)
    )
and e1.exe_ordre=2013
);

begin

    OPEN c1;
        LOOP
        FETCH c1 INTO edmordre;
        EXIT WHEN c1%NOTFOUND;
            MARACUJA.API_EMARGEMENT.SUPPRIMERDETAILEMARGEMENT (edmordre);    
        end loop;
            
     close c1;
end;
