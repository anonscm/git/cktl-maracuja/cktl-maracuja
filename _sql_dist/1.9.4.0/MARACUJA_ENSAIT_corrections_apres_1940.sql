-- Script à passer sur base ENSAIT apres  scripts version 1.9.4.0 


-- bascule du code 104 annule vers RECHERCHE
update jefy_depense.engage_ctrl_action set tyac_id=564 where eact_id=773 and tyac_id=563;
update jefy_depense.depense_ctrl_action set tyac_id=564 where dact_id=1507 and tyac_id=563;
update jefy_depense.engage_ctrl_action set tyac_id=564 where eact_id=831 and tyac_id=563;
update jefy_depense.depense_ctrl_action set tyac_id=564 where dact_id=2383 and tyac_id=563;

-- bascule du code recherche inter annule vers recherche
update jefy_depense.engage_ctrl_action set tyac_id=564 where eact_id in (685,686,531,540,1513,1248,1254,1265) and tyac_id=311;
update jefy_depense.depense_ctrl_action set tyac_id=564 where dact_id in (209,210,216,220,789,795,799,477,481,486 ) and tyac_id=311;


-- install parametre
update maracuja.parametre set par_value='CONSOLIDE SAIC:02' where par_key='org.cocktail.gfc.maracuja.epn.traiterregroupementssuivantcommesacdaveccodebuget' and exe_ordre=2013;

-- suppression codes budgets SACD traités via regroupement 
delete from epn.code_budget_sacd where exe_ordre=2013;

commit;