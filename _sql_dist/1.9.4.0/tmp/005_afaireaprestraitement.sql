
CREATE OR REPLACE PACKAGE BODY MARACUJA."AFAIREAPRESTRAITEMENT" 
is
-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 2.0 - suppression des references aux user < 2007
   procedure apres_visa_bordereau (borid integer)
   is
   begin
    API_BORDEREAU.APRES_VISA ( BORID );
   end;



   procedure apres_reimputation (reiordre integer)
   is
      ex              integer;
      manid           mandat.man_id%type;
      titid           titre.tit_id%type;
      pconumnouveau   plan_comptable_exer.pco_num%type;
      pconumancien    plan_comptable_exer.pco_num%type;
      utlordre        reimputation.utl_ordre%type;
   begin
      select man_id,
             tit_id,
             pco_num_nouveau,
             pco_num_ancien,
             utl_ordre
      into   manid,
             titid,
             pconumnouveau,
             pconumancien,
             utlordre
      from   reimputation
      where  rei_ordre = reiordre;

      if manid is not null then
         jefy_depense.reimputer.reimputation_maracuja (manid, pconumnouveau, utlordre);
      --update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;
      end if;

      if titid is not null then
         update jefy_recette.recette_ctrl_planco
            set pco_num = pconumnouveau
          where tit_id = titid and pco_num = pconumancien;
      end if;
   end;

   procedure apres_paiement (paiordre integer)
   is
      cpt   integer;
   begin
      --SELECT 1 INTO  cpt FROM dual;
      emarger_paiement (paiordre);
   end;

   procedure emarger_paiement (paiordre integer)
   is
      cursor mandats (lepaiordre integer)
      is
         select *
         from   mandat
         where  pai_ordre = lepaiordre;

      cursor non_emarge_debit (lemanid integer)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'D';

      cursor non_emarge_credit_compte (lemanid integer, lepconum varchar)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'C';

      lemandat          maracuja.mandat%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lepaiement        maracuja.paiement%rowtype;
      cpt               integer;
   begin
-- recup infos
      select *
      into   lepaiement
      from   paiement
      where  pai_ordre = paiordre;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lepaiement.exe_ordre,
                   3,
                   lepaiement.utl_ordre,
                   lepaiement.com_ordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les mandats du paiement
      open mandats (paiordre);

      loop
         fetch mandats
         into  lemandat;

         exit when mandats%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_debit (lemandat.man_id);

         loop
            fetch non_emarge_debit
            into  ecriture_debit;

            exit when non_emarge_debit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_credit_compte (lemandat.man_id, ecriture_debit.pco_num);

            loop
               fetch non_emarge_credit_compte
               into  ecriture_credit;

               exit when non_emarge_credit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_debit.ecd_ordre,
                               ecriture_credit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               lemandat.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_credit_compte;
         end loop;

         close non_emarge_debit;
      end loop;

      close mandats;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      end if;
   end;

   procedure apres_recouvrement_releve (recoordre integer)
   is
   begin
      emarger_prelevement_releve (recoordre);
   end;

   procedure emarger_prelevement_releve (recoordre integer)
   is
      cpt   integer;
   begin
      -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
      select 1
      into   cpt
      from   dual;

      emarger_prelevement (recoordre);
   end;

   procedure apres_recouvrement (recoordre integer)
   is
   begin
      emarger_prelevement (recoordre);
   end;

   procedure emarger_prelevement (recoordre integer)
   is
   begin
      if (recoordre is null) then
         raise_application_error (-20001, 'Le parametre recoordre est null.');
      end if;

      emarger_prelev_ac_titre (recoordre);
      emarger_prelev_ac_ecr (recoordre);
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
   procedure emarger_prelev_ac_titre (recoordre integer)
   is
      cursor titres (lerecoordre integer)
      is
         select *
         from   titre
         where  tit_id in (select tit_id
                           from   prelevement p, echeancier e
                           where  e.eche_echeancier_ordre = p.eche_echeancier_ordre and p.reco_ordre = lerecoordre);

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lerecouvrement    maracuja.recouvrement%rowtype;
      cpt               integer;
   begin
      -- recup infos
      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lerecouvrement.exe_ordre,
                   3,
                   lerecouvrement.utl_ordre,
                   lerecouvrement.com_ordre,
                   0,
                   'VALIDE'
                  );

      -- on fetch les titres du recrouvement
      open titres (recoordre);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

                if ((afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) and 
                    (ecriture_debit.ges_code = ecriture_credit.ges_code ) ) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               ecriture_debit.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

      -- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
   procedure emarger_prelev_ac_ecr (recoordre integer)
   is
      cursor lesprelevements (lerecoordre integer)
      is
         select *
         from   maracuja.prelevement
         where  reco_ordre = lerecoordre;

      ecrdetailcredit   ecriture_detail%rowtype;
      ecrdetaildebit    ecriture_detail%rowtype;
      ecriture_credit   ecriture_detail%rowtype;
      ecriture_debit    ecriture_detail%rowtype;
      leprelevement     maracuja.prelevement%rowtype;
      flag              integer;
      flag2             integer;
      retval            integer;
      utlordre          integer;

      cursor non_emarge_credit (prelevordre integer)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (prelevordre integer, lepconum varchar)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and pco_num = lepconum and e.ecd_sens = 'D';
   begin
      select utl_ordre
      into   utlordre
      from   recouvrement
      where  reco_ordre = recoordre;

      open lesprelevements (recoordre);

      loop
         fetch lesprelevements
         into  leprelevement;

         exit when lesprelevements%notfound;
         ecrdetailcredit := null;
         ecrdetaildebit := null;

         -- recuperation de l'ecriture de l'echeancier non emargee en debit
         select count (*)
         into   flag2
         from   maracuja.ecriture_detail
         where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                            from   maracuja.echeancier_detail_ecr
                                                                            where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

         if (flag2 = 1) then
            select *
            into   ecrdetaildebit
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                               from   maracuja.echeancier_detail_ecr
                                                                               where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

            -- s'il y a une ecriture non emargee en debit sur l'echeancier,
            -- on emarge avec le credit correspondant du prelevement
            -- recuperation de l'ecriture du prelevement non emargee en credit
            select count (*)
            into   flag
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                     from   maracuja.prelevement_detail_ecr
                                                                                                                     where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

            if (flag = 1) then
               select *
               into   ecrdetailcredit
               from   maracuja.ecriture_detail
               where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                        from   maracuja.prelevement_detail_ecr
                                                                                                                        where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

               -- si les deux ecritures ont ete recuperees, on les emarge
               if (afaireaprestraitement.verifier_emar_exercice (ecrdetailcredit.ecr_ordre, ecrdetaildebit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecrdetailcredit.ecd_ordre, ecrdetaildebit.ecd_ordre, 3, utlordre);
               end if;
            end if;
         end if;

         -- emargement entre les debits et credits associes au prelevement
         -- (suite a saisie releve)

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (leprelevement.prel_prelev_ordre);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (leprelevement.prel_prelev_ordre, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, utlordre);
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close lesprelevements;
   end;

   procedure emarger_visa_bord_prelevement (borid integer)
   is
      cursor titres (leborid integer)
      is
         select *
         from   titre
         where  bor_id = leborid;

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lebordereau       maracuja.bordereau%rowtype;
      cpt               integer;
      comordre          integer;
   begin
-- recup infos
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

-- recup du com_ordre
      select com_ordre
      into   comordre
      from   gestion
      where  ges_code = lebordereau.ges_code;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lebordereau.exe_ordre,
                   3,
                   lebordereau.utl_ordre,
                   comordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les titres du recouvrement
      open titres (borid);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_debit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               letitre.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

   function verifier_emar_exercice (ecrcredit integer, ecrdebit integer)
      return integer
   is
      reponse     integer;
      execredit   exercice.exe_ordre%type;
      exedebit    exercice.exe_ordre%type;
   begin
-- init
      reponse := 0;

      select exe_ordre
      into   execredit
      from   ecriture
      where  ecr_ordre = ecrcredit;

      select exe_ordre
      into   exedebit
      from   ecriture
      where  ecr_ordre = ecrdebit;

      if exedebit = execredit then
         return 1;
      else
         return 0;
      end if;
   end;
end;
/

