set DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  MARACUJA, EPN
-- Schéma d'execution du script : GRHUM ou dba
-- Numéro de version :  1.9.4.0
-- Date de publication : 13/11/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- correction creation d'emargements lors des ecritures de prelevement (prelevement sur SACD)
-- modifications user EPN pour prise en compte des regroupements de codes gestion
----------------------------------------------
whenever sqlerror exit sql.sqlcode;




exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.5', 'MARACUJA' );

exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 4, '1.9.4.0', null );
commit ;

grant select on MARACUJA.gestion_agregat_repart to epn;
grant select on MARACUJA.gestion_agregat to epn;

CREATE OR REPLACE PACKAGE BODY MARACUJA."AFAIREAPRESTRAITEMENT" 
is
-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 2.0 - suppression des references aux user < 2007
   procedure apres_visa_bordereau (borid integer)
   is
   begin
    API_BORDEREAU.APRES_VISA ( BORID );
   end;



   procedure apres_reimputation (reiordre integer)
   is
      ex              integer;
      manid           mandat.man_id%type;
      titid           titre.tit_id%type;
      pconumnouveau   plan_comptable_exer.pco_num%type;
      pconumancien    plan_comptable_exer.pco_num%type;
      utlordre        reimputation.utl_ordre%type;
   begin
      select man_id,
             tit_id,
             pco_num_nouveau,
             pco_num_ancien,
             utl_ordre
      into   manid,
             titid,
             pconumnouveau,
             pconumancien,
             utlordre
      from   reimputation
      where  rei_ordre = reiordre;

      if manid is not null then
         jefy_depense.reimputer.reimputation_maracuja (manid, pconumnouveau, utlordre);
      --update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;
      end if;

      if titid is not null then
         update jefy_recette.recette_ctrl_planco
            set pco_num = pconumnouveau
          where tit_id = titid and pco_num = pconumancien;
      end if;
   end;

   procedure apres_paiement (paiordre integer)
   is
      cpt   integer;
   begin
      --SELECT 1 INTO  cpt FROM dual;
      emarger_paiement (paiordre);
   end;

   procedure emarger_paiement (paiordre integer)
   is
      cursor mandats (lepaiordre integer)
      is
         select *
         from   mandat
         where  pai_ordre = lepaiordre;

      cursor non_emarge_debit (lemanid integer)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'D';

      cursor non_emarge_credit_compte (lemanid integer, lepconum varchar)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'C';

      lemandat          maracuja.mandat%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lepaiement        maracuja.paiement%rowtype;
      cpt               integer;
   begin
-- recup infos
      select *
      into   lepaiement
      from   paiement
      where  pai_ordre = paiordre;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lepaiement.exe_ordre,
                   3,
                   lepaiement.utl_ordre,
                   lepaiement.com_ordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les mandats du paiement
      open mandats (paiordre);

      loop
         fetch mandats
         into  lemandat;

         exit when mandats%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_debit (lemandat.man_id);

         loop
            fetch non_emarge_debit
            into  ecriture_debit;

            exit when non_emarge_debit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_credit_compte (lemandat.man_id, ecriture_debit.pco_num);

            loop
               fetch non_emarge_credit_compte
               into  ecriture_credit;

               exit when non_emarge_credit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_debit.ecd_ordre,
                               ecriture_credit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               lemandat.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_credit_compte;
         end loop;

         close non_emarge_debit;
      end loop;

      close mandats;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      end if;
   end;

   procedure apres_recouvrement_releve (recoordre integer)
   is
   begin
      emarger_prelevement_releve (recoordre);
   end;

   procedure emarger_prelevement_releve (recoordre integer)
   is
      cpt   integer;
   begin
      -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
      select 1
      into   cpt
      from   dual;

      emarger_prelevement (recoordre);
   end;

   procedure apres_recouvrement (recoordre integer)
   is
   begin
      emarger_prelevement (recoordre);
   end;

   procedure emarger_prelevement (recoordre integer)
   is
   begin
      if (recoordre is null) then
         raise_application_error (-20001, 'Le parametre recoordre est null.');
      end if;

      emarger_prelev_ac_titre (recoordre);
      emarger_prelev_ac_ecr (recoordre);
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
   procedure emarger_prelev_ac_titre (recoordre integer)
   is
      cursor titres (lerecoordre integer)
      is
         select *
         from   titre
         where  tit_id in (select tit_id
                           from   prelevement p, echeancier e
                           where  e.eche_echeancier_ordre = p.eche_echeancier_ordre and p.reco_ordre = lerecoordre);

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lerecouvrement    maracuja.recouvrement%rowtype;
      cpt               integer;
   begin
      -- recup infos
      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lerecouvrement.exe_ordre,
                   3,
                   lerecouvrement.utl_ordre,
                   lerecouvrement.com_ordre,
                   0,
                   'VALIDE'
                  );

      -- on fetch les titres du recrouvement
      open titres (recoordre);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

                if ((afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) and 
                    (ecriture_debit.ges_code = ecriture_credit.ges_code ) ) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               ecriture_debit.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

      -- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
   procedure emarger_prelev_ac_ecr (recoordre integer)
   is
      cursor lesprelevements (lerecoordre integer)
      is
         select *
         from   maracuja.prelevement
         where  reco_ordre = lerecoordre;

      ecrdetailcredit   ecriture_detail%rowtype;
      ecrdetaildebit    ecriture_detail%rowtype;
      ecriture_credit   ecriture_detail%rowtype;
      ecriture_debit    ecriture_detail%rowtype;
      leprelevement     maracuja.prelevement%rowtype;
      flag              integer;
      flag2             integer;
      retval            integer;
      utlordre          integer;

      cursor non_emarge_credit (prelevordre integer)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (prelevordre integer, lepconum varchar)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and pco_num = lepconum and e.ecd_sens = 'D';
   begin
      select utl_ordre
      into   utlordre
      from   recouvrement
      where  reco_ordre = recoordre;

      open lesprelevements (recoordre);

      loop
         fetch lesprelevements
         into  leprelevement;

         exit when lesprelevements%notfound;
         ecrdetailcredit := null;
         ecrdetaildebit := null;

         -- recuperation de l'ecriture de l'echeancier non emargee en debit
         select count (*)
         into   flag2
         from   maracuja.ecriture_detail
         where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                            from   maracuja.echeancier_detail_ecr
                                                                            where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

         if (flag2 = 1) then
            select *
            into   ecrdetaildebit
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                               from   maracuja.echeancier_detail_ecr
                                                                               where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

            -- s'il y a une ecriture non emargee en debit sur l'echeancier,
            -- on emarge avec le credit correspondant du prelevement
            -- recuperation de l'ecriture du prelevement non emargee en credit
            select count (*)
            into   flag
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                     from   maracuja.prelevement_detail_ecr
                                                                                                                     where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

            if (flag = 1) then
               select *
               into   ecrdetailcredit
               from   maracuja.ecriture_detail
               where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                        from   maracuja.prelevement_detail_ecr
                                                                                                                        where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

               -- si les deux ecritures ont ete recuperees, on les emarge
               if (afaireaprestraitement.verifier_emar_exercice (ecrdetailcredit.ecr_ordre, ecrdetaildebit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecrdetailcredit.ecd_ordre, ecrdetaildebit.ecd_ordre, 3, utlordre);
               end if;
            end if;
         end if;

         -- emargement entre les debits et credits associes au prelevement
         -- (suite a saisie releve)

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (leprelevement.prel_prelev_ordre);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (leprelevement.prel_prelev_ordre, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, utlordre);
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close lesprelevements;
   end;

   procedure emarger_visa_bord_prelevement (borid integer)
   is
      cursor titres (leborid integer)
      is
         select *
         from   titre
         where  bor_id = leborid;

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lebordereau       maracuja.bordereau%rowtype;
      cpt               integer;
      comordre          integer;
   begin
-- recup infos
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

-- recup du com_ordre
      select com_ordre
      into   comordre
      from   gestion
      where  ges_code = lebordereau.ges_code;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lebordereau.exe_ordre,
                   3,
                   lebordereau.utl_ordre,
                   comordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les titres du recouvrement
      open titres (borid);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_debit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               letitre.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

   function verifier_emar_exercice (ecrcredit integer, ecrdebit integer)
      return integer
   is
      reponse     integer;
      execredit   exercice.exe_ordre%type;
      exedebit    exercice.exe_ordre%type;
   begin
-- init
      reponse := 0;

      select exe_ordre
      into   execredit
      from   ecriture
      where  ecr_ordre = ecrcredit;

      select exe_ordre
      into   exedebit
      from   ecriture
      where  ecr_ordre = ecrdebit;

      if exedebit = execredit then
         return 1;
      else
         return 0;
      end if;
   end;
end;
/

CREATE OR REPLACE PACKAGE         epn.EpN3_BAL
IS

procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_bal(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);
procedure prv_genere_bal_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_genere_bal_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2);

procedure prv_insert_bal2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne in out number, compte varchar2, debit_be NUMBER, debit_exer NUMBER, credit_be NUMBER, credit_exer NUMBER, deviseTaux NUMBER);
procedure prv_insert_bal1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY                                 epn.Epn3_BAL  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_bal(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_BAL_1 where EPNB1_TYPE_DOC=typeFichier and EPNB1_COD_BUD=codeBudget and EPNB_ordre=rang and EPNB1_EXERCICE=exeOrdre;
    delete from EPN_BAL_2 where EPNB2_TYPE_DOC=typeFichier and EPNB2_COD_BUD=codeBudget and EPNB_ORDRE=rang and EPNB2_EXERCICE=exeOrdre;
end;

-- genere le fichier de balance comptable
procedure epn_genere_bal(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_bal(typeFichier, codeBudget, exeOrdre, rang);

    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_bal_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_bal_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
            if (substr(gescodesacd,1,1)='+') then
              prv_genere_bal_03G_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            else
              prv_genere_bal_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            end if;
            return;
        end if;

    end if;

end;


-- generation des fichiers de balance consolides (type 01)
-- NB : Maracuja ne gere pas les comptabilites secondaires, donc ce fichier sera identique aux fichiers 02 (aggreges)
procedure prv_genere_bal_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is
select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (
    select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '01';
    numLigne := 2;

    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;



-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_bal_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is
select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (
    select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;

    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_bal_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;
    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    (select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;

    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

   -- Insert du header/footer du fichier
   prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);


end;


-- generation des fichiers balance detailles (type 03) pour un SACD
procedure prv_genere_bal_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;

    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;

    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    ( select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code = gescodeSACD
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd, maracuja.ecriture e
    where ecd.ecr_ordre=e.ecr_ordre
    and substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ges_code = gescodeSACD
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    gesCode := gesCodeSACD;
    typeFichier := '03';




    numLigne := 2;
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

    -- insert entete
     prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;


--dans ce cas le gescodesacd est en fait le libelle du regroupement (agregat_gestion) 
procedure prv_genere_bal_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    deviseTaux NUMBER;
    typeFichier EPN_BAL_1.EPNB1_TYPE_DOC%type;
    gescode epn_balance.GESTION%type;
  regroupementLibCourt maracuja.gestion_agregat.ga_libelle%type;
    compte EPN_BALANCE.IMPUTATION%TYPE;
    debit_be NUMBER;
    credit_be NUMBER;
    debit_exer NUMBER;
    credit_exer NUMBER;

    numLigne number;

cursor c_balance is select pco_num, sum(debit_be) as debit_be, sum(credit_be) as credit_be, sum(debit_exer) as debit_exer, sum(credit_exer) as credit_exer from
    ( select pco_num, sum(ecd_debit) as debit_be, sum(ecd_credit) as credit_be,0 as debit_exer, 0 as credit_exer
    from maracuja.ecriture_detail ecd
    inner join maracuja.ecriture e on (ecd.ecr_ordre=e.ecr_ordre)
    inner join MARACUJA.gestion_agregat_repart gar on (gar.exe_ordre=ecd.exe_ordre and gar.ges_code=ecd.ges_code)
    inner join MARACUJA.gestion_agregat ga on (gar.ga_id=ga.ga_id )  
    where substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre=2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ga.ga_libelle = regroupementLibCourt
    group by pco_num
    union
    select pco_num,0 as debit_be,0 as credit_be,sum(ecd_debit) as debit_exer, sum(ecd_credit) as credit_exer
    from maracuja.ecriture_detail ecd    inner join maracuja.ecriture e on (ecd.ecr_ordre=e.ecr_ordre)
    inner join MARACUJA.gestion_agregat_repart gar on (gar.exe_ordre=ecd.exe_ordre and gar.ges_code=ecd.ges_code)
    inner join MARACUJA.gestion_agregat ga on (gar.ga_id=ga.ga_id )  
    where substr(e.ecr_etat,1,1)='V'
    and e.tjo_ordre<>2
    and e.exe_ordre=exeOrdre
    and to_date(e.ecr_date_saisie)<=vDateFichier
    and ga.ga_libelle = regroupementLibCourt
    group by pco_num
    )
    group by pco_num
    order by pco_num;


begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date);
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    -- regroupementlib doit commencer par +
    regroupementLibCourt := substr(regroupementLib,2);
    gesCode := substr(regroupementLib,2,10);
    
    typeFichier := '03';

    numLigne := 2;
    -- insert des lignes de detail
     OPEN c_balance;
        LOOP
        FETCH c_balance INTO  compte,  debit_be, credit_be, debit_exer, credit_exer;
            EXIT WHEN  c_balance%NOTFOUND;

            prv_insert_bal2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, debit_be, debit_exer, credit_be, credit_exer, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_balance;

    -- insert entete
     prv_insert_bal1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);

end;






-- se charge de remplir EPN_BAL_2
procedure prv_insert_bal2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne in out number, compte varchar2, debit_be NUMBER, debit_exer NUMBER, credit_be NUMBER, credit_exer NUMBER, deviseTaux NUMBER)
is
    debit_total NUMBER;
    credit_total NUMBER;
    solde_debiteur NUMBER;
    solde_crediteur NUMBER;
    vCompte varchar2(60);

    vDebitBe number;
    vDebitExer number;
    vCreditBe number;
    vCreditExer number;


begin

    vDebitBe := round(debit_be*deviseTaux, 2);
    vDebitExer := round(debit_exer*deviseTaux, 2);
    vCreditBe := round(credit_be*deviseTaux, 2);
    vCreditExer := round(credit_exer*deviseTaux, 2);

    debit_total  :=  vDebitBe  +  vDebitExer;
    credit_total  :=  vCreditBe  +  vCreditExer;

    if (debit_total=0 and credit_total=0) then
        numLigne := numLigne - 1;
        return;
    end if;

    IF debit_total > credit_total THEN
        solde_debiteur := debit_total - credit_total;
        solde_crediteur := 0;
    ELSE
        solde_debiteur := 0;
        solde_crediteur := credit_total - debit_total;
    END IF;

    vCompte := compte || '               ';

    INSERT INTO EPN.EPN_BAL_2
        VALUES (to_number(rang),  --EPNB_ORDRE,
                gesCode,   --EPNB2_GES_CODE,
                '2',    --EPNB2_TYPE,
                numLigne,   --EPNB2_NUMERO,
                1,    --EPNB2_TYPE_CPT,
                (SUBSTR(vCompte,1,15)),    --EPNB2_COMPTE,
                 vDebitBe,   --EPNB2_DEBBE,
                 vDebitExer, --EPNB2_DEBCUM,
                 debit_total,   --EPNB2_DEBTOT,
                 vCreditBe,   --EPNB2_CREBE,
                 vCreditExer,   --EPNB2_CRECUM,
                 credit_total,   --EPNB2_CRETOT,
                 solde_debiteur,   --EPNB2_BSDEB,
                 solde_crediteur,   --EPNB2_BSCRE,
                  exeOrdre,  --EPNB2_EXERCICE
                  codeBudget,  --EPNB2_COD_BUD
                  typeFichier --EPNB2_TYPE_DOC
                  );

end;



procedure prv_insert_bal1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER)
is

    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_bal_1.EPNB1_COD_NOMEN%type;
    vDateFichier2 date;

begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);

    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;


   -- Insert du header/footer du fichier
    INSERT INTO EPN.EPN_BAL_1 (EPNB_ORDRE, EPNB1_GES_CODE, EPNB1_TYPE,
               EPNB1_NUMERO, EPNB1_IDENTIFIANT, EPNB1_TYPE_DOC,
               EPNB1_COD_NOMEN, EPNB1_COD_BUD, EPNB1_EXERCICE,
               EPNB1_RANG, EPNB1_DATE, EPNB1_SIREN,
               EPNB1_SIRET, EPNB1_NB_ENREG)
        VALUES (   to_number(rang), --EPNB_ORDRE
                    gesCode, --EPNB1_GES_CODE
                    '1', --EPNB1_TYPE
                    1, --EPNB1_NUMERO (numero de ligne dans le fichier)
                    identifiantDGCP, --EPNB1_IDENTIFIANT
                    typeFichier, --EPNB1_TYPE_DOC
                    codeNomenclature, --EPNB1_COD_NOMEN
                    codeBudget, --EPNB1_COD_BUD
                    exeOrdre, --EPNB1_EXERCICE
                    rang, --EPNB1_RANG
                    TO_CHAR(vDateFichier2,'DDMMYYYY'), --EPNB1_DATE
                    siren, --EPNB1_SIREN
                    siret, --EPNB1_SIRET
                    nbEnregistrement --EPNB1_NB_ENREG);
                    );



end;


END;
/
CREATE OR REPLACE PACKAGE         epn.EpN3_ECCB
IS

procedure epn_genere_eccb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_eccb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);
--procedure prv_genere_eccb_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eccb_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2);

procedure prv_insert_ECCB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, credPrevOuvert NUMBER, credPrevOB NUMBER, credPrevNonEmpl NUMBER, credPrevExtourneNonEmpl NUMBER, sensCompte VARCHAR2, deviseTaux NUMBER);
procedure prv_insert_eccb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

--procedure prv_prepare_budnat_planco(exeOrdre NUMBER);
--procedure prv_prepare_ECCB_DEP(exeOrdre NUMBER);
--procedure prv_prepare_ECCB_REC(exeOrdre NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY         epn.Epn3_ECCB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_ECCB(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_CRE_BUD_1 where EPNCCB1_TYPE_DOC=typeFichier and EPNCCB1_COD_BUD=codeBudget and EPNCCB_ordre=rang and EPNCCB1_EXERCICE=exeOrdre;
    delete from EPN_CRE_BUD_2 where EPNCCB2_TYPE_DOC=typeFichier and EPNCCB2_COD_BUD=codeBudget and EPNCCB_ORDRE=rang and EPNCCB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_ECCB(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_ECCB(typeFichier, codeBudget, exeOrdre, rang);
--    prv_prepare_budnat_planco(exeOrdre);
--    prv_prepare_ECCB_DEP(exeOrdre);
--    prv_prepare_ECCB_REC(exeOrdre);
    
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_ECCB_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_ECCB_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
         if (substr(gescodesacd,1,1)='+') then
               prv_genere_ECCB_03G_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            else
              prv_genere_ECCB_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            end if;
            return;
        end if;
        
    end if;

end;




---- remplit la table BUDNAT_PLANCO a partir du plan comptable
--procedure prv_prepare_budnat_planco(exeOrdre NUMBER)
--is
--begin

--    -- construire la liste des comptes budgetaires
--    DELETE FROM BUDNAT_PLANCO;

--    INSERT INTO BUDNAT_PLANCO
--    SELECT PCO_NUM, SUBSTR(PCO_NUM,1,2), '1'
--    FROM maracuja.PLAN_COMPTABLE_exer
--    WHERE (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%') AND PCO_VALIDITE = 'VALIDE' and exe_ordre=exeOrdre;

--    INSERT INTO BUDNAT_PLANCO
--    SELECT PCO_NUM, SUBSTR(PCO_NUM,1,3), '2'
--    FROM maracuja.PLAN_COMPTABLE_exer
--    WHERE ( PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%')AND PCO_VALIDITE = 'VALIDE' and exe_ordre=exeOrdre;
--    
--    
--end;


---- construit le cadre 2 (depenses + ORV + previsions budgetaires)
--procedure prv_prepare_ECCB_DEP(exeOrdre NUMBER)
--is
--begin
--   -- construire le cadre 2
--    DELETE FROM EPN_DEP where exe_ordre=exeOrdre;
--    
--    --depenses + OR --
--    INSERT INTO EPN_DEP
--        SELECT 
--            db.PCO_NUM,
--            bp.PCO_NUM_BDN,
--            db.GES_CODE,
--            db.DEP_MONT MDT_MONT,
--            db.REVERS TIT_MONT,
--            0 CO,
--            bp.SECTION,
--            db.EPN_DATE EPN_DATE,
--            db.EXE_ORDRE
--            FROM EPN_DEP_BUD db, BUDNAT_PLANCO bp
--            WHERE db.PCO_NUM = bp.PCO_NUM and db.exe_ordre=exeOrdre;    
--    DELETE FROM EPN_DEP WHERE MDT_MONT = 0 AND CO = 0 AND TIT_MONT = 0;
--    
--    -- prévisions budgétaires --
--    INSERT INTO EPN_DEP       
--        SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
--            FROM EPN_BUDNAT bn , BUDNAT_PLANCO bp
--            WHERE bn.PCO_NUM = bp.PCO_NUM_bdn
--            AND (bn.PCO_NUM LIKE '6%' OR bn.PCO_NUM LIKE '2%')
--            AND CO <> 0
--            and bn.exe_ordre=exeOrdre
--            ORDER BY exe_ordre, ges_code, bn.pco_num;
--            
--        
--    
--end; 


---- construit le cadre 3 (recettes + Annulations + previsions budgetaires)
--procedure prv_prepare_ECCB_REC(exeOrdre NUMBER)
--is
--begin
--   -- construire le cadre 3
--    DELETE FROM EPN_REC where exe_ordre=exeOrdre;
--     DELETE FROM EPN_REC;

-- -- Recettes et Déductions--
--    INSERT INTO EPN_REC     
--        SELECT erb.PCO_NUM,
--          bp.PCO_NUM_BDN,
--          erb.GES_CODE,
--          erb.MONT_REC TIT_MONT,
--          erb.ANNUL_TIT_REC RED_MONT,
--          0 CO,
--          bp.SECTION,
--          erb.EPN_DATE,
--          erb.exe_ordre
--        FROM EPN_REC_BUD erb, BUDNAT_PLANCO bp
--        WHERE erb.PCO_NUM = bp.PCO_NUM and erb.exe_ordre=exeOrdre; 

--    DELETE FROM EPN_REC WHERE TIT_MONT = 0 AND RED_MONT = 0 AND CO = 0;

--    -- Prévisions budgétaires --
--    INSERT INTO EPN_REC  
--    SELECT UNIQUE bn.PCO_NUM, bn.PCO_NUM, GES_CODE, 0, 0, CO, bp.SECTION, DATE_CO, exe_ordre
--    FROM EPN_BUDNAT bn, BUDNAT_PLANCO bp
--    WHERE bp.PCO_NUM_BDN = bn.PCO_NUM
--    AND ( bn.PCO_NUM LIKE '7%' OR bn.PCO_NUM LIKE '1%')
--    AND CO <> 0
--    and bn.exe_ordre=exeOrdre;
--    
--end; 

-- generation des fichiers aggreges (type 02)
procedure prv_genere_ECCB_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    
    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;

--    cursor c_ECCB_DEP is SELECT pco_num_bdn, SUM(co), SUM(MDT_MONT - TIT_MONT), SUM(CO - MDT_MONT + TIT_MONT)
--            FROM EPN_DEP WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre
--            GROUP BY pco_num_bdn
--            order by pco_num_bdn;
            
--    CURSOR c_ECCB_REC IS
--            SELECT pco_num_bdn, SUM(co), SUM(TIT_MONT - RED_MONT)
--            FROM EPN_REC WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre           
--            GROUP BY pco_num_bdn
--            order by pco_num_bdn;     
            
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            --sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
        where exe_ordre=exeOrdre
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        --sum(recettes) RECETTES, sum(reductions) REDUCTIONS,
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d     
        where exe_ordre=exeOrdre
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_ECCB_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;

  
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            --sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
        where exe_ordre=exeOrdre
        and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        --sum(recettes) RECETTES, sum(reductions) REDUCTIONS,
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d     
        where exe_ordre=exeOrdre
        and ges_code in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;                  
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;

    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_ECCB_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;

    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;

  
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            --sum(mandats) MANDATS, sum(reversements) REVERSEMENTS, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
        where exe_ordre=exeOrdre
        and ges_code = gesCodeSACD
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        --sum(recettes) RECETTES, sum(reductions) REDUCTIONS,
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d     
        where exe_ordre=exeOrdre
        and ges_code = gesCodeSACD
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;                  
         
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
  
    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


procedure prv_genere_ECCB_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_CRE_BUD_1.EPNCCB1_TYPE_DOC%type;
    gescode EPN_CRE_BUD_1.EPNCCB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;

    consoNet NUMBER;
    
    credPrevOuvert      NUMBER;
    credPrevOB      NUMBER;
    credPrevNonEmpl NUMBER;
    credPrevExtourneNonEmpl NUMBER;
  regroupementLibCourt maracuja.gestion_agregat.ga_libelle%type;
  
    cursor c_ECCB_DEP is select PCO_NUM_BDN, 
            sum(montant_net) MONTANT_NET, sum(cr_ouverts) CR_OUVERTS, sum(non_empl) NON_EMPL 
        from comptefi.v_dvlop_dep d
            inner join MARACUJA.gestion_agregat_repart gar on (gar.exe_ordre=d.exe_ordre and gar.ges_code=d.ges_code)
            inner join MARACUJA.gestion_agregat ga on (gar.ga_id=ga.ga_id )  
        where d.exe_ordre=exeOrdre
        and ga.ga_libelle = regroupementLibCourt
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;      
        
     CURSOR c_ECCB_REC IS select pco_num_bdn, 
        sum(montant_net) MONTANT_NET, sum(previsions) PREVISIONS     
        from comptefi.v_dvlop_rec d   
         inner join MARACUJA.gestion_agregat_repart gar on (gar.exe_ordre=d.exe_ordre and gar.ges_code=d.ges_code)
         inner join MARACUJA.gestion_agregat ga on (gar.ga_id=ga.ga_id )  
        where d.exe_ordre=exeOrdre
        and ga.ga_libelle = regroupementLibCourt
        and DEP_DATE <= vDateFichier
        group by PCO_NUM_BDN
        order by PCO_NUM_BDN;                  
         
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    -- regroupementlib doit commencer par +
    regroupementLibCourt := substr(regroupementLib,2);
    gesCode := substr(regroupementLib,2,10);
    typeFichier := '03';
    numLigne := 2;
  
    
    credPrevOuvert := 0;
    credPrevOB := 0; -- virements internes ?
    credPrevNonEmpl := 0;
    credPrevExtourneNonEmpl := 0; -- impossible de recuperer les extournes
    

    -- insert des lignes de detail des depenses
     OPEN c_ECCB_DEP;
        LOOP
        FETCH c_ECCB_DEP INTO  compte,  consoNet, credPrevOuvert, credPrevNonEmpl;
            EXIT WHEN  c_ECCB_DEP%NOTFOUND;

            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'D', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_DEP;
    
    
    -- insert des lignes de detail des recettes
     OPEN c_ECCB_REC;
        LOOP
        FETCH c_ECCB_REC INTO  compte,  consoNet, credPrevOuvert;
            EXIT WHEN  c_ECCB_REC%NOTFOUND;
            if (credPrevOuvert > consoNet) then
                credPrevNonEmpl := credPrevOuvert - consoNet;
            else
                credPrevNonEmpl :=0;
            end if;
            prv_insert_ECCB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, credPrevOuvert, credPrevOB, credPrevNonEmpl, credPrevExtourneNonEmpl, 'R', deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_ECCB_REC;
            
   -- Insert du header/footer du fichier
   prv_insert_ECCB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;






-- se charge de remplir EPN_ECCB_2
procedure prv_insert_ECCB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, credPrevOuvert NUMBER, credPrevOB NUMBER, credPrevNonEmpl NUMBER, credPrevExtourneNonEmpl NUMBER, sensCompte VARCHAR2, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
        
        INSERT INTO EPN.EPN_CRE_BUD_2 
        VALUES (
                  to_number(rang),   --EPNCCB_ORDRE, 
                  gesCode,  --EPNCCB2_GES_CODE, 
                  '2',  --EPNCCB2_TYPE, 
                  numLigne,  --EPNCCB2_NUMERO, 
                  1,  --EPNCCB2_TYPE_CPT, 
                  (SUBSTR(vCompte,1,15)),  --EPNCCB2_COMPTE, 
                  credPrevOuvert*deviseTaux,  --EPNCCB2_CREPREVOUV, 
                  credPrevOB*deviseTaux,  --EPNCCB2_CREPREVOB, 
                  credPrevNonEmpl*deviseTaux ,  --EPNCCB2_CREPREVNOEMPL, 
                  credPrevExtourneNonEmpl*deviseTaux ,  --EPNCCB2_CREPREVEXTNOEMPL, 
                  sensCompte,  --EPNCCB2_SENSCPT, 
                  exeOrdre,  --EPNCCB2_EXERCICE, 
                  codeBudget,  --EPNCCB2_COD_BUD, 
                  typeFichier  --EPNCCB2_TYPE_DOC
                );
end;



procedure prv_insert_ECCB1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_CRE_BUD_1.EPNCCB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_CRE_BUD_1 
            VALUES (
                    to_number(rang), --EPNCCB_ORDRE, 
                    gesCode, --EPNCCB1_GES_CODE, 
                    '1', --EPNCCB1_TYPE, 
                    1, --EPNCCB1_NUMERO, 
                    identifiantDGCP, --EPNCCB1_IDENTIFIANT, 
                    typeFichier,--EPNCCB1_TYPE_DOC, 
                    codeNomenclature, --EPNCCB1_COD_NOMEN, 
                    codeBudget,--EPNCCB1_COD_BUD, 
                    exeOrdre,--EPNCCB1_EXERCICE, 
                    rang, --EPNCCB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNCCB1_DATE, 
                    siren, --EPNCCB1_SIREN, 
                    siret,--EPNCCB1_SIRET, 
                    nbEnregistrement --EPNCCB1_NBENREG
                    ); 
    

end;








END;
/
CREATE OR REPLACE PACKAGE         epn.EpN3_EDDB
IS

procedure epn_genere_eddb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_eddb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);

procedure prv_genere_eddb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eddb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eddb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_genere_eddb_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2);

procedure prv_insert_eddb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER);
procedure prv_insert_eddb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY                 epn.Epn3_EDDB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_eddb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_dep_bud_1 where EPNDB1_TYPE_DOC=typeFichier and EPNDB1_COD_BUD=codeBudget and EPNDB_ordre=rang and EPNDB1_EXERCICE=exeOrdre;
    delete from EPN_dep_bud_2 where EPNDB2_TYPE_DOC=typeFichier and EPNDB2_COD_BUD=codeBudget and EPNDB_ORDRE=rang and EPNDB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_eddb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_eddb(typeFichier, codeBudget, exeOrdre, rang);
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_eddb_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_eddb_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
         if (substr(gescodesacd,1,1)='+') then
              prv_genere_eddb_03G_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            else
             prv_genere_eddb_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            end if;
            return;
        end if;
        
    end if;

end;


-- generation des fichiers de balance consolides (type 01)
-- NB : Maracuja ne gere pas les comptabilites secondaires, donc ce fichier sera identique aux fichiers 02 (aggreges) 



-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_eddb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD
    WHERE EPN_DATE <= vDateFichier 
    AND exe_ordre = exeOrdre
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);    
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_eddb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD
    WHERE TO_DATE(EPN_DATE) <= vDateFichier 
    AND exe_ordre = exeOrdre
    AND GES_CODE in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_eddb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD
    WHERE TO_DATE(EPN_DATE) <= vDateFichier 
    AND exe_ordre = exeOrdre
    AND GES_CODE = gesCodeSACD
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;




-- generation des fichiers detailles (type 03) pour un regroupement de SACD
procedure prv_genere_eddb_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_DEP_BUD_1.EPNDB1_TYPE_DOC%type;
    gescode EPN_DEP_BUD_1.EPNDB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    depensesMontant NUMBER; 
    reversementsMontant NUMBER;
    depensesSurCreditExtourne NUMBER; 
    depensesExtournes NUMBER;    
    regroupementLibCourt maracuja.gestion_agregat.ga_libelle%type;

cursor c_eddb IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) 
    FROM EPN_DEP_BUD d
    inner join MARACUJA.gestion_agregat_repart gar on (gar.exe_ordre=d.exe_ordre and gar.ges_code=d.ges_code)
    inner join MARACUJA.gestion_agregat ga on (gar.ga_id=ga.ga_id )  
    WHERE TO_DATE(EPN_DATE) <= vDateFichier 
    AND d.exe_ordre = exeOrdre
     and ga.ga_libelle = regroupementLibCourt
    GROUP BY PCO_NUM
    ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    -- regroupementlib doit commencer par +
    regroupementLibCourt := substr(regroupementLib,2);
    gesCode := substr(regroupementLib,2,10);
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    depensesSurCreditExtourne := 0;
    depensesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_eddb;
        LOOP
        FETCH c_eddb INTO  compte,  depensesMontant, reversementsMontant;
            EXIT WHEN  c_eddb%NOTFOUND;

            prv_insert_eddb2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, depensesMontant, reversementsMontant, depensesSurCreditExtourne, depensesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_eddb;
    
   -- Insert du header/footer du fichier
   prv_insert_eddb1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;







-- se charge de remplir EPN_eddb_2
procedure prv_insert_eddb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
         INSERT INTO EPN.EPN_DEP_BUD_2  
            VALUES (
                    to_number(rang),    --EPNDB_ORDRE, 
                    gesCode,    --EPNDB2_GES_CODE, 
                    '2',    --EPNDB2_TYPE, 
                     numLigne,   --EPNDB2_NUMERO, 
                     1,   --EPNDB2_TYPE_CPT, 
                     (SUBSTR(vCompte,1,15)),   --EPNDB2_COMPTE, 
                     depensesMontant*deviseTaux,  --EPNDB2_MNTBRUT, 
                     depensesSurCreditExtourne*deviseTaux,   --EPNDB2_DEP_CREEXT, 
                     reversementsMontant*deviseTaux,   --EPNDB2_MNTREVERS, 
                     depensesExtournes*deviseTaux,   --EPNDB2_DEP_DEPEXT, 
                     (depensesMontant-reversementsMontant)*deviseTaux,   --EPNDB2_MNTNET, 
                     exeOrdre,   --EPNDB2_EXERCICE, 
                     codeBudget,   --EPNDB2_COD_BUD, 
                     typeFichier   --EPNDB2_TYPE_DOC
                    );                           

    --INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;

end;



procedure prv_insert_eddb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_dep_bud_1.EPNDB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_DEP_BUD_1 
            VALUES (
                    to_number(rang), --EPNDB_ORDRE, 
                    gesCode, --EPNDB1_GES_CODE, 
                    '1', --EPNDB1_TYPE, 
                    1, --EPNDB1_NUMERO, 
                    identifiantDGCP, --EPNDB1_IDENTIFIANT, 
                    typeFichier,--EPNDB1_TYPE_DOC, 
                    codeNomenclature, --EPNDB1_COD_NOMEN, 
                    codeBudget,--EPNDB1_COD_BUD, 
                    exeOrdre,--EPNDB1_EXERCICE, 
                    rang, --EPNDB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNDB1_DATE, 
                    siren, --EPNDB1_SIREN, 
                    siret,--EPNDB1_SIRET, 
                    nbEnregistrement --EPNDB1_NBENREG
                    ); 
        

end;









--PROCEDURE Epn_Proc_Depbud (ordre VARCHAR2,comp VARCHAR2,EXE_ORDRE NUMBER, V_DATE VARCHAR2)
--IS


--NUM_SEQ NUMBER(5);
--PCO_NUM VARCHAR2(15);
--EPN_DATE DATE;
--DEP_MONT  NUMBER(15,2);
--REVERS NUMBER(15,2);
--EXERC VARCHAR2(4);
--EXERCN1 VARCHAR2(4);
--SIRET NUMBER(14);
--SIRET_C VARCHAR2(14);
--IDENTIFIANT NUMBER(10);
--SIREN NUMBER(9);
--VDATE VARCHAR2(9);
--VDATE1 VARCHAR2(9);
--VARDATE DATE;
--VARDATE1 DATE;
--RANG VARCHAR2(2);
--compte VARCHAR2(30);
--CPT NUMBER;
--COD_BUDGET VARCHAR2(2);
--ordre_num NUMBER;
--TYP_DOC VARCHAR2(2);
--codeNomenclature VARCHAR2(2);
--flag INTEGER;
--deviseTaux NUMBER; -- Ajout by ProfesseurBougna le 18 mars 2008  

--    CURSOR c_tot IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS)
--     FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
--     GROUP BY PCO_NUM
--     ORDER BY PCO_NUM;

--    CURSOR c_sacd  IS SELECT PCO_NUM,SUM( DEP_MONT), SUM( REVERS)
--    FROM EPN_DEP_BUD WHERE TO_DATE(EPN_DATE) <= VARDATE AND GES_CODE = comp AND exe_ordre = exerc
--    GROUP BY PCO_NUM
--    ORDER BY PCO_NUM;

--    CURSOR c_hors_sacd IS SELECT PCO_NUM, SUM( DEP_MONT), SUM( REVERS) FROM EPN_DEP_BUD
--    WHERE TO_DATE(EPN_DATE) <= VARDATE AND exe_ordre = exerc
--    AND GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc)
--     GROUP BY PCO_NUM
--     ORDER BY PCO_NUM;


--BEGIN

--/*DELETE TAB_MANDAT_MAJ;

--INSERT INTO TAB_MANDAT_MAJ SELECT * FROM MANDAT_MAJ;*/

--    ORDRE_NUM := ordre;
--    EXERC := EXE_ORDRE;
--    codeNomenclature := epn_getCodeNomenclature(EXERC);
--    deviseTaux := epn_getDeviseTaux(EXERC);  -- Ajout by ProfesseurBougna le 18 mars 2008    

--    IF comp = 'HSA'  THEN
--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
--        AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_EXERCICE = exerc
--            AND EPNDB1_GES_CODE NOT IN (SELECT sacd FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc);
--        END IF;

--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
--        AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_EXERCICE = exerc
--            AND EPNDB2_GES_CODE NOT IN (SELECT SACD FROM CODE_BUDGET_SACD WHERE exe_ordre = exerc) ;
--        END IF;

--    ELSE
--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_1
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_1 WHERE EPNDB_ORDRE = ordre_num AND EPNDB1_GES_CODE = comp
--            AND EPNDB1_EXERCICE = exerc;
--        END IF;

--        SELECT COUNT(*) INTO CPT FROM EPN_DEP_BUD_2
--        WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp AND EPNDB2_EXERCICE = exerc;
--        IF CPT <> 0  THEN
--            DELETE FROM EPN_DEP_BUD_2 WHERE EPNDB_ORDRE = ordre_num AND EPNDB2_GES_CODE = comp
--            AND EPNDB2_EXERCICE = exerc;
--        END IF;

--    END IF;


--    -- SELECT PARAM_VALUE INTO EXERC FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';

--    SELECT replace(REPLACE(PAR_VALUE, '.',''),' ','') INTO SIRET_C FROM maracuja.PARAMETRE
--    WHERE PAR_KEY = 'NUMERO_SIRET' AND exe_ordre=EXERC;
--    SIRET := TO_NUMBER(SUBSTR(SIRET_C,1,14));
--    SIREN := TO_NUMBER(SUBSTR(SIRET_C,1,9));

--    SELECT PAR_VALUE INTO IDENTIFIANT FROM maracuja.PARAMETRE
--    WHERE PAR_KEY = 'IDENTIFIANT_DGCP' AND exe_ordre = EXERC;
--    IF IDENTIFIANT IS NULL THEN
--        RAISE_APPLICATION_ERROR (-20002,'Parametre IDENTIFIANT_DGCP non renseigné dans     maracuja.PARAMETRE');
--    END IF;

--    IF ordre = '1' THEN
--        RANG := '01';
--        VDATE := '3103' || EXERC;
--        VDATE1 := '3103' || EXERC;
--    ELSE IF ordre =  '2' THEN
--            RANG := '02';
--            VDATE := '3006' || EXERC;
--            VDATE1 := '3006' || EXERC;
--        ELSE IF ordre = '3' THEN
--                RANG := '03';
--                VDATE := '3009' || EXERC;
--                VDATE1 := '3009' || EXERC;
--            ELSE IF ordre = '4' THEN
--                    RANG := '04';
--                    VDATE := '3112' || EXERC;
--                    VDATE1 := '3112' || EXERC;
--                ELSE IF ordre = '5' THEN
--                    RANG := '05';
--                    VDATE := V_DATE;
--                    VDATE1 := '3112' || EXERC;
--                    ELSE IF ordre = '6' THEN
--                            RANG := '06';
--                            VDATE := V_DATE;
--                            VDATE1 := '3112' || EXERC;
--                        ELSE
--                            RAISE_APPLICATION_ERROR (-20001,'type édition non renseigné');
--                        END IF;
--                    END IF;
--                END IF;
--            END IF;
--        END IF;
--    END IF;

--    VARDATE := TO_DATE(VDATE,'ddmmyyyy');
--    VARDATE1 := TO_DATE(VDATE1,'ddmmyyyy');

--        /*SELECT COUNT(*) INTO cpt FROM jefy.SACD_PARAM;
--        IF ( comp <> 'ALL') AND (comp <> 'HSA') AND (cpt <> 0) THEN
--                  TYP_DOC := '02';
--        ELSE
--               TYP_DOC := '03';
--           END IF;*/

--    -- Correction type document --
--    TYP_DOC := '03';

--    /*    IF  comp = 'ALL' THEN
--               COD_BUDGET := '01';
--        ELSE  IF comp =  'HSA' THEN
--                COD_BUDGET := '00';
--            ELSE
--                SELECT   COD_BUD  INTO COD_BUDGET  FROM CODE_BUDGET_SACD WHERE sacd = comp;
--            END IF;
--        END IF;*/

--    -- Correction du code budget pour l'établissement ou SACD --
--    IF comp = 'HSA' THEN
--        COD_BUDGET := '01';
--    ELSE
--        SELECT COUNT(*) INTO flag FROM CODE_BUDGET_SACD
--        WHERE sacd = comp AND exe_ordre = exerc;
--        IF (flag=0) THEN
--           RAISE_APPLICATION_ERROR(-20001, 'Vous devez renseigner la table EPN.CODE_BUDGET_SACD pour le SACD '||comp);
--        END IF;
--        SELECT COD_BUD INTO COD_BUDGET FROM CODE_BUDGET_SACD
--        WHERE sacd = comp AND EXE_ORDRE = exerc;
--    END IF;

--    INSERT INTO EPN_DEP_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,                     EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);

--    NUM_SEQ := 2;
--    /*
--      IF  ordre = '4'  OR   ordre = '5'  OR   ordre = '6' THEN
--      SELECT PARAM_VALUE INTO EXERCN1 FROM jefy.PARAMETRES WHERE PARAM_KEY = 'EXERCICE';
--    --        DATE := '31122005';
--           VDATE := '3112'|| EXERCN1;
--            VARDATE := TO_DATE(VDATE,'dd/mm/yyyy');
--      END IF;
--    */


--    -- Pas de consolidation Etablissement + SACD
--    /* IF comp = 'ALL'  THEN
--    -- Etablissement + SACD --
--    OPEN c_tot;
--    LOOP
--        FETCH c_tot INTO  PCO_NUM,  DEP_MONT, REVERS;
--        EXIT WHEN c_tot%NOTFOUND;
--        compte := PCO_NUM || '              ';
--        INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT, 0, REVERS, 0,  DEP_MONT-REVERS);
--        NUM_SEQ := NUM_SEQ + 1;
--    END LOOP;
--    CLOSE c_tot;

--    ELSE */
--    IF comp = 'HSA' THEN
--        --  hors SACD
--        OPEN c_hors_sacd ;
--        LOOP
--            FETCH c_hors_sacd INTO  PCO_NUM, DEP_MONT, REVERS ;
--            EXIT WHEN c_hors_sacd%NOTFOUND ;
--            compte := PCO_NUM || '              ' ;

--            -- Modifié by ProfesseurBougna le 18 mars 2008  
--            INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;
--            NUM_SEQ := NUM_SEQ + 1 ;
--            END LOOP;
--        CLOSE c_hors_sacd;

--    ELSE
--        --  cas SACD
--        OPEN c_sacd;
--        LOOP
--            FETCH c_sacd INTO  PCO_NUM,  DEP_MONT, REVERS;
--            EXIT WHEN c_sacd%NOTFOUND;
--            compte := PCO_NUM || '              ';
--            
--            -- Modifié by ProfesseurBougna le 18 mars 2008  
--            INSERT INTO EPN_DEP_BUD_2 VALUES (ORDRE, comp, '2', num_seq,1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc);
--            NUM_SEQ := NUM_SEQ + 1;
--        END LOOP;
--        CLOSE c_sacd;
--    END IF;

--    -- END IF;

--    -- Correction M1 MAJ du Nb enreg de l'édition du ges_code
--    UPDATE EPN_DEP_BUD_1 SET EPNDB1_NBENREG = num_seq
--    WHERE EPNDB_ORDRE = ORDRE_NUM AND EPNDB1_GES_CODE = comp AND EPNDB1_EXERCICE = exerc;

--END;


END;
/
CREATE OR REPLACE PACKAGE         epn.EpN3_EDRB
IS

procedure epn_genere_edrb(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_nettoie_edrb(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2);

procedure prv_genere_edrb_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2);
procedure prv_genere_edrb_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2);

procedure prv_insert_edrb2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER);
procedure prv_insert_edrb1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER);

END;
/


CREATE OR REPLACE PACKAGE BODY         epn.Epn3_EDRB  IS
-- v. 3 (changement structure du package, prise en charge des fichiers aggreges)



procedure prv_nettoie_EDRB(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang VARCHAR2)
is
begin
    delete from EPN_REC_BUD_1 where EPNRB1_TYPE_DOC=typeFichier and EPNRB1_COD_BUD=codeBudget and EPNRB_ordre=rang and EPNRB1_EXERCICE=exeOrdre;
    delete from EPN_REC_BUD_2 where EPNRB2_TYPE_DOC=typeFichier and EPNRB2_COD_BUD=codeBudget and EPNRB_ORDRE=rang and EPNRB2_EXERCICE=exeOrdre;    
end;


procedure epn_genere_EDRB(identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is

begin
    prv_nettoie_EDRB(typeFichier, codeBudget, exeOrdre, rang);
    
    if (typeFichier = '01') then
        -- si type=01 (consolide), on totalise tout (idem type 2 car maracuja ne gere pas les comptabilites secondaires)
        return;        
    elsif (typeFichier = '02') then
         -- si type=02 (aggrege), on totalise tout
         prv_genere_EDRB_02(identifiantDGCP, exeOrdre, rang, v_date);
        return;    
    elsif (typeFichier = '03') then
        -- si type=03 (detaille), on totalise agence hors SACD ou SACD
        if (codeBudget='01') then
            prv_genere_EDRB_03_01(identifiantDGCP, exeOrdre, rang , v_date );
        else
         if (substr(gescodesacd,1,1)='+') then
               prv_genere_EDRB_03G_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            else
               prv_genere_EDRB_03_xx(identifiantDGCP, codeBudget, exeOrdre , rang , gescodeSACD, v_date );
            end if;
           
            return;
        end if;
        
    end if;

end;





-- generation des fichiers de balance aggreges (type 02)
procedure prv_genere_EDRB_02(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
    
    numLigne number;
    compte varchar2(20);   
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE EPN_DATE <= vDateFichier AND exe_ordre = exeOrdre     
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
          

begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'ALL';
    typeFichier := '02';
    numLigne := 2;
    
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
end;


-- generation des fichiers balance detailles (type 03) pour l'agence (code budget 01), donc hors SACD
procedure prv_genere_EDRB_03_01(identifiantDGCP VARCHAR2, exeOrdre number, rang VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;
    codeBudget varchar2(2);
    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre
     AND GES_CODE in (select ges_code from maracuja.gestion_exercice where exe_ordre=exeOrdre and pco_num_185 is null)
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);
    codeBudget := '01';
    gesCode := 'HSA';
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);   
    
    
end;


-- generation des fichiers detailles (type 03) pour un SACD
procedure prv_genere_EDRB_03_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, gescodeSACD VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD WHERE TO_DATE(EPN_DATE) <= vDateFichier AND exe_ordre = exeOrdre
     AND GES_CODE = gescodeSACD
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    gesCode := gesCodeSACD;
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);     
    
end;



-- generation des fichiers detailles (type 03) pour regroupement de SACD
procedure prv_genere_EDRB_03G_xx(identifiantDGCP VARCHAR2, codeBudget VARCHAR2, exeOrdre number, rang VARCHAR2, regroupementLib VARCHAR2, v_date VARCHAR2)
is
    vDateFichier DATE;

    deviseTaux NUMBER;
    typeFichier EPN_REC_BUD_1.EPNRB1_TYPE_DOC%type;
    gescode EPN_REC_BUD_1.EPNRB1_ges_code %type;  
      
    compte varchar2(20);   
    numLigne number;
    recettesMontant NUMBER; 
    annulationsMontant NUMBER;
    recettesSurPrevisionExtourne NUMBER; 
    recettesExtournes NUMBER;    
    regroupementLibCourt maracuja.gestion_agregat.ga_libelle%type;

cursor c_EDRB IS SELECT PCO_NUM, SUM(MONT_REC), SUM(ANNUL_TIT_REC)
     FROM EPN_REC_BUD  d
    inner join MARACUJA.gestion_agregat_repart gar on (gar.exe_ordre=d.exe_ordre and gar.ges_code=d.ges_code)
    inner join MARACUJA.gestion_agregat ga on (gar.ga_id=ga.ga_id ) 
     
     WHERE TO_DATE(EPN_DATE) <= vDateFichier 
     AND d.exe_ordre = exeOrdre
      and ga.ga_libelle = regroupementLibCourt
     GROUP BY PCO_NUM
     ORDER BY PCO_NUM; 
         
        
begin
    vDateFichier := epn3.prv_getDateForRang(rang, exeOrdre, v_date); 
    deviseTaux := epn3.epn_getDeviseTaux(exeOrdre);   
    -- regroupementlib doit commencer par +
    regroupementLibCourt := substr(regroupementLib,2);
    gesCode := substr(regroupementLib,2,10);
    typeFichier := '03';
    numLigne := 2;
    -- impossible de recuperer les extournes
    recettesSurPrevisionExtourne := 0;
    recettesExtournes :=0;
    
    -- insert des lignes de detail
     OPEN c_EDRB;
        LOOP
        FETCH c_EDRB INTO  compte,  recettesMontant, annulationsMontant;
            EXIT WHEN  c_EDRB%NOTFOUND;

            prv_insert_EDRB2(typeFichier, codeBudget, exeOrdre, rang, gesCode, numLigne, compte, recettesMontant, annulationsMontant, recettesSurPrevisionExtourne, recettesExtournes, deviseTaux);
            numLigne := numLigne + 1;

        END LOOP;
        CLOSE c_EDRB;
    
   -- Insert du header/footer du fichier
   prv_insert_EDRB1(gesCode, identifiantDGCP, typeFichier, codeBudget, exeOrdre, rang, vDateFichier, numLigne);     
    
end;





-- se charge de remplir EPN_EDRB_2
procedure prv_insert_EDRB2(typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre Number, rang VARCHAR2, gesCode varchar2, numLigne number, compte varchar2, depensesMontant NUMBER, reversementsMontant NUMBER, depensesSurCreditExtourne NUMBER, depensesExtournes NUMBER, deviseTaux NUMBER) 
is
    vCompte varchar2(60);
begin
        vCompte := compte || '               ';
         INSERT INTO EPN.EPN_REC_BUD_2  
            VALUES (
                    to_number(rang),    --EPNRB_ORDRE, 
                    gesCode,    --EPNRB2_GES_CODE, 
                    '2',    --EPNRB2_TYPE, 
                     numLigne,   --EPNRB2_NUMERO, 
                     1,   --EPNRB2_TYPE_CPT, 
                     (SUBSTR(vCompte,1,15)),   --EPNRB2_COMPTE, 
                     depensesMontant*deviseTaux,  --EPNRB2_MNTBRUT, 
                     depensesSurCreditExtourne*deviseTaux,   --EPNRB2_DEP_CREEXT, 
                     reversementsMontant*deviseTaux,   --EPNRB2_MNTREVERS, 
                     depensesExtournes*deviseTaux,   --EPNRB2_DEP_DEPEXT, 
                     (depensesMontant-reversementsMontant)*deviseTaux,   --EPNRB2_MNTNET, 
                     exeOrdre,   --EPNRB2_EXERCICE, 
                     codeBudget,   --EPNRB2_COD_BUD, 
                     typeFichier   --EPNRB2_TYPE_DOC
                    );                           

    --INSERT INTO EPN_REC_BUD_2 VALUES (ORDRE, comp, '2', num_seq, 1, (SUBSTR(compte,1,15)), DEP_MONT*deviseTaux, 0, REVERS*deviseTaux, 0, (DEP_MONT-REVERS)*deviseTaux, exerc) ;

end;



procedure prv_insert_EDRB1(gesCode VARCHAR2, identifiantDGCP VARCHAR2, typeFichier VARCHAR2, codeBudget VARCHAR2, exeOrdre NUMBER, rang varchar2, dateFichier DATE, nbEnregistrement NUMBER) 
is
   
    siret varchar2(14);
    siren varchar2(9);
    codeNomenclature epn_REC_BUD_1.EPNRB1_COD_NOMEN%type;
    vDateFichier2 date;
    
begin

    siren := epn3.prv_getCodeSiren(exeOrdre);
    siret := epn3.prv_getCodeSiret(exeOrdre);    
    codeNomenclature := epn3.epn_getCodeNomenclature(exeOrdre);
        
    vDateFichier2 := dateFichier;
   if (rang = '05' or rang = '06') then
         vDateFichier2 := to_date('3112'||exeOrdre, 'ddmmyyyy');
   end if;
       
   -- Insert du header/footer du fichier
   
        INSERT INTO EPN.EPN_REC_BUD_1 
            VALUES (
                    to_number(rang), --EPNRB_ORDRE, 
                    gesCode, --EPNRB1_GES_CODE, 
                    '1', --EPNRB1_TYPE, 
                    1, --EPNRB1_NUMERO, 
                    identifiantDGCP, --EPNRB1_IDENTIFIANT, 
                    typeFichier,--EPNRB1_TYPE_DOC, 
                    codeNomenclature, --EPNRB1_COD_NOMEN, 
                    codeBudget,--EPNRB1_COD_BUD, 
                    exeOrdre,--EPNRB1_EXERCICE, 
                    rang, --EPNRB1_RANG, 
                    TO_CHAR(vDateFichier2,'DDMMYYYY'),--EPNRB1_DATE, 
                    siren, --EPNRB1_SIREN, 
                    siret,--EPNRB1_SIRET, 
                    nbEnregistrement --EPNRB1_NBENREG
                    ); 
        
        --INSERT INTO EPN_REC_BUD_1 VALUES (ORDRE, comp, '1', 1, IDENTIFIANT, TYP_DOC, codeNomenclature, COD_BUDGET,                     EXERC, RANG, (TO_CHAR(VARDATE1,'DDMMYYYY')), SIREN, SIRET, 0);
    

end;


END;
/




create or replace procedure grhum.inst_patch_maracuja_1940
is
begin
	 maracuja.util.creer_parametre_exenonclos(
			'org.cocktail.gfc.maracuja.epn.traiterregroupementssuivantcommesacdaveccodebuget', 
			'',
			'Dans les cas où un SACD a plusieurs codes gestions pour les remontées EPN. Liste séparée par des virgules du type "Regroupement":"Code budget". Le regroupement doit exister comme un regroupement de codes gestion SACD, le code budget permet à la DGFIP d''identifier le SACD.ex. SAIC:02'
		);
   jefy_admin.patch_util.end_patch (4, '1.9.4.0');
end;
/

