SET DEFINE OFF;
CREATE TABLE MARACUJA.ZANALYSE_PROBLEM
(
  ZAP_ID              NUMBER(27)                NOT NULL,
  EXE_ORDRE			  NUMBER(4) NOT NULL,	
  ZAP_CATEGORIE       VARCHAR2(100)        NOT NULL,
  ZAP_SOUS_CATEGORIE  VARCHAR2(100)        NOT NULL,
  ZAP_ENTITY          VARCHAR2(100),
  ZAP_ENTITY_KEY      VARCHAR2(100),
  ZAP_ENTITY_KEY_VALUE VARCHAR2(100),
  ZAP_PROBLEME        VARCHAR2(1000)        NOT NULL,
  ZAP_CONSEQUENCE     VARCHAR2(1000)        ,
  ZAP_SOLUTION        VARCHAR2(1000),
  ZAP_DATE            DATE                      NOT NULL,
  ZAP_NIVEAU		number(2)
)
TABLESPACE GFC;


CREATE UNIQUE INDEX MARACUJA.ZANALYSE_PROBLEM_PK ON MARACUJA.ZANALYSE_PROBLEM
(ZAP_ID)
TABLESPACE GFC_INDX;


ALTER TABLE MARACUJA.ZANALYSE_PROBLEM ADD (
  CONSTRAINT ZANALYSE_PROBLEM_PK
 PRIMARY KEY
 (ZAP_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    );
    
CREATE SEQUENCE  "MARACUJA"."ZANALYSE_PROBLEM_SEQ"  
MINVALUE 1 MAXVALUE 999999999999999999999999999 
INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE ;
 
    





  CREATE OR REPLACE PACKAGE "MARACUJA"."ZANALYSE" IS

    PROCEDURE checkRejets(exeordre INTEGER);
    PROCEDURE checkBordereauNonVise(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco(exeordre INTEGER);
    PROCEDURE checkActionDepenses(exeordre INTEGER);
    PROCEDURE checkActionRecettes(exeordre INTEGER);
    PROCEDURE checkIncoherenceMandats(exeordre INTEGER);
    PROCEDURE checkBalancePi(exeOrdre INTEGER);
    PROCEDURE checkBalanceGen(exeOrdre INTEGER);
    PROCEDURE checkBalance185(exeOrdre INTEGER);
    
    
    procedure checkAllProblemes(exeOrdre integer);
    
    
END; 
 

 
  CREATE OR REPLACE PACKAGE BODY "MARACUJA"."ZANALYSE" 
IS
  -- verifier s'il reste des bordereaux de rejet non vises
PROCEDURE checkRejets
  (
    exeordre INTEGER)
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau_rejet%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau_rejet
    where exe_ordre=exeOrdre
    and BRJ_ETAT  <>'VISE';
BEGIN
  categorie := 'BORDEREAU DE REJET';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau de rejet non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BRJ_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU_REJET',                                                         --ZAP_ENTITY,
        'MARACUJA.BORDEREAU_REJET.BRJ_ORDRE',                                               --ZAP_ENTITY_KEY,
        leBordereau.brj_ordre,                                                              --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau de rejet ',                                                     --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBordereauNonVise
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau
    where exe_ordre=exeOrdre
    and BOR_ETAT   ='VALIDE';
BEGIN
  categorie := 'BORDEREAU';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BOR_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU',                                                               --ZAP_ENTITY,
        'MARACUJA.BORDEREAU.BOR_ID',                                                        --ZAP_ENTITY_KEY,
        leBordereau.bor_id,                                                                 --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau',                                                               --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkIncoherenceMandats
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and man_ETAT   ='ATTENTE'
    and bor_etat  in ('VISE');
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num
    into borNum
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'Etat du mandat (' || objet.man_etat || ') incoherent avec etat du bordereau (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Intervention necessaire dans la base de donn�es : remettre la bonne valeur (VISE ou PAYE) dans le champ man_etat pour man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkEcritureDetailPlanco
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre
      and substr(pco_VALIDITE,1,1)<>'V'
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte non valide (numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionDepenses
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_depense.depense_papier.DPP_NUMERO_FACTURE%type;
  founom jefy_depense.v_Fournisseur.fou_nom%type;
  dateFacture jefy_depense.depense_papier.dpp_date_FACTURE%type;
  objet jefy_depense.depense_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_depense.depense_ctrl_action
    where exe_ordre  =exeOrdre
    and TYAC_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_depense
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Depenses';
    select dpp_numero_facture ,
      dpp_date_facture
    into numero,
      dateFacture
    from jefy_depense.depense_papier dpp,
      jefy_depense.depense_budget db
    where db.dpp_id=dpp.dpp_id
    and db.dep_id  =objet.dep_id;
    select distinct fou_nom
    into founom
    from jefy_depense.v_fournisseur f,
      jefy_depense.depense_budget db,
      jefy_depense.depense_papier dpp
    where db.dpp_id=dpp.dpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.dep_id  =objet.dep_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / fournisseur:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION.DACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.DACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la liquidation ',                             --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionRecettes
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_recette.recette_papier.rpp_NUMERO%type;
  founom varchar2
  (
    200
  )
  ;
  dateFacture jefy_recette.recette_papier.rpp_date_recette%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_recette.recette_ctrl_action
    where exe_ordre  =exeOrdre
    and LOLF_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_recette
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Recettes';
    select rpp_numero,
      rpp_date_recette
    into numero,
      dateFacture
    from jefy_recette.recette_papier dpp,
      jefy_recette.recette_budget db
    where db.rpp_id=dpp.rpp_id
    and db.rec_id  =objet.rec_id;
    select distinct adr_nom
      ||' '
      ||adr_prenom
    into founom
    from grhum.v_fournis_grhum f,
      jefy_recette.recette_budget db,
      jefy_recette.recette_papier dpp
    where db.rpp_id=dpp.rpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.rec_id  =objet.rec_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / client:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION.RACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.RACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la recette ',                                 --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalancePi
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre
  and (pco_num like '186%'
  or pco_num like '187%');
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'PRESTATIONS INTERNES';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : le solde des comptes 186 et 187 devrait etre nul (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalanceGen
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre;
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'GENERALE';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalance185
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num like '185%';
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'COMPTES DE LIAISON 185';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance du 185 n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
procedure checkAllProblemes
  (
    exeOrdre integer
  )
is
begin
  delete from MARACUJA.ZANALYSE_PROBLEM where exe_ordre=exeOrdre;
  
  checkRejets(exeordre);
  checkBordereauNonVise(exeOrdre);
  checkIncoherenceMandats(exeOrdre);
  checkEcritureDetailPlanco(exeordre);
  checkActionDepenses(exeordre);
  checkActionRecettes(exeordre);
  checkBalanceGen(exeOrdre);
  checkBalancePi(exeOrdre);
  checkBalance185(exeOrdre);
end;
END;
 

 

