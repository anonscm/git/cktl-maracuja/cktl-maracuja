SET DEFINE OFF;

  CREATE OR REPLACE FORCE VIEW "MARACUJA"."V_JEFY_MULTIEX_TITRE" ("EXE_EXERCICE", "TIT_ORDRE", "TIT_DATE", "TIT_MONT", "TIT_MONTTVA", "TIT_LIB", "TIT_TYPE", "TIT_PIECE", "JOU_ORDRE", "FOU_ORDRE", "PCO_NUM", "TIT_IMPUTTVA", "BOR_ORDRE", "ORG_ORDRE", "TIT_INTERNE", "TIT_NUM", "AGT_ORDRE", "TIT_STAT", "GES_CODE", "TIT_DEBITEUR", "DEP_ORDRE", "RIB_ORDRE", "TIT_MONNAIE", "TIT_VIREMENT", "MOD_CODE", "DST_CODE", "TIT_REF", "CONV_ORDRE", "CAN_CODE", "PRES_ORDRE") AS 
  select EXE_EXERCICE, TIT_ORDRE, TIT_DATE, TIT_MONT, TIT_MONTTVA, 
 TIT_LIB, TIT_TYPE, TIT_PIECE, JOU_ORDRE, FOU_ORDRE, 
 PCO_NUM, TIT_IMPUTTVA, BOR_ORDRE, ORG_ORDRE, TIT_INTERNE, 
 TIT_NUM, AGT_ORDRE, TIT_STAT, GES_CODE, TIT_DEBITEUR, 
 DEP_ORDRE, RIB_ORDRE, TIT_MONNAIE, TIT_VIREMENT, MOD_CODE, 
 DST_CODE, TIT_REF, CONV_ORDRE, CAN_CODE, PRES_ORDRE
 from maracuja.jefy_old_titre
UNION ALL
SELECT exe_ordre AS exe_exercice, tit_ordre, TIT_DATE_REMISE AS tit_date, REPLACE( TO_CHAR(TIT_TTC),',','.' )AS tit_mont,  REPLACE( TO_CHAR(TIT_TVA),',','.' )AS tit_monttva, tit_libelle AS tit_lib, NULL AS tit_type, tit_nb_piece AS tit_piece, TO_NUMBER(NULL) AS jou_ordre, f.fou_ordre, pco_num, NULL AS tit_imputtva, bor_ordre, org_ordre,
TO_NUMBER(NULL) AS tit_interne,  TIT_NUMero AS tit_num,utl_ORDRE AS agt_ordre, NULL AS TIT_STAT, GES_CODE,  f.ADR_NOM || ' ' || f.adr_prenom    AS TIT_DEBITEUR, TO_NUMBER(NULL) AS DEP_ORDRE, TO_NUMBER(NULL) AS RIB_ORDRE, NULL AS TIT_MONNAIE,
NULL AS TIT_VIREMENT, NULL AS MOD_CODE,
NULL AS DST_CODE, NULL AS TIT_REF, TO_NUMBER(NULL) AS CONV_ORDRE, NULL AS CAN_CODE, TO_NUMBER(NULL) AS PRES_ORDRE
FROM maracuja.TITRE t, v_fournisseur f
WHERE t.exe_ordre>2006 AND t.fou_ordre=f.fou_ordre ;

/



CREATE OR REPLACE FUNCTION COMPTEFI.EXECUTION_BUD (exeordre number, compte VARCHAR2, comp VARCHAR2, sacd CHAR, pconature char)
RETURN NUMBER

IS
 nombre NUMBER(12,2);

BEGIN

IF exeordre >= 2005 THEN
   	 IF pconature = 'D' THEN
	 	 IF sacd = 'O' THEN
	   	 	SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvlopdep
			WHERE PCO_NUM LIKE compte AND GES_CODE = comp and exe_ordre =exeordre;
         ELSIF sacd = 'G' THEN
	   	 	SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvlopdep
			WHERE PCO_NUM LIKE compte and exe_ordre =exeordre;
         ELSE
	   	 	SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvlopdep
			WHERE PCO_NUM LIKE compte and exe_ordre = exeordre
			and ges_code not in (select ges_code from maracuja.gestion_exercice where pco_num_185 is not null and exe_ordre = exeordre);
		 END IF;
	 ELSE
	 	 IF sacd = 'O' THEN
		 	SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvloprec
			WHERE PCO_NUM LIKE compte AND GES_CODE = comp and exe_ordre = exeordre;
         ELSIF  sacd = 'G' THEN
         	SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvloprec
			WHERE PCO_NUM LIKE compte AND exe_ordre = exeordre;
         ELSE
	   	 	SELECT NVL(SUM(montant_net),0) INTO nombre FROM v_dvloprec
			WHERE PCO_NUM LIKE compte and exe_ordre = exeordre
			and ges_code not in (select ges_code from maracuja.gestion_exercice where pco_num_185 is not null and exe_ordre = exeordre);
		 END IF;
	 END IF;

--dbms_output.put_line(nombre);

ELSE
	-- A Refaire ---
	nombre := 0;
END IF;

IF  nombre IS NULL
   THEN nombre := 0;
END IF;

RETURN nombre;
END ;
/




CREATE OR REPLACE FUNCTION COMPTEFI.Resultat_Compte (exeordre number, compte VARCHAR2, comp VARCHAR2, sacd CHAR)
RETURN NUMBER

IS
 nombre NUMBER(12,2);

BEGIN

IF exeordre >= 2005 THEN
   	 CASE
	 WHEN (SUBSTR(compte,1,1) = '6') OR (SUBSTR(compte,1,3) = '186') THEN
	 	 IF sacd = 'O' THEN
	   	 	SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO nombre
			FROM MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
			WHERE PCO_NUM LIKE compte AND GES_CODE = comp and exe_ordre =exeordre;
         ELSIF sacd = 'G' THEN
	   	 	SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO nombre
			FROM MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
			WHERE PCO_NUM LIKE compte and exe_ordre =exeordre;         
         ELSE
	   	 	SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO nombre
			FROM MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
			WHERE PCO_NUM LIKE compte AND ECR_SACD = 'N' and exe_ordre = exeordre;
		 END IF;
	 WHEN (SUBSTR(compte,1,1) = '7') OR (SUBSTR(compte,1,3) = '187') THEN
	 	 IF sacd = 'O' THEN
		 	SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO nombre
			FROM MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
			WHERE PCO_NUM LIKE compte AND GES_CODE = comp and exe_ordre = exeordre;
         ELSIF sacd = 'G' THEN
		 	SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO nombre
			FROM MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
			WHERE PCO_NUM LIKE compte and exe_ordre = exeordre;            
         ELSE
	   	 	SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO nombre
			FROM MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE
			WHERE PCO_NUM LIKE compte AND ECR_SACD = 'N' and exe_ordre = exeordre;
		 END IF;
	 END CASE;
	-- A Refaire ---
--	nombre := 0;
END IF;

IF  nombre IS NULL
   THEN nombre := 0;
END IF;

RETURN nombre;
END ;
/




CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte (exeordre number, compte VARCHAR2, sens_compte CHAR, comp VARCHAR2, sacd CHAR)
RETURN NUMBER

IS
 solde NUMBER(12,2);

BEGIN

IF exeordre >= 2005 THEN
	IF sens_compte = 'D'   THEN
		IF sacd = 'O' THEN
   	   		SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO solde
	   		FROM MARACUJA.cfi_ecritures
		    WHERE pco_num LIKE compte AND GES_CODE = comp and exe_ordre=exeordre AND ecr_sacd = 'O';
        ELSIF sacd = 'G' THEN
   	   		SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO solde
	   		FROM MARACUJA.cfi_ecritures
		    WHERE pco_num LIKE compte AND exe_ordre=exeordre;
		ELSE
			SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) INTO solde
	   		FROM MARACUJA.cfi_ecritures
		    WHERE pco_num LIKE compte AND ecr_sacd = 'N' and exe_ordre =exeordre;
		END IF;
	ELSE
		IF sacd = 'O' THEN
   	   		SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO solde
	   		FROM MARACUJA.cfi_ecritures
			WHERE pco_num LIKE compte AND GES_CODE = comp and exe_ordre = exeordre AND ecr_sacd = 'O';
        ELSIF sacd = 'G' THEN
   	   		SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO solde
	   		FROM MARACUJA.cfi_ecritures
			WHERE pco_num LIKE compte and exe_ordre = exeordre ;    
		ELSE
			SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) INTO solde
	   		FROM MARACUJA.cfi_ecritures
			WHERE pco_num LIKE compte AND ecr_sacd = 'N' and exe_ordre = exeordre;
		END IF;
	END IF;
ELSE
	solde := 0;
END IF;

IF solde < 0
    THEN solde := 0;
END IF;


RETURN solde;
END ;
/



CREATE OR REPLACE FUNCTION COMPTEFI.Solde_Compte_ext (exeordre number, pco_condition VARCHAR2, sens_compte CHAR, comp VARCHAR2, sacd CHAR, vue varchar2)
RETURN NUMBER

IS
solde NUMBER(12,2);
 LC$req varchar2(1000);
 lavue varchar2(50);
BEGIN

lavue := vue;
if lavue is null then
  lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
end if;

IF exeordre >= 2005 THEN
   IF sens_compte = 'D'   THEN
       IF sacd = 'O' THEN
          LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND GES_CODE = '''||comp||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
       ELSIF sacd = 'G' THEN
           LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' and exe_ordre ='||exeordre ;   
       ELSE
           LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
       END IF;
   ELSE
       IF sacd = 'O' THEN
          LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND GES_CODE = '''||comp||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
       ELSIF sacd = 'G' then
           LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' and exe_ordre ='||exeordre ;
       ELSE
           LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| pco_condition  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
       END IF;
   END IF;
ELSE
   --- A Refaire ---
   solde := 0;
END IF;

-- dbms_output.put_line( LC$req  );

EXECUTE IMMEDIATE LC$req INTO solde ;

IF solde < 0
   THEN solde := 0;
END IF;



RETURN solde;
END ;
/




