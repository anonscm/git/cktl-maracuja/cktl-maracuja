SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/3
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.8.5.0
-- Date de publication : 
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Mise à jour des comptes pour les valeurs inactives
-- Modifications pour la prise en charge des conventions RA
-- Corrections pour le calcul du bilan
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.8.5.0',  null, '');
commit;

create procedure grhum.inst_patch_maracuja_1850 is
begin
	INSERT INTO MARACUJA.TYPE_JOURNAL (TJO_LIBELLE, TJO_ORDRE) VALUES ('JOURNAL GRAND LIVRE DES VALEURS INACTIVES' ,18 );
	update maracuja.plan_comptable set pco_j_fin_exercice='N' where pco_num like '86%';
	update maracuja.plan_comptable set pco_j_exercice='N' where pco_num like '86%';
	update maracuja.plan_comptable set pco_j_be='N' where pco_num like '86%';
	update maracuja.plan_comptable_exer set pco_j_fin_exercice='N' where pco_num like '86%';
	update maracuja.plan_comptable_exer set pco_j_exercice='N' where pco_num like '86%';
	update maracuja.plan_comptable_exer set pco_j_be='N' where pco_num like '86%';
	
	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 566, 'VISBRO', 'Visa/Réimputations', null, 'Visa des brouillards en attente', 'N', 'N', 4 );
	
	INSERT INTO MARACUJA.TYPE_NUMEROTATION (
	   TNU_ENTITE, TNU_LIBELLE, TNU_ORDRE) 
	VALUES ( 'BORDEREAU','BORDEREAUX DE BROUILARDS' , 24);
   	
	INSERT INTO MARACUJA.TYPE_BORDEREAU (TBO_LIBELLE, TBO_ORDRE, TBO_SOUS_TYPE, TBO_TYPE, TBO_TYPE_CREATION, TNU_ORDRE) 
	VALUES ('DEMANDE DE PRISE EN CHARGE DE CONVENTION R.A.', 400, 'PECCONVRA', 'BTBROUILLARD', NULL, 24);   	

    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.8.5.0';           
end;
/

drop table maracuja.brouillard cascade constraints;
--drop table maracuja.brouillard_detail cascade constraints;
create table maracuja.brouillard (
    bro_id number not null,
    EXE_ORDRE              NUMBER                 NOT NULL,
    COM_ORDRE              NUMBER                 NOT NULL,
    BRO_NUMERO              NUMBER                 NOT NULL,
      BRO_ETAT               VARCHAR2(20)      NOT NULL,
      BRO_LIBELLE            VARCHAR2(200)     NOT NULL,
      BRO_POSTIT             VARCHAR2(200)        NULL,
      ORI_ORDRE              NUMBER                NULL,
      TJO_ORDRE              NUMBER               NULL,
      TOP_ORDRE              NUMBER               NULL,
      PERS_ID_CREATION       NUMBER               NOT NULL,
      DATE_CREATION            date               not null,
      BRO_MOTIF_REJET            varchar2(200)    null,
      BRO_DATE_VISA            DATE            NULL,
      pers_id_visa            number            null,
      BOR_ID                    number             null
) tablespace GFC;

alter table MARACUJA.brouillard add constraint PK_BROUILLARD primary key (BRO_ID);
alter table maracuja.brouillard add (constraint FK_BROUILLARD_COM_ORDRE FOREIGN KEY (COM_ORDRE)  REFERENCES MARACUJA.COMPTABILITE (COM_ORDRE));
alter table maracuja.brouillard add (constraint FK_BROUILLARD_EXE_ORDRE FOREIGN KEY (EXE_ORDRE)  REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE) );
alter table maracuja.brouillard add (constraint FK_BROUILLARD_ORI_ORDRE FOREIGN KEY (ORI_ORDRE)  REFERENCES MARACUJA.ORIGINE (ORI_ORDRE) );
alter table maracuja.brouillard add (constraint FK_BROUILLARD_TJO_ORDRE FOREIGN KEY (TJO_ORDRE)  REFERENCES MARACUJA.TYPE_JOURNAL (TJO_ORDRE) );
alter table maracuja.brouillard add (constraint FK_BROUILLARD_TOP_ORDRE FOREIGN KEY (TOP_ORDRE)  REFERENCES MARACUJA.TYPE_OPERATION (TOP_ORDRE) );
alter table maracuja.brouillard add (constraint FK_BROUILLARD_PERS_ID_CREATION FOREIGN KEY (PERS_ID_CREATION)  REFERENCES GRHUM.PERSONNE (PERS_ID) );
alter table maracuja.brouillard add (constraint FK_BROUILLARD_PERS_ID_VISA FOREIGN KEY (PERS_ID_VISA)  REFERENCES GRHUM.PERSONNE (PERS_ID) );
alter table maracuja.brouillard add (constraint FK_BROUILLARD_BOR_ID FOREIGN KEY (BOR_ID)  REFERENCES MARACUJA.BORDEREAU(BOR_ID) );
alter table maracuja.brouillard add (constraint CHK_BROUILLARD_BRO_ETAT CHECK (BRO_ETAT IN ('ATTENTE','VISE','REJETE')));

COMMENT ON TABLE maracuja.BROUILLARD IS 'Un brouillard d''ecriture';
COMMENT ON COLUMN maracuja.BROUILLARD.bro_id IS 'Cle';    
COMMENT ON COLUMN maracuja.BROUILLARD.COM_ORDRE IS 'Reference a la comptabilite';    
COMMENT ON COLUMN maracuja.BROUILLARD.BRO_ETAT IS 'Etat du brouillard (ATTENTE, VISE, REJETE)';    
COMMENT ON COLUMN maracuja.BROUILLARD.BRO_LIBELLE IS 'Libelle du brouillard';    
COMMENT ON COLUMN maracuja.BROUILLARD.BRO_POSTIT IS 'Info complementaire';    
COMMENT ON COLUMN maracuja.BROUILLARD.EXE_ORDRE IS 'Reference a l''exercice';    
COMMENT ON COLUMN maracuja.BROUILLARD.ORI_ORDRE IS 'Reference a l''origine';    
COMMENT ON COLUMN maracuja.BROUILLARD.TJO_ORDRE IS 'Reference au type de journal';    
COMMENT ON COLUMN maracuja.BROUILLARD.TOP_ORDRE IS 'Reference au type d''operation';    
COMMENT ON COLUMN maracuja.BROUILLARD.PERS_ID_CREATION IS 'Reference a la personne qui a genere le brouillard';    
COMMENT ON COLUMN maracuja.BROUILLARD.DATE_CREATION IS 'Date de creation du brouillard';    
COMMENT ON COLUMN maracuja.BROUILLARD.BRO_MOTIF_REJET IS 'Motif de l''eventuel rejet';    
COMMENT ON COLUMN maracuja.BROUILLARD.BRO_DATE_VISA IS 'Date du visa du brouillard';    
COMMENT ON COLUMN maracuja.BROUILLARD.pers_id_visa IS 'Reference a la personne qui effectue le visa du brouillard';    
COMMENT ON COLUMN maracuja.BROUILLARD.BOR_ID IS 'Reference au bordereau si le brouillard est rattache a un bordereau';    
COMMENT ON COLUMN maracuja.BROUILLARD.BRO_NUMERO IS 'Numero du brouillard';    


create table maracuja.brouillard_detail (
    BROD_ID             NUMBER             NOT NULL,
    BRO_ID                NUMBER             NOT NULL,
    BROD_INDEX          NUMBER          NOT NULL,
    GES_CODE            VARCHAR2(10)    NOT NULL,
    BROD_PCO_NUM        VARCHAR2(20)    NOT NULL,
      BROD_PCO_LIBELLE    VARCHAR2(200)   NULL, 
    BROD_DEBIT          NUMBER(12,2)   DEFAULT 0  NOT NULL,
      BROD_CREDIT         NUMBER(12,2)   DEFAULT 0  NOT NULL,
      BROD_SENS           VARCHAR2(1)     NOT NULL,
      BROD_LIBELLE        VARCHAR2(200)    NULL,
      BROD_POSTIT         VARCHAR2(200)    NULL,
      BROD_COMMENTAIRE    VARCHAR2(200)    NULL,
      ECD_ORDRE           NUMBER          NULL
) tablespace GFC;


alter table MARACUJA.brouillard_detail add constraint PK_BROUILLARD_DETAIL primary key (BROD_ID);
alter table maracuja.brouillard_detail add (constraint FK_BROD_BRO_ID FOREIGN KEY (BRO_ID)  REFERENCES MARACUJA.BROUILLARD (BRO_ID));
alter table maracuja.brouillard_detail add (constraint FK_BROD_GES_CODE FOREIGN KEY (GES_CODE)  REFERENCES MARACUJA.GESTION (GES_CODE));
alter table maracuja.brouillard_detail add (constraint FK_BROD_ECD_ORDRE FOREIGN KEY (ECD_ORDRE)  REFERENCES MARACUJA.ECRITURE_DETAIL (ECD_ORDRE));
alter table maracuja.brouillard_detail add (constraint CHK_BROD_SENS CHECK (BROD_SENS IN ('D','C')));

COMMENT ON TABLE maracuja.brouillard_detail IS 'Une ligne de brouillard d''ecriture';
COMMENT ON COLUMN maracuja.brouillard_detail.brod_id IS 'Cle';    
COMMENT ON COLUMN maracuja.brouillard_detail.bro_id IS 'Reference au brouillard';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_DEBIT IS 'Montant du debit';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_CREDIT IS 'Montant du credit';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_INDEX IS 'Index de la ligne';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_SENS IS 'Sens (D/C) de la ligne';    
COMMENT ON COLUMN maracuja.brouillard_detail.GES_CODE IS 'Reference au code gestion';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_PCO_NUM IS 'Numero du compte a mouvementer (n''existe pas obligatoirement dans le plan comptable)';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_PCO_LIBELLE IS 'Libelle du compte (s''il doit etre créé)';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_LIBELLE IS 'Libelle de la ligne';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_POSTIT IS 'Info complementaire';    
COMMENT ON COLUMN maracuja.brouillard_detail.BROD_COMMENTAIRE IS 'Info complementaire';    
COMMENT ON COLUMN maracuja.brouillard_detail.ECD_ORDRE IS 'Reference a l''ecriture_detail creee a partir de cette ligne de brouillard';    

CREATE SEQUENCE MARACUJA.BROUILLARD_DETAIL_SEQ START WITH 1 MINVALUE 1 NOCYCLE NOCACHE NOORDER;



CREATE OR REPLACE PACKAGE MARACUJA."NUMEROTATIONOBJECT" IS


-- PUBLIC -
PROCEDURE numeroter_ecriture (ecrordre INTEGER);
PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER);
PROCEDURE private_numeroter_ecriture (ecrordre INTEGER);

PROCEDURE numeroter_brouillard (broid INTEGER);

PROCEDURE numeroter_bordereauRejet (brjordre INTEGER);
PROCEDURE numeroter_bordereau (borid INTEGER);

PROCEDURE numeroter_bordereaucheques (borid INTEGER);

PROCEDURE numeroter_emargement (emaordre INTEGER);

PROCEDURE numeroter_ordre_paiement (odpordre INTEGER);

PROCEDURE numeroter_paiement (paiordre INTEGER);

PROCEDURE numeroter_retenue (retordre INTEGER);

PROCEDURE numeroter_reimputation (reiordre INTEGER);

PROCEDURE numeroter_recouvrement (recoordre INTEGER);


-- PRIVATE --
PROCEDURE numeroter_mandat_rejete (manid INTEGER);

PROCEDURE numeroter_titre_rejete (titid INTEGER);

PROCEDURE numeroter_mandat (borid INTEGER);

PROCEDURE numeroter_titre (borid INTEGER);

PROCEDURE numeroter_orv (borid INTEGER);

PROCEDURE numeroter_aor (borid INTEGER);

FUNCTION numeroter(
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
) RETURN INTEGER;

END;
/


GRANT EXECUTE ON MARACUJA.NUMEROTATIONOBJECT TO JEFY_PAYE;


CREATE OR REPLACE PACKAGE BODY MARACUJA."NUMEROTATIONOBJECT" IS


PROCEDURE private_numeroter_ecriture (ecrordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ECRITURE INEXISTANTE , CLE '||ecrordre);
END IF;

SELECT ecr_numero INTO cpt  FROM ECRITURE WHERE ecr_ordre = ecrordre;

IF cpt = 0 THEN
-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM ECRITURE
WHERE ecr_ordre = ecrordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ECRITURE DU JOURNAL';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ECRITURE SET ecr_numero = lenumero
WHERE ecr_ordre = ecrordre;
END IF;
END;

PROCEDURE numeroter_ecriture (ecrordre INTEGER)
IS
monecriture ECRITURE%ROWTYPE;

BEGIN
Numerotationobject.private_numeroter_ecriture(ecrordre);
SELECT * INTO monecriture  FROM ECRITURE WHERE ecr_ordre = ecrordre;
Numerotationobject.Numeroter_Ecriture_Verif (monecriture.com_ordre,monecriture.exe_ordre);
END;

PROCEDURE Numeroter_Ecriture_Verif (comordre INTEGER ,exeordre INTEGER)
IS
ecrordre INTEGER;
lenumero INTEGER;

GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

CURSOR lesEcrituresNonNumerotees IS
SELECT ecr_ordre
FROM ECRITURE
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ecr_numero =0
AND bro_ordre IS NULL
ORDER BY ecr_ordre;

BEGIN

OPEN lesEcrituresNonNumerotees;
LOOP
FETCH lesEcrituresNonNumerotees INTO ecrordre;
EXIT WHEN lesEcrituresNonNumerotees%NOTFOUND;
Numerotationobject.private_numeroter_ecriture(ecrordre);
END LOOP;
CLOSE lesEcrituresNonNumerotees;

END;

PROCEDURE numeroter_brouillard (broid INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM BROUILLARD WHERE bro_id = broid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BROUILLARD INEXISTANTE, CLE '||broid);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO comordre,exeordre
FROM BROUILLARD
WHERE bro_id = broid;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='BROUILLARD';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE BROUILLARD SET bro_numero = lenumero
WHERE bro_id = broid;


END;


PROCEDURE numeroter_bordereauRejet (brjordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;

mandat_rejet MANDAT%ROWTYPE;
titre_rejet TITRE%ROWTYPE;

CURSOR mdt_rejet IS
SELECT *
FROM MANDAT WHERE brj_ordre = brjordre;

CURSOR tit_rejet IS
SELECT *
FROM TITRE WHERE brj_ordre = brjordre;

BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU_REJET WHERE brj_ordre = brjordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU REJET INEXISTANT , CLE '||brjordre);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU_REJET b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.brj_ordre = brjordre
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



IF (tbotype ='BTMNA') THEN
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. DEPENSE NON ADMIS';
ELSE
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORD. RECETTE NON ADMIS';
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;


-- affectation du numero -
UPDATE BORDEREAU_REJET SET brj_num = lenumero
WHERE brj_ordre = brjordre;

-- numeroter les titres rejetes -
IF (tbotype ='BTMNA') THEN
OPEN mdt_rejet;
LOOP
FETCH  mdt_rejet INTO mandat_rejet;
EXIT WHEN mdt_rejet%NOTFOUND;
Numerotationobject.numeroter_mandat_rejete (mandat_rejet.man_id);
END LOOP;
CLOSE mdt_rejet;

END IF;

-- numeroter les mandats rejetes -
IF (tbotype ='BTTNA') THEN

OPEN tit_rejet;
LOOP
FETCH  tit_rejet INTO titre_rejet;
EXIT WHEN  tit_rejet%NOTFOUND;
Numerotationobject.numeroter_titre_rejete (titre_rejet.tit_id);
END LOOP;
CLOSE tit_rejet;

END IF;

END;


PROCEDURE numeroter_bordereaucheques (borid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
parvalue PARAMETRE.par_value%TYPE;



BEGIN

SELECT COUNT(*) INTO cpt FROM BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU DE CHEQUES INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;


-- TNU_ORDRE A DEFINIR
 SELECT tnu_ordre INTO tnuordre
 FROM TYPE_NUMEROTATION
 WHERE tnu_libelle ='BORDEREAU CHEQUE';



-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

--IF parvalue = 'COMPOSANTE' THEN
--lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
--ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
--END IF;


-- affectation du numero -
UPDATE BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;



END;


PROCEDURE numeroter_emargement (emaordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM EMARGEMENT WHERE ema_ordre = emaordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' EMARGEMENT INEXISTANT , CLE '||emaordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM EMARGEMENT
WHERE ema_ordre = emaordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RAPPROCHEMENT / LETTRAGE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -

UPDATE EMARGEMENT  SET ema_numero = lenumero
WHERE ema_ordre = emaordre;


END;


PROCEDURE numeroter_ordre_paiement (odpordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM ORDRE_DE_PAIEMENT WHERE odp_ordre = odpordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' ORDRE DE PAIEMENT INEXISTANT , CLE '||odpordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
INTO COMORDRE,EXEORDRE
FROM ORDRE_DE_PAIEMENT
WHERE odp_ordre = odpordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORDRE DE PAIEMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE ORDRE_DE_PAIEMENT SET odp_numero = lenumero
WHERE odp_ordre = odpordre;

END;


PROCEDURE numeroter_paiement (paiordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM PAIEMENT WHERE pai_ordre = paiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' PAIEMENT INEXISTANT , CLE '||paiordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM PAIEMENT
WHERE pai_ordre = paiordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='VIREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE PAIEMENT SET pai_numero = lenumero
WHERE pai_ordre = paiordre;

END;


PROCEDURE numeroter_recouvrement (recoordre INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RECOUVREMENT WHERE reco_ordre = recoordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RECOUVREMENT INEXISTANT , CLE '||RECOordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre,exe_ordre
--select 1,1
INTO COMORDRE,EXEORDRE
FROM RECOUVREMENT
WHERE RECO_ordre = recoordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RECOUVREMENT';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

-- affectation du numero -
UPDATE RECOUVREMENT SET reco_numero = lenumero
WHERE reco_ordre = recoordre;

END;



PROCEDURE numeroter_retenue (retordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;

BEGIN

SELECT COUNT(*) INTO cpt FROM RETENUE WHERE ret_id = retordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' RETENUE INEXISTANTE , CLE '||retordre);
END IF;

-- recup des infos de l objet -
SELECT com_ordre, exe_ordre
INTO COMORDRE,EXEORDRE
FROM RETENUE
WHERE ret_id = retordre;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='RETENUE';

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE RETENUE SET ret_numero = lenumero
WHERE RET_ID=retordre;

END;

PROCEDURE numeroter_reimputation (reiordre INTEGER)
IS
cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
TNULIBELLE TYPE_NUMEROTATION.TNU_libelle%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM REIMPUTATION WHERE rei_ordre = reiordre;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' REIMPUTATION INEXISTANTE , CLE '||reiordre);
END IF;

SELECT COUNT(*)
--select 1,1
INTO  cpt
FROM REIMPUTATION r,MANDAT m,GESTION g
WHERE rei_ordre = reiordre
AND m.man_id = r.man_id
AND m.ges_code = g.ges_code;

IF cpt != 0 THEN

-- recup des infos de l objet -
--select com_ordre,exe_ordre
 SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION MANDAT'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,MANDAT m,GESTION g
 WHERE rei_ordre = reiordre
 AND m.man_id = r.man_id
 AND m.ges_code = g.ges_code;
ELSE
--select com_ordre,exe_ordre
SELECT DISTINCT g.com_ordre,r.exe_ordre,'REIMPUTATION TITRE'
--select 1,1
 INTO  COMORDRE,EXEORDRE,TNULIBELLE
 FROM REIMPUTATION r,TITRE t,GESTION g
 WHERE rei_ordre = reiordre
 AND t.tit_id = r.tit_id
 AND t.ges_code = g.ges_code;
END IF;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle =TNULIBELLE;

-- recup du numero -
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);

--affectation du numero -
UPDATE REIMPUTATION SET rei_numero = lenumero
WHERE rei_ordre = reiordre;

END;




PROCEDURE numeroter_mandat_rejete (manid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM MANDAT WHERE man_id=manid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' MANDAT REJETE INEXISTANT , CLE '||manid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,m.exe_ordre,m.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM MANDAT m ,GESTION g
WHERE man_id = manid
AND m.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;



-- affectation du numero -
UPDATE MANDAT  SET man_numero_rejet = lenumero
WHERE man_id=manid;

END;



PROCEDURE numeroter_titre_rejete (titid INTEGER)
IS

cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
BEGIN

SELECT COUNT(*) INTO cpt FROM TITRE WHERE tit_id=titid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' TITRE REJETE INEXISTANT , CLE '||titid);
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,t.exe_ordre,t.ges_code
--select 1,1
INTO COMORDRE,EXEORDRE,GESCODE
FROM TITRE t ,GESTION g
WHERE t.tit_id = titid
AND t.ges_code = g.ges_code;

SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE REJET';

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU' AND exe_ordre=EXEORDRE;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

-- affectation du numero -
UPDATE TITRE  SET tit_numero_rejet = lenumero
WHERE tit_id=titid;

END;



FUNCTION numeroter (
COMORDRE COMPTABILITE.com_ordre%TYPE,
EXEORDRE EXERCICE.exe_ordre%TYPE,
GESCODE GESTION.ges_ordre%TYPE,
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE
)
RETURN INTEGER
IS
cpt INTEGER;
numordre INTEGER;
BEGIN

LOCK TABLE NUMEROTATION IN EXCLUSIVE MODE;

--COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE

IF gescode IS NULL THEN
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code IS NULL
AND tnu_ordre = tnuordre;
ELSE
SELECT COUNT(*) INTO cpt
FROM NUMEROTATION
WHERE com_ordre = comordre
AND exe_ordre = exeordre
AND ges_code = gescode
AND tnu_ordre = tnuordre;
END IF;

IF cpt  = 0 THEN
--raise_application_error (-20002,'trace:'||COMORDRE||''||EXEORDRE||''||GESCODE||''||numordre||''||TNUORDRE);
 SELECT numerotation_seq.NEXTVAL INTO numordre FROM dual;


 INSERT INTO NUMEROTATION
 ( COM_ORDRE, EXE_ORDRE, GES_CODE, NUM_NUMERO, NUM_ORDRE, TNU_ORDRE)
 VALUES (COMORDRE, EXEORDRE, GESCODE, 1,numordre, TNUORDRE);
 RETURN 1;
ELSE
 IF gescode IS NOT NULL THEN
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code = gescode
  AND tnu_ordre = tnuordre;
 ELSE
  SELECT num_numero+1 INTO cpt
  FROM NUMEROTATION
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;

  UPDATE NUMEROTATION SET num_numero = cpt
  WHERE com_ordre = comordre
  AND exe_ordre = exeordre
  AND ges_code IS NULL
  AND tnu_ordre = tnuordre;
 END IF;
 RETURN cpt;
END IF;
END ;




PROCEDURE numeroter_bordereau (borid INTEGER) IS


cpt INTEGER;
lenumero INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
tbotype TYPE_BORDEREAU.tbo_type%TYPE;
tboordre TYPE_BORDEREAU.tbo_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;

lebordereau BORDEREAU%ROWTYPE;


BEGIN

SELECT COUNT(*) INTO cpt FROM maracuja.BORDEREAU WHERE bor_id = borid;
IF cpt = 0 THEN
 RAISE_APPLICATION_ERROR (-20001,' BORDEREAU  INEXISTANT , CLE '||borid);
END IF;

-- recup des infos de l objet -
SELECT t.tbo_type,t.tbo_ordre,g.com_ordre,b.exe_ordre,b.ges_code
INTO tbotype,tboordre,comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;



SELECT  COUNT(*) INTO cpt
FROM maracuja.TYPE_BORDEREAU tb , maracuja.TYPE_NUMEROTATION tn
WHERE tn.tnu_ordre = tb.tnu_ordre
AND tb.tbo_ordre = tboordre;

IF cpt = 1 THEN
 SELECT tn.tnu_ordre INTO tnuordre
 FROM maracuja.TYPE_BORDEREAU tb , TYPE_NUMEROTATION tn
 WHERE tn.tnu_ordre = tb.tnu_ordre
 AND tb.tbo_ordre = tboordre;
ELSE
 RAISE_APPLICATION_ERROR (-20001,'IMPOSSIBLE DE DETERMINER LE TYPE DE NUMEROTATION'||borid);
END IF;


-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;

IF parvalue = 'COMPOSANTE' THEN
lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
ELSE
lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
END IF;

IF lenumero IS NULL THEN
 RAISE_APPLICATION_ERROR (-20001,'PROBLEME DE NUMEROTATION'||borid);
END IF;


-- affectation du numero -
UPDATE maracuja.BORDEREAU SET bor_num = lenumero
WHERE bor_id = borid;

END;


PROCEDURE numeroter_mandat (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid ORDER BY pco_num;

BEGIN

-- si c un bordereau orv
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id=borid;

select exe_ordre into exeordre from bordereau  WHERE bor_id=borid;
if (exeordre>2010) then
  IF (tboordre in (8,18,21) ) THEN
     numeroter_orv(borid);
     RETURN;
  END IF;
else
 IF (tboordre in (8,18) ) THEN
     numeroter_orv(borid);
     RETURN;
  END IF;
end if;



-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;



-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='MANDAT';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_titre (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
tboordre BORDEREAU.tbo_ordre%TYPE;

CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid ORDER BY pco_num, tit_id;


BEGIN


-- si c un bordereau aor
SELECT tbo_ordre INTO  tboordre FROM BORDEREAU WHERE bor_id=borid;
IF (tboordre=9) THEN
   numeroter_aor(borid);
   RETURN;
END IF;

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='TITRE';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;

 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;



PROCEDURE numeroter_orv (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
manid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT man_id FROM MANDAT WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='ORV';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO manid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.MANDAT SET man_numero = lenumero
 WHERE bor_id = borid AND man_id = manid;
END LOOP;
CLOSE a_numeroter;
END ;

PROCEDURE numeroter_aor (borid INTEGER)IS
cpt INTEGER;
lenumero INTEGER;
titid INTEGER;
COMORDRE COMPTABILITE.com_ordre%TYPE;
EXEORDRE EXERCICE.exe_ordre%TYPE;
GESCODE GESTION.ges_ordre%TYPE;
TNUORDRE TYPE_NUMEROTATION.TNU_ordre%TYPE;
parvalue PARAMETRE.par_value%TYPE;
CURSOR a_numeroter IS
SELECT tit_id FROM TITRE WHERE bor_id = borid;

BEGIN

-- recup des infos de l objet -
SELECT g.com_ordre,b.exe_ordre,b.ges_code
INTO comordre,exeordre,gescode
FROM BORDEREAU b,GESTION g,COMPTABILITE c,TYPE_BORDEREAU t
WHERE b.bor_id = borid
AND g.ges_code = b.ges_code
AND t.tbo_ordre = b.tbo_ordre;

-- recup du numero -
SELECT par_value  INTO parvalue
FROM PARAMETRE
WHERE par_key ='NUMERO BORDEREAU'AND exe_ordre=exeordre;


-- type de numerotation  = MANDAT
SELECT tnu_ordre INTO tnuordre
FROM TYPE_NUMEROTATION
WHERE tnu_libelle ='AOR';
OPEN a_numeroter;
LOOP
FETCH a_numeroter INTO titid;
EXIT WHEN a_numeroter%NOTFOUND;
 IF parvalue = 'COMPOSANTE' THEN
  lenumero := numeroter (COMORDRE,EXEORDRE,GESCODE,TNUORDRE);
 ELSE
  lenumero := numeroter (COMORDRE,EXEORDRE,NULL,TNUORDRE);
 END IF;
 UPDATE maracuja.TITRE SET tit_numero =  lenumero
 WHERE bor_id = borid AND tit_id = titid;
END LOOP;
CLOSE a_numeroter;

END;

END NUMEROTATIONOBJECT;
/


GRANT EXECUTE ON MARACUJA.NUMEROTATIONOBJECT TO JEFY_PAYE;


CREATE OR REPLACE PACKAGE MARACUJA.API_BILAN AS

  --FUNCTION add(Param1 IN NUMBER) RETURN NUMBER;
--  PROCEDURE addPlancoBilanPoste(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--        pbpSigne planco_bilan_poste.pbp_signe%type,
--        pbpSens planco_bilan_poste.pbp_sens%type,
--        pconum plan_comptable_exer.pco_num%type,
--        pbpSubDiv planco_bilan_poste.pbp_subdiv%type,
--        pbpExceptSubdiv planco_bilan_poste.PBP_except_subdiv%type,
--        pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

--  procedure deletePlancoBilanPostes(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--         );
--

--  PROCEDURE setPlancoBilanFormule(exeordre NUMBER,
--         bpStrId bilan_poste.bp_str_id%type,
--         formule varchar2,
--         pbpIdColonne planco_bilan_poste.pbp_id_colonne%type
--        );

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
        );

        -- Calcule le poste pour l'exercice en fonction de la formule trouvÃ©e
--  FUNCTION calcPoste(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type,
--              gesCode gestion.ges_code%type,
--              isSacd char
--      )
--      RETURN NUMBER;
    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, gescode VARCHAR2, sacd CHAR, vue VARCHAR2) RETURN VARCHAR2;

  FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              gesCode gestion.ges_code%TYPE,
              isSacd CHAR,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2
      )
      RETURN NUMBER;

--    FUNCTION prv_getCritSqlForExcept(
--              compte VARCHAR2,
--            pbpSubdiv PLANCO_BILAN_POSTE.PBP_SUBDIV%type
--      ) return varchar2;
    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 ;

--  FUNCTION getFormule(
--            exeOrdre bilan_poste.exe_ordre%type,
--              bpStrId bilan_poste.bp_str_id%type,
--              bpbIdColonne PLANCO_BILAN_POSTE.PBP_ID_COLONNE%type
--      )
--      RETURN varchar2;

   PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE, exeOrdre bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, sacd CHAR);
   PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR);
   PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR);
   PROCEDURE checkFormule(formule VARCHAR2);

   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER);
   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE);

END API_BILAN;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.API_BILAN AS

  PROCEDURE setBilanPosteFormule(
         btStrId bilan_type.bt_str_id%TYPE,
         exeordre NUMBER,
         bpStrId bilan_poste.bp_str_id%TYPE,
         formuleMontant VARCHAR2,
         formuleAmortissement  VARCHAR2
  ) IS
    btId bilan_type.bt_id%TYPE;
  BEGIN
    SELECT bt_id INTO btId FROM bilan_type WHERE bt_str_id=btstrid;
    UPDATE bilan_poste SET BP_FORMULE_MONTANT=formuleMontant, BP_FORMULE_AMORTISSEMENT=formuleAmortissement WHERE bt_id=btId AND exe_ordre=exeOrdre AND bp_str_id=bpStrId;
  END setBilanPosteFormule;


    FUNCTION getSoldeReq(formule VARCHAR2, exeordre NUMBER, gescode VARCHAR2, sacd CHAR, vue VARCHAR2) RETURN VARCHAR2 IS
    signe VARCHAR2(1);
    signeNum INTEGER;
    sens VARCHAR2(2);
    compteext VARCHAR2(2000);
    critStrPlus VARCHAR2(2000);
    res VARCHAR2(4000);
    LC$req VARCHAR2(4000);
    lavue VARCHAR2(50);
   BEGIN
        res := '';
        IF (formule IS NOT NULL AND LENGTH(formule)>0) THEN
             dbms_output.put_line('traitement de ='|| formule);
            signe := SUBSTR(formule,1,1); -- +
            signeNum := 1;
            IF (signe='-') THEN
                signeNum := -1;
            END IF;
            sens :=  SUBSTR(formule,2,2); -- SD
            IF (sens IS NULL OR sens NOT IN ('SD','SC')) THEN
                 RAISE_APPLICATION_ERROR (-20001, formule ||' : ' ||sens||' : le sens doit etre SD ou SC ');
            END IF;
            compteext := SUBSTR(formule,4); -- 4012 ou (4012-40125-40126)
            --subdivStr := '';
            IF (LENGTH(compteext)=0) THEN
                RAISE_APPLICATION_ERROR (-20001, 'compte non recupere dans la formule '|| formule);
            END IF;
            critStrPlus := prv_getCritSqlForSingleFormule(compteext);
           -- sInstructionReelle := sInstructionReelle || ' ' || signe || sens || '(' || critStrPlus ||')';

            dbms_output.put_line('exeordre='|| exeordre);
            dbms_output.put_line('critStrPlus='||critStrPlus);
            dbms_output.put_line('sens='||sens);
            dbms_output.put_line('sacd='||sacd);

            lavue := vue;
            IF lavue IS NULL THEN
              lavue := 'MARACUJA.CFI_TOTAUX_6_7_AVANT_SOLDE';
            END IF;

           IF sens = 'SD'   THEN
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(debit),0) - NVL(SUM(credit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           ELSE
               IF sacd = 'O' THEN
                  LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0)  FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND GES_CODE = '''||gescode||''' and exe_ordre='||exeordre||' AND ecr_sacd = ''O''';
               ELSIF sacd = 'G' THEN
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' and exe_ordre ='||exeordre ;
               ELSE
                   LC$req := 'SELECT NVL(SUM(credit),0) - NVL(SUM(debit),0) FROM  '|| lavue ||' WHERE '|| critStrPlus  || ' AND ecr_sacd = ''N'' and exe_ordre ='||exeordre ;
               END IF;
           END IF;
           dbms_output.put_line('req = '|| LC$req  );
            res := LC$req;
        END IF;
        RETURN res;
   END getSoldeReq;

    -- Calcule le montant d'apres une formule de type SDxxx + SC YYY etc.
    FUNCTION getSoldeMontant(
              exeOrdre bilan_poste.exe_ordre%TYPE,
              formule VARCHAR2,
              gesCode gestion.ges_code%TYPE,
              isSacd CHAR,
              faireCalcul SMALLINT,
              sqlReq OUT VARCHAR2
      )
      RETURN NUMBER IS

    str VARCHAR2(4000);
    compteext VARCHAR2(250);
    --subdivStr planco_bilan_poste.PBP_EXCEPT_SUBDIV%type;
    pos INTEGER;
    nb INTEGER;
    i INTEGER;
    j INTEGER;
    nextS INTEGER;
    signe VARCHAR2(10);
    sens VARCHAR2(10);
    pconum plan_comptable_exer.pco_num%TYPE;
    subdiv plan_comptable_exer.pco_num%TYPE;
    v_array ZtableForSplit;
    --v_arrayExt ZtableForSplit;
    --varray_comptesPlus ZtableForSplit;
    sinstruction VARCHAR2(255);
    --critStrPlus varchar2(2000);
    resTmp NUMBER;
    signeNum NUMBER;
    sInstructionReelle VARCHAR2(4000);
    req VARCHAR2(4000);
    solde NUMBER;
  BEGIN
    /* une formule est composÃ©es de suites de :
            Signe Sens Compte (par exemple - SD 201 ou + SD472)
    */
dbms_output.put_line('');
dbms_output.put_line('Formule brute = ' || formule);
    resTmp := 0;
    sqlReq := '';
    sInstructionReelle := '';
    -- analyser la formule
    -- supprimer tous les espaces
    str := REPLACE(formule, ' ', '');

    IF (LENGTH(str)=0) THEN
     RAISE_APPLICATION_ERROR (-20001, 'formule vide' );
    END IF;

    str := UPPER(str);

    IF (SUBSTR(str,1,1) = 'S') THEN
        str := '+'||str;
    END IF;

    str := REPLACE(str, '+S', '|+S');
    str := REPLACE(str, '-S', '|-S');
    str := SUBSTR(str,2);
dbms_output.put_line('Formule analysee = '|| formule);
    v_array := str2tbl(str, '|');
    FOR i IN 1 .. v_array.COUNT LOOP
        sinstruction := v_array(i);
        sinstruction := trim(sinstruction);
        signe := SUBSTR(sinstruction,1,1); -- +
        signeNum := 1;
        IF (signe='-') THEN
            signeNum := -1;
        END IF;
        dbms_output.put_line('sinstruction = '|| sinstruction);
        req := getSoldeReq(sinstruction, exeOrdre, gescode, issacd,'MARACUJA.cfi_ecritures');
        dbms_output.put_line('REQ = '|| req);
        IF ( NVL(faireCalcul, 1)=1) THEN
            EXECUTE IMMEDIATE req INTO solde ;
        END IF;

        IF (solde IS NULL) THEN
            solde := 0;
        END IF;
        IF solde < 0
           THEN solde := 0;
        END IF;
        resTmp := resTmp + signenum*solde;
        sqlReq := sqlReq || ' ' || signe || req;
    END LOOP;
    --dbms_output.put_line('Formule = '|| formule);
    --dbms_output.put_line('Instruction reelle = '|| req);
    RETURN resTmp;
    END getSoldeMontant;





    FUNCTION prv_getCritSqlForSingleFormule(formule VARCHAR2) RETURN VARCHAR2 IS
            varray_comptesPlus ZtableForSplit;
            varray_comptesMoins ZtableForSplit;
            pconum VARCHAR2(2000);
            critStrMoins VARCHAR2(2000);
            critStrPlus VARCHAR2(2000);
            str VARCHAR2(2000);
            pconumTmp VARCHAR2(2000);
            subdiv VARCHAR2(1000);
    BEGIN
            str := formule;
            -- traite les formules du genre (53 + 54 -541 -542 + 55)
            -- on nettoie les Ã©ventuelles parenthese de debut / fin
            IF (SUBSTR(str,1,1) = '(') THEN
               --cas complexe (avec parentheses : ex SD(53 + 54 -541 -542 + 55)
               IF (SUBSTR(str,LENGTH(str),1) <> ')' ) THEN
                RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(str,LENGTH(str),1)) || ': '|| formule);
               END IF;
               str := SUBSTR(str,2,LENGTH(str)-2);
             END IF;

            -- separer les + (pour traiter (53 | 54 -541 -542 | 55) )
            varray_comptesPlus := str2tbl(str, '+');
            critStrPLus := '';
            FOR z IN 1 ..  varray_comptesPlus.COUNT LOOP
                pconum :=  varray_comptesPlus(z);
                -- pconum peut etre de la forme 54 ou bien (54-541-542) ou bien 54-541-542
                -- si parentheses debut/fin, on les vire
                IF (SUBSTR(pconum,1,1)='(' ) THEN
                    IF (SUBSTR(pconum,LENGTH(pconum),1) <> ')' ) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese de fin non trouvee :'|| (SUBSTR(pconum,LENGTH(pconum),1)) || ': '|| formule);
                    END IF;
                    pconum := SUBSTR(pconum,2,LENGTH(pconum)-2);
                    IF (INSTR(pconum,'(' )>0) THEN
                        RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non générée ici :'|| pconum || ': '|| formule);
                    END IF;
                END IF;
                IF (INSTR(pconum,')' )>0) THEN
                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, parenthese non gérée ici :'|| pconum || ': '|| formule);
                END IF;
                -- on separe les elements exclus
                critStrMoins := '';
                varray_comptesMoins :=  str2tbl(pconum, '-');
                FOR j IN 1 .. varray_comptesMoins.COUNT LOOP
                   IF (j=1) THEN
                            pconumTmp := varray_comptesMoins(j);
                            critStrMoins := 'pco_num like '''|| pconumTmp || '%'' ';
                            dbms_output.put_line(pconumTmp);
                   ELSE
                            subdiv := varray_comptesMoins(j);
                            dbms_output.put_line(subdiv);
                            IF (LENGTH(subdiv)>0) THEN
                                IF (subdiv NOT LIKE pconumTmp||'%') THEN
                                    RAISE_APPLICATION_ERROR (-20001, 'erreur dans la formule, seules les subdivisions du compte initial sont autorisÃ©es : '|| pconum);
                                END IF;

                                critStrMoins := critStrMoins || ' and pco_num not like '''|| subdiv || '%'' ';
                            END IF;
                   END IF;
                END LOOP;
                IF (LENGTH(critStrMoins)>0) THEN
                    critStrMoins := '(' || critStrMoins ||')';
                    IF (LENGTH(critStrPlus)>0) THEN
                        critStrPlus := critStrPlus || ' or ';
                    END IF;
                    critStrPlus := critStrPlus || critStrMoins;
                END IF;

            END LOOP;

            RETURN critStrPlus;

    END;


 PROCEDURE prepare_bilan_actif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR) AS
    bp_id0 bilan_poste.bp_id%TYPE;
    bp_id1 bilan_poste.bp_id%TYPE;
    bp_id2 bilan_poste.bp_id%TYPE;
    rec_bp1 bilan_poste%ROWTYPE;
    rec_bp2 bilan_poste%ROWTYPE;
    rec_bp3 bilan_poste%ROWTYPE;
    bilan_groupe1 VARCHAR2(1000);
    bilan_groupe2 VARCHAR2(1000);
    bilan_libelle VARCHAR2(1000);
    bilan_montant NUMBER;
     bilan_amort NUMBER;
    bpStrId bilan_poste.bp_str_id%TYPE;
    formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
    formuleAmortissement bilan_poste.BP_FORMULE_AMORTISSEMENT%TYPE;
    CURSOR carbre IS
            SELECT *
            FROM bilan_poste
            WHERE exe_ordre=exeOrdre
            CONNECT BY PRIOR BP_ID = bp_id_pere
            START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bp_str_id=bpStrId);

    CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
    CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
    CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
    sqlreq VARCHAR2(4000);
   flag INTEGER;

   BEGIN
        bpStrId := 'actif';

        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (gesCode IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' gesCode obligatoire');
        END IF;
        IF (isSacd IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' isSacd obligatoire');
        END IF;
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  exercice= '|| exeordre || ', type='||btId);
        END IF;

        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;

--*************** CREATION TABLE ACTIF *********************************
        IF issacd = 'O' THEN
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSIF issacd = 'G' THEN
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        ELSE
            DELETE FROM comptefi.BILAN_ACTIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;
                    dbms_output.put_line('rec_bp1.bp_libelle = '|| rec_bp1.bp_id || ' / '|| rec_bp1.bp_libelle );
                    dbms_output.put_line('rec_bp2.bp_libelle = '|| rec_bp2.bp_id || ' / '||rec_bp2.bp_libelle );
                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                            formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq);
                             
                            bilan_amort := 0;
                            IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq);
                            END IF;
                            INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant+bilan_amort, 0, gescode,exeordre);
                            end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                                    formuleAmortissement := rec_bp3.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                                      gesCode,
                                                      isSacd,
                                                      1,
                                                      sqlReq);
                                    bilan_amort := 0;
                                    IF (LENGTH(trim(formuleAmortissement)) >0  )   THEN
                                        bilan_amort := getSoldeMontant(exeordre, formuleAmortissement,
                                                      gesCode,
                                                      isSacd,
                                                      1,
                                                      sqlReq);
                                    END IF;
                                    INSERT INTO comptefi.BILAN_ACTIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant,  bilan_amort,  bilan_montant-bilan_amort, 0, gescode,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;


    PROCEDURE prepare_bilan_passif(btId bilan_poste.bt_id%TYPE,exeOrdre  bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, isSacd CHAR) AS
        bp_id0 bilan_poste.bp_id%TYPE;
        bp_id1 bilan_poste.bp_id%TYPE;
        bp_id2 bilan_poste.bp_id%TYPE;
        rec_bp1 bilan_poste%ROWTYPE;
        rec_bp2 bilan_poste%ROWTYPE;
        rec_bp3 bilan_poste%ROWTYPE;
        bilan_groupe1 VARCHAR2(1000);
        bilan_groupe2 VARCHAR2(1000);
        bilan_libelle VARCHAR2(1000);
        bilan_montant NUMBER;
        -- bilan_amort number;
         formuleMontant bilan_poste.BP_FORMULE_MONTANT%TYPE;
        bpStrId bilan_poste.bp_str_id%TYPE;
        CURSOR carbre IS
                SELECT *
                FROM bilan_poste
                WHERE exe_ordre=exeOrdre
                CONNECT BY PRIOR BP_ID = bp_id_pere
                START WITH BP_id_pere IN (SELECT BP_ID FROM bilan_poste WHERE bp_id_pere IS NULL AND bt_id=btId AND bp_str_id=bpStrId);

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = bp_id0 ORDER BY bp_ordre;
        CURSOR c2 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp1.bp_id ORDER BY bp_ordre;
        CURSOR c3 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeOrdre AND bp_id_pere = rec_bp2.bp_id ORDER BY bp_ordre;
        sqlreq VARCHAR2(4000);
        flag INTEGER;


   BEGIN
        bpStrId := 'passif';
        IF (exeordre IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' exeOrdre obligatoire');
        END IF;

        IF (btId IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' btId obligatoire');
        END IF;
        IF (gesCode IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' gesCode obligatoire');
        END IF;
        IF (isSacd IS NULL) THEN
            RAISE_APPLICATION_ERROR (-20001, ' isSacd obligatoire');
        END IF;

        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId AND bt_id=btId;
        IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001, bpStrId || ' non paramétré dans la table bilan_poste pour  : '|| exeordre || ', type '||btId);
        END IF;
        SELECT bp_id INTO bp_id0 FROM bilan_poste WHERE exe_ordre=exeordre AND bp_str_id=bpStrId;
        IF issacd = 'O' THEN
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = gescode;
        ELSIF issacd = 'G' THEN
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'AGREGE';
        ELSE
            DELETE FROM comptefi.BILAN_PASSIF WHERE exe_ordre = exeordre AND ges_code = 'ETAB';
        END IF;

         OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp1;
               EXIT WHEN c1%NOTFOUND;
                bilan_groupe1 := rec_bp1.bp_libelle;

                 OPEN c2;
                   LOOP
                     FETCH C2 INTO rec_bp2;
                       EXIT WHEN c2%NOTFOUND;

                     IF (rec_bp2.bp_groupe='N') THEN
                        IF (LENGTH(trim(rec_bp2.BP_FORMULE_MONTANT)) >0  )   THEN
                            bilan_groupe2 := NULL;
                            bilan_libelle := rec_bp2.bp_libelle;
                            formuleMontant := rec_bp2.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                            bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlreq);


                            INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                      end if;
                      ELSE
                         bilan_groupe2 := rec_bp2.bp_libelle;
                         OPEN c3;
                           LOOP
                             FETCH C3 INTO rec_bp3;
                               EXIT WHEN c3%NOTFOUND;
                                IF (LENGTH(trim(rec_bp3.BP_FORMULE_MONTANT)) >0  )   THEN
                                    bilan_libelle := rec_bp3.bp_libelle;
                                    formuleMontant := rec_bp3.BP_FORMULE_MONTANT;
                           -- formuleAmortissement := rec_bp2.BP_FORMULE_AMORTISSEMENT;
                                    bilan_montant := getSoldeMontant(exeordre, formuleMontant,
                                              gesCode,
                                              isSacd,
                                              1,
                                              sqlReq);


                                    INSERT INTO comptefi.BILAN_PASSIF VALUES (comptefi.seq_bilan_actif.NEXTVAL,bilan_groupe1, bilan_groupe2, bilan_libelle, bilan_montant, 0, gescode,exeordre);
                                end if;
                          END LOOP;
                          CLOSE c3;

                     END IF;

                  END LOOP;
                  CLOSE c2;

          END LOOP;
          CLOSE c1;

   END;

    PROCEDURE prepare_bilan(btId bilan_poste.bt_id%TYPE,exeOrdre bilan_poste.exe_ordre%TYPE, gescode VARCHAR2, sacd CHAR) IS
    BEGIN
         prepare_bilan_actif(btId, exeOrdre, gescode, sacd) ;
         prepare_bilan_passif(btId, exeOrdre, gescode, sacd) ;
    END;




    PROCEDURE checkFormule(formule VARCHAR2) IS
        sqlReq VARCHAR2(4000);
        res NUMBER;
    BEGIN
        res := getSoldeMontant(2010, formule, 'AGREGE','G', 0,sqlReq );

        RETURN;
    END checkFormule;


   PROCEDURE duplicateExercice(exeordreOld INTEGER, exeordreNew INTEGER) IS
        rec_bp bilan_poste%ROWTYPE;
        CURSOR c1 IS SELECT * FROM bilan_poste WHERE exe_ordre=exeordreOld AND BP_ID_PERE IS NULL;
        bpIdNew bilan_poste.bp_id%TYPE;
        flag INTEGER;

   BEGIN
        SELECT COUNT(*) INTO flag FROM bilan_poste WHERE exe_ordre=exeordreNew AND BP_ID_PERE IS NULL;
        IF (flag >0) THEN
            RAISE_APPLICATION_ERROR (-20001, 'Bilan deja cree pour   : '|| exeordreNew);
        END IF;


       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
            SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;

             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   NULL, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );

            prv_duplicateExercice(exeOrdreNew,rec_bp.bp_id, bpIdNew);
        END LOOP;
        CLOSE c1;
   END;

   PROCEDURE prv_duplicateExercice(exeordreNew INTEGER, racineOld bilan_poste.bp_id%TYPE, racineNew bilan_poste.bp_id%TYPE) IS
        --bp_idPere bilan_poste.bp_id%type;
        rec_bp bilan_poste%ROWTYPE;
        bpIdNew bilan_poste.bp_id%TYPE;

        CURSOR c1 IS SELECT * FROM bilan_poste WHERE bp_id_Pere = racineOld;
   BEGIN




       OPEN c1;
           LOOP
             FETCH C1 INTO rec_bp;
               EXIT WHEN c1%NOTFOUND;
             SELECT bilan_poste_seq.NEXTVAL INTO bpIdNew FROM dual;


             INSERT INTO MARACUJA.BILAN_POSTE (
                   BP_ID,
                   EXE_ORDRE,
                   BT_ID,
                   BP_ID_PERE,
                   BP_NIVEAU,
                   BP_ORDRE,
                   BP_STR_ID,
                   BP_LIBELLE,
                   BP_GROUPE,
                   BP_MODIFIABLE,
                   BP_FORMULE_MONTANT,
                   BP_FORMULE_AMORTISSEMENT
                   )
                VALUES (
                    bpIdNew,
                   exeOrdreNew, --EXE_ORDRE,
                   rec_bp.bt_id, --BT_ID,
                   racineNew, --BP_ID_PERE,
                   rec_bp.bp_niveau, --BP_NIVEAU,
                   rec_bp.bp_ordre, --BP_ORDRE,
                   rec_bp.bp_str_id, --BP_STR_ID,
                   rec_bp.bp_libelle, --BP_LIBELLE,
                   rec_bp.bp_groupe, --BP_GROUPE,
                   rec_bp.bp_modifiable, --BP_MODIFIABLE,
                   rec_bp.bp_formule_montant, --BP_FORMULE_MONTANT,
                   rec_bp.bp_formule_amortissement --BP_FORMULE_AMORTISSEMENT
                   );
             prv_duplicateExercice(exeordreNew, rec_bp.bp_id, bpIdNew);


           END LOOP;
       CLOSE c1;

   END prv_duplicateExercice;


END API_BILAN;
/


