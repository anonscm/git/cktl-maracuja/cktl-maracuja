CREATE OR REPLACE FORCE VIEW EPN.EPN_REC_BUD
(EXE_ORDRE, PCO_NUM, GES_CODE, EPN_DATE, MONT_REC, 
 ANNUL_TIT_REC)
AS 
SELECT t.exe_ordre, t.pco_num, b.ges_code, b.BOR_date_VISA, SUM(t.TIT_ht), 0
FROM maracuja.TITRE t, maracuja.BORDEREAU b
--  titres de recette
WHERE (b.TBO_ORDRE = 7 OR b.tbo_ordre=11 or b.tbo_ordre=200) AND b.BOR_ETAT = 'VISE' AND tit_etat = 'VISE'
AND t.bor_id = b.bor_id
GROUP BY t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa
UNION ALL
-- r�duction de recette
SELECT t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa, 0, SUM(t.TIT_ht)
FROM   maracuja.TITRE t, maracuja.BORDEREAU b
WHERE b.tbo_ordre = 9 AND b.BOR_ETAT = 'VISE' AND tit_etat = 'VISE'
AND t.bor_id = b.bor_id
GROUP BY t.exe_ordre, t.pco_num, b.ges_code, b.bor_date_visa;


