SET define OFF
SET scan OFF
CREATE OR REPLACE FORCE VIEW MARACUJA.V_MANDAT_REIMP_0
(EXE_ORDRE, ECR_DATE_SAISIE, ECR_DATE, GES_CODE, PCO_NUM, 
 BOR_ID, TBO_ORDRE, MAN_ID, MAN_NUM, MAN_LIB, 
 MAN_MONT, MAN_TVA, MAN_ETAT, FOU_ORDRE, ECR_ORDRE, 
 ECR_SACD, MDE_ORIGINE, ECD_MONTANT)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num, 
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, m.man_tva, 
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant 
 --- mandats des d�penses --- 
 FROM MANDAT m, 
      MANDAT_DETAIL_ECRITURE mde, 
       ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      BORDEREAU b, 
      TYPE_BORDEREAU tb 
WHERE m.man_id = mde.man_id 
       AND m.BOR_ID=b.bor_id 
      AND b.tbo_ordre=tb.tbo_ordre 
  AND mde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND mde_origine IN ('VISA', 'REIMPUTATION') 
  AND man_etat IN ('VISE', 'PAYE') 
  --AND ecd_debit<>0 
  AND ecd_credit = 0 -- pour avoir les mandats de r�gul TVA 
  AND ABS (ecd_montant)=ABS(man_ht) 
  AND ED.pco_num NOT LIKE '185%' 
  AND (tb.TBO_TYPE='BTME' OR tb.TBO_TYPE='BTMS') 
  AND tb.tbo_sous_type <> 'REVERSEMENTS'  -- pour �liminer les �critures de d�bit des OR 
UNION ALL 
 ---  mandats des PI --- 
 SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, SUBSTR(ed.pco_num,3,20) AS pco_num, 
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_debit, m.man_tva, 
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant 
 FROM MANDAT m, 
      MANDAT_DETAIL_ECRITURE mde, 
       ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      BORDEREAU b, 
      TYPE_BORDEREAU tb 
WHERE m.man_id = mde.man_id 
       AND m.BOR_ID=b.bor_id 
      AND b.tbo_ordre=tb.tbo_ordre 
  AND mde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND mde_origine IN ('VISA', 'REIMPUTATION') 
  AND man_etat IN ('VISE', 'PAYE') 
  AND ecd_debit <> 0 
  AND ABS (ecd_montant)=ABS(man_ht) 
  AND ED.pco_num NOT LIKE '185%' 
  AND (tb.TBO_TYPE='BTPI' OR tb.TBO_TYPE='BTPID') -- ajout du nouveau type BTPID -- 
UNION ALL 
-- Ordres de Reversements � partir de 2007 -- 
 SELECT ed.exe_ordre, e.ecr_date_saisie, e.ecr_date, m.ges_code, ed.pco_num, 
      m.bor_id, b.tbo_ordre, m.man_id, m.man_numero, ed.ecd_libelle, ecd_credit, (SIGN(ecd_credit))*m.man_tva, 
      m.man_etat, m.fou_ordre, ed.ecr_ordre, ei.ecr_sacd, mde_origine, ecd_montant 
 FROM MANDAT m, 
      MANDAT_DETAIL_ECRITURE mde, 
       ECRITURE_DETAIL ed, 
      v_ecriture_infos ei, 
      ECRITURE e, 
      BORDEREAU b, 
      TYPE_BORDEREAU tb 
WHERE m.man_id = mde.man_id 
       AND m.BOR_ID=b.bor_id 
      AND b.tbo_ordre=tb.tbo_ordre 
  AND mde.ecd_ordre = ed.ecd_ordre 
  AND ed.ecd_ordre = ei.ecd_ordre 
  AND ed.ecr_ordre = e.ecr_ordre 
  --AND r.pco_num_ancien = ed.pco_num 
  AND mde_origine IN ('VISA', 'REIMPUTATION') 
  AND man_etat IN ('VISE', 'PAYE') 
  AND ecd_credit <> 0 
  AND ABS (ecd_montant)=ABS(man_ht) 
  AND ED.pco_num NOT LIKE '185%' 
  AND tb.tbo_sous_type = 'REVERSEMENTS' -- Bordereau des OR;
  