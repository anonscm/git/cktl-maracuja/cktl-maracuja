

-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE MARACUJA.Api_Plsql_Papaye IS

/*
CRI G guadeloupe
Rivalland Frederic.

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja.

IL PERMET DE GENERER LES ECRITURES POUR LE
BROUILLARD DE PAYE.


*/


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures SACD   d un bordereau --
PROCEDURE passerEcritureSACDBord ( borid INTEGER);

-- permet de passer les  ecritures SACD  d un mois de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un bordereau --
PROCEDURE passerEcritureOppRetBord ( borid INTEGER, passer_ecritures VARCHAR);

-- permet de passer les  ecritures d oppositions  retenues  d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR);

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR);


-- PRIVATE --
-- permet de creer une ecriture --
FUNCTION creerEcriture (
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL
  ) RETURN INTEGER;


-- permet d ajouter des details a une ecriture.
FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  ) RETURN INTEGER;



END;
/


CREATE OR REPLACE PACKAGE BODY MARACUJA.Api_Plsql_Papaye IS
-- PUBLIC --


-- permet de generer les ecriture de visa d un bordereau --
PROCEDURE passerEcritureVISABord (borid INTEGER)
IS

CURSOR mandats IS
SELECT * FROM MANDAT_BROUILLARD WHERE man_id IN (SELECT man_id FROM MANDAT WHERE bor_id = borid)
AND mab_operation = 'VISA SALAIRES';

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'VISA SALAIRES' AND bob_etat = 'VALIDE';

currentmab maracuja.MANDAT_BROUILLARD%ROWTYPE;
currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;
tboordre maracuja.BORDEREAU.tbo_ordre%TYPE;

borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

bornum maracuja.BORDEREAU.bor_num%TYPE;
gescode maracuja.BORDEREAU.ges_code%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- Recuperation du libelle du mois a traiter
SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification du type de bordereau (Bordereau de salaires ? (tbo_ordre = 3)
SELECT tbo_ordre INTO tboordre FROM BORDEREAU WHERE bor_id = borid;
-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO boretat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (tboordre = 3 AND boretat = 'VALIDE')
THEN

--raise_application_error(-20001,'DANS TBOORDRE = 3');
   -- Creation de l'ecriture des visa (Debit 6, Credit 4).
   SELECT exe_ordre INTO exeordre FROM BORDEREAU WHERE bor_id = borid;

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- Creation des details ecritures debit 6 a partir des mandats de paye.
  OPEN mandats;
  LOOP
    FETCH mandats INTO currentmab;
    EXIT WHEN mandats%NOTFOUND;

	SELECT ori_ordre INTO oriordre FROM MANDAT WHERE man_id = currentmab.man_id;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentmab.mab_montant,
	NULL,
	currentmab.mab_sens,
	ecrordre,
	currentmab.ges_code,
	currentmab.pco_num
	);

	SELECT mandat_detail_ecriture_seq.NEXTVAL INTO mdeordre FROM dual;

	-- Insertion des mandat_detail_ecritures
	INSERT INTO MANDAT_DETAIL_ECRITURE VALUES (
	ecdordre,
	currentmab.exe_ordre,
	currentmab.man_id,
	SYSDATE,
	mdeordre,
	'VISA',
	oriordre
	);



	END LOOP;
  CLOSE mandats;

  -- Creation des details ecritures credit 4 a partir des bordereaux_brouillards.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'VISA SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  -- Validation de l'ecriture des visas
  Api_Plsql_Journal.validerecriture(ecrordre);

  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

  SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation LIKE 'RETENUES SALAIRES' AND bob_etat = 'VALIDE';

  -- Mise a jour de l'etat du bordereau (RETENUES OU PAIEMENT)
  IF (cpt > 0)
  THEN
  	  boretat := 'RETENUES';
  ELSE
  	  boretat := 'PAIEMENT';
  END IF;

  UPDATE MANDAT SET man_date_remise=TO_DATE(TO_CHAR(SYSDATE,'dd/mm/yyyy'),'dd/mm/yyyy') WHERE bor_id=borid;
  UPDATE BORDEREAU_BROUILLARD SET bob_etat = boretat WHERE bor_id = borid;
  UPDATE BORDEREAU SET bor_date_visa = SYSDATE,utl_ordre_visa = 0,bor_etat = boretat WHERE bor_id = borid;

  IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET etat = boretat, jou_ordre_visa = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET jpc_etat = boretat, ecr_ordre_visa = ecrordre WHERE bor_id = borid;
  END IF;

  passerecrituresacdbord(borid);

END IF;

END ;



-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureVISA ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;

-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)

END ;



-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACDBord ( borid INTEGER)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'SACD SALAIRES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
boretat maracuja.BORDEREAU.bor_etat%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

-- TEST ; Verification du type de bordereau (Bordereau de salaires ?)

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

   SELECT COUNT(*) INTO cpt FROM BORDEREAU_BROUILLARD WHERE bob_operation = 'SACD SALAIRES' AND bor_id = borid;

   IF (cpt > 0)
   THEN

	ecrordre := creerecriture(
	1,
	SYSDATE,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	exeordre,
	oriordre,
	14,
	9,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'SACD SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);

    IF (exeordre<2007) THEN
     UPDATE jefy.papaye_compta SET jou_ordre_sacd = ecrordre WHERE bor_ordre = borordre;
  ELSE
  	  UPDATE jefy_paye.jefy_paye_compta SET ecr_ordre_sacd = ecrordre WHERE bor_id = borid;
  END IF;

  END IF;

END ;

-- permet de passer les  ecritures SACD  d un bordereau de paye --
PROCEDURE passerEcritureSACD ( borlibelle_mois VARCHAR)
IS
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureVISA (borid INTEGER)


cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;


END ;


-- permet de passer les  ecritures d opp ret d un bordereau de paye --
PROCEDURE passerEcritureOppRetBord (borid INTEGER, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bor_id = borid AND bob_operation = 'RETENUES SALAIRES' AND bob_etat = 'RETENUES';

currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

moislibelle maracuja.BORDEREAU_BROUILLARD.bob_libelle2%TYPE;

bobetat  maracuja.BORDEREAU_BROUILLARD.bob_etat%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;
borordre maracuja.BORDEREAU.bor_ordre%TYPE;
exeordre maracuja.BORDEREAU.exe_ordre%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;

BEGIN

IF (passer_ecritures = 'O')
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;
   SELECT DISTINCT bob_libelle2 INTO moislibelle FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

   SELECT DISTINCT ges_code INTO gescode FROM BORDEREAU WHERE bor_id = borid;
   SELECT DISTINCT bor_num INTO bornum FROM BORDEREAU WHERE bor_id = borid;

-- Verification de l'etat du bordereau - Si etat != VALIDE, les ecritures ont deja ete passees.
SELECT DISTINCT bob_etat INTO bobetat FROM BORDEREAU_BROUILLARD WHERE bor_id = borid;

IF (bobetat = 'RETENUES')
THEN
				ecrordre := creerecriture(
				1,
				SYSDATE,
				'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
				exeordre,
				oriordre,
				14,
				9,
				0
				);

			  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
			  OPEN bordereaux;
			  LOOP
			    FETCH bordereaux INTO currentbob;
			    EXIT WHEN bordereaux%NOTFOUND;

						ecdordre := creerecrituredetail (
						NULL,
						'RETENUES SALAIRES '||moislibelle||' Bord. '||bornum||' du '||gescode,
						currentbob.bob_montant,
						NULL,
						currentbob.bob_sens,
						ecrordre,
						currentbob.ges_code,
						currentbob.pco_num
						);

				END LOOP;
			  CLOSE bordereaux;

			  Api_Plsql_Journal.validerecriture(ecrordre);


			  --UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;

  END IF;

END IF;



		  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAIEMENT' WHERE bor_id = borid;
		  UPDATE BORDEREAU
		  SET
		  bor_date_visa = SYSDATE,
		  utl_ordre_visa = 0,
		  bor_etat = 'PAIEMENT'
		  WHERE bor_id = borid;

		  SELECT bor_ordre INTO borordre FROM BORDEREAU WHERE bor_id = borid;

		      IF (exeordre<2007) THEN
		      	 		UPDATE jefy.papaye_compta SET etat = 'PAIEMENT', jou_ordre_opp = ecrordre WHERE bor_ordre = borordre;
		  	 ELSE
		  	  	 	 	UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'PAIEMENT', ECR_ORDRE_OPP = ecrordre WHERE bor_id = borid;
		  	 END IF;

END ;

-- permet de passer les  ecritures de visa d un mois de paye --
PROCEDURE passerEcritureOppRet ( borlibelle_mois VARCHAR)
IS

cpt INTEGER;
BEGIN
SELECT 1 INTO cpt FROM dual;
-- on boucle sur les bordereaux du mois bordereau_infos.libelle
-- appel de  passerEcritureOppRet (borid INTEGER)


END ;

-- permet de passer les  ecritures de paiement d un mois de paye --
PROCEDURE passerEcriturePaiement ( borlibelle_mois VARCHAR, passer_ecritures VARCHAR)
IS

CURSOR bordereaux IS
SELECT * FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois AND bob_operation = 'PAIEMENT SALAIRES'
AND bob_etat = 'PAIEMENT';

CURSOR gescodenontraites IS
SELECT ges_code FROM (
SELECT DISTINCT ges_code
FROM jefy_paye.jefy_ecritures e, jefy_paye.paye_mois p
WHERE e.mois_ordre=p.mois_ordre
AND p.mois_complet = borlibelle_mois
AND e.ecr_type=64
MINUS
SELECT DISTINCT b.ges_code
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE'
) ORDER BY ges_code;


currentbob maracuja.BORDEREAU_BROUILLARD%ROWTYPE;

cpt INTEGER;

mdeordre maracuja.MANDAT_DETAIL_ECRITURE.mde_ordre%TYPE;
oriordre maracuja.MANDAT.ori_ordre%TYPE;

exeordre maracuja.BORDEREAU.exe_ordre%TYPE;
gescode	 maracuja.BORDEREAU.ges_code%TYPE;
bornum	 maracuja.BORDEREAU.bor_num%TYPE;

ecrordre maracuja.ECRITURE.ecr_ordre%TYPE;
ecdordre maracuja.ECRITURE_DETAIL.ecd_ordre%TYPE;
curgescode maracuja.GESTION.ges_code%TYPE;
gescodes VARCHAR2(1000);

BEGIN
	 gescodes := '';
-- verifier que tous les gescode sont traites
  OPEN gescodenontraites;
  LOOP
    FETCH gescodenontraites INTO curgescode;
    EXIT WHEN gescodenontraites%NOTFOUND;
		 IF (LENGTH(gescodes)>0) THEN
		 	gescodes := gescodes || ', ';
		 END IF;
		  gescodes := gescodes || curgescode;
	END LOOP;
  CLOSE gescodenontraites;

  IF (LENGTH(gescodes)>0) THEN
  	 RAISE_APPLICATION_ERROR (-20001,'Les composantes suivantes n''ont pas encore ete liquidees et/ou mandatees dans Papaye : ' || gescodes);
  END IF;

  -- verifier qu etous les bordereaux du mois sont a l''etat PAIEMENT
SELECT COUNT(*) INTO cpt
 FROM BORDEREAU b, BORDEREAU_INFO bi
WHERE b.bor_id=bi.bor_id
AND b.tbo_ordre=3
AND bi.BOR_LIBELLE = borlibelle_mois
AND B.BOR_ETAT <> 'ANNULE' AND B.BOR_ETAT <> 'PAIEMENT';

IF (cpt >0) THEN
 RAISE_APPLICATION_ERROR (-20001,'Certains bordereaux ne sont pas a l''etat PAIEMENT');
END IF;



IF passer_ecritures = 'O'
THEN

   SELECT DISTINCT exe_ordre INTO exeordre FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois;

   -- On verifie que les ecritures de paiement ne soient pas deja passees.
   SELECT COUNT(*) INTO cpt FROM ECRITURE WHERE ecr_libelle = 'PAIEMENT SALAIRES '||borlibelle_mois;

   IF (cpt = 0)
   THEN
	ecrordre := creerecriture(
	1,
	SYSDATE,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	exeordre,
	oriordre,
	14,
	6,
	0
	);

  -- On parcourt les bordereaux_brouillards pour avoir les ecritures credit 4.
  OPEN bordereaux;
  LOOP
    FETCH bordereaux INTO currentbob;
    EXIT WHEN bordereaux%NOTFOUND;

	ecdordre := creerecrituredetail (
	NULL,
	'PAIEMENT SALAIRES '||borlibelle_mois,
	currentbob.bob_montant,
	NULL,
	currentbob.bob_sens,
	ecrordre,
	currentbob.ges_code,
	currentbob.pco_num
	);

	END LOOP;
  CLOSE bordereaux;

  Api_Plsql_Journal.validerecriture(ecrordre);


	      IF (exeordre<2007) THEN
	     UPDATE jefy.papaye_compta SET etat = 'TERMINEE', jou_ordre_dsk = ecrordre WHERE mois = borlibelle_mois;
	  ELSE
	  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE', ECR_ORDRE_DSK = ecrordre WHERE mois = borlibelle_mois;
	  END IF;
  END IF;

 ELSE
	      IF (exeordre<2007) THEN
	       UPDATE jefy.papaye_compta SET etat = 'TERMINEE' WHERE mois = borlibelle_mois;
	  ELSE
	  	  UPDATE jefy_paye.jefy_paye_compta SET JPC_ETAT = 'TERMINEE' WHERE mois = borlibelle_mois;
	  END IF;


END IF;

  UPDATE BORDEREAU_BROUILLARD SET bob_etat = 'PAYE' WHERE bob_libelle2 = borlibelle_mois;

    UPDATE MANDAT
 SET
  man_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

  UPDATE BORDEREAU
  SET
  bor_date_visa = SYSDATE,
  utl_ordre_visa = 0,
  bor_etat = 'PAYE'
  WHERE bor_id IN (
  SELECT bor_id FROM BORDEREAU_BROUILLARD WHERE bob_libelle2 = borlibelle_mois);

END;



-- PRIVATE --
FUNCTION creerEcriture(
  COMORDRE              NUMBER,--        NOT NULL,
  ECRDATE               DATE ,--         NOT NULL,
  ECRLIBELLE            VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE              NUMBER,--        NOT NULL,
  ORIORDRE              NUMBER,
  TJOORDRE              NUMBER,--        NOT NULL,
  TOPORDRE              NUMBER,--        NOT NULL,
  UTLORDRE              NUMBER--        NOT NULL,
  )RETURN INTEGER
IS
cpt INTEGER;

localExercice INTEGER;
localEcrDate DATE;

BEGIN

SELECT exe_exercice INTO localExercice FROM maracuja.EXERCICE WHERE exe_ordre = exeordre;

IF (SYSDATE > TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy'))
THEN
	localEcrDate := TO_DATE('31/12/'||localExercice, 'dd/mm/yyyy');
ELSE
	localEcrDate := ecrdate;
END IF;

RETURN Api_Plsql_Journal.creerEcritureExerciceType(
  COMORDRE     ,--         NUMBER,--        NOT NULL,
  localEcrDate      ,--         DATE ,--         NOT NULL,
  ECRLIBELLE   ,--       VARCHAR2,-- (200)  NOT NULL,
  EXEORDRE     ,--         NUMBER,--        NOT NULL,
  ORIORDRE     ,--        NUMBER,
  TOPORDRE     ,--         NUMBER,--        NOT NULL,
  UTLORDRE     ,--         NUMBER,--        NOT NULL,
  TJOORDRE 		--		INTEGER
  );

END;

FUNCTION creerEcritureDetail (
ECDCOMMENTAIRE    VARCHAR2,-- (200),
  ECDLIBELLE        VARCHAR2,-- (200),
  ECDMONTANT        NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE     VARCHAR2,-- (20),
  ECDSENS           VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE          NUMBER,--        NOT NULL,
 -- EXEORDRE          NUMBER,--        NOT NULL,
  GESCODE           VARCHAR2,-- (10)  NOT NULL,
  PCONUM            VARCHAR2-- (20)  NOT NULL
  )RETURN INTEGER

IS
EXEORDRE          NUMBER;
BEGIN

SELECT exe_ordre INTO EXEORDRE FROM ECRITURE
WHERE ecr_ordre = ecrordre;


RETURN Api_Plsql_Journal.creerEcritureDetail
(
  ECDCOMMENTAIRE,--   VARCHAR2,-- (200),
  ECDLIBELLE    ,--    VARCHAR2,-- (200),
  ECDMONTANT    ,--   NUMBER,-- (12,2) NOT NULL,
  ECDSECONDAIRE ,--  VARCHAR2,-- (20),
  ECDSENS       ,-- VARCHAR2,-- (1)  NOT NULL,
  ECRORDRE      ,-- NUMBER,--        NOT NULL,
  GESCODE       ,--    VARCHAR2,-- (10)  NOT NULL,
  PCONUM         --   VARCHAR2-- (20)  NOT NULL
  );


END;
END;
/


