create or replace procedure grhum.inst_maracuja_recup_conv_histo
is
   ecdordre   maracuja.ecriture_detail.ecd_ordre%type;
   conordre   maracuja.ecriture_detail.con_ordre%type;

   cursor c1
   is
      -- recuperer les ecritures de visa de recette avec con_ordre is null et ayant un con_ordre unique dans recette
      select ecd.ecd_ordre,
             jrcc.con_ordre
      from   maracuja.ecriture_detail ecd inner join maracuja.titre_detail_ecriture tde on (tde.ecd_ordre = ecd.ecd_ordre and tde.tde_origine like 'VISA%')
             inner join maracuja.recette r on (r.rec_id = tde.rec_id)
             inner join jefy_recette.recette_ctrl_planco rpco on (rpco.tit_id = tde.tit_id and rpco.rpco_id = r.rec_ordre)
             inner join jefy_recette.recette jr on (rpco.rec_id = jr.rec_id)
             inner join
             (select   rcc.rec_id
              from     jefy_recette.recette_ctrl_convention rcc
              group by rcc.rec_id
              having   count (*) = 1) x on (x.rec_id = jr.rec_id)
             inner join jefy_recette.recette_ctrl_convention jrcc on (jrcc.rec_id = jr.rec_id)
      where  ecd.con_ordre is null;

begin
   -- recuperation des conventions sur les recettes
   open c1;

   loop
      fetch c1
      into  ecdordre,
            conordre;

      exit when c1%notfound;

      update maracuja.ecriture_detail
         set con_ordre = conordre
       where ecd_ordre = ecdordre;
   end loop;

   close c1;


end;
