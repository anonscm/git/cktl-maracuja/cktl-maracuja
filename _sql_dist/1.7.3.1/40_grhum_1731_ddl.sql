SET DEFINE OFF;
CREATE OR REPLACE PROCEDURE MARACUJA.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

    precedentExercice := nouvelExercice - 1;
    
    -- -------------------------------------------------------
    -- Vérifications concernant l'exercice precedent
    
    
    -- Verif que l'exercice precedent existe
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=precedentExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    SELECT COUNT(*) INTO flag FROM exercice WHERE exe_exercice=nouvelExercice;
    IF (flag=0) THEN
       RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas dans JEFY_ADMIN, impossible de préparer l''exercice '|| nouvelExercice ||'.');
    END IF;
    
    --  -------------------------------------------------------
    -- Preparation des parametres
    INSERT INTO PARAMETRE (SELECT nouvelExercice, PAR_DESCRIPTION, PAR_KEY, parametre_seq.NEXTVAL, PAR_VALUE
                             FROM PARAMETRE
                          WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des codes gestion
    INSERT INTO GESTION_EXERCICE (SELECT nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185
                                    FROM GESTION_EXERCICE
                                 WHERE exe_ordre=precedentExercice) ;
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de paiement
    INSERT INTO MODE_PAIEMENT (SELECT nouvelExercice, MOD_LIBELLE, mode_paiement_seq.NEXTVAL, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
                                 MOD_VISA_TYPE, MOD_EMA_AUTO, MOD_CONTREPARTIE_GESTION
                                 FROM MODE_PAIEMENT
                              WHERE exe_ordre=precedentExercice);
    
    
    --  -------------------------------------------------------
    -- Récupération des modes de recouvrement
    INSERT INTO MODE_RECOUVREMENT(EXE_ORDRE, MOD_LIBELLE, MOD_ORDRE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE,
    MOD_CODE, MOD_EMA_AUTO, MOD_DOM)  (SELECT nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.NEXTVAL, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO, mod_dom
                                 FROM MODE_RECOUVREMENT
                              WHERE exe_ordre=precedentExercice);
    

    --  -------------------------------------------------------
    -- Récupération des plan comptables
    INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
   PCOE_ID, EXE_ORDRE, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
SELECT 
MARACUJA.PLAN_COMPTABLE_EXER_SEQ.NEXTVAL, nouvelExercice, PCO_NUM, 
   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
FROM MARACUJA.PLAN_COMPTABLE_EXER where exe_ordre=precedentExercice;
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_credit
    -- les nouveaux types de credit doivent exister
    INSERT INTO maracuja.PLANCO_CREDIT (PCC_ORDRE, TCD_ORDRE, PCO_NUM, PLA_QUOI, PCC_ETAT) (
    SELECT planco_credit_seq.NEXTVAL, x.TCD_ORDRE_new,  pcc.PCO_NUM, pcc.PLA_QUOI, pcc.PCC_ETAT
    FROM maracuja.PLANCO_CREDIT pcc, (SELECT tcnew.TCD_ORDRE AS tcd_ordre_new, tcold.tcd_ordre AS tcd_ordre_old
     FROM maracuja.TYPE_CREDIT tcold, maracuja.TYPE_CREDIT tcnew
    WHERE
    tcold.exe_ordre=precedentExercice
    AND tcnew.exe_ordre=nouvelExercice
    AND tcold.tcd_code=tcnew.tcd_code
    AND tcnew.tyet_id=1
    AND tcold.tcd_type=tcnew.tcd_type
    ) x
    WHERE
    pcc.tcd_ordre=x.tcd_ordre_old
    AND pcc.pcc_etat='VALIDE'
    );
    
    
    --  -------------------------------------------------------
    -- Récupération des planco_amortissment
    INSERT INTO maracuja.PLANCO_AMORTISSEMENT (PCA_ID, PCOA_ID, EXE_ORDRE, PCO_NUM, PCA_DUREE) (
    SELECT maracuja.PLANCO_AMORTISSEMENT_seq.NEXTVAL, p.PCOA_ID, nouvelExercice, PCO_NUM, PCA_DUREE
    FROM maracuja.PLANCO_AMORTISSEMENT p, maracuja.PLAN_COMPTABLE_AMO a
    WHERE exe_ordre=precedentExercice
    AND p.PCOA_ID = a.PCOA_ID
    AND a.TYET_ID=1
    );

    --  -------------------------------------------------------
    -- Récupération des planco_visa
    INSERT INTO maracuja.planco_visa(PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, pvi_ordre, PVI_ETAT, PVI_CONTREPARTIE_GESTION, exe_ordre) (
    SELECT PCO_NUM_CTREPARTIE, PCO_NUM_ORDONNATEUR, PCO_NUM_TVA, PVI_LIBELLE, maracuja.planco_visa_seq.nextval, PVI_ETAT, PVI_CONTREPARTIE_GESTION, nouvelExercice
    FROM maracuja.planco_visa p
    WHERE exe_ordre=precedentExercice    
    AND p.PVI_ETAT='VALIDE'
    );
 

    -- --------------------------------------------------------
    -- codes budgets pour les epn
    insert into epn.code_budget_sacd(sacd, cod_bud, exe_ordre) 
        select sacd, cod_bud, nouvelExercice from epn.code_budget_sacd where exe_ordre=precedentExercice; 

   
END;
/


GRANT EXECUTE ON  MARACUJA.PREPARE_EXERCICE TO JEFY_ADMIN;

