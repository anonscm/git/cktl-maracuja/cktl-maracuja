SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW COMPTEFI.V_ECR_MANDATS_DEBITS
(EXE_ORDRE, ECR_DATE_SAISIE, GES_CODE, PCO_NUM, BOR_ID, 
 MAN_ID, MAN_NUMERO, ECD_LIBELLE, ECD_MONTANT, TVA, 
 MAN_ETAT, FOU_ORDRE, ECR_ORDRE, ECR_SACD, MDE_ORIGINE)
AS 
SELECT ed.exe_ordre, e.ecr_date_saisie, m.ges_code, ed.pco_num, m.bor_id,
       m.man_id, m.man_numero, ed.ecd_libelle, ecd_montant,
       (SIGN (ecd_montant)) * m.man_tva tva, m.man_etat, m.fou_ordre,
       ed.ecr_ordre, ei.ecr_sacd, mde_origine
  FROM maracuja.mandat m,
  	   maracuja.bordereau b,
	   maracuja.type_bordereau tb,
       maracuja.mandat_detail_ecriture mde,
       maracuja.ecriture_detail ed,
       maracuja.v_ecriture_infos ei,
       maracuja.ecriture e
 WHERE b.bor_id=m.bor_id
    and b.tbo_ordre=tb.tbo_ordre    
   AND  m.man_id = mde.man_id
   AND mde.ecd_ordre = ed.ecd_ordre
   AND ed.ecd_ordre = ei.ecd_ordre
   AND ed.ecr_ordre = e.ecr_ordre
   --AND r.pco_num_ancien = ed.pco_num
   AND mde_origine IN ('VISA', 'REIMPUTATION')
   AND man_etat IN ('VISE', 'PAYE')
   AND ecd_debit <> 0
   AND tb.tbo_sous_type<> 'REVERSEMENTS'
   AND tb.tbo_sous_type<> 'SCOLARITE'
   AND ABS (ecd_montant) = ABS (man_ht);
   
   
   