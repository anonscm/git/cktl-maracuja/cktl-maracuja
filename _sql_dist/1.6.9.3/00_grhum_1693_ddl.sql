SET DEFINE OFF;
CREATE OR REPLACE FORCE VIEW MARACUJA.V_BUDGET_LOLF_EXEC
(EXERCICE, NATURE, MASSE, TYPE_CREDIT, TCD_LIBELLE, 
 BUDGET, UB, LOLF_CODE_PERE, LOLF_LIBELLE_PERE, CODE_LOLF, 
 LIBELLE_LOLF, MONTANT)
AS 
select exercice, nature, masse, type_credit, tcd_libelle,operation BUDGET, ub, lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf,  sum(montant_budgetaire) montant
from
(
   select tcd.tcd_libelle tcd_libelle,tcd_sect masse,'execution' operation,o.org_ub ub,rpca.exe_ordre exercice, jlfr.lolf_code lolf_code_pere, jlfr.lolf_libelle lolf_libelle_pere,   
    lfr.lolf_code code_lolf, lfr.lolf_libelle libelle_lolf,rpca.RACT_HT_SAISIE montant_budgetaire, ' ' libelle,tcd.tcd_code type_credit,tcd.tcd_type nature
   from
   JEFY_recette.recette_CTRL_ACTION rpca ,
   jefy_recette.recette_BUDGET rb ,
   jefy_recette.V_LOLF_NOMENCLATURE_RECETTE lfr,
   jefy_admin.lolf_nomenclature_recette jlfr,
   jefy_recette.facture_budget fb,
   jefy_admin.organ o,
   jefy_admin.type_credit tcd
   where rb.rec_ID = rpca.rec_id
   and lfr.lolf_id = rpca.lolf_id
   and lfr.lolf_pere=jlfr.lolf_id
   and fb.fac_id = rb.fac_id
   and o.org_id = fb.org_id
   and tcd.tcd_ordre = fb.tcd_ordre
   and lfr.exe_ordre = rpca.exe_ordre
   and rb.rec_id in (select distinct rec_id from JEFY_RECETTE.RECETTE_CTRL_planco where tit_id is not null)
union all
   select tcd.tcd_libelle tcd_libelle,tcd_sect masse,'execution' operation,o.org_ub ub,dpp.exe_ordre exercice, jlfd.lolf_code lolf_code_pere, 
   jlfd.lolf_libelle lolf_libelle_pere,lfd.lolf_code code_lolf,lfd.lolf_libelle libelle_lolf,dpca.dact_montant_budgetaire montant_budgetaire,' ' libelle,tcd.tcd_code type_credit,tcd.tcd_type nature
   from JEFY_DEPENSE.DEPENSE_CTRL_ACTION dpca ,
   jefy_depense.DEPENSE_BUDGET db ,
   jefy_depense.DEPENSE_PAPIER dpp,
   jefy_admin.V_LOLF_NOMENCLATURE_DEPENSE lfd,
   jefy_admin.lolf_nomenclature_depense jlfd,
   jefy_depense.engage_budget eb,
   jefy_admin.organ o,
   jefy_admin.type_credit tcd
   where db.DPP_ID = dpp.dpp_id
   and db.DEP_ID = dpca.DEP_ID
   and lfd.lolf_id = dpca.tyac_id
   and lfd.lolf_pere=jlfd.lolf_id
  and lfd.exe_ordre = dpca.exe_ordre       
  and eb.eng_id = db.eng_id
   and o.org_id = eb.org_id
   and tcd.tcd_ordre = eb.tcd_ordre
   and db.dep_id in (select distinct dep_id from JEFY_DEPENSE.DEPENSE_CTRL_planco where man_id is not null)
)
group by exercice, nature, masse, type_credit, tcd_libelle,operation , ub, lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf
order by exercice, nature, masse, type_credit, tcd_libelle, BUDGET, ub, lolf_code_pere, lolf_libelle_pere, code_lolf,libelle_lolf;


-- --------------------------

CREATE OR REPLACE FORCE VIEW COMPTEFI.V_BUDNAT
(EXE_ORDRE, GES_CODE, PCO_NUM, BDN_QUOI, BDN_NUMERO, 
 DATE_CO, CO)
AS 
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 1
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 2
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,3), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
select bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation, sum(bdsn_saisi)
from jefy_budget.budget_saisie_nature bsn, jefy_budget.budget_masque_nature bmn, jefy_budget.v_type_credit_budget tcb,
jefy_budget.budget_saisie bs, maracuja.v_organ_exer o
where bsn.org_id = o.org_id and bsn.exe_ordre = o.exe_ordre
and bsn.bdsa_id = bs.bdsa_id and bsn.exe_ordre = bs.exe_ordre and bsn.PCO_NUM = bmn.PCO_NUM
and bsn.tcd_ordre = tcb.tcd_ordre and bmn.TCD_ORDRE = tcb.TCD_ORDRE and tcb.TCD_SECT = 3
and bdsa_date_validation is not null
group by bsn.exe_ordre, o.org_UB, substr(bsn.pco_num,1,2), substr(tcb.TCD_TYPE,0,1), bsn.bdsa_id, bdsa_date_validation
union all
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy.budnat b, jefy.exer e, jefy.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy.budnat b, jefy.exer e, jefy.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq)
FROM jefy.budnat b, jefy.organ o
WHERE b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY')
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY') exr_cloture, SUM (bdn_reliq)
FROM jefy.budnat b, jefy.organ o
WHERE b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, TO_DATE('01/01/2006','DD/MM/YYYY')
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2006, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy.budnat_dbm b, jefy.exer e , jefy.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_prim)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'PRIM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'RELI'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, EXR_CLOTURE, SUM (bdn_reliq)
FROM jefy05.budnat b, jefy05.exer e, jefy05.organ o
WHERE e.EXR_TYPE = 'RELI'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, 0, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '6%' OR PCO_NUM LIKE '7%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,2), bdn_quoi, bdn_numero, exr_cloture
UNION ALL
SELECT 2005, org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, EXR_CLOTURE,
SUM (bdn_dbm)
FROM jefy05.budnat_dbm b, jefy05.exer e , jefy05.organ o
WHERE b.BDN_NUMERO = e.EXR_NUMERO AND e.EXR_TYPE = 'DBM'
AND b.org_ordre = o.org_ordre
AND o.org_niv = 2
AND (PCO_NUM LIKE '1%' OR PCO_NUM LIKE '2%' )
GROUP BY org_comp, SUBSTR(PCO_NUM,1,3), bdn_quoi, bdn_numero, exr_cloture;



   
   