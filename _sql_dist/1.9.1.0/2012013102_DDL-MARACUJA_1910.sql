SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/3
-- Type : DDL
-- Schéma modifié :  MARACUJA, COMPTEFI
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.0
-- Date de publication : 31/01/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Modification de la table mode_paiement pour paiement HT et prise en compte des comptes de TVA intra
-- Correction d'une vue utilisee dans le cadre 5 du compte financier (pb recuperation des re-imputations)
-- ajout de parametres pour indiquer les comptes de contrepartie a utiliser en fonction des types de crédit lors de la génération des ordres de reversement
-- ajout d'un parametre indiquant la racine des comptes de TVA pour simplifier la selection de ces comptes dans les applications
-- typage des ecritures de VISA TVA (auparavant typées VISA)
-- transfert des ecritures 86* vers le journal des valeurs inactives (auparavant sur le journal exercice) 
-- nettoyage des tables maracuja.planco_visa et maracuja.planco_credit qui pointent vers un pco_num/exe_ordre non existant dans maracujaa.plpan_comptable_exer)
-- Creation de plan_comptable_exer "ANNULE" qui ne seraient pas existant mais references dans les tables titre, mandat, titre_brouillard, etc. Ceci afin de preparer la creation des contraientes de reference sur pco_num/exe_ordre.
----------------------------------------------





INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 4, 'BD', '1.9.1.0',  null, '');
commit;






ALTER TABLE MARACUJA.MODE_PAIEMENT ADD (PCO_NUM_TVA  VARCHAR2(20));
ALTER TABLE MARACUJA.MODE_PAIEMENT ADD (PCO_NUM_TVA_CTP  VARCHAR2(20));
ALTER TABLE MARACUJA.MODE_PAIEMENT ADD (MOD_PAIEMENT_HT  VARCHAR2(1)  DEFAULT 'N' NOT NULL);

ALTER TABLE MARACUJA.MODE_PAIEMENT ADD  CONSTRAINT FK_MP_PCONUMTVACTP FOREIGN KEY (EXE_ORDRE, PCO_NUM_TVA_CTP) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE MARACUJA.MODE_PAIEMENT ADD  CONSTRAINT FK_MP_PCONUMTVA FOREIGN KEY (EXE_ORDRE, PCO_NUM_TVA) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;


COMMENT ON COLUMN MARACUJA.MODE_PAIEMENT.PCO_NUM_TVA IS 'Référence au compte de TVA déductible à utiliser en débit (ex. 445x). Si nul, celui de planco_visa est utilisé par défaut.';
COMMENT ON COLUMN MARACUJA.MODE_PAIEMENT.PCO_NUM_TVA_CTP IS 'Référence au compte de TVA à utiliser en crédit en cas de paiement HT (ex.4452). Obligatoire si pco_paiement_HT est à O';
COMMENT ON COLUMN MARACUJA.MODE_PAIEMENT.MOD_PAIEMENT_HT IS 'Payer le fournisseur avec le montant HT (O/N)';




create or replace force view comptefi.v_ecr_titres_credits (exe_ordre, ecr_date_saisie, ges_code, pco_num, bor_id, tit_id, tit_numero, ecd_libelle, ecd_montant, tva, tit_etat, fou_ordre, ecr_ordre, ecr_sacd, tde_origine)
  as    select  
          ed.exe_ordre,
          e.ecr_date_saisie,
          m.ges_code,
          ed.pco_num,
          m.bor_id,
          m.tit_id,
          m.tit_numero,
          ed.ecd_libelle,
          ecd_montant,
          (sign (ecd_montant)) * m.tit_tva tva,
          m.tit_etat,
          m.fou_ordre,
          ed.ecr_ordre,
          ei.ecr_sacd,
          tde_origine
   from   maracuja.titre m, (select tit_id, sum(rec_mont) as rec_mont_total from maracuja.recette group by tit_id) r, maracuja.bordereau b, maracuja.titre_detail_ecriture mde, maracuja.ecriture_detail ed, maracuja.v_ecriture_infos ei, maracuja.ecriture e
   where  m.bor_id = b.bor_id
   and    m.tit_id = mde.tit_id
   and    m.tit_id = r.tit_id
   and    mde.exe_ordre = m.exe_ordre
   and    mde.ecd_ordre = ed.ecd_ordre
   and    ed.ecd_ordre = ei.ecd_ordre
   and    ed.ecr_ordre = e.ecr_ordre
   and    tde_origine in ('VISA', 'REIMPUTATION')
   and    tit_etat in ('VISE')
   and    ecd_credit <> 0
   and    tbo_ordre in (7, 11, 200)
   and    e.top_ordre <> 11
   and    abs (ecd_montant) = abs (rec_mont_total);
/


--**************---
CREATE OR REPLACE PACKAGE MARACUJA.api_planco
is
   function is_planco_valide (pconum plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return integer;
      
   function is_planco_existe (pconum plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return integer;

   function creer_planco_pi (exeordre integer, pconumbudgetaire varchar)
      return plan_comptable_exer.pco_num%type;

   function creer_planco (exeordre integer, pconum varchar, pcolibelle varchar, pcobudgetaire varchar, pcoemargement varchar, pconature varchar, pcosensemargement varchar, pcovalidite varchar, pcojexercice varchar, pcojfinexercice varchar, pcojbe varchar)
      return plan_comptable_exer.pco_num%type;

   function prv_fn_creer_planco (pconum varchar, pcolibelle varchar, pcobudgetaire varchar, pcoemargement varchar, pconature varchar, pcosensemargement varchar, pcovalidite varchar, pcojexercice varchar, pcojfinexercice varchar, pcojbe varchar)
      return plan_comptable.pco_num%type;

   function prv_fn_creer_planco_exer (
      exeordre            number,
      pconum              varchar,
      pcolibelle          varchar,
      pcobudgetaire       varchar,
      pcoemargement       varchar,
      pconature           varchar,
      pcosensemargement   varchar,
      pcovalidite         varchar,
      pcojexercice        varchar,
      pcojfinexercice     varchar,
      pcojbe              varchar
   )
      return plan_comptable_exer.pco_num%type;

   function get_pco_libelle (pconum varchar, exeordre number)
      return plan_comptable_exer.pco_libelle%type;

   procedure valider_planco (exeordre number, pconum varchar, valide varchar);

   /* Creer un compte avec toutes les caracteristiques d'un autre */
   procedure creer_planco_from_ref (exeordre integer, pconumnew varchar, pcolibelle varchar, pconumref varchar);

   procedure set_sens_solde (exeordre integer, pconum varchar, senssolde varchar);

   /** Affecter tous les types de credit tcdsect */
   procedure affecte_type_credit (exeordre integer, pconum varchar, tcdsect varchar, tcdtype varchar, plaquoi varchar);
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.api_planco IS

   function is_planco_existe (pconum plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return integer
      is
        res integer;
      begin
        select count(*) into res from plan_comptable_exer where pco_num=pconum and exe_ordre=exeOrdre;
        return res;
      end;
      --------------------------------------------------------------------------
      
   function is_planco_valide (pconum plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return integer
      is
        res integer;
      begin
        select count(*) into res from plan_comptable_exer where pco_num=pconum and exe_ordre=exeOrdre and pco_validite='VALIDE';
        return res;
      end;
      --------------------------------------------------------------------------
    /** Creer un compte de prestation interne a partir du compte budgetaire */
    function creer_planco_pi(
        exeordre INTEGER,  
        pconumBudgetaire VARCHAR
    )
       RETURN plan_comptable_exer.pco_num%TYPE
    is
        flag integer;
        plancoexer plan_comptable_exer%rowtype; 
    begin
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        if (flag=0) then
            return null;
        end if;
        
        select * into plancoexer from plan_comptable_exer where exe_ordre=exeordre and pco_num = pconumBudgetaire;
        
        return creer_planco(exeordre,    
            '18'||pconumBudgetaire,
            plancoexer.pco_libelle,
            plancoexer.pco_budgetaire,
            plancoexer.pco_emargement,
            plancoexer.pco_nature,
            plancoexer.pco_sens_emargement,
            plancoexer.pco_validite,
            plancoexer.pco_j_exercice,
            plancoexer.pco_j_fin_exercice,
            plancoexer.pco_j_be);
         
    end;

    /** creer un enregistrement dans plan comptable (plan_comptable_exer + plan_comptable)  */
    function creer_planco (
      exeordre           INTEGER,    
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable_exer.pco_num%TYPE
    is
        res plan_comptable.pco_num%TYPE;
    begin
        res := prv_fn_creer_planco_exer(exeordre, pconum,pcolibelle,pcobudgetaire,pcoemargement,pconature,pcosensemargement,pcovalidite,pcojexercice,pcojfinexercice,pcojbe);
        return res;
    end;

----------------------------------------------

   FUNCTION prv_fn_creer_planco (pconum VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
      RETURN plan_comptable.pco_num%TYPE
   IS
      flag   INTEGER;
      pconiveau integer;
   BEGIN
      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable
       WHERE pco_num = pconum;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable
                  (pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite,                   --PCO_VALIDITE,
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

      RETURN pconum;
   END;
   
   -----------------------------------------------------
   
   FUNCTION prv_fn_creer_planco_exer (
      exeordre            NUMBER,
      pconum              VARCHAR,
      pcolibelle          VARCHAR,
      pcobudgetaire       VARCHAR,
      pcoemargement       VARCHAR,
      pconature           VARCHAR,
      pcosensemargement   VARCHAR,
      pcovalidite         VARCHAR,
      pcojexercice        VARCHAR,
      pcojfinexercice     VARCHAR,
      pcojbe              VARCHAR
   )
   RETURN plan_comptable_exer.pco_num%TYPE
    IS
      flag   INTEGER;
      pconiveau integer;
      res plan_comptable_exer.pco_num%TYPE;
   BEGIN
        

      -- verifier si le compte existe
      SELECT COUNT (*)
        INTO flag
        FROM plan_comptable_exer
       WHERE pco_num = pconum and exe_ordre=exeordre;

      IF (flag > 0)
      THEN
         RETURN pconum;
      END IF;
      
      pconiveau := length(pconum);

      INSERT INTO plan_comptable_exer
                  (pcoe_id, exe_ordre, pco_budgetaire, pco_emargement, pco_libelle, pco_nature,
                   pco_niveau, pco_num, pco_sens_emargement, pco_validite,
                   pco_j_exercice, pco_j_fin_exercice, pco_j_be
                  )
           VALUES (plan_comptable_exer_seq.nextval, 
                   exeordre, 
                   pcobudgetaire,                            --PCO_BUDGETAIRE,
                   pcoemargement,              --PCO_EMARGEMENT,
                   pcolibelle,      --PCO_LIBELLE,
                   pconature, --PCO_NATURE,
                   pconiveau, --PCO_NIVEAU,
                   pconum,                                --PCO_NUM,
                   pcosensemargement,  --PCO_SENS_EMARGEMENT,
                   pcovalidite, --PCO_VALIDITE
                   pcojexercice,                             --PCO_J_EXERCICE,
                   pcojfinexercice,        --PCO_J_FIN_EXERCICE,
                   pcojbe
                  );

        -- creer le compte dans plan comptable s'il n'existe pas
       res := prv_fn_creer_planco(pconum,
                    pcolibelle,
                    pcobudgetaire,
                    pcoemargement,
                    pconature,
                    pcosensemargement,
                    pcovalidite,
                    pcojexercice,
                    pcojfinexercice,
                    pcojbe);        
      RETURN pconum;
   END;
   
    function get_pco_libelle (pconum varchar, exeordre number)
        return plan_comptable_exer.pco_libelle%type
    is
        res plan_comptable_exer.pco_libelle%type;
    begin
        select pco_libelle into res from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        return res;
    end;   
   
   
   
   
       procedure valider_Planco( exeordre number,pconum varchar, valide varchar)
       is
       begin
            if (valide='O' or valide='VALIDE') then
                 update plan_comptable_exer set pco_validite='VALIDE' where pco_num=pconum and exe_ordre=exeordre;
            end if;
            if (valide='N' or valide='ANNULE') then
                 update plan_comptable_exer set pco_validite='ANNULE' where pco_num=pconum and exe_ordre=exeordre;
            end if;
       
           
       end;
    
    /* Creer un compte avec toutes les caracteristiques d'un autre (planco_visa, planco_credit) */
    procedure creer_Planco_From_ref(
            exeordre           INTEGER,    
            pconumNew              VARCHAR,
            pcolibelle          VARCHAR,
            pconumRef           varchar)
      is
        pcoRefExiste integer;
        pcoNewExiste integer;
        plancovisaNewexiste integer;
        plancocreditNewexiste integer;
        planconew plan_comptable_exer.PCO_NUM%type;
        pcoref plan_comptable_exer%rowtype;
      begin
                pcoRefExiste := 0;
                pcoNewExiste := 0;
    
                -- verifier si le compte de reference existe
                select count(*) into pcoRefExiste from maracuja.plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumRef;
    
                -- verifier si le nouveau compte existe
                select count(*) into pcoNewExiste from maracuja.plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumNew;

    
                -- si le nouveau compte existe et s'il est invalide, on le valide
                -- si existe pas, on le cree 
                if (pcoNewExiste >0) then
                    valider_planco(pconumNew, exeordre, 'O');
                else
                    if (pcoRefExiste>0) then
                        select * into pcoref from plan_comptable_exer where exe_ordre = exeordre and pco_num=pconumRef;
                    
                        planconew := creer_planco (exeordre,    
                                    pconumNew,
                                    pcolibelle,
                                    pcoref.pco_budgetaire,
                                    pcoref.pco_emargement,
                                    pcoref.pco_nature,
                                    pcoref.pco_sens_emargement,
                                    pcoref.pco_validite,
                                    pcoref.pco_j_exercice,
                                    pcoref.pco_j_fin_exercice,
                                    pcoref.pco_j_be);                                
                    else
                       planconew := creer_planco (exeordre,    
                                    pconumNew,
                                    pcolibelle,
                                    'O',
                                    'N',
                                    'D',
                                    '0',
                                    'VALIDE',
                                    'N',
                                    'N',
                                    'N');                        
                    end if;
                
                end if;
    
    
                -- dupliquer les donnees sur les tables peripheriques
                  -- planco_visa
                select count(*) into plancovisaNewexiste from planco_visa where pco_num_ordonnateur = pconumNew and exe_ordre=exeordre;
                if (plancovisaNewexiste = 0 and pcoRefExiste>0) then
                        INSERT INTO MARACUJA.PLANCO_VISA (
                                    PVI_ORDRE,
                                    EXE_ORDRE,
                                    PCO_NUM_ORDONNATEUR, 
                                    PCO_NUM_CTREPARTIE,                                      
                                    PCO_NUM_TVA, 
                                    PVI_LIBELLE,                                    
                                    PVI_ETAT, 
                                    PVI_CONTREPARTIE_GESTION 
                                    ) 
                            select  planco_visa_seq.nextval,
                                    exeordre,
                                    pconumNew, 
                                    PCO_NUM_CTREPARTIE,                                      
                                    PCO_NUM_TVA, 
                                    PVI_LIBELLE,                                    
                                    PVI_ETAT, 
                                    PVI_CONTREPARTIE_GESTION from planco_visa where pco_num_ordonnateur = pconumRef and exe_ordre=exeordre;                    
                end if;
                
                
                
                -- planco_credit                
                select count(*) into plancocreditNewexiste from planco_credit p, type_credit tc where p.tcd_ordre=tc.tcd_ordre and pco_num = pconumNew and tc.exe_ordre = exeordre;
                if (plancocreditNewexiste = 0 and pcoRefExiste>0) then
                        INSERT INTO MARACUJA.PLANCO_CREDIT (
                           PCC_ORDRE, 
                           PCO_NUM, 
                           TCD_ORDRE,                            
                           PLA_QUOI, 
                           PCC_ETAT) 
                        select PLANCO_CREDIT_seq.nextval, 
                                pconumNew, 
                               tc.TCD_ORDRE,                                
                               PLA_QUOI, 
                               PCC_ETAT from planco_credit p, type_credit tc where p.tcd_ordre=tc.tcd_ordre and pco_num=pconumRef and tc.exe_ordre = exeordre;                    
                end if;                
                
      
      end;
   
   
    procedure set_sens_solde(exeordre integer, pconum varchar, sensSolde varchar)
    is
    begin
        update plan_comptable_exer set PCO_SENS_SOLDE=sensSolde where pco_num=pconum and exe_ordre=exeordre;
    end;
 

    procedure affecte_type_credit(exeordre integer, pconum varchar, tcdSect varchar, tcdType varchar, plaquoi varchar)
    is
        flag integer;
     begin
        select count(*) into flag from type_credit where exe_ordre=exeordre and tcd_Sect=tcdsect and tcd_type=tcdType;
        if (flag=0) then
            raise_application_error (-20001,'tcd_sect '|| tcdsect || ' non trouve pour exercice '||exeordre);
        end if;
        
        select count(*) into flag from plan_comptable_exer where exe_ordre=exeordre and pco_num=pconum;
        if (flag=0) then
            raise_application_error (-20001,'pco_num '|| pconum || ' non trouve pour exercice '||exeordre);
        end if;
        
        
        DELETE FROM MARACUJA.PLANCO_CREDIT where pco_num=pconum and tcd_ordre in (select tcd_ordre from type_credit where exe_ordre=exeordre and tcd_sect=tcdsect and tcd_type=tcdType); 
        
        INSERT INTO MARACUJA.PLANCO_CREDIT (
                    PCC_ORDRE, 
                    TCD_ORDRE, 
                    PCO_NUM, 
                    PLA_QUOI, 
                    PCC_ETAT) 
                select  
                    PLANCO_CREDIT_seq.nextval, 
                    TCD_ORDRE, 
                    pconum, 
                    plaquoi, 
                    'VALIDE'
                    from type_credit where exe_ordre = exeordre and tcd_sect = tcdsect and tcd_type=tcdType; 

     end;    
   
END;
/
--**************---
CREATE OR REPLACE PACKAGE MARACUJA.util
is
   procedure annuler_visa_bor_mandat (borid integer);

   procedure annuler_visa_bor_titre (borid integer);

   procedure supprimer_visa_btme (borid integer);

   procedure supprimer_visa_btms (borid integer);

   procedure supprimer_visa_btte (borid integer);

   procedure creer_ecriture_annulation (ecrordre integer);

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer;

   procedure annuler_emargement (emaordre integer);

   procedure supprimer_bordereau_dep (borid integer);

   procedure supprimer_bordereau_rec (borid integer);

   procedure supprimer_bordereau_btms (borid integer);

   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer);

   procedure supprimer_paiement_virement (paiordre integer);

   procedure corriger_etat_mandats;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.util
is
   -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
   -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

   -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
   procedure annuler_visa_bor_mandat (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, mandat_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME' and tbotype <> 'BTRU') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME ou BTRU');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.mde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandats associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from mandat_detail_ecriture
               where man_id = tmpmanid;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
--
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

      --                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

      --                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure annuler_visa_bor_titre (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      titre.tit_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmpmanid;

         exit when lestitres%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from titre_detail_ecriture
               where tit_id = tmpmanid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmpmanid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
   procedure supprimer_visa_btme (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.mde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         select count (*)
         into   flag
         from   ecriture_detail ecd, mandat_detail_ecriture mde
         where  ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

         if (flag > 0) then
            select distinct ecr_ordre
            into            tmpecrordre
            from            ecriture_detail ecd, mandat_detail_ecriture mde
            where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

            delete from ecriture_detail
                  where ecr_ordre = tmpecrordre;

            delete from ecriture
                  where ecr_ordre = tmpecrordre;

            delete from mandat_detail_ecriture
                  where man_id = tmpmanid;
         end if;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTMS
   procedure supprimer_visa_btms (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesecritures
      is
         select distinct ed.ecr_ordre
         from            ecriture_detail ed, mandat_detail_ecriture mde, mandat m
         where           m.man_id = mde.man_id and mde.ecd_ordre = ed.ecd_ordre and m.bor_id = borid;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTMS') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTMS');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat = 'VALIDE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

      --            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;
      open lesecritures;

      loop
         fetch lesecritures
         into  tmpecrordre;

         exit when lesecritures%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre in (select ecd_ordre
                                   from   ecriture_detail
                                   where  ecr_ordre = tmpecrordre);

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;
      end loop;

      close lesecritures;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;

      update jefy_paye.jefy_paye_compta
         set ecr_ordre_visa = null,
             ecr_ordre_sacd = null,
             ecr_ordre_opp = null
       where bor_id in (borid);
   end;

   -- annulation du visa d'un bordereau de depense BTTE
   procedure supprimer_visa_btte (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmptitid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmptitid;

         exit when lestitres%notfound;

         select distinct ecr_ordre
         into            tmpecrordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmptitid;

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;

         delete from titre_detail_ecriture
               where tit_id = tmptitid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmptitid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure creer_ecriture_annulation (ecrordre integer)
   is
      ecr           maracuja.ecriture%rowtype;
      ecd           maracuja.ecriture_detail%rowtype;
      flag          integer;
      str           ecriture.ecr_libelle%type;
      newecrordre   ecriture.ecr_ordre%type;
      newecdordre   ecriture_detail.ecd_ordre%type;
      x             integer;

      cursor c1
      is
         select *
         from   maracuja.ecriture_detail
         where  ecr_ordre = ecrordre;
   begin
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucune ecriture retrouvee pour ecr_ordre= ' || ecrordre || ' .');
      end if;

      select *
      into   ecr
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      str := 'Annulation Ecriture ' || ecr.exe_ordre || '/' || ecr.ecr_numero;

      -- verifier que l'ecriture n'a pas deja ete annulee
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_libelle = str;

      if (flag > 0) then
         select ecr_numero
         into   flag
         from   maracuja.ecriture
         where  ecr_libelle = str;

         raise_application_error (-20001, 'L''ecriture numero ' || ecr.ecr_numero || ' (ecr_ordre= ' || ecrordre || ') a deja ete annulee par l''ecriture numero ' || flag || '.');
      end if;

      -- verifier que les details ne sont pas emarges
      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;

         if (ecd.ecd_reste_emarger < abs (ecd.ecd_montant)) then
            raise_application_error (-20001, 'L''ecriture ' || ecr.ecr_numero || ' ecr_ordre= ' || ecrordre || ' a ete emargee pour le compte ' || ecd.pco_num || '. Impossible d''annuler');
         end if;
      end loop;

      close c1;

--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--
--

      --             END LOOP;
--             CLOSE c1;
      newecrordre := api_plsql_journal.creerecriture (ecr.com_ordre, sysdate, str, ecr.exe_ordre, ecr.ori_ordre, ecr.tjo_ordre, ecr.top_ordre, ecr.utl_ordre);

      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;
         newecdordre := api_plsql_journal.creerecrituredetail (ecd.ecd_commentaire, ecd.ecd_libelle, -ecd.ecd_montant, ecd.ecd_secondaire, ecd.ecd_sens, newecrordre, ecd.ges_code, ecd.pco_num);
         x := creeremargementmemesens (ecd.ecd_ordre, newecdordre, 1);
      end loop;

      close c1;

      numerotationobject.numeroter_ecriture (newecrordre);
   end;

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer
   is
   begin
      return api_emargement.creeremargementmemesens (ecdordresource, ecdordredest, typeemargement);
   end;

--
--       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
--            emaordre EMARGEMENT.EMA_ORDRE%type;
--            ecdSource ecriture_detail%rowtype;
--            ecdDest ecriture_detail%rowtype;
--
--            utlOrdre ecriture.utl_ordre%type;
--            comOrdre ecriture.com_ordre%type;
--            exeOrdre ecriture.exe_ordre%type;
--            emaMontant emargement.ema_montant%type;
--            flag integer;
--       BEGIN

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
--            end if;

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
--            end if;

   --            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

   --            -- verifier que les ecriture_detail sont sur le meme sens
--            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;
--
--            -- verifier que les ecriture_detail ont le meme compte
--            if (ecdSource.pco_num <> ecdDest.pco_num) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les ecriture_detail ne sont pas emargees
--            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
--            end if;
--            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
--            end if;

   --            -- verifier que les montant s'annulent
--            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
--                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les exercices sont les memes
--            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
--                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- trouver le montant a emarger
--            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;

   --            -- trouver l'utilisateur
--            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;

   --            -- creation de l emargement
--             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

   --             INSERT INTO EMARGEMENT
--              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
--             VALUES
--              (
--              SYSDATE,
--              0,
--              EMAORDRE,
--              ecdSource.exe_ordre,
--              typeEmargement,
--              utlOrdre,
--              comordre,
--              emaMontant,
--              'VALIDE'
--              );

   --              -- creation de l emargement detail
--              INSERT INTO EMARGEMENT_DETAIL
--              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
--              VALUES
--              (
--              ecdSource.ecd_ordre,
--              ecdDest.ecd_ordre,
--              EMAORDRE,
--              emaMontant,
--              emargement_detail_seq.NEXTVAL,
--              exeOrdre
--              );

   --              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
--              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;

   --              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);

   --              return emaOrdre;
--       END;
--
--
   procedure annuler_emargement (emaordre integer)
   as
   begin
      api_emargement.annuler_emargement (emaordre);
   end;

   procedure supprimer_bordereau_dep (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de depense.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les mandat_brouillards
      delete from maracuja.mandat_brouillard
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- supprimer les depenses
      delete from maracuja.depense
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- mettre a jour les depense_ctrl_planco
      update jefy_depense.depense_ctrl_planco
         set man_id = null
       where man_id in (select man_id
                        from   maracuja.mandat
                        where  bor_id = borid);

      -- supprimer les mandats
      delete from maracuja.mandat
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   procedure supprimer_bordereau_btms (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau b, type_bordereau tb
      where  bor_id = borid and b.tbo_ordre = tb.tbo_ordre and tb.tbo_type = 'BTMS';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas ou n''est pas de type BTMS');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      update jefy_paye.jefy_paye_compta
         set jpc_etat = 'LIQUIDEE',
             bor_id = null
       where bor_id in (borid);

      --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
      supprimer_bordereau_dep (borid);
   end;

   procedure supprimer_bordereau_rec (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de recette.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les titre_brouillards
      delete from maracuja.titre_brouillard
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- supprimer les recettes
      delete from maracuja.recette
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- mettre a jour les recette_ctrl_planco
      update jefy_recette.recette_ctrl_planco
         set tit_id = null
       where tit_id in (select tit_id
                        from   maracuja.titre
                        where  bor_id = borid);

      -- supprimer les titres
      delete from maracuja.titre
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer)
   is
      flag             integer;
      lerecouvrement   recouvrement%rowtype;
      tmpemaordre      emargement.ema_ordre%type;
      tmpecrordre      ecriture.ecr_ordre%type;

      cursor emargements
      is
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_source in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))))
         union
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_destination in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))));

      cursor ecritures
      is
         select distinct e.ecr_ordre
         from            ecriture_detail ecd, ecriture e
         where           ecd.ecr_ordre = e.ecr_ordre and e.ecr_numero >= ecrnumeromin and e.ecr_numero <= ecrnumeromax and ecd_ordre in (select ecd_ordre
                                                                                                                                         from   titre_detail_ecriture
                                                                                                                                         where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                                                                                                           from   echeancier
                                                                                                                                                                                           where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                                                                                                            from   prelevement
                                                                                                                                                                                                                            where  reco_ordre = recoordre)));
   begin
      -- recuperer les numeros des ecritures min et max pour les passer en parametre
      --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
      select count (*)
      into   flag
      from   prelevement
      where  reco_ordre = recoordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun prelevement correspondant a recoordre= ' || recoordre);
      end if;

      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- verifier les dates des ecritures avec la date du prelevement
      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromin;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromax;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      -- supprimer les emargements
      open emargements;

      loop
         fetch emargements
         into  tmpemaordre;

         exit when emargements%notfound;
         annuler_emargement (tmpemaordre);
      end loop;

      close emargements;

      -- modifier les etats des prelevements
      update prelevement
         set prel_etat_maracuja = 'ATTENTE'
       where reco_ordre = recoordre;

      --supprimer le contenu du fichier (prelevement_fichier)
      delete from prelevement_fichier
            where reco_ordre = recoordre;

      -- supprimer les écritures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
      open ecritures;

      loop
         fetch ecritures
         into  tmpecrordre;

         exit when ecritures%notfound;
         creer_ecriture_annulation (tmpecrordre);
      end loop;

      close ecritures;

      delete from titre_detail_ecriture
            where ecd_ordre in (
                         select distinct ecd.ecd_ordre
                         from            ecriture_detail ecd, ecriture e
                         where           ecd.ecr_ordre = e.ecr_ordre
                         and             e.ecr_numero >= ecrnumeromin
                         and             e.ecr_numero <= ecrnumeromax
                         and             ecd_ordre in (select ecd_ordre
                                                       from   titre_detail_ecriture
                                                       where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                         from   echeancier
                                                                                                         where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                          from   prelevement
                                                                                                                                          where  reco_ordre = recoordre))));

      -- supprimer association des prélèvements au recouvrement
      update prelevement
         set reco_ordre = null
       where reco_ordre = recoordre;

      -- supprimer l'objet recouvrement
      delete from recouvrement
            where reco_ordre = recoordre;
--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))

   --        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--
   end;

   -- suppression d'un paiement de type virement avec annulation des émargements et écritures de paiements
   procedure supprimer_paiement_virement (paiordre integer)
   is
      flag       integer;
      ecdp       ecriture_detail%rowtype;
      ecdordre   ecriture_detail.ecd_ordre%type;
      emaordre   emargement.ema_ordre%type;
      ecrordre   ecriture.ecr_ordre%type;

      cursor ecdpaiement5
      is
         select ecd.*
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.man_id = mde.man_id
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    mde_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.*
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.odp_ordre = mde.odp_ordre
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    ope_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%';

      cursor ecdpaiementall
      is
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor ecrordres
      is
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor emaordreforecd
      is
         select emd.ema_ordre
         from   emargement_detail emd, emargement ema
         where  ema.ema_ordre = emd.ema_ordre and ema_etat = 'VALIDE' and ecd_ordre_source = ecdordre or ecd_ordre_destination = ecdordre;
   begin
      select count (*)
      into   flag
      from   maracuja.paiement
      where  pai_ordre = paiordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun paiement trouvé correspondant a pai_ordre= ' || paiordre);
      end if;

      --verifier que classe 5 non émargée
      open ecdpaiement5;

      loop
         fetch ecdpaiement5
         into  ecdp;

         exit when ecdpaiement5%notfound;

         if (ecdp.ecd_reste_emarger <> abs (ecdp.ecd_credit)) then
            raise_application_error (-20001, 'L''écriture ' || ecdp.pco_num || ' - ' || ecdp.ecd_libelle || ' a été émargée, supprimez l''émargement.');
         end if;
      end loop;

      close ecdpaiement5;

      -- supprimer tous les emargements
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         open emaordreforecd;

         loop
            fetch emaordreforecd
            into  emaordre;

            exit when emaordreforecd%notfound;
            api_emargement.annuler_emargement (emaordre);
         end loop;

         close emaordreforecd;
      end loop;

      close ecdpaiementall;

      --  creer ecritures annulation
      open ecrordres;

      loop
         fetch ecrordres
         into  ecrordre;

         exit when ecrordres%notfound;
         creer_ecriture_annulation (ecrordre);
      end loop;

      close ecrordres;

      -- supprimer les mde et ope
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre = ecdordre;

         delete from odpaiem_detail_ecriture
               where ecd_ordre = ecdordre;
      end loop;

      close ecdpaiementall;

      -- changer etats des mandats et ODP
      update mandat
         set man_etat = 'VISE',
             pai_ordre = null
       where pai_ordre = paiordre;

      update ordre_de_paiement
         set odp_etat = 'VALIDE',
             pai_ordre = null
       where pai_ordre = paiordre;

      -- supprimer le paiement
      delete from virement_fichier
            where pai_ordre = paiordre;

      delete from paiement
            where pai_ordre = paiordre;
   end;

   procedure corriger_etat_mandats
   is
   begin
      update maracuja.mandat
         set man_etat = 'VISE'
       where man_id in (select m.man_id
                        from   maracuja.mandat m, maracuja.mandat_detail_ecriture mde
                        where  m.man_id = mde.man_id and m.man_etat = 'ATTENTE' and mde.mde_origine = 'VISA');
   end;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type
   is
      flag     integer;
      pconum   plan_comptable_exer.pco_num%type;
   begin
      select count (*)
      into   flag
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' n''a pas été trouvé dans la table maracuja.parametre pour l''exercice ' || exeordre);
      end if;

      select par_value
      into   pconum
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      select count (*)
      into   flag
      from   plan_comptable_exer
      where  pco_num = pconum and exe_ordre = exeordre and pco_validite = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' contient la valeur ' || pconum || ' qui ne correspond pas a un compte valide du plan comptable de ' || exeordre);
      end if;

      return pconum;
   end;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   parametre
      where  exe_ordre = exeordre and par_key = parkey;

      if (flag = 0) then
         insert into parametre
                     (par_ordre,
                      exe_ordre,
                      par_description,
                      par_key,
                      par_value
                     )
         values      (parametre_seq.nextval,
                      exeordre,
                      pardescription,
                      parkey,
                      parvalue
                     );
      end if;
   end;

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat <> 'C';

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;
end;
/

--**************---
CREATE OR REPLACE PACKAGE MARACUJA.abricot_util
is
/*
 * Copyright Cocktail, 2001-2012
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
-- rodolphe.prin at cocktail.org
   function is_dpco_tva_collectee (dpcoid integer)
      return integer;

   function is_man_sur_sacd (manid integer)
      return integer;

   function get_dpco_taux_prorata (dpcoid integer)
      return number;

   function get_dpco_montant_budgetaire (dpcoid integer)
      return number;

   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number;

   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type;

   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type;

   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type;

   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type;

   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number;

   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number;

   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type;

   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type;

   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type;

   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type;

   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type;

   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type;

   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type);
end abricot_util;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.abricot_util
is
-- Methodes utilitaires pour la generation des mandats et titres
--
--//FIXME 06/01/2012 adapter les fonctions lorsque la respartition par taux de TVA sera active, pour l'instant le taux n'est pas exploité
--

   /* indique si la TVA doit etre collectee pour la depense (depend du mode de paiement affecte) res=0 sinon*/
   function is_dpco_tva_collectee (dpcoid integer)
      return integer
   as
      res            integer;
      modcode        mode_paiement.mod_code%type;
      pconumtvactp   mode_paiement.pco_num_tva_ctp%type;
   begin
      select count (*)
      into   res
      from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id=db.dep_id and dpco.dpco_id=dpcoid;

      if (res > 0) then
         select mod_code,
                pco_num_tva_ctp
         into   modcode,
                pconumtvactp
         from   jefy_depense.depense_papier dpp, maracuja.mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and mp.mod_paiement_ht = 'O' and dpco.dep_id=db.dep_id and dpco.dpco_id=dpcoid and pco_num_tva_ctp is not null;

         if (pconumtvactp is null) then
            raise_application_error (-20001, 'Le compte de TVA collectee n''est pas defini pour le mode de paiement ' || modcode || ' alors que le mode de paiement est défini comme "Paiement HT"');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* >0 si le mandat est passe sur un sacd */
   function is_man_sur_sacd (manid integer)
      return integer
   as
      res   integer;
   begin
      select count (*)
      into   res
      from   mandat m, gestion_exercice g
      where  m.ges_code = g.ges_code and m.exe_ordre = g.exe_ordre and g.pco_num_185 is not null and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* renvoie le taux de prorata affecte a la depense */
   function get_dpco_taux_prorata (dpcoid integer)
      return number
   as
      res   number;
   begin
      select tap_taux
      into   res
      from   jefy_admin.taux_prorata tp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.tap_id = tp.tap_id and dpco.dep_id = db.dep_id and dpco.dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie le montant budgetaire de la depense*/
   function get_dpco_montant_budgetaire (dpcoid integer)
      return number
   as
      res   number;
   begin
      select dpco_montant_budgetaire
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant de TVA de la facture pour le taux specifié */
   function get_dpco_montant_tva (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      -- //FIXME adapter ca lorsque la respartition par taux de TVA sera active
      select dpco_tva_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la part de TVA a deduire : TTC - budgetaire */
   function get_dpco_montant_tva_ded (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      --res := get_dep_montant_tva (depid, tauxtva) * get_dep_taux_prorata (depid) / 100;
      select (dpco_ttc_saisie - dpco_montant_budgetaire)
      into   res
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /*  Renvoie la part de TVA a collecter : Montant de la TVA si mode de paiement collecte la TVA (i.e. fournisseur soumis TVA intra)  */
   function get_dpco_montant_tva_coll (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
      cpt   plan_comptable_exer.pco_num%type;
   begin
      res := 0;

      if (is_dpco_tva_collectee (dpcoid) > 0) then
         res := get_dpco_montant_tva (dpcoid, tauxtva);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie la TVA a reverser : collectee - deduite  */
   function get_dpco_montant_tva_rev (dpcoid integer, tauxtva number)
      return number
   as
      res   number;
   begin
      res := get_dpco_montant_tva_coll (dpcoid, tauxtva) - get_dpco_montant_tva_ded (dpcoid, tauxtva);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_coll (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_dpco_compte_tva_ded (dpcoid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_papier dpp
      where  db.dpp_id = dpp.dpp_id and dpp.mod_ordre = mp.mod_ordre and db.dep_id = dpco.dep_id and dpco.dpco_id = dpcoid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   jefy_depense.depense_ctrl_planco
         where  dpco_id = dpcoid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant TTC pour un depense_ctrl_planco  */
   function get_dpco_montant_ttc (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      select dpco.dpco_ttc_saisie
      into   res
      from   jefy_depense.depense_ctrl_planco dpco
      where  dpco_id = dpcoid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le montant a payer pour un depense_ctrl_planco : TTC - TVA collectee */
   function get_dpco_montant_apayer (dpcoid integer)
      return mandat_brouillard.mab_montant%type
   as
      res   mandat_brouillard.mab_montant%type;
   begin
      res := get_dpco_montant_ttc (dpcoid) - get_dpco_montant_tva_coll (dpcoid, null);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le tbo_ordre du bordereau du mandat */
   function get_man_tboordre (manid integer)
      return type_bordereau.tbo_ordre%type
   as
      res   type_bordereau.tbo_ordre%type;
   begin
      select b.tbo_ordre
      into   res
      from   bordereau b, mandat m
      where  m.bor_id = b.bor_id and m.man_id = manid;

      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA de collecte affecte au mode de paiement du mandat s'il existe et est actif */
   function get_man_compte_tva_coll (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
   begin
      select mp.mod_ordre
      into   modordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      res := get_mp_compte_tva_coll (modordre);
      return res;
   end;

-----------------------------------------------------------------------------
   /* Renvoie le compte de TVA a deduire affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_tva_ded (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de tva affecte au mode de paiement
      res := get_mp_compte_tva_ded (modordre);

      -- si null, on recupere le compte de tva affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_tva_ded (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement de la depense s'il existe et est actif */
   function get_man_compte_ctp (manid integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      modordre   mode_paiement.mod_ordre%type;
      exeordre   mode_paiement.exe_ordre%type;
   begin
      select mp.mod_ordre,
             mp.exe_ordre
      into   modordre,
             exeordre
      from   mode_paiement mp, mandat m
      where  m.mod_ordre = mp.mod_ordre and m.man_id = manid;

      -- on recupere le compte de ctp affecte au mode de paiement
      res := get_mp_compte_ctp (modordre);

      -- si null, on recupere le compte de  ctp affecte par defaut comme contrepartie de l'imputation
      if (res is null) then
         select pco_num
         into   res
         from   mandat
         where  man_id = manid;

         res := get_ctp_compte_ctp (res, exeordre);
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
 /* Renvoie le code gestion a utiliser pour la contrepartie du mandat (la composante ou agence suivant les cas) */
   function get_man_gestion_ctp (manid integer)
      return gestion.ges_code%type
   as
      res                  gestion.ges_code%type;
      gescode_composante   gestion.ges_code%type;
      gescode_agence       gestion.ges_code%type;
      exeordre             mandat.exe_ordre%type;
      pconumordo           mandat.pco_num%type;
      typegestion          planco_visa.pvi_contrepartie_gestion%type;
      modordre             mandat.mod_ordre%type;
   begin
      -- si la ctp est definie dans le mandat on la prend, sinon on prend celle definie dans planco_visa
      -- si c'est un SACD la ctp va forcement sur le SACD
      select m.ges_code,
             m.exe_ordre,
             m.pco_num,
             c.ges_code,
             m.mod_ordre
      into   gescode_composante,
             exeordre,
             pconumordo,
             gescode_agence,
             modordre
      from   mandat m, gestion g, comptabilite c
      where  m.ges_code = g.ges_code and g.com_ordre = c.com_ordre and m.man_id = manid;

      if (is_man_sur_sacd (manid) > 0) then
         res := gescode_composante;
      else
         select mod_contrepartie_gestion
         into   typegestion
         from   mode_paiement mp
         where  mp.mod_ordre = modordre;

         if (typegestion is null) then
            select pvi_contrepartie_gestion
            into   typegestion
            from   planco_visa pv
            where  pv.pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;
         end if;

         -- si non specifie, on prend l'agence
         if (typegestion = 'COMPOSANTE') then
            res := gescode_composante;
         else
            res := gescode_agence;
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le montant de la TVA a collecter pour le mandat (depend du mode de paiement) */
   function get_man_montant_tva_coll (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_coll (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_tva_ded (manid integer, tauxtva number)
      return number
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      -- FIXME adapter ca en fonction du taux quand dispo
      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_tva_ded (dpco.dpco_id, tauxtva);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_apayer (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_apayer (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

   function get_man_montant_ttc (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_ttc (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
   function get_man_montant_budgetaire (manid integer)
      return mandat_brouillard.mab_montant%type
   as
      res    number;
      flag   integer;
      dpco   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dpcos
      is
         select *
         from   jefy_depense.depense_ctrl_planco
         where  man_id = manid;
   begin
      res := 0;

      open dpcos;

      loop
         fetch dpcos
         into  dpco;

         exit when dpcos%notfound;
         res := res + get_dpco_montant_budgetaire (dpco.dpco_id);
      end loop;

      close dpcos;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA de collecte affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_coll (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva_ctp,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie (VISA) affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_ctp (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_visa,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA collectee ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire affecte au mode de paiement s'il existe et est actif*/
   function get_mp_compte_tva_ded (modordre integer)
      return plan_comptable_exer.pco_num%type
   as
      res        plan_comptable_exer.pco_num%type;
      exeordre   mode_paiement.exe_ordre%type;
      modcode    mode_paiement.mod_code%type;
   begin
      select pco_num_tva,
             mp.mod_code,
             exe_ordre
      into   res,
             modcode,
             exeordre
      from   mode_paiement mp
      where  mp.mod_ordre = modordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini pour le mode de paiement ' || modcode || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de TVA a deduire associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_tva_ded (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res   plan_comptable_exer.pco_num%type;
   begin
      res := null;

      select pco_num_tva
      into   res
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de TVA a deduire ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
/* Renvoie le compte de contrepartie associe par defaut a un compte pour un exercice (recupere dans plancovisa) */
   function get_ctp_compte_ctp (pconumordo plan_comptable_exer.pco_num%type, exeordre plan_comptable_exer.exe_ordre%type)
      return plan_comptable_exer.pco_num%type
   as
      res   plan_comptable_exer.pco_num%type;
   begin
      res := null;

      select pco_num_ctrepartie
      into   res
      from   planco_visa
      where  pco_num_ordonnateur = pconumordo and exe_ordre = exeordre;

      if (res is not null) then
         if (api_planco.is_planco_valide (res, exeordre) = 0) then
            raise_application_error (-20001, 'Le compte de contrepartie ' || res || ' defini par defaut pour le compte ' || pconumordo || ' n''est pas actif sur l''exercice' || exeordre || '.');
         end if;
      end if;

      return res;
   end;

-----------------------------------------------------------------------------
   procedure creer_mandat_brouillard (exeordre integer, gescode gestion.ges_code%type, montant number, operation mandat_brouillard.mab_operation%type, sens mandat_brouillard.mab_sens%type, manid integer, pconum plan_comptable_exer.pco_num%type)
   is
   begin
      insert into mandat_brouillard
      values      (null,   --ECD_ORDRE,
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   montant,   --MAB_MONTANT,
                   operation,   --MAB_OPERATION,
                   mandat_brouillard_seq.nextval,   --MAB_ORDRE,
                   sens,   --MAB_SENS,
                   manid,
                   --MAN_ID,
                   pconum   --PCO_NU
                  );
   end;
end;
/

--**************---
CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT_SAV" AS

/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007
-- version du 01/10/2009 -- ajout de controles sur la generation des bd de PI

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- GES_CODE a prendre en compte en fonction du mandat.
FUNCTION  get_ges_code_for_man_id(manid NUMBER)
  RETURN comptabilite.ges_code%TYPE;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_NR1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;
function getFournisNom (fouOrdre integer) return varchar;


-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);
procedure ctrl_bordereaux_PI(borIdDep integer, borIdRec integer);


END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT_SAV" 
AS
  PROCEDURE creer_bordereau (abrid INTEGER)
  IS
     cpt            INTEGER;
     abrgroupby     abricot_bord_selection.abr_group_by%TYPE;
     monborid_dep   INTEGER;
     monborid_rec   INTEGER;
     flag integer;

     CURSOR lesmandats
     IS
        SELECT man_id
          FROM mandat
         WHERE bor_id = monborid_dep;

     CURSOR lestitres
     IS
        SELECT tit_id
          FROM titre
         WHERE bor_id = monborid_rec;

     tmpmandid      INTEGER;
     tmptitid       INTEGER;
     tboordre       INTEGER;
  BEGIN
-- est ce une selection vide ???
     SELECT COUNT (*)
       INTO cpt
       FROM abricot_bord_selection
      WHERE abr_id = abrid;

     IF cpt != 0
     THEN
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

        -- verifier l etat de l exercice
        select count(*) into flag from jefy_admin.exercice where exe_ordre=recup_exeordre (abrid) and exe_stat in ('O', 'R');
        if (flag = 0) then
            raise_application_error
                 (-20001,
                  'L''exercice ' || recup_exeordre (abrid) || ' n''est pas ouvert.'
                 );
        end if;


        -- recup du group by pour traiter les cursors
        abrgroupby := recup_groupby (abrid);

        IF (abrgroupby = 'bordereau_1R1T')
        THEN
           monborid_rec :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_1r1t (abrid, monborid_rec);
        END IF;

        IF (abrgroupby = 'bordereau_NR1T')
        THEN
           monborid_rec :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_nr1t (abrid, monborid_rec);

-- controle RA
           SELECT COUNT (*)
             INTO cpt
             FROM titre
            WHERE ori_ordre IS NOT NULL AND bor_id = monborid_rec;

           IF cpt != 0
           THEN
              raise_application_error
                 (-20001,
                  'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !'
                 );
           END IF;
        END IF;

        IF (abrgroupby = 'bordereau_1D1M')
        THEN
           monborid_dep :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_1d1m (abrid, monborid_dep);
        END IF;

/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
monborid_dep := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

monborid_rec := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

bordereau_1D1M(abrid,monborid_dep);
bordereau_1R1T(abrid,monborid_rec);


END IF;
*/
        IF (abrgroupby NOT IN
               ('bordereau_1R1T',
                'bordereau_NR1T',
                'bordereau_1D1M',
                'bordereau_1D1M1R1T'
               )
           )
        THEN
           monborid_dep :=
              get_num_borid (recup_tboordre (abrid),
                             recup_exeordre (abrid),
                             recup_gescode (abrid),
                             recup_utlordre (abrid)
                            );
           bordereau_nd1m (abrid, monborid_dep);
        END IF;

        IF (monborid_dep IS NOT NULL)
        THEN
           bordereau_abricot.numeroter_bordereau (monborid_dep);

           OPEN lesmandats;

           LOOP
              FETCH lesmandats
               INTO tmpmandid;

              EXIT WHEN lesmandats%NOTFOUND;
              get_depense_jefy_depense (tmpmandid);
           END LOOP;

           CLOSE lesmandats;

           controle_bordereau (monborid_dep);
        END IF;

        IF (monborid_rec IS NOT NULL)
        THEN
           bordereau_abricot.numeroter_bordereau (monborid_rec);

           OPEN lestitres;

           LOOP
              FETCH lestitres
               INTO tmptitid;

              EXIT WHEN lestitres%NOTFOUND;
              -- recup du brouillard
              get_recette_jefy_recette (tmptitid);
              set_titre_brouillard (tmptitid);
              get_recette_prelevements (tmptitid);
           END LOOP;

           CLOSE lestitres;

           controle_bordereau (monborid_rec);
        END IF;

-- maj de l etat dans la selection
        IF (monborid_dep IS NOT NULL OR monborid_rec IS NOT NULL)
        THEN
           IF monborid_rec IS NOT NULL
           THEN
              UPDATE abricot_bord_selection
                 SET abr_etat = 'TRAITE',
                     bor_id = monborid_rec
               WHERE abr_id = abrid;
           END IF;

           IF monborid_dep IS NOT NULL
           THEN
              UPDATE abricot_bord_selection
                 SET abr_etat = 'TRAITE',
                     bor_id = monborid_dep
               WHERE abr_id = abrid;

              SELECT tbo_ordre
                INTO tboordre
                FROM bordereau
               WHERE bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
              IF tboordre = 3
              THEN
                 bordereau_abricot_paye.basculer_bouillard_paye
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
              IF tboordre = 18
              THEN
                 bordereau_abricot_paye.basculer_bouillard_paye_orv
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
              IF tboordre = 19
              THEN
                 bordereau_abricot_paye.basculer_bouillard_paye_regul
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
              IF tboordre = 22
              THEN
                 bordereau_abricot_paf.basculer_bouillard_paye_regul
                                                               (monborid_dep);
              END IF;

-- pour les bordereaux de PAF on retravaille le brouillard
              IF tboordre = 20
              THEN
                 bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
              END IF;
-- pour les bordereaux d'orv de PAF on retravaille le brouillard
 if tboordre = 21  then
  bordereau_abricot_paf.basculer_bouillard_paye_orv(monborid_dep);
  end if;

           -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
           END IF;
        END IF;
     END IF;
  END;

  PROCEDURE viser_bordereau_rejet (brjordre INTEGER)
  IS
     cpt              INTEGER;
     flag               integer;
     manid            maracuja.mandat.man_id%TYPE;
     titid            maracuja.titre.tit_id%TYPE;
     tboordre         INTEGER;
     reduction        INTEGER;
     utlordre         INTEGER;
     dpcoid           INTEGER;
     recid            INTEGER;
     depsuppression   VARCHAR2 (20);
     rpcoid           INTEGER;
     recsuppression   VARCHAR2 (20);
     exeordre         INTEGER;
     depid             integer;
     boridInitial    integer;

     CURSOR mandats
     IS
        SELECT man_id
          FROM maracuja.mandat
         WHERE brj_ordre = brjordre;

     CURSOR depenses
     IS
        SELECT dep_ordre, dep_suppression
          FROM maracuja.depense
         WHERE man_id = manid;

     CURSOR titres
     IS
        SELECT tit_id
          FROM maracuja.titre
         WHERE brj_ordre = brjordre;

     CURSOR recettes
     IS
        SELECT rec_ordre, rec_suppression
          FROM maracuja.recette
         WHERE tit_id = titid;

     deliq            INTEGER;
  BEGIN
  
    -- verifier si le bordereau est deja vise
    select count(*) into flag from  bordereau_rejet where brj_etat = 'VISE' and brj_ordre = brjordre;
    if (flag > 0) then
        raise_application_error(-20001,'Ce bordereau a déjà été visé');
    end if;
  
  
     OPEN mandats;

     LOOP
        FETCH mandats
         INTO manid;

        EXIT WHEN mandats%NOTFOUND;

        select bor_id into boridInitial from mandat where man_id = manid;
        -- memoriser le bor-id du mandat


        OPEN depenses;

        LOOP
           FETCH depenses
            INTO dpcoid, depsuppression;

           EXIT WHEN depenses%NOTFOUND;
           -- casser le liens des mand_id dans depense_ctrl_planco
             -- supprimer le liens compteble <-> depense dans l inventaire
           jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, NULL);

           SELECT tbo_ordre, exe_ordre
             INTO tboordre, exeordre
             FROM jefy_depense.depense_ctrl_planco
            WHERE dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
           IF depsuppression = 'OUI' AND tboordre != 201
           THEN
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
 deliq:=jefy_depense.Get_Fonction('DELIQ');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
 uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

   if utlordre is null then
   deliq:=jefy_depense.Get_Fonction('DELIQINV');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre
 AND ufe.exe_ordre=exeordre
 AND uf.fon_ordre=deliq
 AND ufe.exe_ordre=e.exe_ordre
 AND exe_stat_eng='R';
 end if;
*/
              SELECT utl_ordre
                INTO utlordre
                FROM jefy_depense.depense_budget
               WHERE dep_id IN (SELECT dep_id
                                  FROM jefy_depense.depense_ctrl_planco
                                 WHERE dpco_id = dpcoid);

             select dep_id into depid from jefy_depense.depense_ctrl_planco where dpco_id=dpcoid;
              -- si cest le rejet d'un bordereau de paye
              if (tboordre=18) then
                  jefy_paye. paye_reversement.viser_rejet_reversement(depid);
              end if;
              jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
           END IF;
        END LOOP;

        CLOSE depenses;
     END LOOP;

     CLOSE mandats;

     -- pour les bordereau de PAF, appeler la proc
     jefy_paf.paf_budget.viser_rejet_paf(boridInitial);
     jefy_paye.paye_budget.viser_rejet_papaye(boridInitial);






     OPEN titres;

     LOOP
        FETCH titres
         INTO titid;

        EXIT WHEN titres%NOTFOUND;

        OPEN recettes;

        LOOP
           FETCH recettes
            INTO rpcoid, recsuppression;

           EXIT WHEN recettes%NOTFOUND;

-- casser le liens des tit_id dans recette_ctrl_planco
           SELECT r.rec_id_reduction
             INTO reduction
             FROM jefy_recette.recette r,
                  jefy_recette.recette_ctrl_planco rpco
            WHERE rpco.rpco_id = rpcoid AND rpco.rec_id = r.rec_id;

           IF reduction IS NOT NULL
           THEN
              jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, NULL);
           ELSE
              jefy_recette.api.upd_recette_ctrl_planco (rpcoid, NULL);
           END IF;

           SELECT tbo_ordre, exe_ordre
             INTO tboordre, exeordre
             FROM jefy_recette.recette_ctrl_planco
            WHERE rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
           IF recsuppression = 'OUI' AND tboordre != 200
           THEN
              SELECT utl_ordre
                INTO utlordre
                FROM jefy_recette.recette_budget
               WHERE rec_id IN (SELECT rec_id
                                  FROM jefy_recette.recette_ctrl_planco
                                 WHERE rpco_id = rpcoid);

              SELECT rec_id
                INTO recid
                FROM jefy_recette.recette_ctrl_planco
               WHERE rpco_id = rpcoid;
          IF reduction IS NOT NULL
           THEN
              jefy_recette.api.del_reduction (recid, utlordre);
           ELSE
              jefy_recette.api.del_recette (recid, utlordre);
           END IF;
                        END IF;
        END LOOP;

        CLOSE recettes;
     END LOOP;

     CLOSE titres;

-- on passe le brjordre a VISE
     UPDATE bordereau_rejet
        SET brj_etat = 'VISE'
      WHERE brj_ordre = brjordre;
  END;

  FUNCTION get_selection_id (info VARCHAR)
     RETURN INTEGER
  IS
     selection   INTEGER;
  BEGIN
     SELECT maracuja.abricot_bord_selection_seq.NEXTVAL
       INTO selection
       FROM DUAL;

     RETURN selection;
  END;

  FUNCTION get_selection_borid (abrid INTEGER)
     RETURN INTEGER
  IS
     borid   INTEGER;
  BEGIN
     SELECT DISTINCT bor_id
                INTO borid
                FROM maracuja.abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN borid;
  END;

  PROCEDURE set_selection_id (
     a01abrid        INTEGER,
     a02lesdepid     VARCHAR,
     a03lesrecid     VARCHAR,
     a04utlordre     INTEGER,
     a05exeordre     INTEGER,
     a06tboordre     INTEGER,
     a07abrgroupby   VARCHAR,
     a08gescode      VARCHAR
  )
  IS
     chaine     VARCHAR (32000);
     premier    INTEGER;
     tmpdepid   INTEGER;
     tmprecid   INTEGER;
     cpt        INTEGER;
  BEGIN
/*
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

     -- traitement de la chaine des depid
     IF a02lesdepid IS NOT NULL OR LENGTH (a02lesdepid) > 0
     THEN
        chaine := a02lesdepid;

        LOOP
           premier := 1;

           -- On recupere le depordre
           LOOP
              IF SUBSTR (chaine, premier, 1) = '$'
              THEN
                 tmpdepid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                 --   IF premier=1 THEN depordre := NULL; END IF;
                 EXIT;
              ELSE
                 premier := premier + 1;
              END IF;
           END LOOP;

           INSERT INTO maracuja.abricot_bord_selection
                       (abr_id, utl_ordre, dep_id, rec_id, exe_ordre,
                        tbo_ordre, abr_etat, abr_group_by,
                        ges_code
                       )
                VALUES (a01abrid,                                    --ABR_ID
                                 a04utlordre,                     --ult_ordre
                                             tmpdepid,               --DEP_ID
                                                      NULL,          --REC_ID
                                                           a05exeordre,
                                                                  --EXE_ORDRE
                        a06tboordre,                             --TBO_ORDRE,
                                    'ATTENTE',                    --ABR_ETAT,
                                              a07abrgroupby,
                                                     --,ABR_GROUP_BY,GES_CODE
                        a08gescode                                 --ges_code
                       );

--RECHERCHE DU CARACTERE SENTINELLE
           IF SUBSTR (chaine, premier + 1, 1) = '$'
           THEN
              EXIT;
           END IF;

           chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
        END LOOP;
     END IF;

     -- traitement de la chaine des recid
     IF a03lesrecid IS NOT NULL OR LENGTH (a03lesrecid) > 0
     THEN
        chaine := a03lesrecid;

        LOOP
           premier := 1;

           -- On recupere le depordre
           LOOP
              IF SUBSTR (chaine, premier, 1) = '$'
              THEN
                 tmprecid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                 --   IF premier=1 THEN depordre := NULL; END IF;
                 EXIT;
              ELSE
                 premier := premier + 1;
              END IF;
           END LOOP;

           INSERT INTO maracuja.abricot_bord_selection
                       (abr_id, utl_ordre, dep_id, rec_id, exe_ordre,
                        tbo_ordre, abr_etat, abr_group_by,
                        ges_code
                       )
                VALUES (a01abrid,                                    --ABR_ID
                                 a04utlordre,                     --ult_ordre
                                             NULL,                   --DEP_ID
                                                  tmprecid,          --REC_ID
                                                           a05exeordre,
                                                                  --EXE_ORDRE
                        a06tboordre,                             --TBO_ORDRE,
                                    'ATTENTE',                    --ABR_ETAT,
                                              a07abrgroupby,
                                                     --,ABR_GROUP_BY,GES_CODE
                        a08gescode                                 --ges_code
                       );

--RECHERCHE DU CARACTERE SENTINELLE
           IF SUBSTR (chaine, premier + 1, 1) = '$'
           THEN
              EXIT;
           END IF;

           chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
        END LOOP;
     END IF;

     SELECT COUNT (*)
       INTO cpt
       FROM jefy_depense.depense_ctrl_planco
      WHERE dpco_id IN (SELECT dep_id
                          FROM abricot_bord_selection
                         WHERE abr_id = a01abrid) AND man_id IS NOT NULL;

     IF cpt > 0
     THEN
        raise_application_error
               (-20001,
                'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !'
               );
     END IF;

     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.recette_ctrl_planco
      WHERE rpco_id IN (SELECT rec_id
                          FROM abricot_bord_selection
                         WHERE abr_id = a01abrid) AND tit_id IS NOT NULL;

     IF cpt > 0
     THEN
        raise_application_error
                (-20001,
                 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !'
                );
     END IF;

     bordereau_abricot.creer_bordereau (a01abrid);
  END;

  PROCEDURE set_selection_intern (
     a01abrid           INTEGER,
     a02lesdepid        VARCHAR,
     a03lesrecid        VARCHAR,
     a04utlordre        INTEGER,
     a05exeordre        INTEGER,
     a07abrgroupby      VARCHAR,
     a08gescodemandat   VARCHAR,
     a09gescodetitre    VARCHAR
  )
  IS
   borIdDep bordereau.bor_id%type;
   borIdRec bordereau.bor_id%type;
   flag integer;
    BEGIN
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

     -- les mandats
     set_selection_id (a01abrid,
                       a02lesdepid,
                       NULL,
                       a04utlordre,
                       a05exeordre,
                       201,
                       'bordereau_1D1M',
                       a08gescodemandat
                      );
-- les titres
     set_selection_id (-a01abrid,
                       NULL,
                       a03lesrecid,
                       a04utlordre,
                       a05exeordre,
                       200,
                       'bordereau_1R1T',
                       a09gescodetitre
                      );
                          -- verifier que les bordereaux crees sont coherents entre eux                     
                          select count(*) into flag from (  select distinct bor_id from abricot_bord_selection where abr_id=a01abrid );
                     if (flag<>1) then 
                        raise_application_error (-20001,'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id='||a01abrid);  
                     end if;
     select max(bor_id) into borIdDep from abricot_bord_selection where abr_id=a01abrid;
          select count(*) into flag from (                       
          select distinct bor_id from abricot_bord_selection where abr_id=-a01abrid
     );
     if (flag<>1) then raise_application_error (-20001,'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id='||-a01abrid);  end if;
     select max(bor_id) into borIdRec from abricot_bord_selection where abr_id=-a01abrid;                           
     
     -- verifier qu'on a 1 prest_id par titre/mandat
     select count(*) into flag from (select prest_id, count(distinct tit_id) nb from titre where bor_id=borIdRec group by prest_id) where nb>1;
     if (flag >0) then
        raise_application_error (-20001,'Plusieurs titres concernant la mÃªme prestation ne peuvent Ãªtre intÃ©grÃ©s sur un seul bordereau. CrÃ©ez plusieurs bordereaux.');
     end if;
     
     select count(*) into flag from (select prest_id, count(distinct man_id) nb from mandat where bor_id=borIdDep group by prest_id) where nb>1;
     if (flag >0) then
        raise_application_error (-20001,'Plusieurs mandats concernant la mÃªme prestation ne peuvent Ãªtre intÃ©grÃ©s sur un seul bordereau. CrÃ©ez plusieurs bordereaux.');
     end if;     
     ctrl_bordereaux_PI(borIdDep, borIdRec);                                                                                 
   END;

  PROCEDURE set_selection_paye (
     a01abrid           INTEGER,
     a02lesdepid        VARCHAR,
     a03lesrecid        VARCHAR,
     a04utlordre        INTEGER,
     a05exeordre        INTEGER,
     a07abrgroupby      VARCHAR,
     a08gescodemandat   VARCHAR,
     a09gescodetitre    VARCHAR
  )
  IS
     boridtmp    INTEGER;
     moisordre   INTEGER;
  BEGIN
/*
-- a07abrgroupby = mois
select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

-- CONTROLES
-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

if (cpt = 1) then
 raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
end if;
*/
-- ATENTION
-- tboordre : 3 salaires

     -- les mandats de papaye
     set_selection_id (a01abrid,
                       a02lesdepid,
                       NULL,
                       a04utlordre,
                       a05exeordre,
                       3,
                       'bordereau_1D1M',
                       a08gescodemandat
                      );
     boridtmp := get_selection_borid (a01abrid);
/*
-- maj de l etat de papaye_compta et du bor_ordre -
update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

  -- bascule du brouillard de papaye

  -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
  END;

-- creer bordereau (tbo_ordre) + numerotation
  FUNCTION get_num_borid (
     tboordre   INTEGER,
     exeordre   INTEGER,
     gescode    VARCHAR,
     utlordre   INTEGER
  )
     RETURN INTEGER
  IS
     cpt      INTEGER;
     borid    INTEGER;
     bornum   INTEGER;
  BEGIN
-- creation du bor_id --
     SELECT bordereau_seq.NEXTVAL
       INTO borid
       FROM DUAL;

-- creation du bordereau --
     bornum := -1;

     INSERT INTO bordereau
                 (bor_date_visa, bor_etat, bor_id, bor_num, bor_ordre,
                  exe_ordre, ges_code, tbo_ordre, utl_ordre, utl_ordre_visa,
                  bor_date_creation
                 )
          VALUES (NULL,                                      --BOR_DATE_VISA,
                       'VALIDE',                                  --BOR_ETAT,
                                borid,                              --BOR_ID,
                                      bornum,                      --BOR_NUM,
                                             -borid,             --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                  exeordre,                                      --EXE_ORDRE,
                           gescode,                               --GES_CODE,
                                   tboordre,                     --TBO_ORDRE,
                                            utlordre,            --UTL_ORDRE,
                                                     NULL,   --UTL_ORDRE_VISA
                  SYSDATE
                 );

     RETURN borid;
  END;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
  PROCEDURE bordereau_1r1t (abrid INTEGER, monborid INTEGER)
  IS
     cpt          INTEGER;
     tmprecette   jefy_recette.recette_ctrl_planco%ROWTYPE;

     CURSOR rec_tit
     IS
        SELECT   r.*
            FROM abricot_bord_selection ab,
                 jefy_recette.recette_ctrl_planco r
           WHERE r.rpco_id = ab.rec_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
        ORDER BY r.pco_num, r.rec_id ASC;
  BEGIN
     OPEN rec_tit;

     LOOP
        FETCH rec_tit
         INTO tmprecette;

        EXIT WHEN rec_tit%NOTFOUND;
        cpt := set_titre_recette (tmprecette.rpco_id, monborid);
     END LOOP;

     CLOSE rec_tit;
  END;

  PROCEDURE bordereau_nr1t (abrid INTEGER, monborid INTEGER)
  IS
     ht           NUMBER (12, 2);
     tva          NUMBER (12, 2);
     ttc          NUMBER (12, 2);
     pconumero    VARCHAR (20);
     nbpieces     INTEGER;
     cpt          INTEGER;
     titidtemp    INTEGER;
     tmprecette   jefy_recette.recette_ctrl_planco%ROWTYPE;

-- curseur de regroupement
     CURSOR rec_tit_group_by
     IS
        SELECT   r.pco_num, SUM (r.rpco_ht_saisie), SUM (r.rpco_tva_saisie),
                 SUM (r.rpco_ttc_saisie)
            FROM abricot_bord_selection ab,
                 jefy_recette.recette_ctrl_planco r
           WHERE r.rpco_id = ab.rec_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
        GROUP BY r.pco_num
        ORDER BY r.pco_num ASC;

     CURSOR rec_tit
     IS
        SELECT   r.*
            FROM abricot_bord_selection ab,
                 jefy_recette.recette_ctrl_planco r
           WHERE r.rpco_id = ab.rec_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
             AND r.pco_num = pconumero
        ORDER BY r.pco_num ASC, r.rec_id;
  BEGIN
     OPEN rec_tit_group_by;

     LOOP
        FETCH rec_tit_group_by
         INTO pconumero, ht, tva, ttc;

        EXIT WHEN rec_tit_group_by%NOTFOUND;
        titidtemp := 0;

        OPEN rec_tit;

        LOOP
           FETCH rec_tit
            INTO tmprecette;

           EXIT WHEN rec_tit%NOTFOUND;

           IF titidtemp = 0
           THEN
              titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
           ELSE
              UPDATE jefy_recette.recette_ctrl_planco
                 SET tit_id = titidtemp
               WHERE rpco_id = tmprecette.rpco_id;
           END IF;
        END LOOP;

        CLOSE rec_tit;

-- recup du nombre de pieces
-- TODO
        nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

        -- maj des montants du titre
        UPDATE titre
           SET tit_ht = ht,
               tit_nb_piece = nbpieces,
               tit_ttc = ttc,
               tit_tva = tva,
               tit_libelle = 'TITRE COLLECTIF'
         WHERE tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
     END LOOP;

     CLOSE rec_tit_group_by;
  END;

  PROCEDURE bordereau_nd1m (abrid INTEGER, monborid INTEGER)
  IS
     cpt          INTEGER;
     tmpdepense   jefy_depense.depense_ctrl_planco%ROWTYPE;
     abrgroupby   abricot_bord_selection.abr_group_by%TYPE;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
     CURSOR mand_dep_convra
     IS
        SELECT DISTINCT d.*
                   FROM abricot_bord_selection ab,
                        jefy_depense.depense_ctrl_planco d,
                        jefy_depense.depense_budget db,
                        jefy_depense.engage_budget e,
                        maracuja.v_convention_limitative c
                  WHERE d.dpco_id = ab.dep_id
                    AND abr_id = abrid
                    AND db.dep_id = d.dep_id
                    AND e.eng_id = db.eng_id
                    AND e.org_id = c.org_id(+)
                    AND e.exe_ordre = c.exe_ordre(+)
                    AND c.org_id IS NOT NULL
                    AND d.man_id IS NULL
                    AND ab.abr_etat = 'ATTENTE'
               ORDER BY d.pco_num ASC, d.DEP_ID;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
  BEGIN
     OPEN mand_dep_convra;

     LOOP
        FETCH mand_dep_convra
         INTO tmpdepense;

        EXIT WHEN mand_dep_convra%NOTFOUND;
        cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
     END LOOP;

     CLOSE mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
     SELECT DISTINCT abr_group_by
                INTO abrgroupby
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
     IF (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod')
     THEN
        cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
     END IF;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
     IF (abrgroupby = 'ndep_mand_fou_rib_pco_mod')
     THEN
        cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
     END IF;
  END;

  PROCEDURE bordereau_1d1m (abrid INTEGER, monborid INTEGER)
  IS
     cpt          INTEGER;
     tmpdepense   jefy_depense.depense_ctrl_planco%ROWTYPE;

     CURSOR dep_mand
     IS
        SELECT   d.*
            FROM abricot_bord_selection ab,
                 jefy_depense.depense_ctrl_planco d
           WHERE d.dpco_id = ab.dep_id
             AND abr_id = abrid
             AND ab.abr_etat = 'ATTENTE'
        ORDER BY d.pco_num ASC, d.DEP_ID;
  BEGIN
     OPEN dep_mand;

     LOOP
        FETCH dep_mand
         INTO tmpdepense;

        EXIT WHEN dep_mand%NOTFOUND;
        cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
     END LOOP;

     CLOSE dep_mand;
  END;

  PROCEDURE bordereau_1d1m1r1t (
     abrid      INTEGER,
     boridep    INTEGER,
     boridrec   INTEGER
  )
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     bordereau_1d1m (abrid, boridep);
     bordereau_1r1t (abrid, boridrec);
         ctrl_bordereaux_PI(boridep, boridrec);
          END;

-- les mandats et titres
  FUNCTION set_mandat_depense (dpcoid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
     cpt               INTEGER;
     flag              INTEGER;
     ladepense         jefy_depense.depense_ctrl_planco%ROWTYPE;
     ladepensepapier   jefy_depense.depense_papier%ROWTYPE;
     leengagebudget    jefy_depense.engage_budget%ROWTYPE;
     gescode           gestion.ges_code%TYPE;
     manid             mandat.man_id%TYPE;
     manorgine_key     mandat.man_orgine_key%TYPE;
     manorigine_lib    mandat.man_origine_lib%TYPE;
     oriordre          mandat.ori_ordre%TYPE;
     prestid           mandat.prest_id%TYPE;
     torordre          mandat.tor_ordre%TYPE;
     virordre          mandat.pai_ordre%TYPE;
     mannumero         mandat.man_numero%TYPE;
  BEGIN
-- recuperation du ges_code --
     SELECT ges_code
       INTO gescode
       FROM bordereau
      WHERE bor_id = borid;

     SELECT *
       INTO ladepense
       FROM jefy_depense.depense_ctrl_planco
      WHERE dpco_id = dpcoid;

     SELECT DISTINCT dpp.*
                INTO ladepensepapier
                FROM jefy_depense.depense_papier dpp,
                     jefy_depense.depense_budget db,
                     jefy_depense.depense_ctrl_planco dpco
               WHERE db.dep_id = dpco.dep_id
                 AND dpp.dpp_id = db.dpp_id
                 AND dpco_id = dpcoid;

     SELECT eb.*
       INTO leengagebudget
       FROM jefy_depense.engage_budget eb,
            jefy_depense.depense_budget db,
            jefy_depense.depense_ctrl_planco dpco
      WHERE db.eng_id = eb.eng_id
        AND db.dep_id = dpco.dep_id
        AND dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
     SELECT COUNT (*)
       INTO flag
       FROM maracuja.v_organ_exer
      WHERE org_id = leengagebudget.org_id
        AND exe_ordre = leengagebudget.exe_ordre;

     IF (flag = 0)
     THEN
        raise_application_error
                    (-20001,
                        'La ligne budgetaire affectee a l''engagement num. '
                     || leengagebudget.eng_numero
                     || ' n''est pas ouverte sur '
                     || leengagebudget.exe_ordre
                     || '.'
                    );
     END IF;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
     manorgine_key := NULL;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
     manorigine_lib := NULL;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
     oriordre :=
        gestionorigine.traiter_orgid (leengagebudget.org_id,
                                      leengagebudget.exe_ordre
                                     );

--PRESTID : PRESTATION INTERNE --
     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      WHERE d.pef_id = e.pef_id AND d.dep_id = ladepense.dep_id;

     IF cpt = 1
     THEN
        SELECT prest_id
          INTO prestid
          FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         WHERE d.pef_id = e.pef_id AND d.dep_id = ladepense.dep_id;
     ELSE
        prestid := NULL;
     END IF;

--TORORDRE : ORIGINE DU MANDAT --
     torordre := 1;
--VIRORDRE --
     virordre := NULL;

-- creation du man_id --
     SELECT mandat_seq.NEXTVAL
       INTO manid
       FROM DUAL;

-- recup du numero de mandat
     mannumero := -1;

     INSERT INTO mandat
                 (bor_id, brj_ordre, exe_ordre,
                  fou_ordre, ges_code, man_date_remise, man_date_visa_princ,
                  man_etat, man_etat_remise, man_ht,
                  man_id, man_motif_rejet, man_nb_piece, man_numero,
                  man_numero_rejet, man_ordre, man_orgine_key,
                  man_origine_lib, man_ttc,
                  man_tva,
                  mod_ordre, ori_ordre, pco_num,
                  prest_id, tor_ordre, pai_ordre, org_ordre,
                  rib_ordre_ordonnateur,
                  rib_ordre_comptable
                 )
          VALUES (borid,                                            --BOR_ID,
                        NULL,                                    --BRJ_ORDRE,
                             ladepensepapier.exe_ordre,          --EXE_ORDRE,
                  ladepensepapier.fou_ordre,                     --FOU_ORDRE,
                                            gescode,              --GES_CODE,
                                                    NULL,  --MAN_DATE_REMISE,
                                                         NULL,
                                                       --MAN_DATE_VISA_PRINC,
                  'ATTENTE',                                      --MAN_ETAT,
                            'ATTENTE',                     --MAN_ETAT_REMISE,
                                      ladepense.dpco_montant_budgetaire,
                                                                    --MAN_HT,
                  manid,                                            --MAN_ID,
                        NULL,                              --MAN_MOTIF_REJET,
                             ladepensepapier.dpp_nb_piece,    --MAN_NB_PIECE,
                                                          mannumero,
                                                                --MAN_NUMERO,
                  NULL,                                   --MAN_NUMERO_REJET,
                       -manid,                                   --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                              manorgine_key,                --MAN_ORGINE_KEY,
                  manorigine_lib,                          --MAN_ORIGINE_LIB,
                                 ladepense.dpco_ttc_saisie,        --MAN_TTC,
                    ladepense.dpco_ttc_saisie
                  - ladepense.dpco_montant_budgetaire,             --MAN_TVA,
                  ladepensepapier.mod_ordre,                     --MOD_ORDRE,
                                            oriordre,            --ORI_ORDRE,
                                                     ladepense.pco_num,
                                                                   --PCO_NUM,
                  prestid,                                        --PREST_ID,
                          torordre,                              --TOR_ORDRE,
                                   virordre,                      --VIR_ORDRE
                                            leengagebudget.org_id,
                                                                  --org_ordre
                  ladepensepapier.rib_ordre,                       --rib ordo
                  ladepensepapier.rib_ordre                  -- rib_comptable
                 );

-- maj du man_id  dans la depense
     UPDATE jefy_depense.depense_ctrl_planco
        SET man_id = manid
      WHERE dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

     -- recup du brouillard
     set_mandat_brouillard (manid);
     RETURN manid;
  END;

-- lesdepid XX$FF$....$DDD$ZZZ$$
  FUNCTION set_mandat_depenses (lesdpcoid VARCHAR, borid INTEGER)
     RETURN INTEGER
  IS
     cpt             INTEGER;
     premier         INTEGER;
     tmpdpcoid       INTEGER;
     chaine          VARCHAR (5000);
     premierdpcoid   INTEGER;
     manid           INTEGER;
     ttc             mandat.man_ttc%TYPE;
     tva             mandat.man_tva%TYPE;
     ht              mandat.man_ht%TYPE;
     utlordre        INTEGER;
     nb_pieces       INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
     premierdpcoid := NULL;

     -- traitement de la chaine des depid xx$xx$xx$.....$x$$
     IF lesdpcoid IS NOT NULL OR LENGTH (lesdpcoid) > 0
     THEN
        chaine := lesdpcoid;

        LOOP
           premier := 1;

           -- On recupere le depid
           LOOP
              IF SUBSTR (chaine, premier, 1) = '$'
              THEN
                 tmpdpcoid := en_nombre (SUBSTR (chaine, 1, premier - 1));
                 --   IF premier=1 THEN depordre := NULL; END IF;
                 EXIT;
              ELSE
                 premier := premier + 1;
              END IF;
           END LOOP;

-- creation du mandat lie au borid
           IF premierdpcoid IS NULL
           THEN
              manid := set_mandat_depense (tmpdpcoid, borid);

              -- suppression du brouillard car il est uniquement sur la premiere depense
              DELETE FROM mandat_brouillard
                    WHERE man_id = manid;

              premierdpcoid := tmpdpcoid;
           ELSE
              -- maj du man_id  dans la depense
              UPDATE jefy_depense.depense_ctrl_planco
                 SET man_id = manid
               WHERE dpco_id = tmpdpcoid;

              -- recup de la depense (maracuja)
              SELECT DISTINCT dpp.utl_ordre
                         INTO utlordre
                         FROM jefy_depense.depense_papier dpp,
                              jefy_depense.depense_budget db,
                              jefy_depense.depense_ctrl_planco dpco
                        WHERE db.dep_id = dpco.dep_id
                          AND dpp.dpp_id = db.dpp_id
                          AND dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
           END IF;

--RECHERCHE DU CARACTERE SENTINELLE
           IF SUBSTR (chaine, premier + 1, 1) = '$'
           THEN
              EXIT;
           END IF;

           chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
        END LOOP;
     END IF;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
     SELECT SUM (dpco_ttc_saisie),
            SUM (dpco_ttc_saisie - dpco_montant_budgetaire),
            SUM (dpco_montant_budgetaire)
       INTO ttc,
            tva,
            ht
       FROM jefy_depense.depense_ctrl_planco
      WHERE man_id = manid;

-- recup du nb de pieces
     SELECT SUM (dpp.dpp_nb_piece)
       INTO nb_pieces
       FROM jefy_depense.depense_papier dpp,
            jefy_depense.depense_budget db,
            jefy_depense.depense_ctrl_planco dpco
      WHERE db.dep_id = dpco.dep_id
        AND dpp.dpp_id = db.dpp_id
        AND man_id = manid;

-- maj du mandat
     UPDATE mandat
        SET man_ht = ht,
            man_tva = tva,
            man_ttc = ttc,
            man_nb_piece = nb_pieces
      WHERE man_id = manid;

-- recup du brouillard
     set_mandat_brouillard (manid);
     RETURN cpt;
  END;

  FUNCTION set_titre_recette (rpcoid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
--     jefytitre           jefy.titre%ROWTYPE;
     gescode             gestion.ges_code%TYPE;
     titid               titre.tit_id%TYPE;
     titorginekey        titre.tit_orgine_key%TYPE;
     titoriginelib       titre.tit_origine_lib%TYPE;
     oriordre            titre.ori_ordre%TYPE;
     prestid             titre.prest_id%TYPE;
     torordre            titre.tor_ordre%TYPE;
     modordre            titre.mod_ordre%TYPE;
     presid              INTEGER;
     cpt                 INTEGER;
     virordre            INTEGER;
     flag                INTEGER;
     recettepapier       jefy_recette.recette_papier%ROWTYPE;
     recettebudget       jefy_recette.recette_budget%ROWTYPE;
     facturebudget       jefy_recette.facture_budget%ROWTYPE;
     recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
  BEGIN
-- recuperation du ges_code --
     SELECT ges_code
       INTO gescode
       FROM bordereau
      WHERE bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
     SELECT *
       INTO recettectrlplanco
       FROM jefy_recette.recette_ctrl_planco
      WHERE rpco_id = rpcoid;

     SELECT *
       INTO recettebudget
       FROM jefy_recette.recette_budget
      WHERE rec_id = recettectrlplanco.rec_id;

     SELECT *
       INTO facturebudget
       FROM jefy_recette.facture_budget
      WHERE fac_id = recettebudget.fac_id;

     SELECT *
       INTO recettepapier
       FROM jefy_recette.recette_papier
      WHERE rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
     SELECT COUNT (*)
       INTO flag
       FROM maracuja.v_organ_exer
      WHERE org_id = facturebudget.org_id
        AND exe_ordre = facturebudget.exe_ordre;

     IF (flag = 0)
     THEN
        raise_application_error
                       (-20001,
                           'La ligne budgetaire affectee a la recette num. '
                        || recettebudget.rec_numero
                        || ' n''est pas ouverte sur '
                        || facturebudget.exe_ordre
                        || '.'
                       );
     END IF;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
     titorginekey := NULL;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
     titoriginelib := NULL;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
     oriordre :=
        gestionorigine.traiter_orgid (facturebudget.org_id,
                                      facturebudget.exe_ordre
                                     );

--PRESTID : PRESTATION INTERNE --
     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      WHERE d.pef_id = e.pef_id AND d.rec_id = recettectrlplanco.rec_id;

     IF cpt = 1
     THEN
        SELECT prest_id
          INTO prestid
          FROM jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         WHERE d.pef_id = e.pef_id AND d.rec_id = recettectrlplanco.rec_id;
     ELSE
        prestid := NULL;
     END IF;

--TORORDRE : ORIGINE DU MANDAT --
     torordre := 1;
--VIRORDRE --
     virordre := NULL;

     SELECT titre_seq.NEXTVAL
       INTO titid
       FROM DUAL;

     INSERT INTO titre
                 (bor_id, bor_ordre, brj_ordre, exe_ordre, ges_code,
                  mod_ordre, ori_ordre, pco_num, prest_id,
                  tit_date_remise, tit_date_visa_princ, tit_etat,
                  tit_etat_remise, tit_ht, tit_id, tit_motif_rejet,
                  tit_nb_piece, tit_numero, tit_numero_rejet, tit_ordre,
                  tit_orgine_key, tit_origine_lib,
                  tit_ttc,
                  tit_tva, tor_ordre,
                  utl_ordre, org_ordre,
                  fou_ordre, mor_ordre, pai_ordre,
                  rib_ordre_ordonnateur, rib_ordre_comptable,
                  tit_libelle
                 )
          VALUES (borid,                                            --BOR_ID,
                        -borid,                                  --BOR_ORDRE,
                               NULL,                             --BRJ_ORDRE,
                                    recettepapier.exe_ordre,     --EXE_ORDRE,
                                                            gescode,
                                                                  --GES_CODE,
                  NULL,   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                       oriordre,                                 --ORI_ORDRE,
                                recettectrlplanco.pco_num,         --PCO_NUM,
                                                          prestid,
                                                                  --PREST_ID,
                  SYSDATE,                                 --TIT_DATE_REMISE,
                          NULL,                        --TIT_DATE_VISA_PRINC,
                               'ATTENTE',                         --TIT_ETAT,
                  'ATTENTE',                               --TIT_ETAT_REMISE,
                            recettectrlplanco.rpco_ht_saisie,       --TIT_HT,
                                                             titid, --TIT_ID,
                                                                   NULL,
                                                           --TIT_MOTIF_REJET,
                  recettepapier.rpp_nb_piece,                 --TIT_NB_PIECE,
                                             -1,
                             --TIT_NUMERO, numerotation en fin de transaction
                                                NULL,     --TIT_NUMERO_REJET,
                                                     -titid,
                       --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                  titorginekey,                             --TIT_ORGINE_KEY,
                               titoriginelib,              --TIT_ORIGINE_LIB,
                  recettectrlplanco.rpco_ttc_saisie,               --TIT_TTC,
                  recettectrlplanco.rpco_tva_saisie,               --TIT_TVA,
                                                    torordre,    --TOR_ORDRE,
                  recettepapier.utl_ordre,                        --UTL_ORDRE
                                          facturebudget.org_id,  --ORG_ORDRE,
                  recettepapier.fou_ordre,
                                -- FOU_ORDRE  --TOCHECK certains sont nuls...
                                          facturebudget.mor_ordre,
                                                                  --MOR_ORDRE
                                                                  NULL,
                                                                 -- VIR_ORDRE
                  recettepapier.rib_ordre, recettepapier.rib_ordre,
                  recettebudget.rec_lib
                 );

-- maj du tit_id dans la recette
     UPDATE jefy_recette.recette_ctrl_planco
        SET tit_id = titid
      WHERE rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
     RETURN titid;
  END;

  FUNCTION set_titre_recettes (lesrpcoid VARCHAR, borid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     raise_application_error (-20001, 'OPERATION NON TRAITEE');
     RETURN cpt;
  END;


  FUNCTION ndep_mand_org_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
     cpt            INTEGER;
     fouordre       V_Fournis_light.fou_ordre%TYPE;
     ribordre       v_rib.rib_ordre%TYPE;
     pconum         plan_comptable.pco_num%TYPE;
     modordre       mode_paiement.mod_ordre%TYPE;
     orgid          jefy_admin.organ.org_id%TYPE;
     ht             mandat.man_ht%TYPE;
     tva            mandat.man_ht%TYPE;
     ttc            mandat.man_ht%TYPE;
     budgetaire     mandat.man_ht%TYPE;
      
      Cursor Ndep_Mand_Org_Fou_Rib_Pco_Mod
      IS                 
        SELECT e.org_id, dpp.fou_ordre, dpp.rib_ordre, d.pco_num,
          dpp.mod_ordre, SUM (dpco_ht_saisie) ht,
          SUM (dpco_tva_saisie) tva, SUM (dpco_ttc_saisie) ttc,
          Sum (Dpco_Montant_Budgetaire) Budgetaire
        FROM MARACUJA.abricot_bord_selection ab,
          jefy_depense.depense_ctrl_planco d,
          jefy_depense.depense_budget db,
          Jefy_Depense.Depense_Papier Dpp,
          Jefy_Depense.Engage_Budget E,
          jefy_Admin.organ Vo,
          Maracuja.V_Fournis_light Vf
        WHERE d.dpco_id = ab.dep_id
          And Dpp.Dpp_Id = Db.Dpp_Id
          and dpp.fou_ordre = vf.fou_ordre
          And Db.Dep_Id = D.Dep_Id
          AND abr_id = abrid
          And E.Eng_Id = Db.Eng_Id
          and vo.org_id = e.org_id
         AND ab.abr_etat = 'ATTENTE'
         And D.Man_Id Is Null
        Group By vo.org_univ, vo.org_etab, vo.org_UB, vo.org_CR, vo.org_souscr,E.Org_Id,
          Dpp.Fou_Ordre,
          vf.fou_code,
          dpp.rib_ordre,
          d.pco_num,
          Dpp.Mod_Ordre
        order by vo.org_univ, vo.org_etab, vo.org_UB, vo.org_CR, vo.org_souscr,
          vf.fou_code,
          DPP.RIB_ORDRE,
          D.Pco_Num,
          DPP.MOD_ORDRE;

     CURSOR lesdpcoids
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp,
               jefy_depense.engage_budget e
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND e.eng_id = db.eng_id
           AND ab.abr_etat = 'ATTENTE'
           AND e.org_id = orgid
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre = ribordre
           AND d.pco_num = pconum
           AND dpp.mod_ordre = modordre
           AND d.man_id IS NULL
           order by d.dpco_id;

     CURSOR lesdpcoidsribnull
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp,
               jefy_depense.engage_budget e
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND e.eng_id = db.eng_id
           AND ab.abr_etat = 'ATTENTE'
           AND e.org_id = orgid
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre IS NULL
           AND d.pco_num = pconum
           AND dpp.mod_ordre = modordre
           AND d.man_id IS NULL
           order by d.dpco_id;

     chainedpcoid   VARCHAR (5000);
     tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%TYPE;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     OPEN ndep_mand_org_fou_rib_pco_mod;

     LOOP
        FETCH ndep_mand_org_fou_rib_pco_mod
         INTO orgid, fouordre, ribordre, pconum, modordre, ht, tva, ttc,
              budgetaire;

        EXIT WHEN ndep_mand_org_fou_rib_pco_mod%NOTFOUND;
        chainedpcoid := NULL;

        IF ribordre IS NOT NULL
        THEN
           OPEN lesdpcoids;

           LOOP
              FETCH lesdpcoids
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoids%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoids;
        ELSE
           OPEN lesdpcoidsribnull;

           LOOP
              FETCH lesdpcoidsribnull
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoidsribnull%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoidsribnull;
        END IF;

        chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
        cpt := set_mandat_depenses (chainedpcoid, borid);
     END LOOP;

     CLOSE ndep_mand_org_fou_rib_pco_mod;

     RETURN cpt;
  END;




  FUNCTION ndep_mand_fou_rib_pco_mod (abrid INTEGER, borid INTEGER)
     RETURN INTEGER
  IS
     cpt            INTEGER;
     fouordre       V_Fournis_light.fou_ordre%TYPE;
     ribordre       v_rib.rib_ordre%TYPE;
     pconum         plan_comptable.pco_num%TYPE;
     modordre       mode_paiement.mod_ordre%TYPE;
     orgid          jefy_admin.organ.org_id%TYPE;
     ht             mandat.man_ht%TYPE;
     tva            mandat.man_ht%TYPE;
     ttc            mandat.man_ht%TYPE;
     budgetaire     mandat.man_ht%TYPE;

      Cursor Ndep_Mand_Fou_Rib_Pco_Mod
      IS
        SELECT dpp.fou_ordre, dpp.rib_ordre, d.pco_num, dpp.mod_ordre,
          SUM (dpco_ht_saisie) ht, SUM (dpco_tva_saisie) tva,
          SUM (dpco_ttc_saisie) ttc,
          Sum (Dpco_Montant_Budgetaire) Budgetaire
        FROM MARACUJA.abricot_bord_selection ab,
          jefy_depense.depense_ctrl_planco d,
          Jefy_Depense.Depense_Budget Db,
          Jefy_Depense.Depense_Papier Dpp,
          Maracuja.V_Fournis_light Vf
        WHERE d.dpco_id = ab.dep_id
          And Dpp.Dpp_Id = Db.Dpp_Id
          And Dpp.Fou_Ordre = Vf.Fou_Ordre 
          And Db.Dep_Id = D.Dep_Id
          AND abr_id = abrid
          And Ab.Abr_Etat = 'ATTENTE'
          And D.Man_Id Is Null
        Group By Dpp.Fou_Ordre, Vf.Fou_Code, Dpp.Rib_Ordre, D.Pco_Num, Dpp.Mod_Ordre
        Order By Vf.Fou_Code, Dpp.Rib_Ordre, D.Pco_Num, Dpp.Mod_Ordre;

     CURSOR lesdpcoids
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND ab.abr_etat = 'ATTENTE'
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre = ribordre
           AND d.pco_num = pconum
           AND d.man_id IS NULL
           AND dpp.mod_ordre = modordre
           order by d.dpco_id;

     CURSOR lesdpcoidsnull
     IS
        SELECT d.dpco_id
          FROM abricot_bord_selection ab,
               jefy_depense.depense_ctrl_planco d,
               jefy_depense.depense_budget db,
               jefy_depense.depense_papier dpp
         WHERE d.dpco_id = ab.dep_id
           AND dpp.dpp_id = db.dpp_id
           AND db.dep_id = d.dep_id
           AND abr_id = abrid
           AND ab.abr_etat = 'ATTENTE'
           AND dpp.fou_ordre = fouordre
           AND dpp.rib_ordre IS NULL
           AND d.pco_num = pconum
           AND d.man_id IS NULL
           AND dpp.mod_ordre = modordre
           order by d.dpco_id;

     chainedpcoid   VARCHAR (5000);
     tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%TYPE;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     OPEN ndep_mand_fou_rib_pco_mod;

     LOOP
        FETCH ndep_mand_fou_rib_pco_mod
         INTO fouordre, ribordre, pconum, modordre, ht, tva, ttc,
              budgetaire;

        EXIT WHEN ndep_mand_fou_rib_pco_mod%NOTFOUND;
        chainedpcoid := NULL;

        IF ribordre IS NOT NULL
        THEN
           OPEN lesdpcoids;

           LOOP
              FETCH lesdpcoids
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoids%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoids;
        ELSE
           OPEN lesdpcoidsnull;

           LOOP
              FETCH lesdpcoidsnull
               INTO tmpdpcoid;

              EXIT WHEN lesdpcoidsnull%NOTFOUND;
              chainedpcoid := chainedpcoid || tmpdpcoid || '$';
           END LOOP;

           CLOSE lesdpcoidsnull;
        END IF;

        chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
        cpt := set_mandat_depenses (chainedpcoid, borid);
     END LOOP;

     CLOSE ndep_mand_fou_rib_pco_mod;

     RETURN cpt;
  END;

-- procedures de verifications
  FUNCTION selection_valide (abrid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

-- meme exercice

     -- si PI somme recette = somme depense

     -- recette_valides

     -- depense_valides
     RETURN cpt;
  END;

  FUNCTION recette_valide (recid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     RETURN cpt;
  END;

  FUNCTION depense_valide (depid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

     RETURN cpt;
  END;

  FUNCTION verif_bordereau_selection (borid INTEGER, abrid INTEGER)
     RETURN INTEGER
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

     -- verifier sum TTC recette selection = sum TTC titre du bord

     -- verifier sum TTC depense  = sum TTC mandat du bord

     -- verifier sum TTC recette  = sum TTC titre  du bord
     RETURN cpt;
  END;

-- procedures de locks de transaction
  PROCEDURE lock_mandats
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;
  END;

  PROCEDURE lock_titres
  IS
     cpt   INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM DUAL;
  END;

  PROCEDURE get_depense_jefy_depense (manid INTEGER)
  IS
     depid               depense.dep_id%TYPE;
     jefydepensebudget   jefy_depense.depense_budget%ROWTYPE;
     tmpdepensepapier    jefy_depense.depense_papier%ROWTYPE;
     jefydepenseplanco   jefy_depense.depense_ctrl_planco%ROWTYPE;
     lignebudgetaire     depense.dep_ligne_budgetaire%TYPE;
     fouadresse          depense.dep_adresse%TYPE;
     founom              depense.dep_fournisseur%TYPE;
     lotordre            depense.dep_lot%TYPE;
     marordre            depense.dep_marches%TYPE;
     fouordre            depense.fou_ordre%TYPE;
     gescode             depense.ges_code%TYPE;
     modordre            depense.mod_ordre%TYPE;
     cpt                 INTEGER;
     tcdordre            type_credit.tcd_ordre%TYPE;
     tcdcode             type_credit.tcd_code%TYPE;
     ecd_ordre_ema       ecriture_detail.ecd_ordre%TYPE;
     orgid               INTEGER;

     CURSOR depenses
     IS
        SELECT db.*
          FROM jefy_depense.depense_budget db,
               jefy_depense.depense_ctrl_planco dpco
         WHERE dpco.man_id = manid AND db.dep_id = dpco.dep_id;
  BEGIN
     OPEN depenses;

     LOOP
        FETCH depenses
         INTO jefydepensebudget;

        EXIT WHEN depenses%NOTFOUND;

        -- creation du depid --
        SELECT depense_seq.NEXTVAL
          INTO depid
          FROM DUAL;

        -- creation de lignebudgetaire--
        SELECT org_ub || ' ' || org_cr || ' ' || org_souscr
          INTO lignebudgetaire
          FROM jefy_admin.organ
         WHERE org_id = (SELECT org_id
                           FROM jefy_depense.engage_budget
                          WHERE eng_id = jefydepensebudget.eng_id
                                                                 --AND eng_stat !='A'
                       );

        --recuperer le type de credit a partir de la commande
        SELECT tcd_ordre
          INTO tcdordre
          FROM jefy_depense.engage_budget
         WHERE eng_id = jefydepensebudget.eng_id;

        --AND eng_stat !='A'
        SELECT org_ub, org_id
          INTO gescode, orgid
          FROM jefy_admin.organ
         WHERE org_id = (SELECT org_id
                           FROM jefy_depense.engage_budget
                          WHERE eng_id = jefydepensebudget.eng_id
                                                                 --AND eng_stat !='A'
                       );


        -- fouordre --
        SELECT fou_ordre
          INTO fouordre
          FROM jefy_depense.engage_budget
         WHERE eng_id = jefydepensebudget.eng_id;


        -- founom --
        founom := getFournisNom(fouOrdre);


        -- fouadresse --
        
        select count(*) into cpt from v_depense_adresse where dep_id = jefydepensebudget.dep_id;

        if (cpt = 0)
        then                
            raise_application_error(-20001, 'Pas d''adresse de facturation pour le fournisseur '|| founom);
        end if;
        
        select substr(adresse,1,196)
        INTO fouadresse
        from (select * from v_depense_adresse
        WHERE dep_id = jefydepensebudget.dep_id)
        where rownum=1;
        

        --AND eng_stat !='A'

        -- lotordre --
        SELECT COUNT (*)
          INTO cpt
          FROM jefy_marches.attribution
         WHERE att_ordre = (SELECT att_ordre
                              FROM jefy_depense.engage_ctrl_marche
                             WHERE eng_id = jefydepensebudget.eng_id);

        IF cpt = 0
        THEN
           lotordre := NULL;
        ELSE
           SELECT lot_ordre
             INTO lotordre
             FROM jefy_marches.attribution
            WHERE att_ordre = (SELECT att_ordre
                                 FROM jefy_depense.engage_ctrl_marche
                                WHERE eng_id = jefydepensebudget.eng_id);
        END IF;

        -- marordre --
        SELECT COUNT (*)
          INTO cpt
          FROM jefy_marches.lot
         WHERE lot_ordre = lotordre;

        IF cpt = 0
        THEN
           marordre := NULL;
        ELSE
           SELECT mar_ordre
             INTO marordre
             FROM jefy_marches.lot
            WHERE lot_ordre = lotordre;
        END IF;

        --MOD_ORDRE --
        SELECT mod_ordre
          INTO modordre
          FROM jefy_depense.depense_papier
         WHERE dpp_id = jefydepensebudget.dpp_id;

        -- recuperer l'ecriture_detail pour emargements semi-auto
        SELECT ecd_ordre
          INTO ecd_ordre_ema
          FROM jefy_depense.depense_ctrl_planco
         WHERE dep_id = jefydepensebudget.dep_id;

        -- recup de la depense papier
        SELECT *
          INTO tmpdepensepapier
          FROM jefy_depense.depense_papier
         WHERE dpp_id = jefydepensebudget.dpp_id;

        -- recup des infos de depense_ctrl_planco
        SELECT *
          INTO jefydepenseplanco
          FROM jefy_depense.depense_ctrl_planco
         WHERE dep_id = jefydepensebudget.dep_id;

        -- creation de la depense --
        INSERT INTO depense
             VALUES (fouadresse,                               --DEP_ADRESSE,
                      NULL,                      --DEP_DATE_COMPTA,
                      tmpdepensepapier.dpp_date_reception,
                                                        --DEP_DATE_RECEPTION,
                     tmpdepensepapier.dpp_date_service_fait,
                                                          --DEP_DATE_SERVICE,
                      'VALIDE',
                                                                  --DEP_ETAT,
                     founom,
                                                          --DEP_FOURNISSEUR,
                     jefydepenseplanco.dpco_montant_budgetaire,     --DEP_HT,
                     depense_seq.NEXTVAL,                           --DEP_ID,
                     lignebudgetaire,
                                                      --DEP_LIGNE_BUDGETAIRE,
                     lotordre, --DEP_LOT,
                     marordre,
                                                               --DEP_MARCHES,
                     jefydepenseplanco.dpco_ttc_saisie,
                                                     --DEP_MONTANT_DISQUETTE,
                                                       NULL,
               -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                     SUBSTR (tmpdepensepapier.dpp_numero_facture, 1, 199),
                                                                --DEP_NUMERO,
                     jefydepenseplanco.dpco_id,                  --DEP_ORDRE,
                                        NULL,             --DEP_REJET,
                     tmpdepensepapier.rib_ordre,                   --DEP_RIB,
                                             'NON',     --DEP_SUPPRESSION,
                     jefydepenseplanco.dpco_ttc_saisie,            --DEP_TTC,
                       jefydepenseplanco.dpco_ttc_saisie
                     - jefydepenseplanco.dpco_montant_budgetaire, -- DEP_TVA,
                     tmpdepensepapier.exe_ordre,                 --EXE_ORDRE,
                     fouordre,        --FOU_ORDRE,
                     gescode, --GES_CODE,
                     manid,
                                                                    --MAN_ID,
                     jefydepenseplanco.man_id,                   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                     modordre,
                     jefydepenseplanco.pco_num,                  --PCO_ORDRE,
                     tmpdepensepapier.utl_ordre,
                                                                  --UTL_ORDRE
                     orgid,                                       --org_ordre
                     tcdordre, ecd_ordre_ema,
               -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                     tmpdepensepapier.dpp_date_facture);
     END LOOP;

     CLOSE depenses;
  END;

  PROCEDURE get_recette_jefy_recette (titid INTEGER)
  IS
     recettepapier          jefy_recette.recette_papier%ROWTYPE;
     recettebudget          jefy_recette.recette_budget%ROWTYPE;
     facturebudget          jefy_recette.facture_budget%ROWTYPE;
     recettectrlplanco      jefy_recette.recette_ctrl_planco%ROWTYPE;
     recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%ROWTYPE;
     maracujatitre          maracuja.titre%ROWTYPE;
     adrnom                 VARCHAR2 (200);
     letyperecette          VARCHAR2 (200);
     titinterne             VARCHAR2 (200);
     lbud                   VARCHAR2 (200);
     tboordre               INTEGER;
     cpt                    INTEGER;

     CURSOR c_recette
     IS
        SELECT *
          FROM jefy_recette.recette_ctrl_planco
         WHERE tit_id = titid;
  BEGIN
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
     OPEN c_recette;

     LOOP
        FETCH c_recette
         INTO recettectrlplanco;

        EXIT WHEN c_recette%NOTFOUND;

        SELECT *
          INTO recettebudget
          FROM jefy_recette.recette_budget
         WHERE rec_id = recettectrlplanco.rec_id;

        SELECT *
          INTO facturebudget
          FROM jefy_recette.facture_budget
         WHERE fac_id = recettebudget.fac_id;

        SELECT *
          INTO recettepapier
          FROM jefy_recette.recette_papier
         WHERE rpp_id = recettebudget.rpp_id;

        SELECT *
          INTO maracujatitre
          FROM maracuja.titre
         WHERE tit_id = titid;

        IF (recettebudget.rec_id_reduction IS NULL)
        THEN
           letyperecette := 'R';
        ELSE
           letyperecette := 'T';
        END IF;

        SELECT COUNT (*)
          INTO cpt
          FROM jefy_recette.pi_dep_rec
         WHERE rec_id = recettectrlplanco.rec_id;

        IF cpt > 0
        THEN
           titinterne := 'O';
        ELSE
           titinterne := 'N';
        END IF;

       adrnom := getfournisnom(recettepapier.fou_ordre);
       
        SELECT org_ub || '/' || org_cr || '/' || org_souscr
          INTO lbud
          FROM jefy_admin.organ
         WHERE org_id = facturebudget.org_id;

        SELECT DISTINCT tbo_ordre
                   INTO tboordre
                   FROM maracuja.titre t, maracuja.bordereau b
                  WHERE b.bor_id = t.bor_id AND t.tit_id = titid;

-- 200 bordereau de presntation interne recette
        IF tboordre = 200
        THEN
           tboordre := NULL;
        ELSE
           tboordre := facturebudget.org_id;
        END IF;

        INSERT INTO recette
             VALUES (recettectrlplanco.exe_ordre,                --EXE_ORDRE,
                     maracujatitre.ges_code,
                                                                  --GES_CODE,
                     NULL,                                        --MOD_CODE,
                     recettectrlplanco.pco_num,               --PCO_NUM,
                     recettebudget.rec_date_saisie,
                                            --jefytitre.tit_date,-- REC_DATE,
                     adrnom,   -- REC_DEBITEUR,
                     recette_seq.NEXTVAL,                          -- REC_ID,
                     NULL,               -- REC_IMPUTTVA,
                     NULL,
                                                  -- REC_INTERNE, // TODO ROD
                     facturebudget.fac_lib,
                                                              -- REC_LIBELLE,
                     lbud,                           -- REC_LIGNE_BUDGETAIRE,
                     'E',                                -- REC_MONNAIE,
                     recettectrlplanco.rpco_ht_saisie,         --HT,
                     recettectrlplanco.rpco_ttc_saisie,                --TTC,
                     recettectrlplanco.rpco_ttc_saisie,          --DISQUETTE,
                     recettectrlplanco.rpco_tva_saisie,     --   REC_MONTTVA,
                     facturebudget.fac_numero,                  --   REC_NUM,
                     recettectrlplanco.rpco_id,
                                                              --   REC_ORDRE,
                     recettepapier.rpp_nb_piece,              --   REC_PIECE,
                     facturebudget.fac_numero,
                                                                --   REC_REF,
                     'VALIDE',                                 --   REC_STAT,
                     'NON',        --    REC_SUPPRESSION,  Modif Rod
                     letyperecette,           --     REC_TYPE,
                     NULL,  --     REC_VIREMENT,
                     titid, --      TIT_ID,
                     -titid,
                                                           --      TIT_ORDRE,
                     recettebudget.utl_ordre,              --       UTL_ORDRE
                     facturebudget.org_id,
                                               --       ORG_ORDRE --ajout rod
                     facturebudget.fou_ordre,         --FOU_ORDRE --ajout rod
                     NULL,                --mod_ordre
                     recettepapier.mor_ordre,
                                                                  --mor_ordre
                     recettepapier.rib_ordre, NULL);
     END LOOP;

     CLOSE c_recette;
  END;

-- procedures du brouillard
  PROCEDURE set_mandat_brouillard (manid INTEGER)
  IS
     lemandat                  mandat%ROWTYPE;
     pconum_ctrepartie         mandat.pco_num%TYPE;
     pconum_tva                planco_visa.pco_num_tva%TYPE;
     gescodecompta             mandat.ges_code%TYPE;
     pvicontrepartie_gestion   planco_visa.pvi_contrepartie_gestion%TYPE;
     modcontrepartie_gestion   mode_paiement.mod_contrepartie_gestion%TYPE;
     pconum_185                planco_visa.pco_num_tva%TYPE;
     parvalue                  parametre.par_value%TYPE;
     cpt                       INTEGER;
     tboordre                  type_bordereau.tbo_ordre%TYPE;
     sens                      ecriture_detail.ecd_sens%TYPE;
  BEGIN
     SELECT *
       INTO lemandat
       FROM mandat
      WHERE man_id = manid;

     SELECT DISTINCT tbo_ordre
                INTO tboordre
                FROM bordereau
               WHERE bor_id IN (SELECT bor_id
                                  FROM mandat
                                 WHERE man_id = manid);

--    select count(*) into cpt from v_titre_prest_interne where man_ordre=lemandat.man_ordre and tit_ordre is not null;
--    if cpt = 0 then
     IF lemandat.prest_id IS NULL
     THEN
        -- creation du mandat_brouillard visa DEBIT--
        sens := inverser_sens_orv (tboordre, 'D');

        INSERT INTO mandat_brouillard
             VALUES (NULL,                                      --ECD_ORDRE,
                          lemandat.exe_ordre,                   --EXE_ORDRE,
                                             lemandat.ges_code,  --GES_CODE,
                     ABS (lemandat.man_ht),                   --MAB_MONTANT,
                                           'VISA MANDAT',   --MAB_OPERATION,
                     mandat_brouillard_seq.NEXTVAL,             --MAB_ORDRE,
                                                   sens,         --MAB_SENS,
                                                        manid,     --MAN_ID,
                     lemandat.pco_num                               --PCO_NU
                                     );

        -- credit=ctrepartie
        --debit = ordonnateur
        -- recup des infos du VISA CREDIT --
        SELECT COUNT (*)
          INTO cpt
          FROM planco_visa
         WHERE pco_num_ordonnateur = lemandat.pco_num
           AND exe_ordre = lemandat.exe_ordre;

        IF cpt = 0
        THEN
           raise_application_error (-20001,
                                       'PROBLEM DE CONTRE PARTIE '
                                    || lemandat.pco_num
                                   );
        END IF;

        SELECT pco_num_ctrepartie, pco_num_tva, pvi_contrepartie_gestion
          INTO pconum_ctrepartie, pconum_tva, pvicontrepartie_gestion
          FROM planco_visa
         WHERE pco_num_ordonnateur = lemandat.pco_num
           AND exe_ordre = lemandat.exe_ordre;

        SELECT COUNT (*)
          INTO cpt
          FROM mode_paiement
         WHERE exe_ordre = lemandat.exe_ordre
           AND mod_ordre = lemandat.mod_ordre
           AND pco_num_visa IS NOT NULL;

        IF cpt != 0
        THEN
           SELECT pco_num_visa, mod_contrepartie_gestion
             INTO pconum_ctrepartie, modcontrepartie_gestion
             FROM mode_paiement
            WHERE exe_ordre = lemandat.exe_ordre
              AND mod_ordre = lemandat.mod_ordre
              AND pco_num_visa IS NOT NULL;
        END IF;

        -- modif 15/09/2005 compatibilite avec new gestion_exercice
        SELECT c.ges_code, ge.pco_num_185
          INTO gescodecompta, pconum_185
          FROM gestion g, comptabilite c, gestion_exercice ge
         WHERE g.ges_code = lemandat.ges_code
           AND g.com_ordre = c.com_ordre
           AND g.ges_code = ge.ges_code
           AND ge.exe_ordre = lemandat.exe_ordre;

                -- 5/12/2007
                -- on ne prend plus le parametre mais PVICONTREPARTIE_GESTION
                -- PVICONTREPARTIE_GESTION de la table planc_visa dans un premier temps
                -- dans un second temps il peut etre ecras} par mod_CONTREPARTIE_GESTION de MODE_PAIEMENT
                      --SELECT par_value   INTO parvalue
        --    FROM PARAMETRE
        --    WHERE par_key ='CONTRE PARTIE VISA'
        --    AND exe_ordre = lemandat.exe_ordre;
        parvalue := pvicontrepartie_gestion;

        IF (modcontrepartie_gestion IS NOT NULL)
        THEN
           parvalue := modcontrepartie_gestion;
        END IF;

        IF parvalue = 'COMPOSANTE'
        THEN
           gescodecompta := lemandat.ges_code;
        END IF;

        IF pconum_185 IS NULL
        THEN
           -- creation du mandat_brouillard visa CREDIT --
           sens := inverser_sens_orv (tboordre, 'C');

           IF sens = 'D'
           THEN
              pconum_ctrepartie := '4632';
           END IF;

           INSERT INTO mandat_brouillard
                VALUES (NULL,                                    --ECD_ORDRE,
                             lemandat.exe_ordre,                 --EXE_ORDRE,
                                                gescodecompta,    --GES_CODE,
                        ABS (lemandat.man_ttc),                --MAB_MONTANT,
                                               'VISA MANDAT',
                                                             --MAB_OPERATION,
                        mandat_brouillard_seq.NEXTVAL,           --MAB_ORDRE,
                                                      sens,       --MAB_SENS,
                                                           manid,   --MAN_ID,
                        pconum_ctrepartie                            --PCO_NU
                                         );
        ELSE
           --au SACD --
           sens := inverser_sens_orv (tboordre, 'C');

           IF sens = 'D'
           THEN
              pconum_ctrepartie := '4632';
           END IF;

           INSERT INTO mandat_brouillard
                VALUES (NULL,                                    --ECD_ORDRE,
                             lemandat.exe_ordre,                 --EXE_ORDRE,
                                                lemandat.ges_code,
                                                                  --GES_CODE,
                        ABS (lemandat.man_ttc),                --MAB_MONTANT,
                                               'VISA MANDAT',
                                                             --MAB_OPERATION,
                        mandat_brouillard_seq.NEXTVAL,           --MAB_ORDRE,
                                                      sens,       --MAB_SENS,
                                                           manid,   --MAN_ID,
                        pconum_ctrepartie                            --PCO_NU
                                         );
        END IF;

        IF lemandat.man_tva != 0
        THEN
           -- creation du mandat_brouillard visa CREDIT TVA --
           sens := inverser_sens_orv (tboordre, 'D');

           INSERT INTO mandat_brouillard
                VALUES (NULL,                                   --ECD_ORDRE,
                             lemandat.exe_ordre,                --EXE_ORDRE,
                                                lemandat.ges_code,
                                                                 --GES_CODE,
                        ABS (lemandat.man_tva),               --MAB_MONTANT,
                                               'VISA TVA',  --MAB_OPERATION,
                        mandat_brouillard_seq.NEXTVAL,          --MAB_ORDRE,
                                                      sens,      --MAB_SENS,
                                                           manid,  --MAN_ID,
                        pconum_tva                                  --PCO_NU
                                  );
        END IF;
     ELSE
        bordereau_abricot.set_mandat_brouillard_intern (manid);
     END IF;
  END;

  PROCEDURE set_mandat_brouillard_intern (manid INTEGER)
  IS
     lemandat            mandat%ROWTYPE;
     leplancomptable     plan_comptable%ROWTYPE;
     pconum_ctrepartie   mandat.pco_num%TYPE;
     pconum_tva          planco_visa.pco_num_tva%TYPE;
     gescodecompta       mandat.ges_code%TYPE;
     ctpgescode          mandat.ges_code%TYPE;
     pconum_185          planco_visa.pco_num_tva%TYPE;
     parvalue            parametre.par_value%TYPE;
     cpt                 INTEGER;
     lepconum            plan_comptable_exer.pco_num%TYPE;
     chap varchar2(2);
  BEGIN
     SELECT *
       INTO lemandat
       FROM mandat
      WHERE man_id = manid;

     -- modif 15/09/2005 compatibilite avec new gestion_exercice
     SELECT c.ges_code, ge.pco_num_185
       INTO gescodecompta, pconum_185
       FROM gestion g, comptabilite c, gestion_exercice ge
      WHERE g.ges_code = lemandat.ges_code
        AND g.com_ordre = c.com_ordre
        AND g.ges_code = ge.ges_code
        AND ge.exe_ordre = lemandat.exe_ordre;

     -- recup des infos du VISA CREDIT --
     SELECT COUNT (*)
       INTO cpt
       FROM planco_visa
      WHERE pco_num_ordonnateur = lemandat.pco_num
        AND exe_ordre = lemandat.exe_ordre;

     IF cpt = 0
     THEN
        raise_application_error (-20001,
                                    'PROBLEM DE CONTRE PARTIE '
                                 || lemandat.pco_num
                                );
     END IF;

     SELECT pco_num_ctrepartie, pco_num_tva
       INTO pconum_ctrepartie, pconum_tva
       FROM planco_visa
      WHERE pco_num_ordonnateur = lemandat.pco_num
        AND exe_ordre = lemandat.exe_ordre;

  -- verification si le compte existe !
--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '18'||lemandat.pco_num;

     --   IF cpt = 0 THEN
--    SELECT * INTO leplancomptable FROM PLAN_COMPTABLE
--    WHERE pco_num = lemandat.pco_num;

     --    maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'18'||lemandat.pco_num);
--   END IF;
--
        -- recup des 2 premiers caracteres du compte
        SELECT SUBSTR (lemandat.pco_num, 1, 2)
          INTO chap
          FROM DUAL;


        IF chap != '18'
        THEN
           lepconum :=
              api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
        ELSE
           raise_application_error (-20001,'Le compte d''imputation ne doit pas etre un compte 18xx (' || lemandat.pco_num ||')');
        END IF;

     lepconum :=
            api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);

--    lemandat.pco_num := '18'||lemandat.pco_num;

     -- creation du mandat_brouillard visa DEBIT--
     INSERT INTO mandat_brouillard
          VALUES (NULL,                                          --ECD_ORDRE,
                       lemandat.exe_ordre,                       --EXE_ORDRE,
                                          lemandat.ges_code,      --GES_CODE,
                  ABS (lemandat.man_ht),                       --MAB_MONTANT,
                                        'VISA MANDAT',       --MAB_OPERATION,
                  mandat_brouillard_seq.NEXTVAL,                 --MAB_ORDRE,
                                                'D',              --MAB_SENS,
                                                    manid,          --MAN_ID,
                  '18' || lemandat.pco_num                           --PCO_NU
                                          );

--   SELECT COUNT(*) INTO cpt FROM PLAN_COMPTABLE
--          WHERE pco_num = '181';

     --   IF cpt = 0 THEN
--        maj_plancomptable_mandat (leplancomptable.pco_nature ,leplancomptable.pco_libelle ,'181');

     --   END IF;
     lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');

     -- planco de CREDIT 181
     -- creation du mandat_brouillard visa CREDIT --

     -- si on est sur un sacd, la contrepartie reste sur le sacd
     IF (pconum_185 IS NOT NULL)
     THEN
        ctpgescode := lemandat.ges_code;
     ELSE
        ctpgescode := gescodecompta;
     END IF;

     INSERT INTO mandat_brouillard
          VALUES (NULL,                                          --ECD_ORDRE,
                       lemandat.exe_ordre,                       --EXE_ORDRE,
                                          ctpgescode,             --GES_CODE,
                                                     ABS (lemandat.man_ttc),
                                                               --MAB_MONTANT,
                  'VISA MANDAT',                             --MAB_OPERATION,
                                mandat_brouillard_seq.NEXTVAL,   --MAB_ORDRE,
                                                              'C',
                                                                  --MAB_SENS,
                                                                  manid,
                                                                    --MAN_ID,
                  '181'                                              --PCO_NU
                       );

     IF lemandat.man_tva != 0
     THEN
        -- creation du mandat_brouillard visa CREDIT TVA --
        INSERT INTO mandat_brouillard
             VALUES (NULL,                                      --ECD_ORDRE,
                          lemandat.exe_ordre,                   --EXE_ORDRE,
                                             lemandat.ges_code,  --GES_CODE,
                     ABS (lemandat.man_tva),                  --MAB_MONTANT,
                                            'VISA TVA',     --MAB_OPERATION,
                     mandat_brouillard_seq.NEXTVAL,             --MAB_ORDRE,
                                                   'D',          --MAB_SENS,
                                                       manid,      --MAN_ID,
                     pconum_tva                                     --PCO_NU
                               );
     END IF;
  END;

  PROCEDURE set_titre_brouillard (titid INTEGER)
  IS
     letitre             titre%ROWTYPE;
     recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
     lesens              VARCHAR2 (20);
     reduction           INTEGER;
     recid               INTEGER;
     flag               integer;

     CURSOR c_recettes
     IS
        SELECT *
          FROM jefy_recette.recette_ctrl_planco
         WHERE tit_id = titid;
  BEGIN
     SELECT *
       INTO letitre
       FROM titre
      WHERE tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
     SELECT MAX (rb.rec_id_reduction)
       INTO reduction
       FROM jefy_recette.recette_budget rb,
            jefy_recette.recette_ctrl_planco rcpo
      WHERE rcpo.rec_id = rb.rec_id AND rcpo.tit_id = titid;

-- si dans le cas d une reduction
     IF (reduction IS NOT NULL)
     THEN
        lesens := 'D';
     ELSE
        lesens := 'C';
     END IF;

     IF letitre.prest_id IS NULL
     THEN
        OPEN c_recettes;

        LOOP
           FETCH c_recettes
            INTO recettectrlplanco;

           EXIT WHEN c_recettes%NOTFOUND;

           SELECT MAX (rec_id)
             INTO recid
             FROM recette
            WHERE rec_ordre = recettectrlplanco.rpco_id;


            -- verifier que la contrepartie n'est pas passée sur l'imputation du titre
            select count(*) into flag FROM jefy_recette.recette_ctrl_planco_ctp
               WHERE rpco_id = recettectrlplanco.rpco_id and PCO_NUM=letitre.pco_num;
            
            if (flag>0) then
               raise_application_error (-20001,'La contrepartie ne doit pas etre identique à l''imputation ( : ' || letitre.tit_libelle ||' / ' || letitre.pco_num || ' / ' || letitre.tit_ttc  || ')');
            end if;



           -- creation du titre_brouillard visa --
           --  RECETTE_CTRL_PLANCO
           INSERT INTO titre_brouillard
                       (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                        tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
              SELECT NULL,                                       --ECD_ORDRE,
                          recettectrlplanco.exe_ordre,           --EXE_ORDRE,
                                                      letitre.ges_code,
                                                                  --GES_CODE,
                     recettectrlplanco.pco_num,                     --PCO_NUM
                     ABS (recettectrlplanco.rpco_ht_saisie),   --TIB_MONTANT,
                                                            'VISA TITRE',
                                                             --TIB_OPERATION,
                     titre_brouillard_seq.NEXTVAL,               --TIB_ORDRE,
                                                  lesens,         --TIB_SENS,
                                                         titid,     --TIT_ID,
                                                               recid
                FROM jefy_recette.recette_ctrl_planco
               WHERE rpco_id = recettectrlplanco.rpco_id;

           -- recette_ctrl_planco_tva
           INSERT INTO titre_brouillard
                       (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                        tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
              SELECT NULL,                                       --ECD_ORDRE,
                          exe_ordre,                             --EXE_ORDRE,
                                    ges_code,                     --GES_CODE,
                                             pco_num,               --PCO_NUM
                     ABS (rpcotva_tva_saisie),                 --TIB_MONTANT,
                                              'VISA TITRE',  --TIB_OPERATION,
                     titre_brouillard_seq.NEXTVAL,               --TIB_ORDRE,
                                                  lesens,         --TIB_SENS,
                                                         titid,     --TIT_ID,
                                                               recid
                FROM jefy_recette.recette_ctrl_planco_tva
               WHERE rpco_id = recettectrlplanco.rpco_id;

           -- recette_ctrl_planco_ctp
           INSERT INTO titre_brouillard
                       (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                        tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
              SELECT NULL,                                       --ECD_ORDRE,
                          recettectrlplanco.exe_ordre,           --EXE_ORDRE,
                                                      ges_code,   --GES_CODE,
                                                               pco_num,
                                                                    --PCO_NUM
                     ABS (rpcoctp_ttc_saisie),                 --TIB_MONTANT,
                                              'VISA TITRE',  --TIB_OPERATION,
                     titre_brouillard_seq.NEXTVAL,               --TIB_ORDRE,
                                                  inverser_sens (lesens),
                                                                  --TIB_SENS,
                     titid,                                         --TIT_ID,
                           recid
                FROM jefy_recette.recette_ctrl_planco_ctp
               WHERE rpco_id = recettectrlplanco.rpco_id;
        END LOOP;

        CLOSE c_recettes;
     ELSE
        set_titre_brouillard_intern (titid);
     END IF;

     -- suppression des lignes d ecritures a ZERO
     DELETE FROM titre_brouillard
           WHERE tib_montant = 0;
  END;

  PROCEDURE set_titre_brouillard_intern (titid INTEGER)
  IS
     letitre             titre%ROWTYPE;
     recettectrlplanco   jefy_recette.recette_ctrl_planco%ROWTYPE;
     lesens              VARCHAR2 (20);
     reduction           INTEGER;
     lepconum            maracuja.plan_comptable.pco_num%TYPE;
     libelle             maracuja.plan_comptable.pco_libelle%TYPE;
     chap                VARCHAR2 (2);
     recid               INTEGER;
     gescodecompta       maracuja.titre.ges_code%TYPE;
     ctpgescode          titre.ges_code%TYPE;
     pconum_185          gestion_exercice.pco_num_185%TYPE;

     CURSOR c_recettes
     IS
        SELECT *
          FROM jefy_recette.recette_ctrl_planco
         WHERE tit_id = titid;
  BEGIN
     SELECT *
       INTO letitre
       FROM titre
      WHERE tit_id = titid;

     -- modif fred 04/2007
     SELECT c.ges_code, ge.pco_num_185
       INTO gescodecompta, pconum_185
       FROM gestion g, comptabilite c, gestion_exercice ge
      WHERE g.ges_code = letitre.ges_code
        AND g.com_ordre = c.com_ordre
        AND g.ges_code = ge.ges_code
        AND ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
     SELECT rb.rec_id_reduction
       INTO reduction
       FROM jefy_recette.recette_budget rb,
            jefy_recette.recette_ctrl_planco rcpo
      WHERE rcpo.rec_id = rb.rec_id AND rcpo.tit_id = titid;

     -- si dans le cas d une reduction
     IF (reduction IS NOT NULL)
     THEN
        lesens := 'D';
     ELSE
        lesens := 'C';
     END IF;

     OPEN c_recettes;

     LOOP
        FETCH c_recettes
         INTO recettectrlplanco;

        EXIT WHEN c_recettes%NOTFOUND;

        SELECT MAX (rec_id)
          INTO recid
          FROM recette
         WHERE rec_ordre = recettectrlplanco.rpco_id;

        -- recup des 2 premiers caracteres du compte
        SELECT SUBSTR (recettectrlplanco.pco_num, 1, 2)
          INTO chap
          FROM DUAL;

        IF chap != '18'
        THEN
           lepconum :=
              api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
        ELSE
           raise_application_error (-20001,'Le compte d''imputation ne doit pas etre un compte 18xx (' || recettectrlplanco.pco_num ||')');
        END IF;

        -- creation du titre_brouillard visa --
        --  RECETTE_CTRL_PLANCO
        INSERT INTO titre_brouillard
                    (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                     tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
           SELECT NULL,                                          --ECD_ORDRE,
                       recettectrlplanco.exe_ordre,              --EXE_ORDRE,
                                                   letitre.ges_code,
                                                                  --GES_CODE,
                  lepconum,                                         --PCO_NUM
                           ABS (recettectrlplanco.rpco_ht_saisie),
                                                               --TIB_MONTANT,
                  'VISA TITRE',                              --TIB_OPERATION,
                               titre_brouillard_seq.NEXTVAL,     --TIB_ORDRE,
                                                            lesens,
                                                                  --TIB_SENS,
                                                                   titid,
                                                                    --TIT_ID,
                  recid
             FROM jefy_recette.recette_ctrl_planco
            WHERE rpco_id = recettectrlplanco.rpco_id;

        -- recette_ctrl_planco_tva
        INSERT INTO titre_brouillard
                    (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                     tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
           SELECT NULL,                                          --ECD_ORDRE,
                       exe_ordre,                                --EXE_ORDRE,
                                 gescodecompta,
                                               -- ges_code,               --GES_CODE,
                                               pco_num,             --PCO_NUM
                  ABS (rpcotva_tva_saisie),                    --TIB_MONTANT,
                                           'VISA TITRE',     --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,                  --TIB_ORDRE,
                                               inverser_sens (lesens),
                                                                  --TIB_SENS,
                                                                      titid,
                                                                    --TIT_ID,
                  recid
             FROM jefy_recette.recette_ctrl_planco_tva
            WHERE rpco_id = recettectrlplanco.rpco_id;

        -- si on est sur un sacd, la contrepartie reste sur le sacd
        IF (pconum_185 IS NOT NULL)
        THEN
           ctpgescode := letitre.ges_code;
        ELSE
           ctpgescode := gescodecompta;
        END IF;

        -- recette_ctrl_planco_ctp on force le 181
        INSERT INTO titre_brouillard
                    (ecd_ordre, exe_ordre, ges_code, pco_num, tib_montant,
                     tib_operation, tib_ordre, tib_sens, tit_id, rec_id)
           SELECT NULL,                                          --ECD_ORDRE,
                       recettectrlplanco.exe_ordre,              --EXE_ORDRE,
                                                   ctpgescode,    --GES_CODE,
                                                              '181',
                                                                    --PCO_NUM
                  ABS (rpcoctp_ttc_saisie),                    --TIB_MONTANT,
                                           'VISA TITRE',     --TIB_OPERATION,
                  titre_brouillard_seq.NEXTVAL,                  --TIB_ORDRE,
                                               inverser_sens (lesens),
                                                                  --TIB_SENS,
                                                                      titid,
                                                                    --TIT_ID,
                  recid
             FROM jefy_recette.recette_ctrl_planco_ctp
            WHERE rpco_id = recettectrlplanco.rpco_id;
     END LOOP;

     CLOSE c_recettes;
  END;


  -- outils
  FUNCTION inverser_sens_orv (tboordre INTEGER, sens VARCHAR)
     RETURN VARCHAR
  IS
     cpt   INTEGER;
  BEGIN
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
-- (meme dans le cas des SACD de m.....)
     SELECT COUNT (*)
       INTO cpt
       FROM type_bordereau
      WHERE tbo_sous_type = 'REVERSEMENTS' AND tbo_ordre = tboordre;

     IF (cpt != 0)
     THEN
        IF (sens = 'C')
        THEN
           RETURN 'D';
        ELSE
           RETURN 'C';
        END IF;
     END IF;

     RETURN sens;
  END;

  FUNCTION recup_gescode (abrid INTEGER)
     RETURN VARCHAR
  IS
     gescode   bordereau.ges_code%TYPE;
  BEGIN
     SELECT DISTINCT ges_code
                INTO gescode
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN gescode;
  END;

  FUNCTION recup_utlordre (abrid INTEGER)
     RETURN INTEGER
  IS
     utlordre   bordereau.utl_ordre%TYPE;
  BEGIN
     SELECT DISTINCT utl_ordre
                INTO utlordre
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN utlordre;
  END;

  FUNCTION recup_exeordre (abrid INTEGER)
     RETURN INTEGER
  IS
     exeordre   bordereau.exe_ordre%TYPE;
  BEGIN
     SELECT DISTINCT exe_ordre
                INTO exeordre
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN exeordre;
  END;

  FUNCTION recup_tboordre (abrid INTEGER)
     RETURN INTEGER
  IS
     tboordre   bordereau.tbo_ordre%TYPE;
  BEGIN
     SELECT DISTINCT tbo_ordre
                INTO tboordre
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN tboordre;
  END;

  FUNCTION recup_groupby (abrid INTEGER)
     RETURN VARCHAR
  IS
     abrgroupby   abricot_bord_selection.abr_group_by%TYPE;
  BEGIN
     SELECT DISTINCT abr_group_by
                INTO abrgroupby
                FROM abricot_bord_selection
               WHERE abr_id = abrid;

     RETURN abrgroupby;
  END;

  FUNCTION inverser_sens (sens VARCHAR)
     RETURN VARCHAR
  IS
  BEGIN
     IF sens = 'D'
     THEN
        RETURN 'C';
     ELSE
        RETURN 'D';
     END IF;
  END;

  PROCEDURE numeroter_bordereau (borid INTEGER)
  IS
     cpt_mandat   INTEGER;
     cpt_titre    INTEGER;
  BEGIN
     SELECT COUNT (*)
       INTO cpt_mandat
       FROM mandat
      WHERE bor_id = borid;

     SELECT COUNT (*)
       INTO cpt_titre
       FROM titre
      WHERE bor_id = borid;

     IF cpt_mandat + cpt_titre = 0
     THEN
        raise_application_error (-20001, 'Bordereau  vide');
     ELSE
        numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
        numerotationobject.numeroter_mandat (borid);
-- boucle titre
        numerotationobject.numeroter_titre (borid);
     END IF;
  END;

  FUNCTION traiter_orgid (orgid INTEGER, exeordre INTEGER)
     RETURN INTEGER
  IS
     topordre     INTEGER;
     cpt          INTEGER;
     orilibelle   origine.ori_libelle%TYPE;
     convordre    INTEGER;
  BEGIN
     IF orgid IS NULL
     THEN
        RETURN NULL;
     END IF;

     SELECT COUNT (*)
       INTO cpt
       FROM accords.convention_limitative
      WHERE org_id = orgid AND exe_ordre = exeordre;

     IF cpt > 0
     THEN
        -- recup du type_origine CONVENTION--
        SELECT top_ordre
          INTO topordre
          FROM type_operation
         WHERE top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

        SELECT DISTINCT con_ordre
                   INTO convordre
                   FROM accords.convention_limitative
                  WHERE org_id = orgid AND exe_ordre = exeordre;

        SELECT (exe_ordre || '-' || LPAD (con_index, 5, '0') || ' '
                || con_objet
               )
          INTO orilibelle
          FROM accords.contrat
         WHERE con_ordre = convordre;
     ELSE
        SELECT COUNT (*)
          INTO cpt
          FROM jefy_admin.organ
         WHERE org_id = orgid AND org_lucrativite = 1;

        IF cpt = 1
        THEN
           -- recup du type_origine OPERATION LUCRATIVE --
           SELECT top_ordre
             INTO topordre
             FROM type_operation
            WHERE top_libelle = 'OPERATION LUCRATIVE';

           --le libelle utilisateur pour le suivie en compta --
           SELECT org_ub || '-' || org_cr || '-' || org_souscr
             INTO orilibelle
             FROM jefy_admin.organ
            WHERE org_id = orgid;
        ELSE
           RETURN NULL;
        END IF;
     END IF;

-- l origine est t elle deja  suivie --
     SELECT COUNT (*)
       INTO cpt
       FROM origine
      WHERE ori_key_name = 'ORG_ID'
        AND ori_entite = 'JEFY_ADMIN.ORGAN'
        AND ori_key_entite = orgid;

     IF cpt >= 1
     THEN
        SELECT ori_ordre
          INTO cpt
          FROM origine
         WHERE ori_key_name = 'ORG_ID'
           AND ori_entite = 'JEFY_ADMIN.ORGAN'
           AND ori_key_entite = orgid
           AND ROWNUM = 1;
     ELSE
        SELECT origine_seq.NEXTVAL
          INTO cpt
          FROM DUAL;

        INSERT INTO origine
                    (ori_entite, ori_key_name, ori_libelle, ori_ordre,
                     ori_key_entite, top_ordre
                    )
             VALUES ('JEFY_ADMIN', 'ORG_ID', orilibelle, cpt,
                     orgid, topordre
                    );
     END IF;

     RETURN cpt;
  END;

  PROCEDURE controle_bordereau (borid INTEGER)
  IS
     ttc             maracuja.titre.tit_ttc%TYPE;
     detailttc       maracuja.titre.tit_ttc%TYPE;
     ordottc         maracuja.titre.tit_ttc%TYPE;
     debit           maracuja.titre.tit_ttc%TYPE;
     credit          maracuja.titre.tit_ttc%TYPE;
     cpt             INTEGER;
     MESSAGE         VARCHAR2 (50);
     messagedetail   VARCHAR2 (50);
  BEGIN
     SELECT COUNT (*)
       INTO cpt
       FROM maracuja.titre
      WHERE bor_id = borid;

     IF cpt = 0
     THEN
-- somme des maracuja.titre
        SELECT SUM (man_ttc)
          INTO ttc
          FROM maracuja.mandat
         WHERE bor_id = borid;

--somme des maracuja.recette
        SELECT SUM (d.dep_ttc)
          INTO detailttc
          FROM maracuja.mandat m, maracuja.depense d
         WHERE m.man_id = d.man_id AND m.bor_id = borid;

-- la somme des credits
        SELECT SUM (mab_montant)
          INTO credit
          FROM maracuja.mandat m, maracuja.mandat_brouillard mb
         WHERE bor_id = borid AND m.man_id = mb.man_id AND mb.mab_sens = 'C';

-- la somme des debits
        SELECT SUM (mab_montant)
          INTO debit
          FROM maracuja.mandat m, maracuja.mandat_brouillard mb
         WHERE bor_id = borid AND m.man_id = mb.man_id AND mb.mab_sens = 'D';

-- somme des jefy.recette
        SELECT SUM (d.dpco_ttc_saisie)
          INTO ordottc
          FROM maracuja.mandat m, jefy_depense.depense_ctrl_planco d
         WHERE m.man_id = d.man_id AND m.bor_id = borid;

        MESSAGE := ' mandats ';
        messagedetail := ' depenses ';
     ELSE
-- somme des maracuja.titre
        SELECT SUM (tit_ttc)
          INTO ttc
          FROM maracuja.titre
         WHERE bor_id = borid;

--somme des maracuja.recette
        SELECT SUM (r.rec_monttva + r.rec_mont)
          INTO detailttc
          FROM maracuja.titre t, maracuja.recette r
         WHERE t.tit_id = r.tit_id AND t.bor_id = borid;

-- la somme des credits
        SELECT SUM (tib_montant)
          INTO credit
          FROM maracuja.titre t, maracuja.titre_brouillard tb
         WHERE bor_id = borid AND t.tit_id = tb.tit_id AND tb.tib_sens = 'C';

-- la somme des debits
        SELECT SUM (tib_montant)
          INTO debit
          FROM maracuja.titre t, maracuja.titre_brouillard tb
         WHERE bor_id = borid AND t.tit_id = tb.tit_id AND tb.tib_sens = 'D';

-- somme des jefy.recette
        SELECT SUM (r.rpco_ttc_saisie)
          INTO ordottc
          FROM maracuja.titre t, jefy_recette.recette_ctrl_planco r
         WHERE t.tit_id = r.tit_id AND t.bor_id = borid;

        MESSAGE := ' titres ';
        messagedetail := ' recettes ';
     END IF;

-- la somme des credits = sommes des debits
     IF (NVL (debit, 0) != NVL (credit, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' :  debit <> credit : '
                                 || debit
                                 || ' '
                                 || credit
                                );
     END IF;

-- la somme des credits = sommes des debits
     IF (NVL (debit, 0) != NVL (credit, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' :  ecriture <> budgetaire : '
                                 || debit
                                 || ' '
                                 || ttc
                                );
     END IF;

-- somme des maracuja.titre = somme des maracuja.recette
     IF (NVL (ttc, 0) != NVL (detailttc, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' : montant des '
                                 || MESSAGE
                                 || ' <>  du montant des '
                                 || messagedetail
                                 || ' :'
                                 || ttc
                                 || ' '
                                 || detailttc
                                );
     END IF;

-- somme des jefy.recette = somme des maracuja.recette
     IF (NVL (ttc, 0) != NVL (ordottc, 0))
     THEN
        raise_application_error (-20001,
                                    'PROBLEME DE '
                                 || MESSAGE
                                 || ' : montant des '
                                 || MESSAGE
                                 || ' <>  du montant ordonnateur des '
                                 || messagedetail
                                 || ' :'
                                 || ttc
                                 || ' '
                                 || ordottc
                                );
     END IF;

     bordereau_abricot.ctrl_date_exercice (borid);
  END;


   -- Controle la coherence des deux bordereaux de prestations internes (dep = rec)
   procedure ctrl_bordereaux_PI(borIdDep integer, borIdRec integer) is
       flag integer;
       nbDep integer;
       nbRec integer;
             manId mandat.man_id%type;
       titId titre.tit_id%type;
       tmpMandat mandat%rowtype;
       tmpTitre titre%rowtype;
       tmpPrest integer;
       montantDep mandat.man_ht%type;
       montantRec titre.tit_ht%type;
             cursor prests is
           select distinct prest_id from (
               select prest_id from mandat where bor_id = borIdDep
               union
               select prest_id from titre where bor_id = borIdRec
          );
                                    begin
       if (borIdDep is null) then  raise_application_error (-20001, 'Reference au bordereau de depense interne nulle.'); end if;
       if (borIdRec is null) then  raise_application_error (-20001, 'Reference au bordereau de recette interne nulle.'); end if;
         -- verifier qu'il s'agit bien de bordereaux de PI
       select count(*) into flag from bordereau where tbo_ordre=201 and bor_id=borIdDep;
       if (flag = 0) then  raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de depense interne.'); end if;
         select count(*) into flag from bordereau where tbo_ordre=200 and bor_id=borIdRec;
       if (flag = 0) then  raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de recette interne.'); end if;
         -- comparer le nombre de titres et de mandats
       select count(*) into nbDep from mandat where bor_id=borIdDep;
       select count(*) into nbRec from titre where bor_id=borIdRec;
             if (nbDep = 0) then
           raise_application_error (-20001, 'Aucun mandat trouve sur le bordereau');
       end if;
                   if (nbDep <> nbRec) then
           raise_application_error (-20001, 'Nombre de mandats different du nombre de titres. ' || 'Mandats : ' || nbDep || ' / Titres : '|| nbRec);
       end if;
                  OPEN prests;

     LOOP
        FETCH prests
         INTO tmpPrest;
        EXIT WHEN prests%NOTFOUND;
               select count(*) into nbDep from mandat where prest_id=tmpPrest and bor_id=borIdDep;
        select count(*) into nbRec from titre where prest_id=tmpPrest  and bor_id=borIdRec;                if (nbDep <> nbRec) then  raise_application_error (-20001, 'Incoherence : Nombre de titres ('|| nbRec ||') different du nombre de mandats ('|| nbDep ||') ('||'prest_id='||tmpPrest||')'); end if;
               select sum(man_ht) into montantDep from mandat where prest_id=tmpPrest and bor_id=borIdDep;
        select sum(tit_ht) into montantRec from titre where prest_id=tmpPrest  and bor_id=borIdRec;                if (montantDep <> montantRec) then  raise_application_error (-20001, 'Incoherence : Montant des titres ('|| montantRec ||') different du montant des mandats  ('|| montantDep ||') ('||'prest_id='||tmpPrest||')'); end if;

     END LOOP;
     CLOSE prests;


     select sum(man_ht) into montantDep from mandat where bor_id=borIdDep;      select sum(tit_ht) into montantRec from titre where bor_id=borIdDep;
       if (montantDep <> montantRec) then  raise_application_error (-20001, 'Incoherence : Montant total des titres ('|| montantRec ||') different du montant total des mandats  ('|| montantDep ||')'); end if;

     end;





  PROCEDURE get_recette_prelevements (titid INTEGER)
  IS
     cpt                      INTEGER;
     facture_titre_data       prestation.facture_titre%ROWTYPE;
--     client_data              prelev.client%ROWTYPE;
     oriordre                 INTEGER;
     modordre                 INTEGER;
     recid                    INTEGER;
     echeid                   INTEGER;
     echeancier_data          jefy_echeancier.echeancier%ROWTYPE;
     echeancier_prelev_data   jefy_echeancier.echeancier_prelev%ROWTYPE;
     facture_data             jefy_recette.facture_budget%ROWTYPE;
     personne_data            grhum.v_personne%ROWTYPE;
     premieredate             DATE;
  BEGIN
-- verifier s il existe un echancier pour ce titre
     SELECT COUNT (*)
       INTO cpt
       FROM jefy_recette.recette_ctrl_planco pco,
            jefy_recette.recette r,
            jefy_recette.facture f
      WHERE pco.tit_id = titid
        AND pco.rec_id = r.rec_id
        AND r.fac_id = f.fac_id
        AND eche_id IS NOT NULL
        AND r.rec_id_reduction IS NULL;

     IF (cpt != 1)
     THEN
        RETURN;
     END IF;

-- recup du eche_id / ech_id
     SELECT eche_id
       INTO echeid
       FROM jefy_recette.recette_ctrl_planco pco,
            jefy_recette.recette r,
            jefy_recette.facture f
      WHERE pco.tit_id = titid
        AND pco.rec_id = r.rec_id
        AND r.fac_id = f.fac_id
        AND eche_id IS NOT NULL
        AND r.rec_id_reduction IS NULL;

-- recup du des infos du prelevements
     SELECT *
       INTO echeancier_data
       FROM jefy_echeancier.echeancier
      WHERE ech_id = echeid;

     SELECT *
       INTO echeancier_prelev_data
       FROM jefy_echeancier.echeancier_prelev
      WHERE ech_id = echeid;

     SELECT *
       INTO facture_data
       FROM jefy_recette.facture_budget
      WHERE eche_id = echeid;

     SELECT *
       INTO personne_data
       FROM grhum.v_personne
      WHERE pers_id = facture_data.pers_id;

     SELECT echd_date_prevue
       INTO premieredate
       FROM jefy_echeancier.echeancier_detail
      WHERE echd_numero = 1 AND ech_id = echeid;

     SELECT rec_id
       INTO recid
       FROM recette
      WHERE tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
  SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
  IF (cpt=0) THEN
        RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
  END IF;
  IF (cpt>1) THEN
        RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
  END IF;

  SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

  UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

     -- recup ??
     oriordre :=
        gestionorigine.traiter_orgid (facture_data.org_id,
                                      facture_data.exe_ordre
                                     );

     INSERT INTO maracuja.echeancier
                 (eche_autoris_signee, fou_ordre_client, con_ordre,
                  eche_date_1ere_echeance, eche_date_creation,
                  eche_date_modif, eche_echeancier_ordre,
                  eche_etat_prelevement, ft_ordre, eche_libelle,
                  eche_montant,
                  eche_montant_en_lettres,
                  eche_nombre_echeances, eche_numero_index,
                  org_ordre, prest_ordre, eche_prise_en_charge,
                  eche_ref_facture_externe, eche_supprime, exe_ordre,
                  tit_id, rec_id, tit_ordre, ori_ordre, pers_id,
                  org_id,
                  pers_description
                 )
          VALUES ('O',                                  --ECHE_AUTORIS_SIGNEE
                      facture_data.fou_ordre,              --FOU_ORDRE_CLIENT
                                             NULL,
                                     --echancier_data.CON_ORDRE  ,--CON_ORDRE
                  premieredate,
              --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                               SYSDATE,
                        --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                  SYSDATE,    --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                          echeancier_data.ech_id,
                  --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                  'V',
                  --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                      facture_data.fac_id,
                                       --echancier_data.FT_ORDRE  ,--FT_ORDRE
                                          echeancier_data.ech_libelle,
                                      --echancier_data.LIBELLE,--ECHE_LIBELLE
                  echeancier_data.ech_montant,                 --ECHE_MONTANT
                  echeancier_data.ech_montant_lettres,
                                                    --ECHE_MONTANT_EN_LETTRES
                  echeancier_data.ech_nb_echeances,   --ECHE_NOMBRE_ECHEANCES
                                                   echeancier_data.ech_id,
                         --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                  facture_data.org_id,
                                    --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                                      NULL,
                                --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                                           'O',        --ECHE_PRISE_EN_CHARGE
                  facture_data.fac_lib,
            --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                                       'N',                   --ECHE_SUPPRIME
                                           facture_data.exe_ordre,
                                                                  --EXE_ORDRE
                  titid, recid,                                     --REC_ID,
                               -titid, oriordre,                 --ORI_ORDRE,
                                                personne_data.pers_id,
                                            --CLIENT_data.pers_id  ,--PERS_ID
                  facture_data.org_id,          --orgid a faire plus tard....
                  personne_data.pers_libelle           --    PERS_DESCRIPTION
                 );

     INSERT INTO maracuja.prelevement
                 (eche_echeancier_ordre, reco_ordre, fou_ordre,
                  prel_commentaire, prel_date_modif, prel_date_prelevement,
                  prel_prelev_date_saisie, prel_prelev_etat,
                  prel_numero_index, prel_prelev_montant, prel_prelev_ordre,
                  rib_ordre, prel_etat_maracuja)
        SELECT ech_id,                                --ECHE_ECHEANCIER_ORDRE
                      NULL,                                 --PREL_FICP_ORDRE
                           facture_data.fou_ordre,                --FOU_ORDRE
                                                  echd_commentaire,
                                                           --PREL_COMMENTAIRE
                                                                   SYSDATE,
                                               --DATE_MODIF,--PREL_DATE_MODIF
               echd_date_prevue,                      --PREL_DATE_PRELEVEMENT
                                SYSDATE,         --,--PREL_PRELEV_DATE_SAISIE
                                        'ATTENTE',         --PREL_PRELEV_ETAT
                                                  echd_numero,
                                                          --PREL_NUMERO_INDEX
                                                              echd_montant,
                                                        --PREL_PRELEV_MONTANT
               echd_id,                                   --PREL_PRELEV_ORDRE
                       echeancier_prelev_data.rib_ordre_debiteur,
                             --RIB_ORDRE
               'ATTENTE'                                 --PREL_ETAT_MARACUJA
          FROM jefy_echeancier.echeancier_detail
         WHERE ech_id = echeancier_data.ech_id;
  END;

  PROCEDURE ctrl_date_exercice (borid INTEGER)
  IS
     exeordre   INTEGER;
     annee      INTEGER;
  BEGIN
     SELECT TO_CHAR (bor_date_creation, 'YYYY'), exe_ordre
       INTO annee, exeordre
       FROM bordereau
      WHERE bor_id = borid AND exe_ordre >= 2007;

     IF exeordre <> annee
     THEN
        UPDATE bordereau
           SET bor_date_creation =
                  TO_DATE ('31/12/' || exe_ordre || ' 12:00:00',
                           'DD/MM/YYYY HH24:MI:SS'
                          )
         WHERE bor_id = borid;

        UPDATE mandat
           SET man_date_remise =
                  TO_DATE ('31/12/' || exe_ordre || ' 12:00:00',
                           'DD/MM/YYYY HH24:MI:SS'
                          )
         WHERE bor_id = borid;

        UPDATE titre
           SET tit_date_remise =
                  TO_DATE ('31/12/' || exe_ordre || ' 12:00:00',
                           'DD/MM/YYYY HH24:MI:SS'
                          )
         WHERE bor_id = borid;
     END IF;
  END;
  
  
  
  
-- GET_GES_CODE_FOR_MAN_ID
-- Renvoie la COMPOSANTE a prendre en compte en fonction du mandat. AGENCE ou COMPOSANTE.  
FUNCTION  get_ges_code_for_man_id(manid NUMBER)
  RETURN comptabilite.ges_code%TYPE
   IS

   current_mandat mandat%ROWTYPE;

   pconumsacd gestion_exercice.pco_num_185%TYPE;    

   visa_mode_paiement planco_visa.pvi_contrepartie_gestion%TYPE;    
   visa_planco planco_visa.pvi_contrepartie_gestion%TYPE;    

   code_agence_comptable comptabilite.ges_code%TYPE;
      
   BEGIN

    select ges_code into code_agence_comptable from comptabilite;

    select * into current_mandat from mandat where man_id = manid;
    
    -- SACD -- S'il s'agit d'un SACD on renvoie la composante associee au mandat.
    select pco_num_185 into pconumsacd from gestion_exercice where exe_ordre = current_mandat.exe_ordre and ges_code = current_mandat.ges_code;

    if (pconumsacd is not null)
    then

        return current_mandat.ges_code;

    else    -- Pas de SACD, on verifie le parametrage du Mode de Paiement (Mode_Paiement) puis du Compte de classe 6 (Planco_Visa).
    
        -- Si le parametrage du mode de paiement est renseigne , il est prioritaire
        select mod_contrepartie_gestion into visa_mode_paiement from mode_paiement where mod_ordre = current_mandat.mod_ordre;
        if (visa_mode_paiement is not null)
        then
            -- Parametres : AGENCE ou COMPOSANTE
            if (visa_mode_paiement = 'AGENCE')
            then

                return code_agence_comptable;

            else    -- COMPOSANTE

                return current_mandat.ges_code;

            end if;
        
        else    -- Pas de parametrage du mode de paiement, on prend celui du compte (PCO_NUM)
        
            select pvi_contrepartie_gestion into visa_planco from planco_visa where pco_num_ordonnateur = current_mandat.pco_num and exe_ordre = current_mandat.exe_ordre;

            if (visa_planco = 'AGENCE') 
            then
        
                return code_agence_comptable;

            else    -- COMPOSANTE

                return current_mandat.ges_code;

            end if;
                
        end if;

    end if;
   
   END;
     
   
   
  function getFournisNom (fouOrdre integer) return varchar is
    founom varchar2(200);
  begin
    founom := '';
     SELECT substr(nom || ' ' || prenom,1,200)
          INTO founom
          FROM v_fournis_light
         WHERE fou_ordre = fouordre;
    return founom;
  end; 
  
  
END;
/

--**************---

CREATE OR REPLACE PACKAGE MARACUJA."BORDEREAU_ABRICOT" AS

/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- DSI PARIS 5
 -- rivalland frederic

/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
 bordereau_1R1T
 bordereau_NR1T
 bordereau_1D1M
 bordereau_1D1M1R1T
 ndep_mand_org_fou_rib_pco (bordereau_ND1M)
 ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
 ndep_mand_fou_rib_pco (bordereau_ND1M)
 ndep_mand_fou_rib_pco_mod (bordereau_ND1M)

abr_etat='ATTENTE' qd la selection n est pas sur un bordereau
abr_etat='TRAITE' qd la selection est sur le bordereau
*/

-- version du 02/03/2007
-- version du 01/10/2009 -- ajout de controles sur la generation des bd de PI

procedure creer_bordereau (abrid integer);
procedure viser_bordereau_rejet (brjordre integer);

function get_selection_id (info varchar ) return integer ;
function get_selection_borid (abrid integer) return integer ;
procedure set_selection_id (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a06tboordre integer,a07abrgroupby varchar,a08gescode varchar);
procedure set_selection_intern (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);
procedure set_selection_paye (a01abrid integer,a02lesdepid varchar,a03lesrecid varchar ,a04utlordre integer,a05exeordre integer ,a07abrgroupby varchar,a08gescodemandat varchar,a09gescodetitre varchar);

-- creer bordereau (tbo_ordre) + numerotation
function get_num_borid (tboordre integer,exeordre integer,gescode varchar,utlordre integer ) return integer;

-- GES_CODE a prendre en compte en fonction du mandat.
FUNCTION  get_ges_code_for_man_id(manid NUMBER)
  RETURN comptabilite.ges_code%TYPE;

-- les algo de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
procedure bordereau_1R1T(abrid integer,monborid integer);
procedure bordereau_NR1T(abrid integer,monborid integer);
procedure bordereau_ND1M(abrid integer,monborid integer);
procedure bordereau_1D1M(abrid integer,monborid integer);
procedure bordereau_1D1M1R1T(abrid integer,boridep integer,boridrec integer);

-- les mandats et titres
function set_mandat_depense (dpcoid integer,borid integer) return integer;
function set_mandat_depenses (lesdpcoid varchar,borid integer) return integer;
function set_titre_recette (rpcoid integer,borid integer) return integer;
function set_titre_recettes (lesrpcoid varchar,borid integer) return integer;


--function ndep_mand_org_fou_rib_pco (abrid integer,borid integer) return integer;
function ndep_mand_org_fou_rib_pco_mod  (abrid integer,borid integer) return integer;
--function ndep_mand_fou_rib_pco  (abrid integer,borid integer) return integer;
function ndep_mand_fou_rib_pco_mod  (abrid integer,borid integer) return integer;


-- procedures de verifications des etats
function selection_valide (abrid integer) return integer;
function recette_valide (recid integer) return integer;
function depense_valide (depid integer) return integer;
function verif_bordereau_selection(borid integer,abrid integer) return integer;


-- procedures de locks de transaction
procedure lock_mandats;
procedure lock_titres;

-- procedure de recuperation des donn?e ordonnateur
--PROCEDURE get_depense_jefy_depense (manid INTEGER,utlordre INTEGER);
PROCEDURE get_depense_jefy_depense (manid INTEGER);
PROCEDURE get_recette_jefy_recette (titid INTEGER);
procedure  Get_recette_prelevements (titid INTEGER);

-- procedures du brouillard
PROCEDURE set_mandat_brouillard(manid INTEGER);
PROCEDURE set_mandat_brouillard_intern(manid INTEGER);
--PROCEDURE maj_plancomptable_mandat (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

PROCEDURE Set_Titre_Brouillard(titid INTEGER);
PROCEDURE Set_Titre_Brouillard_intern(titid INTEGER);
--PROCEDURE maj_plancomptable_titre (nature VARCHAR,libelle VARCHAR,pconum VARCHAR);

-- outils
function inverser_sens_orv (tboordre integer,sens varchar) return varchar;
function recup_gescode (abrid integer) return varchar;
function recup_utlordre (abrid integer) return integer;
function recup_exeordre (abrid integer) return integer;
function recup_tboordre (abrid integer) return integer;
function recup_groupby (abrid integer) return varchar;
function traiter_orgid (orgid integer,exeordre integer) return integer;
function inverser_sens (sens varchar) return varchar;
function getFournisNom (fouOrdre integer) return varchar;
function recup_tcdsect (manid integer) return varchar;

-- apres creation des bordereaux
procedure numeroter_bordereau(borid integer);
procedure controle_bordereau(borid integer);
PROCEDURE ctrl_date_exercice(borid INTEGER);
procedure ctrl_bordereaux_PI(borIdDep integer, borIdRec integer);


END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."BORDEREAU_ABRICOT"
as
   procedure creer_bordereau (abrid integer)
   is
      cpt            integer;
      abrgroupby     abricot_bord_selection.abr_group_by%type;
      monborid_dep   integer;
      monborid_rec   integer;
      flag           integer;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = monborid_dep;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = monborid_rec;

      tmpmandid      integer;
      tmptitid       integer;
      tboordre       integer;
   begin
-- est ce une selection vide ???
      select count (*)
      into   cpt
      from   abricot_bord_selection
      where  abr_id = abrid;

      if cpt != 0 then
/*
TBOORDRE      -> MARACUJA.TYPE_BORDEREAU
ABR_GROUP_BY  -> peut prendre les valeurs suivantes :
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

         -- verifier l etat de l exercice
         select count (*)
         into   flag
         from   jefy_admin.exercice
         where  exe_ordre = recup_exeordre (abrid) and exe_stat in ('O', 'R');

         if (flag = 0) then
            raise_application_error (-20001, 'L''exercice ' || recup_exeordre (abrid) || ' n''est pas ouvert.');
         end if;

         -- recup du group by pour traiter les cursors
         abrgroupby := recup_groupby (abrid);

         if (abrgroupby = 'bordereau_1R1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1r1t (abrid, monborid_rec);
         end if;

         if (abrgroupby = 'bordereau_NR1T') then
            monborid_rec := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nr1t (abrid, monborid_rec);

-- controle RA
            select count (*)
            into   cpt
            from   titre
            where  ori_ordre is not null and bor_id = monborid_rec;

            if cpt != 0 then
               raise_application_error (-20001, 'Impossiblde traiter une recette sur convention affectee dans un bordereau collectif !');
            end if;
         end if;

         if (abrgroupby = 'bordereau_1D1M') then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_1d1m (abrid, monborid_dep);
         end if;

/*
IF (abrgroupby = 'bordereau_1D1M1R1T') THEN
monborid_dep := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

monborid_rec := get_num_borid(
recup_tboordre(abrid),
recup_exeordre(abrid),
recup_gescode(abrid),
recup_utlordre(abrid)
);

bordereau_1D1M(abrid,monborid_dep);
bordereau_1R1T(abrid,monborid_rec);


END IF;
*/
         if (abrgroupby not in ('bordereau_1R1T', 'bordereau_NR1T', 'bordereau_1D1M', 'bordereau_1D1M1R1T')) then
            monborid_dep := get_num_borid (recup_tboordre (abrid), recup_exeordre (abrid), recup_gescode (abrid), recup_utlordre (abrid));
            bordereau_nd1m (abrid, monborid_dep);
         end if;

         if (monborid_dep is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_dep);

            open lesmandats;

            loop
               fetch lesmandats
               into  tmpmandid;

               exit when lesmandats%notfound;
               get_depense_jefy_depense (tmpmandid);
            end loop;

            close lesmandats;

            controle_bordereau (monborid_dep);
         end if;

         if (monborid_rec is not null) then
            bordereau_abricot.numeroter_bordereau (monborid_rec);

            open lestitres;

            loop
               fetch lestitres
               into  tmptitid;

               exit when lestitres%notfound;
               -- recup du brouillard
               get_recette_jefy_recette (tmptitid);
               set_titre_brouillard (tmptitid);
               get_recette_prelevements (tmptitid);
            end loop;

            close lestitres;

            controle_bordereau (monborid_rec);
         end if;

-- maj de l etat dans la selection
         if (monborid_dep is not null or monborid_rec is not null) then
            if monborid_rec is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_rec
                where abr_id = abrid;
            end if;

            if monborid_dep is not null then
               update abricot_bord_selection
                  set abr_etat = 'TRAITE',
                      bor_id = monborid_dep
                where abr_id = abrid;

               select tbo_ordre
               into   tboordre
               from   bordereau
               where  bor_id = monborid_dep;

-- pour les bordereaux de papaye on retravaille le brouillard
               if tboordre = 3 then
                  bordereau_abricot_paye.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de papaye on retravaille le brouillard
               if tboordre = 18 then
                  bordereau_abricot_paye.basculer_bouillard_paye_orv (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 19 then
                  bordereau_abricot_paye.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de regul de papaye on retravaille le brouillard
               if tboordre = 22 then
                  bordereau_abricot_paf.basculer_bouillard_paye_regul (monborid_dep);
               end if;

-- pour les bordereaux de PAF on retravaille le brouillard
               if tboordre = 20 then
                  bordereau_abricot_paf.basculer_bouillard_paye (monborid_dep);
               end if;

-- pour les bordereaux d'orv de PAF on retravaille le brouillard
               if tboordre = 21 then
                  bordereau_abricot_paf.basculer_bouillard_paye_orv (monborid_dep);
               end if;
           -- pour les bordereaux de recette de PAF on retravaille le brouillard
--  if tboordre = -999  then
--   bordereau_abricot_paf.basculer_bouillard_paye_recettte(monborid_rec);
--  end if;
            end if;
         end if;
      end if;
   end;

   procedure viser_bordereau_rejet (brjordre integer)
   is
      cpt              integer;
      flag             integer;
      manid            maracuja.mandat.man_id%type;
      titid            maracuja.titre.tit_id%type;
      tboordre         integer;
      reduction        integer;
      utlordre         integer;
      dpcoid           integer;
      recid            integer;
      depsuppression   varchar2 (20);
      rpcoid           integer;
      recsuppression   varchar2 (20);
      exeordre         integer;
      depid            integer;
      boridinitial     integer;

      cursor mandats
      is
         select man_id
         from   maracuja.mandat
         where  brj_ordre = brjordre;

      cursor depenses
      is
         select dep_ordre,
                dep_suppression
         from   maracuja.depense
         where  man_id = manid;

      cursor titres
      is
         select tit_id
         from   maracuja.titre
         where  brj_ordre = brjordre;

      cursor recettes
      is
         select rec_ordre,
                rec_suppression
         from   maracuja.recette
         where  tit_id = titid;

      deliq            integer;
   begin
      -- verifier si le bordereau est deja vise
      select count (*)
      into   flag
      from   bordereau_rejet
      where  brj_etat = 'VISE' and brj_ordre = brjordre;

      if (flag > 0) then
         raise_application_error (-20001, 'Ce bordereau a déjà été visé');
      end if;

      open mandats;

      loop
         fetch mandats
         into  manid;

         exit when mandats%notfound;

         select bor_id
         into   boridinitial
         from   mandat
         where  man_id = manid;

         -- memoriser le bor-id du mandat
         open depenses;

         loop
            fetch depenses
            into  dpcoid,
                  depsuppression;

            exit when depenses%notfound;
            -- casser le liens des mand_id dans depense_ctrl_planco
              -- supprimer le liens compteble <-> depense dans l inventaire
            jefy_depense.abricot.upd_depense_ctrl_planco (dpcoid, null);

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_depense.depense_ctrl_planco
            where  dpco_id = dpcoid;

-- suppression de la depense demand?e par la personne qui a vis? et pas un bordereau de prestation interne depense 201
            if depsuppression = 'OUI' and tboordre != 201 then
--  select max(utl_ordre) into utlordre from jefy_depense.depense_budget jdb,jefy_depense.depense_ctrl_planco jpbp
--  where jpbp.dep_id = jdb.dep_id
--  and dpco_id = dpcoid;
/*
 deliq:=jefy_depense.Get_Fonction('DELIQ');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=exeordre  AND
 uf.fon_ordre=deliq AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

   if utlordre is null then
   deliq:=jefy_depense.Get_Fonction('DELIQINV');

 SELECT max(utl_ordre ) into utlordre
 FROM jefy_depense.v_utilisateur_fonct uf, jefy_depense.v_utilisateur_fonct_exercice ufe, jefy_depense.v_exercice e
 WHERE ufe.uf_ordre=uf.uf_ordre
 AND ufe.exe_ordre=exeordre
 AND uf.fon_ordre=deliq
 AND ufe.exe_ordre=e.exe_ordre
 AND exe_stat_eng='R';
 end if;
*/
               select utl_ordre
               into   utlordre
               from   jefy_depense.depense_budget
               where  dep_id in (select dep_id
                                 from   jefy_depense.depense_ctrl_planco
                                 where  dpco_id = dpcoid);

               select dep_id
               into   depid
               from   jefy_depense.depense_ctrl_planco
               where  dpco_id = dpcoid;

               -- si cest le rejet d'un bordereau de paye
               if (tboordre = 18) then
                  jefy_paye.paye_reversement.viser_rejet_reversement (depid);
               end if;

               jefy_depense.abricot.del_depense_ctrl_planco (dpcoid, utlordre);
            end if;
         end loop;

         close depenses;
      end loop;

      close mandats;

      -- pour les bordereau de PAF, appeler la proc
      jefy_paf.paf_budget.viser_rejet_paf (boridinitial);
      jefy_paye.paye_budget.viser_rejet_papaye (boridinitial);

      open titres;

      loop
         fetch titres
         into  titid;

         exit when titres%notfound;

         open recettes;

         loop
            fetch recettes
            into  rpcoid,
                  recsuppression;

            exit when recettes%notfound;

-- casser le liens des tit_id dans recette_ctrl_planco
            select r.rec_id_reduction
            into   reduction
            from   jefy_recette.recette r, jefy_recette.recette_ctrl_planco rpco
            where  rpco.rpco_id = rpcoid and rpco.rec_id = r.rec_id;

            if reduction is not null then
               jefy_recette.api.upd_reduction_ctrl_planco (rpcoid, null);
            else
               jefy_recette.api.upd_recette_ctrl_planco (rpcoid, null);
            end if;

            select tbo_ordre,
                   exe_ordre
            into   tboordre,
                   exeordre
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = rpcoid;

-- GESTION DES SUPPRESSIONS
-- suppression de la recette demand?e par la personne qui a vis? et pas un bordereau de prestation interne recette 200
            if recsuppression = 'OUI' and tboordre != 200 then
               select utl_ordre
               into   utlordre
               from   jefy_recette.recette_budget
               where  rec_id in (select rec_id
                                 from   jefy_recette.recette_ctrl_planco
                                 where  rpco_id = rpcoid);

               select rec_id
               into   recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = rpcoid;

               if reduction is not null then
                  jefy_recette.api.del_reduction (recid, utlordre);
               else
                  jefy_recette.api.del_recette (recid, utlordre);
               end if;
            end if;
         end loop;

         close recettes;
      end loop;

      close titres;

-- on passe le brjordre a VISE
      update bordereau_rejet
         set brj_etat = 'VISE'
       where brj_ordre = brjordre;
   end;

   function get_selection_id (info varchar)
      return integer
   is
      selection   integer;
   begin
      select maracuja.abricot_bord_selection_seq.nextval
      into   selection
      from   dual;

      return selection;
   end;

   function get_selection_borid (abrid integer)
      return integer
   is
      borid   integer;
   begin
      select distinct bor_id
      into            borid
      from            maracuja.abricot_bord_selection
      where           abr_id = abrid;

      return borid;
   end;

   procedure set_selection_id (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a06tboordre integer, a07abrgroupby varchar, a08gescode varchar)
   is
      chaine     varchar (32000);
      premier    integer;
      tmpdepid   integer;
      tmprecid   integer;
      cpt        integer;
   begin
/*
bordereau_1R1T
bordereau_1D1M
bordereau_1D1M1R1T
ndep_mand_org_fou_rib_pco (bordereau_ND1M)
ndep_mand_org_fou_rib_pco_mod (bordereau_ND1M)
ndep_mand_fou_rib_pco (bordereau_ND1M)
ndep_mand_fou_rib_pco_mod (bordereau_ND1M)
*/

      -- traitement de la chaine des depid
      if a02lesdepid is not null or length (a02lesdepid) > 0 then
         chaine := a02lesdepid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdepid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         tmpdepid,   --DEP_ID
                         null,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      -- traitement de la chaine des recid
      if a03lesrecid is not null or length (a03lesrecid) > 0 then
         chaine := a03lesrecid;

         loop
            premier := 1;

            -- On recupere le depordre
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmprecid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

            insert into maracuja.abricot_bord_selection
                        (abr_id,
                         utl_ordre,
                         dep_id,
                         rec_id,
                         exe_ordre,
                         tbo_ordre,
                         abr_etat,
                         abr_group_by,
                         ges_code
                        )
            values      (a01abrid,   --ABR_ID
                         a04utlordre,   --ult_ordre
                         null,   --DEP_ID
                         tmprecid,   --REC_ID
                         a05exeordre,
                         --EXE_ORDRE
                         a06tboordre,   --TBO_ORDRE,
                         'ATTENTE',   --ABR_ETAT,
                         a07abrgroupby,
                         --,ABR_GROUP_BY,GES_CODE
                         a08gescode   --ges_code
                        );

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

      select count (*)
      into   cpt
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id in (select dep_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and man_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE FACTURE DEJA SUR BORDEREAU  !');
      end if;

      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id in (select rec_id
                         from   abricot_bord_selection
                         where  abr_id = a01abrid) and tit_id is not null;

      if cpt > 0 then
         raise_application_error (-20001, 'VOTRE SELECTION CONTIENT UNE RECETTE DEJA SUR BORDEREAU !');
      end if;

      bordereau_abricot.creer_bordereau (a01abrid);
   end;

   procedure set_selection_intern (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boriddep   bordereau.bor_id%type;
      boridrec   bordereau.bor_id%type;
      flag       integer;
   begin
-- ATENTION
-- tboordre : 200 recettes internes
-- tboordre : 201 mandats internes

      -- les mandats
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 201, 'bordereau_1D1M', a08gescodemandat);
-- les titres
      set_selection_id (-a01abrid, null, a03lesrecid, a04utlordre, a05exeordre, 200, 'bordereau_1R1T', a09gescodetitre);

      -- verifier que les bordereaux crees sont coherents entre eux
      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || a01abrid);
      end if;

      select max (bor_id)
      into   boriddep
      from   abricot_bord_selection
      where  abr_id = a01abrid;

      select count (*)
      into   flag
      from   (select distinct bor_id
              from            abricot_bord_selection
              where           abr_id = -a01abrid);

      if (flag <> 1) then
         raise_application_error (-20001, 'Plusieurs bordereaux trouves dans abricot_bord_selection pour abr_id=' || -a01abrid);
      end if;

      select max (bor_id)
      into   boridrec
      from   abricot_bord_selection
      where  abr_id = -a01abrid;

      -- verifier qu'on a 1 prest_id par titre/mandat
      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct tit_id) nb
              from     titre
              where    bor_id = boridrec
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs titres concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      select count (*)
      into   flag
      from   (select   prest_id,
                       count (distinct man_id) nb
              from     mandat
              where    bor_id = boriddep
              group by prest_id)
      where  nb > 1;

      if (flag > 0) then
         raise_application_error (-20001, 'Plusieurs mandats concernant la meme prestation ne peuvent etre integres sur un seul bordereau. Creez plusieurs bordereaux.');
      end if;

      ctrl_bordereaux_pi (boriddep, boridrec);
   end;

   procedure set_selection_paye (a01abrid integer, a02lesdepid varchar, a03lesrecid varchar, a04utlordre integer, a05exeordre integer, a07abrgroupby varchar, a08gescodemandat varchar, a09gescodetitre varchar)
   is
      boridtmp    integer;
      moisordre   integer;
   begin
/*
-- a07abrgroupby = mois
select mois_ordre into moisordre from jef_paye.paye_mois where mois_complet = a07abrgroupby;

-- CONTROLES
-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby);

if cpt = 0 then  raise_application_error (-20001,'PAS DE MANDATEMENT A EFFECTUER');  end if;

select mois_ordre into moisordre from papaye.paye_mois where mois_complet = a07abrgroupby;

-- peux t on mandater la composante --
select count(*) into cpt from jefy_depense.papaye_compta
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=moisordre and ETAT<>'LIQUIDEE';

if (cpt = 1) then
 raise_application_error (-20001,' MANDATEMENT DEJA EFFECTUE POUR LE MOIS DE "'||a07abrgroupby||'", composante : '||a08gescodemandat);
end if;
*/
-- ATENTION
-- tboordre : 3 salaires

      -- les mandats de papaye
      set_selection_id (a01abrid, a02lesdepid, null, a04utlordre, a05exeordre, 3, 'bordereau_1D1M', a08gescodemandat);
      boridtmp := get_selection_borid (a01abrid);
/*
-- maj de l etat de papaye_compta et du bor_ordre -
update jefy_depense.papaye_compta set bor_ordre=boridtmp, etat='MANDATEE'
where org_ordre=(select org_ordre from jefy_admin.organ where org_comp=a08gescodemandat and org_niv=2)
and mois_ordre=(select mois_ordre from papaye.paye_mois where mois_complet=a07abrgroupby) and ETAT='LIQUIDEE';
*/
-- Mise a jour des brouillards de paye pour le mois
--  maracuja.bordereau_papaye.maj_brouillards_payes(moisordre, boridtmp);

   -- bascule du brouillard de papaye

   -- les ORV ??????
--set_selection_id(-a01abrid ,null ,a03lesrecid  ,a04utlordre ,a05exeordre  ,200 ,'bordereau_1R1T' ,a09gescodetitre );
   end;

-- creer bordereau (tbo_ordre) + numerotation
   function get_num_borid (tboordre integer, exeordre integer, gescode varchar, utlordre integer)
      return integer
   is
      cpt      integer;
      borid    integer;
      bornum   integer;
   begin
-- creation du bor_id --
      select bordereau_seq.nextval
      into   borid
      from   dual;

-- creation du bordereau --
      bornum := -1;

      insert into bordereau
                  (bor_date_visa,
                   bor_etat,
                   bor_id,
                   bor_num,
                   bor_ordre,
                   exe_ordre,
                   ges_code,
                   tbo_ordre,
                   utl_ordre,
                   utl_ordre_visa,
                   bor_date_creation
                  )
      values      (null,   --BOR_DATE_VISA,
                   'VALIDE',   --BOR_ETAT,
                   borid,   --BOR_ID,
                   bornum,   --BOR_NUM,
                   -borid,   --BOR_ORDRE,
--a partir de 2007 il n existe plus de bor_ordre pour conserver le constraint je met -borid
                   exeordre,   --EXE_ORDRE,
                   gescode,   --GES_CODE,
                   tboordre,   --TBO_ORDRE,
                   utlordre,   --UTL_ORDRE,
                   null,   --UTL_ORDRE_VISA
                   sysdate
                  );

      return borid;
   end;

-- les algos de bordereaux
-- ex : 1R1T 1 recette pour 1 titre
-- ex : 1D1M 1 depense pour 1 mandat
-- ex : 1D1M N depenses pour 1 mandat
-- ex : 1R1T1D1M  pour les prestations interne 1 -> recette/depense 1 -> titre/mandat
   procedure bordereau_1r1t (abrid integer, monborid integer)
   is
      cpt          integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by r.pco_num, r.rec_id asc;
   begin
      open rec_tit;

      loop
         fetch rec_tit
         into  tmprecette;

         exit when rec_tit%notfound;
         cpt := set_titre_recette (tmprecette.rpco_id, monborid);
      end loop;

      close rec_tit;
   end;

   procedure bordereau_nr1t (abrid integer, monborid integer)
   is
      ht           number (12, 2);
      tva          number (12, 2);
      ttc          number (12, 2);
      pconumero    varchar (20);
      nbpieces     integer;
      cpt          integer;
      titidtemp    integer;
      tmprecette   jefy_recette.recette_ctrl_planco%rowtype;

-- curseur de regroupement
      cursor rec_tit_group_by
      is
         select   r.pco_num,
                  sum (r.rpco_ht_saisie),
                  sum (r.rpco_tva_saisie),
                  sum (r.rpco_ttc_saisie)
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         group by r.pco_num
         order by r.pco_num asc;

      cursor rec_tit
      is
         select   r.*
         from     abricot_bord_selection ab, jefy_recette.recette_ctrl_planco r
         where    r.rpco_id = ab.rec_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and r.pco_num = pconumero
         order by r.pco_num asc, r.rec_id;
   begin
      open rec_tit_group_by;

      loop
         fetch rec_tit_group_by
         into  pconumero,
               ht,
               tva,
               ttc;

         exit when rec_tit_group_by%notfound;
         titidtemp := 0;

         open rec_tit;

         loop
            fetch rec_tit
            into  tmprecette;

            exit when rec_tit%notfound;

            if titidtemp = 0 then
               titidtemp := set_titre_recette (tmprecette.rpco_id, monborid);
            else
               update jefy_recette.recette_ctrl_planco
                  set tit_id = titidtemp
                where rpco_id = tmprecette.rpco_id;
            end if;
         end loop;

         close rec_tit;

-- recup du nombre de pieces
-- TODO
         nbpieces := 0;

-- le fouOrdre du titre a faire pointer sur DEBITEUR DIVERS
-- ( a definir dans le parametrage)

         -- maj des montants du titre
         update titre
            set tit_ht = ht,
                tit_nb_piece = nbpieces,
                tit_ttc = ttc,
                tit_tva = tva,
                tit_libelle = 'TITRE COLLECTIF'
          where tit_id = titidtemp;
-- mise a jour du brouillard ?
-- BORDEREAU_ABRICOT.Set_Titre_Brouillard(titidtemp);
      end loop;

      close rec_tit_group_by;
   end;

   procedure bordereau_nd1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;
      abrgroupby   abricot_bord_selection.abr_group_by%type;

-- cursor pour traites les conventions limitatives !!!!
-- 1D1M -> liaison comptabilite
      cursor mand_dep_convra
      is
         select distinct d.*
         from            abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.engage_budget e, maracuja.v_convention_limitative c
         where           d.dpco_id = ab.dep_id and abr_id = abrid and db.dep_id = d.dep_id and e.eng_id = db.eng_id and e.org_id = c.org_id(+) and e.exe_ordre = c.exe_ordre(+) and c.org_id is not null and d.man_id is null and ab.abr_etat = 'ATTENTE'
         order by        d.pco_num asc, d.dep_id;
-- POUR LE RESTE DE LA SELECTION :
-- un cusor par type de abr_goup_by
-- attention une selection est de base limitee a un exercice et une UB et un type de bordereau
-- dans l interface on peut restreindre a l agent qui a saisie la depense.
-- dans l interface on peut restreindre au CR ou SOUS CR qui budgetise la depense.
-- dans l interface on peut restreindre suivant les 2 criteres ci dessus.
   begin
      open mand_dep_convra;

      loop
         fetch mand_dep_convra
         into  tmpdepense;

         exit when mand_dep_convra%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close mand_dep_convra;

-- recup du group by pour traiter le reste des mandats
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

-- il faut traiter les autres depenses non c_convra
--IF ( abrgroupby = 'ndep_mand_org_fou_rib_pco' ) THEN
-- cpt:=ndep_mand_org_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_org_fou_rib_pco_mod') then
         cpt := ndep_mand_org_fou_rib_pco_mod (abrid, monborid);
      end if;

--IF ( abrgroupby = 'ndep_mand_fou_rib_pco') THEN
-- cpt:=ndep_mand_fou_rib_pco(abrid ,monborid );
--END IF;
      if (abrgroupby = 'ndep_mand_fou_rib_pco_mod') then
         cpt := ndep_mand_fou_rib_pco_mod (abrid, monborid);
      end if;
   end;

   procedure bordereau_1d1m (abrid integer, monborid integer)
   is
      cpt          integer;
      tmpdepense   jefy_depense.depense_ctrl_planco%rowtype;

      cursor dep_mand
      is
         select   d.*
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d
         where    d.dpco_id = ab.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE'
         order by d.pco_num asc, d.dep_id;
   begin
      open dep_mand;

      loop
         fetch dep_mand
         into  tmpdepense;

         exit when dep_mand%notfound;
         cpt := set_mandat_depense (tmpdepense.dpco_id, monborid);
      end loop;

      close dep_mand;
   end;

   procedure bordereau_1d1m1r1t (abrid integer, boridep integer, boridrec integer)
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      bordereau_1d1m (abrid, boridep);
      bordereau_1r1t (abrid, boridrec);
      ctrl_bordereaux_pi (boridep, boridrec);
   end;

-- les mandats et titres
   function set_mandat_depense (dpcoid integer, borid integer)
      return integer
   is
      cpt               integer;
      flag              integer;
      ladepense         jefy_depense.depense_ctrl_planco%rowtype;
      ladepensepapier   jefy_depense.depense_papier%rowtype;
      leengagebudget    jefy_depense.engage_budget%rowtype;
      gescode           gestion.ges_code%type;
      manid             mandat.man_id%type;
      manorgine_key     mandat.man_orgine_key%type;
      manorigine_lib    mandat.man_origine_lib%type;
      oriordre          mandat.ori_ordre%type;
      prestid           mandat.prest_id%type;
      torordre          mandat.tor_ordre%type;
      virordre          mandat.pai_ordre%type;
      mannumero         mandat.man_numero%type;
     -- montantapayer     mandat.man_ttc%type;
      montantbud        mandat.man_ht%type;
      montanttva        mandat.man_tva%type;
      ttc               mandat.man_ttc%type;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

      select *
      into   ladepense
      from   jefy_depense.depense_ctrl_planco
      where  dpco_id = dpcoid;

      select distinct dpp.*
      into            ladepensepapier
      from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = dpcoid;

      select eb.*
      into   leengagebudget
      from   jefy_depense.engage_budget eb, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.eng_id = eb.eng_id and db.dep_id = dpco.dep_id and dpco_id = dpcoid;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = leengagebudget.org_id and exe_ordre = leengagebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a l''engagement num. ' || leengagebudget.eng_numero || ' n''est pas ouverte sur ' || leengagebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      manorgine_key := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      manorigine_lib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (leengagebudget.org_id, leengagebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.dep_id = ladepense.dep_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

-- creation du man_id --
      select mandat_seq.nextval
      into   manid
      from   dual;

-- recup du numero de mandat
      mannumero := -1;
      montantbud := abricot_util.get_dpco_montant_budgetaire (ladepense.dpco_id);
      --montantapayer := abricot_util.get_dpco_montant_apayer (ladepense.dpco_id);
      ttc := abricot_util.get_dpco_montant_ttc (ladepense.dpco_id);
      montanttva := ttc - montantbud;

      insert into mandat
                  (bor_id,
                   brj_ordre,
                   exe_ordre,
                   fou_ordre,
                   ges_code,
                   man_date_remise,
                   man_date_visa_princ,
                   man_etat,
                   man_etat_remise,
                   man_ht,
                   man_id,
                   man_motif_rejet,
                   man_nb_piece,
                   man_numero,
                   man_numero_rejet,
                   man_ordre,
                   man_orgine_key,
                   man_origine_lib,
                   man_ttc,
                   man_tva,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tor_ordre,
                   pai_ordre,
                   org_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable
                  )
      values      (borid,   --BOR_ID,
                   null,   --BRJ_ORDRE,
                   ladepensepapier.exe_ordre,   --EXE_ORDRE,
                   ladepensepapier.fou_ordre,   --FOU_ORDRE,
                   gescode,   --GES_CODE,
                   null,   --MAN_DATE_REMISE,
                   null,
                   
                   --MAN_DATE_VISA_PRINC,
                   'ATTENTE',   --MAN_ETAT,
                   'ATTENTE',   --MAN_ETAT_REMISE,
                   montantbud,   --MAN_HT,
                   manid,   --MAN_ID,
                   null,   --MAN_MOTIF_REJET,
                   ladepensepapier.dpp_nb_piece,   --MAN_NB_PIECE,
                   mannumero,
                   --MAN_NUMERO,
                   null,   --MAN_NUMERO_REJET,
                   -manid,   --MAN_ORDRE,
-- a parir de 2007 plus de man_ordre mais pour conserver la contrainte je mets -manid
                   manorgine_key,   --MAN_ORGINE_KEY,
                   manorigine_lib,   --MAN_ORIGINE_LIB,
                   --ladepense.dpco_ttc_saisie,   --MAN_TTC,
                   ttc,   --man_ttc
                   --ladepense.dpco_ttc_saisie - ladepense.dpco_montant_budgetaire,   --MAN_TVA,
                   montanttva,   --man_tva
                   ladepensepapier.mod_ordre,   --MOD_ORDRE,
                   oriordre,   --ORI_ORDRE,
                   ladepense.pco_num,
                   --PCO_NUM,
                   prestid,   --PREST_ID,
                   torordre,   --TOR_ORDRE,
                   virordre,   --VIR_ORDRE
                   leengagebudget.org_id,
                   --org_ordre
                   ladepensepapier.rib_ordre,   --rib ordo
                   ladepensepapier.rib_ordre   -- rib_comptable
                  );

-- maj du man_id  dans la depense
      update jefy_depense.depense_ctrl_planco
         set man_id = manid
       where dpco_id = dpcoid;

-- recup de la depense
--get_depense_jefy_depense(manid,ladepensepapier.utl_ordre);

      -- recup du brouillard
      set_mandat_brouillard (manid);
      return manid;
   end;

-- lesdepid XX$FF$....$DDD$ZZZ$$
   function set_mandat_depenses (lesdpcoid varchar, borid integer)
      return integer
   is
      cpt             integer;
      premier         integer;
      tmpdpcoid       integer;
      chaine          varchar (5000);
      premierdpcoid   integer;
      manid           integer;
      ttc             mandat.man_ttc%type;
      tva             mandat.man_tva%type;
      ht              mandat.man_ht%type;
      utlordre        integer;
      nb_pieces       integer;
   begin
      select count (*)
      into   cpt
      from   dual;

--RAISE_APPLICATION_ERROR (-20001,'lesdpcoid'||lesdpcoid);
      premierdpcoid := null;

      -- traitement de la chaine des depid xx$xx$xx$.....$x$$
      if lesdpcoid is not null or length (lesdpcoid) > 0 then
         chaine := lesdpcoid;

         loop
            premier := 1;

            -- On recupere le depid
            loop
               if substr (chaine, premier, 1) = '$' then
                  tmpdpcoid := en_nombre (substr (chaine, 1, premier - 1));
                  --   IF premier=1 THEN depordre := NULL; END IF;
                  exit;
               else
                  premier := premier + 1;
               end if;
            end loop;

-- creation du mandat lie au borid
            if premierdpcoid is null then
               manid := set_mandat_depense (tmpdpcoid, borid);

               -- suppression du brouillard car il est uniquement sur la premiere depense
               delete from mandat_brouillard
                     where man_id = manid;

               premierdpcoid := tmpdpcoid;
            else
               -- maj du man_id  dans la depense
               update jefy_depense.depense_ctrl_planco
                  set man_id = manid
                where dpco_id = tmpdpcoid;

               -- recup de la depense (maracuja)
               select distinct dpp.utl_ordre
               into            utlordre
               from            jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
               where           db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and dpco_id = tmpdpcoid;
--  get_depense_jefy_depense(manid,utlordre);
            end if;

--RECHERCHE DU CARACTERE SENTINELLE
            if substr (chaine, premier + 1, 1) = '$' then
               exit;
            end if;

            chaine := substr (chaine, premier + 1, length (chaine));
         end loop;
      end if;

-- mise a jour des montants du mandat HT TVA ET TTC nb pieces
--      select sum (dpco_ttc_saisie),
--             sum (dpco_ttc_saisie - dpco_montant_budgetaire),
--             sum (dpco_montant_budgetaire)
--      into   ttc,
--             tva,
--             ht
--      from   jefy_depense.depense_ctrl_planco
--      where  man_id = manid;
      ttc := abricot_util.get_man_montant_ttc (manid);
      ht := abricot_util.get_man_montant_budgetaire (manid);
      tva := ttc - ht;

-- recup du nb de pieces
      select sum (dpp.dpp_nb_piece)
      into   nb_pieces
      from   jefy_depense.depense_papier dpp, jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
      where  db.dep_id = dpco.dep_id and dpp.dpp_id = db.dpp_id and man_id = manid;

-- maj du mandat
      update mandat
         set man_ht = ht,
             man_tva = tva,
             man_ttc = ttc,
             man_nb_piece = nb_pieces
       where man_id = manid;

-- recup du brouillard
      set_mandat_brouillard (manid);
      return cpt;
   end;

   function set_titre_recette (rpcoid integer, borid integer)
      return integer
   is
--     jefytitre           jefy.titre%ROWTYPE;
      gescode             gestion.ges_code%type;
      titid               titre.tit_id%type;
      titorginekey        titre.tit_orgine_key%type;
      titoriginelib       titre.tit_origine_lib%type;
      oriordre            titre.ori_ordre%type;
      prestid             titre.prest_id%type;
      torordre            titre.tor_ordre%type;
      modordre            titre.mod_ordre%type;
      presid              integer;
      cpt                 integer;
      virordre            integer;
      flag                integer;
      recettepapier       jefy_recette.recette_papier%rowtype;
      recettebudget       jefy_recette.recette_budget%rowtype;
      facturebudget       jefy_recette.facture_budget%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
   begin
-- recuperation du ges_code --
      select ges_code
      into   gescode
      from   bordereau
      where  bor_id = borid;

--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
      select *
      into   recettectrlplanco
      from   jefy_recette.recette_ctrl_planco
      where  rpco_id = rpcoid;

      select *
      into   recettebudget
      from   jefy_recette.recette_budget
      where  rec_id = recettectrlplanco.rec_id;

      select *
      into   facturebudget
      from   jefy_recette.facture_budget
      where  fac_id = recettebudget.fac_id;

      select *
      into   recettepapier
      from   jefy_recette.recette_papier
      where  rpp_id = recettebudget.rpp_id;

-- Verifier si ligne budgetaire ouverte sur exercice
      select count (*)
      into   flag
      from   maracuja.v_organ_exer
      where  org_id = facturebudget.org_id and exe_ordre = facturebudget.exe_ordre;

      if (flag = 0) then
         raise_application_error (-20001, 'La ligne budgetaire affectee a la recette num. ' || recettebudget.rec_numero || ' n''est pas ouverte sur ' || facturebudget.exe_ordre || '.');
      end if;

-- recuperations --
--MANORGINE_KEY  CONVENTION RA OU LUCRATIVITE --
      titorginekey := null;
--MANORIGINE_LIB : CONVENTION RA OU LUCRATIVITE --
      titoriginelib := null;
--ORIORDRE : CONVENTION RA OU LUCRATIVITE --
      oriordre := gestionorigine.traiter_orgid (facturebudget.org_id, facturebudget.exe_ordre);

--PRESTID : PRESTATION INTERNE --
      select count (*)
      into   cpt
      from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
      where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;

      if cpt = 1 then
         select prest_id
         into   prestid
         from   jefy_recette.pi_dep_rec d, jefy_recette.pi_eng_fac e
         where  d.pef_id = e.pef_id and d.rec_id = recettectrlplanco.rec_id;
      else
         prestid := null;
      end if;

--TORORDRE : ORIGINE DU MANDAT --
      torordre := 1;
--VIRORDRE --
      virordre := null;

      select titre_seq.nextval
      into   titid
      from   dual;

      insert into titre
                  (bor_id,
                   bor_ordre,
                   brj_ordre,
                   exe_ordre,
                   ges_code,
                   mod_ordre,
                   ori_ordre,
                   pco_num,
                   prest_id,
                   tit_date_remise,
                   tit_date_visa_princ,
                   tit_etat,
                   tit_etat_remise,
                   tit_ht,
                   tit_id,
                   tit_motif_rejet,
                   tit_nb_piece,
                   tit_numero,
                   tit_numero_rejet,
                   tit_ordre,
                   tit_orgine_key,
                   tit_origine_lib,
                   tit_ttc,
                   tit_tva,
                   tor_ordre,
                   utl_ordre,
                   org_ordre,
                   fou_ordre,
                   mor_ordre,
                   pai_ordre,
                   rib_ordre_ordonnateur,
                   rib_ordre_comptable,
                   tit_libelle
                  )
      values      (borid,   --BOR_ID,
                   -borid,   --BOR_ORDRE,
                   null,   --BRJ_ORDRE,
                   recettepapier.exe_ordre,   --EXE_ORDRE,
                   gescode,
                   --GES_CODE,
                   null,   --MOD_ORDRE, n existe plus en 2007 vestige des ORVs
                   oriordre,   --ORI_ORDRE,
                   recettectrlplanco.pco_num,   --PCO_NUM,
                   prestid,
                   --PREST_ID,
                   sysdate,   --TIT_DATE_REMISE,
                   null,   --TIT_DATE_VISA_PRINC,
                   'ATTENTE',   --TIT_ETAT,
                   'ATTENTE',   --TIT_ETAT_REMISE,
                   recettectrlplanco.rpco_ht_saisie,   --TIT_HT,
                   titid,   --TIT_ID,
                   null,
                   --TIT_MOTIF_REJET,
                   recettepapier.rpp_nb_piece,   --TIT_NB_PIECE,
                   -1,
                   --TIT_NUMERO, numerotation en fin de transaction
                   null,   --TIT_NUMERO_REJET,
                   -titid,
                   --TIT_ORDRE,  en 2007 plus de tit_ordre on met  tit_id
                   titorginekey,   --TIT_ORGINE_KEY,
                   titoriginelib,   --TIT_ORIGINE_LIB,
                   recettectrlplanco.rpco_ttc_saisie,   --TIT_TTC,
                   recettectrlplanco.rpco_tva_saisie,   --TIT_TVA,
                   torordre,   --TOR_ORDRE,
                   recettepapier.utl_ordre,   --UTL_ORDRE
                   facturebudget.org_id,   --ORG_ORDRE,
                   recettepapier.fou_ordre,
                   -- FOU_ORDRE  --TOCHECK certains sont nuls...
                   facturebudget.mor_ordre,
                   --MOR_ORDRE
                   null,
                   -- VIR_ORDRE
                   recettepapier.rib_ordre,
                   recettepapier.rib_ordre,
                   recettebudget.rec_lib
                  );

-- maj du tit_id dans la recette
      update jefy_recette.recette_ctrl_planco
         set tit_id = titid
       where rpco_id = rpcoid;

-- recup du brouillard
--Set_Titre_Brouillard(titid);
      return titid;
   end;

   function set_titre_recettes (lesrpcoid varchar, borid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      raise_application_error (-20001, 'OPERATION NON TRAITEE');
      return cpt;
   end;

   function ndep_mand_org_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_org_fou_rib_pco_mod
      is
         select   e.org_id,
                  dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e, jefy_admin.organ vo, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and e.eng_id = db.eng_id and vo.org_id = e.org_id and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, e.org_id, dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vo.org_univ, vo.org_etab, vo.org_ub, vo.org_cr, vo.org_souscr, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      cursor lesdpcoidsribnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, jefy_depense.engage_budget e
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      e.eng_id = db.eng_id
         and      ab.abr_etat = 'ATTENTE'
         and      e.org_id = orgid
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      dpp.mod_ordre = modordre
         and      d.man_id is null
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_org_fou_rib_pco_mod;

      loop
         fetch ndep_mand_org_fou_rib_pco_mod
         into  orgid,
               fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_org_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsribnull;

            loop
               fetch lesdpcoidsribnull
               into  tmpdpcoid;

               exit when lesdpcoidsribnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsribnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_org_fou_rib_pco_mod;

      return cpt;
   end;

   function ndep_mand_fou_rib_pco_mod (abrid integer, borid integer)
      return integer
   is
      cpt            integer;
      fouordre       v_fournis_light.fou_ordre%type;
      ribordre       v_rib.rib_ordre%type;
      pconum         plan_comptable.pco_num%type;
      modordre       mode_paiement.mod_ordre%type;
      orgid          jefy_admin.organ.org_id%type;
      ht             mandat.man_ht%type;
      tva            mandat.man_ht%type;
      ttc            mandat.man_ht%type;
      budgetaire     mandat.man_ht%type;

      cursor ndep_mand_fou_rib_pco_mod
      is
         select   dpp.fou_ordre,
                  dpp.rib_ordre,
                  d.pco_num,
                  dpp.mod_ordre,
                  sum (dpco_ht_saisie) ht,
                  sum (dpco_tva_saisie) tva,
                  sum (dpco_ttc_saisie) ttc,
                  sum (dpco_montant_budgetaire) budgetaire
         from     maracuja.abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp, maracuja.v_fournis_light vf
         where    d.dpco_id = ab.dep_id and dpp.dpp_id = db.dpp_id and dpp.fou_ordre = vf.fou_ordre and db.dep_id = d.dep_id and abr_id = abrid and ab.abr_etat = 'ATTENTE' and d.man_id is null
         group by dpp.fou_ordre, vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre
         order by vf.fou_code, dpp.rib_ordre, d.pco_num, dpp.mod_ordre;

      cursor lesdpcoids
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre = ribordre
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      cursor lesdpcoidsnull
      is
         select   d.dpco_id
         from     abricot_bord_selection ab, jefy_depense.depense_ctrl_planco d, jefy_depense.depense_budget db, jefy_depense.depense_papier dpp
         where    d.dpco_id = ab.dep_id
         and      dpp.dpp_id = db.dpp_id
         and      db.dep_id = d.dep_id
         and      abr_id = abrid
         and      ab.abr_etat = 'ATTENTE'
         and      dpp.fou_ordre = fouordre
         and      dpp.rib_ordre is null
         and      d.pco_num = pconum
         and      d.man_id is null
         and      dpp.mod_ordre = modordre
         order by d.dpco_id;

      chainedpcoid   varchar (5000);
      tmpdpcoid      jefy_depense.depense_ctrl_planco.dpco_id%type;
   begin
      select count (*)
      into   cpt
      from   dual;

      open ndep_mand_fou_rib_pco_mod;

      loop
         fetch ndep_mand_fou_rib_pco_mod
         into  fouordre,
               ribordre,
               pconum,
               modordre,
               ht,
               tva,
               ttc,
               budgetaire;

         exit when ndep_mand_fou_rib_pco_mod%notfound;
         chainedpcoid := null;

         if ribordre is not null then
            open lesdpcoids;

            loop
               fetch lesdpcoids
               into  tmpdpcoid;

               exit when lesdpcoids%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoids;
         else
            open lesdpcoidsnull;

            loop
               fetch lesdpcoidsnull
               into  tmpdpcoid;

               exit when lesdpcoidsnull%notfound;
               chainedpcoid := chainedpcoid || tmpdpcoid || '$';
            end loop;

            close lesdpcoidsnull;
         end if;

         chainedpcoid := chainedpcoid || '$';
-- creation des mandats des pids
         cpt := set_mandat_depenses (chainedpcoid, borid);
      end loop;

      close ndep_mand_fou_rib_pco_mod;

      return cpt;
   end;

-- procedures de verifications
   function selection_valide (abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- meme exercice

      -- si PI somme recette = somme depense

      -- recette_valides

      -- depense_valides
      return cpt;
   end;

   function recette_valide (recid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function depense_valide (depid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

      return cpt;
   end;

   function verif_bordereau_selection (borid integer, abrid integer)
      return integer
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;

-- verifier sum TTC depense selection  = sum TTC mandat du bord

      -- verifier sum TTC recette selection = sum TTC titre du bord

      -- verifier sum TTC depense  = sum TTC mandat du bord

      -- verifier sum TTC recette  = sum TTC titre  du bord
      return cpt;
   end;

-- procedures de locks de transaction
   procedure lock_mandats
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure lock_titres
   is
      cpt   integer;
   begin
      select count (*)
      into   cpt
      from   dual;
   end;

   procedure get_depense_jefy_depense (manid integer)
   is
      depid               depense.dep_id%type;
      jefydepensebudget   jefy_depense.depense_budget%rowtype;
      tmpdepensepapier    jefy_depense.depense_papier%rowtype;
      jefydepenseplanco   jefy_depense.depense_ctrl_planco%rowtype;
      lignebudgetaire     depense.dep_ligne_budgetaire%type;
      fouadresse          depense.dep_adresse%type;
      founom              depense.dep_fournisseur%type;
      lotordre            depense.dep_lot%type;
      marordre            depense.dep_marches%type;
      fouordre            depense.fou_ordre%type;
      gescode             depense.ges_code%type;
      modordre            depense.mod_ordre%type;
      cpt                 integer;
      tcdordre            type_credit.tcd_ordre%type;
      tcdcode             type_credit.tcd_code%type;
      ecd_ordre_ema       ecriture_detail.ecd_ordre%type;
      orgid               integer;
      montantbudgetaire   depense.dep_ht%type;
      montanttva          depense.dep_ht%type;
      montantttc          depense.dep_ht%type;
      montantapayer       depense.dep_ht%type;

      cursor depenses
      is
         select db.*
         from   jefy_depense.depense_budget db, jefy_depense.depense_ctrl_planco dpco
         where  dpco.man_id = manid and db.dep_id = dpco.dep_id;
   begin
      open depenses;

      loop
         fetch depenses
         into  jefydepensebudget;

         exit when depenses%notfound;

         -- creation du depid --
         select depense_seq.nextval
         into   depid
         from   dual;

         -- creation de lignebudgetaire--
         select org_ub || ' ' || org_cr || ' ' || org_souscr
         into   lignebudgetaire
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         --recuperer le type de credit a partir de la commande
         select tcd_ordre
         into   tcdordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         --AND eng_stat !='A'
         select org_ub,
                org_id
         into   gescode,
                orgid
         from   jefy_admin.organ
         where  org_id = (select org_id
                          from   jefy_depense.engage_budget
                          where  eng_id = jefydepensebudget.eng_id
                                                                  --AND eng_stat !='A'
                        );

         -- fouordre --
         select fou_ordre
         into   fouordre
         from   jefy_depense.engage_budget
         where  eng_id = jefydepensebudget.eng_id;

         -- founom --
         founom := getfournisnom (fouordre);

         -- fouadresse --
         select count (*)
         into   cpt
         from   v_depense_adresse
         where  dep_id = jefydepensebudget.dep_id;

         if (cpt = 0) then
            raise_application_error (-20001, 'Pas d''adresse de facturation pour le fournisseur ' || founom);
         end if;

         select substr (adresse, 1, 196)
         into   fouadresse
         from   (select *
                 from   v_depense_adresse
                 where  dep_id = jefydepensebudget.dep_id)
         where  rownum = 1;

         --AND eng_stat !='A'

         -- lotordre --
         select count (*)
         into   cpt
         from   jefy_marches.attribution
         where  att_ordre = (select att_ordre
                             from   jefy_depense.engage_ctrl_marche
                             where  eng_id = jefydepensebudget.eng_id);

         if cpt = 0 then
            lotordre := null;
         else
            select lot_ordre
            into   lotordre
            from   jefy_marches.attribution
            where  att_ordre = (select att_ordre
                                from   jefy_depense.engage_ctrl_marche
                                where  eng_id = jefydepensebudget.eng_id);
         end if;

         -- marordre --
         select count (*)
         into   cpt
         from   jefy_marches.lot
         where  lot_ordre = lotordre;

         if cpt = 0 then
            marordre := null;
         else
            select mar_ordre
            into   marordre
            from   jefy_marches.lot
            where  lot_ordre = lotordre;
         end if;

         --MOD_ORDRE --
         select mod_ordre
         into   modordre
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recuperer l'ecriture_detail pour emargements semi-auto
         select ecd_ordre
         into   ecd_ordre_ema
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         -- recup de la depense papier
         select *
         into   tmpdepensepapier
         from   jefy_depense.depense_papier
         where  dpp_id = jefydepensebudget.dpp_id;

         -- recup des infos de depense_ctrl_planco
         select *
         into   jefydepenseplanco
         from   jefy_depense.depense_ctrl_planco
         where  dep_id = jefydepensebudget.dep_id;

         montantbudgetaire := abricot_util.get_dpco_montant_budgetaire (jefydepenseplanco.dpco_id);
         montantapayer := abricot_util.get_dpco_montant_apayer (jefydepenseplanco.dpco_id);
         montantttc := abricot_util.get_dpco_montant_ttc (jefydepenseplanco.dpco_id);
         montanttva := montantttc - montantbudgetaire;

         -- creation de la depense --
         insert into depense
         values      (fouadresse,   --DEP_ADRESSE,
                      null,   --DEP_DATE_COMPTA,
                      tmpdepensepapier.dpp_date_reception,   --DEP_DATE_RECEPTION,
                      tmpdepensepapier.dpp_date_service_fait,   --DEP_DATE_SERVICE,
                      'VALIDE',   --DEP_ETAT,
                      founom,   --DEP_FOURNISSEUR,
                      montantbudgetaire,   --DEP_HT,
                      depense_seq.nextval,   --DEP_ID,
                      lignebudgetaire,   --DEP_LIGNE_BUDGETAIRE,
                      lotordre,   --DEP_LOT,
                      marordre,   --DEP_MARCHES,
                      montantapayer,   --DEP_MONTANT_DISQUETTE,
                      null,   -- table N !!!jefydepensebudget.cm_ordre , --DEP_NOMENCLATURE,
                      substr (tmpdepensepapier.dpp_numero_facture, 1, 199),   --DEP_NUMERO,
                      jefydepenseplanco.dpco_id,   --DEP_ORDRE,
                      null,   --DEP_REJET,
                      tmpdepensepapier.rib_ordre,   --DEP_RIB,
                      'NON',   --DEP_SUPPRESSION,
                      montantttc,   --DEP_TTC,
                      montanttva,   -- DEP_TVA,
                      tmpdepensepapier.exe_ordre,   --EXE_ORDRE,
                      fouordre,   --FOU_ORDRE,
                      gescode,   --GES_CODE,
                      manid,   --MAN_ID,
                      jefydepenseplanco.man_id,   --MAN_ORDRE,
--            jefyfacture.mod_code,  --MOD_ORDRE,
                      modordre,
                      jefydepenseplanco.pco_num,   --PCO_ORDRE,
                      tmpdepensepapier.utl_ordre,   --UTL_ORDRE
                      orgid,   --org_ordre
                      tcdordre,
                      ecd_ordre_ema,   -- ecd_ordre_ema reference a l'ecriture_detail pour emargement
                      tmpdepensepapier.dpp_date_facture
                     );
      end loop;

      close depenses;
   end;

   procedure get_recette_jefy_recette (titid integer)
   is
      recettepapier          jefy_recette.recette_papier%rowtype;
      recettebudget          jefy_recette.recette_budget%rowtype;
      facturebudget          jefy_recette.facture_budget%rowtype;
      recettectrlplanco      jefy_recette.recette_ctrl_planco%rowtype;
      recettectrlplancotva   jefy_recette.recette_ctrl_planco_tva%rowtype;
      maracujatitre          maracuja.titre%rowtype;
      adrnom                 varchar2 (200);
      letyperecette          varchar2 (200);
      titinterne             varchar2 (200);
      lbud                   varchar2 (200);
      tboordre               integer;
      cpt                    integer;

      cursor c_recette
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
--RAISE_APPLICATION_ERROR (-20001,'rpcoid '||rpcoid);
--SELECT * INTO recettectrlplanco
--FROM  jefy_recette.RECETTE_CTRL_PLANCO
--WHERE tit_id = titid;
      open c_recette;

      loop
         fetch c_recette
         into  recettectrlplanco;

         exit when c_recette%notfound;

         select *
         into   recettebudget
         from   jefy_recette.recette_budget
         where  rec_id = recettectrlplanco.rec_id;

         select *
         into   facturebudget
         from   jefy_recette.facture_budget
         where  fac_id = recettebudget.fac_id;

         select *
         into   recettepapier
         from   jefy_recette.recette_papier
         where  rpp_id = recettebudget.rpp_id;

         select *
         into   maracujatitre
         from   maracuja.titre
         where  tit_id = titid;

         if (recettebudget.rec_id_reduction is null) then
            letyperecette := 'R';
         else
            letyperecette := 'T';
         end if;

         select count (*)
         into   cpt
         from   jefy_recette.pi_dep_rec
         where  rec_id = recettectrlplanco.rec_id;

         if cpt > 0 then
            titinterne := 'O';
         else
            titinterne := 'N';
         end if;

         adrnom := getfournisnom (recettepapier.fou_ordre);

         select org_ub || '/' || org_cr || '/' || org_souscr
         into   lbud
         from   jefy_admin.organ
         where  org_id = facturebudget.org_id;

         select distinct tbo_ordre
         into            tboordre
         from            maracuja.titre t, maracuja.bordereau b
         where           b.bor_id = t.bor_id and t.tit_id = titid;

-- 200 bordereau de presntation interne recette
         if tboordre = 200 then
            tboordre := null;
         else
            tboordre := facturebudget.org_id;
         end if;

         insert into recette
         values      (recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      maracujatitre.ges_code,
                      --GES_CODE,
                      null,   --MOD_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM,
                      recettebudget.rec_date_saisie,
                      --jefytitre.tit_date,-- REC_DATE,
                      adrnom,   -- REC_DEBITEUR,
                      recette_seq.nextval,   -- REC_ID,
                      null,   -- REC_IMPUTTVA,
                      null,
                      -- REC_INTERNE, // TODO ROD
                      facturebudget.fac_lib,
                      -- REC_LIBELLE,
                      lbud,   -- REC_LIGNE_BUDGETAIRE,
                      'E',   -- REC_MONNAIE,
                      recettectrlplanco.rpco_ht_saisie,   --HT,
                      recettectrlplanco.rpco_ttc_saisie,   --TTC,
                      recettectrlplanco.rpco_ttc_saisie,
                      --DISQUETTE,
                      recettectrlplanco.rpco_tva_saisie,   --   REC_MONTTVA,
                      facturebudget.fac_numero,   --   REC_NUM,
                      recettectrlplanco.rpco_id,
                      --   REC_ORDRE,
                      recettepapier.rpp_nb_piece,   --   REC_PIECE,
                      facturebudget.fac_numero,
                      
                      --   REC_REF,
                      'VALIDE',   --   REC_STAT,
                      'NON',   --    REC_SUPPRESSION,  Modif Rod
                      letyperecette,   --     REC_TYPE,
                      null,   --     REC_VIREMENT,
                      titid,   --      TIT_ID,
                      -titid,
                      --      TIT_ORDRE,
                      recettebudget.utl_ordre,   --       UTL_ORDRE
                      facturebudget.org_id,
                      --       ORG_ORDRE --ajout rod
                      facturebudget.fou_ordre,   --FOU_ORDRE --ajout rod
                      null,   --mod_ordre
                      recettepapier.mor_ordre,
                      --mor_ordre
                      recettepapier.rib_ordre,
                      null
                     );
      end loop;

      close c_recette;
   end;

-- procedures du brouillard
   procedure set_mandat_brouillard (manid integer)
   is
      lemandat              mandat%rowtype;
      tboordre              type_bordereau.tbo_ordre%type;
      sens                  mandat_brouillard.mab_sens%type;
      tcdsect               jefy_admin.type_credit.tcd_sect%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      tboordre := abricot_util.get_man_tboordre (manid);

      if lemandat.prest_id is null then
         -- creation du brouillard imputation budgetaire --
         sens := inverser_sens_orv (tboordre, 'D');
         montantbudgetaire := lemandat.man_ht;
         pconumbudgetaire := lemandat.pco_num;
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
         -- creation du brouillard de contrepartie
         sens := inverser_sens_orv (tboordre, 'C');

         -- si on est sur un ORV, on recupere le compte de ctp à utiliser
         if sens = 'D' then
            tcdsect := recup_tcdsect (manid);
            pconumctp := util.getpconumvalidefromparam (lemandat.exe_ordre, 'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section' || tcdsect || '.compte');

            if (pconumctp is null) then
               pconumctp := '4632';
            end if;
         else
            pconumctp := abricot_util.get_man_compte_ctp (manid);
         end if;

         gescodectp := abricot_util.get_man_gestion_ctp (manid);
         montantctp := abricot_util.get_man_montant_apayer (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA MANDAT', sens, manid, pconumctp);
         --si presence de TVA, on gere la TVA deduite et l eventuelle TVA collectee
         montanttvadeduite := abricot_util.get_man_montant_tva_ded (manid, null);
         montanttvacollectee := abricot_util.get_man_montant_tva_coll (manid, null);

         if (montanttvadeduite <> 0) then
            sens := inverser_sens_orv (tboordre, 'D');
            pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);
            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
         end if;

         if (montanttvacollectee <> 0) then
            sens := inverser_sens_orv (tboordre, 'C');
            pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);
            abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
         end if;
      else
         bordereau_abricot.set_mandat_brouillard_intern (manid);
      end if;
   end;

   procedure set_mandat_brouillard_intern (manid integer)
   is
      lemandat              mandat%rowtype;
      chap                  varchar2 (2);
      sens                  mandat_brouillard.mab_sens%type;
      montantbudgetaire     mandat_brouillard.mab_montant%type;
      montantctp            mandat_brouillard.mab_montant%type;
      montanttvadeduite     mandat_brouillard.mab_montant%type;
      montanttvacollectee   mandat_brouillard.mab_montant%type;
      pconumbudgetaire      mandat_brouillard.pco_num%type;
      pconumctp             mandat_brouillard.pco_num%type;
      pconumtvadeduite      mandat_brouillard.pco_num%type;
      pconumtvacollectee    mandat_brouillard.pco_num%type;
      gescodectp            mandat_brouillard.ges_code%type;
   begin
      select *
      into   lemandat
      from   mandat
      where  man_id = manid;

      -- recup des 2 premiers caracteres du compte
      select substr (lemandat.pco_num, 1, 2)
      into   chap
      from   dual;

      if chap = '18' then
         raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || lemandat.pco_num || ')');
      end if;

      --lepconum := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      sens := 'D';
      montantbudgetaire := lemandat.man_ht;
      pconumbudgetaire := api_planco.creer_planco_pi (lemandat.exe_ordre, lemandat.pco_num);
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montantbudgetaire), 'VISA MANDAT', sens, manid, pconumbudgetaire);
      sens := 'C';
      gescodectp := abricot_util.get_man_gestion_ctp (manid);
      montantctp := abricot_util.get_man_montant_apayer (manid);
      pconumctp := api_planco.creer_planco_pi (lemandat.exe_ordre, '181');
      abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, gescodectp, abs (montantctp), 'VISA MANDAT', sens, manid, pconumctp);

      if (montanttvadeduite <> 0) then
         sens := 'D';
         pconumtvadeduite := abricot_util.get_man_compte_tva_ded (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvadeduite), 'VISA TVA', sens, manid, pconumtvadeduite);
      end if;

      if (montanttvacollectee <> 0) then
         sens := 'C';
         pconumtvacollectee := abricot_util.get_man_compte_tva_coll (manid);
         abricot_util.creer_mandat_brouillard (lemandat.exe_ordre, lemandat.ges_code, abs (montanttvacollectee), 'VISA TVA', sens, manid, pconumtvacollectee);
      end if;
   end;

   procedure set_titre_brouillard (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      recid               integer;
      flag                integer;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
-- max car titres collectifs exact fetch return more than one row
      select max (rb.rec_id_reduction)
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

-- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      if letitre.prest_id is null then
         open c_recettes;

         loop
            fetch c_recettes
            into  recettectrlplanco;

            exit when c_recettes%notfound;

            select max (rec_id)
            into   recid
            from   recette
            where  rec_ordre = recettectrlplanco.rpco_id;

            -- verifier que la contrepartie n'est pas passée sur l'imputation du titre
            select count (*)
            into   flag
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id and pco_num = letitre.pco_num;

            if (flag > 0) then
               raise_application_error (-20001, 'La contrepartie ne doit pas etre identique à l''imputation ( : ' || letitre.tit_libelle || ' / ' || letitre.pco_num || ' / ' || letitre.tit_ttc || ')');
            end if;

            -- creation du titre_brouillard visa --
            --  RECETTE_CTRL_PLANCO
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      letitre.ges_code,
                      --GES_CODE,
                      recettectrlplanco.pco_num,   --PCO_NUM
                      abs (recettectrlplanco.rpco_ht_saisie),   --TIB_MONTANT,
                      'VISA TITRE',
                      --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_tva
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,   --PCO_NUM
                      abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                      'VISA TITRE',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      lesens,   --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_tva
               where  rpco_id = recettectrlplanco.rpco_id;

            -- recette_ctrl_planco_ctp
            insert into titre_brouillard
                        (ecd_ordre,
                         exe_ordre,
                         ges_code,
                         pco_num,
                         tib_montant,
                         tib_operation,
                         tib_ordre,
                         tib_sens,
                         tit_id,
                         rec_id
                        )
               select null,   --ECD_ORDRE,
                      recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                      ges_code,   --GES_CODE,
                      pco_num,
                      --PCO_NUM
                      abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                      'VISA TITRE',   --TIB_OPERATION,
                      titre_brouillard_seq.nextval,   --TIB_ORDRE,
                      inverser_sens (lesens),
                      --TIB_SENS,
                      titid,   --TIT_ID,
                      recid
               from   jefy_recette.recette_ctrl_planco_ctp
               where  rpco_id = recettectrlplanco.rpco_id;
         end loop;

         close c_recettes;
      else
         set_titre_brouillard_intern (titid);
      end if;

      -- suppression des lignes d ecritures a ZERO
      delete from titre_brouillard
            where tib_montant = 0;
   end;

   procedure set_titre_brouillard_intern (titid integer)
   is
      letitre             titre%rowtype;
      recettectrlplanco   jefy_recette.recette_ctrl_planco%rowtype;
      lesens              varchar2 (20);
      reduction           integer;
      lepconum            maracuja.plan_comptable.pco_num%type;
      libelle             maracuja.plan_comptable.pco_libelle%type;
      chap                varchar2 (2);
      recid               integer;
      gescodecompta       maracuja.titre.ges_code%type;
      ctpgescode          titre.ges_code%type;
      pconum_185          gestion_exercice.pco_num_185%type;

      cursor c_recettes
      is
         select *
         from   jefy_recette.recette_ctrl_planco
         where  tit_id = titid;
   begin
      select *
      into   letitre
      from   titre
      where  tit_id = titid;

      -- modif fred 04/2007
      select c.ges_code,
             ge.pco_num_185
      into   gescodecompta,
             pconum_185
      from   gestion g, comptabilite c, gestion_exercice ge
      where  g.ges_code = letitre.ges_code and g.com_ordre = c.com_ordre and g.ges_code = ge.ges_code and ge.exe_ordre = letitre.exe_ordre;

-- recup du sens : TITRE = C7 D4 sinon REDUCTION D7 C4
      select rb.rec_id_reduction
      into   reduction
      from   jefy_recette.recette_budget rb, jefy_recette.recette_ctrl_planco rcpo
      where  rcpo.rec_id = rb.rec_id and rcpo.tit_id = titid;

      -- si dans le cas d une reduction
      if (reduction is not null) then
         lesens := 'D';
      else
         lesens := 'C';
      end if;

      open c_recettes;

      loop
         fetch c_recettes
         into  recettectrlplanco;

         exit when c_recettes%notfound;

         select max (rec_id)
         into   recid
         from   recette
         where  rec_ordre = recettectrlplanco.rpco_id;

         -- recup des 2 premiers caracteres du compte
         select substr (recettectrlplanco.pco_num, 1, 2)
         into   chap
         from   dual;

         if chap != '18' then
            lepconum := api_planco.creer_planco_pi (recettectrlplanco.exe_ordre, recettectrlplanco.pco_num);
         else
            raise_application_error (-20001, 'Le compte d''imputation ne doit pas etre un compte 18xx (' || recettectrlplanco.pco_num || ')');
         end if;

         -- creation du titre_brouillard visa --
         --  RECETTE_CTRL_PLANCO
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   letitre.ges_code,
                   --GES_CODE,
                   lepconum,   --PCO_NUM
                   abs (recettectrlplanco.rpco_ht_saisie),
                   
                   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   lesens,
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco
            where  rpco_id = recettectrlplanco.rpco_id;

         -- recette_ctrl_planco_tva
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   exe_ordre,   --EXE_ORDRE,
                   gescodecompta,
                   -- ges_code,               --GES_CODE,
                   pco_num,   --PCO_NUM
                   abs (rpcotva_tva_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_tva
            where  rpco_id = recettectrlplanco.rpco_id;

         -- si on est sur un sacd, la contrepartie reste sur le sacd
         if (pconum_185 is not null) then
            ctpgescode := letitre.ges_code;
         else
            ctpgescode := gescodecompta;
         end if;

         -- recette_ctrl_planco_ctp on force le 181
         insert into titre_brouillard
                     (ecd_ordre,
                      exe_ordre,
                      ges_code,
                      pco_num,
                      tib_montant,
                      tib_operation,
                      tib_ordre,
                      tib_sens,
                      tit_id,
                      rec_id
                     )
            select null,   --ECD_ORDRE,
                   recettectrlplanco.exe_ordre,   --EXE_ORDRE,
                   ctpgescode,   --GES_CODE,
                   '181',
                   --PCO_NUM
                   abs (rpcoctp_ttc_saisie),   --TIB_MONTANT,
                   'VISA TITRE',   --TIB_OPERATION,
                   titre_brouillard_seq.nextval,   --TIB_ORDRE,
                   inverser_sens (lesens),
                   --TIB_SENS,
                   titid,
                   --TIT_ID,
                   recid
            from   jefy_recette.recette_ctrl_planco_ctp
            where  rpco_id = recettectrlplanco.rpco_id;
      end loop;

      close c_recettes;
   end;

   -- outils
   function inverser_sens_orv (tboordre integer, sens varchar)
      return varchar
   is
      cpt   integer;
   begin
-- si c est un bordereau de mandat li?es aux ORV
-- on inverse le sens de tous les details ecritures
      select count (*)
      into   cpt
      from   type_bordereau
      where  tbo_sous_type = 'REVERSEMENTS' and tbo_ordre = tboordre;

      if (cpt != 0) then
         if (sens = 'C') then
            return 'D';
         else
            return 'C';
         end if;
      end if;

      return sens;
   end;

   function recup_gescode (abrid integer)
      return varchar
   is
      gescode   bordereau.ges_code%type;
   begin
      select distinct ges_code
      into            gescode
      from            abricot_bord_selection
      where           abr_id = abrid;

      return gescode;
   end;

   function recup_utlordre (abrid integer)
      return integer
   is
      utlordre   bordereau.utl_ordre%type;
   begin
      select distinct utl_ordre
      into            utlordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return utlordre;
   end;

   function recup_exeordre (abrid integer)
      return integer
   is
      exeordre   bordereau.exe_ordre%type;
   begin
      select distinct exe_ordre
      into            exeordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return exeordre;
   end;

   function recup_tboordre (abrid integer)
      return integer
   is
      tboordre   bordereau.tbo_ordre%type;
   begin
      select distinct tbo_ordre
      into            tboordre
      from            abricot_bord_selection
      where           abr_id = abrid;

      return tboordre;
   end;

   function recup_groupby (abrid integer)
      return varchar
   is
      abrgroupby   abricot_bord_selection.abr_group_by%type;
   begin
      select distinct abr_group_by
      into            abrgroupby
      from            abricot_bord_selection
      where           abr_id = abrid;

      return abrgroupby;
   end;

   function inverser_sens (sens varchar)
      return varchar
   is
   begin
      if sens = 'D' then
         return 'C';
      else
         return 'D';
      end if;
   end;

   procedure numeroter_bordereau (borid integer)
   is
      cpt_mandat   integer;
      cpt_titre    integer;
   begin
      select count (*)
      into   cpt_mandat
      from   mandat
      where  bor_id = borid;

      select count (*)
      into   cpt_titre
      from   titre
      where  bor_id = borid;

      if cpt_mandat + cpt_titre = 0 then
         raise_application_error (-20001, 'Bordereau  vide');
      else
         numerotationobject.numeroter_bordereau (borid);
-- boucle mandat
         numerotationobject.numeroter_mandat (borid);
-- boucle titre
         numerotationobject.numeroter_titre (borid);
      end if;
   end;

   function traiter_orgid (orgid integer, exeordre integer)
      return integer
   is
      topordre     integer;
      cpt          integer;
      orilibelle   origine.ori_libelle%type;
      convordre    integer;
   begin
      if orgid is null then
         return null;
      end if;

      select count (*)
      into   cpt
      from   accords.convention_limitative
      where  org_id = orgid and exe_ordre = exeordre;

      if cpt > 0 then
         -- recup du type_origine CONVENTION--
         select top_ordre
         into   topordre
         from   type_operation
         where  top_libelle = 'CONVENTION RESSOURCE AFFECTEE';

         select distinct con_ordre
         into            convordre
         from            accords.convention_limitative
         where           org_id = orgid and exe_ordre = exeordre;

         select (exe_ordre || '-' || lpad (con_index, 5, '0') || ' ' || con_objet)
         into   orilibelle
         from   accords.contrat
         where  con_ordre = convordre;
      else
         select count (*)
         into   cpt
         from   jefy_admin.organ
         where  org_id = orgid and org_lucrativite = 1;

         if cpt = 1 then
            -- recup du type_origine OPERATION LUCRATIVE --
            select top_ordre
            into   topordre
            from   type_operation
            where  top_libelle = 'OPERATION LUCRATIVE';

            --le libelle utilisateur pour le suivie en compta --
            select org_ub || '-' || org_cr || '-' || org_souscr
            into   orilibelle
            from   jefy_admin.organ
            where  org_id = orgid;
         else
            return null;
         end if;
      end if;

-- l origine est t elle deja  suivie --
      select count (*)
      into   cpt
      from   origine
      where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid;

      if cpt >= 1 then
         select ori_ordre
         into   cpt
         from   origine
         where  ori_key_name = 'ORG_ID' and ori_entite = 'JEFY_ADMIN.ORGAN' and ori_key_entite = orgid and rownum = 1;
      else
         select origine_seq.nextval
         into   cpt
         from   dual;

         insert into origine
                     (ori_entite,
                      ori_key_name,
                      ori_libelle,
                      ori_ordre,
                      ori_key_entite,
                      top_ordre
                     )
         values      ('JEFY_ADMIN',
                      'ORG_ID',
                      orilibelle,
                      cpt,
                      orgid,
                      topordre
                     );
      end if;

      return cpt;
   end;

   procedure controle_bordereau (borid integer)
   is
      ttc             maracuja.titre.tit_ttc%type;
      detailttc       maracuja.titre.tit_ttc%type;
      ordottc         maracuja.titre.tit_ttc%type;
      debit           maracuja.titre.tit_ttc%type;
      credit          maracuja.titre.tit_ttc%type;
      cpt             integer;
      message         varchar2 (50);
      messagedetail   varchar2 (50);
   begin
      select count (*)
      into   cpt
      from   maracuja.titre
      where  bor_id = borid;

      if cpt = 0 then
-- somme des maracuja.titre
         select sum (man_ttc)
         into   ttc
         from   maracuja.mandat
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (d.dep_ttc)
         into   detailttc
         from   maracuja.mandat m, maracuja.depense d
         where  m.man_id = d.man_id and m.bor_id = borid;

-- la somme des credits
         select sum (mab_montant)
         into   credit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'C';

-- la somme des debits
         select sum (mab_montant)
         into   debit
         from   maracuja.mandat m, maracuja.mandat_brouillard mb
         where  bor_id = borid and m.man_id = mb.man_id and mb.mab_sens = 'D';

-- somme des jefy.recette
         select sum (d.dpco_ttc_saisie)
         into   ordottc
         from   maracuja.mandat m, jefy_depense.depense_ctrl_planco d
         where  m.man_id = d.man_id and m.bor_id = borid;

         message := ' mandats ';
         messagedetail := ' depenses ';
      else
-- somme des maracuja.titre
         select sum (tit_ttc)
         into   ttc
         from   maracuja.titre
         where  bor_id = borid;

--somme des maracuja.recette
         select sum (r.rec_monttva + r.rec_mont)
         into   detailttc
         from   maracuja.titre t, maracuja.recette r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

-- la somme des credits
         select sum (tib_montant)
         into   credit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'C';

-- la somme des debits
         select sum (tib_montant)
         into   debit
         from   maracuja.titre t, maracuja.titre_brouillard tb
         where  bor_id = borid and t.tit_id = tb.tit_id and tb.tib_sens = 'D';

-- somme des jefy.recette
         select sum (r.rpco_ttc_saisie)
         into   ordottc
         from   maracuja.titre t, jefy_recette.recette_ctrl_planco r
         where  t.tit_id = r.tit_id and t.bor_id = borid;

         message := ' titres ';
         messagedetail := ' recettes ';
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  debit <> credit : ' || debit || ' ' || credit);
      end if;

-- la somme des credits = sommes des debits
      if (nvl (debit, 0) != nvl (credit, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' :  ecriture <> budgetaire : ' || debit || ' ' || ttc);
      end if;

-- somme des maracuja.titre = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (detailttc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant des ' || messagedetail || ' :' || ttc || ' ' || detailttc);
      end if;

-- somme des jefy.recette = somme des maracuja.recette
      if (nvl (ttc, 0) != nvl (ordottc, 0)) then
         raise_application_error (-20001, 'PROBLEME DE ' || message || ' : montant des ' || message || ' <>  du montant ordonnateur des ' || messagedetail || ' :' || ttc || ' ' || ordottc);
      end if;

      bordereau_abricot.ctrl_date_exercice (borid);
   end;

   -- Controle la coherence des deux bordereaux de prestations internes (dep = rec)
   procedure ctrl_bordereaux_pi (boriddep integer, boridrec integer)
   is
      flag         integer;
      nbdep        integer;
      nbrec        integer;
      manid        mandat.man_id%type;
      titid        titre.tit_id%type;
      tmpmandat    mandat%rowtype;
      tmptitre     titre%rowtype;
      tmpprest     integer;
      montantdep   mandat.man_ht%type;
      montantrec   titre.tit_ht%type;

      cursor prests
      is
         select distinct prest_id
         from            (select prest_id
                          from   mandat
                          where  bor_id = boriddep
                          union
                          select prest_id
                          from   titre
                          where  bor_id = boridrec);
   begin
      if (boriddep is null) then
         raise_application_error (-20001, 'Reference au bordereau de depense interne nulle.');
      end if;

      if (boridrec is null) then
         raise_application_error (-20001, 'Reference au bordereau de recette interne nulle.');
      end if;

      -- verifier qu'il s'agit bien de bordereaux de PI
      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 201 and bor_id = boriddep;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de depense interne.');
      end if;

      select count (*)
      into   flag
      from   bordereau
      where  tbo_ordre = 200 and bor_id = boridrec;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau n''est pas un bordereau de recette interne.');
      end if;

      -- comparer le nombre de titres et de mandats
      select count (*)
      into   nbdep
      from   mandat
      where  bor_id = boriddep;

      select count (*)
      into   nbrec
      from   titre
      where  bor_id = boridrec;

      if (nbdep = 0) then
         raise_application_error (-20001, 'Aucun mandat trouve sur le bordereau');
      end if;

      if (nbdep <> nbrec) then
         raise_application_error (-20001, 'Nombre de mandats different du nombre de titres. ' || 'Mandats : ' || nbdep || ' / Titres : ' || nbrec);
      end if;

      open prests;

      loop
         fetch prests
         into  tmpprest;

         exit when prests%notfound;

         select count (*)
         into   nbdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select count (*)
         into   nbrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (nbdep <> nbrec) then
            raise_application_error (-20001, 'Incoherence : Nombre de titres (' || nbrec || ') different du nombre de mandats (' || nbdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;

         select sum (man_ht)
         into   montantdep
         from   mandat
         where  prest_id = tmpprest and bor_id = boriddep;

         select sum (tit_ht)
         into   montantrec
         from   titre
         where  prest_id = tmpprest and bor_id = boridrec;

         if (montantdep <> montantrec) then
            raise_application_error (-20001, 'Incoherence : Montant des titres (' || montantrec || ') different du montant des mandats  (' || montantdep || ') (' || 'prest_id=' || tmpprest || ')');
         end if;
      end loop;

      close prests;

      select sum (man_ht)
      into   montantdep
      from   mandat
      where  bor_id = boriddep;

      select sum (tit_ht)
      into   montantrec
      from   titre
      where  bor_id = boriddep;

      if (montantdep <> montantrec) then
         raise_application_error (-20001, 'Incoherence : Montant total des titres (' || montantrec || ') different du montant total des mandats  (' || montantdep || ')');
      end if;
   end;

   procedure get_recette_prelevements (titid integer)
   is
      cpt                      integer;
      facture_titre_data       prestation.facture_titre%rowtype;
--     client_data              prelev.client%ROWTYPE;
      oriordre                 integer;
      modordre                 integer;
      recid                    integer;
      echeid                   integer;
      echeancier_data          jefy_echeancier.echeancier%rowtype;
      echeancier_prelev_data   jefy_echeancier.echeancier_prelev%rowtype;
      facture_data             jefy_recette.facture_budget%rowtype;
      personne_data            grhum.v_personne%rowtype;
      premieredate             date;
   begin
-- verifier s il existe un echancier pour ce titre
      select count (*)
      into   cpt
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

      if (cpt != 1) then
         return;
      end if;

-- recup du eche_id / ech_id
      select eche_id
      into   echeid
      from   jefy_recette.recette_ctrl_planco pco, jefy_recette.recette r, jefy_recette.facture f
      where  pco.tit_id = titid and pco.rec_id = r.rec_id and r.fac_id = f.fac_id and eche_id is not null and r.rec_id_reduction is null;

-- recup du des infos du prelevements
      select *
      into   echeancier_data
      from   jefy_echeancier.echeancier
      where  ech_id = echeid;

      select *
      into   echeancier_prelev_data
      from   jefy_echeancier.echeancier_prelev
      where  ech_id = echeid;

      select *
      into   facture_data
      from   jefy_recette.facture_budget
      where  eche_id = echeid;

      select *
      into   personne_data
      from   grhum.v_personne
      where  pers_id = facture_data.pers_id;

      select echd_date_prevue
      into   premieredate
      from   jefy_echeancier.echeancier_detail
      where  echd_numero = 1 and ech_id = echeid;

      select rec_id
      into   recid
      from   recette
      where  tit_id = titid;

/*
-- verification / mise a jour du mode de recouvrement
SELECT mor_ordre INTO modordre FROM maracuja.TITRE WHERE tit_id=titid;
IF (modordre IS NULL) THEN
  SELECT COUNT(*) INTO cpt FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;
  IF (cpt=0) THEN
        RAISE_APPLICATION_ERROR (-20001,'MODE RECOUVREMENT ECHEANCIER NON DEFINI');
  END IF;
  IF (cpt>1) THEN
        RAISE_APPLICATION_ERROR (-20001,'PLUSIEURS MODE RECOUVREMENT ECHEANCIER DEFINIS. IMPOSSIBLE DE DETERMINER.');
  END IF;

  SELECT mod_ordre INTO modordre FROM MODE_RECOUVREMENT WHERE mod_dom='ECHEANCIER' AND exe_ordre=exeordre;

  UPDATE TITRE SET mor_ordre=modordre WHERE tit_id=titid;
END IF;
*/

      -- recup ??
      oriordre := gestionorigine.traiter_orgid (facture_data.org_id, facture_data.exe_ordre);

      insert into maracuja.echeancier
                  (eche_autoris_signee,
                   fou_ordre_client,
                   con_ordre,
                   eche_date_1ere_echeance,
                   eche_date_creation,
                   eche_date_modif,
                   eche_echeancier_ordre,
                   eche_etat_prelevement,
                   ft_ordre,
                   eche_libelle,
                   eche_montant,
                   eche_montant_en_lettres,
                   eche_nombre_echeances,
                   eche_numero_index,
                   org_ordre,
                   prest_ordre,
                   eche_prise_en_charge,
                   eche_ref_facture_externe,
                   eche_supprime,
                   exe_ordre,
                   tit_id,
                   rec_id,
                   tit_ordre,
                   ori_ordre,
                   pers_id,
                   org_id,
                   pers_description
                  )
      values      ('O',   --ECHE_AUTORIS_SIGNEE
                   facture_data.fou_ordre,   --FOU_ORDRE_CLIENT
                   null,
                   --echancier_data.CON_ORDRE  ,--CON_ORDRE
                   premieredate,
                   --echancier_data.DATE_1ERE_ECHEANCE  ,--ECHE_DATE_1ERE_ECHEANCE
                   sysdate,
                   --echancier_data.DATE_CREATION  ,--ECHE_DATE_CREATION
                   sysdate,   --echancier_data.DATE_MODIF  ,--ECHE_DATE_MODIF
                   echeancier_data.ech_id,
                   
                   --echancier_data.ECHEANCIER_ORDRE  ,--ECHE_ECHEANCIER_ORDRE
                   'V',
                   --echancier_data.ETAT_PRELEVEMENT  ,--ECHE_ETAT_PRELEVEMENT
                   facture_data.fac_id,
                   --echancier_data.FT_ORDRE  ,--FT_ORDRE
                   echeancier_data.ech_libelle,
                   --echancier_data.LIBELLE,--ECHE_LIBELLE
                   echeancier_data.ech_montant,   --ECHE_MONTANT
                   echeancier_data.ech_montant_lettres,
                   --ECHE_MONTANT_EN_LETTRES
                   echeancier_data.ech_nb_echeances,   --ECHE_NOMBRE_ECHEANCES
                   echeancier_data.ech_id,
                   --echeancier_data.NUMERO_INDEX  ,--ECHE_NUMERO_INDEX
                   facture_data.org_id,
                   --echeancier_data.ORG_ORDRE  ,--ORG_ORDRE
                   null,
                   
                   --echeancier_data.PREST_ORDRE  ,--PREST_ORDRE
                   'O',   --ECHE_PRISE_EN_CHARGE
                   facture_data.fac_lib,
                   
                   --cheancier_data.REF_FACTURE_EXTERNE  ,--ECHE_REF_FACTURE_EXTERNE
                   'N',   --ECHE_SUPPRIME
                   facture_data.exe_ordre,
                   --EXE_ORDRE
                   titid,
                   recid,   --REC_ID,
                   -titid,
                   oriordre,   --ORI_ORDRE,
                   personne_data.pers_id,
                   --CLIENT_data.pers_id  ,--PERS_ID
                   facture_data.org_id,   --orgid a faire plus tard....
                   personne_data.pers_libelle   --    PERS_DESCRIPTION
                  );

      insert into maracuja.prelevement
                  (eche_echeancier_ordre,
                   reco_ordre,
                   fou_ordre,
                   prel_commentaire,
                   prel_date_modif,
                   prel_date_prelevement,
                   prel_prelev_date_saisie,
                   prel_prelev_etat,
                   prel_numero_index,
                   prel_prelev_montant,
                   prel_prelev_ordre,
                   rib_ordre,
                   prel_etat_maracuja
                  )
         select ech_id,   --ECHE_ECHEANCIER_ORDRE
                null,   --PREL_FICP_ORDRE
                facture_data.fou_ordre,   --FOU_ORDRE
                echd_commentaire,
                --PREL_COMMENTAIRE
                sysdate,
                --DATE_MODIF,--PREL_DATE_MODIF
                echd_date_prevue,   --PREL_DATE_PRELEVEMENT
                sysdate,   --,--PREL_PRELEV_DATE_SAISIE
                'ATTENTE',   --PREL_PRELEV_ETAT
                echd_numero,
                --PREL_NUMERO_INDEX
                echd_montant,
                --PREL_PRELEV_MONTANT
                echd_id,   --PREL_PRELEV_ORDRE
                echeancier_prelev_data.rib_ordre_debiteur,
                
                --RIB_ORDRE
                'ATTENTE'   --PREL_ETAT_MARACUJA
         from   jefy_echeancier.echeancier_detail
         where  ech_id = echeancier_data.ech_id;
   end;

   procedure ctrl_date_exercice (borid integer)
   is
      exeordre   integer;
      annee      integer;
   begin
      select to_char (bor_date_creation, 'YYYY'),
             exe_ordre
      into   annee,
             exeordre
      from   bordereau
      where  bor_id = borid and exe_ordre >= 2007;

      if exeordre <> annee then
         update bordereau
            set bor_date_creation = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update mandat
            set man_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;

         update titre
            set tit_date_remise = to_date ('31/12/' || exe_ordre || ' 12:00:00', 'DD/MM/YYYY HH24:MI:SS')
          where bor_id = borid;
      end if;
   end;

-- GET_GES_CODE_FOR_MAN_ID
-- Renvoie la COMPOSANTE a prendre en compte en fonction du mandat. AGENCE ou COMPOSANTE.
   function get_ges_code_for_man_id (manid number)
      return comptabilite.ges_code%type
   is
      current_mandat          mandat%rowtype;
      pconumsacd              gestion_exercice.pco_num_185%type;
      visa_mode_paiement      planco_visa.pvi_contrepartie_gestion%type;
      visa_planco             planco_visa.pvi_contrepartie_gestion%type;
      code_agence_comptable   comptabilite.ges_code%type;
   begin
      select ges_code
      into   code_agence_comptable
      from   comptabilite;

      select *
      into   current_mandat
      from   mandat
      where  man_id = manid;

      -- SACD -- S'il s'agit d'un SACD on renvoie la composante associee au mandat.
      select pco_num_185
      into   pconumsacd
      from   gestion_exercice
      where  exe_ordre = current_mandat.exe_ordre and ges_code = current_mandat.ges_code;

      if (pconumsacd is not null) then
         return current_mandat.ges_code;
      else   -- Pas de SACD, on verifie le parametrage du Mode de Paiement (Mode_Paiement) puis du Compte de classe 6 (Planco_Visa).
         -- Si le parametrage du mode de paiement est renseigne , il est prioritaire
         select mod_contrepartie_gestion
         into   visa_mode_paiement
         from   mode_paiement
         where  mod_ordre = current_mandat.mod_ordre;

         if (visa_mode_paiement is not null) then
            -- Parametres : AGENCE ou COMPOSANTE
            if (visa_mode_paiement = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         else   -- Pas de parametrage du mode de paiement, on prend celui du compte (PCO_NUM)
            select pvi_contrepartie_gestion
            into   visa_planco
            from   planco_visa
            where  pco_num_ordonnateur = current_mandat.pco_num and exe_ordre = current_mandat.exe_ordre;

            if (visa_planco = 'AGENCE') then
               return code_agence_comptable;
            else   -- COMPOSANTE
               return current_mandat.ges_code;
            end if;
         end if;
      end if;
   end;

   function getfournisnom (fouordre integer)
      return varchar
   is
      founom   varchar2 (200);
   begin
      founom := '';

      select substr (nom || ' ' || prenom, 1, 200)
      into   founom
      from   v_fournis_light
      where  fou_ordre = fouordre;

      return founom;
   end;

   function recup_tcdsect (manid integer)
      return varchar
   is
      tcdsect   jefy_admin.type_credit.tcd_sect%type;
   begin
      select tc.tcd_sect
      into   tcdsect
      from   jefy_depense.depense_ctrl_planco dpco, jefy_depense.depense_budget db, jefy_depense.engage_budget eb, jefy_admin.type_credit tc
      where  dpco.dep_id = db.dep_id and db.eng_id = eb.eng_id and eb.tcd_ordre = tc.tcd_ordre and man_id = manid;

      return tcdsect;
   end;
end;
/

--**************---
CREATE OR REPLACE PACKAGE MARACUJA.Afaireaprestraitement IS
PROCEDURE apres_visa_bordereau (borid INTEGER);
PROCEDURE apres_reimputation (reiordre INTEGER);
PROCEDURE apres_paiement (paiordre INTEGER);
PROCEDURE apres_recouvrement (recoordre INTEGER);
PROCEDURE apres_recouvrement_releve(recoordre INTEGER);


-- emargement automatique d un paiement (mandats_ecriture)
PROCEDURE emarger_paiement(paiordre INTEGER);
PROCEDURE emarger_visa_bord_prelevement(borid INTEGER);
PROCEDURE emarger_prelevement(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_titre(recoordre INTEGER);
PROCEDURE emarger_prelev_ac_ecr(recoordre INTEGER);

PROCEDURE emarger_prelevement_releve(recoordre INTEGER);

-- private
-- si le debit et le credit sont de meme exercice return 1
-- sinon return 0
FUNCTION verifier_emar_exercice (ecrcredit INTEGER,ecrdebit INTEGER)
RETURN INTEGER;

PROCEDURE prv_apres_visa_bordereau (borid INTEGER);
PROCEDURE prv_apres_visa_reversement(borid INTEGER);


END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.afaireaprestraitement
is
-- version 1.0 10/06/2005 - correction pb dans apres_visa_bordereau (ne prenait pas en compte les bordereaux de prestation interne)
--version 1.2.0 20/12/2005 - Multi exercices jefy
-- version 1.5.0 - compatibilite avec jefy 2007
-- version 2.0 - suppression des references aux user < 2007
   procedure apres_visa_bordereau (borid integer)
   is
   begin
      prv_apres_visa_bordereau (borid);
   end;

   procedure prv_apres_visa_bordereau (borid integer)
   is
      lebordereau          bordereau%rowtype;
      lesoustype           type_bordereau.tbo_sous_type%type;
      autoviserbordrejet   maracuja.parametre.par_value%type;
      brjordre             maracuja.bordereau_rejet.brj_ordre%type;
      flag                 integer;
   begin
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      select tbo_sous_type
      into   lesoustype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if lebordereau.bor_etat = 'VISE' then
         -- s il y a un bordereau de rejet, si param = auto viser, viser le bordereau de rejet
         select count (*)
         into   flag
         from   maracuja.mandat
         where  bor_id = lebordereau.bor_id and brj_ordre is not null;

         if (flag > 0) then
            autoviserbordrejet := jefy_admin.api_parametre.get_param_maracuja ('AUTO_VISER_BORD_REJET_DEP', lebordereau.exe_ordre, 'NON');

            if (autoviserbordrejet = 'OUI') then
               select min (brj_ordre)
               into   brjordre
               from   maracuja.mandat
               where  bor_id = lebordereau.bor_id and brj_ordre is not null;

               bordereau_abricot.viser_bordereau_rejet (brjordre);
            end if;
         end if;

         if (lesoustype = 'REVERSEMENTS') then
            prv_apres_visa_reversement (borid);
         end if;

         emarger_visa_bord_prelevement (borid);
      end if;
   end;

-- permet de transmettre a jefy_depense les mandats de reversements vises
-- les mandats rejetes sont traites au moment du visa du bordereau de rejet
   procedure prv_apres_visa_reversement (borid integer)
   is
      manid   mandat.man_id%type;

      cursor c_mandatvises
      is
         select man_id
         from   mandat
         where  bor_id = borid and man_etat = 'VISE';
   begin
      open c_mandatvises;

      loop
         fetch c_mandatvises
         into  manid;

         exit when c_mandatvises%notfound;
         jefy_depense.apres_visa.viser_reversement (manid);
      end loop;

      close c_mandatvises;
   end;

   procedure apres_reimputation (reiordre integer)
   is
      ex              integer;
      manid           mandat.man_id%type;
      titid           titre.tit_id%type;
      pconumnouveau   plan_comptable_exer.pco_num%type;
      pconumancien    plan_comptable_exer.pco_num%type;
   begin
      select man_id,
             tit_id,
             pco_num_nouveau,
             pco_num_ancien
      into   manid,
             titid,
             pconumnouveau,
             pconumancien
      from   reimputation
      where  rei_ordre = reiordre;

      if manid is not null then
         jefy_depense.reimputer.reimputation_maracuja (manid, pconumnouveau);
      --update jefy_depense.depense_ctrl_planco set pco_num=pconumnouveau where man_id=manid and pco_num=pconumancien;
      end if;

      if titid is not null then
         update jefy_recette.recette_ctrl_planco
            set pco_num = pconumnouveau
          where tit_id = titid and pco_num = pconumancien;
      end if;
   end;

   procedure apres_paiement (paiordre integer)
   is
      cpt   integer;
   begin
      --SELECT 1 INTO  cpt FROM dual;
      emarger_paiement (paiordre);
   end;

   procedure emarger_paiement (paiordre integer)
   is
      cursor mandats (lepaiordre integer)
      is
         select *
         from   mandat
         where  pai_ordre = lepaiordre;

      cursor non_emarge_debit (lemanid integer)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'D';

      cursor non_emarge_credit_compte (lemanid integer, lepconum varchar)
      is
         select e.*
         from   mandat_detail_ecriture m, ecriture_detail e
         where  man_id = lemanid and e.ecd_ordre = m.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'C';

      lemandat          maracuja.mandat%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lepaiement        maracuja.paiement%rowtype;
      cpt               integer;
   begin
-- recup infos
      select *
      into   lepaiement
      from   paiement
      where  pai_ordre = paiordre;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lepaiement.exe_ordre,
                   3,
                   lepaiement.utl_ordre,
                   lepaiement.com_ordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les mandats du paiement
      open mandats (paiordre);

      loop
         fetch mandats
         into  lemandat;

         exit when mandats%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_debit (lemandat.man_id);

         loop
            fetch non_emarge_debit
            into  ecriture_debit;

            exit when non_emarge_debit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_credit_compte (lemandat.man_id, ecriture_debit.pco_num);

            loop
               fetch non_emarge_credit_compte
               into  ecriture_credit;

               exit when non_emarge_credit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_debit.ecd_ordre,
                               ecriture_credit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               lemandat.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_credit_compte;
         end loop;

         close non_emarge_debit;
      end loop;

      close mandats;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      end if;
   end;

   procedure apres_recouvrement_releve (recoordre integer)
   is
   begin
      emarger_prelevement_releve (recoordre);
   end;

   procedure emarger_prelevement_releve (recoordre integer)
   is
      cpt   integer;
   begin
      -- TODO : faire emargement a partir des prelevements etat='PRELEVE'
      select 1
      into   cpt
      from   dual;

      emarger_prelevement (recoordre);
   end;

   procedure apres_recouvrement (recoordre integer)
   is
   begin
      emarger_prelevement (recoordre);
   end;

   procedure emarger_prelevement (recoordre integer)
   is
   begin
      if (recoordre is null) then
         raise_application_error (-20001, 'Le parametre recoordre est null.');
      end if;

      emarger_prelev_ac_titre (recoordre);
      emarger_prelev_ac_ecr (recoordre);
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a un titre
   procedure emarger_prelev_ac_titre (recoordre integer)
   is
      cursor titres (lerecoordre integer)
      is
         select *
         from   titre
         where  tit_id in (select tit_id
                           from   prelevement p, echeancier e
                           where  e.eche_echeancier_ordre = p.eche_echeancier_ordre and p.reco_ordre = lerecoordre);

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lerecouvrement    maracuja.recouvrement%rowtype;
      cpt               integer;
   begin
      -- recup infos
      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lerecouvrement.exe_ordre,
                   3,
                   lerecouvrement.utl_ordre,
                   lerecouvrement.com_ordre,
                   0,
                   'VALIDE'
                  );

      -- on fetch les titres du recrouvement
      open titres (recoordre);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_credit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               ecriture_debit.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

      -- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

-- emarge les prelevements generes avec l'ecriture d'attente dans le cas ou l'echeancier est relie a une ecriture d'attente
-- chaque prelevement est relie a une ecriture de credit, on l'emarge avec l'ecriture de debit reliee a l'echeancier
   procedure emarger_prelev_ac_ecr (recoordre integer)
   is
      cursor lesprelevements (lerecoordre integer)
      is
         select *
         from   maracuja.prelevement
         where  reco_ordre = lerecoordre;

      ecrdetailcredit   ecriture_detail%rowtype;
      ecrdetaildebit    ecriture_detail%rowtype;
      ecriture_credit   ecriture_detail%rowtype;
      ecriture_debit    ecriture_detail%rowtype;
      leprelevement     maracuja.prelevement%rowtype;
      flag              integer;
      flag2             integer;
      retval            integer;
      utlordre          integer;

      cursor non_emarge_credit (prelevordre integer)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (prelevordre integer, lepconum varchar)
      is
         select e.*
         from   prelevement_detail_ecr t, ecriture_detail e
         where  prel_prelev_ordre = prelevordre and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger <> 0 and pco_num = lepconum and e.ecd_sens = 'D';
   begin
      select utl_ordre
      into   utlordre
      from   recouvrement
      where  reco_ordre = recoordre;

      open lesprelevements (recoordre);

      loop
         fetch lesprelevements
         into  leprelevement;

         exit when lesprelevements%notfound;
         ecrdetailcredit := null;
         ecrdetaildebit := null;

         -- recuperation de l'ecriture de l'echeancier non emargee en debit
         select count (*)
         into   flag2
         from   maracuja.ecriture_detail
         where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                            from   maracuja.echeancier_detail_ecr
                                                                            where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

         if (flag2 = 1) then
            select *
            into   ecrdetaildebit
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_debit <> 0 and ecd_ordre in (select ecd_ordre
                                                                               from   maracuja.echeancier_detail_ecr
                                                                               where  eche_echeancier_ordre = leprelevement.eche_echeancier_ordre);

            -- s'il y a une ecriture non emargee en debit sur l'echeancier,
            -- on emarge avec le credit correspondant du prelevement
            -- recuperation de l'ecriture du prelevement non emargee en credit
            select count (*)
            into   flag
            from   maracuja.ecriture_detail
            where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                     from   maracuja.prelevement_detail_ecr
                                                                                                                     where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

            if (flag = 1) then
               select *
               into   ecrdetailcredit
               from   maracuja.ecriture_detail
               where  ecd_reste_emarger <> 0 and ecd_credit <> 0 and pco_num = ecrdetaildebit.pco_num and ecd_ordre in (select ecd_ordre
                                                                                                                        from   maracuja.prelevement_detail_ecr
                                                                                                                        where  prel_prelev_ordre = leprelevement.prel_prelev_ordre);

               -- si les deux ecritures ont ete recuperees, on les emarge
               if (afaireaprestraitement.verifier_emar_exercice (ecrdetailcredit.ecr_ordre, ecrdetaildebit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecrdetailcredit.ecd_ordre, ecrdetaildebit.ecd_ordre, 3, utlordre);
               end if;
            end if;
         end if;

         -- emargement entre les debits et credits associes au prelevement
         -- (suite a saisie releve)

         -- on recupere les ecritures non emargees debits
         open non_emarge_credit (leprelevement.prel_prelev_ordre);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

            -- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (leprelevement.prel_prelev_ordre, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  retval := maracuja.api_emargement.creeremargement1d1c (ecriture_credit.ecd_ordre, ecriture_debit.ecd_ordre, 3, utlordre);
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close lesprelevements;
   end;

   procedure emarger_visa_bord_prelevement (borid integer)
   is
      cursor titres (leborid integer)
      is
         select *
         from   titre
         where  bor_id = leborid;

      cursor non_emarge_credit (letitid integer)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and e.ecd_sens = 'C';

      cursor non_emarge_debit_compte (letitid integer, lepconum varchar)
      is
         select e.*
         from   titre_detail_ecriture t, ecriture_detail e
         where  tit_id = letitid and e.ecd_ordre = t.ecd_ordre and ecd_reste_emarger != 0 and pco_num = lepconum and e.ecd_sens = 'D';

      letitre           maracuja.titre%rowtype;
      ecriture_debit    maracuja.ecriture_detail%rowtype;
      ecriture_credit   maracuja.ecriture_detail%rowtype;
      emaordre          emargement.ema_ordre%type;
      lebordereau       maracuja.bordereau%rowtype;
      cpt               integer;
      comordre          integer;
   begin
-- recup infos
      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

-- recup du com_ordre
      select com_ordre
      into   comordre
      from   gestion
      where  ges_code = lebordereau.ges_code;

-- creation de l emargement !
      select emargement_seq.nextval
      into   emaordre
      from   dual;

      insert into emargement
                  (ema_date,
                   ema_numero,
                   ema_ordre,
                   exe_ordre,
                   tem_ordre,
                   utl_ordre,
                   com_ordre,
                   ema_montant,
                   ema_etat
                  )
      values      (sysdate,
                   -1,
                   emaordre,
                   lebordereau.exe_ordre,
                   3,
                   lebordereau.utl_ordre,
                   comordre,
                   0,
                   'VALIDE'
                  );

-- on fetch les titres du recouvrement
      open titres (borid);

      loop
         fetch titres
         into  letitre;

         exit when titres%notfound;

-- on recupere les ecritures non emargees debits
         open non_emarge_credit (letitre.tit_id);

         loop
            fetch non_emarge_credit
            into  ecriture_credit;

            exit when non_emarge_credit%notfound;

-- on recupere les ecritures non emargees credit
            open non_emarge_debit_compte (letitre.tit_id, ecriture_credit.pco_num);

            loop
               fetch non_emarge_debit_compte
               into  ecriture_debit;

               exit when non_emarge_debit_compte%notfound;

               if (afaireaprestraitement.verifier_emar_exercice (ecriture_credit.ecr_ordre, ecriture_debit.ecr_ordre) = 1) then
                  -- creation de l emargement detail !
                  insert into emargement_detail
                              (ecd_ordre_destination,
                               ecd_ordre_source,
                               ema_ordre,
                               emd_montant,
                               emd_ordre,
                               exe_ordre
                              )
                  values      (ecriture_credit.ecd_ordre,
                               ecriture_debit.ecd_ordre,
                               emaordre,
                               ecriture_debit.ecd_reste_emarger,
                               emargement_detail_seq.nextval,
                               letitre.exe_ordre
                              );

                  -- maj de l ecriture debit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_debit.ecd_ordre;

                  -- maj de lecriture credit
                  update ecriture_detail
                     set ecd_reste_emarger = ecd_reste_emarger - ecriture_credit.ecd_reste_emarger
                   where ecd_ordre = ecriture_credit.ecd_ordre;
               end if;
            end loop;

            close non_emarge_debit_compte;
         end loop;

         close non_emarge_credit;
      end loop;

      close titres;

-- suppression de lemagenet si pas de details;
      select count (*)
      into   cpt
      from   emargement_detail
      where  ema_ordre = emaordre;

      if cpt = 0 then
         delete from emargement
               where ema_ordre = emaordre;
      else
         numerotationobject.numeroter_emargement (emaordre);
      end if;
   end;

   function verifier_emar_exercice (ecrcredit integer, ecrdebit integer)
      return integer
   is
      reponse     integer;
      execredit   exercice.exe_ordre%type;
      exedebit    exercice.exe_ordre%type;
   begin
-- init
      reponse := 0;

      select exe_ordre
      into   execredit
      from   ecriture
      where  ecr_ordre = ecrcredit;

      select exe_ordre
      into   exedebit
      from   ecriture
      where  ecr_ordre = ecrdebit;

      if exedebit = execredit then
         return 1;
      else
         return 0;
      end if;
   end;
end;
/

--**************---

CREATE OR REPLACE PACKAGE MARACUJA.Basculer_Be IS

/*
CRI G guadeloupe - Rivalland Frederic.
CRI LR - Prin Rodolphe

Ce package permet de creer des ecritures
et des lignes d ecriture dans maracuja lors
du passage des ecritures de balance entree.

Version du 30/09/2010 

*/
/*
create table ecriture_detail_be_log
(
edb_ordre integer,
edb_date date,
utl_ordre integer,
ecd_ordre integer)

create sequence basculer_solde_du_copmpte_seq start with 1 nocache;


INSERT INTO TYPE_OPERATION ( TOP_LIBELLE, TOP_ORDRE, TOP_TYPE ) VALUES (
'BALANCE D ENTREE AUTOMATIQUE', 11, 'PRIVEE');

*/
-- PUBLIC --

-- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
PROCEDURE basculer_solde_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);

-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
PROCEDURE basculer_DC_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);


PROCEDURE basculer_DC_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER);
PROCEDURE basculer_detail_du_cpt_ges_n(pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT LONG);


-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
PROCEDURE basculer_solde_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
PROCEDURE basculer_solde_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR, ecrordres OUT VARCHAR);



PROCEDURE basculer_DC_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_DC_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR, ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_detail_comptes_ges (lespconums VARCHAR,exeordre INTEGER,utlordre INTEGER,gescode VARCHAR,
 ecrordres OUT VARCHAR);
PROCEDURE basculer_manuel_du_compte_ges (pconum VARCHAR,exeordre INTEGER,utlordre INTEGER, gescode VARCHAR);

PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER);

-- PRIVATE --
PROCEDURE creer_detail_890 (ecrordre INTEGER,pconumlibelle VARCHAR, gescode VARCHAR);
PROCEDURE priv_archiver_la_bascule( pconum VARCHAR,gescode VARCHAR,utlordre INTEGER, exeordre INTEGER);
 PROCEDURE priv_archiver_la_bascule_ecd ( ecdordre    INTEGER, utlordre   INTEGER);
FUNCTION priv_get_exeordre_prec(exeordre INTEGER) RETURN INTEGER;
PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER);
PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER);
FUNCTION priv_getCompteBe(pconumold VARCHAR, exeordreold jefy_admin.exercice.exe_ordre%type) RETURN VARCHAR;

FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER;
FUNCTION priv_getTopOrdre RETURN NUMBER;
FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR;

END Basculer_Be;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.Basculer_Be
IS
-- PUBLIC --

-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES  --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
   PROCEDURE basculer_solde_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      topordre := priv_getTopOrdre;

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
-- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

--raise_application_error (-20001,' '||exeordreprec);
        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );
--

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retraité les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   
   
-- un UNIQUE DEBIT OU CREDIT  POUR CHACUNS DES COMPTES ET COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
-- les pconums : pco$pco$pco$pco$$ --
   PROCEDURE basculer_solde_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;
--       SELECT top_ordre
--         INTO topordre
--         FROM TYPE_OPERATION
--        WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

      SELECT com_ordre, ges_code
        INTO comordre, gescodeagence
        FROM COMPTABILITE;

      chaine := lespconums;
      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe
                                                  (comordre,
                                                   SYSDATE,
                                                   'BE ' || TO_CHAR(exeordre)||' - DE PLUSIEURS COMPTES ',
                                                   exeordre,
                                                   NULL,           --ORIORDRE,
                                                   topordre,
                                                   utlordre
                                                  );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens,         --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
         END IF;

         -- creation de du log pour ne plus retraité les ecritures ! --
--         basculer_be.archiver_la_bascule (pconumtmp, gescode, utlordre);
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

-- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   
   -- POUR L AGENCE COMPTABLE - POUR L AGENCE COMPTABLE --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE  --
-- et LE detail du 890 est a l agence --
   PROCEDURE basculer_solde_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- POUR UN CODE GESTION -- POUR UN CODE GESTION --
-- un UNIQUE DEBIT OU CREDIT SELON LE COMPTE DE LA COMPOSANTE --
-- et LE detail GLOBAL du 890 est a l agence --
   PROCEDURE basculer_solde_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

--raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesolde);
      IF lesolde != 0
      THEN
         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesolde,    --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,        --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
         -- creation du detail pour equilibrer l'ecriture selon le lesens --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, gescode, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- on cree un detail pour le debit du compte a l'agence --
-- on cree un detail pour le credit du compte a l'agence --
-- on cree un detail pour le solde (debiteur ou crediteur) du compte au 890  a lagence --
   PROCEDURE basculer_dc_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', NULL );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         SELECT top_ordre
           INTO topordre
           FROM TYPE_OPERATION
          WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)            --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconum, exeordreprec)             --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, NULL);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      ecrordres := monecdordre;
      priv_nettoie_ecriture(monecdordre);
   END;

   -------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;


        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', NULL );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', NULL );

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN

            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)          --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retraité les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  NULL,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
            

      CURSOR lesd
      IS
        SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconum;
            
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
          priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retraité les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;


 -- Crée une ecriture par ecriture_detail recupere
PROCEDURE basculer_detail_du_cpt_ges_n (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode            VARCHAR,
      ecrordres   OUT   LONG
   )
   IS
      cpt             INTEGER;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      leGesCode       ECRITURE_DETAIL.ges_code%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monEcrOrdre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
      oriordre          ecriture.ori_ordre%type;

      CURSOR lesEcd
      IS
         SELECT ecd_ordre, ecd_sens, ges_code, ecd_libelle,NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde, ori_ordre
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_reste_emarger <> 0
            AND pco_num = pconum
            AND ges_code=gescode;

   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      ecrordres := '';
      OPEN lesEcd;

      LOOP
         FETCH lesEcd
          INTO ecdordreOld, lesens, leGesCode, ecdlibelle, lemontant, oriordre;
         EXIT WHEN lesEcd%NOTFOUND;

        -- creation de lecriture  --
        monEcrOrdre := maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     oriordre,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

         -- creation du detail ecriture --
         ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     lesens,       --ECDSENS,
                                                     monecrordre,  --ECRORDRE,
                                                     leGesCode, --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
          -- basculer les titre_ecriture_detail  --
          priv_histo_relance(ecdordreOld, ecdordre, exeordre);

          -- creation de du log pour ne plus retraiter les ecritures ! --
          priv_archiver_la_bascule_ecd(ecdordreOld, utlordre);

          IF (lesens='C') THEN
            lesens890 := 'D';
          ELSE
            lesens890 := 'C';
          END IF;
          -- creer contrepartie



          ecdordre := maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                    'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '|| pconum,--ECDLIBELLE,
                                                    lemontant,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    monecrordre,      --ECRORDRE,
                                                    priv_getGesCodeCtrePartie(exeordre, legescode),  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
          maracuja.Api_Plsql_Journal.validerecriture (monEcrOrdre);
          ecrordres := ecrordres || monecrordre || '$';

      END LOOP;
      CLOSE lesEcd;


   END;


-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

--      CURSOR lesc
--      IS
--         SELECT ecd_ordre, ecd_libelle,
--                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
--           FROM ECRITURE_DETAIL ecd, ECRITURE e
--          WHERE e.ecr_ordre = ecd.ecr_ordre
--            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--            AND e.exe_ordre = exeordreprec
--            AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                    FROM ECRITURE_DETAIL_BE_LOG)
--            AND ecd_sens = 'C'
--            AND ecd_reste_emarger != 0
--            AND pco_num = pconumtmp;
            
            
      CURSOR lesc
      IS
            SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
            
            

--      CURSOR lesd
--      IS
--         SELECT ecd_ordre, ecd_libelle,
--                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
--           FROM ECRITURE_DETAIL ecd, ECRITURE e
--          WHERE e.ecr_ordre = ecd.ecr_ordre
--            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
--            AND e.exe_ordre = exeordreprec
--            AND ecd_ordre NOT IN (SELECT ecd_ordre
--                                    FROM ECRITURE_DETAIL_BE_LOG)
--            AND ecd_sens = 'D'
--            AND ecd_reste_emarger != 0
--            AND pco_num = pconumtmp;
            
      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e, gestion_exercice ge
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            and ge.EXE_ORDRE = ecd.EXE_ORDRE
            and ge.ges_code = ecd.ges_code
            and ge.pco_num_185 is null
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND pco_num = pconumtmp;
            
            
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

              chaine := lespconums;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );


      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'C',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp, exeordreprec)        --PCONUM
                                                    );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                     substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                     lemontant,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescodeagence, --GESCODE,
                                                     priv_getCompteBe(pconumtmp, exeordreprec)        --PCONUM
                                                    );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retraité les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               NULL,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, NULL);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

        lesdebits := priv_getSolde(pconum, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconum, exeordreprec , 'C', gescode );

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens := 'D';
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens := 'C';
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN

         -- creation de lecriture  --
         monecdordre :=
            maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                        SYSDATE,
                                                        'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                        exeordre,
                                                        NULL,      --ORIORDRE,
                                                        topordre,
                                                        utlordre
                                                       );
         -- creation du detail ecriture selon le DEBIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                    (NULL,   --ECDCOMMENTAIRE,
                                                        'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                     || pconum,
                                                     --ECDLIBELLE,
                                                     lesdebits,  --ECDMONTANT,
                                                     NULL,    --ECDSECONDAIRE,
                                                     'D',           --ECDSENS,
                                                     monecdordre,  --ECRORDRE,
                                                     gescode,       --GESCODE,
                                                     priv_getCompteBe(pconum, exeordreprec)           --PCONUM
                                                    );
         -- creation du detail pour l'ecriture selon le CREDIT --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconum,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconum, exeordreprec)            --PCONUM
                                                   );
         -- creation du detail pour l'ecriture selon le CREDIT --
         Basculer_Be.creer_detail_890 (monecdordre, pconum, gescode);
         maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      END IF;

-- creation de du log pour ne plus retraité les ecritures ! --
--      basculer_be.archiver_la_bascule (pconum, NULL, utlordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            NULL,
                                            utlordre,
                                            exeordreprec
                                           );
      priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_dc_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

           chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      -- creation du detail ecriture selon le DEBIT --
      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

        lesdebits := priv_getSolde(pconumtmp, exeordreprec , 'D', gescode );
        lescredits := priv_getSolde(pconumtmp, exeordreprec , 'C', gescode );

         IF (ABS (lesdebits) >= ABS (lescredits))
         THEN
            lesens := 'D';
            lesens890 := 'C';
            lesolde := lesdebits - lescredits;
         ELSE
            lesens := 'C';
            lesens890 := 'D';
            lesolde := lescredits - lesdebits;
         END IF;

         IF lesolde != 0
         THEN

            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - DEBIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lesdebits,   --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'D',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
            -- creation du detail pour l'ecriture selon le CREDIT --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - CREDIT DU COMPTE '
                                                    || pconumtmp,
                                                    --ECDLIBELLE,
                                                    lescredits,  --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    'C',            --ECDSENS,
                                                    monecdordre,   --ECRORDRE,
                                                    gescode,        --GESCODE,
                                                    priv_getCompteBe(pconumtmp, exeordreprec)         --PCONUM
                                                   );
            -- creation de du log pour ne plus retraité les ecritures ! --
            Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                                  gescode,
                                                  utlordre,
                                                  exeordreprec
                                                 );
         END IF;

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      priv_nettoie_ecriture(monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR,
      ecrordres   OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconum;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ' || pconum,
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      OPEN lesc;

      LOOP
         FETCH lesc
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesc%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum, exeordreprec)          --PCONUM
                                                     );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesc;

      OPEN lesd;

      LOOP
         FETCH lesd
          INTO ecdordreOld, ecdlibelle, lemontant;

         EXIT WHEN lesd%NOTFOUND;
         -- creation du detail ecriture selon le lesens --
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconum, exeordreprec)          --PCONUM
                                                     );
            priv_histo_relance(ecdordreOld, ecdordre, exeordre);
      END LOOP;

      CLOSE lesd;

      -- creation de du log pour ne plus retraité les ecritures ! --
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_detail_comptes_ges (
      lespconums         VARCHAR,
      exeordre           INTEGER,
      utlordre           INTEGER,
      gescode            VARCHAR,
      ecrordres    OUT   VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdordreOld      ECRITURE_DETAIL.ecd_ordre%TYPE;
      ecdlibelle      ECRITURE_DETAIL.ecd_libelle%TYPE;
      lemontant       ECRITURE_DETAIL.ecd_montant%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      premier         INTEGER;
      pconumtmp       PLAN_COMPTABLE_EXER.pco_num%TYPE;
      chaine          VARCHAR (2000);
      exeordreprec    EXERCICE.exe_ordre%TYPE;

      CURSOR lesc
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'C'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;

      CURSOR lesd
      IS
         SELECT ecd_ordre, ecd_libelle,
                NVL (ecd_reste_emarger * SIGN (ecd_montant), 0) solde
           FROM ECRITURE_DETAIL ecd, ECRITURE e
          WHERE e.ecr_ordre = ecd.ecr_ordre
            AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
            AND e.exe_ordre = exeordreprec
            AND ecd_ordre NOT IN (SELECT ecd_ordre
                                    FROM ECRITURE_DETAIL_BE_LOG)
            AND ecd_sens = 'D'
            AND ecd_reste_emarger != 0
            AND ges_code = gescode
            AND pco_num = pconumtmp;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
       topordre := priv_getTopOrdre;

         SELECT com_ordre, ges_code
           INTO comordre, gescodeagence
           FROM COMPTABILITE;

chaine := lespconums;


      -- creation de lecriture  --
      monecdordre :=
         maracuja.Api_Plsql_Journal.creerecriturebe (comordre,
                                                     SYSDATE,
                                                     'BE ' || TO_CHAR(exeordre)||' - COMPTE ',
                                                     exeordre,
                                                     NULL,         --ORIORDRE,
                                                     topordre,
                                                     utlordre
                                                    );

      LOOP
         premier := 1;

         -- On recupere le pconum --
         LOOP
            IF SUBSTR (chaine, premier, 1) = '$'
            THEN
               pconumtmp := SUBSTR (chaine, 1, premier - 1);

               IF premier = 1
               THEN
                  pconumtmp := NULL;
               END IF;

               EXIT;
            ELSE
               premier := premier + 1;
            END IF;
         END LOOP;

         OPEN lesc;

         LOOP
            FETCH lesc
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesc%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                      substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'C',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp, exeordreprec)       --PCONUM
                                                     );
                priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesc;

         OPEN lesd;

         LOOP
            FETCH lesd
             INTO ecdordreOld, ecdlibelle, lemontant;

            EXIT WHEN lesd%NOTFOUND;
            -- creation du detail ecriture selon le lesens --
            ecdordre :=
               maracuja.Api_Plsql_Journal.creerecrituredetail
                                                     (NULL,  --ECDCOMMENTAIRE,
                                                       substr('BE ' || TO_CHAR(exeordre)||' -  '|| ecdlibelle,1,190),
                                                      lemontant, --ECDMONTANT,
                                                      NULL,   --ECDSECONDAIRE,
                                                      'D',          --ECDSENS,
                                                      monecdordre, --ECRORDRE,
                                                      gescode,      --GESCODE,
                                                      priv_getCompteBe(pconumtmp, exeordreprec)       --PCONUM
                                                     );
                priv_histo_relance(ecdordreOld, ecdordre, exeordre);
         END LOOP;

         CLOSE lesd;

         -- creation de du log pour ne plus retraité les ecritures ! --
         Basculer_Be.priv_archiver_la_bascule (pconumtmp,
                                               gescode,
                                               utlordre,
                                               exeordreprec
                                              );

         --RECHERCHE DU CARACTERE SENTINELLE
         IF SUBSTR (chaine, premier + 1, 1) = '$'
         THEN
            EXIT;
         END IF;

         chaine := SUBSTR (chaine, premier + 1, LENGTH (chaine));
      END LOOP;

      --raise_application_error (-20001,' '||lesdebits||' '||lescredits||' '||lesens||' '||pconumtmp);

      -- creation du detail pour equilibrer l'ecriture selon le lesens --
      Basculer_Be.creer_detail_890 (monecdordre, NULL, gescode);
      maracuja.Api_Plsql_Journal.validerecriture (monecdordre);
      ecrordres := monecdordre;
   END;

-------------------------------------------------------------------------------
------------------------------------------------------------------------------
   PROCEDURE basculer_manuel_du_compte_ges (
      pconum            VARCHAR,
      exeordre          INTEGER,
      utlordre          INTEGER,
      gescode           VARCHAR
   )
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      monecdordre     INTEGER;
      exeordreprec    EXERCICE.exe_ordre%TYPE;
   BEGIN
      exeordreprec := priv_get_exeordre_prec (exeordre);
      Basculer_Be.priv_archiver_la_bascule (pconum,
                                            gescode,
                                            utlordre,
                                            exeordreprec
                                           );
   END;






------------------------------------------
------------------------------------------
   -- permet de retrouver le compte dans Maracuja comme s'il n'avait pas ete transfere en BE
    PROCEDURE annuler_bascule (pconum VARCHAR,exeordreold INTEGER)
       IS
       cpt INTEGER;
       BEGIN

       SELECT COUNT(*) INTO cpt FROM ECRITURE_DETAIL_BE_LOG edb, ECRITURE_DETAIL ecd WHERE edb.ecd_ordre=ecd.ecd_ordre AND ecd.pco_num=pconum AND ecd.exe_ordre=exeordreold;
        IF cpt = 0 THEN
           RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture de n'' a été transférée en BE pour le compte '|| pconum);
        END IF;

        DELETE FROM ECRITURE_DETAIL_BE_LOG WHERE ecd_ordre IN (SELECT ecd_ordre FROM ECRITURE_DETAIL WHERE pco_num=pconum AND exe_ordre=exeordreold);


    END;




PROCEDURE creer_detail_890 (ecrordre INTEGER, pconumlibelle VARCHAR, gescode VARCHAR)
   IS
      cpt             INTEGER;
      lesdebits       ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lescredits      ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesolde         ECRITURE_DETAIL.ecd_reste_emarger%TYPE;
      lesens          ECRITURE_DETAIL.ecd_sens%TYPE;
      lesens890       ECRITURE_DETAIL.ecd_sens%TYPE;
      topordre        TYPE_OPERATION.top_ordre%TYPE;
      comordre        COMPTABILITE.com_ordre%TYPE;
      ecdordre        ECRITURE_DETAIL.ecd_ordre%TYPE;
      gescodeagence   COMPTABILITE.ges_code%TYPE;
      exeordre          ECRITURE.EXE_ORDRE%TYPE;
      monecdordre     INTEGER;
   BEGIN
      SELECT exe_ordre INTO exeordre FROM ECRITURE WHERE ecr_ordre=ecrordre;

      SELECT com_ordre, ges_code INTO comordre, gescodeagence
        FROM COMPTABILITE;

        -- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
        gescodeagence := priv_getGesCodeCtrePartie(exeordre, gescode);

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lesdebits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'D';

      SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
        INTO lescredits
        FROM ECRITURE_DETAIL
       WHERE ecr_ordre = ecrordre AND ecd_sens = 'C';

      IF (ABS (lesdebits) >= ABS (lescredits))
      THEN
         lesens890 := 'C';
         lesolde := lesdebits - lescredits;
      ELSE
         lesens890 := 'D';
         lesolde := lescredits - lesdebits;
      END IF;

      IF lesolde != 0
      THEN
         -- creation du detail pour l'ecriture
         ecdordre :=
            maracuja.Api_Plsql_Journal.creerecrituredetail
                                                   (NULL,    --ECDCOMMENTAIRE,
                                                       'BE ' || TO_CHAR(exeordre)||' - SOLDE DU COMPTE '
                                                    || pconumlibelle,
                                                    --ECDLIBELLE,
                                                    lesolde,     --ECDMONTANT,
                                                    NULL,     --ECDSECONDAIRE,
                                                    lesens890,      --ECDSENS,
                                                    ecrordre,      --ECRORDRE,
                                                    gescodeagence,  --GESCODE,
                                                    '890'             --PCONUM
                                                   );
      END IF;
   END;






   PROCEDURE priv_archiver_la_bascule_ecd (
      ecdordre    INTEGER,
      utlordre   INTEGER
   )
   IS
   BEGIN

        INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre, ecdordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre = ecdordre
             AND ecd_ordre NOT IN (SELECT ecd_ordre FROM ECRITURE_DETAIL_BE_LOG);

   END;


   PROCEDURE priv_archiver_la_bascule (
      pconum     VARCHAR,
      gescode    VARCHAR,
      utlordre   INTEGER,
      exeordre   INTEGER
   )
   IS
   BEGIN
   -- si consolidé, on exclue les sacds 
      IF gescode IS NULL
      THEN
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL 
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND exe_ordre = exeordre
               AND pco_num = pconum
               and ges_code not in (select ge1.ges_code from gestion_exercice ge1, gestion_exercice ge2
                where ge1.pco_num_185 is not null  and ge2.pco_num_185 is not null
                and ge1.ges_code=ge2.ges_code and ge1.exe_ordre=ge2.exe_ordre-1 and ge1.exe_ordre=exeordre);
      ELSE
         INSERT INTO ECRITURE_DETAIL_BE_LOG
            SELECT ecriture_detail_be_log_seq.NEXTVAL, SYSDATE, utlordre,
                   ecd_ordre
              FROM ECRITURE_DETAIL
             WHERE ecd_ordre NOT IN (SELECT ecd_ordre
                                       FROM ECRITURE_DETAIL_BE_LOG)
               AND pco_num = pconum
               AND exe_ordre = exeordre
               AND ges_code = gescode;
      END IF;
   END;

-- Renvoie l'identifiant de l'exercice precedent celui identifie par exeordre
   FUNCTION priv_get_exeordre_prec (exeordre INTEGER)
      RETURN INTEGER
   IS
      reponse           INTEGER;
      exeexerciceprec   EXERCICE.exe_exercice%TYPE;
   BEGIN
      SELECT exe_exercice - 1
        INTO exeexerciceprec
        FROM EXERCICE
       WHERE exe_ordre = exeordre;

      SELECT exe_ordre
        INTO reponse
        FROM EXERCICE
       WHERE exe_exercice = exeexerciceprec;

      RETURN reponse;
   END;


   PROCEDURE priv_nettoie_ecriture (ecrOrdre INTEGER) IS
   BEGIN
           DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre = ecrOrdre AND ecd_montant=0 AND ecd_debit=0 AND ecd_credit=0;

   END;



   -- renvoie le compte de BE a utiliser (par ex renvoie 4011 pour 4012)
   -- ce compte est indique dans la table plan_comptable.
   -- si non precise, renvoie le compte passe en parametre
   FUNCTION priv_getCompteBe(pconumold VARCHAR, exeordreold jefy_admin.exercice.exe_ordre%type) RETURN VARCHAR
   IS
        pconumnew PLAN_COMPTABLE_EXER.pco_num%TYPE;
        flag integer;
   BEGIN
        SELECT pco_compte_be INTO pconumnew FROM PLAN_COMPTABLE_exer WHERE pco_num=pconumold and exe_ordre=exeordreold;
        IF pconumnew IS NULL THEN
           pconumnew := pconumold;
        END IF;
        
        select count(*) into flag from PLAN_COMPTABLE_exer WHERE pco_num=pconumnew and exe_ordre=exeordreold+1 and PCO_VALIDITE='VALIDE';
        if (flag=0) then
            RAISE_APPLICATION_ERROR (-20001,'Le compte de BE '|| pconumnew || ' défini sur l''exercice '|| exeordreold || ' pour le compte '||pconumold|| ' n''est pas valide sur l''exercice '||exeordreold+1||'.' );
        end if;
        
        
        RETURN pconumnew;
   END;




    -- Permet de suivre le detailecriture cree lors de la BE pour un titre.
    PROCEDURE priv_histo_relance (ecdordreold INTEGER,ecdordrenew INTEGER, exeordrenew INTEGER)
       IS
       cpt INTEGER;
       infos TITRE_DETAIL_ECRITURE%ROWTYPE;
       BEGIN
       -- est un ecdordre pour un titre ?
          SELECT COUNT(*)
            INTO cpt
            FROM TITRE_DETAIL_ECRITURE
           WHERE ecd_ordre = ecdordreold;
        IF cpt = 1 THEN
            SELECT * INTO infos
               FROM TITRE_DETAIL_ECRITURE
            WHERE ecd_ordre = ecdordreold;
          INSERT INTO TITRE_DETAIL_ECRITURE (ECD_ORDRE, EXE_ORDRE, ORI_ORDRE, TDE_DATE, TDE_ORDRE, TDE_ORIGINE, TIT_ID, REC_ID)
               VALUES (ecdordrenew, exeordrenew, infos.ORI_ORDRE, SYSDATE, titre_detail_ecriture_seq.NEXTVAL, infos.TDE_ORIGINE, infos.TIT_ID, infos.rec_id);
        END IF;


    END;

    -- renvoi le solde non emarge des ecritures non prises en compte en BE
    FUNCTION priv_getSolde(pconum VARCHAR, exeordreprec INTEGER, sens VARCHAR, gescode VARCHAR) RETURN NUMBER
    IS
      lesolde NUMBER;
    BEGIN
       IF (gescode IS NULL) THEN
            -- si consolide, on ne prend pas en compte les SACD
              SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
                INTO lesolde
                FROM ECRITURE_DETAIL ecd, ECRITURE e
               WHERE e.ecr_ordre = ecd.ecr_ordre
                 AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
                 AND e.exe_ordre = exeordreprec
                  and ecd.ges_code not in (select ge1.ges_code from gestion_exercice ge1, gestion_exercice ge2
                where ge1.pco_num_185 is not null  and ge2.pco_num_185 is not null
                and ge1.ges_code=ge2.ges_code and ge1.exe_ordre=ge2.exe_ordre-1 and ge1.exe_ordre=exeordreprec)
                 AND ecd_ordre NOT IN (SELECT ecd_ordre
                                         FROM ECRITURE_DETAIL_BE_LOG)                                        
                 AND ecd_sens = sens
                 AND pco_num = pconum;
         ELSE
              SELECT NVL (SUM (ecd_reste_emarger * SIGN (ecd_montant)), 0)
                INTO lesolde
                FROM ECRITURE_DETAIL ecd, ECRITURE e
               WHERE e.ecr_ordre = ecd.ecr_ordre
                 AND SUBSTR (e.ecr_etat, 1, 1) = 'V'
                 AND e.exe_ordre = exeordreprec
                 AND ges_code = gescode
                 AND ecd_ordre NOT IN (SELECT ecd_ordre
                                         FROM ECRITURE_DETAIL_BE_LOG)
                 AND ecd_sens = sens
                 AND pco_num = pconum;
        END IF;
        RETURN lesolde;
    END;


    FUNCTION priv_getTopOrdre RETURN NUMBER
    IS
      topordre NUMBER;
    BEGIN
          SELECT top_ordre INTO topordre
                FROM TYPE_OPERATION
                   WHERE top_libelle = 'BALANCE D ENTREE AUTOMATIQUE';
            RETURN  topordre;
    END;

    -- renvoie gescode si sacd ou gescodeagence
    FUNCTION priv_getGesCodeCtrePartie(exeordre NUMBER, gescode VARCHAR) RETURN VARCHAR
    IS
      gescodeagence COMPTABILITE.ges_code%TYPE;
      cpt INTEGER;
    BEGIN
      SELECT ges_code INTO gescodeagence
        FROM COMPTABILITE;

        -- verifier si le gescode est un SACD (dans ce cas on ne prend pas l'agence)
        IF gescode IS NOT NULL THEN
                      SELECT COUNT(*) INTO cpt FROM GESTION_EXERCICE
                             WHERE exe_ordre = exeordre AND ges_code=gescode AND pco_num_185 IS NOT NULL;
                    IF cpt<>0 THEN
                                 gescodeagence := gescode;
                    END IF;
        END IF;

        RETURN gescodeagence;

    END;



END Basculer_Be;
/

--**************---

CREATE OR REPLACE PACKAGE MARACUJA.cptefiutil IS
    PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER,libelle VARCHAR, rtatcomp VARCHAR);
    PROCEDURE annuler_ecriture (monecdordre INTEGER);
    PROCEDURE numeroter_ecriture (ecrordre INTEGER);
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.cptefiutil
is
   procedure solde_6_7 (exeordre integer, utlordre integer, libelle varchar, rtatcomp varchar)
   is
      legescode       gestion.ges_code%type;
      lepconum        plan_comptable_exer.pco_num%type;
      lesolde         number (12, 2);
      debit           number (12, 2);
      credit          number (12, 2);
      credit120       number (12, 2);
      debit120        number (12, 2);
      credit129       number (12, 2);
      debit129        number (12, 2);

      cursor solde6
      is
         select ges_code,
                pco_num,
                solde_crediteur
         from   cfi_solde_6_7
         where  solde_crediteur != 0 and (pco_num like '6%' or pco_num like '186%') and exe_ordre = exeordre;

      cursor solde7
      is
         select ges_code,
                pco_num,
                solde_crediteur
         from   cfi_solde_6_7
         where  solde_crediteur != 0 and (pco_num like '7%' or pco_num like '187%') and exe_ordre = exeordre;

      cursor lesgescode
      is
         select distinct ges_code,
                         pco_num_185
         from            gestion_exercice
         where           exe_ordre = exeordre;

      cursor lessacds
      is
         select distinct ges_code
         from            gestion_exercice
         where           exe_ordre = exeordre and pco_num_185 is not null;

      nb              integer;
      gescodeagence   comptabilite.ges_code%type;
      comordre        comptabilite.com_ordre%type;
      topordre        type_operation.top_ordre%type;
      tjoordre        type_journal.tjo_ordre%type;
      monecdordre     ecriture.ecr_ordre%type;
      ecdordre        ecriture_detail.ecd_ordre%type;
      exeexercice     exercice.exe_exercice%type;
   begin
-- creation de l'ecriture de solde --
      select top_ordre
      into   topordre
      from   type_operation
      where  top_libelle = 'ECRITURE SOLDE 6 ET 7';

      select tjo_ordre
      into   tjoordre
      from   type_journal
      where  tjo_libelle = 'JOURNAL FIN EXERCICE';

      select com_ordre,
             ges_code
      into   comordre,
             gescodeagence
      from   comptabilite;

      select exe_exercice
      into   exeexercice
      from   exercice
      where  exe_ordre = exeordre;

-- creation de lecriture  --
      monecdordre := maracuja.api_plsql_journal.creerecriture (comordre, to_date ('31/12/' || exeexercice, 'dd/mm/yyyy'), libelle, exeordre, null,   --ORIORDRE,
                                                               tjoordre, topordre, utlordre);

-- Calcul du résultat par composante ---
-- Etablissement + SACD --
      if rtatcomp = 'O' then
         -- creation de l ecriture de resultat --
         open lesgescode;

         loop
            fetch lesgescode
            into  legescode,
                  lepconum;

            exit when lesgescode%notfound;

            select count (*)
            into   nb
            from   cfi_solde_6_7
            where  exe_ordre = exeordre and ges_code = legescode;

            if nb != 0 then
               --  composante SACD --
               --       IF lepco_num is not null  THEN
               select nvl (-sum (solde_crediteur), 0)
               into   debit
               from   cfi_solde_6_7
               where  (pco_num like '6%' or pco_num like '186%') and ges_code = legescode and exe_ordre = exeordre;

               select nvl (sum (solde_crediteur), 0)
               into   credit
               from   cfi_solde_6_7
               where  (pco_num like '7%' or pco_num like '187%') and ges_code = legescode and exe_ordre = exeordre;

               if debit > credit then
                  -- RESULTAT SOLDE DEBITEUR 129
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  --  contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
               else
                  -- RESULTAT SOLDE CREDITEUR 120 --
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  -- contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
               end if;
            end if;
         end loop;

         close lesgescode;
      else
         -- resultat = 'Etablissement' --
         select count (*)
         into   nb
         from   cfi_solde_6_7
         where  exe_ordre = exeordre and ges_code in (select ges_code
                                                      from   gestion_exercice
                                                      where  exe_ordre = exeordre and pco_num_185 is null);

         if nb != 0 then
            select nvl (-sum (solde_crediteur), 0)
            into   debit
            from   cfi_solde_6_7
            where  (pco_num like '6%' or pco_num like '186%') and exe_ordre = exeordre and ges_code in (select ges_code
                                                                                                        from   gestion_exercice
                                                                                                        where  exe_ordre = exeordre and pco_num_185 is null);

            select nvl (sum (solde_crediteur), 0)
            into   credit
            from   cfi_solde_6_7
            where  (pco_num like '7%' or pco_num like '187%') and exe_ordre = exeordre and ges_code in (select ges_code
                                                                                                        from   gestion_exercice
                                                                                                        where  exe_ordre = exeordre and pco_num_185 is null);

            if debit > credit then
               -- RESULTAT SOLDE DEBITEUR 129
               -- contre-partie classe 7 crédit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           credit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'C',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '129'   --PCONUM
                                                                                );
               --  contre-partie classe 6 débit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           debit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'D',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '129'   --PCONUM
                                                                                );
            else
               -- RESULTAT SOLDE CREDITEUR 120 --
               -- contre-partie classe 7 crédit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           credit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'C',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '120'   --PCONUM
                                                                                );
               -- contre-partie classe 6 débit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           debit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'D',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '120'   --PCONUM
                                                                                );
            end if;
         end if;

         -- Résultat des SACD ---
         open lessacds;

         loop
            fetch lessacds
            into  legescode;

            exit when lessacds%notfound;

            select count (*)
            into   nb
            from   cfi_solde_6_7
            where  exe_ordre = exeordre and ges_code = legescode;

            if nb != 0 then
               --  SACD --
               --       IF lepco_num is not null  THEN
               select nvl (-sum (solde_crediteur), 0)
               into   debit
               from   cfi_solde_6_7
               where  (pco_num like '6%' or pco_num like '186%') and ges_code = legescode and exe_ordre = exeordre;

               select nvl (sum (solde_crediteur), 0)
               into   credit
               from   cfi_solde_6_7
               where  (pco_num like '7%' or pco_num like '187%') and ges_code = legescode and exe_ordre = exeordre;

               if debit > credit then
                  -- RESULTAT SOLDE DEBITEUR 129
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  --  contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
               else
                  -- RESULTAT SOLDE CREDITEUR 120 --
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  -- contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
               end if;
            end if;
         end loop;

         close lessacds;
      end if;

-- Solde classe 6 et 186
      open solde6;

      loop
         fetch solde6
         into  legescode,
               lepconum,
               lesolde;

         exit when solde6%notfound;

         if lesolde < 0 then
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        -lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'C',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         else
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'D',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         end if;
      end loop;

      close solde6;

-- Solde classe 7 et 187
      open solde7;

      loop
         fetch solde7
         into  legescode,
               lepconum,
               lesolde;

         exit when solde7%notfound;

         if lesolde < 0 then
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        -lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'C',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         else
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'D',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         end if;
      end loop;

      close solde7;

-- on numerote pour eviter une numerotation automatique.
-- voir procedure numeroter_ecriture
      update ecriture
         set ecr_numero = 99999999
       where ecr_ordre = monecdordre;
   end;

--------------------------------------------------------------------------------
   procedure numeroter_ecriture (ecrordre integer)
   is
--RIVALLAND FREDRIC
--CRIG
--18/01/2005

      --a utiliser uniquement pour numeroter
--definitivement lecriture de solde des
--classes 6 et 7
      numero   integer;
   begin
      select ecr_numero
      into   numero
      from   ecriture
      where  ecr_ordre = ecrordre;

      if numero != 99999999 then
         raise_application_error (-20001, 'IMPOSSIBLE DE MODIFIER UNE ECRITURE DU JOURNAL !');
      else
         update ecriture
            set ecr_numero = 0
          where ecr_ordre = ecrordre;

         maracuja.api_plsql_journal.validerecriture (ecrordre);
      end if;
   end;

--------------------------------------------------------------------------------
   procedure annuler_ecriture (monecdordre integer)
   is
--RIVALLAND FREDRIC
--CRIG
--18/01/2005

      --a utiliser uniquement pour ANNULER
--definitivement l'ecriture de solde des
--classes 6 et 7
      numero   integer;
   begin
      select ecr_numero
      into   numero
      from   ecriture
      where  ecr_ordre = monecdordre;

      if numero != 99999999 then
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      else
         delete from ecriture_detail
               where ecr_ordre = monecdordre;

         delete from ecriture
               where ecr_ordre = monecdordre;
      end if;
   end;
end;
/

--**************---



CREATE OR REPLACE PACKAGE MARACUJA."ZANALYSE" IS

    PROCEDURE checkRejets(exeordre INTEGER);
    PROCEDURE checkBordereauNonVise(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco(exeordre INTEGER);
    PROCEDURE checkEcritureDetailPlanco2(exeordre INTEGER);
    PROCEDURE checkActionDepenses(exeordre INTEGER);
    PROCEDURE checkActionRecettes(exeordre INTEGER);
    PROCEDURE checkIncoherenceMandats(exeordre INTEGER);
    PROCEDURE checkBalancePi(exeOrdre INTEGER);
    PROCEDURE checkBalanceGen(exeOrdre INTEGER);
    PROCEDURE checkBalance185(exeOrdre INTEGER);
    procedure checkCadre2VsBalance(exeOrdre INTEGER);
    procedure checkEcritureDetailGestion(exeordre integer); 
    procedure checkMandatVsJefyDepense(exeordre integer); 
    procedure checkMontantsEmargements(exeOrdre integer);
    procedure checkEcritureDates(exeordre integer);
    procedure checkAllProblemes(exeOrdre integer);
    
    
    
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA."ZANALYSE" 
IS
  -- verifier s'il reste des bordereaux de rejet non vises
PROCEDURE checkRejets
  (
    exeordre INTEGER)
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau_rejet%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau_rejet
    where exe_ordre=exeOrdre
    and BRJ_ETAT  <>'VISE';
BEGIN
  categorie := 'BORDEREAU DE REJET';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau de rejet non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BRJ_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU_REJET',                                                         --ZAP_ENTITY,
        'MARACUJA.BORDEREAU_REJET.BRJ_ORDRE',                                               --ZAP_ENTITY_KEY,
        leBordereau.brj_ordre,                                                              --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau de rejet ',                                                     --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBordereauNonVise
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  leBordereau maracuja.bordereau%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.bordereau
    where exe_ordre=exeOrdre
    and BOR_ETAT   ='VALIDE';
BEGIN
  categorie := 'BORDEREAU';
  OPEN C1;
  LOOP
    FETCH c1 INTO leBordereau;
    EXIT
  WHEN c1%NOTFOUND;
    select tbo_libelle
    into sousCategorie
    from maracuja.type_bordereau
    where tbo_ordre =leBordereau.tbo_ordre;
    
    probleme := 'Bordereau non vise ('|| leBordereau.ges_code ||' / ' || leBordereau.BOR_NUM ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                              --ZAP_ID,
        exeOrdre,                                                                           -- EXE_ORDRE
        categorie,                                                                          --ZAP_CATEGORIE,
        sousCategorie,                                                                      --ZAP_SOUS_CATEGORIE,
        'MARACUJA.BORDEREAU',                                                               --ZAP_ENTITY,
        'MARACUJA.BORDEREAU.BOR_ID',                                                        --ZAP_ENTITY_KEY,
        leBordereau.bor_id,                                                                 --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                           --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale', -- ZAP_CONSEQUENCE
        'Viser le bordereau',                                                               --ZAP_SOLUTION,
        sysdate,                                                                            --ZAP_DATE)
        3                                                                                   -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkIncoherenceMandats
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and 
    (
    (man_ETAT   not in ('VISE','PAYE', 'ANNULE') and bor_etat  in ('VISE', 'PAYE', 'PAIEMENT'))
    or
    (man_ETAT   not in ('ANNULE') and brj_ordre is not null)
    )
    ;
    
    
    cursor c2 is
        select m.* from mandat m, mandat_detail_ecriture mde, ecriture_detail ecd, mode_paiement mp  
            where m.man_id=mde.man_id and m.exe_ordre=2011 and mde_origine='VISA' 
            and m.mod_ordre=mp.mod_ordre and m.pai_ordre is null
            and mp.MOD_DOM='VIREMENT'
            and mde.ecd_ordre=ecd.ecd_ordre
            and abs(ecd_reste_emarger)<>abs(ecd_montant)
            and man_ht>0;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'Etat du mandat (' || objet.man_etat || ') incoherent avec etat du bordereau ('|| borEtat ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') ou bien mandat sur un bordereau de rejet et non ANNULE';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Intervention necessaire dans la base de données : remettre la bonne valeur dans le champ man_etat pour man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
  
   OPEN C2;
  LOOP
    FETCH c2 INTO objet;
    EXIT
  WHEN c2%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    probleme := 'La contrepartie de prise en charge du mandat  (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||') a été émargée alors que le mode de paiement est de type VIREMENT.';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Provoque un message d''erreur lors de la création d''un paiement (écritures déséquilibrées)', -- ZAP_CONSEQUENCE
        'Vous devez supprimer l''émargement puis modifier le mode de paiement du mandat si le mandat a réellement été payé autrement que par virement via Maracuja.'
        , --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c2;
  
  
  
  
  
  
END;

PROCEDURE checkMandatVsJefyDepense
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  borNum maracuja.bordereau.bor_num%type;
  borEtat maracuja.bordereau.bor_etat%type;
  pco_num_depense jefy_depense.depense_ctrl_planco.PCO_NUM%type;
  objet maracuja.mandat%ROWTYPE;
  CURSOR c1
  IS
    select m.*
    from maracuja.mandat m,
      maracuja.bordereau b, 
      jefy_depense.depense_ctrl_planco dpco
    where b.bor_id =m.bor_id
    and b.exe_ordre=exeOrdre
    and m.man_id=dpco.man_id
    and dpco.pco_num <> m.pco_num
   
    ;
BEGIN
  categorie := 'MANDAT';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'INCOHERENCE';
    -- select tbo_libelle into sousCategorie from maracuja.type_bordereau where tbo_ordre =leBordereau.tbo_ordre;
    select bor_num, bor_etat into borNum,  borEtat
    from bordereau
    where bor_id=objet.bor_id;
    
    select pco_num into pco_num_depense from jefy_depense.depense_ctrl_planco where man_id = objet.man_id;
    
    probleme := 'Imputation du mandat (' || objet.pco_num || ') incoherente avec l''imputation  de la depense côté budgétaire ('|| pco_num_depense ||') (Bord. '|| objet.ges_code ||' / ' || borNum ||' / Md. '|| objet.man_numero ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                                                                   --ZAP_ID,
        exeOrdre,                                                                                                                                -- EXE_ORDRE
        categorie,                                                                                                                               --ZAP_CATEGORIE,
        sousCategorie,                                                                                                                           --ZAP_SOUS_CATEGORIE,
        'MARACUJA.MANDAT',                                                                                                                       --ZAP_ENTITY,
        'MARACUJA.MANDAT.MAN_ID',                                                                                                                --ZAP_ENTITY_KEY,
        objet.man_id,                                                                                                                            --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                                                                                --ZAP_PROBLEME,
        'Entraine des differences entre comptabilite ordonnateur et comptabilite generale, il n''est plus possible d''intervenir sur le mandat', -- ZAP_CONSEQUENCE
        'Une raison possible est qu''il y a eu une réimputation comptable du mandat qui n''a pas été réimpactée budgétairement. Il est nécessaire d''intervenir dans la base de données pour le man_id='
        ||objet.man_id, --ZAP_SOLUTION,
        sysdate,        --ZAP_DATE)
        1               -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;


procedure checkEcritureDates(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture
    where exe_ordre=exeOrdre
    and to_char(ecr_date_saisie,'yyyy')<>to_char(exeOrdre) ;
BEGIN
  categorie := 'ECRITURES';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'La date de saisie de l''écriture est incohérente avec l''exercice (date '|| lecritureDetail.ecr_date_saisie ||' / code gestion ' || lecritureDetail.exe_ordre ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE.ECR_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecr_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Corriger le champ ecr_date_saisie dans la table maracuja.ecriture ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;


procedure checkEcritureDetailGestion(exeordre integer) 
is
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code not in
      (select ges_code
      from maracuja.gestion_exercice
      where exe_ordre              =exeOrdre
      );
BEGIN
  categorie := 'CODES GESTION';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un code gestion non actif sur l''exercice (numero '|| ecrNumero ||' / code gestion ' || lecritureDetail.ges_code ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Provoque des erreurs dans différents documents (dont compte financier)', -- ZAP_CONSEQUENCE
        'Activer le code gestion sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;



PROCEDURE checkEcritureDetailPlanco
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre
      and substr(pco_VALIDITE,1,1)<>'V'
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte non valide (numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice '|| exeOrdre,                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkEcritureDetailPlanco2
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  ecrNumero maracuja.ecriture.ecr_numero%type;
  lecritureDetail maracuja.ecriture_detail%ROWTYPE;
  CURSOR c1
  IS
    select *
    from maracuja.ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num   not in
      (select pco_num
      from maracuja.plan_comptable_exer
      where exe_ordre              =exeOrdre      
      );
BEGIN
  categorie := 'PLAN COMPTABLE';
  OPEN C1;
  LOOP
    FETCH c1 INTO lecritureDetail;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Ecriture';
    select ecr_numero
    into ecrNumero
    from maracuja.ecriture
    where ecr_ordre=lEcritureDetail.ecr_ordre;
    
    probleme := 'Ecriture passee sur un compte supprimé sur '|| exeOrdre || '(numero '|| ecrNumero ||' / compte ' || lecritureDetail.pco_num ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                 --ZAP_ID,
        exeOrdre,                                                              -- EXE_ORDRE
        categorie,                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                         --ZAP_SOUS_CATEGORIE,
        'MARACUJA.ECRITURE_DETAIL',                                            --ZAP_ENTITY,
        'MARACUJA.ECRITURE_DETAIL.ECD_ORDRE',                                  --ZAP_ENTITY_KEY,
        lecritureDetail.ecd_ordre,                                             --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                              --ZAP_PROBLEME,
        'Peut eventuellement provoquer des erreurs dans differents documents', -- ZAP_CONSEQUENCE
        'Activer le compte sur l''exercice ',                                  --ZAP_SOLUTION,
        sysdate,                                                               --ZAP_DATE)
        4                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionDepenses
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_depense.depense_papier.DPP_NUMERO_FACTURE%type;
  founom jefy_depense.v_Fournisseur.fou_nom%type;
  dateFacture jefy_depense.depense_papier.dpp_date_FACTURE%type;
  objet jefy_depense.depense_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_depense.depense_ctrl_action
    where exe_ordre  =exeOrdre
    and TYAC_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_depense
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Depenses';
    select dpp_numero_facture ,
      dpp_date_facture
    into numero,
      dateFacture
    from jefy_depense.depense_papier dpp,
      jefy_depense.depense_budget db
    where db.dpp_id=dpp.dpp_id
    and db.dep_id  =objet.dep_id;
    select distinct fou_nom
    into founom
    from jefy_depense.v_fournisseur f,
      jefy_depense.depense_budget db,
      jefy_depense.depense_papier dpp
    where db.dpp_id=dpp.dpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.dep_id  =objet.dep_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / fournisseur:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_DEPENSE.DEPENSE_CTRL_ACTION.DACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.DACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la liquidation ',                             --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkActionRecettes
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  numero jefy_recette.recette_papier.rpp_NUMERO%type;
  founom varchar2
  (
    200
  )
  ;
  dateFacture jefy_recette.recette_papier.rpp_date_recette%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  CURSOR c1
  IS
    select *
    from jefy_recette.recette_ctrl_action
    where exe_ordre  =exeOrdre
    and LOLF_ID not in
      (select lolf_id
      from jefy_admin.v_lolf_nomenclature_recette
      where exe_ordre=exeOrdre
      and tyet_id    =1
      );
BEGIN
  categorie := 'ACTIONS LOLF';
  OPEN C1;
  LOOP
    FETCH c1 INTO objet;
    EXIT
  WHEN c1%NOTFOUND;
    sousCategorie := 'Recettes';
    select rpp_numero,
      rpp_date_recette
    into numero,
      dateFacture
    from jefy_recette.recette_papier dpp,
      jefy_recette.recette_budget db
    where db.rpp_id=dpp.rpp_id
    and db.rec_id  =objet.rec_id;
    select distinct adr_nom
      ||' '
      ||adr_prenom
    into founom
    from grhum.v_fournis_grhum f,
      jefy_recette.recette_budget db,
      jefy_recette.recette_papier dpp
    where db.rpp_id=dpp.rpp_id
    and f.fou_ordre=dpp.fou_ordre
    and db.rec_id  =objet.rec_id;
    
    probleme := 'Une action a ete affectee a une depense et annulee depuis, ou bien le niveau d''execution pour les actions a change (numero: '|| numero ||' / date:' || dateFacture ||' / client:'|| fouNom ||')';
    INSERT
    INTO MARACUJA.ZANALYSE_PROBLEM VALUES
      (
        MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,                                                 --ZAP_ID,
        exeOrdre,                                                                              -- EXE_ORDRE
        categorie,                                                                             --ZAP_CATEGORIE,
        sousCategorie,                                                                         --ZAP_SOUS_CATEGORIE,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION',                                                    --ZAP_ENTITY,
        'JEFY_RECETTE.RECETTE_CTRL_ACTION.RACT_ID',                                            --ZAP_ENTITY_KEY,
        objet.RACT_ID,                                                                         --ZAP_ENTITY_KEY_VALUE,
        probleme,                                                                              --ZAP_PROBLEME,
        'Entraine des erreurs dans les documents reprenant l''execution du budget de gestion', -- ZAP_CONSEQUENCE
        'Reaffecter la bonne action au niveau de la recette ',                                 --ZAP_SOLUTION,
        sysdate,                                                                               --ZAP_DATE)
        1                                                                                      -- ZAP_NIVEAU
      ) ;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalancePi
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and (pco_num like '186%'
    or pco_num like '187%')
    and ges_code in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre
  and (pco_num like '186%'
  or pco_num like '187%');
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'PRESTATIONS INTERNES';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : le solde des comptes 186 et 187 devrait etre nul (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalanceGen
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE ETABLISSEMENT (HORS SACD)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is null
      )
  
  union all
  
  select 'BALANCE SACD '
    || comptabilite,
    debit,
    credit,
    solde_debiteur
  from
    (select ges_code as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and ges_code  in
      (select ges_code
      from gestion_exercice
      where exe_ordre  =exeOrdre
      and pco_num_185 is not null
      )
    group by ges_code
    )
  
  union all
  
  select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
    sum(ecd_debit) debit,
    sum(ecd_credit) credit,
    sum(ecd_debit) - sum(ecd_credit) solde_debiteur
  from ecriture_detail
  where exe_ordre=exeOrdre;
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'GENERALE';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;
PROCEDURE checkBalance185
  (
    exeordre INTEGER
  )
IS
  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  objet jefy_recette.recette_ctrl_action%ROWTYPE;
  vComptabilite varchar2
  (
    100
  )
  ;
  vDebit         number;
  vCredit        number;
  vSoldeDebiteur number;
  CURSOR c1
  IS
    select 'BALANCE AGREGEE (ETAB+SACDs)' as comptabilite,
      sum(ecd_debit) debit,
      sum(ecd_credit) credit,
      sum(ecd_debit) - sum(ecd_credit) solde_debiteur
    from ecriture_detail
    where exe_ordre=exeOrdre
    and pco_num like '185%';
BEGIN
  categorie     := 'BALANCE';
  sousCategorie := 'COMPTES DE LIAISON 185';
  OPEN C1;
  LOOP
    FETCH c1 INTO vComptabilite ,vDebit, vCredit, vSoldeDebiteur;
    EXIT
  WHEN c1%NOTFOUND;
    if (vSoldeDebiteur is not null and vSoldeDebiteur<>0) then
      probleme         := vComptabilite ||' : la balance du 185 n''est pas equilibree (D: '||vDebit ||', C: '|| vCredit ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          '',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    end if;
  END LOOP;
  CLOSE c1;
END;

procedure checkCadre2VsBalance(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vgescode gestion.ges_code%type;
  vpconum plan_comptable_exer.pco_num%type;
  vbaldebit number;
  vbalCredit number;
  vmandate number;
  vreverse number;
  vsolde number;
  vnet  number;
  

    cursor c1 is 
        select ges_code, pco_num,  sum(bal_debit) bal_debit, sum(bal_credit) bal_credit,  sum(bal_solde) bal_solde , sum(mandate) mandate, sum(reverse) reverse, sum(montant_net) montant_net
        from (
        select ges_code, ecd.pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '6%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, substr(ecd.pco_num,3,10) pco_num, sum(ecd_debit) bal_debit, sum(ecd_credit) bal_credit, sum(ecd_debit)- sum(ecd_credit) as bal_solde , 0 as mandate, 0 as reverse, 0 as montant_net
        from ecriture_detail ecd, ecriture e
        where 
        ecd.ecr_ordre=e.ecr_ordre
        and ecd.pco_num like '186%'
        and e.top_ordre<>12
        and ecd.exe_ordre=exeOrdre
        group by ges_code, ecd.pco_num
        union all
        select ges_code, pco_num, 0,0,0,sum(MANDATS), sum(reversements), sum(montant_net)
        from COMPTEFI.V_DVLOP_DEP
        where exe_ordre=exeOrdre
        and pco_num like '6%'
        group by ges_code, pco_num
        )
        group by ges_code, pco_num
        having (sum(bal_debit)<>sum(mandate) or sum(bal_credit)<> sum(reverse))
        order by ges_code, pco_num;

begin
    categorie     := 'COMPTE FINANCIER';
    sousCategorie := 'DIFFERENCE BALANCE/CADRE 2';
  OPEN C1;
  LOOP
    FETCH c1 INTO vgescode ,vpconum ,vbaldebit,vbalCredit,vSolde,vmandate, vreverse, vnet;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Code gestion: ' || vgescode|| ' Compte: ' || vPcoNum || ' : Balance <> cadre 2 (balance : D='||vBalDebit ||', C='|| vBalCredit ||') (Cadre 2 :  Mandats='||vMandate ||', ORV='|| vReverse ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les documents du compte financier', -- ZAP_CONSEQUENCE
          'Normal si certain bordereaux ne sont pas encore visés. Sinon incoherence dans les tables.',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;



procedure checkMontantsEmargements(exeOrdre integer) is

  flag INTEGER;
  categorie maracuja.zanalyse_problem.ZAP_CATEGORIE%type;
  sousCategorie maracuja.zanalyse_problem.ZAP_SOUS_CATEGORIE%type;
  probleme maracuja.zanalyse_problem.zap_probleme%type;
  vecd_ordre maracuja.ecriture_detail.ecd_ordre%type;
  vecr_numero maracuja.ecriture.ecr_numero%type;
  vecd_index maracuja.ecriture_detail.ecd_index%type;
  vecd_montant maracuja.ecriture_detail.ecd_montant%type;
  vecd_reste_emarge maracuja.ecriture_detail.ecd_reste_emarger%type;
  vemd_montant maracuja.emargement_detail.emd_montant%type;
  vcalcreste maracuja.ecriture_detail.ecd_reste_emarger%type;
  vpco_num maracuja.ecriture_detail.pco_num%type;

    cursor c1 is       
        select ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger , sum(emd_montant), abs(ecd_montant)-sum(emd_montant) 
        from (
            select distinct * from 
            (select ed.* from emargement_detail ed, emargement ema where ema.ema_ordre=ed.ema_ordre and ema_etat='VALIDE') emd, 
            ecriture_detail ecd ,
            ecriture ecr
            where (ecd_ordre=ecd_ordre_source or ecd_ordre = ecd_ordre_destination)
            and ecd.exe_ordre=exeOrdre
            and ecd.ecr_ordre=ecr.ecr_ordre
        ) 
        group by ecd_ordre, pco_num, ecr_numero, ecd_index, ecd_montant , ecd_reste_emarger
        having abs(ecd_montant)-sum(emd_montant)<> ecd_reste_emarger;

begin
    categorie     := 'ECRITURES';
    sousCategorie := 'MONTANTS EMARGEMENTS';
  OPEN C1;
  LOOP
    FETCH c1 INTO vecd_ordre, vpco_num, vecr_numero, vecd_index, vecd_montant , vecd_reste_emarge, vemd_montant, vcalcreste;
    EXIT
  WHEN c1%NOTFOUND;
   
      probleme         := 'Ecriture : ' || vecr_numero || '/'||  vecd_index ||', Compte: ' || vpco_num || ' : Montant du reste à émarger (='|| vecd_reste_emarge ||') différent du reste à émarger calculé (='||vcalcreste ||')';
      INSERT
      INTO MARACUJA.ZANALYSE_PROBLEM VALUES
        (
          MARACUJA.ZANALYSE_PROBLEM_SEQ.NEXTVAL,           --ZAP_ID,
          exeOrdre,                                        -- EXE_ORDRE
          categorie,                                       --ZAP_CATEGORIE,
          sousCategorie,                                   --ZAP_SOUS_CATEGORIE,
          null,                                            --ZAP_ENTITY,
          null,                                            --ZAP_ENTITY_KEY,
          null,                                            --ZAP_ENTITY_KEY_VALUE,
          probleme,                                        --ZAP_PROBLEME,
          'Erreurs sur les états de développement de solde', -- ZAP_CONSEQUENCE
          'Corrigez le reste à émarger de l''écriture (Bouton disponible dans l''écran de consultation des écritures de Maracuja).',                                              --ZAP_SOLUTION,
          sysdate,                                         --ZAP_DATE)
          1                                                -- ZAP_NIVEAU
        ) ;
    
  END LOOP;
  CLOSE c1;

end;


procedure checkAllProblemes
  (
    exeOrdre integer
  )
is
begin
  delete from MARACUJA.ZANALYSE_PROBLEM where exe_ordre=exeOrdre;
  
  checkRejets(exeordre);
  checkBordereauNonVise(exeOrdre);
  checkIncoherenceMandats(exeOrdre);
   checkMandatVsJefyDepense(exeOrdre);
  checkEcritureDetailPlanco(exeordre);
  checkEcritureDetailPlanco2(exeordre);
  checkEcritureDetailGestion(exeordre); 
  checkActionDepenses(exeordre);
  checkActionRecettes(exeordre);
  checkBalanceGen(exeOrdre);
  checkBalancePi(exeOrdre);
  checkBalance185(exeOrdre);
  checkCadre2VsBalance(exeOrdre);
  checkMontantsEmargements(exeOrdre);
  
end;
END;
/
  
--**************---

  
--**************---

  

create procedure grhum.inst_patch_maracuja_1910 is
	pconum maracuja.plan_comptable_exer.pco_num%type;
	exeOrdre jefy_admin.exercice.exe_ordre%type;
  cursor c1 is 
	  select distinct pco_num,exe_ordre
	  from (
		  select pco_num,exe_ordre from maracuja.titre
		  union all 
		  select pco_num,exe_ordre from maracuja.mandat
		  union all 
		  select pco_num,exe_ordre from maracuja.mandat_brouillard
		  union all 
		  select pco_num,exe_ordre from maracuja.titre_brouillard
		  union all 
		  select pco_num,exe_ordre from maracuja.bordereau_brouillard
		  union all 
		  select pco_num,exe_ordre from maracuja.CHEQUE_BROUILLARD
		  union all 
		  select pco_num,exe_ordre from maracuja.mode_paiement
	  )
	  where exe_ordre||'_'||PCO_NUM in ( select distinct exe_ordre||'_'||pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre||'_'||pco_num from maracuja.plan_comptable_exer);
begin
	delete from maracuja.planco_visa
	where exe_ordre||'_'||PCO_NUM_ORDONNATEUR in ( select distinct exe_ordre||'_'||pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre||'_'||pco_num from maracuja.plan_comptable_exer);
	
	commit;

	delete from maracuja.planco_credit where pcc_ordre in (
	  select pcc.pcc_ordre from maracuja.planco_credit pcc, jefy_admin.type_credit tcd 
	  where pcc.tcd_ordre=tcd.tcd_ordre 
	  and exe_ordre||'_'||pco_num in ( select distinct exe_ordre||'_'||pco_num from maracuja.plan_comptable pco, jefy_admin.exercice minus  select distinct exe_ordre||'_'||pco_num from maracuja.plan_comptable_exer)
	  );
	commit;  
	
	--Creation des planco_exer non presents
	 open c1;
      loop
         fetch c1 into  pconum,exeordre;
         exit when c1%notfound;
		 INSERT INTO MARACUJA.PLAN_COMPTABLE_EXER (
		   PCOE_ID, EXE_ORDRE, PCO_NUM, 
		   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
		   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
		   PCO_VALIDITE, PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
		   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE) 
			SELECT MARACUJA.PLAN_COMPTABLE_EXER_seq.nextval,
			exeOrdre,PCO_NUM, 
		   PCO_NIVEAU, PCO_BUDGETAIRE, PCO_EMARGEMENT, 
		   PCO_LIBELLE, PCO_NATURE, PCO_SENS_EMARGEMENT, 
		   'ANNULE', PCO_J_EXERCICE, PCO_J_FIN_EXERCICE, 
		   PCO_J_BE, PCO_COMPTE_BE, PCO_SENS_SOLDE
		FROM MARACUJA.PLAN_COMPTABLE P where pco_num=pconum;        
      end loop;
      close c1;
      commit;
      
      
    MARACUJA.UTIL.CREER_PARAMETRE_exenonrestr (  'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section1.compte', '4632', 'Compte de contrepartie à utiliser pour un ORV émis sur la masse de crédit 1 (Fonctionnement)' );
    MARACUJA.UTIL.CREER_PARAMETRE_exenonrestr (  'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section2.compte', '4632', 'Compte de contrepartie à utiliser pour un ORV émis sur la masse de crédit 2 (Equipements)' );
    MARACUJA.UTIL.CREER_PARAMETRE_exenonrestr (  'org.cocktail.gfc.comptabilite.contrepartie.depense.orv.section3.compte', '4632', 'Compte de contrepartie à utiliser pour un ORV émis sur la masse de crédit 3 (personnel)' );
 	MARACUJA.UTIL.CREER_PARAMETRE_exenonclos (  'org.cocktail.gfc.comptabilite.comptetva.racine', '445', 'Racine des comptes de TVA' );
        
        -- mettre a jour les mandat_detail_ecriture sur la tva
		update maracuja.mandat_detail_ecriture set mde_origine='VISA TVA' 
		where ecd_ordre in (select ecd_ordre from maracuja.ecriture_detail where pco_num like '4456%');        
        
		-- mettre à jour les ecritures 86* sur le journal des valeurs inactives
		update maracuja.ecriture set tjo_ordre=18 where ecr_ordre in (select distinct ecr_ordre from maracuja.ecriture_detail  
		where exe_ordre>=2011 and pco_num like '86%' and tjo_ordre=1);
        
		commit;
end;
/






