CREATE OR REPLACE PACKAGE MARACUJA.util
is
   procedure annuler_visa_bor_mandat (borid integer);

   procedure annuler_visa_bor_titre (borid integer);

   procedure supprimer_visa_btme (borid integer);

   procedure supprimer_visa_btms (borid integer);

   procedure supprimer_visa_btte (borid integer);

   procedure creer_ecriture_annulation (ecrordre integer);

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer;

   procedure annuler_emargement (emaordre integer);

   procedure supprimer_bordereau_dep (borid integer);

   procedure supprimer_bordereau_rec (borid integer);

   procedure supprimer_bordereau_btms (borid integer);

   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer);

   procedure supprimer_paiement_virement (paiordre integer);

   procedure corriger_etat_mandats;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type);
end;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.util
is
   -- version du 17/11/2008 -- ajout annulation bordereaux BTMS
   -- version du 28/08/2008 -- ajout annulation des ecritures dans annuler_visa_bor_mandat

   -- annulation du visa d'un bordereau de mandats (BTME + BTRU) (marche pas pour les bordereaux BTMS ni les OR)
   procedure annuler_visa_bor_mandat (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, mandat_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME' and tbotype <> 'BTRU') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME ou BTRU');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.mde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandats associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from mandat_detail_ecriture
               where man_id = tmpmanid;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
--
--                             SELECT count(*) INTO flag FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                   WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;
--                            if (flag>0) then
--                                SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
--                                       WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

      --                                DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
--                                DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

      --                                DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;
--                            end if;
--                            UPDATE MANDAT SET man_etat='ATTENTE' WHERE man_id=tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure annuler_visa_bor_titre (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      titre.tit_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;

      cursor lesecrs
      is
         select distinct ecr_ordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmpmanid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmpmanid;

         exit when lestitres%notfound;

         open lesecrs;

         loop
            fetch lesecrs
            into  tmpecrordre;

            exit when lesecrs%notfound;
            creer_ecriture_annulation (tmpecrordre);
         end loop;

         close lesecrs;

         delete from titre_detail_ecriture
               where tit_id = tmpmanid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmpmanid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTME (marche pas pour les bordereaux BTMS ni les OR)
   procedure supprimer_visa_btme (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTME') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTME');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

      -- verif si mandats deja paye
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and man_etat = 'PAYE';

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associés au bordereau bor_id= ' || borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
      end if;

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.mde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   mandat m, reimputation r
      where  bor_id = borid and r.man_id = m.man_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

         select count (*)
         into   flag
         from   ecriture_detail ecd, mandat_detail_ecriture mde
         where  ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

         if (flag > 0) then
            select distinct ecr_ordre
            into            tmpecrordre
            from            ecriture_detail ecd, mandat_detail_ecriture mde
            where           ecd.ecd_ordre = mde.ecd_ordre and mde.man_id = tmpmanid;

            delete from ecriture_detail
                  where ecr_ordre = tmpecrordre;

            delete from ecriture
                  where ecr_ordre = tmpecrordre;

            delete from mandat_detail_ecriture
                  where man_id = tmpmanid;
         end if;

         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   -- suppression des ecritures du visa d'un bordereau de depense BTMS
   procedure supprimer_visa_btms (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmpmanid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lesecritures
      is
         select distinct ed.ecr_ordre
         from            ecriture_detail ed, mandat_detail_ecriture mde, mandat m
         where           m.man_id = mde.man_id and mde.ecd_ordre = ed.ecd_ordre and m.bor_id = borid;

      cursor lesmandats
      is
         select man_id
         from   mandat
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTMS') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTMS');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat = 'VALIDE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas VISE ');
      end if;

      -- verif bordereau de mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun mandat associé au bordereau bor_id= ' || borid);
      end if;

/*            -- verif si mandats deja paye
            SELECT COUNT(*) INTO flag FROM MANDAT WHERE bor_id=borid AND man_etat='PAYE';
            IF (flag>0) THEN
                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associés au bordereau bor_id= '|| borid || ' ont deja ete paye, impossible d''annuler le visa. Un ordre de reversement est necessaire.');
               END IF;*/

      --verif si mandat rejete
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains mandat associé au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   mandat m, mandat_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.man_id = m.man_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux mandats ont ete emargees, Impossible d''annuler le visa.');
      end if;

--            -- verifier si ecritures <> VISA
--            SELECT COUNT(*) INTO flag FROM MANDAT m , MANDAT_DETAIL_ECRITURE mde, ECRITURE_DETAIL ecd WHERE bor_id=borid AND mde.MAN_ID=m.man_id AND mde.mde_origine<>'VISA' ;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certaines ecritures associées aux mandats ne correspondent pas au VISA, Impossible d''annuler le visa.');
--               END IF;

      --            -- verifier si reimputations
--            SELECT COUNT(*) INTO flag FROM MANDAT m , REIMPUTATION r WHERE bor_id=borid AND r.MAN_ID=m.man_id;
--            IF (flag>0) THEN
--                    RAISE_APPLICATION_ERROR (-20001,'Certains mandat associé au bordereau bor_id= '|| borid || ' ont ete reimputes, Impossible d''annuler le visa.');
--               END IF;
      open lesecritures;

      loop
         fetch lesecritures
         into  tmpecrordre;

         exit when lesecritures%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre in (select ecd_ordre
                                   from   ecriture_detail
                                   where  ecr_ordre = tmpecrordre);

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;
      end loop;

      close lesecritures;

      open lesmandats;

      loop
         fetch lesmandats
         into  tmpmanid;

         exit when lesmandats%notfound;

/*            SELECT DISTINCT ecr_ordre INTO tmpEcrOrdre FROM ECRITURE_DETAIL ecd, MANDAT_DETAIL_ECRITURE mde
               WHERE ecd.ecd_ordre = mde.ecd_ordre AND mde.man_id=tmpmanid;

            DELETE FROM ECRITURE_DETAIL WHERE ecr_ordre=tmpEcrOrdre;
            DELETE FROM ECRITURE WHERE ecr_ordre=tmpEcrOrdre;

            DELETE FROM MANDAT_DETAIL_ECRITURE WHERE man_id = tmpmanid;*/
         update mandat
            set man_etat = 'ATTENTE'
          where man_id = tmpmanid;
      end loop;

      close lesmandats;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;

      update jefy_paye.jefy_paye_compta
         set ecr_ordre_visa = null,
             ecr_ordre_sacd = null,
             ecr_ordre_opp = null
       where bor_id in (borid);
   end;

   -- annulation du visa d'un bordereau de depense BTTE
   procedure supprimer_visa_btte (borid integer)
   is
      flag          integer;
      lebordereau   bordereau%rowtype;
      tbotype       type_bordereau.tbo_type%type;
      tmptitid      mandat.man_id%type;
      tmpecrordre   ecriture_detail.ecr_ordre%type;

      cursor lestitres
      is
         select tit_id
         from   titre
         where  bor_id = borid;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun bordereau correspondant au bor_id= ' || borid);
      end if;

      select *
      into   lebordereau
      from   bordereau
      where  bor_id = borid;

      -- verif si exercice >2006
      if (lebordereau.exe_ordre < 2007) then
         raise_application_error (-20001, 'Impossible d''annuler le visa d''un bordereau emis avant 2007');
      end if;

      -- verif type bordereau BTME
      select tbo_type
      into   tbotype
      from   type_bordereau
      where  tbo_ordre = lebordereau.tbo_ordre;

      if (tbotype <> 'BTTE') then
         raise_application_error (-20001, 'Le type de bordereau n''est pas BTTE');
      end if;

      -- verif si le bordereau n''est pas vise
      if (lebordereau.bor_etat <> 'VISE') then
         raise_application_error (-20001, 'Le bordereau correspondant au bor_id= ' || borid || ' n''est pas a l''etat VISE ');
      end if;

      -- verif bordereau de titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun titre associé au bordereau bor_id= ' || borid);
      end if;

      --verif si titre rejete
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid and brj_ordre is not null;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete rejetes, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures emargees
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.ecd_ordre = ecd.ecd_ordre and abs (ecd.ecd_reste_emarger) <> abs (ecd_montant);

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ont ete emargees, Impossible d''annuler le visa.');
      end if;

      -- verifier si ecritures <> VISA
      select count (*)
      into   flag
      from   titre m, titre_detail_ecriture mde, ecriture_detail ecd
      where  bor_id = borid and mde.tit_id = m.tit_id and mde.tde_origine <> 'VISA';

      if (flag > 0) then
         raise_application_error (-20001, 'Certaines ecritures associées aux titres ne proviennent pas au VISA, Impossible d''annuler le visa.');
      end if;

      -- verifier si reimputations
      select count (*)
      into   flag
      from   titre m, reimputation r
      where  bor_id = borid and r.tit_id = m.tit_id;

      if (flag > 0) then
         raise_application_error (-20001, 'Certains titres associés au bordereau bor_id= ' || borid || ' ont ete reimputes, Impossible d''annuler le visa.');
      end if;

      open lestitres;

      loop
         fetch lestitres
         into  tmptitid;

         exit when lestitres%notfound;

         select distinct ecr_ordre
         into            tmpecrordre
         from            ecriture_detail ecd, titre_detail_ecriture mde
         where           ecd.ecd_ordre = mde.ecd_ordre and mde.tit_id = tmptitid;

         delete from ecriture_detail
               where ecr_ordre = tmpecrordre;

         delete from ecriture
               where ecr_ordre = tmpecrordre;

         delete from titre_detail_ecriture
               where tit_id = tmptitid;

         update titre
            set tit_etat = 'ATTENTE'
          where tit_id = tmptitid;
      end loop;

      close lestitres;

      update bordereau
         set bor_etat = 'VALIDE'
       where bor_id = borid;

      update bordereau
         set bor_date_visa = null
       where bor_id = borid;

      update bordereau
         set utl_ordre_visa = null
       where bor_id = borid;
   end;

   procedure creer_ecriture_annulation (ecrordre integer)
   is
      ecr           maracuja.ecriture%rowtype;
      ecd           maracuja.ecriture_detail%rowtype;
      flag          integer;
      str           ecriture.ecr_libelle%type;
      newecrordre   ecriture.ecr_ordre%type;
      newecdordre   ecriture_detail.ecd_ordre%type;
      x             integer;

      cursor c1
      is
         select *
         from   maracuja.ecriture_detail
         where  ecr_ordre = ecrordre;
   begin
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucune ecriture retrouvee pour ecr_ordre= ' || ecrordre || ' .');
      end if;

      select *
      into   ecr
      from   maracuja.ecriture
      where  ecr_ordre = ecrordre;

      str := 'Annulation Ecriture ' || ecr.exe_ordre || '/' || ecr.ecr_numero;

      -- verifier que l'ecriture n'a pas deja ete annulee
      select count (*)
      into   flag
      from   maracuja.ecriture
      where  ecr_libelle = str;

      if (flag > 0) then
         select ecr_numero
         into   flag
         from   maracuja.ecriture
         where  ecr_libelle = str;

         raise_application_error (-20001, 'L''ecriture numero ' || ecr.ecr_numero || ' (ecr_ordre= ' || ecrordre || ') a deja ete annulee par l''ecriture numero ' || flag || '.');
      end if;

      -- verifier que les details ne sont pas emarges
      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;

         if (ecd.ecd_reste_emarger < abs (ecd.ecd_montant)) then
            raise_application_error (-20001, 'L''ecriture ' || ecr.ecr_numero || ' ecr_ordre= ' || ecrordre || ' a ete emargee pour le compte ' || ecd.pco_num || '. Impossible d''annuler');
         end if;
      end loop;

      close c1;

--            -- supprimer les mandat_detail_ecriture et titre_detail_ecriture associes
--            -- on ne fait pas, trop dangeureux...
--            OPEN c1;
--             LOOP
--                 FETCH c1 INTO ecd;
--                    EXIT WHEN c1%NOTFOUND;
--
--

      --             END LOOP;
--             CLOSE c1;
      newecrordre := api_plsql_journal.creerecriture (ecr.com_ordre, sysdate, str, ecr.exe_ordre, ecr.ori_ordre, ecr.tjo_ordre, ecr.top_ordre, ecr.utl_ordre);

      open c1;

      loop
         fetch c1
         into  ecd;

         exit when c1%notfound;
         newecdordre := api_plsql_journal.creerecrituredetail (ecd.ecd_commentaire, ecd.ecd_libelle, -ecd.ecd_montant, ecd.ecd_secondaire, ecd.ecd_sens, newecrordre, ecd.ges_code, ecd.pco_num);
         x := creeremargementmemesens (ecd.ecd_ordre, newecdordre, 1);
      end loop;

      close c1;

      numerotationobject.numeroter_ecriture (newecrordre);
   end;

   function creeremargementmemesens (ecdordresource integer, ecdordredest integer, typeemargement type_emargement.tem_ordre%type)
      return integer
   is
   begin
      return api_emargement.creeremargementmemesens (ecdordresource, ecdordredest, typeemargement);
   end;

--
--       function creerEmargementMemeSens(ecdOrdreSource integer, ecdOrdreDest integer, typeEmargement type_emargement.tem_ordre%type ) return integer is
--            emaordre EMARGEMENT.EMA_ORDRE%type;
--            ecdSource ecriture_detail%rowtype;
--            ecdDest ecriture_detail%rowtype;
--
--            utlOrdre ecriture.utl_ordre%type;
--            comOrdre ecriture.com_ordre%type;
--            exeOrdre ecriture.exe_ordre%type;
--            emaMontant emargement.ema_montant%type;
--            flag integer;
--       BEGIN

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreSource || ' .');
--            end if;

   --            select count(*) into flag from ecriture_detail where ecd_ordre = ecdOrdreDest;
--            if (flag=0) then
--                RAISE_APPLICATION_ERROR (-20001,'Aucune ecriture_detail retrouvee pour ecd_ordre= '|| ecdordreDest || ' .');
--            end if;

   --            select * into ecdSource from ecriture_detail where ecd_ordre = ecdOrdreSource;
--            select * into ecdDest from ecriture_detail where ecd_ordre = ecdOrdreDest;

   --            -- verifier que les ecriture_detail sont sur le meme sens
--            if (ecdSource.ecd_sens <> ecdDest.ecd_sens) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail n''ont pas le meme sens ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;
--
--            -- verifier que les ecriture_detail ont le meme compte
--            if (ecdSource.pco_num <> ecdDest.pco_num) then
--                RAISE_APPLICATION_ERROR (-20001,'Les ecriture_detail ne sont pas sur le meme pco_num ecd_ordre= '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les ecriture_detail ne sont pas emargees
--            if (ecdSource.ecd_reste_emarger <> abs(ecdSource.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreSource ||' .');
--            end if;
--            if (ecdDest.ecd_reste_emarger <> abs(ecdDest.ecd_montant)) then
--                RAISE_APPLICATION_ERROR (-20001,'L ecriture_detail est deja emargee ecd_ordre= '|| ecdOrdreDest ||' .');
--            end if;

   --            -- verifier que les montant s'annulent
--            if (ecdSource.ecd_montant+ecdDest.ecd_montant <> 0) then
--                RAISE_APPLICATION_ERROR (-20001,'La somme des montants doit etre nulle ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- verifier que les exercices sont les memes
--            if (ecdSource.exe_ordre <> ecdDest.exe_ordre) then
--                RAISE_APPLICATION_ERROR (-20001,'Les exercices sont differents ecdOrdre = '|| ecdordreDest || ', '|| ecdOrdreSource ||' .');
--            end if;

   --            -- trouver le montant a emarger
--            select min(abs(ecd_montant)) into emaMontant from ecriture_detail where ecd_ordre = ecdordresource or ecd_ordre = ecdordredest;

   --            -- trouver l'utilisateur
--            select utl_ordre into utlOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select com_ordre into comordre from ecriture where ecr_ordre in ecdSource.ecr_ordre;
--            select exe_ordre into exeOrdre from ecriture where ecr_ordre in ecdSource.ecr_ordre;

   --            -- creation de l emargement
--             SELECT emargement_seq.NEXTVAL INTO EMAORDRE FROM dual;

   --             INSERT INTO EMARGEMENT
--              (EMA_DATE, EMA_NUMERO, EMA_ORDRE, EXE_ORDRE, TEM_ORDRE, UTL_ORDRE, COM_ORDRE, EMA_MONTANT, EMA_ETAT)
--             VALUES
--              (
--              SYSDATE,
--              0,
--              EMAORDRE,
--              ecdSource.exe_ordre,
--              typeEmargement,
--              utlOrdre,
--              comordre,
--              emaMontant,
--              'VALIDE'
--              );

   --              -- creation de l emargement detail
--              INSERT INTO EMARGEMENT_DETAIL
--              (ECD_ORDRE_DESTINATION, ECD_ORDRE_SOURCE, EMA_ORDRE, EMD_MONTANT, EMD_ORDRE, EXE_ORDRE)
--              VALUES
--              (
--              ecdSource.ecd_ordre,
--              ecdDest.ecd_ordre,
--              EMAORDRE,
--              emaMontant,
--              emargement_detail_seq.NEXTVAL,
--              exeOrdre
--              );

   --              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdSource.ecd_ordre;
--              UPDATE ECRITURE_DETAIL SET ecd_reste_emarger = ecd_reste_emarger-emaMontant WHERE ecd_ordre = ecdDest.ecd_ordre;

   --              NUMEROTATIONOBJECT.numeroter_emargement(emaOrdre);

   --              return emaOrdre;
--       END;
--
--
   procedure annuler_emargement (emaordre integer)
   as
   begin
      api_emargement.annuler_emargement (emaordre);
   end;

   procedure supprimer_bordereau_dep (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des mandats
      select count (*)
      into   flag
      from   mandat
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de depense.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les mandat_brouillards
      delete from maracuja.mandat_brouillard
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- supprimer les depenses
      delete from maracuja.depense
            where man_id in (select man_id
                             from   mandat
                             where  bor_id = borid);

      -- mettre a jour les depense_ctrl_planco
      update jefy_depense.depense_ctrl_planco
         set man_id = null
       where man_id in (select man_id
                        from   maracuja.mandat
                        where  bor_id = borid);

      -- supprimer les mandats
      delete from maracuja.mandat
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   procedure supprimer_bordereau_btms (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau b, type_bordereau tb
      where  bor_id = borid and b.tbo_ordre = tb.tbo_ordre and tb.tbo_type = 'BTMS';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas ou n''est pas de type BTMS');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      update jefy_paye.jefy_paye_compta
         set jpc_etat = 'LIQUIDEE',
             bor_id = null
       where bor_id in (borid);

      --update jefy_paf.paf_etape set PAE_ETAT = 'LIQUIDEE', bor_id=null WHERE bor_id IN (borid);
      supprimer_bordereau_dep (borid);
   end;

   procedure supprimer_bordereau_rec (borid integer)
   as
      flag   number;
   begin
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''existe pas.');
      end if;

      --verifier que le bordereau a bien des titres
      select count (*)
      into   flag
      from   titre
      where  bor_id = borid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' n''est pas un bordereau de recette.');
      end if;

      -- verifier que le bordereau n'est pas vise
      select count (*)
      into   flag
      from   bordereau
      where  bor_id = borid and bor_etat = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le bordereau borid= ' || borid || ' a deja ete vise.');
      end if;

      -- supprimer les BORDEREAU_INFO
      delete from maracuja.bordereau_info
            where bor_id in (borid);

      -- supprimer les BORDEREAU_BROUILLARD
      delete from maracuja.bordereau_brouillard
            where bor_id in (borid);

      -- supprimer les titre_brouillards
      delete from maracuja.titre_brouillard
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- supprimer les recettes
      delete from maracuja.recette
            where tit_id in (select tit_id
                             from   maracuja.titre
                             where  bor_id = borid);

      -- mettre a jour les recette_ctrl_planco
      update jefy_recette.recette_ctrl_planco
         set tit_id = null
       where tit_id in (select tit_id
                        from   maracuja.titre
                        where  bor_id = borid);

      -- supprimer les titres
      delete from maracuja.titre
            where bor_id = borid;

      -- supprimer le bordereau
      delete from maracuja.bordereau
            where bor_id = borid;
   end;

   /* Permet d'annuler un recouvrement avec prelevements (non transmis a la TG) Il faut indiquer les numero min et max des ecritures dependant de ce recouvrement Un requete est dispo en commentaire dans le code pour ca */
   procedure annuler_recouv_prelevement (recoordre integer, ecrnumeromin integer, ecrnumeromax integer)
   is
      flag             integer;
      lerecouvrement   recouvrement%rowtype;
      tmpemaordre      emargement.ema_ordre%type;
      tmpecrordre      ecriture.ecr_ordre%type;

      cursor emargements
      is
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_source in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))))
         union
         select distinct ema_ordre
         from            emargement_detail
         where           ecd_ordre_destination in (
                            select ecd.ecd_ordre
                            from   ecriture_detail ecd, ecriture e
                            where  ecd.ecd_reste_emarger <> abs (ecd_montant)
                            and    ecd.ecr_ordre = e.ecr_ordre
                            and    e.ecr_numero >= ecrnumeromin
                            and    e.ecr_numero <= ecrnumeromax
                            and    ecd_ordre in (select ecd_ordre
                                                 from   titre_detail_ecriture
                                                 where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                   from   echeancier
                                                                                                   where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                    from   prelevement
                                                                                                                                    where  reco_ordre = recoordre))));

      cursor ecritures
      is
         select distinct e.ecr_ordre
         from            ecriture_detail ecd, ecriture e
         where           ecd.ecr_ordre = e.ecr_ordre and e.ecr_numero >= ecrnumeromin and e.ecr_numero <= ecrnumeromax and ecd_ordre in (select ecd_ordre
                                                                                                                                         from   titre_detail_ecriture
                                                                                                                                         where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                                                                                                           from   echeancier
                                                                                                                                                                                           where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                                                                                                            from   prelevement
                                                                                                                                                                                                                            where  reco_ordre = recoordre)));
   begin
      -- recuperer les numeros des ecritures min et max pour les passer en parametre
      --select * from ecriture_detail  ecd, ecriture e where e.ecr_ordre=ecd.ecr_ordre and ecr_date_saisie = (select RECO_DATE_CREATION from recouvrement where reco_ordre=28) and ecd.ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 28) )) order by ecr_numero
      select count (*)
      into   flag
      from   prelevement
      where  reco_ordre = recoordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun prelevement correspondant a recoordre= ' || recoordre);
      end if;

      select *
      into   lerecouvrement
      from   recouvrement
      where  reco_ordre = recoordre;

      -- verifier les dates des ecritures avec la date du prelevement
      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromin;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      select ecr_date_saisie - lerecouvrement.reco_date_creation
      into   flag
      from   ecriture
      where  exe_ordre = lerecouvrement.exe_ordre and ecr_numero = ecrnumeromax;

      if (flag <> 0) then
         raise_application_error (-20001, 'Les dates des ecriture fournies ne sont pas coherentes avec la date du recouvrement.');
      end if;

      -- supprimer les emargements
      open emargements;

      loop
         fetch emargements
         into  tmpemaordre;

         exit when emargements%notfound;
         annuler_emargement (tmpemaordre);
      end loop;

      close emargements;

      -- modifier les etats des prelevements
      update prelevement
         set prel_etat_maracuja = 'ATTENTE'
       where reco_ordre = recoordre;

      --supprimer le contenu du fichier (prelevement_fichier)
      delete from prelevement_fichier
            where reco_ordre = recoordre;

      -- supprimer les écritures de recouvrement (ecriture_detail, ecriture et titre_detail_ecriture)
      open ecritures;

      loop
         fetch ecritures
         into  tmpecrordre;

         exit when ecritures%notfound;
         creer_ecriture_annulation (tmpecrordre);
      end loop;

      close ecritures;

      delete from titre_detail_ecriture
            where ecd_ordre in (
                         select distinct ecd.ecd_ordre
                         from            ecriture_detail ecd, ecriture e
                         where           ecd.ecr_ordre = e.ecr_ordre
                         and             e.ecr_numero >= ecrnumeromin
                         and             e.ecr_numero <= ecrnumeromax
                         and             ecd_ordre in (select ecd_ordre
                                                       from   titre_detail_ecriture
                                                       where  tde_origine = 'PRELEVEMENT' and rec_id in (select rec_id
                                                                                                         from   echeancier
                                                                                                         where  eche_echeancier_ordre in (select eche_echeancier_ordre
                                                                                                                                          from   prelevement
                                                                                                                                          where  reco_ordre = recoordre))));

      -- supprimer association des prélèvements au recouvrement
      update prelevement
         set reco_ordre = null
       where reco_ordre = recoordre;

      -- supprimer l'objet recouvrement
      delete from recouvrement
            where reco_ordre = recoordre;
--        Select * from recette where rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )
--        select * from ecriture_detail where ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))
--        select * from ecriture_detail ecd, ecriture e where ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) ))

   --        --emargements
--        select distinct ema_ordre from emargement_detail where ecd_ordre_source in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--        union
--        select distinct ema_ordre from emargement_detail where ecd_ordre_destination in (select ecd.ecd_ordre from ecriture_detail ecd, ecriture e where ecd.ecd_reste_emarger<>abs(ecd_montant) and ecd.ecr_ordre=e.ecr_ordre and e.ecr_numero>=63371 and e.ecr_numero<=64284 and ecd_ordre in (select ecd_ordre from titre_detail_ecriture where tde_origine='PRELEVEMENT' and rec_id in (select rec_id from echeancier where eche_echeancier_ordre in (select ECHE_ECHEANCIER_ORDRE from prelevement where RECO_ORDRE = 35) )))
--
   end;

   -- suppression d'un paiement de type virement avec annulation des émargements et écritures de paiements
   procedure supprimer_paiement_virement (paiordre integer)
   is
      flag       integer;
      ecdp       ecriture_detail%rowtype;
      ecdordre   ecriture_detail.ecd_ordre%type;
      emaordre   emargement.ema_ordre%type;
      ecrordre   ecriture.ecr_ordre%type;

      cursor ecdpaiement5
      is
         select ecd.*
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.man_id = mde.man_id
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    mde_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.*
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre
         and    m.odp_ordre = mde.odp_ordre
         and    mde.ecd_ordre = ecd.ecd_ordre
         and    ope_origine = 'VIREMENT'
         and    ecd.ecd_sens = 'C'
         and    ecd.pco_num like '5%'
         and    p.pai_ordre = paiordre
         and    ecd.ecr_ordre = e.ecr_ordre
         and    e.ecr_libelle like 'PAIEMENT%';

      cursor ecdpaiementall
      is
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union all
         select ecd.ecd_ordre
         from   ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where  p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor ecrordres
      is
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, mandat_detail_ecriture mde, mandat m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.man_id = mde.man_id and mde.ecd_ordre = ecd.ecd_ordre and mde_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%'
         union
         select distinct ecd.ecr_ordre
         from            ecriture_detail ecd, ecriture e, odpaiem_detail_ecriture mde, ordre_de_paiement m, paiement p
         where           p.pai_ordre = m.pai_ordre and m.odp_ordre = mde.odp_ordre and mde.ecd_ordre = ecd.ecd_ordre and ope_origine = 'VIREMENT' and p.pai_ordre = paiordre and ecd.ecr_ordre = e.ecr_ordre and e.ecr_libelle like 'PAIEMENT%';

      cursor emaordreforecd
      is
         select emd.ema_ordre
         from   emargement_detail emd, emargement ema
         where  ema.ema_ordre = emd.ema_ordre and ema_etat = 'VALIDE' and ecd_ordre_source = ecdordre or ecd_ordre_destination = ecdordre;
   begin
      select count (*)
      into   flag
      from   maracuja.paiement
      where  pai_ordre = paiordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Aucun paiement trouvé correspondant a pai_ordre= ' || paiordre);
      end if;

      --verifier que classe 5 non émargée
      open ecdpaiement5;

      loop
         fetch ecdpaiement5
         into  ecdp;

         exit when ecdpaiement5%notfound;

         if (ecdp.ecd_reste_emarger <> abs (ecdp.ecd_credit)) then
            raise_application_error (-20001, 'L''écriture ' || ecdp.pco_num || ' - ' || ecdp.ecd_libelle || ' a été émargée, supprimez l''émargement.');
         end if;
      end loop;

      close ecdpaiement5;

      -- supprimer tous les emargements
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         open emaordreforecd;

         loop
            fetch emaordreforecd
            into  emaordre;

            exit when emaordreforecd%notfound;
            api_emargement.annuler_emargement (emaordre);
         end loop;

         close emaordreforecd;
      end loop;

      close ecdpaiementall;

      --  creer ecritures annulation
      open ecrordres;

      loop
         fetch ecrordres
         into  ecrordre;

         exit when ecrordres%notfound;
         creer_ecriture_annulation (ecrordre);
      end loop;

      close ecrordres;

      -- supprimer les mde et ope
      open ecdpaiementall;

      loop
         fetch ecdpaiementall
         into  ecdordre;

         exit when ecdpaiementall%notfound;

         delete from mandat_detail_ecriture
               where ecd_ordre = ecdordre;

         delete from odpaiem_detail_ecriture
               where ecd_ordre = ecdordre;
      end loop;

      close ecdpaiementall;

      -- changer etats des mandats et ODP
      update mandat
         set man_etat = 'VISE',
             pai_ordre = null
       where pai_ordre = paiordre;

      update ordre_de_paiement
         set odp_etat = 'VALIDE',
             pai_ordre = null
       where pai_ordre = paiordre;

      -- supprimer le paiement
      delete from virement_fichier
            where pai_ordre = paiordre;

      delete from paiement
            where pai_ordre = paiordre;
   end;

   procedure corriger_etat_mandats
   is
   begin
      update maracuja.mandat
         set man_etat = 'VISE'
       where man_id in (select m.man_id
                        from   maracuja.mandat m, maracuja.mandat_detail_ecriture mde
                        where  m.man_id = mde.man_id and m.man_etat = 'ATTENTE' and mde.mde_origine = 'VISA');
   end;

   function getpconumvalidefromparam (exeordre integer, paramkey varchar)
      return plan_comptable_exer.pco_num%type
   is
      flag     integer;
      pconum   plan_comptable_exer.pco_num%type;
   begin
      select count (*)
      into   flag
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' n''a pas été trouvé dans la table maracuja.parametre pour l''exercice ' || exeordre);
      end if;

      select par_value
      into   pconum
      from   parametre
      where  par_key = paramkey and exe_ordre = exeordre;

      select count (*)
      into   flag
      from   plan_comptable_exer
      where  pco_num = pconum and exe_ordre = exeordre and pco_validite = 'VALIDE';

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre ' || paramkey || ' contient la valeur ' || pconum || ' qui ne correspond pas a un compte valide du plan comptable de ' || exeordre);
      end if;

      return pconum;
   end;

   procedure creer_parametre (exeordre integer, parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   parametre
      where  exe_ordre = exeordre and par_key = parkey;

      if (flag = 0) then
         insert into parametre
                     (par_ordre,
                      exe_ordre,
                      par_description,
                      par_key,
                      par_value
                     )
         values      (parametre_seq.nextval,
                      exeordre,
                      pardescription,
                      parkey,
                      parvalue
                     );
      end if;
   end;

   procedure creer_parametre_exenonclos (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat <> 'C';

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;

   procedure creer_parametre_exenonrestr (parkey parametre.par_key%type, parvalue parametre.par_value%type, pardescription parametre.par_description%type)
   is
      cursor c1
      is
         select exe_ordre
         from   jefy_admin.exercice
         where  exe_stat in ('O', 'P');

      exeordre   jefy_admin.exercice.exe_ordre%type;
   begin
      open c1;

      loop
         fetch c1
         into  exeordre;

         exit when c1%notfound;
         creer_parametre (exeordre, parkey, parvalue, pardescription);
      end loop;

      close c1;
   end;
end;
/


