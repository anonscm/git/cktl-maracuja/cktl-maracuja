CREATE OR REPLACE PACKAGE MARACUJA.cptefiutil IS
    PROCEDURE solde_6_7 ( exeordre INTEGER,utlordre INTEGER,libelle VARCHAR, rtatcomp VARCHAR);
    PROCEDURE annuler_ecriture (monecdordre INTEGER);
    PROCEDURE numeroter_ecriture (ecrordre INTEGER);
END;
/



CREATE OR REPLACE PACKAGE BODY MARACUJA.cptefiutil
is
   procedure solde_6_7 (exeordre integer, utlordre integer, libelle varchar, rtatcomp varchar)
   is
      legescode       gestion.ges_code%type;
      lepconum        plan_comptable_exer.pco_num%type;
      lesolde         number (12, 2);
      debit           number (12, 2);
      credit          number (12, 2);
      credit120       number (12, 2);
      debit120        number (12, 2);
      credit129       number (12, 2);
      debit129        number (12, 2);

      cursor solde6
      is
         select ges_code,
                pco_num,
                solde_crediteur
         from   cfi_solde_6_7
         where  solde_crediteur != 0 and (pco_num like '6%' or pco_num like '186%') and exe_ordre = exeordre;

      cursor solde7
      is
         select ges_code,
                pco_num,
                solde_crediteur
         from   cfi_solde_6_7
         where  solde_crediteur != 0 and (pco_num like '7%' or pco_num like '187%') and exe_ordre = exeordre;

      cursor lesgescode
      is
         select distinct ges_code,
                         pco_num_185
         from            gestion_exercice
         where           exe_ordre = exeordre;

      cursor lessacds
      is
         select distinct ges_code
         from            gestion_exercice
         where           exe_ordre = exeordre and pco_num_185 is not null;

      nb              integer;
      gescodeagence   comptabilite.ges_code%type;
      comordre        comptabilite.com_ordre%type;
      topordre        type_operation.top_ordre%type;
      tjoordre        type_journal.tjo_ordre%type;
      monecdordre     ecriture.ecr_ordre%type;
      ecdordre        ecriture_detail.ecd_ordre%type;
      exeexercice     exercice.exe_exercice%type;
   begin
-- creation de l'ecriture de solde --
      select top_ordre
      into   topordre
      from   type_operation
      where  top_libelle = 'ECRITURE SOLDE 6 ET 7';

      select tjo_ordre
      into   tjoordre
      from   type_journal
      where  tjo_libelle = 'JOURNAL FIN EXERCICE';

      select com_ordre,
             ges_code
      into   comordre,
             gescodeagence
      from   comptabilite;

      select exe_exercice
      into   exeexercice
      from   exercice
      where  exe_ordre = exeordre;

-- creation de lecriture  --
      monecdordre := maracuja.api_plsql_journal.creerecriture (comordre, to_date ('31/12/' || exeexercice, 'dd/mm/yyyy'), libelle, exeordre, null,   --ORIORDRE,
                                                               tjoordre, topordre, utlordre);

-- Calcul du résultat par composante ---
-- Etablissement + SACD --
      if rtatcomp = 'O' then
         -- creation de l ecriture de resultat --
         open lesgescode;

         loop
            fetch lesgescode
            into  legescode,
                  lepconum;

            exit when lesgescode%notfound;

            select count (*)
            into   nb
            from   cfi_solde_6_7
            where  exe_ordre = exeordre and ges_code = legescode;

            if nb != 0 then
               --  composante SACD --
               --       IF lepco_num is not null  THEN
               select nvl (-sum (solde_crediteur), 0)
               into   debit
               from   cfi_solde_6_7
               where  (pco_num like '6%' or pco_num like '186%') and ges_code = legescode and exe_ordre = exeordre;

               select nvl (sum (solde_crediteur), 0)
               into   credit
               from   cfi_solde_6_7
               where  (pco_num like '7%' or pco_num like '187%') and ges_code = legescode and exe_ordre = exeordre;

               if debit > credit then
                  -- RESULTAT SOLDE DEBITEUR 129
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  --  contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
               else
                  -- RESULTAT SOLDE CREDITEUR 120 --
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  -- contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
               end if;
            end if;
         end loop;

         close lesgescode;
      else
         -- resultat = 'Etablissement' --
         select count (*)
         into   nb
         from   cfi_solde_6_7
         where  exe_ordre = exeordre and ges_code in (select ges_code
                                                      from   gestion_exercice
                                                      where  exe_ordre = exeordre and pco_num_185 is null);

         if nb != 0 then
            select nvl (-sum (solde_crediteur), 0)
            into   debit
            from   cfi_solde_6_7
            where  (pco_num like '6%' or pco_num like '186%') and exe_ordre = exeordre and ges_code in (select ges_code
                                                                                                        from   gestion_exercice
                                                                                                        where  exe_ordre = exeordre and pco_num_185 is null);

            select nvl (sum (solde_crediteur), 0)
            into   credit
            from   cfi_solde_6_7
            where  (pco_num like '7%' or pco_num like '187%') and exe_ordre = exeordre and ges_code in (select ges_code
                                                                                                        from   gestion_exercice
                                                                                                        where  exe_ordre = exeordre and pco_num_185 is null);

            if debit > credit then
               -- RESULTAT SOLDE DEBITEUR 129
               -- contre-partie classe 7 crédit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           credit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'C',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '129'   --PCONUM
                                                                                );
               --  contre-partie classe 6 débit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           debit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'D',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '129'   --PCONUM
                                                                                );
            else
               -- RESULTAT SOLDE CREDITEUR 120 --
               -- contre-partie classe 7 crédit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           credit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'C',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '120'   --PCONUM
                                                                                );
               -- contre-partie classe 6 débit
               ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                           libelle,
                                                                           --ECDLIBELLE,
                                                                           debit,   --ECDMONTANT,
                                                                           null,   --ECDSECONDAIRE,
                                                                           'D',   --ECDSENS,
                                                                           monecdordre,   --ECRORDRE,
                                                                           gescodeagence,   --GESCODE,
                                                                           '120'   --PCONUM
                                                                                );
            end if;
         end if;

         -- Résultat des SACD ---
         open lessacds;

         loop
            fetch lessacds
            into  legescode;

            exit when lessacds%notfound;

            select count (*)
            into   nb
            from   cfi_solde_6_7
            where  exe_ordre = exeordre and ges_code = legescode;

            if nb != 0 then
               --  SACD --
               --       IF lepco_num is not null  THEN
               select nvl (-sum (solde_crediteur), 0)
               into   debit
               from   cfi_solde_6_7
               where  (pco_num like '6%' or pco_num like '186%') and ges_code = legescode and exe_ordre = exeordre;

               select nvl (sum (solde_crediteur), 0)
               into   credit
               from   cfi_solde_6_7
               where  (pco_num like '7%' or pco_num like '187%') and ges_code = legescode and exe_ordre = exeordre;

               if debit > credit then
                  -- RESULTAT SOLDE DEBITEUR 129
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
                  --  contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '129'   --PCONUM
                                                                                   );
               else
                  -- RESULTAT SOLDE CREDITEUR 120 --
                  -- contre-partie classe 7 crédit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              credit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'C',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
                  -- contre-partie classe 6 débit
                  ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                              libelle,
                                                                              --ECDLIBELLE,
                                                                              debit,   --ECDMONTANT,
                                                                              null,   --ECDSECONDAIRE,
                                                                              'D',   --ECDSENS,
                                                                              monecdordre,   --ECRORDRE,
                                                                              legescode,   --GESCODE,
                                                                              '120'   --PCONUM
                                                                                   );
               end if;
            end if;
         end loop;

         close lessacds;
      end if;

-- Solde classe 6 et 186
      open solde6;

      loop
         fetch solde6
         into  legescode,
               lepconum,
               lesolde;

         exit when solde6%notfound;

         if lesolde < 0 then
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        -lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'C',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         else
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'D',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         end if;
      end loop;

      close solde6;

-- Solde classe 7 et 187
      open solde7;

      loop
         fetch solde7
         into  legescode,
               lepconum,
               lesolde;

         exit when solde7%notfound;

         if lesolde < 0 then
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        -lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'C',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         else
            ecdordre := maracuja.api_plsql_journal.creerecrituredetail (null,   --ECDCOMMENTAIRE,
                                                                        libelle,
                                                                        --ECDLIBELLE,
                                                                        lesolde,   --ECDMONTANT,
                                                                        null,   --ECDSECONDAIRE,
                                                                        'D',   --ECDSENS,
                                                                        monecdordre,   --ECRORDRE,
                                                                        legescode,   --GESCODE,
                                                                        lepconum   --PCONUM
                                                                                );
         end if;
      end loop;

      close solde7;

-- on numerote pour eviter une numerotation automatique.
-- voir procedure numeroter_ecriture
      update ecriture
         set ecr_numero = 99999999
       where ecr_ordre = monecdordre;
   end;

--------------------------------------------------------------------------------
   procedure numeroter_ecriture (ecrordre integer)
   is
--RIVALLAND FREDRIC
--CRIG
--18/01/2005

      --a utiliser uniquement pour numeroter
--definitivement lecriture de solde des
--classes 6 et 7
      numero   integer;
   begin
      select ecr_numero
      into   numero
      from   ecriture
      where  ecr_ordre = ecrordre;

      if numero != 99999999 then
         raise_application_error (-20001, 'IMPOSSIBLE DE MODIFIER UNE ECRITURE DU JOURNAL !');
      else
         update ecriture
            set ecr_numero = 0
          where ecr_ordre = ecrordre;

         maracuja.api_plsql_journal.validerecriture (ecrordre);
      end if;
   end;

--------------------------------------------------------------------------------
   procedure annuler_ecriture (monecdordre integer)
   is
--RIVALLAND FREDRIC
--CRIG
--18/01/2005

      --a utiliser uniquement pour ANNULER
--definitivement l'ecriture de solde des
--classes 6 et 7
      numero   integer;
   begin
      select ecr_numero
      into   numero
      from   ecriture
      where  ecr_ordre = monecdordre;

      if numero != 99999999 then
         raise_application_error (-20001, 'IMPOSSIBLE DE SUPPRIMER UNE ECRITURE DU JOURNAL !');
      else
         delete from ecriture_detail
               where ecr_ordre = monecdordre;

         delete from ecriture
               where ecr_ordre = monecdordre;
      end if;
   end;
end;
/


