SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/3
-- Type : DML
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.0
-- Date de publication : 31/01/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- ATTENTION : l'execution de cette procedure peut prendre du temps suivant les cas.
----------------------------------------------

-- 

execute grhum.inst_patch_maracuja_1910;
commit;

drop procedure grhum.inst_patch_maracuja_1910;


