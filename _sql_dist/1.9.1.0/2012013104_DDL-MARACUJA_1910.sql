SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°3/3
-- Type : DDL
-- Schéma modifié :  MARACUJA
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.9.1.0
-- Date de publication : 31/01/2012
-- Licence : CeCILL version 2
--
--



----------------------------------------------
-- Changement des references de plan_comptable vers plan_comptable_exer
----------------------------------------------





---- ajouts d'index et contraintes de ref vers plan_comptable_exer au lieu de plan_comptable
CREATE INDEX MARACUJA.IDX_ECD_PCO_NUM_EX ON MARACUJA.ECRITURE_DETAIL(exe_ordre,PCO_NUM) TABLESPACE GFC_INDX;
ALTER TABLE MARACUJA.ECRITURE_detail ADD  CONSTRAINT FK_ECD_PCOEX FOREIGN KEY (EXE_ORDRE, PCO_NUM)
 REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;

alter table MARACUJA.BORDEREAU_BROUILLARD drop constraint FK_BORDEREAU_BROUILLARD_PCO_NU;
 ALTER TABLE MARACUJA.BORDEREAU_BROUILLARD ADD  CONSTRAINT FK_BORDEREAU_BROUILLARD_PCO_NU FOREIGN KEY (EXE_ORDRE, PCO_NUM) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.CHEQUE_BROUILLARD drop constraint FK_CHEQUE_BROUILLARD_PCO_NUM;
 ALTER TABLE MARACUJA.CHEQUE_BROUILLARD ADD  CONSTRAINT FK_CHEQUE_BROUILLARD_PCO_NUM FOREIGN KEY (EXE_ORDRE, PCO_NUM) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.GESTION_EXERCICE drop constraint FK_GESTION_EXERCICE_PCO_NUM181;
 ALTER TABLE MARACUJA.GESTION_EXERCICE ADD  CONSTRAINT FK_GESTION_EXERCICE_PCO_NUM181 FOREIGN KEY (EXE_ORDRE, PCO_NUM_181) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.GESTION_EXERCICE drop constraint FK_GESTION_EXERCICE_PCO_NUM185;
 ALTER TABLE MARACUJA.GESTION_EXERCICE ADD  CONSTRAINT FK_GESTION_EXERCICE_PCO_NUM185 FOREIGN KEY (EXE_ORDRE, PCO_NUM_185) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.MANDAT drop constraint FK_MANDAT_PCO_NUM;
 ALTER TABLE MARACUJA.MANDAT ADD  CONSTRAINT FK_MANDAT_PCO_NUM FOREIGN KEY (EXE_ORDRE, PCO_NUM) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.MANDAT_BROUILLARD drop constraint FK_MANDAT_BROUILLARD_PCO_NUM;
 ALTER TABLE MARACUJA.MANDAT_BROUILLARD ADD  CONSTRAINT FK_MANDAT_BROUILLARD_PCO_NUM FOREIGN KEY (EXE_ORDRE, PCO_NUM) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.MODE_PAIEMENT drop constraint FK_MODE_PAIEMENT_PCO_NUM_PAIEM;
ALTER TABLE MARACUJA.MODE_PAIEMENT ADD  CONSTRAINT FK_MODE_PAIEMENT_PCO_NUM_PAIEM FOREIGN KEY (EXE_ORDRE, PCO_NUM_PAIEMENT) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.MODE_PAIEMENT drop constraint FK_MODE_PAIEMENT_PCO_NUM_VISA;
ALTER TABLE MARACUJA.MODE_PAIEMENT ADD  CONSTRAINT FK_MODE_PAIEMENT_PCO_NUM_VISA FOREIGN KEY (EXE_ORDRE, PCO_NUM_VISA) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.MODE_RECOUVREMENT drop constraint FK_MODE_RECOUVREMENT_PCO_NUM_V;
ALTER TABLE MARACUJA.MODE_RECOUVREMENT ADD  CONSTRAINT FK_MODE_RECOUVREMENT_PCO_NUM_V FOREIGN KEY (EXE_ORDRE, PCO_NUM_visa) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.MODE_RECOUVREMENT drop constraint FK_MODE_RECOUVREMENT_PCO_NUM_P;
ALTER TABLE MARACUJA.MODE_RECOUVREMENT ADD  CONSTRAINT FK_MODE_RECOUVREMENT_PCO_NUM_P FOREIGN KEY (EXE_ORDRE, PCO_NUM_paiement) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.PLANCO_VISA drop constraint FK_PLANCO_VISA_PCO_NUM_TVA;
 ALTER TABLE MARACUJA.PLANCO_VISA ADD  CONSTRAINT FK_PLANCO_VISA_PCO_NUM_TVA FOREIGN KEY (EXE_ORDRE, PCO_NUM_TVA) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.PLANCO_VISA drop constraint FK_PLANCO_VISA_PCO_NUM_ORDONNA;
 ALTER TABLE MARACUJA.PLANCO_VISA ADD  CONSTRAINT FK_PLANCO_VISA_PCO_NUM_ORDONNA FOREIGN KEY (EXE_ORDRE, PCO_NUM_ORDONNATEUR) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.PLANCO_VISA drop constraint FK_PLANCO_VISA_PCO_NUM_CTREPAR;
 ALTER TABLE MARACUJA.PLANCO_VISA ADD  CONSTRAINT FK_PLANCO_VISA_PCO_NUM_CTREPAR FOREIGN KEY (EXE_ORDRE, PCO_NUM_CTREPARTIE) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.RECETTE drop constraint FK_RECETTE_PCO_NUM;
 ALTER TABLE MARACUJA.RECETTE ADD  CONSTRAINT FK_RECETTE_PCO_NUM FOREIGN KEY (EXE_ORDRE, PCO_NUM) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.REIMPUTATION drop constraint FK_REIMPUTATION_PCO_NUM_NOUVEA;
 ALTER TABLE MARACUJA.REIMPUTATION ADD  CONSTRAINT FK_REIMPUTATION_PCO_NUM_NOUVEA FOREIGN KEY (EXE_ORDRE, PCO_NUM_NOUVEAU) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.REIMPUTATION drop constraint FK_REIMPUTATION_PCO_NUM_ANCIEN;
 ALTER TABLE MARACUJA.REIMPUTATION ADD  CONSTRAINT FK_REIMPUTATION_PCO_NUM_ANCIEN FOREIGN KEY (EXE_ORDRE, PCO_NUM_ANCIEN) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;
alter table MARACUJA.TITRE_BROUILLARD drop constraint FK_TITRE_BROUILLARD_PCO_NUM;
 ALTER TABLE MARACUJA.TITRE_BROUILLARD ADD  CONSTRAINT FK_TITRE_BROUILLARD_PCO_NUM FOREIGN KEY (EXE_ORDRE, PCO_NUM) REFERENCES MARACUJA.PLAN_COMPTABLE_EXER (EXE_ORDRE, PCO_NUM) DEFERRABLE INITIALLY DEFERRED;


 

 

    update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 4 and TYAV_VERSION='1.9.1.0';
    commit;
/






